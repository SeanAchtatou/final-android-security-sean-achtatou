package com.kandian.cartoonapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.LocalActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.sax.Element;
import android.sax.EndElementListener;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.kandian.common.AsyncImageLoader;
import com.kandian.common.FavoriteAsset;
import com.kandian.common.HtmlUtil;
import com.kandian.common.Log;
import com.kandian.common.ObtainVideoService;
import com.kandian.common.PlayUrl;
import com.kandian.common.SiteUrls;
import com.kandian.common.SiteViewUrl;
import com.kandian.common.SystemConfigs;
import com.kandian.common.VideoAsset;
import com.kandian.common.VideoAssetMap;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import com.kandian.other.PreferenceSettingActivity;
import com.kandian.user.ShareFactory;
import com.kandian.user.UserService;
import com.kandian.user.favorite.UserFavoriteService;
import com.kandian.user.history.UserHistoryService;
import com.kandian.videoplayer.VideoPlayerActivity;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

public class AssetActivity extends Activity {
    private final int MSG_ALVOTE = 2;
    private final int MSG_NETWORK_PROBLEM = 3;
    private final int MSG_PLAYURLS = 0;
    private final int MSG_SHOW = 0;
    private final int MSG_VOTE = 1;
    /* access modifiers changed from: private */
    public String TAG = "AssetActivity";
    /* access modifiers changed from: private */
    public AssetActivity _context;
    /* access modifiers changed from: private */
    public VideoAsset asset = null;
    View.OnClickListener favoritesListener = new View.OnClickListener() {
        public void onClick(View v) {
            TextView favview = (TextView) LayoutInflater.from(AssetActivity.this).inflate((int) R.layout.text_entry_dialog, (ViewGroup) null).findViewById(R.id.name_view);
            if (AssetActivity.this.parentAsset == null) {
                AssetActivity.this.parentAsset = AssetActivity.this.asset;
            }
            favview.setText(AssetActivity.this.parentAsset.getAssetName());
            String userName = UserService.getInstance().getUsername();
            if (userName == null || userName.trim().length() == 0) {
                new AlertDialog.Builder(AssetActivity.this).setIcon((int) R.drawable.del).setTitle("未登录您无法收藏这个内容,现在是否要登录?").setPositiveButton((int) R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        UserService.getInstance().login(AssetActivity.this._context);
                    }
                }).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create().show();
                return;
            }
            final FavoriteAsset favoritesAsset = new FavoriteAsset(AssetActivity.this.parentAsset);
            Asynchronous asynchronous = new Asynchronous(AssetActivity.this._context);
            asynchronous.setLoadingMsg("收藏添加中,请稍等...");
            asynchronous.asyncProcess(new AsyncProcess() {
                public int process(Context context, Map<String, Object> map) throws Exception {
                    UserFavoriteService.getInstance().addFavorite(AssetActivity.this.getString(R.string.appcode), AssetActivity.this._context, favoritesAsset);
                    return 0;
                }
            });
            asynchronous.asyncCallback(new AsyncCallback() {
                public void callback(Context context, Map<String, Object> map, Message m) {
                    Toast.makeText(AssetActivity.this._context, "收藏成功!", 0).show();
                }
            });
            asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                public void handle(Context context, Exception e, Message m) {
                    Toast.makeText(AssetActivity.this._context, "收藏失败,请重试!", 0).show();
                }
            });
            asynchronous.start();
            if (AssetActivity.this.parentAsset == null) {
                AssetActivity.this.parentAsset = AssetActivity.this.asset;
            }
            String mytext = "收藏了" + AssetActivity.this.parentAsset.getAssetName() + " #视频快手分享# " + SiteViewUrl.getAssetUrl4W(AssetActivity.this.parentAsset.getAssetType(), AssetActivity.this.parentAsset.getAssetKey(), AssetActivity.this._context);
            Log.v(AssetActivity.this.TAG, "favorites text:" + mytext);
            ShareFactory.getInstance().sync(mytext, 2, AssetActivity.this._context, AssetActivity.this.parentAsset.getSmallPhotoImageUrl());
        }
    };
    /* access modifiers changed from: private */
    public ImageView imageView = null;
    private LocalActivityManager localActivityManager = null;
    Handler myViewUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            Button watchNowButton = (Button) AssetActivity.this.findViewById(R.id.watchnowbutton);
            Button moreWatchNowButton = (Button) AssetActivity.this.findViewById(R.id.morewatchnowbutton);
            String watchNowLabel = AssetActivity.this._context.getString(R.string.watchNowLabel);
            Button downloadNowButton = (Button) AssetActivity.this.findViewById(R.id.downloadnowbutton);
            Button moreDownloadNowButton = (Button) AssetActivity.this.findViewById(R.id.moredownloadnowbutton);
            String downloadNowLabel = AssetActivity.this._context.getString(R.string.downloadNowLabel);
            switch (msg.what) {
                case 0:
                    if (AssetActivity.this.siteUrls != null) {
                        ArrayList<SiteUrls.SiteUrl> playUrls = AssetActivity.this.siteUrls.getPlayUrls();
                        switch (playUrls.size()) {
                            case 0:
                                watchNowButton.setEnabled(false);
                                moreWatchNowButton.setEnabled(false);
                                break;
                            case 1:
                                String watchNowLabel2 = String.valueOf(watchNowLabel) + "[" + playUrls.get(0).getDisplayName() + "]";
                                watchNowButton.setText(watchNowLabel2);
                                Log.v(AssetActivity.this.TAG, "watchNowLabel is: " + watchNowLabel2);
                                watchNowButton.setEnabled(true);
                                moreWatchNowButton.setEnabled(false);
                                break;
                            default:
                                String watchNowLabel3 = String.valueOf(watchNowLabel) + "[" + playUrls.get(0).getDisplayName() + "]";
                                watchNowButton.setText(watchNowLabel3);
                                Log.v(AssetActivity.this.TAG, "watchNowLabel is: " + watchNowLabel3);
                                watchNowButton.setEnabled(true);
                                moreWatchNowButton.setText(AssetActivity.this._context.getString(R.string.moreWatchNowLabel));
                                moreWatchNowButton.setEnabled(true);
                                break;
                        }
                        ArrayList<SiteUrls.SiteUrl> downloadUrls = AssetActivity.this.siteUrls.getDownloadUrls();
                        switch (downloadUrls.size()) {
                            case 0:
                                downloadNowButton.setEnabled(false);
                                moreDownloadNowButton.setEnabled(false);
                                break;
                            case 1:
                                downloadNowButton.setText(String.valueOf(downloadNowLabel) + "[" + downloadUrls.get(0).getDisplayName() + "]");
                                downloadNowButton.setEnabled(true);
                                moreDownloadNowButton.setEnabled(false);
                                break;
                            default:
                                downloadNowButton.setText(String.valueOf(downloadNowLabel) + "[" + downloadUrls.get(0).getDisplayName() + "]");
                                downloadNowButton.setEnabled(true);
                                moreDownloadNowButton.setText(AssetActivity.this._context.getString(R.string.moreDownloadNowLabel));
                                moreDownloadNowButton.setEnabled(true);
                                break;
                        }
                    }
                    ((TextView) AssetActivity.this.findViewById(R.id.numberOfSources)).setText(AssetActivity.this._context.getString(R.string.number_of_sources_label).replace("0", new StringBuilder().append(AssetActivity.this.siteUrls.getDownloadUrls().size()).toString()));
                    break;
            }
            super.handleMessage(msg);
        }
    };
    Handler myVoteUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            ProgressBar pgb = (ProgressBar) AssetActivity.this.findViewById(R.id.pgbrating);
            TextView txtrating = (TextView) AssetActivity.this.findViewById(R.id.txtratingNum);
            SharedPreferences settings = AssetActivity.this.getSharedPreferences(AssetActivity.this.getString(R.string.prefs_name), 0);
            switch (msg.what) {
                case 2:
                    txtrating.setText(AssetActivity.this.getString(R.string.alreadyvote));
                    break;
                case 3:
                    Toast.makeText(AssetActivity.this, AssetActivity.this.getString(R.string.network_problem), 0).show();
                    break;
                default:
                    Map vote = (Map) msg.obj;
                    if (vote != null) {
                        int progress = (int) (Double.parseDouble(vote.get("goodrate").toString()) * 100.0d);
                        int badvoter = Integer.parseInt(vote.get("badvoter").toString().trim());
                        int goodvoter = Integer.parseInt(vote.get("goodvoter").toString().trim());
                        if (badvoter == 0 && goodvoter == 0) {
                            txtrating.setText((int) R.string.novote);
                        } else {
                            txtrating.setText(String.valueOf(progress) + "%/" + (badvoter + goodvoter) + "人投票");
                            pgb.setProgress(progress);
                            pgb.setSecondaryProgress(100);
                        }
                        SharedPreferences.Editor editor = settings.edit();
                        if (msg.what == 1) {
                            editor.putString(new StringBuilder(String.valueOf(AssetActivity.this.getAsset().getAssetId())).toString(), "vote");
                            editor.commit();
                            break;
                        }
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    };
    /* access modifiers changed from: private */
    public VideoAsset parentAsset = null;
    View.OnClickListener refreshListener = new View.OnClickListener() {
        public void onClick(View v) {
            AssetActivity.this.getVote("0", true);
            AssetActivity.this.getPlayUrl();
        }
    };
    View.OnClickListener settingListener = new View.OnClickListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(AssetActivity.this._context, PreferenceSettingActivity.class);
            intent.putExtra("SystemConfigFilter", true);
            AssetActivity.this.startActivity(intent);
        }
    };
    View.OnClickListener shareListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType(StringPart.DEFAULT_CONTENT_TYPE);
            intent.putExtra("android.intent.extra.SUBJECT", AssetActivity.this.getString(R.string.share_subject));
            intent.putExtra("android.intent.extra.TEXT", String.valueOf(AssetActivity.this.getAsset().getDisplayName(AssetActivity.this.getApplication())) + " " + SiteViewUrl.getAssetUrl4W(AssetActivity.this.asset.getAssetType(), AssetActivity.this.asset.getAssetKey(), AssetActivity.this._context) + " From " + AssetActivity.this.getString(R.string.app_name) + AssetActivity.this.getString(R.string.app_url));
            AssetActivity.this.startActivity(Intent.createChooser(intent, AssetActivity.this.getTitle()));
        }
    };
    SiteUrls siteUrls = new SiteUrls();

    public VideoAsset getAsset() {
        return this.asset;
    }

    public void setAsset(VideoAsset asset2) {
        this.asset = asset2;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this._context = this;
        NavigationBar.setup(this);
        Intent intent = getIntent();
        this.asset = VideoAssetMap.instance().getAsset(intent.getStringExtra("assetKey"), intent.getStringExtra("assetType"), getApplication());
        setLocalActivityManager(new LocalActivityManager(this, false));
        getLocalActivityManager().dispatchCreate(savedInstanceState);
        if (getAsset() != null) {
            Log.v("MovieAssetActivity", "Asset is " + getAsset().getAssetName());
            this.imageView = (ImageView) findViewById(R.id.assetImage);
            if (this.imageView != null) {
                this.imageView.setTag(getAsset().getBigImageUrl());
                Drawable cachedImage = AsyncImageLoader.instance().loadDrawable(getAsset().getBigImageUrl(), new AsyncImageLoader.ImageCallback() {
                    public void imageLoaded(Drawable imageDrawable, String imageUrl) {
                        if (AssetActivity.this.imageView != null) {
                            AssetActivity.this.imageView.setImageDrawable(imageDrawable);
                        }
                    }
                });
                if (cachedImage != null) {
                    this.imageView.setImageDrawable(cachedImage);
                }
            } else {
                Log.v(this.TAG, "imageView is null!");
            }
            if (!this.asset.getAssetType().equals("10")) {
                String assetKey = String.valueOf(this.asset.getAssetKey().split("_")[0]) + "_0";
                this.parentAsset = VideoAssetMap.instance().getAsset(assetKey, this.asset.getAssetType(), getApplication());
                Log.v("assetKey", assetKey);
            }
            ((ProgressBar) findViewById(R.id.pgbrating)).setProgressDrawable(getResources().getDrawable(R.drawable.voteprogress));
            getVote("0", true);
            ((Button) findViewById(R.id.btnlike)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    AssetActivity.this.getVote(HtmlUtil.TYPE_PLAY, false);
                }
            });
            ((Button) findViewById(R.id.btnnolike)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    AssetActivity.this.getVote(HtmlUtil.TYPE_DOWN, false);
                }
            });
            if (!this.asset.getType().equals("0") || this.asset.getAssetType().equals("10")) {
                View buttonRoot = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.asset_activity_link_buttons, (LinearLayout) findViewById(R.id.root));
                Button watchNowButton = (Button) buttonRoot.findViewById(R.id.watchnowbutton);
                watchNowButton.setEnabled(false);
                watchNowButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (AssetActivity.this.asset != null && AssetActivity.this.siteUrls.getPlayUrls() != null && AssetActivity.this.siteUrls.getPlayUrls().size() > 0) {
                            AssetActivity.this.startWatch(AssetActivity.this.siteUrls.getPlayUrls().get(0));
                        }
                    }
                });
                Button moreWatchNowButton = (Button) buttonRoot.findViewById(R.id.morewatchnowbutton);
                moreWatchNowButton.setEnabled(false);
                moreWatchNowButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ArrayList<SiteUrls.SiteUrl> playUrls;
                        if (AssetActivity.this.asset != null && (playUrls = AssetActivity.this.siteUrls.getPlayUrls()) != null && playUrls.size() > 0) {
                            CharSequence[] names = new CharSequence[playUrls.size()];
                            for (int i = 0; i < playUrls.size(); i++) {
                                Log.v(AssetActivity.this.TAG, "URL " + i + " " + playUrls.get(i).url);
                                names[i] = playUrls.get(i).getDisplayName();
                            }
                            new AlertDialog.Builder(AssetActivity.this).setTitle((int) R.string.select_source_dialog_title).setItems(names, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AssetActivity.this.startWatch(AssetActivity.this.siteUrls.getPlayUrls().get(which));
                                }
                            }).create().show();
                        }
                    }
                });
                Button downloadNowButton = (Button) buttonRoot.findViewById(R.id.downloadnowbutton);
                downloadNowButton.setEnabled(false);
                downloadNowButton.setOnClickListener(new View.OnClickListener() {
                    /* Debug info: failed to restart local var, previous not found, register: 3 */
                    public void onClick(View v) {
                        if (AssetActivity.this.asset != null) {
                            ArrayList<SiteUrls.SiteUrl> downloadUrls = AssetActivity.this.siteUrls.getDownloadUrls();
                            if (downloadUrls == null || downloadUrls.size() <= 0) {
                                Log.v(AssetActivity.this.TAG, "downloadUrls is null.");
                            } else {
                                AssetActivity.this.startDownload(downloadUrls.get(0));
                            }
                        }
                    }
                });
                Button moreDownloadNowButton = (Button) buttonRoot.findViewById(R.id.moredownloadnowbutton);
                moreDownloadNowButton.setEnabled(false);
                moreDownloadNowButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ArrayList<SiteUrls.SiteUrl> downloadUrls;
                        if (AssetActivity.this.asset != null && (downloadUrls = AssetActivity.this.siteUrls.getDownloadUrls()) != null && downloadUrls.size() > 0) {
                            CharSequence[] names = new CharSequence[downloadUrls.size()];
                            for (int i = 0; i < downloadUrls.size(); i++) {
                                names[i] = downloadUrls.get(i).getDisplayName();
                            }
                            new AlertDialog.Builder(AssetActivity.this).setTitle((int) R.string.select_source_down_dialog_title).setItems(names, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AssetActivity.this.startDownload(AssetActivity.this.siteUrls.getDownloadUrls().get(which));
                                }
                            }).create().show();
                        }
                    }
                });
                getPlayUrl();
                return;
            }
            return;
        }
        Log.v(this.TAG, "asset is null!");
        Toast.makeText(this, R.string.asset_is_null, 0).show();
        finish();
    }

    public void getPlayUrl() {
        new Thread() {
            public void run() {
                ArrayList<PlayUrl> urls = ObtainVideoService.getNewPlayUrls(AssetActivity.this.asset, AssetActivity.this.getApplication());
                AssetActivity.this.siteUrls.newInitialize(urls, SystemConfigs.instance(AssetActivity.this.getApplication()).getSiteConfigs(), AssetActivity.this.getApplication());
                Message message = Message.obtain(AssetActivity.this.myViewUpdateHandler);
                message.what = 0;
                if (urls != null) {
                    message.arg1 = 1;
                } else {
                    message.arg1 = 0;
                }
                message.sendToTarget();
            }
        }.start();
    }

    public void getVote(String type, final boolean isshow) {
        String v;
        SharedPreferences settings = getSharedPreferences(getString(R.string.prefs_name), 0);
        String assetid = new StringBuilder(String.valueOf(getAsset().getAssetId())).toString();
        String cookValue = settings.getString(assetid, "");
        Log.v("message", "type:" + type + "assetid=" + assetid + "cookValue=" + cookValue);
        if (isshow || "".equals(cookValue)) {
            final String voteUrl = String.valueOf(getString(R.string.voteurl)) + "&assetid=" + assetid + "&assettype=" + getAsset().getAssetType() + "&type=" + type;
            Log.v("sentvoteUrl=", voteUrl);
            new Thread() {
                public void run() {
                    Map votemes = new HashMap();
                    Message m = Message.obtain(AssetActivity.this.myVoteUpdateHandler);
                    try {
                        BufferedReader br = new BufferedReader(new InputStreamReader(new URL(voteUrl).openStream()));
                        StringBuffer sb = new StringBuffer("");
                        while (true) {
                            String s = br.readLine();
                            if (s == null) {
                                break;
                            }
                            sb.append(String.valueOf(s) + "\r\n");
                        }
                        br.close();
                        String[] temp = sb.toString().replace("{", "").replace("}", "").split(",");
                        for (String split : temp) {
                            String[] tep = split.split(":");
                            votemes.put(tep[0].replaceAll("\"", ""), tep[1].replaceAll("\"", ""));
                            Log.v(tep[0].replaceAll("\"", ""), tep[1].replaceAll("\"", ""));
                        }
                        Log.v("strvote===", sb.toString());
                        if (isshow) {
                            m.what = 0;
                        } else {
                            m.what = 1;
                        }
                        m.obj = votemes;
                        m.sendToTarget();
                    } catch (Exception e) {
                        e.printStackTrace();
                        m.what = 3;
                        m.sendToTarget();
                    }
                }
            }.start();
            if (!type.equals("0")) {
                if (type.equals(HtmlUtil.TYPE_PLAY)) {
                    v = ",好片";
                } else {
                    v = ",烂片";
                }
                if (this.parentAsset == null) {
                    this.parentAsset = this.asset;
                }
                ShareFactory.getInstance().sync("评价了  " + this.parentAsset.getAssetName() + v + "! #视频快手分享# " + SiteViewUrl.getAssetUrl4W(this.parentAsset.getAssetType(), this.parentAsset.getAssetKey(), this._context), 1, this._context, this.parentAsset.getSmallPhotoImageUrl());
                return;
            }
            return;
        }
        Message m = Message.obtain(this.myVoteUpdateHandler);
        m.what = 2;
        m.sendToTarget();
    }

    public ArrayList<String> parse(String playAddressServiceUrl) {
        RootElement root = new RootElement("DOCUMENT");
        final ArrayList<String> urls = new ArrayList<>();
        Element url = root.getChild("asset").getChild("playurls").getChild("url");
        url.setEndElementListener(new EndElementListener() {
            public void end() {
            }
        });
        url.setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("")) {
                    urls.add(new String(body));
                }
            }
        });
        try {
            InputStream input = (InputStream) new URL(playAddressServiceUrl).getContent();
            if (input != null) {
                Xml.parse(input, Xml.Encoding.UTF_8, root.getContentHandler());
            } else {
                Log.v(this.TAG, "inputStream is null: " + playAddressServiceUrl);
            }
            return urls;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void startWatch(final SiteUrls.SiteUrl siteUrl) {
        Asynchronous asynchronous = new Asynchronous(this._context);
        asynchronous.setLoadingMsg(getString(R.string.get_video_ing));
        asynchronous.asyncProcess(new AsyncProcess() {
            public int process(Context _context, Map<String, Object> map) throws Exception {
                boolean z;
                ArrayList<String> videoUrls = null;
                switch (siteUrl.urlType) {
                    case 1:
                        videoUrls = ObtainVideoService.getVideos(siteUrl.url, AssetActivity.this.getApplication(), 1);
                        break;
                    case 2:
                        videoUrls = ObtainVideoService.getVideos(siteUrl.url, AssetActivity.this.getApplication(), 2);
                        break;
                }
                VideoAsset aVideoAsset = AssetActivity.this.parentAsset;
                if (aVideoAsset == null) {
                    aVideoAsset = AssetActivity.this.asset;
                }
                UserHistoryService.getInstance().recordHistory(aVideoAsset, HtmlUtil.PLATFORM_ANDROID, AssetActivity.this.getString(R.string.appcode), HtmlUtil.TYPE_PLAY, _context, aVideoAsset.getAssetIdX(), aVideoAsset.getShowTime());
                if (videoUrls != null) {
                    z = true;
                } else {
                    z = false;
                }
                if (z && (videoUrls.size() > 0)) {
                    Intent intent = new Intent();
                    intent.setClass(_context, VideoPlayerActivity.class);
                    intent.putStringArrayListExtra("urls", videoUrls);
                    if (siteUrl.url != null) {
                        intent.putExtra("referer", siteUrl.url);
                    }
                    AssetActivity.this.startActivity(intent);
                } else {
                    Toast.makeText(_context, AssetActivity.this.getString(R.string.flvcat_exception_parsefail), 1).show();
                }
                return 0;
            }
        });
        asynchronous.asyncCallback(new AsyncCallback() {
            public void callback(Context _context, Map<String, Object> map, Message m) {
                HtmlUtil.statistics(AssetActivity.this.asset.getAssetType(), AssetActivity.this.asset.getAssetId(), AssetActivity.this.asset.getItemId(), HtmlUtil.PLATFORM_ANDROID, AssetActivity.this.getString(R.string.appcode), HtmlUtil.TYPE_DOWN, _context);
            }
        });
        asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
            public void handle(Context _context, Exception e, Message m) {
                Toast.makeText(_context, e.getMessage(), 1).show();
            }
        });
        asynchronous.start();
    }

    public void startDownload(final SiteUrls.SiteUrl siteUrl) {
        Asynchronous asynchronous = new Asynchronous(this._context);
        asynchronous.setLoadingMsg(getString(R.string.get_video_ing));
        asynchronous.asyncProcess(new AsyncProcess() {
            public int process(Context _context, Map<String, Object> map) throws Exception {
                boolean z;
                ArrayList<String> videoUrls = null;
                switch (siteUrl.urlType) {
                    case 1:
                        videoUrls = ObtainVideoService.getVideos(siteUrl.url, AssetActivity.this.getApplication(), 1);
                        break;
                    case 2:
                        videoUrls = ObtainVideoService.getVideos(siteUrl.url, AssetActivity.this.getApplication(), 2);
                        break;
                }
                if (videoUrls != null) {
                    z = true;
                } else {
                    z = false;
                }
                if (z && (videoUrls.size() > 0)) {
                    Intent intent = new Intent();
                    intent.setClass(_context, DownloadServiceActivity.class);
                    intent.putExtra("videoName", AssetActivity.this.asset.getDisplayName(AssetActivity.this.getApplication()));
                    intent.putExtra("videoType", siteUrl.urlType);
                    intent.putExtra("refererPage", siteUrl.url);
                    intent.putStringArrayListExtra("videoUrls", videoUrls);
                    AssetActivity.this.startActivity(intent);
                } else {
                    Toast.makeText(_context, AssetActivity.this.getString(R.string.flvcat_exception_parsefail), 1).show();
                }
                return 0;
            }
        });
        asynchronous.asyncCallback(new AsyncCallback() {
            public void callback(Context _context, Map<String, Object> map, Message m) {
                HtmlUtil.statistics(AssetActivity.this.asset.getAssetType(), AssetActivity.this.asset.getAssetId(), AssetActivity.this.asset.getItemId(), HtmlUtil.PLATFORM_ANDROID, AssetActivity.this.getString(R.string.appcode), HtmlUtil.TYPE_DOWN, _context);
            }
        });
        asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
            public void handle(Context _context, Exception e, Message m) {
                Toast.makeText(_context, e.getMessage(), 1).show();
            }
        });
        asynchronous.start();
    }

    public String getSwf(String url) {
        String result;
        if (url.startsWith("http://v.youku.com/v_show/id_") && url.endsWith(".html")) {
            result = "http://player.youku.com/player.php/sid/" + url.substring("http://v.youku.com/v_show/id_".length(), url.length() - ".html".length()) + "/v.swf";
        } else if (!url.startsWith("http://hd.openv.com/mov_play-hdmovie_") || !url.endsWith(".html")) {
            result = url;
        } else {
            result = "http://img.openv.tv/hd/swf/hd_player.swf?pid=hdmovie_" + url.substring("http://hd.openv.com/mov_play-hdmovie_".length(), url.length() - ".html".length()) + "&q=tv=0,vtype=1,hd=1,random=8981212065,category=,channel=3&autostart=true&playerUseStatus=2";
        }
        Log.v(this.TAG, "swf is" + result);
        return result;
    }

    public String getResult(String url, int maxLength) {
        StringBuffer result = new StringBuffer();
        try {
            InputStream input = (InputStream) new URL(url).getContent();
            int c = input.read();
            int count = 0;
            while (c != -1 && count < maxLength) {
                result.append((char) c);
                c = input.read();
                count++;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return result.toString();
    }

    public String getRedirectedUrl(String url) {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);
        HttpContext context = new BasicHttpContext();
        try {
            HttpResponse response = httpClient.execute(httpget, context);
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new IOException(response.getStatusLine().toString());
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        String currentUrl = String.valueOf(((HttpHost) context.getAttribute("http.target_host")).toURI()) + ((HttpUriRequest) context.getAttribute("http.request")).getURI();
        Log.v(this.TAG, "real flv is at " + currentUrl);
        return currentUrl;
    }

    public void setLocalActivityManager(LocalActivityManager localActivityManager2) {
        this.localActivityManager = localActivityManager2;
    }

    public LocalActivityManager getLocalActivityManager() {
        return this.localActivityManager;
    }

    public void setTabStyle(TextView tab) {
        TabUtil.setTabStyle(this, tab);
    }

    public String getSourceName(String sourceUrl) {
        String result = sourceUrl;
        URI uri = URI.create(sourceUrl);
        if (uri != null) {
            String host = uri.getHost();
            int lastIndex = host.lastIndexOf(46);
            int firstIndex = host.indexOf(46);
            if (!(lastIndex == -1 || firstIndex == -1 || firstIndex >= lastIndex)) {
                result = host.substring(firstIndex + 1);
            }
        }
        Log.v(this.TAG, "sourceUrl " + sourceUrl + " is at " + result);
        return result;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.assetmenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh /*2131361965*/:
                this.refreshListener.onClick(findViewById(R.id.menu_refresh));
                return true;
            case R.id.menu_setting /*2131361966*/:
                this.settingListener.onClick(findViewById(R.id.menu_refresh));
                return true;
            case R.id.menu_ksfamily /*2131361967*/:
            case R.id.menu_kshelp /*2131361968*/:
            case R.id.menu_ksabout /*2131361969*/:
            case R.id.menu_login /*2131361970*/:
            default:
                return super.onOptionsItemSelected(item);
            case R.id.menu_favorites /*2131361971*/:
                this.favoritesListener.onClick(findViewById(R.id.menu_favorites));
                return true;
            case R.id.menu_share /*2131361972*/:
                this.shareListener.onClick(findViewById(R.id.menu_share));
                return true;
        }
    }
}
