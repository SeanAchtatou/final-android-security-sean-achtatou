package com.kandian.cartoonapp;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.kandian.common.Log;
import com.kandian.common.PreferenceSetting;
import com.kandian.videoplayer.ThirdPartyVideoPlayerActivity;
import java.io.File;
import java.util.ArrayList;

public class DownloadExploreListActivity extends ListActivity {
    static String TAG = "DownloadExploreListActivity";
    ArrayList<String> fileNames = null;
    /* access modifiers changed from: private */
    public String taskDownloadDir = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.download_explore_list_activity);
        ((Button) findViewById(R.id.backbutton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DownloadExploreListActivity.this.finish();
            }
        });
        this.taskDownloadDir = getIntent().getStringExtra("taskDownloadDir");
        this.fileNames = getIntent().getStringArrayListExtra("filenames");
        if (this.fileNames != null) {
            setListAdapter(new FileItemAdaptor(this, R.layout.download_explore_row, this.fileNames));
        }
        getListView().setTextFilterEnabled(true);
        CheckBox continuePlay_checkbox = (CheckBox) findViewById(R.id.continuePlay_checkbox);
        continuePlay_checkbox.setChecked(PreferenceSetting.getContinuePlay(getApplication()));
        continuePlay_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                PreferenceSetting.setContinuePlay(DownloadExploreListActivity.this.getApplication(), isChecked);
            }
        });
    }

    private class FileItemAdaptor extends ArrayAdapter<String> {
        private ArrayList<String> items;

        public FileItemAdaptor(Context context, int textViewResourceId, ArrayList<String> items2) {
            super(context, textViewResourceId, items2);
            this.items = items2;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) DownloadExploreListActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.download_explore_row, (ViewGroup) null);
            }
            File f = new File(this.items.get(position));
            TextView left = (TextView) v.findViewById(R.id.filename);
            if (ThirdPartyVideoPlayerActivity.playHis == null || ThirdPartyVideoPlayerActivity.playHis.get(DownloadExploreListActivity.this.taskDownloadDir) == null || ThirdPartyVideoPlayerActivity.playHis.get(DownloadExploreListActivity.this.taskDownloadDir).intValue() != position) {
                left.setTextColor(-1);
            } else {
                left.setTextColor(-16711936);
            }
            left.setText(f.getName());
            TextView right = (TextView) v.findViewById(R.id.statustext);
            if (f.exists()) {
                right.setText(DownloadExploreListActivity.this.getString(R.string.file_downloaded_text));
            } else {
                right.setText(DownloadExploreListActivity.this.getString(R.string.file_unfinished_text));
            }
            return v;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.v(TAG, "Click on position" + position);
        File f = new File(this.fileNames.get(position));
        if (f.exists()) {
            ThirdPartyVideoPlayerActivity.playHis.put(this.taskDownloadDir, Integer.valueOf(position));
            if (!((CheckBox) findViewById(R.id.continuePlay_checkbox)).isChecked()) {
                Intent intent = new Intent();
                intent.setDataAndType(Uri.fromFile(f), "video/*");
                intent.setAction("android.intent.action.VIEW");
                startActivity(intent);
                return;
            }
            Intent intent2 = new Intent();
            intent2.setClass(this, ThirdPartyVideoPlayerActivity.class);
            ArrayList<String> fileUris = new ArrayList<>();
            for (int i = 0; i < this.fileNames.size(); i++) {
                fileUris.add(Uri.fromFile(new File(this.fileNames.get(i))).toString());
            }
            intent2.putStringArrayListExtra("urls", fileUris);
            intent2.putExtra("currPlayUrlIndex", position);
            intent2.putExtra("taskDownloadDir", this.taskDownloadDir);
            startActivity(intent2);
            return;
        }
        Toast.makeText(this, String.valueOf(f.getName()) + getString(R.string.download_notfinished_text), 0).show();
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        ((BaseAdapter) getListAdapter()).notifyDataSetChanged();
        super.onRestart();
    }
}
