package com.kandian.cartoonapp;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.kandian.common.AsyncImageLoader;
import com.kandian.common.Log;
import com.kandian.common.StringUtil;
import com.kandian.common.SystemConfigs;
import com.kandian.ksfamily.KSApp;
import com.kandian.other.KSAboutActivity;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import javax.xml.parsers.SAXParserFactory;

public class MicroBlogActivity extends ListActivity {
    /* access modifiers changed from: private */
    public static String TAG = "MicroBlogActivity";
    private final int MSG_LIST = 0;
    private final int MSG_NETWORK_PROBLEM = 2;
    private final int MSG_REFRESH_LIST = 1;
    /* access modifiers changed from: private */
    public Context _context = null;
    /* access modifiers changed from: private */
    public AsyncImageLoader asyncImageLoader = null;
    View.OnClickListener backListener = new View.OnClickListener() {
        public void onClick(View v) {
            MicroBlogActivity.this.finish();
        }
    };
    View.OnClickListener ksaboutListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(MicroBlogActivity.this._context, KSAboutActivity.class);
            KSApp ksApp = new KSApp();
            ksApp.setAppname(MicroBlogActivity.this._context.getString(R.string.app_name));
            ksApp.setPartner(MicroBlogActivity.this._context.getString(R.string.partner));
            ksApp.setAppicon(R.drawable.kscartoon);
            ksApp.setDescription(MicroBlogActivity.this._context.getString(R.string.app_description));
            try {
                ksApp.setPackagename(MicroBlogActivity.this._context.getPackageManager().getPackageInfo(MicroBlogActivity.this._context.getPackageName(), 0).packageName);
                ksApp.setVersionname(MicroBlogActivity.this._context.getPackageManager().getPackageInfo(MicroBlogActivity.this._context.getPackageName(), 0).versionName);
                ksApp.setVersioncode(MicroBlogActivity.this._context.getPackageManager().getPackageInfo(MicroBlogActivity.this._context.getPackageName(), 0).versionCode);
            } catch (PackageManager.NameNotFoundException e) {
                ksApp.setPackagename("");
                ksApp.setVersionname("");
            }
            Bundle mBundle = new Bundle();
            mBundle.putSerializable("ksAppInfo", ksApp);
            intent.putExtras(mBundle);
            MicroBlogActivity.this.startActivity(intent);
        }
    };
    protected ArrayAdapter<CharSequence> mAdapter;
    Handler myViewUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            LinearLayout statusBar = (LinearLayout) MicroBlogActivity.this.findViewById(R.id.status);
            MicroBlogAdapter myAdatper = (MicroBlogAdapter) MicroBlogActivity.this.getListAdapter();
            switch (msg.what) {
                case 1:
                    myAdatper.clear();
                case 0:
                    MicroBlogResults microBlogResults = (MicroBlogResults) msg.obj;
                    if (microBlogResults != null) {
                        for (int i = 0; i < microBlogResults.getSize(); i++) {
                            myAdatper.add(microBlogResults.get(i));
                        }
                        statusBar.setVisibility(8);
                    }
                    ((BaseAdapter) MicroBlogActivity.this.getListAdapter()).notifyDataSetChanged();
                    break;
                case 2:
                    Toast.makeText(MicroBlogActivity.this, MicroBlogActivity.this.getString(R.string.network_problem), 0).show();
                    break;
            }
            if (myAdatper.getCount() == 0) {
                statusBar.setVisibility(0);
                TextView emptyStatusView = (TextView) MicroBlogActivity.this.findViewById(16908292);
                if (emptyStatusView != null) {
                    emptyStatusView.setText(MicroBlogActivity.this.getString(R.string.nodata));
                }
            }
            super.handleMessage(msg);
        }
    };
    View.OnClickListener refreshListener = new View.OnClickListener() {
        public void onClick(View v) {
            MicroBlogActivity.this.getData(true);
        }
    };
    private SystemConfigs systemConfigs = null;
    private ProgressDialog v_ProgressDialog = null;
    /* access modifiers changed from: private */
    public long validDataThreadId = -1;

    /* access modifiers changed from: private */
    public MicroBlogResults getMicroBlogResults(String assetsListUrl) {
        String packageName = "";
        try {
            packageName = this._context.getPackageManager().getPackageInfo(this._context.getPackageName(), 0).packageName;
        } catch (PackageManager.NameNotFoundException e) {
        }
        String assetsListUrl2 = StringUtil.replace(assetsListUrl, "{packageName}", packageName);
        MicroBlogSaxHandler saxHandler = new MicroBlogSaxHandler();
        try {
            InputStream input = (InputStream) fetch(assetsListUrl2);
            if (input != null) {
                SAXParserFactory.newInstance().newSAXParser().parse(input, saxHandler);
                return saxHandler.getMicroBlogResults();
            }
            throw new IOException("inputStream is null:" + assetsListUrl2);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private Object fetch(String address) throws MalformedURLException, IOException {
        Log.v(TAG, "Fetching  " + address);
        return new URL(address).getContent();
    }

    /* access modifiers changed from: protected */
    public synchronized void getData(final boolean refresh) {
        Log.v(TAG, "refresh/isGettingData" + refresh);
        Thread t = new Thread() {
            public void run() {
                String searchQueryUrl = MicroBlogActivity.this.getString(R.string.microblogurl);
                int currentCount = ((MicroBlogAdapter) MicroBlogActivity.this.getListAdapter()).getCount();
                MicroBlogResults microBlogResults = MicroBlogActivity.this.getMicroBlogResults(searchQueryUrl);
                if (microBlogResults != null) {
                    ArrayList<MicroBlog> microBlogs = microBlogResults.getMicroBlogs();
                    Log.v(MicroBlogActivity.TAG, "microBlogResults size: " + microBlogs.size() + " currentCount: " + currentCount);
                    if (!(microBlogs.size() == 0 && currentCount == microBlogResults.getTotalcount()) && MicroBlogActivity.this.validDataThreadId == getId()) {
                        Message m = Message.obtain(MicroBlogActivity.this.myViewUpdateHandler);
                        if (refresh) {
                            m.what = 1;
                        } else {
                            m.what = 0;
                        }
                        m.obj = microBlogResults;
                        m.sendToTarget();
                        return;
                    }
                    return;
                }
                Message m2 = Message.obtain(MicroBlogActivity.this.myViewUpdateHandler);
                m2.what = 2;
                m2.sendToTarget();
            }
        };
        this.validDataThreadId = t.getId();
        Log.v(TAG, "Change DataThreadId to " + this.validDataThreadId);
        t.start();
    }

    public void onCreate(Bundle savedInstanceState) {
        this.v_ProgressDialog = ProgressDialog.show(this, "Please wait...", "Retrieving data ...", true);
        super.onCreate(savedInstanceState);
        this.asyncImageLoader = AsyncImageLoader.instance();
        setContentView((int) R.layout.microblog_activity);
        NavigationBar.setup(this);
        setListAdapter(new MicroBlogAdapter(this, R.layout.microblogrow, new ArrayList()));
        this._context = this;
        getListView().setTextFilterEnabled(true);
        this.v_ProgressDialog.dismiss();
    }

    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.v(TAG, "onResume()");
        if (this.systemConfigs == null) {
            this.systemConfigs = SystemConfigs.instance(getApplication());
        }
        if (getListAdapter() == null || getListAdapter().getCount() == 0) {
            getData(false);
        }
        super.onResume();
    }

    private class MicroBlogAdapter extends ArrayAdapter<MicroBlog> {
        public MicroBlogAdapter(Context context, int textViewResourceId, ArrayList<MicroBlog> items) {
            super(context, textViewResourceId, items);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) MicroBlogActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.microblogrow, (ViewGroup) null);
            }
            MicroBlog microBlog = (MicroBlog) getItem(position);
            if (microBlog != null) {
                TextView tt = (TextView) v.findViewById(R.id.mbname);
                if (tt != null) {
                    tt.setText(Html.fromHtml(StringUtil.TwitterCode(StringUtil.HtmlCode(microBlog.getContent_text()))));
                    tt.setMovementMethod(LinkMovementMethod.getInstance());
                    tt.setFocusable(false);
                }
                ImageView thumbnailmage = (ImageView) v.findViewById(R.id.mbithumbnailmage);
                if (thumbnailmage != null) {
                    String imageUrl = microBlog.getMicroBlogUsers().get(0).getProfile_image_url();
                    thumbnailmage.setTag(imageUrl);
                    Bitmap cachedImage = MicroBlogActivity.this.asyncImageLoader.loadBitmap(imageUrl, new AsyncImageLoader.ImageCallback1() {
                        public void imageLoaded(Bitmap imageDrawable, String imageUrl) {
                            ImageView imageViewByTag = (ImageView) MicroBlogActivity.this.getListView().findViewWithTag(imageUrl);
                            if (imageViewByTag != null) {
                                imageViewByTag.setImageBitmap(imageDrawable);
                            }
                        }
                    });
                    if (cachedImage != null) {
                        thumbnailmage.setImageBitmap(cachedImage);
                    }
                }
            }
            return v;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.v(TAG, "donothing");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.assetlistmenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh /*2131361965*/:
                this.refreshListener.onClick(getListView());
                return true;
            case R.id.menu_ksabout /*2131361969*/:
                this.ksaboutListener.onClick(getListView());
                return true;
            case R.id.menu_back /*2131361974*/:
                this.backListener.onClick(getListView());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
