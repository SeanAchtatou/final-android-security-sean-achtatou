package com.kandian.cartoonapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.kandian.cartoonapp.DownloadService;
import com.kandian.common.DownloadTask;
import com.kandian.common.Log;
import com.kandian.common.MemoryStatus;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import com.kandian.other.PreferenceSettingActivity;
import com.kandian.videoplayer.VideoPlayerActivity;
import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class DownloadServiceActivity extends ListActivity implements DownloadService.TaskProgressCallback {
    /* access modifiers changed from: private */
    public static String TAG = "DownloadServiceActivity";
    private final int MSG_DOWNLOADTASK = 1;
    private final int MSG_LIST = 0;
    View.OnClickListener backListener = new View.OnClickListener() {
        public void onClick(View v) {
            DownloadServiceActivity.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public DownloadService downloadService = null;
    /* access modifiers changed from: private */
    public ServiceConnection mConnection = null;
    private boolean mIsBound = false;
    Handler myViewUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            TextView emptyStatusView;
            switch (msg.what) {
                case 0:
                    if (DownloadServiceActivity.this.downloadService != null) {
                        DownloadServiceActivity.this.tasks.clear();
                        DownloadServiceActivity.this.tasks.addAll(DownloadServiceActivity.this.downloadService.getAllDownloadTasks());
                        if (DownloadServiceActivity.this.tasks.size() == 0 && (emptyStatusView = (TextView) DownloadServiceActivity.this.findViewById(16908292)) != null) {
                            emptyStatusView.setText(DownloadServiceActivity.this.getString(R.string.no_downloadtask));
                        }
                        ((BaseAdapter) DownloadServiceActivity.this.getListAdapter()).notifyDataSetChanged();
                        break;
                    } else {
                        Log.v(DownloadServiceActivity.TAG, "downloadService is null in handleMessage");
                        return;
                    }
                case 1:
                    ((BaseAdapter) DownloadServiceActivity.this.getListAdapter()).notifyDataSetChanged();
                    break;
            }
            DownloadServiceActivity.this.checkTotalTask();
            super.handleMessage(msg);
        }
    };
    View.OnClickListener refreshListener = new View.OnClickListener() {
        public void onClick(View v) {
            DownloadServiceActivity.this.checkMemoryStatus();
            DownloadServiceActivity.this.getData();
        }
    };
    View.OnClickListener settingListener = new View.OnClickListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(DownloadServiceActivity.this, PreferenceSettingActivity.class);
            intent.putExtra("SystemConfigFilter", true);
            DownloadServiceActivity.this.startActivity(intent);
        }
    };
    /* access modifiers changed from: private */
    public ArrayList<DownloadTask> tasks = new ArrayList<>();
    private int totalResultCount = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.downloadservice_activity);
        Log.v(TAG, "onCreate");
        NavigationBar.setup(this);
        TextView emptyStatusView = (TextView) findViewById(16908292);
        if (emptyStatusView != null) {
            emptyStatusView.setText(getString(R.string.retrieving));
        }
        setListAdapter(new TaskAdaptor(this, R.layout.downloadservicerow, this.tasks));
        getListView().setTextFilterEnabled(true);
        initDownload();
    }

    public void onStart() {
        Log.v(TAG, "onStart()");
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        checkMemoryStatus();
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        checkMemoryStatus();
        getData();
        super.onResume();
    }

    private void initDownload() {
        if (this.mConnection == null) {
            this.mConnection = new ServiceConnection() {
                public void onServiceConnected(ComponentName className, IBinder service) {
                    DownloadServiceActivity.this.downloadService = ((DownloadService.DownloadBinder) service).getService();
                    DownloadServiceActivity.this.initDownloadTasks();
                    Log.v(DownloadServiceActivity.TAG, "downloadService is connected");
                }

                public void onServiceDisconnected(ComponentName className) {
                    DownloadServiceActivity.this.downloadService = null;
                    Log.v(DownloadServiceActivity.TAG, "downloadService is disconnected");
                }
            };
        }
        doBindService();
    }

    /* access modifiers changed from: private */
    public void checkMemoryStatus() {
        String ksMediaFileRootDir = PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.setting_download_dir_key), null);
        Log.v(TAG, "ksMediaFileRootDir = " + ksMediaFileRootDir);
        ((TextView) findViewById(R.id.memormgs)).setText(String.valueOf(getString(R.string.totalSize)) + MemoryStatus.getTotalExternalMemorySizeText(ksMediaFileRootDir) + ", " + getString(R.string.alreadySize) + MemoryStatus.getAlreadyUsedExternalMemorySizeText(ksMediaFileRootDir) + ", " + getString(R.string.freeSize) + MemoryStatus.getAvailableExternalMemorySizeText(ksMediaFileRootDir));
    }

    /* access modifiers changed from: private */
    public void checkTotalTask() {
        TextView warning_TV = (TextView) findViewById(R.id.warning);
        if (getListAdapter().getCount() == DownloadService.MAX_TOTAL_TASK_COUNT) {
            warning_TV.setVisibility(0);
            warning_TV.setText("已达最大下载量: " + getListAdapter().getCount() + CookieSpec.PATH_DELIM + DownloadService.MAX_TOTAL_TASK_COUNT);
        } else if (getListAdapter().getCount() > DownloadService.MAX_TOTAL_TASK_COUNT - 5) {
            warning_TV.setVisibility(0);
            warning_TV.setText("下载任务: " + getListAdapter().getCount() + CookieSpec.PATH_DELIM + DownloadService.MAX_TOTAL_TASK_COUNT);
        } else {
            warning_TV.setVisibility(8);
        }
    }

    /* access modifiers changed from: package-private */
    public void doBindService() {
        startService(new Intent(this, DownloadService.class));
        if (bindService(new Intent(this, DownloadService.class), this.mConnection, 1)) {
            Log.v(TAG, "succeeding in binding service");
        } else {
            Log.v(TAG, "failed in binding service");
        }
        this.mIsBound = true;
    }

    /* access modifiers changed from: package-private */
    public void doUnbindService() {
        if (this.mIsBound) {
            unbindService(this.mConnection);
            this.mIsBound = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        Log.v(TAG, "handling new intent");
        setIntent(intent);
        handleIntent(intent);
    }

    /* access modifiers changed from: private */
    public void handleIntent(Intent intent) {
        Log.v(TAG, "Handling intent");
        if (intent != null) {
            String videoName = intent.getStringExtra("videoName");
            if (videoName == null) {
                videoName = "";
            }
            int videoType = intent.getIntExtra("videoType", 2);
            String refererPage = intent.getStringExtra("refererPage");
            if (this.downloadService != null && refererPage != null) {
                this.downloadService.startDownload(videoName, videoType, refererPage);
            } else if (this.downloadService == null) {
                Log.v(TAG, "cannot handle intent: downloadService is null.");
            } else {
                Log.v(TAG, "cannot handle intent: refererPage is null.");
            }
            getData();
        }
    }

    /* access modifiers changed from: protected */
    public void getData() {
        new Thread() {
            public void run() {
                Message m = Message.obtain(DownloadServiceActivity.this.myViewUpdateHandler);
                m.what = 0;
                m.sendToTarget();
            }
        }.start();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.i(TAG, "Stop looping the child thread's message queue");
        ((TaskAdaptor) getListAdapter()).setStopMonitoring(true);
        doUnbindService();
        super.onDestroy();
    }

    private class TaskAdaptor extends ArrayAdapter<DownloadTask> {
        final int MSG_PROGRESS_UPDATE = 0;
        private ArrayList<DownloadTask> items;
        boolean stopMontoring = false;
        ThreadGroup tg = new ThreadGroup("monitoring threads");

        public boolean isStopMonitoring() {
            return this.stopMontoring;
        }

        public void setStopMonitoring(boolean v) {
            this.stopMontoring = v;
        }

        public TaskAdaptor(Context context, int textViewResourceId, ArrayList<DownloadTask> items2) {
            super(context, textViewResourceId, items2);
            this.items = items2;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) DownloadServiceActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.downloadservicerow, (ViewGroup) null);
            }
            DownloadTask task = this.items.get(position);
            if (task != null) {
                TextView title = (TextView) v.findViewById(R.id.title);
                TextView url = (TextView) v.findViewById(R.id.url);
                if (title != null) {
                    title.setText(task.getVideoName());
                }
                if (url != null) {
                    url.setText(task.getRefererPage());
                }
                Button playButton = (Button) v.findViewById(R.id.play);
                if (task.getNumOfDownloaded() > 0) {
                    final DownloadTask downloadTask = task;
                    playButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.setClass(DownloadServiceActivity.this, VideoPlayerActivity.class);
                            ArrayList<String> fileUris = new ArrayList<>();
                            ArrayList<String> filePaths = downloadTask.getDownloadedFiles();
                            for (int i = 0; i < filePaths.size(); i++) {
                                fileUris.add(Uri.fromFile(new File(filePaths.get(i))).toString());
                            }
                            intent.putStringArrayListExtra("urls", fileUris);
                            String referer = downloadTask.getRefererPage();
                            if (referer != null) {
                                intent.putExtra("referer", referer);
                            }
                            DownloadServiceActivity.this.startActivity(intent);
                        }
                    });
                }
                TextView status = (TextView) v.findViewById(R.id.status);
                if (status != null) {
                    String statusText = "";
                    switch (task.getTaskStatus()) {
                        case 0:
                            statusText = DownloadServiceActivity.this.getString(R.string.task_status_preparing);
                            break;
                        case 1:
                            statusText = DownloadServiceActivity.this.getString(R.string.task_status_ready);
                            if (task.getCurrentTransmitCount() > 0) {
                                statusText = "第" + (task.getCurrentTransmitCount() + 1) + "次" + statusText;
                                break;
                            }
                            break;
                        case 2:
                            if (task.getCurrentDownloadSpeed() < 10) {
                                if (task.getCurrentDownloadSpeed() != -1) {
                                    statusText = DownloadServiceActivity.this.getString(R.string.task_status_busy);
                                    break;
                                } else {
                                    statusText = DownloadServiceActivity.this.getString(R.string.task_status_ready);
                                    break;
                                }
                            } else {
                                statusText = String.valueOf(task.getCurrentDownloadSpeed()) + "KB/s．";
                                break;
                            }
                        case 3:
                            statusText = DownloadServiceActivity.this.getString(R.string.task_status_paused);
                            break;
                        case 4:
                            statusText = DownloadServiceActivity.this.getString(R.string.task_status_finished);
                            break;
                    }
                    String progressText = DownloadServiceActivity.this.getProgressText(task, playButton);
                    Log.v(DownloadServiceActivity.TAG, "status is " + task.getTaskStatus() + ", " + progressText);
                    status.setText(String.valueOf(statusText) + progressText);
                }
                ((Button) v.findViewById(R.id.explore)).setOnClickListener(new View.OnClickListener(task) {
                    Dialog dialog;
                    DialogInterface.OnClickListener mClickListener;
                    private final /* synthetic */ DownloadTask val$task;

                    {
                        this.val$task = r3;
                        this.mClickListener = new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d("DIALOG", "DISMISS");
                                File f = new File(r3.getDownloadedFiles().get(which));
                                if (f.exists()) {
                                    Intent intent = new Intent();
                                    intent.setDataAndType(Uri.fromFile(f), "video/*");
                                    intent.setAction("android.intent.action.VIEW");
                                    DownloadServiceActivity.this.startActivity(intent);
                                    return;
                                }
                                Toast.makeText(DownloadServiceActivity.this, String.valueOf(f.getName()) + DownloadServiceActivity.this.getString(R.string.download_notfinished_text), 0).show();
                            }
                        };
                    }

                    public void onClick(View v) {
                        File downloadDir = new File(this.val$task.getDownloadDir());
                        if (!downloadDir.exists() || !downloadDir.isDirectory()) {
                            Toast.makeText(DownloadServiceActivity.this, DownloadServiceActivity.this.getString(R.string.download_notstarted_text), 0).show();
                            return;
                        }
                        ArrayList<String> files = this.val$task.getDownloadedFiles();
                        Intent intent = new Intent();
                        intent.setClass(DownloadServiceActivity.this, DownloadExploreListActivity.class);
                        intent.putStringArrayListExtra("filenames", files);
                        intent.putExtra("taskDownloadDir", this.val$task.getDownloadDir());
                        DownloadServiceActivity.this.startActivity(intent);
                    }
                });
                Button stopButton = (Button) v.findViewById(R.id.stop);
                final DownloadTask downloadTask2 = task;
                final Button button = stopButton;
                stopButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (DownloadServiceActivity.this.downloadService != null && !downloadTask2.isFinished()) {
                            if (downloadTask2.isPaused()) {
                                Asynchronous asynchronous = new Asynchronous(DownloadServiceActivity.this);
                                asynchronous.setLoadingMsg("开启续传...");
                                final DownloadTask downloadTask = downloadTask2;
                                asynchronous.asyncProcess(new AsyncProcess() {
                                    public int process(Context _context, Map<String, Object> map) throws Exception {
                                        setCallbackParameter("result", Boolean.valueOf(DownloadServiceActivity.this.downloadService.restartDownloadTask(downloadTask)));
                                        return 0;
                                    }
                                });
                                final Button button = button;
                                asynchronous.asyncCallback(new AsyncCallback() {
                                    public void callback(Context _context, Map<String, Object> outputparameter, Message m) {
                                        if (((Boolean) outputparameter.get("result")).booleanValue()) {
                                            button.setText((int) R.string.download_stop_text);
                                        }
                                    }
                                });
                                asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                                    public void handle(Context _context, Exception e, Message m) {
                                        Toast.makeText(_context, "下载任务续传开启失败，请检查网络并重试", 1).show();
                                    }
                                });
                                asynchronous.start();
                                return;
                            }
                            button.setText((int) R.string.download_start_text);
                            DownloadServiceActivity.this.downloadService.stopDownloadTask(downloadTask2);
                        }
                    }
                });
                if (task.isFinished()) {
                    stopButton.setEnabled(false);
                } else {
                    stopButton.setEnabled(true);
                    if (task.isPaused()) {
                        stopButton.setText(R.string.download_start_text);
                    } else {
                        stopButton.setText(R.string.download_stop_text);
                    }
                }
                final DownloadTask downloadTask3 = task;
                ((Button) v.findViewById(R.id.delete)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        AlertDialog.Builder title = new AlertDialog.Builder(DownloadServiceActivity.this).setTitle((int) R.string.downloadtask_delete_dialog_text);
                        final DownloadTask downloadTask = downloadTask3;
                        title.setPositiveButton((int) R.string.downloadtask_delete_dialog_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (DownloadServiceActivity.this.downloadService != null) {
                                    Asynchronous asynchronous = new Asynchronous(DownloadServiceActivity.this);
                                    asynchronous.setLoadingMsg("正在删除下载任务...");
                                    final DownloadTask downloadTask = downloadTask;
                                    asynchronous.asyncProcess(new AsyncProcess() {
                                        public int process(Context _context, Map<String, Object> map) throws Exception {
                                            DownloadServiceActivity.this.downloadService.deleteDownloadTask(downloadTask);
                                            return 0;
                                        }
                                    });
                                    asynchronous.asyncCallback(new AsyncCallback() {
                                        public void callback(Context _context, Map<String, Object> map, Message m) {
                                            DownloadServiceActivity.this.getData();
                                            DownloadServiceActivity.this.checkMemoryStatus();
                                        }
                                    });
                                    asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                                        public void handle(Context _context, Exception e, Message m) {
                                            Toast.makeText(_context, "下载任务删除失败", 1).show();
                                        }
                                    });
                                    asynchronous.start();
                                }
                            }
                        }).setNegativeButton((int) R.string.downloadtask_delete_dialog_cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).create().show();
                    }
                });
                Log.v(DownloadServiceActivity.TAG, "registerProgressUpdater for " + task.getTaskId() + " " + DownloadServiceActivity.this);
                DownloadServiceActivity.this.downloadService.registerProgressUpdater(task, DownloadServiceActivity.this);
            }
            ProgressBar progressBar = (ProgressBar) v.findViewById(R.id.progress_horizontal);
            progressBar.setMax(100);
            if (task.isFinished()) {
                progressBar.setVisibility(4);
            } else {
                progressBar.setVisibility(0);
                progressBar.setProgress(task.getCurrentFileProgress());
            }
            return v;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.downloadservicemenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh /*2131361965*/:
                this.refreshListener.onClick(getListView());
                return true;
            case R.id.menu_setting /*2131361966*/:
                this.settingListener.onClick(getListView());
                return true;
            case R.id.menu_back /*2131361974*/:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void setTotalResultCount(int totalResultCount2) {
        this.totalResultCount = totalResultCount2;
    }

    public int getTotalResultCount() {
        return this.totalResultCount;
    }

    /* access modifiers changed from: private */
    public void initDownloadTasks() {
        Log.v(TAG, "DownloadService.serviceStatus = " + DownloadService.serviceStatus);
        if (DownloadService.serviceStatus == DownloadService.SERVICE_STATUS_NONE) {
            Asynchronous asynchronous = new Asynchronous(this);
            asynchronous.setLoadingMsg("下载服务载入中...");
            asynchronous.asyncProcess(new AsyncProcess() {
                public int process(Context _context, Map<String, Object> map) throws Exception {
                    Log.v(DownloadServiceActivity.TAG, "initializeDownloadTasks");
                    Log.v(DownloadServiceActivity.TAG, "[downloadService] = " + DownloadServiceActivity.this.downloadService);
                    Log.v(DownloadServiceActivity.TAG, "[mConnection] = " + DownloadServiceActivity.this.mConnection);
                    Looper.prepare();
                    if (DownloadServiceActivity.this.mConnection == null || DownloadServiceActivity.this.downloadService == null) {
                        return 0;
                    }
                    Log.v(DownloadServiceActivity.TAG, "initializeDownloadTasks begin");
                    DownloadServiceActivity.this.downloadService.initializeDownloadTasks();
                    DownloadServiceActivity.this.handleIntent(DownloadServiceActivity.this.getIntent());
                    return 0;
                }
            });
            asynchronous.asyncCallback(new AsyncCallback() {
                public void callback(Context _context, Map<String, Object> map, Message m) {
                    DownloadServiceActivity.this.getData();
                }
            });
            asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                public void handle(Context _context, Exception e, Message m) {
                    Toast.makeText(_context, "网络超时，下载服务载入失败", 1).show();
                }
            });
            asynchronous.start();
            return;
        }
        handleIntent(getIntent());
    }

    /* access modifiers changed from: private */
    public String getProgressText(DownloadTask task, Button pButton) {
        if (task.getNumOfDownloaded() > 0) {
            pButton.setEnabled(true);
        } else {
            pButton.setEnabled(false);
        }
        int currentIndex = task.getNumOfDownloaded() + 1;
        if (currentIndex <= task.getNumOfChapters()) {
            return getString(R.string.progress_text_template).replace("0/0", String.valueOf(currentIndex) + CookieSpec.PATH_DELIM + task.getNumOfChapters());
        }
        return getString(R.string.progress_finished_text_template).replace("0", new StringBuilder(String.valueOf(task.getNumOfChapters())).toString());
    }

    public void onPause() {
        super.onPause();
    }

    public void progressUpdated() {
        Log.v(TAG, "sending MSG_DOWNLOAD message");
        Message m = Message.obtain(this.myViewUpdateHandler);
        m.what = 1;
        m.sendToTarget();
    }
}
