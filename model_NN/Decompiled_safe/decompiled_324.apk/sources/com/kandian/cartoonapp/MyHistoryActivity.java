package com.kandian.cartoonapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.kandian.common.AsyncImageLoader;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.StatisticsUtil;
import com.kandian.common.UpdateUtil;
import com.kandian.common.activity.CommonListActivity;
import com.kandian.common.activity.CommonResultSet;
import com.kandian.user.history.UserHistoryAsset;
import com.kandian.user.history.UserHistoryService;
import java.util.ArrayList;

public class MyHistoryActivity extends CommonListActivity {
    private static String TAG = "MyHistoryActivity";
    private AsyncImageLoader asyncImageLoader = null;
    View.OnClickListener clearListener = new View.OnClickListener() {
        public void onClick(View v) {
            UserHistoryService.getInstance().clearHistory(MyHistoryActivity.this.context);
            MyHistoryActivity.this.clearRowAdapter();
        }
    };
    /* access modifiers changed from: private */
    public MyHistoryActivity context = this;

    /* access modifiers changed from: protected */
    public void onResume() {
        if (getListAdapter() == null || getListAdapter().getCount() == 0) {
            getData();
        }
        UpdateUtil.checkUpdate(this);
        StatisticsUtil.updateCount(this);
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void beforeGetData() {
    }

    /* access modifiers changed from: protected */
    public void afterGetData(BaseAdapter ba, CommonResultSet ars) {
        if (ars.getSize() == 0) {
            TextView emptyStatusView = (TextView) findViewById(R.id.no_favorites);
            emptyStatusView.setText(this.context.getString(R.string.str_ks_no_history));
            emptyStatusView.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public void errorGetData() {
    }

    /* access modifiers changed from: protected */
    public CommonResultSet buildData() {
        CommonResultSet results = new CommonResultSet();
        results.setResults(UserHistoryService.getInstance().getHistory(this.context));
        return results;
    }

    /* access modifiers changed from: protected */
    public View buildRow(Object rowObj, int position, View v, ViewGroup parent) {
        UserHistoryAsset userHistoryAsset = (UserHistoryAsset) rowObj;
        ImageView imageView = (ImageView) v.findViewById(R.id.assetImage);
        TextView tt = (TextView) v.findViewById(R.id.toptext);
        TextView categoryt = (TextView) v.findViewById(R.id.categorytext);
        if (imageView != null) {
            imageView.setTag(userHistoryAsset.getImageUrl());
            Bitmap cachedImage = this.asyncImageLoader.loadBitmap(userHistoryAsset.getImageUrl(), new AsyncImageLoader.ImageCallback1() {
                public void imageLoaded(Bitmap imageDrawable, String imageUrl) {
                    ImageView imageViewByTag = (ImageView) MyHistoryActivity.this.getListView().findViewWithTag(imageUrl);
                    if (imageViewByTag != null) {
                        imageViewByTag.setImageBitmap(imageDrawable);
                    }
                }
            });
            if (cachedImage != null) {
                imageView.setImageBitmap(cachedImage);
            }
            imageView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                }
            });
            if (tt != null) {
                String type = "";
                if (userHistoryAsset.getAsseType().equals("10")) {
                    type = getString(R.string.type_movie);
                } else if (userHistoryAsset.getAsseType().equals("11")) {
                    type = getString(R.string.type_series);
                } else if (userHistoryAsset.getAsseType().equals("12")) {
                    type = getString(R.string.type_variety);
                }
                tt.setText(String.valueOf(type) + userHistoryAsset.getAssetName());
            }
            if (categoryt != null) {
                categoryt.setText(userHistoryAsset.getOrigin());
            }
            TextView bt = (TextView) v.findViewById(R.id.bottomtext);
            if (bt != null) {
                double vote = userHistoryAsset.getVote();
                String voteString = "-";
                if (vote >= 0.6d) {
                    voteString = String.valueOf(new Double(100.0d * vote).intValue()) + "%";
                    bt.setTextColor(getResources().getColor(R.color.good_vote_color));
                } else if (vote >= 0.0d) {
                    voteString = String.valueOf(new Double(100.0d * vote).intValue()) + "%";
                    bt.setTextColor(getResources().getColor(R.color.bad_vote_color));
                } else {
                    bt.setTextColor(getResources().getColor(R.drawable.white));
                }
                bt.setText(voteString);
            }
        }
        return v;
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.v(TAG, "Starting AssetActivity at position" + position);
        UserHistoryAsset userHistoryAsset = (UserHistoryAsset) getItemObject(position);
        Intent intent = new Intent();
        if (userHistoryAsset.getAsseType().equals("10")) {
            intent.setClass(this, MovieAssetActivity.class);
        } else {
            intent.setClass(this, MovieAssetActivity.class);
        }
        intent.putExtra("assetKey", userHistoryAsset.getAssetKey());
        intent.putExtra("assetType", userHistoryAsset.getAsseType());
        startActivity(intent);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.myviews_activity);
        this.asyncImageLoader = AsyncImageLoader.instance();
        initList(this.context, new ArrayList(), R.layout.assetrow, false, 1);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_clear_history /*2131361973*/:
                this.clearListener.onClick(findViewById(R.id.menu_clear_history));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.historymenu, menu);
        return true;
    }
}
