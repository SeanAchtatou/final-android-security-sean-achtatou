package com.kandian.cartoonapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.kandian.common.DownloadTask;
import com.kandian.common.FileUtil;
import com.kandian.common.Log;
import com.kandian.common.PreferenceSetting;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

public class DownloadService extends Service {
    public static int MAX_TOTAL_TASK_COUNT = 50;
    public static int SERVICE_STATUS_DONE = 2;
    public static int SERVICE_STATUS_INIT = 1;
    public static int SERVICE_STATUS_NONE = 0;
    /* access modifiers changed from: private */
    public static String TAG = "DownloadService";
    private static final Class[] mStartForegroundSignature = {Integer.TYPE, Notification.class};
    private static final Class[] mStopForegroundSignature = {Boolean.TYPE};
    public static int serviceStatus = 0;
    int MAX_ACTIVE_TASK_COUNT = 3;
    ArrayList<DownloadTask> activeTasks = new ArrayList<>();
    HashMap<String, ArrayList<WeakReference<TaskProgressCallback>>> callbacksMap = new HashMap<>();
    private File downloadDir = null;
    final long downloadingNotificationId = 1;
    private boolean initializeToStart = false;
    private final IBinder mBinder = new DownloadBinder();
    private Method mStartForeground;
    private Object[] mStartForegroundArgs = new Object[2];
    private Method mStopForeground;
    private Object[] mStopForegroundArgs = new Object[1];
    private File mediaFileDir = null;
    Handler myViewUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            Toast.makeText(DownloadService.this, (String) msg.obj, 0).show();
            super.handleMessage(msg);
        }
    };
    NotificationManager nm = null;
    HashMap<Long, RemoteViews> remoteViewsMap = new HashMap<>();
    ArrayList<DownloadTask> tasks = new ArrayList<>();
    ArrayList<DownloadTask> toStartTasks = new ArrayList<>();

    public interface TaskProgressCallback {
        void progressUpdated();
    }

    /* access modifiers changed from: package-private */
    public void startForegroundCompat(int id, Notification notification) {
        if (this.mStartForeground != null) {
            this.mStartForegroundArgs[0] = Integer.valueOf(id);
            this.mStartForegroundArgs[1] = notification;
            try {
                this.mStartForeground.invoke(this, this.mStartForegroundArgs);
            } catch (InvocationTargetException e) {
                Log.w(TAG, "Unable to invoke startForeground", e);
            } catch (IllegalAccessException e2) {
                Log.w(TAG, "Unable to invoke startForeground", e2);
            }
        } else {
            setForeground(true);
            this.nm.notify(id, notification);
        }
    }

    /* access modifiers changed from: package-private */
    public void stopForegroundCompat(int id, boolean removeFlag) {
        if (this.mStopForeground != null) {
            if (removeFlag) {
                this.mStopForegroundArgs[0] = Boolean.TRUE;
            } else {
                this.mStopForegroundArgs[0] = Boolean.FALSE;
            }
            try {
                this.mStopForeground.invoke(this, this.mStopForegroundArgs);
            } catch (InvocationTargetException e) {
                Log.w(TAG, "Unable to invoke stopForeground", e);
            } catch (IllegalAccessException e2) {
                Log.w(TAG, "Unable to invoke stopForeground", e2);
            }
        } else {
            this.nm.cancel(id);
            setForeground(false);
        }
    }

    public class DownloadBinder extends Binder {
        public DownloadBinder() {
        }

        /* access modifiers changed from: package-private */
        public DownloadService getService() {
            return DownloadService.this;
        }
    }

    public void onCreate() {
        PreferenceSetting.setDownloadDir(this);
        PreferenceSetting.setMediaFileDir(this, null);
        this.initializeToStart = PreferenceSetting.getDownloadAutoResume(this);
        Log.v(TAG, "----downloadDir is " + PreferenceSetting.getDownloadDir());
        Log.v(TAG, "----mediaFileDir is " + PreferenceSetting.getMediaFileDir());
        this.downloadDir = new File(PreferenceSetting.getDownloadDir());
        this.mediaFileDir = new File(PreferenceSetting.getMediaFileDir());
        try {
            if (this.downloadDir.exists()) {
                Log.v(TAG, "downloadDir exists: " + this.downloadDir.getAbsolutePath());
            } else if (!this.downloadDir.mkdirs()) {
                Log.v(TAG, "creating " + this.downloadDir.getAbsolutePath() + "fails.");
            } else {
                Log.v(TAG, "creating " + this.downloadDir.getAbsolutePath() + "succeeds.");
            }
            if (this.mediaFileDir.exists()) {
                Log.v(TAG, "mediaFileDir exists: " + this.mediaFileDir.getAbsolutePath());
            } else if (!this.mediaFileDir.mkdirs()) {
                Log.v(TAG, "creating " + this.mediaFileDir.getAbsolutePath() + "fails.");
            } else {
                Log.v(TAG, "creating " + this.mediaFileDir.getAbsolutePath() + "succeeds.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            sendMessage(getString(R.string.download_service_error_suggestion));
        }
        Log.v(TAG, "downloadService is created");
        serviceStatus = SERVICE_STATUS_NONE;
        this.nm = (NotificationManager) getSystemService("notification");
        try {
            this.mStartForeground = getClass().getMethod("startForeground", mStartForegroundSignature);
            this.mStopForeground = getClass().getMethod("stopForeground", mStopForegroundSignature);
        } catch (NoSuchMethodException e2) {
            this.mStopForeground = null;
            this.mStartForeground = null;
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v(TAG, "Received start id " + startId + ": " + intent);
        return 1;
    }

    public void onDestroy() {
        stopAllActiveTasks();
        stopForegroundCompat(1, true);
        Log.v(TAG, "downloadService is destroyed");
        if (this.nm != null) {
            this.nm.cancelAll();
        }
    }

    public void stopAllActiveTasks() {
        for (int i = 0; i < this.activeTasks.size(); i++) {
            this.activeTasks.get(i).stopDownload();
        }
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    /* access modifiers changed from: protected */
    public void startDownloadTask(DownloadTask task) {
        startDownloadTask(task, true);
    }

    /* access modifiers changed from: protected */
    public void queueDownloadTask(DownloadTask task) {
        startDownloadTask(task, false);
    }

    public boolean restartDownloadTask(DownloadTask task) {
        if (!task.isPaused() || this.toStartTasks.contains(task) || this.activeTasks.contains(task)) {
            sendMessage(String.valueOf(task.getVideoName()) + getString(R.string.cannot_restart_text));
            return false;
        }
        if (this.activeTasks.size() < this.MAX_ACTIVE_TASK_COUNT) {
            this.activeTasks.add(task);
            task.startDownload(true);
        } else {
            this.toStartTasks.add(task);
            sendMessage(String.valueOf(task.getVideoName()) + getString(R.string.put_into_queue_text));
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void startDownloadTask(DownloadTask task, boolean immediate) {
        startDownloadTask(task, immediate, true);
    }

    /* access modifiers changed from: protected */
    public void startDownloadTask(final DownloadTask task, boolean immediate, boolean toStart) {
        if (this.callbacksMap.get(task.getKey()) != null) {
            sendMessage(String.valueOf(task.getVideoName()) + getString(R.string.already_existed));
            Log.v(TAG, "Task Key is " + task.getKey());
        } else if (this.tasks.size() >= MAX_TOTAL_TASK_COUNT) {
            sendMessage(getString(R.string.over_max_downloads_text));
        } else {
            this.callbacksMap.put(task.getKey(), new ArrayList());
            this.tasks.add(0, task);
            task.setTaskCallback(new DownloadTask.TaskCallback() {
                public void taskFinished() {
                    DownloadService.this.removeActiveTask(task);
                    DownloadService.this.sendMessage(String.valueOf(task.getVideoName()) + DownloadService.this.getString(R.string.download_finished_text));
                    taskProgressUpdated();
                    DownloadService.this.taskFinishNotification(task);
                }

                public void taskReady() {
                    DownloadService.this.saveDownloadTask(task);
                    taskProgressUpdated();
                    DownloadService.this.initTaskNotification(task);
                }

                public void taskFailed() {
                    DownloadService.this.removeActiveTask(task);
                    DownloadService.this.sendMessage(String.valueOf(task.getVideoName()) + DownloadService.this.getString(R.string.download_failed_text));
                    taskProgressUpdated();
                    DownloadService.this.taskFailNotification(task);
                }

                public void taskProgressUpdated() {
                    ArrayList<WeakReference<TaskProgressCallback>> progressCallbacks = DownloadService.this.callbacksMap.get(task.getKey());
                    if (progressCallbacks != null) {
                        for (int i = progressCallbacks.size() - 1; i >= 0; i--) {
                            TaskProgressCallback c = (TaskProgressCallback) ((WeakReference) progressCallbacks.get(i)).get();
                            if (c != null) {
                                Log.v(DownloadService.TAG, String.valueOf(task.getVideoName()) + " No " + i + " TaskProgressCallback ");
                                c.progressUpdated();
                                return;
                            }
                            progressCallbacks.remove(i);
                        }
                    }
                }

                public void networkStauts() {
                    DownloadService.this.sendMessage(DownloadService.this.getString(R.string.setting_download_wifi_alert));
                }
            });
            saveDownloadTask(task);
            if (!task.isFinished() && toStart) {
                if (this.activeTasks.size() >= this.MAX_ACTIVE_TASK_COUNT || !immediate) {
                    this.toStartTasks.add(0, task);
                    sendMessage(getString(R.string.put_into_queue_text));
                    return;
                }
                this.activeTasks.add(0, task);
                task.startDownload();
            }
        }
    }

    public void initializeDownloadTasks() {
        if (serviceStatus != SERVICE_STATUS_NONE) {
            Log.v(TAG, "serviceStatus is not Service_STATUS_NONE.");
            return;
        }
        Log.v(TAG, "serviceStatus is Service_STATUS_NONE.");
        serviceStatus = SERVICE_STATUS_INIT;
        File[] taskFiles = this.downloadDir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String filename) {
                if (filename.endsWith(".task")) {
                    return true;
                }
                return false;
            }
        });
        Arrays.sort(taskFiles, new Comparator<File>() {
            public int compare(File f1, File f2) {
                return f1.getName().compareTo(f2.getName());
            }
        });
        if (taskFiles != null && taskFiles.length > 0) {
            for (int i = 0; i < taskFiles.length; i++) {
                Log.v(TAG, "initializing " + taskFiles[i].getName());
                DownloadTask task = DownloadTask.initializeFromFile(taskFiles[i], getApplication());
                if (task != null) {
                    startDownloadTask(task, true, this.initializeToStart);
                } else {
                    Log.v(TAG, "failed in initializing " + taskFiles[i].getName());
                }
            }
        }
        serviceStatus = SERVICE_STATUS_DONE;
        activateQueuedTask();
        FileUtil.storageDefrag(this.mediaFileDir, taskFiles);
    }

    /* access modifiers changed from: private */
    public void removeActiveTask(DownloadTask task) {
        this.activeTasks.remove(task);
        activateQueuedTask();
        if (this.activeTasks.size() == 0) {
            stopForegroundCompat(1, true);
        }
    }

    private synchronized void activateQueuedTask() {
        while (this.toStartTasks.size() > 0 && this.activeTasks.size() < this.MAX_ACTIVE_TASK_COUNT) {
            DownloadTask next = this.toStartTasks.remove(this.toStartTasks.size() - 1);
            if (next != null) {
                this.activeTasks.add(0, next);
                next.startDownload(true);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void saveDownloadTask(DownloadTask task) {
        try {
            FileOutputStream out = new FileOutputStream(new File(this.downloadDir, String.valueOf(task.getTaskId()) + ".task"));
            task.generateProperties().store(out, "");
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void stopDownloadTask(DownloadTask task) {
        task.stopDownload();
        this.toStartTasks.remove(task);
        this.activeTasks.remove(task);
    }

    /* access modifiers changed from: protected */
    public void deleteDownloadTask(DownloadTask task) {
        stopDownloadTask(task);
        if (this.callbacksMap.get(task.getKey()) != null) {
            task.clean();
            File taskFile = new File(this.downloadDir, String.valueOf(task.getTaskId()) + ".task");
            if (taskFile.exists()) {
                taskFile.delete();
            }
            this.tasks.remove(task);
            this.callbacksMap.remove(task.getKey());
            taskRemoveNotification(task);
        }
    }

    public void startDownload(String videoName, int videoType, String refererPage) {
        if (refererPage != null) {
            String vName = "";
            if (videoName != null) {
                vName = videoName;
            }
            DownloadTask task = new DownloadTask(vName, videoType, refererPage, getApplication());
            if (serviceStatus == SERVICE_STATUS_DONE) {
                startDownloadTask(task);
            } else {
                queueDownloadTask(task);
            }
        }
    }

    public ArrayList<DownloadTask> getAllDownloadTasks() {
        return this.tasks;
    }

    public void registerProgressUpdater(DownloadTask task, TaskProgressCallback callback) {
        ArrayList<WeakReference<TaskProgressCallback>> callbacks = this.callbacksMap.get(task.getKey());
        if (callbacks != null) {
            callbacks.clear();
            Log.v(TAG, "adding a new callback to " + callbacks.size() + " callbacks for " + task.getTaskId() + " " + task.getVideoName());
            callbacks.add(new WeakReference(callback));
            return;
        }
        Log.v(TAG, "no callbacks for " + task.getTaskId() + " " + task.getVideoName());
    }

    /* access modifiers changed from: private */
    public void sendMessage(String content) {
        Message m = Message.obtain(this.myViewUpdateHandler);
        m.obj = content;
        m.sendToTarget();
    }

    public void initTaskNotification(DownloadTask task) {
        if (this.activeTasks.size() == 1) {
            String title = String.valueOf(getString(R.string.app_name)) + getString(R.string.service_is_running);
            int icon = getApplicationInfo().icon;
            Notification nf = new Notification(icon, title, System.currentTimeMillis());
            nf.icon = icon;
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, DownloadServiceActivity.class), 0);
            nf.contentIntent = contentIntent;
            nf.setLatestEventInfo(this, title, title, contentIntent);
            startForegroundCompat(1, nf);
        }
    }

    public void taskFinishNotification(DownloadTask task) {
        try {
            String title = task.getVideoName();
            int icon = getResources().getIdentifier(String.valueOf(getPackageName()) + ":drawable/taskdone", null, null);
            Notification nf = new Notification(icon, title, System.currentTimeMillis());
            nf.contentView = new RemoteViews(getPackageName(), (int) R.layout.download_notification);
            nf.icon = icon;
            nf.contentView.setTextViewText(R.id.download_taskname, title);
            nf.contentView.setTextViewText(R.id.download_taskstatus, "下载完成 ");
            nf.contentView.setProgressBar(R.id.download_pb, 100, 100, false);
            nf.contentView.setImageViewResource(R.id.download_icon, icon);
            nf.contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, DownloadServiceActivity.class), 0);
            nf.flags = 4;
            nf.flags |= 16;
            this.nm.notify((int) task.getTaskId(), nf);
        } catch (Exception e) {
        }
    }

    public void taskFailNotification(DownloadTask task) {
        try {
            String title = task.getVideoName();
            int icon = getResources().getIdentifier(String.valueOf(getPackageName()) + ":drawable/taskpause", null, null);
            Notification nf = new Notification(icon, title, System.currentTimeMillis());
            nf.contentView = new RemoteViews(getPackageName(), (int) R.layout.download_notification);
            nf.icon = icon;
            nf.contentView.setTextViewText(R.id.download_taskname, title);
            nf.contentView.setTextViewText(R.id.download_taskstatus, "下载停止");
            nf.contentView.setProgressBar(R.id.download_pb, 100, task.getOverallProgress(), false);
            nf.contentView.setImageViewResource(R.id.download_icon, icon);
            nf.contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, DownloadServiceActivity.class), 0);
            nf.flags = 4;
            nf.flags |= 16;
            this.nm.notify((int) task.getTaskId(), nf);
        } catch (Exception e) {
        }
    }

    public void taskRemoveNotification(DownloadTask task) {
        try {
            this.nm.cancel((int) task.getTaskId());
        } catch (Exception e) {
        }
    }
}
