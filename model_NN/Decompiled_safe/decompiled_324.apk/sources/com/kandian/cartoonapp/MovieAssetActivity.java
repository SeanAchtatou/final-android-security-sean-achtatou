package com.kandian.cartoonapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;
import com.kandian.common.Log;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class MovieAssetActivity extends AssetActivity {
    /* access modifiers changed from: private */
    public MovieAssetActivity _context = null;
    /* access modifiers changed from: private */
    public final List<String> list = new ArrayList();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.asset_activity_head);
        super.onCreate(savedInstanceState);
        this._context = this;
        Log.v("MovieAssetActivity", "onCreate");
        if (getAsset() != null) {
            View inflate = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.movieasset_activity, (LinearLayout) findViewById(R.id.root));
            ((TextView) findViewById(R.id.assetName)).setText(getAsset().getDisplayName(getApplication()));
            TabHost tabHost = (TabHost) findViewById(R.id.tabhost);
            tabHost.setup(getLocalActivityManager());
            tabHost.setup();
            TextView textView = new TextView(this);
            setTabStyle(textView);
            if (!getAsset().getAssetType().equals("10") && getAsset().getAssetIdX() == 0) {
                textView.setText(getString(R.string.assetEpisodesLabel));
                textView.setGravity(17);
                tabHost.addTab(tabHost.newTabSpec("episodesTab").setIndicator(textView).setContent(new Intent(this, AssetListActivity.class).putExtra("listurl", String.valueOf(getString(R.string.episodeslist)) + "&fq=id:" + getAsset().getAssetId()).putExtra("onlyepisodes", true)));
            }
            TextView textView2 = new TextView(this);
            setTabStyle(textView2);
            TabHost.TabSpec detailContentTabSpec = tabHost.newTabSpec("tab1");
            detailContentTabSpec.setIndicator(textView2);
            detailContentTabSpec.setContent(R.id.detailcontent);
            tabHost.addTab(detailContentTabSpec);
            TextView assetDirector = (TextView) findViewById(R.id.assetDirector);
            String text = getAsset().getAssetDirector() != null ? getAsset().getAssetDirector() : "";
            assetDirector.setText(String.valueOf(getString(R.string.assetDirectorLabel)) + text);
            String director = text;
            if (!"".equals(director) && director.indexOf(CookieSpec.PATH_DELIM) != -1) {
                String[] dir = director.split(CookieSpec.PATH_DELIM);
                for (int i = 0; i < dir.length; i++) {
                    this.list.add(dir[i]);
                }
            } else if (!"".equals(director)) {
                this.list.add(director);
            }
            TextView assetActor = (TextView) findViewById(R.id.assetActor);
            String text2 = getAsset().getAssetActor() != null ? getAsset().getAssetActor() : "";
            assetActor.setText(String.valueOf(getString(R.string.assetActorLabel)) + text2);
            String actor = text2;
            if (!"".equals(actor) && actor.indexOf(CookieSpec.PATH_DELIM) != -1) {
                String[] act = actor.split(CookieSpec.PATH_DELIM);
                for (int i2 = 0; i2 < act.length; i2++) {
                    this.list.add(act[i2]);
                }
            } else if (!"".equals(actor)) {
                this.list.add(actor);
            }
            ImageButton search_btn = (ImageButton) findViewById(R.id.search_dir_act_btn);
            if (this.list == null || this.list.size() <= 0) {
                search_btn.setVisibility(8);
            } else {
                CharSequence[] persons = new CharSequence[this.list.size()];
                for (int i3 = 0; i3 < this.list.size(); i3++) {
                    persons[i3] = this.list.get(i3);
                }
                final CharSequence[] charSequenceArr = persons;
                search_btn.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        new AlertDialog.Builder(MovieAssetActivity.this._context).setTitle(MovieAssetActivity.this.getString(R.string.search_btn)).setItems(charSequenceArr, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String diract = (String) MovieAssetActivity.this.list.get(i);
                                Intent intent = new Intent();
                                if (diract != null) {
                                    if (diract.trim().indexOf(" ") != -1) {
                                        diract = diract.trim().replace(" ", "+");
                                    }
                                    intent.setAction("android.intent.action.SEARCH");
                                    intent.putExtra("query", diract);
                                    intent.setClass(MovieAssetActivity.this._context, SearchActivity.class);
                                    MovieAssetActivity.this.startActivity(intent);
                                }
                            }
                        }).create().show();
                    }
                });
            }
            ((TextView) findViewById(R.id.assetYear)).setText(String.valueOf(getString(R.string.assetYearLabel)) + (getAsset().getAssetYear() > 0 ? new StringBuilder(String.valueOf(getAsset().getAssetYear())).toString() : ""));
            ((TextView) findViewById(R.id.assetCategory)).setText(String.valueOf(getString(R.string.assetCategoryLabel)) + (getAsset().getCategory() != null ? getAsset().getCategory() : ""));
            ((TextView) findViewById(R.id.assetIntroduction)).setText(getAsset().getAssetDescription() != null ? getAsset().getAssetDescription() : "");
            return;
        }
        Log.v("MovieAssetActivity", "asset is null:" + getIntent().getStringExtra("assetKey") + "," + getIntent().getStringExtra("assetType"));
    }
}
