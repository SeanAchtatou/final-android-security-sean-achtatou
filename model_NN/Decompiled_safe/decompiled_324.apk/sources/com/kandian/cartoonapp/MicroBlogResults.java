package com.kandian.cartoonapp;

import java.util.ArrayList;

public class MicroBlogResults {
    private ArrayList<MicroBlog> microBlogs = new ArrayList<>();
    private int totalcount;

    public void addMicroBlog(MicroBlog mb) {
        if (mb != null) {
            if (this.microBlogs != null) {
                this.microBlogs.add(mb);
                return;
            }
            this.microBlogs = new ArrayList<>();
            this.microBlogs.add(mb);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public MicroBlog get(int i) {
        if (this.microBlogs != null) {
            return this.microBlogs.get(i);
        }
        return null;
    }

    public int getSize() {
        if (this.microBlogs != null) {
            return this.microBlogs.size();
        }
        return 0;
    }

    public int getTotalcount() {
        return this.totalcount;
    }

    public void setTotalcount(int totalcount2) {
        this.totalcount = totalcount2;
    }

    public ArrayList<MicroBlog> getMicroBlogs() {
        return this.microBlogs;
    }
}
