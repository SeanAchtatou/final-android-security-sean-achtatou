package com.kandian.cartoonapp;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.sax.Element;
import android.sax.EndElementListener;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.sax.StartElementListener;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.kandian.common.AsyncImageLoader;
import com.kandian.common.Log;
import com.kandian.common.NetworkUtil;
import com.kandian.common.VideoAsset;
import com.kandian.common.VideoAssetMap;
import com.kandian.ksfamily.KSApp;
import com.kandian.ksfamily.KSFamilyListActivity;
import com.kandian.other.KSAboutActivity;
import com.kandian.other.KSHelpActivity;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.xml.sax.Attributes;

public class RankingListActivity extends ListActivity {
    /* access modifiers changed from: private */
    public static String TAG = "RankingListActivity";
    private final int MSG_LIST = 0;
    /* access modifiers changed from: private */
    public RankingListActivity _context;
    /* access modifiers changed from: private */
    public ArrayList<RankingItem> assets = null;
    /* access modifiers changed from: private */
    public AsyncImageLoader asyncImageLoader = null;
    View.OnClickListener backListener = new View.OnClickListener() {
        public void onClick(View v) {
            RankingListActivity.this.finish();
        }
    };
    View.OnClickListener ksaboutListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(RankingListActivity.this._context, KSAboutActivity.class);
            KSApp ksApp = new KSApp();
            ksApp.setAppname(RankingListActivity.this._context.getString(R.string.app_name));
            ksApp.setPartner(RankingListActivity.this._context.getString(R.string.partner));
            ksApp.setAppicon(R.drawable.kstogo);
            ksApp.setDescription(RankingListActivity.this._context.getString(R.string.app_description));
            try {
                ksApp.setPackagename(RankingListActivity.this._context.getPackageManager().getPackageInfo(RankingListActivity.this._context.getPackageName(), 0).packageName);
                ksApp.setVersionname(RankingListActivity.this._context.getPackageManager().getPackageInfo(RankingListActivity.this._context.getPackageName(), 0).versionName);
                ksApp.setVersioncode(RankingListActivity.this._context.getPackageManager().getPackageInfo(RankingListActivity.this._context.getPackageName(), 0).versionCode);
            } catch (PackageManager.NameNotFoundException e) {
                ksApp.setPackagename("");
                ksApp.setVersionname("");
            }
            Bundle mBundle = new Bundle();
            mBundle.putSerializable("ksAppInfo", ksApp);
            intent.putExtras(mBundle);
            RankingListActivity.this.startActivity(intent);
        }
    };
    View.OnClickListener ksfamilyListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(RankingListActivity.this._context, KSFamilyListActivity.class);
            RankingListActivity.this.startActivity(intent);
        }
    };
    View.OnClickListener kshelpListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(RankingListActivity.this._context, KSHelpActivity.class);
            KSApp ksApp = new KSApp();
            ksApp.setAppname(RankingListActivity.this._context.getString(R.string.app_name));
            ksApp.setAppicon(R.drawable.kstogo);
            Bundle mBundle = new Bundle();
            mBundle.putSerializable("ksAppInfo", ksApp);
            intent.putExtras(mBundle);
            RankingListActivity.this.startActivity(intent);
        }
    };
    /* access modifiers changed from: private */
    public String listUrl = null;
    Handler myViewUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            TextView emptyStatusView;
            switch (msg.what) {
                case 0:
                    if (RankingListActivity.this.assets.size() == 0 && (emptyStatusView = (TextView) RankingListActivity.this.findViewById(16908292)) != null) {
                        emptyStatusView.setText(RankingListActivity.this.getString(R.string.nodata));
                    }
                    ((BaseAdapter) RankingListActivity.this.getListAdapter()).notifyDataSetChanged();
                    break;
            }
            super.handleMessage(msg);
        }
    };
    View.OnClickListener refreshListener = new View.OnClickListener() {
        public void onClick(View v) {
            RankingListActivity.this.assets.clear();
            RankingListActivity.this.getData();
        }
    };
    private ProgressDialog v_ProgressDialog = null;

    public class HitInfo {
        String hitValue;
        String sourceName;

        public HitInfo() {
        }
    }

    public class RankingItem {
        long assetId;
        String assetName;
        String assetType;
        ArrayList<HitInfo> hitInfoList;
        int ranking = 0;
        String totalHit;
        String weekHit;

        public RankingItem() {
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        this.v_ProgressDialog = ProgressDialog.show(this, "Please wait...", "Retrieving data ...", true);
        super.onCreate(savedInstanceState);
        this._context = this;
        this.asyncImageLoader = AsyncImageLoader.instance();
        setContentView((int) R.layout.rankinglist_activity);
        NavigationBar.setup(this);
        Log.v(TAG, "onCreate");
        this.listUrl = getIntent().getStringExtra("listurl");
        TextView emptyStatusView = (TextView) findViewById(16908292);
        if (emptyStatusView != null) {
            emptyStatusView.setText(getString(R.string.retrieving));
        }
        this.assets = new ArrayList<>();
        getData();
        setListAdapter(new RankingItemAdaptor(this, R.layout.rankingitemrow, this.assets));
        getListView().setTextFilterEnabled(true);
        this.v_ProgressDialog.dismiss();
    }

    /* access modifiers changed from: protected */
    public void getData() {
        new Thread() {
            public void run() {
                RankingListActivity.this.getRankingItems(RankingListActivity.this.assets, RankingListActivity.this.listUrl);
                Message m = Message.obtain(RankingListActivity.this.myViewUpdateHandler);
                m.what = 0;
                m.sendToTarget();
            }
        }.start();
    }

    private class RankingItemAdaptor extends ArrayAdapter<RankingItem> {
        private ArrayList<RankingItem> items;

        public RankingItemAdaptor(Context context, int textViewResourceId, ArrayList<RankingItem> items2) {
            super(context, textViewResourceId, items2);
            this.items = items2;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) RankingListActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.rankingitemrow, (ViewGroup) null);
            }
            RankingItem rankingItem = this.items.get(position);
            if (rankingItem != null) {
                TextView rankingText = (TextView) v.findViewById(R.id.ranking);
                TextView nameText = (TextView) v.findViewById(R.id.assetName);
                TextView hitText = (TextView) v.findViewById(R.id.hitValue);
                if (rankingText != null) {
                    rankingText.setText(new StringBuilder(String.valueOf(rankingItem.ranking)).toString());
                }
                if (nameText != null) {
                    nameText.setText(rankingItem.assetName);
                }
                if (hitText != null) {
                    hitText.setText(rankingItem.weekHit);
                }
                VideoAsset videoAsset = VideoAssetMap.instance().getAsset(rankingItem.assetId, rankingItem.assetType, RankingListActivity.this.getApplication());
                if (!(videoAsset == null || (imageView = (ImageView) v.findViewById(R.id.assetImage)) == null)) {
                    imageView.setTag(videoAsset.getImageUrl());
                    Bitmap cachedImage = RankingListActivity.this.asyncImageLoader.loadBitmap(videoAsset.getImageUrl(), new AsyncImageLoader.ImageCallback1() {
                        public void imageLoaded(Bitmap imageDrawable, String imageUrl) {
                            ImageView imageViewByTag = (ImageView) RankingListActivity.this.getListView().findViewWithTag(imageUrl);
                            if (imageViewByTag != null) {
                                imageViewByTag.setImageBitmap(imageDrawable);
                            }
                        }
                    });
                    if (cachedImage != null) {
                        imageView.setImageBitmap(cachedImage);
                    }
                }
            }
            if (position == getCount() - 1) {
                Log.v("AssetListActivity", "Getting more data at position  " + position);
            }
            return v;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.v("AssetListActivity", "Starting AssetActivity at position" + position);
        RankingItem rankingItem = this.assets.get(position);
        VideoAsset asset = VideoAssetMap.instance().getAsset(rankingItem.assetId, rankingItem.assetType, getApplication());
        if (asset != null) {
            Intent intent = new Intent();
            if (asset.getAssetType().equals("10")) {
                intent.setClass(this, EpisodeAssetActivity.class);
            } else {
                intent.setClass(this, MovieAssetActivity.class);
            }
            intent.putExtra("assetKey", asset.getAssetKey());
            intent.putExtra("assetType", asset.getAssetType());
            startActivity(intent);
            return;
        }
        Log.v(TAG, "cannot find asset: [assetId/assetType]" + rankingItem.assetId + CookieSpec.PATH_DELIM + rankingItem.assetType);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.assetlistmenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                this.refreshListener.onClick(getListView());
                return true;
            case R.id.menu_setting:
            default:
                return super.onOptionsItemSelected(item);
            case R.id.menu_ksfamily:
                this.ksfamilyListener.onClick(getListView());
                return true;
            case R.id.menu_kshelp:
                this.kshelpListener.onClick(getListView());
                return true;
            case R.id.menu_ksabout:
                this.ksaboutListener.onClick(getListView());
                return true;
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    public ArrayList<RankingItem> getRankingItems(ArrayList<RankingItem> rankingItems, String rankingServiceUrl) {
        RootElement rootElement = new RootElement("DOCUMENT");
        Element asset = rootElement.getChild("asset");
        final ArrayList<RankingItem> arrayList = rankingItems;
        asset.setStartElementListener(new StartElementListener() {
            public void start(Attributes attributes) {
                arrayList.add(new RankingItem());
            }
        });
        final ArrayList<RankingItem> arrayList2 = rankingItems;
        asset.setEndElementListener(new EndElementListener() {
            public void end() {
                ((RankingItem) arrayList2.get(arrayList2.size() - 1)).ranking = arrayList2.size();
            }
        });
        final ArrayList<RankingItem> arrayList3 = rankingItems;
        asset.getChild("assetid").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("")) {
                    ((RankingItem) arrayList3.get(arrayList3.size() - 1)).assetId = Long.parseLong(body);
                }
            }
        });
        final ArrayList<RankingItem> arrayList4 = rankingItems;
        asset.getChild("assettype").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("")) {
                    ((RankingItem) arrayList4.get(arrayList4.size() - 1)).assetType = new String(body);
                }
            }
        });
        final ArrayList<RankingItem> arrayList5 = rankingItems;
        asset.getChild("assetname").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("")) {
                    Log.v(RankingListActivity.TAG, "found " + body);
                    ((RankingItem) arrayList5.get(arrayList5.size() - 1)).assetName = new String(body);
                }
            }
        });
        final ArrayList<RankingItem> arrayList6 = rankingItems;
        asset.getChild("totalhit").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("")) {
                    ((RankingItem) arrayList6.get(arrayList6.size() - 1)).totalHit = new String(body);
                }
            }
        });
        final ArrayList<RankingItem> arrayList7 = rankingItems;
        asset.getChild("weekhit").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("")) {
                    ((RankingItem) arrayList7.get(arrayList7.size() - 1)).weekHit = new String(body);
                }
            }
        });
        Element hitInfoList = asset.getChild("hitinfolist");
        final ArrayList<RankingItem> arrayList8 = rankingItems;
        hitInfoList.setStartElementListener(new StartElementListener() {
            public void start(Attributes attributes) {
                ((RankingItem) arrayList8.get(arrayList8.size() - 1)).hitInfoList = new ArrayList<>();
            }
        });
        hitInfoList.setEndElementListener(new EndElementListener() {
            public void end() {
            }
        });
        Element hit = hitInfoList.getChild("hit");
        final ArrayList<RankingItem> arrayList9 = rankingItems;
        hit.setStartElementListener(new StartElementListener() {
            public void start(Attributes attributes) {
                ((RankingItem) arrayList9.get(arrayList9.size() - 1)).hitInfoList.add(new HitInfo());
            }
        });
        hit.setEndElementListener(new EndElementListener() {
            public void end() {
            }
        });
        final ArrayList<RankingItem> arrayList10 = rankingItems;
        hit.getChild("sourcename").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("")) {
                    ArrayList<HitInfo> list = ((RankingItem) arrayList10.get(arrayList10.size() - 1)).hitInfoList;
                    list.get(list.size() - 1).sourceName = new String(body);
                }
            }
        });
        final ArrayList<RankingItem> arrayList11 = rankingItems;
        asset.getChild("value").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("")) {
                    ArrayList<HitInfo> list = ((RankingItem) arrayList11.get(arrayList11.size() - 1)).hitInfoList;
                    list.get(list.size() - 1).hitValue = new String(body);
                }
            }
        });
        try {
            Log.v(TAG, "reading rankings from " + rankingServiceUrl);
            Xml.parse((InputStream) new URL(rankingServiceUrl).getContent(), Xml.Encoding.UTF_8, rootElement.getContentHandler());
        } catch (Exception e) {
            NetworkUtil.checkNetwork(getApplication());
            e.printStackTrace();
        }
        return rankingItems;
    }
}
