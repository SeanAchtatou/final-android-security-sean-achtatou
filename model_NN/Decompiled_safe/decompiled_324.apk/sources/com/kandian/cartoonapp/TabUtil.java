package com.kandian.cartoonapp;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public class TabUtil {
    public static void setTabStyle(Context context, TextView tab) {
        tab.setText(context.getString(R.string.assetDetailsLabel));
        tab.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        tab.setTextSize(14.0f);
        tab.setTypeface(Typeface.create((String) null, 0));
        tab.setGravity(17);
        tab.setBackgroundResource(R.color.tab_indicator_background);
        tab.setPadding(3, 6, 3, 6);
        try {
            ColorStateList createFromXml = ColorStateList.createFromXml(context.getResources(), Resources.getSystem().getXml(17170441));
            tab.setTextColor(-16777216);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1, 1.0f);
        layoutParams.setMargins(1, 0, 1, 0);
        tab.setLayoutParams(layoutParams);
    }
}
