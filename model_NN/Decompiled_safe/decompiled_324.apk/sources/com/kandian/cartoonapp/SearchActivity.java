package com.kandian.cartoonapp;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.SearchRecentSuggestions;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.kandian.common.AssetList;
import com.kandian.common.AssetListSaxHandler;
import com.kandian.common.AsyncImageLoader;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.SiteConfigs;
import com.kandian.common.SystemConfigs;
import com.kandian.common.VideoAsset;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

public class SearchActivity extends ListActivity {
    private final int MSG_LIST = 0;
    /* access modifiers changed from: private */
    public String TAG = "SearchActivity";
    /* access modifiers changed from: private */
    public SearchActivity _context;
    /* access modifiers changed from: private */
    public AssetList assetList = null;
    /* access modifiers changed from: private */
    public AsyncImageLoader asyncImageLoader = null;
    View.OnClickListener backListener = new View.OnClickListener() {
        public void onClick(View v) {
            SearchActivity.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public String listUrl = null;
    Handler myViewUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            TextView emptyStatusView;
            switch (msg.what) {
                case 0:
                    if (SearchActivity.this.assetList.getCurrentItemCount() == 0 && (emptyStatusView = (TextView) SearchActivity.this.findViewById(16908292)) != null) {
                        emptyStatusView.setText(SearchActivity.this._context.getString(R.string.nodata));
                    }
                    ((BaseAdapter) SearchActivity.this.getListAdapter()).notifyDataSetChanged();
                    break;
            }
            super.handleMessage(msg);
        }
    };
    /* access modifiers changed from: private */
    public boolean onlyEpisodes = false;
    View.OnClickListener refreshListener = new View.OnClickListener() {
        public void onClick(View v) {
            SearchActivity.this.assetList.clear();
            SearchActivity.this.getData();
        }
    };
    EditText searchBox = null;
    /* access modifiers changed from: private */
    public SystemConfigs systemConfigs = null;
    private ProgressDialog v_ProgressDialog = null;

    public void onCreate(Bundle savedInstanceState) {
        this.v_ProgressDialog = ProgressDialog.show(this, "Please wait...", "Retrieving data ...", true);
        super.onCreate(savedInstanceState);
        this._context = this;
        this.asyncImageLoader = AsyncImageLoader.instance();
        setContentView((int) R.layout.search_activity);
        Log.v(this.TAG, "onCreate");
        NavigationBar.setup(this);
        this.searchBox = (EditText) findViewById(R.id.searchbox);
        this.searchBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SearchActivity.this.onSearchRequested();
            }
        });
        ((Button) findViewById(R.id.refreshbutton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SearchActivity.this.refreshListener.onClick(SearchActivity.this.getListView());
            }
        });
        TextView emptyStatusView = (TextView) findViewById(16908292);
        if (emptyStatusView != null) {
            emptyStatusView.setText(getString(R.string.retrieving));
        }
        this.assetList = new AssetList();
        this.systemConfigs = SystemConfigs.instance(getApplication());
        setListAdapter(new AssetAdaptor(this, R.layout.assetrow, this.assetList.getList()));
        handleIntent(getIntent());
        getListView().setTextFilterEnabled(true);
        this.v_ProgressDialog.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private String searchString(String query) {
        return getString(R.string.searchServicePrefix).replace("{query}", URLEncoder.encode(query));
    }

    private void handleIntent(Intent intent) {
        String query = "";
        if ("android.intent.action.SEARCH".equals(intent.getAction())) {
            query = intent.getStringExtra("query");
            this.listUrl = searchString(query);
        }
        if (query != null && !query.trim().equals("")) {
            new SearchRecentSuggestions(this, SearchSuggestionProvider.AUTHORITY, 3).saveRecentQuery(query, null);
        }
        EditText searchBox2 = (EditText) findViewById(R.id.searchbox);
        if (searchBox2 != null) {
            searchBox2.setHint(query);
        }
        this.assetList.clear();
        getData();
    }

    /* access modifiers changed from: protected */
    public void getData() {
        new Thread() {
            public void run() {
                String parseUrl = SearchActivity.this.listUrl;
                if (SearchActivity.this.assetList != null) {
                    parseUrl = parseUrl.replace("start=0", "start=" + SearchActivity.this.assetList.getCurrentItemCount());
                }
                SiteConfigs siteConfigs = SearchActivity.this.systemConfigs.getSiteConfigs();
                if (siteConfigs != null) {
                    parseUrl = String.valueOf(parseUrl) + "&" + siteConfigs.getDownloadSourcesQueryString(SearchActivity.this.getApplication());
                } else {
                    Log.v(SearchActivity.this.TAG, "SiteConfigs is null");
                }
                AssetList myDataList = SearchActivity.this.parse(parseUrl);
                if (SearchActivity.this.onlyEpisodes && myDataList.getCurrentItemCount() > 0) {
                    for (int i = myDataList.getCurrentItemCount() - 1; i > 0; i--) {
                        if (myDataList.getList().get(i).getAssetIdX() == 0) {
                            myDataList.remove(i);
                        }
                    }
                }
                SearchActivity.this.assetList.appendList(myDataList);
                Message m = Message.obtain(SearchActivity.this.myViewUpdateHandler);
                m.what = 0;
                m.sendToTarget();
            }
        }.start();
    }

    private class AssetAdaptor extends ArrayAdapter<VideoAsset> {
        private ArrayList<VideoAsset> items;

        public AssetAdaptor(Context context, int textViewResourceId, ArrayList<VideoAsset> items2) {
            super(context, textViewResourceId, items2);
            this.items = items2;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            String categoryText;
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) SearchActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.assetrow, (ViewGroup) null);
            }
            VideoAsset asset = this.items.get(position);
            if (asset != null) {
                ImageView imageView = (ImageView) v.findViewById(R.id.assetImage);
                TextView tt = (TextView) v.findViewById(R.id.toptext);
                if (imageView != null) {
                    imageView.setTag(asset.getImageUrl());
                    Bitmap cachedImage = SearchActivity.this.asyncImageLoader.loadBitmap(asset.getImageUrl(), new AsyncImageLoader.ImageCallback1() {
                        public void imageLoaded(Bitmap imageDrawable, String imageUrl) {
                            ImageView imageViewByTag = (ImageView) SearchActivity.this.getListView().findViewWithTag(imageUrl);
                            if (imageViewByTag != null) {
                                imageViewByTag.setImageBitmap(imageDrawable);
                            }
                        }
                    });
                    if (cachedImage != null) {
                        imageView.setImageBitmap(cachedImage);
                    }
                }
                if (tt != null) {
                    String assetName = asset.getAssetName();
                    tt.setText(asset.getDisplayName(SearchActivity.this.getApplication()));
                }
                if (!SearchActivity.this.onlyEpisodes) {
                    TextView categoryt = (TextView) v.findViewById(R.id.categorytext);
                    if (categoryt != null) {
                        String categoryText2 = asset.getOrigin();
                        if (categoryText2 == null || categoryText2.equals("")) {
                            categoryText = asset.getCategory();
                        } else {
                            categoryText = String.valueOf(categoryText2) + "  " + asset.getCategory();
                        }
                        categoryt.setText(categoryText);
                    }
                    TextView bt = (TextView) v.findViewById(R.id.bottomtext);
                    if (bt != null) {
                        if (asset.getAssetIdX() == 0) {
                            String voteString = "-";
                            if (asset.getVote() >= 0.6d) {
                                voteString = String.valueOf(new Double(asset.getVote() * 100.0d).intValue()) + "%";
                                bt.setTextColor(SearchActivity.this.getResources().getColor(R.color.good_vote_color));
                            } else if (asset.getVote() >= 0.0d) {
                                voteString = String.valueOf(new Double(asset.getVote() * 100.0d).intValue()) + "%";
                                bt.setTextColor(SearchActivity.this.getResources().getColor(R.color.bad_vote_color));
                            }
                            bt.setText(voteString);
                        } else {
                            bt.setTextColor(SearchActivity.this.getResources().getColor(R.drawable.white));
                        }
                    }
                }
            }
            if (position == getCount() - 1) {
                Log.v(SearchActivity.this.TAG, "Getting more data at position  " + position);
                if (SearchActivity.this.onlyEpisodes && getCount() < SearchActivity.this.assetList.getTotalResultCount() - 1) {
                    SearchActivity.this.getData();
                } else if (!SearchActivity.this.onlyEpisodes && getCount() < SearchActivity.this.assetList.getTotalResultCount()) {
                    SearchActivity.this.getData();
                }
            }
            return v;
        }
    }

    public Object fetch(String address) throws MalformedURLException, IOException {
        Log.v(this.TAG, "Fetching  " + address);
        return new URL(address).getContent();
    }

    public AssetList parse(String assetsListUrl) {
        AssetList results = new AssetList();
        AssetListSaxHandler saxHandler = new AssetListSaxHandler(this, results);
        try {
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            InputStream in = (InputStream) fetch(assetsListUrl);
            if (in != null) {
                parser.parse(in, saxHandler);
            }
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
        }
        return results;
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.v(this.TAG, "Starting AssetActivity at position" + position);
        VideoAsset asset = this.assetList.get(position);
        Intent intent = new Intent();
        if (this.onlyEpisodes) {
            intent.setClass(this, EpisodeAssetActivity.class);
        } else {
            intent.setClass(this, MovieAssetActivity.class);
        }
        intent.putExtra("assetKey", asset.getAssetKey());
        intent.putExtra("assetType", asset.getAssetType());
        startActivity(intent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.assetlistmenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                this.refreshListener.onClick(getListView());
                return true;
            case R.id.menu_back:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public boolean onSearchRequested() {
        String queryPrefill = this.searchBox.getHint().toString();
        Log.v(this.TAG, "Search word is " + queryPrefill);
        startSearch(queryPrefill, false, null, false);
        return true;
    }

    public void onResume() {
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }
}
