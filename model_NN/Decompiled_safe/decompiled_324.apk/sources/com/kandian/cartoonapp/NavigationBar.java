package com.kandian.cartoonapp;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import com.kandian.common.Log;
import com.kandian.common.StatisticsUtil;

public class NavigationBar {
    public static void setup(final Activity context) {
        Button contentButton = (Button) context.findViewById(R.id.button_content);
        if (context instanceof AssetListActivity) {
            contentButton.setEnabled(false);
        }
        contentButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("NavigationBar", "invoking content");
                Intent intent = new Intent();
                intent.setClass(context, AssetListActivity.class);
                context.startActivity(intent);
            }
        });
        Button searchButton = (Button) context.findViewById(R.id.button_search);
        if (context instanceof SearchActivity) {
            searchButton.setEnabled(false);
        }
        searchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("NavigationBar", "invoking search");
                context.onSearchRequested();
            }
        });
        Button profileButton = (Button) context.findViewById(R.id.button_profile);
        if (context instanceof DownloadServiceActivity) {
            profileButton.setEnabled(false);
        }
        profileButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("NavigationBar", "invoking profile");
                Intent intent = new Intent();
                intent.setClass(context, DownloadServiceActivity.class);
                context.startActivity(intent);
            }
        });
        Button favoritesButton = (Button) context.findViewById(R.id.button_favorites);
        if (context instanceof MyKSActivity) {
            favoritesButton.setEnabled(false);
        }
        favoritesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("NavigationBar", "invoking favorites");
                Intent intent = new Intent();
                intent.setClass(context, MyKSActivity.class);
                context.startActivity(intent);
            }
        });
        Button rankingButton = (Button) context.findViewById(R.id.bottom_ranking);
        if (context instanceof RankingListActivity) {
            rankingButton.setEnabled(false);
        }
        rankingButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("NavigationBar", "invoking ranking");
                Intent intent = new Intent();
                intent.setClass(context, RankingListActivity.class);
                intent.putExtra("listurl", context.getString(R.string.rankingServiceUrl1));
                context.startActivity(intent);
            }
        });
        StatisticsUtil.updateCount(context);
    }
}
