package com.kandian.exchange.entity;

public class Exchange {
    private String description;
    private long id;
    private String partnercode;
    private String productcode;
    private String productlogo;
    private String productname;
    private String producturl;

    public long getId() {
        return this.id;
    }

    public void setId(long id2) {
        this.id = id2;
    }

    public String getPartnercode() {
        return this.partnercode;
    }

    public void setPartnercode(String partnercode2) {
        this.partnercode = partnercode2;
    }

    public String getProductcode() {
        return this.productcode;
    }

    public void setProductcode(String productcode2) {
        this.productcode = productcode2;
    }

    public String getProductname() {
        return this.productname;
    }

    public void setProductname(String productname2) {
        this.productname = productname2;
    }

    public String getProducturl() {
        return this.producturl;
    }

    public void setProducturl(String producturl2) {
        this.producturl = producturl2;
    }

    public String getProductlogo() {
        return this.productlogo;
    }

    public void setProductlogo(String productlogo2) {
        this.productlogo = productlogo2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }
}
