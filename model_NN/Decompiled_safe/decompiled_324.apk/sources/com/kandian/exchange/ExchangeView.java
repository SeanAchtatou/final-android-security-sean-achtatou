package com.kandian.exchange;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.WebView;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class ExchangeView extends WebView {
    private String TAG;
    private String backgroundColor;
    private String categorys;
    private String textColor;

    public String getBackgroundColor() {
        return this.backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor2) {
        this.backgroundColor = backgroundColor2;
    }

    public String getTextColor() {
        return this.textColor;
    }

    public void setTextColor(String textColor2) {
        this.textColor = textColor2;
    }

    public String getCategorys() {
        return this.categorys;
    }

    public void setCategorys(String categorys2) {
        this.categorys = categorys2;
    }

    public ExchangeView(Context context, AttributeSet paramAttributeSet) {
        this(context, paramAttributeSet, 0);
    }

    public ExchangeView(Context context, AttributeSet paramAttributeSet, int paramInt) {
        super(context, paramAttributeSet, paramInt);
        this.TAG = "ExchangeView";
        this.backgroundColor = "";
        this.textColor = "";
        this.categorys = "";
        ApplicationInfo localApplicationInfo = null;
        try {
            try {
                localApplicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            String appkey = localApplicationInfo.metaData.getString("KS_EXCHANGE_APPKEY");
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            String categorys2 = paramAttributeSet.getAttributeValue(str, "categorys");
            String backgroundColor2 = paramAttributeSet.getAttributeValue(str, "bgColor");
            String textColor2 = paramAttributeSet.getAttributeValue(str, "tColor");
            String url = replace(replace(replace(replace(ExchangeConstants.EXCHANGE_DATA_INTERFACE, "{appkey}", urlEncode(appkey)), "{bc}", urlEncode(replace(backgroundColor2, "#", ""))), "{tc}", urlEncode(replace(textColor2, "#", ""))), "{c}", urlEncode(categorys2));
            Log.v(this.TAG, "url = " + url);
            setBackgroundColor(17170446);
            setTextColor(textColor2);
            getSettings().setJavaScriptEnabled(true);
            getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            loadUrl(url);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void loadUrl(String paramString) {
        super.loadUrl(paramString);
    }

    private String replace(String source, String regex, String replacement) {
        StringBuffer buffer = new StringBuffer();
        while (true) {
            int index = source.indexOf(regex);
            if (index < 0) {
                buffer.append(source);
                return buffer.toString();
            }
            buffer.append(source.substring(0, index));
            buffer.append(replacement);
            source = source.substring(regex.length() + index);
        }
    }

    public String urlEncode(String obj) {
        if (obj == null) {
            return null;
        }
        try {
            return URLEncoder.encode(obj, StringEncodings.UTF8);
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }
}
