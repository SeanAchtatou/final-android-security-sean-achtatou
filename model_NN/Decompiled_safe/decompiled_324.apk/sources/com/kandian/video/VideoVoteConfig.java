package com.kandian.video;

public class VideoVoteConfig {
    private String color;
    private int max;
    private int min;

    public VideoVoteConfig(int min2, int max2, String color2) {
        this.min = min2;
        this.max = max2;
        this.color = color2;
    }

    public int getMin() {
        return this.min;
    }

    public void setMin(int min2) {
        this.min = min2;
    }

    public int getMax() {
        return this.max;
    }

    public void setMax(int max2) {
        this.max = max2;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color2) {
        this.color = color2;
    }

    public boolean isIn(int vote) {
        if (vote > this.max || vote <= this.min) {
            return false;
        }
        return true;
    }
}
