package com.kandian.video;

import android.content.Context;
import com.kandian.cartoonapp.R;
import com.kandian.common.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.SAXParserFactory;

public class VideoVoteHelper {
    private static VideoVoteHelper ourInstance = new VideoVoteHelper();
    private String TAG = "VideoVoteHelper";
    private List<VideoVoteConfig> list = new ArrayList();

    public static VideoVoteHelper getInstance() {
        return ourInstance;
    }

    private VideoVoteHelper() {
    }

    public void init(Context context) {
        try {
            List<VideoVoteConfig> confs = parse(context.getString(R.string.videovoteConfig));
            this.list = confs;
            for (int i = 0; i < confs.size(); i++) {
                VideoVoteConfig vvc = confs.get(i);
                Log.v(this.TAG, "max:" + vvc.getMax() + " min:" + vvc.getMin() + " color:" + vvc.getColor());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getColor(int vote) {
        if (this.list == null || this.list.size() == 0) {
            return "#FF6347";
        }
        for (int i = 0; i < this.list.size(); i++) {
            VideoVoteConfig vvc = this.list.get(i);
            if (vvc.isIn(vote)) {
                return vvc.getColor();
            }
        }
        return "#FF6347";
    }

    public int getColorResId(int vote) {
        if (this.list == null || this.list.size() == 0) {
            return R.drawable.round_btn_vote1;
        }
        for (int i = 0; i < this.list.size(); i++) {
            VideoVoteConfig vvc = this.list.get(i);
            if (vvc.isIn(vote)) {
                String color = vvc.getColor();
                if (color.equals("#FFECEC")) {
                    return R.drawable.round_btn_vote1;
                }
                if (color.equals("#FFB5B5")) {
                    return R.drawable.round_btn_vote2;
                }
                if (color.equals("#ff7575")) {
                    return R.drawable.round_btn_vote3;
                }
                if (color.equals("#FF2D2D")) {
                    return R.drawable.round_btn_vote4;
                }
                if (color.equals("#EA0000")) {
                    return R.drawable.round_btn_vote5;
                }
            }
        }
        return R.drawable.round_btn_vote1;
    }

    public int getSmileResId(int vote) {
        if (this.list == null || this.list.size() == 0) {
            return R.drawable.smile1;
        }
        for (int i = 0; i < this.list.size(); i++) {
            VideoVoteConfig vvc = this.list.get(i);
            if (vvc.isIn(vote)) {
                String color = vvc.getColor();
                if (color.equals("#FFECEC")) {
                    return R.drawable.smile1;
                }
                if (color.equals("#FFB5B5")) {
                    return R.drawable.smile2;
                }
                if (color.equals("#ff7575")) {
                    return R.drawable.smile3;
                }
                if (color.equals("#FF2D2D")) {
                    return R.drawable.smile4;
                }
                if (color.equals("#EA0000")) {
                    return R.drawable.smile5;
                }
            }
        }
        return R.drawable.round_btn_vote1;
    }

    private String encode(String str) {
        if (str == null || str.trim().length() == 0) {
            return "";
        }
        try {
            return URLEncoder.encode(str, "GBK");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    public void vote(long schemaid, String assetcode, String assetname, int type, String key, Context context) {
        String url = String.valueOf(context.getString(R.string.videovote)) + "?schemaid=" + schemaid + "&assetcode=" + assetcode + "&assetname=" + encode(assetname) + "&type=" + type + "&key=" + encode(key);
        try {
            Log.v(this.TAG, url);
            InputStream input = (InputStream) fetch(url);
            if (input != null) {
                input.close();
                return;
            }
            throw new IOException("inputStream is null:" + url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<VideoVoteConfig> parse(String url) {
        VideoVoteConfigSaxHandler saxHandler = new VideoVoteConfigSaxHandler();
        try {
            InputStream input = (InputStream) fetch(url);
            if (input != null) {
                SAXParserFactory.newInstance().newSAXParser().parse(input, saxHandler);
                return saxHandler.getList();
            }
            throw new IOException("inputStream is null:" + url);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Object fetch(String address) throws MalformedURLException, IOException {
        Log.v("AssetListActivity", "Fetching  " + address);
        return new URL(address).getContent();
    }
}
