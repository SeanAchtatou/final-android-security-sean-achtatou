package com.kandian.video;

import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class VideoVoteConfigSaxHandler extends DefaultHandler {
    private List<VideoVoteConfig> list = new ArrayList();

    public List<VideoVoteConfig> getList() {
        return this.list;
    }

    public void startDocument() {
        this.list = new ArrayList();
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        if (localName.equals("vote")) {
            try {
                this.list.add(new VideoVoteConfig(Integer.parseInt(attributes.getValue("min")), Integer.parseInt(attributes.getValue("max")), attributes.getValue("color")));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void endElement(String uri, String localName, String qName) {
    }

    public void endDocument() {
    }

    public void characters(char[] ch, int start, int length) {
    }
}
