package com.kandian.videoplayer;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import com.kandian.common.Log;
import java.io.IOException;

public class VideoView extends SurfaceView implements MediaController.MediaPlayerControl {
    private static boolean isFullScreen = false;
    /* access modifiers changed from: private */
    public String TAG;
    private MediaPlayer.OnBufferingUpdateListener mBufferingUpdateListener;
    private MediaPlayer.OnCompletionListener mCompletionListener;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public int mCurrentBufferPercentage;
    private int mDuration;
    private MediaPlayer.OnErrorListener mErrorListener;
    /* access modifiers changed from: private */
    public boolean mIsPrepared;
    /* access modifiers changed from: private */
    public MediaController mMediaController;
    /* access modifiers changed from: private */
    public MediaPlayer mMediaPlayer;
    /* access modifiers changed from: private */
    public MySizeChangeLinstener mMyChangeLinstener;
    /* access modifiers changed from: private */
    public MediaPlayer.OnCompletionListener mOnCompletionListener;
    /* access modifiers changed from: private */
    public MediaPlayer.OnErrorListener mOnErrorListener;
    /* access modifiers changed from: private */
    public MediaPlayer.OnPreparedListener mOnPreparedListener;
    MediaPlayer.OnPreparedListener mPreparedListener;
    SurfaceHolder.Callback mSHCallback;
    /* access modifiers changed from: private */
    public int mSeekWhenPrepared;
    MediaPlayer.OnVideoSizeChangedListener mSizeChangedListener;
    /* access modifiers changed from: private */
    public boolean mStartWhenPrepared;
    /* access modifiers changed from: private */
    public int mSurfaceHeight;
    /* access modifiers changed from: private */
    public SurfaceHolder mSurfaceHolder;
    /* access modifiers changed from: private */
    public int mSurfaceWidth;
    private Uri mUri;
    /* access modifiers changed from: private */
    public int mVideoHeight;
    /* access modifiers changed from: private */
    public int mVideoWidth;

    public interface MySizeChangeLinstener {
        void doMyThings();
    }

    public int getVideoWidth() {
        return this.mVideoWidth;
    }

    public int getVideoHeight() {
        return this.mVideoHeight;
    }

    public void setVideoScale(int width, int height) {
        ViewGroup.LayoutParams lp = getLayoutParams();
        lp.height = height;
        lp.width = width;
        setLayoutParams(lp);
    }

    public void setMySizeChangeLinstener(MySizeChangeLinstener l) {
        this.mMyChangeLinstener = l;
    }

    public VideoView(Context context) {
        super(context);
        this.TAG = "VideoView";
        this.mSurfaceHolder = null;
        this.mMediaPlayer = null;
        this.mSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener() {
            public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                VideoView.this.mVideoWidth = mp.getVideoWidth();
                VideoView.this.mVideoHeight = mp.getVideoHeight();
                Log.v(VideoView.this.TAG, "onVideoSizeChanged mVideoWidth:" + VideoView.this.mVideoWidth + ", mVideoHeight:" + VideoView.this.mVideoHeight);
                Log.v(VideoView.this.TAG, "onVideoSizeChanged width:" + width + ", height:" + height);
                if (VideoView.this.mMyChangeLinstener != null) {
                    VideoView.this.mMyChangeLinstener.doMyThings();
                }
                if (VideoView.this.mVideoWidth != 0) {
                }
            }
        };
        this.mPreparedListener = new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                VideoView.this.mIsPrepared = true;
                if (VideoView.this.mOnPreparedListener != null) {
                    VideoView.this.mOnPreparedListener.onPrepared(VideoView.this.mMediaPlayer);
                }
                if (VideoView.this.mMediaController != null) {
                    VideoView.this.mMediaController.setEnabled(true);
                }
                VideoView.this.mVideoWidth = mp.getVideoWidth();
                VideoView.this.mVideoHeight = mp.getVideoHeight();
                Log.v(VideoView.this.TAG, "mSeekWhenPrepared: " + VideoView.this.mSeekWhenPrepared + ", mStartWhenPrepared: " + VideoView.this.mStartWhenPrepared);
                Log.v(VideoView.this.TAG, "mVideoWidth: " + VideoView.this.mVideoWidth + ", mVideoHeight: " + VideoView.this.mVideoHeight);
                Log.v(VideoView.this.TAG, "mSurfaceWidth: " + VideoView.this.mSurfaceWidth + ", mSurfaceHeight: " + VideoView.this.mSurfaceHeight);
                if (VideoView.this.mVideoWidth == 0 || VideoView.this.mVideoHeight == 0) {
                    if (VideoView.this.mSeekWhenPrepared != 0) {
                        VideoView.this.mMediaPlayer.seekTo(VideoView.this.mSeekWhenPrepared);
                        VideoView.this.mSeekWhenPrepared = 0;
                    }
                    if (VideoView.this.mStartWhenPrepared) {
                        VideoView.this.mMediaPlayer.start();
                        VideoView.this.mStartWhenPrepared = false;
                        return;
                    }
                    return;
                }
                if (VideoView.this.mSeekWhenPrepared != 0) {
                    VideoView.this.mMediaPlayer.seekTo(VideoView.this.mSeekWhenPrepared);
                    VideoView.this.mSeekWhenPrepared = 0;
                }
                if (VideoView.this.mStartWhenPrepared) {
                    VideoView.this.mMediaPlayer.start();
                    VideoView.this.mStartWhenPrepared = false;
                } else if (VideoView.this.isPlaying()) {
                } else {
                    if ((VideoView.this.mSeekWhenPrepared != 0 || VideoView.this.getCurrentPosition() > 0) && VideoView.this.mMediaController != null) {
                        VideoView.this.mMediaController.show(0);
                    }
                }
            }
        };
        this.mCompletionListener = new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                Log.d(VideoView.this.TAG, "OnCompletion ");
                if (VideoView.this.mMediaController != null) {
                    VideoView.this.mMediaController.hide();
                }
                if (VideoView.this.mOnCompletionListener != null) {
                    VideoView.this.mOnCompletionListener.onCompletion(VideoView.this.mMediaPlayer);
                }
            }
        };
        this.mErrorListener = new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int framework_err, int impl_err) {
                Log.d(VideoView.this.TAG, "Error: " + framework_err + "," + impl_err);
                if (VideoView.this.mMediaController != null) {
                    VideoView.this.mMediaController.hide();
                }
                if (VideoView.this.mOnErrorListener != null && VideoView.this.mOnErrorListener.onError(VideoView.this.mMediaPlayer, framework_err, impl_err)) {
                    return true;
                }
                if (VideoView.this.getWindowToken() != null) {
                    VideoView.this.mContext.getResources();
                }
                return true;
            }
        };
        this.mBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                Log.v(VideoView.this.TAG, "reported percentage:" + percent);
                VideoView.this.mCurrentBufferPercentage = percent;
            }
        };
        this.mSHCallback = new SurfaceHolder.Callback() {
            public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
                VideoView.this.mSurfaceWidth = w;
                VideoView.this.mSurfaceHeight = h;
                if (VideoView.this.mMediaPlayer != null && VideoView.this.mIsPrepared && VideoView.this.mVideoWidth == w && VideoView.this.mVideoHeight == h) {
                    if (VideoView.this.mSeekWhenPrepared != 0) {
                        VideoView.this.mMediaPlayer.seekTo(VideoView.this.mSeekWhenPrepared);
                        VideoView.this.mSeekWhenPrepared = 0;
                    }
                    VideoView.this.mMediaPlayer.start();
                    if (VideoView.this.mMediaController != null) {
                        VideoView.this.mMediaController.show();
                    }
                }
            }

            public void surfaceCreated(SurfaceHolder holder) {
                VideoView.this.mSurfaceHolder = holder;
                VideoView.this.openVideo();
            }

            public void surfaceDestroyed(SurfaceHolder holder) {
                VideoView.this.mSurfaceHolder = null;
                if (VideoView.this.mMediaController != null) {
                    VideoView.this.mMediaController.hide();
                }
                if (VideoView.this.mMediaPlayer != null) {
                    VideoView.this.mMediaPlayer.reset();
                    VideoView.this.mMediaPlayer.release();
                    VideoView.this.mMediaPlayer = null;
                }
            }
        };
        this.mContext = context;
        initVideoView();
    }

    public VideoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        this.mContext = context;
        initVideoView();
    }

    public VideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.TAG = "VideoView";
        this.mSurfaceHolder = null;
        this.mMediaPlayer = null;
        this.mSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener() {
            public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                VideoView.this.mVideoWidth = mp.getVideoWidth();
                VideoView.this.mVideoHeight = mp.getVideoHeight();
                Log.v(VideoView.this.TAG, "onVideoSizeChanged mVideoWidth:" + VideoView.this.mVideoWidth + ", mVideoHeight:" + VideoView.this.mVideoHeight);
                Log.v(VideoView.this.TAG, "onVideoSizeChanged width:" + width + ", height:" + height);
                if (VideoView.this.mMyChangeLinstener != null) {
                    VideoView.this.mMyChangeLinstener.doMyThings();
                }
                if (VideoView.this.mVideoWidth != 0) {
                }
            }
        };
        this.mPreparedListener = new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                VideoView.this.mIsPrepared = true;
                if (VideoView.this.mOnPreparedListener != null) {
                    VideoView.this.mOnPreparedListener.onPrepared(VideoView.this.mMediaPlayer);
                }
                if (VideoView.this.mMediaController != null) {
                    VideoView.this.mMediaController.setEnabled(true);
                }
                VideoView.this.mVideoWidth = mp.getVideoWidth();
                VideoView.this.mVideoHeight = mp.getVideoHeight();
                Log.v(VideoView.this.TAG, "mSeekWhenPrepared: " + VideoView.this.mSeekWhenPrepared + ", mStartWhenPrepared: " + VideoView.this.mStartWhenPrepared);
                Log.v(VideoView.this.TAG, "mVideoWidth: " + VideoView.this.mVideoWidth + ", mVideoHeight: " + VideoView.this.mVideoHeight);
                Log.v(VideoView.this.TAG, "mSurfaceWidth: " + VideoView.this.mSurfaceWidth + ", mSurfaceHeight: " + VideoView.this.mSurfaceHeight);
                if (VideoView.this.mVideoWidth == 0 || VideoView.this.mVideoHeight == 0) {
                    if (VideoView.this.mSeekWhenPrepared != 0) {
                        VideoView.this.mMediaPlayer.seekTo(VideoView.this.mSeekWhenPrepared);
                        VideoView.this.mSeekWhenPrepared = 0;
                    }
                    if (VideoView.this.mStartWhenPrepared) {
                        VideoView.this.mMediaPlayer.start();
                        VideoView.this.mStartWhenPrepared = false;
                        return;
                    }
                    return;
                }
                if (VideoView.this.mSeekWhenPrepared != 0) {
                    VideoView.this.mMediaPlayer.seekTo(VideoView.this.mSeekWhenPrepared);
                    VideoView.this.mSeekWhenPrepared = 0;
                }
                if (VideoView.this.mStartWhenPrepared) {
                    VideoView.this.mMediaPlayer.start();
                    VideoView.this.mStartWhenPrepared = false;
                } else if (VideoView.this.isPlaying()) {
                } else {
                    if ((VideoView.this.mSeekWhenPrepared != 0 || VideoView.this.getCurrentPosition() > 0) && VideoView.this.mMediaController != null) {
                        VideoView.this.mMediaController.show(0);
                    }
                }
            }
        };
        this.mCompletionListener = new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                Log.d(VideoView.this.TAG, "OnCompletion ");
                if (VideoView.this.mMediaController != null) {
                    VideoView.this.mMediaController.hide();
                }
                if (VideoView.this.mOnCompletionListener != null) {
                    VideoView.this.mOnCompletionListener.onCompletion(VideoView.this.mMediaPlayer);
                }
            }
        };
        this.mErrorListener = new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int framework_err, int impl_err) {
                Log.d(VideoView.this.TAG, "Error: " + framework_err + "," + impl_err);
                if (VideoView.this.mMediaController != null) {
                    VideoView.this.mMediaController.hide();
                }
                if (VideoView.this.mOnErrorListener != null && VideoView.this.mOnErrorListener.onError(VideoView.this.mMediaPlayer, framework_err, impl_err)) {
                    return true;
                }
                if (VideoView.this.getWindowToken() != null) {
                    VideoView.this.mContext.getResources();
                }
                return true;
            }
        };
        this.mBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                Log.v(VideoView.this.TAG, "reported percentage:" + percent);
                VideoView.this.mCurrentBufferPercentage = percent;
            }
        };
        this.mSHCallback = new SurfaceHolder.Callback() {
            public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
                VideoView.this.mSurfaceWidth = w;
                VideoView.this.mSurfaceHeight = h;
                if (VideoView.this.mMediaPlayer != null && VideoView.this.mIsPrepared && VideoView.this.mVideoWidth == w && VideoView.this.mVideoHeight == h) {
                    if (VideoView.this.mSeekWhenPrepared != 0) {
                        VideoView.this.mMediaPlayer.seekTo(VideoView.this.mSeekWhenPrepared);
                        VideoView.this.mSeekWhenPrepared = 0;
                    }
                    VideoView.this.mMediaPlayer.start();
                    if (VideoView.this.mMediaController != null) {
                        VideoView.this.mMediaController.show();
                    }
                }
            }

            public void surfaceCreated(SurfaceHolder holder) {
                VideoView.this.mSurfaceHolder = holder;
                VideoView.this.openVideo();
            }

            public void surfaceDestroyed(SurfaceHolder holder) {
                VideoView.this.mSurfaceHolder = null;
                if (VideoView.this.mMediaController != null) {
                    VideoView.this.mMediaController.hide();
                }
                if (VideoView.this.mMediaPlayer != null) {
                    VideoView.this.mMediaPlayer.reset();
                    VideoView.this.mMediaPlayer.release();
                    VideoView.this.mMediaPlayer = null;
                }
            }
        };
        this.mContext = context;
        initVideoView();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.v(this.TAG, "onMeasure " + this.mVideoWidth + "," + this.mVideoHeight);
        Log.v(this.TAG, "onMeasure " + widthMeasureSpec + "," + heightMeasureSpec);
        int width = getDefaultSize(this.mVideoWidth, widthMeasureSpec);
        int height = getDefaultSize(this.mVideoHeight, heightMeasureSpec);
        if (this.mVideoWidth > 0 && this.mVideoHeight > 0 && !isFullScreen) {
            if (this.mVideoWidth * height > this.mVideoHeight * width) {
                height = (this.mVideoHeight * width) / this.mVideoWidth;
            } else if (this.mVideoWidth * height < this.mVideoHeight * width) {
                width = (this.mVideoWidth * height) / this.mVideoHeight;
            }
        }
        Log.v(this.TAG, "onMeasure measuredDimension:" + width + "," + height);
        setMeasuredDimension(width, height);
    }

    public int resolveAdjustedSize(int desiredSize, int measureSpec) {
        int result = desiredSize;
        int specMode = View.MeasureSpec.getMode(measureSpec);
        int specSize = View.MeasureSpec.getSize(measureSpec);
        switch (specMode) {
            case Integer.MIN_VALUE:
                return Math.min(desiredSize, specSize);
            case 0:
                return desiredSize;
            case 1073741824:
                return specSize;
            default:
                return result;
        }
    }

    private void initVideoView() {
        this.mVideoWidth = 0;
        this.mVideoHeight = 0;
        getHolder().addCallback(this.mSHCallback);
        getHolder().setType(3);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
    }

    public void setVideoPath(String path) {
        Log.v(this.TAG, "setVideoPath:'" + path + "'");
        setVideoURI(Uri.parse(path));
    }

    public void setVideoURI(Uri uri) {
        this.mUri = uri;
        this.mStartWhenPrepared = false;
        this.mSeekWhenPrepared = 0;
        openVideo();
        requestLayout();
        invalidate();
    }

    public void stopPlayback() {
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.stop();
            this.mMediaPlayer.release();
            this.mMediaPlayer = null;
        }
    }

    /* access modifiers changed from: private */
    public void openVideo() {
        if (this.mUri != null && this.mSurfaceHolder != null) {
            Intent i = new Intent("com.android.music.musicservicecommand");
            i.putExtra("command", "pause");
            this.mContext.sendBroadcast(i);
            if (this.mMediaPlayer != null) {
                this.mMediaPlayer.reset();
                this.mMediaPlayer.release();
                this.mMediaPlayer = null;
            }
            try {
                this.mMediaPlayer = new MediaPlayer();
                this.mMediaPlayer.setOnPreparedListener(this.mPreparedListener);
                this.mMediaPlayer.setOnVideoSizeChangedListener(this.mSizeChangedListener);
                this.mIsPrepared = false;
                Log.v(this.TAG, "reset duration to -1 in openVideo");
                this.mDuration = -1;
                this.mMediaPlayer.setOnCompletionListener(this.mCompletionListener);
                this.mMediaPlayer.setOnErrorListener(this.mErrorListener);
                this.mMediaPlayer.setOnBufferingUpdateListener(this.mBufferingUpdateListener);
                this.mCurrentBufferPercentage = 0;
                this.mMediaPlayer.setDataSource(this.mContext, this.mUri);
                this.mMediaPlayer.setDisplay(this.mSurfaceHolder);
                this.mMediaPlayer.setAudioStreamType(3);
                this.mMediaPlayer.setScreenOnWhilePlaying(true);
                this.mMediaPlayer.prepareAsync();
                attachMediaController();
            } catch (IOException e) {
                Log.w(this.TAG, "Unable to open content: " + this.mUri, e);
            } catch (IllegalArgumentException e2) {
                Log.w(this.TAG, "Unable to open content: " + this.mUri, e2);
            }
        }
    }

    public void setMediaController(MediaController controller) {
        if (this.mMediaController != null) {
            this.mMediaController.hide();
        }
        this.mMediaController = controller;
        attachMediaController();
    }

    private void attachMediaController() {
        View anchorView;
        if (this.mMediaPlayer != null && this.mMediaController != null) {
            this.mMediaController.setMediaPlayer(this);
            if (getParent() instanceof View) {
                anchorView = (View) getParent();
            } else {
                anchorView = this;
            }
            this.mMediaController.setAnchorView(anchorView);
            this.mMediaController.setEnabled(this.mIsPrepared);
        }
    }

    public void setOnPreparedListener(MediaPlayer.OnPreparedListener l) {
        this.mOnPreparedListener = l;
    }

    public void setOnCompletionListener(MediaPlayer.OnCompletionListener l) {
        this.mOnCompletionListener = l;
    }

    public void setOnErrorListener(MediaPlayer.OnErrorListener l) {
        this.mOnErrorListener = l;
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (!this.mIsPrepared || this.mMediaPlayer == null || this.mMediaController == null) {
            return false;
        }
        toggleMediaControlsVisiblity();
        return false;
    }

    public boolean onTrackballEvent(MotionEvent ev) {
        if (!this.mIsPrepared || this.mMediaPlayer == null || this.mMediaController == null) {
            return false;
        }
        toggleMediaControlsVisiblity();
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!(!this.mIsPrepared || keyCode == 4 || keyCode == 24 || keyCode == 25 || keyCode == 82 || keyCode == 5 || keyCode == 6 || this.mMediaPlayer == null || this.mMediaController == null)) {
            if (keyCode == 79 || keyCode == 85) {
                if (this.mMediaPlayer.isPlaying()) {
                    pause();
                    this.mMediaController.show();
                } else {
                    start();
                    this.mMediaController.hide();
                }
                return true;
            } else if (keyCode != 86 || !this.mMediaPlayer.isPlaying()) {
                toggleMediaControlsVisiblity();
            } else {
                pause();
                this.mMediaController.show();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void toggleMediaControlsVisiblity() {
        if (this.mMediaController.isShowing()) {
            this.mMediaController.hide();
        } else {
            this.mMediaController.show();
        }
    }

    public void start() {
        Log.v(this.TAG, "Start video!");
        if (this.mMediaPlayer == null || !this.mIsPrepared) {
            this.mStartWhenPrepared = true;
            return;
        }
        this.mMediaPlayer.start();
        this.mStartWhenPrepared = false;
    }

    public void pause() {
        Log.v(this.TAG, "pausing video!");
        if (this.mMediaPlayer != null && this.mIsPrepared && this.mMediaPlayer.isPlaying()) {
            this.mMediaPlayer.pause();
        }
        this.mStartWhenPrepared = false;
    }

    public int getDuration() {
        if (this.mMediaPlayer == null || !this.mIsPrepared) {
            this.mDuration = -1;
            return this.mDuration;
        } else if (this.mDuration > 0) {
            return this.mDuration;
        } else {
            this.mDuration = this.mMediaPlayer.getDuration();
            return this.mDuration;
        }
    }

    public int getCurrentPosition() {
        if (this.mMediaPlayer == null || !this.mIsPrepared) {
            return 0;
        }
        return this.mMediaPlayer.getCurrentPosition();
    }

    public void seekTo(int msec) {
        Log.v(this.TAG, "seek video to" + msec);
        if (this.mMediaPlayer == null || !this.mIsPrepared) {
            this.mSeekWhenPrepared = msec;
        } else {
            this.mMediaPlayer.seekTo(msec);
        }
    }

    public boolean isPlaying() {
        if (this.mMediaPlayer == null || !this.mIsPrepared) {
            return false;
        }
        return this.mMediaPlayer.isPlaying();
    }

    public int getBufferPercentage() {
        if (this.mMediaPlayer != null) {
            return this.mCurrentBufferPercentage;
        }
        return 0;
    }

    public boolean canPause() {
        return true;
    }

    public boolean canSeekBackward() {
        return true;
    }

    public boolean canSeekForward() {
        return true;
    }

    public void setFullScreen(boolean isFullScreen2) {
        isFullScreen = isFullScreen2;
    }

    public boolean isFullScreen() {
        return isFullScreen;
    }
}
