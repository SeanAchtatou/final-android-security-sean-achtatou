package com.kandian.videoplayer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.TextView;
import com.kandian.cartoonapp.R;
import com.kandian.common.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ThirdPartyVideoPlayerActivity extends Activity {
    public static Map<String, Integer> playHis = new HashMap();
    private final int COUNTDOWN_UPDATE = 1;
    private final int PLAY_COMPLETE = 3;
    private final int PLAY_NEXT = 2;
    private String TAG = "ThirdPartyVideoPlayerActivity";
    private Context _context = null;
    /* access modifiers changed from: private */
    public int countTime = 0;
    /* access modifiers changed from: private */
    public int currPlayUrlIndex = 0;
    private GestureDetector mGestureDetector = null;
    Handler myViewUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            TextView countdown_tv = (TextView) ThirdPartyVideoPlayerActivity.this.findViewById(R.id.countdown_label);
            switch (msg.what) {
                case 1:
                    countdown_tv.setText(String.valueOf(msg.arg1));
                    break;
                case 2:
                    countdown_tv.setText("");
                    if (!ThirdPartyVideoPlayerActivity.this.play_cancel && ThirdPartyVideoPlayerActivity.this.currPlayUrlIndex < ThirdPartyVideoPlayerActivity.this.urls.size()) {
                        ThirdPartyVideoPlayerActivity.playHis.put(ThirdPartyVideoPlayerActivity.this.taskDownloadDir, Integer.valueOf(ThirdPartyVideoPlayerActivity.this.currPlayUrlIndex));
                        Intent extPlayerIntent = new Intent();
                        extPlayerIntent.setDataAndType(Uri.parse((String) ThirdPartyVideoPlayerActivity.this.urls.get(ThirdPartyVideoPlayerActivity.this.currPlayUrlIndex)), "video/*");
                        extPlayerIntent.setAction("android.intent.action.VIEW");
                        ThirdPartyVideoPlayerActivity.this.startActivityForResult(extPlayerIntent, 0);
                        ThirdPartyVideoPlayerActivity thirdPartyVideoPlayerActivity = ThirdPartyVideoPlayerActivity.this;
                        thirdPartyVideoPlayerActivity.currPlayUrlIndex = thirdPartyVideoPlayerActivity.currPlayUrlIndex + 1;
                        break;
                    }
            }
            super.handleMessage(msg);
        }
    };
    /* access modifiers changed from: private */
    public boolean play_cancel = false;
    /* access modifiers changed from: private */
    public String taskDownloadDir = null;
    /* access modifiers changed from: private */
    public boolean ttFlag = false;
    /* access modifiers changed from: private */
    public ArrayList<String> urls = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.thirdparty_videoplayer_activity);
        this._context = this;
        this.urls = getIntent().getStringArrayListExtra("urls");
        this.currPlayUrlIndex = getIntent().getIntExtra("currPlayUrlIndex", 0);
        Log.v(this.TAG, "urls: " + this.urls.size());
        Log.v(this.TAG, "currPlayUrlIndex: " + this.currPlayUrlIndex);
        Intent extPlayerIntent = new Intent();
        extPlayerIntent.setDataAndType(Uri.parse(this.urls.get(this.currPlayUrlIndex)), "video/*");
        this.currPlayUrlIndex++;
        extPlayerIntent.setAction("android.intent.action.VIEW");
        startActivityForResult(extPlayerIntent, 0);
        this.taskDownloadDir = getIntent().getStringExtra("taskDownloadDir");
        this.mGestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
            public boolean onDoubleTap(MotionEvent e) {
                Message message = Message.obtain(ThirdPartyVideoPlayerActivity.this.myViewUpdateHandler);
                ThirdPartyVideoPlayerActivity.this.ttFlag = false;
                message.what = 2;
                message.sendToTarget();
                return true;
            }

            public boolean onSingleTapConfirmed(MotionEvent e) {
                Message message = Message.obtain(ThirdPartyVideoPlayerActivity.this.myViewUpdateHandler);
                ThirdPartyVideoPlayerActivity.this.ttFlag = false;
                message.what = 2;
                message.sendToTarget();
                return true;
            }

            public void onLongPress(MotionEvent e) {
                Message message = Message.obtain(ThirdPartyVideoPlayerActivity.this.myViewUpdateHandler);
                ThirdPartyVideoPlayerActivity.this.ttFlag = false;
                message.what = 2;
                message.sendToTarget();
            }
        });
        getWindow().addFlags(1024);
    }

    public boolean onTouchEvent(MotionEvent event) {
        boolean result = this.mGestureDetector.onTouchEvent(event);
        Log.v(this.TAG, "gesture result is " + result);
        if (result) {
            return result;
        }
        event.getAction();
        return super.onTouchEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (this.currPlayUrlIndex < this.urls.size()) {
            this.ttFlag = true;
            CountDown(3);
            return;
        }
        this.play_cancel = true;
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.play_cancel = true;
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        this.play_cancel = true;
        return super.onKeyDown(keyCode, event);
    }

    private void CountDown(int ct) {
        Timer time = new Timer(true);
        this.countTime = ct;
        time.schedule(new TimerTask() {
            public void run() {
                if (ThirdPartyVideoPlayerActivity.this.ttFlag) {
                    Message message = Message.obtain(ThirdPartyVideoPlayerActivity.this.myViewUpdateHandler);
                    if (ThirdPartyVideoPlayerActivity.this.countTime > 0) {
                        message.what = 1;
                        message.arg1 = ThirdPartyVideoPlayerActivity.this.countTime;
                        message.sendToTarget();
                        ThirdPartyVideoPlayerActivity thirdPartyVideoPlayerActivity = ThirdPartyVideoPlayerActivity.this;
                        thirdPartyVideoPlayerActivity.countTime = thirdPartyVideoPlayerActivity.countTime - 1;
                        return;
                    }
                    ThirdPartyVideoPlayerActivity.this.countTime = 0;
                    message.what = 2;
                    message.sendToTarget();
                    cancel();
                    return;
                }
                cancel();
            }
        }, 1000, 1000);
    }
}
