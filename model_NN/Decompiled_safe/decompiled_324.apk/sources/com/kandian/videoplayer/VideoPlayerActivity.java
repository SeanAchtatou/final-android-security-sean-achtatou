package com.kandian.videoplayer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.MessageQueue;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Display;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.kandian.cartoonapp.R;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.SiteConfigs;
import com.kandian.common.SystemConfigs;
import com.kandian.videoplayer.VideoView;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

public class VideoPlayerActivity extends Activity {
    private static final int SCREEN_DEFAULT = 1;
    private static final int SCREEN_FULL = 0;
    private static int screenHeight = 0;
    private static int screenWidth = 0;
    private final int HIDE_HEAD = 0;
    private final int SHOW_HEAD = 1;
    private final int SHOW_HEAD_TIMER = 5000;
    /* access modifiers changed from: private */
    public String TAG = "VideoPlayerActivity";
    /* access modifiers changed from: private */
    public View headView = null;
    /* access modifiers changed from: private */
    public PopupWindow headWindow = null;
    private boolean isChangedVideo = false;
    /* access modifiers changed from: private */
    public boolean isFullScreen = false;
    private boolean isPlaying = true;
    /* access modifiers changed from: private */
    public boolean isShowHead = false;
    private boolean keepingPausedTime = false;
    private GestureDetector mGestureDetector = null;
    private KeyguardManager.KeyguardLock mLock = null;
    private TextView mSubjectView = null;
    private TextView mVideoPathView = null;
    /* access modifiers changed from: private */
    public VideoView mVideoView = null;
    private MediaController mc = null;
    Handler myViewUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    VideoPlayerActivity.this.hideHead();
                    break;
            }
            super.handleMessage(msg);
        }
    };
    CharSequence[] names = null;
    public LinkedList<MovieInfo> playList = new LinkedList<>();
    private int playedTime = 0;
    /* access modifiers changed from: private */
    public int position = -1;
    private LocalProxy proxy = LocalProxy.instance();
    private SiteConfigs siteConfigs = null;

    public class MovieInfo implements Cloneable {
        final String displayName;
        final String path;
        final String referer;

        public MovieInfo(String name, String p, String ref) {
            this.displayName = new String(name);
            this.path = new String(p);
            if (ref != null) {
                this.referer = new String(ref);
            } else {
                this.referer = null;
            }
        }

        public Object clone() {
            try {
                return (MovieInfo) super.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(this.TAG, "onCreate");
        getScreenSize();
        setContentView((int) R.layout.videoplayer_activity);
        this.mLock = ((KeyguardManager) getSystemService("keyguard")).newKeyguardLock(this.TAG);
        handleIntent(getIntent());
        Looper.myQueue().addIdleHandler(new MessageQueue.IdleHandler() {
            public boolean queueIdle() {
                if (VideoPlayerActivity.this.headWindow != null && VideoPlayerActivity.this.mVideoView.isShown()) {
                    VideoPlayerActivity.this.headWindow.showAtLocation(VideoPlayerActivity.this.mVideoView, 48, 0, 0);
                    VideoPlayerActivity.this.showHead();
                }
                return false;
            }
        });
        this.headView = getLayoutInflater().inflate((int) R.layout.videoplayer_head, (ViewGroup) null);
        this.headWindow = new PopupWindow(this.headView);
        ((Button) this.headView.findViewById(R.id.video_back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VideoPlayerActivity.this.finish();
            }
        });
        ((Button) this.headView.findViewById(R.id.video_hide)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VideoPlayerActivity.this.hideHead();
            }
        });
        Button screenButton = (Button) this.headView.findViewById(R.id.screen_size);
        this.isFullScreen = getSharedPreferences(getString(R.string.video_progress_settings), 0).getBoolean(getString(R.string.video_fullscreen_attr), false);
        if (this.isFullScreen) {
            screenButton.setText((int) R.string.original_screen_text);
        } else {
            screenButton.setText((int) R.string.full_screen_text);
        }
        screenButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VideoView myVideoView = (VideoView) VideoPlayerActivity.this.findViewById(R.id.VideoView01);
                if (VideoPlayerActivity.this.isFullScreen) {
                    if (myVideoView != null) {
                        Log.v(VideoPlayerActivity.this.TAG, "changing isFullScreen to false");
                        VideoPlayerActivity.this.setVideoScale(1);
                    }
                } else if (myVideoView != null) {
                    Log.v(VideoPlayerActivity.this.TAG, "changing isFullScreen to true");
                    VideoPlayerActivity.this.setVideoScale(0);
                }
            }
        });
        this.mSubjectView = (TextView) this.headView.findViewById(R.id.video_subject);
        this.mVideoPathView = (TextView) this.headView.findViewById(R.id.video_url_path);
        this.mVideoView = (VideoView) findViewById(R.id.VideoView01);
        this.mc = new MediaController(this);
        this.mc.setAnchorView(this.mVideoView);
        this.mVideoView.setMediaController(this.mc);
        this.mVideoView.requestFocus();
        this.mc.setPrevNextListeners(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v(VideoPlayerActivity.this.TAG, "click Previous on position " + VideoPlayerActivity.this.position);
                VideoPlayerActivity videoPlayerActivity = VideoPlayerActivity.this;
                int access$7 = videoPlayerActivity.position + 1;
                videoPlayerActivity.position = access$7;
                if (access$7 < VideoPlayerActivity.this.playList.size()) {
                    VideoPlayerActivity.this.restartPlay(VideoPlayerActivity.this.position);
                    return;
                }
                VideoPlayerActivity.this.position = VideoPlayerActivity.this.playList.size() - 1;
                VideoPlayerActivity.this.restartPlay(VideoPlayerActivity.this.position);
            }
        }, new View.OnClickListener() {
            public void onClick(View v) {
                Log.v(VideoPlayerActivity.this.TAG, "click Previous on position " + VideoPlayerActivity.this.position);
                VideoPlayerActivity videoPlayerActivity = VideoPlayerActivity.this;
                int access$7 = videoPlayerActivity.position - 1;
                videoPlayerActivity.position = access$7;
                if (access$7 >= 0) {
                    VideoPlayerActivity.this.restartPlay(VideoPlayerActivity.this.position);
                    return;
                }
                VideoPlayerActivity.this.position = 0;
                VideoPlayerActivity.this.restartPlay(0);
            }
        });
        this.mGestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
            public boolean onDoubleTap(MotionEvent e) {
                if (VideoPlayerActivity.this.isFullScreen) {
                    VideoPlayerActivity.this.setVideoScale(1);
                } else {
                    VideoPlayerActivity.this.setVideoScale(0);
                }
                Log.d(VideoPlayerActivity.this.TAG, "onDoubleTap");
                return true;
            }

            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (!VideoPlayerActivity.this.isShowHead) {
                    VideoPlayerActivity.this.showHead();
                }
                Log.d(VideoPlayerActivity.this.TAG, "onSingleTapConfirmed");
                return true;
            }

            public void onLongPress(MotionEvent e) {
                if (!VideoPlayerActivity.this.isShowHead) {
                    VideoPlayerActivity.this.showHead();
                }
                Log.d(VideoPlayerActivity.this.TAG, "onLongPress");
            }
        });
        this.mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer arg0) {
                ProgressBar bar = (ProgressBar) VideoPlayerActivity.this.headView.findViewById(R.id.loading_progress);
                if (bar != null) {
                    bar.setVisibility(8);
                }
                if (VideoPlayerActivity.this.isFullScreen) {
                    VideoPlayerActivity.this.setVideoScale(0);
                } else {
                    VideoPlayerActivity.this.setVideoScale(1);
                }
                if (VideoPlayerActivity.this.getString(R.string.autohide_videoplayer_head) != null && VideoPlayerActivity.this.getString(R.string.autohide_videoplayer_head).equals("true")) {
                    VideoPlayerActivity.this.myViewUpdateHandler.removeMessages(0);
                    VideoPlayerActivity.this.myViewUpdateHandler.sendEmptyMessageDelayed(0, 5000);
                }
            }
        });
        this.mVideoView.setMySizeChangeLinstener(new VideoView.MySizeChangeLinstener() {
            public void doMyThings() {
                if (VideoPlayerActivity.this.isFullScreen) {
                    VideoPlayerActivity.this.setVideoScale(0);
                } else {
                    VideoPlayerActivity.this.setVideoScale(1);
                }
            }
        });
        this.mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                ProgressBar bar = (ProgressBar) VideoPlayerActivity.this.headView.findViewById(R.id.loading_progress);
                if (bar != null) {
                    bar.setVisibility(8);
                }
                VideoPlayerActivity.this.mVideoView.stopPlayback();
                switch (what) {
                    case 1:
                        Log.v(VideoPlayerActivity.this.TAG, "Unknown play error");
                        break;
                    case HttpStatus.SC_CONTINUE /*100*/:
                        Log.v(VideoPlayerActivity.this.TAG, "Server died");
                        break;
                }
                new AlertDialog.Builder(VideoPlayerActivity.this).setTitle("提示").setMessage("您的播放已中断，可能是：您的网络不稳定；或您的手机不能播放当前内容。").setPositiveButton("返回", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        VideoPlayerActivity.this.finish();
                    }
                }).setNeutralButton("重播", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        VideoPlayerActivity.this.restartPlay(VideoPlayerActivity.this.position);
                    }
                }).setNegativeButton("帮助", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        intent.setAction("android.intent.action.VIEW");
                        intent.setData(Uri.parse("http://sinaurl.cn/hqD9U9"));
                        VideoPlayerActivity.this.startActivity(intent);
                    }
                }).setCancelable(false).show();
                return false;
            }
        });
        this.mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer arg0) {
                Log.v(VideoPlayerActivity.this.TAG, "Completioin");
                int n = VideoPlayerActivity.this.playList.size();
                VideoPlayerActivity videoPlayerActivity = VideoPlayerActivity.this;
                int access$7 = videoPlayerActivity.position + 1;
                videoPlayerActivity.position = access$7;
                if (access$7 < n) {
                    Log.v(VideoPlayerActivity.this.TAG, "mVideoView OnCompletionListener: startPlay position " + VideoPlayerActivity.this.position);
                    VideoPlayerActivity.this.restartPlay(VideoPlayerActivity.this.position);
                    return;
                }
                VideoPlayerActivity.this.mVideoView.stopPlayback();
                VideoPlayerActivity.this.finish();
            }
        });
        getWindow().addFlags(1024);
        this.siteConfigs = SystemConfigs.instance(getApplication()).getSiteConfigs();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        Log.v(this.TAG, "handling new Intent");
        this.isPlaying = true;
        handleIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void handleIntent(Intent intent) {
        String aReferer;
        ArrayList<String> urls = intent.getStringArrayListExtra("urls");
        String referer = intent.getStringExtra("referer");
        ArrayList<String> referers = intent.getStringArrayListExtra("referers");
        ArrayList<String> titles = intent.getStringArrayListExtra("titles");
        Log.v(this.TAG, "urls: " + urls);
        Log.v(this.TAG, "referer: " + referer);
        Log.v(this.TAG, "referers: " + referers);
        Log.v(this.TAG, "titles: " + titles);
        Uri data = intent.getData();
        this.playList.clear();
        if (urls != null && urls.size() > 0) {
            this.names = new CharSequence[urls.size()];
            for (int i = 0; i < urls.size(); i++) {
                if (referers == null || referers.size() != urls.size()) {
                    aReferer = referer;
                } else {
                    aReferer = referers.get(i);
                }
                String aTitle = "第" + (i + 1) + "节";
                if (titles != null && titles.size() == urls.size()) {
                    aTitle = String.valueOf(aTitle) + " " + titles.get(i);
                }
                MovieInfo mi = new MovieInfo(aTitle, urls.get(i), aReferer);
                this.playList.add(mi);
                this.names[i] = new String(mi.displayName);
            }
        } else if (data != null) {
            this.names = new CharSequence[1];
            MovieInfo mi2 = new MovieInfo(data.getLastPathSegment(), data.toString(), null);
            this.playList.add(mi2);
            this.names[0] = new String(mi2.displayName);
        }
        if (referers != null) {
            this.keepingPausedTime = false;
        } else {
            this.keepingPausedTime = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.i(this.TAG, "onDestroy");
        if (this.headWindow != null) {
            this.headWindow.dismiss();
        }
        super.onDestroy();
    }

    public void restartPlay(int positionToPlay) {
        this.playedTime = 0;
        this.isPlaying = true;
        startPlay(positionToPlay);
    }

    public void startPlay(int positionToPlay) {
        Log.v(this.TAG, "loading " + this.playList.get(positionToPlay).path);
        this.mSubjectView.setText(String.valueOf(this.playList.get(positionToPlay).displayName) + "/共" + this.playList.size() + "节");
        if (this.playList.get(positionToPlay).referer != null) {
            this.mVideoPathView.setText(Html.fromHtml("<a href=\"" + this.playList.get(positionToPlay).referer + "\" link=\"#FFFFFF\">" + this.playList.get(positionToPlay).referer + "</a>"));
            this.mVideoPathView.setMovementMethod(LinkMovementMethod.getInstance());
        }
        String realUrl = this.playList.get(positionToPlay).path;
        Log.v(this.TAG, "loading real url: " + realUrl);
        if (this.playList.get(positionToPlay).referer == null || !URLUtil.isNetworkUrl(realUrl) || this.siteConfigs == null || this.siteConfigs.isReferer(this.playList.get(positionToPlay).referer) != 1) {
            this.mVideoView.setVideoURI(Uri.parse(realUrl));
        } else {
            Log.v(this.TAG, "uses referer");
            this.proxy.setReferer(realUrl, this.playList.get(positionToPlay).referer);
            this.mVideoView.setVideoURI(Uri.parse("http://" + LocalProxy.DEFAULT_PROXY_HOST + ":" + this.proxy.getPort() + CookieSpec.PATH_DELIM + realUrl));
        }
        ProgressBar bar = (ProgressBar) this.headView.findViewById(R.id.loading_progress);
        if (bar != null) {
            bar.setVisibility(0);
        }
        this.position = positionToPlay;
        if (this.playedTime > 0) {
            this.mVideoView.seekTo(this.playedTime);
        }
        if (this.isPlaying) {
            this.mVideoView.start();
        }
        if (getString(R.string.autohide_videoplayer_head) != null && getString(R.string.autohide_videoplayer_head).equals("true")) {
            showHead();
        }
    }

    public String getRedirectedUrl(String url, String referer) {
        if (!URLUtil.isNetworkUrl(url)) {
            return url;
        }
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);
        HttpContext context = new BasicHttpContext();
        context.setAttribute("Accept", "*/*");
        context.setAttribute("Accept-Encoding", "gzip, deflate");
        context.setAttribute("Accept-Language", "zh-cn");
        if (referer != null) {
            Log.v(this.TAG, "setting referer in getRedirectedUrl " + referer);
            context.setAttribute("Referer", referer);
        }
        context.setAttribute("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; zh-cn) AppleWebKit/533.17.8 (KHTML, like Gecko) Version/5.0.1 Safari/533.17.8");
        try {
            HttpResponse response = httpClient.execute(httpget, context);
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new IOException(response.getStatusLine().toString());
            }
            Log.v(this.TAG, "content type is " + response.getEntity().getContentType());
            Log.v(this.TAG, "status is " + response.getStatusLine().toString());
            String currentUrl = String.valueOf(((HttpHost) context.getAttribute("http.target_host")).toURI()) + ((HttpUriRequest) context.getAttribute("http.request")).getURI();
            Log.v(this.TAG, "real flv is at " + currentUrl);
            return currentUrl;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        boolean result = this.mGestureDetector.onTouchEvent(event);
        Log.v(this.TAG, "gesture result is " + result);
        if (result) {
            return result;
        }
        event.getAction();
        return super.onTouchEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.isPlaying = false;
        this.playedTime = this.mVideoView.getCurrentPosition();
        Log.i(this.TAG, "onPause " + this.playedTime);
        this.mVideoView.pause();
        if (this.playList != null && this.playList.size() > 0 && this.keepingPausedTime) {
            SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.video_progress_settings), 0).edit();
            editor.putInt(String.valueOf(this.playList.get(0).path) + "_position", this.position);
            editor.putInt(String.valueOf(this.playList.get(0).path) + "_playedTime", this.playedTime);
            editor.commit();
        }
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.i(this.TAG, "onResume");
        if (getRequestedOrientation() != 0) {
            setRequestedOrientation(0);
        }
        if (this.mVideoView.isPlaying()) {
            Log.v(this.TAG, "mVideoView isPlaying");
        } else if (this.isPlaying && this.playList != null && this.playList.size() > 0) {
            try {
                SharedPreferences settings = getSharedPreferences(getString(R.string.video_progress_settings), 0);
                this.isFullScreen = settings.getBoolean(getString(R.string.video_fullscreen_attr), false);
                int oldPosition = settings.getInt(String.valueOf(this.playList.get(0).path) + "_position", -1);
                int oldPlayedTime = settings.getInt(String.valueOf(this.playList.get(0).path) + "_playedTime", -1);
                if (oldPlayedTime == -1 || oldPosition < 0 || oldPosition >= this.playList.size()) {
                    restartPlay(0);
                } else {
                    this.playedTime = oldPlayedTime;
                    startPlay(oldPosition);
                }
                Log.v(this.TAG, "resume at position " + oldPosition + " playedTime " + oldPlayedTime + " by " + getString(R.string.video_progress_settings));
            } catch (Exception e) {
                SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.video_progress_settings), 0).edit();
                editor.clear();
                editor.commit();
                restartPlay(0);
            }
        }
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    /* access modifiers changed from: private */
    public void setVideoScale(int flag) {
        Button screenButton = (Button) this.headView.findViewById(R.id.screen_size);
        switch (flag) {
            case 0:
                setFullScreen(true);
                Log.v(this.TAG, "setting video scale to screenWidth:" + screenWidth + " screenHeight: " + screenHeight);
                this.mVideoView.setFullScreen(true);
                this.mVideoView.setVideoScale(screenWidth, screenHeight);
                screenButton.setText((int) R.string.original_screen_text);
                return;
            case 1:
                setFullScreen(false);
                int videoWidth = this.mVideoView.getVideoWidth();
                int videoHeight = this.mVideoView.getVideoHeight();
                int mWidth = screenWidth;
                int mHeight = screenHeight;
                if (videoWidth > 0 && videoHeight > 0) {
                    if (videoWidth * mHeight >= mWidth * videoHeight) {
                        mHeight = (mWidth * videoHeight) / videoWidth;
                    } else if (videoWidth * mHeight < mWidth * videoHeight) {
                        mWidth = (mHeight * videoWidth) / videoHeight;
                    }
                }
                Log.v(this.TAG, "setting video scale to mWidth: " + mWidth + " mHeight: " + mHeight);
                this.mVideoView.setFullScreen(false);
                this.mVideoView.setVideoScale(mWidth, mHeight);
                screenButton.setText((int) R.string.full_screen_text);
                return;
            default:
                return;
        }
    }

    private void getScreenSize() {
        Display display = getWindowManager().getDefaultDisplay();
        screenHeight = display.getHeight();
        screenWidth = display.getWidth();
        if (screenHeight > screenWidth) {
            int tmp = screenWidth;
            screenWidth = screenHeight;
            screenHeight = tmp;
        }
    }

    /* access modifiers changed from: private */
    public void hideHead() {
        if (this.headWindow != null) {
            this.headWindow.update(-10, 0, screenWidth, 0);
        }
        this.isShowHead = false;
    }

    /* access modifiers changed from: private */
    public void showHead() {
        if (this.headWindow != null) {
            this.headWindow.update(0, 0, screenWidth, 180);
        }
        this.isShowHead = true;
    }

    public void setFullScreen(boolean fullScreen) {
        this.isFullScreen = fullScreen;
        SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.video_progress_settings), 0).edit();
        editor.putBoolean(getString(R.string.video_fullscreen_attr), this.isFullScreen);
        editor.commit();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.videoplayeractivitymenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_video_chapters:
                Dialog dialog = new AlertDialog.Builder(this).setTitle((int) R.string.select_chapter_dialog_title).setItems(this.names, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("DIALOG", "DISMISS");
                        dialog.dismiss();
                        VideoPlayerActivity.this.restartPlay(which);
                    }
                }).setPositiveButton((int) R.string.select_chapter_dialog_back, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        VideoPlayerActivity.this.mVideoView.start();
                    }
                }).create();
                this.mVideoView.pause();
                dialog.show();
                return true;
            case R.id.menu_video_reload:
                restartPlay(this.position);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
