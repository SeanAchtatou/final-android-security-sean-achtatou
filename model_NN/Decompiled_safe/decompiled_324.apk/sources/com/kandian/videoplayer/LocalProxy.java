package com.kandian.videoplayer;

import com.kandian.common.Log;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.util.HashMap;

public class LocalProxy extends Thread {
    public static final int DEFAULT_PORT = 8070;
    public static final String DEFAULT_PROXY_HOST = "127.0.0.1";
    public static String TAG = "LocalProxy";
    private static LocalProxy _instance = null;
    private int debugLevel = 1;
    private PrintStream debugOut = System.out;
    private int fwdPort = 0;
    private String fwdServer = "";
    private ThreadGroup proxyThreads = new ThreadGroup("proxies");
    private int ptTimeout = 20000;
    private HashMap<String, String> refererCache = new HashMap<>();
    private ServerSocket server = null;
    private boolean stopFlag = false;
    private int thisPort = DEFAULT_PORT;

    public static LocalProxy instance() {
        if (_instance == null) {
            _instance = new LocalProxy(DEFAULT_PORT);
            _instance.start();
        } else if (_instance.isStopFlag()) {
            Log.v(TAG, "trying to restart proxy");
            _instance.setStopFlag(false);
            _instance.start();
        }
        return _instance;
    }

    public static void main(String[] args) {
        String fwdProxyServer = "";
        int fwdProxyPort = 0;
        if (args.length == 0) {
            System.err.println("USAGE: java jProxy <port number> [<fwd proxy> <fwd port>]");
            System.err.println("  <port number>   the port this service listens on");
            System.err.println("  <fwd proxy>     optional proxy server to forward requests to");
            System.err.println("  <fwd port>      the port that the optional proxy server is on");
            System.err.println("\nHINT: if you don't want to see all the debug information flying by,");
            System.err.println("you can pipe the output to a file or to 'nul' using \">\". For example:");
            System.err.println("  to send output to the file prox.txt: java jProxy 8080 > prox.txt");
            System.err.println("  to make the output go away: java jProxy 8080 > nul");
            return;
        }
        int port = Integer.parseInt(args[0]);
        if (args.length > 2) {
            fwdProxyServer = args[1];
            fwdProxyPort = Integer.parseInt(args[2]);
        }
        System.err.println("  **  Starting jProxy on port " + port + ". Press CTRL-C to end.  **\n");
        LocalProxy jp = new LocalProxy(port, fwdProxyServer, fwdProxyPort, 20);
        jp.setDebug(1, System.out);
        jp.start();
        while (true) {
            try {
                Thread.sleep(3000);
            } catch (Exception e) {
            }
        }
    }

    protected LocalProxy(int port) {
        this.thisPort = port;
    }

    protected LocalProxy(int port, String proxyServer, int proxyPort) {
        this.thisPort = port;
        this.fwdServer = proxyServer;
        this.fwdPort = proxyPort;
    }

    protected LocalProxy(int port, String proxyServer, int proxyPort, int timeout) {
        this.thisPort = port;
        this.fwdServer = proxyServer;
        this.fwdPort = proxyPort;
        this.ptTimeout = timeout;
    }

    public void setDebug(int level, PrintStream out) {
        this.debugLevel = level;
        this.debugOut = out;
    }

    public int getPort() {
        return this.thisPort;
    }

    public boolean isRunning() {
        if (this.server == null) {
            return false;
        }
        return true;
    }

    public void closeSocket() {
        Log.v(TAG, "closing server socket!");
        try {
            this.server.close();
        } catch (Exception e) {
            Exception e2 = e;
            if (this.debugLevel > 0) {
                this.debugOut.println(e2);
            }
        }
        this.server = null;
    }

    public void run() {
        try {
            this.server = new ServerSocket(this.thisPort);
            if (this.debugLevel > 0) {
                this.debugOut.println("Started jProxy on port " + this.thisPort);
            }
            while (!isStopFlag()) {
                ProxyThread t = new ProxyThread(this.proxyThreads, this.server.accept(), this.fwdServer, this.fwdPort);
                t.setDebug(this.debugLevel, this.debugOut);
                t.setTimeout(this.ptTimeout);
                t.start();
                Log.v(TAG, String.valueOf(this.proxyThreads.activeCount()) + " proxies are active.");
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (this.debugLevel > 0) {
                this.debugOut.println("jProxy Thread error: " + e2);
            }
        }
        closeSocket();
    }

    public void setReferer(String url, String refererUrl) {
        this.refererCache.put(url, refererUrl);
    }

    public String getReferer(String url) {
        return this.refererCache.get(url);
    }

    public void setStopFlag(boolean stopFlag2) {
        this.stopFlag = stopFlag2;
    }

    public boolean isStopFlag() {
        return this.stopFlag;
    }
}
