package com.kandian.videoplayer;

import com.kandian.common.Log;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import org.apache.commons.httpclient.HttpStatus;

/* compiled from: LocalProxy */
class ProxyThread extends Thread {
    public static final int DEFAULT_TIMEOUT = 20000;
    private static String TAG = "ProxyThread";
    private int debugLevel = 0;
    private PrintStream debugOut = System.out;
    private int fwdPort = 0;
    private String fwdServer = "";
    private Socket pSocket;
    private LocalProxy proxy = LocalProxy.instance();
    private int socketTimeout = 20000;

    /* compiled from: LocalProxy */
    class MyHttpRequest {
        String hostName = "";
        String hostPort = "";
        byte[] request = null;

        MyHttpRequest() {
        }
    }

    public ProxyThread(ThreadGroup tg, Socket s) {
        super(tg, new StringBuilder(String.valueOf(s.getLocalPort())).toString());
        this.pSocket = s;
    }

    public ProxyThread(ThreadGroup tg, Socket s, String proxy2, int port) {
        super(tg, new StringBuilder(String.valueOf(s.getLocalPort())).toString());
        this.pSocket = s;
        this.fwdServer = proxy2;
        this.fwdPort = port;
    }

    public void setTimeout(int timeout) {
        this.socketTimeout = timeout * 1000;
    }

    public void setDebug(int level, PrintStream out) {
        this.debugLevel = level;
        this.debugOut = out;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x01db A[Catch:{ Exception -> 0x03bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0244 A[Catch:{ Exception -> 0x03bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0258 A[Catch:{ Exception -> 0x03bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x02f9 A[Catch:{ Exception -> 0x03bd }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r35 = this;
            long r28 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x03bd }
            java.io.BufferedInputStream r5 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x03bd }
            r0 = r35
            java.net.Socket r0 = r0.pSocket     // Catch:{ Exception -> 0x03bd }
            r31 = r0
            java.io.InputStream r31 = r31.getInputStream()     // Catch:{ Exception -> 0x03bd }
            r0 = r5
            r1 = r31
            r0.<init>(r1)     // Catch:{ Exception -> 0x03bd }
            java.io.BufferedOutputStream r6 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x03bd }
            r0 = r35
            java.net.Socket r0 = r0.pSocket     // Catch:{ Exception -> 0x03bd }
            r31 = r0
            java.io.OutputStream r31 = r31.getOutputStream()     // Catch:{ Exception -> 0x03bd }
            r0 = r6
            r1 = r31
            r0.<init>(r1)     // Catch:{ Exception -> 0x03bd }
            r24 = 0
            r20 = 0
            byte[] r20 = (byte[]) r20     // Catch:{ Exception -> 0x03bd }
            r22 = 0
            byte[] r22 = (byte[]) r22     // Catch:{ Exception -> 0x03bd }
            r21 = 0
            r23 = 0
            r19 = -1
            java.lang.StringBuffer r11 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03bd }
            java.lang.String r31 = ""
            r0 = r11
            r1 = r31
            r0.<init>(r1)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r12 = ""
            r13 = 80
            r31 = 0
            r0 = r35
            r1 = r5
            r2 = r11
            r3 = r31
            byte[] r20 = r0.getHTTPData(r1, r2, r3)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r31 = com.kandian.videoplayer.ProxyThread.TAG     // Catch:{ Exception -> 0x03bd }
            java.lang.StringBuilder r32 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = "original request: "
            r32.<init>(r33)     // Catch:{ Exception -> 0x03bd }
            r0 = r32
            r1 = r20
            java.lang.StringBuilder r32 = r0.append(r1)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r32 = r32.toString()     // Catch:{ Exception -> 0x03bd }
            com.kandian.common.Log.v(r31, r32)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r31 = r11.toString()     // Catch:{ Exception -> 0x03bd }
            java.lang.String r32 = "127.0.0.1"
            boolean r31 = r31.contains(r32)     // Catch:{ Exception -> 0x03bd }
            if (r31 == 0) goto L_0x0160
            java.lang.String r31 = com.kandian.videoplayer.ProxyThread.TAG     // Catch:{ Exception -> 0x03bd }
            java.lang.StringBuilder r32 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = "host is "
            r32.<init>(r33)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = r11.toString()     // Catch:{ Exception -> 0x03bd }
            java.lang.StringBuilder r32 = r32.append(r33)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r32 = r32.toString()     // Catch:{ Exception -> 0x03bd }
            com.kandian.common.Log.v(r31, r32)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r30 = new java.lang.String     // Catch:{ Exception -> 0x03bd }
            r0 = r30
            r1 = r20
            r0.<init>(r1)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r31 = com.kandian.videoplayer.ProxyThread.TAG     // Catch:{ Exception -> 0x03bd }
            java.lang.StringBuilder r32 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = "tmp is:"
            r32.<init>(r33)     // Catch:{ Exception -> 0x03bd }
            r0 = r32
            r1 = r30
            java.lang.StringBuilder r32 = r0.append(r1)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r32 = r32.toString()     // Catch:{ Exception -> 0x03bd }
            com.kandian.common.Log.v(r31, r32)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r31 = "http://"
            int r15 = r30.indexOf(r31)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r31 = "/"
            int r32 = r15 + 7
            int r16 = r30.indexOf(r31, r32)     // Catch:{ Exception -> 0x03bd }
            r17 = 0
            r31 = -1
            r0 = r15
            r1 = r31
            if (r0 == r1) goto L_0x0354
            r31 = -1
            r0 = r16
            r1 = r31
            if (r0 == r1) goto L_0x0354
            int r31 = r15 + 7
            r0 = r30
            r1 = r31
            r2 = r16
            java.lang.String r17 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x03bd }
        L_0x00da:
            if (r17 == 0) goto L_0x00ea
            r31 = 0
            r0 = r11
            r1 = r31
            r0.setLength(r1)     // Catch:{ Exception -> 0x03bd }
            r0 = r11
            r1 = r17
            r0.append(r1)     // Catch:{ Exception -> 0x03bd }
        L_0x00ea:
            java.lang.StringBuilder r31 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03bd }
            r32 = 0
            r33 = 1
            int r33 = r15 - r33
            r0 = r30
            r1 = r32
            r2 = r33
            java.lang.String r32 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r32 = java.lang.String.valueOf(r32)     // Catch:{ Exception -> 0x03bd }
            r31.<init>(r32)     // Catch:{ Exception -> 0x03bd }
            r0 = r30
            r1 = r16
            java.lang.String r32 = r0.substring(r1)     // Catch:{ Exception -> 0x03bd }
            java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r18 = r31.toString()     // Catch:{ Exception -> 0x03bd }
            java.lang.String r31 = "http://127.0.0.1:8070/"
            java.lang.String r32 = ""
            r0 = r18
            r1 = r31
            r2 = r32
            java.lang.String r18 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r31 = "127.0.0.1:8070"
            r0 = r18
            r1 = r31
            r2 = r17
            java.lang.String r18 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r31 = com.kandian.videoplayer.ProxyThread.TAG     // Catch:{ Exception -> 0x03bd }
            java.lang.StringBuilder r32 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = "new Request:"
            r32.<init>(r33)     // Catch:{ Exception -> 0x03bd }
            r0 = r32
            r1 = r18
            java.lang.StringBuilder r32 = r0.append(r1)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r32 = r32.toString()     // Catch:{ Exception -> 0x03bd }
            com.kandian.common.Log.v(r31, r32)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r31 = com.kandian.videoplayer.ProxyThread.TAG     // Catch:{ Exception -> 0x03bd }
            java.lang.StringBuilder r32 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = "host:"
            r32.<init>(r33)     // Catch:{ Exception -> 0x03bd }
            r0 = r32
            r1 = r11
            java.lang.StringBuilder r32 = r0.append(r1)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r32 = r32.toString()     // Catch:{ Exception -> 0x03bd }
            com.kandian.common.Log.v(r31, r32)     // Catch:{ Exception -> 0x03bd }
            byte[] r20 = r18.getBytes()     // Catch:{ Exception -> 0x03bd }
        L_0x0160:
            int r21 = java.lang.reflect.Array.getLength(r20)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r12 = r11.toString()     // Catch:{ Exception -> 0x03bd }
            java.lang.String r31 = ":"
            r0 = r12
            r1 = r31
            int r19 = r0.indexOf(r1)     // Catch:{ Exception -> 0x03bd }
            if (r19 <= 0) goto L_0x018b
            int r31 = r19 + 1
            r0 = r12
            r1 = r31
            java.lang.String r31 = r0.substring(r1)     // Catch:{ Exception -> 0x03f8 }
            int r13 = java.lang.Integer.parseInt(r31)     // Catch:{ Exception -> 0x03f8 }
        L_0x0180:
            r31 = 0
            r0 = r12
            r1 = r31
            r2 = r19
            java.lang.String r12 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x03bd }
        L_0x018b:
            r0 = r35
            java.lang.String r0 = r0.fwdServer     // Catch:{ Exception -> 0x038c }
            r31 = r0
            int r31 = r31.length()     // Catch:{ Exception -> 0x038c }
            if (r31 <= 0) goto L_0x037f
            r0 = r35
            int r0 = r0.fwdPort     // Catch:{ Exception -> 0x038c }
            r31 = r0
            if (r31 <= 0) goto L_0x037f
            java.net.Socket r25 = new java.net.Socket     // Catch:{ Exception -> 0x038c }
            r0 = r35
            java.lang.String r0 = r0.fwdServer     // Catch:{ Exception -> 0x038c }
            r31 = r0
            r0 = r35
            int r0 = r0.fwdPort     // Catch:{ Exception -> 0x038c }
            r32 = r0
            r0 = r25
            r1 = r31
            r2 = r32
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x038c }
            r24 = r25
        L_0x01b8:
            com.kandian.videoplayer.ProxyThread$MyHttpRequest r14 = new com.kandian.videoplayer.ProxyThread$MyHttpRequest     // Catch:{ Exception -> 0x03bd }
            r0 = r14
            r1 = r35
            r0.<init>()     // Catch:{ Exception -> 0x03bd }
            r0 = r20
            r1 = r14
            r1.request = r0     // Catch:{ Exception -> 0x03bd }
            r14.hostName = r12     // Catch:{ Exception -> 0x03bd }
            java.lang.StringBuilder r31 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03bd }
            java.lang.String r32 = java.lang.String.valueOf(r13)     // Catch:{ Exception -> 0x03bd }
            r31.<init>(r32)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r31 = r31.toString()     // Catch:{ Exception -> 0x03bd }
            r0 = r31
            r1 = r14
            r1.hostPort = r0     // Catch:{ Exception -> 0x03bd }
            if (r24 == 0) goto L_0x0236
            r0 = r35
            int r0 = r0.socketTimeout     // Catch:{ Exception -> 0x03bd }
            r31 = r0
            r0 = r24
            r1 = r31
            r0.setSoTimeout(r1)     // Catch:{ Exception -> 0x03bd }
            java.io.BufferedInputStream r26 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x03bd }
            java.io.InputStream r31 = r24.getInputStream()     // Catch:{ Exception -> 0x03bd }
            r0 = r26
            r1 = r31
            r0.<init>(r1)     // Catch:{ Exception -> 0x03bd }
            java.io.BufferedOutputStream r27 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x03bd }
            java.io.OutputStream r31 = r24.getOutputStream()     // Catch:{ Exception -> 0x03bd }
            r0 = r27
            r1 = r31
            r0.<init>(r1)     // Catch:{ Exception -> 0x03bd }
            r31 = 0
            r0 = r27
            r1 = r20
            r2 = r31
            r3 = r21
            r0.write(r1, r2, r3)     // Catch:{ Exception -> 0x03bd }
            r27.flush()     // Catch:{ Exception -> 0x03bd }
            r0 = r35
            int r0 = r0.debugLevel     // Catch:{ Exception -> 0x03bd }
            r31 = r0
            r32 = 1
            r0 = r31
            r1 = r32
            if (r0 <= r1) goto L_0x03e8
            r31 = 1
            r0 = r35
            r1 = r26
            r2 = r31
            byte[] r22 = r0.getHTTPData(r1, r2)     // Catch:{ Exception -> 0x03bd }
            int r23 = java.lang.reflect.Array.getLength(r22)     // Catch:{ Exception -> 0x03bd }
        L_0x0230:
            r26.close()     // Catch:{ Exception -> 0x03bd }
            r27.close()     // Catch:{ Exception -> 0x03bd }
        L_0x0236:
            r0 = r35
            int r0 = r0.debugLevel     // Catch:{ Exception -> 0x03bd }
            r31 = r0
            r32 = 1
            r0 = r31
            r1 = r32
            if (r0 <= r1) goto L_0x0250
            r31 = 0
            r0 = r6
            r1 = r22
            r2 = r31
            r3 = r23
            r0.write(r1, r2, r3)     // Catch:{ Exception -> 0x03bd }
        L_0x0250:
            r0 = r35
            int r0 = r0.debugLevel     // Catch:{ Exception -> 0x03bd }
            r31 = r0
            if (r31 <= 0) goto L_0x02eb
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x03bd }
            r0 = r35
            java.io.PrintStream r0 = r0.debugOut     // Catch:{ Exception -> 0x03bd }
            r31 = r0
            java.lang.StringBuilder r32 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = "Request from "
            r32.<init>(r33)     // Catch:{ Exception -> 0x03bd }
            r0 = r35
            java.net.Socket r0 = r0.pSocket     // Catch:{ Exception -> 0x03bd }
            r33 = r0
            java.net.InetAddress r33 = r33.getInetAddress()     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = r33.getHostAddress()     // Catch:{ Exception -> 0x03bd }
            java.lang.StringBuilder r32 = r32.append(r33)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = " on Port "
            java.lang.StringBuilder r32 = r32.append(r33)     // Catch:{ Exception -> 0x03bd }
            r0 = r35
            java.net.Socket r0 = r0.pSocket     // Catch:{ Exception -> 0x03bd }
            r33 = r0
            int r33 = r33.getLocalPort()     // Catch:{ Exception -> 0x03bd }
            java.lang.StringBuilder r32 = r32.append(r33)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = " to host "
            java.lang.StringBuilder r32 = r32.append(r33)     // Catch:{ Exception -> 0x03bd }
            r0 = r32
            r1 = r12
            java.lang.StringBuilder r32 = r0.append(r1)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = ":"
            java.lang.StringBuilder r32 = r32.append(r33)     // Catch:{ Exception -> 0x03bd }
            r0 = r32
            r1 = r13
            java.lang.StringBuilder r32 = r0.append(r1)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = "\n  ("
            java.lang.StringBuilder r32 = r32.append(r33)     // Catch:{ Exception -> 0x03bd }
            r0 = r32
            r1 = r21
            java.lang.StringBuilder r32 = r0.append(r1)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = " bytes sent, "
            java.lang.StringBuilder r32 = r32.append(r33)     // Catch:{ Exception -> 0x03bd }
            r0 = r32
            r1 = r23
            java.lang.StringBuilder r32 = r0.append(r1)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = " bytes returned, "
            java.lang.StringBuilder r32 = r32.append(r33)     // Catch:{ Exception -> 0x03bd }
            long r33 = r8 - r28
            java.lang.String r33 = java.lang.Long.toString(r33)     // Catch:{ Exception -> 0x03bd }
            java.lang.StringBuilder r32 = r32.append(r33)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = " ms elapsed)"
            java.lang.StringBuilder r32 = r32.append(r33)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r32 = r32.toString()     // Catch:{ Exception -> 0x03bd }
            r31.println(r32)     // Catch:{ Exception -> 0x03bd }
            r0 = r35
            java.io.PrintStream r0 = r0.debugOut     // Catch:{ Exception -> 0x03bd }
            r31 = r0
            r31.flush()     // Catch:{ Exception -> 0x03bd }
        L_0x02eb:
            r0 = r35
            int r0 = r0.debugLevel     // Catch:{ Exception -> 0x03bd }
            r31 = r0
            r32 = 1
            r0 = r31
            r1 = r32
            if (r0 <= r1) goto L_0x0344
            r0 = r35
            java.io.PrintStream r0 = r0.debugOut     // Catch:{ Exception -> 0x03bd }
            r31 = r0
            java.lang.StringBuilder r32 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = "REQUEST:\n"
            r32.<init>(r33)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = new java.lang.String     // Catch:{ Exception -> 0x03bd }
            r0 = r33
            r1 = r20
            r0.<init>(r1)     // Catch:{ Exception -> 0x03bd }
            java.lang.StringBuilder r32 = r32.append(r33)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r32 = r32.toString()     // Catch:{ Exception -> 0x03bd }
            r31.println(r32)     // Catch:{ Exception -> 0x03bd }
            r0 = r35
            java.io.PrintStream r0 = r0.debugOut     // Catch:{ Exception -> 0x03bd }
            r31 = r0
            java.lang.StringBuilder r32 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = "RESPONSE:\n"
            r32.<init>(r33)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r33 = new java.lang.String     // Catch:{ Exception -> 0x03bd }
            r0 = r33
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x03bd }
            java.lang.StringBuilder r32 = r32.append(r33)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r32 = r32.toString()     // Catch:{ Exception -> 0x03bd }
            r31.println(r32)     // Catch:{ Exception -> 0x03bd }
            r0 = r35
            java.io.PrintStream r0 = r0.debugOut     // Catch:{ Exception -> 0x03bd }
            r31 = r0
            r31.flush()     // Catch:{ Exception -> 0x03bd }
        L_0x0344:
            r6.close()     // Catch:{ Exception -> 0x03bd }
            r5.close()     // Catch:{ Exception -> 0x03bd }
            r0 = r35
            java.net.Socket r0 = r0.pSocket     // Catch:{ Exception -> 0x03bd }
            r31 = r0
            r31.close()     // Catch:{ Exception -> 0x03bd }
        L_0x0353:
            return
        L_0x0354:
            java.lang.String r31 = "http:/"
            int r15 = r30.indexOf(r31)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r31 = "/"
            int r32 = r15 + 6
            int r16 = r30.indexOf(r31, r32)     // Catch:{ Exception -> 0x03bd }
            r31 = -1
            r0 = r15
            r1 = r31
            if (r0 == r1) goto L_0x00da
            r31 = -1
            r0 = r16
            r1 = r31
            if (r0 == r1) goto L_0x00da
            int r31 = r15 + 6
            r0 = r30
            r1 = r31
            r2 = r16
            java.lang.String r17 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x03bd }
            goto L_0x00da
        L_0x037f:
            java.net.Socket r25 = new java.net.Socket     // Catch:{ Exception -> 0x038c }
            r0 = r25
            r1 = r12
            r2 = r13
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x038c }
            r24 = r25
            goto L_0x01b8
        L_0x038c:
            r31 = move-exception
            r7 = r31
            java.lang.StringBuilder r31 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03bd }
            java.lang.String r32 = "HTTP/1.0 500\nContent Type: text/plain\n\nError connecting to the server:\n"
            r31.<init>(r32)     // Catch:{ Exception -> 0x03bd }
            r0 = r31
            r1 = r7
            java.lang.StringBuilder r31 = r0.append(r1)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r32 = "\n"
            java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r10 = r31.toString()     // Catch:{ Exception -> 0x03bd }
            byte[] r31 = r10.getBytes()     // Catch:{ Exception -> 0x03bd }
            r32 = 0
            int r33 = r10.length()     // Catch:{ Exception -> 0x03bd }
            r0 = r6
            r1 = r31
            r2 = r32
            r3 = r33
            r0.write(r1, r2, r3)     // Catch:{ Exception -> 0x03bd }
            goto L_0x01b8
        L_0x03bd:
            r31 = move-exception
            r7 = r31
            r0 = r35
            int r0 = r0.debugLevel
            r31 = r0
            if (r31 <= 0) goto L_0x03e3
            r0 = r35
            java.io.PrintStream r0 = r0.debugOut
            r31 = r0
            java.lang.StringBuilder r32 = new java.lang.StringBuilder
            java.lang.String r33 = "Error in ProxyThread: "
            r32.<init>(r33)
            r0 = r32
            r1 = r7
            java.lang.StringBuilder r32 = r0.append(r1)
            java.lang.String r32 = r32.toString()
            r31.println(r32)
        L_0x03e3:
            r7.printStackTrace()
            goto L_0x0353
        L_0x03e8:
            r31 = 1
            r0 = r35
            r1 = r26
            r2 = r6
            r3 = r31
            r4 = r14
            int r23 = r0.streamHTTPData(r1, r2, r3, r4)     // Catch:{ Exception -> 0x03bd }
            goto L_0x0230
        L_0x03f8:
            r31 = move-exception
            goto L_0x0180
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kandian.videoplayer.ProxyThread.run():void");
    }

    private byte[] getHTTPData(InputStream in, boolean waitForDisconnect) {
        return getHTTPData(in, new StringBuffer(""), waitForDisconnect);
    }

    private byte[] getHTTPData(InputStream in, StringBuffer host, boolean waitForDisconnect) {
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        streamHTTPData(in, bs, host, waitForDisconnect, null);
        return bs.toByteArray();
    }

    private int streamHTTPData(InputStream in, OutputStream out, boolean waitForDisconnect, MyHttpRequest request) {
        return streamHTTPData(in, out, new StringBuffer(""), waitForDisconnect, request);
    }

    private int streamHTTPData(InputStream in, OutputStream out, StringBuffer host, boolean waitForDisconnect, MyHttpRequest request) {
        String url;
        StringBuffer stringBuffer = new StringBuffer("");
        int responseCode = HttpStatus.SC_OK;
        int contentLength = 0;
        int byteCount = 0;
        String contentType = "";
        String path = "";
        try {
            String data = readLine(in);
            if (data != null) {
                stringBuffer.append(String.valueOf(data) + "\r\n");
                int pos = data.indexOf(" ");
                if (data.toLowerCase().startsWith("http") && pos >= 0 && data.indexOf(" ", pos + 1) >= 0) {
                    String rcString = data.substring(pos + 1, data.indexOf(" ", pos + 1));
                    try {
                        responseCode = Integer.parseInt(rcString);
                    } catch (Exception e) {
                        if (this.debugLevel > 0) {
                            this.debugOut.println("Error parsing response code " + rcString);
                        }
                    }
                }
                if ((data.toLowerCase().startsWith("get") || data.toLowerCase().startsWith("post")) && pos >= 0 && data.indexOf(" ", pos + 1) >= 0) {
                    path = data.substring(pos + 1, data.indexOf(" ", pos + 1));
                }
            }
            while (true) {
                String data2 = readLine(in);
                if (data2 != null) {
                    if (data2.length() == 0) {
                        break;
                    }
                    int pos2 = data2.toLowerCase().indexOf("host:");
                    if (pos2 >= 0) {
                        host.setLength(0);
                        host.append(data2.substring(pos2 + 5).trim());
                    }
                    int pos3 = data2.toLowerCase().indexOf("content-length:");
                    if (pos3 >= 0) {
                        contentLength = Integer.parseInt(data2.substring(pos3 + 15).trim());
                    }
                    int pos4 = data2.toLowerCase().indexOf("content-type:");
                    if (pos4 >= 0) {
                        contentType = data2.substring(pos4 + 13).trim();
                    }
                    if (request == null) {
                        if (path.toLowerCase().startsWith("http://")) {
                            url = path;
                        } else {
                            url = "http://" + ((Object) host) + path;
                        }
                        String referer = this.proxy.getReferer(url);
                        if (referer != null) {
                            stringBuffer.append("Referer:" + referer);
                        }
                    }
                    if (data2.toLowerCase().indexOf("range:") < 0 && data2.toLowerCase().indexOf("connection:") < 0 && !data2.toLowerCase().startsWith("accept")) {
                        stringBuffer.append(String.valueOf(data2) + "\r\n");
                    }
                } else {
                    break;
                }
            }
            stringBuffer.append("Accept: */*\r\n");
            stringBuffer.append("Accept-Encoding: gzip, deflate\r\n");
            stringBuffer.append("Accept-Language: zh-cn\r\n");
            stringBuffer.append("\r\n");
            out.write(stringBuffer.toString().getBytes(), 0, stringBuffer.length());
            Log.v(TAG, "header: " + ((Object) stringBuffer));
            if (responseCode == 200 || contentLength != 0) {
                if (contentLength > 0) {
                    waitForDisconnect = false;
                }
                if (contentLength > 0 || waitForDisconnect) {
                    try {
                        byte[] buf = new byte[4096];
                        boolean found = false;
                        while (true) {
                            if (byteCount < contentLength || waitForDisconnect) {
                                int bytesIn = in.read(buf);
                                if (bytesIn >= 0) {
                                    out.write(buf, 0, bytesIn);
                                    out.flush();
                                    byteCount += bytesIn;
                                    if (!found && (byteCount > 3000000 || contentType.contains("video") || request.hostName.contains("data.vod.itc.cn"))) {
                                        System.out.println("found request:\n" + new String(request.request) + "\nResponse: " + ((Object) stringBuffer));
                                        System.out.flush();
                                        found = true;
                                    }
                                }
                            }
                        }
                    } catch (Exception e2) {
                        Exception e3 = e2;
                        String errMsg = "Error getting HTTP body at " + 0 + "bytes/" + contentLength + ": " + e3;
                        if (this.debugLevel > 0) {
                            e3.printStackTrace();
                            this.debugOut.println(errMsg);
                            if (request != null) {
                                this.debugOut.println("found request:\n" + new String(request.request) + "\nResponse: " + ((Object) stringBuffer));
                                this.debugOut.flush();
                            } else {
                                this.debugOut.println(stringBuffer);
                            }
                        }
                    }
                }
                try {
                    out.flush();
                } catch (Exception e4) {
                }
                return stringBuffer.length() + byteCount;
            }
            out.flush();
            return stringBuffer.length();
        } catch (Exception e5) {
            Exception e6 = e5;
            if (this.debugLevel > 0) {
                this.debugOut.println("Error getting HTTP data: " + e6);
            }
        }
    }

    private String readLine(InputStream in) {
        int c;
        StringBuffer data = new StringBuffer("");
        try {
            in.mark(1);
            if (in.read() == -1) {
                return null;
            }
            in.reset();
            while (true) {
                c = in.read();
                if (!(c < 0 || c == 0 || c == 10 || c == 13)) {
                    data.append((char) c);
                }
            }
            if (c == 13) {
                in.mark(1);
                if (in.read() != 10) {
                    in.reset();
                }
            }
            return data.toString();
        } catch (Exception e) {
            Exception e2 = e;
            if (this.debugLevel > 0) {
                this.debugOut.println("Error getting header: " + e2);
            }
        }
    }
}
