package com.kandian.other;

import com.kandian.common.Log;
import com.kandian.common.StringUtil;
import com.kandian.ksfamily.KSApp;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

public class OtherDataService {
    private static String TAG = "OtherDataService";
    public static KSApp ksAppInfo = null;

    public static KSApp getKSAppInfo(String dataIF) {
        Log.v(TAG, "GetData begin!! ");
        Log.v(TAG, "dataIF = " + dataIF);
        KSApp ksApp = null;
        try {
            JSONObject jsonObject = new JSONObject(StringUtil.getStringFromURL(dataIF));
            KSApp ksApp2 = new KSApp();
            try {
                ksApp2.setId(jsonObject.getLong("id"));
                ksApp2.setPackagename(jsonObject.getString("packagename"));
                ksApp2.setAppname(jsonObject.getString("appname"));
                ksApp2.setVersioncode(jsonObject.getInt("versioncode"));
                ksApp2.setVersionname(jsonObject.getString("versionname"));
                ksApp2.setApplogo(jsonObject.getString("applogo"));
                ksApp2.setDescription(jsonObject.getString("description"));
                ksApp2.setUpdateinfo(jsonObject.getString("updateinfo"));
                ksApp2.setDownloadurl(jsonObject.getString("downloadurl"));
                ksApp2.setSinaweiboid(jsonObject.getString("sinaweiboid"));
                ksApp2.setSinaweibokeyword(jsonObject.getString("sinaweibokeyword"));
                ksApp2.setFirmware(jsonObject.getString("firmware"));
                ksAppInfo = ksApp2;
                Log.v(TAG, "GetData end!! ");
                return ksApp2;
            } catch (IOException e) {
                ksApp = ksApp2;
                return ksApp;
            } catch (JSONException e2) {
                ksApp = ksApp2;
                return ksApp;
            }
        } catch (IOException e3) {
            return ksApp;
        } catch (JSONException e4) {
            return ksApp;
        }
    }
}
