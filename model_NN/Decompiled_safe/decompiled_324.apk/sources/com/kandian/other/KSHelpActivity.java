package com.kandian.other;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import android.widget.TextView;
import com.kandian.cartoonapp.R;
import com.kandian.common.AsyncImageLoader;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.ksfamily.KSApp;

public class KSHelpActivity extends Activity {
    private final int MSG_LIST = 0;
    private String TAG = "KSHelpActivity";
    private Context _context = null;
    private AsyncImageLoader asyncImageLoader = null;
    /* access modifiers changed from: private */
    public KSApp ksAppInfo = null;
    Handler myViewUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    KSApp currKSApp = (KSApp) msg.obj;
                    if (currKSApp != null) {
                        ImageView imageView = (ImageView) KSHelpActivity.this.findViewById(R.id.ksapp_icon);
                        if (imageView != null) {
                            imageView.setImageResource(KSHelpActivity.this.ksAppInfo.getAppicon());
                        }
                        TextView ksappnameTT = (TextView) KSHelpActivity.this.findViewById(R.id.ksapp_name);
                        if (ksappnameTT != null) {
                            ksappnameTT.setText(currKSApp.getAppname());
                            break;
                        }
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.kshelp_activity);
        this._context = this;
        this.asyncImageLoader = AsyncImageLoader.instance();
        this.ksAppInfo = (KSApp) getIntent().getSerializableExtra("ksAppInfo");
        if (this.ksAppInfo != null) {
            Message m = Message.obtain(this.myViewUpdateHandler);
            m.what = 0;
            m.obj = this.ksAppInfo;
            m.sendToTarget();
        }
    }

    public void onResume() {
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }
}
