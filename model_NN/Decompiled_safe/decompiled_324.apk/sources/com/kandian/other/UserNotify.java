package com.kandian.other;

import com.kandian.common.VideoAsset;
import java.io.Serializable;
import java.util.List;

public class UserNotify implements Serializable {
    private static final long serialVersionUID = 7805890005443375148L;
    private String content;
    private long id;
    private String title;
    private List<VideoAsset> updateAssets;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id2) {
        this.id = id2;
    }

    public List<VideoAsset> getUpdateAssets() {
        return this.updateAssets;
    }

    public void setUpdateAssets(List<VideoAsset> updateAssets2) {
        this.updateAssets = updateAssets2;
    }
}
