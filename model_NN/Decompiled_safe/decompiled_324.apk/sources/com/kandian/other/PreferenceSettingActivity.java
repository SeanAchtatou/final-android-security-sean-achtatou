package com.kandian.other;

import android.content.SharedPreferences;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import com.kandian.cartoonapp.R;
import com.kandian.common.Log;
import com.kandian.common.PreferenceSetting;

public class PreferenceSettingActivity extends PreferenceActivity implements Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener, SharedPreferences.OnSharedPreferenceChangeListener {
    private String TAG = "PreferenceSettingActivity";

    public class StorageInfo {
        private String entry;
        private String entryValue;

        public StorageInfo() {
        }

        public String getEntry() {
            return this.entry;
        }

        public void setEntry(String entry2) {
            this.entry = entry2;
        }

        public String getEntryValue() {
            return this.entryValue;
        }

        public void setEntryValue(String entryValue2) {
            this.entryValue = entryValue2;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x077a A[LOOP:4: B:50:0x0410->B:101:0x077a, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:122:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x02b1  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x045d  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0476  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0490  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x04a9  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x04c3  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x04dd  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0625 A[LOOP:1: B:32:0x02d9->B:88:0x0625, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0647 A[LOOP:2: B:35:0x0327->B:89:0x0647, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0676  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r45) {
        /*
            r44 = this;
            super.onCreate(r45)
            r38 = 2130968577(0x7f040001, float:1.7545812E38)
            r0 = r44
            r1 = r38
            r0.addPreferencesFromResource(r1)
            java.lang.String r38 = "settingGroup"
            r0 = r44
            r1 = r38
            android.preference.Preference r20 = r0.findPreference(r1)
            android.preference.PreferenceGroup r20 = (android.preference.PreferenceGroup) r20
            r38 = 2131099834(0x7f0600ba, float:1.7812032E38)
            r0 = r44
            r1 = r38
            java.lang.String r38 = r0.getString(r1)
            r0 = r44
            r1 = r38
            android.preference.Preference r7 = r0.findPreference(r1)
            android.preference.ListPreference r7 = (android.preference.ListPreference) r7
            java.lang.String r6 = com.kandian.common.PreferenceSetting.getMediaFileDir()
            r0 = r44
            java.lang.String r0 = r0.TAG
            r38 = r0
            java.lang.StringBuilder r39 = new java.lang.StringBuilder
            java.lang.String r40 = "currDownloadDir = "
            r39.<init>(r40)
            r0 = r39
            r1 = r6
            java.lang.StringBuilder r39 = r0.append(r1)
            java.lang.String r39 = r39.toString()
            com.kandian.common.Log.v(r38, r39)
            if (r6 == 0) goto L_0x005b
            java.lang.String r38 = ""
            java.lang.String r39 = r6.trim()
            boolean r38 = r38.equals(r39)
            if (r38 == 0) goto L_0x0068
        L_0x005b:
            r38 = 0
            r0 = r44
            r1 = r38
            com.kandian.common.PreferenceSetting.setMediaFileDir(r0, r1)
            java.lang.String r6 = com.kandian.common.PreferenceSetting.getMediaFileDir()
        L_0x0068:
            r38 = 2131099838(0x7f0600be, float:1.781204E38)
            r0 = r44
            r1 = r38
            java.lang.String r38 = r0.getString(r1)
            r0 = r6
            r1 = r38
            boolean r38 = r0.endsWith(r1)
            if (r38 == 0) goto L_0x00b7
            r38 = 0
            r39 = 2131099838(0x7f0600be, float:1.781204E38)
            r0 = r44
            r1 = r39
            java.lang.String r39 = r0.getString(r1)
            r0 = r6
            r1 = r39
            int r39 = r0.lastIndexOf(r1)
            r0 = r6
            r1 = r38
            r2 = r39
            java.lang.String r6 = r0.substring(r1, r2)
            java.lang.String r38 = "/"
            r0 = r6
            r1 = r38
            boolean r38 = r0.endsWith(r1)
            if (r38 != 0) goto L_0x00b7
            java.lang.StringBuilder r38 = new java.lang.StringBuilder
            java.lang.String r39 = java.lang.String.valueOf(r6)
            r38.<init>(r39)
            java.lang.String r39 = "/"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r6 = r38.toString()
        L_0x00b7:
            r0 = r44
            java.lang.String r0 = r0.TAG
            r38 = r0
            java.lang.StringBuilder r39 = new java.lang.StringBuilder
            java.lang.String r40 = "currDownloadDir = "
            r39.<init>(r40)
            r0 = r39
            r1 = r6
            java.lang.StringBuilder r39 = r0.append(r1)
            java.lang.String r39 = r39.toString()
            com.kandian.common.Log.v(r38, r39)
            java.util.ArrayList r34 = new java.util.ArrayList
            r34.<init>()
            r32 = 0
            r10 = 0
            java.util.HashMap r36 = new java.util.HashMap
            r36.<init>()
            java.lang.String r38 = "mounted"
            java.lang.String r39 = android.os.Environment.getExternalStorageState()
            boolean r38 = r38.equals(r39)
            if (r38 == 0) goto L_0x01ac
            java.io.File r38 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r10 = r38.toString()
            com.kandian.other.PreferenceSettingActivity$StorageInfo r32 = new com.kandian.other.PreferenceSettingActivity$StorageInfo
            r0 = r32
            r1 = r44
            r0.<init>()
            java.lang.StringBuilder r38 = new java.lang.StringBuilder
            java.lang.String r39 = java.lang.String.valueOf(r10)
            r38.<init>(r39)
            java.lang.String r39 = " (可用"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r39 = com.kandian.common.MemoryStatus.getAvailableExternalMemorySizeText(r10)
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r39 = "/总"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r39 = com.kandian.common.MemoryStatus.getTotalExternalMemorySizeText(r10)
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r39 = ")"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r38 = r38.toString()
            r0 = r32
            r1 = r38
            r0.setEntry(r1)
            r0 = r32
            r1 = r10
            r0.setEntryValue(r1)
            r0 = r34
            r1 = r32
            r0.add(r1)
            com.kandian.other.PreferenceSettingActivity$StorageInfo r32 = new com.kandian.other.PreferenceSettingActivity$StorageInfo
            r0 = r32
            r1 = r44
            r0.<init>()
            java.lang.StringBuilder r38 = new java.lang.StringBuilder
            java.lang.String r39 = java.lang.String.valueOf(r10)
            r38.<init>(r39)
            java.lang.String r39 = "/video/"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r39 = " (可用"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r39 = com.kandian.common.MemoryStatus.getAvailableExternalMemorySizeText(r10)
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r39 = "/总"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r39 = com.kandian.common.MemoryStatus.getTotalExternalMemorySizeText(r10)
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r39 = ")"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r38 = r38.toString()
            r0 = r32
            r1 = r38
            r0.setEntry(r1)
            java.lang.StringBuilder r38 = new java.lang.StringBuilder
            java.lang.String r39 = java.lang.String.valueOf(r10)
            r38.<init>(r39)
            java.lang.String r39 = "/video/"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r38 = r38.toString()
            r0 = r32
            r1 = r38
            r0.setEntryValue(r1)
            r0 = r34
            r1 = r32
            r0.add(r1)
            r0 = r36
            r1 = r10
            r2 = r10
            r0.put(r1, r2)
        L_0x01ac:
            r33 = r32
            android.os.StatFs r26 = new android.os.StatFs     // Catch:{ Exception -> 0x07a4 }
            java.lang.String r38 = "java.io.tmpdir"
            java.lang.String r38 = java.lang.System.getProperty(r38)     // Catch:{ Exception -> 0x07a4 }
            r0 = r26
            r1 = r38
            r0.<init>(r1)     // Catch:{ Exception -> 0x07a4 }
            int r38 = r26.getBlockCount()     // Catch:{ Exception -> 0x07a4 }
            r0 = r38
            long r0 = (long) r0     // Catch:{ Exception -> 0x07a4 }
            r4 = r0
            java.io.File r38 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x07a4 }
            java.lang.String r38 = r38.toString()     // Catch:{ Exception -> 0x07a4 }
            java.lang.String r39 = "java.io.tmpdir"
            java.lang.String r39 = java.lang.System.getProperty(r39)     // Catch:{ Exception -> 0x07a4 }
            boolean r38 = r38.endsWith(r39)     // Catch:{ Exception -> 0x07a4 }
            if (r38 != 0) goto L_0x07b0
            r38 = 0
            int r38 = (r4 > r38 ? 1 : (r4 == r38 ? 0 : -1))
            if (r38 <= 0) goto L_0x07b0
            java.lang.String r38 = "java.io.tmpdir"
            java.lang.String r10 = java.lang.System.getProperty(r38)     // Catch:{ Exception -> 0x07a4 }
            com.kandian.other.PreferenceSettingActivity$StorageInfo r32 = new com.kandian.other.PreferenceSettingActivity$StorageInfo     // Catch:{ Exception -> 0x07a4 }
            r0 = r32
            r1 = r44
            r0.<init>()     // Catch:{ Exception -> 0x07a4 }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07a9 }
            java.lang.String r39 = java.lang.String.valueOf(r10)     // Catch:{ Exception -> 0x07a9 }
            r38.<init>(r39)     // Catch:{ Exception -> 0x07a9 }
            java.lang.String r39 = " (可用"
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ Exception -> 0x07a9 }
            java.lang.String r39 = com.kandian.common.MemoryStatus.getAvailableExternalMemorySizeText(r10)     // Catch:{ Exception -> 0x07a9 }
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ Exception -> 0x07a9 }
            java.lang.String r39 = "/总"
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ Exception -> 0x07a9 }
            java.lang.String r39 = com.kandian.common.MemoryStatus.getTotalExternalMemorySizeText(r10)     // Catch:{ Exception -> 0x07a9 }
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ Exception -> 0x07a9 }
            java.lang.String r39 = ")"
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ Exception -> 0x07a9 }
            java.lang.String r38 = r38.toString()     // Catch:{ Exception -> 0x07a9 }
            r0 = r32
            r1 = r38
            r0.setEntry(r1)     // Catch:{ Exception -> 0x07a9 }
            r0 = r32
            r1 = r10
            r0.setEntryValue(r1)     // Catch:{ Exception -> 0x07a9 }
            r0 = r34
            r1 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x07a9 }
            com.kandian.other.PreferenceSettingActivity$StorageInfo r33 = new com.kandian.other.PreferenceSettingActivity$StorageInfo     // Catch:{ Exception -> 0x07a9 }
            r0 = r33
            r1 = r44
            r0.<init>()     // Catch:{ Exception -> 0x07a9 }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07a4 }
            java.lang.String r39 = java.lang.String.valueOf(r10)     // Catch:{ Exception -> 0x07a4 }
            r38.<init>(r39)     // Catch:{ Exception -> 0x07a4 }
            java.lang.String r39 = "/video/"
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ Exception -> 0x07a4 }
            java.lang.String r39 = " (可用"
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ Exception -> 0x07a4 }
            java.lang.String r39 = com.kandian.common.MemoryStatus.getAvailableExternalMemorySizeText(r10)     // Catch:{ Exception -> 0x07a4 }
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ Exception -> 0x07a4 }
            java.lang.String r39 = "/总"
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ Exception -> 0x07a4 }
            java.lang.String r39 = com.kandian.common.MemoryStatus.getTotalExternalMemorySizeText(r10)     // Catch:{ Exception -> 0x07a4 }
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ Exception -> 0x07a4 }
            java.lang.String r39 = ")"
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ Exception -> 0x07a4 }
            java.lang.String r38 = r38.toString()     // Catch:{ Exception -> 0x07a4 }
            r0 = r33
            r1 = r38
            r0.setEntry(r1)     // Catch:{ Exception -> 0x07a4 }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07a4 }
            java.lang.String r39 = java.lang.String.valueOf(r10)     // Catch:{ Exception -> 0x07a4 }
            r38.<init>(r39)     // Catch:{ Exception -> 0x07a4 }
            java.lang.String r39 = "/video/"
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ Exception -> 0x07a4 }
            java.lang.String r38 = r38.toString()     // Catch:{ Exception -> 0x07a4 }
            r0 = r33
            r1 = r38
            r0.setEntryValue(r1)     // Catch:{ Exception -> 0x07a4 }
            r0 = r34
            r1 = r33
            r0.add(r1)     // Catch:{ Exception -> 0x07a4 }
            r0 = r36
            r1 = r10
            r2 = r10
            r0.put(r1, r2)     // Catch:{ Exception -> 0x07a4 }
            r32 = r33
        L_0x02a0:
            java.io.File r17 = new java.io.File
            java.lang.String r38 = "/mnt"
            r0 = r17
            r1 = r38
            r0.<init>(r1)
            boolean r38 = r17.exists()
            if (r38 == 0) goto L_0x02c6
            java.io.File[] r38 = r17.listFiles()
            r0 = r38
            int r0 = r0.length
            r39 = r0
            r40 = 0
            r33 = r32
        L_0x02be:
            r0 = r40
            r1 = r39
            if (r0 < r1) goto L_0x04e4
            r32 = r33
        L_0x02c6:
            int r38 = r34.size()
            r0 = r38
            java.lang.CharSequence[] r0 = new java.lang.CharSequence[r0]
            r8 = r0
            int r38 = r34.size()
            r0 = r38
            java.lang.CharSequence[] r0 = new java.lang.CharSequence[r0]
            r9 = r0
            r14 = 0
        L_0x02d9:
            int r38 = r34.size()
            r0 = r14
            r1 = r38
            if (r0 < r1) goto L_0x0625
            r7.setEntries(r8)
            r7.setEntryValues(r9)
            r7.setSummary(r6)
            r0 = r7
            r1 = r44
            r0.setOnPreferenceChangeListener(r1)
            r38 = 2131099837(0x7f0600bd, float:1.7812038E38)
            r0 = r44
            r1 = r38
            java.lang.String r38 = r0.getString(r1)
            r0 = r44
            r1 = r38
            android.preference.Preference r24 = r0.findPreference(r1)
            android.preference.ListPreference r24 = (android.preference.ListPreference) r24
            android.app.Application r38 = r44.getApplication()
            com.kandian.common.ServiceConfig r38 = com.kandian.common.ServiceConfig.instance(r38)
            java.util.List r25 = r38.getServiceList()
            int r38 = r25.size()
            r0 = r38
            java.lang.CharSequence[] r0 = new java.lang.CharSequence[r0]
            r22 = r0
            int r38 = r25.size()
            r0 = r38
            java.lang.CharSequence[] r0 = new java.lang.CharSequence[r0]
            r23 = r0
            r14 = 0
        L_0x0327:
            int r38 = r25.size()
            r0 = r14
            r1 = r38
            if (r0 < r1) goto L_0x0647
            r0 = r24
            r1 = r22
            r0.setEntries(r1)
            r0 = r24
            r1 = r23
            r0.setEntryValues(r1)
            java.lang.CharSequence r38 = r24.getEntry()
            if (r38 == 0) goto L_0x0350
            java.lang.String r38 = ""
            java.lang.CharSequence r39 = r24.getEntry()
            boolean r38 = r38.equals(r39)
            if (r38 == 0) goto L_0x0669
        L_0x0350:
            r38 = 0
            r38 = r22[r38]
            r0 = r24
            r1 = r38
            r0.setSummary(r1)
            r38 = 0
            r45 = r23[r38]
            java.lang.String r45 = (java.lang.String) r45
            r0 = r24
            r1 = r45
            r0.setValue(r1)
        L_0x0368:
            android.content.SharedPreferences r21 = android.preference.PreferenceManager.getDefaultSharedPreferences(r44)
            r0 = r21
            r1 = r44
            r0.registerOnSharedPreferenceChangeListener(r1)
            r38 = 2131099829(0x7f0600b5, float:1.7812022E38)
            r0 = r44
            r1 = r38
            java.lang.String r38 = r0.getString(r1)
            r0 = r44
            r1 = r38
            android.preference.Preference r37 = r0.findPreference(r1)
            android.preference.CheckBoxPreference r37 = (android.preference.CheckBoxPreference) r37
            android.app.Application r38 = r44.getApplication()
            boolean r38 = com.kandian.common.PreferenceSetting.getDownloadByWifi(r38)
            r37.setChecked(r38)
            r38 = 2131099823(0x7f0600af, float:1.781201E38)
            r0 = r44
            r1 = r38
            java.lang.String r38 = r0.getString(r1)
            r0 = r44
            r1 = r38
            android.preference.Preference r3 = r0.findPreference(r1)
            android.preference.CheckBoxPreference r3 = (android.preference.CheckBoxPreference) r3
            android.app.Application r38 = r44.getApplication()
            boolean r38 = com.kandian.common.PreferenceSetting.getDownloadAutoResume(r38)
            r0 = r3
            r1 = r38
            r0.setChecked(r1)
            r38 = 2131099841(0x7f0600c1, float:1.7812047E38)
            r0 = r44
            r1 = r38
            java.lang.String r38 = r0.getString(r1)
            r0 = r44
            r1 = r38
            android.preference.Preference r16 = r0.findPreference(r1)
            com.kandian.other.ListPreferenceMultiSelect r16 = (com.kandian.other.ListPreferenceMultiSelect) r16
            java.lang.String r38 = r16.getValue()
            r0 = r16
            r1 = r38
            r0.setSummary(r1)
            android.app.Application r38 = r44.getApplication()
            com.kandian.common.SystemConfigs r35 = com.kandian.common.SystemConfigs.instance(r38)
            com.kandian.common.SiteConfigs r29 = r35.getSiteConfigs()
            if (r29 == 0) goto L_0x0427
            java.util.ArrayList r28 = r29.getSiteConfigs()
            if (r28 == 0) goto L_0x0427
            java.util.ArrayList r19 = new java.util.ArrayList
            r19.<init>()
            r18 = 0
            r14 = 0
        L_0x03f2:
            int r38 = r28.size()
            r0 = r14
            r1 = r38
            if (r0 < r1) goto L_0x0676
            int r38 = r19.size()
            r0 = r38
            java.lang.CharSequence[] r0 = new java.lang.CharSequence[r0]
            r30 = r0
            int r38 = r19.size()
            r0 = r38
            java.lang.CharSequence[] r0 = new java.lang.CharSequence[r0]
            r31 = r0
            r14 = 0
        L_0x0410:
            int r38 = r19.size()
            r0 = r14
            r1 = r38
            if (r0 < r1) goto L_0x077a
            r0 = r16
            r1 = r30
            r0.setEntries(r1)
            r0 = r16
            r1 = r31
            r0.setEntryValues(r1)
        L_0x0427:
            r38 = 2131099844(0x7f0600c4, float:1.7812053E38)
            r0 = r44
            r1 = r38
            java.lang.String r38 = r0.getString(r1)
            r0 = r44
            r1 = r38
            android.preference.Preference r13 = r0.findPreference(r1)
            android.preference.CheckBoxPreference r13 = (android.preference.CheckBoxPreference) r13
            android.app.Application r38 = r44.getApplication()
            boolean r38 = com.kandian.common.PreferenceSetting.getVideoplayerFullscreen(r38)
            r0 = r13
            r1 = r38
            r0.setChecked(r1)
            java.lang.String r38 = "false"
            r39 = 2131099821(0x7f0600ad, float:1.7812006E38)
            r0 = r44
            r1 = r39
            java.lang.String r39 = r0.getString(r1)
            boolean r38 = r38.equals(r39)
            if (r38 == 0) goto L_0x0463
            r0 = r20
            r1 = r3
            r0.removePreference(r1)
        L_0x0463:
            java.lang.String r38 = "false"
            r39 = 2131099827(0x7f0600b3, float:1.7812018E38)
            r0 = r44
            r1 = r39
            java.lang.String r39 = r0.getString(r1)
            boolean r38 = r38.equals(r39)
            if (r38 == 0) goto L_0x047d
            r0 = r20
            r1 = r37
            r0.removePreference(r1)
        L_0x047d:
            java.lang.String r38 = "false"
            r39 = 2131099832(0x7f0600b8, float:1.7812028E38)
            r0 = r44
            r1 = r39
            java.lang.String r39 = r0.getString(r1)
            boolean r38 = r38.equals(r39)
            if (r38 == 0) goto L_0x0496
            r0 = r20
            r1 = r7
            r0.removePreference(r1)
        L_0x0496:
            java.lang.String r38 = "false"
            r39 = 2131099836(0x7f0600bc, float:1.7812036E38)
            r0 = r44
            r1 = r39
            java.lang.String r39 = r0.getString(r1)
            boolean r38 = r38.equals(r39)
            if (r38 == 0) goto L_0x04b0
            r0 = r20
            r1 = r24
            r0.removePreference(r1)
        L_0x04b0:
            java.lang.String r38 = "false"
            r39 = 2131099840(0x7f0600c0, float:1.7812045E38)
            r0 = r44
            r1 = r39
            java.lang.String r39 = r0.getString(r1)
            boolean r38 = r38.equals(r39)
            if (r38 == 0) goto L_0x04ca
            r0 = r20
            r1 = r16
            r0.removePreference(r1)
        L_0x04ca:
            java.lang.String r38 = "false"
            r39 = 2131099843(0x7f0600c3, float:1.781205E38)
            r0 = r44
            r1 = r39
            java.lang.String r39 = r0.getString(r1)
            boolean r38 = r38.equals(r39)
            if (r38 == 0) goto L_0x04e3
            r0 = r20
            r1 = r13
            r0.removePreference(r1)
        L_0x04e3:
            return
        L_0x04e4:
            r11 = r38[r40]
            r0 = r44
            java.lang.String r0 = r0.TAG
            r41 = r0
            java.lang.StringBuilder r42 = new java.lang.StringBuilder
            java.lang.String r43 = "f.getAbsolutePath() = "
            r42.<init>(r43)
            java.lang.String r43 = r11.getAbsolutePath()
            java.lang.StringBuilder r42 = r42.append(r43)
            java.lang.String r42 = r42.toString()
            com.kandian.common.Log.v(r41, r42)
            r0 = r44
            java.lang.String r0 = r0.TAG
            r41 = r0
            java.lang.StringBuilder r42 = new java.lang.StringBuilder
            java.lang.String r43 = "f.canWrite() = "
            r42.<init>(r43)
            boolean r43 = r11.canWrite()
            java.lang.StringBuilder r42 = r42.append(r43)
            java.lang.String r42 = r42.toString()
            com.kandian.common.Log.v(r41, r42)
            android.os.StatFs r26 = new android.os.StatFs     // Catch:{ Exception -> 0x079c }
            java.lang.String r41 = r11.getAbsolutePath()     // Catch:{ Exception -> 0x079c }
            r0 = r26
            r1 = r41
            r0.<init>(r1)     // Catch:{ Exception -> 0x079c }
            r0 = r44
            java.lang.String r0 = r0.TAG     // Catch:{ Exception -> 0x079c }
            r41 = r0
            java.lang.StringBuilder r42 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x079c }
            java.lang.String r43 = " sf.getBlockCount() = "
            r42.<init>(r43)     // Catch:{ Exception -> 0x079c }
            int r43 = r26.getBlockCount()     // Catch:{ Exception -> 0x079c }
            java.lang.StringBuilder r42 = r42.append(r43)     // Catch:{ Exception -> 0x079c }
            java.lang.String r42 = r42.toString()     // Catch:{ Exception -> 0x079c }
            com.kandian.common.Log.v(r41, r42)     // Catch:{ Exception -> 0x079c }
            int r41 = r26.getBlockCount()     // Catch:{ Exception -> 0x079c }
            if (r41 <= 0) goto L_0x07ac
            boolean r41 = r11.canWrite()     // Catch:{ Exception -> 0x079c }
            if (r41 == 0) goto L_0x07ac
            boolean r41 = r11.isDirectory()     // Catch:{ Exception -> 0x079c }
            if (r41 == 0) goto L_0x07ac
            java.lang.String r41 = r11.getAbsolutePath()     // Catch:{ Exception -> 0x079c }
            r0 = r36
            r1 = r41
            boolean r41 = r0.containsKey(r1)     // Catch:{ Exception -> 0x079c }
            if (r41 != 0) goto L_0x07ac
            java.lang.String r10 = r11.getAbsolutePath()     // Catch:{ Exception -> 0x079c }
            com.kandian.other.PreferenceSettingActivity$StorageInfo r32 = new com.kandian.other.PreferenceSettingActivity$StorageInfo     // Catch:{ Exception -> 0x079c }
            r0 = r32
            r1 = r44
            r0.<init>()     // Catch:{ Exception -> 0x079c }
            java.lang.StringBuilder r41 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07a1 }
            java.lang.String r42 = java.lang.String.valueOf(r10)     // Catch:{ Exception -> 0x07a1 }
            r41.<init>(r42)     // Catch:{ Exception -> 0x07a1 }
            java.lang.String r42 = " (可用"
            java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ Exception -> 0x07a1 }
            java.lang.String r42 = com.kandian.common.MemoryStatus.getAvailableExternalMemorySizeText(r10)     // Catch:{ Exception -> 0x07a1 }
            java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ Exception -> 0x07a1 }
            java.lang.String r42 = "/总"
            java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ Exception -> 0x07a1 }
            java.lang.String r42 = com.kandian.common.MemoryStatus.getTotalExternalMemorySizeText(r10)     // Catch:{ Exception -> 0x07a1 }
            java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ Exception -> 0x07a1 }
            java.lang.String r42 = ")"
            java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ Exception -> 0x07a1 }
            java.lang.String r41 = r41.toString()     // Catch:{ Exception -> 0x07a1 }
            r0 = r32
            r1 = r41
            r0.setEntry(r1)     // Catch:{ Exception -> 0x07a1 }
            r0 = r32
            r1 = r10
            r0.setEntryValue(r1)     // Catch:{ Exception -> 0x07a1 }
            r0 = r34
            r1 = r32
            r0.add(r1)     // Catch:{ Exception -> 0x07a1 }
            com.kandian.other.PreferenceSettingActivity$StorageInfo r33 = new com.kandian.other.PreferenceSettingActivity$StorageInfo     // Catch:{ Exception -> 0x07a1 }
            r0 = r33
            r1 = r44
            r0.<init>()     // Catch:{ Exception -> 0x07a1 }
            java.lang.StringBuilder r41 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x079c }
            java.lang.String r42 = java.lang.String.valueOf(r10)     // Catch:{ Exception -> 0x079c }
            r41.<init>(r42)     // Catch:{ Exception -> 0x079c }
            java.lang.String r42 = "/video/"
            java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ Exception -> 0x079c }
            java.lang.String r42 = " (可用"
            java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ Exception -> 0x079c }
            java.lang.String r42 = com.kandian.common.MemoryStatus.getAvailableExternalMemorySizeText(r10)     // Catch:{ Exception -> 0x079c }
            java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ Exception -> 0x079c }
            java.lang.String r42 = "/总"
            java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ Exception -> 0x079c }
            java.lang.String r42 = com.kandian.common.MemoryStatus.getTotalExternalMemorySizeText(r10)     // Catch:{ Exception -> 0x079c }
            java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ Exception -> 0x079c }
            java.lang.String r42 = ")"
            java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ Exception -> 0x079c }
            java.lang.String r41 = r41.toString()     // Catch:{ Exception -> 0x079c }
            r0 = r33
            r1 = r41
            r0.setEntry(r1)     // Catch:{ Exception -> 0x079c }
            java.lang.StringBuilder r41 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x079c }
            java.lang.String r42 = java.lang.String.valueOf(r10)     // Catch:{ Exception -> 0x079c }
            r41.<init>(r42)     // Catch:{ Exception -> 0x079c }
            java.lang.String r42 = "/video/"
            java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ Exception -> 0x079c }
            java.lang.String r41 = r41.toString()     // Catch:{ Exception -> 0x079c }
            r0 = r33
            r1 = r41
            r0.setEntryValue(r1)     // Catch:{ Exception -> 0x079c }
            r0 = r34
            r1 = r33
            r0.add(r1)     // Catch:{ Exception -> 0x079c }
            r32 = r33
        L_0x061f:
            int r40 = r40 + 1
            r33 = r32
            goto L_0x02be
        L_0x0625:
            r0 = r34
            r1 = r14
            java.lang.Object r45 = r0.get(r1)
            com.kandian.other.PreferenceSettingActivity$StorageInfo r45 = (com.kandian.other.PreferenceSettingActivity.StorageInfo) r45
            java.lang.String r38 = r45.getEntry()
            r8[r14] = r38
            r0 = r34
            r1 = r14
            java.lang.Object r45 = r0.get(r1)
            com.kandian.other.PreferenceSettingActivity$StorageInfo r45 = (com.kandian.other.PreferenceSettingActivity.StorageInfo) r45
            java.lang.String r38 = r45.getEntryValue()
            r9[r14] = r38
            int r14 = r14 + 1
            goto L_0x02d9
        L_0x0647:
            r0 = r25
            r1 = r14
            java.lang.Object r45 = r0.get(r1)
            com.kandian.common.ServiceConfig$ServiceEntrance r45 = (com.kandian.common.ServiceConfig.ServiceEntrance) r45
            java.lang.String r38 = r45.getName()
            r22[r14] = r38
            r0 = r25
            r1 = r14
            java.lang.Object r45 = r0.get(r1)
            com.kandian.common.ServiceConfig$ServiceEntrance r45 = (com.kandian.common.ServiceConfig.ServiceEntrance) r45
            java.lang.String r38 = r45.getEntrance()
            r23[r14] = r38
            int r14 = r14 + 1
            goto L_0x0327
        L_0x0669:
            java.lang.CharSequence r38 = r24.getEntry()
            r0 = r24
            r1 = r38
            r0.setSummary(r1)
            goto L_0x0368
        L_0x0676:
            r0 = r28
            r1 = r14
            java.lang.Object r27 = r0.get(r1)
            com.kandian.common.SiteConfigs$Site r27 = (com.kandian.common.SiteConfigs.Site) r27
            java.lang.String r38 = r27.getFileType()
            if (r38 == 0) goto L_0x0713
            java.lang.String r38 = r27.getFileType()
            java.lang.String r39 = ","
            int r38 = r38.indexOf(r39)
            r39 = -1
            r0 = r38
            r1 = r39
            if (r0 <= r1) goto L_0x0713
            java.lang.String r38 = r27.getFileType()
            java.lang.String r39 = ","
            java.lang.String[] r12 = r38.split(r39)
            r15 = 0
        L_0x06a2:
            r0 = r12
            int r0 = r0.length
            r38 = r0
            r0 = r15
            r1 = r38
            if (r0 < r1) goto L_0x06af
        L_0x06ab:
            int r14 = r14 + 1
            goto L_0x03f2
        L_0x06af:
            com.kandian.common.SiteConfigs$Site r18 = new com.kandian.common.SiteConfigs$Site
            r29.getClass()
            r0 = r18
            r1 = r29
            r0.<init>()
            java.lang.StringBuilder r38 = new java.lang.StringBuilder
            java.lang.String r39 = r27.getCode()
            java.lang.String r39 = java.lang.String.valueOf(r39)
            r38.<init>(r39)
            java.lang.String r39 = "_"
            java.lang.StringBuilder r38 = r38.append(r39)
            r39 = r12[r15]
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r38 = r38.toString()
            r0 = r18
            r1 = r38
            r0.setCode(r1)
            java.lang.StringBuilder r38 = new java.lang.StringBuilder
            java.lang.String r39 = r27.getName()
            java.lang.String r39 = java.lang.String.valueOf(r39)
            r38.<init>(r39)
            java.lang.String r39 = "["
            java.lang.StringBuilder r38 = r38.append(r39)
            r39 = r12[r15]
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r39 = "]"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r38 = r38.toString()
            r0 = r18
            r1 = r38
            r0.setName(r1)
            r0 = r19
            r1 = r18
            r0.add(r1)
            int r15 = r15 + 1
            goto L_0x06a2
        L_0x0713:
            com.kandian.common.SiteConfigs$Site r18 = new com.kandian.common.SiteConfigs$Site
            r29.getClass()
            r0 = r18
            r1 = r29
            r0.<init>()
            java.lang.StringBuilder r38 = new java.lang.StringBuilder
            java.lang.String r39 = r27.getCode()
            java.lang.String r39 = java.lang.String.valueOf(r39)
            r38.<init>(r39)
            java.lang.String r39 = "_"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r39 = r27.getFileType()
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r38 = r38.toString()
            r0 = r18
            r1 = r38
            r0.setCode(r1)
            java.lang.StringBuilder r38 = new java.lang.StringBuilder
            java.lang.String r39 = r27.getName()
            java.lang.String r39 = java.lang.String.valueOf(r39)
            r38.<init>(r39)
            java.lang.String r39 = "["
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r39 = r27.getFileType()
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r39 = "]"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r38 = r38.toString()
            r0 = r18
            r1 = r38
            r0.setName(r1)
            r0 = r19
            r1 = r18
            r0.add(r1)
            goto L_0x06ab
        L_0x077a:
            r0 = r19
            r1 = r14
            java.lang.Object r45 = r0.get(r1)
            com.kandian.common.SiteConfigs$Site r45 = (com.kandian.common.SiteConfigs.Site) r45
            java.lang.String r38 = r45.getName()
            r30[r14] = r38
            r0 = r19
            r1 = r14
            java.lang.Object r45 = r0.get(r1)
            com.kandian.common.SiteConfigs$Site r45 = (com.kandian.common.SiteConfigs.Site) r45
            java.lang.String r38 = r45.getCode()
            r31[r14] = r38
            int r14 = r14 + 1
            goto L_0x0410
        L_0x079c:
            r41 = move-exception
            r32 = r33
            goto L_0x061f
        L_0x07a1:
            r41 = move-exception
            goto L_0x061f
        L_0x07a4:
            r38 = move-exception
            r32 = r33
            goto L_0x02a0
        L_0x07a9:
            r38 = move-exception
            goto L_0x02a0
        L_0x07ac:
            r32 = r33
            goto L_0x061f
        L_0x07b0:
            r32 = r33
            goto L_0x02a0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kandian.other.PreferenceSettingActivity.onCreate(android.os.Bundle):void");
    }

    public boolean onPreferenceClick(Preference preference) {
        return false;
    }

    public boolean onPreferenceChange(Preference preference, Object newValue) {
        Log.v(this.TAG, "preference is changed");
        Log.v(this.TAG, preference.getKey());
        if (preference.getKey().equals(getString(R.string.setting_download_autoResume_key))) {
            Log.v(this.TAG, "setting_download_autoResume_key preference is changed");
        } else if (preference.getKey().equals(getString(R.string.setting_download_wifi_key))) {
            Log.v(this.TAG, "setting_download_wifi_key preference is changed");
        } else if (preference.getKey().equals(getString(R.string.setting_download_dir_key))) {
            ListPreference downloadDir_LP = (ListPreference) findPreference(getString(R.string.setting_download_dir_key));
            downloadDir_LP.setSummary((String) newValue);
            downloadDir_LP.setValue((String) newValue);
            PreferenceSetting.setMediaFileDir(this, (String) newValue);
            Log.v(this.TAG, "setting_download_dir_key preference is changed = " + newValue);
        } else if (!preference.getKey().equals(getString(R.string.setting_videoplayer_fullscreen_key))) {
            return false;
        } else {
            Log.v(this.TAG, "--------------newValue = " + newValue);
            SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.video_progress_settings), 0).edit();
            editor.putBoolean(getString(R.string.video_fullscreen_attr), false);
            editor.commit();
        }
        return true;
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.setting_service_entrance_key))) {
            ListPreference serviceEntrance_LP = (ListPreference) findPreference(getString(R.string.setting_service_entrance_key));
            serviceEntrance_LP.setSummary(serviceEntrance_LP.getEntry());
        } else if (key.equals(getString(R.string.setting_systemconfig_filter_key))) {
            Log.v(this.TAG, "setting_systemconfig_filter_key preference is changed");
            ListPreferenceMultiSelect listPreferenceMultiSelect = (ListPreferenceMultiSelect) findPreference(getString(R.string.setting_systemconfig_filter_key));
            listPreferenceMultiSelect.setSummary(listPreferenceMultiSelect.getValue());
        } else if (key.equals(getString(R.string.setting_videoplayer_fullscreen_key))) {
            boolean fullscreen = sharedPreferences.getBoolean(getString(R.string.setting_videoplayer_fullscreen_key), false);
            Log.v(this.TAG, "---newValue = " + fullscreen);
            SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.video_progress_settings), 0).edit();
            editor.putBoolean(getString(R.string.video_fullscreen_attr), fullscreen);
            editor.commit();
        }
    }
}
