package com.kandian.other;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import com.kandian.common.AppActiveUtil;
import com.kandian.common.AsyncImageLoader;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.StringUtil;
import com.kandian.ksfamily.KSApp;
import org.json.JSONException;
import org.json.JSONObject;

public class KSAboutActivity extends Activity {
    private final int MSG_LIST = 0;
    private final int MSG_NETWORK_PROBLEM = 1;
    private final int MSG_PARTNER_FAIL = 3;
    private final int MSG_PARTNER_SUCC = 2;
    private String TAG = "KSAboutActivity";
    /* access modifiers changed from: private */
    public Context _context = null;
    private AsyncImageLoader asyncImageLoader = null;
    View.OnClickListener backListener = new View.OnClickListener() {
        public void onClick(View v) {
            KSAboutActivity.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public KSApp ksAppInfo = null;
    Handler myViewUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    KSApp currKSApp = (KSApp) msg.obj;
                    if (currKSApp != null) {
                        ImageView imageView = (ImageView) KSAboutActivity.this.findViewById(R.id.ksapp_icon);
                        if (imageView != null) {
                            imageView.setImageResource(KSAboutActivity.this.ksAppInfo.getAppicon());
                        }
                        TextView ksappnameTT = (TextView) KSAboutActivity.this.findViewById(R.id.ksapp_name);
                        if (ksappnameTT != null) {
                            ksappnameTT.setText(currKSApp.getAppname());
                        }
                        TextView ksappversionnameTT = (TextView) KSAboutActivity.this.findViewById(R.id.ksapp_versionname);
                        if (ksappversionnameTT != null) {
                            ksappversionnameTT.setText(StringUtil.replace(StringUtil.replace(KSAboutActivity.this.getString(R.string.versionname_text), "{versionname}", currKSApp.getVersionname()), "{versioncode}", String.valueOf(currKSApp.getVersioncode())));
                        }
                        TextView ksappdescriptionTT = (TextView) KSAboutActivity.this.findViewById(R.id.ksapp_description);
                        if (ksappdescriptionTT != null) {
                            ksappdescriptionTT.setText(currKSApp.getDescription());
                        }
                        ((LinearLayout) KSAboutActivity.this.findViewById(R.id.loadProgress)).setVisibility(8);
                        break;
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.ksabout_activity);
        this._context = this;
        this.asyncImageLoader = AsyncImageLoader.instance();
        this.ksAppInfo = (KSApp) getIntent().getSerializableExtra("ksAppInfo");
        if (this.ksAppInfo != null) {
            Message m = Message.obtain(this.myViewUpdateHandler);
            m.what = 0;
            m.obj = this.ksAppInfo;
            m.sendToTarget();
        }
        LinearLayout ksapppartnerLL = (LinearLayout) findViewById(R.id.ksapp_partner_layout);
        JSONObject activeInfo = AppActiveUtil.getActiveInfo(this._context);
        int status = 0;
        String localInfoPartner = "";
        if (activeInfo != null) {
            try {
                status = activeInfo.getInt("status");
                localInfoPartner = activeInfo.getString("partner");
            } catch (JSONException e) {
                Log.v(this.TAG, "JSONException = " + e.getMessage());
            }
        }
        if (status == 1 || (this.ksAppInfo.getPartner() != null && this.ksAppInfo.getPartner().trim().length() > 0)) {
            Log.v(this.TAG, "client is actived!!!");
            ksapppartnerLL.setVisibility(8);
            return;
        }
        final EditText partnerET = (EditText) findViewById(R.id.ksapp_partner);
        if (partnerET != null) {
            if (this.ksAppInfo.getPartner() == null || this.ksAppInfo.getPartner().trim().length() <= 0) {
                partnerET.setText(localInfoPartner);
            } else {
                partnerET.setText(this.ksAppInfo.getPartner());
                lockEditText(partnerET, true);
            }
        }
        final Button activeBTN = (Button) findViewById(R.id.ksapp_partner_btn);
        activeBTN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (partnerET == null || partnerET.getText().toString().trim().length() <= 0) {
                    Toast.makeText(KSAboutActivity.this, KSAboutActivity.this.getString(R.string.ksapp_partner_null), 0).show();
                    return;
                }
                ((ProgressBar) KSAboutActivity.this.findViewById(R.id.partnerCommitProgress)).setVisibility(0);
                activeBTN.setEnabled(false);
                AppActiveUtil.updatePartner(KSAboutActivity.this._context, partnerET.getText().toString().trim());
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    private void lockEditText(EditText editText, boolean value) {
        if (value) {
            editText.setFilters(new InputFilter[]{new InputFilter() {
                public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                    if (source.length() < 1) {
                        return dest.subSequence(dstart, dend);
                    }
                    return "";
                }
            }});
            return;
        }
        editText.setFilters(new InputFilter[]{new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                return null;
            }
        }});
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.ksfamilymenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_back:
                this.backListener.onClick(getCurrentFocus());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }
}
