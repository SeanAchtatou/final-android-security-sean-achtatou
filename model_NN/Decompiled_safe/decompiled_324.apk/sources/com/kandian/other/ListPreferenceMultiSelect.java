package com.kandian.other;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.preference.ListPreference;
import android.util.AttributeSet;

public class ListPreferenceMultiSelect extends ListPreference {
    private static final String SEPARATOR = ",";
    /* access modifiers changed from: private */
    public boolean[] mClickedDialogEntryIndices;

    public ListPreferenceMultiSelect(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mClickedDialogEntryIndices = new boolean[getEntries().length];
    }

    public void setEntries(CharSequence[] entries) {
        super.setEntries(entries);
        this.mClickedDialogEntryIndices = new boolean[entries.length];
    }

    public ListPreferenceMultiSelect(Context context) {
        this(context, null);
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        CharSequence[] entries = getEntries();
        CharSequence[] entryValues = getEntryValues();
        if (entries == null || entryValues == null || entries.length != entryValues.length) {
            throw new IllegalStateException("ListPreference requires an entries array and an entryValues array which are both the same length");
        }
        restoreCheckedEntries();
        builder.setMultiChoiceItems(entries, this.mClickedDialogEntryIndices, new DialogInterface.OnMultiChoiceClickListener() {
            public void onClick(DialogInterface dialog, int which, boolean val) {
                ListPreferenceMultiSelect.this.mClickedDialogEntryIndices[which] = val;
            }
        });
    }

    public static String[] parseStoredValue(CharSequence val) {
        if ("".equals(val)) {
            return null;
        }
        return ((String) val).split(SEPARATOR);
    }

    private void restoreCheckedEntries() {
        CharSequence[] entryValues = getEntryValues();
        String[] vals = parseStoredValue(getValue());
        if (vals != null) {
            for (String trim : vals) {
                String val = trim.trim();
                int i = 0;
                while (true) {
                    if (i >= entryValues.length) {
                        break;
                    } else if (entryValues[i].equals(val)) {
                        this.mClickedDialogEntryIndices[i] = true;
                        break;
                    } else {
                        i++;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDialogClosed(boolean positiveResult) {
        CharSequence[] entryValues = getEntryValues();
        if (positiveResult && entryValues != null) {
            StringBuffer value = new StringBuffer();
            for (int i = 0; i < entryValues.length; i++) {
                if (this.mClickedDialogEntryIndices[i]) {
                    value.append(entryValues[i]).append(SEPARATOR);
                }
            }
            if (callChangeListener(value)) {
                String val = value.toString();
                if (val.length() > 0) {
                    val = val.substring(0, val.length() - SEPARATOR.length());
                }
                setValue(val);
            }
        }
    }
}
