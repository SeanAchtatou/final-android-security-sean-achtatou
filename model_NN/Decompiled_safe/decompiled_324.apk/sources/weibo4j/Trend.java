package weibo4j;

import java.io.Serializable;
import weibo4j.org.json.JSONException;
import weibo4j.org.json.JSONObject;

public class Trend implements Serializable {
    private static final long serialVersionUID = 1925956704460743946L;
    private String name;
    private String query = null;
    private String url = null;

    public Trend(JSONObject json) throws JSONException {
        this.name = json.getString("name");
        if (!json.isNull("url")) {
            this.url = json.getString("url");
        }
        if (!json.isNull("query")) {
            this.query = json.getString("query");
        }
    }

    public String getName() {
        return this.name;
    }

    public String getUrl() {
        return this.url;
    }

    public String getQuery() {
        return this.query;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Trend)) {
            return false;
        }
        Trend trend = (Trend) o;
        if (!this.name.equals(trend.name)) {
            return false;
        }
        if (this.query == null ? trend.query != null : !this.query.equals(trend.query)) {
            return false;
        }
        return this.url == null ? trend.url == null : this.url.equals(trend.url);
    }

    public int hashCode() {
        int i;
        int i2;
        int hashCode = this.name.hashCode() * 31;
        if (this.url != null) {
            i = this.url.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 31;
        if (this.query != null) {
            i2 = this.query.hashCode();
        } else {
            i2 = 0;
        }
        return i3 + i2;
    }

    public String toString() {
        return "Trend{name='" + this.name + '\'' + ", url='" + this.url + '\'' + ", query='" + this.query + '\'' + '}';
    }
}
