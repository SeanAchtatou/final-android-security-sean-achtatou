package weibo4j.http;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.httpclient.cookie.CookieSpec;
import weibo4j.Configuration;
import weibo4j.Weibo;

public class OAuth implements Serializable {
    private static final boolean DEBUG = Configuration.getDebug();
    private static final String HMAC_SHA1 = "HmacSHA1";
    private static final PostParameter OAUTH_SIGNATURE_METHOD = new PostParameter(qq.api.OAuth.OAuthSignatureMethodKey, qq.api.OAuth.HMACSHA1SignatureType_TEXT);
    private static Random RAND = new Random();
    static final long serialVersionUID = -4368426677157998618L;
    private String consumerKey = Weibo.CONSUMER_KEY;
    private String consumerSecret = Weibo.CONSUMER_SECRET;

    public OAuth() {
    }

    public OAuth(String consumerKey2, String consumerSecret2) {
        setConsumerKey(consumerKey2);
        setConsumerSecret(consumerSecret2);
    }

    public OAuthParam generateXauthorizationHeader(String method, String url, PostParameter[] params) {
        long timestamp = System.currentTimeMillis() / 1000;
        return generateXauthorizationHeader(method, url, params, String.valueOf(timestamp + ((long) RAND.nextInt())), String.valueOf(timestamp));
    }

    private OAuthParam generateXauthorizationHeader(String method, String url, PostParameter[] params, String nonce, String timestamp) {
        if (params == null) {
            params = new PostParameter[0];
        }
        List<PostParameter> oauthHeaderParams = new ArrayList<>(5);
        oauthHeaderParams.add(new PostParameter("x_auth_mode", "client_auth"));
        oauthHeaderParams.add(new PostParameter(qq.api.OAuth.OAuthConsumerKeyKey, this.consumerKey));
        oauthHeaderParams.add(OAUTH_SIGNATURE_METHOD);
        oauthHeaderParams.add(new PostParameter(qq.api.OAuth.OAuthTimestampKey, timestamp));
        oauthHeaderParams.add(new PostParameter(qq.api.OAuth.OAuthNonceKey, nonce));
        oauthHeaderParams.add(new PostParameter(qq.api.OAuth.OAuthVersionKey, qq.api.OAuth.OAuthVersion));
        List<PostParameter> signatureBaseParams = new ArrayList<>(oauthHeaderParams.size() + params.length);
        signatureBaseParams.addAll(oauthHeaderParams);
        signatureBaseParams.addAll(toParamList(params));
        parseGetParameters(url, signatureBaseParams);
        StringBuffer base = new StringBuffer(method).append("&").append(encode(constructRequestURL(url))).append("&");
        base.append(encode(normalizeRequestParameters(signatureBaseParams)));
        String oauthBaseString = base.toString();
        String signature = generateSignature(oauthBaseString);
        OAuthParam op = new OAuthParam();
        op.setNonce(nonce);
        op.setOauthBaseString(oauthBaseString);
        op.setSignature(signature);
        op.setTimestamp(timestamp);
        return op;
    }

    /* access modifiers changed from: package-private */
    public String generateAuthorizationHeader(String method, String url, PostParameter[] params, String nonce, String timestamp, OAuthToken otoken) {
        if (params == null) {
            params = new PostParameter[0];
        }
        List<PostParameter> oauthHeaderParams = new ArrayList<>(5);
        oauthHeaderParams.add(new PostParameter(qq.api.OAuth.OAuthConsumerKeyKey, this.consumerKey));
        oauthHeaderParams.add(OAUTH_SIGNATURE_METHOD);
        oauthHeaderParams.add(new PostParameter(qq.api.OAuth.OAuthTimestampKey, timestamp));
        oauthHeaderParams.add(new PostParameter(qq.api.OAuth.OAuthNonceKey, nonce));
        oauthHeaderParams.add(new PostParameter(qq.api.OAuth.OAuthVersionKey, qq.api.OAuth.OAuthVersion));
        if (otoken != null) {
            oauthHeaderParams.add(new PostParameter(qq.api.OAuth.OAuthTokenKey, otoken.getToken()));
        }
        List<PostParameter> signatureBaseParams = new ArrayList<>(oauthHeaderParams.size() + params.length);
        signatureBaseParams.addAll(oauthHeaderParams);
        signatureBaseParams.addAll(toParamList(params));
        parseGetParameters(url, signatureBaseParams);
        StringBuffer base = new StringBuffer(method).append("&").append(encode(constructRequestURL(url))).append("&");
        base.append(encode(normalizeRequestParameters(signatureBaseParams)));
        String oauthBaseString = base.toString();
        log("OAuth base string:", oauthBaseString);
        String signature = generateSignature(oauthBaseString, otoken);
        log("OAuth signature:", signature);
        oauthHeaderParams.add(new PostParameter(qq.api.OAuth.OAuthSignatureKey, signature));
        return "OAuth " + encodeParameters(oauthHeaderParams, ",", true);
    }

    private void parseGetParameters(String url, List<PostParameter> signatureBaseParams) {
        int queryStart = url.indexOf("?");
        if (-1 != queryStart) {
            try {
                for (String query : url.substring(queryStart + 1).split("&")) {
                    String[] split = query.split("=");
                    if (split.length == 2) {
                        signatureBaseParams.add(new PostParameter(URLDecoder.decode(split[0], StringEncodings.UTF8), URLDecoder.decode(split[1], StringEncodings.UTF8)));
                    } else {
                        signatureBaseParams.add(new PostParameter(URLDecoder.decode(split[0], StringEncodings.UTF8), ""));
                    }
                }
            } catch (UnsupportedEncodingException e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String generateAuthorizationHeader(String method, String url, PostParameter[] params, OAuthToken token) {
        long timestamp = System.currentTimeMillis() / 1000;
        return generateAuthorizationHeader(method, url, params, String.valueOf(timestamp + ((long) RAND.nextInt())), String.valueOf(timestamp), token);
    }

    /* access modifiers changed from: package-private */
    public String generateSignature(String data, OAuthToken token) {
        SecretKeySpec spec;
        byte[] byteHMAC = null;
        try {
            Mac mac = Mac.getInstance("HmacSHA1");
            if (token == null) {
                spec = new SecretKeySpec((encode(this.consumerSecret) + "&").getBytes(), "HmacSHA1");
            } else {
                if (token.getSecretKeySpec() == null) {
                    token.setSecretKeySpec(new SecretKeySpec((encode(this.consumerSecret) + "&" + encode(token.getTokenSecret())).getBytes(), "HmacSHA1"));
                }
                spec = token.getSecretKeySpec();
            }
            mac.init(spec);
            byteHMAC = mac.doFinal(data.getBytes());
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e2) {
        }
        return new BASE64Encoder().encode(byteHMAC);
    }

    /* access modifiers changed from: package-private */
    public String generateSignature(String data) {
        return generateSignature(data, null);
    }

    public static String normalizeRequestParameters(PostParameter[] params) {
        return normalizeRequestParameters(toParamList(params));
    }

    public static String normalizeRequestParameters(List<PostParameter> params) {
        Collections.sort(params);
        return encodeParameters(params);
    }

    public static String normalizeAuthorizationHeaders(List<PostParameter> params) {
        Collections.sort(params);
        return encodeParameters(params);
    }

    public static List<PostParameter> toParamList(PostParameter[] params) {
        List<PostParameter> paramList = new ArrayList<>(params.length);
        paramList.addAll(Arrays.asList(params));
        return paramList;
    }

    public static String encodeParameters(List<PostParameter> postParams) {
        return encodeParameters(postParams, "&", DEBUG);
    }

    public static String encodeParameters(List<PostParameter> postParams, String splitter, boolean quot) {
        StringBuffer buf = new StringBuffer();
        for (PostParameter param : postParams) {
            if (buf.length() != 0) {
                if (quot) {
                    buf.append("\"");
                }
                buf.append(splitter);
            }
            buf.append(encode(param.name)).append("=");
            if (quot) {
                buf.append("\"");
            }
            buf.append(encode(param.value));
        }
        if (buf.length() != 0 && quot) {
            buf.append("\"");
        }
        return buf.toString();
    }

    public static String encode(String value) {
        String encoded = null;
        try {
            encoded = URLEncoder.encode(value, StringEncodings.UTF8);
        } catch (UnsupportedEncodingException e) {
        }
        StringBuffer buf = new StringBuffer(encoded.length());
        int i = 0;
        while (i < encoded.length()) {
            char focus = encoded.charAt(i);
            if (focus == '*') {
                buf.append("%2A");
            } else if (focus == '+') {
                buf.append("%20");
            } else if (focus == '%' && i + 1 < encoded.length() && encoded.charAt(i + 1) == '7' && encoded.charAt(i + 2) == 'E') {
                buf.append('~');
                i += 2;
            } else {
                buf.append(focus);
            }
            i++;
        }
        return buf.toString();
    }

    public static String constructRequestURL(String url) {
        int index = url.indexOf("?");
        if (-1 != index) {
            url = url.substring(0, index);
        }
        int slashIndex = url.indexOf(CookieSpec.PATH_DELIM, 8);
        String baseURL = url.substring(0, slashIndex).toLowerCase();
        int colonIndex = baseURL.indexOf(":", 8);
        if (-1 != colonIndex) {
            if (baseURL.startsWith("http://") && baseURL.endsWith(":80")) {
                baseURL = baseURL.substring(0, colonIndex);
            } else if (baseURL.startsWith("https://") && baseURL.endsWith(":443")) {
                baseURL = baseURL.substring(0, colonIndex);
            }
        }
        return baseURL + url.substring(slashIndex);
    }

    public void setConsumerKey(String consumerKey2) {
        this.consumerKey = consumerKey2 != null ? consumerKey2 : "";
    }

    public void setConsumerSecret(String consumerSecret2) {
        this.consumerSecret = consumerSecret2 != null ? consumerSecret2 : "";
    }

    private void log(String message) {
        if (DEBUG) {
            System.out.println("[" + new Date() + "]" + message);
        }
    }

    private void log(String message, String message2) {
        if (DEBUG) {
            log(message + message2);
        }
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OAuth)) {
            return DEBUG;
        }
        OAuth oAuth = (OAuth) o;
        if (this.consumerKey == null ? oAuth.consumerKey != null : !this.consumerKey.equals(oAuth.consumerKey)) {
            return DEBUG;
        }
        if (this.consumerSecret == null ? oAuth.consumerSecret != null : !this.consumerSecret.equals(oAuth.consumerSecret)) {
            return DEBUG;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i;
        if (this.consumerKey != null) {
            result = this.consumerKey.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.consumerSecret != null) {
            i = this.consumerSecret.hashCode();
        } else {
            i = 0;
        }
        return i2 + i;
    }

    public String toString() {
        return "OAuth{consumerKey='" + this.consumerKey + '\'' + ", consumerSecret='" + this.consumerSecret + '\'' + '}';
    }
}
