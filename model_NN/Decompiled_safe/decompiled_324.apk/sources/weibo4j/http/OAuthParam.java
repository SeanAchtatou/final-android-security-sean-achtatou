package weibo4j.http;

public class OAuthParam {
    private String nonce;
    private String oauthBaseString;
    private String signature;
    private String timestamp;

    public String getNonce() {
        return this.nonce;
    }

    public void setNonce(String nonce2) {
        this.nonce = nonce2;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(String timestamp2) {
        this.timestamp = timestamp2;
    }

    public String getOauthBaseString() {
        return this.oauthBaseString;
    }

    public void setOauthBaseString(String oauthBaseString2) {
        this.oauthBaseString = oauthBaseString2;
    }

    public String getSignature() {
        return this.signature;
    }

    public void setSignature(String signature2) {
        this.signature = signature2;
    }
}
