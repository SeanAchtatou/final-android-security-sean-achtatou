package weibo4j.http;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.activation.MimetypesFileTypeMap;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.PartBase;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.httpclient.params.HostParams;
import qq.api.OAuth;
import weibo4j.Configuration;
import weibo4j.Weibo;
import weibo4j.WeiboException;

public class HttpClient implements Serializable {
    private static final int BAD_GATEWAY = 502;
    private static final int BAD_REQUEST = 400;
    private static final boolean DEBUG = Configuration.getDebug();
    private static final int FORBIDDEN = 403;
    private static final int INTERNAL_SERVER_ERROR = 500;
    private static final int NOT_ACCEPTABLE = 406;
    private static final int NOT_AUTHORIZED = 401;
    private static final int NOT_FOUND = 404;
    private static final int NOT_MODIFIED = 304;
    private static final int OK = 200;
    private static final int SERVICE_UNAVAILABLE = 503;
    private static boolean isJDK14orEarlier = false;
    private static final long serialVersionUID = 808018030183407996L;
    private String accessTokenURL;
    private String authenticationURL;
    private String authorizationURL;
    private String basic;
    private int connectionTimeout;
    private OAuth oauth;
    private OAuthToken oauthToken;
    private String password;
    /* access modifiers changed from: private */
    public String proxyAuthPassword;
    /* access modifiers changed from: private */
    public String proxyAuthUser;
    private String proxyHost;
    private int proxyPort;
    private int readTimeout;
    private Map<String, String> requestHeaders;
    private String requestTokenURL;
    private int retryCount;
    private int retryIntervalMillis;
    private String token;
    private String userId;

    static {
        boolean z;
        isJDK14orEarlier = DEBUG;
        try {
            String versionStr = System.getProperty("java.specification.version");
            if (versionStr != null) {
                if (1.5d > Double.parseDouble(versionStr)) {
                    z = true;
                } else {
                    z = false;
                }
                isJDK14orEarlier = z;
            }
        } catch (AccessControlException e) {
            isJDK14orEarlier = true;
        }
    }

    public HttpClient(String userId2, String password2) {
        this();
        setUserId(userId2);
        setPassword(password2);
    }

    public HttpClient() {
        this.retryCount = Configuration.getRetryCount();
        this.retryIntervalMillis = Configuration.getRetryIntervalSecs() * 1000;
        this.userId = Configuration.getUser();
        this.password = Configuration.getPassword();
        this.proxyHost = Configuration.getProxyHost();
        this.proxyPort = Configuration.getProxyPort();
        this.proxyAuthUser = Configuration.getProxyUser();
        this.proxyAuthPassword = Configuration.getProxyPassword();
        this.connectionTimeout = Configuration.getConnectionTimeout();
        this.readTimeout = Configuration.getReadTimeout();
        this.requestHeaders = new HashMap();
        this.oauth = null;
        this.requestTokenURL = Configuration.getScheme() + "api.t.sina.com.cn/oauth/request_token";
        this.authorizationURL = Configuration.getScheme() + "api.t.sina.com.cn/oauth/authorize";
        this.authenticationURL = Configuration.getScheme() + "api.t.sina.com.cn/oauth/authenticate";
        this.accessTokenURL = Configuration.getScheme() + "api.t.sina.com.cn/oauth/access_token";
        this.oauthToken = null;
        this.token = null;
        this.basic = null;
        setUserAgent(null);
        setOAuthConsumer(null, null);
        setRequestHeader("Accept-Encoding", "gzip");
    }

    public void setUserId(String userId2) {
        this.userId = userId2;
        encodeBasicAuthenticationString();
    }

    public void setPassword(String password2) {
        this.password = password2;
        encodeBasicAuthenticationString();
    }

    public String getUserId() {
        return this.userId;
    }

    public String getPassword() {
        return this.password;
    }

    public boolean isAuthenticationEnabled() {
        if (this.basic == null && this.oauth == null) {
            return DEBUG;
        }
        return true;
    }

    public void setOAuthConsumer(String consumerKey, String consumerSecret) {
        String consumerKey2 = Configuration.getOAuthConsumerKey(consumerKey);
        String consumerSecret2 = Configuration.getOAuthConsumerSecret(consumerSecret);
        if (consumerKey2 != null && consumerSecret2 != null && consumerKey2.length() != 0 && consumerSecret2.length() != 0) {
            this.oauth = new OAuth(consumerKey2, consumerSecret2);
        }
    }

    public RequestToken setToken(String token2, String tokenSecret) {
        this.token = token2;
        this.oauthToken = new RequestToken(token2, tokenSecret);
        return (RequestToken) this.oauthToken;
    }

    public RequestToken getOAuthRequestToken() throws WeiboException {
        this.oauthToken = new RequestToken(httpRequest(this.requestTokenURL, null, true), this);
        return (RequestToken) this.oauthToken;
    }

    public RequestToken getOauthRequestToken(String callback_url) throws WeiboException {
        this.oauthToken = new RequestToken(httpRequest(this.requestTokenURL, new PostParameter[]{new PostParameter((String) OAuth.OAuthCallbackKey, callback_url)}, true), this);
        return (RequestToken) this.oauthToken;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public AccessToken getOAuthAccessToken(RequestToken token2) throws WeiboException {
        try {
            this.oauthToken = token2;
            this.oauthToken = new AccessToken(httpRequest(this.accessTokenURL, new PostParameter[0], true));
            return (AccessToken) this.oauthToken;
        } catch (WeiboException e) {
            WeiboException te = e;
            throw new WeiboException("The user has not given access to the account.", te, te.getStatusCode());
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public AccessToken getOAuthAccessToken(RequestToken token2, String pin) throws WeiboException {
        try {
            this.oauthToken = token2;
            this.oauthToken = new AccessToken(httpRequest(this.accessTokenURL, new PostParameter[]{new PostParameter((String) OAuth.oAauthVerifier, pin)}, true));
            return (AccessToken) this.oauthToken;
        } catch (WeiboException e) {
            WeiboException te = e;
            throw new WeiboException("The user has not given access to the account.", te, te.getStatusCode());
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public AccessToken getOAuthAccessToken(String token2, String tokenSecret) throws WeiboException {
        try {
            this.oauthToken = new OAuthToken(token2, tokenSecret) {
            };
            this.oauthToken = new AccessToken(httpRequest(this.accessTokenURL, new PostParameter[0], true));
            return (AccessToken) this.oauthToken;
        } catch (WeiboException e) {
            WeiboException te = e;
            throw new WeiboException("The user has not given access to the account.", te, te.getStatusCode());
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public AccessToken getOAuthAccessToken(String token2, String tokenSecret, String oauth_verifier) throws WeiboException {
        try {
            this.oauthToken = new OAuthToken(token2, tokenSecret) {
            };
            this.oauthToken = new AccessToken(httpRequest(this.accessTokenURL, new PostParameter[]{new PostParameter((String) OAuth.oAauthVerifier, oauth_verifier)}, true));
            return (AccessToken) this.oauthToken;
        } catch (WeiboException e) {
            WeiboException te = e;
            throw new WeiboException("The user has not given access to the account.", te, te.getStatusCode());
        }
    }

    public void setOAuthAccessToken(AccessToken token2) {
        this.oauthToken = token2;
    }

    public void setRequestTokenURL(String requestTokenURL2) {
        this.requestTokenURL = requestTokenURL2;
    }

    public String getRequestTokenURL() {
        return this.requestTokenURL;
    }

    public void setAuthorizationURL(String authorizationURL2) {
        this.authorizationURL = authorizationURL2;
    }

    public String getAuthorizationURL() {
        return this.authorizationURL;
    }

    public String getAuthenticationRL() {
        return this.authenticationURL;
    }

    public void setAccessTokenURL(String accessTokenURL2) {
        this.accessTokenURL = accessTokenURL2;
    }

    public String getAccessTokenURL() {
        return this.accessTokenURL;
    }

    public String getProxyHost() {
        return this.proxyHost;
    }

    public void setProxyHost(String proxyHost2) {
        this.proxyHost = Configuration.getProxyHost(proxyHost2);
    }

    public int getProxyPort() {
        return this.proxyPort;
    }

    public void setProxyPort(int proxyPort2) {
        this.proxyPort = Configuration.getProxyPort(proxyPort2);
    }

    public String getProxyAuthUser() {
        return this.proxyAuthUser;
    }

    public void setProxyAuthUser(String proxyAuthUser2) {
        this.proxyAuthUser = Configuration.getProxyUser(proxyAuthUser2);
    }

    public String getProxyAuthPassword() {
        return this.proxyAuthPassword;
    }

    public void setProxyAuthPassword(String proxyAuthPassword2) {
        this.proxyAuthPassword = Configuration.getProxyPassword(proxyAuthPassword2);
    }

    public int getConnectionTimeout() {
        return this.connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout2) {
        this.connectionTimeout = Configuration.getConnectionTimeout(connectionTimeout2);
    }

    public int getReadTimeout() {
        return this.readTimeout;
    }

    public void setReadTimeout(int readTimeout2) {
        this.readTimeout = Configuration.getReadTimeout(readTimeout2);
    }

    private void encodeBasicAuthenticationString() {
        if (this.userId != null && this.password != null) {
            this.basic = "Basic " + new String(new BASE64Encoder().encode((this.userId + ":" + this.password).getBytes()));
            this.oauth = null;
        }
    }

    public void setRetryCount(int retryCount2) {
        if (retryCount2 >= 0) {
            this.retryCount = Configuration.getRetryCount(retryCount2);
            return;
        }
        throw new IllegalArgumentException("RetryCount cannot be negative.");
    }

    public void setUserAgent(String ua) {
        setRequestHeader("User-Agent", Configuration.getUserAgent(ua));
    }

    public String getUserAgent() {
        return getRequestHeader("User-Agent");
    }

    public void setRetryIntervalSecs(int retryIntervalSecs) {
        if (retryIntervalSecs >= 0) {
            this.retryIntervalMillis = Configuration.getRetryIntervalSecs(retryIntervalSecs) * 1000;
            return;
        }
        throw new IllegalArgumentException("RetryInterval cannot be negative.");
    }

    public Response post(String url, PostParameter[] postParameters, boolean authenticated) throws WeiboException {
        PostParameter[] newPostParameters = (PostParameter[]) copy(postParameters, postParameters.length + 1);
        newPostParameters[postParameters.length] = new PostParameter("source", Weibo.CONSUMER_KEY);
        return httpRequest(url, newPostParameters, authenticated);
    }

    public Response delete(String url, boolean authenticated) throws WeiboException {
        return httpRequest(url, null, authenticated, "DELETE");
    }

    public Response multPartURL(String url, PostParameter[] params, ImageItem item, boolean authenticated) throws WeiboException {
        Part[] parts;
        String authorization;
        PostMethod postMethod = new PostMethod(url);
        try {
            org.apache.commons.httpclient.HttpClient client = new org.apache.commons.httpclient.HttpClient();
            long t = System.currentTimeMillis();
            if (params == null) {
                parts = new Part[1];
            } else {
                parts = new Part[(params.length + 1)];
            }
            if (params != null) {
                PostParameter[] arr$ = params;
                int len$ = arr$.length;
                int i$ = 0;
                int i = 0;
                while (i$ < len$) {
                    PostParameter entry = arr$[i$];
                    parts[i] = new StringPart(entry.getName(), entry.getValue());
                    i$++;
                    i++;
                }
                parts[parts.length - 1] = new ByteArrayPart(item.getContent(), item.getName(), item.getContentType());
            }
            postMethod.setRequestEntity(new MultipartRequestEntity(parts, postMethod.getParams()));
            ArrayList arrayList = new ArrayList();
            if (authenticated) {
                if (!(this.basic == null && this.oauth == null)) {
                }
                if (this.oauth != null) {
                    authorization = this.oauth.generateAuthorizationHeader("POST", url, params, this.oauthToken);
                } else if (this.basic != null) {
                    authorization = this.basic;
                } else {
                    throw new IllegalStateException("Neither user ID/password combination nor OAuth consumer key/secret combination supplied");
                }
                arrayList.add(new Header("Authorization", authorization));
                log("Authorization: " + authorization);
            }
            client.getHostConfiguration().getParams().setParameter(HostParams.DEFAULT_HEADERS, arrayList);
            client.executeMethod(postMethod);
            Response response = new Response();
            response.setResponseAsString(postMethod.getResponseBodyAsString());
            response.setStatusCode(postMethod.getStatusCode());
            log("multPartURL URL:" + url + ", result:" + response + ", time:" + (System.currentTimeMillis() - t));
            postMethod.releaseConnection();
            return response;
        } catch (Exception e) {
            Exception ex = e;
            throw new WeiboException(ex.getMessage(), ex, -1);
        } catch (Throwable th) {
            postMethod.releaseConnection();
            throw th;
        }
    }

    public Response multPartURL(String fileParamName, String url, PostParameter[] params, File file, boolean authenticated) throws WeiboException {
        Part[] parts;
        String authorization;
        PostMethod postMethod = new PostMethod(url);
        org.apache.commons.httpclient.HttpClient client = new org.apache.commons.httpclient.HttpClient();
        try {
            long t = System.currentTimeMillis();
            if (params == null) {
                parts = new Part[1];
            } else {
                parts = new Part[(params.length + 1)];
            }
            if (params != null) {
                PostParameter[] arr$ = params;
                int len$ = arr$.length;
                int i$ = 0;
                int i = 0;
                while (i$ < len$) {
                    PostParameter entry = arr$[i$];
                    parts[i] = new StringPart(entry.getName(), entry.getValue());
                    i$++;
                    i++;
                }
            }
            FilePart filePart = new FilePart(fileParamName, file.getName(), file, new MimetypesFileTypeMap().getContentType(file), StringEncodings.UTF8);
            filePart.setTransferEncoding(FilePart.DEFAULT_TRANSFER_ENCODING);
            parts[parts.length - 1] = filePart;
            postMethod.setRequestEntity(new MultipartRequestEntity(parts, postMethod.getParams()));
            ArrayList arrayList = new ArrayList();
            if (authenticated) {
                if (!(this.basic == null && this.oauth == null)) {
                }
                if (this.oauth != null) {
                    authorization = this.oauth.generateAuthorizationHeader("POST", url, params, this.oauthToken);
                } else if (this.basic != null) {
                    authorization = this.basic;
                } else {
                    throw new IllegalStateException("Neither user ID/password combination nor OAuth consumer key/secret combination supplied");
                }
                arrayList.add(new Header("Authorization", authorization));
                log("Authorization: " + authorization);
            }
            client.getHostConfiguration().getParams().setParameter(HostParams.DEFAULT_HEADERS, arrayList);
            client.executeMethod(postMethod);
            Response response = new Response();
            response.setResponseAsString(postMethod.getResponseBodyAsString());
            response.setStatusCode(postMethod.getStatusCode());
            log("multPartURL URL:" + url + ", result:" + response + ", time:" + (System.currentTimeMillis() - t));
            postMethod.releaseConnection();
            return response;
        } catch (Exception e) {
            Exception ex = e;
            throw new WeiboException(ex.getMessage(), ex, -1);
        } catch (Throwable th) {
            postMethod.releaseConnection();
            throw th;
        }
    }

    public String postXaouth(String url, String[] params, boolean authenticate) {
        OAuth oAuth;
        String response = null;
        PostParameter[] pps = {new PostParameter("x_auth_username", params[0]), new PostParameter("x_auth_password", params[1])};
        if (this.oauth == null) {
            oAuth = new OAuth();
        } else {
            oAuth = this.oauth;
        }
        this.oauth = oAuth;
        OAuthParam result = this.oauth.generateXauthorizationHeader("POST", url, pps);
        try {
            org.apache.commons.httpclient.HttpClient client = new org.apache.commons.httpclient.HttpClient();
            PostMethod postMethod = new PostMethod(url);
            postMethod.setRequestBody(new NameValuePair[]{new NameValuePair(OAuth.OAuthConsumerKeyKey, Weibo.CONSUMER_KEY), new NameValuePair(OAuth.OAuthSignatureMethodKey, OAuth.HMACSHA1SignatureType_TEXT), new NameValuePair(OAuth.OAuthConsumerKeyKey, result.getTimestamp()), new NameValuePair(OAuth.OAuthNonceKey, result.getNonce()), new NameValuePair(OAuth.OAuthVersionKey, OAuth.OAuthVersion), new NameValuePair(OAuth.OAuthSignatureKey, result.getSignature()), new NameValuePair("source", Weibo.CONSUMER_KEY), new NameValuePair("x_auth_mode", "client_auth"), new NameValuePair("x_auth_password", params[1]), new NameValuePair("x_auth_username", params[0])});
            client.executeMethod(postMethod);
            System.out.println(postMethod.getStatusCode());
            if (postMethod.getStatusCode() == 200) {
                response = new String(postMethod.getResponseBodyAsString());
            }
            postMethod.releaseConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    private static class ByteArrayPart extends PartBase {
        private byte[] mData;
        private String mName;

        public ByteArrayPart(byte[] data, String name, String type) throws IOException {
            super(name, type, StringEncodings.UTF8, FilePart.DEFAULT_TRANSFER_ENCODING);
            this.mName = name;
            this.mData = data;
        }

        /* access modifiers changed from: protected */
        public void sendData(OutputStream out) throws IOException {
            out.write(this.mData);
        }

        /* access modifiers changed from: protected */
        public long lengthOfData() throws IOException {
            return (long) this.mData.length;
        }

        /* access modifiers changed from: protected */
        public void sendDispositionHeader(OutputStream out) throws IOException {
            super.sendDispositionHeader(out);
            StringBuilder buf = new StringBuilder();
            buf.append("; filename=\"").append(this.mName).append("\"");
            out.write(buf.toString().getBytes());
        }
    }

    public Response post(String url, boolean authenticated) throws WeiboException {
        return httpRequest(url, new PostParameter[0], authenticated);
    }

    public Response post(String url, PostParameter[] PostParameters) throws WeiboException {
        return httpRequest(url, PostParameters, DEBUG);
    }

    public Response post(String url) throws WeiboException {
        return httpRequest(url, new PostParameter[0], DEBUG);
    }

    public Response get(String url, boolean authenticated) throws WeiboException {
        return httpRequest(url, null, authenticated);
    }

    public Response get(String url) throws WeiboException {
        return httpRequest(url, null, DEBUG);
    }

    /* access modifiers changed from: protected */
    public Response httpRequest(String url, PostParameter[] postParams, boolean authenticated) throws WeiboException {
        PostParameter[] newPostParameters = postParams;
        String method = "GET";
        if (postParams != null) {
            method = "POST";
            newPostParameters = (PostParameter[]) copy(postParams, postParams.length + 1);
            newPostParameters[postParams.length] = new PostParameter("source", Weibo.CONSUMER_KEY);
        }
        return httpRequest(url, newPostParameters, authenticated, method);
    }

    public Response httpRequest(String url, PostParameter[] postParams, boolean authenticated, String httpMethod) throws WeiboException {
        int responseCode;
        int retry = this.retryCount + 1;
        Response res = null;
        int retriedCount = 0;
        while (true) {
            Response res2 = res;
            if (retriedCount >= retry) {
                return res2;
            }
            responseCode = -1;
            OutputStream osw = null;
            try {
                HttpURLConnection con = getConnection(url);
                con.setDoInput(true);
                setHeaders(url, postParams, con, authenticated, httpMethod);
                if (postParams != null || "POST".equals(httpMethod)) {
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", PostMethod.FORM_URL_ENCODED_CONTENT_TYPE);
                    con.setDoOutput(true);
                    String postParam = "";
                    if (postParams != null) {
                        postParam = encodeParameters(postParams);
                    }
                    log("Post Params: ", postParam);
                    byte[] bytes = postParam.getBytes(StringEncodings.UTF8);
                    con.setRequestProperty("Content-Length", Integer.toString(bytes.length));
                    osw = con.getOutputStream();
                    osw.write(bytes);
                    osw.flush();
                    osw.close();
                } else if ("DELETE".equals(httpMethod)) {
                    con.setRequestMethod("DELETE");
                } else {
                    con.setRequestMethod("GET");
                }
                res = new Response(con);
                try {
                    responseCode = con.getResponseCode();
                    if (DEBUG) {
                        log("Response: ");
                        Map<String, List<String>> responseHeaders = con.getHeaderFields();
                        for (String key : responseHeaders.keySet()) {
                            for (String value : responseHeaders.get(key)) {
                                if (key != null) {
                                    log(key + ": " + value);
                                } else {
                                    log(value);
                                }
                            }
                        }
                    }
                    if (responseCode == 200) {
                        try {
                            osw.close();
                            return res;
                        } catch (Exception e) {
                            return res;
                        }
                    } else if (responseCode >= 500 && retriedCount != this.retryCount) {
                        try {
                            osw.close();
                        } catch (Exception e2) {
                        }
                        try {
                            if (DEBUG && res != null) {
                                res.asString();
                            }
                            log("Sleeping " + this.retryIntervalMillis + " millisecs for next retry.");
                            Thread.sleep((long) this.retryIntervalMillis);
                        } catch (InterruptedException e3) {
                        }
                        retriedCount++;
                    }
                } catch (Throwable th) {
                    th = th;
                    try {
                        osw.close();
                    } catch (Exception e4) {
                    }
                    try {
                        throw th;
                    } catch (IOException e5) {
                        IOException ioe = e5;
                        if (retriedCount == this.retryCount) {
                            throw new WeiboException(ioe.getMessage(), ioe, responseCode);
                        }
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                res = res2;
                osw.close();
                throw th;
            }
        }
        throw new WeiboException(getCause(responseCode) + "\n" + res.asString(), responseCode);
    }

    public static String encodeParameters(PostParameter[] postParams) {
        StringBuffer buf = new StringBuffer();
        for (int j = 0; j < postParams.length; j++) {
            if (j != 0) {
                buf.append("&");
            }
            try {
                buf.append(URLEncoder.encode(postParams[j].name, StringEncodings.UTF8)).append("=").append(URLEncoder.encode(postParams[j].value, StringEncodings.UTF8));
            } catch (UnsupportedEncodingException e) {
            }
        }
        return buf.toString();
    }

    private void setHeaders(String url, PostParameter[] params, HttpURLConnection connection, boolean authenticated, String httpMethod) {
        String authorization;
        log("Request: ");
        log(httpMethod + " ", url);
        if (authenticated) {
            if (!(this.basic == null && this.oauth == null)) {
            }
            if (this.oauth != null) {
                authorization = this.oauth.generateAuthorizationHeader(httpMethod, url, params, this.oauthToken);
            } else if (this.basic != null) {
                authorization = this.basic;
            } else {
                throw new IllegalStateException("Neither user ID/password combination nor OAuth consumer key/secret combination supplied");
            }
            connection.addRequestProperty("Authorization", authorization);
            log("Authorization: " + authorization);
        }
        for (String key : this.requestHeaders.keySet()) {
            connection.addRequestProperty(key, this.requestHeaders.get(key));
            log(key + ": " + this.requestHeaders.get(key));
        }
    }

    public void setRequestHeader(String name, String value) {
        this.requestHeaders.put(name, value);
    }

    public String getRequestHeader(String name) {
        return this.requestHeaders.get(name);
    }

    private HttpURLConnection getConnection(String url) throws IOException {
        HttpURLConnection con;
        if (this.proxyHost == null || this.proxyHost.equals("")) {
            con = (HttpURLConnection) new URL(url).openConnection();
        } else {
            if (this.proxyAuthUser != null && !this.proxyAuthUser.equals("")) {
                log("Proxy AuthUser: " + this.proxyAuthUser);
                log("Proxy AuthPassword: " + this.proxyAuthPassword);
                Authenticator.setDefault(new Authenticator() {
                    /* access modifiers changed from: protected */
                    public PasswordAuthentication getPasswordAuthentication() {
                        if (getRequestorType().equals(Authenticator.RequestorType.PROXY)) {
                            return new PasswordAuthentication(HttpClient.this.proxyAuthUser, HttpClient.this.proxyAuthPassword.toCharArray());
                        }
                        return null;
                    }
                });
            }
            Proxy proxy = new Proxy(Proxy.Type.HTTP, InetSocketAddress.createUnresolved(this.proxyHost, this.proxyPort));
            if (DEBUG) {
                log("Opening proxied connection(" + this.proxyHost + ":" + this.proxyPort + ")");
            }
            con = (HttpURLConnection) new URL(url).openConnection(proxy);
        }
        if (this.connectionTimeout > 0 && !isJDK14orEarlier) {
            con.setConnectTimeout(this.connectionTimeout);
        }
        if (this.readTimeout > 0 && !isJDK14orEarlier) {
            con.setReadTimeout(this.readTimeout);
        }
        return con;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HttpClient)) {
            return DEBUG;
        }
        HttpClient that = (HttpClient) o;
        if (this.connectionTimeout != that.connectionTimeout) {
            return DEBUG;
        }
        if (this.proxyPort != that.proxyPort) {
            return DEBUG;
        }
        if (this.readTimeout != that.readTimeout) {
            return DEBUG;
        }
        if (this.retryCount != that.retryCount) {
            return DEBUG;
        }
        if (this.retryIntervalMillis != that.retryIntervalMillis) {
            return DEBUG;
        }
        if (this.accessTokenURL == null ? that.accessTokenURL != null : !this.accessTokenURL.equals(that.accessTokenURL)) {
            return DEBUG;
        }
        if (!this.authenticationURL.equals(that.authenticationURL)) {
            return DEBUG;
        }
        if (!this.authorizationURL.equals(that.authorizationURL)) {
            return DEBUG;
        }
        if (this.basic == null ? that.basic != null : !this.basic.equals(that.basic)) {
            return DEBUG;
        }
        if (this.oauth == null ? that.oauth != null : !this.oauth.equals(that.oauth)) {
            return DEBUG;
        }
        if (this.oauthToken == null ? that.oauthToken != null : !this.oauthToken.equals(that.oauthToken)) {
            return DEBUG;
        }
        if (this.password == null ? that.password != null : !this.password.equals(that.password)) {
            return DEBUG;
        }
        if (this.proxyAuthPassword == null ? that.proxyAuthPassword != null : !this.proxyAuthPassword.equals(that.proxyAuthPassword)) {
            return DEBUG;
        }
        if (this.proxyAuthUser == null ? that.proxyAuthUser != null : !this.proxyAuthUser.equals(that.proxyAuthUser)) {
            return DEBUG;
        }
        if (this.proxyHost == null ? that.proxyHost != null : !this.proxyHost.equals(that.proxyHost)) {
            return DEBUG;
        }
        if (!this.requestHeaders.equals(that.requestHeaders)) {
            return DEBUG;
        }
        if (!this.requestTokenURL.equals(that.requestTokenURL)) {
            return DEBUG;
        }
        if (this.userId == null ? that.userId != null : !this.userId.equals(that.userId)) {
            return DEBUG;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        if (this.basic != null) {
            result = this.basic.hashCode();
        } else {
            result = 0;
        }
        int i9 = ((((result * 31) + this.retryCount) * 31) + this.retryIntervalMillis) * 31;
        if (this.userId != null) {
            i = this.userId.hashCode();
        } else {
            i = 0;
        }
        int i10 = (i9 + i) * 31;
        if (this.password != null) {
            i2 = this.password.hashCode();
        } else {
            i2 = 0;
        }
        int i11 = (i10 + i2) * 31;
        if (this.proxyHost != null) {
            i3 = this.proxyHost.hashCode();
        } else {
            i3 = 0;
        }
        int i12 = (((i11 + i3) * 31) + this.proxyPort) * 31;
        if (this.proxyAuthUser != null) {
            i4 = this.proxyAuthUser.hashCode();
        } else {
            i4 = 0;
        }
        int i13 = (i12 + i4) * 31;
        if (this.proxyAuthPassword != null) {
            i5 = this.proxyAuthPassword.hashCode();
        } else {
            i5 = 0;
        }
        int hashCode = (((((((i13 + i5) * 31) + this.connectionTimeout) * 31) + this.readTimeout) * 31) + this.requestHeaders.hashCode()) * 31;
        if (this.oauth != null) {
            i6 = this.oauth.hashCode();
        } else {
            i6 = 0;
        }
        int hashCode2 = (((((((hashCode + i6) * 31) + this.requestTokenURL.hashCode()) * 31) + this.authorizationURL.hashCode()) * 31) + this.authenticationURL.hashCode()) * 31;
        if (this.accessTokenURL != null) {
            i7 = this.accessTokenURL.hashCode();
        } else {
            i7 = 0;
        }
        int i14 = (hashCode2 + i7) * 31;
        if (this.oauthToken != null) {
            i8 = this.oauthToken.hashCode();
        } else {
            i8 = 0;
        }
        return i14 + i8;
    }

    private static void log(String message) {
        if (DEBUG) {
            System.out.println("[" + new Date() + "]" + message);
        }
    }

    private static void log(String message, String message2) {
        if (DEBUG) {
            log(message + message2);
        }
    }

    private static String getCause(int statusCode) {
        String cause = null;
        switch (statusCode) {
            case 304:
                break;
            case 400:
                cause = "The request was invalid.  An accompanying error message will explain why. This is the status code will be returned during rate limiting.";
                break;
            case 401:
                cause = "Authentication credentials were missing or incorrect.";
                break;
            case 403:
                cause = "The request is understood, but it has been refused.  An accompanying error message will explain why.";
                break;
            case 404:
                cause = "The URI requested is invalid or the resource requested, such as a user, does not exists.";
                break;
            case 406:
                cause = "Returned by the Search API when an invalid format is specified in the request.";
                break;
            case 500:
                cause = "Something is broken.  Please post to the group so the Weibo team can investigate.";
                break;
            case 502:
                cause = "Weibo is down or being upgraded.";
                break;
            case 503:
                cause = "Service Unavailable: The Weibo servers are up, but overloaded with requests. Try again later. The search and trend methods use this to indicate when you are being rate limited.";
                break;
            default:
                cause = "";
                break;
        }
        return statusCode + ":" + cause;
    }

    private static final <T> T[] copy(T[] source, int length) {
        T[] target = (Object[]) ((Object[]) Array.newInstance(source.getClass().getComponentType(), length));
        System.arraycopy(source, 0, target, 0, source.length);
        return target;
    }
}
