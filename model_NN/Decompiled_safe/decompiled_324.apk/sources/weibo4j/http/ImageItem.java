package weibo4j.http;

public class ImageItem {
    private byte[] content;
    private String contentType;
    private String name;

    public ImageItem(String name2, byte[] content2) throws Exception {
        String imgtype = getContentType(content2);
        if (imgtype == null || (!imgtype.equalsIgnoreCase("image/gif") && !imgtype.equalsIgnoreCase("image/png") && !imgtype.equalsIgnoreCase("image/jpeg"))) {
            throw new IllegalStateException("Unsupported image type, Only Suport JPG ,GIF,PNG!");
        }
        this.content = content2;
        this.name = name2;
        this.content = content2;
    }

    public byte[] getContent() {
        return this.content;
    }

    public String getName() {
        return this.name;
    }

    public String getContentType() {
        return this.contentType;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0048 A[SYNTHETIC, Splitter:B:30:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x004d A[SYNTHETIC, Splitter:B:33:0x004d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getContentType(byte[] r9) throws java.io.IOException {
        /*
            java.lang.String r6 = ""
            r0 = 0
            r3 = 0
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ all -> 0x0045 }
            r1.<init>(r9)     // Catch:{ all -> 0x0045 }
            javax.imageio.stream.MemoryCacheImageInputStream r4 = new javax.imageio.stream.MemoryCacheImageInputStream     // Catch:{ all -> 0x0059 }
            r4.<init>(r1)     // Catch:{ all -> 0x0059 }
            java.util.Iterator r2 = javax.imageio.ImageIO.getImageReaders(r4)     // Catch:{ all -> 0x005c }
        L_0x0012:
            boolean r7 = r2.hasNext()     // Catch:{ all -> 0x005c }
            if (r7 == 0) goto L_0x003a
            java.lang.Object r5 = r2.next()     // Catch:{ all -> 0x005c }
            javax.imageio.ImageReader r5 = (javax.imageio.ImageReader) r5     // Catch:{ all -> 0x005c }
            boolean r7 = r5 instanceof com.sun.imageio.plugins.gif.GIFImageReader     // Catch:{ all -> 0x005c }
            if (r7 == 0) goto L_0x0025
            java.lang.String r6 = "image/gif"
            goto L_0x0012
        L_0x0025:
            boolean r7 = r5 instanceof com.sun.imageio.plugins.jpeg.JPEGImageReader     // Catch:{ all -> 0x005c }
            if (r7 == 0) goto L_0x002c
            java.lang.String r6 = "image/jpeg"
            goto L_0x0012
        L_0x002c:
            boolean r7 = r5 instanceof com.sun.imageio.plugins.png.PNGImageReader     // Catch:{ all -> 0x005c }
            if (r7 == 0) goto L_0x0033
            java.lang.String r6 = "image/png"
            goto L_0x0012
        L_0x0033:
            boolean r7 = r5 instanceof com.sun.imageio.plugins.bmp.BMPImageReader     // Catch:{ all -> 0x005c }
            if (r7 == 0) goto L_0x0012
            java.lang.String r6 = "application/x-bmp"
            goto L_0x0012
        L_0x003a:
            if (r1 == 0) goto L_0x003f
            r1.close()     // Catch:{ IOException -> 0x0051 }
        L_0x003f:
            if (r4 == 0) goto L_0x0044
            r4.close()     // Catch:{ IOException -> 0x0053 }
        L_0x0044:
            return r6
        L_0x0045:
            r7 = move-exception
        L_0x0046:
            if (r0 == 0) goto L_0x004b
            r0.close()     // Catch:{ IOException -> 0x0055 }
        L_0x004b:
            if (r3 == 0) goto L_0x0050
            r3.close()     // Catch:{ IOException -> 0x0057 }
        L_0x0050:
            throw r7
        L_0x0051:
            r7 = move-exception
            goto L_0x003f
        L_0x0053:
            r7 = move-exception
            goto L_0x0044
        L_0x0055:
            r8 = move-exception
            goto L_0x004b
        L_0x0057:
            r8 = move-exception
            goto L_0x0050
        L_0x0059:
            r7 = move-exception
            r0 = r1
            goto L_0x0046
        L_0x005c:
            r7 = move-exception
            r3 = r4
            r0 = r1
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: weibo4j.http.ImageItem.getContentType(byte[]):java.lang.String");
    }
}
