package weibo4j.http;

import java.io.File;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

public class PostParameter implements Serializable, Comparable {
    private static final String GIF = "image/gif";
    private static final String JPEG = "image/jpeg";
    private static final String OCTET = "application/octet-stream";
    private static final String PNG = "image/png";
    private static final long serialVersionUID = -8708108746980739212L;
    private File file = null;
    String name;
    String value;

    public PostParameter(String name2, String value2) {
        this.name = name2;
        this.value = value2;
    }

    public PostParameter(String name2, double value2) {
        this.name = name2;
        this.value = String.valueOf(value2);
    }

    public PostParameter(String name2, int value2) {
        this.name = name2;
        this.value = String.valueOf(value2);
    }

    public PostParameter(String name2, File file2) {
        this.name = name2;
        this.file = file2;
    }

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }

    public File getFile() {
        return this.file;
    }

    public boolean isFile() {
        return this.file != null;
    }

    public String getContentType() {
        if (!isFile()) {
            throw new IllegalStateException("not a file");
        }
        String extensions = this.file.getName();
        if (-1 == extensions.lastIndexOf(".")) {
            return "application/octet-stream";
        }
        String extensions2 = extensions.substring(extensions.lastIndexOf(".") + 1).toLowerCase();
        if (extensions2.length() == 3) {
            if ("gif".equals(extensions2)) {
                return GIF;
            }
            if ("png".equals(extensions2)) {
                return PNG;
            }
            if ("jpg".equals(extensions2)) {
                return JPEG;
            }
            return "application/octet-stream";
        } else if (extensions2.length() != 4 || !"jpeg".equals(extensions2)) {
            return "application/octet-stream";
        } else {
            return JPEG;
        }
    }

    public static boolean containsFile(PostParameter[] params) {
        boolean containsFile = false;
        if (params == null) {
            return false;
        }
        PostParameter[] arr$ = params;
        int len$ = arr$.length;
        int i$ = 0;
        while (true) {
            if (i$ >= len$) {
                break;
            } else if (arr$[i$].isFile()) {
                containsFile = true;
                break;
            } else {
                i$++;
            }
        }
        return containsFile;
    }

    static boolean containsFile(List<PostParameter> params) {
        for (PostParameter param : params) {
            if (param.isFile()) {
                return true;
            }
        }
        return false;
    }

    public static PostParameter[] getParameterArray(String name2, String value2) {
        return new PostParameter[]{new PostParameter(name2, value2)};
    }

    public static PostParameter[] getParameterArray(String name2, int value2) {
        return getParameterArray(name2, String.valueOf(value2));
    }

    public static PostParameter[] getParameterArray(String name1, String value1, String name2, String value2) {
        return new PostParameter[]{new PostParameter(name1, value1), new PostParameter(name2, value2)};
    }

    public static PostParameter[] getParameterArray(String name1, int value1, String name2, int value2) {
        return getParameterArray(name1, String.valueOf(value1), name2, String.valueOf(value2));
    }

    public int hashCode() {
        return (((this.name.hashCode() * 31) + this.value.hashCode()) * 31) + (this.file != null ? this.file.hashCode() : 0);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PostParameter)) {
            return false;
        }
        PostParameter that = (PostParameter) obj;
        if (this.file == null ? that.file != null : !this.file.equals(that.file)) {
            return false;
        }
        return this.name.equals(that.name) && this.value.equals(that.value);
    }

    public String toString() {
        return "PostParameter{name='" + this.name + '\'' + ", value='" + this.value + '\'' + ", file=" + this.file + '}';
    }

    public int compareTo(Object o) {
        PostParameter that = (PostParameter) o;
        int compared = this.name.compareTo(that.name);
        if (compared == 0) {
            return this.value.compareTo(that.value);
        }
        return compared;
    }

    public static String encodeParameters(PostParameter[] httpParams) {
        if (httpParams == null) {
            return "";
        }
        StringBuffer buf = new StringBuffer();
        for (int j = 0; j < httpParams.length; j++) {
            if (httpParams[j].isFile()) {
                throw new IllegalArgumentException("parameter [" + httpParams[j].name + "]should be text");
            }
            if (j != 0) {
                buf.append("&");
            }
            try {
                buf.append(URLEncoder.encode(httpParams[j].name, StringEncodings.UTF8)).append("=").append(URLEncoder.encode(httpParams[j].value, StringEncodings.UTF8));
            } catch (UnsupportedEncodingException e) {
            }
        }
        return buf.toString();
    }
}
