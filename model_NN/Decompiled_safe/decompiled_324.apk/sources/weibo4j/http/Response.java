package weibo4j.http;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import weibo4j.Configuration;
import weibo4j.WeiboException;
import weibo4j.org.json.JSONArray;
import weibo4j.org.json.JSONException;
import weibo4j.org.json.JSONObject;

public class Response {
    private static final boolean DEBUG = Configuration.getDebug();
    private static ThreadLocal<DocumentBuilder> builders = new ThreadLocal<DocumentBuilder>() {
        /* access modifiers changed from: protected */
        public DocumentBuilder initialValue() {
            try {
                return DocumentBuilderFactory.newInstance().newDocumentBuilder();
            } catch (ParserConfigurationException e) {
                throw new ExceptionInInitializerError(e);
            }
        }
    };
    private static Pattern escaped = Pattern.compile("&#([0-9]{3,5});");
    private HttpURLConnection con;
    private InputStream is;
    private Document responseAsDocument = null;
    private String responseAsString = null;
    private int statusCode;
    private boolean streamConsumed = false;

    public Response() {
    }

    public Response(HttpURLConnection con2) throws IOException {
        this.con = con2;
        this.statusCode = con2.getResponseCode();
        InputStream errorStream = con2.getErrorStream();
        this.is = errorStream;
        if (errorStream == null) {
            this.is = con2.getInputStream();
        }
        if (this.is != null && "gzip".equals(con2.getContentEncoding())) {
            this.is = new GZIPInputStream(this.is);
        }
    }

    Response(String content) {
        this.responseAsString = content;
    }

    public int getStatusCode() {
        return this.statusCode;
    }

    public String getResponseHeader(String name) {
        if (this.con != null) {
            return this.con.getHeaderField(name);
        }
        return null;
    }

    public InputStream asStream() {
        if (!this.streamConsumed) {
            return this.is;
        }
        throw new IllegalStateException("Stream has already been consumed.");
    }

    public String asString() throws WeiboException {
        if (this.responseAsString == null) {
            try {
                InputStream stream = asStream();
                if (stream == null) {
                    return null;
                }
                BufferedReader br = new BufferedReader(new InputStreamReader(stream, StringEncodings.UTF8));
                StringBuffer buf = new StringBuffer();
                while (true) {
                    String line = br.readLine();
                    if (line == null) {
                        break;
                    }
                    buf.append(line).append("\n");
                }
                this.responseAsString = buf.toString();
                if (Configuration.isDalvik()) {
                    this.responseAsString = unescape(this.responseAsString);
                }
                log(this.responseAsString);
                stream.close();
                this.con.disconnect();
                this.streamConsumed = true;
            } catch (NullPointerException e) {
                NullPointerException npe = e;
                throw new WeiboException(npe.getMessage(), npe);
            } catch (IOException e2) {
                IOException ioe = e2;
                throw new WeiboException(ioe.getMessage(), ioe);
            }
        }
        return this.responseAsString;
    }

    public Document asDocument() throws WeiboException {
        if (this.responseAsDocument == null) {
            try {
                this.responseAsDocument = builders.get().parse(new ByteArrayInputStream(asString().getBytes(StringEncodings.UTF8)));
            } catch (SAXException e) {
                throw new WeiboException("The response body was not well-formed:\n" + this.responseAsString, e);
            } catch (IOException e2) {
                throw new WeiboException("There's something with the connection.", e2);
            }
        }
        return this.responseAsDocument;
    }

    public JSONObject asJSONObject() throws WeiboException {
        try {
            return new JSONObject(asString());
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new WeiboException(jsone.getMessage() + ":" + this.responseAsString, jsone);
        }
    }

    public JSONArray asJSONArray() throws WeiboException {
        try {
            return new JSONArray(asString());
        } catch (Exception e) {
            Exception jsone = e;
            throw new WeiboException(jsone.getMessage() + ":" + this.responseAsString, jsone);
        }
    }

    public InputStreamReader asReader() {
        try {
            return new InputStreamReader(this.is, StringEncodings.UTF8);
        } catch (UnsupportedEncodingException e) {
            return new InputStreamReader(this.is);
        }
    }

    public void disconnect() {
        this.con.disconnect();
    }

    public static String unescape(String original) {
        Matcher mm = escaped.matcher(original);
        StringBuffer unescaped = new StringBuffer();
        while (mm.find()) {
            mm.appendReplacement(unescaped, Character.toString((char) Integer.parseInt(mm.group(1), 10)));
        }
        mm.appendTail(unescaped);
        return unescaped.toString();
    }

    public String toString() {
        if (this.responseAsString != null) {
            return this.responseAsString;
        }
        return "Response{statusCode=" + this.statusCode + ", response=" + this.responseAsDocument + ", responseString='" + this.responseAsString + '\'' + ", is=" + this.is + ", con=" + this.con + '}';
    }

    private void log(String message) {
        if (DEBUG) {
            System.out.println("[" + new Date() + "]" + message);
        }
    }

    private void log(String message, String message2) {
        if (DEBUG) {
            log(message + message2);
        }
    }

    public String getResponseAsString() {
        return this.responseAsString;
    }

    public void setResponseAsString(String responseAsString2) {
        this.responseAsString = responseAsString2;
    }

    public void setStatusCode(int statusCode2) {
        this.statusCode = statusCode2;
    }
}
