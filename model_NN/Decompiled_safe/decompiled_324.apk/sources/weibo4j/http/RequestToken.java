package weibo4j.http;

import weibo4j.WeiboException;

public class RequestToken extends OAuthToken {
    private static final long serialVersionUID = -8214365845469757952L;
    private HttpClient httpClient;

    public /* bridge */ /* synthetic */ String getParameter(String x0) {
        return super.getParameter(x0);
    }

    public /* bridge */ /* synthetic */ String getToken() {
        return super.getToken();
    }

    public /* bridge */ /* synthetic */ String getTokenSecret() {
        return super.getTokenSecret();
    }

    public /* bridge */ /* synthetic */ String toString() {
        return super.toString();
    }

    RequestToken(Response res, HttpClient httpClient2) throws WeiboException {
        super(res);
        this.httpClient = httpClient2;
    }

    RequestToken(String token, String tokenSecret) {
        super(token, tokenSecret);
    }

    public String getAuthorizationURL() {
        return this.httpClient.getAuthorizationURL() + "?oauth_token=" + getToken();
    }

    public String getAuthenticationURL() {
        return this.httpClient.getAuthenticationRL() + "?oauth_token=" + getToken();
    }

    public AccessToken getAccessToken(String pin) throws WeiboException {
        return this.httpClient.getOAuthAccessToken(this, pin);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        RequestToken that = (RequestToken) o;
        return this.httpClient == null ? that.httpClient == null : this.httpClient.equals(that.httpClient);
    }

    public int hashCode() {
        return (super.hashCode() * 31) + (this.httpClient != null ? this.httpClient.hashCode() : 0);
    }
}
