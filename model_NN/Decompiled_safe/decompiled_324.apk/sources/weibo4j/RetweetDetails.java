package weibo4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import weibo4j.http.Response;
import weibo4j.org.json.JSONArray;
import weibo4j.org.json.JSONException;
import weibo4j.org.json.JSONObject;

public class RetweetDetails extends WeiboResponse implements Serializable {
    static final long serialVersionUID = 1957982268696560598L;
    private long retweetId;
    private Date retweetedAt;
    private User retweetingUser;

    RetweetDetails(Response res, Weibo weibo) throws WeiboException {
        super(res);
        init(res, res.asDocument().getDocumentElement(), weibo);
    }

    RetweetDetails(JSONObject json) throws WeiboException {
        init(json);
    }

    private void init(JSONObject json) throws WeiboException {
        try {
            this.retweetId = (long) json.getInt("retweetId");
            this.retweetedAt = parseDate(json.getString("retweetedAt"), "EEE MMM dd HH:mm:ss z yyyy");
            this.retweetingUser = new User(json.getJSONObject("retweetingUser"));
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new WeiboException(jsone.getMessage() + ":" + json.toString(), jsone);
        }
    }

    RetweetDetails(Response res, Element elem, Weibo weibo) throws WeiboException {
        super(res);
        init(res, elem, weibo);
    }

    private void init(Response res, Element elem, Weibo weibo) throws WeiboException {
        ensureRootNodeNameIs("retweet_details", elem);
        this.retweetId = getChildLong("retweet_id", elem);
        this.retweetedAt = getChildDate("retweeted_at", elem);
        this.retweetingUser = new User(res, (Element) elem.getElementsByTagName("retweeting_user").item(0), weibo);
    }

    public long getRetweetId() {
        return this.retweetId;
    }

    public Date getRetweetedAt() {
        return this.retweetedAt;
    }

    public User getRetweetingUser() {
        return this.retweetingUser;
    }

    static List<RetweetDetails> createRetweetDetails(Response res) throws WeiboException {
        try {
            JSONArray list = res.asJSONArray();
            int size = list.length();
            List<RetweetDetails> retweets = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                retweets.add(new RetweetDetails(list.getJSONObject(i)));
            }
            return retweets;
        } catch (JSONException e) {
            throw new WeiboException(e);
        } catch (WeiboException e2) {
            throw e2;
        }
    }

    static List<RetweetDetails> createRetweetDetails(Response res, Weibo weibo) throws WeiboException {
        Document doc = res.asDocument();
        if (isRootNodeNilClasses(doc)) {
            return new ArrayList(0);
        }
        try {
            ensureRootNodeNameIs("retweets", doc);
            NodeList list = doc.getDocumentElement().getElementsByTagName("retweet_details");
            int size = list.getLength();
            List<RetweetDetails> statuses = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                statuses.add(new RetweetDetails(res, (Element) list.item(i), weibo));
            }
            return statuses;
        } catch (WeiboException e) {
            ensureRootNodeNameIs("nil-classes", doc);
            return new ArrayList(0);
        }
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RetweetDetails)) {
            return false;
        }
        return this.retweetId == ((RetweetDetails) o).retweetId;
    }

    public int hashCode() {
        return (((((int) (this.retweetId ^ (this.retweetId >>> 32))) * 31) + this.retweetedAt.hashCode()) * 31) + this.retweetingUser.hashCode();
    }

    public String toString() {
        return "RetweetDetails{retweetId=" + this.retweetId + ", retweetedAt=" + this.retweetedAt + ", retweetingUser=" + this.retweetingUser + '}';
    }
}
