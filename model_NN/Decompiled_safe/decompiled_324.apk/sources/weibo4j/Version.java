package weibo4j;

public class Version {
    private static final String TITLE = "Weibo4J";
    private static final String VERSION = "2.0.10";

    public static String getVersion() {
        return VERSION;
    }

    public static void main(String[] args) {
        System.out.println("Weibo4J 2.0.10");
    }
}
