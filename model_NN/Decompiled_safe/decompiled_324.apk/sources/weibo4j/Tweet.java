package weibo4j;

import java.util.Date;
import weibo4j.org.json.JSONException;
import weibo4j.org.json.JSONObject;

public class Tweet extends WeiboResponse {
    private static final long serialVersionUID = 4299736733993211587L;
    private Date createdAt;
    private String fromUser;
    private int fromUserId;
    private long id;
    private String isoLanguageCode = null;
    private String profileImageUrl;
    private String source;
    private String text;
    private String toUser = null;
    private int toUserId = -1;

    Tweet(JSONObject tweet, WeiboSupport weiboSupport) throws WeiboException {
        try {
            this.text = getString("text", tweet, false);
            try {
                this.toUserId = tweet.getInt("to_user_id");
                this.toUser = tweet.getString("to_user");
            } catch (JSONException e) {
            }
            this.fromUser = tweet.getString("from_user");
            this.id = tweet.getLong("id");
            this.fromUserId = tweet.getInt("from_user_id");
            try {
                this.isoLanguageCode = tweet.getString("iso_language_code");
            } catch (JSONException e2) {
            }
            this.source = getString("source", tweet, true);
            this.profileImageUrl = getString("profile_image_url", tweet, true);
            this.createdAt = parseDate(tweet.getString("created_at"), "EEE, dd MMM yyyy HH:mm:ss z");
        } catch (JSONException e3) {
            JSONException jsone = e3;
            throw new WeiboException(jsone.getMessage() + ":" + tweet.toString(), jsone);
        }
    }

    public String getText() {
        return this.text;
    }

    public int getToUserId() {
        return this.toUserId;
    }

    public String getToUser() {
        return this.toUser;
    }

    public String getFromUser() {
        return this.fromUser;
    }

    public long getId() {
        return this.id;
    }

    public int getFromUserId() {
        return this.fromUserId;
    }

    public String getIsoLanguageCode() {
        return this.isoLanguageCode;
    }

    public String getSource() {
        return this.source;
    }

    public String getProfileImageUrl() {
        return this.profileImageUrl;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tweet that = (Tweet) o;
        if (this.fromUserId != that.fromUserId) {
            return false;
        }
        if (this.id != that.id) {
            return false;
        }
        if (this.toUserId != that.toUserId) {
            return false;
        }
        if (this.createdAt == null ? that.createdAt != null : !this.createdAt.equals(that.createdAt)) {
            return false;
        }
        if (this.fromUser == null ? that.fromUser != null : !this.fromUser.equals(that.fromUser)) {
            return false;
        }
        if (this.isoLanguageCode == null ? that.isoLanguageCode != null : !this.isoLanguageCode.equals(that.isoLanguageCode)) {
            return false;
        }
        if (this.profileImageUrl == null ? that.profileImageUrl != null : !this.profileImageUrl.equals(that.profileImageUrl)) {
            return false;
        }
        if (this.source == null ? that.source != null : !this.source.equals(that.source)) {
            return false;
        }
        if (this.text == null ? that.text != null : !this.text.equals(that.text)) {
            return false;
        }
        return this.toUser == null ? that.toUser == null : this.toUser.equals(that.toUser);
    }

    public int hashCode() {
        int result;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        if (this.text != null) {
            result = this.text.hashCode();
        } else {
            result = 0;
        }
        int i7 = ((result * 31) + (this.toUserId ^ (this.toUserId >>> 32))) * 31;
        if (this.toUser != null) {
            i = this.toUser.hashCode();
        } else {
            i = 0;
        }
        int i8 = (i7 + i) * 31;
        if (this.fromUser != null) {
            i2 = this.fromUser.hashCode();
        } else {
            i2 = 0;
        }
        int i9 = (((((i8 + i2) * 31) + ((int) (this.id ^ (this.id >>> 32)))) * 31) + (this.fromUserId ^ (this.fromUserId >>> 32))) * 31;
        if (this.isoLanguageCode != null) {
            i3 = this.isoLanguageCode.hashCode();
        } else {
            i3 = 0;
        }
        int i10 = (i9 + i3) * 31;
        if (this.source != null) {
            i4 = this.source.hashCode();
        } else {
            i4 = 0;
        }
        int i11 = (i10 + i4) * 31;
        if (this.profileImageUrl != null) {
            i5 = this.profileImageUrl.hashCode();
        } else {
            i5 = 0;
        }
        int i12 = (i11 + i5) * 31;
        if (this.createdAt != null) {
            i6 = this.createdAt.hashCode();
        } else {
            i6 = 0;
        }
        return i12 + i6;
    }

    public String toString() {
        return "Tweet{text='" + this.text + '\'' + ", toUserId=" + this.toUserId + ", toUser='" + this.toUser + '\'' + ", fromUser='" + this.fromUser + '\'' + ", id=" + this.id + ", fromUserId=" + this.fromUserId + ", isoLanguageCode='" + this.isoLanguageCode + '\'' + ", source='" + this.source + '\'' + ", profileImageUrl='" + this.profileImageUrl + '\'' + ", createdAt=" + this.createdAt + '}';
    }
}
