package weibo4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import weibo4j.http.Response;
import weibo4j.org.json.JSONArray;
import weibo4j.org.json.JSONException;
import weibo4j.org.json.JSONObject;

public class ListObject extends WeiboResponse implements Serializable {
    private static final long serialVersionUID = 4208232205515192208L;
    private String description;
    private String fullName;
    private long id;
    private int memberCount;
    private String mode;
    private String name;
    private String slug;
    private int subscriberCount;
    private String uri;
    private User user;

    ListObject(Response res, Weibo weibo) throws WeiboException {
        super(res);
        init(res, res.asDocument().getDocumentElement(), weibo);
    }

    ListObject(Response res, Element elem, Weibo weibo) throws WeiboException {
        super(res);
        init(res, elem, weibo);
    }

    ListObject(JSONObject json) throws WeiboException {
        try {
            this.id = json.getLong("id");
            this.name = json.getString("name");
            this.fullName = json.getString("full_name");
            this.slug = json.getString("slug");
            this.description = json.getString("description");
            this.subscriberCount = json.getInt("subscriber_count");
            this.memberCount = json.getInt("member_count");
            this.uri = json.getString("uri");
            this.mode = json.getString("mode");
            if (!json.isNull("user")) {
                this.user = new User(json.getJSONObject("user"));
            }
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new WeiboException(jsone.getMessage() + ":" + json.toString(), jsone);
        }
    }

    private void init(Response res, Element elem, Weibo weibo) throws WeiboException {
        ensureRootNodeNameIs("list", elem);
        this.id = getChildLong("id", elem);
        this.name = getChildText("name", elem);
        this.fullName = getChildText("full_name", elem);
        this.slug = getChildText("slug", elem);
        this.description = getChildText("description", elem);
        this.subscriberCount = getChildInt("subscriber_count", elem);
        this.memberCount = getChildInt("member_count", elem);
        this.uri = getChildText("uri", elem);
        this.mode = getChildText("mode", elem);
        NodeList statuses = elem.getElementsByTagName("user");
        if (statuses.getLength() != 0) {
            this.user = new User(res, (Element) statuses.item(0), weibo);
        }
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getFullName() {
        return this.fullName;
    }

    public void setFullName(String fullName2) {
        this.fullName = fullName2;
    }

    public String getSlug() {
        return this.slug;
    }

    public void setSlug(String slug2) {
        this.slug = slug2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public String getUri() {
        return this.uri;
    }

    public void setUri(String uri2) {
        this.uri = uri2;
    }

    public int getSubscriberCount() {
        return this.subscriberCount;
    }

    public void setSubscriberCount(int subscriberCount2) {
        this.subscriberCount = subscriberCount2;
    }

    public int getMemberCount() {
        return this.memberCount;
    }

    public void setMemberCount(int memberCount2) {
        this.memberCount = memberCount2;
    }

    public void setId(long id2) {
        this.id = id2;
    }

    public long getId() {
        return this.id;
    }

    public String getMode() {
        return this.mode;
    }

    public void setMode(String mode2) {
        this.mode = mode2;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user2) {
        this.user = user2;
    }

    /* JADX INFO: Multiple debug info for r2v1 long: [D('previousCursor' long), D('length' int)] */
    static ListObjectWapper constructListObjects(Response res, Weibo weibo) throws WeiboException {
        Document doc = res.asDocument();
        if (isRootNodeNilClasses(doc)) {
            return new ListObjectWapper(new ArrayList(0), 0, 0);
        }
        try {
            ensureRootNodeNameIs("lists_list", doc);
            Element root = doc.getDocumentElement();
            NodeList list = root.getElementsByTagName("lists");
            if (list.getLength() == 0) {
                return new ListObjectWapper(new ArrayList(0), 0, 0);
            }
            NodeList list2 = ((Element) list.item(0)).getElementsByTagName("list");
            int length = list2.getLength();
            List<ListObject> lists = new ArrayList<>(length);
            for (int i = 0; i < length; i++) {
                lists.add(new ListObject(res, (Element) list2.item(i), weibo));
            }
            long previousCursor = getChildLong("previous_curosr", root);
            long nextCursor = getChildLong("next_curosr", root);
            if (nextCursor == -1) {
                nextCursor = getChildLong("nextCurosr", root);
            }
            return new ListObjectWapper(lists, previousCursor, nextCursor);
        } catch (WeiboException te) {
            if (isRootNodeNilClasses(doc)) {
                return new ListObjectWapper(new ArrayList(0), 0, 0);
            }
            throw te;
        }
    }

    static ListObjectWapper constructListObjects(Response res) throws WeiboException {
        JSONObject jsonLists = res.asJSONObject();
        try {
            JSONArray list = jsonLists.getJSONArray("lists");
            int size = list.length();
            List<ListObject> listObjects = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                listObjects.add(new ListObject(list.getJSONObject(i)));
            }
            long previousCursor = jsonLists.getLong("previous_curosr");
            long nextCursor = jsonLists.getLong("next_cursor");
            if (nextCursor == -1) {
                nextCursor = jsonLists.getLong("nextCursor");
            }
            return new ListObjectWapper(listObjects, previousCursor, nextCursor);
        } catch (JSONException e) {
            throw new WeiboException(e);
        }
    }

    public int hashCode() {
        return (int) this.id;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return (obj instanceof ListObject) && ((ListObject) obj).id == this.id;
    }

    public String toString() {
        return "ListObject{id=" + this.id + ", name='" + this.name + '\'' + ", fullName='" + this.fullName + '\'' + ", slug='" + this.slug + '\'' + ", description='" + this.description + '\'' + ", subscriberCount=" + this.subscriberCount + ", memberCount=" + this.memberCount + ", mode='" + this.mode + "', uri='" + this.uri + '\'' + ", user='" + this.user.toString() + "'}";
    }
}
