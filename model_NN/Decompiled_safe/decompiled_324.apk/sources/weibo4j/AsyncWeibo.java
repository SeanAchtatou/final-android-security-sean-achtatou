package weibo4j;

import java.util.Date;
import weibo4j.Weibo;

public class AsyncWeibo extends Weibo {
    public static final int BLOCK = 22;
    public static final int CREATE = 12;
    public static final int CREATED_BLOCK = 43;
    public static final int CREATE_FAVORITE = 18;
    public static final int CREATE_FRIENDSHIP = 32;
    public static final int CURRENT_TRENDS = 45;
    public static final int DAILY_TRENDS = 46;
    public static final int DESTORY = 13;
    public static final int DESTROY = 13;
    public static final int DESTROYED_BLOCK = 42;
    public static final int DESTROY_DIRECT_MESSAGES = 40;
    public static final int DESTROY_FAVORITE = 19;
    public static final int DESTROY_FRIENDSHIP = 33;
    public static final int DESTROY_STATUS = 26;
    public static final int DIRECT_MESSAGES = 10;
    public static final int DISABLE_NOTIFICATION = 36;
    public static final int ENABLE_NOTIFICATION = 35;
    public static final int EXISTS = 28;
    private static final int EXISTS_BLOCK = 48;
    public static final int EXISTS_FRIENDSHIP = 34;
    public static final int FAVORITES = 17;
    public static final int FEATURED = 8;
    public static final int FOLLOW = 14;
    public static final int FOLLOWERS = 7;
    public static final int FOLLOWERS_IDS = 30;
    public static final int FRIENDS = 6;
    public static final int FRIENDS_IDS = 29;
    public static final int FRIENDS_TIMELINE = 1;
    private static final int GET_BLOCKING_USERS = 49;
    private static final int GET_BLOCKING_USERS_IDS = 50;
    public static final int GET_DOWNTIME_SCHEDULE = 25;
    public static final int HOME_TIMELINE = 51;
    public static final int LEAVE = 15;
    public static final int MENTIONS = 37;
    public static final int PUBLIC_TIMELINE = 0;
    public static final int RATE_LIMIT_STATUS = 28;
    public static final int REPLIES = 5;
    public static final int RETWEETED_BY_ME = 53;
    public static final int RETWEETED_TO_ME = 54;
    public static final int RETWEETS_OF_ME = 55;
    public static final int RETWEET_STATUS = 52;
    public static final int SEARCH = 27;
    public static final int SEND_DIRECT_MESSAGE = 11;
    public static final int SHOW = 3;
    public static final int SHOW_STATUS = 38;
    public static final int TEST = 24;
    public static final int TRENDS = 44;
    public static final int UNBLOCK = 23;
    public static final int UPDATE = 4;
    public static final int UPDATE_DELIVERLY_DEVICE = 21;
    public static final int UPDATE_LOCATION = 20;
    public static final int UPDATE_PROFILE = 41;
    public static final int UPDATE_PROFILE_COLORS = 31;
    public static final int UPDATE_STATUS = 39;
    public static final int USER_DETAIL = 9;
    public static final int USER_TIMELINE = 2;
    public static final int WEEKLY_TRENDS = 47;
    private static transient Dispatcher dispatcher = null;
    private static final long serialVersionUID = -2008667933225051907L;
    private boolean shutdown = false;

    public AsyncWeibo(String id, String password) {
        super(id, password);
    }

    public AsyncWeibo(String id, String password, String baseURL) {
        super(id, password, baseURL);
    }

    public void searchAcync(Query query, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(27, listener, new Object[]{query}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.searched(AsyncWeibo.this.search((Query) args[0]));
            }
        });
    }

    public void getTrendsAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(44, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotTrends(AsyncWeibo.this.getTrends());
            }
        });
    }

    public void getCurrentTrendsAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(45, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotCurrentTrends(AsyncWeibo.this.getCurrentTrends());
            }
        });
    }

    public void getCurrentTrendsAsync(boolean excludeHashTags, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(45, listener, new Object[]{Boolean.valueOf(excludeHashTags)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotCurrentTrends(AsyncWeibo.this.getCurrentTrends(((Boolean) args[0]).booleanValue()));
            }
        });
    }

    public void getDailyTrendsAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(46, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotDailyTrends(AsyncWeibo.this.getDailyTrends());
            }
        });
    }

    public void getDailyTrendsAsync(Date date, boolean excludeHashTags, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(46, listener, new Object[]{date, Boolean.valueOf(excludeHashTags)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotDailyTrends(AsyncWeibo.this.getDailyTrends((Date) args[0], ((Boolean) args[1]).booleanValue()));
            }
        });
    }

    public void getWeeklyTrendsAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(47, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotWeeklyTrends(AsyncWeibo.this.getWeeklyTrends());
            }
        });
    }

    public void getWeeklyTrendsAsync(Date date, boolean excludeHashTags, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(47, listener, new Object[]{date, Boolean.valueOf(excludeHashTags)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotWeeklyTrends(AsyncWeibo.this.getWeeklyTrends((Date) args[0], ((Boolean) args[1]).booleanValue()));
            }
        });
    }

    public void getPublicTimelineAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(0, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotPublicTimeline(AsyncWeibo.this.getPublicTimeline());
            }
        });
    }

    public void getPublicTimelineAsync(int sinceID, WeiboListener listener) {
        getPublicTimelineAsync((long) sinceID, listener);
    }

    public void getPublicTimelineAsync(long sinceID, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(0, listener, new Long[]{Long.valueOf(sinceID)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotPublicTimeline(AsyncWeibo.this.getPublicTimeline(((Long) args[0]).longValue()));
            }
        });
    }

    public void getHomeTimelineAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(51, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotHomeTimeline(AsyncWeibo.this.getHomeTimeline());
            }
        });
    }

    public void getHomeTimelineAsync(Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(51, listener, new Object[]{paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotHomeTimeline(AsyncWeibo.this.getHomeTimeline((Paging) args[0]));
            }
        });
    }

    public void getFriendsTimelineAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(1, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFriendsTimeline(AsyncWeibo.this.getFriendsTimeline());
            }
        });
    }

    public void getFriendsTimelineAsync(Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(1, listener, new Object[]{paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFriendsTimeline(AsyncWeibo.this.getFriendsTimeline((Paging) args[0]));
            }
        });
    }

    public void getFriendsTimelineByPageAsync(int page, WeiboListener listener) {
        getFriendsTimelineAsync(new Paging(page), listener);
    }

    public void getFriendsTimelineAsync(int page, WeiboListener listener) {
        getFriendsTimelineAsync(new Paging(page), listener);
    }

    public void getFriendsTimelineAsync(long sinceId, int page, WeiboListener listener) {
        getFriendsTimelineAsync(new Paging(page, sinceId), listener);
    }

    public void getFriendsTimelineAsync(String id, WeiboListener listener) {
        throw new IllegalStateException("The Weibo API is not supporting this method anymore");
    }

    public void getFriendsTimelineAsync(String id, Paging paging, WeiboListener listener) {
        throw new IllegalStateException("The Weibo API is not supporting this method anymore");
    }

    public void getFriendsTimelineByPageAsync(String id, int page, WeiboListener listener) {
        throw new IllegalStateException("The Weibo API is not supporting this method anymore");
    }

    public void getFriendsTimelineAsync(String id, int page, WeiboListener listener) {
        throw new IllegalStateException("The Weibo API is not supporting this method anymore");
    }

    public void getFriendsTimelineAsync(long sinceId, String id, int page, WeiboListener listener) {
        throw new IllegalStateException("The Weibo API is not supporting this method anymore");
    }

    public void getFriendsTimelineAsync(Date since, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(1, listener, new Object[]{since}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFriendsTimeline(AsyncWeibo.this.getFriendsTimeline((Date) args[0]));
            }
        });
    }

    public void getFriendsTimelineAsync(long sinceId, WeiboListener listener) {
        getFriendsTimelineAsync(new Paging(sinceId), listener);
    }

    public void getFriendsTimelineAsync(String id, Date since, WeiboListener listener) {
        throw new IllegalStateException("The Weibo API is not supporting this method anymore");
    }

    public void getFriendsTimelineAsync(String id, long sinceId, WeiboListener listener) {
        throw new IllegalStateException("The Weibo API is not supporting this method anymore");
    }

    public void getUserTimelineAsync(String id, int count, Date since, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(2, listener, new Object[]{id, Integer.valueOf(count), since}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotUserTimeline(AsyncWeibo.this.getUserTimeline((String) args[0], ((Integer) args[1]).intValue(), (Date) args[2]));
            }
        });
    }

    public void getUserTimelineAsync(String id, Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(2, listener, new Object[]{id, paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotUserTimeline(AsyncWeibo.this.getUserTimeline((String) args[0], (Paging) args[1]));
            }
        });
    }

    public void getUserTimelineAsync(String id, int page, long sinceId, WeiboListener listener) {
        getUserTimelineAsync(id, new Paging(page, sinceId), listener);
    }

    public void getUserTimelineAsync(String id, Date since, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(2, listener, new Object[]{id, since}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotUserTimeline(AsyncWeibo.this.getUserTimeline((String) args[0], (Date) args[1]));
            }
        });
    }

    public void getUserTimelineAsync(String id, int count, WeiboListener listener) {
        getUserTimelineAsync(id, new Paging().count(count), listener);
    }

    public void getUserTimelineAsync(int count, Date since, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(2, listener, new Object[]{Integer.valueOf(count), since}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotUserTimeline(AsyncWeibo.this.getUserTimeline(((Integer) args[0]).intValue(), (Date) args[1]));
            }
        });
    }

    public void getUserTimelineAsync(Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(2, listener, new Object[]{paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotUserTimeline(AsyncWeibo.this.getUserTimeline((Paging) args[0]));
            }
        });
    }

    public void getUserTimelineAsync(int count, long sinceId, WeiboListener listener) {
        getUserTimelineAsync(new Paging(sinceId).count(count), listener);
    }

    public void getUserTimelineAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(2, listener, new Object[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotUserTimeline(AsyncWeibo.this.getUserTimeline((String) args[0]));
            }
        });
    }

    public void getUserTimelineAsync(String id, long sinceId, WeiboListener listener) {
        getUserTimelineAsync(id, new Paging(sinceId), listener);
    }

    public void getUserTimelineAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(2, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotUserTimeline(AsyncWeibo.this.getUserTimeline());
            }
        });
    }

    public void getUserTimelineAsync(long sinceId, WeiboListener listener) {
        getUserTimelineAsync(new Paging(sinceId), listener);
    }

    public void getRepliesAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(5, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotReplies(AsyncWeibo.this.getReplies());
            }
        });
    }

    public void getMentionsAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(37, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotMentions(AsyncWeibo.this.getMentions());
            }
        });
    }

    public void getMentionsAsync(Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(37, listener, new Object[]{paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotMentions(AsyncWeibo.this.getMentions((Paging) args[0]));
            }
        });
    }

    public void getRetweetedByMeAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(53, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotRetweetedByMe(AsyncWeibo.this.getRetweetedByMe());
            }
        });
    }

    public void getRetweetedByMeAsync(Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(53, listener, new Object[]{paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotRetweetedByMe(AsyncWeibo.this.getRetweetedByMe((Paging) args[0]));
            }
        });
    }

    public void getRetweetedToMeAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(54, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotRetweetedToMe(AsyncWeibo.this.getRetweetedToMe());
            }
        });
    }

    public void getRetweetedToMeAsync(Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(54, listener, new Object[]{paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotRetweetedToMe(AsyncWeibo.this.getRetweetedToMe((Paging) args[0]));
            }
        });
    }

    public void getRetweetsOfMeAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(55, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotRetweetsOfMe(AsyncWeibo.this.getRetweetsOfMe());
            }
        });
    }

    public void getRetweetsOfMeAsync(Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(55, listener, new Object[]{paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotRetweetsOfMe(AsyncWeibo.this.getRetweetsOfMe((Paging) args[0]));
            }
        });
    }

    public void getRepliesAsync(long sinceId, WeiboListener listener) {
        getMentionsAsync(new Paging(sinceId), listener);
    }

    public void getRepliesByPageAsync(int page, WeiboListener listener) {
        getMentionsAsync(new Paging(page), listener);
    }

    public void getRepliesAsync(int page, WeiboListener listener) {
        getMentionsAsync(new Paging(page), listener);
    }

    public void getRepliesAsync(long sinceId, int page, WeiboListener listener) {
        getMentionsAsync(new Paging(page, sinceId), listener);
    }

    public void showAsync(int id, WeiboListener listener) {
        showAsync((long) id, listener);
    }

    public void showAsync(long id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(3, listener, new Object[]{Long.valueOf(id)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotShow(AsyncWeibo.this.show(((Long) args[0]).longValue()));
            }
        });
    }

    public void showStatusAsync(long id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(38, listener, new Object[]{Long.valueOf(id)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotShowStatus(AsyncWeibo.this.showStatus(((Long) args[0]).longValue()));
            }
        });
    }

    public void updateAsync(String status, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(4, listener, new String[]{status}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.updated(AsyncWeibo.this.update((String) args[0]));
            }
        });
    }

    public void updateAsync(String status) {
        getDispatcher().invokeLater(new AsyncTask(4, new WeiboAdapter(), new String[]{status}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.updated(AsyncWeibo.this.update((String) args[0]));
            }
        });
    }

    public void updateStatusAsync(String status, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(39, listener, new String[]{status}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.updatedStatus(AsyncWeibo.this.updateStatus((String) args[0]));
            }
        });
    }

    public void updateStatusAsync(String status) {
        getDispatcher().invokeLater(new AsyncTask(39, new WeiboAdapter(), new String[]{status}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.updatedStatus(AsyncWeibo.this.updateStatus((String) args[0]));
            }
        });
    }

    public void updateAsync(String status, long inReplyToStatusId, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(4, listener, new Object[]{status, Long.valueOf(inReplyToStatusId)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.updated(AsyncWeibo.this.update((String) args[0], ((Long) args[1]).longValue()));
            }
        });
    }

    public void updateAsync(String status, long inReplyToStatusId) {
        getDispatcher().invokeLater(new AsyncTask(4, new WeiboAdapter(), new Object[]{status, Long.valueOf(inReplyToStatusId)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.updated(AsyncWeibo.this.update((String) args[0], ((Long) args[1]).longValue()));
            }
        });
    }

    public void updateStatusAsync(String status, long inReplyToStatusId, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(39, listener, new Object[]{status, Long.valueOf(inReplyToStatusId)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.updatedStatus(AsyncWeibo.this.updateStatus((String) args[0], ((Long) args[1]).longValue()));
            }
        });
    }

    public void updateStatusAsync(String status, long inReplyToStatusId) {
        getDispatcher().invokeLater(new AsyncTask(39, new WeiboAdapter(), new Object[]{status, Long.valueOf(inReplyToStatusId)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.updatedStatus(AsyncWeibo.this.updateStatus((String) args[0], ((Long) args[1]).longValue()));
            }
        });
    }

    public void destoryStatusAsync(int statusId) {
        destroyStatusAsync((long) statusId);
    }

    public void destroyStatusAsync(int statusId) {
        destroyStatusAsync((long) statusId);
    }

    public void destroyStatusAsync(long statusId) {
        getDispatcher().invokeLater(new AsyncTask(26, new WeiboAdapter(), new Long[]{Long.valueOf(statusId)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.destroyedStatus(AsyncWeibo.this.destroyStatus(((Long) args[0]).longValue()));
            }
        });
    }

    public void destoryStatusAsync(int statusId, WeiboListener listener) {
        destroyStatusAsync((long) statusId, listener);
    }

    public void destroyStatusAsync(int statusId, WeiboListener listener) {
        destroyStatusAsync((long) statusId, listener);
    }

    public void destroyStatusAsync(long statusId, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(26, listener, new Long[]{Long.valueOf(statusId)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.destroyedStatus(AsyncWeibo.this.destroyStatus(((Long) args[0]).longValue()));
            }
        });
    }

    public void retweetStatusAsync(long statusId, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(52, listener, new Long[]{Long.valueOf(statusId)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.retweetedStatus(AsyncWeibo.this.retweetStatus(((Long) args[0]).longValue()));
            }
        });
    }

    public void retweetStatusAsync(long statusId) {
        retweetStatusAsync(statusId, new WeiboAdapter());
    }

    public void getUserDetailAsync(String id, WeiboListener listener) {
        showUserAsync(id, listener);
    }

    public void showUserAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(9, listener, new Object[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotUserDetail(AsyncWeibo.this.showUser((String) args[0]));
            }
        });
    }

    public void getFriendsAsync(WeiboListener listener) {
        getFriendsStatusesAsync(listener);
    }

    public void getFriendsStatusesAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(6, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFriends(AsyncWeibo.this.getFriendsStatuses());
            }
        });
    }

    public void getFriendsAsync(Paging paging, WeiboListener listener) {
        getFriendsStatusesAsync(paging, listener);
    }

    public void getFriendsStatusesAsync(Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(6, listener, new Object[]{paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFriends(AsyncWeibo.this.getFriendsStatuses((Paging) args[0]));
            }
        });
    }

    public void getFriendsAsync(int page, WeiboListener listener) {
        getFriendsStatusesAsync(new Paging(page), listener);
    }

    public void getFriendsAsync(String id, WeiboListener listener) {
        getFriendsStatusesAsync(id, listener);
    }

    public void getFriendsStatusesAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(6, listener, new Object[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFriends(AsyncWeibo.this.getFriendsStatuses((String) args[0]));
            }
        });
    }

    public void getFriendsAsync(String id, Paging paging, WeiboListener listener) {
        getFriendsStatusesAsync(id, paging, listener);
    }

    public void getFriendsStatusesAsync(String id, Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(6, listener, new Object[]{id, paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFriends(AsyncWeibo.this.getFriendsStatuses((String) args[0], (Paging) args[1]));
            }
        });
    }

    public void getFriendsAsync(String id, int page, WeiboListener listener) {
        getFriendsStatusesAsync(id, new Paging(page), listener);
    }

    public void getFollowersAsync(WeiboListener listener) {
        getFollowersStatusesAsync(listener);
    }

    public void getFollowersStatusesAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(7, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFollowers(AsyncWeibo.this.getFollowers());
            }
        });
    }

    public void getFollowersAsync(Paging paging, WeiboListener listener) {
        getFollowersStatusesAsync(paging, listener);
    }

    public void getFollowersStatusesAsync(Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(7, listener, new Object[]{paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFollowers(AsyncWeibo.this.getFollowersStatuses((Paging) args[0]));
            }
        });
    }

    public void getFollowersAsync(int page, WeiboListener listener) {
        getFollowersStatusesAsync(new Paging(page), listener);
    }

    public void getFollowersAsync(String id, WeiboListener listener) {
        getFollowersStatusesAsync(id, listener);
    }

    public void getFollowersStatusesAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(7, listener, new Object[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFollowers(AsyncWeibo.this.getFollowersStatuses((String) args[0]));
            }
        });
    }

    public void getFollowersAsync(String id, Paging paging, WeiboListener listener) {
        getFollowersStatusesAsync(id, paging, listener);
    }

    public void getFollowersStatusesAsync(String id, Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(7, listener, new Object[]{id, paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFollowers(AsyncWeibo.this.getFollowersStatuses((String) args[0], (Paging) args[1]));
            }
        });
    }

    public void getFollowersAsync(String id, int page, WeiboListener listener) {
        getFollowersStatusesAsync(id, new Paging(page), listener);
    }

    public void getFeaturedAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(8, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFeatured(AsyncWeibo.this.getFeatured());
            }
        });
    }

    public void getDirectMessagesAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(10, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotDirectMessages(AsyncWeibo.this.getDirectMessages());
            }
        });
    }

    public void getDirectMessagesAsync(Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(10, listener, new Object[]{paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotDirectMessages(AsyncWeibo.this.getDirectMessages((Paging) args[0]));
            }
        });
    }

    public void getDirectMessagesByPageAsync(int page, WeiboListener listener) {
        getDirectMessagesAsync(new Paging(page), listener);
    }

    public void getDirectMessagesByPageAsync(int page, int sinceId, WeiboListener listener) {
        getDirectMessagesAsync(new Paging(page, (long) sinceId), listener);
    }

    public void getDirectMessagesAsync(int sinceId, WeiboListener listener) {
        getDirectMessagesAsync(new Paging((long) sinceId), listener);
    }

    public void getDirectMessagesAsync(Date since, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(10, listener, new Object[]{since}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotDirectMessages(AsyncWeibo.this.getDirectMessages((Date) args[0]));
            }
        });
    }

    public void getSentDirectMessagesAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(10, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotSentDirectMessages(AsyncWeibo.this.getSentDirectMessages());
            }
        });
    }

    public void getSentDirectMessagesAsync(Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(10, listener, new Object[]{paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotSentDirectMessages(AsyncWeibo.this.getSentDirectMessages((Paging) args[0]));
            }
        });
    }

    public void getSentDirectMessagesAsync(Date since, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(10, listener, new Object[]{since}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotSentDirectMessages(AsyncWeibo.this.getSentDirectMessages((Date) args[0]));
            }
        });
    }

    public void getSentDirectMessagesAsync(int sinceId, WeiboListener listener) {
        getSentDirectMessagesAsync(new Paging((long) sinceId), listener);
    }

    public void getSentDirectMessagesAsync(int page, int sinceId, WeiboListener listener) {
        getSentDirectMessagesAsync(new Paging(page, (long) sinceId), listener);
    }

    public void sendDirectMessageAsync(String id, String text, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(11, listener, new String[]{id, text}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.sentDirectMessage(AsyncWeibo.this.sendDirectMessage((String) args[0], (String) args[1]));
            }
        });
    }

    public void sendDirectMessageAsync(String id, String text) {
        getDispatcher().invokeLater(new AsyncTask(11, new WeiboAdapter(), new String[]{id, text}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.sentDirectMessage(AsyncWeibo.this.sendDirectMessage((String) args[0], (String) args[1]));
            }
        });
    }

    public void deleteDirectMessageAsync(int id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(40, listener, new Object[]{Integer.valueOf(id)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.deletedDirectMessage(AsyncWeibo.this.deleteDirectMessage(((Integer) args[0]).intValue()));
            }
        });
    }

    public void destroyDirectMessageAsync(int id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(40, listener, new Object[]{Integer.valueOf(id)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.destroyedDirectMessage(AsyncWeibo.this.destroyDirectMessage(((Integer) args[0]).intValue()));
            }
        });
    }

    public void destroyDirectMessageAsync(int id) {
        getDispatcher().invokeLater(new AsyncTask(40, new WeiboAdapter(), new Object[]{Integer.valueOf(id)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.destroyedDirectMessage(AsyncWeibo.this.destroyDirectMessage(((Integer) args[0]).intValue()));
            }
        });
    }

    public void createAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(12, listener, new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.created(AsyncWeibo.this.create((String) args[0]));
            }
        });
    }

    public void createFriendshipAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(32, listener, new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.createdFriendship(AsyncWeibo.this.createFriendship((String) args[0]));
            }
        });
    }

    public void createFriendshipAsync(String id, boolean follow, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(32, listener, new Object[]{id, Boolean.valueOf(follow)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.createdFriendship(AsyncWeibo.this.createFriendship((String) args[0], ((Boolean) args[1]).booleanValue()));
            }
        });
    }

    public void createAsync(String id) {
        getDispatcher().invokeLater(new AsyncTask(12, new WeiboAdapter(), new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.created(AsyncWeibo.this.create((String) args[0]));
            }
        });
    }

    public void createFriendshipAsync(String id) {
        createFriendshipAsync(id, new WeiboAdapter());
    }

    public void destroyAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(13, listener, new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.destroyed(AsyncWeibo.this.destroy((String) args[0]));
            }
        });
    }

    public void destroyAsync(String id) {
        getDispatcher().invokeLater(new AsyncTask(13, new WeiboAdapter(), new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.destroyed(AsyncWeibo.this.destroy((String) args[0]));
            }
        });
    }

    public void destroyFriendshipAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(33, listener, new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.destroyedFriendship(AsyncWeibo.this.destroyFriendship((String) args[0]));
            }
        });
    }

    public void destroyFriendshipAsync(String id) {
        destroyFriendshipAsync(id, new WeiboAdapter());
    }

    public void existsAsync(String userA, String userB, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(28, listener, new String[]{userA, userB}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotExists(AsyncWeibo.this.exists((String) args[0], (String) args[1]));
            }
        });
    }

    public void existsFriendshipAsync(String userA, String userB, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(34, listener, new String[]{userA, userB}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotExistsFriendship(AsyncWeibo.this.existsFriendship((String) args[0], (String) args[1]));
            }
        });
    }

    public void getFriendsIDsAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(29, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFriendsIDs(AsyncWeibo.this.getFriendsIDs());
            }
        });
    }

    public void getFriendsIDsAsync(Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(29, listener, new Object[]{paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFriendsIDs(AsyncWeibo.this.getFriendsIDs((Paging) args[0]));
            }
        });
    }

    public void getFriendsIDsAsync(long cursor, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(29, listener, new Object[]{Long.valueOf(cursor)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFriendsIDs(AsyncWeibo.this.getFriendsIDs(((Long) args[0]).longValue()));
            }
        });
    }

    public void getFriendsIDsAsync(int userId, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(29, listener, new Integer[]{Integer.valueOf(userId)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFriendsIDs(AsyncWeibo.this.getFriendsIDs(((Integer) args[0]).intValue()));
            }
        });
    }

    public void getFriendsIDsAsync(int userId, Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(29, listener, new Object[]{Integer.valueOf(userId), paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFriendsIDs(AsyncWeibo.this.getFriendsIDs(((Integer) args[0]).intValue(), (Paging) args[1]));
            }
        });
    }

    public void getFriendsIDsAsync(int userId, long cursor, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(29, listener, new Object[]{Integer.valueOf(userId), Long.valueOf(cursor)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFriendsIDs(AsyncWeibo.this.getFriendsIDs(((Integer) args[0]).intValue(), ((Long) args[1]).longValue()));
            }
        });
    }

    public void getFriendsIDsAsync(String screenName, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(29, listener, new String[]{screenName}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFriendsIDs(AsyncWeibo.this.getFriendsIDs((String) args[0]));
            }
        });
    }

    public void getFriendsIDsAsync(String screenName, Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(29, listener, new Object[]{screenName, paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFriendsIDs(AsyncWeibo.this.getFriendsIDs((String) args[0], (Paging) args[1]));
            }
        });
    }

    public void getFriendsIDsAsync(String screenName, long cursor, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(29, listener, new Object[]{screenName, Long.valueOf(cursor)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFriendsIDs(AsyncWeibo.this.getFriendsIDs((String) args[0], ((Long) args[1]).longValue()));
            }
        });
    }

    public void getFollowersIDsAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(30, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFollowersIDs(AsyncWeibo.this.getFollowersIDs());
            }
        });
    }

    public void getFollowersIDsAsync(Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(30, listener, new Object[]{paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFollowersIDs(AsyncWeibo.this.getFollowersIDs((Paging) args[0]));
            }
        });
    }

    public void getFollowersIDsAsync(long cursor, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(30, listener, new Object[]{Long.valueOf(cursor)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFollowersIDs(AsyncWeibo.this.getFollowersIDs(((Long) args[0]).longValue()));
            }
        });
    }

    public void getFollowersIDsAsync(int userId, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(30, listener, new Integer[]{Integer.valueOf(userId)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFollowersIDs(AsyncWeibo.this.getFollowersIDs(((Integer) args[0]).intValue()));
            }
        });
    }

    public void getFollowersIDsAsync(int userId, Paging paging, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(30, listener, new Object[]{Integer.valueOf(userId), paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFollowersIDs(AsyncWeibo.this.getFollowersIDs(((Integer) args[0]).intValue(), (Paging) args[1]));
            }
        });
    }

    public void getFollowersIDsAsync(int userId, long cursor, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(30, listener, new Object[]{Integer.valueOf(userId), Long.valueOf(cursor)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFollowersIDs(AsyncWeibo.this.getFollowersIDs(((Integer) args[0]).intValue(), ((Long) args[1]).longValue()));
            }
        });
    }

    public void getFollowersIDsAsync(String screenName, WeiboListener listener) throws WeiboException {
        getDispatcher().invokeLater(new AsyncTask(30, listener, new String[]{screenName}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFollowersIDs(AsyncWeibo.this.getFollowersIDs((String) args[0]));
            }
        });
    }

    public void getFollowersIDsAsync(String screenName, Paging paging, WeiboListener listener) throws WeiboException {
        getDispatcher().invokeLater(new AsyncTask(30, listener, new Object[]{screenName, paging}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFollowersIDs(AsyncWeibo.this.getFollowersIDs((String) args[0], (Paging) args[1]));
            }
        });
    }

    public void getFollowersIDsAsync(String screenName, long cursor, WeiboListener listener) throws WeiboException {
        getDispatcher().invokeLater(new AsyncTask(30, listener, new Object[]{screenName, Long.valueOf(cursor)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFollowersIDs(AsyncWeibo.this.getFollowersIDs((String) args[0], ((Long) args[1]).longValue()));
            }
        });
    }

    public void updateLocationAsync(String location, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(20, listener, new Object[]{location}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.updatedLocation(AsyncWeibo.this.updateLocation((String) args[0]));
            }
        });
    }

    public void updateProfileAsync(String name, String email, String url, String location, String description, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(41, listener, new String[]{name, email, url, location, description}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.updatedProfile(AsyncWeibo.this.updateProfile((String) args[0], (String) args[1], (String) args[2], (String) args[3], (String) args[4]));
            }
        });
    }

    public void updateProfileAsync(String name, String email, String url, String location, String description) {
        updateProfileAsync(name, email, url, location, description, new WeiboAdapter());
    }

    public void rateLimitStatusAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(28, listener, new Object[0]) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotRateLimitStatus(AsyncWeibo.this.rateLimitStatus());
            }
        });
    }

    public void updateDeliverlyDeviceAsync(Weibo.Device device, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(20, listener, new Object[]{device}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.updatedDeliverlyDevice(AsyncWeibo.this.updateDeliverlyDevice((Weibo.Device) args[0]));
            }
        });
    }

    public void updateProfileColorsAsync(String profileBackgroundColor, String profileTextColor, String profileLinkColor, String profileSidebarFillColor, String profileSidebarBorderColor, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(31, listener, new Object[]{profileBackgroundColor, profileTextColor, profileLinkColor, profileSidebarFillColor, profileSidebarBorderColor}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.updatedProfileColors(AsyncWeibo.this.updateProfileColors((String) args[0], (String) args[1], (String) args[2], (String) args[3], (String) args[4]));
            }
        });
    }

    public void updateProfileColorsAsync(String profileBackgroundColor, String profileTextColor, String profileLinkColor, String profileSidebarFillColor, String profileSidebarBorderColor) {
        updateProfileColorsAsync(profileBackgroundColor, profileTextColor, profileLinkColor, profileSidebarFillColor, profileSidebarBorderColor, new WeiboAdapter());
    }

    public void favoritesAsync(WeiboListener listener) {
        getFavoritesAsync(listener);
    }

    public void getFavoritesAsync(WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(17, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFavorites(AsyncWeibo.this.getFavorites());
            }
        });
    }

    public void favoritesAsync(int page, WeiboListener listener) {
        getFavoritesAsync(page, listener);
    }

    public void getFavoritesAsync(int page, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(17, listener, new Object[]{Integer.valueOf(page)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFavorites(AsyncWeibo.this.getFavorites(((Integer) args[0]).intValue()));
            }
        });
    }

    public void favoritesAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(17, listener, new Object[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFavorites(AsyncWeibo.this.favorites((String) args[0]));
            }
        });
    }

    public void getFavoritesAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(17, listener, new Object[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFavorites(AsyncWeibo.this.getFavorites((String) args[0]));
            }
        });
    }

    public void favoritesAsync(String id, int page, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(17, listener, new Object[]{id, Integer.valueOf(page)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFavorites(AsyncWeibo.this.favorites((String) args[0], ((Integer) args[1]).intValue()));
            }
        });
    }

    public void getFavoritesAsync(String id, int page, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(17, listener, new Object[]{id, Integer.valueOf(page)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotFavorites(AsyncWeibo.this.getFavorites((String) args[0], ((Integer) args[1]).intValue()));
            }
        });
    }

    public void createFavoriteAsync(int id, WeiboListener listener) {
        createFavoriteAsync((long) id, listener);
    }

    public void createFavoriteAsync(long id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(17, listener, new Object[]{Long.valueOf(id)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.createdFavorite(AsyncWeibo.this.createFavorite(((Long) args[0]).longValue()));
            }
        });
    }

    public void createFavoriteAsync(int id) {
        createFavoriteAsync((long) id);
    }

    public void createFavoriteAsync(long id) {
        getDispatcher().invokeLater(new AsyncTask(17, new WeiboAdapter(), new Object[]{Long.valueOf(id)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.createdFavorite(AsyncWeibo.this.createFavorite(((Long) args[0]).longValue()));
            }
        });
    }

    public void destroyFavoriteAsync(int id, WeiboListener listener) {
        destroyFavoriteAsync((long) id, listener);
    }

    public void destroyFavoriteAsync(long id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(17, listener, new Object[]{Long.valueOf(id)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.destroyedFavorite(AsyncWeibo.this.destroyFavorite(((Long) args[0]).longValue()));
            }
        });
    }

    public void destroyFavoriteAsync(int id) {
        destroyFavoriteAsync((long) id);
    }

    public void destroyFavoriteAsync(long id) {
        getDispatcher().invokeLater(new AsyncTask(17, new WeiboAdapter(), new Object[]{Long.valueOf(id)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.destroyedFavorite(AsyncWeibo.this.destroyFavorite(((Long) args[0]).longValue()));
            }
        });
    }

    public void followAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(14, listener, new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.followed(AsyncWeibo.this.follow((String) args[0]));
            }
        });
    }

    public void enableNotificationAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(35, listener, new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.enabledNotification(AsyncWeibo.this.enableNotification((String) args[0]));
            }
        });
    }

    public void followAsync(String id) {
        getDispatcher().invokeLater(new AsyncTask(14, new WeiboAdapter(), new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.followed(AsyncWeibo.this.follow((String) args[0]));
            }
        });
    }

    public void enableNotificationAsync(String id) {
        enableNotificationAsync(id, new WeiboAdapter());
    }

    public void leaveAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(15, listener, new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.left(AsyncWeibo.this.leave((String) args[0]));
            }
        });
    }

    public void disableNotificationAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(36, listener, new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.disabledNotification(AsyncWeibo.this.disableNotification((String) args[0]));
            }
        });
    }

    public void leaveAsync(String id) {
        getDispatcher().invokeLater(new AsyncTask(15, new WeiboAdapter(), new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.left(AsyncWeibo.this.leave((String) args[0]));
            }
        });
    }

    public void disableNotificationAsync(String id) {
        disableNotificationAsync(id, new WeiboAdapter());
    }

    public void blockAsync(String id) {
        getDispatcher().invokeLater(new AsyncTask(22, new WeiboAdapter(), new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.blocked(AsyncWeibo.this.block((String) args[0]));
            }
        });
    }

    public void createBlockAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(43, listener, new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.createdBlock(AsyncWeibo.this.createBlock((String) args[0]));
            }
        });
    }

    public void createBlockAsync(String id) {
        createBlockAsync(id, new WeiboAdapter());
    }

    public void unblockAsync(String id) {
        getDispatcher().invokeLater(new AsyncTask(23, new WeiboAdapter(), new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.unblocked(AsyncWeibo.this.unblock((String) args[0]));
            }
        });
    }

    public void destroyBlockAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(42, listener, new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.destroyedBlock(AsyncWeibo.this.destroyBlock((String) args[0]));
            }
        });
    }

    public void destroyBlockAsync(String id) {
        destroyBlockAsync(id, new WeiboAdapter());
    }

    public void existsBlockAsync(String id, WeiboListener listener) {
        getDispatcher().invokeLater(new AsyncTask(EXISTS_BLOCK, listener, new String[]{id}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotExistsBlock(AsyncWeibo.this.existsBlock((String) args[0]));
            }
        });
    }

    public void getBlockingUsersAsync(WeiboListener listener) throws WeiboException {
        getDispatcher().invokeLater(new AsyncTask(GET_BLOCKING_USERS, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotBlockingUsers(AsyncWeibo.this.getBlockingUsers());
            }
        });
    }

    public void getBlockingUsersAsync(int page, WeiboListener listener) throws WeiboException {
        getDispatcher().invokeLater(new AsyncTask(GET_BLOCKING_USERS, listener, new Integer[]{Integer.valueOf(page)}) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotBlockingUsers(AsyncWeibo.this.getBlockingUsers(((Integer) args[0]).intValue()));
            }
        });
    }

    public void getBlockingUsersIDsAsync(WeiboListener listener) throws WeiboException {
        getDispatcher().invokeLater(new AsyncTask(GET_BLOCKING_USERS_IDS, listener, null) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.gotBlockingUsersIDs(AsyncWeibo.this.getBlockingUsersIDs());
            }
        });
    }

    public void testAsync() {
        getDispatcher().invokeLater(new AsyncTask(24, new WeiboAdapter(), new Object[0]) {
            public void invoke(WeiboListener listener, Object[] args) throws WeiboException {
                listener.tested(AsyncWeibo.this.test());
            }
        });
    }

    public void shutdown() {
        synchronized (AsyncWeibo.class) {
            this.shutdown = true;
            if (1 != 0) {
                throw new IllegalStateException("Already shut down");
            }
            getDispatcher().shutdown();
            dispatcher = null;
            this.shutdown = true;
        }
    }

    private Dispatcher getDispatcher() {
        if (true == this.shutdown) {
            throw new IllegalStateException("Already shut down");
        }
        if (dispatcher == null) {
            dispatcher = new Dispatcher("Weibo4J Async Dispatcher", Configuration.getNumberOfAsyncThreads());
        }
        return dispatcher;
    }

    public void getDowntimeScheduleAsync() {
        throw new RuntimeException("this method is not supported by the Weibo API anymore", new NoSuchMethodException("this method is not supported by the Weibo API anymore"));
    }

    public void getAuthenticatedUserAsync(WeiboListener listener) {
        if (getUserId() == null) {
            throw new IllegalStateException("User Id not specified.");
        }
        getUserDetailAsync(getUserId(), listener);
    }

    abstract class AsyncTask implements Runnable {
        Object[] args;
        WeiboListener listener;
        int method;

        /* access modifiers changed from: package-private */
        public abstract void invoke(WeiboListener weiboListener, Object[] objArr) throws WeiboException;

        AsyncTask(int method2, WeiboListener listener2, Object[] args2) {
            this.method = method2;
            this.listener = listener2;
            this.args = args2;
        }

        public void run() {
            try {
                invoke(this.listener, this.args);
            } catch (WeiboException e) {
                WeiboException te = e;
                if (this.listener != null) {
                    this.listener.onException(te, this.method);
                }
            }
        }
    }
}
