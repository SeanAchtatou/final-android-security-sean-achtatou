package weibo4j;

import com.kandian.common.HtmlUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.AccessControlException;
import java.util.Properties;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class Configuration {
    private static boolean DALVIK;
    private static Properties defaultProperty;

    static {
        init();
    }

    static void init() {
        defaultProperty = new Properties();
        defaultProperty.setProperty("weibo4j.debug", HttpState.PREEMPTIVE_DEFAULT);
        defaultProperty.setProperty("weibo4j.source", Weibo.CONSUMER_KEY);
        defaultProperty.setProperty("weibo4j.clientURL", "http://open.t.sina.com.cn/-{weibo4j.clientVersion}.xml");
        defaultProperty.setProperty("weibo4j.http.userAgent", "weibo4j http://open.t.sina.com.cn/ /{weibo4j.clientVersion}");
        defaultProperty.setProperty("weibo4j.http.useSSL", HttpState.PREEMPTIVE_DEFAULT);
        defaultProperty.setProperty("weibo4j.http.proxyHost.fallback", "http.proxyHost");
        defaultProperty.setProperty("weibo4j.http.proxyPort.fallback", "http.proxyPort");
        defaultProperty.setProperty("weibo4j.http.connectionTimeout", "20000");
        defaultProperty.setProperty("weibo4j.http.readTimeout", "120000");
        defaultProperty.setProperty("weibo4j.http.retryCount", "3");
        defaultProperty.setProperty("weibo4j.http.retryIntervalSecs", "10");
        defaultProperty.setProperty("weibo4j.async.numThreads", HtmlUtil.TYPE_PLAY);
        defaultProperty.setProperty("weibo4j.clientVersion", Version.getVersion());
        try {
            Class.forName("dalvik.system.VMRuntime");
            defaultProperty.setProperty("weibo4j.dalvik", "true");
        } catch (ClassNotFoundException e) {
            defaultProperty.setProperty("weibo4j.dalvik", HttpState.PREEMPTIVE_DEFAULT);
        }
        DALVIK = getBoolean("weibo4j.dalvik");
        if (!loadProperties(defaultProperty, "." + File.separatorChar + "weibo4j.properties") && !loadProperties(defaultProperty, Configuration.class.getResourceAsStream("/WEB-INF/" + "weibo4j.properties")) && !loadProperties(defaultProperty, Configuration.class.getResourceAsStream(CookieSpec.PATH_DELIM + "weibo4j.properties"))) {
        }
    }

    private static boolean loadProperties(Properties props, String path) {
        try {
            File file = new File(path);
            if (file.exists() && file.isFile()) {
                props.load(new FileInputStream(file));
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    private static boolean loadProperties(Properties props, InputStream is) {
        try {
            props.load(is);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isDalvik() {
        return DALVIK;
    }

    public static boolean useSSL() {
        return getBoolean("weibo4j.http.useSSL");
    }

    public static String getScheme() {
        return useSSL() ? "https://" : "http://";
    }

    public static String getCilentVersion() {
        return getProperty("weibo4j.clientVersion");
    }

    public static String getCilentVersion(String clientVersion) {
        return getProperty("weibo4j.clientVersion", clientVersion);
    }

    public static String getSource() {
        return getProperty("weibo4j.source");
    }

    public static String getSource(String source) {
        return getProperty("weibo4j.source", source);
    }

    public static String getProxyHost() {
        return getProperty("weibo4j.http.proxyHost");
    }

    public static String getProxyHost(String proxyHost) {
        return getProperty("weibo4j.http.proxyHost", proxyHost);
    }

    public static String getProxyUser() {
        return getProperty("weibo4j.http.proxyUser");
    }

    public static String getProxyUser(String user) {
        return getProperty("weibo4j.http.proxyUser", user);
    }

    public static String getClientURL() {
        return getProperty("weibo4j.clientURL");
    }

    public static String getClientURL(String clientURL) {
        return getProperty("weibo4j.clientURL", clientURL);
    }

    public static String getProxyPassword() {
        return getProperty("weibo4j.http.proxyPassword");
    }

    public static String getProxyPassword(String password) {
        return getProperty("weibo4j.http.proxyPassword", password);
    }

    public static int getProxyPort() {
        return getIntProperty("weibo4j.http.proxyPort");
    }

    public static int getProxyPort(int port) {
        return getIntProperty("weibo4j.http.proxyPort", port);
    }

    public static int getConnectionTimeout() {
        return getIntProperty("weibo4j.http.connectionTimeout");
    }

    public static int getConnectionTimeout(int connectionTimeout) {
        return getIntProperty("weibo4j.http.connectionTimeout", connectionTimeout);
    }

    public static int getReadTimeout() {
        return getIntProperty("weibo4j.http.readTimeout");
    }

    public static int getReadTimeout(int readTimeout) {
        return getIntProperty("weibo4j.http.readTimeout", readTimeout);
    }

    public static int getRetryCount() {
        return getIntProperty("weibo4j.http.retryCount");
    }

    public static int getRetryCount(int retryCount) {
        return getIntProperty("weibo4j.http.retryCount", retryCount);
    }

    public static int getRetryIntervalSecs() {
        return getIntProperty("weibo4j.http.retryIntervalSecs");
    }

    public static int getRetryIntervalSecs(int retryIntervalSecs) {
        return getIntProperty("weibo4j.http.retryIntervalSecs", retryIntervalSecs);
    }

    public static String getUser() {
        return getProperty("weibo4j.user");
    }

    public static String getUser(String userId) {
        return getProperty("weibo4j.user", userId);
    }

    public static String getPassword() {
        return getProperty("weibo4j.password");
    }

    public static String getPassword(String password) {
        return getProperty("weibo4j.password", password);
    }

    public static String getUserAgent() {
        return getProperty("weibo4j.http.userAgent");
    }

    public static String getUserAgent(String userAgent) {
        return getProperty("weibo4j.http.userAgent", userAgent);
    }

    public static String getOAuthConsumerKey() {
        return getProperty("weibo4j.oauth.consumerKey");
    }

    public static String getOAuthConsumerKey(String consumerKey) {
        return getProperty("weibo4j.oauth.consumerKey", consumerKey);
    }

    public static String getOAuthConsumerSecret() {
        return getProperty("weibo4j.oauth.consumerSecret");
    }

    public static String getOAuthConsumerSecret(String consumerSecret) {
        return getProperty("weibo4j.oauth.consumerSecret", consumerSecret);
    }

    public static boolean getBoolean(String name) {
        return Boolean.valueOf(getProperty(name)).booleanValue();
    }

    public static int getIntProperty(String name) {
        try {
            return Integer.parseInt(getProperty(name));
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static int getIntProperty(String name, int fallbackValue) {
        try {
            return Integer.parseInt(getProperty(name, String.valueOf(fallbackValue)));
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static long getLongProperty(String name) {
        try {
            return Long.parseLong(getProperty(name));
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static String getProperty(String name) {
        return getProperty(name, null);
    }

    public static String getProperty(String name, String fallbackValue) {
        String value;
        String fallback;
        try {
            value = System.getProperty(name, fallbackValue);
            if (value == null) {
                value = defaultProperty.getProperty(name);
            }
            if (value == null && (fallback = defaultProperty.getProperty(name + ".fallback")) != null) {
                value = System.getProperty(fallback);
            }
        } catch (AccessControlException e) {
            value = fallbackValue;
        }
        return replace(value);
    }

    private static String replace(String value) {
        int closeBrace;
        if (value == null) {
            return value;
        }
        String newValue = value;
        int openBrace = value.indexOf("{", 0);
        if (-1 != openBrace && (closeBrace = value.indexOf("}", openBrace)) > openBrace + 1) {
            String name = value.substring(openBrace + 1, closeBrace);
            if (name.length() > 0) {
                newValue = value.substring(0, openBrace) + getProperty(name) + value.substring(closeBrace + 1);
            }
        }
        if (newValue.equals(value)) {
            return value;
        }
        return replace(newValue);
    }

    public static int getNumberOfAsyncThreads() {
        return getIntProperty("weibo4j.async.numThreads");
    }

    public static boolean getDebug() {
        return getBoolean("weibo4j.debug");
    }
}
