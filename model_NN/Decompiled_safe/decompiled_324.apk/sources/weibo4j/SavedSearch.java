package weibo4j;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import weibo4j.http.Response;
import weibo4j.org.json.JSONArray;
import weibo4j.org.json.JSONException;
import weibo4j.org.json.JSONObject;

public class SavedSearch extends WeiboResponse {
    private static final long serialVersionUID = 3083819860391598212L;
    private Date createdAt;
    private int id;
    private String name;
    private int position;
    private String query;

    SavedSearch(Response res) throws WeiboException {
        super(res);
        init(res.asJSONObject());
    }

    SavedSearch(Response res, JSONObject json) throws WeiboException {
        super(res);
        init(json);
    }

    SavedSearch(JSONObject savedSearch) throws WeiboException {
        init(savedSearch);
    }

    static List<SavedSearch> constructSavedSearches(Response res) throws WeiboException {
        JSONArray json = res.asJSONArray();
        try {
            List<SavedSearch> savedSearches = new ArrayList<>(json.length());
            for (int i = 0; i < json.length(); i++) {
                savedSearches.add(new SavedSearch(res, json.getJSONObject(i)));
            }
            return savedSearches;
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new WeiboException(jsone.getMessage() + ":" + res.asString(), jsone);
        }
    }

    private void init(JSONObject savedSearch) throws WeiboException {
        try {
            this.createdAt = parseDate(savedSearch.getString("created_at"), "EEE MMM dd HH:mm:ss z yyyy");
            this.query = getString("query", savedSearch, true);
            this.position = getInt("position", savedSearch);
            this.name = getString("name", savedSearch, true);
            this.id = getInt("id", savedSearch);
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new WeiboException(jsone.getMessage() + ":" + savedSearch.toString(), jsone);
        }
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public String getQuery() {
        return this.query;
    }

    public int getPosition() {
        return this.position;
    }

    public String getName() {
        return this.name;
    }

    public int getId() {
        return this.id;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SavedSearch)) {
            return false;
        }
        SavedSearch that = (SavedSearch) o;
        if (this.id != that.id) {
            return false;
        }
        if (this.position != that.position) {
            return false;
        }
        if (!this.createdAt.equals(that.createdAt)) {
            return false;
        }
        if (!this.name.equals(that.name)) {
            return false;
        }
        return this.query.equals(that.query);
    }

    public int hashCode() {
        return (((((((this.createdAt.hashCode() * 31) + this.query.hashCode()) * 31) + this.position) * 31) + this.name.hashCode()) * 31) + this.id;
    }

    public String toString() {
        return "SavedSearch{createdAt=" + this.createdAt + ", query='" + this.query + '\'' + ", position=" + this.position + ", name='" + this.name + '\'' + ", id=" + this.id + '}';
    }
}
