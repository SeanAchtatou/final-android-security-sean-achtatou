package weibo4j;

import java.io.Serializable;
import org.w3c.dom.Element;
import weibo4j.http.Response;
import weibo4j.org.json.JSONException;
import weibo4j.org.json.JSONObject;

public class ListUserCount extends WeiboResponse implements Serializable {
    private static final long serialVersionUID = 2638697034012299545L;
    private int listCount;
    private int listedCount;
    private int subscriberCount;

    public ListUserCount(JSONObject json) throws WeiboException, JSONException {
        this.listCount = json.getInt("lists");
        this.subscriberCount = json.getInt("subscriptions");
        this.listedCount = json.getInt("listed");
    }

    public ListUserCount(Response res) throws WeiboException {
        Element elem = res.asDocument().getDocumentElement();
        ensureRootNodeNameIs("count", elem);
        this.listCount = getChildInt("lists", elem);
        this.subscriberCount = getChildInt("subscriptions", elem);
        this.listedCount = getChildInt("listed", elem);
    }

    public int hashCode() {
        return (this.listCount * 100) + (this.subscriberCount * 10) + this.listedCount;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return (obj instanceof ListUserCount) && ((ListUserCount) obj).hashCode() == hashCode();
    }

    public int getListCount() {
        return this.listCount;
    }

    public void setListCount(int listCount2) {
        this.listCount = listCount2;
    }

    public int getSubscriberCount() {
        return this.subscriberCount;
    }

    public void setSubscriberCount(int subscriberCount2) {
        this.subscriberCount = subscriberCount2;
    }

    public int getListedCount() {
        return this.listedCount;
    }

    public void setListedCount(int listedCount2) {
        this.listedCount = listedCount2;
    }

    public String toString() {
        return "ListUserCount{listCount=" + this.listCount + ", subscriberCount=" + this.subscriberCount + ", listedCount=" + this.listedCount + '}';
    }
}
