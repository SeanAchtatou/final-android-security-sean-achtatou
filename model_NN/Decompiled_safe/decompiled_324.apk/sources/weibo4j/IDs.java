package weibo4j;

import java.util.Arrays;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import weibo4j.http.Response;
import weibo4j.org.json.JSONArray;
import weibo4j.org.json.JSONException;
import weibo4j.org.json.JSONObject;

public class IDs extends WeiboResponse {
    private static String[] ROOT_NODE_NAMES = {"id_list", "ids"};
    private static final long serialVersionUID = -6585026560164704953L;
    private int[] ids;
    private long nextCursor;
    private long previousCursor;

    IDs(Response res) throws WeiboException {
        super(res);
        Element elem = res.asDocument().getDocumentElement();
        ensureRootNodeNameIs(ROOT_NODE_NAMES, elem);
        NodeList idlist = elem.getElementsByTagName("id");
        this.ids = new int[idlist.getLength()];
        int i = 0;
        while (i < idlist.getLength()) {
            try {
                this.ids[i] = Integer.parseInt(idlist.item(i).getFirstChild().getNodeValue());
                i++;
            } catch (NumberFormatException e) {
                throw new WeiboException("Weibo API returned malformed response: " + elem, e);
            }
        }
        this.previousCursor = getChildLong("previous_cursor", elem);
        this.nextCursor = getChildLong("next_cursor", elem);
    }

    IDs(Response res, Weibo w) throws WeiboException {
        super(res);
        JSONObject json = res.asJSONObject();
        try {
            this.previousCursor = json.getLong("previous_cursor");
            this.nextCursor = json.getLong("next_cursor");
            if (!json.isNull("ids")) {
                JSONArray jsona = json.getJSONArray("ids");
                int size = jsona.length();
                this.ids = new int[size];
                for (int i = 0; i < size; i++) {
                    this.ids[i] = jsona.getInt(i);
                }
            }
        } catch (JSONException e) {
            throw new WeiboException(e);
        }
    }

    public int[] getIDs() {
        return this.ids;
    }

    public boolean hasPrevious() {
        return 0 != this.previousCursor;
    }

    public long getPreviousCursor() {
        return this.previousCursor;
    }

    public boolean hasNext() {
        return 0 != this.nextCursor;
    }

    public long getNextCursor() {
        return this.nextCursor;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IDs)) {
            return false;
        }
        return Arrays.equals(this.ids, ((IDs) o).ids);
    }

    public int hashCode() {
        if (this.ids != null) {
            return Arrays.hashCode(this.ids);
        }
        return 0;
    }

    public String toString() {
        return "IDs{ids=" + this.ids + ", previousCursor=" + this.previousCursor + ", nextCursor=" + this.nextCursor + '}';
    }
}
