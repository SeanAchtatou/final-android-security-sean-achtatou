package weibo4j;

import com.viewtoo.core.utils.StringUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import weibo4j.http.Response;
import weibo4j.org.json.JSONArray;
import weibo4j.org.json.JSONException;
import weibo4j.org.json.JSONObject;

public class Status extends WeiboResponse implements Serializable {
    private static final long serialVersionUID = 1608000492860584608L;
    private String bmiddle_pic;
    private Date createdAt;
    private long id;
    private String inReplyToScreenName;
    private long inReplyToStatusId;
    private int inReplyToUserId;
    private boolean isFavorited;
    private int isLink = 0;
    private boolean isTruncated;
    private double latitude = -1.0d;
    private double longitude = -1.0d;
    private String original_pic;
    private long parentId = 0;
    private RetweetDetails retweetDetails;
    private Status retweeted_status;
    private String source;
    private String text;
    private String thumbnail_pic;
    private User user = null;

    Status(Response res, Weibo weibo) throws WeiboException {
        super(res);
        init(res, res.asDocument().getDocumentElement(), weibo);
    }

    Status(Response res, Element elem, Weibo weibo) throws WeiboException {
        super(res);
        init(res, elem, weibo);
    }

    Status(Response res) throws WeiboException {
        super(res);
        JSONObject json = res.asJSONObject();
        try {
            this.id = json.getLong("id");
            this.text = json.getString("text");
            this.source = json.getString("source");
            this.createdAt = parseDate(json.getString("created_at"), "EEE MMM dd HH:mm:ss z yyyy");
            this.inReplyToStatusId = getLong("in_reply_to_status_id", json);
            this.inReplyToUserId = getInt("in_reply_to_user_id", json);
            this.isFavorited = getBoolean("favorited", json);
            this.thumbnail_pic = json.getString("thumbnail_pic");
            this.bmiddle_pic = json.getString("bmiddle_pic");
            this.original_pic = json.getString("original_pic");
            if (!json.isNull("user")) {
                this.user = new User(json.getJSONObject("user"));
            }
            this.inReplyToScreenName = json.getString("inReplyToScreenName");
            if (!json.isNull("retweetDetails")) {
                this.retweetDetails = new RetweetDetails(json.getJSONObject("retweetDetails"));
            }
        } catch (JSONException e) {
            JSONException je = e;
            throw new WeiboException(je.getMessage() + ":" + json.toString(), je);
        }
    }

    public Status(JSONObject json) throws WeiboException, JSONException {
        this.id = json.getLong("id");
        this.text = json.getString("text");
        this.source = json.getString("source");
        this.createdAt = parseDate(json.getString("created_at"), "EEE MMM dd HH:mm:ss z yyyy");
        this.isFavorited = getBoolean("favorited", json);
        this.isTruncated = getBoolean("truncated", json);
        this.inReplyToStatusId = getLong("in_reply_to_status_id", json);
        this.inReplyToUserId = getInt("in_reply_to_user_id", json);
        this.inReplyToScreenName = json.getString("in_reply_to_screen_name");
        this.thumbnail_pic = json.getString("thumbnail_pic");
        this.bmiddle_pic = json.getString("bmiddle_pic");
        this.original_pic = json.getString("original_pic");
        this.user = new User(json.getJSONObject("user"));
    }

    public Status(String str) throws WeiboException, JSONException {
        JSONObject json = new JSONObject(str);
        this.id = json.getLong("id");
        this.text = json.getString("text");
        this.source = json.getString("source");
        this.createdAt = parseDate(json.getString("created_at"), "EEE MMM dd HH:mm:ss z yyyy");
        this.inReplyToStatusId = getLong("in_reply_to_status_id", json);
        this.inReplyToUserId = getInt("in_reply_to_user_id", json);
        this.isFavorited = getBoolean("favorited", json);
        this.thumbnail_pic = json.getString("thumbnail_pic");
        this.bmiddle_pic = json.getString("bmiddle_pic");
        this.original_pic = json.getString("original_pic");
        this.user = new User(json.getJSONObject("user"));
    }

    private void init(Response res, Element elem, Weibo weibo) throws WeiboException {
        ensureRootNodeNameIs("status", elem);
        this.user = new User(res, (Element) elem.getElementsByTagName("user").item(0), weibo);
        this.id = getChildLong("id", elem);
        this.text = getChildText("text", elem);
        this.source = getChildText("source", elem);
        this.createdAt = getChildDate("created_at", elem);
        this.isTruncated = getChildBoolean("truncated", elem);
        this.inReplyToStatusId = getChildLong("in_reply_to_status_id", elem);
        this.inReplyToUserId = getChildInt("in_reply_to_user_id", elem);
        this.isFavorited = getChildBoolean("favorited", elem);
        this.inReplyToScreenName = getChildText("in_reply_to_screen_name", elem);
        NodeList georssPoint = elem.getElementsByTagName("georss:point");
        if (1 == georssPoint.getLength()) {
            String[] point = georssPoint.item(0).getFirstChild().getNodeValue().split(" ");
            if (!"null".equals(point[0])) {
                this.latitude = Double.parseDouble(point[0]);
            }
            if (!"null".equals(point[1])) {
                this.longitude = Double.parseDouble(point[1]);
            }
        }
        NodeList retweetDetailsNode = elem.getElementsByTagName("retweet_details");
        if (1 == retweetDetailsNode.getLength()) {
            this.retweetDetails = new RetweetDetails(res, (Element) retweetDetailsNode.item(0), weibo);
        }
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public long getId() {
        return this.id;
    }

    public String getText() {
        return this.text;
    }

    public String getSource() {
        return this.source;
    }

    public boolean isTruncated() {
        return this.isTruncated;
    }

    public long getInReplyToStatusId() {
        return this.inReplyToStatusId;
    }

    public int getInReplyToUserId() {
        return this.inReplyToUserId;
    }

    public String getInReplyToScreenName() {
        return this.inReplyToScreenName;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public boolean isFavorited() {
        return this.isFavorited;
    }

    public String getThumbnail_pic() {
        return this.thumbnail_pic;
    }

    public String getBmiddle_pic() {
        return this.bmiddle_pic;
    }

    public String getOriginal_pic() {
        return this.original_pic;
    }

    public User getUser() {
        return this.user;
    }

    public boolean isRetweet() {
        return this.retweetDetails != null;
    }

    public RetweetDetails getRetweetDetails() {
        return this.retweetDetails;
    }

    static List<Status> constructStatuses(Response res, Weibo weibo) throws WeiboException {
        Document doc = res.asDocument();
        if (isRootNodeNilClasses(doc)) {
            return new ArrayList(0);
        }
        try {
            ensureRootNodeNameIs("statuses", doc);
            NodeList list = doc.getDocumentElement().getElementsByTagName("status");
            int size = list.getLength();
            List<Status> statuses = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                statuses.add(new Status(res, (Element) list.item(i), weibo));
            }
            return statuses;
        } catch (WeiboException e) {
            ensureRootNodeNameIs("nil-classes", doc);
            return new ArrayList(0);
        }
    }

    static List<Status> constructStatuses(Response res) throws WeiboException {
        try {
            JSONArray list = res.asJSONArray();
            int size = list.length();
            List<Status> statuses = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                JSONObject obj = list.getJSONObject(i);
                if (obj.getJSONObject("user") != null) {
                    Status status = new Status(obj);
                    statuses.add(status);
                    if (obj.get("retweeted_status") != null) {
                        JSONObject retweeted_status2 = (JSONObject) obj.get("retweeted_status");
                        if (retweeted_status2.getJSONObject("user") != null) {
                            Status asStatus = new Status(retweeted_status2);
                            status.setParentId(Long.parseLong(retweeted_status2.get("id").toString()));
                            if (StringUtil.isContainLink(retweeted_status2.get("text").toString())) {
                                asStatus.setLink(1);
                            }
                            statuses.add(asStatus);
                        }
                    } else if (StringUtil.isContainLink(status.getText())) {
                        status.setLink(1);
                    }
                }
            }
            return statuses;
        } catch (JSONException e) {
            throw new WeiboException(e);
        } catch (WeiboException e2) {
            throw e2;
        }
    }

    static List<Status> constructStatuses1(Response res) throws WeiboException {
        try {
            JSONArray list = res.asJSONArray();
            int size = list.length();
            List<Status> statuses = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                JSONObject obj = list.getJSONObject(i);
                Status status = new Status(obj);
                if (obj.get("retweeted_status") != null) {
                    status.setRetweeted_status(new Status((JSONObject) obj.get("retweeted_status")));
                }
                statuses.add(status);
            }
            return statuses;
        } catch (JSONException e) {
            throw new WeiboException(e);
        } catch (WeiboException e2) {
            throw e2;
        }
    }

    public int hashCode() {
        return (int) this.id;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return (obj instanceof Status) && ((Status) obj).id == this.id;
    }

    public String toString() {
        String str = "Status{createdAt=" + this.createdAt + ", id=" + this.id + ", text='" + this.text + '\'' + ", source='" + this.source + '\'' + ", isTruncated=" + this.isTruncated + ", inReplyToStatusId=" + this.inReplyToStatusId + ", inReplyToUserId=" + this.inReplyToUserId + ", isFavorited=" + this.isFavorited + ", thumbnail_pic=" + this.thumbnail_pic + ", bmiddle_pic=" + this.bmiddle_pic + ", original_pic=" + this.original_pic + ", inReplyToScreenName='" + this.inReplyToScreenName + '\'' + ", latitude=" + this.latitude + ", longitude=" + this.longitude + ", retweetDetails=" + this.retweetDetails + ", user=" + this.user;
        if (this.retweeted_status != null) {
            str = str + ", retweeted_status=" + this.retweeted_status;
        }
        return str + '}';
    }

    public long getParentId() {
        return this.parentId;
    }

    public void setParentId(long parentId2) {
        this.parentId = parentId2;
    }

    public Status getRetweeted_status() {
        return this.retweeted_status;
    }

    public void setRetweeted_status(Status retweeted_status2) {
        this.retweeted_status = retweeted_status2;
    }

    public int getLink() {
        return this.isLink;
    }

    public void setLink(int link) {
        this.isLink = link;
    }
}
