package weibo4j.org.json;

import weibo4j.AsyncWeibo;

public class CDL {
    private static String getValue(JSONTokener x) throws JSONException {
        char c;
        while (true) {
            c = x.next();
            if (c != ' ' && c != 9) {
                break;
            }
        }
        switch (c) {
            case 0:
                return null;
            case AsyncWeibo.EXISTS_FRIENDSHIP:
            case AsyncWeibo.UPDATE_STATUS:
                return x.nextString(c);
            case AsyncWeibo.TRENDS:
                x.back();
                return "";
            default:
                x.back();
                return x.nextTo(',');
        }
    }

    public static JSONArray rowToJSONArray(JSONTokener x) throws JSONException {
        JSONArray ja = new JSONArray();
        while (true) {
            String value = getValue(x);
            if (value == null || (ja.length() == 0 && value.length() == 0)) {
                return null;
            }
            ja.put(value);
            while (true) {
                char c = x.next();
                if (c != ',') {
                    if (c != ' ') {
                        if (c == 10 || c == 13 || c == 0) {
                            return ja;
                        }
                        throw x.syntaxError("Bad character '" + c + "' (" + ((int) c) + ").");
                    }
                }
            }
        }
        return null;
    }

    public static JSONObject rowToJSONObject(JSONArray names, JSONTokener x) throws JSONException {
        JSONArray ja = rowToJSONArray(x);
        if (ja != null) {
            return ja.toJSONObject(names);
        }
        return null;
    }

    public static JSONArray toJSONArray(String string) throws JSONException {
        return toJSONArray(new JSONTokener(string));
    }

    public static JSONArray toJSONArray(JSONTokener x) throws JSONException {
        return toJSONArray(rowToJSONArray(x), x);
    }

    public static JSONArray toJSONArray(JSONArray names, String string) throws JSONException {
        return toJSONArray(names, new JSONTokener(string));
    }

    public static JSONArray toJSONArray(JSONArray names, JSONTokener x) throws JSONException {
        if (names == null || names.length() == 0) {
            return null;
        }
        JSONArray ja = new JSONArray();
        while (true) {
            JSONObject jo = rowToJSONObject(names, x);
            if (jo == null) {
                break;
            }
            ja.put(jo);
        }
        if (ja.length() == 0) {
            return null;
        }
        return ja;
    }

    public static String rowToString(JSONArray ja) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < ja.length(); i++) {
            if (i > 0) {
                sb.append(',');
            }
            Object o = ja.opt(i);
            if (o != null) {
                String s = o.toString();
                if (s.indexOf(44) < 0) {
                    sb.append(s);
                } else if (s.indexOf(34) >= 0) {
                    sb.append('\'');
                    sb.append(s);
                    sb.append('\'');
                } else {
                    sb.append('\"');
                    sb.append(s);
                    sb.append('\"');
                }
            }
        }
        sb.append(10);
        return sb.toString();
    }

    public static String toString(JSONArray ja) throws JSONException {
        JSONArray names;
        JSONObject jo = ja.optJSONObject(0);
        if (jo == null || (names = jo.names()) == null) {
            return null;
        }
        return rowToString(names) + toString(names, ja);
    }

    public static String toString(JSONArray names, JSONArray ja) throws JSONException {
        if (names == null || names.length() == 0) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < ja.length(); i++) {
            JSONObject jo = ja.optJSONObject(i);
            if (jo != null) {
                sb.append(rowToString(jo.toJSONArray(names)));
            }
        }
        return sb.toString();
    }
}
