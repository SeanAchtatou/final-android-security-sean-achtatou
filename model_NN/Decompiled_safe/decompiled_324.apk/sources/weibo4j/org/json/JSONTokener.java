package weibo4j.org.json;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import org.apache.commons.httpclient.HttpStatus;
import weibo4j.AsyncWeibo;

public class JSONTokener {
    private int index;
    private char lastChar;
    private Reader reader;
    private boolean useLastChar;

    public JSONTokener(Reader reader2) {
        this.reader = reader2.markSupported() ? reader2 : new BufferedReader(reader2);
        this.useLastChar = false;
        this.index = 0;
    }

    public JSONTokener(String s) {
        this(new StringReader(s));
    }

    public void back() throws JSONException {
        if (this.useLastChar || this.index <= 0) {
            throw new JSONException("Stepping back two steps is not supported");
        }
        this.index--;
        this.useLastChar = true;
    }

    public static int dehexchar(char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        }
        if (c >= 'A' && c <= 'F') {
            return c - '7';
        }
        if (c < 'a' || c > 'f') {
            return -1;
        }
        return c - 'W';
    }

    public boolean more() throws JSONException {
        if (next() == 0) {
            return false;
        }
        back();
        return true;
    }

    public char next() throws JSONException {
        if (this.useLastChar) {
            this.useLastChar = false;
            if (this.lastChar != 0) {
                this.index++;
            }
            return this.lastChar;
        }
        try {
            int c = this.reader.read();
            if (c <= 0) {
                this.lastChar = 0;
                return 0;
            }
            this.index++;
            this.lastChar = (char) c;
            return this.lastChar;
        } catch (IOException e) {
            throw new JSONException(e);
        }
    }

    public char next(char c) throws JSONException {
        char n = next();
        if (n == c) {
            return n;
        }
        throw syntaxError("Expected '" + c + "' and instead saw '" + n + "'");
    }

    public String next(int n) throws JSONException {
        if (n == 0) {
            return "";
        }
        char[] buffer = new char[n];
        int pos = 0;
        if (this.useLastChar) {
            this.useLastChar = false;
            buffer[0] = this.lastChar;
            pos = 1;
        }
        while (pos < n) {
            try {
                int len = this.reader.read(buffer, pos, n - pos);
                if (len == -1) {
                    break;
                }
                pos += len;
            } catch (IOException e) {
                throw new JSONException(e);
            }
        }
        this.index += pos;
        if (pos < n) {
            throw syntaxError("Substring bounds error");
        }
        this.lastChar = buffer[n - 1];
        return new String(buffer);
    }

    public char nextClean() throws JSONException {
        char c;
        do {
            c = next();
            if (c == 0) {
                break;
            }
        } while (c <= ' ');
        return c;
    }

    public String nextString(char quote) throws JSONException {
        StringBuffer sb = new StringBuffer();
        while (true) {
            char c = next();
            switch (c) {
                case 0:
                case AsyncWeibo.DIRECT_MESSAGES:
                case 13:
                    throw syntaxError("Unterminated string");
                case '\\':
                    char c2 = next();
                    switch (c2) {
                        case 'b':
                            sb.append(8);
                            continue;
                        case HttpStatus.SC_PROCESSING:
                            sb.append(12);
                            continue;
                        case 'n':
                            sb.append(10);
                            continue;
                        case 'r':
                            sb.append(13);
                            continue;
                        case 't':
                            sb.append(9);
                            continue;
                        case 'u':
                            sb.append((char) Integer.parseInt(next(4), 16));
                            continue;
                        case 'x':
                            sb.append((char) Integer.parseInt(next(2), 16));
                            continue;
                        default:
                            sb.append(c2);
                            continue;
                    }
                default:
                    if (c != quote) {
                        sb.append(c);
                        break;
                    } else {
                        return sb.toString();
                    }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0017  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String nextTo(char r4) throws weibo4j.org.json.JSONException {
        /*
            r3 = this;
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
        L_0x0005:
            char r0 = r3.next()
            if (r0 == r4) goto L_0x0015
            if (r0 == 0) goto L_0x0015
            r2 = 10
            if (r0 == r2) goto L_0x0015
            r2 = 13
            if (r0 != r2) goto L_0x0023
        L_0x0015:
            if (r0 == 0) goto L_0x001a
            r3.back()
        L_0x001a:
            java.lang.String r2 = r1.toString()
            java.lang.String r2 = r2.trim()
            return r2
        L_0x0023:
            r1.append(r0)
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: weibo4j.org.json.JSONTokener.nextTo(char):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x001b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String nextTo(java.lang.String r4) throws weibo4j.org.json.JSONException {
        /*
            r3 = this;
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
        L_0x0005:
            char r0 = r3.next()
            int r2 = r4.indexOf(r0)
            if (r2 >= 0) goto L_0x0019
            if (r0 == 0) goto L_0x0019
            r2 = 10
            if (r0 == r2) goto L_0x0019
            r2 = 13
            if (r0 != r2) goto L_0x0027
        L_0x0019:
            if (r0 == 0) goto L_0x001e
            r3.back()
        L_0x001e:
            java.lang.String r2 = r1.toString()
            java.lang.String r2 = r2.trim()
            return r2
        L_0x0027:
            r1.append(r0)
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: weibo4j.org.json.JSONTokener.nextTo(java.lang.String):java.lang.String");
    }

    public Object nextValue() throws JSONException {
        char c = nextClean();
        switch (c) {
            case AsyncWeibo.EXISTS_FRIENDSHIP:
            case AsyncWeibo.UPDATE_STATUS:
                return nextString(c);
            case AsyncWeibo.DESTROY_DIRECT_MESSAGES:
            case '[':
                back();
                return new JSONArray(this);
            case '{':
                back();
                return new JSONObject(this);
            default:
                StringBuffer sb = new StringBuffer();
                while (c >= ' ' && ",:]}/\\\"[{;=#".indexOf(c) < 0) {
                    sb.append(c);
                    c = next();
                }
                back();
                String s = sb.toString().trim();
                if (!s.equals("")) {
                    return JSONObject.stringToValue(s);
                }
                throw syntaxError("Missing value");
        }
    }

    public char skipTo(char to) throws JSONException {
        char c;
        try {
            int startIndex = this.index;
            this.reader.mark(Integer.MAX_VALUE);
            while (true) {
                c = next();
                if (c != 0) {
                    if (c == to) {
                        back();
                        break;
                    }
                } else {
                    this.reader.reset();
                    this.index = startIndex;
                    break;
                }
            }
            return c;
        } catch (IOException e) {
            throw new JSONException(e);
        }
    }

    public JSONException syntaxError(String message) {
        return new JSONException(message + toString());
    }

    public String toString() {
        return " at character " + this.index;
    }
}
