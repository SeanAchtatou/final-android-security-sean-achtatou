package weibo4j.org.json;

public class Test {
    /* JADX INFO: Multiple debug info for r8v25 weibo4j.org.json.JSONObject: [D('j' weibo4j.org.json.JSONObject), D('s' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v121 weibo4j.org.json.JSONArray: [D('s' java.lang.String), D('ja' weibo4j.org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r8v186 weibo4j.org.json.JSONObject: [D('j' weibo4j.org.json.JSONObject), D('i' int)] */
    /* JADX INFO: Multiple debug info for r8v233 weibo4j.org.json.JSONObject: [D('j' weibo4j.org.json.JSONObject), D('s' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v168 weibo4j.org.json.JSONArray: [D('s' java.lang.String), D('ja' weibo4j.org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r8v278 java.lang.Exception: [D('e' java.lang.Exception), D('ja' weibo4j.org.json.JSONArray)] */
    /*  JADX ERROR: NullPointerException in pass: CodeShrinkVisitor
        java.lang.NullPointerException
        	at jadx.core.dex.instructions.args.InsnArg.wrapInstruction(InsnArg.java:119)
        	at jadx.core.dex.visitors.shrink.CodeShrinkVisitor.inline(CodeShrinkVisitor.java:145)
        	at jadx.core.dex.visitors.shrink.CodeShrinkVisitor.shrinkBlock(CodeShrinkVisitor.java:71)
        	at jadx.core.dex.visitors.shrink.CodeShrinkVisitor.shrinkMethod(CodeShrinkVisitor.java:43)
        	at jadx.core.dex.visitors.shrink.CodeShrinkVisitor.visit(CodeShrinkVisitor.java:35)
        */
    public static void main(java.lang.String[] r8) {
        /*
            weibo4j.org.json.Test$1Obj r1 = new weibo4j.org.json.Test$1Obj
            java.lang.String r8 = "A beany object"
            r2 = 4631107791820423168(0x4045000000000000, double:42.0)
            r0 = 1
            r1.<init>(r8, r2, r0)
            java.lang.String r8 = "<![CDATA[This is a collection of test patterns and examples for org.json.]]>  Ignore the stuff past the end.  "
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.XML.toJSONObject(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "{     \"list of lists\" : [         [1, 2, 3],         [4, 5, 6],     ] }"
            weibo4j.org.json.JSONObject r8 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            r8.<init>(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r2 = 4
            java.lang.String r2 = r8.toString(r2)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r2)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.XML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "<recipe name=\"bread\" prep_time=\"5 mins\" cook_time=\"3 hours\"> <title>Basic bread</title> <ingredient amount=\"8\" unit=\"dL\">Flour</ingredient> <ingredient amount=\"10\" unit=\"grams\">Yeast</ingredient> <ingredient amount=\"4\" unit=\"dL\" state=\"warm\">Water</ingredient> <ingredient amount=\"1\" unit=\"teaspoon\">Salt</ingredient> <instructions> <step>Mix all ingredients together.</step> <step>Knead thoroughly.</step> <step>Cover with a cloth, and leave for one hour in warm room.</step> <step>Knead again.</step> <step>Place in a bread baking tin.</step> <step>Cover with a cloth, and leave for one hour in warm room.</step> <step>Bake in the oven at 180(degrees)C for 30 minutes.</step> </instructions> </recipe> "
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.XML.toJSONObject(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r3 = 4
            java.lang.String r8 = r8.toString(r3)     // Catch:{ Exception -> 0x0a44 }
            r2.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r8.println()     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.JSONML.toJSONObject(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r3 = r8.toString()     // Catch:{ Exception -> 0x0a44 }
            r2.println(r3)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.JSONML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r2.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r8.println()     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONArray r8 = weibo4j.org.json.JSONML.toJSONArray(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r2 = 4
            java.lang.String r2 = r8.toString(r2)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r2)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.JSONML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r8.println()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "<div id=\"demo\" class=\"JSONML\"><p>JSONML is a transformation between <b>JSON</b> and <b>XML</b> that preserves ordering of document features.</p><p>JSONML can work with JSON arrays or JSON objects.</p><p>Three<br/>little<br/>words</p></div>"
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.JSONML.toJSONObject(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r3 = 4
            java.lang.String r3 = r8.toString(r3)     // Catch:{ Exception -> 0x0a44 }
            r2.println(r3)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.JSONML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r2.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r8.println()     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONArray r8 = weibo4j.org.json.JSONML.toJSONArray(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r2 = 4
            java.lang.String r2 = r8.toString(r2)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r2)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.JSONML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r8.println()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "<person created=\"2006-11-11T19:23\" modified=\"2006-12-31T23:59\">\n <firstName>Robert</firstName>\n <lastName>Smith</lastName>\n <address type=\"home\">\n <street>12345 Sixth Ave</street>\n <city>Anytown</city>\n <state>CA</state>\n <postalCode>98765-4321</postalCode>\n </address>\n </person>"
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.XML.toJSONObject(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r2 = 4
            java.lang.String r8 = r8.toString(r2)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONObject r8 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            r8.<init>(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "{ \"entity\": { \"imageURL\": \"\", \"name\": \"IXXXXXXXXXXXXX\", \"id\": 12336, \"ratingCount\": null, \"averageRating\": null } }"
            weibo4j.org.json.JSONObject r8 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            r8.<init>(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r2 = 2
            java.lang.String r8 = r8.toString(r2)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONStringer r8 = new weibo4j.org.json.JSONStringer     // Catch:{ Exception -> 0x0a44 }
            r8.<init>()     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r8 = r8.object()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "single"
            weibo4j.org.json.JSONWriter r8 = r8.key(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "MARIE HAA'S"
            weibo4j.org.json.JSONWriter r8 = r8.value(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "Johnny"
            weibo4j.org.json.JSONWriter r8 = r8.key(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "MARIE HAA\\'S"
            weibo4j.org.json.JSONWriter r8 = r8.value(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "foo"
            weibo4j.org.json.JSONWriter r8 = r8.key(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "bar"
            weibo4j.org.json.JSONWriter r8 = r8.value(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "baz"
            weibo4j.org.json.JSONWriter r8 = r8.key(r0)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r8 = r8.array()     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r8 = r8.object()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "quux"
            weibo4j.org.json.JSONWriter r8 = r8.key(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "Thanks, Josh!"
            weibo4j.org.json.JSONWriter r8 = r8.value(r0)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r8 = r8.endObject()     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r8 = r8.endArray()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "obj keys"
            weibo4j.org.json.JSONWriter r8 = r8.key(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String[] r0 = weibo4j.org.json.JSONObject.getNames(r1)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r8 = r8.value(r0)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r8 = r8.endObject()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONStringer r0 = new weibo4j.org.json.JSONStringer     // Catch:{ Exception -> 0x0a44 }
            r0.<init>()     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r0 = r0.object()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "a"
            weibo4j.org.json.JSONWriter r0 = r0.key(r2)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r0 = r0.array()     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r0 = r0.array()     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r0 = r0.array()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "b"
            weibo4j.org.json.JSONWriter r0 = r0.value(r2)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r0 = r0.endArray()     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r0 = r0.endArray()     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r0 = r0.endArray()     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r0 = r0.endObject()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONStringer r3 = new weibo4j.org.json.JSONStringer     // Catch:{ Exception -> 0x0a44 }
            r3.<init>()     // Catch:{ Exception -> 0x0a44 }
            r3.array()     // Catch:{ Exception -> 0x0a44 }
            r4 = 1
            r3.value(r4)     // Catch:{ Exception -> 0x0a44 }
            r3.array()     // Catch:{ Exception -> 0x0a44 }
            r8 = 0
            r3.value(r8)     // Catch:{ Exception -> 0x0a44 }
            r3.array()     // Catch:{ Exception -> 0x0a44 }
            r3.object()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "empty-array"
            weibo4j.org.json.JSONWriter r8 = r3.key(r8)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r8 = r8.array()     // Catch:{ Exception -> 0x0a44 }
            r8.endArray()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "answer"
            weibo4j.org.json.JSONWriter r8 = r3.key(r8)     // Catch:{ Exception -> 0x0a44 }
            r4 = 42
            r8.value(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "null"
            weibo4j.org.json.JSONWriter r8 = r3.key(r8)     // Catch:{ Exception -> 0x0a44 }
            r0 = 0
            r8.value(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "false"
            weibo4j.org.json.JSONWriter r8 = r3.key(r8)     // Catch:{ Exception -> 0x0a44 }
            r0 = 0
            r8.value(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "true"
            weibo4j.org.json.JSONWriter r8 = r3.key(r8)     // Catch:{ Exception -> 0x0a44 }
            r0 = 1
            r8.value(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "big"
            weibo4j.org.json.JSONWriter r8 = r3.key(r8)     // Catch:{ Exception -> 0x0a44 }
            r4 = 6044533123932129715(0x53e27ed9d50e89b3, double:1.23456789E96)
            r8.value(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "small"
            weibo4j.org.json.JSONWriter r8 = r3.key(r8)     // Catch:{ Exception -> 0x0a44 }
            r4 = 3411313870812105556(0x2f576be43f1e4b54, double:1.23456789E-80)
            r8.value(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "empty-object"
            weibo4j.org.json.JSONWriter r8 = r3.key(r8)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r8 = r8.object()     // Catch:{ Exception -> 0x0a44 }
            r8.endObject()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "long"
            r3.key(r8)     // Catch:{ Exception -> 0x0a44 }
            r4 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r3.value(r4)     // Catch:{ Exception -> 0x0a44 }
            r3.endObject()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "two"
            r3.value(r8)     // Catch:{ Exception -> 0x0a44 }
            r3.endArray()     // Catch:{ Exception -> 0x0a44 }
            r8 = 1
            r3.value(r8)     // Catch:{ Exception -> 0x0a44 }
            r3.endArray()     // Catch:{ Exception -> 0x0a44 }
            r4 = 4636638775112787558(0x4058a66666666666, double:98.6)
            r3.value(r4)     // Catch:{ Exception -> 0x0a44 }
            r4 = -4586634745500139520(0xc059000000000000, double:-100.0)
            r3.value(r4)     // Catch:{ Exception -> 0x0a44 }
            r3.object()     // Catch:{ Exception -> 0x0a44 }
            r3.endObject()     // Catch:{ Exception -> 0x0a44 }
            r3.object()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "one"
            r3.key(r8)     // Catch:{ Exception -> 0x0a44 }
            r4 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r3.value(r4)     // Catch:{ Exception -> 0x0a44 }
            r3.endObject()     // Catch:{ Exception -> 0x0a44 }
            r3.value(r1)     // Catch:{ Exception -> 0x0a44 }
            r3.endArray()     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONArray r0 = new weibo4j.org.json.JSONArray     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = r3.toString()     // Catch:{ Exception -> 0x0a44 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0a44 }
            r2 = 4
            java.lang.String r0 = r0.toString(r2)     // Catch:{ Exception -> 0x0a44 }
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            r8 = 3
            int[] r8 = new int[r8]     // Catch:{ Exception -> 0x0a44 }
            r8 = {1, 2, 3} // fill-array     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONArray r0 = new weibo4j.org.json.JSONArray     // Catch:{ Exception -> 0x0a44 }
            r0.<init>(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            r8 = 3
            java.lang.String[] r0 = new java.lang.String[r8]     // Catch:{ Exception -> 0x0a44 }
            r8 = 0
            java.lang.String r2 = "aString"
            r0[r8] = r2     // Catch:{ Exception -> 0x0a44 }
            r8 = 1
            java.lang.String r2 = "aNumber"
            r0[r8] = r2     // Catch:{ Exception -> 0x0a44 }
            r8 = 2
            java.lang.String r2 = "aBoolean"
            r0[r8] = r2     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONObject r8 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            r8.<init>(r1, r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "Testing JSONString interface"
            r8.put(r0, r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 4
            java.lang.String r8 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONObject r8 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "{slashes: '///', closetag: '</script>', backslash:'\\\\', ei: {quotes: '\"\\''},eo: {a: '\"quoted\"', b:\"don't\"}, quotes: [\"'\", '\"']}"
            r8.<init>(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 2
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.XML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONObject r0 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "{foo: [true, false,9876543210,    0.0, 1.00000001,  1.000000000001, 1.00000000000000001, .00000000000000001, 2.00, 0.1, 2e100, -32,[],{}, \"string\"],   to   : null, op : 'Good',ten:10} postfix comment"
            r0.<init>(r8)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "String"
            java.lang.String r1 = "98.6"
            r0.put(r8, r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "JSONObject"
            weibo4j.org.json.JSONObject r1 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            r0.put(r8, r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "JSONArray"
            weibo4j.org.json.JSONArray r1 = new weibo4j.org.json.JSONArray     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            r0.put(r8, r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "int"
            r1 = 57
            r0.put(r8, r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "double"
            r1 = 5042042089369253694(0x45f8ee90ff6c373e, double:1.2345678901234568E29)
            r0.put(r8, r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "true"
            r1 = 1
            r0.put(r8, r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "false"
            r1 = 0
            r0.put(r8, r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "null"
            java.lang.Object r1 = weibo4j.org.json.JSONObject.NULL     // Catch:{ Exception -> 0x0a44 }
            r0.put(r8, r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "bool"
            java.lang.String r1 = "true"
            r0.put(r8, r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "zero"
            r1 = -9223372036854775808
            r0.put(r8, r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "\\u2028"
            java.lang.String r1 = " "
            r0.put(r8, r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "\\u2029"
            java.lang.String r1 = " "
            r0.put(r8, r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "foo"
            weibo4j.org.json.JSONArray r8 = r0.getJSONArray(r8)     // Catch:{ Exception -> 0x0a44 }
            r1 = 666(0x29a, float:9.33E-43)
            r8.put(r1)     // Catch:{ Exception -> 0x0a44 }
            r1 = 4656519660581116969(0x409f47f5c28f5c29, double:2001.99)
            r8.put(r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = "so \"fine\"."
            r8.put(r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = "so <fine>."
            r8.put(r1)     // Catch:{ Exception -> 0x0a44 }
            r1 = 1
            r8.put(r1)     // Catch:{ Exception -> 0x0a44 }
            r1 = 0
            r8.put(r1)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONArray r1 = new weibo4j.org.json.JSONArray     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            r8.put(r1)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONObject r1 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            r8.put(r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "keys"
            java.lang.String[] r1 = weibo4j.org.json.JSONObject.getNames(r0)     // Catch:{ Exception -> 0x0a44 }
            r0.put(r8, r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 4
            java.lang.String r1 = r0.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = weibo4j.org.json.XML.toString(r0)     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "String: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "String"
            double r4 = r0.getDouble(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "  bool: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "bool"
            boolean r2 = r0.getBoolean(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "    to: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "to"
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "  true: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "true"
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "   foo: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "foo"
            weibo4j.org.json.JSONArray r2 = r0.getJSONArray(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "    op: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "op"
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "   ten: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "ten"
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "  oops: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "oops"
            boolean r0 = r0.optBoolean(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "<xml one = 1 two=' \"2\" '><five></five>First \t&lt;content&gt;<five></five> This is \"content\". <three>  3  </three>JSON does not preserve the sequencing of elements and contents.<three>  III  </three>  <three>  T H R E E</three><four/>Content text is an implied structure in XML. <six content=\"6\"/>JSON does not have implied structure:<seven>7</seven>everything is explicit.<![CDATA[CDATA blocks<are><supported>!]]></xml>"
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.XML.toJSONObject(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r2 = 2
            java.lang.String r2 = r8.toString(r2)     // Catch:{ Exception -> 0x0a44 }
            r1.println(r2)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.XML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r1.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = ""
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONArray r8 = weibo4j.org.json.JSONML.toJSONArray(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 4
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.JSONML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "<xml do='0'>uno<a re='1' mi='2'>dos<b fa='3'/>tres<c>true</c>quatro</a>cinqo<d>seis<e/></d></xml>"
            weibo4j.org.json.JSONArray r8 = weibo4j.org.json.JSONML.toJSONArray(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 4
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.JSONML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "<mapping><empty/>   <class name = \"Customer\">      <field name = \"ID\" type = \"string\">         <bind-xml name=\"ID\" node=\"attribute\"/>      </field>      <field name = \"FirstName\" type = \"FirstName\"/>      <field name = \"MI\" type = \"MI\"/>      <field name = \"LastName\" type = \"LastName\"/>   </class>   <class name = \"FirstName\">      <field name = \"text\">         <bind-xml name = \"text\" node = \"text\"/>      </field>   </class>   <class name = \"MI\">      <field name = \"text\">         <bind-xml name = \"text\" node = \"text\"/>      </field>   </class>   <class name = \"LastName\">      <field name = \"text\">         <bind-xml name = \"text\" node = \"text\"/>      </field>   </class></mapping>"
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.XML.toJSONObject(r2)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 2
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.XML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONArray r8 = weibo4j.org.json.JSONML.toJSONArray(r2)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 4
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.JSONML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "<?xml version=\"1.0\" ?><Book Author=\"Anonymous\"><Title>Sample Book</Title><Chapter id=\"1\">This is chapter 1. It is not very long or interesting.</Chapter><Chapter id=\"2\">This is chapter 2. Although it is longer than chapter 1, it is not any more interesting.</Chapter></Book>"
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.XML.toJSONObject(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 2
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.XML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "<!DOCTYPE bCard 'http://www.cs.caltech.edu/~adam/schemas/bCard'><bCard><?xml default bCard        firstname = ''        lastname  = '' company   = '' email = '' homepage  = ''?><bCard        firstname = 'Rohit'        lastname  = 'Khare'        company   = 'MCI'        email     = 'khare@mci.net'        homepage  = 'http://pest.w3.org/'/><bCard        firstname = 'Adam'        lastname  = 'Rifkin'        company   = 'Caltech Infospheres Project'        email     = 'adam@cs.caltech.edu'        homepage  = 'http://www.cs.caltech.edu/~adam/'/></bCard>"
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.XML.toJSONObject(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 2
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.XML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "<?xml version=\"1.0\"?><customer>    <firstName>        <text>Fred</text>    </firstName>    <ID>fbs0001</ID>    <lastName> <text>Scerbo</text>    </lastName>    <MI>        <text>B</text>    </MI></customer>"
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.XML.toJSONObject(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 2
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.XML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "<!ENTITY tp-address PUBLIC '-//ABC University::Special Collections Library//TEXT (titlepage: name and address)//EN' 'tpspcoll.sgm'><list type='simple'><head>Repository Address </head><item>Special Collections Library</item><item>ABC University</item><item>Main Library, 40 Circle Drive</item><item>Ourtown, Pennsylvania</item><item>17654 USA</item></list>"
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.XML.toJSONObject(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r8.toString()     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.XML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "<test intertag status=ok><empty/>deluxe<blip sweet=true>&amp;&quot;toot&quot;&toot;&#x41;</blip><x>eks</x><w>bonus</w><w>bonus2</w></test>"
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.XML.toJSONObject(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 2
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.XML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "GET / HTTP/1.0\nAccept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/vnd.ms-powerpoint, application/vnd.ms-excel, application/msword, */*\nAccept-Language: en-us\nUser-Agent: Mozilla/4.0 (compatible; MSIE 5.5; Windows 98; Win 9x 4.90; T312461; Q312461)\nHost: www.nokko.com\nConnection: keep-alive\nAccept-encoding: gzip, deflate\n"
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.HTTP.toJSONObject(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 2
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.HTTP.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "HTTP/1.1 200 Oki Doki\nDate: Sun, 26 May 2002 17:38:52 GMT\nServer: Apache/1.3.23 (Unix) mod_perl/1.26\nKeep-Alive: timeout=15, max=100\nConnection: Keep-Alive\nTransfer-Encoding: chunked\nContent-Type: text/html\n"
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.HTTP.toJSONObject(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 2
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.HTTP.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONObject r8 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "{nix: null, nux: false, null: 'null', 'Request-URI': '/', Method: 'GET', 'HTTP-Version': 'HTTP/1.0'}"
            r8.<init>(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 2
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "isNull: "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "nix"
            boolean r4 = r8.isNull(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "   has: "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "nix"
            boolean r4 = r8.has(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = weibo4j.org.json.XML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.HTTP.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "<?xml version='1.0' encoding='UTF-8'?>\n\n<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/1999/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/1999/XMLSchema\"><SOAP-ENV:Body><ns1:doGoogleSearch xmlns:ns1=\"urn:GoogleSearch\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"><key xsi:type=\"xsd:string\">GOOGLEKEY</key> <q xsi:type=\"xsd:string\">'+search+'</q> <start xsi:type=\"xsd:int\">0</start> <maxResults xsi:type=\"xsd:int\">10</maxResults> <filter xsi:type=\"xsd:boolean\">true</filter> <restrict xsi:type=\"xsd:string\"></restrict> <safeSearch xsi:type=\"xsd:boolean\">false</safeSearch> <lr xsi:type=\"xsd:string\"></lr> <ie xsi:type=\"xsd:string\">latin1</ie> <oe xsi:type=\"xsd:string\">latin1</oe></ns1:doGoogleSearch></SOAP-ENV:Body></SOAP-ENV:Envelope>"
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.XML.toJSONObject(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 2
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.XML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONObject r8 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "{Envelope: {Body: {\"ns1:doGoogleSearch\": {oe: \"latin1\", filter: true, q: \"'+search+'\", key: \"GOOGLEKEY\", maxResults: 10, \"SOAP-ENV:encodingStyle\": \"http://schemas.xmlsoap.org/soap/encoding/\", start: 0, ie: \"latin1\", safeSearch:false, \"xmlns:ns1\": \"urn:GoogleSearch\"}}}}"
            r8.<init>(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 2
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.XML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "  f%oo = b+l=ah  ; o;n%40e = t.wo "
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.CookieList.toJSONObject(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 2
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.CookieList.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "f%oo=blah; secure ;expires = April 24, 2002"
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.Cookie.toJSONObject(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 2
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.Cookie.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONObject r8 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "{script: 'It is not allowed in HTML to send a close script tag in a string<script>because it confuses browsers</script>so we insert a backslash before the /'}"
            r8.<init>(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONTokener r0 = new weibo4j.org.json.JSONTokener     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "{op:'test', to:'session', pre:1}{op:'test', to:'session', pre:2}"
            r0.<init>(r8)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONObject r8 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            r8.<init>(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = r8.toString()     // Catch:{ Exception -> 0x0a44 }
            r1.println(r4)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r4.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = "pre: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = "pre"
            int r8 = r8.optInt(r5)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r8 = r4.append(r8)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0a44 }
            r1.println(r8)     // Catch:{ Exception -> 0x0a44 }
            r8 = 123(0x7b, float:1.72E-43)
            char r8 = r0.skipTo(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1.println(r8)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONObject r8 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            r8.<init>(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "No quotes, 'Single Quotes', \"Double Quotes\"\n1,'2',\"3\"\n,'It is \"good,\"', \"It works.\"\n\n"
            weibo4j.org.json.JSONArray r8 = weibo4j.org.json.CDL.toJSONArray(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = weibo4j.org.json.CDL.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = ""
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 4
            java.lang.String r8 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = ""
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONArray r8 = new weibo4j.org.json.JSONArray     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = " [\"<escape>\", next is an implied null , , ok,] "
            r8.<init>(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r8.toString()     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = ""
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = weibo4j.org.json.XML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = ""
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONObject r0 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = "{ fun => with non-standard forms ; forgiving => This package can be used to parse formats that are similar to but not stricting conforming to JSON; why=To make it easier to migrate existing data to JSON,one = [[1.00]]; uno=[[{1=>1}]];'+':+6e66 ;pluses=+++;empty = '' , 'double':0.666,true: TRUE, false: FALSE, null=NULL;[true] = [[!,@;*]]; string=>  o. k. ; \r oct=0666; hex=0x666; dec=666; o=0999; noh=0x0x}"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r4 = 4
            java.lang.String r4 = r0.toString(r4)     // Catch:{ Exception -> 0x0a44 }
            r1.println(r4)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = ""
            r1.println(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = "true"
            boolean r1 = r0.getBoolean(r1)     // Catch:{ Exception -> 0x0a44 }
            if (r1 == 0) goto L_0x07b0
            java.lang.String r1 = "false"
            boolean r1 = r0.getBoolean(r1)     // Catch:{ Exception -> 0x0a44 }
            if (r1 != 0) goto L_0x07b0
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "It's all good"
            r1.println(r4)     // Catch:{ Exception -> 0x0a44 }
        L_0x07b0:
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = ""
            r1.println(r4)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONObject r1 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            r4 = 4
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0a44 }
            r5 = 0
            java.lang.String r6 = "dec"
            r4[r5] = r6     // Catch:{ Exception -> 0x0a44 }
            r5 = 1
            java.lang.String r6 = "oct"
            r4[r5] = r6     // Catch:{ Exception -> 0x0a44 }
            r5 = 2
            java.lang.String r6 = "hex"
            r4[r5] = r6     // Catch:{ Exception -> 0x0a44 }
            r5 = 3
            java.lang.String r6 = "missing"
            r4[r5] = r6     // Catch:{ Exception -> 0x0a44 }
            r1.<init>(r0, r4)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r4 = 4
            java.lang.String r4 = r1.toString(r4)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r4)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = ""
            r0.println(r4)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONStringer r4 = new weibo4j.org.json.JSONStringer     // Catch:{ Exception -> 0x0a44 }
            r4.<init>()     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r4 = r4.array()     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r8 = r4.value(r8)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r8 = r8.value(r1)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONWriter r8 = r8.endArray()     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONObject r0 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "{string: \"98.6\", long: 2147483648, int: 2147483647, longer: 9223372036854775807, double: 9223372036854775808}"
            r0.<init>(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 4
            java.lang.String r1 = r0.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = "\ngetInt"
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "int    "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "int"
            int r4 = r0.getInt(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "long   "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "long"
            int r4 = r0.getInt(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "longer "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "longer"
            int r4 = r0.getInt(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "double "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "double"
            int r4 = r0.getInt(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "string "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "string"
            int r4 = r0.getInt(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = "\ngetLong"
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "int    "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "int"
            long r4 = r0.getLong(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "long   "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "long"
            long r4 = r0.getLong(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "longer "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "longer"
            long r4 = r0.getLong(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "double "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "double"
            long r4 = r0.getLong(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "string "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "string"
            long r4 = r0.getLong(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = "\ngetDouble"
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "int    "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "int"
            double r4 = r0.getDouble(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "long   "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "long"
            double r4 = r0.getDouble(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "longer "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "longer"
            double r4 = r0.getDouble(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "double "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "double"
            double r4 = r0.getDouble(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "string "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "string"
            double r4 = r0.getDouble(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "good sized"
            r4 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r0.put(r8, r4)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 4
            java.lang.String r1 = r0.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONArray r8 = new weibo4j.org.json.JSONArray     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = "[2147483647, 2147483648, 9223372036854775807, 9223372036854775808]"
            r8.<init>(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r4 = 4
            java.lang.String r8 = r8.toString(r4)     // Catch:{ Exception -> 0x0a44 }
            r1.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = "\nKeys: "
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.util.Iterator r8 = r0.keys()     // Catch:{ Exception -> 0x0a44 }
            r1 = r2
        L_0x0a17:
            boolean r1 = r8.hasNext()     // Catch:{ Exception -> 0x0a44 }
            if (r1 == 0) goto L_0x0a4f
            java.lang.Object r1 = r8.next()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a44 }
            r4.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = ": "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = r0.getString(r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0a44 }
            r2.println(r4)     // Catch:{ Exception -> 0x0a44 }
            goto L_0x0a17
        L_0x0a44:
            r8 = move-exception
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.String r8 = r8.toString()
            r0.println(r8)
        L_0x0a4e:
            return
        L_0x0a4f:
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "\naccumulate: "
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONObject r0 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            r0.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "stooge"
            java.lang.String r1 = "Curly"
            r0.accumulate(r8, r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "stooge"
            java.lang.String r1 = "Larry"
            r0.accumulate(r8, r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "stooge"
            java.lang.String r1 = "Moe"
            r0.accumulate(r8, r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "stooge"
            weibo4j.org.json.JSONArray r8 = r0.getJSONArray(r8)     // Catch:{ Exception -> 0x0a44 }
            r1 = 5
            java.lang.String r2 = "Shemp"
            r8.put(r1, r2)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 4
            java.lang.String r1 = r0.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = "\nwrite:"
            r8.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.io.StringWriter r1 = new java.io.StringWriter     // Catch:{ Exception -> 0x0a44 }
            r1.<init>()     // Catch:{ Exception -> 0x0a44 }
            java.io.Writer r0 = r0.write(r1)     // Catch:{ Exception -> 0x0a44 }
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "<xml empty><a></a><a>1</a><a>22</a><a>333</a></xml>"
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.XML.toJSONObject(r8)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 4
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.XML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "<book><chapter>Content of the first chapter</chapter><chapter>Content of the second chapter      <chapter>Content of the first subchapter</chapter>      <chapter>Content of the second subchapter</chapter></chapter><chapter>Third Chapter</chapter></book>"
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.XML.toJSONObject(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r2 = 4
            java.lang.String r2 = r8.toString(r2)     // Catch:{ Exception -> 0x0a44 }
            r1.println(r2)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.XML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r1.println(r8)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONArray r8 = weibo4j.org.json.JSONML.toJSONArray(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 4
            java.lang.String r1 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = weibo4j.org.json.JSONML.toString(r8)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            r0 = 0
            r2 = 0
            weibo4j.org.json.JSONObject r1 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONArray r8 = new weibo4j.org.json.JSONArray     // Catch:{ Exception -> 0x0a44 }
            r8.<init>(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "stooge"
            java.lang.String r5 = "Joe DeRita"
            r1.append(r4, r5)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "stooge"
            java.lang.String r5 = "Shemp"
            r1.append(r4, r5)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "stooges"
            java.lang.String r5 = "Curly"
            r1.accumulate(r4, r5)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "stooges"
            java.lang.String r5 = "Larry"
            r1.accumulate(r4, r5)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "stooges"
            java.lang.String r5 = "Moe"
            r1.accumulate(r4, r5)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "stoogearray"
            java.lang.String r5 = "stooges"
            java.lang.Object r5 = r1.get(r5)     // Catch:{ Exception -> 0x0a44 }
            r1.accumulate(r4, r5)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "map"
            r1.put(r4, r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "collection"
            r1.put(r4, r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "array"
            r1.put(r4, r8)     // Catch:{ Exception -> 0x0a44 }
            r8.put(r2)     // Catch:{ Exception -> 0x0a44 }
            r8.put(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r0 = 4
            java.lang.String r0 = r1.toString(r0)     // Catch:{ Exception -> 0x0a44 }
            r8.println(r0)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "{plist=Apple; AnimalSmells = { pig = piggish; lamb = lambish; worm = wormy; }; AnimalSounds = { pig = oink; lamb = baa; worm = baa;  Lisa = \"Why is the worm talking like a lamb?\" } ; AnimalColors = { pig = pink; lamb = black; worm = pink; } } "
            weibo4j.org.json.JSONObject r8 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0a44 }
            r8.<init>(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r1 = 4
            java.lang.String r8 = r8.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r8)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = " (\"San Francisco\", \"New York\", \"Seoul\", \"London\", \"Seattle\", \"Shanghai\")"
            weibo4j.org.json.JSONArray r8 = new weibo4j.org.json.JSONArray     // Catch:{ Exception -> 0x0a44 }
            r8.<init>(r0)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r1 = r8.toString()     // Catch:{ Exception -> 0x0a44 }
            r0.println(r1)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "<a ichi='1' ni='2'><b>The content of b</b> and <c san='3'>The content of c</c><d>do</d><e></e><d>re</d><f/><d>mi</d></a>"
            weibo4j.org.json.JSONObject r1 = weibo4j.org.json.XML.toJSONObject(r4)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r2 = 2
            java.lang.String r2 = r1.toString(r2)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r2)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = weibo4j.org.json.XML.toString(r1)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r2)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = ""
            r0.println(r2)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONArray r2 = weibo4j.org.json.JSONML.toJSONArray(r4)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r5 = 4
            java.lang.String r5 = r2.toString(r5)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r5)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = weibo4j.org.json.JSONML.toString(r2)     // Catch:{ Exception -> 0x0a44 }
            r0.println(r5)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = ""
            r0.println(r5)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = "\nTesting Exceptions: "
            r0.println(r5)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = "Exception: "
            r0.print(r5)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONArray r0 = new weibo4j.org.json.JSONArray     // Catch:{ Exception -> 0x0d0f }
            r0.<init>()     // Catch:{ Exception -> 0x0d0f }
            r5 = -4503599627370496(0xfff0000000000000, double:-Infinity)
            r0.put(r5)     // Catch:{ Exception -> 0x0dbb }
            r5 = 9221120237041090560(0x7ff8000000000000, double:NaN)
            r0.put(r5)     // Catch:{ Exception -> 0x0dbb }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0dbb }
            java.lang.String r5 = r0.toString()     // Catch:{ Exception -> 0x0dbb }
            r8.println(r5)     // Catch:{ Exception -> 0x0dbb }
        L_0x0bc2:
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = "Exception: "
            r8.print(r5)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0d18 }
            java.lang.String r5 = "stooge"
            double r5 = r1.getDouble(r5)     // Catch:{ Exception -> 0x0d18 }
            r8.println(r5)     // Catch:{ Exception -> 0x0d18 }
        L_0x0bd4:
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = "Exception: "
            r8.print(r5)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0d20 }
            java.lang.String r5 = "howard"
            double r5 = r1.getDouble(r5)     // Catch:{ Exception -> 0x0d20 }
            r8.println(r5)     // Catch:{ Exception -> 0x0d20 }
        L_0x0be6:
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = "Exception: "
            r8.print(r5)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0d28 }
            r5 = 0
            java.lang.String r6 = "howard"
            weibo4j.org.json.JSONObject r5 = r1.put(r5, r6)     // Catch:{ Exception -> 0x0d28 }
            r8.println(r5)     // Catch:{ Exception -> 0x0d28 }
        L_0x0bf9:
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = "Exception: "
            r8.print(r5)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0d30 }
            r5 = 0
            double r5 = r0.getDouble(r5)     // Catch:{ Exception -> 0x0d30 }
            r8.println(r5)     // Catch:{ Exception -> 0x0d30 }
        L_0x0c0a:
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = "Exception: "
            r8.print(r5)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0d38 }
            r5 = -1
            java.lang.Object r5 = r0.get(r5)     // Catch:{ Exception -> 0x0d38 }
            r8.println(r5)     // Catch:{ Exception -> 0x0d38 }
        L_0x0c1b:
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = "Exception: "
            r8.print(r5)     // Catch:{ Exception -> 0x0a44 }
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0d40 }
            r5 = 9221120237041090560(0x7ff8000000000000, double:NaN)
            weibo4j.org.json.JSONArray r5 = r0.put(r5)     // Catch:{ Exception -> 0x0d40 }
            r8.println(r5)     // Catch:{ Exception -> 0x0d40 }
        L_0x0c2d:
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = "Exception: "
            r8.print(r5)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "<a><b>    "
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.XML.toJSONObject(r8)     // Catch:{ Exception -> 0x0d48 }
            r1 = r8
        L_0x0c3b:
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = "Exception: "
            r8.print(r5)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "<a></b>    "
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.XML.toJSONObject(r8)     // Catch:{ Exception -> 0x0d50 }
            r1 = r8
        L_0x0c49:
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = "Exception: "
            r8.print(r5)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r8 = "<a></a    "
            weibo4j.org.json.JSONObject r8 = weibo4j.org.json.XML.toJSONObject(r8)     // Catch:{ Exception -> 0x0d58 }
            r1 = r8
        L_0x0c57:
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = "Exception: "
            r8.print(r5)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONArray r8 = new weibo4j.org.json.JSONArray     // Catch:{ Exception -> 0x0d60 }
            java.lang.Object r5 = new java.lang.Object     // Catch:{ Exception -> 0x0d60 }
            r5.<init>()     // Catch:{ Exception -> 0x0d60 }
            r8.<init>(r5)     // Catch:{ Exception -> 0x0d60 }
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x0db6 }
            java.lang.String r5 = r8.toString()     // Catch:{ Exception -> 0x0db6 }
            r2.println(r5)     // Catch:{ Exception -> 0x0db6 }
            r2 = r8
        L_0x0c72:
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r5 = "Exception: "
            r8.print(r5)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "[)"
            weibo4j.org.json.JSONArray r8 = new weibo4j.org.json.JSONArray     // Catch:{ Exception -> 0x0d68 }
            r8.<init>(r4)     // Catch:{ Exception -> 0x0d68 }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0db4 }
            java.lang.String r5 = r8.toString()     // Catch:{ Exception -> 0x0db4 }
            r0.println(r5)     // Catch:{ Exception -> 0x0db4 }
            r0 = r4
        L_0x0c8a:
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "Exception: "
            r8.print(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "<xml"
            weibo4j.org.json.JSONArray r8 = weibo4j.org.json.JSONML.toJSONArray(r0)     // Catch:{ Exception -> 0x0d74 }
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x0dae }
            r4 = 4
            java.lang.String r4 = r8.toString(r4)     // Catch:{ Exception -> 0x0dae }
            r2.println(r4)     // Catch:{ Exception -> 0x0dae }
        L_0x0ca1:
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r4 = "Exception: "
            r2.print(r4)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "<right></wrong>"
            weibo4j.org.json.JSONArray r8 = weibo4j.org.json.JSONML.toJSONArray(r0)     // Catch:{ Exception -> 0x0d81 }
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x0d81 }
            r4 = 4
            java.lang.String r4 = r8.toString(r4)     // Catch:{ Exception -> 0x0d81 }
            r2.println(r4)     // Catch:{ Exception -> 0x0d81 }
        L_0x0cb8:
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "Exception: "
            r8.print(r2)     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r0 = "{\"koda\": true, \"koda\": true}"
            weibo4j.org.json.JSONObject r8 = new weibo4j.org.json.JSONObject     // Catch:{ Exception -> 0x0d8f }
            r8.<init>(r0)     // Catch:{ Exception -> 0x0d8f }
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ Exception -> 0x0da8 }
            r2 = 4
            java.lang.String r2 = r8.toString(r2)     // Catch:{ Exception -> 0x0da8 }
            r1.println(r2)     // Catch:{ Exception -> 0x0da8 }
            r1 = r0
        L_0x0cd1:
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            java.lang.String r2 = "Exception: "
            r0.print(r2)     // Catch:{ Exception -> 0x0a44 }
            weibo4j.org.json.JSONStringer r0 = new weibo4j.org.json.JSONStringer     // Catch:{ Exception -> 0x0d9b }
            r0.<init>()     // Catch:{ Exception -> 0x0d9b }
            weibo4j.org.json.JSONWriter r2 = r0.object()     // Catch:{ Exception -> 0x0da6 }
            java.lang.String r3 = "bosanda"
            weibo4j.org.json.JSONWriter r2 = r2.key(r3)     // Catch:{ Exception -> 0x0da6 }
            java.lang.String r3 = "MARIE HAA'S"
            weibo4j.org.json.JSONWriter r2 = r2.value(r3)     // Catch:{ Exception -> 0x0da6 }
            java.lang.String r3 = "bosanda"
            weibo4j.org.json.JSONWriter r2 = r2.key(r3)     // Catch:{ Exception -> 0x0da6 }
            java.lang.String r3 = "MARIE HAA\\'S"
            weibo4j.org.json.JSONWriter r2 = r2.value(r3)     // Catch:{ Exception -> 0x0da6 }
            weibo4j.org.json.JSONWriter r2 = r2.endObject()     // Catch:{ Exception -> 0x0da6 }
            java.lang.String r1 = r2.toString()     // Catch:{ Exception -> 0x0da6 }
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x0da6 }
            r3 = 4
            java.lang.String r8 = r8.toString(r3)     // Catch:{ Exception -> 0x0da6 }
            r2.println(r8)     // Catch:{ Exception -> 0x0da6 }
            r8 = r0
            r0 = r1
            goto L_0x0a4e
        L_0x0d0f:
            r0 = move-exception
        L_0x0d10:
            java.io.PrintStream r5 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r5.println(r0)     // Catch:{ Exception -> 0x0a44 }
            r0 = r8
            goto L_0x0bc2
        L_0x0d18:
            r8 = move-exception
            java.io.PrintStream r5 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r5.println(r8)     // Catch:{ Exception -> 0x0a44 }
            goto L_0x0bd4
        L_0x0d20:
            r8 = move-exception
            java.io.PrintStream r5 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r5.println(r8)     // Catch:{ Exception -> 0x0a44 }
            goto L_0x0be6
        L_0x0d28:
            r8 = move-exception
            java.io.PrintStream r5 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r5.println(r8)     // Catch:{ Exception -> 0x0a44 }
            goto L_0x0bf9
        L_0x0d30:
            r8 = move-exception
            java.io.PrintStream r5 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r5.println(r8)     // Catch:{ Exception -> 0x0a44 }
            goto L_0x0c0a
        L_0x0d38:
            r8 = move-exception
            java.io.PrintStream r5 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r5.println(r8)     // Catch:{ Exception -> 0x0a44 }
            goto L_0x0c1b
        L_0x0d40:
            r8 = move-exception
            java.io.PrintStream r5 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r5.println(r8)     // Catch:{ Exception -> 0x0a44 }
            goto L_0x0c2d
        L_0x0d48:
            r8 = move-exception
            java.io.PrintStream r5 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r5.println(r8)     // Catch:{ Exception -> 0x0a44 }
            goto L_0x0c3b
        L_0x0d50:
            r8 = move-exception
            java.io.PrintStream r5 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r5.println(r8)     // Catch:{ Exception -> 0x0a44 }
            goto L_0x0c49
        L_0x0d58:
            r8 = move-exception
            java.io.PrintStream r5 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r5.println(r8)     // Catch:{ Exception -> 0x0a44 }
            goto L_0x0c57
        L_0x0d60:
            r8 = move-exception
        L_0x0d61:
            java.io.PrintStream r5 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r5.println(r8)     // Catch:{ Exception -> 0x0a44 }
            goto L_0x0c72
        L_0x0d68:
            r8 = move-exception
            r7 = r8
            r8 = r0
            r0 = r7
        L_0x0d6c:
            java.io.PrintStream r5 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r5.println(r0)     // Catch:{ Exception -> 0x0a44 }
            r0 = r4
            goto L_0x0c8a
        L_0x0d74:
            r8 = move-exception
            r7 = r2
            r2 = r0
            r0 = r7
        L_0x0d78:
            java.io.PrintStream r4 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r4.println(r8)     // Catch:{ Exception -> 0x0a44 }
            r8 = r0
            r0 = r2
            goto L_0x0ca1
        L_0x0d81:
            r2 = move-exception
            r7 = r2
            r2 = r0
            r0 = r8
            r8 = r7
            java.io.PrintStream r4 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r4.println(r8)     // Catch:{ Exception -> 0x0a44 }
            r8 = r0
            r0 = r2
            goto L_0x0cb8
        L_0x0d8f:
            r8 = move-exception
            r7 = r0
            r0 = r1
            r1 = r7
        L_0x0d93:
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r2.println(r8)     // Catch:{ Exception -> 0x0a44 }
            r8 = r0
            goto L_0x0cd1
        L_0x0d9b:
            r8 = move-exception
            r0 = r3
        L_0x0d9d:
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x0a44 }
            r2.println(r8)     // Catch:{ Exception -> 0x0a44 }
            r8 = r0
            r0 = r1
            goto L_0x0a4e
        L_0x0da6:
            r8 = move-exception
            goto L_0x0d9d
        L_0x0da8:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r8
            r8 = r7
            goto L_0x0d93
        L_0x0dae:
            r2 = move-exception
            r7 = r2
            r2 = r0
            r0 = r8
            r8 = r7
            goto L_0x0d78
        L_0x0db4:
            r0 = move-exception
            goto L_0x0d6c
        L_0x0db6:
            r2 = move-exception
            r7 = r2
            r2 = r8
            r8 = r7
            goto L_0x0d61
        L_0x0dbb:
            r8 = move-exception
            r7 = r8
            r8 = r0
            r0 = r7
            goto L_0x0d10
        */
        throw new UnsupportedOperationException("Method not decompiled: weibo4j.org.json.Test.main(java.lang.String[]):void");
    }
}
