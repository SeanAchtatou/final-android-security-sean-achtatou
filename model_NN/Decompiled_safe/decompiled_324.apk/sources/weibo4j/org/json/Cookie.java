package weibo4j.org.json;

import org.apache.commons.httpclient.cookie.Cookie2;

public class Cookie {
    public static String escape(String string) {
        String s = string.trim();
        StringBuffer sb = new StringBuffer();
        int len = s.length();
        for (int i = 0; i < len; i++) {
            char c = s.charAt(i);
            if (c < ' ' || c == '+' || c == '%' || c == '=' || c == ';') {
                sb.append('%');
                sb.append(Character.forDigit((char) ((c >>> 4) & 15), 16));
                sb.append(Character.forDigit((char) (c & 15), 16));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static JSONObject toJSONObject(String string) throws JSONException {
        Object unescape;
        JSONObject o = new JSONObject();
        JSONTokener x = new JSONTokener(string);
        o.put("name", x.nextTo('='));
        x.next('=');
        o.put("value", x.nextTo(';'));
        x.next();
        while (x.more()) {
            String n = unescape(x.nextTo("=;"));
            if (x.next() == '=') {
                unescape = unescape(x.nextTo(';'));
                x.next();
            } else if (n.equals(Cookie2.SECURE)) {
                unescape = Boolean.TRUE;
            } else {
                throw x.syntaxError("Missing '=' in cookie parameter.");
            }
            o.put(n, unescape);
        }
        return o;
    }

    public static String toString(JSONObject o) throws JSONException {
        StringBuffer sb = new StringBuffer();
        sb.append(escape(o.getString("name")));
        sb.append("=");
        sb.append(escape(o.getString("value")));
        if (o.has("expires")) {
            sb.append(";expires=");
            sb.append(o.getString("expires"));
        }
        if (o.has(Cookie2.DOMAIN)) {
            sb.append(";domain=");
            sb.append(escape(o.getString(Cookie2.DOMAIN)));
        }
        if (o.has(Cookie2.PATH)) {
            sb.append(";path=");
            sb.append(escape(o.getString(Cookie2.PATH)));
        }
        if (o.optBoolean(Cookie2.SECURE)) {
            sb.append(";secure");
        }
        return sb.toString();
    }

    public static String unescape(String s) {
        int len = s.length();
        StringBuffer b = new StringBuffer();
        int i = 0;
        while (i < len) {
            char c = s.charAt(i);
            if (c == '+') {
                c = ' ';
            } else if (c == '%' && i + 2 < len) {
                int d = JSONTokener.dehexchar(s.charAt(i + 1));
                int e = JSONTokener.dehexchar(s.charAt(i + 2));
                if (d >= 0 && e >= 0) {
                    c = (char) ((d * 16) + e);
                    i += 2;
                }
            }
            b.append(c);
            i++;
        }
        return b.toString();
    }
}
