package weibo4j.org.json;

import java.util.Iterator;
import weibo4j.AsyncWeibo;

public class XML {
    public static final Character AMP = new Character('&');
    public static final Character APOS = new Character('\'');
    public static final Character BANG = new Character('!');
    public static final Character EQ = new Character('=');
    public static final Character GT = new Character('>');
    public static final Character LT = new Character('<');
    public static final Character QUEST = new Character('?');
    public static final Character QUOT = new Character('\"');
    public static final Character SLASH = new Character('/');

    public static String escape(String string) {
        StringBuffer sb = new StringBuffer();
        int len = string.length();
        for (int i = 0; i < len; i++) {
            char c = string.charAt(i);
            switch (c) {
                case AsyncWeibo.EXISTS_FRIENDSHIP:
                    sb.append("&quot;");
                    break;
                case AsyncWeibo.SHOW_STATUS:
                    sb.append("&amp;");
                    break;
                case '<':
                    sb.append("&lt;");
                    break;
                case '>':
                    sb.append("&gt;");
                    break;
                default:
                    sb.append(c);
                    break;
            }
        }
        return sb.toString();
    }

    public static void noSpace(String string) throws JSONException {
        int length = string.length();
        if (length == 0) {
            throw new JSONException("Empty string.");
        }
        for (int i = 0; i < length; i++) {
            if (Character.isWhitespace(string.charAt(i))) {
                throw new JSONException("'" + string + "' contains a space character.");
            }
        }
    }

    private static boolean parse(XMLTokener x, JSONObject context, String name) throws JSONException {
        Object obj;
        Object t = x.nextToken();
        if (t == BANG) {
            char c = x.next();
            if (c == '-') {
                if (x.next() == '-') {
                    x.skipPast("-->");
                    return false;
                }
                x.back();
            } else if (c == '[') {
                if (!x.nextToken().equals("CDATA") || x.next() != '[') {
                    throw x.syntaxError("Expected 'CDATA['");
                }
                String s = x.nextCDATA();
                if (s.length() > 0) {
                    context.accumulate("content", s);
                }
                return false;
            }
            int i = 1;
            do {
                Object t2 = x.nextMeta();
                if (t2 == null) {
                    throw x.syntaxError("Missing '>' after '<!'.");
                } else if (t2 == LT) {
                    i++;
                    continue;
                } else if (t2 == GT) {
                    i--;
                    continue;
                } else {
                    continue;
                }
            } while (i > 0);
            return false;
        } else if (t == QUEST) {
            x.skipPast("?>");
            return false;
        } else if (t == SLASH) {
            Object t3 = x.nextToken();
            if (name == null) {
                throw x.syntaxError("Mismatched close tag" + t3);
            } else if (!t3.equals(name)) {
                throw x.syntaxError("Mismatched " + name + " and " + t3);
            } else if (x.nextToken() == GT) {
                return true;
            } else {
                throw x.syntaxError("Misshaped close tag");
            }
        } else if (t instanceof Character) {
            throw x.syntaxError("Misshaped tag");
        } else {
            String n = (String) t;
            Object t4 = null;
            JSONObject o = new JSONObject();
            while (true) {
                if (t4 == null) {
                    obj = x.nextToken();
                } else {
                    obj = t4;
                }
                if (obj instanceof String) {
                    String s2 = (String) obj;
                    t4 = x.nextToken();
                    if (t4 == EQ) {
                        Object t5 = x.nextToken();
                        if (!(t5 instanceof String)) {
                            throw x.syntaxError("Missing value");
                        }
                        o.accumulate(s2, JSONObject.stringToValue((String) t5));
                        t4 = null;
                    } else {
                        o.accumulate(s2, "");
                    }
                } else if (obj == SLASH) {
                    if (x.nextToken() != GT) {
                        throw x.syntaxError("Misshaped tag");
                    }
                    context.accumulate(n, o);
                    return false;
                } else if (obj == GT) {
                    while (true) {
                        Object t6 = x.nextContent();
                        if (t6 == null) {
                            if (n == null) {
                                return false;
                            }
                            throw x.syntaxError("Unclosed tag " + n);
                        } else if (t6 instanceof String) {
                            String s3 = (String) t6;
                            if (s3.length() > 0) {
                                o.accumulate("content", JSONObject.stringToValue(s3));
                            }
                        } else if (t6 == LT && parse(x, o, n)) {
                            if (o.length() == 0) {
                                context.accumulate(n, "");
                            } else if (o.length() != 1 || o.opt("content") == null) {
                                context.accumulate(n, o);
                            } else {
                                context.accumulate(n, o.opt("content"));
                            }
                            return false;
                        }
                    }
                } else {
                    throw x.syntaxError("Misshaped tag");
                }
            }
        }
    }

    public static JSONObject toJSONObject(String string) throws JSONException {
        JSONObject o = new JSONObject();
        XMLTokener x = new XMLTokener(string);
        while (x.more() && x.skipPast("<")) {
            parse(x, o, null);
        }
        return o;
    }

    public static String toString(Object o) throws JSONException {
        return toString(o, null);
    }

    public static String toString(Object o, String tagName) throws JSONException {
        String str;
        StringBuffer b = new StringBuffer();
        if (o instanceof JSONObject) {
            if (tagName != null) {
                b.append('<');
                b.append(tagName);
                b.append('>');
            }
            JSONObject jo = (JSONObject) o;
            Iterator keys = jo.keys();
            while (keys.hasNext()) {
                String k = keys.next().toString();
                Object v = jo.opt(k);
                if (v == null) {
                    v = "";
                }
                if (v instanceof String) {
                    String str2 = (String) v;
                }
                if (k.equals("content")) {
                    if (v instanceof JSONArray) {
                        JSONArray ja = (JSONArray) v;
                        int len = ja.length();
                        for (int i = 0; i < len; i++) {
                            if (i > 0) {
                                b.append(10);
                            }
                            b.append(escape(ja.get(i).toString()));
                        }
                    } else {
                        b.append(escape(v.toString()));
                    }
                } else if (v instanceof JSONArray) {
                    JSONArray ja2 = (JSONArray) v;
                    int len2 = ja2.length();
                    for (int i2 = 0; i2 < len2; i2++) {
                        Object v2 = ja2.get(i2);
                        if (v2 instanceof JSONArray) {
                            b.append('<');
                            b.append(k);
                            b.append('>');
                            b.append(toString(v2));
                            b.append("</");
                            b.append(k);
                            b.append('>');
                        } else {
                            b.append(toString(v2, k));
                        }
                    }
                } else if (v.equals("")) {
                    b.append('<');
                    b.append(k);
                    b.append("/>");
                } else {
                    b.append(toString(v, k));
                }
            }
            if (tagName != null) {
                b.append("</");
                b.append(tagName);
                b.append('>');
            }
            return b.toString();
        } else if (o instanceof JSONArray) {
            JSONArray ja3 = (JSONArray) o;
            int len3 = ja3.length();
            for (int i3 = 0; i3 < len3; i3++) {
                Object v3 = ja3.opt(i3);
                if (tagName == null) {
                    str = "array";
                } else {
                    str = tagName;
                }
                b.append(toString(v3, str));
            }
            return b.toString();
        } else {
            String s = o == null ? "null" : escape(o.toString());
            if (tagName == null) {
                return "\"" + s + "\"";
            }
            return s.length() == 0 ? "<" + tagName + "/>" : "<" + tagName + ">" + s + "</" + tagName + ">";
        }
    }
}
