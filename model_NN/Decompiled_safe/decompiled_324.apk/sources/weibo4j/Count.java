package weibo4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import weibo4j.http.Response;
import weibo4j.org.json.JSONArray;
import weibo4j.org.json.JSONException;
import weibo4j.org.json.JSONObject;

public class Count implements Serializable {
    private static final long serialVersionUID = 9076424494907778181L;
    private long comments;
    private long dm;
    private long followers;
    private long id;
    private long mentions;
    private long rt;

    public Count(JSONObject json) throws WeiboException, JSONException {
        this.id = json.getLong("id");
        this.comments = json.getLong("comments");
        this.rt = json.getLong("rt");
        this.dm = json.getLong("dm");
        this.mentions = json.getLong("mentions");
        this.followers = json.getLong("followers");
    }

    static List<Count> constructCounts(Response res) throws WeiboException {
        try {
            JSONArray list = res.asJSONArray();
            int size = list.length();
            List<Count> counts = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                counts.add(new Count(list.getJSONObject(i)));
            }
            return counts;
        } catch (JSONException e) {
            throw new WeiboException(e);
        } catch (WeiboException e2) {
            throw e2;
        }
    }

    public int hashCode() {
        return (int) this.id;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return (obj instanceof Count) && ((Count) obj).id == this.id;
    }

    public long getComments() {
        return this.comments;
    }

    public long getRt() {
        return this.rt;
    }

    public long getDm() {
        return this.dm;
    }

    public long getMentions() {
        return this.mentions;
    }

    public long getFollowers() {
        return this.followers;
    }

    public String toString() {
        return "Count{ id=" + this.id + ", comments=" + this.comments + ", rt=" + this.rt + ", dm=" + this.dm + ", mentions=" + this.mentions + ", followers=" + this.followers + '}';
    }
}
