package weibo4j;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import weibo4j.http.PostParameter;

public class WeiboStream extends WeiboSupport {
    private static final boolean DEBUG = Configuration.getDebug();
    private StreamHandlingThread handler = null;
    /* access modifiers changed from: private */
    public int retryPerMinutes = 1;
    /* access modifiers changed from: private */
    public StatusListener statusListener;

    public /* bridge */ /* synthetic */ void forceUsePost(boolean x0) {
        super.forceUsePost(x0);
    }

    public /* bridge */ /* synthetic */ String getClientURL() {
        return super.getClientURL();
    }

    public /* bridge */ /* synthetic */ String getClientVersion() {
        return super.getClientVersion();
    }

    public /* bridge */ /* synthetic */ String getPassword() {
        return super.getPassword();
    }

    public /* bridge */ /* synthetic */ String getSource() {
        return super.getSource();
    }

    public /* bridge */ /* synthetic */ String getUserAgent() {
        return super.getUserAgent();
    }

    public /* bridge */ /* synthetic */ String getUserId() {
        return super.getUserId();
    }

    public /* bridge */ /* synthetic */ boolean isUsePostForced() {
        return super.isUsePostForced();
    }

    public /* bridge */ /* synthetic */ void setClientURL(String x0) {
        super.setClientURL(x0);
    }

    public /* bridge */ /* synthetic */ void setClientVersion(String x0) {
        super.setClientVersion(x0);
    }

    public /* bridge */ /* synthetic */ void setHttpConnectionTimeout(int x0) {
        super.setHttpConnectionTimeout(x0);
    }

    public /* bridge */ /* synthetic */ void setHttpProxy(String x0, int x1) {
        super.setHttpProxy(x0, x1);
    }

    public /* bridge */ /* synthetic */ void setHttpProxyAuth(String x0, String x1) {
        super.setHttpProxyAuth(x0, x1);
    }

    public /* bridge */ /* synthetic */ void setHttpReadTimeout(int x0) {
        super.setHttpReadTimeout(x0);
    }

    public /* bridge */ /* synthetic */ void setPassword(String x0) {
        super.setPassword(x0);
    }

    public /* bridge */ /* synthetic */ void setRequestHeader(String x0, String x1) {
        super.setRequestHeader(x0, x1);
    }

    public /* bridge */ /* synthetic */ void setRetryCount(int x0) {
        super.setRetryCount(x0);
    }

    public /* bridge */ /* synthetic */ void setRetryIntervalSecs(int x0) {
        super.setRetryIntervalSecs(x0);
    }

    public /* bridge */ /* synthetic */ void setSource(String x0) {
        super.setSource(x0);
    }

    public /* bridge */ /* synthetic */ void setUserAgent(String x0) {
        super.setUserAgent(x0);
    }

    public /* bridge */ /* synthetic */ void setUserId(String x0) {
        super.setUserId(x0);
    }

    public WeiboStream() {
    }

    public WeiboStream(String userId, String password) {
        super(userId, password);
    }

    public WeiboStream(String userId, String password, StatusListener listener) {
        super(userId, password);
        this.statusListener = listener;
    }

    public void firehose(int count) throws WeiboException {
        startHandler(new StreamHandlingThread(new Object[]{Integer.valueOf(count)}) {
            public StatusStream getStream() throws WeiboException {
                return WeiboStream.this.getFirehoseStream(((Integer) this.args[0]).intValue());
            }
        });
    }

    public StatusStream getFirehoseStream(int count) throws WeiboException {
        try {
            return new StatusStream(http.post(getStreamBaseURL() + "1/statuses/firehose.json", new PostParameter[]{new PostParameter("count", String.valueOf(count))}, true));
        } catch (IOException e) {
            throw new WeiboException(e);
        }
    }

    public void retweet() throws WeiboException {
        startHandler(new StreamHandlingThread(new Object[0]) {
            public StatusStream getStream() throws WeiboException {
                return WeiboStream.this.getRetweetStream();
            }
        });
    }

    public StatusStream getRetweetStream() throws WeiboException {
        try {
            return new StatusStream(http.post(getStreamBaseURL() + "1/statuses/retweet.json", new PostParameter[0], true));
        } catch (IOException e) {
            throw new WeiboException(e);
        }
    }

    public void sample() throws WeiboException {
        startHandler(new StreamHandlingThread(null) {
            public StatusStream getStream() throws WeiboException {
                return WeiboStream.this.getSampleStream();
            }
        });
    }

    public StatusStream getSampleStream() throws WeiboException {
        try {
            return new StatusStream(http.get(getStreamBaseURL() + "1/statuses/sample.json", true));
        } catch (IOException e) {
            throw new WeiboException(e);
        }
    }

    public void filter(int count, int[] follow, String[] track) throws WeiboException {
        startHandler(new StreamHandlingThread(new Object[]{Integer.valueOf(count), follow, track}) {
            public StatusStream getStream() throws WeiboException {
                return WeiboStream.this.getFilterStream(((Integer) this.args[0]).intValue(), (int[]) this.args[1], (String[]) this.args[2]);
            }
        });
    }

    public StatusStream getFilterStream(int count, int[] follow, String[] track) throws WeiboException {
        List<PostParameter> postparams = new ArrayList<>();
        postparams.add(new PostParameter("count", count));
        if (follow != null && follow.length > 0) {
            postparams.add(new PostParameter("follow", toFollowString(follow)));
        }
        if (track != null && track.length > 0) {
            postparams.add(new PostParameter("track", toTrackString(track)));
        }
        try {
            return new StatusStream(http.post(getStreamBaseURL() + "1/statuses/filter.json", (PostParameter[]) postparams.toArray(new PostParameter[0]), true));
        } catch (IOException e) {
            throw new WeiboException(e);
        }
    }

    public void gardenhose() throws WeiboException {
        startHandler(new StreamHandlingThread(null) {
            public StatusStream getStream() throws WeiboException {
                return WeiboStream.this.getGardenhoseStream();
            }
        });
    }

    public StatusStream getGardenhoseStream() throws WeiboException {
        return getSampleStream();
    }

    public void spritzer() throws WeiboException {
        startHandler(new StreamHandlingThread(null) {
            public StatusStream getStream() throws WeiboException {
                return WeiboStream.this.getSpritzerStream();
            }
        });
    }

    public StatusStream getSpritzerStream() throws WeiboException {
        return getSampleStream();
    }

    public void birddog(int count, int[] follow) throws WeiboException {
        startHandler(new StreamHandlingThread(new Object[]{Integer.valueOf(count), follow}) {
            public StatusStream getStream() throws WeiboException {
                return WeiboStream.this.getBirddogStream(((Integer) this.args[0]).intValue(), (int[]) this.args[1]);
            }
        });
    }

    public StatusStream getBirddogStream(int count, int[] follow) throws WeiboException {
        return getFilterStream(count, follow, null);
    }

    public void shadow(int count, int[] follow) throws WeiboException {
        startHandler(new StreamHandlingThread(new Object[]{Integer.valueOf(count), follow}) {
            public StatusStream getStream() throws WeiboException {
                return WeiboStream.this.getShadowStream(((Integer) this.args[0]).intValue(), (int[]) this.args[1]);
            }
        });
    }

    public StatusStream getShadowStream(int count, int[] follow) throws WeiboException {
        return getFilterStream(count, follow, null);
    }

    public void follow(int[] follow) throws WeiboException {
        startHandler(new StreamHandlingThread(new Object[]{follow}) {
            public StatusStream getStream() throws WeiboException {
                return WeiboStream.this.getFollowStream((int[]) this.args[0]);
            }
        });
    }

    public StatusStream getFollowStream(int[] follow) throws WeiboException {
        return getFilterStream(0, follow, null);
    }

    private String toFollowString(int[] follows) {
        StringBuffer buf = new StringBuffer(follows.length * 11);
        for (int follow : follows) {
            if (buf.length() != 0) {
                buf.append(",");
            }
            buf.append(follow);
        }
        return buf.toString();
    }

    public void track(final String[] keywords) throws WeiboException {
        startHandler(new StreamHandlingThread(null) {
            public StatusStream getStream() throws WeiboException {
                return WeiboStream.this.getTrackStream(keywords);
            }
        });
    }

    public StatusStream getTrackStream(String[] keywords) throws WeiboException {
        return getFilterStream(0, null, keywords);
    }

    private String toTrackString(String[] keywords) {
        StringBuffer buf = new StringBuffer(keywords.length * 20 * 4);
        for (String keyword : keywords) {
            if (buf.length() != 0) {
                buf.append(",");
            }
            buf.append(keyword);
        }
        return buf.toString();
    }

    private synchronized void startHandler(StreamHandlingThread handler2) throws WeiboException {
        cleanup();
        if (this.statusListener == null) {
            throw new IllegalStateException("StatusListener is not set.");
        }
        this.handler = handler2;
        this.handler.start();
    }

    public synchronized void cleanup() {
        if (this.handler != null) {
            try {
                this.handler.close();
            } catch (IOException e) {
            }
        }
    }

    public StatusListener getStatusListener() {
        return this.statusListener;
    }

    public void setStatusListener(StatusListener statusListener2) {
        this.statusListener = statusListener2;
    }

    abstract class StreamHandlingThread extends Thread {
        private static final String NAME = "Weibo Stream Handling Thread";
        Object[] args;
        private boolean closed = false;
        private List<Long> retryHistory;
        StatusStream stream = null;

        /* access modifiers changed from: package-private */
        public abstract StatusStream getStream() throws WeiboException;

        StreamHandlingThread(Object[] args2) {
            super("Weibo Stream Handling Thread[initializing]");
            this.args = args2;
            this.retryHistory = new ArrayList(WeiboStream.this.retryPerMinutes);
        }

        public void run() {
            Status status;
            while (!this.closed) {
                try {
                    if (this.retryHistory.size() > 0 && System.currentTimeMillis() - this.retryHistory.get(0).longValue() > 60000) {
                        this.retryHistory.remove(0);
                    }
                    if (this.retryHistory.size() < WeiboStream.this.retryPerMinutes) {
                        setStatus("[establishing connection]");
                        while (!this.closed && this.stream == null) {
                            if (this.retryHistory.size() < WeiboStream.this.retryPerMinutes) {
                                this.retryHistory.add(Long.valueOf(System.currentTimeMillis()));
                                this.stream = getStream();
                            }
                        }
                    } else {
                        long timeToSleep = 60000 - (System.currentTimeMillis() - this.retryHistory.get(this.retryHistory.size() - 1).longValue());
                        setStatus("[retry limit reached. sleeping for " + (timeToSleep / 1000) + " secs]");
                        try {
                            Thread.sleep(timeToSleep);
                        } catch (InterruptedException e) {
                        }
                    }
                    if (this.stream != null) {
                        setStatus("[receiving stream]");
                        while (!this.closed && (status = this.stream.next()) != null) {
                            WeiboStream.this.log("received:", status.toString());
                            if (WeiboStream.this.statusListener != null) {
                                WeiboStream.this.statusListener.onStatus(status);
                            }
                        }
                    }
                } catch (WeiboException e2) {
                    WeiboException te = e2;
                    this.stream = null;
                    te.printStackTrace();
                    WeiboStream.this.log(te.getMessage());
                    WeiboStream.this.statusListener.onException(te);
                }
            }
        }

        public synchronized void close() throws IOException {
            setStatus("[disposing thread]");
            if (this.stream != null) {
                this.stream.close();
                this.closed = true;
            }
        }

        private void setStatus(String message) {
            String actualMessage = NAME + message;
            setName(actualMessage);
            WeiboStream.this.log(actualMessage);
        }
    }

    private String getStreamBaseURL() {
        return this.USE_SSL ? "https://stream.t.sina.com.cn/" : "http://stream.t.sina.com.cn/";
    }

    /* access modifiers changed from: private */
    public void log(String message) {
        if (DEBUG) {
            System.out.println("[" + new Date() + "]" + message);
        }
    }

    /* access modifiers changed from: private */
    public void log(String message, String message2) {
        if (DEBUG) {
            log(message + message2);
        }
    }
}
