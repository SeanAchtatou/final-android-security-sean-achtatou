package weibo4j;

public interface StatusListener {
    void onException(Exception exc);

    void onStatus(Status status);
}
