package weibo4j;

import java.io.Serializable;

public class Paging implements Serializable {
    private static final long serialVersionUID = -3285857427993796670L;
    private int count;
    private long maxId;
    private int page;
    private long sinceId;
    private long userId;

    public Paging() {
        this.page = -1;
        this.count = -1;
        this.sinceId = -1;
        this.maxId = -1;
        this.userId = -1;
    }

    public Paging(int page2) {
        this.page = -1;
        this.count = -1;
        this.sinceId = -1;
        this.maxId = -1;
        this.userId = -1;
        setPage(page2);
    }

    public Paging(long sinceId2) {
        this.page = -1;
        this.count = -1;
        this.sinceId = -1;
        this.maxId = -1;
        this.userId = -1;
        setSinceId(sinceId2);
    }

    public Paging(int page2, int count2) {
        this(page2);
        setCount(count2);
    }

    public Paging(int page2, int count2, String userId2) {
        this(page2, count2);
        setUserId(Long.parseLong(userId2));
    }

    public Paging(int page2, long sinceId2) {
        this(page2);
        setSinceId(sinceId2);
    }

    public Paging(int page2, int count2, long sinceId2) {
        this(page2, count2);
        setSinceId(sinceId2);
    }

    public Paging(int page2, int count2, long sinceId2, int userId2) {
        this(page2, count2, sinceId2);
        setUserId((long) userId2);
    }

    public Paging(int page2, int count2, long sinceId2, long maxId2) {
        this(page2, count2, sinceId2);
        setMaxId(maxId2);
    }

    public int getPage() {
        return this.page;
    }

    public void setPage(int page2) {
        if (page2 < 1) {
            throw new IllegalArgumentException("page should be positive integer. passed:" + page2);
        }
        this.page = page2;
    }

    public int getCount() {
        return this.count;
    }

    public void setCount(int count2) {
        if (count2 < 1) {
            throw new IllegalArgumentException("count should be positive integer. passed:" + count2);
        }
        this.count = count2;
    }

    public Paging count(int count2) {
        setCount(count2);
        return this;
    }

    public long getSinceId() {
        return this.sinceId;
    }

    public void setSinceId(int sinceId2) {
        if (sinceId2 < 1) {
            throw new IllegalArgumentException("since_id should be positive integer. passed:" + sinceId2);
        }
        this.sinceId = (long) sinceId2;
    }

    public Paging sinceId(int sinceId2) {
        setSinceId(sinceId2);
        return this;
    }

    public void setSinceId(long sinceId2) {
        if (sinceId2 < 1) {
            throw new IllegalArgumentException("since_id should be positive integer. passed:" + sinceId2);
        }
        this.sinceId = sinceId2;
    }

    public Paging sinceId(long sinceId2) {
        setSinceId(sinceId2);
        return this;
    }

    public long getMaxId() {
        return this.maxId;
    }

    public void setMaxId(long maxId2) {
        if (maxId2 < 1) {
            throw new IllegalArgumentException("max_id should be positive integer. passed:" + maxId2);
        }
        this.maxId = maxId2;
    }

    public Paging maxId(long maxId2) {
        setMaxId(maxId2);
        return this;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId2) {
        if (userId2 < 1) {
            throw new IllegalArgumentException("userId should be positive integer. passed:" + userId2);
        }
        this.userId = userId2;
    }
}
