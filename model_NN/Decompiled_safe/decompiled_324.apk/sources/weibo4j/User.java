package weibo4j;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.httpclient.cookie.Cookie2;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import weibo4j.http.Response;
import weibo4j.org.json.JSONArray;
import weibo4j.org.json.JSONException;
import weibo4j.org.json.JSONObject;

public class User extends WeiboResponse implements Serializable {
    static final String[] POSSIBLE_ROOT_NAMES = {"user", "sender", "recipient", "retweeting_user"};
    private static final long serialVersionUID = -6345893237975349030L;
    private Date createdAt;
    private String description;
    private String domain;
    private int favouritesCount;
    private int followersCount;
    private boolean following;
    private int friendsCount;
    private String gender;
    private boolean geoEnabled;
    private int id;
    private boolean isProtected;
    private String location;
    private String name;
    private boolean notificationEnabled;
    private String profileBackgroundColor;
    private String profileBackgroundImageUrl;
    private String profileBackgroundTile;
    private String profileImageUrl;
    private String profileLinkColor;
    private String profileSidebarBorderColor;
    private String profileSidebarFillColor;
    private String profileTextColor;
    private String screenName;
    private Date statusCreatedAt;
    private boolean statusFavorited = false;
    private long statusId = -1;
    private String statusInReplyToScreenName = null;
    private long statusInReplyToStatusId = -1;
    private int statusInReplyToUserId = -1;
    private String statusSource = null;
    private String statusText = null;
    private boolean statusTruncated = false;
    private int statusesCount;
    private String timeZone;
    private String url;
    private int utcOffset;
    private boolean verified;
    private Weibo weibo;

    User(Response res, Weibo weibo2) throws WeiboException {
        super(res);
        init(res.asDocument().getDocumentElement(), weibo2);
    }

    User(Response res, Element elem, Weibo weibo2) throws WeiboException {
        super(res);
        init(elem, weibo2);
    }

    User(JSONObject json) throws WeiboException {
        init(json);
    }

    private void init(JSONObject json) throws WeiboException {
        try {
            this.id = json.getInt("id");
            this.name = json.getString("name");
            this.screenName = json.getString("screen_name");
            this.location = json.getString("location");
            this.description = json.getString("description");
            this.profileImageUrl = json.getString("profile_image_url");
            this.url = json.getString("url");
            this.isProtected = json.getBoolean("protected");
            this.followersCount = json.getInt("followers_count");
            this.domain = json.getString(Cookie2.DOMAIN);
            this.gender = json.getString("gender");
            this.profileBackgroundColor = json.getString("profile_background_color");
            this.profileTextColor = json.getString("profile_text_color");
            this.profileLinkColor = json.getString("profile_link_color");
            this.profileSidebarFillColor = json.getString("profile_sidebar_fill_color");
            this.profileSidebarBorderColor = json.getString("profile_sidebar_border_color");
            this.friendsCount = json.getInt("friends_count");
            this.createdAt = parseDate(json.getString("created_at"), "EEE MMM dd HH:mm:ss z yyyy");
            this.favouritesCount = json.getInt("favourites_count");
            this.utcOffset = getInt("utc_offset", json);
            this.timeZone = json.getString("time_zone");
            this.profileBackgroundImageUrl = json.getString("profile_background_image_url");
            this.profileBackgroundTile = json.getString("profile_background_tile");
            this.following = getBoolean("following", json);
            this.notificationEnabled = getBoolean("notifications", json);
            this.statusesCount = json.getInt("statuses_count");
            if (!json.isNull("status")) {
                JSONObject status = json.getJSONObject("status");
                this.statusCreatedAt = parseDate(status.getString("created_at"), "EEE MMM dd HH:mm:ss z yyyy");
                this.statusId = status.getLong("id");
                this.statusText = status.getString("text");
                this.statusSource = status.getString("source");
                this.statusTruncated = status.getBoolean("truncated");
                this.statusInReplyToStatusId = status.getLong("in_reply_to_status_id");
                this.statusInReplyToUserId = status.getInt("in_reply_to_user_id");
                this.statusFavorited = status.getBoolean("favorited");
                this.statusInReplyToScreenName = status.getString("in_reply_to_screen_name");
            }
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new WeiboException(jsone.getMessage() + ":" + json.toString(), jsone);
        }
    }

    private void init(Element elem, Weibo weibo2) throws WeiboException {
        this.weibo = weibo2;
        ensureRootNodeNameIs(POSSIBLE_ROOT_NAMES, elem);
        this.id = getChildInt("id", elem);
        this.name = getChildText("name", elem);
        this.screenName = getChildText("screen_name", elem);
        this.location = getChildText("location", elem);
        this.description = getChildText("description", elem);
        this.profileImageUrl = getChildText("profile_image_url", elem);
        this.url = getChildText("url", elem);
        this.isProtected = getChildBoolean("protected", elem);
        this.followersCount = getChildInt("followers_count", elem);
        this.domain = getChildText(Cookie2.DOMAIN, elem);
        this.gender = getChildText("gender", elem);
        this.profileBackgroundColor = getChildText("profile_background_color", elem);
        this.profileTextColor = getChildText("profile_text_color", elem);
        this.profileLinkColor = getChildText("profile_link_color", elem);
        this.profileSidebarFillColor = getChildText("profile_sidebar_fill_color", elem);
        this.profileSidebarBorderColor = getChildText("profile_sidebar_border_color", elem);
        this.friendsCount = getChildInt("friends_count", elem);
        this.createdAt = getChildDate("created_at", elem);
        this.favouritesCount = getChildInt("favourites_count", elem);
        this.utcOffset = getChildInt("utc_offset", elem);
        this.timeZone = getChildText("time_zone", elem);
        this.profileBackgroundImageUrl = getChildText("profile_background_image_url", elem);
        this.profileBackgroundTile = getChildText("profile_background_tile", elem);
        this.following = getChildBoolean("following", elem);
        this.notificationEnabled = getChildBoolean("notifications", elem);
        this.statusesCount = getChildInt("statuses_count", elem);
        this.geoEnabled = getChildBoolean("geo_enabled", elem);
        this.verified = getChildBoolean("verified", elem);
        NodeList statuses = elem.getElementsByTagName("status");
        if (statuses.getLength() != 0) {
            Element status = (Element) statuses.item(0);
            this.statusCreatedAt = getChildDate("created_at", status);
            this.statusId = getChildLong("id", status);
            this.statusText = getChildText("text", status);
            this.statusSource = getChildText("source", status);
            this.statusTruncated = getChildBoolean("truncated", status);
            this.statusInReplyToStatusId = getChildLong("in_reply_to_status_id", status);
            this.statusInReplyToUserId = getChildInt("in_reply_to_user_id", status);
            this.statusFavorited = getChildBoolean("favorited", status);
            this.statusInReplyToScreenName = getChildText("in_reply_to_screen_name", status);
        }
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getScreenName() {
        return this.screenName;
    }

    public String getLocation() {
        return this.location;
    }

    public String getDescription() {
        return this.description;
    }

    public URL getProfileImageURL() {
        try {
            return new URL(this.profileImageUrl);
        } catch (MalformedURLException e) {
            return null;
        }
    }

    public URL getURL() {
        try {
            return new URL(this.url);
        } catch (MalformedURLException e) {
            return null;
        }
    }

    public boolean isProtected() {
        return this.isProtected;
    }

    public int getFollowersCount() {
        return this.followersCount;
    }

    public DirectMessage sendDirectMessage(String text) throws WeiboException {
        return this.weibo.sendDirectMessage(getName(), text);
    }

    public static List<User> constructUsers(Response res, Weibo weibo2) throws WeiboException {
        Document doc = res.asDocument();
        if (isRootNodeNilClasses(doc)) {
            return new ArrayList(0);
        }
        try {
            ensureRootNodeNameIs("users", doc);
            NodeList list = doc.getDocumentElement().getChildNodes();
            List<User> users = new ArrayList<>(list.getLength());
            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);
                if (node.getNodeName().equals("user")) {
                    users.add(new User(res, (Element) node, weibo2));
                }
            }
            return users;
        } catch (WeiboException e) {
            WeiboException te = e;
            if (isRootNodeNilClasses(doc)) {
                return new ArrayList(0);
            }
            throw te;
        }
    }

    /* JADX INFO: Multiple debug info for r2v1 long: [D('previousCursor' long), D('list' org.w3c.dom.NodeList)] */
    public static UserWapper constructWapperUsers(Response res, Weibo weibo2) throws WeiboException {
        Document doc = res.asDocument();
        if (isRootNodeNilClasses(doc)) {
            return new UserWapper(new ArrayList(0), 0, 0);
        }
        try {
            ensureRootNodeNameIs("users_list", doc);
            Element root = doc.getDocumentElement();
            NodeList user = root.getElementsByTagName("users");
            if (user.getLength() == 0) {
                return new UserWapper(new ArrayList(0), 0, 0);
            }
            NodeList list = ((Element) user.item(0)).getChildNodes();
            List<User> users = new ArrayList<>(list.getLength());
            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);
                if (node.getNodeName().equals("user")) {
                    users.add(new User(res, (Element) node, weibo2));
                }
            }
            long previousCursor = getChildLong("previous_curosr", root);
            long nextCursor = getChildLong("next_curosr", root);
            if (nextCursor == -1) {
                nextCursor = getChildLong("nextCurosr", root);
            }
            return new UserWapper(users, previousCursor, nextCursor);
        } catch (WeiboException te) {
            if (isRootNodeNilClasses(doc)) {
                return new UserWapper(new ArrayList(0), 0, 0);
            }
            throw te;
        }
    }

    public static List<User> constructUsers(Response res) throws WeiboException {
        try {
            JSONArray list = res.asJSONArray();
            int size = list.length();
            List<User> users = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                users.add(new User(list.getJSONObject(i)));
            }
            return users;
        } catch (JSONException e) {
            throw new WeiboException(e);
        } catch (WeiboException e2) {
            throw e2;
        }
    }

    public static UserWapper constructWapperUsers(Response res) throws WeiboException {
        JSONObject jsonUsers = res.asJSONObject();
        try {
            JSONArray user = jsonUsers.getJSONArray("users");
            int size = user.length();
            List<User> users = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                users.add(new User(user.getJSONObject(i)));
            }
            long previousCursor = jsonUsers.getLong("previous_curosr");
            long nextCursor = jsonUsers.getLong("next_cursor");
            if (nextCursor == -1) {
                nextCursor = jsonUsers.getLong("nextCursor");
            }
            return new UserWapper(users, previousCursor, nextCursor);
        } catch (JSONException e) {
            throw new WeiboException(e);
        }
    }

    static List<User> constructResult(Response res) throws WeiboException {
        JSONArray list = res.asJSONArray();
        try {
            int size = list.length();
            List<User> users = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                users.add(new User(list.getJSONObject(i)));
            }
            return users;
        } catch (JSONException e) {
            return null;
        }
    }

    public Date getStatusCreatedAt() {
        return this.statusCreatedAt;
    }

    public long getStatusId() {
        return this.statusId;
    }

    public String getStatusText() {
        return this.statusText;
    }

    public String getStatusSource() {
        return this.statusSource;
    }

    public boolean isStatusTruncated() {
        return this.statusTruncated;
    }

    public long getStatusInReplyToStatusId() {
        return this.statusInReplyToStatusId;
    }

    public int getStatusInReplyToUserId() {
        return this.statusInReplyToUserId;
    }

    public boolean isStatusFavorited() {
        return this.statusFavorited;
    }

    public String getStatusInReplyToScreenName() {
        if (-1 != this.statusInReplyToUserId) {
            return this.statusInReplyToScreenName;
        }
        return null;
    }

    public String getProfileBackgroundColor() {
        return this.profileBackgroundColor;
    }

    public String getProfileTextColor() {
        return this.profileTextColor;
    }

    public String getProfileLinkColor() {
        return this.profileLinkColor;
    }

    public String getProfileSidebarFillColor() {
        return this.profileSidebarFillColor;
    }

    public String getProfileSidebarBorderColor() {
        return this.profileSidebarBorderColor;
    }

    public int getFriendsCount() {
        return this.friendsCount;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public int getFavouritesCount() {
        return this.favouritesCount;
    }

    public int getUtcOffset() {
        return this.utcOffset;
    }

    public String getTimeZone() {
        return this.timeZone;
    }

    public String getProfileBackgroundImageUrl() {
        return this.profileBackgroundImageUrl;
    }

    public String getProfileBackgroundTile() {
        return this.profileBackgroundTile;
    }

    public boolean isFollowing() {
        return this.following;
    }

    public boolean isNotifications() {
        return this.notificationEnabled;
    }

    public boolean isNotificationEnabled() {
        return this.notificationEnabled;
    }

    public int getStatusesCount() {
        return this.statusesCount;
    }

    public boolean isGeoEnabled() {
        return this.geoEnabled;
    }

    public boolean isVerified() {
        return this.verified;
    }

    public String getDomain() {
        return this.domain;
    }

    public void setDomain(String domain2) {
        this.domain = domain2;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender2) {
        this.gender = gender2;
    }

    public int hashCode() {
        return this.id;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return (obj instanceof User) && ((User) obj).id == this.id;
    }

    public String toString() {
        return "User{weibo=" + this.weibo + ", id=" + this.id + ", name='" + this.name + '\'' + ", screenName='" + this.screenName + '\'' + ", location='" + this.location + '\'' + ", description='" + this.description + '\'' + ", profileImageUrl='" + this.profileImageUrl + '\'' + ", url='" + this.url + '\'' + ", isProtected=" + this.isProtected + ", followersCount=" + this.followersCount + ", statusCreatedAt=" + this.statusCreatedAt + ", statusId=" + this.statusId + ", statusText='" + this.statusText + '\'' + ", statusSource='" + this.statusSource + '\'' + ", statusTruncated=" + this.statusTruncated + ", statusInReplyToStatusId=" + this.statusInReplyToStatusId + ", statusInReplyToUserId=" + this.statusInReplyToUserId + ", statusFavorited=" + this.statusFavorited + ", statusInReplyToScreenName='" + this.statusInReplyToScreenName + '\'' + ", profileBackgroundColor='" + this.profileBackgroundColor + '\'' + ", profileTextColor='" + this.profileTextColor + '\'' + ", profileLinkColor='" + this.profileLinkColor + '\'' + ", profileSidebarFillColor='" + this.profileSidebarFillColor + '\'' + ", profileSidebarBorderColor='" + this.profileSidebarBorderColor + '\'' + ", friendsCount=" + this.friendsCount + ", createdAt=" + this.createdAt + ", favouritesCount=" + this.favouritesCount + ", utcOffset=" + this.utcOffset + ", timeZone='" + this.timeZone + '\'' + ", profileBackgroundImageUrl='" + this.profileBackgroundImageUrl + '\'' + ", profileBackgroundTile='" + this.profileBackgroundTile + '\'' + ", following=" + this.following + ", notificationEnabled=" + this.notificationEnabled + ", statusesCount=" + this.statusesCount + ", geoEnabled=" + this.geoEnabled + ", verified=" + this.verified + ", domain=" + this.domain + ", gender=" + this.gender + '}';
    }
}
