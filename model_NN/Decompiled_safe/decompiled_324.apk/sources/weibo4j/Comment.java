package weibo4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.httpclient.cookie.Cookie2;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import weibo4j.http.Response;
import weibo4j.org.json.JSONArray;
import weibo4j.org.json.JSONException;
import weibo4j.org.json.JSONObject;

public class Comment extends WeiboResponse implements Serializable {
    private static final long serialVersionUID = 1608000492860584608L;
    private Date createdAt;
    private long id;
    private String inReplyToScreenName;
    private long inReplyToStatusId;
    private int inReplyToUserId;
    private boolean isFavorited;
    private boolean isTruncated;
    private double latitude = -1.0d;
    private double longitude = -1.0d;
    private RetweetDetails retweetDetails;
    private String source;
    private String text;
    private User user = null;

    Comment(Response res, Weibo weibo) throws WeiboException {
        super(res);
        init(res, res.asDocument().getDocumentElement(), weibo);
    }

    Comment(Response res) throws WeiboException {
        super(res);
        JSONObject json = res.asJSONObject();
        try {
            this.id = json.getLong("id");
            this.text = json.getString("text");
            this.source = json.getString("source");
            this.createdAt = parseDate(json.getString("created_at"), "EEE MMM dd HH:mm:ss z yyyy");
            if (!json.isNull("user")) {
                this.user = new User(json.getJSONObject("user"));
            }
        } catch (JSONException e) {
            JSONException je = e;
            throw new WeiboException(je.getMessage() + ":" + json.toString(), je);
        }
    }

    public Comment(JSONObject json) throws WeiboException, JSONException {
        this.id = json.getLong("id");
        this.text = json.getString("text");
        this.source = json.getString("source");
        this.createdAt = parseDate(json.getString("created_at"), "EEE MMM dd HH:mm:ss z yyyy");
        if (!json.isNull("user")) {
            this.user = new User(json.getJSONObject("user"));
        }
    }

    Comment(Response res, Element elem, Weibo weibo) throws WeiboException {
        super(res);
        init(res, elem, weibo);
    }

    public Comment(String str) throws WeiboException, JSONException {
        JSONObject json = new JSONObject(str);
        this.id = json.getLong("id");
        this.text = json.getString("text");
        this.source = json.getString("source");
        this.createdAt = parseDate(json.getString("created_at"), "EEE MMM dd HH:mm:ss z yyyy");
        this.user = new User(json.getJSONObject("user"));
    }

    private void init(Response res, Element elem, Weibo weibo) throws WeiboException {
        ensureRootNodeNameIs(Cookie2.COMMENT, elem);
        this.user = new User(res, (Element) elem.getElementsByTagName("user").item(0), weibo);
        this.id = getChildLong("id", elem);
        this.text = getChildText("text", elem);
        this.source = getChildText("source", elem);
        this.createdAt = getChildDate("created_at", elem);
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public long getId() {
        return this.id;
    }

    public String getText() {
        return this.text;
    }

    public String getSource() {
        return this.source;
    }

    public boolean isTruncated() {
        return this.isTruncated;
    }

    public long getInReplyToStatusId() {
        return this.inReplyToStatusId;
    }

    public int getInReplyToUserId() {
        return this.inReplyToUserId;
    }

    public String getInReplyToScreenName() {
        return this.inReplyToScreenName;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public boolean isFavorited() {
        return this.isFavorited;
    }

    public User getUser() {
        return this.user;
    }

    public boolean isRetweet() {
        return this.retweetDetails != null;
    }

    public RetweetDetails getRetweetDetails() {
        return this.retweetDetails;
    }

    static List<Comment> constructStatuses(Response res, Weibo weibo) throws WeiboException {
        Document doc = res.asDocument();
        if (isRootNodeNilClasses(doc)) {
            return new ArrayList(0);
        }
        try {
            ensureRootNodeNameIs("statuses", doc);
            NodeList list = doc.getDocumentElement().getElementsByTagName("status");
            int size = list.getLength();
            List<Comment> statuses = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                statuses.add(new Comment(res, (Element) list.item(i), weibo));
            }
            return statuses;
        } catch (WeiboException e) {
            ensureRootNodeNameIs("nil-classes", doc);
            return new ArrayList(0);
        }
    }

    static List<Comment> constructComments(Response res) throws WeiboException {
        try {
            JSONArray list = res.asJSONArray();
            int size = list.length();
            List<Comment> comments = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                comments.add(new Comment(list.getJSONObject(i)));
            }
            return comments;
        } catch (JSONException e) {
            throw new WeiboException(e);
        } catch (WeiboException e2) {
            throw e2;
        }
    }

    public int hashCode() {
        return (int) this.id;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return (obj instanceof Comment) && ((Comment) obj).id == this.id;
    }

    public String toString() {
        return "Comment{createdAt=" + this.createdAt + ", id=" + this.id + ", text='" + this.text + '\'' + ", source='" + this.source + '\'' + ", isTruncated=" + this.isTruncated + ", inReplyToStatusId=" + this.inReplyToStatusId + ", inReplyToUserId=" + this.inReplyToUserId + ", isFavorited=" + this.isFavorited + ", inReplyToScreenName='" + this.inReplyToScreenName + '\'' + ", latitude=" + this.latitude + ", longitude=" + this.longitude + ", retweetDetails=" + this.retweetDetails + ", user=" + this.user + '}';
    }
}
