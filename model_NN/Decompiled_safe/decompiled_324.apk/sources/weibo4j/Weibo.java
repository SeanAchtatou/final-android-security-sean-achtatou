package weibo4j;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import org.apache.commons.httpclient.cookie.Cookie2;
import org.apache.commons.httpclient.cookie.CookieSpec;
import weibo4j.http.AccessToken;
import weibo4j.http.HttpClient;
import weibo4j.http.ImageItem;
import weibo4j.http.PostParameter;
import weibo4j.http.RequestToken;
import weibo4j.http.Response;
import weibo4j.org.json.JSONException;
import weibo4j.org.json.JSONObject;

public class Weibo extends WeiboSupport implements Serializable {
    public static final String CONSUMER_KEY = "1148902872";
    public static final String CONSUMER_SECRET = "3bb48f85576f8eba082d258c2fcb6832";
    public static final Device IM = new Device("im");
    public static final Device NONE = new Device("none");
    public static final Device SMS = new Device("sms");
    private static final long serialVersionUID = -1486360080128882436L;
    public static String userId;
    private String baseURL;
    private SimpleDateFormat format;
    private String searchBaseURL;

    public /* bridge */ /* synthetic */ void forceUsePost(boolean x0) {
        super.forceUsePost(x0);
    }

    public /* bridge */ /* synthetic */ String getClientURL() {
        return super.getClientURL();
    }

    public /* bridge */ /* synthetic */ String getClientVersion() {
        return super.getClientVersion();
    }

    public /* bridge */ /* synthetic */ String getPassword() {
        return super.getPassword();
    }

    public /* bridge */ /* synthetic */ String getSource() {
        return super.getSource();
    }

    public /* bridge */ /* synthetic */ String getUserAgent() {
        return super.getUserAgent();
    }

    public /* bridge */ /* synthetic */ String getUserId() {
        return super.getUserId();
    }

    public /* bridge */ /* synthetic */ boolean isUsePostForced() {
        return super.isUsePostForced();
    }

    public /* bridge */ /* synthetic */ void setClientURL(String x0) {
        super.setClientURL(x0);
    }

    public /* bridge */ /* synthetic */ void setClientVersion(String x0) {
        super.setClientVersion(x0);
    }

    public /* bridge */ /* synthetic */ void setHttpConnectionTimeout(int x0) {
        super.setHttpConnectionTimeout(x0);
    }

    public /* bridge */ /* synthetic */ void setHttpProxy(String x0, int x1) {
        super.setHttpProxy(x0, x1);
    }

    public /* bridge */ /* synthetic */ void setHttpProxyAuth(String x0, String x1) {
        super.setHttpProxyAuth(x0, x1);
    }

    public /* bridge */ /* synthetic */ void setHttpReadTimeout(int x0) {
        super.setHttpReadTimeout(x0);
    }

    public /* bridge */ /* synthetic */ void setPassword(String x0) {
        super.setPassword(x0);
    }

    public /* bridge */ /* synthetic */ void setRequestHeader(String x0, String x1) {
        super.setRequestHeader(x0, x1);
    }

    public /* bridge */ /* synthetic */ void setRetryCount(int x0) {
        super.setRetryCount(x0);
    }

    public /* bridge */ /* synthetic */ void setRetryIntervalSecs(int x0) {
        super.setRetryIntervalSecs(x0);
    }

    public /* bridge */ /* synthetic */ void setSource(String x0) {
        super.setSource(x0);
    }

    public /* bridge */ /* synthetic */ void setUserAgent(String x0) {
        super.setUserAgent(x0);
    }

    public /* bridge */ /* synthetic */ void setUserId(String x0) {
        super.setUserId(x0);
    }

    public Weibo() {
        this.baseURL = Configuration.getScheme() + "api.t.sina.com.cn/";
        this.searchBaseURL = Configuration.getScheme() + "api.t.sina.com.cn/";
        this.format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z", Locale.ENGLISH);
        this.format.setTimeZone(TimeZone.getTimeZone("GMT"));
        http.setRequestTokenURL(Configuration.getScheme() + "api.t.sina.com.cn/oauth/request_token");
        http.setAuthorizationURL(Configuration.getScheme() + "api.t.sina.com.cn/oauth/authorize");
        http.setAccessTokenURL(Configuration.getScheme() + "api.t.sina.com.cn/oauth/access_token");
    }

    public void setToken(String token, String tokenSecret) {
        http.setToken(token, tokenSecret);
    }

    public Weibo(String baseURL2) {
        this();
        this.baseURL = baseURL2;
    }

    public Weibo(String id, String password) {
        this();
        setUserId(id);
        setPassword(password);
    }

    public Weibo(String id, String password, String baseURL2) {
        this();
        setUserId(id);
        setPassword(password);
        this.baseURL = baseURL2;
    }

    public static Weibo instantiate(String id, String password) {
        try {
            return new Weibo().getWeibo(id, password);
        } catch (WeiboException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setBaseURL(String baseURL2) {
        this.baseURL = baseURL2;
    }

    public String getBaseURL() {
        return this.baseURL;
    }

    public void setSearchBaseURL(String searchBaseURL2) {
        this.searchBaseURL = searchBaseURL2;
    }

    public String getSearchBaseURL() {
        return this.searchBaseURL;
    }

    public synchronized void setOAuthConsumer(String consumerKey, String consumerSecret) {
        http.setOAuthConsumer(consumerKey, consumerSecret);
    }

    public RequestToken getOAuthRequestToken() throws WeiboException {
        return http.getOAuthRequestToken();
    }

    public RequestToken getOAuthRequestToken(String callback_url) throws WeiboException {
        return http.getOauthRequestToken(callback_url);
    }

    public synchronized AccessToken getOAuthAccessToken(RequestToken requestToken) throws WeiboException {
        return http.getOAuthAccessToken(requestToken);
    }

    public synchronized AccessToken getOAuthAccessToken(RequestToken requestToken, String pin) throws WeiboException {
        AccessToken accessToken;
        accessToken = http.getOAuthAccessToken(requestToken, pin);
        setUserId(accessToken.getScreenName());
        return accessToken;
    }

    public synchronized AccessToken getOAuthAccessToken(String token, String tokenSecret) throws WeiboException {
        AccessToken accessToken;
        accessToken = http.getOAuthAccessToken(token, tokenSecret);
        setUserId(accessToken.getScreenName());
        return accessToken;
    }

    public synchronized AccessToken getOAuthAccessToken(String token, String tokenSecret, String oauth_verifier) throws WeiboException {
        return http.getOAuthAccessToken(token, tokenSecret, oauth_verifier);
    }

    public void setOAuthAccessToken(AccessToken accessToken) {
        http.setOAuthAccessToken(accessToken);
    }

    public void setOAuthAccessToken(String token, String tokenSecret) {
        setOAuthAccessToken(new AccessToken(token, tokenSecret));
    }

    private Response get(String url, boolean authenticate) throws WeiboException {
        return get(url, null, authenticate);
    }

    /* access modifiers changed from: protected */
    public Response get(String url, String name1, String value1, boolean authenticate) throws WeiboException {
        return get(url, new PostParameter[]{new PostParameter(name1, value1)}, authenticate);
    }

    /* access modifiers changed from: protected */
    public Response get(String url, String name1, String value1, String name2, String value2, boolean authenticate) throws WeiboException {
        return get(url, new PostParameter[]{new PostParameter(name1, value1), new PostParameter(name2, value2)}, authenticate);
    }

    /* access modifiers changed from: protected */
    public Response get(String url, PostParameter[] params, boolean authenticate) throws WeiboException {
        if (url.indexOf("?") == -1) {
            url = url + "?source=1148902872";
        } else if (url.indexOf("source") == -1) {
            url = url + "&source=1148902872";
        }
        if (params != null && params.length > 0) {
            url = url + "&" + HttpClient.encodeParameters(params);
        }
        return http.get(url, authenticate);
    }

    /* access modifiers changed from: protected */
    public Response get(String url, PostParameter[] params, Paging paging, boolean authenticate) throws WeiboException {
        if (paging == null) {
            return get(url, params, authenticate);
        }
        List<PostParameter> pagingParams = new ArrayList<>(4);
        if (-1 != paging.getMaxId()) {
            pagingParams.add(new PostParameter("max_id", String.valueOf(paging.getMaxId())));
        }
        if (-1 != paging.getSinceId()) {
            pagingParams.add(new PostParameter("since_id", String.valueOf(paging.getSinceId())));
        }
        if (-1 != paging.getPage()) {
            pagingParams.add(new PostParameter("page", String.valueOf(paging.getPage())));
        }
        if (-1 != paging.getUserId()) {
            pagingParams.add(new PostParameter("id", String.valueOf(paging.getUserId())));
        }
        if (-1 != paging.getCount()) {
            if (-1 != url.indexOf("search")) {
                pagingParams.add(new PostParameter("rpp", String.valueOf(paging.getCount())));
            } else {
                pagingParams.add(new PostParameter("count", String.valueOf(paging.getCount())));
            }
        }
        PostParameter[] newparams = null;
        PostParameter[] arrayPagingParams = (PostParameter[]) pagingParams.toArray(new PostParameter[pagingParams.size()]);
        if (params != null) {
            newparams = new PostParameter[(params.length + pagingParams.size())];
            System.arraycopy(params, 0, newparams, 0, params.length);
            System.arraycopy(arrayPagingParams, 0, newparams, params.length, pagingParams.size());
        } else if (arrayPagingParams.length != 0) {
            String encodedParams = HttpClient.encodeParameters(arrayPagingParams);
            if (-1 != url.indexOf("?")) {
                url = url + "&source=1148902872&" + encodedParams;
            } else {
                url = url + "?source=1148902872&" + encodedParams;
            }
        }
        return get(url, newparams, authenticate);
    }

    public QueryResult search(Query query) throws WeiboException {
        try {
            return new QueryResult(get(this.searchBaseURL + "search.json", query.asPostParameters(), false), this);
        } catch (WeiboException e) {
            WeiboException te = e;
            if (404 == te.getStatusCode()) {
                return new QueryResult(query);
            }
            throw te;
        }
    }

    public Trends getTrends() throws WeiboException {
        return Trends.constructTrends(get(this.searchBaseURL + "trends.json", false));
    }

    public Trends getCurrentTrends() throws WeiboException {
        return Trends.constructTrendsList(get(this.searchBaseURL + "trends/current.json", false)).get(0);
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Trends getCurrentTrends(boolean excludeHashTags) throws WeiboException {
        return Trends.constructTrendsList(get(this.searchBaseURL + "trends/current.json" + (excludeHashTags ? "?exclude=hashtags" : ""), false)).get(0);
    }

    public List<Trends> getDailyTrends() throws WeiboException {
        return Trends.constructTrendsList(get(this.searchBaseURL + "trends/daily.json", false));
    }

    public List<Trends> getDailyTrends(Date date, boolean excludeHashTags) throws WeiboException {
        return Trends.constructTrendsList(get(this.searchBaseURL + "trends/daily.json?date=" + toDateStr(date) + (excludeHashTags ? "&exclude=hashtags" : ""), false));
    }

    private String toDateStr(Date date) {
        if (date == null) {
            date = new Date();
        }
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public List<Trends> getWeeklyTrends() throws WeiboException {
        return Trends.constructTrendsList(get(this.searchBaseURL + "trends/weekly.json", false));
    }

    public List<Trends> getWeeklyTrends(Date date, boolean excludeHashTags) throws WeiboException {
        return Trends.constructTrendsList(get(this.searchBaseURL + "trends/weekly.json?date=" + toDateStr(date) + (excludeHashTags ? "&exclude=hashtags" : ""), false));
    }

    public List<Status> getPublicTimeline() throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/public_timeline.json", true));
    }

    public RateLimitStatus getRateLimitStatus() throws WeiboException {
        return new RateLimitStatus(get(getBaseURL() + "account/rate_limit_status.json", true), this);
    }

    public List<Status> getPublicTimeline(int sinceID) throws WeiboException {
        return getPublicTimeline((long) sinceID);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public List<Status> getPublicTimeline(long sinceID) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/public_timeline.json", (PostParameter[]) null, new Paging(sinceID), false));
    }

    public List<Status> getHomeTimeline() throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/home_timeline.json", true));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public List<Status> getHomeTimeline(Paging paging) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/home_timeline.json", (PostParameter[]) null, paging, true));
    }

    public List<Status> getFriendsTimeline() throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/friends_timeline.json", true));
    }

    public List<Status> getFriendsTimelineByPage(int page) throws WeiboException {
        return getFriendsTimeline(new Paging(page));
    }

    public List<Status> getFriendsTimeline(int page) throws WeiboException {
        return getFriendsTimeline(new Paging(page));
    }

    public List<Status> getFriendsTimeline(long sinceId, int page) throws WeiboException {
        return getFriendsTimeline(new Paging(page).sinceId(sinceId));
    }

    public List<Status> getFriendsTimeline(String id) throws WeiboException {
        throw new IllegalStateException("The Weibo API is not supporting this method anymore");
    }

    public List<Status> getFriendsTimelineByPage(String id, int page) throws WeiboException {
        throw new IllegalStateException("The Weibo API is not supporting this method anymore");
    }

    public List<Status> getFriendsTimeline(String id, int page) throws WeiboException {
        throw new IllegalStateException("The Weibo API is not supporting this method anymore");
    }

    public List<Status> getFriendsTimeline(long sinceId, String id, int page) throws WeiboException {
        throw new IllegalStateException("The Weibo API is not supporting this method anymore");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public List<Status> getFriendsTimeline(Paging paging) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/friends_timeline.json", (PostParameter[]) null, paging, true));
    }

    public List<Status> getFriendsTimeline(String id, Paging paging) throws WeiboException {
        throw new IllegalStateException("The Weibo API is not supporting this method anymore");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response */
    public List<Status> getFriendsTimeline(Date since) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/friends_timeline.xml", "since", this.format.format(since), true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response */
    public List<Status> getFriendsTimeline(long sinceId) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/friends_timeline.xml", "since_id", String.valueOf(sinceId), true), this);
    }

    public List<Status> getFriendsTimeline(String id, Date since) throws WeiboException {
        throw new IllegalStateException("The Weibo API is not supporting this method anymore");
    }

    public List<Status> getFriendsTimeline(String id, long sinceId) throws WeiboException {
        throw new IllegalStateException("The Weibo API is not supporting this method anymore");
    }

    public List<Status> getUserTimeline(String id, int count, Date since) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/user_timeline/" + id + ".xml", "since", this.format.format(since), "count", String.valueOf(count), http.isAuthenticationEnabled()), this);
    }

    public List<Status> getUserTimeline(String id, int count, long sinceId) throws WeiboException {
        return getUserTimeline(id, new Paging(sinceId).count(count));
    }

    public List<Status> getUserTimeline(String id, Paging paging) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/user_timeline/" + id + ".xml", (PostParameter[]) null, paging, http.isAuthenticationEnabled()), this);
    }

    public List<Status> getUserTimeline(String id, Date since) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/user_timeline/" + id + ".xml", "since", this.format.format(since), http.isAuthenticationEnabled()), this);
    }

    public List<Status> getUserTimeline(String id, int count) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/user_timeline/" + id + ".xml", "count", String.valueOf(count), http.isAuthenticationEnabled()), this);
    }

    public List<Status> getUserTimeline(int count, Date since) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/user_timeline.xml", "since", this.format.format(since), "count", String.valueOf(count), true), this);
    }

    public List<Status> getUserTimeline(int count, long sinceId) throws WeiboException {
        return getUserTimeline(new Paging(sinceId).count(count));
    }

    public List<Status> getUserTimeline(String id) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/user_timeline/" + id + ".json", http.isAuthenticationEnabled()));
    }

    public List<Status> getUserTimeline(String id, long sinceId) throws WeiboException {
        return getUserTimeline(id, new Paging(sinceId));
    }

    public List<Status> getUserTimeline() throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/user_timeline.json", true));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public List<Status> getUserTimeline(Paging paging) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/user_timeline.json", (PostParameter[]) null, paging, true));
    }

    public List<Status> getUserTimeline(long sinceId) throws WeiboException {
        return getUserTimeline(new Paging(sinceId));
    }

    public List<Status> getReplies() throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/replies.xml", true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response */
    public List<Status> getReplies(long sinceId) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/replies.xml", "since_id", String.valueOf(sinceId), true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response */
    public List<Status> getRepliesByPage(int page) throws WeiboException {
        if (page >= 1) {
            return Status.constructStatuses(get(getBaseURL() + "statuses/replies.xml", "page", String.valueOf(page), true), this);
        }
        throw new IllegalArgumentException("page should be positive integer. passed:" + page);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response */
    public List<Status> getReplies(int page) throws WeiboException {
        if (page >= 1) {
            return Status.constructStatuses(get(getBaseURL() + "statuses/replies.xml", "page", String.valueOf(page), true), this);
        }
        throw new IllegalArgumentException("page should be positive integer. passed:" + page);
    }

    public List<Status> getReplies(long sinceId, int page) throws WeiboException {
        if (page >= 1) {
            return Status.constructStatuses(get(getBaseURL() + "statuses/replies.xml", "since_id", String.valueOf(sinceId), "page", String.valueOf(page), true), this);
        }
        throw new IllegalArgumentException("page should be positive integer. passed:" + page);
    }

    public List<Status> getMentions() throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/mentions.json", null, true));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public List<Status> getMentions(Paging paging) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/mentions.json", (PostParameter[]) null, paging, true));
    }

    public List<Status> getRetweetedByMe() throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/retweeted_by_me.json", null, true));
    }

    public List<Status> getRetweetedByMe(Paging paging) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/retweeted_by_me.json", null, true));
    }

    public List<Status> getRetweetedToMe() throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/retweeted_to_me.json", null, true));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public List<Status> getRetweetedToMe(Paging paging) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/retweeted_to_me.json", (PostParameter[]) null, paging, true));
    }

    public List<Status> getRetweetsOfMe() throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/retweets_of_me.json", null, true));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public List<Status> getRetweetsOfMe(Paging paging) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "statuses/retweets_of_me.json", (PostParameter[]) null, paging, true));
    }

    public Status show(int id) throws WeiboException {
        return showStatus((long) id);
    }

    public Status show(long id) throws WeiboException {
        return new Status(get(getBaseURL() + "statuses/show/" + id + ".xml", false), this);
    }

    public Status showStatus(long id) throws WeiboException {
        return new Status(get(getBaseURL() + "statuses/show/" + id + ".json", true));
    }

    public Status update(String status) throws WeiboException {
        return updateStatus(status);
    }

    public Status updateStatus(String status) throws WeiboException {
        return new Status(http.post(getBaseURL() + "statuses/update.json", new PostParameter[]{new PostParameter("status", status)}, true));
    }

    public Comment updateComment(String comment, String id, String cid) throws WeiboException {
        return new Comment(http.post(getBaseURL() + "statuses/comment.json", cid == null ? new PostParameter[]{new PostParameter((String) Cookie2.COMMENT, comment), new PostParameter("id", id)} : new PostParameter[]{new PostParameter((String) Cookie2.COMMENT, comment), new PostParameter("cid", cid), new PostParameter("id", id)}, true));
    }

    public Comment updateComment(String comment, String id, String cid, int comment_ori) throws WeiboException {
        return new Comment(http.post(getBaseURL() + "statuses/comment.json", cid == null ? new PostParameter[]{new PostParameter((String) Cookie2.COMMENT, comment), new PostParameter("id", id), new PostParameter("comment_ori", comment_ori)} : new PostParameter[]{new PostParameter((String) Cookie2.COMMENT, comment), new PostParameter("cid", cid), new PostParameter("id", id), new PostParameter("comment_ori", comment_ori)}, true));
    }

    public Status uploadStatus(String status, ImageItem item) throws WeiboException {
        return new Status(http.multPartURL(getBaseURL() + "statuses/upload.json", new PostParameter[]{new PostParameter("status", status), new PostParameter("source", this.source)}, item, true));
    }

    public Status uploadStatus(String status, File file) throws WeiboException {
        return new Status(http.multPartURL("pic", getBaseURL() + "statuses/upload.json", new PostParameter[]{new PostParameter("status", status), new PostParameter("source", this.source)}, file, true));
    }

    public Status updateStatus(String status, double latitude, double longitude) throws WeiboException, JSONException {
        return new Status(http.post(getBaseURL() + "statuses/update.json", new PostParameter[]{new PostParameter("status", status), new PostParameter("lat", latitude), new PostParameter("long", longitude)}, true));
    }

    public Status update(String status, long inReplyToStatusId) throws WeiboException {
        return updateStatus(status, inReplyToStatusId);
    }

    public Status updateStatus(String status, long inReplyToStatusId) throws WeiboException {
        return new Status(http.post(getBaseURL() + "statuses/update.json", new PostParameter[]{new PostParameter("status", status), new PostParameter("in_reply_to_status_id", String.valueOf(inReplyToStatusId)), new PostParameter("source", this.source)}, true));
    }

    public Status updateStatus(String status, long inReplyToStatusId, double latitude, double longitude) throws WeiboException {
        return new Status(http.post(getBaseURL() + "statuses/update.json", new PostParameter[]{new PostParameter("status", status), new PostParameter("lat", latitude), new PostParameter("long", longitude), new PostParameter("in_reply_to_status_id", String.valueOf(inReplyToStatusId)), new PostParameter("source", this.source)}, true));
    }

    public Status destroyStatus(long statusId) throws WeiboException {
        return new Status(http.post(getBaseURL() + "statuses/destroy/" + statusId + ".json", new PostParameter[0], true));
    }

    public Comment destroyComment(long commentId) throws WeiboException {
        return new Comment(http.delete(getBaseURL() + "statuses/comment_destroy/" + commentId + ".json?source=" + CONSUMER_KEY, true));
    }

    public Status retweetStatus(long statusId) throws WeiboException {
        return new Status(http.post(getBaseURL() + "statuses/retweet/" + statusId + ".json", new PostParameter[0], true));
    }

    public List<RetweetDetails> getRetweets(long statusId) throws WeiboException {
        return RetweetDetails.createRetweetDetails(get(getBaseURL() + "statuses/retweets/" + statusId + ".json", true));
    }

    public User getUserDetail(String id) throws WeiboException {
        return showUser(id);
    }

    public User showUser(String id) throws WeiboException {
        return new User(get(getBaseURL() + "users/show/" + id + ".json", http.isAuthenticationEnabled()).asJSONObject());
    }

    public List<User> getFriends() throws WeiboException {
        return getFriendsStatuses();
    }

    public List<User> getFriendsStatuses() throws WeiboException {
        return User.constructResult(get(getBaseURL() + "statuses/friends.json", true));
    }

    public List<User> getFriends(Paging paging) throws WeiboException {
        return getFriendsStatuses(paging);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public List<User> getFriendsStatuses(Paging paging) throws WeiboException {
        return User.constructUsers(get(getBaseURL() + "statuses/friends.json", (PostParameter[]) null, paging, true));
    }

    public List<User> getFriends(int page) throws WeiboException {
        return getFriendsStatuses(new Paging(page));
    }

    public List<User> getFriends(String id) throws WeiboException {
        return getFriendsStatuses(id);
    }

    public List<User> getFriendsStatuses(String id) throws WeiboException {
        return User.constructUsers(get(getBaseURL() + "statuses/friends/" + id + ".json", false));
    }

    public List<User> getFriends(String id, Paging paging) throws WeiboException {
        return getFriendsStatuses(id, paging);
    }

    public List<User> getFriendsStatuses(String id, Paging paging) throws WeiboException {
        return User.constructUsers(get(getBaseURL() + "statuses/friends/" + id + ".json", false));
    }

    public List<User> getFriends(String id, int page) throws WeiboException {
        return getFriendsStatuses(id, new Paging(page));
    }

    public List<User> getFollowers() throws WeiboException {
        return getFollowersStatuses();
    }

    public List<User> getFollowersStatuses() throws WeiboException {
        return User.constructResult(get(getBaseURL() + "statuses/followers.json", true));
    }

    public List<User> getFollowers(Paging paging) throws WeiboException {
        return getFollowersStatuses(paging);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public List<User> getFollowersStatuses(Paging paging) throws WeiboException {
        return User.constructUsers(get(getBaseURL() + "statuses/followers.json", (PostParameter[]) null, paging, true));
    }

    public List<User> getFollowers(int page) throws WeiboException {
        return getFollowersStatuses(new Paging(page));
    }

    public List<User> getFollowers(String id) throws WeiboException {
        return getFollowersStatuses(id);
    }

    public List<User> getFollowersStatuses(String id) throws WeiboException {
        return User.constructUsers(get(getBaseURL() + "statuses/followers/" + id + ".json", true));
    }

    public List<User> getFollowers(String id, Paging paging) throws WeiboException {
        return getFollowersStatuses(id, paging);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public List<User> getFollowersStatuses(String id, Paging paging) throws WeiboException {
        return User.constructUsers(get(getBaseURL() + "statuses/followers/" + id + ".json", (PostParameter[]) null, paging, true));
    }

    public List<User> getFollowers(String id, int page) throws WeiboException {
        return getFollowersStatuses(id, new Paging(page));
    }

    public List<User> getFeatured() throws WeiboException {
        return User.constructUsers(get(getBaseURL() + "statuses/featured.json", true));
    }

    public List<DirectMessage> getDirectMessages() throws WeiboException {
        return DirectMessage.constructDirectMessages(get(getBaseURL() + "direct_messages.json", true));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public List<DirectMessage> getDirectMessages(Paging paging) throws WeiboException {
        return DirectMessage.constructDirectMessages(get(getBaseURL() + "direct_messages.json", (PostParameter[]) null, paging, true));
    }

    public List<DirectMessage> getDirectMessagesByPage(int page) throws WeiboException {
        return getDirectMessages(new Paging(page));
    }

    public List<DirectMessage> getDirectMessages(int page, int sinceId) throws WeiboException {
        return getDirectMessages(new Paging(page).sinceId(sinceId));
    }

    public List<DirectMessage> getDirectMessages(int sinceId) throws WeiboException {
        return getDirectMessages(new Paging((long) sinceId));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response */
    public List<DirectMessage> getDirectMessages(Date since) throws WeiboException {
        return DirectMessage.constructDirectMessages(get(getBaseURL() + "direct_messages.xml", "since", this.format.format(since), true), this);
    }

    public List<DirectMessage> getSentDirectMessages() throws WeiboException {
        return DirectMessage.constructDirectMessages(get(getBaseURL() + "direct_messages/sent.json", new PostParameter[0], true));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public List<DirectMessage> getSentDirectMessages(Paging paging) throws WeiboException {
        return DirectMessage.constructDirectMessages(get(getBaseURL() + "direct_messages/sent.json", new PostParameter[0], paging, true));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response */
    public List<DirectMessage> getSentDirectMessages(Date since) throws WeiboException {
        return DirectMessage.constructDirectMessages(get(getBaseURL() + "direct_messages/sent.xml", "since", this.format.format(since), true), this);
    }

    public List<DirectMessage> getSentDirectMessages(int sinceId) throws WeiboException {
        return getSentDirectMessages(new Paging((long) sinceId));
    }

    public List<DirectMessage> getSentDirectMessages(int page, int sinceId) throws WeiboException {
        return getSentDirectMessages(new Paging(page, (long) sinceId));
    }

    public DirectMessage sendDirectMessage(String id, String text) throws WeiboException {
        return new DirectMessage(http.post(getBaseURL() + "direct_messages/new.json", new PostParameter[]{new PostParameter("user_id", id), new PostParameter("text", text), new PostParameter("source", this.source)}, true).asJSONObject());
    }

    public DirectMessage deleteDirectMessage(int id) throws WeiboException {
        return destroyDirectMessage(id);
    }

    public DirectMessage destroyDirectMessage(int id) throws WeiboException {
        return new DirectMessage(http.post(getBaseURL() + "direct_messages/destroy/" + id + ".json", new PostParameter[0], true).asJSONObject());
    }

    public User create(String id) throws WeiboException {
        return createFriendship(id);
    }

    public User createFriendship(String id) throws WeiboException {
        return new User(http.post(getBaseURL() + "friendships/create/" + id + ".json", new PostParameter[0], true).asJSONObject());
    }

    public User createFriendship(String id, boolean follow) throws WeiboException {
        return new User(http.post(getBaseURL() + "friendships/create/" + id + ".json", new PostParameter[]{new PostParameter("follow", String.valueOf(follow))}, true).asJSONObject());
    }

    public User destroy(String id) throws WeiboException {
        return destroyFriendship(id);
    }

    public User destroyFriendship(String id) throws WeiboException {
        return new User(http.post(getBaseURL() + "friendships/destroy/" + id + ".json", new PostParameter[0], true).asJSONObject());
    }

    public boolean exists(String userA, String userB) throws WeiboException {
        return existsFriendship(userA, userB);
    }

    public boolean existsFriendship(String userA, String userB) throws WeiboException {
        return -1 != get(new StringBuilder().append(getBaseURL()).append("friendships/exists.json").toString(), "user_a", userA, "user_b", userB, true).asString().indexOf("true");
    }

    public IDs getFriendsIDs() throws WeiboException {
        return getFriendsIDs(-1L);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public IDs getFriendsIDs(Paging paging) throws WeiboException {
        return new IDs(get(getBaseURL() + "friends/ids.xml", (PostParameter[]) null, paging, true));
    }

    public IDs getFriendsIDs(long cursor) throws WeiboException {
        return new IDs(get(getBaseURL() + "friends/ids.xml?cursor=" + cursor, true));
    }

    public IDs getFriendsIDs(int userId2) throws WeiboException {
        return getFriendsIDs(userId2, -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public IDs getFriendsIDs(int userId2, Paging paging) throws WeiboException {
        return new IDs(get(getBaseURL() + "friends/ids.xml?user_id=" + userId2, (PostParameter[]) null, paging, true));
    }

    public IDs getFriendsIDs(int userId2, long cursor) throws WeiboException {
        return new IDs(get(getBaseURL() + "friends/ids.json?user_id=" + userId2 + "&cursor=" + cursor, true), this);
    }

    public IDs getFriendsIDs(String screenName) throws WeiboException {
        return getFriendsIDs(screenName, -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public IDs getFriendsIDs(String screenName, Paging paging) throws WeiboException {
        return new IDs(get(getBaseURL() + "friends/ids.xml?screen_name=" + screenName, (PostParameter[]) null, paging, true));
    }

    public IDs getFriendsIDs(String screenName, long cursor) throws WeiboException {
        return new IDs(get(getBaseURL() + "friends/ids.json?screen_name=" + screenName + "&cursor=" + cursor, true), this);
    }

    public IDs getFollowersIDs() throws WeiboException {
        return getFollowersIDs(-1L);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public IDs getFollowersIDs(Paging paging) throws WeiboException {
        return new IDs(get(getBaseURL() + "followers/ids.xml", (PostParameter[]) null, paging, true));
    }

    public IDs getFollowersIDs(long cursor) throws WeiboException {
        return new IDs(get(getBaseURL() + "followers/ids.json?cursor=" + cursor, true), this);
    }

    public IDs getFollowersIDs(int userId2) throws WeiboException {
        return getFollowersIDs(userId2, -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public IDs getFollowersIDs(int userId2, Paging paging) throws WeiboException {
        return new IDs(get(getBaseURL() + "followers/ids.xml?user_id=" + userId2, (PostParameter[]) null, paging, true));
    }

    public IDs getFollowersIDs(int userId2, long cursor) throws WeiboException {
        return new IDs(get(getBaseURL() + "followers/ids.xml?user_id=" + userId2 + "&cursor=" + cursor, true));
    }

    public IDs getFollowersIDs(String screenName) throws WeiboException {
        return getFollowersIDs(screenName, -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public IDs getFollowersIDs(String screenName, Paging paging) throws WeiboException {
        return new IDs(get(getBaseURL() + "followers/ids.xml?screen_name=" + screenName, (PostParameter[]) null, paging, true));
    }

    public IDs getFollowersIDs(String screenName, long cursor) throws WeiboException {
        return new IDs(get(getBaseURL() + "followers/ids.json?screen_name=" + screenName + "&cursor=" + cursor, true), this);
    }

    public User verifyCredentials() throws WeiboException {
        return null;
    }

    public Weibo getWeibo(String id, String password) throws WeiboException {
        Weibo weibo = null;
        String response = post("http://api.t.sina.com.cn/oauth/access_token", new String[]{id, password}, true);
        if (response != null) {
            weibo = new Weibo();
            String[] lines = response.split("&");
            for (int i = 0; i < lines.length; i++) {
                String line = lines[i].trim();
                lines[i] = line.substring(line.indexOf("=") + 1).trim();
            }
            weibo.setToken(lines[0], lines[1]);
            userId = lines[2];
        }
        return weibo;
    }

    /* access modifiers changed from: protected */
    public String post(String url, String[] params, boolean authenticate) throws WeiboException {
        return http.postXaouth(url, params, authenticate);
    }

    public User updateLocation(String location) throws WeiboException {
        return new User(http.post(getBaseURL() + "account/update_location.xml", new PostParameter[]{new PostParameter("location", location)}, true), this);
    }

    public User updateProfile(String name, String email, String url, String location, String description) throws WeiboException {
        List<PostParameter> profile = new ArrayList<>(5);
        addParameterToList(profile, "name", name);
        addParameterToList(profile, "email", email);
        addParameterToList(profile, "url", url);
        addParameterToList(profile, "location", location);
        addParameterToList(profile, "description", description);
        return new User(http.post(getBaseURL() + "account/update_profile.json", (PostParameter[]) profile.toArray(new PostParameter[profile.size()]), true).asJSONObject());
    }

    public User updateProfileImage(File image) throws WeiboException {
        return new User(http.multPartURL("image", getBaseURL() + "account/update_profile_image.json", null, image, true).asJSONObject());
    }

    public RateLimitStatus rateLimitStatus() throws WeiboException {
        return new RateLimitStatus(http.get(getBaseURL() + "account/rate_limit_status.json", true), this);
    }

    static class Device {
        final String DEVICE;

        public Device(String device) {
            this.DEVICE = device;
        }
    }

    public User updateDeliverlyDevice(Device device) throws WeiboException {
        return new User(http.post(getBaseURL() + "account/update_delivery_device.json", new PostParameter[]{new PostParameter("device", device.DEVICE)}, true).asJSONObject());
    }

    public User updateProfileColors(String profileBackgroundColor, String profileTextColor, String profileLinkColor, String profileSidebarFillColor, String profileSidebarBorderColor) throws WeiboException {
        List<PostParameter> colors = new ArrayList<>(5);
        addParameterToList(colors, "profile_background_color", profileBackgroundColor);
        addParameterToList(colors, "profile_text_color", profileTextColor);
        addParameterToList(colors, "profile_link_color", profileLinkColor);
        addParameterToList(colors, "profile_sidebar_fill_color", profileSidebarFillColor);
        addParameterToList(colors, "profile_sidebar_border_color", profileSidebarBorderColor);
        return new User(http.post(getBaseURL() + "account/update_profile_colors.json", (PostParameter[]) colors.toArray(new PostParameter[colors.size()]), true).asJSONObject());
    }

    private void addParameterToList(List<PostParameter> colors, String paramName, String color) {
        if (color != null) {
            colors.add(new PostParameter(paramName, color));
        }
    }

    public List<Status> favorites() throws WeiboException {
        return getFavorites();
    }

    public List<Status> getFavorites() throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "favorites.json", new PostParameter[0], true));
    }

    public List<Status> favorites(int page) throws WeiboException {
        return getFavorites(page);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response */
    public List<Status> getFavorites(int page) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "favorites.json", "page", String.valueOf(page), true));
    }

    public List<Status> favorites(String id) throws WeiboException {
        return getFavorites(id);
    }

    public List<Status> getFavorites(String id) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "favorites/" + id + ".json", new PostParameter[0], true));
    }

    public List<Status> favorites(String id, int page) throws WeiboException {
        return getFavorites(id, page);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response */
    public List<Status> getFavorites(String id, int page) throws WeiboException {
        return Status.constructStatuses(get(getBaseURL() + "favorites/" + id + ".json", "page", String.valueOf(page), true));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      weibo4j.http.HttpClient.post(java.lang.String, weibo4j.http.PostParameter[]):weibo4j.http.Response
      weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response */
    public Status createFavorite(long id) throws WeiboException {
        return new Status(http.post(getBaseURL() + "favorites/create/" + id + ".json", true));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      weibo4j.http.HttpClient.post(java.lang.String, weibo4j.http.PostParameter[]):weibo4j.http.Response
      weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response */
    public Status destroyFavorite(long id) throws WeiboException {
        return new Status(http.post(getBaseURL() + "favorites/destroy/" + id + ".json", true));
    }

    public User follow(String id) throws WeiboException {
        return enableNotification(id);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      weibo4j.http.HttpClient.post(java.lang.String, weibo4j.http.PostParameter[]):weibo4j.http.Response
      weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response */
    public User enableNotification(String id) throws WeiboException {
        return new User(http.post(getBaseURL() + "notifications/follow/" + id + ".json", true).asJSONObject());
    }

    public User leave(String id) throws WeiboException {
        return disableNotification(id);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      weibo4j.http.HttpClient.post(java.lang.String, weibo4j.http.PostParameter[]):weibo4j.http.Response
      weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response */
    public User disableNotification(String id) throws WeiboException {
        return new User(http.post(getBaseURL() + "notifications/leave/" + id + ".json", true).asJSONObject());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      weibo4j.http.HttpClient.post(java.lang.String, weibo4j.http.PostParameter[]):weibo4j.http.Response
      weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response */
    public User block(String id) throws WeiboException {
        return new User(http.post(getBaseURL() + "blocks/create/" + id + ".xml", true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      weibo4j.http.HttpClient.post(java.lang.String, weibo4j.http.PostParameter[]):weibo4j.http.Response
      weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response */
    public User createBlock(String id) throws WeiboException {
        return new User(http.post(getBaseURL() + "blocks/create/" + id + ".json", true).asJSONObject());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      weibo4j.http.HttpClient.post(java.lang.String, weibo4j.http.PostParameter[]):weibo4j.http.Response
      weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response */
    public User unblock(String id) throws WeiboException {
        return new User(http.post(getBaseURL() + "blocks/destroy/" + id + ".xml", true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      weibo4j.http.HttpClient.post(java.lang.String, weibo4j.http.PostParameter[]):weibo4j.http.Response
      weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response */
    public User destroyBlock(String id) throws WeiboException {
        return new User(http.post(getBaseURL() + "blocks/destroy/" + id + ".json", true).asJSONObject());
    }

    public boolean existsBlock(String id) throws WeiboException {
        try {
            if (-1 == get(getBaseURL() + "blocks/exists/" + id + ".json", true).asString().indexOf("<error>You are not blocking this user.</error>")) {
                return true;
            }
            return false;
        } catch (WeiboException e) {
            WeiboException te = e;
            if (te.getStatusCode() == 404) {
                return false;
            }
            throw te;
        }
    }

    public List<User> getBlockingUsers() throws WeiboException {
        return User.constructUsers(get(getBaseURL() + "blocks/blocking.json", true));
    }

    public List<User> getBlockingUsers(int page) throws WeiboException {
        return User.constructUsers(get(getBaseURL() + "blocks/blocking.json?page=" + page, true));
    }

    public IDs getBlockingUsersIDs() throws WeiboException {
        return new IDs(get(getBaseURL() + "blocks/blocking/ids.json", true), this);
    }

    public List<SavedSearch> getSavedSearches() throws WeiboException {
        return SavedSearch.constructSavedSearches(get(getBaseURL() + "saved_searches.json", true));
    }

    public SavedSearch showSavedSearch(int id) throws WeiboException {
        return new SavedSearch(get(getBaseURL() + "saved_searches/show/" + id + ".json", true));
    }

    public SavedSearch createSavedSearch(String query) throws WeiboException {
        return new SavedSearch(http.post(getBaseURL() + "saved_searches/create.json", new PostParameter[]{new PostParameter("query", query)}, true));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      weibo4j.http.HttpClient.post(java.lang.String, weibo4j.http.PostParameter[]):weibo4j.http.Response
      weibo4j.http.HttpClient.post(java.lang.String, boolean):weibo4j.http.Response */
    public SavedSearch destroySavedSearch(int id) throws WeiboException {
        return new SavedSearch(http.post(getBaseURL() + "saved_searches/destroy/" + id + ".json", true));
    }

    public ListObject getList(String uid, String listId, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(uid).append("/lists/").append(listId).append(".xml").append("?source=").append(CONSUMER_KEY);
        return new ListObject(http.httpRequest(sb.toString(), null, auth, "GET"), this);
    }

    public List<Status> getListStatuses(String uid, String listId, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(uid).append("/lists/").append(listId).append("/statuses.xml").append("?source=").append(CONSUMER_KEY);
        return Status.constructStatuses(http.httpRequest(sb.toString(), null, auth, "GET"), this);
    }

    public ListObjectWapper getUserLists(String uid, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(uid).append("/lists.xml").append("?source=").append(CONSUMER_KEY);
        return ListObject.constructListObjects(http.httpRequest(sb.toString(), null, auth, "GET"), this);
    }

    public ListObjectWapper getUserSubscriberLists(String uid, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(uid).append("/lists/subscriptions.xml").append("?source=").append(CONSUMER_KEY);
        return ListObject.constructListObjects(http.httpRequest(sb.toString(), null, auth, "GET"), this);
    }

    public ListObjectWapper getUserListedLists(String uid, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(uid).append("/lists/memberships.xml").append("?source=").append(CONSUMER_KEY);
        return ListObject.constructListObjects(http.httpRequest(sb.toString(), null, auth, "GET"), this);
    }

    public ListUserCount getListUserCount(String uid, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(String.valueOf(uid)).append("/lists").append("/counts.xml").append("?source=").append(CONSUMER_KEY);
        return new ListUserCount(http.httpRequest(sb.toString(), null, auth, "GET"));
    }

    public UserWapper getListMembers(String uid, String listId, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(uid).append(CookieSpec.PATH_DELIM).append(listId).append("/members.xml").append("?source=").append(CONSUMER_KEY);
        return User.constructWapperUsers(http.httpRequest(sb.toString(), null, auth, "GET"), this);
    }

    public UserWapper getListSubscribers(String uid, String listId, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(uid).append(CookieSpec.PATH_DELIM).append(listId).append("/subscribers.xml").append("?source=").append(CONSUMER_KEY);
        return User.constructWapperUsers(http.httpRequest(sb.toString(), null, auth, "GET"), this);
    }

    public ListObject insertList(String uid, String name, boolean mode, String description, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(uid).append("/lists.xml");
        List<PostParameter> postParams = new LinkedList<>();
        postParams.add(new PostParameter("name", name));
        postParams.add(new PostParameter("description", description));
        postParams.add(new PostParameter("mode", mode ? "public" : "private"));
        postParams.add(new PostParameter("source", CONSUMER_KEY));
        PostParameter[] params = new PostParameter[postParams.size()];
        int index = 0;
        for (PostParameter postParam : postParams) {
            params[index] = postParam;
            index++;
        }
        return new ListObject(http.httpRequest(sb.toString(), params, auth, "POST"), this);
    }

    public ListObject updateList(String uid, String listId, String name, boolean mode, String description, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(uid).append("/lists/").append(listId).append(".xml");
        List<PostParameter> postParams = new LinkedList<>();
        postParams.add(new PostParameter("name", name));
        postParams.add(new PostParameter("mode", mode ? "public" : "private"));
        postParams.add(new PostParameter("description", description));
        postParams.add(new PostParameter("source", CONSUMER_KEY));
        PostParameter[] params = new PostParameter[postParams.size()];
        int index = 0;
        for (PostParameter postParam : postParams) {
            params[index] = postParam;
            index++;
        }
        return new ListObject(http.httpRequest(sb.toString(), params, auth, "POST"), this);
    }

    public ListObject removeList(String uid, String listId, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(uid).append("/lists/").append(listId).append(".xml").append("?source=").append(CONSUMER_KEY);
        return new ListObject(http.httpRequest(sb.toString(), null, auth, "DELETE"), this);
    }

    public ListObject addListMember(String uid, String listId, String targetUid, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(uid).append(CookieSpec.PATH_DELIM).append(listId).append("/members.xml");
        String url = sb.toString();
        List<PostParameter> postParams = new LinkedList<>();
        postParams.add(new PostParameter("id", String.valueOf(targetUid)));
        postParams.add(new PostParameter("source", CONSUMER_KEY));
        PostParameter[] params = new PostParameter[postParams.size()];
        int index = 0;
        for (PostParameter postParam : postParams) {
            params[index] = postParam;
            index++;
        }
        return new ListObject(http.httpRequest(url, params, auth, "POST"), this);
    }

    public ListObject removeListMember(String uid, String listId, String targetUid, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(uid).append(CookieSpec.PATH_DELIM).append(listId).append("/members.xml").append("?source=").append(CONSUMER_KEY).append("&id=").append(String.valueOf(targetUid));
        return new ListObject(http.httpRequest(sb.toString(), null, auth, "DELETE"), this);
    }

    public ListObject addListSubscriber(String uid, String listId, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(uid).append(CookieSpec.PATH_DELIM).append(listId).append("/subscribers.xml");
        String url = sb.toString();
        List<PostParameter> postParams = new LinkedList<>();
        postParams.add(new PostParameter("source", CONSUMER_KEY));
        PostParameter[] params = new PostParameter[postParams.size()];
        int index = 0;
        for (PostParameter postParam : postParams) {
            params[index] = postParam;
            index++;
        }
        return new ListObject(http.httpRequest(url, params, auth, "POST"), this);
    }

    public ListObject removeListSubscriber(String uid, String listId, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(uid).append(CookieSpec.PATH_DELIM).append(listId).append("/subscribers.xml").append("?source=").append(CONSUMER_KEY);
        return new ListObject(http.httpRequest(sb.toString(), null, auth, "DELETE"), this);
    }

    public boolean isListMember(String uid, String listId, String targetUid, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(uid).append(CookieSpec.PATH_DELIM).append(listId).append("/members/").append(targetUid).append(".xml").append("?source=").append(CONSUMER_KEY);
        return "true".equals(http.httpRequest(sb.toString(), null, auth, "GET").asDocument().getDocumentElement().getNodeValue());
    }

    public boolean isListSubscriber(String uid, String listId, String targetUid, boolean auth) throws WeiboException {
        StringBuilder sb = new StringBuilder();
        sb.append(getBaseURL()).append(uid).append(CookieSpec.PATH_DELIM).append(listId).append("/subscribers/").append(targetUid).append(".xml").append("?source=").append(CONSUMER_KEY);
        return "true".equals(http.httpRequest(sb.toString(), null, auth, "GET").asDocument().getDocumentElement().getNodeValue());
    }

    public boolean test() throws WeiboException {
        return -1 != get(new StringBuilder().append(getBaseURL()).append("help/test.json").toString(), false).asString().indexOf("ok");
    }

    public User getAuthenticatedUser() throws WeiboException {
        return new User(get(getBaseURL() + "account/verify_credentials.xml", true), this);
    }

    public String getDowntimeSchedule() throws WeiboException {
        throw new WeiboException("this method is not supported by the Weibo API anymore", new NoSuchMethodException("this method is not supported by the Weibo API anymore"));
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Weibo weibo = (Weibo) o;
        if (!this.baseURL.equals(weibo.baseURL)) {
            return false;
        }
        if (!this.format.equals(weibo.format)) {
            return false;
        }
        if (!http.equals(http)) {
            return false;
        }
        if (!this.searchBaseURL.equals(weibo.searchBaseURL)) {
            return false;
        }
        return this.source.equals(weibo.source);
    }

    public int hashCode() {
        return (((((((http.hashCode() * 31) + this.baseURL.hashCode()) * 31) + this.searchBaseURL.hashCode()) * 31) + this.source.hashCode()) * 31) + this.format.hashCode();
    }

    public String toString() {
        return "Weibo{http=" + http + ", baseURL='" + this.baseURL + '\'' + ", searchBaseURL='" + this.searchBaseURL + '\'' + ", source='" + this.source + '\'' + ", format=" + this.format + '}';
    }

    public List<Comment> getComments(String id) throws WeiboException {
        return Comment.constructComments(get(getBaseURL() + "statuses/comments.json?id=" + id, true));
    }

    public List<Comment> getCommentsTimeline() throws WeiboException {
        return Comment.constructComments(get(getBaseURL() + "statuses/comments_timeline.json", true));
    }

    public List<Comment> getCommentsByMe() throws WeiboException {
        return Comment.constructComments(get(getBaseURL() + "statuses/comments_by_me.json", true));
    }

    public List<Count> getCounts(String ids) throws WeiboException {
        return Count.constructCounts(get(getBaseURL() + "statuses/counts.json?ids=" + ids, true));
    }

    public Count getUnread() throws WeiboException, JSONException {
        return new Count(get(getBaseURL() + "statuses/unread.json", true).asJSONObject());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], weibo4j.Paging, int]
     candidates:
      weibo4j.Weibo.get(java.lang.String, java.lang.String, java.lang.String, boolean):weibo4j.http.Response
      weibo4j.Weibo.get(java.lang.String, weibo4j.http.PostParameter[], weibo4j.Paging, boolean):weibo4j.http.Response */
    public List<Status> getRepostByMe(Paging paging) throws WeiboException {
        return Status.constructStatuses1(get(getBaseURL() + "statuses/repost_by_me.json", (PostParameter[]) null, paging, true));
    }

    public Status repost(String sid, String status) throws WeiboException {
        return new Status(http.post(getBaseURL() + "statuses/repost.json", new PostParameter[]{new PostParameter("id", sid), new PostParameter("status", status)}, true));
    }

    public Status repost(String sid, String status, int is_comment) throws WeiboException {
        return new Status(http.post(getBaseURL() + "statuses/repost.json", new PostParameter[]{new PostParameter("id", sid), new PostParameter("is_comment", is_comment), new PostParameter("status", status)}, true));
    }

    public Status reply(String sid, String cid, String comment) throws WeiboException {
        return new Status(http.post(getBaseURL() + "statuses/reply.json", new PostParameter[]{new PostParameter("id", sid), new PostParameter("cid", cid), new PostParameter((String) Cookie2.COMMENT, comment)}, true));
    }

    public JSONObject showFriendships(String target_id) throws WeiboException {
        return get(getBaseURL() + "friendships/show.json?target_id=" + target_id, true).asJSONObject();
    }

    public JSONObject showFriendships(String source_id, String target_id) throws WeiboException {
        return get(getBaseURL() + "friendships/show.json?target_id=" + target_id + "&source_id" + source_id, true).asJSONObject();
    }

    public User endSession() throws WeiboException {
        return new User(get(getBaseURL() + "account/end_session.json", true).asJSONObject());
    }

    public JSONObject register(String ip, String... args) throws WeiboException {
        return http.post(getBaseURL() + "account/register.json", new PostParameter[]{new PostParameter("nick", args[2]), new PostParameter("gender", args[3]), new PostParameter("password", args[4]), new PostParameter("email", args[5]), new PostParameter("ip", ip)}, true).asJSONObject();
    }
}
