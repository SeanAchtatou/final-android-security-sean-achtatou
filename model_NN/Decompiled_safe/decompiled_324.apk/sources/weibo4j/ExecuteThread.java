package weibo4j;

/* compiled from: Dispatcher */
class ExecuteThread extends Thread {
    private boolean alive = true;
    Dispatcher q;

    ExecuteThread(String name, Dispatcher q2, int index) {
        super(name + "[" + index + "]");
        this.q = q2;
    }

    public void shutdown() {
        this.alive = false;
    }

    public void run() {
        while (this.alive) {
            Runnable task = this.q.poll();
            if (task != null) {
                try {
                    task.run();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
