package weibo4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import weibo4j.http.Response;
import weibo4j.org.json.JSONArray;
import weibo4j.org.json.JSONException;
import weibo4j.org.json.JSONObject;

public class DirectMessage extends WeiboResponse implements Serializable {
    private static final long serialVersionUID = -3253021825891789737L;
    private Date created_at;
    private int id;
    private User recipient;
    private int recipient_id;
    private String recipient_screen_name;
    private User sender;
    private int sender_id;
    private String sender_screen_name;
    private String text;

    DirectMessage(Response res, Weibo weibo) throws WeiboException {
        super(res);
        init(res, res.asDocument().getDocumentElement(), weibo);
    }

    DirectMessage(Response res, Element elem, Weibo weibo) throws WeiboException {
        super(res);
        init(res, elem, weibo);
    }

    DirectMessage(JSONObject json) throws WeiboException {
        try {
            this.id = json.getInt("id");
            this.text = json.getString("text");
            this.sender_id = json.getInt("sender_id");
            this.recipient_id = json.getInt("recipient_id");
            this.created_at = parseDate(json.getString("created_at"), "EEE MMM dd HH:mm:ss z yyyy");
            this.sender_screen_name = json.getString("sender_screen_name");
            this.recipient_screen_name = json.getString("recipient_screen_name");
            if (!json.isNull("sender")) {
                this.sender = new User(json.getJSONObject("sender"));
            }
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new WeiboException(jsone.getMessage() + ":" + json.toString(), jsone);
        }
    }

    private void init(Response res, Element elem, Weibo weibo) throws WeiboException {
        ensureRootNodeNameIs("direct_message", elem);
        this.sender = new User(res, (Element) elem.getElementsByTagName("sender").item(0), weibo);
        this.recipient = new User(res, (Element) elem.getElementsByTagName("recipient").item(0), weibo);
        this.id = getChildInt("id", elem);
        this.text = getChildText("text", elem);
        this.sender_id = getChildInt("sender_id", elem);
        this.recipient_id = getChildInt("recipient_id", elem);
        this.created_at = getChildDate("created_at", elem);
        this.sender_screen_name = getChildText("sender_screen_name", elem);
        this.recipient_screen_name = getChildText("recipient_screen_name", elem);
    }

    public int getId() {
        return this.id;
    }

    public String getText() {
        return this.text;
    }

    public int getSenderId() {
        return this.sender_id;
    }

    public int getRecipientId() {
        return this.recipient_id;
    }

    public Date getCreatedAt() {
        return this.created_at;
    }

    public String getSenderScreenName() {
        return this.sender_screen_name;
    }

    public String getRecipientScreenName() {
        return this.recipient_screen_name;
    }

    public User getSender() {
        return this.sender;
    }

    public User getRecipient() {
        return this.recipient;
    }

    static List<DirectMessage> constructDirectMessages(Response res, Weibo weibo) throws WeiboException {
        Document doc = res.asDocument();
        if (isRootNodeNilClasses(doc)) {
            return new ArrayList(0);
        }
        try {
            ensureRootNodeNameIs("direct-messages", doc);
            NodeList list = doc.getDocumentElement().getElementsByTagName("direct_message");
            int size = list.getLength();
            List<DirectMessage> messages = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                messages.add(new DirectMessage(res, (Element) list.item(i), weibo));
            }
            return messages;
        } catch (WeiboException e) {
            WeiboException te = e;
            if (isRootNodeNilClasses(doc)) {
                return new ArrayList(0);
            }
            throw te;
        }
    }

    static List<DirectMessage> constructDirectMessages(Response res) throws WeiboException {
        JSONArray list = res.asJSONArray();
        try {
            int size = list.length();
            List<DirectMessage> messages = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                messages.add(new DirectMessage(list.getJSONObject(i)));
            }
            return messages;
        } catch (JSONException e) {
            throw new WeiboException(e);
        }
    }

    public int hashCode() {
        return this.id;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return (obj instanceof DirectMessage) && ((DirectMessage) obj).id == this.id;
    }

    public String toString() {
        return "DirectMessage{id=" + this.id + ", text='" + this.text + '\'' + ", sender_id=" + this.sender_id + ", recipient_id=" + this.recipient_id + ", created_at=" + this.created_at + ", sender_screen_name='" + this.sender_screen_name + '\'' + ", recipient_screen_name='" + this.recipient_screen_name + '\'' + ", sender=" + this.sender + ", recipient=" + this.recipient + '}';
    }
}
