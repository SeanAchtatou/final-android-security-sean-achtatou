package weibo4j;

import java.io.Serializable;
import java.util.List;

public class UserWapper implements Serializable {
    private static final long serialVersionUID = -3119107701303920284L;
    private long nextCursor;
    private long previousCursor;
    private List<User> users;

    public UserWapper(List<User> users2, long previousCursor2, long nextCursor2) {
        this.users = users2;
        this.previousCursor = previousCursor2;
        this.nextCursor = nextCursor2;
    }

    public List<User> getUsers() {
        return this.users;
    }

    public void setUsers(List<User> users2) {
        this.users = users2;
    }

    public long getPreviousCursor() {
        return this.previousCursor;
    }

    public void setPreviousCursor(long previousCursor2) {
        this.previousCursor = previousCursor2;
    }

    public long getNextCursor() {
        return this.nextCursor;
    }

    public void setNextCursor(long nextCursor2) {
        this.nextCursor = nextCursor2;
    }
}
