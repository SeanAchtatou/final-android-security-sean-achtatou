package weibo4j.examples;

import weibo4j.Weibo;
import weibo4j.WeiboException;

public class OAuthSetTokenUpdate {
    public static void main(String[] args) {
        try {
            if (args.length < 3) {
                System.out.println("Usage: java Weibo4j.examples.Update token tokenSecret text");
                System.exit(-1);
            }
            System.setProperty("Weibo4j.oauth.consumerKey", Weibo.CONSUMER_KEY);
            System.setProperty("Weibo4j.oauth.consumerSecret", Weibo.CONSUMER_SECRET);
            Weibo weibo = new Weibo();
            weibo.setToken(args[0], args[1]);
            System.out.println("Successfully updated the status to [" + weibo.updateStatus(args[2]).getText() + "].");
            System.exit(0);
        } catch (WeiboException e) {
            System.out.println("Failed to get timeline: " + e.getMessage());
            System.exit(-1);
        } catch (Exception e2) {
            System.out.println("Failed to read the system input.");
            System.exit(-1);
        }
    }
}
