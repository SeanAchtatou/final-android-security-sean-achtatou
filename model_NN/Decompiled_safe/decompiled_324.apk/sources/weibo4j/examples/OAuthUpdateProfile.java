package weibo4j.examples;

import java.io.File;
import weibo4j.Weibo;

public class OAuthUpdateProfile {
    public static void main(String[] args) {
        try {
            if (args.length < 3) {
                System.out.println("Usage: java weibo4j.examples.OAuthUpdateProfile token tokenSecret filePath");
                System.exit(-1);
            }
            System.setProperty("weibo4j.oauth.consumerKey", Weibo.CONSUMER_KEY);
            System.setProperty("weibo4j.oauth.consumerSecret", Weibo.CONSUMER_SECRET);
            Weibo weibo = new Weibo();
            weibo.setToken(args[0], args[1]);
            try {
                File file = new File(args[2]);
                if (file == null) {
                    System.out.println("file is null");
                    System.exit(-1);
                }
                System.out.println("Successfully upload the status to [" + weibo.updateProfileImage(file).getName() + "].");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e2) {
            System.out.println("Failed to read the system input.");
        }
    }
}
