package weibo4j.examples;

import weibo4j.Weibo;
import weibo4j.WeiboException;

public class OAuthUpdateTwo {
    public static void main(String[] args) {
        try {
            if (args.length < 2) {
                System.out.println("Usage: java weibo4j.examples.OAuthUpdateTwo token tokenSecret");
                System.exit(-1);
            }
            System.setProperty("weibo4j.oauth.consumerKey", Weibo.CONSUMER_KEY);
            System.setProperty("weibo4j.oauth.consumerSecret", Weibo.CONSUMER_SECRET);
            Weibo weibo = new Weibo();
            weibo.setToken("e42f35bd66fce37d7c6dfeb6110d8957", "a1943b8ea10eb708e67825d25675d246");
            System.out.println("Successfully updated the status to [" + weibo.updateStatus("浣犲ソ鍚楋紵").getText() + "].");
            System.exit(0);
        } catch (WeiboException e) {
            System.out.println("Failed to get timeline: " + e.getMessage());
            System.exit(-1);
        } catch (Exception e2) {
            System.out.println("Failed to read the system input.");
            System.exit(-1);
        }
    }
}
