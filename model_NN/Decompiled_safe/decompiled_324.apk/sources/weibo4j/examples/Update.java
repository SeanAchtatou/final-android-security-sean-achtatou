package weibo4j.examples;

import java.util.Date;
import weibo4j.Comment;
import weibo4j.Status;
import weibo4j.Weibo;
import weibo4j.WeiboException;
import weibo4j.org.json.JSONException;

public class Update {
    /* JADX INFO: Multiple debug info for r1v7 java.util.Iterator<weibo4j.Status>: [D('statuses' java.util.List<weibo4j.Status>), D('i$' java.util.Iterator)] */
    /* JADX INFO: Multiple debug info for r1v12 long: [D('l2' long), D('msg' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r9v16 weibo4j.Comment: [D('cmt2' weibo4j.Comment), D('cmt' weibo4j.Comment)] */
    public static void main(String[] args) throws WeiboException {
        Status status;
        String[] args2 = {"chenhui_3014@yeah.net", "301488521", "瑕佸彂甯冪殑寰崥淇℃伅~~~"};
        if (args2.length < 3) {
            System.out.println("Usage: java weibo4j.examples.Update ID Password text");
            System.exit(-1);
        }
        long l1 = System.currentTimeMillis();
        Weibo weibo = new Weibo(args2[0], args2[1]);
        for (Status status2 : weibo.getPublicTimeline()) {
            System.out.println(status2);
        }
        String msg = args2[2] + new Date();
        Status status3 = weibo.updateStatus(args2[2] + System.currentTimeMillis());
        try {
            status = weibo.updateStatus(msg, 40.7579d, -73.985d);
        } catch (JSONException e1) {
            e1.printStackTrace();
            status = status3;
        }
        long l2 = System.currentTimeMillis();
        System.out.println("Successfully updated the status to [" + status.getText() + "].");
        System.out.println("Time elapsed: " + (l2 - l1));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long sid = status.getId();
        Comment cmt = weibo.updateComment("璇勮1 " + new Date(), String.valueOf(sid), null);
        weibo.getComments(String.valueOf(sid));
        weibo.updateComment("璇勮2 " + new Date(), String.valueOf(sid), null);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        System.out.println("delete " + weibo.destroyComment(cmt.getId()));
    }
}
