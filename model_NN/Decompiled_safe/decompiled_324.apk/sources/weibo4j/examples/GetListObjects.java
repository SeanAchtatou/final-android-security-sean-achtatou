package weibo4j.examples;

import weibo4j.ListObject;
import weibo4j.ListObjectWapper;
import weibo4j.Weibo;

public class GetListObjects {
    public static void main(String[] args) {
        try {
            if (args.length < 3) {
                System.out.println("No Token/TokenSecret/(Uid or ScreenName) specified.");
                System.out.println("Usage: java Weibo4j.examples.GetListObjects Token TokenSecret Uid");
                System.exit(-1);
            }
            System.setProperty("weibo4j.oauth.consumerKey", Weibo.CONSUMER_KEY);
            System.setProperty("weibo4j.oauth.consumerSecret", Weibo.CONSUMER_SECRET);
            Weibo weibo = new Weibo();
            weibo.setToken(args[0], args[1]);
            String screenName = args[2];
            try {
                ListObjectWapper wapper = weibo.getUserLists(screenName, true);
                for (ListObject listObject : wapper.getListObjects()) {
                    System.out.println(listObject.toString());
                }
                System.out.println("previous_cursor: " + wapper.getPreviousCursor());
                System.out.println("next_cursor: " + wapper.getNextCursor());
                System.out.println("Successfully get lists of [" + screenName + "].");
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.exit(0);
        } catch (Exception e2) {
            System.out.println("Failed to read the system input.");
            System.exit(-1);
        }
    }
}
