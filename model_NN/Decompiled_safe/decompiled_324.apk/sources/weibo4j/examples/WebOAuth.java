package weibo4j.examples;

import weibo4j.User;
import weibo4j.Weibo;
import weibo4j.WeiboException;
import weibo4j.http.AccessToken;
import weibo4j.http.RequestToken;

public class WebOAuth {
    public static RequestToken request(String backUrl) {
        try {
            System.setProperty("weibo4j.oauth.consumerKey", Weibo.CONSUMER_KEY);
            System.setProperty("weibo4j.oauth.consumerSecret", Weibo.CONSUMER_SECRET);
            RequestToken requestToken = new Weibo().getOAuthRequestToken(backUrl);
            System.out.println("Got request token.");
            System.out.println("Request token: " + requestToken.getToken());
            System.out.println("Request token secret: " + requestToken.getTokenSecret());
            return requestToken;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static AccessToken requstAccessToken(RequestToken requestToken, String verifier) {
        try {
            System.setProperty("weibo4j.oauth.consumerKey", Weibo.CONSUMER_KEY);
            System.setProperty("weibo4j.oauth.consumerSecret", Weibo.CONSUMER_SECRET);
            AccessToken accessToken = new Weibo().getOAuthAccessToken(requestToken.getToken(), requestToken.getTokenSecret(), verifier);
            System.out.println("Got access token.");
            System.out.println("access token: " + accessToken.getToken());
            System.out.println("access token secret: " + accessToken.getTokenSecret());
            return accessToken;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void update(AccessToken access, String content) {
        try {
            Weibo weibo = new Weibo();
            weibo.setToken(access.getToken(), access.getTokenSecret());
            System.out.println("Successfully updated the status to [" + weibo.updateStatus(content).getText() + "].");
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }

    public static User showUser(AccessToken access, String userId) {
        try {
            Weibo weibo = new Weibo();
            weibo.setToken(access.getToken(), access.getTokenSecret());
            return weibo.showUser(userId);
        } catch (WeiboException e) {
            e.printStackTrace();
            return null;
        }
    }
}
