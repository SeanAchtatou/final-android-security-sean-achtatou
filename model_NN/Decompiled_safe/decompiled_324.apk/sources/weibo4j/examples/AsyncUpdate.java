package weibo4j.examples;

import weibo4j.AsyncWeibo;
import weibo4j.Status;
import weibo4j.WeiboAdapter;
import weibo4j.WeiboException;

public class AsyncUpdate {
    static Object lock = new Object();

    public static void main(String[] args) throws InterruptedException {
        if (args.length < 3) {
            System.out.println("Usage: java weibo4j.examples.AsyncUpdate ID Password text");
            System.exit(-1);
        }
        new AsyncWeibo(args[0], args[1]).updateStatusAsync(args[2], new WeiboAdapter() {
            public void updated(Status status) {
                System.out.println("Successfully updated the status to [" + status.getText() + "].");
                synchronized (AsyncUpdate.lock) {
                    AsyncUpdate.lock.notify();
                }
            }

            public void onException(WeiboException e, int method) {
                if (method == 39) {
                    e.printStackTrace();
                    synchronized (AsyncUpdate.lock) {
                        AsyncUpdate.lock.notify();
                    }
                    return;
                }
                synchronized (AsyncUpdate.lock) {
                    AsyncUpdate.lock.notify();
                }
                throw new AssertionError("Should not happen");
            }
        });
        synchronized (lock) {
            lock.wait();
        }
    }
}
