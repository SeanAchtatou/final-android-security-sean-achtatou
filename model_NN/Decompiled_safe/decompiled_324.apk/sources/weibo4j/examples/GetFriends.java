package weibo4j.examples;

import weibo4j.Weibo;

public class GetFriends {
    public static void main(String[] args) {
        try {
            System.setProperty("weibo4j.oauth.consumerKey", Weibo.CONSUMER_KEY);
            System.setProperty("weibo4j.oauth.consumerSecret", Weibo.CONSUMER_SECRET);
            Weibo weibo = new Weibo();
            weibo.setToken(args[0], args[1]);
            try {
                System.out.println("Successfully get Friends to [" + weibo.getFriendsStatuses() + "].");
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.exit(0);
        } catch (Exception e2) {
            System.out.println("Failed to read the system input.");
            System.exit(-1);
        }
    }
}
