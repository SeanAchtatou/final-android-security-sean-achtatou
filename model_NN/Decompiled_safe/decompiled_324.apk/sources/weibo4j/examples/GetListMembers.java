package weibo4j.examples;

import weibo4j.User;
import weibo4j.UserWapper;
import weibo4j.Weibo;

public class GetListMembers {
    public static void main(String[] args) {
        try {
            if (args.length < 4) {
                System.out.println("No Token/TokenSecret/(Uid or ScreenName)/(ListId or Slug) specified.");
                System.out.println("Usage: java Weibo4j.examples.GetListMembers Token TokenSecret Uid ListId");
                System.exit(-1);
            }
            System.setProperty("weibo4j.oauth.consumerKey", Weibo.CONSUMER_KEY);
            System.setProperty("weibo4j.oauth.consumerSecret", Weibo.CONSUMER_SECRET);
            Weibo weibo = new Weibo();
            weibo.setToken(args[0], args[1]);
            String screenName = args[2];
            String listId = args[3];
            try {
                UserWapper wapper = weibo.getListMembers(screenName, listId, true);
                for (User user : wapper.getUsers()) {
                    System.out.println(user.toString());
                }
                System.out.println("previous_cursor: " + wapper.getPreviousCursor());
                System.out.println("next_cursor: " + wapper.getNextCursor());
                System.out.println("Successfully get users of [" + listId + "].");
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.exit(0);
        } catch (Exception e2) {
            System.out.println("Failed to read the system input.");
            System.exit(-1);
        }
    }
}
