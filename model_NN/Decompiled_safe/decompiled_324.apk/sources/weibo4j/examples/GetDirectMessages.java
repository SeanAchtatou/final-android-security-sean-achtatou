package weibo4j.examples;

import weibo4j.DirectMessage;
import weibo4j.Weibo;
import weibo4j.WeiboException;

public class GetDirectMessages {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("No WeiboID/Password specified.");
            System.out.println("Usage: java weibo4j.examples.GetDirectMessages ID Password");
            System.exit(-1);
        }
        try {
            for (DirectMessage message : new Weibo(args[0], args[1]).getDirectMessages()) {
                System.out.println("Sender:" + message.getSenderScreenName());
                System.out.println("Text:" + message.getText() + "\n");
            }
            System.exit(0);
        } catch (WeiboException e) {
            System.out.println("Failed to get messages: " + e.getMessage());
            System.exit(-1);
        }
    }
}
