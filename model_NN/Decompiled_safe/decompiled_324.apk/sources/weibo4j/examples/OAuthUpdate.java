package weibo4j.examples;

public class OAuthUpdate {
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x00b0, code lost:
        if (401 == r5.getStatusCode()) goto L_0x00b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x00b2, code lost:
        java.lang.System.out.println("Unable to get the access token.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r5.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00e0, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00e1, code lost:
        java.lang.System.out.println("Failed to read the system input.");
        java.lang.System.exit(-1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x00a9, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00e0 A[ExcHandler: IOException (r7v1 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:1:0x0005] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void main(java.lang.String[] r11) {
        /*
            r10 = -1
            java.lang.String r7 = "weibo4j.oauth.consumerKey"
            java.lang.String r8 = "1148902872"
            java.lang.System.setProperty(r7, r8)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r7 = "weibo4j.oauth.consumerSecret"
            java.lang.String r8 = "3bb48f85576f8eba082d258c2fcb6832"
            java.lang.System.setProperty(r7, r8)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            weibo4j.Weibo r6 = new weibo4j.Weibo     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            r6.<init>()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            weibo4j.http.RequestToken r4 = r6.getOAuthRequestToken()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r8 = "Got request token."
            r7.println(r8)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            r8.<init>()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r9 = "Request token: "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r9 = r4.getToken()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r8 = r8.toString()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            r7.println(r8)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            r8.<init>()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r9 = "Request token secret: "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r9 = r4.getTokenSecret()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r8 = r8.toString()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            r7.println(r8)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            r0 = 0
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.io.InputStream r8 = java.lang.System.in     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            r7.<init>(r8)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            r1.<init>(r7)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
        L_0x0064:
            if (r0 != 0) goto L_0x00ed
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r8 = "Open the following URL and grant access to your account:"
            r7.println(r8)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r8 = r4.getAuthorizationURL()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            r7.println(r8)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r7 = r4.getAuthorizationURL()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            weibo4j.util.BareBonesBrowserLaunch.openURL(r7)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r8 = "Hit enter when it's done.[Enter]:"
            r7.print(r8)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r3 = r1.readLine()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            r8.<init>()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r9 = "pin: "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r9 = r1.toString()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r8 = r8.toString()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            r7.println(r8)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            weibo4j.http.AccessToken r0 = r4.getAccessToken(r3)     // Catch:{ WeiboException -> 0x00a9, IOException -> 0x00e0 }
            goto L_0x0064
        L_0x00a9:
            r5 = move-exception
            r7 = 401(0x191, float:5.62E-43)
            int r8 = r5.getStatusCode()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            if (r7 != r8) goto L_0x00dc
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r8 = "Unable to get the access token."
            r7.println(r8)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            goto L_0x0064
        L_0x00ba:
            r7 = move-exception
            r5 = r7
            java.io.PrintStream r7 = java.lang.System.out
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "Failed to get timeline: "
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = r5.getMessage()
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r8 = r8.toString()
            r7.println(r8)
            java.lang.System.exit(r10)
        L_0x00db:
            return
        L_0x00dc:
            r5.printStackTrace()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            goto L_0x0064
        L_0x00e0:
            r7 = move-exception
            r2 = r7
            java.io.PrintStream r7 = java.lang.System.out
            java.lang.String r8 = "Failed to read the system input."
            r7.println(r8)
            java.lang.System.exit(r10)
            goto L_0x00db
        L_0x00ed:
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r8 = "Got access token."
            r7.println(r8)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            r8.<init>()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r9 = "Access token: "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r9 = r0.getToken()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r8 = r8.toString()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            r7.println(r8)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            r8.<init>()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r9 = "Access token secret: "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r9 = r0.getTokenSecret()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            java.lang.String r8 = r8.toString()     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            r7.println(r8)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            r7 = 0
            java.lang.System.exit(r7)     // Catch:{ WeiboException -> 0x00ba, IOException -> 0x00e0 }
            goto L_0x00db
        */
        throw new UnsupportedOperationException("Method not decompiled: weibo4j.examples.OAuthUpdate.main(java.lang.String[]):void");
    }
}
