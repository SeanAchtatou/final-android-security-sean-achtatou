package weibo4j.examples;

import java.util.List;
import weibo4j.Status;
import weibo4j.Weibo;
import weibo4j.WeiboException;

public class GetTimelines {
    public static void main(String[] args) {
        System.out.println("Showing public timeline.");
        try {
            if (args.length < 2) {
                System.out.println("You need to specify WeiboID/Password combination to show UserTimelines.");
                System.out.println("Usage: java weibo4j.examples.GetTimelines ID Password");
                System.exit(0);
            }
            Weibo weibo = new Weibo(args[0], args[1]);
            for (Status status : weibo.getPublicTimeline()) {
                System.out.println(status.getUser().getName() + ":" + status.getText());
            }
            List<Status> statuses = weibo.getFriendsTimeline();
            System.out.println("------------------------------");
            System.out.println("Showing " + args[0] + "'s friends timeline.");
            for (Status status2 : statuses) {
                System.out.println(status2.getUser().getName() + ":" + status2.getText());
            }
            List<Status> statuses2 = weibo.getUserTimeline();
            System.out.println("------------------------------");
            System.out.println("Showing " + args[0] + "'s timeline.");
            for (Status status3 : statuses2) {
                System.out.println(status3.getUser().getName() + ":" + status3.getText());
            }
            System.exit(0);
        } catch (WeiboException e) {
            System.out.println("Failed to get timeline: " + e.getMessage());
            System.exit(-1);
        }
    }
}
