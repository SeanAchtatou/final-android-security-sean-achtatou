package weibo4j;

import java.util.Date;
import org.w3c.dom.Element;
import weibo4j.http.Response;
import weibo4j.org.json.JSONException;
import weibo4j.org.json.JSONObject;

public class RateLimitStatus extends WeiboResponse {
    private static final long serialVersionUID = 933996804168952707L;
    private int hourlyLimit;
    private int remainingHits;
    private Date resetTime;
    private int resetTimeInSeconds;

    RateLimitStatus(Response res) throws WeiboException {
        super(res);
        Element elem = res.asDocument().getDocumentElement();
        this.remainingHits = getChildInt("remaining-hits", elem);
        this.hourlyLimit = getChildInt("hourly-limit", elem);
        this.resetTimeInSeconds = getChildInt("reset-time-in-seconds", elem);
        this.resetTime = getChildDate("reset-time", elem, "EEE MMM d HH:mm:ss z yyyy");
    }

    RateLimitStatus(Response res, Weibo w) throws WeiboException {
        super(res);
        JSONObject json = res.asJSONObject();
        try {
            this.remainingHits = json.getInt("remaining_hits");
            this.hourlyLimit = json.getInt("hourly_limit");
            this.resetTimeInSeconds = json.getInt("reset_time_in_seconds");
            this.resetTime = parseDate(json.getString("reset_time"), "EEE MMM dd HH:mm:ss z yyyy");
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new WeiboException(jsone.getMessage() + ":" + json.toString(), jsone);
        }
    }

    public int getRemainingHits() {
        return this.remainingHits;
    }

    public int getHourlyLimit() {
        return this.hourlyLimit;
    }

    public int getResetTimeInSeconds() {
        return this.resetTimeInSeconds;
    }

    public Date getDateTime() {
        return this.resetTime;
    }

    public Date getResetTime() {
        return this.resetTime;
    }

    public String toString() {
        return "RateLimitStatus{remainingHits:" + this.remainingHits + ";hourlyLimit:" + this.hourlyLimit + ";resetTimeInSeconds:" + this.resetTimeInSeconds + ";resetTime:" + this.resetTime + "}";
    }
}
