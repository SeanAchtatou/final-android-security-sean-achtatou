package weibo4j;

import java.io.Serializable;
import java.util.List;

public class ListObjectWapper implements Serializable {
    private static final long serialVersionUID = -3119168701303920284L;
    private List<ListObject> listObjects;
    private long nextCursor;
    private long previousCursor;

    public ListObjectWapper(List<ListObject> listObjects2, long previousCursor2, long nextCursor2) {
        this.listObjects = listObjects2;
        this.previousCursor = previousCursor2;
        this.nextCursor = nextCursor2;
    }

    public List<ListObject> getListObjects() {
        return this.listObjects;
    }

    public void setListObjects(List<ListObject> listObjects2) {
        this.listObjects = listObjects2;
    }

    public long getPreviousCursor() {
        return this.previousCursor;
    }

    public void setPreviousCursor(long previousCursor2) {
        this.previousCursor = previousCursor2;
    }

    public long getNextCursor() {
        return this.nextCursor;
    }

    public void setNextCursor(long nextCursor2) {
        this.nextCursor = nextCursor2;
    }
}
