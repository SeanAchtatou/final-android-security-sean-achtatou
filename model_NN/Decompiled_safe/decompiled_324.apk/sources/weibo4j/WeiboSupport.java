package weibo4j;

import weibo4j.http.HttpClient;

class WeiboSupport {
    protected static HttpClient http = new HttpClient();
    protected final boolean USE_SSL;
    protected String source;

    WeiboSupport() {
        this(null, null);
    }

    WeiboSupport(String userId, String password) {
        this.source = Configuration.getSource();
        this.USE_SSL = Configuration.useSSL();
        setClientVersion(null);
        setClientURL(null);
        setUserId(userId);
        setPassword(password);
    }

    public void setUserAgent(String userAgent) {
        http.setUserAgent(userAgent);
    }

    public String getUserAgent() {
        return http.getUserAgent();
    }

    public void setClientVersion(String version) {
        setRequestHeader("X-Weibo-Client-Version", Configuration.getCilentVersion(version));
    }

    public String getClientVersion() {
        return http.getRequestHeader("X-Weibo-Client-Version");
    }

    public void setClientURL(String clientURL) {
        setRequestHeader("X-Weibo-Client-URL", Configuration.getClientURL(clientURL));
    }

    public String getClientURL() {
        return http.getRequestHeader("X-Weibo-Client-URL");
    }

    public synchronized void setUserId(String userId) {
        http.setUserId(Configuration.getUser(userId));
    }

    public String getUserId() {
        return http.getUserId();
    }

    public synchronized void setPassword(String password) {
        http.setPassword(Configuration.getPassword(password));
    }

    public String getPassword() {
        return http.getPassword();
    }

    public void setHttpProxy(String proxyHost, int proxyPort) {
        http.setProxyHost(proxyHost);
        http.setProxyPort(proxyPort);
    }

    public void setHttpProxyAuth(String proxyUser, String proxyPass) {
        http.setProxyAuthUser(proxyUser);
        http.setProxyAuthPassword(proxyPass);
    }

    public void setHttpConnectionTimeout(int connectionTimeout) {
        http.setConnectionTimeout(connectionTimeout);
    }

    public void setHttpReadTimeout(int readTimeoutMilliSecs) {
        http.setReadTimeout(readTimeoutMilliSecs);
    }

    public void setSource(String source2) {
        this.source = Configuration.getSource(source2);
        setRequestHeader("X-Weibo-Client", this.source);
    }

    public String getSource() {
        return this.source;
    }

    public void setRequestHeader(String name, String value) {
        http.setRequestHeader(name, value);
    }

    public void forceUsePost(boolean forceUsePost) {
    }

    public boolean isUsePostForced() {
        return false;
    }

    public void setRetryCount(int retryCount) {
        http.setRetryCount(retryCount);
    }

    public void setRetryIntervalSecs(int retryIntervalSecs) {
        http.setRetryIntervalSecs(retryIntervalSecs);
    }
}
