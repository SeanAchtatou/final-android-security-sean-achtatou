package jaycom.Wallpaper081301wallpaper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Calendar;

public class WallpaperView extends Activity implements AdapterView.OnItemSelectedListener, ViewSwitcher.ViewFactory {
    public static int curIndex;
    protected static InputStream is;
    public static int photoRes;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public Integer[] mImageIds = {Integer.valueOf((int) R.drawable.p01), Integer.valueOf((int) R.drawable.p02), Integer.valueOf((int) R.drawable.p03), Integer.valueOf((int) R.drawable.p04), Integer.valueOf((int) R.drawable.p05), Integer.valueOf((int) R.drawable.p06), Integer.valueOf((int) R.drawable.p07), Integer.valueOf((int) R.drawable.p08), Integer.valueOf((int) R.drawable.p09), Integer.valueOf((int) R.drawable.p10), Integer.valueOf((int) R.drawable.p11), Integer.valueOf((int) R.drawable.p12), Integer.valueOf((int) R.drawable.p13), Integer.valueOf((int) R.drawable.p14), Integer.valueOf((int) R.drawable.p15), Integer.valueOf((int) R.drawable.p16), Integer.valueOf((int) R.drawable.p17), Integer.valueOf((int) R.drawable.p18), Integer.valueOf((int) R.drawable.p19), Integer.valueOf((int) R.drawable.p20)};
    private ImageSwitcher mSwitcher;
    /* access modifiers changed from: private */
    public Integer[] mThumbIds = {Integer.valueOf((int) R.drawable.s01), Integer.valueOf((int) R.drawable.s02), Integer.valueOf((int) R.drawable.s03), Integer.valueOf((int) R.drawable.s04), Integer.valueOf((int) R.drawable.s05), Integer.valueOf((int) R.drawable.s06), Integer.valueOf((int) R.drawable.s07), Integer.valueOf((int) R.drawable.s08), Integer.valueOf((int) R.drawable.s09), Integer.valueOf((int) R.drawable.s10), Integer.valueOf((int) R.drawable.s11), Integer.valueOf((int) R.drawable.s12), Integer.valueOf((int) R.drawable.s13), Integer.valueOf((int) R.drawable.s14), Integer.valueOf((int) R.drawable.s15), Integer.valueOf((int) R.drawable.s16), Integer.valueOf((int) R.drawable.s17), Integer.valueOf((int) R.drawable.s18), Integer.valueOf((int) R.drawable.s19), Integer.valueOf((int) R.drawable.s20)};
    String myMetrics;
    int screenHeight;
    int screenWidth;
    RelativeLayout sv;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setRequestedOrientation(1);
        curIndex = 0;
        setContentView(R.layout.main);
        new DisplayMetrics();
        DisplayMetrics dm = getApplicationContext().getResources().getDisplayMetrics();
        this.screenWidth = dm.widthPixels;
        this.screenHeight = dm.heightPixels;
        this.myMetrics = getDisplayMetrics(this);
        this.sv = (RelativeLayout) findViewById(R.id.relativeLayout01);
        is = getBaseContext().getResources().openRawResource(R.drawable.p01);
        this.sv.setBackgroundDrawable(new BitmapDrawable(RotateBitmap(new BitmapDrawable(is).getBitmap())));
        this.mSwitcher = (ImageSwitcher) findViewById(R.id.switcher);
        this.mSwitcher.setFactory(this);
        Gallery g = (Gallery) findViewById(R.id.gallery);
        g.setAdapter((SpinnerAdapter) new ImageAdapter(this));
        g.setSelection(80);
        g.setOnItemSelectedListener(this);
        g.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                WallpaperView.curIndex = position;
                if (WallpaperView.curIndex < 0) {
                    WallpaperView.curIndex += WallpaperView.this.mThumbIds.length;
                }
                WallpaperView.curIndex %= WallpaperView.this.mThumbIds.length;
                new AlertDialog.Builder(WallpaperView.this).setTitle((int) R.string.app_about).setIcon(WallpaperView.this.mThumbIds[WallpaperView.curIndex].intValue()).setMessage((int) R.string.app_about_msg).setPositiveButton((int) R.string.str_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        Bitmap bmp;
                        Resources resources = WallpaperView.this.getBaseContext().getResources();
                        if (WallpaperView.curIndex < 0) {
                            WallpaperView.curIndex += WallpaperView.this.mImageIds.length;
                        }
                        WallpaperView.curIndex %= WallpaperView.this.mImageIds.length;
                        WallpaperView.is = resources.openRawResource(WallpaperView.this.mImageIds[WallpaperView.curIndex].intValue());
                        try {
                            Bitmap bmpa = new BitmapDrawable(WallpaperView.is).getBitmap();
                            if (bmpa.getWidth() > bmpa.getHeight()) {
                                bmp = WallpaperView.resizeImage(bmpa, WallpaperView.this.screenWidth * 2, WallpaperView.this.screenHeight);
                            } else {
                                bmp = WallpaperView.resizeImage(bmpa, WallpaperView.this.screenWidth, WallpaperView.this.screenHeight);
                            }
                            WallpaperView.this.setWallpaper(bmp);
                            Toast.makeText(WallpaperView.this, WallpaperView.this.getString(R.string.my_text_pre), 0).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).setNegativeButton((int) R.string.str_no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
            }
        });
        ((ImageView) findViewById(R.id.btn_goback)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new AlertDialog.Builder(WallpaperView.this).setTitle((int) R.string.exit_title).setMessage((int) R.string.exit_message).setPositiveButton((int) R.string.str_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        WallpaperView.this.onKeyDown(3, new KeyEvent(3, 0));
                        Intent it = new Intent();
                        it.setAction("android.intent.action.MAIN");
                        it.addCategory("android.intent.category.HOME");
                        it.setFlags(270532608);
                        WallpaperView.this.startActivity(it);
                        WallpaperView.this.finish();
                        System.gc();
                    }
                }).setNegativeButton((int) R.string.str_no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
            }
        });
        ((Button) findViewById(R.id.btn_gotoweb)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent webScreenItent = new Intent();
                webScreenItent.setClass(WallpaperView.this, MyWebView.class);
                WallpaperView.this.startActivity(webScreenItent);
            }
        });
        ((Button) findViewById(R.id.btn_wallpaper)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new AlertDialog.Builder(WallpaperView.this).setTitle((int) R.string.app_about).setIcon(WallpaperView.this.mThumbIds[WallpaperView.curIndex].intValue()).setMessage((int) R.string.app_about_msg).setPositiveButton((int) R.string.str_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        Bitmap bmp;
                        Resources resources = WallpaperView.this.getBaseContext().getResources();
                        if (WallpaperView.curIndex < 0) {
                            WallpaperView.curIndex += WallpaperView.this.mThumbIds.length;
                        }
                        WallpaperView.curIndex %= WallpaperView.this.mThumbIds.length;
                        WallpaperView.is = resources.openRawResource(WallpaperView.this.mImageIds[WallpaperView.curIndex].intValue());
                        try {
                            Bitmap bmpa = new BitmapDrawable(WallpaperView.is).getBitmap();
                            if (bmpa.getWidth() > bmpa.getHeight()) {
                                bmp = WallpaperView.resizeImage(bmpa, WallpaperView.this.screenWidth * 2, WallpaperView.this.screenHeight);
                            } else {
                                bmp = WallpaperView.resizeImage(bmpa, WallpaperView.this.screenWidth, WallpaperView.this.screenHeight);
                            }
                            WallpaperView.this.setWallpaper(bmp);
                            Toast.makeText(WallpaperView.this, WallpaperView.this.getString(R.string.my_text_pre), 0).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).setNegativeButton((int) R.string.str_no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
            }
        });
        ((Button) findViewById(R.id.btn_save)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bitmap bmp;
                if (Environment.getExternalStorageState().equals("mounted")) {
                    Log.d("-------------getExternalStorageState()-----------------", "getExternalStorageState()");
                    Calendar c = Calendar.getInstance();
                    int i = c.get(1);
                    int mMonth = c.get(2) + 1;
                    int mDay = c.get(5);
                    int mHour = c.get(11);
                    int mMinute = c.get(12);
                    String pfilename = "androidmy.com-" + (mMonth + "~" + mDay + "~" + mHour + "~" + mMinute + "~" + c.get(13)) + ".jpg";
                    File myFile = new File("/sdcard/" + pfilename);
                    if (myFile.exists()) {
                        myFile.delete();
                    }
                    try {
                        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(myFile));
                        Bitmap temp = BitmapFactory.decodeResource(WallpaperView.this.mContext.getResources(), R.drawable.p01 + WallpaperView.curIndex);
                        if (temp.getWidth() > temp.getHeight()) {
                            bmp = WallpaperView.resizeImage(temp, WallpaperView.this.screenWidth * 2, WallpaperView.this.screenHeight);
                        } else {
                            bmp = WallpaperView.resizeImage(temp, WallpaperView.this.screenWidth, WallpaperView.this.screenHeight);
                        }
                        bmp.compress(Bitmap.CompressFormat.JPEG, 80, bufferedOutputStream);
                        bufferedOutputStream.flush();
                        bufferedOutputStream.close();
                        String savemessage = WallpaperView.this.getResources().getString(R.string.save_message);
                        Toast.makeText(WallpaperView.this, String.valueOf(savemessage) + WallpaperView.this.getResources().getString(R.string.filename_title) + pfilename, 1).show();
                    } catch (Exception e) {
                    }
                } else {
                    new AlertDialog.Builder(WallpaperView.this).setTitle((int) R.string.operationfailure).setMessage((int) R.string.NOSDCard).setPositiveButton((int) R.string.str_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).show();
                }
            }
        });
        ((Button) findViewById(R.id.btn_market)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent installIntent = new Intent("android.intent.action.VIEW");
                installIntent.setData(Uri.parse("http://market.android.com/search?q=pub:" + "RomoRodrigez"));
                WallpaperView.this.startActivity(installIntent);
            }
        });
    }

    public void onNothingSelected(AdapterView parent) {
    }

    public void onItemSelected(AdapterView parent, View v, int position, long id) {
        Resources r = getBaseContext().getResources();
        if (position < 0) {
            position += this.mImageIds.length;
        }
        is = r.openRawResource(this.mImageIds[position % this.mImageIds.length].intValue());
        Bitmap bmp = new BitmapDrawable(is).getBitmap();
        this.sv.setBackgroundDrawable(new BitmapDrawable(RotateBitmap(bmp)));
        this.mSwitcher.setImageDrawable(new BitmapDrawable(createReflectedImage(bmp)));
        Log.d("--------onItemSelected----------", "mSwitcher.setImageResource(mImageIds[position])");
        if (position < 0) {
            position += this.mThumbIds.length;
        }
        int position2 = position % this.mThumbIds.length;
        curIndex = position2;
        photoRes = this.mImageIds[position2].intValue();
    }

    public View makeView() {
        ImageView i = new ImageView(this);
        i.setDrawingCacheEnabled(true);
        i.setBackgroundColor(Color.argb(216, 0, 0, 0));
        i.setScaleType(ImageView.ScaleType.FIT_CENTER);
        i.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        return i;
    }

    public class ImageAdapter extends BaseAdapter {
        public ImageAdapter(Context c) {
            WallpaperView.this.mContext = c;
        }

        public int getCount() {
            return Integer.MAX_VALUE;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView i = new ImageView(WallpaperView.this.mContext);
            if (position < 0) {
                position += WallpaperView.this.mThumbIds.length;
            }
            i.setImageResource(WallpaperView.this.mThumbIds[position % WallpaperView.this.mThumbIds.length].intValue());
            i.setAdjustViewBounds(true);
            i.setLayoutParams(new Gallery.LayoutParams(-2, -2));
            i.setBackgroundResource(R.drawable.picture_frame);
            return i;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* JADX INFO: Multiple debug info for r8v4 android.graphics.Paint: [D('reflectionImage' android.graphics.Bitmap), D('paint' android.graphics.Paint)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    public static Bitmap createReflectedImage(Bitmap originalImage) {
        Log.d("----------createReflectedImage----------", "createReflectedImage");
        int width = originalImage.getWidth();
        int height = originalImage.getHeight();
        Matrix matrix = new Matrix();
        matrix.preScale(1.0f, -1.0f);
        Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0, height / 2, width, height / 2, matrix, false);
        Bitmap bitmapWithReflection = Bitmap.createBitmap(width, (height / 2) + height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmapWithReflection);
        canvas.drawBitmap(originalImage, 0.0f, 0.0f, (Paint) null);
        canvas.drawRect(0.0f, (float) height, (float) width, (float) (height + 4), new Paint());
        canvas.drawBitmap(reflectionImage, 0.0f, (float) (height + 4), (Paint) null);
        Paint paint = new Paint();
        paint.setShader(new LinearGradient(0.0f, (float) originalImage.getHeight(), 0.0f, (float) (bitmapWithReflection.getHeight() + 4), 1895825407, 16777215, Shader.TileMode.CLAMP));
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawRect(0.0f, (float) height, (float) width, (float) (bitmapWithReflection.getHeight() + 4), paint);
        return bitmapWithReflection;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap RotateBitmap(Bitmap originalImage) {
        Matrix mMatrix = new Matrix();
        int mWidth = originalImage.getWidth();
        int mHeight = originalImage.getHeight();
        if (mWidth <= mHeight) {
            return originalImage;
        }
        mMatrix.reset();
        mMatrix.setRotate(90.0f);
        return Bitmap.createBitmap(originalImage, 0, 0, mWidth, mHeight, mMatrix, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap resizeImage(Bitmap bitmap, int w, int h) {
        Bitmap BitmapOrg = bitmap;
        int width = BitmapOrg.getWidth();
        int height = BitmapOrg.getHeight();
        int newWidth = w;
        int newHeight = h;
        Log.v("-------String.valueOf(width)-----", String.valueOf(width));
        Log.v("------String.valueOf(height)---------", String.valueOf(height));
        Log.v("---String.valueOf(newWidth)--------", String.valueOf(newWidth));
        Log.v("----String.valueOf(newHeight)-----", String.valueOf(newHeight));
        Matrix matrix = new Matrix();
        matrix.postScale(((float) newWidth) / ((float) width), ((float) newHeight) / ((float) height));
        return Bitmap.createBitmap(BitmapOrg, 0, 0, width, height, matrix, true);
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap.Config config;
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        if (drawable.getOpacity() != -1) {
            config = Bitmap.Config.ARGB_8888;
        } else {
            config = Bitmap.Config.RGB_565;
        }
        Bitmap bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, config);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static String getDisplayMetrics(Context cx) {
        new DisplayMetrics();
        DisplayMetrics dm = cx.getApplicationContext().getResources().getDisplayMetrics();
        return String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf("") + "The absolute width:" + String.valueOf(dm.widthPixels) + "pixels\n") + "The absolute heightin:" + String.valueOf(dm.heightPixels) + "pixels\n") + "The logical density of the display.:" + String.valueOf(dm.density) + "\n") + "X dimension :" + String.valueOf(dm.xdpi) + "pixels per inch\n") + "Y dimension :" + String.valueOf(dm.ydpi) + "pixels per inch\n";
    }
}
