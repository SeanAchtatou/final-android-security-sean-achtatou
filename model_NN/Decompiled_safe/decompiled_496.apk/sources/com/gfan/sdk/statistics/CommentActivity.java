package com.gfan.sdk.statistics;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.gfan.sdk.statistics.Collector;
import org.apache.http.HttpResponse;

public class CommentActivity extends Activity {
    private static final int COMMENT = 1;
    private static final int LOGIN_OVER = 2;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    CommentActivity.this.mProgressDialog = new ProgressDialog(CommentActivity.this);
                    CommentActivity.this.mProgressDialog.setMessage("发送中...");
                    CommentActivity.this.mProgressDialog.show();
                    return;
                case 2:
                    CommentActivity.this.mProgressDialog.dismiss();
                    CommentActivity.this.mProgressDialog = null;
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(newContentView());
        setTitle("留言");
    }

    private View newContentView() {
        RelativeLayout rootLayout = new RelativeLayout(getApplicationContext());
        rootLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        final EditText inputComment = new EditText(getApplicationContext());
        inputComment.setId(6);
        RelativeLayout.LayoutParams commentParams2 = new RelativeLayout.LayoutParams(-1, -2);
        commentParams2.setMargins(10, 5, 10, 5);
        inputComment.setLayoutParams(commentParams2);
        LinearLayout buttonArea = new LinearLayout(getApplicationContext());
        RelativeLayout.LayoutParams areaParams = new RelativeLayout.LayoutParams(-1, -2);
        areaParams.setMargins(10, 5, 10, 5);
        areaParams.addRule(3, 6);
        buttonArea.setLayoutParams(areaParams);
        Button comfirmButton = new Button(getApplicationContext());
        LinearLayout.LayoutParams comfirmParams = new LinearLayout.LayoutParams(-2, -2);
        comfirmParams.weight = 1.0f;
        comfirmButton.setLayoutParams(comfirmParams);
        comfirmButton.setId(7);
        comfirmButton.setText("确定");
        comfirmButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String comment = inputComment.getText().toString();
                if (!TextUtils.isEmpty(comment)) {
                    CommentActivity.this.mHandler.sendEmptyMessage(1);
                    Collector.comment(CommentActivity.this, comment, new Collector.IResponse() {
                        public void onSuccess(HttpResponse response) {
                            CommentActivity.this.mHandler.sendEmptyMessage(2);
                            Toast.makeText(CommentActivity.this.getApplicationContext(), "发送成功", 0).show();
                            CommentActivity.this.setResult(-1);
                            CommentActivity.this.finish();
                        }

                        public void onFailed(Exception e) {
                            Toast.makeText(CommentActivity.this.getApplicationContext(), "发送失败", 0).show();
                            CommentActivity.this.mHandler.sendEmptyMessage(2);
                        }
                    });
                }
            }
        });
        Button cancelButton = new Button(getApplicationContext());
        LinearLayout.LayoutParams cancelParams = new LinearLayout.LayoutParams(-2, -2);
        cancelParams.weight = 1.0f;
        cancelButton.setLayoutParams(cancelParams);
        cancelButton.setId(8);
        cancelButton.setText("取消");
        cancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CommentActivity.this.setResult(0);
                CommentActivity.this.finish();
            }
        });
        buttonArea.addView(comfirmButton);
        buttonArea.addView(cancelButton);
        rootLayout.addView(inputComment);
        rootLayout.addView(buttonArea);
        return rootLayout;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Collector.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Collector.onResume(this);
    }
}
