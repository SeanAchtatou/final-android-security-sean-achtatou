package softkos.findpairs;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import softkos.findpairs.Vars;

public class MemoryActivity extends Activity {
    static MemoryActivity instance = null;
    RelativeLayout gameLayout;
    GameView gameView = null;
    MenuCanvas menuCanvas = null;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        instance = this;
        ImageLoader.getInstance().LoadImages();
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Vars.getInstance().screenSize(dm.widthPixels, dm.heightPixels);
        ImageLoader.getInstance().LoadImages();
        showMenu();
    }

    public void showMenu() {
        if (this.menuCanvas == null) {
            this.menuCanvas = new MenuCanvas(this);
        }
        Vars.getInstance().setAppState(Vars.AppState.Menu);
        this.menuCanvas.showUI();
        setContentView(this.menuCanvas);
    }

    public void showGame() {
        if (this.gameView == null) {
            this.gameLayout = new RelativeLayout(this);
            this.gameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            this.gameView = new GameView(this);
            AdView adView = new AdView(this, AdSize.BANNER, "a14c8a7fc128099");
            RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams(-1, -2);
            adView.setLayoutParams(lparams);
            lparams.addRule(12);
            this.gameLayout.addView(adView);
            this.gameLayout.addView(this.gameView);
            adView.loadAd(new AdRequest());
            adView.bringToFront();
        }
        Vars.getInstance().setAppState(Vars.AppState.Game);
        Vars.getInstance().generateBoard();
        setContentView(this.gameLayout);
    }

    public static MemoryActivity getInstance() {
        return instance;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && Vars.getInstance().getAppState() == Vars.AppState.Game) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyUp(keyCode, event);
        }
        if (Vars.getInstance().getAppState() == Vars.AppState.Game) {
            showMenu();
        }
        return true;
    }
}
