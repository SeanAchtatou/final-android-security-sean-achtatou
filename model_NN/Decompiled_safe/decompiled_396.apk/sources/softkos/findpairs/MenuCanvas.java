package softkos.findpairs;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;

public class MenuCanvas extends View {
    static MenuCanvas instance = null;
    gButton mobilsoftBtn = null;
    Paint paint = new Paint();
    PaintManager paintMgr;
    gButton startGame1 = null;
    gButton startGame2 = null;
    gButton startGame3 = null;
    gButton startGame4 = null;
    Vars vars;

    public MenuCanvas(Context c) {
        super(c);
        instance = this;
        this.vars = Vars.getInstance();
        this.paintMgr = PaintManager.getInstance();
        initUI();
    }

    public void showUI() {
        for (int i = 0; i < this.vars.getButtonList().size(); i++) {
            this.vars.getButton(i).hide();
        }
        this.startGame1.show();
        this.startGame2.show();
        this.startGame3.show();
        this.startGame4.show();
        this.mobilsoftBtn.show();
    }

    public void initUI() {
        this.startGame1 = new gButton();
        this.startGame1.setSize(110, 55);
        this.startGame1.setId(Vars.getInstance().START_GAME_1);
        this.startGame1.setText("5x6");
        this.startGame1.show();
        Vars.getInstance().addButton(this.startGame1);
        this.startGame2 = new gButton();
        this.startGame2.setSize(110, 55);
        this.startGame2.setId(Vars.getInstance().START_GAME_2);
        this.startGame2.setText("6x7");
        this.startGame2.show();
        Vars.getInstance().addButton(this.startGame2);
        this.startGame3 = new gButton();
        this.startGame3.setSize(110, 55);
        this.startGame3.setId(Vars.getInstance().START_GAME_3);
        this.startGame3.setText("8x9");
        this.startGame3.show();
        Vars.getInstance().addButton(this.startGame3);
        this.startGame4 = new gButton();
        this.startGame4.setSize(110, 55);
        this.startGame4.setId(Vars.getInstance().START_GAME_4);
        this.startGame4.setText("10x10");
        this.startGame4.show();
        Vars.getInstance().addButton(this.startGame4);
        this.mobilsoftBtn = new gButton();
        this.mobilsoftBtn.setSize(240, 50);
        this.mobilsoftBtn.setId(Vars.getInstance().GO_TO_MOBILSOFT);
        this.mobilsoftBtn.setText("More apps");
        this.mobilsoftBtn.show();
        Vars.getInstance().addButton(this.mobilsoftBtn);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        layoutUI();
    }

    public void layoutUI() {
        this.mobilsoftBtn.setPosition((getWidth() / 2) - (this.mobilsoftBtn.getWidth() / 2), getHeight() - ((int) (((double) this.mobilsoftBtn.getHeight()) * 1.1d)));
        this.startGame1.setPosition(((getWidth() / 2) - this.startGame3.getWidth()) - 10, getHeight() - ((int) (((double) this.startGame3.getHeight()) * 3.4d)));
        this.startGame2.setPosition((getWidth() / 2) + 10, getHeight() - ((int) (((double) this.startGame4.getHeight()) * 3.4d)));
        this.startGame3.setPosition(((getWidth() / 2) - this.startGame3.getWidth()) - 10, getHeight() - ((int) (((double) this.startGame3.getHeight()) * 2.2d)));
        this.startGame4.setPosition((getWidth() / 2) + 10, getHeight() - ((int) (((double) this.startGame4.getHeight()) * 2.2d)));
    }

    public void mouseDown(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void mouseUp(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                invalidate();
                return;
            }
        }
    }

    public void mouseDrag(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void handleCommand(int id) {
        if (id >= Vars.getInstance().START_GAME_1 && id <= Vars.getInstance().START_GAME_4) {
            if (id == Vars.getInstance().START_GAME_1) {
                Vars.getInstance().setBoardSize(5, 6);
            }
            if (id == Vars.getInstance().START_GAME_2) {
                Vars.getInstance().setBoardSize(6, 7);
            }
            if (id == Vars.getInstance().START_GAME_3) {
                Vars.getInstance().setBoardSize(8, 9);
            }
            if (id == Vars.getInstance().START_GAME_4) {
                Vars.getInstance().setBoardSize(10, 10);
            }
            MemoryActivity.getInstance().showGame();
        } else if (id == Vars.getInstance().GO_TO_MOBILSOFT) {
            MemoryActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.mobilsoft.pl")));
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.paintMgr.drawImage(canvas, ImageLoader.getInstance().MENUBACK, 0, 0, getWidth(), getHeight());
        this.paintMgr.drawImage(canvas, ImageLoader.getInstance().GAMENAME, 0, 0, getWidth(), getWidth());
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            Vars.getInstance().getButton(i).draw(canvas, this.paint);
        }
    }

    public static MenuCanvas getInstance() {
        return instance;
    }
}
