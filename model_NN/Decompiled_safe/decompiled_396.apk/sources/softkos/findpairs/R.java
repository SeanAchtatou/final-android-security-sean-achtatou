package softkos.findpairs;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int card_00 = 2130837504;
        public static final int card_01 = 2130837505;
        public static final int card_02 = 2130837506;
        public static final int card_03 = 2130837507;
        public static final int card_04 = 2130837508;
        public static final int card_05 = 2130837509;
        public static final int card_06 = 2130837510;
        public static final int card_07 = 2130837511;
        public static final int card_08 = 2130837512;
        public static final int card_09 = 2130837513;
        public static final int card_10 = 2130837514;
        public static final int card_11 = 2130837515;
        public static final int card_12 = 2130837516;
        public static final int card_13 = 2130837517;
        public static final int card_14 = 2130837518;
        public static final int card_15 = 2130837519;
        public static final int card_16 = 2130837520;
        public static final int card_17 = 2130837521;
        public static final int card_18 = 2130837522;
        public static final int card_19 = 2130837523;
        public static final int card_20 = 2130837524;
        public static final int card_21 = 2130837525;
        public static final int card_22 = 2130837526;
        public static final int card_23 = 2130837527;
        public static final int card_24 = 2130837528;
        public static final int card_25 = 2130837529;
        public static final int card_26 = 2130837530;
        public static final int card_27 = 2130837531;
        public static final int card_28 = 2130837532;
        public static final int card_29 = 2130837533;
        public static final int card_30 = 2130837534;
        public static final int card_31 = 2130837535;
        public static final int card_32 = 2130837536;
        public static final int card_33 = 2130837537;
        public static final int card_34 = 2130837538;
        public static final int card_35 = 2130837539;
        public static final int card_36 = 2130837540;
        public static final int card_37 = 2130837541;
        public static final int card_38 = 2130837542;
        public static final int card_39 = 2130837543;
        public static final int card_40 = 2130837544;
        public static final int card_41 = 2130837545;
        public static final int card_42 = 2130837546;
        public static final int card_43 = 2130837547;
        public static final int card_44 = 2130837548;
        public static final int card_45 = 2130837549;
        public static final int card_46 = 2130837550;
        public static final int card_47 = 2130837551;
        public static final int card_48 = 2130837552;
        public static final int card_49 = 2130837553;
        public static final int card_50 = 2130837554;
        public static final int card_empty = 2130837555;
        public static final int gamename = 2130837556;
        public static final int icon = 2130837557;
        public static final int menuback = 2130837558;
        public static final int push_off_0 = 2130837559;
        public static final int push_off_1 = 2130837560;
        public static final int push_off_2 = 2130837561;
        public static final int push_off_3 = 2130837562;
        public static final int push_off_4 = 2130837563;
        public static final int push_off_5 = 2130837564;
        public static final int push_off_6 = 2130837565;
        public static final int push_off_7 = 2130837566;
        public static final int push_off_8 = 2130837567;
        public static final int push_on_0 = 2130837568;
        public static final int push_on_1 = 2130837569;
        public static final int push_on_2 = 2130837570;
        public static final int push_on_3 = 2130837571;
        public static final int push_on_4 = 2130837572;
        public static final int push_on_5 = 2130837573;
        public static final int push_on_6 = 2130837574;
        public static final int push_on_7 = 2130837575;
        public static final int push_on_8 = 2130837576;
        public static final int smile = 2130837577;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968576;
    }
}
