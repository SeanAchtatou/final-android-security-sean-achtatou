package softkos.findpairs;

import android.graphics.Canvas;
import android.graphics.Paint;

public class gButton {
    boolean enabled;
    boolean hasKeyboardFocus;
    int height;
    int id;
    int image;
    ImageLoader imgInst;
    boolean isToggleBtn;
    boolean is_pressed;
    boolean mouseDown;
    int px;
    int py;
    String text;
    int toggleState;
    boolean visible;
    int width;

    public void setToggleButton() {
        this.isToggleBtn = true;
    }

    public boolean isPushed() {
        if (this.toggleState > 0) {
            return true;
        }
        return false;
    }

    public void toggleBtn() {
        if (this.toggleState > 0) {
            this.toggleState = 0;
        } else {
            this.toggleState = 1;
        }
    }

    public void setEnabled(boolean e) {
        this.enabled = e;
    }

    public void setKeyboardFocus(boolean f) {
        this.hasKeyboardFocus = f;
    }

    public boolean getKeyboardFocus() {
        return this.hasKeyboardFocus;
    }

    public void setImage(int img) {
        this.image = img;
    }

    public gButton() {
        this.imgInst = null;
        this.image = -1;
        this.enabled = true;
        this.isToggleBtn = false;
        this.toggleState = 0;
        this.is_pressed = false;
        this.mouseDown = false;
        this.text = "";
        this.id = -1;
        this.imgInst = ImageLoader.getInstance();
    }

    public void setText(String t) {
        this.text = t;
    }

    public String getText() {
        return this.text;
    }

    public void setId(int i) {
        this.id = i;
    }

    public int getId() {
        return this.id;
    }

    public void show() {
        this.visible = true;
    }

    public void hide() {
        this.visible = false;
    }

    public void setPosition(int x, int y) {
        this.px = x;
        this.py = y;
    }

    public void setSize(int w, int h) {
        this.width = w;
        this.height = h;
    }

    public int getPx() {
        return this.px;
    }

    public int getPy() {
        return this.py;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public void drawImage(Canvas canvas, int image2, int x, int y, int w, int h) {
        if (ImageLoader.getInstance().getImage(image2) != null) {
            ImageLoader.getInstance().getImage(image2).setBounds(x, y, x + w, y + h);
            ImageLoader.getInstance().getImage(image2).draw(canvas);
        }
    }

    public void draw(Canvas g, Paint p) {
        if (this.visible) {
            if ((!this.is_pressed || !this.mouseDown) && !isPushed()) {
                drawImage(g, ImageLoader.getInstance().PUSH_OFF_8, this.px, this.py, this.width, this.height);
                drawImage(g, ImageLoader.getInstance().PUSH_OFF_1, this.px, this.py, this.width, 10);
                drawImage(g, ImageLoader.getInstance().PUSH_OFF_5, this.px, (this.py + this.height) - 10, this.width, 10);
                drawImage(g, ImageLoader.getInstance().PUSH_OFF_7, this.px, this.py, 10, this.height);
                drawImage(g, ImageLoader.getInstance().PUSH_OFF_3, (this.px - 10) + this.width, this.py, 10, this.height);
                drawImage(g, ImageLoader.getInstance().PUSH_OFF_0, this.px, this.py, 10, 10);
                drawImage(g, ImageLoader.getInstance().PUSH_OFF_2, (this.px + this.width) - 10, this.py, 10, 10);
                drawImage(g, ImageLoader.getInstance().PUSH_OFF_4, (this.px + this.width) - 10, (this.py + this.height) - 10, 10, 10);
                drawImage(g, ImageLoader.getInstance().PUSH_OFF_6, this.px, (this.py - 10) + this.height, 10, 10);
            } else {
                drawImage(g, ImageLoader.getInstance().PUSH_ON_8, this.px, this.py, this.width, this.height);
                drawImage(g, ImageLoader.getInstance().PUSH_ON_1, this.px, this.py, this.width, 10);
                drawImage(g, ImageLoader.getInstance().PUSH_ON_5, this.px, (this.py + this.height) - 10, this.width, 10);
                drawImage(g, ImageLoader.getInstance().PUSH_ON_7, this.px, this.py, 10, this.height);
                drawImage(g, ImageLoader.getInstance().PUSH_ON_3, (this.px - 10) + this.width, this.py, 10, this.height);
                drawImage(g, ImageLoader.getInstance().PUSH_ON_0, this.px, this.py, 10, 10);
                drawImage(g, ImageLoader.getInstance().PUSH_ON_2, (this.px + this.width) - 10, this.py, 10, 10);
                drawImage(g, ImageLoader.getInstance().PUSH_ON_4, (this.px + this.width) - 10, (this.py + this.height) - 10, 10, 10);
                drawImage(g, ImageLoader.getInstance().PUSH_ON_6, this.px, (this.py - 10) + this.height, 10, 10);
            }
            if (this.image >= 0) {
                drawImage(g, this.image, this.px, this.py, this.width, this.height);
                return;
            }
            p.setAntiAlias(true);
            p.setTextSize(24.0f);
            if (!this.is_pressed || !this.mouseDown) {
                p.setColor(-1);
            } else {
                p.setColor(-7829368);
            }
            g.drawText(this.text, ((float) (this.px + (this.width / 2))) - (p.measureText(this.text) / 2.0f), ((float) (this.py + (this.height / 2))) + (p.getTextSize() / 2.0f), p);
        }
    }

    public int mouseDrag(int x, int y) {
        if (!this.visible || !this.enabled) {
            return 0;
        }
        if (x <= this.px || y <= this.py || x >= this.px + this.width || y >= this.py + this.height) {
            this.is_pressed = false;
            return 0;
        }
        this.is_pressed = true;
        return 1;
    }

    public int mouseUp(int x, int y) {
        if (!this.visible || !this.enabled) {
            return 0;
        }
        int val = 0;
        if (x > this.px && y > this.py && x < this.px + this.width && y < this.py + this.height && this.is_pressed && this.mouseDown) {
            val = 1;
            if (this.isToggleBtn) {
                toggleBtn();
            }
        }
        this.is_pressed = false;
        this.mouseDown = false;
        return val;
    }

    public int mouseDown(int x, int y) {
        if (!this.visible || !this.enabled) {
            return 0;
        }
        if (x <= this.px || y <= this.py || x >= this.px + this.width || y >= this.py + this.height) {
            return 0;
        }
        this.is_pressed = true;
        this.mouseDown = true;
        return 1;
    }
}
