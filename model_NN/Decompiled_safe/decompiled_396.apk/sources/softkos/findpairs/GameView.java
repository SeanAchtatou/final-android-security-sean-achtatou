package softkos.findpairs;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

public class GameView extends View {
    Paint paint;
    PaintManager paintMgr;
    Vars vars;

    public GameView(Context c) {
        super(c);
        this.paint = null;
        this.paint = new Paint();
        this.paintMgr = PaintManager.getInstance();
        this.vars = Vars.getInstance();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.paintMgr.drawImage(canvas, ImageLoader.getInstance().MENUBACK, 0, 0, getWidth(), getHeight());
        this.paint.setColor(-16711936);
        for (int i = 0; i < this.vars.getCardList().size(); i++) {
            int x = (int) this.vars.getCard(i).getPx();
            int y = (int) this.vars.getCard(i).getPy();
            int size = (int) this.vars.getCard(i).getWidth();
            int image = ImageLoader.getInstance().CARD_0;
            if (this.vars.getCard(i).isVisible()) {
                image = ImageLoader.getInstance().CARD_0 + this.vars.getCard(i).getId() + 1;
            }
            ImageLoader.getInstance().drawImage(canvas, image, x, y, size, size);
        }
        this.paint.setAntiAlias(true);
        if (!this.vars.isEndGame()) {
            this.paint.setColor(-16777216);
            this.paint.setTextSize(20.0f);
            String str1 = "Attempts: " + this.vars.getAttempts();
            canvas.drawText(str1, (float) ((getWidth() / 2) - (((int) this.paint.measureText(str1)) / 2)), (float) (((int) (((double) this.vars.getBoardHeight()) * this.vars.getCard(0).getHeight())) + 20), this.paint);
        }
        if (this.vars.isEndGame()) {
            ImageLoader.getInstance().drawImage(canvas, ImageLoader.getInstance().SMILE, 10, 50, getWidth() - 20, getWidth() - 20);
            this.paint.setColor(-16777216);
            this.paint.setTextSize(20.0f);
            canvas.drawText("Press back to menu", ((float) (getWidth() / 2)) - (this.paint.measureText("Press back to menu") / 2.0f), (float) (getHeight() - 30), this.paint);
        }
    }

    public void mouseDown(int x, int y) {
    }

    public void mouseDrag(int x, int y) {
    }

    public void mouseUp(int x, int y) {
        this.vars.checkCards(x, y);
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }
}
