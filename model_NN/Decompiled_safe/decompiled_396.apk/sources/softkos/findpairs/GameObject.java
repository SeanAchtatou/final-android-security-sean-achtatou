package softkos.findpairs;

public class GameObject {
    double dest_px;
    double dest_py;
    double height;
    int id;
    int owner_id;
    double px;
    double py;
    boolean visible;
    double width;

    public boolean isVisible() {
        return this.visible;
    }

    public void setVisible(boolean v) {
        this.visible = v;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int i) {
        this.id = i;
    }

    public GameObject() {
        this.visible = false;
        this.owner_id = -1;
        this.id = -1;
        this.visible = false;
    }

    public void setOwnerID(int id2) {
        this.owner_id = id2;
    }

    public int getOwnerID() {
        return this.owner_id;
    }

    public double getPx() {
        return this.px;
    }

    public double getPy() {
        return this.py;
    }

    public void SetPos(double x, double y) {
        this.px = x;
        this.py = y;
    }

    public double getWidth() {
        return this.width;
    }

    public double getHeight() {
        return this.height;
    }

    public void setSize(int w, int h) {
        this.width = (double) w;
        this.height = (double) h;
    }

    public double getDestPx() {
        return this.dest_px;
    }

    public double getDestPy() {
        return this.dest_py;
    }

    public void setDestPos(double x, double y) {
        this.dest_px = x;
        this.dest_py = y;
    }
}
