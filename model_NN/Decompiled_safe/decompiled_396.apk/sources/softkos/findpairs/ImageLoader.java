package softkos.findpairs;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

public class ImageLoader {
    static ImageLoader instance = null;
    public int CARD_0 = 0;
    public int CARD_1 = 1;
    public int CARD_10 = 10;
    public int CARD_11 = 11;
    public int CARD_12 = 12;
    public int CARD_13 = 13;
    public int CARD_14 = 14;
    public int CARD_15 = 15;
    public int CARD_16 = 16;
    public int CARD_17 = 17;
    public int CARD_18 = 18;
    public int CARD_19 = 19;
    public int CARD_2 = 2;
    public int CARD_20 = 20;
    public int CARD_21 = 21;
    public int CARD_22 = 22;
    public int CARD_23 = 23;
    public int CARD_24 = 24;
    public int CARD_25 = 25;
    public int CARD_26 = 26;
    public int CARD_27 = 27;
    public int CARD_28 = 28;
    public int CARD_29 = 29;
    public int CARD_3 = 3;
    public int CARD_30 = 30;
    public int CARD_31 = 31;
    public int CARD_32 = 32;
    public int CARD_33 = 33;
    public int CARD_34 = 34;
    public int CARD_35 = 35;
    public int CARD_36 = 36;
    public int CARD_37 = 37;
    public int CARD_38 = 38;
    public int CARD_39 = 39;
    public int CARD_4 = 4;
    public int CARD_40 = 40;
    public int CARD_41 = 41;
    public int CARD_42 = 42;
    public int CARD_43 = 43;
    public int CARD_44 = 44;
    public int CARD_45 = 45;
    public int CARD_46 = 46;
    public int CARD_47 = 47;
    public int CARD_48 = 48;
    public int CARD_49 = 49;
    public int CARD_5 = 5;
    public int CARD_50 = 50;
    public int CARD_6 = 6;
    public int CARD_7 = 7;
    public int CARD_8 = 8;
    public int CARD_9 = 9;
    public int GAMENAME = 71;
    public int MENUBACK = 70;
    public int PUSH_OFF_0 = 52;
    public int PUSH_OFF_1 = 53;
    public int PUSH_OFF_2 = 54;
    public int PUSH_OFF_3 = 55;
    public int PUSH_OFF_4 = 56;
    public int PUSH_OFF_5 = 57;
    public int PUSH_OFF_6 = 58;
    public int PUSH_OFF_7 = 59;
    public int PUSH_OFF_8 = 60;
    public int PUSH_ON_0 = 61;
    public int PUSH_ON_1 = 62;
    public int PUSH_ON_2 = 63;
    public int PUSH_ON_3 = 64;
    public int PUSH_ON_4 = 65;
    public int PUSH_ON_5 = 66;
    public int PUSH_ON_6 = 67;
    public int PUSH_ON_7 = 68;
    public int PUSH_ON_8 = 69;
    public int SMILE = 51;
    Drawable[] images;
    int[] img_id = {R.drawable.card_00, R.drawable.card_01, R.drawable.card_02, R.drawable.card_03, R.drawable.card_04, R.drawable.card_05, R.drawable.card_06, R.drawable.card_07, R.drawable.card_08, R.drawable.card_09, R.drawable.card_10, R.drawable.card_11, R.drawable.card_12, R.drawable.card_13, R.drawable.card_14, R.drawable.card_15, R.drawable.card_16, R.drawable.card_17, R.drawable.card_18, R.drawable.card_19, R.drawable.card_20, R.drawable.card_21, R.drawable.card_22, R.drawable.card_23, R.drawable.card_24, R.drawable.card_25, R.drawable.card_26, R.drawable.card_27, R.drawable.card_28, R.drawable.card_29, R.drawable.card_30, R.drawable.card_31, R.drawable.card_32, R.drawable.card_33, R.drawable.card_34, R.drawable.card_35, R.drawable.card_36, R.drawable.card_37, R.drawable.card_38, R.drawable.card_39, R.drawable.card_40, R.drawable.card_41, R.drawable.card_42, R.drawable.card_43, R.drawable.card_44, R.drawable.card_45, R.drawable.card_46, R.drawable.card_47, R.drawable.card_48, R.drawable.card_49, R.drawable.card_50, R.drawable.smile, R.drawable.push_off_0, R.drawable.push_off_1, R.drawable.push_off_2, R.drawable.push_off_3, R.drawable.push_off_4, R.drawable.push_off_5, R.drawable.push_off_6, R.drawable.push_off_7, R.drawable.push_off_8, R.drawable.push_on_0, R.drawable.push_on_1, R.drawable.push_on_2, R.drawable.push_on_3, R.drawable.push_on_4, R.drawable.push_on_5, R.drawable.push_on_6, R.drawable.push_on_7, R.drawable.push_on_8, R.drawable.menuback, R.drawable.gamename};

    public static ImageLoader getInstance() {
        if (instance == null) {
            instance = new ImageLoader();
        }
        return instance;
    }

    public void LoadImages() {
        int len = this.img_id.length;
        this.images = new Drawable[100];
        for (int i = 0; i < len; i++) {
            this.images[i] = MemoryActivity.getInstance().getResources().getDrawable(this.img_id[i]);
        }
    }

    public Drawable getImage(int img) {
        if (img < this.images.length) {
            return this.images[img];
        }
        return null;
    }

    public void drawImage(Canvas canvas, int image, int x, int y, int w, int h) {
        if (getImage(image) != null) {
            getImage(image).setBounds(x, y, x + w, y + h);
            getImage(image).draw(canvas);
        }
    }
}
