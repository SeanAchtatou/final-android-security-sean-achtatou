package softkos.findpairs;

import java.util.ArrayList;
import java.util.Random;

public class Vars {
    static Vars instance = null;
    public int GO_TO_MOBILSOFT;
    public int START_GAME_1;
    public int START_GAME_2;
    public int START_GAME_3;
    public int START_GAME_4;
    AppState appState;
    int attempts;
    int board_height;
    int board_width;
    ArrayList<gButton> buttonList;
    ArrayList<Card> cardList;
    boolean gameDone;
    Random random;
    int screen_height;
    int screen_width;
    int selectedCard1;
    int selectedCard2;

    public enum AppState {
        Menu,
        About,
        Game
    }

    public Vars() {
        this.random = new Random();
        this.buttonList = null;
        this.cardList = new ArrayList<>();
        this.attempts = 0;
        this.gameDone = false;
        this.START_GAME_1 = 1;
        this.START_GAME_2 = 2;
        this.START_GAME_3 = 3;
        this.START_GAME_4 = 4;
        this.GO_TO_MOBILSOFT = 100;
        this.buttonList = new ArrayList<>();
    }

    public void addButton(gButton b) {
        this.buttonList.add(b);
    }

    public ArrayList<gButton> getButtonList() {
        return this.buttonList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public gButton getButton(int b) {
        if (b < 0 || b >= this.buttonList.size()) {
            return null;
        }
        return this.buttonList.get(b);
    }

    public ArrayList<Card> getCardList() {
        return this.cardList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Card getCard(int i) {
        if (i < 0 || i >= this.cardList.size()) {
            return null;
        }
        return this.cardList.get(i);
    }

    public int getAttempts() {
        return this.attempts;
    }

    public void setAttempts(int a) {
        this.attempts = a;
    }

    public void screenSize(int w, int h) {
        this.screen_width = w;
        this.screen_height = h;
    }

    public int getScreenWidth() {
        return this.screen_width;
    }

    public int getScreenHeight() {
        return this.screen_height;
    }

    public void setAppState(AppState as) {
        this.appState = as;
    }

    public AppState getAppState() {
        return this.appState;
    }

    public void setBoardSize(int w, int h) {
        this.board_width = w;
        this.board_height = h;
    }

    public int getBoardWidth() {
        return this.board_width;
    }

    public int getBoardHeight() {
        return this.board_height;
    }

    public void checkCards(int x, int y) {
        for (int i = 0; i < this.cardList.size(); i++) {
            if (((double) x) > getCard(i).getPx() && ((double) x) < getCard(i).getPx() + getCard(i).getWidth() && ((double) y) > getCard(i).getPy() && ((double) y) < getCard(i).getPy() + getCard(i).getHeight() && !getCard(i).isVisible()) {
                if (this.selectedCard1 < 0) {
                    this.selectedCard1 = i;
                    getCard(i).setVisible(true);
                } else if (i != this.selectedCard1) {
                    if (this.selectedCard2 < 0) {
                        this.selectedCard2 = i;
                        getCard(i).setVisible(true);
                        if (getCard(this.selectedCard1).getId() == getCard(this.selectedCard2).getId()) {
                            this.selectedCard1 = -1;
                            this.selectedCard2 = -1;
                        }
                    } else {
                        getCard(this.selectedCard1).setVisible(false);
                        getCard(this.selectedCard2).setVisible(false);
                        this.selectedCard1 = -1;
                        this.selectedCard2 = -1;
                        this.attempts++;
                    }
                }
            }
        }
        boolean done = true;
        int i2 = 0;
        while (true) {
            if (i2 >= this.cardList.size()) {
                break;
            } else if (!getCard(i2).isVisible()) {
                done = false;
                break;
            } else {
                i2++;
            }
        }
        if (done) {
            this.gameDone = true;
        }
    }

    public boolean isEndGame() {
        return this.gameDone;
    }

    public void generateBoard() {
        this.selectedCard1 = -1;
        this.selectedCard2 = -1;
        this.gameDone = false;
        this.attempts = 0;
        int max_card = this.board_width * this.board_height;
        int max_id = max_card / 2;
        this.cardList.clear();
        int card_size = this.screen_width / this.board_width;
        int[] ids = new int[max_card];
        for (int i = 0; i < max_card; i++) {
            ids[i] = -1;
        }
        for (int i2 = 0; i2 < max_id; i2++) {
            int index = this.random.nextInt(max_card);
            while (ids[index] >= 0) {
                index = this.random.nextInt(max_card);
            }
            ids[index] = i2;
            int index2 = this.random.nextInt(max_card);
            while (ids[index2] >= 0) {
                index2 = this.random.nextInt(max_card);
            }
            ids[index2] = i2;
        }
        for (int i3 = 0; i3 < max_card; i3++) {
            Card c = new Card();
            c.setId(ids[i3]);
            c.setSize(card_size, card_size);
            c.SetPos((double) ((i3 % this.board_width) * card_size), (double) ((i3 / this.board_width) * card_size));
            this.cardList.add(c);
        }
    }

    public static Vars getInstance() {
        if (instance == null) {
            instance = new Vars();
        }
        return instance;
    }
}
