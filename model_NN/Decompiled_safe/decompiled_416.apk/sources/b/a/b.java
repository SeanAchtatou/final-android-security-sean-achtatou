package b.a;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

public class b {
    private static Object kp = new c();
    private Map map;

    public b() {
        this.map = new HashMap();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0063 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x008e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001c  */
    public b(b.a.d r5) {
        /*
            r4 = this;
            r4.<init>()
            char r0 = r5.dy()
            r1 = 123(0x7b, float:1.72E-43)
            if (r0 == r1) goto L_0x0015
            java.lang.String r0 = "A JSONObject text must begin with '{'"
            b.a.f r0 = r5.av(r0)
            throw r0
        L_0x0012:
            r5.dw()
        L_0x0015:
            char r0 = r5.dy()
            switch(r0) {
                case 0: goto L_0x0063;
                case 125: goto L_0x008e;
                default: goto L_0x001c;
            }
        L_0x001c:
            r5.dw()
            java.lang.Object r0 = r5.dz()
            java.lang.String r0 = r0.toString()
            char r1 = r5.dy()
            r2 = 61
            if (r1 != r2) goto L_0x006a
            char r1 = r5.next()
            r2 = 62
            if (r1 == r2) goto L_0x003a
            r5.dw()
        L_0x003a:
            java.lang.Object r1 = r5.dz()
            if (r0 == 0) goto L_0x0078
            if (r1 == 0) goto L_0x0078
            java.lang.Object r2 = r4.I(r0)
            if (r2 == 0) goto L_0x0075
            b.a.f r1 = new b.a.f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Duplicate key \""
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = "\""
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x0063:
            java.lang.String r0 = "A JSONObject text must end with '}'"
            b.a.f r0 = r5.av(r0)
            throw r0
        L_0x006a:
            r2 = 58
            if (r1 == r2) goto L_0x003a
            java.lang.String r0 = "Expected a ':' after a key"
            b.a.f r0 = r5.av(r0)
            throw r0
        L_0x0075:
            r4.a(r0, r1)
        L_0x0078:
            char r0 = r5.dy()
            switch(r0) {
                case 44: goto L_0x0086;
                case 59: goto L_0x0086;
                case 125: goto L_0x008e;
                default: goto L_0x007f;
            }
        L_0x007f:
            java.lang.String r0 = "Expected a ',' or '}'"
            b.a.f r0 = r5.av(r0)
            throw r0
        L_0x0086:
            char r0 = r5.dy()
            r1 = 125(0x7d, float:1.75E-43)
            if (r0 != r1) goto L_0x0012
        L_0x008e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.b.<init>(b.a.d):void");
    }

    private b(Object obj) {
        this();
        Class<?> cls = obj.getClass();
        Method[] methods = cls.getClassLoader() != null ? cls.getMethods() : cls.getDeclaredMethods();
        for (int i = 0; i < methods.length; i++) {
            try {
                Method method = methods[i];
                if (Modifier.isPublic(method.getModifiers())) {
                    String name = method.getName();
                    String substring = name.startsWith("get") ? (name.equals("getClass") || name.equals("getDeclaringClass")) ? "" : name.substring(3) : name.startsWith("is") ? name.substring(2) : "";
                    if (substring.length() > 0 && Character.isUpperCase(substring.charAt(0)) && method.getParameterTypes().length == 0) {
                        if (substring.length() == 1) {
                            substring = substring.toLowerCase();
                        } else if (!Character.isUpperCase(substring.charAt(1))) {
                            substring = String.valueOf(substring.substring(0, 1).toLowerCase()) + substring.substring(1);
                        }
                        this.map.put(substring, f(method.invoke(obj, null)));
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    public b(String str) {
        this(new d(str));
    }

    public b(Map map2) {
        this.map = new HashMap();
        if (map2 != null) {
            for (Map.Entry entry : map2.entrySet()) {
                this.map.put(entry.getKey(), f(entry.getValue()));
            }
        }
    }

    private Object I(String str) {
        if (str == null) {
            return null;
        }
        return this.map.get(str);
    }

    private boolean K(String str) {
        try {
            return getBoolean(str);
        } catch (Exception e) {
            return false;
        }
    }

    public static Object Q(String str) {
        if (str.equals("")) {
            return str;
        }
        if (str.equalsIgnoreCase("true")) {
            return Boolean.TRUE;
        }
        if (str.equalsIgnoreCase("false")) {
            return Boolean.FALSE;
        }
        if (str.equalsIgnoreCase("null")) {
            return kp;
        }
        char charAt = str.charAt(0);
        if ((charAt >= '0' && charAt <= '9') || charAt == '.' || charAt == '-' || charAt == '+') {
            if (charAt == '0' && str.length() > 2 && (str.charAt(1) == 'x' || str.charAt(1) == 'X')) {
                try {
                    return new Integer(Integer.parseInt(str.substring(2), 16));
                } catch (Exception e) {
                }
            }
            try {
                if (str.indexOf(46) >= 0 || str.indexOf(101) >= 0 || str.indexOf(69) >= 0) {
                    return Double.valueOf(str);
                }
                Long l = new Long(str);
                return l.longValue() == ((long) l.intValue()) ? new Integer(l.intValue()) : l;
            } catch (Exception e2) {
            }
        }
        return str;
    }

    private static String a(Number number) {
        if (number == null) {
            throw new f("Null pointer");
        }
        d(number);
        String obj = number.toString();
        if (obj.indexOf(46) <= 0 || obj.indexOf(101) >= 0 || obj.indexOf(69) >= 0) {
            return obj;
        }
        while (obj.endsWith("0")) {
            obj = obj.substring(0, obj.length() - 1);
        }
        return obj.endsWith(".") ? obj.substring(0, obj.length() - 1) : obj;
    }

    static String a(Object obj, int i, int i2) {
        if (obj == null || obj.equals(null)) {
            return "null";
        }
        try {
            if (obj instanceof e) {
                String eG = ((e) obj).eG();
                if (eG instanceof String) {
                    return eG;
                }
            }
        } catch (Exception e) {
        }
        return obj instanceof Number ? a((Number) obj) : obj instanceof Boolean ? obj.toString() : obj instanceof b ? ((b) obj).toString(i, i2) : obj instanceof a ? ((a) obj).toString(i, i2) : obj instanceof Map ? new b((Map) obj).toString(i, i2) : obj instanceof Collection ? new a((Collection) obj).toString(i, i2) : obj.getClass().isArray() ? new a(obj).toString(i, i2) : quote(obj.toString());
    }

    private static void d(Object obj) {
        if (obj == null) {
            return;
        }
        if (obj instanceof Double) {
            if (((Double) obj).isInfinite() || ((Double) obj).isNaN()) {
                throw new f("JSON does not allow non-finite numbers.");
            }
        } else if (!(obj instanceof Float)) {
        } else {
            if (((Float) obj).isInfinite() || ((Float) obj).isNaN()) {
                throw new f("JSON does not allow non-finite numbers.");
            }
        }
    }

    static String e(Object obj) {
        if (obj == null || obj.equals(null)) {
            return "null";
        }
        if (!(obj instanceof e)) {
            return obj instanceof Number ? a((Number) obj) : ((obj instanceof Boolean) || (obj instanceof b) || (obj instanceof a)) ? obj.toString() : obj instanceof Map ? new b((Map) obj).toString() : obj instanceof Collection ? new a((Collection) obj).toString() : obj.getClass().isArray() ? new a(obj).toString() : quote(obj.toString());
        }
        try {
            String eG = ((e) obj).eG();
            if (eG instanceof String) {
                return eG;
            }
            throw new f("Bad value from toJSONString: " + ((Object) eG));
        } catch (Exception e) {
            throw new f(e);
        }
    }

    static Object f(Object obj) {
        if (obj == null) {
            try {
                return kp;
            } catch (Exception e) {
                return null;
            }
        } else if ((obj instanceof b) || (obj instanceof a) || kp.equals(obj) || (obj instanceof e) || (obj instanceof Byte) || (obj instanceof Character) || (obj instanceof Short) || (obj instanceof Integer) || (obj instanceof Long) || (obj instanceof Boolean) || (obj instanceof Float) || (obj instanceof Double) || (obj instanceof String)) {
            return obj;
        } else {
            if (obj instanceof Collection) {
                return new a((Collection) obj);
            }
            if (obj.getClass().isArray()) {
                return new a(obj);
            }
            if (obj instanceof Map) {
                return new b((Map) obj);
            }
            Package packageR = obj.getClass().getPackage();
            String name = packageR != null ? packageR.getName() : "";
            return (name.startsWith("java.") || name.startsWith("javax.") || obj.getClass().getClassLoader() == null) ? obj.toString() : new b(obj);
        }
    }

    private static String quote(String str) {
        char c = 0;
        if (str == null || str.length() == 0) {
            return "\"\"";
        }
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer(length + 4);
        stringBuffer.append('\"');
        int i = 0;
        while (i < length) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case 8:
                    stringBuffer.append("\\b");
                    break;
                case 9:
                    stringBuffer.append("\\t");
                    break;
                case 10:
                    stringBuffer.append("\\n");
                    break;
                case 12:
                    stringBuffer.append("\\f");
                    break;
                case 13:
                    stringBuffer.append("\\r");
                    break;
                case '\"':
                case '\\':
                    stringBuffer.append('\\');
                    stringBuffer.append(charAt);
                    break;
                case '/':
                    if (c == '<') {
                        stringBuffer.append('\\');
                    }
                    stringBuffer.append(charAt);
                    break;
                default:
                    if (charAt >= ' ' && ((charAt < 128 || charAt >= 160) && (charAt < 8192 || charAt >= 8448))) {
                        stringBuffer.append(charAt);
                        break;
                    } else {
                        String str2 = "000" + Integer.toHexString(charAt);
                        stringBuffer.append("\\u" + str2.substring(str2.length() - 4));
                        break;
                    }
                    break;
            }
            i++;
            c = charAt;
        }
        stringBuffer.append('\"');
        return stringBuffer.toString();
    }

    private String toString(int i, int i2) {
        int size = this.map.size();
        if (size == 0) {
            return "{}";
        }
        Iterator it = new TreeSet(this.map.keySet()).iterator();
        StringBuffer stringBuffer = new StringBuffer("{");
        int i3 = i2 + i;
        if (size == 1) {
            Object next = it.next();
            stringBuffer.append(quote(next.toString()));
            stringBuffer.append(": ");
            stringBuffer.append(a(this.map.get(next), i, i2));
        } else {
            while (it.hasNext()) {
                Object next2 = it.next();
                if (stringBuffer.length() > 1) {
                    stringBuffer.append(",\n");
                } else {
                    stringBuffer.append(10);
                }
                for (int i4 = 0; i4 < i3; i4++) {
                    stringBuffer.append(' ');
                }
                stringBuffer.append(quote(next2.toString()));
                stringBuffer.append(": ");
                stringBuffer.append(a(this.map.get(next2), i, i3));
            }
            if (stringBuffer.length() > 1) {
                stringBuffer.append(10);
                for (int i5 = 0; i5 < i2; i5++) {
                    stringBuffer.append(' ');
                }
            }
        }
        stringBuffer.append('}');
        return stringBuffer.toString();
    }

    public a E(String str) {
        Object obj = get(str);
        if (obj instanceof a) {
            return (a) obj;
        }
        throw new f("JSONObject[" + quote(str) + "] is not a JSONArray.");
    }

    public b F(String str) {
        Object obj = get(str);
        if (obj instanceof b) {
            return (b) obj;
        }
        throw new f("JSONObject[" + quote(str) + "] is not a JSONObject.");
    }

    public final boolean G(String str) {
        return this.map.containsKey(str);
    }

    public final boolean H(String str) {
        return kp.equals(I(str));
    }

    public final boolean J(String str) {
        return K(str);
    }

    public final int L(String str) {
        return b(str, 0);
    }

    public final a M(String str) {
        Object I = I(str);
        if (I instanceof a) {
            return (a) I;
        }
        return null;
    }

    public final b N(String str) {
        Object I = I(str);
        if (I instanceof b) {
            return (b) I;
        }
        return null;
    }

    public final String O(String str) {
        Object I = I(str);
        return I != null ? I.toString() : "";
    }

    public final Object P(String str) {
        return this.map.remove(str);
    }

    public final b a(String str, double d) {
        a(str, new Double(d));
        return this;
    }

    public final b a(String str, long j) {
        a(str, new Long(j));
        return this;
    }

    public final b a(String str, Object obj) {
        if (str == null) {
            throw new f("Null key.");
        }
        if (obj != null) {
            d(obj);
            this.map.put(str, obj);
        } else {
            P(str);
        }
        return this;
    }

    public final b a(String str, boolean z) {
        a(str, z ? Boolean.TRUE : Boolean.FALSE);
        return this;
    }

    public final int b(String str, int i) {
        try {
            return getInt(str);
        } catch (Exception e) {
            return i;
        }
    }

    public final Iterator bA() {
        return this.map.keySet().iterator();
    }

    public final String bB() {
        return toString(2, 0);
    }

    public final b c(String str, int i) {
        a(str, new Integer(i));
        return this;
    }

    public Object get(String str) {
        Object I = I(str);
        if (I != null) {
            return I;
        }
        throw new f("JSONObject[" + quote(str) + "] not found.");
    }

    public boolean getBoolean(String str) {
        Object obj = get(str);
        if (obj.equals(Boolean.FALSE) || ((obj instanceof String) && ((String) obj).equalsIgnoreCase("false"))) {
            return false;
        }
        if (obj.equals(Boolean.TRUE) || ((obj instanceof String) && ((String) obj).equalsIgnoreCase("true"))) {
            return true;
        }
        throw new f("JSONObject[" + quote(str) + "] is not a Boolean.");
    }

    public double getDouble(String str) {
        Object obj = get(str);
        try {
            return obj instanceof Number ? ((Number) obj).doubleValue() : Double.valueOf((String) obj).doubleValue();
        } catch (Exception e) {
            throw new f("JSONObject[" + quote(str) + "] is not a number.");
        }
    }

    public int getInt(String str) {
        Object obj = get(str);
        try {
            return obj instanceof Number ? ((Number) obj).intValue() : Integer.parseInt((String) obj);
        } catch (Exception e) {
            throw new f("JSONObject[" + quote(str) + "] is not an int.");
        }
    }

    public long getLong(String str) {
        Object obj = get(str);
        try {
            return obj instanceof Number ? ((Number) obj).longValue() : Long.parseLong((String) obj);
        } catch (Exception e) {
            throw new f("JSONObject[" + quote(str) + "] is not a long.");
        }
    }

    public String getString(String str) {
        return get(str).toString();
    }

    public String toString() {
        try {
            Iterator bA = bA();
            StringBuffer stringBuffer = new StringBuffer("{");
            while (bA.hasNext()) {
                if (stringBuffer.length() > 1) {
                    stringBuffer.append(',');
                }
                Object next = bA.next();
                stringBuffer.append(quote(next.toString()));
                stringBuffer.append(':');
                stringBuffer.append(e(this.map.get(next)));
            }
            stringBuffer.append('}');
            return stringBuffer.toString();
        } catch (Exception e) {
            return null;
        }
    }
}
