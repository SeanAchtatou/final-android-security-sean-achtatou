package b.a;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public final class d {
    private boolean eof;
    private int index;
    private int sA;
    private int sB;
    private char sC;
    private Reader sD;
    private boolean sE;

    private d(Reader reader) {
        this.sD = reader.markSupported() ? reader : new BufferedReader(reader);
        this.eof = false;
        this.sE = false;
        this.sC = 0;
        this.index = 0;
        this.sA = 1;
        this.sB = 1;
    }

    public d(String str) {
        this(new StringReader(str));
    }

    private String dx() {
        char[] cArr = new char[4];
        for (int i = 0; i < 4; i++) {
            cArr[i] = next();
            if (this.eof && !this.sE) {
                throw av("Substring bounds error");
            }
        }
        return new String(cArr);
    }

    public final f av(String str) {
        return new f(String.valueOf(str) + toString());
    }

    public final void dw() {
        if (this.sE || this.index <= 0) {
            throw new f("Stepping back two steps is not supported");
        }
        this.index--;
        this.sA--;
        this.sE = true;
        this.eof = false;
    }

    public final char dy() {
        char next;
        do {
            next = next();
            if (next == 0) {
                break;
            }
        } while (next <= ' ');
        return next;
    }

    public final Object dz() {
        char dy = dy();
        switch (dy) {
            case '\"':
            case '\'':
                StringBuffer stringBuffer = new StringBuffer();
                while (true) {
                    char next = next();
                    switch (next) {
                        case 0:
                        case 10:
                        case 13:
                            throw av("Unterminated string");
                        case '\\':
                            char next2 = next();
                            switch (next2) {
                                case '\"':
                                case '\'':
                                case '/':
                                case '\\':
                                    stringBuffer.append(next2);
                                    continue;
                                case 'b':
                                    stringBuffer.append(8);
                                    continue;
                                case 'f':
                                    stringBuffer.append(12);
                                    continue;
                                case 'n':
                                    stringBuffer.append(10);
                                    continue;
                                case 'r':
                                    stringBuffer.append(13);
                                    continue;
                                case 't':
                                    stringBuffer.append(9);
                                    continue;
                                case 'u':
                                    stringBuffer.append((char) Integer.parseInt(dx(), 16));
                                    continue;
                                default:
                                    throw av("Illegal escape.");
                            }
                        default:
                            if (next != dy) {
                                stringBuffer.append(next);
                                break;
                            } else {
                                return stringBuffer.toString();
                            }
                    }
                }
            case '(':
            case '[':
                dw();
                return new a(this);
            case '{':
                dw();
                return new b(this);
            default:
                StringBuffer stringBuffer2 = new StringBuffer();
                while (dy >= ' ' && ",:]}/\\\"[{;=#".indexOf(dy) < 0) {
                    stringBuffer2.append(dy);
                    dy = next();
                }
                dw();
                String trim = stringBuffer2.toString().trim();
                if (!trim.equals("")) {
                    return b.Q(trim);
                }
                throw av("Missing value");
        }
    }

    public final char next() {
        int read;
        if (this.sE) {
            this.sE = false;
            read = this.sC;
        } else {
            try {
                read = this.sD.read();
                if (read <= 0) {
                    this.eof = true;
                    read = 0;
                }
            } catch (IOException e) {
                throw new f(e);
            }
        }
        this.index++;
        if (this.sC == 13) {
            this.sB++;
            this.sA = read == 10 ? 0 : 1;
        } else if (read == 10) {
            this.sB++;
            this.sA = 0;
        } else {
            this.sA++;
        }
        this.sC = (char) read;
        return this.sC;
    }

    public final String toString() {
        return " at " + this.index + " [character " + this.sA + " line " + this.sB + "]";
    }
}
