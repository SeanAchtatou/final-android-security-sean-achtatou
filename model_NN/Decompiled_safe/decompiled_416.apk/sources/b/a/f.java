package b.a;

public final class f extends Exception {
    private Throwable Ci;

    public f(String str) {
        super(str);
    }

    public f(Throwable th) {
        super(th.getMessage());
        this.Ci = th;
    }

    public final Throwable getCause() {
        return this.Ci;
    }
}
