package b.a;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;

public final class a {
    private ArrayList gU;

    public a() {
        this.gU = new ArrayList();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005c A[SYNTHETIC] */
    public a(b.a.d r5) {
        /*
            r4 = this;
            r3 = 93
            r4.<init>()
            char r0 = r5.dy()
            r1 = 91
            if (r0 != r1) goto L_0x0015
            r0 = r3
        L_0x000e:
            char r1 = r5.dy()
            if (r1 != r3) goto L_0x0023
        L_0x0014:
            return
        L_0x0015:
            r1 = 40
            if (r0 != r1) goto L_0x001c
            r0 = 41
            goto L_0x000e
        L_0x001c:
            java.lang.String r0 = "A JSONArray text must start with '['"
            b.a.f r0 = r5.av(r0)
            throw r0
        L_0x0023:
            r5.dw()
        L_0x0026:
            char r1 = r5.dy()
            r2 = 44
            if (r1 != r2) goto L_0x0045
            r5.dw()
            java.util.ArrayList r1 = r4.gU
            r2 = 0
            r1.add(r2)
        L_0x0037:
            char r1 = r5.dy()
            switch(r1) {
                case 41: goto L_0x005c;
                case 44: goto L_0x0052;
                case 59: goto L_0x0052;
                case 93: goto L_0x005c;
                default: goto L_0x003e;
            }
        L_0x003e:
            java.lang.String r0 = "Expected a ',' or ']'"
            b.a.f r0 = r5.av(r0)
            throw r0
        L_0x0045:
            r5.dw()
            java.util.ArrayList r1 = r4.gU
            java.lang.Object r2 = r5.dz()
            r1.add(r2)
            goto L_0x0037
        L_0x0052:
            char r1 = r5.dy()
            if (r1 == r3) goto L_0x0014
            r5.dw()
            goto L_0x0026
        L_0x005c:
            if (r0 == r1) goto L_0x0014
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Expected a '"
            r1.<init>(r2)
            java.lang.Character r2 = new java.lang.Character
            r2.<init>(r0)
            java.lang.StringBuilder r0 = r1.append(r2)
            java.lang.String r1 = "'"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            b.a.f r0 = r5.av(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.<init>(b.a.d):void");
    }

    public a(Object obj) {
        this();
        if (obj.getClass().isArray()) {
            int length = Array.getLength(obj);
            for (int i = 0; i < length; i++) {
                b(b.f(Array.get(obj, i)));
            }
            return;
        }
        throw new f("JSONArray initial value should be a string or collection or array.");
    }

    public a(Collection collection) {
        this.gU = new ArrayList();
        if (collection != null) {
            for (Object f : collection) {
                this.gU.add(b.f(f));
            }
        }
    }

    private Object get(int i) {
        Object obj = (i < 0 || i >= this.gU.size()) ? null : this.gU.get(i);
        if (obj != null) {
            return obj;
        }
        throw new f("JSONArray[" + i + "] not found.");
    }

    private String w(String str) {
        int size = this.gU.size();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                stringBuffer.append(str);
            }
            stringBuffer.append(b.e(this.gU.get(i)));
        }
        return stringBuffer.toString();
    }

    public final a b(Object obj) {
        this.gU.add(obj);
        return this;
    }

    public final String getString(int i) {
        return get(i).toString();
    }

    public final int length() {
        return this.gU.size();
    }

    public final b m(int i) {
        Object obj = get(i);
        if (obj instanceof b) {
            return (b) obj;
        }
        throw new f("JSONArray[" + i + "] is not a JSONObject.");
    }

    public final String toString() {
        try {
            return String.valueOf('[') + w(",") + ']';
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final String toString(int i, int i2) {
        int size = this.gU.size();
        if (size == 0) {
            return "[]";
        }
        StringBuffer stringBuffer = new StringBuffer("[");
        if (size == 1) {
            stringBuffer.append(b.a(this.gU.get(0), i, i2));
        } else {
            int i3 = i2 + i;
            stringBuffer.append(10);
            for (int i4 = 0; i4 < size; i4++) {
                if (i4 > 0) {
                    stringBuffer.append(",\n");
                }
                for (int i5 = 0; i5 < i3; i5++) {
                    stringBuffer.append(' ');
                }
                stringBuffer.append(b.a(this.gU.get(i4), i, i3));
            }
            stringBuffer.append(10);
            for (int i6 = 0; i6 < i2; i6++) {
                stringBuffer.append(' ');
            }
        }
        stringBuffer.append(']');
        return stringBuffer.toString();
    }
}
