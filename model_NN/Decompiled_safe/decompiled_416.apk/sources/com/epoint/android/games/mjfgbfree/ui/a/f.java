package com.epoint.android.games.mjfgbfree.ui.a;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import com.epoint.android.games.mjfgbfree.MJ16ViewActivity;
import com.epoint.android.games.mjfgbfree.af;
import com.epoint.android.games.mjfgbfree.bb;
import com.epoint.android.games.mjfgbfree.e.o;
import com.epoint.android.games.mjfgbfree.ui.a;
import java.util.Vector;

public class f extends e implements i {
    private a gI;
    private a gJ;
    private a gK;
    private WebView gL;
    private Vector gM;
    private Paint gN;
    private String gP;
    private Thread gR;
    private int gl;
    private int gm;
    private int go;
    private o ho;
    private int mc;
    private int md;
    private boolean me;

    public f() {
        this(-1, Color.rgb(128, 0, 0), 200, -1, false);
    }

    public f(int i, int i2, int i3, int i4, boolean z) {
        this.mc = 1;
        this.gM = new Vector();
        this.gP = "";
        this.ho = null;
        this.gm = i;
        this.gl = i2;
        this.go = i3;
        this.md = i4;
        this.me = z;
        this.gI = new a(100, 60, -65536, -16777216, 160, af.aa("好"));
        this.gJ = new a(100, 60, Color.rgb(205, 100, 0), -16777216, 160, af.aa("抓圖"));
        this.gK = new a(100, 60, Color.rgb(205, 100, 0), -16777216, 160, af.aa("分享"));
        this.gI.a(true, false);
        this.gM.addElement(this.gI);
        this.gN = new Paint();
        this.gN.setAlpha(255);
        this.gN.setStyle(Paint.Style.FILL);
        this.gN.setAntiAlias(true);
        this.gN.setTextSize(28.0f);
        this.gN.setColor(-16777216);
    }

    private void bh() {
        this.gM.size();
        if (MJ16Activity.ra > MJ16Activity.qZ) {
            this.js = new a(420, 350, this.gm, this.gl, this.go, null);
            this.ju = new Rect(this.js.getBounds());
            return;
        }
        this.js = new a(420, 350, this.gm, this.gl, this.go, null);
        this.ju = new Rect(this.js.getBounds());
    }

    private void e(boolean z) {
        this.gJ.b(false);
        this.gK.b(false);
        this.gI.a(z, false);
        this.gJ.a(z, false);
        this.gK.a(z, false);
    }

    private void f(boolean z) {
        e(false);
        this.jr.a((int) (((float) this.gJ.ba()) * MJ16Activity.rc), z);
    }

    public final Object a(int i, int i2, int i3, Object obj) {
        int i4 = 0;
        a aVar = null;
        while (i4 < this.gM.size()) {
            a aVar2 = (a) this.gM.elementAt(i4);
            aVar2.b(false);
            if (aVar2.isVisible() && aVar2.bf()) {
                Rect bounds = aVar2.getBounds();
                if (i >= bounds.left && i <= bounds.right && i2 >= bounds.top && i2 <= bounds.bottom && (i3 == 0 || (i3 != 0 && obj == aVar2))) {
                    aVar2.b(true);
                    i4++;
                    aVar = aVar2;
                }
            }
            aVar2 = aVar;
            i4++;
            aVar = aVar2;
        }
        if (aVar != null) {
            return aVar;
        }
        return null;
    }

    public final void a(Bitmap bitmap, boolean z) {
        e(true);
        this.jr.er();
        this.jq.a(z, bitmap);
    }

    public final synchronized void a(Canvas canvas) {
        int ba;
        int i;
        if (this.jt == 1 || this.jt >= 4) {
            bh();
            int width = this.ju.width();
            int height = this.ju.height();
            this.ju.left = (MJ16Activity.qZ - width) / 2;
            this.ju.top = (bb.ty == 1 ? 0 : bb.dR()) + (((MJ16Activity.ra - bb.dR()) - height) / 2);
            this.ju.right = width + this.ju.left;
            this.ju.bottom = height + this.ju.top;
            this.gI.c((MJ16Activity.qZ - this.gI.bb()) / 2, (this.ju.bottom - this.gI.ba()) - 10);
            this.gJ.c(this.ju.left + 10, (this.ju.bottom - this.gJ.ba()) - 10);
            this.gK.c((this.ju.right - this.gK.bb()) - 10, (this.ju.bottom - this.gK.ba()) - 10);
            Rect rect = this.ju;
            int i2 = rect.left + 10;
            int i3 = rect.top + 10;
            int i4 = 0;
            while (i4 < this.gM.size() - this.mc) {
                ((a) this.gM.elementAt(i4)).c(i2, i3);
                if (MJ16Activity.ra > MJ16Activity.qZ) {
                    i = ((a) this.gM.elementAt(i4)).bb() + 10 + i2;
                    ba = i3;
                } else {
                    int i5 = i2;
                    ba = ((a) this.gM.elementAt(i4)).ba() + 10 + i3;
                    i = i5;
                }
                i4++;
                i3 = ba;
                i2 = i;
            }
        }
        super.a(canvas);
    }

    public final void a(MJ16ViewActivity mJ16ViewActivity, bb bbVar) {
        this.jq = mJ16ViewActivity;
        this.jr = bbVar;
        bh();
        this.gL = (WebView) this.jq.findViewById(C0000R.id.ResultView);
        this.gL.setBackgroundColor(0);
        this.gL.setInitialScale(100);
        this.gL.setVerticalFadingEdgeEnabled(true);
        this.gL.loadDataWithBaseURL("/", "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"DTD/xhtml1-transitional.dtd\"><html><head><META HTTP-EQUIV=\"content-type\" CONTENT=\"text/html; charset=UTF-8\" /></head><body marginwidth=\"0\" marginheight=\"0\" leftmargin=\"0\" topmargin=\"0\"></body></html>", "text/html", "UTF-8", "/");
    }

    public final void a(Object obj) {
        if (obj == this.gI) {
            this.gI.b(false);
            super.a(obj);
            this.gL.setVisibility(8);
            if (this.me) {
                Rect rect = new Rect();
                rect.left = 0;
                rect.top = 0;
                rect.bottom = MJ16Activity.ra;
                rect.right = MJ16Activity.qZ;
                this.jr.ut = rect;
            }
            super.b(this.gR);
        } else if (obj == this.gJ) {
            f(false);
        } else if (obj == this.gK) {
            f(true);
        }
    }

    public final void a(String str, o oVar, boolean z) {
        this.gM.removeAllElements();
        this.gP = str;
        this.gL.clearHistory();
        this.gL.loadDataWithBaseURL("/", "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"DTD/xhtml1-transitional.dtd\"><html><head><META HTTP-EQUIV=\"content-type\" CONTENT=\"text/html; charset=UTF-8\" /></head><body marginwidth=\"0\" marginheight=\"0\" leftmargin=\"0\" topmargin=\"0\">" + this.gP + "<br>" + "</body>" + "</html>", "text/html", "UTF-8", "/");
        this.gM.addElement(this.gI);
        this.gJ.a(z, false);
        this.gK.a(z, false);
        if (z) {
            this.gM.add(this.gJ);
            this.gM.add(this.gK);
            this.mc = 3;
        } else {
            this.mc = 1;
        }
        this.ho = oVar;
    }

    public final void a(Thread thread) {
        this.gR = thread;
        a(this.gI);
    }

    public final void b(Canvas canvas) {
        canvas.drawARGB(64, 0, 0, 0);
        this.js.draw(canvas);
        int i = this.js.getBounds().top + 20;
        for (int i2 = 0; i2 < this.gM.size(); i2++) {
            if (((a) this.gM.elementAt(i2)).isVisible()) {
                ((a) this.gM.elementAt(i2)).draw(canvas);
            }
        }
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) ((LinearLayout) this.gL.getParent()).getLayoutParams();
        double d = ((double) MJ16Activity.qR) / ((double) MJ16Activity.ra);
        double d2 = ((double) MJ16Activity.qS) / ((double) MJ16Activity.qZ);
        layoutParams.bottomMargin = (MJ16Activity.ra - this.gI.getBounds().top) + (this.md == -1 ? 4 : 15);
        layoutParams.bottomMargin = (int) (((double) layoutParams.bottomMargin) * d);
        layoutParams.leftMargin = (int) (((double) (this.js.getBounds().left + 23)) * d2);
        layoutParams.rightMargin = layoutParams.leftMargin;
        layoutParams.topMargin = i;
        layoutParams.topMargin = (int) (((double) layoutParams.topMargin) * d);
        ((LinearLayout) this.gL.getParent()).setLayoutParams(layoutParams);
        this.gL.setVisibility(0);
        ((LinearLayout) this.gL.getParent()).bringToFront();
        this.gL.scrollTo(0, 0);
        if (this.md != -1) {
            Rect bounds = this.js.getBounds();
            int bd = this.js.bd();
            this.js.l(this.md);
            this.js.setBounds(new Rect(((int) (((double) layoutParams.leftMargin) / d2)) - 8, ((int) (((double) layoutParams.topMargin) / d)) - 5, (MJ16Activity.qZ - ((int) (((double) layoutParams.rightMargin) / d2))) + 8, (MJ16Activity.ra - ((int) (((double) layoutParams.bottomMargin) / d))) + 5));
            this.js.draw(canvas);
            this.js.setBounds(bounds);
            this.js.l(bd);
        }
    }

    public final void bi() {
        a((Thread) null);
    }

    public final void bk() {
        if (this.jw) {
            this.gI.b(false);
            if (this.ho != null) {
                this.ho.a(5, this);
            } else {
                this.jr.a(this, -1, null);
            }
        }
    }
}
