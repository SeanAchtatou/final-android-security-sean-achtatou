package com.epoint.android.games.mjfgbfree;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;
import com.epoint.android.games.mjfgbfree.ui.entity.f;

final class s implements AdapterView.OnItemClickListener {
    private /* synthetic */ MJ16ViewActivity iu;

    s(MJ16ViewActivity mJ16ViewActivity) {
        this.iu = mJ16ViewActivity;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        ComponentName componentName = ((f) view.getTag()).By;
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("image/jpeg");
        intent.setComponent(componentName);
        intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(this.iu.wj));
        this.iu.startActivity(intent);
        this.iu.wk.dismiss();
    }
}
