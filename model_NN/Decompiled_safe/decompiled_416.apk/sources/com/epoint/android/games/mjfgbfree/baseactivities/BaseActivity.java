package com.epoint.android.games.mjfgbfree.baseactivities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import b.a.b;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public abstract class BaseActivity extends Activity implements Handler.Callback {
    private static int ot = 0;
    private static volatile Hashtable ox = null;
    private Handler mHandler;
    protected boolean oA = false;
    /* access modifiers changed from: private */
    public volatile Hashtable oB = new Hashtable();
    private volatile Hashtable oC = new Hashtable();
    /* access modifiers changed from: private */
    public volatile Vector oD = new Vector();
    /* access modifiers changed from: private */
    public boolean oE = false;
    private String os;
    private int ou = -1;
    private String ov;
    private ProgressDialog ow;
    protected boolean oy = false;
    protected int oz = 0;

    static /* synthetic */ boolean a(BaseActivity baseActivity, e eVar, b bVar) {
        if (!bVar.H("error")) {
            if (eVar.dM == 2) {
                baseActivity.b(eVar.dL, bVar);
            }
            baseActivity.bm(bVar.F("error").getString("message"));
            return false;
        }
        baseActivity.b(eVar.dL, bVar);
        return true;
    }

    private synchronized void ap(String str) {
        ox.remove(String.valueOf(this.os) + str);
    }

    private synchronized void cB() {
        synchronized (ox) {
            Enumeration keys = ox.keys();
            synchronized (keys) {
                while (keys.hasMoreElements()) {
                    String str = (String) keys.nextElement();
                    if (str.startsWith(this.os)) {
                        ox.remove(str);
                    }
                }
            }
        }
    }

    public final void a(Bundle bundle, String str) {
        if (ox == null) {
            ox = new Hashtable();
        }
        super.onCreate(bundle);
        this.ov = str;
        StringBuilder append = new StringBuilder(String.valueOf(str)).append("#");
        int i = ot + 1;
        ot = i;
        this.os = append.append(i).append("#").toString();
        this.mHandler = new Handler(this);
        this.ow = new ProgressDialog(this);
        this.ow.setMessage("Loading...");
        this.ow.setIndeterminate(true);
        this.ow.setCancelable(false);
    }

    /* access modifiers changed from: protected */
    public final void a(Message message) {
        this.mHandler.sendMessage(message);
    }

    /* access modifiers changed from: protected */
    public final void a(e eVar) {
        this.oB.put(eVar.dL, eVar);
    }

    /* access modifiers changed from: protected */
    public final synchronized Object ao(String str) {
        return ox.get(String.valueOf(this.os) + str);
    }

    /* access modifiers changed from: protected */
    public final void b(Exception exc) {
        Message message = new Message();
        message.what = 999;
        message.obj = exc.toString();
        a(message);
    }

    /* access modifiers changed from: protected */
    public final synchronized void b(String str, Object obj) {
        ox.put(String.valueOf(this.os) + str, obj);
    }

    /* access modifiers changed from: protected */
    public final void bm(String str) {
        Message message = new Message();
        message.what = 997;
        message.obj = str;
        a(message);
    }

    /* access modifiers changed from: protected */
    public final boolean cC() {
        return !this.oD.isEmpty();
    }

    /* access modifiers changed from: protected */
    public final boolean cD() {
        if (this.oB.size() == 0) {
            return false;
        }
        if (this.oD.size() != 0) {
            return false;
        }
        this.oE = false;
        Message message = new Message();
        message.what = 0;
        a(message);
        Enumeration elements = this.oB.elements();
        while (elements.hasMoreElements()) {
            this.oD.addElement(new b(this, this, (e) elements.nextElement()));
        }
        synchronized (this.oD) {
            int i = 0;
            while (i < this.oD.size() && i < 5) {
                ((Thread) this.oD.elementAt(i)).start();
                i++;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final void cE() {
        if (!this.ow.isShowing()) {
            Message message = new Message();
            message.what = 996;
            a(message);
        }
    }

    /* access modifiers changed from: protected */
    public final void cF() {
        if (this.ow.isShowing()) {
            Message message = new Message();
            message.what = 995;
            a(message);
        }
    }

    public synchronized boolean handleMessage(Message message) {
        switch (message.what) {
            case 0:
                setProgressBarIndeterminateVisibility(true);
                break;
            case 2:
            case 4:
                if (message.obj != null) {
                    ap((String) message.obj);
                    break;
                }
                break;
            case 5:
            case 6:
                setProgressBarIndeterminateVisibility(false);
                break;
            case 995:
                dismissDialog(1);
                break;
            case 996:
                showDialog(1);
                break;
            case 997:
                Toast.makeText(this, (String) message.obj, 0).show();
                break;
            case 998:
                Toast.makeText(this, (String) message.obj, 1).show();
                break;
            case 999:
                Toast.makeText(this, (String) message.obj, 0).show();
                break;
        }
        return false;
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (intent != null) {
            switch (i2) {
                case -1:
                    if (intent.getBooleanExtra("system_back_to_main", false)) {
                        setResult(i2, intent);
                        finish();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 1:
                return this.ow;
            default:
                return null;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public void onDestroy() {
        cB();
        ot--;
        super.onDestroy();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 999:
                cB();
                Intent intent = super.getIntent();
                intent.putExtra("system_restart_activity", true);
                setResult(-1, intent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    public void onWindowFocusChanged(boolean z) {
        if (z) {
            this.oy = true;
        }
        super.onWindowFocusChanged(z);
    }
}
