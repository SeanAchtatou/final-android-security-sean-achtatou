package com.epoint.android.games.mjfgbfree.cam;

import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.io.IOException;

public final class f extends SurfaceView implements Camera.AutoFocusCallback, SurfaceHolder.Callback {
    Camera bf;
    private SurfaceHolder zy = getHolder();
    private CapturePlayerActivity zz;

    f(CapturePlayerActivity capturePlayerActivity) {
        super(capturePlayerActivity);
        this.zz = capturePlayerActivity;
        this.zy.addCallback(this);
        this.zy.setType(3);
    }

    public final void onAutoFocus(boolean z, Camera camera) {
        if (z) {
            camera.takePicture(this.zz.vp, this.zz.vq, this.zz.vr);
        }
    }

    public final void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        Camera.Parameters parameters = this.bf.getParameters();
        parameters.setPreviewSize(i2, i3);
        this.bf.setParameters(parameters);
        this.bf.startPreview();
    }

    public final void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.bf = Camera.open();
        try {
            this.bf.setPreviewDisplay(surfaceHolder);
            this.bf.setPreviewCallback(new e(this));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.bf.stopPreview();
        this.bf.release();
        this.bf = null;
    }
}
