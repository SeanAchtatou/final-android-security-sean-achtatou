package com.epoint.android.games.mjfgbfree;

import android.preference.Preference;

final class bs implements Preference.OnPreferenceChangeListener {
    private /* synthetic */ PreferencesActivity qn;

    bs(PreferencesActivity preferencesActivity) {
        this.qn = preferencesActivity;
    }

    public final boolean onPreferenceChange(Preference preference, Object obj) {
        preference.setSummary(String.valueOf(obj));
        return true;
    }
}
