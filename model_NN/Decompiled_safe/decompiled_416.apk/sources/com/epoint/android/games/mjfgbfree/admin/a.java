package com.epoint.android.games.mjfgbfree.admin;

import android.graphics.Color;
import android.view.View;
import android.widget.AdapterView;
import com.epoint.android.games.mjfgbfree.bb;

final class a implements AdapterView.OnItemSelectedListener {
    private /* synthetic */ MJColorPicker eh;

    a(MJColorPicker mJColorPicker) {
        this.eh = mJColorPicker;
    }

    public final void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        if (this.eh.jd != -1) {
            this.eh.iR.setProgress(Color.red(((Integer) this.eh.jb.elementAt(i)).intValue()));
            this.eh.iS.setProgress(Color.green(((Integer) this.eh.jb.elementAt(i)).intValue()));
            this.eh.iT.setProgress(Color.blue(((Integer) this.eh.jb.elementAt(i)).intValue()));
            this.eh.iU.setText(String.valueOf(this.eh.iR.getProgress()));
            this.eh.iV.setText(String.valueOf(this.eh.iS.getProgress()));
            this.eh.iW.setText(String.valueOf(this.eh.iT.getProgress()));
            bb.d(this.eh, Color.rgb(Integer.valueOf(this.eh.iU.getText().toString()).intValue(), Integer.valueOf(this.eh.iV.getText().toString()).intValue(), Integer.valueOf(this.eh.iW.getText().toString()).intValue()));
            this.eh.iZ.invalidate();
            return;
        }
        this.eh.jd = i;
    }

    public final void onNothingSelected(AdapterView adapterView) {
    }
}
