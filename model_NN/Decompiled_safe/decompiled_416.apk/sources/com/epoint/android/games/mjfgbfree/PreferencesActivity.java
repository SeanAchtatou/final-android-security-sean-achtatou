package com.epoint.android.games.mjfgbfree;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.text.InputFilter;
import android.view.KeyEvent;
import com.epoint.android.games.mjfgbfree.a.d;
import com.epoint.android.games.mjfgbfree.ui.entity.CheckboxListPreference;
import com.epoint.android.games.mjfgbfree.ui.entity.IconListPreference;

public class PreferencesActivity extends PreferenceActivity {
    /* access modifiers changed from: private */
    public CheckboxListPreference jC;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setVolumeControlStream(3);
        PreferenceScreen createPreferenceScreen = getPreferenceManager().createPreferenceScreen(this);
        EditTextPreference editTextPreference = new EditTextPreference(this);
        if (!getIntent().getBooleanExtra("is_playing", false)) {
            editTextPreference.setKey("text_user_name_preference");
            editTextPreference.setTitle(String.valueOf(af.aa("玩家名字")) + " (" + af.aa("網路模式") + ")");
            editTextPreference.setDialogTitle(editTextPreference.getTitle());
            editTextPreference.setText(ai.nm);
            editTextPreference.getEditText().setSingleLine();
            editTextPreference.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(16)});
            editTextPreference.setOnPreferenceChangeListener(new bm(this));
            editTextPreference.setSummary(ai.nm);
            createPreferenceScreen.addPreference(editTextPreference);
            IconListPreference iconListPreference = new IconListPreference(this);
            iconListPreference.setEntries(ai.nk == 2 ? C0000R.array.player_pic : ai.nk == 3 ? C0000R.array.player_pic_gb : ai.nk == 4 ? C0000R.array.player_pic_jp : ai.nk == 5 ? C0000R.array.player_pic_fr : C0000R.array.player_pic_en);
            iconListPreference.setEntryValues((int) C0000R.array.player_pic_values);
            iconListPreference.setKey("text_player_pic_preference");
            iconListPreference.setTitle(String.valueOf(af.aa("頭像")) + " (" + af.aa("網路模式") + ")");
            iconListPreference.setSummary((CharSequence) null);
            iconListPreference.setDefaultValue(ai.nn);
            iconListPreference.c(d.c(this, "images/" + ai.nn + ".png"), -1);
            iconListPreference.setOnPreferenceChangeListener(new bn(this));
            createPreferenceScreen.addPreference(iconListPreference);
            if (MJ16Activity.rf) {
                IconListPreference iconListPreference2 = new IconListPreference(this);
                iconListPreference2.setEntries(ai.nk == 2 ? C0000R.array.wallpaper : ai.nk == 3 ? C0000R.array.wallpaper_gb : ai.nk == 4 ? C0000R.array.wallpaper_jp : ai.nk == 5 ? C0000R.array.wallpaper_fr : C0000R.array.wallpaper_en);
                iconListPreference2.setEntryValues((int) C0000R.array.wallpaper_values);
                iconListPreference2.setKey("text_wallpaper_preference");
                iconListPreference2.setTitle(af.aa("背景"));
                iconListPreference2.setSummary((CharSequence) null);
                iconListPreference2.setDefaultValue(ai.nu);
                iconListPreference2.c(d.c(this, "images/" + ai.nu + ".png"), -1);
                iconListPreference2.setOnPreferenceChangeListener(new bo(this));
                createPreferenceScreen.addPreference(iconListPreference2);
            }
            IconListPreference iconListPreference3 = new IconListPreference(this, ai.no);
            iconListPreference3.setEntries((int) C0000R.array.tile_color);
            iconListPreference3.setEntryValues((int) C0000R.array.tile_color_values);
            iconListPreference3.setKey("list_tile_color_preference");
            iconListPreference3.setTitle(af.aa("麻雀牌顏色"));
            iconListPreference3.setSummary((CharSequence) null);
            iconListPreference3.setDefaultValue(ai.no);
            iconListPreference3.b(d.c(this, "images/frame_none.gif"));
            iconListPreference3.c(d.c(this, "images/bk.gif"), 0);
            iconListPreference3.setOnPreferenceChangeListener(new bp(this));
            createPreferenceScreen.addPreference(iconListPreference3);
            ListPreference listPreference = new ListPreference(this);
            listPreference.setEntries((int) C0000R.array.langauge);
            listPreference.setEntryValues((int) C0000R.array.language_values);
            listPreference.setKey("list_langauge_preference");
            listPreference.setTitle(af.aa("語言"));
            listPreference.setDefaultValue(String.valueOf(ai.nk));
            listPreference.setSummary(listPreference.getEntries()[ai.nk - 1]);
            listPreference.setOnPreferenceChangeListener(new bq(this));
            createPreferenceScreen.addPreference(listPreference);
            ListPreference listPreference2 = new ListPreference(this);
            listPreference2.setEntryValues((int) C0000R.array.game_rounds_values);
            listPreference2.setKey("list_game_rounds_preference");
            listPreference2.setEntries((int) C0000R.array.game_rounds);
            listPreference2.setTitle(af.aa("每局盤數"));
            listPreference2.setDefaultValue(String.valueOf(ai.nr));
            listPreference2.setSummary(String.valueOf(ai.nr));
            listPreference2.setOnPreferenceChangeListener(new br(this));
            createPreferenceScreen.addPreference(listPreference2);
            ListPreference listPreference3 = new ListPreference(this);
            listPreference3.setEntries((int) C0000R.array.min_fans);
            listPreference3.setEntryValues((int) C0000R.array.min_fans);
            listPreference3.setKey("list_min_fans_preference");
            listPreference3.setTitle(af.aa("起胡番數"));
            listPreference3.setDefaultValue(String.valueOf(ai.ns));
            listPreference3.setSummary(String.valueOf(ai.ns));
            listPreference3.setOnPreferenceChangeListener(new bs(this));
            createPreferenceScreen.addPreference(listPreference3);
            if (!MJ16Activity.qL) {
                CheckBoxPreference checkBoxPreference = new CheckBoxPreference(this);
                checkBoxPreference.setKey("toggle_hide_notification_bar_preference");
                checkBoxPreference.setTitle(af.aa("以全屏幕進行游戲"));
                checkBoxPreference.setDefaultValue(Boolean.valueOf(ai.nt));
                checkBoxPreference.setOnPreferenceChangeListener(new bt(this));
                createPreferenceScreen.addPreference(checkBoxPreference);
            }
        }
        CheckBoxPreference checkBoxPreference2 = new CheckBoxPreference(this);
        checkBoxPreference2.setKey("toggle_autoget_preference");
        checkBoxPreference2.setTitle(af.aa("自動摸牌"));
        checkBoxPreference2.setDefaultValue(Boolean.valueOf(ai.ng));
        createPreferenceScreen.addPreference(checkBoxPreference2);
        CheckBoxPreference checkBoxPreference3 = new CheckBoxPreference(this);
        checkBoxPreference3.setKey("toggle_display_chara_preference");
        checkBoxPreference3.setTitle(af.aa("不要顯示動畫角色"));
        checkBoxPreference3.setDefaultValue(Boolean.valueOf(!ai.np));
        createPreferenceScreen.addPreference(checkBoxPreference3);
        CheckBoxPreference checkBoxPreference4 = new CheckBoxPreference(this);
        checkBoxPreference4.setKey("toggle_eco_preference");
        checkBoxPreference4.setTitle(af.aa("動畫效果"));
        checkBoxPreference4.setDefaultValue(Boolean.valueOf(!ai.nh));
        createPreferenceScreen.addPreference(checkBoxPreference4);
        CheckBoxPreference checkBoxPreference5 = new CheckBoxPreference(this);
        checkBoxPreference5.setKey("toggle_wipe_discard_preference");
        checkBoxPreference5.setTitle(af.aa("輕掃出牌"));
        checkBoxPreference5.setDefaultValue(Boolean.valueOf(ai.nq));
        createPreferenceScreen.addPreference(checkBoxPreference5);
        CheckBoxPreference checkBoxPreference6 = new CheckBoxPreference(this);
        checkBoxPreference6.setKey("toggle_sound_effect_preference");
        checkBoxPreference6.setTitle(af.aa("音效"));
        checkBoxPreference6.setDefaultValue(Boolean.valueOf(ai.nl));
        createPreferenceScreen.addPreference(checkBoxPreference6);
        IconListPreference iconListPreference4 = new IconListPreference(this);
        iconListPreference4.setEntries(ai.nk == 2 ? C0000R.array.tile_style : ai.nk == 3 ? C0000R.array.tile_style_gb : ai.nk == 4 ? C0000R.array.tile_style_jp : ai.nk == 5 ? C0000R.array.tile_style_fr : C0000R.array.tile_style_en);
        iconListPreference4.setEntryValues((int) C0000R.array.tile_style_values);
        iconListPreference4.setKey("list_tile_style_preference");
        iconListPreference4.setTitle(af.aa("麻雀牌風格"));
        iconListPreference4.setDefaultValue(String.valueOf(ai.nj));
        iconListPreference4.setSummary(iconListPreference4.getEntries()[ai.nj]);
        iconListPreference4.aU("tile_style/");
        iconListPreference4.c(d.c(this, "tile_style/" + ai.nj + ".png"));
        iconListPreference4.setOnPreferenceChangeListener(new bu(this));
        createPreferenceScreen.addPreference(iconListPreference4);
        if (!MJ16Activity.rd) {
            ListPreference listPreference4 = new ListPreference(this);
            listPreference4.setEntries(ai.nk == 2 ? C0000R.array.screen_orientation : ai.nk == 3 ? C0000R.array.screen_orientation_gb : ai.nk == 4 ? C0000R.array.screen_orientation_jp : ai.nk == 5 ? C0000R.array.screen_orientation_fr : C0000R.array.screen_orientation_en);
            listPreference4.setEntryValues((int) C0000R.array.screen_orientation_values);
            listPreference4.setKey("list_orientation_preference");
            listPreference4.setTitle(af.aa("畫面顯示方向"));
            listPreference4.setDefaultValue(String.valueOf(ai.nf));
            listPreference4.setOnPreferenceChangeListener(new ba(this));
            createPreferenceScreen.addPreference(listPreference4);
        }
        if (!getIntent().getBooleanExtra("is_playing", false)) {
            this.jC = new CheckboxListPreference(this);
            this.jC.setEntries(ai.nk == 2 ? C0000R.array.clear_record : ai.nk == 3 ? C0000R.array.clear_record : ai.nk == 4 ? C0000R.array.clear_record_jp : ai.nk == 5 ? C0000R.array.clear_record_fr : C0000R.array.clear_record_en);
            this.jC.setTitle(af.aa("清除游戲記錄"));
            this.jC.a(new az(this));
            createPreferenceScreen.addPreference(this.jC);
        }
        setPreferenceScreen(createPreferenceScreen);
        if (!MJ16Activity.rd || ai.nt) {
            setRequestedOrientation(ai.nf);
        } else {
            setRequestedOrientation(0);
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        Bundle extras;
        if (i == 4 && (extras = getIntent().getExtras()) != null && extras.getBoolean("IS_RESTART_GAME")) {
            setResult(-1, getIntent());
            finish();
        }
        return super.onKeyDown(i, keyEvent);
    }
}
