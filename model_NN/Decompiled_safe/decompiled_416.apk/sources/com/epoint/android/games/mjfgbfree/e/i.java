package com.epoint.android.games.mjfgbfree.e;

import android.graphics.Color;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import com.epoint.android.games.mjfgbfree.MJ16ViewActivity;
import com.epoint.android.games.mjfgbfree.ai;
import com.epoint.android.games.mjfgbfree.bb;
import com.epoint.android.games.mjfgbfree.d.e;
import com.epoint.android.games.mjfgbfree.tournament.a;

final class i extends Thread {
    private /* synthetic */ l kq;
    private final /* synthetic */ a lr;

    i(l lVar, a aVar) {
        this.kq = lVar;
        this.lr = aVar;
    }

    public final void run() {
        if (this.kq.pi.io <= this.lr.gF) {
            bb.dV();
            this.kq.qj.op++;
            if (this.kq.qj.cA() == null) {
                this.kq.cu.a(this.kq.qj);
                this.kq.cu.eu().cW();
            } else {
                if (ai.nu.equals("random_bg_s")) {
                    String[] stringArray = this.kq.fu.getResources().getStringArray(C0000R.array.wallpaper_values);
                    String str = MJ16Activity.rg;
                    while (MJ16Activity.rg.equals(str)) {
                        MJ16Activity.rg = stringArray[(int) (Math.random() * ((double) (stringArray.length - 1)))];
                    }
                    e eL = ((MJ16ViewActivity) this.kq.fu).eL();
                    if (MJ16Activity.rg.equals("ext_bg1_s")) {
                        eL.sn = Color.rgb(65, 14, 7);
                    } else if (MJ16Activity.rg.equals("ext_bg2_s")) {
                        eL.sn = Color.rgb(163, 86, 12);
                    } else if (MJ16Activity.rg.equals("ext_bg3_s")) {
                        eL.sn = Color.rgb(12, 18, 65);
                    } else {
                        eL.sn = Color.rgb(20, 120, 0);
                    }
                }
                this.kq.cu.bK();
            }
            this.kq.cu.dP().cG();
            this.kq.cu.n(true);
            return;
        }
        this.kq.fu.finish();
    }
}
