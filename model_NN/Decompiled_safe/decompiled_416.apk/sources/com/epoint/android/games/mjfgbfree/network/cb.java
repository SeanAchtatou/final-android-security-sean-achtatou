package com.epoint.android.games.mjfgbfree.network;

import android.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.epoint.android.games.mjfgbfree.af;

final class cb implements View.OnClickListener {
    final /* synthetic */ TCPServerConnectionActivity sh;

    cb(TCPServerConnectionActivity tCPServerConnectionActivity) {
        this.sh = tCPServerConnectionActivity;
    }

    public final void onClick(View view) {
        if (this.sh.ad.size() > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.sh);
            builder.setMessage(String.valueOf(af.aa("停止等候")) + "?");
            builder.setPositiveButton(af.aa("是"), new q(this));
            builder.setNegativeButton(af.aa("否"), new p(this));
            builder.create().show();
            return;
        }
        AlertDialog.Builder builder2 = new AlertDialog.Builder(this.sh);
        builder2.setTitle(af.aa("設立遊戲密碼(可留空)"));
        EditText editText = new EditText(this.sh);
        editText.setSingleLine();
        editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        LinearLayout linearLayout = new LinearLayout(this.sh);
        linearLayout.setPadding(5, 2, 5, 2);
        linearLayout.addView(editText);
        builder2.setView(linearLayout);
        builder2.setPositiveButton(af.aa("好"), new s(this, editText));
        builder2.setNegativeButton(af.aa("取消"), new r(this));
        builder2.create().show();
    }
}
