package com.epoint.android.games.mjfgbfree.network;

import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

final class cg implements DialogInterface.OnClickListener {
    private /* synthetic */ av Bv;
    private final /* synthetic */ View fo;
    private final /* synthetic */ LinearLayout fp;
    private final /* synthetic */ EditText fw;
    private final /* synthetic */ EditText fx;

    cg(av avVar, EditText editText, EditText editText2, View view, LinearLayout linearLayout) {
        this.Bv = avVar;
        this.fw = editText;
        this.fx = editText2;
        this.fo = view;
        this.fp = linearLayout;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String replace = this.fw.getText().toString().trim().replace(",", "").replace(";", "");
        String replace2 = this.fx.getText().toString().trim().replace(",", "").replace(";", "");
        String str = (String) this.Bv.hZ.kl.elementAt(((Integer) this.fo.getTag()).intValue());
        this.Bv.hZ.kl.remove(str);
        int indexOf = this.Bv.hZ.km.indexOf(str);
        this.Bv.hZ.km.remove(str);
        this.Bv.hZ.km.insertElementAt(String.valueOf(replace2) + ";" + replace, indexOf);
        this.Bv.hZ.kl.insertElementAt((String) this.Bv.hZ.km.elementAt(indexOf), ((Integer) this.fo.getTag()).intValue());
        dialogInterface.dismiss();
        this.Bv.hZ.f(this.Bv.hZ.km);
        TCPLocalHostConnectionActivity.a(this.Bv.hZ, this.fp);
    }
}
