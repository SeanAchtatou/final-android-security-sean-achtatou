package com.epoint.android.games.mjfgbfree;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PaintFlagsDrawFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import com.epoint.android.games.mjfgbfree.a.d;
import com.epoint.android.games.mjfgbfree.f.a;
import com.epoint.android.games.mjfgbfree.tournament.TournamentActivity;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.UUID;

public class MJ16Activity extends Activity {
    public static int qJ = 1;
    public static String qK;
    public static boolean qL = true;
    public static boolean qM = false;
    private static int qN;
    private static int qO;
    private static int qP;
    private static int qQ;
    public static int qR;
    public static int qS;
    private static int qT;
    private static int qU;
    private static int qV;
    private static int qW;
    private static int qX;
    private static int qY;
    public static int qZ = qV;
    public static int ra = qW;
    public static int rb = 23;
    public static float rc = 1.0f;
    public static boolean rd;
    public static boolean re;
    public static boolean rf;
    public static String rg;
    private static Display rh;
    public static Bitmap ri;
    public static Canvas rj;
    public static PaintFlagsDrawFilter rk = new PaintFlagsDrawFilter(0, 2);
    /* access modifiers changed from: private */
    public static ab rl;
    public static int rp;
    public static long rq;
    private bj rm;
    private RelativeLayout rn;
    private AlertDialog ro = null;

    public static void a(Activity activity) {
        String locale = Locale.getDefault().toString();
        int i = locale.endsWith("CN") ? 3 : locale.startsWith("zh") ? 2 : locale.startsWith("ja") ? 4 : locale.startsWith("fr") ? 5 : 1;
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        ai.nf = Integer.parseInt(defaultSharedPreferences.getString("list_orientation_preference", String.valueOf(ai.nf)));
        ai.ng = defaultSharedPreferences.getBoolean("toggle_autoget_preference", true);
        ai.nh = !defaultSharedPreferences.getBoolean("toggle_eco_preference", true);
        ai.ni = Integer.parseInt(defaultSharedPreferences.getString("list_vibration_preference", "0"));
        ai.nj = Integer.parseInt(defaultSharedPreferences.getString("list_tile_style_preference", String.valueOf((i == 3 || i == 2 || i == 4 || rd) ? c.ef : c.eg)));
        ai.nl = defaultSharedPreferences.getBoolean("toggle_sound_effect_preference", false);
        ai.nm = defaultSharedPreferences.getString("text_user_name_preference", Build.DEVICE);
        ai.nk = Integer.parseInt(defaultSharedPreferences.getString("list_langauge_preference", String.valueOf(i)));
        ai.nn = defaultSharedPreferences.getString("text_player_pic_preference", "qboy2_h");
        ai.no = defaultSharedPreferences.getString("list_tile_color_preference", String.valueOf(Color.rgb(52, 98, 172)));
        ai.np = !defaultSharedPreferences.getBoolean("toggle_display_chara_preference", false);
        ai.nq = defaultSharedPreferences.getBoolean("toggle_wipe_discard_preference", true);
        ai.nr = Integer.parseInt(defaultSharedPreferences.getString("list_game_rounds_preference", String.valueOf(16)));
        ai.ns = Integer.parseInt(defaultSharedPreferences.getString("list_min_fans_preference", String.valueOf(6)));
        ai.nt = defaultSharedPreferences.getBoolean("toggle_hide_notification_bar_preference", false);
        if (rd) {
            activity.setRequestedOrientation(0);
        } else {
            activity.setRequestedOrientation(ai.nf);
        }
        ai.nu = defaultSharedPreferences.getString("text_wallpaper_preference", "bg_s");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    static /* synthetic */ void a(MJ16Activity mJ16Activity, int i) {
        Intent intent = new Intent(mJ16Activity, MJ16ViewActivity.class);
        intent.putExtra("is_local_game", true);
        intent.putExtra("type", i);
        if (a.a(mJ16Activity, i)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(mJ16Activity);
            builder.setTitle(af.aa("雀友麻雀"));
            builder.setMessage(String.valueOf(af.aa("繼續遊戲")) + "?");
            builder.setPositiveButton(af.aa("是"), new at(mJ16Activity, intent));
            builder.setNeutralButton(af.aa("否"), new bd(mJ16Activity, intent));
            builder.setNegativeButton(af.aa("取消"), new bc(mJ16Activity));
            builder.setCancelable(true);
            builder.create().show();
            return;
        }
        intent.putExtra("is_restore", false);
        mJ16Activity.startActivity(intent);
    }

    public static Display db() {
        return rh;
    }

    public static boolean dc() {
        if (rh.getWidth() > rh.getHeight()) {
            ra = qY;
            qZ = qX;
            qR = qQ;
            qS = qO;
        } else {
            ra = qW;
            qZ = qV;
            qR = qP;
            qS = qN;
        }
        if (ri != null) {
            if (ri.getWidth() == qZ && ri.getHeight() == ra) {
                return false;
            }
            ri.recycle();
        }
        ri = Bitmap.createBitmap(qZ, ra, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(ri);
        rj = canvas;
        canvas.setDrawFilter(rk);
        return true;
    }

    public static void dd() {
        rl.bz();
    }

    public final void a(Integer num) {
        switch (num.intValue()) {
            case -8:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(af.aa("雀友麻雀"));
                builder.setMessage(af.aa("發表評分，送你多個新背景！現在就為雀友麻雀評分嗎？"));
                builder.setPositiveButton(af.aa("是"), new ar(this));
                builder.setNegativeButton(af.aa("否"), new aq(this));
                builder.create().show();
                return;
            case -7:
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setTitle(af.aa("雀友麻雀"));
                builder2.setMessage(af.aa("給我們一個 Facebook\"讚\"？\n(網頁開啟後請按一下頁上的\"讚\"按鈕方為完成)"));
                builder2.setPositiveButton(af.aa("是"), new ap(this));
                builder2.setNegativeButton(af.aa("否"), new au(this));
                builder2.create().show();
                return;
            case -6:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.epoint.android.games.mjfgb")));
                return;
            case -5:
                if (this.ro == null || !this.ro.isShowing()) {
                    CharSequence[] charSequenceArr = {af.aa("本機遊戲"), String.valueOf(af.aa("本機遊戲")) + ": " + af.aa("積分模式"), String.valueOf(af.aa("本機遊戲")) + ": " + af.Z("挑戰賽"), af.aa("連線: 藍牙"), af.aa("連線: 網路"), af.aa("連線: 伺服器")};
                    AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
                    builder3.setTitle(af.aa("遊戲模式"));
                    builder3.setItems(charSequenceArr, new as(this));
                    this.ro = builder3.create();
                    this.ro.show();
                    return;
                }
                return;
            case -4:
                startActivity(new Intent(this, LocalFanHistoryActivity.class));
                return;
            case -3:
                startActivityForResult(new Intent(this, PreferencesActivity.class), 5);
                return;
            case -2:
                startActivity(new Intent(this, InstructionActivity.class));
                return;
            case -1:
                AlertDialog.Builder builder4 = new AlertDialog.Builder(this);
                builder4.setTitle("Action");
                builder4.setItems(new CharSequence[]{"Play", "Play(Endless)", "Story 1", "Story 2", "Story 3", "MAP", "Network(Server)", "Network(Local)", "Bluetooth", "Bump", "Capture", "Admin", "Challenge", "Tournament"}, new al(this));
                builder4.create().show();
                return;
            default:
                return;
        }
    }

    public final void de() {
        int height = this.rm.getHeight();
        int width = this.rm.getWidth();
        if (width > height) {
            int i = qN - height;
            if (width > qX) {
                qX = width;
            }
            if (height > qY) {
                qY = height;
            }
            if (!(width == qX && height == qY)) {
                qT = 640;
                qU = ai.nt ? 480 : 442;
                float f = ((float) width) / ((float) height);
                int ceil = (int) Math.ceil((double) (((float) qU) * f));
                int ceil2 = (int) Math.ceil((double) (f * ((float) qT)));
                if (ceil >= qT) {
                    qX = ceil;
                    qY = qU;
                } else {
                    qX = ceil2;
                    qY = qT;
                }
            }
            rb = (width * 23) / qX;
            rc = ((float) width) / ((float) qX);
            qP = width - i;
            qQ = height;
            int i2 = qN;
            int i3 = qO - i;
            if (i2 > qV) {
                qV = i2;
            }
            if (i3 > qW) {
                qW = i3;
            }
            if (!(i2 == qV && i3 == qW)) {
                qT = 602;
                qU = 480;
                float f2 = ((float) i3) / ((float) i2);
                int ceil3 = (int) Math.ceil((double) (((float) qU) * f2));
                int ceil4 = (int) Math.ceil((double) (f2 * ((float) qT)));
                if (ceil3 >= qT) {
                    qW = ceil3;
                    qV = qU;
                } else {
                    qW = ceil4;
                    qV = qT;
                }
            }
        } else {
            int i4 = qO - height;
            if (width > qV) {
                qV = width;
            }
            if (height > qW) {
                qW = height;
            }
            if (!(width == qV && height == qW)) {
                qT = 602;
                qU = 480;
                float f3 = ((float) height) / ((float) width);
                int ceil5 = (int) Math.ceil((double) (((float) qU) * f3));
                int ceil6 = (int) Math.ceil((double) (f3 * ((float) qT)));
                if (ceil5 >= qT) {
                    qW = ceil5;
                    qV = qU;
                } else {
                    qW = ceil6;
                    qV = qT;
                }
            }
            rb = (width * 23) / qV;
            rc = ((float) width) / ((float) qV);
            qP = height;
            qQ = width - i4;
            int i5 = qO;
            int i6 = qN - i4;
            if (i5 > qX) {
                qX = i5;
            }
            if (i6 > qY) {
                qY = i6;
            }
            if (!(i5 == qX && i6 == qY)) {
                qT = 640;
                qU = ai.nt ? 480 : 442;
                float f4 = ((float) i5) / ((float) i6);
                int ceil7 = (int) Math.ceil((double) (((float) qU) * f4));
                int ceil8 = (int) Math.ceil((double) (f4 * ((float) qT)));
                if (ceil7 >= qT) {
                    qX = ceil7;
                    qY = qU;
                } else {
                    qX = ceil8;
                    qY = qT;
                }
            }
        }
        this.rn.removeView(this.rm);
        this.rm = null;
        dc();
        rl = new ab(this);
        this.rn.addView(rl);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        switch (i) {
            case 5:
                a(this);
                if (i2 == -1) {
                    if (!intent.getBooleanExtra("IS_RESTART", false)) {
                        if (intent.getBooleanExtra("IS_RESTART_GAME", false)) {
                            Intent intent2 = new Intent(getBaseContext(), MJ16Activity.class);
                            intent2.addFlags(67108864);
                            startActivity(intent2);
                            break;
                        }
                    } else {
                        rl.bz();
                        rl.invalidate();
                        intent.removeExtra("IS_RESTART");
                        startActivityForResult(intent, i);
                        break;
                    }
                }
                break;
        }
        super.onActivityResult(i, i2, intent);
    }

    public void onConfigurationChanged(Configuration configuration) {
        dc();
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setVolumeControlStream(3);
        boolean a2 = a.a(this, "local_data_cap");
        qM = a2;
        if (a2) {
            com.epoint.android.games.mjfgbfree.g.a.oc[1] = new int[]{1, 2, 3, 5, 8};
            com.epoint.android.games.mjfgbfree.g.a.oc[2] = new int[]{1, 3, 5, 8, 12};
            com.epoint.android.games.mjfgbfree.g.a.oc[3] = new int[]{2, 4, 6, 9, 15};
            com.epoint.android.games.mjfgbfree.g.a.oc[4] = new int[]{2, 6, 9, 12, 20};
            com.epoint.android.games.mjfgbfree.g.a.oc[5] = new int[]{2, 6, 10, 15, 22};
            com.epoint.android.games.mjfgbfree.g.a.oc[6] = new int[]{2, 7, 12, 20, 30};
            com.epoint.android.games.mjfgbfree.g.a.oc[7] = new int[]{2, 10, 20, 32, 50};
            com.epoint.android.games.mjfgbfree.g.a.oc[8] = new int[]{3, 15, 25, 40, 60};
            com.epoint.android.games.mjfgbfree.g.a.oc[9] = new int[]{5, 15, 30, 50, 80};
            com.epoint.android.games.mjfgbfree.g.a.oc[10] = new int[]{8, 20, 40, 70, 100};
            com.epoint.android.games.mjfgbfree.g.a.od[1] = new int[]{1, 2, 3, 5, 8};
            com.epoint.android.games.mjfgbfree.g.a.od[2] = new int[]{1, 3, 5, 8, 12};
            com.epoint.android.games.mjfgbfree.g.a.od[3] = new int[]{1, 4, 6, 9, 15};
            com.epoint.android.games.mjfgbfree.g.a.od[4] = new int[]{1, 6, 9, 12, 20};
            com.epoint.android.games.mjfgbfree.g.a.od[5] = new int[]{2, 6, 10, 15, 22};
            com.epoint.android.games.mjfgbfree.g.a.od[6] = new int[]{2, 7, 12, 20, 30};
            com.epoint.android.games.mjfgbfree.g.a.od[7] = new int[]{3, 10, 20, 32, 50};
            com.epoint.android.games.mjfgbfree.g.a.od[8] = new int[]{3, 15, 30, 50, 80};
            com.epoint.android.games.mjfgbfree.g.a.od[9] = new int[]{5, 15, 25, 40, 60};
            com.epoint.android.games.mjfgbfree.g.a.od[10] = new int[]{8, 20, 40, 70, 100};
        }
        rh = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        String deviceId = ((TelephonyManager) getSystemService("phone")).getDeviceId();
        qK = deviceId;
        if (deviceId == null) {
            qK = Settings.Secure.getString(getContentResolver(), "android_id");
        }
        try {
            qK = d.B(qK);
        } catch (UnsupportedEncodingException e) {
            qK = UUID.nameUUIDFromBytes(qK.getBytes()).toString();
        }
        int width = rh.getWidth();
        int height = rh.getHeight();
        if (width > height) {
            qO = width;
            qN = height;
        } else {
            qO = height;
            qN = width;
        }
        boolean z = qO <= 320;
        rd = z;
        if (!z) {
            re = qO <= 480;
        }
        a(this);
        rf = "Y".equals(a.b(this, "is_wallpaper_unlocked", "N"));
        if (ai.nt) {
            requestWindowFeature(1);
            getWindow().setFlags(1024, 1024);
        }
        qV = 480;
        qW = ai.nt ? 640 : 602;
        qX = 640;
        qY = ai.nt ? 480 : 442;
        qZ = qV;
        ra = qW;
        this.rn = (RelativeLayout) LayoutInflater.from(this).inflate((int) C0000R.layout.main, (ViewGroup) null);
        this.rm = new bj(this, this);
        this.rn.addView(this.rm);
        setContentView(this.rn);
        rp = a.f(this).intValue();
        rq = Long.valueOf(a.b(this, "last_ticket_update", "0")).longValue();
        TournamentActivity.s(this);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 7, 0, (CharSequence) null).setIcon(17301569);
        menu.add(0, 1, 0, (CharSequence) null).setIcon(17301580);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(af.aa("雀友麻雀"));
            builder.setMessage(String.valueOf(af.aa("離開遊戲")) + "?");
            builder.setPositiveButton(af.aa("是"), new an(this));
            builder.setNegativeButton(af.aa("否"), new am(this));
            builder.create().show();
            return true;
        } else if (i == 84) {
            return true;
        } else {
            return super.onKeyDown(i, keyEvent);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 0:
                startActivity(new Intent(this, InstructionActivity.class));
                return true;
            case 1:
                System.exit(0);
                return true;
            case 5:
                startActivityForResult(new Intent(this, PreferencesActivity.class), 5);
                return true;
            case 7:
                startActivity(new Intent(this, AboutActivity.class));
                break;
        }
        return false;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            switch (item.getItemId()) {
                case 1:
                    item.setTitle(af.aa("離開"));
                    break;
                case 7:
                    item.setTitle(af.aa("關於"));
                    break;
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }
}
