package com.epoint.android.games.mjfgbfree.ui.a;

import android.graphics.Rect;
import com.epoint.android.games.mjfgbfree.MJ16Activity;

final class j extends Thread {
    private /* synthetic */ e zB;
    private final /* synthetic */ boolean zC;

    j(e eVar, boolean z) {
        this.zB = eVar;
        this.zC = z;
    }

    public final void run() {
        while (this.zB.jr.dP().oR) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (Throwable th) {
                e.jz = false;
                throw th;
            }
        }
        while (this.zB.jr.uu) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
        if (this.zC) {
            Rect rect = this.zB.jr.ut;
            Rect rect2 = new Rect();
            int max = Math.max(MJ16Activity.ra, MJ16Activity.qZ);
            rect2.top = (MJ16Activity.ra - max) / 2;
            rect2.left = (MJ16Activity.qZ - max) / 2;
            rect2.bottom = rect2.top + max;
            rect2.right = max + rect2.left;
            this.zB.jr.ut = rect2;
            this.zB.jr.dP().a(this.zB.js, true);
            this.zB.jr.ut = rect;
        } else {
            this.zB.jr.dP().a(this.zB.js, true);
        }
        while (this.zB.jr.uu) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e3) {
                e3.printStackTrace();
            }
        }
        this.zB.jt = 4;
        this.zB.jr.et();
        while (this.zB.jr.uu) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e4) {
                e4.printStackTrace();
            }
        }
        e.jz = false;
    }
}
