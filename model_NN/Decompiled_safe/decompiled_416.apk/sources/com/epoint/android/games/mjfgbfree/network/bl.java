package com.epoint.android.games.mjfgbfree.network;

import b.a.b;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import com.epoint.android.games.mjfgbfree.af;
import com.epoint.android.games.mjfgbfree.ai;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

final class bl extends Thread {
    private /* synthetic */ TCPServerConnectionActivity sh;
    private final /* synthetic */ String sk;

    bl(TCPServerConnectionActivity tCPServerConnectionActivity, String str) {
        this.sh = tCPServerConnectionActivity;
        this.sk = str;
    }

    public final void run() {
        Socket socket = new Socket();
        try {
            String charSequence = this.sh.kg.getText().toString();
            socket.connect(new InetSocketAddress(charSequence.equals("<default>") ? "s1.mjguy.com" : charSequence, 7890), 5000);
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();
            b bVar = new b();
            bVar.a("identity", this.sh.kd == null ? "guest" : this.sh.kd);
            bVar.a("name", ai.nm);
            bVar.a("uuid", MJ16Activity.qK);
            outputStream.write((String.valueOf(bVar.toString()) + "\r\n").getBytes("UTF-8"));
            b bVar2 = new b(this.sh.a(inputStream));
            this.sh.jZ = bVar2.getString("identity");
            this.sh.ka = bVar2.getString("uuid");
            b bVar3 = new b();
            bVar3.a("host", "MJ-GB1.3A");
            bVar3.a("secret", this.sk);
            bVar3.c("limit", this.sh.ac + 1);
            outputStream.write((String.valueOf(bVar3.toString()) + "\r\n").getBytes("UTF-8"));
            if (!TCPServerConnectionActivity.b(this.sh.kl, charSequence) && !TCPServerConnectionActivity.b(this.sh.km, charSequence)) {
                this.sh.km.add(charSequence);
                this.sh.kl.add(charSequence);
                this.sh.f(this.sh.km);
            }
            this.sh.a(3, String.valueOf(this.sk.equals("") ? "" : String.valueOf(this.sk) + "@") + this.sh.jZ);
            this.sh.a(2, socket);
        } catch (Exception e) {
            if (!socket.isClosed()) {
                try {
                    socket.close();
                } catch (IOException e2) {
                }
                this.sh.a(5, af.aa("連線失敗"));
            }
            this.sh.a(4, null);
            e.printStackTrace();
        }
    }
}
