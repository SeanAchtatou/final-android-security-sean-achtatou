package com.epoint.android.games.mjfgbfree;

import android.preference.Preference;

final class bt implements Preference.OnPreferenceChangeListener {
    private /* synthetic */ PreferencesActivity qn;

    bt(PreferencesActivity preferencesActivity) {
        this.qn = preferencesActivity;
    }

    public final boolean onPreferenceChange(Preference preference, Object obj) {
        this.qn.getIntent().putExtra("IS_RESTART_GAME", ((Boolean) obj).booleanValue() ^ ai.nt);
        return true;
    }
}
