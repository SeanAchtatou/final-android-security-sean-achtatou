package com.epoint.android.games.mjfgbfree;

import android.view.View;

final class e implements View.OnClickListener {
    private /* synthetic */ InstructionActivity fU;

    e(InstructionActivity instructionActivity) {
        this.fU = instructionActivity;
    }

    public final void onClick(View view) {
        this.fU.sH.setClickable(false);
        this.fU.sH.setChecked(true);
        this.fU.sI.setClickable(true);
        this.fU.sI.setChecked(false);
        this.fU.sG.setClickable(true);
        this.fU.sG.setChecked(false);
        this.fU.a(this.fU.sK);
    }
}
