package com.epoint.android.games.mjfgbfree.a;

import java.io.UnsupportedEncodingException;

public class e {
    private static /* synthetic */ boolean cL = (!e.class.desiredAssertionStatus());

    private e() {
    }

    public static byte[] D(String str) {
        byte[] bytes = str.getBytes("UTF-8");
        int length = bytes.length;
        f fVar = new f(new byte[((length * 3) / 4)]);
        if (!fVar.a(bytes, length)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (fVar.o == fVar.n.length) {
            return fVar.n;
        } else {
            byte[] bArr = new byte[fVar.o];
            System.arraycopy(fVar.n, 0, bArr, 0, fVar.o);
            return bArr;
        }
    }

    public static String b(byte[] bArr) {
        try {
            int length = bArr.length;
            b bVar = new b();
            int i = (length / 3) * 4;
            if (!bVar.cH) {
                switch (length % 3) {
                    case 1:
                        i += 2;
                        break;
                    case 2:
                        i += 3;
                        break;
                }
            } else if (length % 3 > 0) {
                i += 4;
            }
            if (bVar.cI && length > 0) {
                i += (((length - 1) / 57) + 1) * (bVar.cJ ? 2 : 1);
            }
            bVar.n = new byte[i];
            bVar.a(bArr, length);
            if (cL || bVar.o == i) {
                return new String(bVar.n, "UTF-8");
            }
            throw new AssertionError();
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }
}
