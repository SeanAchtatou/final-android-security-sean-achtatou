package com.epoint.android.games.mjfgbfree.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import b.a.b;
import b.a.f;
import com.epoint.android.games.mjfgbfree.f.a;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Vector;

public final class d {
    public static String iq = null;

    /* JADX INFO: finally extract failed */
    public static b A(String str) {
        char charAt = str.charAt(str.length() - 1);
        String[] split = ((charAt == '?' || charAt == '&') ? str.substring(0, str.length() - 1) : str).split("\\?");
        if (split.length != 2) {
            return null;
        }
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(split[0]).openConnection();
        httpURLConnection.setConnectTimeout(12000);
        httpURLConnection.setReadTimeout(12000);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setAllowUserInteraction(false);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Connection", "keep-alive");
        httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        httpURLConnection.setRequestProperty("Content-Length", String.valueOf(split[1].getBytes().length));
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
        try {
            bufferedOutputStream.write(split[1].getBytes());
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            String str2 = "";
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        bufferedReader.close();
                        return new b(str2);
                    }
                    str2 = String.valueOf(str2) + readLine;
                } catch (Throwable th) {
                    bufferedReader.close();
                    throw th;
                }
            }
        } catch (Throwable th2) {
            bufferedOutputStream.close();
            throw th2;
        }
    }

    public static String B(String str) {
        try {
            return new BigInteger(1, MessageDigest.getInstance("MD5").digest(str.getBytes("UTF-8"))).toString(16);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static Bitmap a(Bitmap bitmap, int i) {
        return Bitmap.createScaledBitmap(bitmap, i, (int) ((((float) i) / ((float) bitmap.getWidth())) * ((float) bitmap.getHeight())), true);
    }

    public static Bitmap a(String str, int i, int i2) {
        int height;
        char charAt = str.charAt(str.length() - 1);
        URLConnection openConnection = new URL((charAt == '?' || charAt == '&') ? str.substring(0, str.length() - 1) : str).openConnection();
        openConnection.setConnectTimeout(12000);
        openConnection.setReadTimeout(12000);
        Bitmap bitmap = null;
        for (int i3 = 0; i3 < 2 && (bitmap = BitmapFactory.decodeStream(new BufferedInputStream(openConnection.getInputStream()))) == null; i3++) {
        }
        Bitmap bitmap2 = bitmap;
        if (bitmap2 == null || i == -1 || i2 == -1) {
            return bitmap2;
        }
        int max = Math.max(i, i2);
        if (bitmap2.getWidth() > bitmap2.getHeight()) {
            height = max;
            max = (int) ((((float) max) / ((float) bitmap2.getWidth())) * ((float) bitmap2.getHeight()));
        } else {
            height = (int) ((((float) max) / ((float) bitmap2.getHeight())) * ((float) bitmap2.getWidth()));
        }
        return height < bitmap2.getWidth() ? Bitmap.createScaledBitmap(bitmap2, height, max, true) : bitmap2;
    }

    public static Object a(byte[] bArr) {
        ObjectInputStream objectInputStream;
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        try {
            ObjectInputStream objectInputStream2 = new ObjectInputStream(byteArrayInputStream);
            try {
                Object readObject = objectInputStream2.readObject();
                try {
                    objectInputStream2.close();
                } catch (Exception e) {
                }
                try {
                    byteArrayInputStream.close();
                } catch (Exception e2) {
                }
                return readObject;
            } catch (Exception e3) {
                Exception exc = e3;
                objectInputStream = objectInputStream2;
                e = exc;
            } catch (Throwable th) {
                Throwable th2 = th;
                objectInputStream = objectInputStream2;
                th = th2;
                try {
                    objectInputStream.close();
                } catch (Exception e4) {
                }
                try {
                    byteArrayInputStream.close();
                } catch (Exception e5) {
                }
                throw th;
            }
        } catch (Exception e6) {
            e = e6;
            objectInputStream = null;
            try {
                e.printStackTrace();
                try {
                    objectInputStream.close();
                } catch (Exception e7) {
                }
                try {
                    byteArrayInputStream.close();
                } catch (Exception e8) {
                }
                return null;
            } catch (Throwable th3) {
                th = th3;
                objectInputStream.close();
                byteArrayInputStream.close();
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            objectInputStream = null;
            objectInputStream.close();
            byteArrayInputStream.close();
            throw th;
        }
    }

    public static Bitmap c(Context context, String str) {
        try {
            return BitmapFactory.decodeStream(context.getAssets().open(str));
        } catch (IOException e) {
            return null;
        }
    }

    public static byte[] c(Object obj) {
        ObjectOutputStream objectOutputStream;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ObjectOutputStream objectOutputStream2 = new ObjectOutputStream(byteArrayOutputStream);
            try {
                objectOutputStream2.writeObject(obj);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                try {
                    objectOutputStream2.close();
                } catch (Exception e) {
                }
                try {
                    byteArrayOutputStream.close();
                    return byteArray;
                } catch (Exception e2) {
                    return byteArray;
                }
            } catch (IOException e3) {
                IOException iOException = e3;
                objectOutputStream = objectOutputStream2;
                e = iOException;
            } catch (Throwable th) {
                Throwable th2 = th;
                objectOutputStream = objectOutputStream2;
                th = th2;
                try {
                    objectOutputStream.close();
                } catch (Exception e4) {
                }
                try {
                    byteArrayOutputStream.close();
                } catch (Exception e5) {
                }
                throw th;
            }
        } catch (IOException e6) {
            e = e6;
            objectOutputStream = null;
            try {
                e.printStackTrace();
                try {
                    objectOutputStream.close();
                } catch (Exception e7) {
                }
                try {
                    byteArrayOutputStream.close();
                    return null;
                } catch (Exception e8) {
                    return null;
                }
            } catch (Throwable th3) {
                th = th3;
                objectOutputStream.close();
                byteArrayOutputStream.close();
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            objectOutputStream = null;
            objectOutputStream.close();
            byteArrayOutputStream.close();
            throw th;
        }
    }

    public static Bitmap d(Context context, String str) {
        int i;
        boolean z;
        try {
            Bitmap decodeStream = BitmapFactory.decodeStream(context.getAssets().open("images/body1.png"));
            int i2 = 175;
            if (str.endsWith("/qboy1_h.png")) {
                i2 = 170;
                i = 0;
                z = false;
            } else if (str.endsWith("/qboy2_h.png")) {
                i2 = 173;
                z = false;
                i = 2;
            } else if (str.endsWith("/qboy3_h.png")) {
                i2 = 172;
                i = 0;
                z = false;
            } else if (str.endsWith("/qboy4_h.png")) {
                i2 = 168;
                i = 0;
                z = false;
            } else if (str.endsWith("/qgirl1_h.png")) {
                i2 = 168;
                z = false;
                i = 2;
            } else if (str.endsWith("/qman1_h.png")) {
                i = 4;
                z = false;
            } else if (str.endsWith("/qman2_h.png")) {
                i2 = 166;
                i = 0;
                z = false;
            } else if (str.endsWith("/qman3_h.png")) {
                i2 = 167;
                z = false;
                i = -2;
            } else if (str.endsWith("/qman4_h.png")) {
                i2 = 162;
                z = false;
                i = -2;
            } else if (str.endsWith("/qwoman1_h.png")) {
                i2 = 169;
                i = 0;
                z = false;
            } else if (str.endsWith("/qwoman2_h.png")) {
                i2 = 186;
                z = false;
                i = 2;
            } else if (str.endsWith("/qwoman3_h.png")) {
                i2 = 188;
                z = false;
                i = -2;
            } else if (str.endsWith("/qwoman4_h.png")) {
                i2 = 178;
                z = false;
                i = -2;
            } else if (str.endsWith("/qanimal1_h.png")) {
                i2 = 164;
                i = 0;
                z = true;
            } else if (str.endsWith("/qanimal2_h.png")) {
                i2 = 164;
                i = 0;
                z = true;
            } else if (str.endsWith("/qanimal3_h.png")) {
                z = true;
                i2 = 164;
                i = 2;
            } else if (str.endsWith("/qmisc1_h.png")) {
                i2 = 162;
                z = false;
                i = 2;
            } else {
                i = 0;
                z = false;
            }
            Bitmap createBitmap = Bitmap.createBitmap(decodeStream.getWidth(), i2, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            if (z) {
                Paint paint = new Paint();
                paint.setColor(-16777216);
                paint.setStyle(Paint.Style.FILL);
                canvas.drawRect(new Rect(79, 78, 123, 100), paint);
            }
            canvas.drawBitmap(decodeStream, 0.0f, (float) (createBitmap.getHeight() - decodeStream.getHeight()), (Paint) null);
            decodeStream.recycle();
            Bitmap decodeStream2 = BitmapFactory.decodeStream(context.getAssets().open(str));
            canvas.drawBitmap(decodeStream2, (float) ((((createBitmap.getWidth() - 38) - decodeStream2.getWidth()) / 2) + 38 + i), 0.0f, (Paint) null);
            decodeStream2.recycle();
            return createBitmap;
        } catch (IOException e) {
            return null;
        }
    }

    public static Vector h(Context context) {
        Object b2 = a.b(context, 3);
        Vector vector = new Vector();
        if (b2 instanceof a.b.d.a) {
            String[] strArr = (String[]) ((a.b.d.a) b2).lq.get("uuid");
            for (int i = 0; i < strArr.length; i++) {
                if (strArr[i] != null) {
                    vector.add(strArr[i]);
                }
            }
        }
        return vector;
    }

    public static b y(String str) {
        int i = 1;
        while (i <= 2) {
            try {
                return new b(z(str));
            } catch (f e) {
                if (i == 2) {
                    throw e;
                }
                i++;
            }
        }
        return null;
    }

    public static String z(String str) {
        char charAt = str.charAt(str.length() - 1);
        URLConnection openConnection = new URL((charAt == '?' || charAt == '&') ? str.substring(0, str.length() - 1) : str).openConnection();
        openConnection.setConnectTimeout(12000);
        openConnection.setReadTimeout(12000);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(openConnection.getInputStream()));
        String str2 = "";
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                bufferedReader.close();
                return str2;
            }
            str2 = String.valueOf(str2) + readLine;
        }
    }
}
