package com.epoint.android.games.mjfgbfree;

import android.content.Intent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class a extends WebViewClient {
    private /* synthetic */ ManualActivity aw;

    a(ManualActivity manualActivity) {
        this.aw = manualActivity;
    }

    public final void onPageFinished(WebView webView, String str) {
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (!str.startsWith("mailto:")) {
            return super.shouldOverrideUrlLoading(webView, str);
        }
        String trim = str.replaceFirst("mailto:", "").trim();
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("plain/text").putExtra("android.intent.extra.EMAIL", new String[]{trim});
        this.aw.startActivity(intent);
        return true;
    }
}
