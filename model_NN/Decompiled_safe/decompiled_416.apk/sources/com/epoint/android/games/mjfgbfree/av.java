package com.epoint.android.games.mjfgbfree;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.view.View;
import com.epoint.android.games.mjfgbfree.d.g;
import com.epoint.android.games.mjfgbfree.ui.a;

public final class av extends View {
    private bb cu;
    private Rect ju;
    private int oH;
    private int oI;
    private Bitmap oJ;
    private float oK;
    private float oL;
    private a oM;
    private int oN;
    private int oO;
    private a oP;
    private Rect oQ;
    public volatile boolean oR = false;
    private boolean oS = false;
    private int pos = -1;
    private int type = -1;

    public av(Context context, bb bbVar) {
        super(context);
        this.cu = bbVar;
        this.oR = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0051  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void b(int r8, com.epoint.android.games.mjfgbfree.d.g r9, java.lang.String[] r10) {
        /*
            r7 = this;
            r3 = 3
            r2 = 2
            r1 = 1
            monitor-enter(r7)
            monitor-enter(r7)     // Catch:{ all -> 0x0075 }
            android.graphics.Matrix r0 = com.epoint.android.games.mjfgbfree.bb.mMatrix     // Catch:{ all -> 0x0072 }
            r0.reset()     // Catch:{ all -> 0x0072 }
            int r0 = r9.aG     // Catch:{ all -> 0x0072 }
            int r0 = r0 + 1
            int r0 = r0 % 4
            if (r0 != r8) goto L_0x0078
            int r0 = r7.pos     // Catch:{ all -> 0x0072 }
            if (r0 != r1) goto L_0x0019
            monitor-exit(r7)     // Catch:{ all -> 0x0072 }
        L_0x0017:
            monitor-exit(r7)
            return
        L_0x0019:
            r0 = 1
            r7.pos = r0     // Catch:{ all -> 0x0072 }
            java.util.Hashtable r0 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0072 }
            r1 = 0
            r1 = r10[r1]     // Catch:{ all -> 0x0072 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0072 }
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0     // Catch:{ all -> 0x0072 }
            r7.oJ = r0     // Catch:{ all -> 0x0072 }
            android.graphics.Matrix r0 = com.epoint.android.games.mjfgbfree.bb.mMatrix     // Catch:{ all -> 0x0072 }
            r1 = 1132920832(0x43870000, float:270.0)
            r0.setRotate(r1)     // Catch:{ all -> 0x0072 }
        L_0x0030:
            android.graphics.Bitmap r0 = r7.oJ     // Catch:{ all -> 0x0072 }
            r1 = 0
            r2 = 0
            android.graphics.Bitmap r3 = r7.oJ     // Catch:{ all -> 0x0072 }
            int r3 = r3.getWidth()     // Catch:{ all -> 0x0072 }
            android.graphics.Bitmap r4 = r7.oJ     // Catch:{ all -> 0x0072 }
            int r4 = r4.getHeight()     // Catch:{ all -> 0x0072 }
            android.graphics.Matrix r5 = com.epoint.android.games.mjfgbfree.bb.mMatrix     // Catch:{ all -> 0x0072 }
            r6 = 1
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createBitmap(r0, r1, r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0072 }
            r7.oJ = r0     // Catch:{ all -> 0x0072 }
            float r0 = com.epoint.android.games.mjfgbfree.MJ16Activity.rc     // Catch:{ all -> 0x0072 }
            r1 = 1065353216(0x3f800000, float:1.0)
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0070
            android.graphics.Bitmap r0 = r7.oJ     // Catch:{ all -> 0x0072 }
            android.graphics.Bitmap r1 = r7.oJ     // Catch:{ all -> 0x0072 }
            int r1 = r1.getWidth()     // Catch:{ all -> 0x0072 }
            float r1 = (float) r1     // Catch:{ all -> 0x0072 }
            float r2 = r7.oK     // Catch:{ all -> 0x0072 }
            float r1 = r1 * r2
            int r1 = (int) r1     // Catch:{ all -> 0x0072 }
            android.graphics.Bitmap r2 = r7.oJ     // Catch:{ all -> 0x0072 }
            int r2 = r2.getHeight()     // Catch:{ all -> 0x0072 }
            float r2 = (float) r2     // Catch:{ all -> 0x0072 }
            float r3 = r7.oL     // Catch:{ all -> 0x0072 }
            float r2 = r2 * r3
            int r2 = (int) r2     // Catch:{ all -> 0x0072 }
            r3 = 1
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createScaledBitmap(r0, r1, r2, r3)     // Catch:{ all -> 0x0072 }
            r7.oJ = r0     // Catch:{ all -> 0x0072 }
        L_0x0070:
            monitor-exit(r7)     // Catch:{ all -> 0x0072 }
            goto L_0x0017
        L_0x0072:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0072 }
            throw r0     // Catch:{ all -> 0x0075 }
        L_0x0075:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        L_0x0078:
            int r0 = r9.aG     // Catch:{ all -> 0x0072 }
            int r0 = r0 + 2
            int r0 = r0 % 4
            if (r0 != r8) goto L_0x009e
            int r0 = r7.pos     // Catch:{ all -> 0x0072 }
            if (r0 != r2) goto L_0x0086
            monitor-exit(r7)     // Catch:{ all -> 0x0072 }
            goto L_0x0017
        L_0x0086:
            r0 = 2
            r7.pos = r0     // Catch:{ all -> 0x0072 }
            java.util.Hashtable r0 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0072 }
            r1 = 1
            r1 = r10[r1]     // Catch:{ all -> 0x0072 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0072 }
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0     // Catch:{ all -> 0x0072 }
            r7.oJ = r0     // Catch:{ all -> 0x0072 }
            android.graphics.Matrix r0 = com.epoint.android.games.mjfgbfree.bb.mMatrix     // Catch:{ all -> 0x0072 }
            r1 = 1127481344(0x43340000, float:180.0)
            r0.setRotate(r1)     // Catch:{ all -> 0x0072 }
            goto L_0x0030
        L_0x009e:
            int r0 = r9.aG     // Catch:{ all -> 0x0072 }
            int r0 = r0 + 3
            int r0 = r0 % 4
            if (r0 != r8) goto L_0x0030
            int r0 = r7.pos     // Catch:{ all -> 0x0072 }
            if (r0 != r3) goto L_0x00ad
            monitor-exit(r7)     // Catch:{ all -> 0x0072 }
            goto L_0x0017
        L_0x00ad:
            r0 = 3
            r7.pos = r0     // Catch:{ all -> 0x0072 }
            java.util.Hashtable r0 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0072 }
            r1 = 2
            r1 = r10[r1]     // Catch:{ all -> 0x0072 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0072 }
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0     // Catch:{ all -> 0x0072 }
            r7.oJ = r0     // Catch:{ all -> 0x0072 }
            android.graphics.Matrix r0 = com.epoint.android.games.mjfgbfree.bb.mMatrix     // Catch:{ all -> 0x0072 }
            r1 = 1119092736(0x42b40000, float:90.0)
            r0.setRotate(r1)     // Catch:{ all -> 0x0072 }
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.av.b(int, com.epoint.android.games.mjfgbfree.d.g, java.lang.String[]):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(int r10, android.graphics.Canvas r11, com.epoint.android.games.mjfgbfree.d.g r12, java.lang.String[] r13) {
        /*
            r9 = this;
            r2 = 1
            r7 = 0
            monitor-enter(r9)
            monitor-enter(r9)     // Catch:{ all -> 0x007b }
            android.graphics.Matrix r0 = com.epoint.android.games.mjfgbfree.bb.mMatrix     // Catch:{ all -> 0x0078 }
            r0.reset()     // Catch:{ all -> 0x0078 }
            int r0 = r12.aG     // Catch:{ all -> 0x0078 }
            int r0 = r0 + 1
            int r0 = r0 % 4
            if (r0 != r10) goto L_0x0065
            r0 = 1
            r9.pos = r0     // Catch:{ all -> 0x0078 }
            android.graphics.Matrix r0 = com.epoint.android.games.mjfgbfree.bb.mMatrix     // Catch:{ all -> 0x0078 }
            r1 = 1132920832(0x43870000, float:270.0)
            r0.setRotate(r1)     // Catch:{ all -> 0x0078 }
        L_0x001b:
            r0 = 4
            r9.oI = r0     // Catch:{ all -> 0x0078 }
            java.util.Hashtable r0 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0078 }
            int r1 = r9.pos     // Catch:{ all -> 0x0078 }
            int r1 = r1 - r2
            r1 = r13[r1]     // Catch:{ all -> 0x0078 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0078 }
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0     // Catch:{ all -> 0x0078 }
            r1 = 0
            r2 = 0
            int r3 = r0.getWidth()     // Catch:{ all -> 0x0078 }
            int r4 = r0.getHeight()     // Catch:{ all -> 0x0078 }
            android.graphics.Matrix r5 = com.epoint.android.games.mjfgbfree.bb.mMatrix     // Catch:{ all -> 0x0078 }
            r6 = 0
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createBitmap(r0, r1, r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0078 }
            float r1 = com.epoint.android.games.mjfgbfree.MJ16Activity.rc     // Catch:{ all -> 0x0078 }
            r2 = 1065353216(0x3f800000, float:1.0)
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 == 0) goto L_0x0093
            int r1 = r0.getWidth()     // Catch:{ all -> 0x0078 }
            float r1 = (float) r1     // Catch:{ all -> 0x0078 }
            float r2 = r9.oK     // Catch:{ all -> 0x0078 }
            float r1 = r1 * r2
            int r1 = (int) r1     // Catch:{ all -> 0x0078 }
            int r2 = r0.getHeight()     // Catch:{ all -> 0x0078 }
            float r2 = (float) r2     // Catch:{ all -> 0x0078 }
            float r3 = r9.oL     // Catch:{ all -> 0x0078 }
            float r2 = r2 * r3
            int r2 = (int) r2     // Catch:{ all -> 0x0078 }
            r3 = 1
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createScaledBitmap(r0, r1, r2, r3)     // Catch:{ all -> 0x0078 }
            r9.oJ = r1     // Catch:{ all -> 0x0078 }
        L_0x005d:
            int r1 = r9.pos     // Catch:{ all -> 0x0078 }
            switch(r1) {
                case 1: goto L_0x0096;
                case 2: goto L_0x00e6;
                case 3: goto L_0x00ff;
                default: goto L_0x0062;
            }     // Catch:{ all -> 0x0078 }
        L_0x0062:
            monitor-exit(r9)     // Catch:{ all -> 0x0078 }
        L_0x0063:
            monitor-exit(r9)
            return
        L_0x0065:
            int r0 = r12.aG     // Catch:{ all -> 0x0078 }
            int r0 = r0 + 2
            int r0 = r0 % 4
            if (r0 != r10) goto L_0x007e
            r0 = 2
            r9.pos = r0     // Catch:{ all -> 0x0078 }
            android.graphics.Matrix r0 = com.epoint.android.games.mjfgbfree.bb.mMatrix     // Catch:{ all -> 0x0078 }
            r1 = 1127481344(0x43340000, float:180.0)
            r0.setRotate(r1)     // Catch:{ all -> 0x0078 }
            goto L_0x001b
        L_0x0078:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0078 }
            throw r0     // Catch:{ all -> 0x007b }
        L_0x007b:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x007e:
            int r0 = r12.aG     // Catch:{ all -> 0x0078 }
            int r0 = r0 + 3
            int r0 = r0 % 4
            if (r0 != r10) goto L_0x0091
            r0 = 3
            r9.pos = r0     // Catch:{ all -> 0x0078 }
            android.graphics.Matrix r0 = com.epoint.android.games.mjfgbfree.bb.mMatrix     // Catch:{ all -> 0x0078 }
            r1 = 1119092736(0x42b40000, float:90.0)
            r0.setRotate(r1)     // Catch:{ all -> 0x0078 }
            goto L_0x001b
        L_0x0091:
            monitor-exit(r9)     // Catch:{ all -> 0x0078 }
            goto L_0x0063
        L_0x0093:
            r9.oJ = r0     // Catch:{ all -> 0x0078 }
            goto L_0x005d
        L_0x0096:
            int r1 = r11.getWidth()     // Catch:{ all -> 0x0078 }
            int r2 = r11.getHeight()     // Catch:{ all -> 0x0078 }
            int r3 = r0.getHeight()     // Catch:{ all -> 0x0078 }
            int r2 = r2 - r3
            int r2 = r2 / 2
            int r3 = r9.oI     // Catch:{ all -> 0x0078 }
            int r3 = r3 * 40
            int r1 = r1 - r3
            r8 = r2
            r2 = r1
            r1 = r8
        L_0x00ad:
            boolean r3 = com.epoint.android.games.mjfgbfree.ai.np     // Catch:{ all -> 0x0078 }
            if (r3 == 0) goto L_0x00e3
            if (r1 >= 0) goto L_0x011a
            int r3 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x0078 }
            if (r3 != 0) goto L_0x0118
            int r3 = com.epoint.android.games.mjfgbfree.bb.dR()     // Catch:{ all -> 0x0078 }
        L_0x00bb:
            android.graphics.Rect r4 = new android.graphics.Rect     // Catch:{ all -> 0x0078 }
            r5 = 0
            int r1 = java.lang.Math.abs(r1)     // Catch:{ all -> 0x0078 }
            int r6 = r0.getWidth()     // Catch:{ all -> 0x0078 }
            int r7 = r0.getHeight()     // Catch:{ all -> 0x0078 }
            r4.<init>(r5, r1, r6, r7)     // Catch:{ all -> 0x0078 }
            android.graphics.Rect r1 = new android.graphics.Rect     // Catch:{ all -> 0x0078 }
            int r5 = r4.width()     // Catch:{ all -> 0x0078 }
            int r5 = r5 + r2
            int r6 = r4.height()     // Catch:{ all -> 0x0078 }
            int r6 = r6 + r3
            r1.<init>(r2, r3, r5, r6)     // Catch:{ all -> 0x0078 }
            com.epoint.android.games.mjfgbfree.bb r2 = r9.cu     // Catch:{ all -> 0x0078 }
            android.graphics.Paint r2 = r2.gN     // Catch:{ all -> 0x0078 }
            r11.drawBitmap(r0, r4, r1, r2)     // Catch:{ all -> 0x0078 }
        L_0x00e3:
            monitor-exit(r9)     // Catch:{ all -> 0x0078 }
            goto L_0x0063
        L_0x00e6:
            int r1 = r11.getWidth()     // Catch:{ all -> 0x0078 }
            int r2 = r0.getWidth()     // Catch:{ all -> 0x0078 }
            int r1 = r1 - r2
            int r1 = r1 / 2
            int r2 = r0.getHeight()     // Catch:{ all -> 0x0078 }
            int r2 = -r2
            int r3 = r9.oI     // Catch:{ all -> 0x0078 }
            int r3 = r3 * 40
            int r2 = r2 + r3
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x00ad
        L_0x00ff:
            int r1 = r0.getWidth()     // Catch:{ all -> 0x0078 }
            int r1 = -r1
            int r2 = r11.getHeight()     // Catch:{ all -> 0x0078 }
            int r3 = r0.getHeight()     // Catch:{ all -> 0x0078 }
            int r2 = r2 - r3
            int r2 = r2 / 2
            int r3 = r9.oI     // Catch:{ all -> 0x0078 }
            int r3 = r3 * 40
            int r1 = r1 + r3
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x00ad
        L_0x0118:
            r3 = r7
            goto L_0x00bb
        L_0x011a:
            float r2 = (float) r2     // Catch:{ all -> 0x0078 }
            float r1 = (float) r1     // Catch:{ all -> 0x0078 }
            com.epoint.android.games.mjfgbfree.bb r3 = r9.cu     // Catch:{ all -> 0x0078 }
            android.graphics.Paint r3 = r3.gN     // Catch:{ all -> 0x0078 }
            r11.drawBitmap(r0, r2, r1, r3)     // Catch:{ all -> 0x0078 }
            goto L_0x00e3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.av.a(int, android.graphics.Canvas, com.epoint.android.games.mjfgbfree.d.g, java.lang.String[]):void");
    }

    public final void a(int i, g gVar, String[] strArr) {
        this.oR = true;
        synchronized (this) {
            this.type = 1;
            if (gVar.aG != i) {
                b(i, gVar, strArr);
            } else {
                this.pos = 0;
            }
            this.oH = 4;
            try {
                Thread.sleep(40);
            } catch (InterruptedException e) {
            }
        }
        for (int i2 = ai.nh ? this.oH : 1; i2 <= this.oH; i2++) {
            this.oI = i2;
            this.cu.eq();
            do {
                try {
                    Thread.sleep(80);
                } catch (InterruptedException e2) {
                }
            } while (this.cu.uu);
        }
        this.type = 2;
        this.oR = false;
    }

    public final void a(int i, g gVar, String[] strArr, a aVar) {
        int i2;
        int i3;
        float textSize;
        this.oR = true;
        synchronized (this) {
            this.type = 4;
            this.oK = ((float) MJ16Activity.qS) / ((float) MJ16Activity.qZ);
            this.oL = ((float) MJ16Activity.qR) / ((float) MJ16Activity.ra);
            this.oP = aVar;
            this.ju = new Rect(this.oP.getBounds());
            int width = (int) (((float) this.oP.getBounds().width()) * this.oK);
            int height = (int) (((float) this.oP.getBounds().height()) * this.oL);
            if ((gVar.aG + 1) % 4 == i) {
                i2 = (MJ16Activity.qR - height) / 2;
                i3 = (MJ16Activity.qS - width) - ((int) (((float) (ai.np ? 150 : 100)) * this.oK));
            } else if ((gVar.aG + 2) % 4 == i) {
                i2 = ((int) (((float) (ai.np ? 150 : 100)) * this.oL)) + (bb.ty == 0 ? (int) (((float) bb.dR()) * this.oL) : 0);
                i3 = (MJ16Activity.qS - width) / 2;
            } else if ((gVar.aG + 3) % 4 == i) {
                i2 = (MJ16Activity.qR - height) / 2;
                i3 = (int) (((float) (ai.np ? 150 : 100)) * this.oK);
            } else if (gVar.aG == i) {
                i2 = (int) (((float) (MJ16Activity.qR - ((int) (250.0f * this.oL)))) + (bb.ty == 0 ? ((float) bb.dR()) * this.oL : 0.0f));
                i3 = (MJ16Activity.qS - width) / 2;
            } else {
                i2 = 0;
                i3 = 0;
            }
            this.oP.setBounds(new Rect(i3, i2, width + i3, height + i2));
            this.oQ = new Rect(this.oP.getBounds());
            if (gVar.aG != i) {
                b(i, gVar, strArr);
            } else {
                this.pos = 0;
            }
            this.oH = 4;
            textSize = this.oP.aY().getTextSize();
            this.oP.aY().setTextSize(MJ16Activity.rc * textSize);
        }
        for (int i4 = ai.nh ? this.oH : 1; i4 <= this.oH; i4++) {
            this.oI = i4;
            this.cu.eq();
            do {
                try {
                    Thread.sleep(60);
                } catch (InterruptedException e) {
                }
            } while (this.cu.uu);
        }
        do {
            try {
                Thread.sleep(80);
            } catch (InterruptedException e2) {
            }
        } while (this.cu.uu);
        this.oP.aY().setTextSize(textSize);
        this.oP.setBounds(this.ju);
        this.oR = false;
        this.type = 0;
    }

    public final void a(int i, boolean z, g gVar, String[] strArr) {
        while (this.cu.uu) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
            }
        }
        this.oR = true;
        synchronized (this) {
            this.type = 0;
            b(i, gVar, strArr);
            this.oH = 4;
        }
        int i2 = ai.nh ? this.oH : 1;
        int i3 = i2;
        int i4 = this.oH - i2;
        while (i3 <= this.oH) {
            this.oI = z ? i3 : i4;
            this.cu.eq();
            do {
                try {
                    Thread.sleep(70);
                } catch (InterruptedException e2) {
                }
            } while (this.cu.uu);
            i3++;
            i4--;
        }
        this.oR = false;
    }

    public final void a(a aVar) {
        if ((ai.nh ? this.oH : 1) <= 3) {
            this.cu.uu = true;
            synchronized (this) {
                this.type = 3;
                aVar.c((MJ16Activity.qZ - aVar.bb()) / 2, (bb.ty == 1 ? 0 : bb.dR()) + (((MJ16Activity.ra - bb.dR()) - aVar.ba()) / 2));
                this.oM = aVar;
                this.ju = new Rect(aVar.getBounds());
                this.oN = 64;
                this.oO = aVar.aZ();
                this.oH = 4;
                this.oI = 3;
            }
            this.cu.eq();
        }
    }

    public final void a(a aVar, boolean z) {
        this.oR = true;
        synchronized (this) {
            this.type = 3;
            aVar.c((MJ16Activity.qZ - aVar.bb()) / 2, (bb.ty == 1 ? 0 : bb.dR()) + (((MJ16Activity.ra - bb.dR()) - aVar.ba()) / 2));
            this.oM = aVar;
            this.ju = new Rect(aVar.getBounds());
            this.oN = 64;
            this.oO = aVar.aZ();
            this.oH = 4;
        }
        int i = ai.nh ? this.oH : 1;
        if (i < this.oH) {
            int i2 = i;
            int i3 = this.oH - i;
            while (i2 < this.oH) {
                this.oI = z ? i2 : i3;
                this.cu.eq();
                do {
                    try {
                        Thread.sleep(60);
                    } catch (InterruptedException e) {
                    }
                } while (this.cu.uu);
                i2++;
                i3--;
            }
            this.oM.setBounds(this.ju);
        } else {
            cH();
        }
        this.oR = false;
    }

    public final void cG() {
        this.pos = -1;
    }

    public final void cH() {
        this.cu.uu = true;
        this.type = 2;
        this.cu.eq();
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x0067  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void onDraw(android.graphics.Canvas r10) {
        /*
            r9 = this;
            r4 = 1
            r5 = 0
            monitor-enter(r9)
            monitor-enter(r9)     // Catch:{ all -> 0x0045 }
            boolean r0 = r9.oS     // Catch:{ all -> 0x0042 }
            if (r0 != 0) goto L_0x0022
            r0 = 1
            r9.oS = r0     // Catch:{ all -> 0x0042 }
            com.epoint.android.games.mjfgbfree.bb r0 = r9.cu     // Catch:{ all -> 0x0042 }
            r0.dX()     // Catch:{ all -> 0x0042 }
            int r0 = com.epoint.android.games.mjfgbfree.MJ16Activity.qS     // Catch:{ all -> 0x0042 }
            float r0 = (float) r0     // Catch:{ all -> 0x0042 }
            int r1 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0042 }
            float r1 = (float) r1     // Catch:{ all -> 0x0042 }
            float r0 = r0 / r1
            r9.oK = r0     // Catch:{ all -> 0x0042 }
            int r0 = com.epoint.android.games.mjfgbfree.MJ16Activity.qR     // Catch:{ all -> 0x0042 }
            float r0 = (float) r0     // Catch:{ all -> 0x0042 }
            int r1 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra     // Catch:{ all -> 0x0042 }
            float r1 = (float) r1     // Catch:{ all -> 0x0042 }
            float r0 = r0 / r1
            r9.oL = r0     // Catch:{ all -> 0x0042 }
        L_0x0022:
            int r0 = r9.oI     // Catch:{ all -> 0x0042 }
            if (r0 != 0) goto L_0x0029
            monitor-exit(r9)     // Catch:{ all -> 0x0042 }
        L_0x0027:
            monitor-exit(r9)
            return
        L_0x0029:
            com.epoint.android.games.mjfgbfree.bb r0 = r9.cu     // Catch:{ all -> 0x0042 }
            r1 = 1
            r0.uu = r1     // Catch:{ all -> 0x0042 }
            int r0 = r9.type     // Catch:{ all -> 0x0042 }
            if (r0 != 0) goto L_0x00fd
            r0 = 0
            r10.drawColor(r0)     // Catch:{ all -> 0x0042 }
            int r0 = r9.pos     // Catch:{ all -> 0x0042 }
            switch(r0) {
                case 1: goto L_0x0048;
                case 2: goto L_0x00ac;
                case 3: goto L_0x00cd;
                default: goto L_0x003b;
            }     // Catch:{ all -> 0x0042 }
        L_0x003b:
            com.epoint.android.games.mjfgbfree.bb r0 = r9.cu     // Catch:{ all -> 0x0042 }
            r1 = 0
            r0.uu = r1     // Catch:{ all -> 0x0042 }
            monitor-exit(r9)     // Catch:{ all -> 0x0042 }
            goto L_0x0027
        L_0x0042:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0042 }
            throw r0     // Catch:{ all -> 0x0045 }
        L_0x0045:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x0048:
            int r0 = com.epoint.android.games.mjfgbfree.MJ16Activity.qS     // Catch:{ all -> 0x0042 }
            int r1 = com.epoint.android.games.mjfgbfree.MJ16Activity.qR     // Catch:{ all -> 0x0042 }
            android.graphics.Bitmap r2 = r9.oJ     // Catch:{ all -> 0x0042 }
            int r2 = r2.getHeight()     // Catch:{ all -> 0x0042 }
            int r1 = r1 - r2
            int r1 = r1 / 2
            float r0 = (float) r0     // Catch:{ all -> 0x0042 }
            int r2 = r9.oI     // Catch:{ all -> 0x0042 }
            int r2 = r2 * 40
            float r2 = (float) r2     // Catch:{ all -> 0x0042 }
            float r3 = r9.oK     // Catch:{ all -> 0x0042 }
            float r2 = r2 * r3
            float r0 = r0 - r2
            int r0 = (int) r0     // Catch:{ all -> 0x0042 }
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x0063:
            boolean r2 = com.epoint.android.games.mjfgbfree.ai.np     // Catch:{ all -> 0x0042 }
            if (r2 == 0) goto L_0x00a4
            if (r0 >= 0) goto L_0x00f1
            int r2 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x0042 }
            if (r2 != 0) goto L_0x00ef
            int r2 = com.epoint.android.games.mjfgbfree.bb.dR()     // Catch:{ all -> 0x0042 }
        L_0x0071:
            float r2 = (float) r2     // Catch:{ all -> 0x0042 }
            float r3 = r9.oL     // Catch:{ all -> 0x0042 }
            float r2 = r2 * r3
            int r2 = (int) r2     // Catch:{ all -> 0x0042 }
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x0042 }
            r4 = 0
            int r0 = java.lang.Math.abs(r0)     // Catch:{ all -> 0x0042 }
            android.graphics.Bitmap r5 = r9.oJ     // Catch:{ all -> 0x0042 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0042 }
            android.graphics.Bitmap r6 = r9.oJ     // Catch:{ all -> 0x0042 }
            int r6 = r6.getHeight()     // Catch:{ all -> 0x0042 }
            r3.<init>(r4, r0, r5, r6)     // Catch:{ all -> 0x0042 }
            android.graphics.Rect r0 = new android.graphics.Rect     // Catch:{ all -> 0x0042 }
            int r4 = r3.width()     // Catch:{ all -> 0x0042 }
            int r4 = r4 + r1
            int r5 = r3.height()     // Catch:{ all -> 0x0042 }
            int r5 = r5 + r2
            r0.<init>(r1, r2, r4, r5)     // Catch:{ all -> 0x0042 }
            android.graphics.Bitmap r1 = r9.oJ     // Catch:{ all -> 0x0042 }
            com.epoint.android.games.mjfgbfree.bb r2 = r9.cu     // Catch:{ all -> 0x0042 }
            android.graphics.Paint r2 = r2.gN     // Catch:{ all -> 0x0042 }
            r10.drawBitmap(r1, r3, r0, r2)     // Catch:{ all -> 0x0042 }
        L_0x00a4:
            com.epoint.android.games.mjfgbfree.bb r0 = r9.cu     // Catch:{ all -> 0x0042 }
            r1 = 0
            r0.uu = r1     // Catch:{ all -> 0x0042 }
            monitor-exit(r9)     // Catch:{ all -> 0x0042 }
            goto L_0x0027
        L_0x00ac:
            int r0 = com.epoint.android.games.mjfgbfree.MJ16Activity.qS     // Catch:{ all -> 0x0042 }
            android.graphics.Bitmap r1 = r9.oJ     // Catch:{ all -> 0x0042 }
            int r1 = r1.getWidth()     // Catch:{ all -> 0x0042 }
            int r0 = r0 - r1
            int r0 = r0 / 2
            android.graphics.Bitmap r1 = r9.oJ     // Catch:{ all -> 0x0042 }
            int r1 = r1.getHeight()     // Catch:{ all -> 0x0042 }
            int r1 = -r1
            float r1 = (float) r1     // Catch:{ all -> 0x0042 }
            int r2 = r9.oI     // Catch:{ all -> 0x0042 }
            int r2 = r2 * 40
            float r2 = (float) r2     // Catch:{ all -> 0x0042 }
            float r3 = r9.oL     // Catch:{ all -> 0x0042 }
            float r2 = r2 * r3
            float r1 = r1 + r2
            int r1 = (int) r1     // Catch:{ all -> 0x0042 }
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0063
        L_0x00cd:
            android.graphics.Bitmap r0 = r9.oJ     // Catch:{ all -> 0x0042 }
            int r0 = r0.getWidth()     // Catch:{ all -> 0x0042 }
            int r0 = -r0
            int r1 = com.epoint.android.games.mjfgbfree.MJ16Activity.qR     // Catch:{ all -> 0x0042 }
            android.graphics.Bitmap r2 = r9.oJ     // Catch:{ all -> 0x0042 }
            int r2 = r2.getHeight()     // Catch:{ all -> 0x0042 }
            int r1 = r1 - r2
            int r1 = r1 / 2
            float r0 = (float) r0     // Catch:{ all -> 0x0042 }
            int r2 = r9.oI     // Catch:{ all -> 0x0042 }
            int r2 = r2 * 40
            float r2 = (float) r2     // Catch:{ all -> 0x0042 }
            float r3 = r9.oK     // Catch:{ all -> 0x0042 }
            float r2 = r2 * r3
            float r0 = r0 + r2
            int r0 = (int) r0     // Catch:{ all -> 0x0042 }
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0063
        L_0x00ef:
            r2 = r5
            goto L_0x0071
        L_0x00f1:
            android.graphics.Bitmap r2 = r9.oJ     // Catch:{ all -> 0x0042 }
            float r1 = (float) r1     // Catch:{ all -> 0x0042 }
            float r0 = (float) r0     // Catch:{ all -> 0x0042 }
            com.epoint.android.games.mjfgbfree.bb r3 = r9.cu     // Catch:{ all -> 0x0042 }
            android.graphics.Paint r3 = r3.gN     // Catch:{ all -> 0x0042 }
            r10.drawBitmap(r2, r1, r0, r3)     // Catch:{ all -> 0x0042 }
            goto L_0x00a4
        L_0x00fd:
            int r0 = r9.type     // Catch:{ all -> 0x0042 }
            if (r0 != r4) goto L_0x01cd
            int r0 = r9.oI     // Catch:{ all -> 0x0042 }
            int r0 = r0 % 2
            if (r0 != r4) goto L_0x0163
            r0 = 128(0x80, float:1.794E-43)
            r1 = 255(0xff, float:3.57E-43)
            r2 = 255(0xff, float:3.57E-43)
            r3 = 255(0xff, float:3.57E-43)
            r10.drawARGB(r0, r1, r2, r3)     // Catch:{ all -> 0x0042 }
        L_0x0112:
            int r0 = r9.pos     // Catch:{ all -> 0x0042 }
            if (r0 == 0) goto L_0x00a4
            int r0 = r9.oH     // Catch:{ all -> 0x0042 }
            boolean r1 = com.epoint.android.games.mjfgbfree.ai.np     // Catch:{ all -> 0x0042 }
            if (r1 != 0) goto L_0x011d
            r0 = r4
        L_0x011d:
            int r1 = r9.pos     // Catch:{ all -> 0x0042 }
            switch(r1) {
                case 1: goto L_0x0168;
                case 2: goto L_0x0182;
                case 3: goto L_0x019e;
                default: goto L_0x0122;
            }     // Catch:{ all -> 0x0042 }
        L_0x0122:
            r0 = r5
            r1 = r5
        L_0x0124:
            if (r0 >= 0) goto L_0x01c0
            int r2 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x0042 }
            if (r2 != 0) goto L_0x01bd
            int r2 = com.epoint.android.games.mjfgbfree.bb.dR()     // Catch:{ all -> 0x0042 }
        L_0x012e:
            float r2 = (float) r2     // Catch:{ all -> 0x0042 }
            float r3 = r9.oL     // Catch:{ all -> 0x0042 }
            float r2 = r2 * r3
            int r2 = (int) r2     // Catch:{ all -> 0x0042 }
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x0042 }
            r4 = 0
            int r0 = java.lang.Math.abs(r0)     // Catch:{ all -> 0x0042 }
            android.graphics.Bitmap r5 = r9.oJ     // Catch:{ all -> 0x0042 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0042 }
            android.graphics.Bitmap r6 = r9.oJ     // Catch:{ all -> 0x0042 }
            int r6 = r6.getHeight()     // Catch:{ all -> 0x0042 }
            r3.<init>(r4, r0, r5, r6)     // Catch:{ all -> 0x0042 }
            android.graphics.Rect r0 = new android.graphics.Rect     // Catch:{ all -> 0x0042 }
            int r4 = r3.width()     // Catch:{ all -> 0x0042 }
            int r4 = r4 + r1
            int r5 = r3.height()     // Catch:{ all -> 0x0042 }
            int r5 = r5 + r2
            r0.<init>(r1, r2, r4, r5)     // Catch:{ all -> 0x0042 }
            android.graphics.Bitmap r1 = r9.oJ     // Catch:{ all -> 0x0042 }
            com.epoint.android.games.mjfgbfree.bb r2 = r9.cu     // Catch:{ all -> 0x0042 }
            android.graphics.Paint r2 = r2.gN     // Catch:{ all -> 0x0042 }
            r10.drawBitmap(r1, r3, r0, r2)     // Catch:{ all -> 0x0042 }
            goto L_0x00a4
        L_0x0163:
            r0 = 0
            r10.drawColor(r0)     // Catch:{ all -> 0x0042 }
            goto L_0x0112
        L_0x0168:
            int r1 = com.epoint.android.games.mjfgbfree.MJ16Activity.qS     // Catch:{ all -> 0x0042 }
            int r2 = com.epoint.android.games.mjfgbfree.MJ16Activity.qR     // Catch:{ all -> 0x0042 }
            android.graphics.Bitmap r3 = r9.oJ     // Catch:{ all -> 0x0042 }
            int r3 = r3.getHeight()     // Catch:{ all -> 0x0042 }
            int r2 = r2 - r3
            int r2 = r2 / 2
            float r1 = (float) r1     // Catch:{ all -> 0x0042 }
            int r0 = r0 * 40
            float r0 = (float) r0     // Catch:{ all -> 0x0042 }
            float r3 = r9.oK     // Catch:{ all -> 0x0042 }
            float r0 = r0 * r3
            float r0 = r1 - r0
            int r0 = (int) r0     // Catch:{ all -> 0x0042 }
            r1 = r0
            r0 = r2
            goto L_0x0124
        L_0x0182:
            int r1 = com.epoint.android.games.mjfgbfree.MJ16Activity.qS     // Catch:{ all -> 0x0042 }
            android.graphics.Bitmap r2 = r9.oJ     // Catch:{ all -> 0x0042 }
            int r2 = r2.getWidth()     // Catch:{ all -> 0x0042 }
            int r1 = r1 - r2
            int r1 = r1 / 2
            android.graphics.Bitmap r2 = r9.oJ     // Catch:{ all -> 0x0042 }
            int r2 = r2.getHeight()     // Catch:{ all -> 0x0042 }
            int r2 = -r2
            float r2 = (float) r2     // Catch:{ all -> 0x0042 }
            int r0 = r0 * 40
            float r0 = (float) r0     // Catch:{ all -> 0x0042 }
            float r3 = r9.oL     // Catch:{ all -> 0x0042 }
            float r0 = r0 * r3
            float r0 = r0 + r2
            int r0 = (int) r0     // Catch:{ all -> 0x0042 }
            goto L_0x0124
        L_0x019e:
            android.graphics.Bitmap r1 = r9.oJ     // Catch:{ all -> 0x0042 }
            int r1 = r1.getWidth()     // Catch:{ all -> 0x0042 }
            int r1 = -r1
            int r2 = com.epoint.android.games.mjfgbfree.MJ16Activity.qR     // Catch:{ all -> 0x0042 }
            android.graphics.Bitmap r3 = r9.oJ     // Catch:{ all -> 0x0042 }
            int r3 = r3.getHeight()     // Catch:{ all -> 0x0042 }
            int r2 = r2 - r3
            int r2 = r2 / 2
            float r1 = (float) r1     // Catch:{ all -> 0x0042 }
            int r0 = r0 * 40
            float r0 = (float) r0     // Catch:{ all -> 0x0042 }
            float r3 = r9.oK     // Catch:{ all -> 0x0042 }
            float r0 = r0 * r3
            float r0 = r0 + r1
            int r0 = (int) r0     // Catch:{ all -> 0x0042 }
            r1 = r0
            r0 = r2
            goto L_0x0124
        L_0x01bd:
            r2 = r5
            goto L_0x012e
        L_0x01c0:
            android.graphics.Bitmap r2 = r9.oJ     // Catch:{ all -> 0x0042 }
            float r1 = (float) r1     // Catch:{ all -> 0x0042 }
            float r0 = (float) r0     // Catch:{ all -> 0x0042 }
            com.epoint.android.games.mjfgbfree.bb r3 = r9.cu     // Catch:{ all -> 0x0042 }
            android.graphics.Paint r3 = r3.gN     // Catch:{ all -> 0x0042 }
            r10.drawBitmap(r2, r1, r0, r3)     // Catch:{ all -> 0x0042 }
            goto L_0x00a4
        L_0x01cd:
            int r0 = r9.type     // Catch:{ all -> 0x0042 }
            r1 = 2
            if (r0 != r1) goto L_0x01d8
            r0 = 0
            r10.drawColor(r0)     // Catch:{ all -> 0x0042 }
            goto L_0x00a4
        L_0x01d8:
            int r0 = r9.type     // Catch:{ all -> 0x0042 }
            r1 = 3
            if (r0 != r1) goto L_0x0308
            android.graphics.Rect r0 = new android.graphics.Rect     // Catch:{ all -> 0x0042 }
            com.epoint.android.games.mjfgbfree.ui.a r1 = r9.oM     // Catch:{ all -> 0x0042 }
            android.graphics.Rect r1 = r1.getBounds()     // Catch:{ all -> 0x0042 }
            r0.<init>(r1)     // Catch:{ all -> 0x0042 }
            com.epoint.android.games.mjfgbfree.bb r1 = r9.cu     // Catch:{ all -> 0x0042 }
            android.graphics.Rect r1 = r1.ut     // Catch:{ all -> 0x0042 }
            if (r1 == 0) goto L_0x02dd
            android.graphics.Rect r1 = r9.ju     // Catch:{ all -> 0x0042 }
            int r1 = r1.top     // Catch:{ all -> 0x0042 }
            com.epoint.android.games.mjfgbfree.bb r2 = r9.cu     // Catch:{ all -> 0x0042 }
            android.graphics.Rect r2 = r2.ut     // Catch:{ all -> 0x0042 }
            int r2 = r2.top     // Catch:{ all -> 0x0042 }
            int r1 = r1 - r2
            int r2 = r9.oI     // Catch:{ all -> 0x0042 }
            int r1 = r1 * r2
            int r2 = r9.oH     // Catch:{ all -> 0x0042 }
            int r1 = r1 / r2
            android.graphics.Rect r2 = r9.ju     // Catch:{ all -> 0x0042 }
            int r2 = r2.left     // Catch:{ all -> 0x0042 }
            com.epoint.android.games.mjfgbfree.bb r3 = r9.cu     // Catch:{ all -> 0x0042 }
            android.graphics.Rect r3 = r3.ut     // Catch:{ all -> 0x0042 }
            int r3 = r3.left     // Catch:{ all -> 0x0042 }
            int r2 = r2 - r3
            int r3 = r9.oI     // Catch:{ all -> 0x0042 }
            int r2 = r2 * r3
            int r3 = r9.oH     // Catch:{ all -> 0x0042 }
            int r2 = r2 / r3
            com.epoint.android.games.mjfgbfree.bb r3 = r9.cu     // Catch:{ all -> 0x0042 }
            android.graphics.Rect r3 = r3.ut     // Catch:{ all -> 0x0042 }
            int r3 = r3.top     // Catch:{ all -> 0x0042 }
            int r1 = r1 + r3
            r0.top = r1     // Catch:{ all -> 0x0042 }
            com.epoint.android.games.mjfgbfree.bb r1 = r9.cu     // Catch:{ all -> 0x0042 }
            android.graphics.Rect r1 = r1.ut     // Catch:{ all -> 0x0042 }
            int r1 = r1.left     // Catch:{ all -> 0x0042 }
            int r1 = r1 + r2
            r0.left = r1     // Catch:{ all -> 0x0042 }
            com.epoint.android.games.mjfgbfree.bb r1 = r9.cu     // Catch:{ all -> 0x0042 }
            android.graphics.Rect r1 = r1.ut     // Catch:{ all -> 0x0042 }
            int r1 = r1.height()     // Catch:{ all -> 0x0042 }
            android.graphics.Rect r2 = r9.ju     // Catch:{ all -> 0x0042 }
            int r2 = r2.height()     // Catch:{ all -> 0x0042 }
            com.epoint.android.games.mjfgbfree.bb r3 = r9.cu     // Catch:{ all -> 0x0042 }
            android.graphics.Rect r3 = r3.ut     // Catch:{ all -> 0x0042 }
            int r3 = r3.height()     // Catch:{ all -> 0x0042 }
            int r2 = r2 - r3
            int r3 = r9.oI     // Catch:{ all -> 0x0042 }
            int r2 = r2 * r3
            int r3 = r9.oH     // Catch:{ all -> 0x0042 }
            int r2 = r2 / r3
            int r1 = r1 + r2
            com.epoint.android.games.mjfgbfree.bb r2 = r9.cu     // Catch:{ all -> 0x0042 }
            android.graphics.Rect r2 = r2.ut     // Catch:{ all -> 0x0042 }
            int r2 = r2.width()     // Catch:{ all -> 0x0042 }
            android.graphics.Rect r3 = r9.ju     // Catch:{ all -> 0x0042 }
            int r3 = r3.width()     // Catch:{ all -> 0x0042 }
            com.epoint.android.games.mjfgbfree.bb r4 = r9.cu     // Catch:{ all -> 0x0042 }
            android.graphics.Rect r4 = r4.ut     // Catch:{ all -> 0x0042 }
            int r4 = r4.width()     // Catch:{ all -> 0x0042 }
            int r3 = r3 - r4
            int r4 = r9.oI     // Catch:{ all -> 0x0042 }
            int r3 = r3 * r4
            int r4 = r9.oH     // Catch:{ all -> 0x0042 }
            int r3 = r3 / r4
            int r2 = r2 + r3
            r8 = r2
            r2 = r1
            r1 = r8
        L_0x0261:
            int r3 = r0.top     // Catch:{ all -> 0x0042 }
            int r2 = r2 + r3
            r0.bottom = r2     // Catch:{ all -> 0x0042 }
            int r2 = r0.left     // Catch:{ all -> 0x0042 }
            int r1 = r1 + r2
            r0.right = r1     // Catch:{ all -> 0x0042 }
            int r1 = r9.oN     // Catch:{ all -> 0x0042 }
            int r2 = r9.oI     // Catch:{ all -> 0x0042 }
            int r1 = r1 * r2
            int r2 = r9.oH     // Catch:{ all -> 0x0042 }
            int r1 = r1 / r2
            r2 = 0
            r3 = 0
            r4 = 0
            r10.drawARGB(r1, r2, r3, r4)     // Catch:{ all -> 0x0042 }
            com.epoint.android.games.mjfgbfree.ui.a r1 = r9.oM     // Catch:{ all -> 0x0042 }
            int r2 = r9.oO     // Catch:{ all -> 0x0042 }
            r3 = 20
            int r2 = r2 - r3
            int r3 = r9.oI     // Catch:{ all -> 0x0042 }
            int r2 = r2 * r3
            int r3 = r9.oH     // Catch:{ all -> 0x0042 }
            int r2 = r2 / r3
            r1.i(r2)     // Catch:{ all -> 0x0042 }
            float r1 = com.epoint.android.games.mjfgbfree.MJ16Activity.rc     // Catch:{ all -> 0x0042 }
            r2 = 1065353216(0x3f800000, float:1.0)
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 == 0) goto L_0x02d1
            int r1 = com.epoint.android.games.mjfgbfree.MJ16Activity.qS     // Catch:{ all -> 0x0042 }
            float r1 = (float) r1     // Catch:{ all -> 0x0042 }
            int r2 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0042 }
            float r2 = (float) r2     // Catch:{ all -> 0x0042 }
            float r1 = r1 / r2
            r9.oK = r1     // Catch:{ all -> 0x0042 }
            int r1 = com.epoint.android.games.mjfgbfree.MJ16Activity.qR     // Catch:{ all -> 0x0042 }
            float r1 = (float) r1     // Catch:{ all -> 0x0042 }
            int r2 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra     // Catch:{ all -> 0x0042 }
            float r2 = (float) r2     // Catch:{ all -> 0x0042 }
            float r1 = r1 / r2
            r9.oL = r1     // Catch:{ all -> 0x0042 }
            int r1 = r0.width()     // Catch:{ all -> 0x0042 }
            float r1 = (float) r1     // Catch:{ all -> 0x0042 }
            float r2 = r9.oK     // Catch:{ all -> 0x0042 }
            float r1 = r1 * r2
            int r1 = (int) r1     // Catch:{ all -> 0x0042 }
            int r2 = r0.height()     // Catch:{ all -> 0x0042 }
            float r2 = (float) r2     // Catch:{ all -> 0x0042 }
            float r3 = r9.oL     // Catch:{ all -> 0x0042 }
            float r2 = r2 * r3
            int r2 = (int) r2     // Catch:{ all -> 0x0042 }
            int r3 = r0.left     // Catch:{ all -> 0x0042 }
            float r3 = (float) r3     // Catch:{ all -> 0x0042 }
            float r4 = r9.oK     // Catch:{ all -> 0x0042 }
            float r3 = r3 * r4
            int r3 = (int) r3     // Catch:{ all -> 0x0042 }
            r0.left = r3     // Catch:{ all -> 0x0042 }
            int r3 = r0.top     // Catch:{ all -> 0x0042 }
            float r3 = (float) r3     // Catch:{ all -> 0x0042 }
            float r4 = r9.oL     // Catch:{ all -> 0x0042 }
            float r3 = r3 * r4
            int r3 = (int) r3     // Catch:{ all -> 0x0042 }
            r0.top = r3     // Catch:{ all -> 0x0042 }
            int r3 = r0.left     // Catch:{ all -> 0x0042 }
            int r1 = r1 + r3
            r0.right = r1     // Catch:{ all -> 0x0042 }
            int r1 = r0.top     // Catch:{ all -> 0x0042 }
            int r1 = r1 + r2
            r0.bottom = r1     // Catch:{ all -> 0x0042 }
        L_0x02d1:
            com.epoint.android.games.mjfgbfree.ui.a r1 = r9.oM     // Catch:{ all -> 0x0042 }
            r1.setBounds(r0)     // Catch:{ all -> 0x0042 }
            com.epoint.android.games.mjfgbfree.ui.a r0 = r9.oM     // Catch:{ all -> 0x0042 }
            r0.draw(r10)     // Catch:{ all -> 0x0042 }
            goto L_0x00a4
        L_0x02dd:
            android.graphics.Rect r1 = r9.ju     // Catch:{ all -> 0x0042 }
            int r1 = r1.height()     // Catch:{ all -> 0x0042 }
            int r2 = r9.oI     // Catch:{ all -> 0x0042 }
            int r1 = r1 * r2
            int r2 = r9.oH     // Catch:{ all -> 0x0042 }
            int r1 = r1 / r2
            android.graphics.Rect r2 = r9.ju     // Catch:{ all -> 0x0042 }
            int r2 = r2.width()     // Catch:{ all -> 0x0042 }
            int r3 = r9.oI     // Catch:{ all -> 0x0042 }
            int r2 = r2 * r3
            int r3 = r9.oH     // Catch:{ all -> 0x0042 }
            int r2 = r2 / r3
            int r3 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra     // Catch:{ all -> 0x0042 }
            int r3 = r3 - r1
            int r3 = r3 / 2
            r0.top = r3     // Catch:{ all -> 0x0042 }
            int r3 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0042 }
            int r3 = r3 - r2
            int r3 = r3 / 2
            r0.left = r3     // Catch:{ all -> 0x0042 }
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x0261
        L_0x0308:
            int r0 = r9.type     // Catch:{ all -> 0x0042 }
            r1 = 4
            if (r0 != r1) goto L_0x00a4
            com.epoint.android.games.mjfgbfree.ui.a r0 = r9.oP     // Catch:{ all -> 0x0042 }
            monitor-enter(r0)     // Catch:{ all -> 0x0042 }
            r1 = 0
            r10.drawColor(r1)     // Catch:{ all -> 0x03b0 }
            com.epoint.android.games.mjfgbfree.ui.a r1 = r9.oP     // Catch:{ all -> 0x03b0 }
            android.graphics.Rect r2 = new android.graphics.Rect     // Catch:{ all -> 0x03b0 }
            android.graphics.Rect r3 = r9.oQ     // Catch:{ all -> 0x03b0 }
            r2.<init>(r3)     // Catch:{ all -> 0x03b0 }
            r1.setBounds(r2)     // Catch:{ all -> 0x03b0 }
            android.graphics.Rect r1 = new android.graphics.Rect     // Catch:{ all -> 0x03b0 }
            com.epoint.android.games.mjfgbfree.ui.a r2 = r9.oP     // Catch:{ all -> 0x03b0 }
            android.graphics.Rect r2 = r2.getBounds()     // Catch:{ all -> 0x03b0 }
            r1.<init>(r2)     // Catch:{ all -> 0x03b0 }
            int r2 = r1.width()     // Catch:{ all -> 0x03b0 }
            int r3 = r9.oI     // Catch:{ all -> 0x03b0 }
            int r2 = r2 * r3
            int r3 = r9.oH     // Catch:{ all -> 0x03b0 }
            int r2 = r2 / r3
            int r2 = r2 / 2
            int r3 = r1.height()     // Catch:{ all -> 0x03b0 }
            int r4 = r9.oI     // Catch:{ all -> 0x03b0 }
            int r3 = r3 * r4
            int r4 = r9.oH     // Catch:{ all -> 0x03b0 }
            int r3 = r3 / r4
            int r3 = r3 / 2
            int r4 = r1.left     // Catch:{ all -> 0x03b0 }
            int r4 = r4 - r2
            r1.left = r4     // Catch:{ all -> 0x03b0 }
            int r4 = r1.right     // Catch:{ all -> 0x03b0 }
            int r2 = r2 + r4
            r1.right = r2     // Catch:{ all -> 0x03b0 }
            int r2 = r1.top     // Catch:{ all -> 0x03b0 }
            int r2 = r2 - r3
            r1.top = r2     // Catch:{ all -> 0x03b0 }
            int r2 = r1.bottom     // Catch:{ all -> 0x03b0 }
            int r2 = r2 + r3
            r1.bottom = r2     // Catch:{ all -> 0x03b0 }
            com.epoint.android.games.mjfgbfree.ui.a r2 = r9.oP     // Catch:{ all -> 0x03b0 }
            r2.setBounds(r1)     // Catch:{ all -> 0x03b0 }
            int r1 = r9.pos     // Catch:{ all -> 0x03b0 }
            if (r1 == 0) goto L_0x03a8
            int r1 = r9.pos     // Catch:{ all -> 0x03b0 }
            switch(r1) {
                case 1: goto L_0x03b3;
                case 2: goto L_0x03cf;
                case 3: goto L_0x03f1;
                default: goto L_0x0365;
            }     // Catch:{ all -> 0x03b0 }
        L_0x0365:
            r1 = r5
            r2 = r5
        L_0x0367:
            boolean r3 = com.epoint.android.games.mjfgbfree.ai.np     // Catch:{ all -> 0x03b0 }
            if (r3 == 0) goto L_0x03a8
            if (r1 >= 0) goto L_0x0416
            int r3 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x03b0 }
            if (r3 != 0) goto L_0x0413
            int r3 = com.epoint.android.games.mjfgbfree.bb.dR()     // Catch:{ all -> 0x03b0 }
        L_0x0375:
            float r3 = (float) r3     // Catch:{ all -> 0x03b0 }
            float r4 = r9.oL     // Catch:{ all -> 0x03b0 }
            float r3 = r3 * r4
            int r3 = (int) r3     // Catch:{ all -> 0x03b0 }
            android.graphics.Rect r4 = new android.graphics.Rect     // Catch:{ all -> 0x03b0 }
            r5 = 0
            int r1 = java.lang.Math.abs(r1)     // Catch:{ all -> 0x03b0 }
            android.graphics.Bitmap r6 = r9.oJ     // Catch:{ all -> 0x03b0 }
            int r6 = r6.getWidth()     // Catch:{ all -> 0x03b0 }
            android.graphics.Bitmap r7 = r9.oJ     // Catch:{ all -> 0x03b0 }
            int r7 = r7.getHeight()     // Catch:{ all -> 0x03b0 }
            r4.<init>(r5, r1, r6, r7)     // Catch:{ all -> 0x03b0 }
            android.graphics.Rect r1 = new android.graphics.Rect     // Catch:{ all -> 0x03b0 }
            int r5 = r4.width()     // Catch:{ all -> 0x03b0 }
            int r5 = r5 + r2
            int r6 = r4.height()     // Catch:{ all -> 0x03b0 }
            int r6 = r6 + r3
            r1.<init>(r2, r3, r5, r6)     // Catch:{ all -> 0x03b0 }
            android.graphics.Bitmap r2 = r9.oJ     // Catch:{ all -> 0x03b0 }
            com.epoint.android.games.mjfgbfree.bb r3 = r9.cu     // Catch:{ all -> 0x03b0 }
            android.graphics.Paint r3 = r3.gN     // Catch:{ all -> 0x03b0 }
            r10.drawBitmap(r2, r4, r1, r3)     // Catch:{ all -> 0x03b0 }
        L_0x03a8:
            com.epoint.android.games.mjfgbfree.ui.a r1 = r9.oP     // Catch:{ all -> 0x03b0 }
            r1.draw(r10)     // Catch:{ all -> 0x03b0 }
            monitor-exit(r0)     // Catch:{ all -> 0x03b0 }
            goto L_0x00a4
        L_0x03b0:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x03b0 }
            throw r1     // Catch:{ all -> 0x0042 }
        L_0x03b3:
            int r1 = com.epoint.android.games.mjfgbfree.MJ16Activity.qS     // Catch:{ all -> 0x03b0 }
            int r2 = com.epoint.android.games.mjfgbfree.MJ16Activity.qR     // Catch:{ all -> 0x03b0 }
            android.graphics.Bitmap r3 = r9.oJ     // Catch:{ all -> 0x03b0 }
            int r3 = r3.getHeight()     // Catch:{ all -> 0x03b0 }
            int r2 = r2 - r3
            int r2 = r2 / 2
            float r1 = (float) r1     // Catch:{ all -> 0x03b0 }
            int r3 = r9.oH     // Catch:{ all -> 0x03b0 }
            int r3 = r3 * 40
            float r3 = (float) r3     // Catch:{ all -> 0x03b0 }
            float r4 = r9.oK     // Catch:{ all -> 0x03b0 }
            float r3 = r3 * r4
            float r1 = r1 - r3
            int r1 = (int) r1     // Catch:{ all -> 0x03b0 }
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x0367
        L_0x03cf:
            int r1 = com.epoint.android.games.mjfgbfree.MJ16Activity.qS     // Catch:{ all -> 0x03b0 }
            android.graphics.Bitmap r2 = r9.oJ     // Catch:{ all -> 0x03b0 }
            int r2 = r2.getWidth()     // Catch:{ all -> 0x03b0 }
            int r1 = r1 - r2
            int r1 = r1 / 2
            android.graphics.Bitmap r2 = r9.oJ     // Catch:{ all -> 0x03b0 }
            int r2 = r2.getHeight()     // Catch:{ all -> 0x03b0 }
            int r2 = -r2
            float r2 = (float) r2     // Catch:{ all -> 0x03b0 }
            int r3 = r9.oH     // Catch:{ all -> 0x03b0 }
            int r3 = r3 * 40
            float r3 = (float) r3     // Catch:{ all -> 0x03b0 }
            float r4 = r9.oL     // Catch:{ all -> 0x03b0 }
            float r3 = r3 * r4
            float r2 = r2 + r3
            int r2 = (int) r2     // Catch:{ all -> 0x03b0 }
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x0367
        L_0x03f1:
            android.graphics.Bitmap r1 = r9.oJ     // Catch:{ all -> 0x03b0 }
            int r1 = r1.getWidth()     // Catch:{ all -> 0x03b0 }
            int r1 = -r1
            int r2 = com.epoint.android.games.mjfgbfree.MJ16Activity.qR     // Catch:{ all -> 0x03b0 }
            android.graphics.Bitmap r3 = r9.oJ     // Catch:{ all -> 0x03b0 }
            int r3 = r3.getHeight()     // Catch:{ all -> 0x03b0 }
            int r2 = r2 - r3
            int r2 = r2 / 2
            float r1 = (float) r1     // Catch:{ all -> 0x03b0 }
            int r3 = r9.oH     // Catch:{ all -> 0x03b0 }
            int r3 = r3 * 40
            float r3 = (float) r3     // Catch:{ all -> 0x03b0 }
            float r4 = r9.oK     // Catch:{ all -> 0x03b0 }
            float r3 = r3 * r4
            float r1 = r1 + r3
            int r1 = (int) r1     // Catch:{ all -> 0x03b0 }
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x0367
        L_0x0413:
            r3 = r5
            goto L_0x0375
        L_0x0416:
            android.graphics.Bitmap r3 = r9.oJ     // Catch:{ all -> 0x03b0 }
            float r2 = (float) r2     // Catch:{ all -> 0x03b0 }
            float r1 = (float) r1     // Catch:{ all -> 0x03b0 }
            com.epoint.android.games.mjfgbfree.bb r4 = r9.cu     // Catch:{ all -> 0x03b0 }
            android.graphics.Paint r4 = r4.gN     // Catch:{ all -> 0x03b0 }
            r10.drawBitmap(r3, r2, r1, r4)     // Catch:{ all -> 0x03b0 }
            goto L_0x03a8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.av.onDraw(android.graphics.Canvas):void");
    }
}
