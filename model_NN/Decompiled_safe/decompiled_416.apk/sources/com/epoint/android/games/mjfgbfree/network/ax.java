package com.epoint.android.games.mjfgbfree.network;

import android.app.AlertDialog;
import android.view.View;
import android.widget.TextView;
import com.epoint.android.games.mjfgbfree.af;
import com.epoint.android.games.mjfgbfree.ai;

final class ax implements View.OnClickListener {
    final /* synthetic */ BTConnectionActivity pk;

    ax(BTConnectionActivity bTConnectionActivity) {
        this.pk = bTConnectionActivity;
    }

    public final void onClick(View view) {
        if (this.pk.ah != null) {
            this.pk.a(2, af.aa("等候狀態中不能主動連接其他裝置"));
        } else if (this.pk.ag.isDiscovering()) {
            this.pk.a(2, af.aa("偵測狀能中不能連線"));
        } else {
            String str = (String) view.getTag();
            AlertDialog.Builder builder = new AlertDialog.Builder(this.pk);
            if (ai.nk == 4) {
                builder.setTitle(String.valueOf(af.aa("連接到").replace("XX", " \"" + ((Object) ((TextView) view).getText()) + "\"")) + "?");
            } else {
                builder.setTitle(String.valueOf(af.aa("連接到")) + " \"" + ((Object) ((TextView) view).getText()) + "\" ?");
            }
            builder.setPositiveButton(af.aa("是"), new ac(this, str));
            builder.setNegativeButton(af.aa("否"), new ad(this));
            builder.create().show();
        }
    }
}
