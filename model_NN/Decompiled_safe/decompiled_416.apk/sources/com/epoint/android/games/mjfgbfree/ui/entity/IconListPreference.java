package com.epoint.android.games.mjfgbfree.ui.entity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.a.d;
import com.epoint.android.games.mjfgbfree.af;
import java.util.Vector;

public class IconListPreference extends ListPreference implements DialogInterface.OnClickListener {
    private CharSequence[] Z;
    private Context aa;
    private Bitmap iY = null;
    private TableLayout yV;
    private Bitmap yW = null;
    private CharSequence[] yX;
    private String yY = "images/";
    private String yZ = "";

    public IconListPreference(Context context) {
        super(context);
        u(context);
    }

    public IconListPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        u(context);
    }

    public IconListPreference(Context context, String str) {
        super(context);
        this.yZ = str;
        u(context);
    }

    private void u(Context context) {
        this.aa = context;
        this.yV = (TableLayout) LayoutInflater.from(context).inflate((int) C0000R.layout.image_preference_element, (ViewGroup) null);
    }

    public final void a(Bitmap bitmap, int i, String str) {
        Bitmap bitmap2;
        ImageView imageView = (ImageView) this.yV.findViewById(C0000R.id.icon);
        if (this.iY != null) {
            this.iY.recycle();
        }
        if (!"".equals(str)) {
            bitmap2 = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap2);
            canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
            canvas.drawColor(Integer.valueOf(str).intValue(), PorterDuff.Mode.SCREEN);
            if (this.yW != null) {
                canvas.drawBitmap(this.yW, 0.0f, 0.0f, (Paint) null);
                bitmap2.setPixel(0, 0, 0);
                bitmap2.setPixel(bitmap2.getWidth() - 1, 0, 0);
                bitmap2.setPixel(0, bitmap2.getHeight() - 1, 0);
                bitmap2.setPixel(bitmap2.getWidth() - 1, bitmap2.getHeight() - 1, 0);
            }
        } else {
            bitmap2 = bitmap;
        }
        imageView.setImageBitmap(bitmap2);
        imageView.setBackgroundColor(i);
        this.iY = bitmap;
    }

    public final void aU(String str) {
        this.yY = str;
    }

    public final void b(Bitmap bitmap) {
        this.yW = bitmap;
    }

    public final void c(Bitmap bitmap) {
        a(bitmap, -16777216, this.yZ);
    }

    public final void c(Bitmap bitmap, int i) {
        a(bitmap, i, this.yZ);
    }

    public View getView(View view, ViewGroup viewGroup) {
        return this.yV;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (i >= 0 && getOnPreferenceChangeListener().onPreferenceChange(this, this.yX[i].toString())) {
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.aa).edit();
            edit.putString(getKey(), this.yX[i].toString());
            edit.commit();
        }
        super.onClick(dialogInterface, i);
    }

    /* access modifiers changed from: protected */
    public View onCreateView(ViewGroup viewGroup) {
        ((ImageView) this.yV.findViewById(C0000R.id.icon)).setImageBitmap(this.iY);
        return this.yV;
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        this.yX = getEntryValues();
        this.Z = getEntries();
        Vector vector = new Vector();
        Vector vector2 = new Vector();
        for (int i = 0; i < this.yX.length; i++) {
            vector.add(new StringBuilder(this.Z[i]));
            if (!"".equals(this.yZ)) {
                Bitmap createBitmap = Bitmap.createBitmap(this.iY.getWidth(), this.iY.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(createBitmap);
                canvas.drawBitmap(this.iY, 0.0f, 0.0f, (Paint) null);
                canvas.drawColor(Integer.valueOf(this.yX[i].toString()).intValue(), PorterDuff.Mode.SCREEN);
                if (this.yW != null) {
                    canvas.drawBitmap(this.yW, 0.0f, 0.0f, (Paint) null);
                }
                createBitmap.setPixel(0, 0, 0);
                createBitmap.setPixel(createBitmap.getWidth() - 1, 0, 0);
                createBitmap.setPixel(0, createBitmap.getHeight() - 1, 0);
                createBitmap.setPixel(createBitmap.getWidth() - 1, createBitmap.getHeight() - 1, 0);
                vector2.add(new BitmapDrawable(this.aa.getResources(), createBitmap));
            } else {
                vector2.add(new BitmapDrawable(this.aa.getResources(), d.c(this.aa, String.valueOf(this.yY) + this.yX[i].toString() + ".png")));
            }
        }
        c cVar = new c(this.aa, vector, vector2, null);
        cVar.cQ();
        builder.setPositiveButton((CharSequence) null, (DialogInterface.OnClickListener) null);
        builder.setNegativeButton(af.aa("取消"), this);
        builder.setAdapter(cVar, this);
    }

    public void setOnPreferenceChangeListener(Preference.OnPreferenceChangeListener onPreferenceChangeListener) {
        super.setOnPreferenceChangeListener(onPreferenceChangeListener);
    }

    public void setSummary(CharSequence charSequence) {
        TextView textView = (TextView) this.yV.findViewById(C0000R.id.summary);
        if (charSequence != null) {
            textView.setText(charSequence);
            super.setSummary(charSequence);
            return;
        }
        textView.setVisibility(8);
    }

    public void setTitle(CharSequence charSequence) {
        ((TextView) this.yV.findViewById(C0000R.id.title)).setText(charSequence);
        super.setTitle(charSequence);
    }
}
