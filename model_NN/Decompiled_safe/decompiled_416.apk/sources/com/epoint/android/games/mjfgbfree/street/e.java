package com.epoint.android.games.mjfgbfree.street;

import android.graphics.drawable.Drawable;
import android.view.GestureDetector;
import android.view.MotionEvent;
import com.epoint.android.games.mjfgbfree.a.c;
import com.epoint.android.games.mjfgbfree.a.g;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import java.util.Vector;

final class e extends ItemizedOverlay implements c {
    private /* synthetic */ PlacesMapActivity aP;
    private GestureDetector gS = new GestureDetector(new g(this));
    private Vector gT = new Vector();

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(PlacesMapActivity placesMapActivity, Drawable drawable) {
        super(boundCenterBottom(drawable));
        this.aP = placesMapActivity;
    }

    public final void a(OverlayItem overlayItem) {
        this.gT.add(overlayItem);
        populate();
    }

    public final boolean a(MotionEvent motionEvent) {
        this.aP.a(this.aP.fD.getProjection().fromPixels((int) motionEvent.getX(), (int) motionEvent.getY()));
        return false;
    }

    public final void bl() {
        this.gT.removeAllElements();
        populate();
    }

    public final boolean bm() {
        this.aP.fE.zoomIn();
        return false;
    }

    public final boolean bn() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final OverlayItem createItem(int i) {
        return (OverlayItem) this.gT.get(i);
    }

    public final boolean onTouchEvent(MotionEvent motionEvent, MapView mapView) {
        this.gS.onTouchEvent(motionEvent);
        return e.super.onTouchEvent(motionEvent, mapView);
    }

    public final int size() {
        return this.gT.size();
    }
}
