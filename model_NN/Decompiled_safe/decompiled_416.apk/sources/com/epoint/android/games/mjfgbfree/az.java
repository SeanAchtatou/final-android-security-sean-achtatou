package com.epoint.android.games.mjfgbfree;

import android.app.AlertDialog;
import android.content.DialogInterface;

final class az implements DialogInterface.OnDismissListener {
    final /* synthetic */ PreferencesActivity qn;

    az(PreferencesActivity preferencesActivity) {
        this.qn = preferencesActivity;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        boolean z;
        boolean[] f = this.qn.jC.f();
        int i = 0;
        while (true) {
            if (i >= f.length) {
                z = false;
                break;
            } else if (f[i]) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.qn);
            builder.setMessage(String.valueOf(af.aa("記錄清除後將不能復原，你是否要確定")) + "?");
            builder.setPositiveButton(af.aa("是"), new ak(this, f));
            builder.setNegativeButton(af.aa("否"), new aj(this));
            builder.create().show();
        }
    }
}
