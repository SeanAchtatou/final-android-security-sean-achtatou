package com.epoint.android.games.mjfgbfree;

import com.epoint.android.games.mjfgbfree.e.l;
import com.epoint.android.games.mjfgbfree.f.a;

final class n extends Thread {
    private /* synthetic */ bb gV;
    private final /* synthetic */ boolean hA;
    private final /* synthetic */ boolean hB;

    n(bb bbVar, boolean z, boolean z2) {
        this.gV = bbVar;
        this.hA = z;
        this.hB = z2;
    }

    public final void run() {
        try {
            this.gV.uB.cU().lq.put("mAndroid_mapping", bb.tH);
            this.gV.uB.cU().lq.put("rounds", Integer.valueOf(this.gV.cI()));
            if (this.gV.uM == 2) {
                a.a(this.gV.jq, "endless_money", String.valueOf(this.gV.uB.cU().bN()[this.gV.gO.aG].gd));
                a.a(this.gV.jq, "endless_points", String.valueOf(this.gV.uB.cU().bN()[this.gV.gO.aG].ge));
            } else if (this.gV.ho instanceof l) {
                this.gV.uB.cU().lq.put("tournament_detail", ((l) this.gV.ho).cM());
            }
            a.a(this.gV.jq, this.gV.uM, this.gV.uB.cU());
        } finally {
            if (this.hA && this.gV.tT != null) {
                this.gV.tT.a(true, false);
            }
            this.gV.uJ = false;
            if (!this.hB && !this.gV.uv.oR) {
                this.gV.er();
            }
        }
    }
}
