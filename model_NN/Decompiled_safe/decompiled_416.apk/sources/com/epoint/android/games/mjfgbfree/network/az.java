package com.epoint.android.games.mjfgbfree.network;

import android.app.AlertDialog;
import android.view.View;
import com.epoint.android.games.mjfgbfree.af;

final class az implements View.OnClickListener {
    final /* synthetic */ BTConnectionActivity pk;

    az(BTConnectionActivity bTConnectionActivity) {
        this.pk = bTConnectionActivity;
    }

    public final void onClick(View view) {
        if (this.pk.ah == null) {
            this.pk.setTitle(String.valueOf(af.aa("等待其他裝置連線")) + "...");
            this.pk.am.removeAllViews();
            this.pk.h();
            this.pk.ai.setEnabled(false);
            this.pk.aj.setText(af.aa("停止等候"));
            this.pk.ap.setVisibility(8);
            new Thread(this.pk).start();
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this.pk);
        builder.setTitle(String.valueOf(af.aa("停止等候")) + "?");
        builder.setPositiveButton(af.aa("是"), new b(this));
        builder.setNegativeButton(af.aa("否"), new a(this));
        builder.create().show();
    }
}
