package com.epoint.android.games.mjfgbfree.f;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import com.epoint.android.games.mjfgbfree.a.d;
import com.epoint.android.games.mjfgbfree.d.b;
import com.epoint.android.games.mjfgbfree.d.c;
import com.epoint.android.games.mjfgbfree.d.g;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.Vector;

public final class a {
    private static int hY = 999;

    private static SQLiteDatabase a(Context context) {
        return new b(context).getWritableDatabase();
    }

    public static synchronized Bitmap a(Context context, String str, int i) {
        Cursor cursor;
        Bitmap bitmap;
        synchronized (a.class) {
            if (hY > 25) {
                Log.e(">>>>>", "entries removed: " + g(context));
                hY = 0;
            }
            SQLiteDatabase a2 = a(context);
            try {
                Cursor rawQuery = a2.rawQuery("SELECT data FROM image_cache WHERE url='" + str + "'" + " AND ref_width>=" + i, null);
                try {
                    if (rawQuery.moveToNext()) {
                        byte[] blob = rawQuery.getBlob(0);
                        Log.e(">>>>>", "image from cache: " + str);
                        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(blob, 0, blob.length);
                        if (i > 0 && decodeByteArray.getWidth() > i) {
                            decodeByteArray = d.a(decodeByteArray, i);
                        }
                        if (rawQuery != null) {
                            try {
                                rawQuery.close();
                            } catch (Exception e) {
                            }
                        }
                        if (a2 != null) {
                            try {
                                a2.close();
                            } catch (Exception e2) {
                            }
                        }
                        bitmap = decodeByteArray;
                    } else {
                        if (rawQuery != null) {
                            try {
                                rawQuery.close();
                            } catch (Exception e3) {
                            }
                        }
                        if (a2 != null) {
                            try {
                                a2.close();
                            } catch (Exception e4) {
                            }
                        }
                        bitmap = null;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        try {
                            cursor.close();
                        } catch (Exception e5) {
                        }
                    }
                    if (a2 != null) {
                        try {
                            a2.close();
                        } catch (Exception e6) {
                        }
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                cursor = null;
            }
        }
        return bitmap;
    }

    public static com.epoint.android.games.mjfgbfree.d.d a(Context context, g gVar) {
        c cVar;
        Cursor cursor;
        int i = 0;
        while (true) {
            if (i >= gVar.wf.size()) {
                cVar = null;
                break;
            } else if (((c) gVar.wf.elementAt(i)).lJ == gVar.aG) {
                cVar = (c) gVar.wf.elementAt(i);
                break;
            } else {
                i++;
            }
        }
        if (cVar == null) {
            return null;
        }
        SQLiteDatabase a2 = a(context);
        try {
            Cursor rawQuery = a2.rawQuery("SELECT fans_from, fans_to, count, grade_id FROM local_total_fan_stat WHERE ? BETWEEN fans_from AND fans_to", new String[]{String.valueOf(cVar.ge)});
            try {
                if (rawQuery.moveToNext()) {
                    com.epoint.android.games.mjfgbfree.d.d dVar = new com.epoint.android.games.mjfgbfree.d.d();
                    dVar.mm = rawQuery.getInt(0);
                    dVar.mn = rawQuery.getInt(1);
                    dVar.mo = rawQuery.getInt(2);
                    dVar.mp = rawQuery.getInt(3);
                    if (dVar.mp > 0 && dVar.mp <= 10) {
                        int i2 = 0;
                        while (i2 < com.epoint.android.games.mjfgbfree.g.a.od[dVar.mp].length) {
                            if (dVar.mo != com.epoint.android.games.mjfgbfree.g.a.od[dVar.mp][i2]) {
                                i2++;
                            } else if (!MJ16Activity.qM || i2 <= 0) {
                                if (rawQuery != null) {
                                    try {
                                        rawQuery.close();
                                    } catch (Exception e) {
                                    }
                                }
                                if (a2 != null) {
                                    try {
                                        a2.close();
                                    } catch (Exception e2) {
                                    }
                                }
                                return dVar;
                            } else {
                                if (rawQuery != null) {
                                    try {
                                        rawQuery.close();
                                    } catch (Exception e3) {
                                    }
                                }
                                if (a2 != null) {
                                    try {
                                        a2.close();
                                    } catch (Exception e4) {
                                    }
                                }
                                return null;
                            }
                        }
                    }
                }
                if (rawQuery != null) {
                    try {
                        rawQuery.close();
                    } catch (Exception e5) {
                    }
                }
                if (a2 != null) {
                    try {
                        a2.close();
                    } catch (Exception e6) {
                    }
                }
                return null;
            } catch (Throwable th) {
                Throwable th2 = th;
                cursor = rawQuery;
                th = th2;
                if (cursor != null) {
                    try {
                        cursor.close();
                    } catch (Exception e7) {
                    }
                }
                if (a2 != null) {
                    try {
                        a2.close();
                    } catch (Exception e8) {
                    }
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    public static void a(Context context, int i, int i2) {
        SQLiteDatabase a2 = a(context);
        SQLiteStatement compileStatement = a2.compileStatement("INSERT INTO item_v2 (item_id, count) VALUES(?,?)");
        try {
            compileStatement.bindLong(1, (long) i);
            compileStatement.bindLong(2, (long) i2);
            compileStatement.executeInsert();
        } catch (SQLiteConstraintException e) {
            compileStatement.close();
            compileStatement = a2.compileStatement("UPDATE item_v2 SET count=count+? WHERE item_id = ?");
            compileStatement.bindLong(1, (long) i2);
            compileStatement.bindLong(2, (long) i);
            compileStatement.execute();
        } catch (Throwable th) {
            Throwable th2 = th;
            SQLiteStatement sQLiteStatement = compileStatement;
            Throwable th3 = th2;
            if (sQLiteStatement != null) {
                try {
                    sQLiteStatement.close();
                } catch (Exception e2) {
                }
            }
            if (a2 != null) {
                try {
                    a2.close();
                } catch (Exception e3) {
                }
            }
            throw th3;
        }
        if (compileStatement != null) {
            try {
                compileStatement.close();
            } catch (Exception e4) {
            }
        }
        if (a2 != null) {
            try {
                a2.close();
            } catch (Exception e5) {
            }
        }
    }

    public static void a(Context context, b bVar) {
        SQLiteDatabase a2 = a(context);
        SQLiteStatement compileStatement = a2.compileStatement("INSERT INTO local_score (guid, user_name, score, rounds, ranking, timestamp) VALUES (?,?,?,?,?,?)");
        try {
            compileStatement.bindString(1, UUID.randomUUID().toString());
            compileStatement.bindString(2, bVar.im);
            compileStatement.bindLong(3, (long) bVar.gH);
            compileStatement.bindLong(4, (long) bVar.in);
            compileStatement.bindLong(5, (long) bVar.io);
            compileStatement.bindLong(6, new Date().getTime());
            compileStatement.execute();
            compileStatement = a2.compileStatement("DELETE FROM local_score WHERE rounds=? AND guid NOT IN (SELECT guid FROM local_score WHERE rounds=? ORDER BY score DESC LIMIT 30)");
            compileStatement.bindLong(1, (long) bVar.in);
            compileStatement.bindLong(2, (long) bVar.in);
            compileStatement.execute();
            if (compileStatement != null) {
                try {
                    compileStatement.close();
                } catch (Exception e) {
                }
            }
            if (a2 != null) {
                try {
                    a2.close();
                } catch (Exception e2) {
                }
            }
        } catch (Throwable th) {
            Throwable th2 = th;
            SQLiteStatement sQLiteStatement = compileStatement;
            Throwable th3 = th2;
            if (sQLiteStatement != null) {
                try {
                    sQLiteStatement.close();
                } catch (Exception e3) {
                }
            }
            if (a2 != null) {
                try {
                    a2.close();
                } catch (Exception e4) {
                }
            }
            throw th3;
        }
    }

    public static synchronized void a(Context context, boolean z, boolean z2, boolean z3, boolean z4) {
        synchronized (a.class) {
            SQLiteDatabase a2 = a(context);
            if (z) {
                try {
                    a2.execSQL("UPDATE local_fan_history SET count=0");
                } catch (Throwable th) {
                    if (a2 != null) {
                        try {
                            a2.close();
                        } catch (Exception e) {
                        }
                    }
                    throw th;
                }
            }
            if (z2) {
                a2.execSQL("UPDATE local_total_fan_stat SET count=0");
            }
            if (z3) {
                a2.execSQL("DELETE FROM local_score");
            }
            if (z4) {
                a2.execSQL("DELETE FROM lookup WHERE key='endless_money'");
                a2.execSQL("DELETE FROM lookup WHERE key='endless_points'");
                a2.execSQL("DELETE FROM local_save WHERE type=2");
            }
            if (a2 != null) {
                try {
                    a2.close();
                } catch (Exception e2) {
                }
            }
        }
        return;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x003b A[SYNTHETIC, Splitter:B:19:0x003b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r6, int r7) {
        /*
            r4 = 0
            r3 = 1
            android.database.sqlite.SQLiteDatabase r0 = a(r6)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0032 }
            java.lang.String r2 = "SELECT 1 FROM local_save WHERE type="
            r1.<init>(r2)     // Catch:{ all -> 0x0032 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ all -> 0x0032 }
            java.lang.String r2 = " LIMIT 1"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0032 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0032 }
            r2 = 0
            android.database.Cursor r1 = r0.rawQuery(r1, r2)     // Catch:{ all -> 0x0032 }
            int r2 = r1.getCount()     // Catch:{ all -> 0x0057 }
            if (r2 != r3) goto L_0x003f
            if (r1 == 0) goto L_0x002b
            r1.close()     // Catch:{ Exception -> 0x004b }
        L_0x002b:
            if (r0 == 0) goto L_0x0030
            r0.close()     // Catch:{ Exception -> 0x004d }
        L_0x0030:
            r0 = r3
        L_0x0031:
            return r0
        L_0x0032:
            r1 = move-exception
            r2 = r4
        L_0x0034:
            if (r2 == 0) goto L_0x0039
            r2.close()     // Catch:{ Exception -> 0x004f }
        L_0x0039:
            if (r0 == 0) goto L_0x003e
            r0.close()     // Catch:{ Exception -> 0x0051 }
        L_0x003e:
            throw r1
        L_0x003f:
            if (r1 == 0) goto L_0x0044
            r1.close()     // Catch:{ Exception -> 0x0053 }
        L_0x0044:
            if (r0 == 0) goto L_0x0049
            r0.close()     // Catch:{ Exception -> 0x0055 }
        L_0x0049:
            r0 = 0
            goto L_0x0031
        L_0x004b:
            r1 = move-exception
            goto L_0x002b
        L_0x004d:
            r0 = move-exception
            goto L_0x0030
        L_0x004f:
            r2 = move-exception
            goto L_0x0039
        L_0x0051:
            r0 = move-exception
            goto L_0x003e
        L_0x0053:
            r1 = move-exception
            goto L_0x0044
        L_0x0055:
            r0 = move-exception
            goto L_0x0049
        L_0x0057:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r5
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.f.a.a(android.content.Context, int):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0073 A[SYNTHETIC, Splitter:B:34:0x0073] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0078 A[SYNTHETIC, Splitter:B:37:0x0078] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r7, int r8, java.lang.Object r9) {
        /*
            r5 = 1
            android.database.sqlite.SQLiteDatabase r0 = a(r7)
            r1 = 0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x006d }
            java.lang.String r3 = "DELETE FROM local_save WHERE type="
            r2.<init>(r3)     // Catch:{ all -> 0x006d }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ all -> 0x006d }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x006d }
            r0.execSQL(r2)     // Catch:{ all -> 0x006d }
            if (r9 == 0) goto L_0x0051
            java.lang.String r2 = "INSERT INTO local_save (guid, type, rounds, data, timestamp) VALUES (?,?,0,?,?)"
            android.database.sqlite.SQLiteStatement r1 = r0.compileStatement(r2)     // Catch:{ all -> 0x006d }
            r2 = 1
            java.util.UUID r3 = java.util.UUID.randomUUID()     // Catch:{ all -> 0x0088 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0088 }
            r1.bindString(r2, r3)     // Catch:{ all -> 0x0088 }
            r2 = 2
            long r3 = (long) r8     // Catch:{ all -> 0x0088 }
            r1.bindLong(r2, r3)     // Catch:{ all -> 0x0088 }
            r2 = 3
            byte[] r3 = com.epoint.android.games.mjfgbfree.a.d.c(r9)     // Catch:{ Exception -> 0x005d }
            java.lang.String r3 = com.epoint.android.games.mjfgbfree.a.e.b(r3)     // Catch:{ Exception -> 0x005d }
            java.lang.String r3 = com.epoint.android.games.mjfgbfree.a.h.bh(r3)     // Catch:{ Exception -> 0x005d }
            r1.bindString(r2, r3)     // Catch:{ Exception -> 0x005d }
            r2 = 4
            java.util.Date r3 = new java.util.Date     // Catch:{ all -> 0x0088 }
            r3.<init>()     // Catch:{ all -> 0x0088 }
            long r3 = r3.getTime()     // Catch:{ all -> 0x0088 }
            r1.bindLong(r2, r3)     // Catch:{ all -> 0x0088 }
            r1.execute()     // Catch:{ all -> 0x0088 }
        L_0x0051:
            if (r1 == 0) goto L_0x0056
            r1.close()     // Catch:{ Exception -> 0x0084 }
        L_0x0056:
            if (r0 == 0) goto L_0x005b
            r0.close()     // Catch:{ Exception -> 0x0086 }
        L_0x005b:
            r0 = r5
        L_0x005c:
            return r0
        L_0x005d:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x0088 }
            if (r1 == 0) goto L_0x0066
            r1.close()     // Catch:{ Exception -> 0x007c }
        L_0x0066:
            if (r0 == 0) goto L_0x006b
            r0.close()     // Catch:{ Exception -> 0x007e }
        L_0x006b:
            r0 = 0
            goto L_0x005c
        L_0x006d:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x0071:
            if (r2 == 0) goto L_0x0076
            r2.close()     // Catch:{ Exception -> 0x0080 }
        L_0x0076:
            if (r0 == 0) goto L_0x007b
            r0.close()     // Catch:{ Exception -> 0x0082 }
        L_0x007b:
            throw r1
        L_0x007c:
            r1 = move-exception
            goto L_0x0066
        L_0x007e:
            r0 = move-exception
            goto L_0x006b
        L_0x0080:
            r2 = move-exception
            goto L_0x0076
        L_0x0082:
            r0 = move-exception
            goto L_0x007b
        L_0x0084:
            r1 = move-exception
            goto L_0x0056
        L_0x0086:
            r0 = move-exception
            goto L_0x005b
        L_0x0088:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x0071
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.f.a.a(android.content.Context, int, java.lang.Object):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x003b A[SYNTHETIC, Splitter:B:19:0x003b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r6, java.lang.String r7) {
        /*
            r4 = 0
            r3 = 1
            android.database.sqlite.SQLiteDatabase r0 = a(r6)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0032 }
            java.lang.String r2 = "SELECT name FROM sqlite_master WHERE type='table' AND name='"
            r1.<init>(r2)     // Catch:{ all -> 0x0032 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ all -> 0x0032 }
            java.lang.String r2 = "'"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0032 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0032 }
            r2 = 0
            android.database.Cursor r1 = r0.rawQuery(r1, r2)     // Catch:{ all -> 0x0032 }
            int r2 = r1.getCount()     // Catch:{ all -> 0x0057 }
            if (r2 != r3) goto L_0x003f
            if (r1 == 0) goto L_0x002b
            r1.close()     // Catch:{ Exception -> 0x004b }
        L_0x002b:
            if (r0 == 0) goto L_0x0030
            r0.close()     // Catch:{ Exception -> 0x004d }
        L_0x0030:
            r0 = r3
        L_0x0031:
            return r0
        L_0x0032:
            r1 = move-exception
            r2 = r4
        L_0x0034:
            if (r2 == 0) goto L_0x0039
            r2.close()     // Catch:{ Exception -> 0x004f }
        L_0x0039:
            if (r0 == 0) goto L_0x003e
            r0.close()     // Catch:{ Exception -> 0x0051 }
        L_0x003e:
            throw r1
        L_0x003f:
            if (r1 == 0) goto L_0x0044
            r1.close()     // Catch:{ Exception -> 0x0053 }
        L_0x0044:
            if (r0 == 0) goto L_0x0049
            r0.close()     // Catch:{ Exception -> 0x0055 }
        L_0x0049:
            r0 = 0
            goto L_0x0031
        L_0x004b:
            r1 = move-exception
            goto L_0x002b
        L_0x004d:
            r0 = move-exception
            goto L_0x0030
        L_0x004f:
            r2 = move-exception
            goto L_0x0039
        L_0x0051:
            r0 = move-exception
            goto L_0x003e
        L_0x0053:
            r1 = move-exception
            goto L_0x0044
        L_0x0055:
            r0 = move-exception
            goto L_0x0049
        L_0x0057:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r5
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.f.a.a(android.content.Context, java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00cd A[SYNTHETIC, Splitter:B:40:0x00cd] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean a(android.content.Context r10, java.lang.String r11, android.graphics.Bitmap r12, int r13) {
        /*
            r8 = 1
            r7 = 0
            java.lang.Class<com.epoint.android.games.mjfgbfree.f.a> r0 = com.epoint.android.games.mjfgbfree.f.a.class
            monitor-enter(r0)
            android.database.sqlite.SQLiteDatabase r1 = a(r10)     // Catch:{ all -> 0x00d1 }
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c2 }
            java.lang.String r4 = "DELETE FROM image_cache WHERE url='"
            r3.<init>(r4)     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r3 = r3.append(r11)     // Catch:{ all -> 0x00c2 }
            java.lang.String r4 = "'"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00c2 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00c2 }
            r1.execSQL(r3)     // Catch:{ all -> 0x00c2 }
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x00c2 }
            r3.<init>()     // Catch:{ all -> 0x00c2 }
            java.lang.String r4 = ".jpg"
            boolean r4 = r11.endsWith(r4)     // Catch:{ all -> 0x00c2 }
            if (r4 != 0) goto L_0x0037
            java.lang.String r4 = ".jpeg"
            boolean r4 = r11.endsWith(r4)     // Catch:{ all -> 0x00c2 }
            if (r4 == 0) goto L_0x00b9
        L_0x0037:
            android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ all -> 0x00c2 }
            r5 = 75
            r12.compress(r4, r5, r3)     // Catch:{ all -> 0x00c2 }
        L_0x003e:
            int r4 = r3.size()     // Catch:{ all -> 0x00c2 }
            r5 = 20480(0x5000, float:2.8699E-41)
            if (r4 > r5) goto L_0x00db
            java.lang.String r4 = "store image >>>"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c2 }
            java.lang.String r6 = "("
            r5.<init>(r6)     // Catch:{ all -> 0x00c2 }
            int r6 = r3.size()     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x00c2 }
            java.lang.String r6 = ") "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r5 = r5.append(r11)     // Catch:{ all -> 0x00c2 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x00c2 }
            android.util.Log.e(r4, r5)     // Catch:{ all -> 0x00c2 }
            java.lang.String r4 = "INSERT INTO image_cache (url, data, ref_width, timestamp, size) VALUES (?,?,?,?,?)"
            android.database.sqlite.SQLiteStatement r2 = r1.compileStatement(r4)     // Catch:{ all -> 0x00c2 }
            r4 = 1
            r2.bindString(r4, r11)     // Catch:{ all -> 0x010e }
            r4 = 2
            byte[] r5 = r3.toByteArray()     // Catch:{ all -> 0x010e }
            r2.bindBlob(r4, r5)     // Catch:{ all -> 0x010e }
            r4 = 3
            if (r13 <= 0) goto L_0x00d4
            r5 = r13
        L_0x007e:
            long r5 = (long) r5     // Catch:{ all -> 0x010e }
            r2.bindLong(r4, r5)     // Catch:{ all -> 0x010e }
            r4 = 4
            java.util.Calendar r5 = java.util.Calendar.getInstance()     // Catch:{ all -> 0x010e }
            java.util.Date r5 = r5.getTime()     // Catch:{ all -> 0x010e }
            long r5 = r5.getTime()     // Catch:{ all -> 0x010e }
            r2.bindLong(r4, r5)     // Catch:{ all -> 0x010e }
            r4 = 5
            int r3 = r3.size()     // Catch:{ all -> 0x010e }
            long r5 = (long) r3     // Catch:{ all -> 0x010e }
            r2.bindLong(r4, r5)     // Catch:{ all -> 0x010e }
            int r3 = com.epoint.android.games.mjfgbfree.f.a.hY     // Catch:{ all -> 0x010e }
            int r3 = r3 + 1
            com.epoint.android.games.mjfgbfree.f.a.hY = r3     // Catch:{ all -> 0x010e }
            long r3 = r2.executeInsert()     // Catch:{ all -> 0x010e }
            r5 = 1
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 != 0) goto L_0x00d9
            r3 = r8
        L_0x00ac:
            if (r2 == 0) goto L_0x00b1
            r2.close()     // Catch:{ Exception -> 0x0104 }
        L_0x00b1:
            if (r1 == 0) goto L_0x00b6
            r1.close()     // Catch:{ Exception -> 0x0106 }
        L_0x00b6:
            r1 = r3
        L_0x00b7:
            monitor-exit(r0)
            return r1
        L_0x00b9:
            android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ all -> 0x00c2 }
            r5 = 100
            r12.compress(r4, r5, r3)     // Catch:{ all -> 0x00c2 }
            goto L_0x003e
        L_0x00c2:
            r3 = move-exception
            r9 = r3
            r3 = r2
            r2 = r9
        L_0x00c6:
            if (r3 == 0) goto L_0x00cb
            r3.close()     // Catch:{ Exception -> 0x010a }
        L_0x00cb:
            if (r1 == 0) goto L_0x00d0
            r1.close()     // Catch:{ Exception -> 0x010c }
        L_0x00d0:
            throw r2     // Catch:{ all -> 0x00d1 }
        L_0x00d1:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x00d4:
            int r5 = r12.getWidth()     // Catch:{ all -> 0x010e }
            goto L_0x007e
        L_0x00d9:
            r3 = r7
            goto L_0x00ac
        L_0x00db:
            java.lang.String r4 = ">>>>>"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c2 }
            java.lang.String r6 = "image too large: ("
            r5.<init>(r6)     // Catch:{ all -> 0x00c2 }
            int r3 = r3.size()     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ all -> 0x00c2 }
            java.lang.String r5 = ")"
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r3 = r3.append(r11)     // Catch:{ all -> 0x00c2 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00c2 }
            android.util.Log.e(r4, r3)     // Catch:{ all -> 0x00c2 }
            if (r1 == 0) goto L_0x0102
            r1.close()     // Catch:{ Exception -> 0x0108 }
        L_0x0102:
            r1 = r7
            goto L_0x00b7
        L_0x0104:
            r2 = move-exception
            goto L_0x00b1
        L_0x0106:
            r1 = move-exception
            goto L_0x00b6
        L_0x0108:
            r1 = move-exception
            goto L_0x0102
        L_0x010a:
            r3 = move-exception
            goto L_0x00cb
        L_0x010c:
            r1 = move-exception
            goto L_0x00d0
        L_0x010e:
            r3 = move-exception
            r9 = r3
            r3 = r2
            r2 = r9
            goto L_0x00c6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.f.a.a(android.content.Context, java.lang.String, android.graphics.Bitmap, int):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x004e A[SYNTHETIC, Splitter:B:25:0x004e] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0053 A[SYNTHETIC, Splitter:B:28:0x0053] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean a(android.content.Context r9, java.lang.String r10, java.lang.String r11) {
        /*
            r7 = 1
            java.lang.Class<com.epoint.android.games.mjfgbfree.f.a> r0 = com.epoint.android.games.mjfgbfree.f.a.class
            monitor-enter(r0)
            android.database.sqlite.SQLiteDatabase r1 = a(r9)     // Catch:{ all -> 0x0057 }
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0048 }
            java.lang.String r4 = "DELETE FROM lookup WHERE key='"
            r3.<init>(r4)     // Catch:{ all -> 0x0048 }
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ all -> 0x0048 }
            java.lang.String r4 = "'"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0048 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0048 }
            r1.execSQL(r3)     // Catch:{ all -> 0x0048 }
            java.lang.String r3 = "INSERT INTO lookup (key, value) VALUES (?,?)"
            android.database.sqlite.SQLiteStatement r2 = r1.compileStatement(r3)     // Catch:{ all -> 0x0048 }
            r3 = 1
            r2.bindString(r3, r10)     // Catch:{ all -> 0x0062 }
            r3 = 2
            r2.bindString(r3, r11)     // Catch:{ all -> 0x0062 }
            long r3 = r2.executeInsert()     // Catch:{ all -> 0x0062 }
            r5 = -1
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 == 0) goto L_0x0046
            r3 = r7
        L_0x003a:
            if (r2 == 0) goto L_0x003f
            r2.close()     // Catch:{ Exception -> 0x005a }
        L_0x003f:
            if (r1 == 0) goto L_0x0044
            r1.close()     // Catch:{ Exception -> 0x005c }
        L_0x0044:
            monitor-exit(r0)
            return r3
        L_0x0046:
            r3 = 0
            goto L_0x003a
        L_0x0048:
            r3 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
        L_0x004c:
            if (r3 == 0) goto L_0x0051
            r3.close()     // Catch:{ Exception -> 0x005e }
        L_0x0051:
            if (r1 == 0) goto L_0x0056
            r1.close()     // Catch:{ Exception -> 0x0060 }
        L_0x0056:
            throw r2     // Catch:{ all -> 0x0057 }
        L_0x0057:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x005a:
            r2 = move-exception
            goto L_0x003f
        L_0x005c:
            r1 = move-exception
            goto L_0x0044
        L_0x005e:
            r3 = move-exception
            goto L_0x0051
        L_0x0060:
            r1 = move-exception
            goto L_0x0056
        L_0x0062:
            r3 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.f.a.a(android.content.Context, java.lang.String, java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x0076 A[SYNTHETIC, Splitter:B:32:0x0076] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x007b A[SYNTHETIC, Splitter:B:35:0x007b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean a(android.content.Context r10, java.lang.String r11, java.lang.String r12, long r13) {
        /*
            r8 = 1
            r7 = 0
            java.lang.Class<com.epoint.android.games.mjfgbfree.f.a> r0 = com.epoint.android.games.mjfgbfree.f.a.class
            monitor-enter(r0)
            android.database.sqlite.SQLiteDatabase r1 = a(r10)     // Catch:{ all -> 0x007f }
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0070 }
            java.lang.String r4 = "DELETE FROM text_cache WHERE url='"
            r3.<init>(r4)     // Catch:{ all -> 0x0070 }
            java.lang.StringBuilder r3 = r3.append(r11)     // Catch:{ all -> 0x0070 }
            java.lang.String r4 = "'"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0070 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0070 }
            r1.execSQL(r3)     // Catch:{ all -> 0x0070 }
            byte[] r3 = r12.getBytes()     // Catch:{ all -> 0x0070 }
            int r3 = r3.length     // Catch:{ all -> 0x0070 }
            r4 = 20480(0x5000, float:2.8699E-41)
            if (r3 > r4) goto L_0x0069
            java.lang.String r4 = "INSERT INTO text_cache (url, data, timestamp, size) VALUES (?,?,?,?)"
            android.database.sqlite.SQLiteStatement r2 = r1.compileStatement(r4)     // Catch:{ all -> 0x0070 }
            r4 = 1
            r2.bindString(r4, r11)     // Catch:{ all -> 0x008c }
            r4 = 2
            r2.bindString(r4, r12)     // Catch:{ all -> 0x008c }
            r4 = 3
            java.util.Calendar r5 = java.util.Calendar.getInstance()     // Catch:{ all -> 0x008c }
            java.util.Date r5 = r5.getTime()     // Catch:{ all -> 0x008c }
            long r5 = r5.getTime()     // Catch:{ all -> 0x008c }
            long r5 = r5 + r13
            r2.bindLong(r4, r5)     // Catch:{ all -> 0x008c }
            r4 = 4
            long r5 = (long) r3     // Catch:{ all -> 0x008c }
            r2.bindLong(r4, r5)     // Catch:{ all -> 0x008c }
            long r3 = r2.executeInsert()     // Catch:{ all -> 0x008c }
            r5 = 1
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 != 0) goto L_0x0067
            r3 = r8
        L_0x005a:
            if (r2 == 0) goto L_0x005f
            r2.close()     // Catch:{ Exception -> 0x0082 }
        L_0x005f:
            if (r1 == 0) goto L_0x0064
            r1.close()     // Catch:{ Exception -> 0x0084 }
        L_0x0064:
            r1 = r3
        L_0x0065:
            monitor-exit(r0)
            return r1
        L_0x0067:
            r3 = r7
            goto L_0x005a
        L_0x0069:
            if (r1 == 0) goto L_0x006e
            r1.close()     // Catch:{ Exception -> 0x0086 }
        L_0x006e:
            r1 = r7
            goto L_0x0065
        L_0x0070:
            r3 = move-exception
            r9 = r3
            r3 = r2
            r2 = r9
        L_0x0074:
            if (r3 == 0) goto L_0x0079
            r3.close()     // Catch:{ Exception -> 0x0088 }
        L_0x0079:
            if (r1 == 0) goto L_0x007e
            r1.close()     // Catch:{ Exception -> 0x008a }
        L_0x007e:
            throw r2     // Catch:{ all -> 0x007f }
        L_0x007f:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x0082:
            r2 = move-exception
            goto L_0x005f
        L_0x0084:
            r1 = move-exception
            goto L_0x0064
        L_0x0086:
            r1 = move-exception
            goto L_0x006e
        L_0x0088:
            r3 = move-exception
            goto L_0x0079
        L_0x008a:
            r1 = move-exception
            goto L_0x007e
        L_0x008c:
            r3 = move-exception
            r9 = r3
            r3 = r2
            r2 = r9
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.f.a.a(android.content.Context, java.lang.String, java.lang.String, long):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x005b A[SYNTHETIC, Splitter:B:32:0x005b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object b(android.content.Context r5, int r6) {
        /*
            r3 = 0
            android.database.sqlite.SQLiteDatabase r0 = a(r5)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0052 }
            java.lang.String r2 = "SELECT data FROM local_save WHERE type="
            r1.<init>(r2)     // Catch:{ all -> 0x0052 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ all -> 0x0052 }
            java.lang.String r2 = " LIMIT 1"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0052 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0052 }
            r2 = 0
            android.database.Cursor r1 = r0.rawQuery(r1, r2)     // Catch:{ all -> 0x0052 }
            boolean r2 = r1.moveToNext()     // Catch:{ all -> 0x006b }
            if (r2 == 0) goto L_0x0046
            r2 = 0
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Exception -> 0x0042 }
            java.lang.String r2 = com.epoint.android.games.mjfgbfree.a.h.bi(r2)     // Catch:{ Exception -> 0x0042 }
            byte[] r2 = com.epoint.android.games.mjfgbfree.a.e.D(r2)     // Catch:{ Exception -> 0x0042 }
            java.lang.Object r2 = com.epoint.android.games.mjfgbfree.a.d.a(r2)     // Catch:{ Exception -> 0x0042 }
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ Exception -> 0x005f }
        L_0x003b:
            if (r0 == 0) goto L_0x0040
            r0.close()     // Catch:{ Exception -> 0x0061 }
        L_0x0040:
            r0 = r2
        L_0x0041:
            return r0
        L_0x0042:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x006b }
        L_0x0046:
            if (r1 == 0) goto L_0x004b
            r1.close()     // Catch:{ Exception -> 0x0067 }
        L_0x004b:
            if (r0 == 0) goto L_0x0050
            r0.close()     // Catch:{ Exception -> 0x0069 }
        L_0x0050:
            r0 = r3
            goto L_0x0041
        L_0x0052:
            r1 = move-exception
            r2 = r3
        L_0x0054:
            if (r2 == 0) goto L_0x0059
            r2.close()     // Catch:{ Exception -> 0x0063 }
        L_0x0059:
            if (r0 == 0) goto L_0x005e
            r0.close()     // Catch:{ Exception -> 0x0065 }
        L_0x005e:
            throw r1
        L_0x005f:
            r1 = move-exception
            goto L_0x003b
        L_0x0061:
            r0 = move-exception
            goto L_0x0040
        L_0x0063:
            r2 = move-exception
            goto L_0x0059
        L_0x0065:
            r0 = move-exception
            goto L_0x005e
        L_0x0067:
            r1 = move-exception
            goto L_0x004b
        L_0x0069:
            r0 = move-exception
            goto L_0x0050
        L_0x006b:
            r2 = move-exception
            r4 = r2
            r2 = r1
            r1 = r4
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.f.a.b(android.content.Context, int):java.lang.Object");
    }

    public static synchronized String b(Context context, String str) {
        Cursor cursor;
        String str2;
        synchronized (a.class) {
            SQLiteDatabase a2 = a(context);
            try {
                a2.execSQL("DELETE FROM text_cache WHERE timestamp<" + Calendar.getInstance().getTimeInMillis());
                Cursor rawQuery = a2.rawQuery("SELECT data FROM text_cache WHERE url='" + str + "'", null);
                try {
                    if (rawQuery.moveToNext()) {
                        String string = rawQuery.getString(0);
                        if (rawQuery != null) {
                            try {
                                rawQuery.close();
                            } catch (Exception e) {
                            }
                        }
                        if (a2 != null) {
                            try {
                                a2.close();
                            } catch (Exception e2) {
                            }
                        }
                        str2 = string;
                    } else {
                        if (rawQuery != null) {
                            try {
                                rawQuery.close();
                            } catch (Exception e3) {
                            }
                        }
                        if (a2 != null) {
                            try {
                                a2.close();
                            } catch (Exception e4) {
                            }
                        }
                        str2 = null;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = rawQuery;
                    th = th2;
                    if (cursor != null) {
                        try {
                            cursor.close();
                        } catch (Exception e5) {
                        }
                    }
                    if (a2 != null) {
                        try {
                            a2.close();
                        } catch (Exception e6) {
                        }
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                cursor = null;
            }
        }
        return str2;
    }

    public static synchronized String b(Context context, String str, String str2) {
        Cursor cursor;
        Throwable th;
        String str3;
        synchronized (a.class) {
            SQLiteDatabase a2 = a(context);
            try {
                Cursor rawQuery = a2.rawQuery("SELECT value FROM lookup WHERE key=?", new String[]{str});
                try {
                    if (rawQuery.moveToNext()) {
                        String string = rawQuery.getString(0);
                        if (rawQuery != null) {
                            try {
                                rawQuery.close();
                            } catch (Exception e) {
                            }
                        }
                        if (a2 != null) {
                            try {
                                a2.close();
                            } catch (Exception e2) {
                            }
                        }
                        str3 = string;
                    } else {
                        if (rawQuery != null) {
                            try {
                                rawQuery.close();
                            } catch (Exception e3) {
                            }
                        }
                        if (a2 != null) {
                            try {
                                a2.close();
                            } catch (Exception e4) {
                            }
                        }
                        str3 = str2;
                    }
                } catch (Throwable th2) {
                    Throwable th3 = th2;
                    cursor = rawQuery;
                    th = th3;
                    if (cursor != null) {
                        try {
                            cursor.close();
                        } catch (Exception e5) {
                        }
                    }
                    if (a2 != null) {
                        try {
                            a2.close();
                        } catch (Exception e6) {
                        }
                    }
                    throw th;
                }
            } catch (Throwable th4) {
                Throwable th5 = th4;
                cursor = null;
                th = th5;
            }
        }
        return str3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0038 A[SYNTHETIC, Splitter:B:20:0x0038] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Vector b(android.content.Context r5) {
        /*
            r4 = 0
            java.util.Vector r0 = new java.util.Vector
            r0.<init>()
            android.database.sqlite.SQLiteDatabase r1 = a(r5)
            java.lang.String r2 = "SELECT DISTINCT rounds FROM local_score ORDER BY rounds DESC"
            r3 = 0
            android.database.Cursor r2 = r1.rawQuery(r2, r3)     // Catch:{ all -> 0x0044 }
        L_0x0011:
            boolean r3 = r2.moveToNext()     // Catch:{ all -> 0x0030 }
            if (r3 != 0) goto L_0x0022
            if (r2 == 0) goto L_0x001c
            r2.close()     // Catch:{ Exception -> 0x0040 }
        L_0x001c:
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ Exception -> 0x0042 }
        L_0x0021:
            return r0
        L_0x0022:
            java.lang.Integer r3 = new java.lang.Integer     // Catch:{ all -> 0x0030 }
            r4 = 0
            int r4 = r2.getInt(r4)     // Catch:{ all -> 0x0030 }
            r3.<init>(r4)     // Catch:{ all -> 0x0030 }
            r0.add(r3)     // Catch:{ all -> 0x0030 }
            goto L_0x0011
        L_0x0030:
            r0 = move-exception
        L_0x0031:
            if (r2 == 0) goto L_0x0036
            r2.close()     // Catch:{ Exception -> 0x003c }
        L_0x0036:
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ Exception -> 0x003e }
        L_0x003b:
            throw r0
        L_0x003c:
            r2 = move-exception
            goto L_0x0036
        L_0x003e:
            r1 = move-exception
            goto L_0x003b
        L_0x0040:
            r2 = move-exception
            goto L_0x001c
        L_0x0042:
            r1 = move-exception
            goto L_0x0021
        L_0x0044:
            r0 = move-exception
            r2 = r4
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.f.a.b(android.content.Context):java.util.Vector");
    }

    /* JADX WARNING: Removed duplicated region for block: B:58:0x012f A[SYNTHETIC, Splitter:B:58:0x012f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Vector b(android.content.Context r10, com.epoint.android.games.mjfgbfree.d.g r11) {
        /*
            r6 = 3
            r4 = 2
            r5 = 0
            r8 = 0
            r1 = r8
        L_0x0005:
            java.util.Vector r0 = r11.wf
            int r0 = r0.size()
            if (r1 < r0) goto L_0x0017
            r1 = r5
        L_0x000e:
            java.util.Vector r2 = new java.util.Vector
            r2.<init>()
            if (r1 != 0) goto L_0x0033
            r0 = r2
        L_0x0016:
            return r0
        L_0x0017:
            java.util.Vector r0 = r11.wf
            java.lang.Object r0 = r0.elementAt(r1)
            com.epoint.android.games.mjfgbfree.d.c r0 = (com.epoint.android.games.mjfgbfree.d.c) r0
            int r0 = r0.lJ
            int r2 = r11.aG
            if (r0 != r2) goto L_0x002f
            java.util.Vector r0 = r11.wf
            java.lang.Object r0 = r0.elementAt(r1)
            com.epoint.android.games.mjfgbfree.d.c r0 = (com.epoint.android.games.mjfgbfree.d.c) r0
            r1 = r0
            goto L_0x000e
        L_0x002f:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0005
        L_0x0033:
            java.lang.String r0 = ""
            java.util.Vector r3 = r11.wf
            int r3 = r3.size()
            if (r3 != r6) goto L_0x0078
            java.lang.String r0 = "'一炮三響'"
        L_0x003f:
            r3 = r8
        L_0x0040:
            java.util.Vector r4 = r1.lN
            int r4 = r4.size()
            if (r3 < r4) goto L_0x0083
            android.database.sqlite.SQLiteDatabase r1 = a(r10)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0126 }
            java.lang.String r4 = "SELECT fan_name, count, fans, ranking_fans, grade_id, idx FROM local_fan_history WHERE fan_name IN ("
            r3.<init>(r4)     // Catch:{ all -> 0x0126 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x0126 }
            java.lang.String r3 = ") ORDER BY fans DESC"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ all -> 0x0126 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0126 }
            r3 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r3)     // Catch:{ all -> 0x0126 }
        L_0x0066:
            boolean r3 = r0.moveToNext()     // Catch:{ all -> 0x013d }
            if (r3 != 0) goto L_0x00cb
            if (r0 == 0) goto L_0x0071
            r0.close()     // Catch:{ Exception -> 0x0137 }
        L_0x0071:
            if (r1 == 0) goto L_0x0076
            r1.close()     // Catch:{ Exception -> 0x013a }
        L_0x0076:
            r0 = r2
            goto L_0x0016
        L_0x0078:
            java.util.Vector r3 = r11.wf
            int r3 = r3.size()
            if (r3 != r4) goto L_0x003f
            java.lang.String r0 = "'一炮雙響'"
            goto L_0x003f
        L_0x0083:
            java.lang.String r4 = ""
            boolean r4 = r0.equals(r4)
            if (r4 != 0) goto L_0x009e
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r4.<init>(r0)
            java.lang.String r0 = ","
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r0 = r0.toString()
        L_0x009e:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r4.<init>(r0)
            java.lang.String r0 = "'"
            java.lang.StringBuilder r4 = r4.append(r0)
            java.util.Vector r0 = r1.lN
            java.lang.Object r0 = r0.elementAt(r3)
            a.a.g r0 = (a.a.g) r0
            java.lang.String r0 = r0.cx()
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r4 = "'"
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            int r3 = r3 + 1
            goto L_0x0040
        L_0x00cb:
            com.epoint.android.games.mjfgbfree.d.i r3 = new com.epoint.android.games.mjfgbfree.d.i     // Catch:{ all -> 0x013d }
            r3.<init>()     // Catch:{ all -> 0x013d }
            r4 = 0
            java.lang.String r4 = r0.getString(r4)     // Catch:{ all -> 0x013d }
            r3.nY = r4     // Catch:{ all -> 0x013d }
            r4 = 1
            int r4 = r0.getInt(r4)     // Catch:{ all -> 0x013d }
            r3.mo = r4     // Catch:{ all -> 0x013d }
            r4 = 2
            int r4 = r0.getInt(r4)     // Catch:{ all -> 0x013d }
            r3.AT = r4     // Catch:{ all -> 0x013d }
            r4 = 3
            int r4 = r0.getInt(r4)     // Catch:{ all -> 0x013d }
            r3.AU = r4     // Catch:{ all -> 0x013d }
            r4 = 4
            int r4 = r0.getInt(r4)     // Catch:{ all -> 0x013d }
            r3.mp = r4     // Catch:{ all -> 0x013d }
            r4 = 5
            int r4 = r0.getInt(r4)     // Catch:{ all -> 0x013d }
            r3.AV = r4     // Catch:{ all -> 0x013d }
            int r4 = r3.mp     // Catch:{ all -> 0x013d }
            if (r4 <= 0) goto L_0x0066
            int r4 = r3.mp     // Catch:{ all -> 0x013d }
            r5 = 10
            if (r4 > r5) goto L_0x0066
            r4 = r8
        L_0x0105:
            int[][] r5 = com.epoint.android.games.mjfgbfree.g.a.oc     // Catch:{ all -> 0x013d }
            int r6 = r3.mp     // Catch:{ all -> 0x013d }
            r5 = r5[r6]     // Catch:{ all -> 0x013d }
            int r5 = r5.length     // Catch:{ all -> 0x013d }
            if (r4 >= r5) goto L_0x0066
            int r5 = r3.mo     // Catch:{ all -> 0x013d }
            int[][] r6 = com.epoint.android.games.mjfgbfree.g.a.oc     // Catch:{ all -> 0x013d }
            int r7 = r3.mp     // Catch:{ all -> 0x013d }
            r6 = r6[r7]     // Catch:{ all -> 0x013d }
            r6 = r6[r4]     // Catch:{ all -> 0x013d }
            if (r5 != r6) goto L_0x0123
            boolean r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.qM     // Catch:{ all -> 0x013d }
            if (r5 == 0) goto L_0x0120
            if (r4 > 0) goto L_0x0123
        L_0x0120:
            r2.add(r3)     // Catch:{ all -> 0x013d }
        L_0x0123:
            int r4 = r4 + 1
            goto L_0x0105
        L_0x0126:
            r0 = move-exception
            r2 = r5
        L_0x0128:
            if (r2 == 0) goto L_0x012d
            r2.close()     // Catch:{ Exception -> 0x0133 }
        L_0x012d:
            if (r1 == 0) goto L_0x0132
            r1.close()     // Catch:{ Exception -> 0x0135 }
        L_0x0132:
            throw r0
        L_0x0133:
            r2 = move-exception
            goto L_0x012d
        L_0x0135:
            r1 = move-exception
            goto L_0x0132
        L_0x0137:
            r0 = move-exception
            goto L_0x0071
        L_0x013a:
            r0 = move-exception
            goto L_0x0076
        L_0x013d:
            r2 = move-exception
            r9 = r2
            r2 = r0
            r0 = r9
            goto L_0x0128
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.f.a.b(android.content.Context, com.epoint.android.games.mjfgbfree.d.g):java.util.Vector");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x004f A[SYNTHETIC, Splitter:B:20:0x004f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Vector c(android.content.Context r5) {
        /*
            r4 = 0
            java.util.Vector r0 = new java.util.Vector
            r0.<init>()
            android.database.sqlite.SQLiteDatabase r1 = a(r5)
            java.lang.String r2 = "SELECT fans_from, fans_to, count, grade_id FROM local_total_fan_stat ORDER BY fans_from DESC"
            r3 = 0
            android.database.Cursor r2 = r1.rawQuery(r2, r3)     // Catch:{ all -> 0x005b }
        L_0x0011:
            boolean r3 = r2.moveToNext()     // Catch:{ all -> 0x0047 }
            if (r3 != 0) goto L_0x0022
            if (r2 == 0) goto L_0x001c
            r2.close()     // Catch:{ Exception -> 0x0057 }
        L_0x001c:
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ Exception -> 0x0059 }
        L_0x0021:
            return r0
        L_0x0022:
            com.epoint.android.games.mjfgbfree.d.d r3 = new com.epoint.android.games.mjfgbfree.d.d     // Catch:{ all -> 0x0047 }
            r3.<init>()     // Catch:{ all -> 0x0047 }
            r4 = 0
            int r4 = r2.getInt(r4)     // Catch:{ all -> 0x0047 }
            r3.mm = r4     // Catch:{ all -> 0x0047 }
            r4 = 1
            int r4 = r2.getInt(r4)     // Catch:{ all -> 0x0047 }
            r3.mn = r4     // Catch:{ all -> 0x0047 }
            r4 = 2
            int r4 = r2.getInt(r4)     // Catch:{ all -> 0x0047 }
            r3.mo = r4     // Catch:{ all -> 0x0047 }
            r4 = 3
            int r4 = r2.getInt(r4)     // Catch:{ all -> 0x0047 }
            r3.mp = r4     // Catch:{ all -> 0x0047 }
            r0.add(r3)     // Catch:{ all -> 0x0047 }
            goto L_0x0011
        L_0x0047:
            r0 = move-exception
        L_0x0048:
            if (r2 == 0) goto L_0x004d
            r2.close()     // Catch:{ Exception -> 0x0053 }
        L_0x004d:
            if (r1 == 0) goto L_0x0052
            r1.close()     // Catch:{ Exception -> 0x0055 }
        L_0x0052:
            throw r0
        L_0x0053:
            r2 = move-exception
            goto L_0x004d
        L_0x0055:
            r1 = move-exception
            goto L_0x0052
        L_0x0057:
            r2 = move-exception
            goto L_0x001c
        L_0x0059:
            r1 = move-exception
            goto L_0x0021
        L_0x005b:
            r0 = move-exception
            r2 = r4
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.f.a.c(android.content.Context):java.util.Vector");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0067 A[SYNTHETIC, Splitter:B:20:0x0067] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Vector c(android.content.Context r7, int r8) {
        /*
            r4 = 0
            java.util.Vector r0 = new java.util.Vector
            r0.<init>()
            android.database.sqlite.SQLiteDatabase r1 = a(r7)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0073 }
            java.lang.String r3 = "SELECT user_name, score, ranking, timestamp FROM local_score WHERE rounds="
            r2.<init>(r3)     // Catch:{ all -> 0x0073 }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ all -> 0x0073 }
            java.lang.String r3 = " ORDER BY score DESC LIMIT 10"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0073 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0073 }
            r3 = 0
            android.database.Cursor r2 = r1.rawQuery(r2, r3)     // Catch:{ all -> 0x0073 }
        L_0x0024:
            boolean r3 = r2.moveToNext()     // Catch:{ all -> 0x005f }
            if (r3 != 0) goto L_0x0035
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ Exception -> 0x006f }
        L_0x002f:
            if (r1 == 0) goto L_0x0034
            r1.close()     // Catch:{ Exception -> 0x0071 }
        L_0x0034:
            return r0
        L_0x0035:
            com.epoint.android.games.mjfgbfree.d.b r3 = new com.epoint.android.games.mjfgbfree.d.b     // Catch:{ all -> 0x005f }
            r3.<init>()     // Catch:{ all -> 0x005f }
            r4 = 0
            java.lang.String r4 = r2.getString(r4)     // Catch:{ all -> 0x005f }
            r3.im = r4     // Catch:{ all -> 0x005f }
            r4 = 1
            int r4 = r2.getInt(r4)     // Catch:{ all -> 0x005f }
            r3.gH = r4     // Catch:{ all -> 0x005f }
            r4 = 2
            int r4 = r2.getInt(r4)     // Catch:{ all -> 0x005f }
            r3.io = r4     // Catch:{ all -> 0x005f }
            java.util.Date r4 = new java.util.Date     // Catch:{ all -> 0x005f }
            r5 = 3
            long r5 = r2.getLong(r5)     // Catch:{ all -> 0x005f }
            r4.<init>(r5)     // Catch:{ all -> 0x005f }
            r3.ip = r4     // Catch:{ all -> 0x005f }
            r0.add(r3)     // Catch:{ all -> 0x005f }
            goto L_0x0024
        L_0x005f:
            r0 = move-exception
        L_0x0060:
            if (r2 == 0) goto L_0x0065
            r2.close()     // Catch:{ Exception -> 0x006b }
        L_0x0065:
            if (r1 == 0) goto L_0x006a
            r1.close()     // Catch:{ Exception -> 0x006d }
        L_0x006a:
            throw r0
        L_0x006b:
            r2 = move-exception
            goto L_0x0065
        L_0x006d:
            r1 = move-exception
            goto L_0x006a
        L_0x006f:
            r2 = move-exception
            goto L_0x002f
        L_0x0071:
            r1 = move-exception
            goto L_0x0034
        L_0x0073:
            r0 = move-exception
            r2 = r4
            goto L_0x0060
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.f.a.c(android.content.Context, int):java.util.Vector");
    }

    public static void c(Context context, g gVar) {
        c cVar;
        SQLiteStatement sQLiteStatement;
        Throwable th;
        Cursor cursor;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= gVar.wf.size()) {
                cVar = null;
                break;
            } else if (((c) gVar.wf.elementAt(i2)).lJ == gVar.aG) {
                cVar = (c) gVar.wf.elementAt(i2);
                break;
            } else {
                i = i2 + 1;
            }
        }
        if (cVar != null) {
            SQLiteDatabase a2 = a(context);
            SQLiteStatement compileStatement = a2.compileStatement("UPDATE local_total_fan_stat SET count=count+1 WHERE ? BETWEEN fans_from AND fans_to");
            try {
                compileStatement.bindLong(1, (long) cVar.ge);
                compileStatement.execute();
                SQLiteStatement compileStatement2 = a2.compileStatement("INSERT INTO local_fan_history (guid, fan_name, count, fans, ranking_fans, grade_id, idx) VALUES (?,?,?,?,?,?,?)");
                try {
                    Vector vector = (Vector) cVar.lN.clone();
                    if (gVar.wf.size() == 3) {
                        vector.add(new a.a.g("一炮三響"));
                    } else if (gVar.wf.size() == 2) {
                        vector.add(new a.a.g("一炮雙響"));
                    }
                    Cursor cursor2 = null;
                    int i3 = 0;
                    while (i3 < vector.size()) {
                        try {
                            a.a.g gVar2 = (a.a.g) vector.elementAt(i3);
                            if (!gVar2.nX) {
                                cursor2 = a2.rawQuery("SELECT count, grade_id, idx FROM local_fan_history WHERE fan_name = ?", new String[]{gVar2.cx()});
                                if (cursor2.moveToNext()) {
                                    long j = cursor2.getLong(0);
                                    long j2 = cursor2.getLong(1);
                                    long j3 = cursor2.getLong(2);
                                    a2.beginTransaction();
                                    a2.execSQL("DELETE FROM local_fan_history WHERE fan_name='" + gVar2.cx() + "'");
                                    compileStatement2.bindString(1, UUID.randomUUID().toString());
                                    compileStatement2.bindString(2, gVar2.cx());
                                    compileStatement2.bindLong(3, j + ((long) gVar2.mo));
                                    compileStatement2.bindLong(4, (long) (gVar2.nV / gVar2.mo));
                                    compileStatement2.bindLong(5, (long) gVar2.nW);
                                    compileStatement2.bindLong(6, j2);
                                    compileStatement2.bindLong(7, j3);
                                    compileStatement2.executeInsert();
                                    a2.setTransactionSuccessful();
                                    a2.endTransaction();
                                }
                                cursor2.close();
                            }
                            i3++;
                            cursor2 = cursor2;
                        } catch (Throwable th2) {
                            th = th2;
                            cursor = cursor2;
                            sQLiteStatement = compileStatement2;
                        }
                    }
                    if (cursor2 != null) {
                        try {
                            cursor2.close();
                        } catch (Exception e) {
                        }
                    }
                    if (compileStatement2 != null) {
                        try {
                            compileStatement2.close();
                        } catch (Exception e2) {
                        }
                    }
                    if (a2 != null) {
                        try {
                            a2.close();
                        } catch (Exception e3) {
                        }
                    }
                } catch (Throwable th3) {
                    th = th3;
                    cursor = null;
                    sQLiteStatement = compileStatement2;
                    if (cursor != null) {
                        try {
                            cursor.close();
                        } catch (Exception e4) {
                        }
                    }
                    if (sQLiteStatement != null) {
                        try {
                            sQLiteStatement.close();
                        } catch (Exception e5) {
                        }
                    }
                    if (a2 != null) {
                        try {
                            a2.close();
                        } catch (Exception e6) {
                        }
                    }
                    throw th;
                }
            } catch (Throwable th4) {
                sQLiteStatement = compileStatement;
                th = th4;
                cursor = null;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x005d A[SYNTHETIC, Splitter:B:20:0x005d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Vector d(android.content.Context r5) {
        /*
            r4 = 0
            java.util.Vector r0 = new java.util.Vector
            r0.<init>()
            android.database.sqlite.SQLiteDatabase r1 = a(r5)
            java.lang.String r2 = "SELECT fan_name, count, fans, ranking_fans, grade_id, idx FROM local_fan_history ORDER BY fans DESC, idx ASC"
            r3 = 0
            android.database.Cursor r2 = r1.rawQuery(r2, r3)     // Catch:{ all -> 0x0069 }
        L_0x0011:
            boolean r3 = r2.moveToNext()     // Catch:{ all -> 0x0055 }
            if (r3 != 0) goto L_0x0022
            if (r2 == 0) goto L_0x001c
            r2.close()     // Catch:{ Exception -> 0x0065 }
        L_0x001c:
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ Exception -> 0x0067 }
        L_0x0021:
            return r0
        L_0x0022:
            com.epoint.android.games.mjfgbfree.d.i r3 = new com.epoint.android.games.mjfgbfree.d.i     // Catch:{ all -> 0x0055 }
            r3.<init>()     // Catch:{ all -> 0x0055 }
            r4 = 0
            java.lang.String r4 = r2.getString(r4)     // Catch:{ all -> 0x0055 }
            r3.nY = r4     // Catch:{ all -> 0x0055 }
            r4 = 1
            int r4 = r2.getInt(r4)     // Catch:{ all -> 0x0055 }
            r3.mo = r4     // Catch:{ all -> 0x0055 }
            r4 = 2
            int r4 = r2.getInt(r4)     // Catch:{ all -> 0x0055 }
            r3.AT = r4     // Catch:{ all -> 0x0055 }
            r4 = 3
            int r4 = r2.getInt(r4)     // Catch:{ all -> 0x0055 }
            r3.AU = r4     // Catch:{ all -> 0x0055 }
            r4 = 4
            int r4 = r2.getInt(r4)     // Catch:{ all -> 0x0055 }
            r3.mp = r4     // Catch:{ all -> 0x0055 }
            r4 = 5
            int r4 = r2.getInt(r4)     // Catch:{ all -> 0x0055 }
            r3.AV = r4     // Catch:{ all -> 0x0055 }
            r0.add(r3)     // Catch:{ all -> 0x0055 }
            goto L_0x0011
        L_0x0055:
            r0 = move-exception
        L_0x0056:
            if (r2 == 0) goto L_0x005b
            r2.close()     // Catch:{ Exception -> 0x0061 }
        L_0x005b:
            if (r1 == 0) goto L_0x0060
            r1.close()     // Catch:{ Exception -> 0x0063 }
        L_0x0060:
            throw r0
        L_0x0061:
            r2 = move-exception
            goto L_0x005b
        L_0x0063:
            r1 = move-exception
            goto L_0x0060
        L_0x0065:
            r2 = move-exception
            goto L_0x001c
        L_0x0067:
            r1 = move-exception
            goto L_0x0021
        L_0x0069:
            r0 = move-exception
            r2 = r4
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.f.a.d(android.content.Context):java.util.Vector");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0040 A[SYNTHETIC, Splitter:B:21:0x0040] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Hashtable e(android.content.Context r5) {
        /*
            r4 = 0
            java.util.Hashtable r0 = new java.util.Hashtable
            r0.<init>()
            android.database.sqlite.SQLiteDatabase r1 = a(r5)
            java.lang.String r2 = "SELECT item_id, count FROM item_v2 ORDER BY item_id"
            r3 = 0
            android.database.Cursor r2 = r1.rawQuery(r2, r3)     // Catch:{ all -> 0x004c }
        L_0x0011:
            boolean r3 = r2.moveToNext()     // Catch:{ all -> 0x0038 }
            if (r3 != 0) goto L_0x0022
            if (r2 == 0) goto L_0x001c
            r2.close()     // Catch:{ Exception -> 0x0048 }
        L_0x001c:
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ Exception -> 0x004a }
        L_0x0021:
            return r0
        L_0x0022:
            r3 = 0
            int r3 = r2.getInt(r3)     // Catch:{ all -> 0x0038 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0038 }
            r4 = 1
            int r4 = r2.getInt(r4)     // Catch:{ all -> 0x0038 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0038 }
            r0.put(r3, r4)     // Catch:{ all -> 0x0038 }
            goto L_0x0011
        L_0x0038:
            r0 = move-exception
        L_0x0039:
            if (r2 == 0) goto L_0x003e
            r2.close()     // Catch:{ Exception -> 0x0044 }
        L_0x003e:
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ Exception -> 0x0046 }
        L_0x0043:
            throw r0
        L_0x0044:
            r2 = move-exception
            goto L_0x003e
        L_0x0046:
            r1 = move-exception
            goto L_0x0043
        L_0x0048:
            r2 = move-exception
            goto L_0x001c
        L_0x004a:
            r1 = move-exception
            goto L_0x0021
        L_0x004c:
            r0 = move-exception
            r2 = r4
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.f.a.e(android.content.Context):java.util.Hashtable");
    }

    public static Integer f(Context context) {
        Cursor cursor;
        Throwable th;
        SQLiteDatabase a2 = a(context);
        try {
            Cursor rawQuery = a2.rawQuery("SELECT count FROM item_v2 WHERE item_id=?", new String[]{String.valueOf(0)});
            try {
                if (rawQuery.moveToNext()) {
                    Integer valueOf = Integer.valueOf(rawQuery.getInt(0));
                    if (rawQuery != null) {
                        try {
                            rawQuery.close();
                        } catch (Exception e) {
                        }
                    }
                    if (a2 != null) {
                        try {
                            a2.close();
                        } catch (Exception e2) {
                        }
                    }
                    return valueOf;
                }
                if (rawQuery != null) {
                    try {
                        rawQuery.close();
                    } catch (Exception e3) {
                    }
                }
                if (a2 != null) {
                    try {
                        a2.close();
                    } catch (Exception e4) {
                    }
                }
                return -1;
            } catch (Throwable th2) {
                Throwable th3 = th2;
                cursor = rawQuery;
                th = th3;
                if (cursor != null) {
                    try {
                        cursor.close();
                    } catch (Exception e5) {
                    }
                }
                if (a2 != null) {
                    try {
                        a2.close();
                    } catch (Exception e6) {
                    }
                }
                throw th;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            cursor = null;
            th = th5;
        }
    }

    private static int g(Context context) {
        Cursor cursor;
        Cursor cursor2;
        SQLiteDatabase a2 = a(context);
        try {
            Cursor rawQuery = a2.rawQuery("SELECT COUNT(*) FROM image_cache", null);
            try {
                if (rawQuery.moveToNext()) {
                    int i = rawQuery.getInt(0);
                    Log.e(">>>", "cache size before cleanupimage_cache: " + i);
                    int i2 = i - 250;
                    if (i2 > 0) {
                        rawQuery.close();
                        rawQuery = a2.rawQuery("SELECT timestamp FROM image_cache ORDER BY timestamp ASC LIMIT " + (i2 + 125), null);
                        if (rawQuery.moveToLast()) {
                            long j = rawQuery.getLong(0);
                            rawQuery.close();
                            int delete = a2.delete("image_cache", "timestamp<?", new String[]{String.valueOf(j)});
                            if (a2 != null) {
                                try {
                                    a2.close();
                                } catch (Exception e) {
                                }
                            }
                            return delete;
                        }
                    }
                }
                if (rawQuery != null) {
                    try {
                        rawQuery.close();
                    } catch (Exception e2) {
                    }
                }
                if (a2 != null) {
                    try {
                        a2.close();
                    } catch (Exception e3) {
                    }
                }
                return 0;
            } catch (Throwable th) {
                Throwable th2 = th;
                cursor = cursor2;
                th = th2;
                if (cursor != null) {
                    try {
                        cursor.close();
                    } catch (Exception e4) {
                    }
                }
                if (a2 != null) {
                    try {
                        a2.close();
                    } catch (Exception e5) {
                    }
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }
}
