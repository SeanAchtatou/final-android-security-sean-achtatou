package com.epoint.android.games.mjfgbfree.network;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import b.a.b;
import b.a.f;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import com.epoint.android.games.mjfgbfree.MJ16ViewActivity;
import com.epoint.android.games.mjfgbfree.a.d;
import com.epoint.android.games.mjfgbfree.af;
import com.epoint.android.games.mjfgbfree.ai;
import com.epoint.android.games.mjfgbfree.f.a;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Vector;

public class TCPLocalHostConnectionActivity extends Activity implements t, Runnable {
    /* access modifiers changed from: private */
    public int ac;
    /* access modifiers changed from: private */
    public Vector ad;
    private Vector af;
    private Button ak;
    /* access modifiers changed from: private */
    public ScrollView an;
    /* access modifiers changed from: private */
    public ImageView ap;
    private Handler handler;
    /* access modifiers changed from: private */
    public Button ke;
    /* access modifiers changed from: private */
    public Button kf;
    /* access modifiers changed from: private */
    public Button kg;
    /* access modifiers changed from: private */
    public ImageButton kh;
    /* access modifiers changed from: private */
    public LinearLayout ki;
    /* access modifiers changed from: private */
    public AlertDialog kk;
    /* access modifiers changed from: private */
    public Vector kl;
    /* access modifiers changed from: private */
    public Vector km;
    /* access modifiers changed from: private */
    public String xS = "";
    /* access modifiers changed from: private */
    public ServerSocket xT;
    /* access modifiers changed from: private */
    public TextView xU;

    public TCPLocalHostConnectionActivity() {
        this.ac = MJ16Activity.qL ? 1 : 3;
        this.ad = new Vector();
        this.kl = new Vector();
        this.km = new Vector();
        this.handler = new ap(this);
    }

    /* access modifiers changed from: private */
    public void a(int i, Object obj) {
        Message message = new Message();
        message.what = i;
        message.obj = obj;
        this.handler.sendMessage(message);
    }

    static /* synthetic */ void a(TCPLocalHostConnectionActivity tCPLocalHostConnectionActivity, LinearLayout linearLayout) {
        linearLayout.removeAllViews();
        for (int i = 0; i < tCPLocalHostConnectionActivity.kl.size(); i++) {
            as asVar = new as(tCPLocalHostConnectionActivity);
            LinearLayout linearLayout2 = (LinearLayout) LayoutInflater.from(tCPLocalHostConnectionActivity).inflate((int) C0000R.layout.ip_list_element, (ViewGroup) null);
            LinearLayout linearLayout3 = (LinearLayout) linearLayout2.findViewById(C0000R.id.address_item);
            TextView textView = (TextView) linearLayout2.findViewById(C0000R.id.text);
            TextView textView2 = (TextView) linearLayout2.findViewById(C0000R.id.alias);
            String[] split = ((String) tCPLocalHostConnectionActivity.kl.elementAt(i)).split(";");
            textView.setText(split[0]);
            linearLayout3.setBackgroundResource(17301602);
            linearLayout3.setClickable(true);
            linearLayout3.setFocusable(true);
            linearLayout3.setOnClickListener(asVar);
            Button button = (Button) linearLayout2.findViewById(C0000R.id.remove);
            if (!b(tCPLocalHostConnectionActivity.km, textView.getText().toString())) {
                button.setVisibility(8);
                textView2.setVisibility(8);
            } else {
                if (split.length > 1) {
                    textView2.setText(split[1]);
                    float textSize = textView.getTextSize();
                    textView.setTextSize(0, textView2.getTextSize());
                    textView2.setTextSize(0, textSize);
                } else {
                    textView2.setVisibility(8);
                }
                linearLayout3.setTag(Integer.valueOf(i));
                linearLayout3.setLongClickable(true);
                linearLayout3.setOnLongClickListener(new av(tCPLocalHostConnectionActivity, linearLayout));
                button.setTag(tCPLocalHostConnectionActivity.kl.elementAt(i));
                button.setOnClickListener(new y(tCPLocalHostConnectionActivity, linearLayout));
            }
            linearLayout.addView(linearLayout2);
            if (i < tCPLocalHostConnectionActivity.kl.size() - 1) {
                LinearLayout linearLayout4 = new LinearLayout(tCPLocalHostConnectionActivity);
                linearLayout4.setMinimumHeight(1);
                linearLayout4.setBackgroundColor(-3355444);
                linearLayout.addView(linearLayout4);
            }
        }
    }

    static /* synthetic */ void a(TCPLocalHostConnectionActivity tCPLocalHostConnectionActivity, boolean z) {
        if (tCPLocalHostConnectionActivity.xT != null) {
            String bVar = b("command", "start_game").toString();
            for (int i = 0; i < tCPLocalHostConnectionActivity.ad.size(); i++) {
                ((aa) tCPLocalHostConnectionActivity.ad.elementAt(i)).a(null);
                ((aa) tCPLocalHostConnectionActivity.ad.elementAt(i)).d(bVar);
            }
            MJ16ViewActivity.ad = tCPLocalHostConnectionActivity.ad;
            tCPLocalHostConnectionActivity.ad = null;
            Intent intent = new Intent();
            intent.putExtra("is_restore_game", z);
            tCPLocalHostConnectionActivity.setResult(-1, intent);
            tCPLocalHostConnectionActivity.finish();
            return;
        }
        ((aa) tCPLocalHostConnectionActivity.ad.elementAt(0)).d(b("command", "quit").toString());
        tCPLocalHostConnectionActivity.a((aa) tCPLocalHostConnectionActivity.ad.elementAt(0));
    }

    private void a(boolean z, String str) {
        TextView textView = new TextView(this);
        textView.setTextColor(-1);
        textView.setText(String.valueOf(z ? af.aa("主持") : af.aa("賓客")) + ": " + str);
        textView.setBackgroundResource(17301602);
        textView.setTextSize(20.0f);
        textView.setPadding(4, 10, 4, 10);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setMinimumHeight(1);
        linearLayout.setBackgroundColor(-12303292);
        this.ki.addView(textView);
        this.ki.addView(linearLayout);
        this.an.post(new z(this));
    }

    private static b b(String str, String str2) {
        b bVar = new b();
        try {
            bVar.a(str, str2);
        } catch (f e) {
            e.printStackTrace();
        }
        return bVar;
    }

    private static boolean b(Vector vector, String str) {
        for (int i = 0; i < vector.size(); i++) {
            String str2 = (String) vector.elementAt(i);
            if (str2.equals(str) || str2.startsWith(String.valueOf(str) + ";")) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void f(Vector vector) {
        String str = "";
        for (int i = 0; i < vector.size(); i++) {
            if (i > 0) {
                str = String.valueOf(str) + ",";
            }
            str = String.valueOf(str) + ((String) vector.elementAt(i));
        }
        a.a(this, "local_host_ip_str", str);
    }

    /* access modifiers changed from: private */
    public boolean fr() {
        setTitle(af.aa("等待其他裝置連線"));
        this.kg.setEnabled(false);
        this.kf.setEnabled(false);
        this.ke.setText(af.aa("停止等候"));
        this.ap.setVisibility(8);
        try {
            String charSequence = this.kg.getText().toString();
            this.xT = new ServerSocket(7889, this.ac, InetAddress.getByName(charSequence));
            h();
            if (!b(this.kl, charSequence) && !b(this.km, charSequence)) {
                this.km.add(charSequence);
                this.kl.add(charSequence);
                f(this.km);
            }
            new Thread(this).start();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            a(4, af.aa("錯誤的IP地址"));
            a(3, (Object) null);
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(int, java.lang.Object):void
      com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity, android.app.AlertDialog):void
      com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity, android.widget.LinearLayout):void
      com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity, java.lang.String):void
      com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity, java.util.Vector):void
      com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity, boolean):void
      com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.aa, java.lang.String):void
      com.epoint.android.games.mjfgbfree.network.t.a(com.epoint.android.games.mjfgbfree.network.aa, java.lang.String):void
      com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(boolean, java.lang.String):void */
    private void h() {
        this.ki.removeAllViews();
        b b2 = b("host_name", ai.nm);
        a(true, ai.nm);
        int i = 0;
        while (i < this.ad.size()) {
            try {
                String o = ((aa) this.ad.elementAt(i)).o();
                b2.a("client_name" + i, o);
                a(false, o);
                i++;
            } catch (f e) {
                e.printStackTrace();
            }
        }
        String bVar = b2.toString();
        for (int i2 = 0; i2 < this.ad.size(); i2++) {
            ((aa) this.ad.elementAt(i2)).d(bVar);
        }
        i();
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0084  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void i() {
        /*
            r5 = this;
            r4 = 0
            monitor-enter(r5)
            java.util.Vector r0 = r5.ad     // Catch:{ all -> 0x0089 }
            int r0 = r0.size()     // Catch:{ all -> 0x0089 }
            if (r0 <= 0) goto L_0x0066
            java.net.ServerSocket r0 = r5.xT     // Catch:{ all -> 0x0089 }
            if (r0 == 0) goto L_0x0047
            java.util.Vector r0 = r5.af     // Catch:{ all -> 0x0089 }
            int r0 = r0.size()     // Catch:{ all -> 0x0089 }
            java.util.Vector r1 = r5.ad     // Catch:{ all -> 0x0089 }
            int r1 = r1.size()     // Catch:{ all -> 0x0089 }
            if (r0 != r1) goto L_0x0068
            r0 = 1
            r1 = r0
        L_0x001e:
            if (r1 == 0) goto L_0x0029
            r2 = r4
        L_0x0021:
            java.util.Vector r0 = r5.ad     // Catch:{ all -> 0x0089 }
            int r0 = r0.size()     // Catch:{ all -> 0x0089 }
            if (r2 < r0) goto L_0x006a
        L_0x0029:
            r0 = r1
        L_0x002a:
            android.widget.Button r1 = new android.widget.Button     // Catch:{ all -> 0x0089 }
            r1.<init>(r5)     // Catch:{ all -> 0x0089 }
            java.lang.String r2 = "繼續遊戲"
            java.lang.String r2 = com.epoint.android.games.mjfgbfree.af.aa(r2)     // Catch:{ all -> 0x0089 }
            r1.setText(r2)     // Catch:{ all -> 0x0089 }
            if (r0 == 0) goto L_0x0084
            com.epoint.android.games.mjfgbfree.network.w r0 = new com.epoint.android.games.mjfgbfree.network.w     // Catch:{ all -> 0x0089 }
            r0.<init>(r5)     // Catch:{ all -> 0x0089 }
            r1.setOnClickListener(r0)     // Catch:{ all -> 0x0089 }
        L_0x0042:
            android.widget.LinearLayout r0 = r5.ki     // Catch:{ all -> 0x0089 }
            r0.addView(r1)     // Catch:{ all -> 0x0089 }
        L_0x0047:
            android.widget.Button r0 = new android.widget.Button     // Catch:{ all -> 0x0089 }
            r0.<init>(r5)     // Catch:{ all -> 0x0089 }
            java.net.ServerSocket r1 = r5.xT     // Catch:{ all -> 0x0089 }
            if (r1 == 0) goto L_0x008c
            java.lang.String r1 = "開始新遊戲"
            java.lang.String r1 = com.epoint.android.games.mjfgbfree.af.aa(r1)     // Catch:{ all -> 0x0089 }
        L_0x0056:
            r0.setText(r1)     // Catch:{ all -> 0x0089 }
            com.epoint.android.games.mjfgbfree.network.x r1 = new com.epoint.android.games.mjfgbfree.network.x     // Catch:{ all -> 0x0089 }
            r1.<init>(r5)     // Catch:{ all -> 0x0089 }
            r0.setOnClickListener(r1)     // Catch:{ all -> 0x0089 }
            android.widget.LinearLayout r1 = r5.ki     // Catch:{ all -> 0x0089 }
            r1.addView(r0)     // Catch:{ all -> 0x0089 }
        L_0x0066:
            monitor-exit(r5)
            return
        L_0x0068:
            r1 = r4
            goto L_0x001e
        L_0x006a:
            java.util.Vector r3 = r5.af     // Catch:{ all -> 0x0089 }
            java.util.Vector r0 = r5.ad     // Catch:{ all -> 0x0089 }
            java.lang.Object r0 = r0.elementAt(r2)     // Catch:{ all -> 0x0089 }
            com.epoint.android.games.mjfgbfree.network.aa r0 = (com.epoint.android.games.mjfgbfree.network.aa) r0     // Catch:{ all -> 0x0089 }
            java.lang.String r0 = r0.p()     // Catch:{ all -> 0x0089 }
            boolean r0 = r3.contains(r0)     // Catch:{ all -> 0x0089 }
            if (r0 != 0) goto L_0x0080
            r0 = r4
            goto L_0x002a
        L_0x0080:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0021
        L_0x0084:
            r0 = 0
            r1.setEnabled(r0)     // Catch:{ all -> 0x0089 }
            goto L_0x0042
        L_0x0089:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x008c:
            java.lang.String r1 = "離開"
            java.lang.String r1 = com.epoint.android.games.mjfgbfree.af.aa(r1)     // Catch:{ all -> 0x0089 }
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.i():void");
    }

    /* access modifiers changed from: private */
    public boolean l(String str, String str2) {
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(str, 7889), 5000);
            ab abVar = new ab(socket);
            abVar.b(0);
            this.ad.add(abVar);
            abVar.a(this);
            b b2 = b("join_game", ai.nm);
            try {
                b2.a("password", str2);
                b2.a("game_name", "MJ-GB1.3A");
                b2.a("uuid", MJ16Activity.qK);
            } catch (f e) {
                e.printStackTrace();
            }
            abVar.d(b2.toString());
            setTitle(af.aa("已連線的玩家"));
            this.kg.setEnabled(false);
            this.kf.setEnabled(false);
            this.ke.setEnabled(false);
            this.ap.setVisibility(8);
            if (!b(this.km, str)) {
                this.km.add(str);
                this.kl.add(str);
                f(this.km);
            }
            return true;
        } catch (IOException e2) {
            e2.printStackTrace();
            a(4, af.aa("連線失敗"));
            a(3, (Object) null);
            return false;
        }
    }

    public final synchronized void a(aa aaVar) {
        if (!aaVar.m()) {
            aaVar.l();
            if (this.ad != null) {
                this.ad.remove(aaVar);
                if (this.xT == null) {
                    a(3, (Object) null);
                } else {
                    h();
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(int, java.lang.Object):void
      com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity, android.app.AlertDialog):void
      com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity, android.widget.LinearLayout):void
      com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity, java.lang.String):void
      com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity, java.util.Vector):void
      com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity, boolean):void
      com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.aa, java.lang.String):void
      com.epoint.android.games.mjfgbfree.network.t.a(com.epoint.android.games.mjfgbfree.network.aa, java.lang.String):void
      com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity.a(boolean, java.lang.String):void */
    public final synchronized void a(aa aaVar, String str) {
        try {
            b bVar = new b(str);
            if (!bVar.H("join_game")) {
                if (!bVar.getString("password").equals(this.xU.getText().toString()) || !bVar.getString("game_name").equals("MJ-GB1.3A")) {
                    aaVar.d(b("command", "disconnect").toString());
                    a(aaVar);
                } else {
                    aaVar.e(bVar.getString("join_game"));
                    aaVar.f(bVar.O("uuid"));
                    h();
                }
            } else if (!bVar.H("host_name")) {
                this.ki.removeAllViews();
                a(true, bVar.getString("host_name"));
                for (int i = 0; !bVar.H("client_name" + i); i++) {
                    a(false, bVar.getString("client_name" + i));
                }
                i();
            } else if (!bVar.H("command")) {
                String string = bVar.getString("command");
                if (string.equals("start_game")) {
                    aaVar.a(null);
                    MJ16ViewActivity.ad = this.ad;
                    this.ad = null;
                    setResult(-1);
                    finish();
                } else if (string.equals("disconnect")) {
                    a(aaVar);
                    a(4, af.aa("連線失敗"));
                } else if (string.equals("quit")) {
                    a(aaVar);
                }
            }
        } catch (f e) {
            e.printStackTrace();
        }
        return;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.af = d.h(this);
        if (!MJ16Activity.rd || ai.nt) {
            setRequestedOrientation(ai.nf);
        } else {
            setRequestedOrientation(0);
        }
        requestWindowFeature(5);
        a(3, (Object) null);
        setContentView((int) C0000R.layout.tcp_connection_activity);
        Bitmap c = d.c(this, "images/connect.png");
        this.ap = (ImageView) findViewById(C0000R.id.bg);
        this.ap.setImageBitmap(c);
        this.ak = (Button) findViewById(C0000R.id.help);
        this.ak.setText(af.aa("幫助"));
        this.ak.setOnClickListener(new aq(this));
        this.xU = (TextView) findViewById(C0000R.id.identity);
        this.kg = (Button) findViewById(C0000R.id.ip_address);
        this.ki = (LinearLayout) findViewById(C0000R.id.players);
        Bitmap c2 = d.c(this, "images/clipboard.png");
        this.kh = (ImageButton) findViewById(C0000R.id.clipboard_btn);
        this.kh.setImageBitmap(c2);
        this.kh.setOnClickListener(new an(this));
        this.an = (ScrollView) this.ki.getParent();
        this.ke = (Button) findViewById(C0000R.id.host);
        this.ke.setText(af.aa("主持遊戲"));
        this.ke.setOnClickListener(new ao(this));
        this.kf = (Button) findViewById(C0000R.id.connect);
        this.kf.setText(af.aa("連線"));
        this.kf.setOnClickListener(new at(this));
        ((TextView) findViewById(C0000R.id.ip_label)).setText(String.valueOf(af.aa("IP地址")) + ":");
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                InetAddress nextElement = networkInterfaces.nextElement().getInetAddresses().nextElement();
                if (!nextElement.isLoopbackAddress() && nextElement.getHostAddress().indexOf(":") == -1) {
                    this.kl.add(nextElement.getHostAddress());
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        if (this.kl.size() > 0) {
            String[] split = a.b(this, "local_host_ip_str", "").split(",");
            for (int i = 0; i < split.length; i++) {
                if (!split[i].equals("")) {
                    this.km.add(split[i]);
                    this.kl.add(split[i]);
                }
            }
            this.kg.setText((CharSequence) this.kl.firstElement());
            this.kg.setOnClickListener(new ar(this));
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(af.aa("沒有可用連線"));
        builder.setPositiveButton(af.aa("好"), new au(this));
        builder.setCancelable(false);
        builder.create().show();
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.xT != null && !this.xT.isClosed()) {
            try {
                this.xT.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (this.ad != null) {
            String bVar = b("command", "quit").toString();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.ad.size()) {
                    aa aaVar = (aa) this.ad.elementAt(i2);
                    aaVar.d(bVar);
                    aaVar.l();
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4 && this.ad.size() > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(String.valueOf(af.aa("取消連線")) + "?");
            builder.setPositiveButton(af.aa("是"), new u(this));
            builder.setNegativeButton(af.aa("否"), new v(this));
            builder.create().show();
            return true;
        } else if (i == 84) {
            return true;
        } else {
            return super.onKeyDown(i, keyEvent);
        }
    }

    public void run() {
        while (!this.xT.isClosed()) {
            try {
                Socket accept = this.xT.accept();
                if (this.ad.size() < this.ac) {
                    a(1, accept);
                } else {
                    accept.getOutputStream().write((b("command", "disconnect") + "\r\n").toString().getBytes());
                }
            } catch (IOException e) {
                if (this.ad != null) {
                    String bVar = b("command", "quit").toString();
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= this.ad.size()) {
                            break;
                        }
                        aa aaVar = (aa) this.ad.elementAt(i2);
                        aaVar.d(bVar);
                        aaVar.l();
                        i = i2 + 1;
                    }
                    this.ad.removeAllElements();
                    a(4, af.aa("已取消"));
                    a(3, (Object) null);
                }
            }
        }
        this.xT = null;
    }
}
