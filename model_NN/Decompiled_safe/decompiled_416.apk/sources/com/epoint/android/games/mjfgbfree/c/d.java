package com.epoint.android.games.mjfgbfree.c;

import a.a.b;
import a.a.f;
import a.b.d.a;
import com.epoint.android.games.mjfgbfree.af;
import com.epoint.android.games.mjfgbfree.d.g;
import java.util.Date;

public abstract class d {
    public static int qB = -1;
    protected g gO;
    private b lp;
    protected a qC = null;
    protected boolean qD = false;
    protected int qE;

    public d(int i) {
        this.qE = i;
    }

    private void cV() {
        if (this.qC != null && this.qC.cd() && this.qC.bM().size() > 0) {
            int[] cw = this.qC.bL().cw();
            for (int i = 0; i < this.qC.bN().length; i++) {
                this.qC.bN()[i].gd += cw[i];
            }
            for (int i2 = 0; i2 < this.qC.bM().size(); i2++) {
                a.b.c.a.b bVar = this.qC.bN()[((f) this.qC.bM().elementAt(i2)).ct()];
                bVar.ge = ((f) this.qC.bM().elementAt(i2)).cs() + bVar.ge;
            }
        }
    }

    public int N() {
        if (this.qC == null) {
            return -1;
        }
        this.qC.bP();
        Date date = new Date();
        int bJ = this.qC.bJ();
        cV();
        this.gO = P();
        this.gO.vK = bJ == 7;
        d(bJ);
        if (!this.gO.vU[qB].ga) {
            int i = 300;
            if (bJ != 5) {
                i = 100;
            }
            int time = i - ((int) (new Date().getTime() - date.getTime()));
            if (time > 0) {
                try {
                    Thread.sleep((long) time);
                } catch (InterruptedException e) {
                }
            }
        }
        switch (this.gO.lh) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                a(this.gO.li, this.gO.lh);
            case 0:
                b(this.gO.li, this.gO.lh);
                break;
        }
        return bJ;
    }

    public boolean O() {
        this.qD = false;
        if (this.qC != null) {
            return this.qC.O();
        }
        return false;
    }

    public g P() {
        if (this.qC != null) {
            this.gO = af.a(qB, this.qC);
        }
        return this.gO;
    }

    public boolean S() {
        return false;
    }

    public final void a(int i, int i2) {
        this.lp.a(i, i2);
    }

    public final void a(b bVar) {
        this.lp = bVar;
    }

    public void a(int[] iArr, int[] iArr2) {
        if (this.qC != null) {
            for (int i = 0; i < this.qC.bN().length; i++) {
                this.qC.bN()[i].gd = iArr[i];
                this.qC.bN()[i].ge = iArr2[i];
            }
        }
    }

    public final void b(int i, int i2) {
        this.lp.b(i, i2);
    }

    public final boolean b(int i, String str) {
        if (this.qC != null) {
            boolean b2 = this.qC.b(i, str);
            if (!b2) {
                return b2;
            }
            c("UpdateSnap", (String) null);
            if (i == qB) {
                return b2;
            }
            this.gO = P();
            b(i, 0);
            return b2;
        }
        c("SendCard", str);
        return false;
    }

    public void c(String str, String str2) {
    }

    public final boolean c(int i, String str) {
        boolean z = false;
        if (this.qC != null) {
            z = this.qC.c(i, str);
            if (z) {
                c("UpdateSnap", (String) null);
                if (i != qB) {
                    this.gO = P();
                    a(i, 4);
                    b(i, 4);
                }
            }
        } else {
            c("Gong", str);
        }
        return z;
    }

    public final int cS() {
        return this.qE;
    }

    public final boolean cT() {
        return this.qD;
    }

    public final a cU() {
        return this.qC;
    }

    public final boolean cW() {
        if (this.qC == null) {
            return false;
        }
        this.qC.bK();
        a(new int[]{500, 500, 500, 500}, new int[4]);
        return true;
    }

    public final boolean cX() {
        return this.qC != null;
    }

    public final void ce() {
        if (this.qC != null) {
            this.qC.ce();
        }
    }

    /* access modifiers changed from: protected */
    public void d(int i) {
    }

    public final boolean d(int i, int i2) {
        boolean z = false;
        if (this.qC != null) {
            z = this.qC.d(i, i2);
            if (z) {
                c("UpdateSnap", (String) null);
                if (i != qB) {
                    this.gO = P();
                    a(i, 6);
                    b(i, 6);
                }
            }
        } else {
            c("Shung", String.valueOf(i2));
        }
        return z;
    }

    public boolean e(int i) {
        return false;
    }

    public final boolean t(int i) {
        if (this.qC != null) {
            boolean t = this.qC.t(i);
            if (!t) {
                return t;
            }
            c("UpdateSnap", (String) null);
            if (i == qB) {
                return t;
            }
            this.gO = P();
            b(i, 0);
            return t;
        }
        c("GetCard", (String) null);
        return false;
    }

    public final boolean u(int i) {
        boolean z = false;
        if (this.qC != null) {
            z = this.qC.u(i);
            if (z) {
                c("UpdateSnap", (String) null);
                if (i != qB) {
                    this.gO = P();
                    a(i, 5);
                    b(i, 5);
                }
            }
        } else {
            c("Pong", (String) null);
        }
        return z;
    }

    public final boolean v(int i) {
        boolean z = false;
        if (this.qC != null) {
            z = this.qC.v(i);
            if (z) {
                c("UpdateSnap", (String) null);
                if (i != qB) {
                    this.gO = P();
                    a(i, 7);
                    b(i, 7);
                }
            }
        } else {
            c("Listen", (String) null);
        }
        return z;
    }

    public final boolean w(int i) {
        if (this.qC != null) {
            boolean w = this.qC.w(i);
            if (!w) {
                return w;
            }
            c("UpdateSnap", (String) null);
            if (i == qB) {
                return w;
            }
            this.gO = P();
            this.gO.vK = true;
            b(i, 0);
            return w;
        }
        c("Pass", (String) null);
        return false;
    }

    public final boolean x(int i) {
        boolean z = false;
        if (this.qC != null) {
            z = this.qC.x(i);
            cV();
            if (z) {
                c("UpdateSnap", (String) null);
                if (i != qB) {
                    this.gO = P();
                    a(i, 1);
                    b(i, 1);
                }
            }
        } else {
            c("Wu", (String) null);
        }
        return z;
    }
}
