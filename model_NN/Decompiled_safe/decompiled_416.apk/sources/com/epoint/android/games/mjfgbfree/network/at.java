package com.epoint.android.games.mjfgbfree.network;

import android.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.epoint.android.games.mjfgbfree.af;

final class at implements View.OnClickListener {
    final /* synthetic */ TCPLocalHostConnectionActivity hZ;

    at(TCPLocalHostConnectionActivity tCPLocalHostConnectionActivity) {
        this.hZ = tCPLocalHostConnectionActivity;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.hZ);
        builder.setTitle(String.valueOf(af.aa("遊戲密碼(如有)")) + ":");
        EditText editText = new EditText(this.hZ);
        editText.setSingleLine();
        editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        LinearLayout linearLayout = new LinearLayout(this.hZ);
        linearLayout.setPadding(5, 2, 5, 2);
        linearLayout.addView(editText);
        builder.setView(linearLayout);
        builder.setPositiveButton(af.aa("好"), new ah(this, editText));
        builder.setNegativeButton(af.aa("取消"), new ai(this));
        builder.create().show();
    }
}
