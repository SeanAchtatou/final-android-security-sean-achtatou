package com.epoint.android.games.mjfgbfree;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AboutActivity extends Activity {
    private String u(String str) {
        String str2 = "1.3";
        try {
            str2 = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getAssets().open(str)));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    return "<html><head></head><body><span style='font-size:110%'>" + af.aa("版本") + ": " + str2 + "<br>" + sb.toString() + "</span>" + "</body>" + "</html>";
                }
                sb.append(String.valueOf(readLine) + "\r\n");
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setVolumeControlStream(3);
        View inflate = LayoutInflater.from(this).inflate((int) C0000R.layout.webview_activity, (ViewGroup) null);
        requestWindowFeature(3);
        setContentView(inflate);
        WebView webView = (WebView) inflate.findViewById(C0000R.id.web_view);
        webView.setScrollBarStyle(0);
        webView.setWebViewClient(new aw(this));
        webView.loadDataWithBaseURL("local", u("about" + af.ch() + ".html"), "text/html", "UTF-8", "");
        setTitle(String.valueOf(af.aa("雀友麻雀")) + (MJ16Activity.qL ? " Free" : ""));
        Button button = (Button) findViewById(C0000R.id.ok);
        button.setText(af.aa("好"));
        button.setOnClickListener(new ax(this));
        getWindow().setFeatureDrawableResource(3, C0000R.drawable.icon);
    }
}
