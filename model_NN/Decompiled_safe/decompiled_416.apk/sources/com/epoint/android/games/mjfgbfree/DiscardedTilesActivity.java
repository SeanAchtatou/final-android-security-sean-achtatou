package com.epoint.android.games.mjfgbfree;

import android.app.Activity;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import com.epoint.android.games.mjfgbfree.a.d;
import com.epoint.android.games.mjfgbfree.d.g;
import java.util.Hashtable;

public class DiscardedTilesActivity extends Activity {
    /* access modifiers changed from: private */
    public static g gO;
    /* access modifiers changed from: private */
    public static ae jn;
    private static Handler mHandler = new ay();
    /* access modifiers changed from: private */
    public Paint gN;
    /* access modifiers changed from: private */
    public Hashtable je;
    /* access modifiers changed from: private */
    public int jf;
    /* access modifiers changed from: private */
    public int jg;
    /* access modifiers changed from: private */
    public Rect jh;
    /* access modifiers changed from: private */
    public Rect ji;
    /* access modifiers changed from: private */
    public int jj;
    /* access modifiers changed from: private */
    public int jk;
    /* access modifiers changed from: private */
    public int jl;
    /* access modifiers changed from: private */
    public int jm;
    /* access modifiers changed from: private */
    public int jo;
    /* access modifiers changed from: private */
    public Paint.FontMetrics jp;

    public static synchronized void b(g gVar) {
        synchronized (DiscardedTilesActivity.class) {
            Message message = new Message();
            message.what = 0;
            message.obj = gVar;
            mHandler.sendMessage(message);
        }
    }

    public static ae bv() {
        return jn;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setVolumeControlStream(3);
        if (MJ16Activity.rd) {
            setRequestedOrientation(0);
        } else {
            setRequestedOrientation(ai.nf);
        }
        gO = (g) d.a(getIntent().getByteArrayExtra("gi"));
        this.gN = new Paint();
        this.gN.setAntiAlias(true);
        this.gN.setStyle(Paint.Style.FILL);
        this.gN.setColor(-12303292);
        this.gN.setUnderlineText(true);
        this.gN.setTextAlign(Paint.Align.CENTER);
        this.gN.setTextSize(30.0f * MJ16Activity.rc);
        jn = new ae(this, this);
        setContentView(jn);
    }

    public synchronized void onDestroy() {
        jn = null;
        super.onDestroy();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 1) {
            jn = null;
            finish();
        }
        return super.onTouchEvent(motionEvent);
    }
}
