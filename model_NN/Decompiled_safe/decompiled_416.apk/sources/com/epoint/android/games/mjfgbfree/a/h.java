package com.epoint.android.games.mjfgbfree.a;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public final class h {
    private static final byte[] AW = {122, 100, -68, 112, -71, 114, -85, 7, 88, 94, -22, -41, 79, 75, 100, -103};
    private static Cipher AX;
    private static Cipher AY;

    static {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(AW, "AES");
            Cipher instance = Cipher.getInstance("AES");
            AX = instance;
            instance.init(1, secretKeySpec);
            Cipher instance2 = Cipher.getInstance("AES");
            AY = instance2;
            instance2.init(2, secretKeySpec);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String bh(String str) {
        return e.b(AX.doFinal(str.getBytes("UTF-8")));
    }

    public static String bi(String str) {
        return new String(AY.doFinal(e.D(str)), "UTF-8");
    }
}
