package com.epoint.android.games.mjfgbfree;

import android.preference.Preference;
import com.epoint.android.games.mjfgbfree.a.d;
import com.epoint.android.games.mjfgbfree.ui.entity.IconListPreference;

final class bo implements Preference.OnPreferenceChangeListener {
    private /* synthetic */ PreferencesActivity qn;

    bo(PreferencesActivity preferencesActivity) {
        this.qn = preferencesActivity;
    }

    public final boolean onPreferenceChange(Preference preference, Object obj) {
        ((IconListPreference) preference).c(d.c(this.qn, "images/" + ((String) obj) + ".png"), -1);
        return true;
    }
}
