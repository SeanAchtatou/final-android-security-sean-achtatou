package com.epoint.android.games.mjfgbfree.street;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

final class h implements AdapterView.OnItemClickListener {
    private /* synthetic */ PlacesMapActivity aP;

    h(PlacesMapActivity placesMapActivity) {
        this.aP = placesMapActivity;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.content.Context, com.epoint.android.games.mjfgbfree.street.PlacesMapActivity] */
    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        a aVar = (a) view.getTag();
        Intent intent = new Intent((Context) this.aP, PlaceActivity.class);
        intent.putExtra("place_name", aVar.aM);
        intent.putExtra("address", aVar.aN);
        intent.putExtra("people_count", aVar.aO);
        this.aP.startActivity(intent);
    }
}
