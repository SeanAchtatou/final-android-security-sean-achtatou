package com.epoint.android.games.mjfgbfree.cam;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.Environment;
import android.util.Log;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

final class d implements Camera.PictureCallback {
    private /* synthetic */ CapturePlayerActivity it;

    d(CapturePlayerActivity capturePlayerActivity) {
        this.it = capturePlayerActivity;
    }

    public final void onPictureTaken(byte[] bArr, Camera camera) {
        try {
            camera.stopPreview();
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
            float f = ((float) this.it.kD) / ((float) this.it.vn);
            Bitmap createBitmap = Bitmap.createBitmap(this.it.vn, this.it.kD, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            int width = (int) (f * ((float) decodeByteArray.getWidth()));
            Rect rect = new Rect();
            rect.top = 0;
            rect.left = 0;
            rect.top = (decodeByteArray.getHeight() - width) / 2;
            rect.right = decodeByteArray.getWidth();
            rect.bottom = width + rect.top;
            Rect rect2 = new Rect();
            rect2.top = 0;
            rect2.left = 0;
            rect2.bottom = createBitmap.getHeight();
            rect2.right = createBitmap.getWidth();
            canvas.setDrawFilter(MJ16Activity.rk);
            canvas.drawBitmap(decodeByteArray, rect, rect2, (Paint) null);
            canvas.drawBitmap(this.it.vk, this.it.vl, this.it.vm, (Paint) null);
            Bitmap createBitmap2 = Bitmap.createBitmap(this.it.vk.getWidth(), this.it.vk.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas2 = new Canvas(createBitmap2);
            canvas2.setDrawFilter(MJ16Activity.rk);
            rect.top = 0;
            rect2.left = 0;
            rect2.right = this.it.vk.getWidth();
            rect2.bottom = this.it.vk.getHeight();
            canvas2.drawBitmap(createBitmap, this.it.vm, rect2, (Paint) null);
            for (int i = 0; i < this.it.vk.getWidth(); i++) {
                for (int i2 = 0; i2 < this.it.vk.getHeight(); i2++) {
                    if ((this.it.vk.getPixel(i, i2) & 16777215) == 16777215) {
                        createBitmap2.setPixel(i, i2, 0);
                    }
                }
            }
            FileOutputStream fileOutputStream = new FileOutputStream(Environment.getExternalStorageDirectory() + "/com.epoint.android.games.mj16android/out.png");
            createBitmap2.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            this.it.vj.setVisibility(8);
            this.it.vk = createBitmap2;
            this.it.vo.invalidate();
            Log.d("CameraDemo", "onPictureTaken - wrote bytes: " + bArr.length);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        Log.d("CameraDemo", "onPictureTaken - jpeg");
    }
}
