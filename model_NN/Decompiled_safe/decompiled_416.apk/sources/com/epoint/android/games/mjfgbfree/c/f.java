package com.epoint.android.games.mjfgbfree.c;

import a.a.c;
import a.b.c.a.b;
import android.os.Bundle;
import com.epoint.android.games.mjfgbfree.MJ16ViewActivity;
import com.epoint.android.games.mjfgbfree.ai;
import com.epoint.android.games.mjfgbfree.bb;
import com.epoint.android.games.mjfgbfree.f.a;
import java.util.Hashtable;

public final class f extends d {
    public f(MJ16ViewActivity mJ16ViewActivity, int i, Bundle bundle) {
        super(i);
        Integer num;
        int i2;
        int i3;
        Integer valueOf = Integer.valueOf(ai.nr);
        if (bundle.getBoolean("is_restore", false)) {
            Object b2 = a.b(mJ16ViewActivity, i);
            if (b2 instanceof a.b.d.a) {
                this.qC = (a.b.d.a) b2;
            }
            if (i == 4 && this.qC == null) {
                throw new com.epoint.android.games.mjfgbfree.d.f();
            }
        }
        if (this.qC == null) {
            this.qC = new a.b.d.a(ai.ns);
            this.qD = false;
            for (b bc : this.qC.bN()) {
                bc.bc("");
            }
            if (i == 2) {
                String b3 = a.b(mJ16ViewActivity, "endless_money", null);
                String b4 = a.b(mJ16ViewActivity, "endless_points", null);
                int parseInt = b3 != null ? Integer.parseInt(b3) : 500;
                if (b4 != null) {
                    int parseInt2 = Integer.parseInt(b4);
                    i3 = parseInt;
                    valueOf = 9999999;
                    i2 = parseInt2;
                } else {
                    i3 = parseInt;
                    valueOf = 9999999;
                    i2 = 0;
                }
            } else {
                i2 = 0;
                i3 = 500;
            }
            int[] iArr = {i3, 500, 500, 500};
            int[] iArr2 = new int[4];
            iArr2[0] = i2;
            super.a(iArr, iArr2);
            num = valueOf;
        } else {
            c.mu = (Hashtable) this.qC.lq.get("AllCardHash");
            c.mv = (a.a.a[]) this.qC.lq.get("AllCards");
            bb.tH = (String[]) this.qC.lq.get("mAndroid_mapping");
            this.qD = true;
            num = this.qC.lq.get("rounds") != null ? (Integer) this.qC.lq.get("rounds") : valueOf;
        }
        d.qB = 0;
        mJ16ViewActivity.a(this, num.intValue());
    }
}
