package com.epoint.android.games.mjfgbfree;

import a.a.a;
import com.epoint.android.games.mjfgbfree.c.f;
import java.util.Vector;

final class ah extends Thread {
    private /* synthetic */ bb gV;
    private boolean my = false;

    public ah(bb bbVar) {
        this.gV = bbVar;
        if (bbVar.uB instanceof f) {
            bbVar.uV = false;
            this.my = true;
        }
    }

    public final void run() {
        do {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
        } while (this.gV.uJ);
        bb.uE = true;
        if (!this.my || this.gV.qC == null) {
            this.gV.gO = this.gV.uB.P();
        } else {
            Vector[] vectorArr = new Vector[4];
            Vector[] vectorArr2 = new Vector[4];
            Vector[] vectorArr3 = new Vector[4];
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= vectorArr.length) {
                    break;
                }
                Vector fw = this.gV.qC.bN()[i2].gb().fw();
                vectorArr[i2] = (Vector) fw.clone();
                vectorArr2[i2] = (Vector) this.gV.qC.bN()[i2].gb().fy().clone();
                int i3 = 0;
                while (true) {
                    int i4 = i3;
                    if (i4 >= fw.size()) {
                        break;
                    }
                    int random = (int) (Math.random() * ((double) fw.size()));
                    fw.setElementAt((a) fw.elementAt(random), i4);
                    fw.setElementAt((a) fw.elementAt(i4), random);
                    i3 = i4 + 1;
                }
                int i5 = 0;
                while (true) {
                    int i6 = i5;
                    if (i6 >= vectorArr2[i2].size()) {
                        break;
                    }
                    fw.insertElementAt((a) vectorArr2[i2].elementAt(i6), (int) (Math.random() * ((double) vectorArr[i2].size())));
                    i5 = i6 + 1;
                }
                vectorArr3[i2] = (Vector) fw.clone();
                for (int i7 = 0; i7 < vectorArr3[i2].size(); i7++) {
                    this.gV.qC.bW().addElement(vectorArr3[i2].elementAt(i7));
                }
                fw.removeAllElements();
                this.gV.qC.bN()[i2].gb().fy().removeAllElements();
                i = i2 + 1;
            }
            this.gV.gO = this.gV.uB.P();
            if (!this.gV.uV) {
                this.gV.ef();
                this.gV.el();
                this.gV.er();
                do {
                    try {
                        Thread.sleep((long) (this.gV.uV ? 10 : 100));
                    } catch (InterruptedException e2) {
                    }
                } while (this.gV.uu);
            }
            int bV = this.gV.qC.bV();
            if (!this.gV.uV) {
                this.gV.er();
                do {
                    try {
                        Thread.sleep((long) (this.gV.uV ? 10 : 50));
                    } catch (InterruptedException e3) {
                    }
                } while (this.gV.uu);
                try {
                    Thread.sleep((long) (this.gV.uV ? 10 : 300));
                } catch (InterruptedException e4) {
                }
            }
            int i8 = 0;
            int i9 = bV;
            int i10 = 4;
            while (true) {
                if (i9 == this.gV.qC.bV() && (i8 = i8 + 1) == 4) {
                    i10 = 1;
                }
                if (!this.gV.uV) {
                    do {
                        try {
                            Thread.sleep((long) (this.gV.uV ? 10 : 50));
                        } catch (InterruptedException e5) {
                        }
                    } while (this.gV.uu);
                    try {
                        Thread.sleep((long) (this.gV.uV ? 10 : 100));
                    } catch (InterruptedException e6) {
                    }
                }
                if (this.gV.qC.bN()[i9].gb().fw().size() == 13) {
                    break;
                }
                if (i9 == this.gV.gO.aG) {
                    for (int i11 = 0; i11 < i10; i11++) {
                        this.gV.qC.bN()[i9].gb().fw().addElement(null);
                        this.gV.qC.bW().removeElementAt(this.gV.qC.bW().size() - 1);
                    }
                    this.gV.gO = this.gV.uB.P();
                    if (!this.gV.uV) {
                        this.gV.ep();
                        do {
                            try {
                                Thread.sleep((long) (this.gV.uV ? 10 : 50));
                            } catch (InterruptedException e7) {
                            }
                        } while (this.gV.uu);
                        try {
                            Thread.sleep((long) (this.gV.uV ? 10 : 100));
                        } catch (InterruptedException e8) {
                        }
                    }
                    for (int i12 = 0; i12 < i10; i12++) {
                        this.gV.qC.bN()[i9].gb().fw().removeElementAt(this.gV.qC.bN()[i9].gb().fw().size() - 1);
                    }
                }
                for (int i13 = 0; i13 < i10; i13++) {
                    this.gV.qC.bN()[i9].gb().fw().addElement(vectorArr3[i9].remove(0));
                    if (i9 != this.gV.gO.aG) {
                        this.gV.qC.bW().removeElementAt(this.gV.qC.bW().size() - 1);
                    }
                }
                this.gV.gO = this.gV.uB.P();
                if (!this.gV.uV) {
                    this.gV.ep();
                    this.gV.uI.a(this.gV.uI.U);
                }
                i9 = (i9 + 1) % 4;
            }
            int i14 = 0;
            int i15 = this.gV.gO.lb;
            while (i14 < vectorArr2.length) {
                this.gV.qC.bN()[i15].gb().fw().removeAllElements();
                for (int i16 = 0; i16 < vectorArr[i15].size(); i16++) {
                    this.gV.qC.bN()[i15].gb().fw().addElement(vectorArr[i15].elementAt(i16));
                }
                for (int i17 = 0; i17 < vectorArr2[i15].size(); i17++) {
                    this.gV.qC.bN()[i15].gb().fy().addElement(vectorArr2[i15].elementAt(i17));
                    this.gV.qC.bW().removeElementAt(this.gV.qC.bW().size() - 1);
                }
                if (vectorArr2[i15].size() > 0) {
                    this.gV.gO = this.gV.uB.P();
                    this.gV.ef();
                    this.gV.uI.a(this.gV.uI.Q);
                    this.gV.ui[i15].a(true, false);
                    this.gV.ui[i15].v(af.aa("補花"));
                    this.gV.ui[i15].gx = null;
                    this.gV.er();
                    do {
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e9) {
                        }
                    } while (this.gV.uu);
                    try {
                        Thread.sleep((long) (this.gV.uV ? 300 : 450));
                    } catch (InterruptedException e10) {
                    }
                    this.gV.ui[i15].a(false, false);
                }
                i14++;
                i15 = (i15 + 1) % 4;
            }
            for (int i18 = 0; i18 < vectorArr.length; i18++) {
                vectorArr3[i18] = null;
                vectorArr2[i18] = null;
                vectorArr[i18] = null;
            }
        }
        if (this.gV.uV) {
            this.gV.ep();
            do {
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e11) {
                }
            } while (this.gV.uu);
        }
        this.gV.en();
        bb.uE = false;
    }
}
