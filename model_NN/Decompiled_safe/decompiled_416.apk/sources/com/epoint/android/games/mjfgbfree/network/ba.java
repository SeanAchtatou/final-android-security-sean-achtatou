package com.epoint.android.games.mjfgbfree.network;

import android.content.Intent;
import android.view.View;
import com.epoint.android.games.mjfgbfree.af;

final class ba implements View.OnClickListener {
    private /* synthetic */ BTConnectionActivity pk;

    ba(BTConnectionActivity bTConnectionActivity) {
        this.pk = bTConnectionActivity;
    }

    public final void onClick(View view) {
        int scanMode = this.pk.ag.getScanMode();
        this.pk.al.setChecked(scanMode == 23);
        if (scanMode != 23) {
            Intent intent = new Intent("android.bluetooth.adapter.action.REQUEST_DISCOVERABLE");
            intent.putExtra("android.bluetooth.adapter.extra.DISCOVERABLE_DURATION", 120);
            this.pk.startActivityForResult(intent, 5);
            return;
        }
        this.pk.a(2, af.aa("裝置目前已在可被偵測狀態"));
    }
}
