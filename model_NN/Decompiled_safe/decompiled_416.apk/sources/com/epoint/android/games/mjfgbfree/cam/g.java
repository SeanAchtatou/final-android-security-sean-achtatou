package com.epoint.android.games.mjfgbfree.cam;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

final class g extends View {
    private /* synthetic */ CapturePlayerActivity it;
    private int zE = 0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g(CapturePlayerActivity capturePlayerActivity, Context context) {
        super(context);
        this.it = capturePlayerActivity;
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize(24.0f);
        paint.setColor(-65536);
        paint.setTextAlign(Paint.Align.CENTER);
        this.it.vn = getWidth();
        this.it.kD = getHeight();
        this.it.vl = new Rect();
        Rect rect = this.it.vl;
        this.it.vl.left = 0;
        rect.top = 0;
        this.it.vl.bottom = this.it.vk.getHeight();
        this.it.vl.right = this.it.vk.getWidth();
        float height = ((float) getHeight()) / ((float) this.it.vk.getHeight());
        this.it.vm = new Rect();
        Rect rect2 = this.it.vm;
        this.it.vm.left = 0;
        rect2.top = 0;
        this.it.vm.right = (int) (((float) this.it.vk.getWidth()) * height);
        this.it.vm.bottom = (int) (height * ((float) this.it.vk.getHeight()));
        int width = this.it.vm.width();
        this.it.vm.left = (getWidth() - width) / 2;
        this.it.vm.right = width + this.it.vm.left;
        canvas.drawBitmap(this.it.vk, this.it.vl, this.it.vm, paint);
        super.onDraw(canvas);
    }
}
