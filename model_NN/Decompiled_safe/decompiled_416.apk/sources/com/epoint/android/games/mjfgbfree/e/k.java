package com.epoint.android.games.mjfgbfree.e;

import android.app.Activity;
import com.epoint.android.games.mjfgbfree.bb;
import com.epoint.android.games.mjfgbfree.d.b;
import com.epoint.android.games.mjfgbfree.ui.a.f;
import com.epoint.android.games.mjfgbfree.ui.a.i;

public final class k extends a {
    private int pg;
    /* access modifiers changed from: private */
    public f ph = null;
    /* access modifiers changed from: private */
    public b pi;
    private int pj = -1;

    public k(Activity activity, bb bbVar, int i) {
        super(activity, bbVar);
        this.pg = i;
    }

    public final void a(int i, i iVar) {
        switch (i) {
            case 0:
                if (!this.cu.eb()) {
                    this.cu.ea();
                    return;
                } else if (this.pj == -1) {
                    this.pj = 0;
                    this.cu.er();
                    new m(this).start();
                    return;
                } else {
                    return;
                }
            case 1:
            case 2:
            case 3:
            case 4:
            case 6:
            case 7:
            default:
                return;
            case 5:
                if (!this.cu.eb()) {
                    return;
                }
                if (this.pj == 0) {
                    this.pj = 1;
                    this.cu.er();
                    new n(this).start();
                    return;
                } else if (this.pj == 1) {
                    this.fu.finish();
                    return;
                } else {
                    return;
                }
            case 8:
                if (this.ft) {
                    this.ft = false;
                    this.cu.dZ();
                    return;
                }
                return;
        }
    }

    public final void aR() {
        super.aR();
    }

    public final void aS() {
        super.aS();
    }

    public final int cI() {
        return this.pg;
    }
}
