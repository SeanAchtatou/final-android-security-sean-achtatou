package com.epoint.android.games.mjfgbfree.cam;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.a.d;

public class CapturePlayerActivity extends Activity {
    int kD;
    f vi;
    Button vj;
    Bitmap vk;
    Rect vl;
    Rect vm;
    int vn;
    g vo;
    public Camera.ShutterCallback vp = new b(this);
    public Camera.PictureCallback vq = new a(this);
    public Camera.PictureCallback vr = new d(this);

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRequestedOrientation(0);
        setContentView((int) C0000R.layout.capture_player_layout);
        this.vo = new g(this, this);
        addContentView(this.vo, new ViewGroup.LayoutParams(-1, -1));
        this.vi = new f(this);
        ((FrameLayout) findViewById(C0000R.id.camera_preview)).addView(this.vi);
        this.vj = (Button) findViewById(C0000R.id.capture_button);
        this.vj.setOnClickListener(new c(this));
        this.vk = d.c(this, "mask1");
    }
}
