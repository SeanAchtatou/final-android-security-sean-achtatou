package com.epoint.android.games.mjfgbfree.network;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.epoint.android.games.mjfgbfree.af;
import java.net.Socket;

final class ap extends Handler {
    private /* synthetic */ TCPLocalHostConnectionActivity hZ;

    ap(TCPLocalHostConnectionActivity tCPLocalHostConnectionActivity) {
        this.hZ = tCPLocalHostConnectionActivity;
    }

    public final synchronized void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                if (this.hZ.ad.size() < this.hZ.ac) {
                    ab abVar = new ab((Socket) message.obj);
                    this.hZ.ad.add(abVar);
                    abVar.a(this.hZ);
                    break;
                }
                break;
            case 2:
                this.hZ.setProgressBarIndeterminateVisibility(false);
                if (((String) message.obj).equals("")) {
                    this.hZ.xU.setText("");
                    break;
                } else {
                    this.hZ.xS = (String) message.obj;
                    this.hZ.xU.setText(String.valueOf(af.aa("密碼")) + ": " + ((String) message.obj));
                    this.hZ.xU.setVisibility(0);
                    this.hZ.kh.setVisibility(0);
                    break;
                }
            case 3:
                this.hZ.setProgressBarIndeterminateVisibility(false);
                this.hZ.setTitle(af.aa("網路模式"));
                this.hZ.ki.removeAllViews();
                this.hZ.ap.setVisibility(0);
                this.hZ.xU.setVisibility(8);
                this.hZ.kh.setVisibility(8);
                this.hZ.kg.setEnabled(true);
                this.hZ.kf.setEnabled(true);
                this.hZ.ke.setEnabled(true);
                this.hZ.ke.setText(af.aa("主持遊戲"));
                break;
            case 4:
                Toast.makeText(this.hZ, (String) message.obj, 0).show();
                break;
            case 5:
                this.hZ.setProgressBarIndeterminateVisibility(false);
                this.hZ.kg.setText((String) message.obj);
                break;
        }
    }
}
