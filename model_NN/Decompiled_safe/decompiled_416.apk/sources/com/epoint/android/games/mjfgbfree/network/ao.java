package com.epoint.android.games.mjfgbfree.network;

import android.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.epoint.android.games.mjfgbfree.af;

final class ao implements View.OnClickListener {
    final /* synthetic */ TCPLocalHostConnectionActivity hZ;

    ao(TCPLocalHostConnectionActivity tCPLocalHostConnectionActivity) {
        this.hZ = tCPLocalHostConnectionActivity;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.hZ);
        if (this.hZ.xT == null) {
            builder.setTitle(af.aa("設立遊戲密碼(可留空)"));
            EditText editText = new EditText(this.hZ);
            editText.setSingleLine();
            editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
            LinearLayout linearLayout = new LinearLayout(this.hZ);
            linearLayout.setPadding(5, 2, 5, 2);
            linearLayout.addView(editText);
            builder.setView(linearLayout);
            builder.setPositiveButton(af.aa("好"), new f(this, editText));
            builder.setNegativeButton(af.aa("取消"), new g(this));
        } else {
            builder.setTitle(String.valueOf(af.aa("停止等候")) + "?");
            builder.setPositiveButton(af.aa("是"), new d(this));
            builder.setNegativeButton(af.aa("否"), new e(this));
        }
        builder.create().show();
    }
}
