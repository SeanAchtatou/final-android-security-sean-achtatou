package com.epoint.android.games.mjfgbfree.ui;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PaintDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.PathShape;
import com.epoint.android.games.mjfgbfree.ai;

public final class a {
    private PaintDrawable gA;
    private ShapeDrawable gB;
    private Drawable gC;
    private int gl;
    private int gm;
    private int gn;
    private int go;
    private Paint gp;
    private String gq;
    private int gr;
    private boolean gs;
    private boolean gt;
    private boolean gu;
    private boolean gv;
    private Integer gw;
    public String gx;
    private float[] gy;
    private Path gz;
    private int x;
    private int y;

    public a(int i, int i2, int i3, int i4, int i5, String str) {
        this(i, i2, i3, i4, i5, str, -1, null, null);
    }

    public a(int i, int i2, int i3, int i4, int i5, String str, int i6, Path path, Integer num) {
        this.x = 0;
        this.y = 0;
        this.gu = true;
        this.gv = false;
        this.gw = null;
        this.gx = null;
        this.gy = new float[]{16.0f, 16.0f, 16.0f, 16.0f, 16.0f, 16.0f, 16.0f, 16.0f};
        this.gz = null;
        this.gA = null;
        this.gB = null;
        this.gC = null;
        this.gw = num;
        this.gm = i3;
        this.gl = i4;
        this.gn = i6;
        this.go = i5;
        Rect rect = new Rect(0, 0, i, i2);
        if (path == null) {
            this.gA = new PaintDrawable();
            this.gp = this.gA.getPaint();
            this.gA.setBounds(rect);
            this.gA.setCornerRadii(this.gy);
            this.gC = this.gA;
        } else {
            this.gB = new ShapeDrawable(new PathShape(path, 90.0f, 36.0f));
            this.gp = this.gB.getPaint();
            this.gB.setBounds(rect);
            this.gC = this.gB;
        }
        this.gp.setStrokeWidth(2.0f);
        this.gp.setAntiAlias(true);
        this.gp.setTextSize((float) ((ai.nk == 1 || ai.nk == 4 || ai.nk == 5) ? 23 : 28));
        this.gp.setTextAlign(Paint.Align.CENTER);
        this.gq = str;
        this.gC.setVisible(false, false);
    }

    public a(int i, int i2, String str, int i3, Path path) {
        this(i, 60, -1, -12303292, i2, str, i3, path, null);
    }

    public final void a(boolean z, boolean z2) {
        this.gC.setVisible(z, z2);
    }

    public final void aX() {
        this.gp.setTextSize((float) ((ai.nk == 1 || ai.nk == 4 || ai.nk == 5) ? 23 : 28));
    }

    public final Paint aY() {
        return this.gp;
    }

    public final int aZ() {
        return this.go;
    }

    public final void b(boolean z) {
        this.gs = z;
    }

    public final int ba() {
        return this.gC.getBounds().height();
    }

    public final int bb() {
        return this.gC.getBounds().width();
    }

    public final String bc() {
        return this.gq;
    }

    public final int bd() {
        return this.gm;
    }

    public final boolean be() {
        return this.gt;
    }

    public final boolean bf() {
        return this.gu;
    }

    public final void bg() {
        this.gv = true;
    }

    public final void c(int i, int i2) {
        this.x = i;
        this.y = i2;
        int width = this.gC.getBounds().width();
        int height = this.gC.getBounds().height();
        Rect bounds = this.gC.getBounds();
        bounds.top = i2;
        bounds.left = i;
        bounds.bottom = height + bounds.top;
        bounds.right = width + bounds.left;
        this.gC.setBounds(bounds);
        if (this.gA != null) {
            this.gA.setCornerRadii(this.gy);
        }
    }

    public final void c(boolean z) {
        this.gt = z;
    }

    public final void d(boolean z) {
        this.gu = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public final void draw(Canvas canvas) {
        if (!this.gv) {
            this.gp.setStyle(Paint.Style.FILL);
            if (!this.gu) {
                this.gp.setColor(-12303292);
            } else {
                this.gp.setColor(this.gm);
            }
            if (this.gs || this.gt) {
                this.gp.setAlpha(255);
            } else {
                this.gp.setAlpha(this.go);
            }
            this.gC.draw(canvas);
            if (this.gl != 0) {
                this.gp.setStyle(Paint.Style.STROKE);
                this.gp.setStrokeWidth(1.2f);
                this.gp.setColor(this.gl);
                this.gp.setAlpha(this.go);
                this.gC.draw(canvas);
            }
            if (this.gq != null) {
                this.gp.setStyle(Paint.Style.FILL);
                this.gp.setColor(this.gn);
                this.gp.setAlpha(255);
                this.gr = ((int) (this.gp.getTextSize() / 2.0f)) - 5;
                if (this.gw != null) {
                    this.gp.setShadowLayer(2.0f, 1.0f, 1.0f, this.gw.intValue());
                }
                canvas.drawText(String.valueOf(this.gt ? "✓" : "") + this.gq, (float) (this.gC.getBounds().left + (this.gC.getBounds().width() / 2)), (float) (this.gC.getBounds().top + (this.gC.getBounds().height() / 2) + this.gr + ((ai.nk == 1 || ai.nk == 5 || ai.nk == 4) ? 2 : 1)), this.gp);
                if (this.gw != null) {
                    this.gp.setShadowLayer(0.0f, 0.0f, 0.0f, 0);
                }
            }
        }
    }

    public final Rect getBounds() {
        return this.gC.getBounds();
    }

    public final void i(int i) {
        this.go = i;
    }

    public final boolean isVisible() {
        return this.gC.isVisible();
    }

    public final void j(int i) {
        Rect bounds = this.gC.getBounds();
        bounds.right = bounds.left + i;
        this.gC.setBounds(bounds);
    }

    public final void k(int i) {
        Rect bounds = this.gC.getBounds();
        bounds.bottom = bounds.top + i;
        this.gC.setBounds(bounds);
    }

    public final void l(int i) {
        this.gm = i;
    }

    public final void setBounds(Rect rect) {
        this.gC.setBounds(rect);
        if (this.gA != null) {
            this.gA.setCornerRadii(this.gy);
        }
        this.x = rect.left;
        this.y = rect.top;
    }

    public final void v(String str) {
        this.gq = str;
        aX();
    }
}
