package com.epoint.android.games.mjfgbfree.network;

import android.os.Handler;
import android.os.Message;
import com.bumptech.bumpapi.BumpConnection;
import com.bumptech.bumpapi.as;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public final class ag implements as, aa {
    private String aH;
    private String aI;
    private Handler handler = new br(this);
    private Thread iA;
    /* access modifiers changed from: private */
    public t iC;
    /* access modifiers changed from: private */
    public int iy;
    private ByteArrayOutputStream kc = new ByteArrayOutputStream(4096);
    private volatile boolean lD = false;
    /* access modifiers changed from: private */
    public BumpConnection lE;
    private int lF = 2;

    public ag(BumpConnection bumpConnection) {
        this.lE = bumpConnection;
    }

    public final void a(t tVar) {
        this.iC = tVar;
        this.lE.a(this);
    }

    public final void b(int i) {
    }

    public final void c(byte[] bArr) {
        Message message = new Message();
        message.what = 1;
        int i = 0;
        while (i < bArr.length) {
            try {
                byte b2 = bArr[i];
                if (b2 != 13) {
                    if (b2 == 10) {
                        message.obj = this.kc.toString("UTF-8");
                        this.kc.reset();
                    } else {
                        this.kc.write(b2);
                    }
                }
                i++;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        this.iC.a(this, (String) message.obj);
    }

    public final void cg() {
        new Message().what = 2;
        this.iC.a(this);
    }

    public final synchronized boolean d(String str) {
        boolean z;
        String str2 = String.valueOf(str) + "\r\n";
        try {
            synchronized (this.lE) {
                this.lE.j(str2.getBytes("UTF-8"));
            }
            if (this.iA != null && this.iA.isAlive()) {
                this.iA.interrupt();
            }
            this.iA = new bs(this);
            this.iA.start();
            z = true;
        } catch (IOException e) {
            e.printStackTrace();
            if (this.iA != null && this.iA.isAlive()) {
                this.iA.interrupt();
            }
            z = false;
        }
        return z;
    }

    public final void e(String str) {
        this.aH = str;
    }

    public final void f(String str) {
        this.aI = str;
    }

    public final boolean l() {
        if (this.lD) {
            return false;
        }
        this.lD = true;
        this.lE.disconnect();
        return true;
    }

    public final boolean m() {
        return this.lD;
    }

    public final int n() {
        return this.lF;
    }

    public final String o() {
        return this.aH;
    }

    public final String p() {
        return this.aI;
    }
}
