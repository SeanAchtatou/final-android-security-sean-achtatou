package com.epoint.android.games.mjfgbfree.network;

import android.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.epoint.android.games.mjfgbfree.af;

final class ca implements View.OnClickListener {
    final /* synthetic */ TCPServerConnectionActivity sh;

    ca(TCPServerConnectionActivity tCPServerConnectionActivity) {
        this.sh = tCPServerConnectionActivity;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.sh);
        builder.setTitle(af.aa("請輸入Game Key"));
        EditText editText = new EditText(this.sh);
        editText.setSingleLine();
        editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        LinearLayout linearLayout = new LinearLayout(this.sh);
        linearLayout.setPadding(5, 2, 5, 2);
        linearLayout.addView(editText);
        builder.setView(linearLayout);
        builder.setPositiveButton(af.aa("好"), new o(this, editText));
        builder.setNegativeButton(af.aa("取消"), new n(this));
        builder.create().show();
    }
}
