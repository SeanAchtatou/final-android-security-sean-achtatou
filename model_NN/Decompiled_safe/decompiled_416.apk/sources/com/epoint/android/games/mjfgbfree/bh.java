package com.epoint.android.games.mjfgbfree;

import android.view.View;

final class bh implements View.OnClickListener {
    private /* synthetic */ LocalFanHistoryActivity vh;

    bh(LocalFanHistoryActivity localFanHistoryActivity) {
        this.vh = localFanHistoryActivity;
    }

    public final void onClick(View view) {
        this.vh.B.setClickable(true);
        this.vh.B.setChecked(false);
        this.vh.C.setClickable(true);
        this.vh.C.setChecked(false);
        this.vh.A.setClickable(true);
        this.vh.A.setChecked(false);
        this.vh.D.setClickable(false);
        this.vh.D.setChecked(true);
        LocalFanHistoryActivity.h(this.vh);
    }
}
