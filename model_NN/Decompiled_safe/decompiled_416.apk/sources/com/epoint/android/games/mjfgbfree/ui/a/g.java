package com.epoint.android.games.mjfgbfree.ui.a;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import com.epoint.android.games.mjfgbfree.MJ16ViewActivity;
import com.epoint.android.games.mjfgbfree.af;
import com.epoint.android.games.mjfgbfree.bb;
import com.epoint.android.games.mjfgbfree.ui.a;
import java.util.Vector;

public final class g extends e implements i {
    private Vector gM = new Vector();
    private Thread gR;
    private a mq = new a(100, 60, -65536, -16777216, 160, af.aa("取消"));
    private Vector mr = new Vector();
    private String ms = "";
    private int type = -1;

    public g() {
        this.js = new a(380, 410, -1, Color.rgb(0, 128, 0), 192, null);
    }

    private void ci() {
        int i;
        int i2 = 55;
        int i3 = 0;
        while (true) {
            i = i2;
            if (i3 >= this.gM.size()) {
                break;
            }
            a aVar = (a) this.gM.elementAt(i3);
            if (i3 < this.gM.size() - 1) {
                aVar.c((MJ16Activity.qZ - aVar.bb()) / 2, i);
            } else {
                int i4 = i + 2;
                ((a) this.gM.elementAt(i3)).c((MJ16Activity.qZ - aVar.bb()) / 2, i4);
                i = i4 + 2;
            }
            i2 = aVar.ba() + i + 10;
            i3++;
        }
        Rect bounds = this.js.getBounds();
        bounds.top = 0;
        bounds.bottom = i;
        this.js.setBounds(bounds);
        this.js.c((MJ16Activity.qZ - this.js.bb()) / 2, (bb.ty == 1 ? 0 : bb.dR()) + (((MJ16Activity.ra - bb.dR()) - this.js.ba()) / 2));
        this.ju = new Rect(this.js.getBounds());
        for (int i5 = 0; i5 < this.gM.size(); i5++) {
            a aVar2 = (a) this.gM.elementAt(i5);
            aVar2.c(aVar2.getBounds().left, aVar2.getBounds().top + this.js.getBounds().top);
        }
    }

    public final Object a(int i, int i2, int i3, Object obj) {
        int i4 = 0;
        a aVar = null;
        while (i4 < this.gM.size()) {
            a aVar2 = (a) this.gM.elementAt(i4);
            aVar2.b(false);
            if (aVar2.isVisible() && aVar2.bf()) {
                Rect bounds = aVar2.getBounds();
                if (i >= bounds.left && i <= bounds.right && i2 >= bounds.top && i2 <= bounds.bottom && (i3 == 0 || (i3 != 0 && obj == aVar2))) {
                    aVar2.b(true);
                    i4++;
                    aVar = aVar2;
                }
            }
            aVar2 = aVar;
            i4++;
            aVar = aVar2;
        }
        if (aVar != null) {
            return aVar;
        }
        return null;
    }

    public final void a(Bitmap bitmap, boolean z) {
    }

    public final void a(MJ16ViewActivity mJ16ViewActivity, bb bbVar) {
        this.jq = mJ16ViewActivity;
        this.jr = bbVar;
    }

    public final void a(Object obj) {
        super.a(obj);
        if (obj == this.mq) {
            this.mq.a(false, false);
            super.b(this.gR);
            return;
        }
        for (int i = 0; i < this.gM.size(); i++) {
            a aVar = (a) this.gM.elementAt(i);
            aVar.b(false);
            if (aVar == obj) {
                this.jr.a(this, this.type, aVar.gx);
            }
        }
    }

    public final void a(Thread thread) {
        this.gR = thread;
        a(this.mq);
    }

    public final void b(Canvas canvas) {
        int i;
        int i2;
        ci();
        canvas.drawARGB(64, 0, 0, 0);
        this.js.draw(canvas);
        Paint paint = new Paint();
        paint.setColor(-16777216);
        paint.setAntiAlias(true);
        paint.setTextSize(28.0f);
        paint.setUnderlineText(true);
        paint.setTextAlign(Paint.Align.CENTER);
        canvas.drawText(af.aa("請選擇組合"), (float) (MJ16Activity.qZ / 2), (float) (this.js.getBounds().top + 35), paint);
        paint.setColor(-65536);
        paint.setStyle(Paint.Style.STROKE);
        int i3 = 0;
        int i4 = 0;
        while (i3 < this.gM.size()) {
            a aVar = (a) this.gM.elementAt(i3);
            aVar.a(true, false);
            if (i3 < this.gM.size() - 1) {
                aVar.draw(canvas);
                int bb = ((aVar.bb() - ((((Bitmap) bb.tE.get("bc")).getWidth() + 2) * (this.type == 0 ? 4 : 3))) / 2) + aVar.getBounds().left;
                if (i4 < this.mr.size()) {
                    while (true) {
                        int i5 = bb;
                        i2 = i4;
                        if (((String) this.mr.elementAt(i2)).equals(".")) {
                            break;
                        }
                        Bitmap bitmap = (Bitmap) bb.tE.get(this.mr.elementAt(i2));
                        canvas.drawBitmap(bitmap, (float) i5, (float) (aVar.getBounds().top + ((aVar.ba() - bitmap.getHeight()) / 2)), paint);
                        if (this.type != 0 && ((String) this.mr.elementAt(i2)).equals(this.ms)) {
                            canvas.drawRect((float) i5, (float) (aVar.getBounds().top + ((aVar.ba() - bitmap.getHeight()) / 2)), (float) (bitmap.getWidth() + i5), (float) (aVar.getBounds().top + ((aVar.ba() - bitmap.getHeight()) / 2) + bitmap.getHeight()), paint);
                        }
                        bb = bitmap.getWidth() + 2 + i5;
                        i4 = i2 + 1;
                    }
                    i = i2 + 1;
                }
                i = i4;
            } else {
                aVar.draw(canvas);
                i = i4;
            }
            i3++;
            i4 = i;
        }
    }

    public final void bi() {
        a((Thread) null);
    }

    public final void bk() {
        this.mq.b(false);
        this.jr.a(this, -1, String.valueOf(this.type));
    }

    public final void d(int i, String str) {
        this.type = i;
        this.ms = str;
        this.gM.removeAllElements();
        int parseInt = Integer.parseInt(str.substring(0, 1));
        String substring = str.substring(1);
        this.mr.removeAllElements();
        switch (i) {
            case 10:
                a aVar = new a(300, 60, Color.rgb(170, 220, 88), -16777216, 96, null);
                aVar.gx = "1";
                this.gM.add(aVar);
                this.mr.addElement(String.valueOf(parseInt - 2) + substring);
                this.mr.addElement(String.valueOf(parseInt - 1) + substring);
                this.mr.addElement(String.valueOf(parseInt) + substring);
                this.mr.addElement(".");
                break;
            case 11:
                a aVar2 = new a(300, 60, Color.rgb(170, 220, 88), -16777216, 96, null);
                aVar2.gx = "1";
                this.gM.add(aVar2);
                a aVar3 = new a(300, 60, Color.rgb(170, 220, 88), -16777216, 96, null);
                aVar3.gx = "2";
                this.gM.add(aVar3);
                this.mr.addElement(String.valueOf(parseInt - 2) + substring);
                this.mr.addElement(String.valueOf(parseInt - 1) + substring);
                this.mr.addElement(String.valueOf(parseInt) + substring);
                this.mr.addElement(".");
                this.mr.addElement(String.valueOf(parseInt - 1) + substring);
                this.mr.addElement(String.valueOf(parseInt) + substring);
                this.mr.addElement(String.valueOf(parseInt + 1) + substring);
                this.mr.addElement(".");
                break;
            case 12:
                a aVar4 = new a(300, 60, Color.rgb(170, 220, 88), -16777216, 96, null);
                aVar4.gx = "2";
                this.gM.add(aVar4);
                this.mr.addElement(String.valueOf(parseInt - 1) + substring);
                this.mr.addElement(String.valueOf(parseInt) + substring);
                this.mr.addElement(String.valueOf(parseInt + 1) + substring);
                this.mr.addElement(".");
                break;
            case 13:
                a aVar5 = new a(300, 60, Color.rgb(170, 220, 88), -16777216, 96, null);
                aVar5.gx = "2";
                this.gM.add(aVar5);
                a aVar6 = new a(300, 60, Color.rgb(170, 220, 88), -16777216, 96, null);
                aVar6.gx = "3";
                this.gM.add(aVar6);
                this.mr.addElement(String.valueOf(parseInt - 1) + substring);
                this.mr.addElement(String.valueOf(parseInt) + substring);
                this.mr.addElement(String.valueOf(parseInt + 1) + substring);
                this.mr.addElement(".");
                this.mr.addElement(String.valueOf(parseInt) + substring);
                this.mr.addElement(String.valueOf(parseInt + 1) + substring);
                this.mr.addElement(String.valueOf(parseInt + 2) + substring);
                this.mr.addElement(".");
                break;
            case 14:
                a aVar7 = new a(300, 60, Color.rgb(170, 220, 88), -16777216, 96, null);
                aVar7.gx = "3";
                this.gM.add(aVar7);
                this.mr.addElement(String.valueOf(parseInt) + substring);
                this.mr.addElement(String.valueOf(parseInt + 1) + substring);
                this.mr.addElement(String.valueOf(parseInt + 2) + substring);
                this.mr.addElement(".");
                break;
            case 15:
                a aVar8 = new a(300, 60, Color.rgb(170, 220, 88), -16777216, 96, null);
                aVar8.gx = "1";
                this.gM.add(aVar8);
                a aVar9 = new a(300, 60, Color.rgb(170, 220, 88), -16777216, 96, null);
                aVar9.gx = "2";
                this.gM.add(aVar9);
                a aVar10 = new a(300, 60, Color.rgb(170, 220, 88), -16777216, 96, null);
                aVar10.gx = "3";
                this.gM.add(aVar10);
                this.mr.addElement(String.valueOf(parseInt - 2) + substring);
                this.mr.addElement(String.valueOf(parseInt - 1) + substring);
                this.mr.addElement(String.valueOf(parseInt) + substring);
                this.mr.addElement(".");
                this.mr.addElement(String.valueOf(parseInt - 1) + substring);
                this.mr.addElement(String.valueOf(parseInt) + substring);
                this.mr.addElement(String.valueOf(parseInt + 1) + substring);
                this.mr.addElement(".");
                this.mr.addElement(String.valueOf(parseInt) + substring);
                this.mr.addElement(String.valueOf(parseInt + 1) + substring);
                this.mr.addElement(String.valueOf(parseInt + 2) + substring);
                this.mr.addElement(".");
                break;
        }
        this.gM.add(this.mq);
        ci();
    }

    public final void h(Vector vector) {
        this.type = 0;
        this.ms = null;
        this.gM.removeAllElements();
        this.mr.removeAllElements();
        int i = vector.size() == 4 ? 55 : 60;
        for (int i2 = 0; i2 < vector.size(); i2++) {
            a aVar = new a(300, i, Color.rgb(170, 220, 88), -16777216, 96, null);
            aVar.gx = (String) vector.elementAt(i2);
            this.gM.add(aVar);
            this.mr.addElement(aVar.gx);
            this.mr.addElement(aVar.gx);
            this.mr.addElement(aVar.gx);
            this.mr.addElement(aVar.gx);
            this.mr.addElement(".");
        }
        this.gM.add(this.mq);
        ci();
    }
}
