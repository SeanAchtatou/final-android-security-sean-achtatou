package com.epoint.android.games.mjfgbfree;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.epoint.android.games.mjfgbfree.c.a;
import com.epoint.android.games.mjfgbfree.c.d;

final class aa extends Handler {
    final /* synthetic */ MJ16ViewActivity iu;

    aa(MJ16ViewActivity mJ16ViewActivity) {
        this.iu = mJ16ViewActivity;
    }

    public final synchronized void handleMessage(Message message) {
        boolean z;
        switch (message.what) {
            case 0:
                this.iu.wk = ((AlertDialog.Builder) message.obj).create();
                this.iu.wk.show();
                break;
            case 1:
                Bundle data = message.getData();
                Toast.makeText(this.iu, data.getString("msg"), data.getInt("msg_length")).show();
                break;
            case 2:
                try {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this.iu);
                    builder.setMessage(af.aa("連線中斷，遊戲結束"));
                    if (d.qB == 0) {
                        String[] strArr = ((a) this.iu.cu.eu()).cp;
                        int i = 0;
                        while (true) {
                            if (i >= strArr.length) {
                                z = true;
                            } else if ("".equals(strArr[i])) {
                                z = false;
                            } else {
                                i++;
                            }
                        }
                        if (z) {
                            builder.setPositiveButton(af.aa("儲存及離開"), new q(this));
                        }
                    }
                    builder.setNegativeButton(af.aa("離開"), new r(this)).create().show();
                    break;
                } catch (Exception e) {
                    break;
                }
            case 3:
                aa.super.finish();
                break;
        }
    }
}
