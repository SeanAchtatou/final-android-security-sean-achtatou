package com.epoint.android.games.mjfgbfree;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ManualActivity extends Activity {
    private String u(String str) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getAssets().open(str)));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    return "<html><head></head><body>" + sb.toString() + "</body>" + "</html>";
                }
                sb.append(String.valueOf(readLine) + "\r\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setVolumeControlStream(3);
        View inflate = LayoutInflater.from(this).inflate((int) C0000R.layout.webview_activity, (ViewGroup) null);
        requestWindowFeature(1);
        setContentView(inflate);
        WebView webView = (WebView) inflate.findViewById(C0000R.id.web_view);
        webView.setScrollBarStyle(0);
        webView.setWebViewClient(new a(this));
        webView.loadDataWithBaseURL("local", u(String.valueOf(getIntent().getExtras().getString("doc")) + af.ch() + ".html"), "text/html", "UTF-8", "");
        Button button = (Button) findViewById(C0000R.id.ok);
        button.setText(af.aa("好"));
        button.setOnClickListener(new b(this));
    }
}
