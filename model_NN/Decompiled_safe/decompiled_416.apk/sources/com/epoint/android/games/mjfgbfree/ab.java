package com.epoint.android.games.mjfgbfree;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import com.epoint.android.games.mjfgbfree.a.d;
import com.epoint.android.games.mjfgbfree.ui.a;
import java.util.Vector;

public final class ab extends View {
    private Vector gM = new Vector();
    private Object jF;
    private Object jG = null;
    private MJ16Activity jH;
    private Bitmap jI = null;
    private Bitmap jJ = null;
    private Bitmap jK = null;
    private Bitmap jL = null;
    private Bitmap jM = null;
    private Paint jN = null;
    private a jO = null;
    public a jP = null;
    private int jQ = 0;
    private int jR = 0;
    private boolean jS = false;

    public ab(MJ16Activity mJ16Activity) {
        super(mJ16Activity);
        this.jH = mJ16Activity;
        int rgb = Color.rgb(240, 240, 230);
        a aVar = new a(200, 60, rgb, -16777216, 208, "TEST", -16777216, null, -7829368);
        this.gM.addElement(aVar);
        aVar.a(true, false);
        aVar.gx = String.valueOf(-5);
        a aVar2 = new a(200, 60, rgb, -16777216, 208, null, -16777216, null, -7829368);
        this.gM.addElement(aVar2);
        aVar2.a(true, false);
        aVar2.gx = String.valueOf(-3);
        a aVar3 = new a(200, 60, rgb, -16777216, 208, null, -16777216, null, -7829368);
        this.gM.addElement(aVar3);
        aVar3.a(true, false);
        aVar3.gx = String.valueOf(-4);
        a aVar4 = new a(200, 60, rgb, -16777216, 208, null, -16777216, null, -7829368);
        this.gM.addElement(aVar4);
        aVar4.a(true, false);
        aVar4.gx = String.valueOf(-2);
        a aVar5 = new a(250, 55, rgb, -16777216, 208, null, -16777216, null, -7829368);
        this.gM.addElement(aVar5);
        aVar5.a(true, false);
        aVar5.gx = String.valueOf(-6);
        a aVar6 = new a(50, 50, rgb, -16777216, 208, null, -16777216, null, -7829368);
        this.gM.addElement(aVar6);
        aVar6.a(true, false);
        aVar6.gx = String.valueOf(-7);
        a aVar7 = new a(50, 50, rgb, -16777216, 208, null, -16777216, null, -7829368);
        this.jP = aVar7;
        this.gM.addElement(aVar7);
        aVar7.a(!MJ16Activity.rf, false);
        aVar7.gx = String.valueOf(-8);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x014c  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x015c  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0162 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void i(boolean r15) {
        /*
            r14 = this;
            r12 = 15
            r11 = 1092616192(0x41200000, float:10.0)
            r10 = 10
            r9 = 0
            r7 = 0
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>()
            android.graphics.Rect r1 = new android.graphics.Rect
            r1.<init>()
            r0.left = r7
            r0.top = r7
            int r2 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra
            r0.bottom = r2
            int r2 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ
            r0.right = r2
            int r2 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ
            int r3 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra
            if (r2 <= r3) goto L_0x00c7
            android.graphics.Bitmap r2 = r14.jM
            int r2 = r2.getHeight()
            int r3 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra
            int r2 = r2 - r3
            int r2 = r2 / 2
            r1.top = r2
            r1.left = r7
            int r2 = r1.top
            int r3 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra
            int r2 = r2 + r3
            r1.bottom = r2
            int r2 = r1.left
            int r3 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ
            int r2 = r2 + r3
            r1.right = r2
        L_0x0041:
            android.graphics.Canvas r2 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj
            android.graphics.Bitmap r3 = r14.jM
            r2.drawBitmap(r3, r1, r0, r9)
            if (r15 == 0) goto L_0x00e6
            android.graphics.Bitmap r0 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri
            int r0 = r0.getWidth()
            android.graphics.Bitmap r1 = r14.jL
            int r1 = r1.getWidth()
            float r1 = (float) r1
            r2 = 1072064102(0x3fe66666, float:1.8)
            float r1 = r1 * r2
            int r1 = (int) r1
            int r0 = r0 - r1
            android.graphics.Bitmap r1 = r14.jI
            int r1 = r1.getWidth()
            int r0 = r0 - r1
            int r1 = r0 / 2
            android.graphics.Bitmap r0 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri
            int r0 = r0.getHeight()
            android.graphics.Bitmap r2 = r14.jI
            int r2 = r2.getHeight()
            int r0 = r0 - r2
            int r2 = r0 / 2
            java.util.Vector r0 = r14.gM
            java.lang.Object r0 = r0.firstElement()
            com.epoint.android.games.mjfgbfree.ui.a r0 = (com.epoint.android.games.mjfgbfree.ui.a) r0
            int r0 = r0.ba()
            int r0 = r0 - r12
            int r0 = r2 - r0
            android.graphics.Bitmap r2 = r14.jI
            int r2 = r2.getWidth()
            int r2 = r2 + r1
            int r2 = r2 + 15
            r3 = r1
            r1 = r2
            r2 = r0
        L_0x0090:
            android.graphics.Canvas r4 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj
            android.graphics.Bitmap r5 = r14.jI
            float r3 = (float) r3
            float r2 = (float) r2
            r4.drawBitmap(r5, r3, r2, r9)
            android.graphics.Canvas r2 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj
            android.graphics.Bitmap r3 = r14.jL
            android.graphics.Canvas r4 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj
            int r4 = r4.getWidth()
            android.graphics.Bitmap r5 = r14.jL
            int r5 = r5.getWidth()
            int r4 = r4 - r5
            float r4 = (float) r4
            android.graphics.Canvas r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj
            int r5 = r5.getHeight()
            android.graphics.Bitmap r6 = r14.jL
            int r6 = r6.getHeight()
            int r5 = r5 - r6
            float r5 = (float) r5
            r2.drawBitmap(r3, r4, r5, r9)
            r2 = r0
            r3 = r7
        L_0x00be:
            java.util.Vector r0 = r14.gM
            int r0 = r0.size()
            if (r3 < r0) goto L_0x0124
            return
        L_0x00c7:
            r1.top = r7
            android.graphics.Bitmap r2 = r14.jM
            int r2 = r2.getWidth()
            int r3 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ
            int r2 = r2 - r3
            int r2 = r2 / 2
            r1.left = r2
            int r2 = r1.top
            int r3 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra
            int r2 = r2 + r3
            r1.bottom = r2
            int r2 = r1.left
            int r3 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ
            int r2 = r2 + r3
            r1.right = r2
            goto L_0x0041
        L_0x00e6:
            android.graphics.Bitmap r0 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri
            int r0 = r0.getWidth()
            android.graphics.Bitmap r1 = r14.jI
            int r1 = r1.getWidth()
            int r0 = r0 - r1
            int r1 = r0 / 2
            java.util.Vector r0 = r14.gM
            java.lang.Object r0 = r0.firstElement()
            com.epoint.android.games.mjfgbfree.ui.a r0 = (com.epoint.android.games.mjfgbfree.ui.a) r0
            int r0 = r0.ba()
            int r2 = r0 / 2
            android.graphics.Bitmap r0 = r14.jI
            int r0 = r0.getHeight()
            int r0 = r0 + r2
            int r3 = r0 + 15
            int r4 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ
            java.util.Vector r0 = r14.gM
            java.lang.Object r0 = r0.firstElement()
            com.epoint.android.games.mjfgbfree.ui.a r0 = (com.epoint.android.games.mjfgbfree.ui.a) r0
            int r0 = r0.bb()
            int r0 = r4 - r0
            int r0 = r0 / 2
            r13 = r3
            r3 = r1
            r1 = r0
            r0 = r13
            goto L_0x0090
        L_0x0124:
            java.util.Vector r0 = r14.gM
            java.lang.Object r0 = r0.elementAt(r3)
            com.epoint.android.games.mjfgbfree.ui.a r0 = (com.epoint.android.games.mjfgbfree.ui.a) r0
            boolean r4 = r0.isVisible()
            if (r4 == 0) goto L_0x0161
            java.lang.String r4 = r0.gx
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            int r4 = r4.intValue()
            switch(r4) {
                case -8: goto L_0x0219;
                case -7: goto L_0x025b;
                case -6: goto L_0x0190;
                case -5: goto L_0x0168;
                case -4: goto L_0x0172;
                case -3: goto L_0x017c;
                case -2: goto L_0x0186;
                default: goto L_0x013f;
            }
        L_0x013f:
            java.lang.String r4 = r0.gx
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            int r4 = r4.intValue()
            r5 = -6
            if (r4 == r5) goto L_0x0156
            r0.c(r1, r2)
            int r4 = r0.ba()
            int r4 = r4 + 10
            int r2 = r2 + r4
        L_0x0156:
            boolean r4 = r0.isVisible()
            if (r4 == 0) goto L_0x0161
            android.graphics.Canvas r4 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj
            r0.draw(r4)
        L_0x0161:
            r0 = r2
        L_0x0162:
            int r2 = r3 + 1
            r3 = r2
            r2 = r0
            goto L_0x00be
        L_0x0168:
            java.lang.String r4 = "開始"
            java.lang.String r4 = com.epoint.android.games.mjfgbfree.af.aa(r4)
            r0.v(r4)
            goto L_0x013f
        L_0x0172:
            java.lang.String r4 = "小統計"
            java.lang.String r4 = com.epoint.android.games.mjfgbfree.af.aa(r4)
            r0.v(r4)
            goto L_0x013f
        L_0x017c:
            java.lang.String r4 = "設定"
            java.lang.String r4 = com.epoint.android.games.mjfgbfree.af.aa(r4)
            r0.v(r4)
            goto L_0x013f
        L_0x0186:
            java.lang.String r4 = "遊戲規則"
            java.lang.String r4 = com.epoint.android.games.mjfgbfree.af.aa(r4)
            r0.v(r4)
            goto L_0x013f
        L_0x0190:
            int r4 = com.epoint.android.games.mjfgbfree.ai.nk
            r5 = 5
            if (r4 != r5) goto L_0x0206
            r4 = 320(0x140, float:4.48E-43)
        L_0x0197:
            r0.j(r4)
            java.lang.String r4 = "購買完全版"
            java.lang.String r4 = com.epoint.android.games.mjfgbfree.af.aa(r4)
            r0.v(r4)
            android.graphics.Canvas r4 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj
            int r4 = r4.getHeight()
            int r4 = r4 - r10
            int r5 = r0.ba()
            int r4 = r4 - r5
            r0.c(r10, r4)
            java.lang.String r4 = ""
            com.epoint.android.games.mjfgbfree.MJ16Activity r5 = r14.jH     // Catch:{ NameNotFoundException -> 0x0298 }
            android.content.pm.PackageManager r5 = r5.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0298 }
            com.epoint.android.games.mjfgbfree.MJ16Activity r6 = r14.jH     // Catch:{ NameNotFoundException -> 0x0298 }
            java.lang.String r6 = r6.getPackageName()     // Catch:{ NameNotFoundException -> 0x0298 }
            r7 = 0
            android.content.pm.PackageInfo r5 = r5.getPackageInfo(r6, r7)     // Catch:{ NameNotFoundException -> 0x0298 }
            java.lang.String r4 = r5.versionName     // Catch:{ NameNotFoundException -> 0x0298 }
        L_0x01c7:
            boolean r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.qL
            if (r5 == 0) goto L_0x013f
            android.graphics.Canvas r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "Free Version"
            java.lang.String r7 = com.epoint.android.games.mjfgbfree.af.aa(r7)
            java.lang.String r7 = java.lang.String.valueOf(r7)
            r6.<init>(r7)
            java.lang.String r7 = ""
            boolean r7 = r7.equals(r4)
            if (r7 == 0) goto L_0x0209
            java.lang.String r4 = ""
        L_0x01e6:
            java.lang.StringBuilder r4 = r6.append(r4)
            java.lang.String r4 = r4.toString()
            r6 = 1097859072(0x41700000, float:15.0)
            android.graphics.Canvas r7 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj
            int r7 = r7.getHeight()
            r8 = 17
            int r7 = r7 - r8
            int r8 = r0.ba()
            int r7 = r7 - r8
            float r7 = (float) r7
            android.graphics.Paint r8 = r14.jN
            r5.drawText(r4, r6, r7, r8)
            goto L_0x013f
        L_0x0206:
            r4 = 250(0xfa, float:3.5E-43)
            goto L_0x0197
        L_0x0209:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = ": "
            r7.<init>(r8)
            java.lang.StringBuilder r4 = r7.append(r4)
            java.lang.String r4 = r4.toString()
            goto L_0x01e6
        L_0x0219:
            android.graphics.Bitmap r4 = r14.jK
            int r4 = r4.getWidth()
            r0.j(r4)
            android.graphics.Bitmap r4 = r14.jK
            int r4 = r4.getHeight()
            r0.k(r4)
            android.graphics.Canvas r4 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj
            int r4 = r4.getHeight()
            r5 = 40
            int r4 = r4 - r5
            int r5 = r0.ba()
            int r4 = r4 - r5
            android.graphics.Bitmap r5 = r14.jJ
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            int r4 = r4 - r12
            android.graphics.Bitmap r5 = r14.jK
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            r0.c(r10, r4)
            android.graphics.Canvas r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj
            r0.draw(r5)
            android.graphics.Canvas r0 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj
            android.graphics.Bitmap r5 = r14.jK
            float r4 = (float) r4
            r0.drawBitmap(r5, r11, r4, r9)
            r0 = r2
            goto L_0x0162
        L_0x025b:
            android.graphics.Bitmap r4 = r14.jJ
            int r4 = r4.getWidth()
            r0.j(r4)
            android.graphics.Bitmap r4 = r14.jJ
            int r4 = r4.getHeight()
            r0.k(r4)
            android.graphics.Canvas r4 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj
            int r4 = r4.getHeight()
            r5 = 20
            int r4 = r4 - r5
            int r5 = r0.ba()
            int r4 = r4 - r5
            android.graphics.Bitmap r5 = r14.jJ
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            r5 = 20
            int r4 = r4 - r5
            r0.c(r10, r4)
            android.graphics.Canvas r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj
            r0.draw(r5)
            android.graphics.Canvas r0 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj
            android.graphics.Bitmap r5 = r14.jJ
            float r4 = (float) r4
            r0.drawBitmap(r5, r11, r4, r9)
            r0 = r2
            goto L_0x0162
        L_0x0298:
            r5 = move-exception
            goto L_0x01c7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.ab.i(boolean):void");
    }

    public final void bz() {
        if (this.jI != null) {
            this.jI.recycle();
            this.jI = null;
        }
        if (this.jJ != null) {
            this.jJ.recycle();
            this.jJ = null;
        }
        if (this.jK != null) {
            this.jK.recycle();
            this.jK = null;
        }
        if (this.jL != null) {
            this.jL.recycle();
            this.jL = null;
        }
        if (this.jM != null) {
            this.jM.recycle();
            this.jM = null;
        }
        this.jN = null;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.gM.size()) {
                ((a) this.gM.elementAt(i2)).aX();
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public final void onDraw(Canvas canvas) {
        Canvas canvas2;
        MJ16Activity.rj.drawColor(Color.rgb(0, 104, 63));
        if (MJ16Activity.ri != null) {
            boolean z = MJ16Activity.qZ > MJ16Activity.ra;
            boolean z2 = getWidth() > getHeight();
            if (this.jI == null) {
                this.jI = d.c(this.jH, "images/title" + af.ch() + ".png");
                this.jL = d.c(this.jH, "images/title_tile.png");
                this.jJ = d.c(this.jH, "images/fb_like.png");
                this.jK = d.c(this.jH, "images/star_l.png");
                Bitmap c = d.c(this.jH, "images/bg2.jpg");
                int width = MJ16Activity.ri.getWidth() > MJ16Activity.ri.getHeight() ? MJ16Activity.ri.getWidth() : MJ16Activity.ri.getHeight();
                if (width > c.getWidth()) {
                    this.jM = Bitmap.createBitmap(width, width, Bitmap.Config.RGB_565);
                    Canvas canvas3 = new Canvas(this.jM);
                    Rect rect = new Rect();
                    rect.left = 0;
                    rect.top = 0;
                    rect.bottom = c.getHeight();
                    rect.right = c.getWidth();
                    Rect rect2 = new Rect();
                    rect2.left = 0;
                    rect2.top = 0;
                    rect2.bottom = width;
                    rect2.right = width;
                    Paint paint = new Paint();
                    paint.setDither(true);
                    paint.setFilterBitmap(true);
                    canvas3.drawBitmap(c, rect, rect2, paint);
                    canvas2 = canvas3;
                } else {
                    this.jM = c.copy(Bitmap.Config.RGB_565, true);
                    canvas2 = new Canvas(this.jM);
                }
                c.recycle();
                canvas2.drawColor(Color.rgb(80, 10, 0), PorterDuff.Mode.SCREEN);
                this.jN = new Paint();
                this.jN.setColor(-1);
                this.jN.setTextSize(22.0f);
                this.jN.setAntiAlias(true);
                this.jN.setShadowLayer(3.0f, 2.0f, 2.0f, -16777216);
            }
            i(z);
            if (z != z2) {
                return;
            }
            if (MJ16Activity.ra == getHeight() && MJ16Activity.qZ == getWidth()) {
                canvas.drawBitmap(MJ16Activity.ri, 0.0f, 0.0f, (Paint) null);
                return;
            }
            Rect rect3 = new Rect();
            Rect rect4 = new Rect();
            int i = rect4.left;
            rect4.top = i;
            rect3.left = i;
            rect3.top = i;
            rect3.bottom = MJ16Activity.ri.getHeight();
            rect3.right = MJ16Activity.ri.getWidth();
            rect4.right = getWidth();
            rect4.bottom = getHeight();
            canvas.setDrawFilter(MJ16Activity.rk);
            canvas.drawBitmap(MJ16Activity.ri, rect3, rect4, (Paint) null);
            return;
        }
        invalidate();
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (!(((int) motionEvent.getX()) == this.jQ && ((int) motionEvent.getY()) == this.jR && action != 1) && (action == 0 || action == 1 || action == 2)) {
            this.jQ = (int) motionEvent.getX();
            this.jR = (int) motionEvent.getY();
            int a2 = af.a(this.jQ, MJ16Activity.ri.getWidth(), getWidth());
            int a3 = af.a(this.jR, MJ16Activity.ri.getHeight(), getHeight());
            int i = 0;
            a aVar = null;
            while (i < this.gM.size()) {
                a aVar2 = (a) this.gM.elementAt(i);
                aVar2.b(false);
                if (aVar2.isVisible()) {
                    Rect bounds = aVar2.getBounds();
                    if (a2 >= bounds.left && a2 <= bounds.right && a3 >= bounds.top && a3 <= bounds.bottom && (action == 0 || (action != 0 && this.jF == aVar2))) {
                        aVar2.b(true);
                        i++;
                        aVar = aVar2;
                    }
                }
                aVar2 = aVar;
                i++;
                aVar = aVar2;
            }
            a aVar3 = aVar != null ? aVar : null;
            if (action == 0) {
                this.jF = aVar3;
                this.jS = false;
            } else if (action == 1) {
                if (aVar3 != null) {
                    if (aVar3 == this.jF) {
                        for (int i2 = 0; i2 < this.gM.size(); i2++) {
                            a aVar4 = (a) this.gM.elementAt(i2);
                            aVar4.b(false);
                            if (aVar4 == aVar3) {
                                this.jF = null;
                                this.jG = null;
                                this.jR = 0;
                                this.jQ = 0;
                                invalidate();
                                this.jH.a(Integer.valueOf(aVar4.gx));
                            }
                        }
                        return true;
                    }
                } else if (aVar3 == null && this.jF == null && !this.jS) {
                    invalidate();
                }
            }
            if (aVar3 != this.jG) {
                this.jG = aVar3;
                invalidate();
            }
        }
        return true;
    }
}
