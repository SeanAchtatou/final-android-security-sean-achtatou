package com.epoint.android.games.mjfgbfree;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;
import com.epoint.android.games.mjfgbfree.ui.e;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InstructionActivity extends Activity {
    private RelativeLayout.LayoutParams I;
    private int J;
    /* access modifiers changed from: private */
    public WebView gL;
    /* access modifiers changed from: private */
    public ToggleButton sG;
    /* access modifiers changed from: private */
    public ToggleButton sH;
    /* access modifiers changed from: private */
    public ToggleButton sI;
    /* access modifiers changed from: private */
    public String sJ;
    /* access modifiers changed from: private */
    public String sK;
    /* access modifiers changed from: private */
    public String sL;

    /* access modifiers changed from: private */
    public void a(String str) {
        WebView webView = new WebView(this);
        webView.setLayoutParams(this.I);
        webView.setFadingEdgeLength(this.J);
        webView.setVerticalFadingEdgeEnabled(true);
        webView.setBackgroundColor(0);
        webView.setInitialScale(100);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new f(this));
        RelativeLayout relativeLayout = (RelativeLayout) this.gL.getParent();
        this.gL.clearView();
        relativeLayout.removeView(this.gL);
        this.gL.destroy();
        this.gL = webView;
        relativeLayout.addView(this.gL);
        this.gL.loadDataWithBaseURL("local", str, "text/html", "UTF-8", "");
    }

    private String u(String str) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getAssets().open(String.valueOf(str) + af.ch() + ".html")));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append("<html>");
            sb2.append("<head></head><script type='text/javascript'>");
            sb2.append("var tile_style='" + (ai.nj == c.NUMERIC ? "en" : ai.nj == c.eg ? "mix" : "") + "';");
            sb2.append("var min_points=" + ai.ns + ";");
            if (str.equals("GBMJ_types")) {
                sb2.append("var imgs = [");
                sb2.append("['" + af.ab("四風會").replace("'", "\\'") + "','ET','ET','ET','','SH','SH','SH','','WS','WS','WS','NR','NR','NR','PK','','PK'],");
                sb2.append("['" + af.ab("大三元").replace("'", "\\'") + "','1T','2T','3T','','CN','CN','CN','','FT','FT','FT','','PK','PK','PK','9M','','9M'],");
                sb2.append("['" + af.ab("綠一色").replace("'", "\\'") + "','2S','2S','2S','','3S','3S','3S','','6S','6S','6S','8S','8S','8S','FT','','FT'],");
                sb2.append("['" + af.ab("九蓮寶燈").replace("'", "\\'") + "','1T','1T','1T','2T','3T','4T','5T','6T','7T','8T','9T','9T','9T','','9T'],");
                sb2.append("['" + af.ab("四槓").replace("'", "\\'") + "','2T','2T','2T','BK','','5T','5T','5T','5T','','1M','1M','1M','BK','','9M','9M','9M','BK','','WS','WS'],");
                sb2.append("['" + af.ab("連七對").replace("'", "\\'") + "','2T','2T','3T','3T','4T','4T','5T','5T','6T','6T','7T','7T','8T','','8T'],");
                sb2.append("['" + af.ab("十三么").replace("'", "\\'") + "','1T','9T','1S','9S','1M','9M','ET','SH','WS','NR','CN','FT','PK','','CN'],");
                sb2.append("['" + af.ab("清么九").replace("'", "\\'") + "','9T','9T','9T','','1S','1S','1S','9S','9S','9S','1M','1M','1M','1T','','1T'],");
                sb2.append("['" + af.ab("小四風會").replace("'", "\\'") + "','ET','ET','ET','ET','','SH','SH','SH','WS','WS','WS','NR','NR','3M','4M','','5M'],");
                sb2.append("['" + af.ab("小三元").replace("'", "\\'") + "','2S','2S','2S','','CN','CN','CN','','FT','FT','FT','','PK','PK','9M','9M','','9M'],");
                sb2.append("['" + af.ab("字一色").replace("'", "\\'") + "','ET','ET','ET','','SH','SH','SH','WS','WS','WS','CN','CN','FT','FT','','CN'],");
                sb2.append("['" + af.ab("四暗刻").replace("'", "\\'") + "','3S','3S','3S','4S','4S','4S','7M','7M','7M','9M','9M','9M','CN','','CN'],");
                sb2.append("['" + af.ab("一色雙龍會").replace("'", "\\'") + "','1M','2M','3M','','1M','2M','3M','','7M','8M','9M','7M','8M','9M','5M','','5M'],");
                sb2.append("['" + af.ab("一色四同順").replace("'", "\\'") + "','1S','2S','3S','','1S','2S','3S','','1S','2S','3S','1S','2S','3S','1M','','1M'],");
                sb2.append("['" + af.ab("一色四節高").replace("'", "\\'") + "','6T','6T','6T','7T','7T','7T','8T','8T','8T','9T','9T','9T','CN','','CN'],");
                sb2.append("['" + af.ab("一色四步高").replace("'", "\\'") + "','1M','2M','3M','','3M','4M','5M','','5M','6M','7M','','7M','8M','9M','WS','','WS'],");
                sb2.append("['" + af.ab("三槓").replace("'", "\\'") + "','2T','2T','2T','BK','','4T','4T','4T','4T','','1S','1S','1S','BK','','3M','3M','3M','PK','','PK'],");
                sb2.append("['" + af.ab("混么九").replace("'", "\\'") + "','9S','9S','9S','','1M','1M','1M','ET','ET','ET','SH','SH','SH','FT','','FT'],");
                sb2.append("['" + af.ab("七對").replace("'", "\\'") + "','1S','1S','4S','4S','6S','6S','8S','8S','2M','2M','5M','5M','SH','','SH'],");
                sb2.append("['" + af.ab("七星不靠").replace("'", "\\'") + "','4T','7T','2S','5S','3M','9M','ET','SH','WS','NR','CN','FT','PK','','1T'],");
                sb2.append("['" + af.ab("全雙刻").replace("'", "\\'") + "','2T','2T','2T','BK','','6T','6T','6T','','4S','4S','4S','8S','8S','8S','6M','','6M'],");
                sb2.append("['" + af.ab("清一色").replace("'", "\\'") + "','1M','1M','1M','','2M','3M','4M','2M','3M','4M','6M','6M','6M','8M','','8M'],");
                sb2.append("['" + af.ab("一色三同順").replace("'", "\\'") + "','2S','3S','4S','','2S','3S','4S','','2S','3S','4S','','1M','1M','1M','FT','','FT'],");
                sb2.append("['" + af.ab("一色三節高").replace("'", "\\'") + "','1S','1S','1S','','2S','2S','2S','','3S','3S','3S','','9M','9M','9M','','SH','SH'],");
                sb2.append("['" + af.ab("全大").replace("'", "\\'") + "','7T','8T','9T','','7T','8T','9T','','7S','8S','9S','','9S','9S','9S','','8M','8M'],");
                sb2.append("['" + af.ab("全中").replace("'", "\\'") + "','4T','4T','4T','','6T','6T','6T','','4S','5S','6S','','4M','5M','6M','6M','','6M'],");
                sb2.append("['" + af.ab("全小").replace("'", "\\'") + "','2S','2S','2S','','3S','3S','3S','1M','2M','3M','2M','2M','2M','3M','','3M'],");
                sb2.append("['" + af.ab("清龍").replace("'", "\\'") + "','2S','2S','2S','','1M','2M','3M','','4M','5M','6M','','7M','8M','9M','WS','','WS'],");
                sb2.append("['" + af.ab("三色雙龍會").replace("'", "\\'") + "','1S','2S','3S','','7S','8S','9S','','5T','5T','','1M','2M','3M','8M','9M','','7M'],");
                sb2.append("['" + af.ab("一色三步高").replace("'", "\\'") + "','1T','2T','3T','','2T','3T','4T','','3T','4T','5T','SH','SH','PK','PK','','SH'],");
                sb2.append("['" + af.ab("全帶五").replace("'", "\\'") + "','3S','4S','5S','','4T','5T','6T','','3T','4T','5T','','5S','5S','5M','5M','','5M'],");
                sb2.append("['" + af.ab("三同刻").replace("'", "\\'") + "','1T','1T','1T','','5T','5T','5T','','1S','1S','1S','1M','1M','1M','NR','','NR'],");
                sb2.append("['" + af.ab("三暗刻").replace("'", "\\'") + "','4S','4S','4S','','7M','7M','7M','8M','8M','8M','CN','CN','CN','PK','','PK'],");
                sb2.append("['" + af.ab("全不靠").replace("'", "\\'") + "','1T','4T','7T','2S','8S','3M','6M','9M','SH','WS','NR','CN','FT','','PK'],");
                sb2.append("['" + af.ab("組合龍").replace("'", "\\'") + "','1M','1M','1M','','FT','FT','','1T','4T','7T','3S','6S','9S','2M','5M','','8M'],");
                sb2.append("['" + af.ab("大於五").replace("'", "\\'") + "','6T','6T','6T','','9T','9T','9T','','7S','8S','9S','','7M','7M','7M','','9S','9S'],");
                sb2.append("['" + af.ab("小於五").replace("'", "\\'") + "','1T','2T','3T','','2T','3T','4T','','2S','3S','4S','','2M','2M','2M','','4M','4M'],");
                sb2.append("['" + af.ab("三風刻").replace("'", "\\'") + "','6M','6M','6M','','ET','ET','ET','','SH','SH','SH','','NR','NR','NR','','CN','CN'],");
                sb2.append("['" + af.ab("花龍").replace("'", "\\'") + "','4T','5T','6T','','1S','2S','3S','','7M','8M','9M','','WS','WS','WS','','NR','NR'],");
                sb2.append("['" + af.ab("推不倒").replace("'", "\\'") + "','2T','3T','4T','','2T','3T','4T','','5T','5T','5T','','8S','8S','8S','','PK','PK'],");
                sb2.append("['" + af.ab("三色三同順").replace("'", "\\'") + "','4T','5T','6T','','4S','5S','6S','','4M','5M','6M','','7M','8M','9M','','1S','1S'],");
                sb2.append("['" + af.ab("三色三節高").replace("'", "\\'") + "','2T','2T','2T','','3S','3S','3S','','4M','4M','4M','','6M','7M','8M','','SH','SH'],");
                sb2.append("['" + af.ab("無番和").replace("'", "\\'") + "','2T','3T','4T','','5S','5S','5S','','7M','8M','9M','','CN','CN','4T','5T','','6T'],");
                sb2.append("['" + af.ab("妙手回春").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("海底撈月").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("槓上開花").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("搶槓").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("雙暗槓").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("碰碰和").replace("'", "\\'") + "','1S','1S','1S','','2S','2S','2S','','1M','1M','1M','2M','2M','ET','ET','','2M'],");
                sb2.append("['" + af.ab("混一色").replace("'", "\\'") + "','2M','2M','2M','','1M','2M','3M','6M','6M','6M','ET','ET','ET','CN','','CN'],");
                sb2.append("['" + af.ab("三色三步高").replace("'", "\\'") + "','2T','3T','4T','','3S','4S','5S','','5S','5S','5S','4M','5M','6M','FT','','FT'],");
                sb2.append("['" + af.ab("五門齊").replace("'", "\\'") + "','1T','1T','','3S','4S','5S','','3M','4M','5M','SH','SH','SH','PK','PK','','PK'],");
                sb2.append("['" + af.ab("全求人").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("雙箭刻").replace("'", "\\'") + "','CN','CN','CN','','FT','FT','FT'],");
                sb2.append("['" + af.ab("全帶么").replace("'", "\\'") + "','1T','1T','1T','','1T','2T','3T','','7S','8S','9S','NR','NR','NR','FT','','FT'],");
                sb2.append("['" + af.ab("不求人").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("雙槓").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("和絕張").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("箭刻").replace("'", "\\'") + "','FT','FT','FT'],");
                sb2.append("['" + af.ab("圈風刻").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("門風刻").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("門前清").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("平和").replace("'", "\\'") + "','4S','5S','6S','','4S','5S','6S','','4M','5M','6M','7M','8M','9M','1T','','1T'],");
                sb2.append("['" + af.ab("四歸一").replace("'", "\\'") + "','5T','6T','7T','','6T','7T','8T','','6T','6T'],");
                sb2.append("['" + af.ab("雙同刻").replace("'", "\\'") + "','2S','2S','2S','','2M','2M','2M'],");
                sb2.append("['" + af.ab("雙暗刻").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("暗槓").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("斷么").replace("'", "\\'") + "','2S','3S','4S','','6S','6S','6S','','2M','3M','4M','6M','6M','6M','8M','','8M'],");
                sb2.append("['" + af.ab("一般高").replace("'", "\\'") + "','4T','5T','6T','','4T','5T','6T'],");
                sb2.append("['" + af.ab("喜雙逢").replace("'", "\\'") + "','1S','2S','3S','','1M','2M','3M'],");
                sb2.append("['" + af.ab("連六").replace("'", "\\'") + "','3M','4M','5M','','6M','7M','8M'],");
                sb2.append("['" + af.ab("老少副").replace("'", "\\'") + "','1T','2T','3T','','7T','8T','9T'],");
                sb2.append("['" + af.ab("么九刻").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("明槓").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("缺一門").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("無字").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("邊張").replace("'", "\\'") + "','8S','9S','','7S'],");
                sb2.append("['" + af.ab("坎張").replace("'", "\\'") + "','6M','8M','','7M'],");
                sb2.append("['" + af.ab("單調將").replace("'", "\\'") + "','1S','2S','3S','NR','','NR'],");
                sb2.append("['" + af.ab("自摸").replace("'", "\\'") + "'],");
                sb2.append("['" + af.ab("花牌").replace("'", "\\'") + "','S1','S2','S3','S4','F1','F2','F3','F4']];");
            }
            sb2.append("</script><style>");
            sb2.append("#tile_img{");
            sb2.append("height:" + ((int) (44.0f * MJ16Activity.rc)) + "px;");
            sb2.append("}");
            sb2.append("#help_img{");
            sb2.append("height:" + ((int) (260.0f * MJ16Activity.rc)) + "px;");
            sb2.append("}");
            sb2.append("#myfont{");
            sb2.append("font-size:" + (MJ16Activity.rb + 1) + "px;");
            sb2.append("}");
            sb2.append("</style>");
            sb2.append("<body onload='init();'>");
            sb2.append("<table width=\"100%\">");
            sb2.append("<tr>");
            sb2.append("<td><font style='font-size:" + (MJ16Activity.rb + 2) + "px'>");
            sb2.append(sb.toString());
            sb2.append("</font></td>");
            sb2.append("</tr>");
            sb2.append("</table>");
            sb2.append("</body>");
            sb2.append("</html>");
            return sb2.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        MJ16Activity.dd();
        setVolumeControlStream(3);
        if (MJ16Activity.rd) {
            setRequestedOrientation(0);
        } else {
            setRequestedOrientation(ai.nf);
        }
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(this).inflate((int) C0000R.layout.instruction_dialog, (ViewGroup) null);
        this.gL = (WebView) linearLayout.findViewById(C0000R.id.InstructionView);
        this.I = (RelativeLayout.LayoutParams) this.gL.getLayoutParams();
        this.J = this.gL.getVerticalFadingEdgeLength();
        setContentView(linearLayout);
        ((ImageView) linearLayout.findViewById(C0000R.id.bg)).setImageBitmap(e.y(this));
        this.sI = (ToggleButton) linearLayout.findViewById(C0000R.id.btn_quickstart);
        String aa = af.aa("快速指南");
        this.sI.setText(aa);
        this.sI.setTextOn(aa);
        this.sI.setTextOff(aa);
        this.sI.setOnClickListener(new g(this));
        this.sG = (ToggleButton) linearLayout.findViewById(C0000R.id.btn_rules);
        String aa2 = af.aa("規則");
        this.sG.setText(aa2);
        this.sG.setTextOn(aa2);
        this.sG.setTextOff(aa2);
        this.sG.setOnClickListener(new d(this));
        this.sH = (ToggleButton) linearLayout.findViewById(C0000R.id.btn_types);
        String aa3 = af.aa("牌型");
        this.sH.setText(aa3);
        this.sH.setTextOn(aa3);
        this.sH.setTextOff(aa3);
        this.sH.setOnClickListener(new e(this));
        this.sK = u("GBMJ_types");
        this.sJ = u("GBMJ_rules");
        this.sL = u("GBMJ_quickstart");
        this.sI.setChecked(true);
        a(this.sL);
    }

    public void onDestroy() {
        e.gt();
        super.onDestroy();
    }
}
