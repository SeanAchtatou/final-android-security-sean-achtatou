package com.epoint.android.games.mjfgbfree.f;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.Date;
import java.util.UUID;

public final class b extends SQLiteOpenHelper {
    public b(Context context) {
        super(context, "mjfgb.db", (SQLiteDatabase.CursorFactory) null, 3);
    }

    private static void a(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE item_v2 (item_id NUMERIC PRIMARY KEY, count NUMERIC, link_id TEXT DEFAULT NULL)");
        sQLiteDatabase.execSQL("INSERT INTO item_v2 (item_id, count) VALUES(0,20)");
        sQLiteDatabase.execSQL("INSERT INTO lookup VALUES('last_ticket_update'," + new Date().getTime() + ")");
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE item (type_id NUMERIC, item_group_id NUMERIC, category_id NUMERIC, to_fan_level NUMERIC, item_id INTEGER PRIMARY KEY, item_name TEXT, from_fan_level NUMERIC)");
        sQLiteDatabase.execSQL("CREATE TABLE user_profile (user_guid TEXT, displayname TEXT)");
        sQLiteDatabase.execSQL("CREATE TABLE local_score (guid TEXT PRIMARY KEY, user_name TEXT, score NUMERIC, rounds NUMERIC, ranking NUMERIC, timestamp NUMERIC)");
        sQLiteDatabase.execSQL("CREATE TABLE local_save (guid TEXT PRIMARY KEY, type NUMERIC, rounds NUMERIC, data TEXT, timestamp NUMERIC)");
        sQLiteDatabase.execSQL("CREATE TABLE local_data_cap (data_name TEXT PRIMARY KEY)");
        sQLiteDatabase.execSQL("INSERT INTO local_data_cap VALUES('fan_history')");
        sQLiteDatabase.execSQL("INSERT INTO local_data_cap VALUES('total_fan')");
        sQLiteDatabase.execSQL("CREATE TABLE image_cache (url TEXT PRIMARY KEY, data BLOB, ref_width NUMERIC, timestamp NUMERIC, size NUMERIC)");
        sQLiteDatabase.execSQL("CREATE TABLE text_cache (url TEXT PRIMARY KEY, data TEXT, timestamp NUMERIC, size NUMERIC)");
        sQLiteDatabase.execSQL("CREATE TABLE local_fan_history (guid TEXT PRIMARY KEY, fan_name TEXT, count NUMERIC, fans NUMERIC, ranking_fans NUMERIC, grade_id NUMERIC, idx NUMERIC)");
        sQLiteDatabase.execSQL("CREATE TABLE local_total_fan_stat (fans_from NUMERIC, fans_to NUMERIC, count NUMERIC, grade_id NUMERIC)");
        sQLiteDatabase.execSQL("INSERT INTO local_total_fan_stat VALUES(1,10,0,10)");
        sQLiteDatabase.execSQL("INSERT INTO local_total_fan_stat VALUES(11,20,0,10)");
        sQLiteDatabase.execSQL("INSERT INTO local_total_fan_stat VALUES(21,30,0,9)");
        sQLiteDatabase.execSQL("INSERT INTO local_total_fan_stat VALUES(31,40,0,9)");
        sQLiteDatabase.execSQL("INSERT INTO local_total_fan_stat VALUES(41,50,0,8)");
        sQLiteDatabase.execSQL("INSERT INTO local_total_fan_stat VALUES(51,60,0,7)");
        sQLiteDatabase.execSQL("INSERT INTO local_total_fan_stat VALUES(61,70,0,6)");
        sQLiteDatabase.execSQL("INSERT INTO local_total_fan_stat VALUES(71,80,0,5)");
        sQLiteDatabase.execSQL("INSERT INTO local_total_fan_stat VALUES(81,90,0,4)");
        sQLiteDatabase.execSQL("INSERT INTO local_total_fan_stat VALUES(91,100,0,2)");
        sQLiteDatabase.execSQL("INSERT INTO local_total_fan_stat VALUES(101,120,0,1)");
        sQLiteDatabase.execSQL("INSERT INTO local_total_fan_stat VALUES(121,9999,0,1)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','四風會',0,88,88,2,1)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','大三元',0,88,88,3,2)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','綠一色',0,88,88,2,3)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','九蓮寶燈',0,88,88,2,4)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','四槓',0,88,88,2,5)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','連七對',0,88,88,2,6)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','十三么',0,88,88,2,7)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','清么九',0,64,64,2,8)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','小四風會',0,64,64,2,9)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','小三元',0,64,64,4,10)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','字一色',0,64,64,2,11)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','四暗刻',0,64,64,2,12)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','一色雙龍會',0,64,64,2,13)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','一色四同順',0,48,48,2,14)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','一色四節高',0,48,48,2,15)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','一色四步高',0,32,32,2,16)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','三槓',0,32,32,4,17)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','混么九',0,32,32,4,18)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','七對',0,24,24,6,19)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','七星不靠',0,24,24,5,20)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','全雙刻',0,24,24,7,21)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','清一色',0,24,24,9,22)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','一色三同順',0,24,24,8,23)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','一色三節高',0,24,24,8,24)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','全大',0,24,24,8,25)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','全中',0,24,24,8,26)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','全小',0,24,24,8,27)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','清龍',0,16,16,9,28)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','三色雙龍會',0,16,16,8,29)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','一色三步高',0,16,16,8,30)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','全帶五',0,16,16,8,31)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','三同刻',0,16,16,8,32)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','三暗刻',0,16,16,8,33)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','全不靠',0,12,12,8,34)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','組合龍',0,12,12,8,35)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','大於五',0,12,12,8,36)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','小於五',0,12,12,8,37)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','三風刻',0,12,12,8,38)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','花龍',0,8,8,9,39)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','推不倒',0,8,8,8,40)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','三色三同順',0,8,8,8,41)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','三色三節高',0,8,8,8,42)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','無番和',0,8,8,8,43)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','妙手回春',0,8,8,2,44)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','海底撈月',0,8,8,8,45)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','槓上開花',0,8,8,8,46)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','搶槓',0,8,8,6,47)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','雙暗槓',0,8,8,9,48)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','碰碰和',0,6,6,9,49)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','混一色',0,6,6,9,50)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','三色三步高',0,6,6,8,51)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','五門齊',0,6,6,9,52)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','全求人',0,6,6,9,53)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','雙箭刻',0,6,6,9,54)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','全帶么',0,4,4,9,55)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','不求人',0,4,4,8,56)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','雙槓',0,4,4,9,57)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','和絕張',0,4,4,9,58)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','箭刻',0,2,2,0,59)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','圈風刻',0,2,2,0,60)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','門風刻',0,2,2,0,61)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','門前清',0,2,2,9,62)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','平和',0,2,2,9,63)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','四歸一',0,2,2,9,64)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','雙同刻',0,2,2,9,65)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','雙暗刻',0,2,2,9,66)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','暗槓',0,2,2,0,67)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','斷么',0,2,2,9,68)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','一般高',0,1,1,10,69)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','喜雙逢',0,1,1,10,70)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','連六',0,1,1,10,71)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','老少副',0,1,1,10,72)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','么九刻',0,1,1,0,73)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','明槓',0,1,1,0,74)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','缺一門',0,1,1,10,75)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','無字',0,1,1,10,76)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','邊張',0,1,1,10,77)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','坎張',0,1,1,10,78)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','單調將',0,1,1,10,79)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','自摸',0,1,1,10,81)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','一炮雙響',0,0,0,2,998)");
        sQLiteDatabase.execSQL("INSERT INTO local_fan_history VALUES('" + UUID.randomUUID().toString() + "','一炮三響',0,0,0,1,999)");
        sQLiteDatabase.execSQL("CREATE TABLE lookup (key TEXT PRIMARY KEY, value TEXT)");
        a(sQLiteDatabase);
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (i <= 1) {
            sQLiteDatabase.execSQL("CREATE TABLE lookup (key TEXT PRIMARY KEY, value TEXT)");
        }
        if (i <= 2) {
            a(sQLiteDatabase);
        }
    }
}
