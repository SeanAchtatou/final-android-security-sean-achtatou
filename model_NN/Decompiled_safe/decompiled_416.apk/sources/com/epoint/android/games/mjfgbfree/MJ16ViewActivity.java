package com.epoint.android.games.mjfgbfree;

import a.b.c.a.b;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.bumptech.bumpapi.BumpAPI;
import com.bumptech.bumpapi.BumpConnection;
import com.epoint.android.games.mjfgbfree.c.d;
import com.epoint.android.games.mjfgbfree.c.f;
import com.epoint.android.games.mjfgbfree.d.e;
import com.epoint.android.games.mjfgbfree.d.g;
import com.epoint.android.games.mjfgbfree.e.a;
import com.epoint.android.games.mjfgbfree.e.k;
import com.epoint.android.games.mjfgbfree.e.l;
import com.epoint.android.games.mjfgbfree.e.q;
import com.epoint.android.games.mjfgbfree.e.r;
import com.epoint.android.games.mjfgbfree.network.BTConnectionActivity;
import com.epoint.android.games.mjfgbfree.network.TCPLocalHostConnectionActivity;
import com.epoint.android.games.mjfgbfree.network.TCPServerConnectionActivity;
import com.epoint.android.games.mjfgbfree.network.aa;
import com.epoint.android.games.mjfgbfree.network.ag;
import com.epoint.android.games.mjfgbfree.tournament.TournamentActivity;
import com.epoint.android.games.mjfgbfree.tournament.c;
import java.io.File;
import java.util.Enumeration;
import java.util.Vector;

public class MJ16ViewActivity extends Activity {
    public static Vector ad;
    public static byte[] wl;
    public static int wn;
    public static int wo;
    private String cn;
    /* access modifiers changed from: private */
    public bb cu = null;
    private Handler mHandler = new aa(this);
    private LinearLayout uF;
    /* access modifiers changed from: private */
    public File wj;
    /* access modifiers changed from: private */
    public AlertDialog wk;
    private int wm = 0;
    private e wp;
    private a wq;

    private synchronized void a(AlertDialog.Builder builder) {
        Message message = new Message();
        message.what = 0;
        message.obj = builder;
        this.mHandler.sendMessage(message);
    }

    private synchronized void aB(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("msg", str);
        bundle.putInt("msg_length", 1);
        Message message = new Message();
        message.what = 1;
        message.setData(bundle);
        this.mHandler.sendMessage(message);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.epoint.android.games.mjfgbfree.bb.b(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.epoint.android.games.mjfgbfree.bb.b(android.graphics.Bitmap, int):android.graphics.Bitmap
      com.epoint.android.games.mjfgbfree.bb.b(int, int):void
      a.a.b.b(int, int):void
      com.epoint.android.games.mjfgbfree.bb.b(boolean, boolean):void */
    public final bb a(d dVar, int i) {
        RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(this).inflate((int) C0000R.layout.mj16_activity, (ViewGroup) null);
        this.uF = (LinearLayout) relativeLayout.findViewById(C0000R.id.banner_layout);
        this.cu = new bb(this, dVar, this.uF);
        this.wq = null;
        Bundle extras = getIntent().getExtras();
        if (!extras.getBoolean("is_network_game")) {
            if (!extras.getBoolean("is_local_game")) {
                if (extras.getBoolean("is_story_game")) {
                    switch (extras.getInt("script_id")) {
                        case 1:
                        case 2:
                        case 3:
                            bb bbVar = this.cu;
                            extras.getInt("script_id");
                            this.wq = new r(this, bbVar);
                            break;
                    }
                }
            } else if (extras.getInt("type") == 4) {
                c cVar = (c) dVar.cU().lq.get("tournament_detail");
                if (cVar == null) {
                    this.wq = new l(this, this.cu, extras.getInt("tournament_type"));
                } else {
                    this.wq = new l(this, this.cu, cVar);
                    this.cu.a(this.wq);
                    cVar.on++;
                    this.cu.b(false, false);
                }
            } else {
                this.wq = new k(this, this.cu, i);
            }
        } else {
            this.wq = new q(this, this.cu, i);
        }
        this.cu.a(this.wq);
        relativeLayout.addView(this.cu);
        this.uF.bringToFront();
        setContentView(relativeLayout);
        MJ16Activity.ri.eraseColor(-16777216);
        return this.cu;
    }

    /* JADX WARNING: Removed duplicated region for block: B:63:0x019a A[SYNTHETIC, Splitter:B:63:0x019a] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x01ab A[SYNTHETIC, Splitter:B:73:0x01ab] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01b0 A[SYNTHETIC, Splitter:B:76:0x01b0] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(boolean r14, android.graphics.Bitmap r15) {
        /*
            r13 = this;
            r11 = 0
            monitor-enter(r13)
            r0 = 0
            java.io.File r1 = new java.io.File     // Catch:{ all -> 0x019f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019f }
            r2.<init>()     // Catch:{ all -> 0x019f }
            java.io.File r3 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ all -> 0x019f }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019f }
            java.lang.String r3 = "/Mahjong_and_Friends/Screen_Shot"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019f }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019f }
            java.lang.String r3 = ""
            r1.<init>(r2, r3)     // Catch:{ all -> 0x019f }
            r13.wj = r1     // Catch:{ all -> 0x019f }
            java.io.File r1 = r13.wj     // Catch:{ all -> 0x019f }
            boolean r1 = r1.exists()     // Catch:{ all -> 0x019f }
            if (r1 != 0) goto L_0x0030
            java.io.File r1 = r13.wj     // Catch:{ all -> 0x019f }
            r1.mkdirs()     // Catch:{ all -> 0x019f }
        L_0x0030:
            if (r14 == 0) goto L_0x0118
            java.io.File r1 = new java.io.File     // Catch:{ all -> 0x019f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x019f }
            r2.<init>()     // Catch:{ all -> 0x019f }
            java.io.File r3 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ all -> 0x019f }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019f }
            java.lang.String r3 = "/Mahjong_and_Friends/Screen_Shot"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x019f }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x019f }
            java.lang.String r3 = "tmp_shareZZZ.jpg"
            r1.<init>(r2, r3)     // Catch:{ all -> 0x019f }
            r13.wj = r1     // Catch:{ all -> 0x019f }
            java.io.File r1 = r13.wj     // Catch:{ all -> 0x019f }
            boolean r1 = r1.exists()     // Catch:{ all -> 0x019f }
            if (r1 == 0) goto L_0x005f
            java.io.File r1 = r13.wj     // Catch:{ all -> 0x019f }
            r1.delete()     // Catch:{ all -> 0x019f }
        L_0x005f:
            java.io.File r1 = r13.wj     // Catch:{ Exception -> 0x021c, all -> 0x022d }
            boolean r1 = r1.exists()     // Catch:{ Exception -> 0x021c, all -> 0x022d }
            if (r1 != 0) goto L_0x01ef
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x021c, all -> 0x022d }
            java.io.File r2 = r13.wj     // Catch:{ Exception -> 0x021c, all -> 0x022d }
            r1.<init>(r2)     // Catch:{ Exception -> 0x021c, all -> 0x022d }
            android.graphics.Bitmap$CompressFormat r0 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r2 = 75
            boolean r0 = r15.compress(r0, r2, r1)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            if (r0 == 0) goto L_0x01a2
            if (r14 != 0) goto L_0x0094
            int r2 = com.epoint.android.games.mjfgbfree.ai.nk     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r3 = 4
            if (r2 != r3) goto L_0x015b
            java.lang.String r2 = "截圖已儲存至"
            java.lang.String r2 = com.epoint.android.games.mjfgbfree.af.aa(r2)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.String r3 = "XX"
            java.io.File r4 = r13.wj     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.String r4 = r4.getAbsolutePath()     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.String r2 = r2.replace(r3, r4)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r13.aB(r2)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
        L_0x0094:
            r1.flush()     // Catch:{ IOException -> 0x021f }
        L_0x0097:
            r1.close()     // Catch:{ IOException -> 0x0222 }
        L_0x009a:
            if (r0 == 0) goto L_0x0239
            if (r14 == 0) goto L_0x0239
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.String r2 = "android.intent.action.SEND"
            r0.<init>(r2)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.String r2 = "image/jpeg"
            r0.setType(r2)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.io.File r2 = r13.wj     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            android.net.Uri r2 = android.net.Uri.fromFile(r2)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.String r3 = "android.intent.extra.STREAM"
            r0.putExtra(r3, r2)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.util.Vector r2 = new java.util.Vector     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r2.<init>()     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.util.Vector r3 = new java.util.Vector     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r3.<init>()     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.util.Vector r4 = new java.util.Vector     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r4.<init>()     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            android.content.pm.PackageManager r5 = r13.getPackageManager()     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r6 = 0
            java.util.List r6 = r5.queryIntentActivities(r0, r6)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r7 = r11
        L_0x00ce:
            int r0 = r6.size()     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            if (r7 < r0) goto L_0x01b4
            android.widget.ListView r5 = new android.widget.ListView     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r5.<init>(r13)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r0 = 0
            r5.setCacheColorHint(r0)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            com.epoint.android.games.mjfgbfree.ui.entity.c r0 = new com.epoint.android.games.mjfgbfree.ui.entity.c     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r0.<init>(r13, r3, r4, r2)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r5.setAdapter(r0)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            com.epoint.android.games.mjfgbfree.s r0 = new com.epoint.android.games.mjfgbfree.s     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r0.<init>(r13)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r5.setOnItemClickListener(r0)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            int r0 = r3.size()     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.CharSequence[] r2 = new java.lang.CharSequence[r0]     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r4 = r11
        L_0x00f4:
            int r0 = r2.length     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            if (r4 < r0) goto L_0x01e2
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r0.<init>(r13)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.String r2 = "分享截圖"
            java.lang.String r2 = com.epoint.android.games.mjfgbfree.af.aa(r2)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r0.setTitle(r2)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r0.setView(r5)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r13.a(r0)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r0 = r1
        L_0x010c:
            if (r0 == 0) goto L_0x0111
            r0.close()     // Catch:{ IOException -> 0x022a }
        L_0x0111:
            if (r15 == 0) goto L_0x0116
            r15.recycle()     // Catch:{ all -> 0x019f }
        L_0x0116:
            monitor-exit(r13)
            return
        L_0x0118:
            r1 = 1
        L_0x0119:
            r2 = 2000(0x7d0, float:2.803E-42)
            if (r1 > r2) goto L_0x005f
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x019f }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x019f }
            r3.<init>()     // Catch:{ all -> 0x019f }
            java.io.File r4 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ all -> 0x019f }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x019f }
            java.lang.String r4 = "/Mahjong_and_Friends/Screen_Shot"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x019f }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x019f }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x019f }
            java.lang.String r5 = "screen_"
            r4.<init>(r5)     // Catch:{ all -> 0x019f }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ all -> 0x019f }
            java.lang.String r5 = ".jpg"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x019f }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x019f }
            r2.<init>(r3, r4)     // Catch:{ all -> 0x019f }
            r13.wj = r2     // Catch:{ all -> 0x019f }
            java.io.File r2 = r13.wj     // Catch:{ all -> 0x019f }
            boolean r2 = r2.exists()     // Catch:{ all -> 0x019f }
            if (r2 == 0) goto L_0x005f
            int r1 = r1 + 1
            goto L_0x0119
        L_0x015b:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.String r3 = "截圖已儲存至"
            java.lang.String r3 = com.epoint.android.games.mjfgbfree.af.aa(r3)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.String r3 = ": "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.io.File r3 = r13.wj     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.String r3 = r3.getAbsolutePath()     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r13.aB(r2)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            goto L_0x0094
        L_0x0183:
            r0 = move-exception
            r0 = r1
        L_0x0185:
            java.lang.String r1 = "截圖失敗"
            java.lang.String r1 = com.epoint.android.games.mjfgbfree.af.aa(r1)     // Catch:{ all -> 0x0233 }
            r2 = 0
            android.widget.Toast r1 = android.widget.Toast.makeText(r13, r1, r2)     // Catch:{ all -> 0x0233 }
            r1.show()     // Catch:{ all -> 0x0233 }
            if (r0 == 0) goto L_0x0198
            r0.close()     // Catch:{ IOException -> 0x0225 }
        L_0x0198:
            if (r15 == 0) goto L_0x0116
            r15.recycle()     // Catch:{ all -> 0x019f }
            goto L_0x0116
        L_0x019f:
            r0 = move-exception
            monitor-exit(r13)
            throw r0
        L_0x01a2:
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r0.<init>()     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            throw r0     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
        L_0x01a8:
            r0 = move-exception
        L_0x01a9:
            if (r1 == 0) goto L_0x01ae
            r1.close()     // Catch:{ IOException -> 0x0228 }
        L_0x01ae:
            if (r15 == 0) goto L_0x01b3
            r15.recycle()     // Catch:{ all -> 0x019f }
        L_0x01b3:
            throw r0     // Catch:{ all -> 0x019f }
        L_0x01b4:
            java.lang.Object r0 = r6.get(r7)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            android.content.pm.ResolveInfo r0 = (android.content.pm.ResolveInfo) r0     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            android.content.pm.ActivityInfo r8 = r0.activityInfo     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            android.content.ComponentName r9 = new android.content.ComponentName     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            android.content.pm.ApplicationInfo r10 = r8.applicationInfo     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.String r10 = r10.packageName     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.String r8 = r8.name     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r9.<init>(r10, r8)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r2.addElement(r9)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.CharSequence r9 = r0.loadLabel(r5)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r8.<init>(r9)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r3.addElement(r8)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            android.graphics.drawable.Drawable r0 = r0.loadIcon(r5)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r4.addElement(r0)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            int r0 = r7 + 1
            r7 = r0
            goto L_0x00ce
        L_0x01e2:
            java.lang.Object r0 = r3.elementAt(r4)     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            r2[r4] = r0     // Catch:{ Exception -> 0x0183, all -> 0x01a8 }
            int r0 = r4 + 1
            r4 = r0
            goto L_0x00f4
        L_0x01ef:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x021c, all -> 0x022d }
            java.lang.String r2 = "截圖失敗"
            java.lang.String r2 = com.epoint.android.games.mjfgbfree.af.aa(r2)     // Catch:{ Exception -> 0x021c, all -> 0x022d }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x021c, all -> 0x022d }
            r1.<init>(r2)     // Catch:{ Exception -> 0x021c, all -> 0x022d }
            java.lang.String r2 = ": "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x021c, all -> 0x022d }
            java.lang.String r2 = "太多檔案"
            java.lang.String r2 = com.epoint.android.games.mjfgbfree.af.aa(r2)     // Catch:{ Exception -> 0x021c, all -> 0x022d }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x021c, all -> 0x022d }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x021c, all -> 0x022d }
            r2 = 0
            android.widget.Toast r1 = android.widget.Toast.makeText(r13, r1, r2)     // Catch:{ Exception -> 0x021c, all -> 0x022d }
            r1.show()     // Catch:{ Exception -> 0x021c, all -> 0x022d }
            goto L_0x010c
        L_0x021c:
            r1 = move-exception
            goto L_0x0185
        L_0x021f:
            r2 = move-exception
            goto L_0x0097
        L_0x0222:
            r2 = move-exception
            goto L_0x009a
        L_0x0225:
            r0 = move-exception
            goto L_0x0198
        L_0x0228:
            r1 = move-exception
            goto L_0x01ae
        L_0x022a:
            r0 = move-exception
            goto L_0x0111
        L_0x022d:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x01a9
        L_0x0233:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x01a9
        L_0x0239:
            r0 = r1
            goto L_0x010c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.MJ16ViewActivity.a(boolean, android.graphics.Bitmap):void");
    }

    public final e eL() {
        return this.wp;
    }

    public final synchronized void eM() {
        Message message = new Message();
        message.what = 2;
        this.mHandler.sendMessage(message);
    }

    public void finish() {
        Message message = new Message();
        message.what = 3;
        this.mHandler.sendMessage(message);
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        switch (i) {
            case 1:
                if (i2 == -1) {
                    Vector vector = new Vector();
                    ad = vector;
                    vector.add(new ag((BumpConnection) intent.getParcelableExtra("EXTRA_CONNECTION")));
                    new com.epoint.android.games.mjfgbfree.c.a(ad, this, this.cn, intent != null ? intent.getBooleanExtra("is_restore_game", false) : false);
                    return;
                } else if (intent != null) {
                    intent.getSerializableExtra("EXTRA_REASON");
                    finish();
                    return;
                } else {
                    return;
                }
            case 2:
            case 3:
                if (i2 == -1) {
                    new com.epoint.android.games.mjfgbfree.c.a(ad, this, this.cn, intent != null ? intent.getBooleanExtra("is_restore_game", false) : false);
                    return;
                }
                if (ad != null) {
                    for (int i3 = 0; i3 < ad.size(); i3++) {
                        ((aa) ad.elementAt(i3)).l();
                    }
                    ad = null;
                }
                finish();
                return;
            case 4:
                if (i2 == -1) {
                    getIntent().putExtra("tournament_type", intent.getIntExtra("tournament_type", 0));
                    try {
                        new f(this, 4, intent.getExtras());
                        return;
                    } catch (com.epoint.android.games.mjfgbfree.d.f e) {
                        e.printStackTrace();
                    }
                }
                finish();
                return;
            case 5:
                MJ16Activity.a(this);
                if (i2 != -1 || !intent.getBooleanExtra("IS_RESTART", false)) {
                    this.cu.dY();
                    return;
                }
                intent.removeExtra("IS_RESTART");
                startActivityForResult(intent, i);
                return;
            default:
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        if (MJ16Activity.dc() && this.cu != null) {
            bb.dW();
            this.cu.er();
        }
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (ai.nt) {
            requestWindowFeature(1);
            getWindow().setFlags(1024, 1024);
        }
        bb.dQ();
        if (MJ16Activity.ri == null) {
            finish();
            return;
        }
        if (MJ16Activity.rd) {
            setRequestedOrientation(0);
        } else {
            setRequestedOrientation(ai.nf);
        }
        Bundle extras = getIntent().getExtras();
        if (extras.getBoolean("is_local_game") || extras.getBoolean("is_story_game")) {
            for (int i = 0; i < bb.tG.length; i++) {
                String str = bb.tG[i];
                int random = (int) (Math.random() * ((double) bb.tG.length));
                bb.tG[i] = bb.tG[random];
                bb.tG[random] = str;
            }
            for (int i2 = 0; i2 < bb.tH.length; i2++) {
                bb.tH[i2] = bb.tG[i2];
            }
            int i3 = extras.getInt("type");
            boolean z = extras.getBoolean("is_restore");
            if (i3 != 4 || z) {
                try {
                    new f(this, i3, extras);
                } catch (com.epoint.android.games.mjfgbfree.d.f e) {
                    extras.putBoolean("is_restore", false);
                    startActivityForResult(new Intent(this, TournamentActivity.class), 4);
                }
            } else {
                startActivityForResult(new Intent(this, TournamentActivity.class), 4);
            }
        } else {
            this.cn = extras.getString("local_chara");
            if (extras.getBoolean("is_bump_game")) {
                Intent intent = new Intent(this, BumpAPI.class);
                intent.putExtra("EXTRA_API_KEY", "fce5f163a36c446db5f697495383d095");
                startActivityForResult(intent, 1);
            } else if (extras.getBoolean("is_tcp_game")) {
                startActivityForResult(new Intent(this, TCPServerConnectionActivity.class), 2);
            } else if (extras.getBoolean("is_tcp_localhost_game")) {
                startActivityForResult(new Intent(this, TCPLocalHostConnectionActivity.class), 2);
            } else if (extras.getBoolean("is_bt_game")) {
                startActivityForResult(new Intent(this, BTConnectionActivity.class), 3);
            }
        }
        this.wp = new e();
        if (ai.nu.equals("random_bg_s")) {
            String[] stringArray = getResources().getStringArray(C0000R.array.wallpaper_values);
            MJ16Activity.rg = stringArray[(int) (Math.random() * ((double) (stringArray.length - 1)))];
        } else {
            MJ16Activity.rg = ai.nu;
        }
        this.wp = new e();
        if (MJ16Activity.rg.equals("ext_bg1_s")) {
            this.wp.sn = Color.rgb(65, 14, 7);
        } else if (MJ16Activity.rg.equals("ext_bg2_s")) {
            this.wp.sn = Color.rgb(163, 86, 12);
        } else if (MJ16Activity.rg.equals("ext_bg3_s")) {
            this.wp.sn = Color.rgb(12, 18, 65);
        } else if (MJ16Activity.rg.equals("ext_bg4_s")) {
            this.wp.sn = -1;
        } else if (MJ16Activity.rg.equals("ext_bg5_s")) {
            this.wp.sn = -1;
        } else {
            this.wp.sn = Color.rgb(20, 120, 0);
        }
        if (ai.no.equals("0")) {
            String[] stringArray2 = getResources().getStringArray(C0000R.array.tile_color_values);
            this.wp.sm = Integer.valueOf(stringArray2[(int) (Math.random() * ((double) (stringArray2.length - 1)))]).intValue();
        } else {
            this.wp.sm = Integer.valueOf(ai.no).intValue();
        }
        this.wp.so = null;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 11, 0, (CharSequence) null).setIcon(17301581);
        menu.add(0, 9, 0, (CharSequence) null).setIcon(17301569);
        menu.add(0, 10, 0, (CharSequence) null).setIcon(17301583);
        menu.add(0, 5, 0, (CharSequence) null).setIcon(17301570);
        menu.add(0, 0, 0, (CharSequence) null).setIcon(17301568);
        return super.onCreateOptionsMenu(menu);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.cu != null) {
            this.cu.dS();
        }
        Enumeration keys = bb.tE.keys();
        while (keys.hasMoreElements()) {
            ((Bitmap) bb.tE.remove(keys.nextElement())).recycle();
        }
        if (this.cu == null) {
            return;
        }
        if (this.cu.eu() instanceof com.epoint.android.games.mjfgbfree.c.a) {
            this.cu = null;
            ((com.epoint.android.games.mjfgbfree.c.a) this.cu.eu()).a((aa) null);
        } else if ((this.cu.eu() instanceof f) && getIntent().getExtras().getBoolean("is_local_game")) {
            this.cu.o(true);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            if (this.cu.uJ) {
                return true;
            }
            if (bb.eA() || bb.dU()) {
                return true;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(String.valueOf(af.aa("雀友麻雀")) + " Free");
            builder.setMessage(String.valueOf(af.aa("返回主畫面")) + "?");
            builder.setPositiveButton(af.aa("是"), new u(this));
            builder.setNegativeButton(af.aa("否"), new t(this));
            builder.create().show();
            return true;
        } else if (i == 84) {
            return true;
        } else {
            return super.onKeyDown(i, keyEvent);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        String str;
        switch (menuItem.getItemId()) {
            case 0:
                startActivity(new Intent(this, InstructionActivity.class));
                return true;
            case 5:
                Intent intent = new Intent(this, PreferencesActivity.class);
                intent.putExtra("is_playing", true);
                startActivityForResult(intent, 5);
                return true;
            case 8:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Title");
                builder.setMessage("Message");
                EditText editText = new EditText(this);
                b bVar = this.cu.eu().cU().bN()[d.qB];
                editText.setText(String.valueOf(bVar.gd));
                builder.setView(editText);
                builder.setPositiveButton("Ok", new z(this, editText, bVar));
                builder.setNegativeButton("Cancel", new y(this));
                builder.show();
                break;
            case 9:
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                if (this.wq instanceof l) {
                    c cM = ((l) this.wq).cM();
                    str = String.valueOf(String.valueOf("") + c.D(cM.oo) + " - " + cM.cy() + "\n") + (cM.cA() == null ? "" : String.valueOf(af.Z("最低晉級排名")) + ": " + cM.cz().gF + "\n");
                } else {
                    str = "";
                }
                g P = this.cu.eu().P();
                if (P != null && P.kS > 0) {
                    str = String.valueOf(str) + af.Z("起胡番數") + ": " + P.kS;
                }
                builder2.setMessage(str);
                builder2.setPositiveButton(af.aa("好"), new v(this));
                builder2.show();
                return true;
            case 10:
                this.cu.ej();
                return true;
            case 11:
                AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
                builder3.setMessage(String.valueOf(String.valueOf(af.Z("用2張遊戲劵重新挑戰此階段")) + "?") + "\n" + af.Z("剩餘遊戲劵") + ": " + MJ16Activity.rp);
                builder3.setPositiveButton(af.aa("是"), new x(this));
                builder3.setNegativeButton(af.aa("否"), new w(this));
                builder3.show();
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void onPause() {
        super.onPause();
        if (this.cu != null) {
            this.cu.dS();
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            switch (item.getItemId()) {
                case 0:
                    item.setTitle(af.aa("遊戲規則"));
                    break;
                case 5:
                    item.setTitle(af.aa("設定"));
                    break;
                case 9:
                    if (this.cu != null && this.cu.eu().cS() == 4 && this.cu.tD != 0) {
                        item.setTitle(af.aa("遊戲資訊"));
                        item.setVisible(true);
                        break;
                    } else {
                        item.setVisible(false);
                        break;
                    }
                    break;
                case 10:
                    if (this.cu != null && this.cu.eu().P() != null && this.cu.eu().P().vM.size() > 0 && this.cu.tD != 0) {
                        item.setTitle(af.aa("已打出的牌"));
                        item.setVisible(true);
                        break;
                    } else {
                        item.setVisible(false);
                        break;
                    }
                case 11:
                    if (this.cu != null && this.cu.eu().cS() == 4 && this.cu.tD != 0 && !bb.eA() && !bb.dU() && !this.cu.eu().P().le) {
                        c cM = ((l) this.wq).cM();
                        if (cM.cA() != null && MJ16Activity.rp >= 2 && cM.oo != 5) {
                            item.setTitle(String.valueOf(af.Z("重新挑戰")) + "...");
                            item.setVisible(true);
                            break;
                        } else {
                            item.setVisible(false);
                            break;
                        }
                    } else {
                        item.setVisible(false);
                        break;
                    }
                    break;
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public void onResume() {
        super.onResume();
        if (this.cu != null) {
            this.cu.dT();
        }
    }
}
