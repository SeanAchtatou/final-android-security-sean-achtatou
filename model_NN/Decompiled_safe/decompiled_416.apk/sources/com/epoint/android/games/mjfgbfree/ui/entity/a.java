package com.epoint.android.games.mjfgbfree.ui.entity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.epoint.android.games.mjfgbfree.C0000R;
import java.util.Vector;

public final class a extends BaseAdapter {
    private Vector E;
    private LayoutInflater cY;

    public a(Context context, Vector vector) {
        this.cY = LayoutInflater.from(context);
        this.E = vector;
    }

    public final int getCount() {
        return this.E.size();
    }

    public final Object getItem(int i) {
        return this.E.elementAt(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        e eVar;
        d dVar = (d) this.E.elementAt(i);
        if (view == null) {
            view2 = this.cY.inflate((int) C0000R.layout.item_grid_element, (ViewGroup) null);
            eVar = new e(this);
            eVar.vs = (ImageView) view2.findViewById(C0000R.id.image);
            eVar.vt = (TextView) view2.findViewById(C0000R.id.title);
            eVar.vu = (TextView) view2.findViewById(C0000R.id.subtitle);
            view2.setTag(eVar);
        } else {
            view2 = view;
            eVar = (e) view.getTag();
        }
        eVar.vs.setImageBitmap(dVar.aL);
        eVar.vt.setText(dVar.sv);
        eVar.vu.setText(dVar.sw);
        eVar.vv = dVar;
        return view2;
    }
}
