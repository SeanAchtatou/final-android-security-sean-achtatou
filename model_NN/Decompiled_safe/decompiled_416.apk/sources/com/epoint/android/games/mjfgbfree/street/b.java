package com.epoint.android.games.mjfgbfree.street;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.epoint.android.games.mjfgbfree.C0000R;
import java.util.Vector;

final class b extends BaseAdapter {
    private Vector E;
    private /* synthetic */ PlacesMapActivity aP;
    private LayoutInflater cY;

    public b(PlacesMapActivity placesMapActivity, Context context, Vector vector) {
        this.aP = placesMapActivity;
        this.cY = LayoutInflater.from(context);
        this.E = vector;
    }

    public final int getCount() {
        return this.E.size();
    }

    public final Object getItem(int i) {
        return this.E.elementAt(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        a aVar = (a) this.E.elementAt(i);
        View inflate = view == null ? this.cY.inflate((int) C0000R.layout.place_element, (ViewGroup) null) : view;
        ((TextView) inflate.findViewById(C0000R.id.place)).setText(aVar.aM);
        ((TextView) inflate.findViewById(C0000R.id.address)).setText(aVar.aN);
        ((TextView) inflate.findViewById(C0000R.id.message)).setText("People: " + aVar.aO);
        inflate.setTag(aVar);
        return inflate;
    }
}
