package com.epoint.android.games.mjfgbfree.ui.entity;

import android.graphics.Bitmap;

public final class d {
    public Bitmap aL;
    public String sv;
    public String sw;
    private int sx;

    public d(Bitmap bitmap, String str, String str2, int i) {
        this.aL = bitmap;
        this.sv = str;
        this.sw = str2;
        this.sx = i;
    }
}
