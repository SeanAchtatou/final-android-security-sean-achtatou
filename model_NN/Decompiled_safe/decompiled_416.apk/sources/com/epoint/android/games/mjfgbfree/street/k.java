package com.epoint.android.games.mjfgbfree.street;

import android.os.Message;
import android.util.Log;
import com.a.a.a.g;
import com.a.a.a.j;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

final class k implements j {
    private /* synthetic */ StreetMainActivity fP;

    /* synthetic */ k(StreetMainActivity streetMainActivity) {
        this(streetMainActivity, (byte) 0);
    }

    private k(StreetMainActivity streetMainActivity, byte b2) {
        this.fP = streetMainActivity;
    }

    public final void a(g gVar) {
        Log.e("Facebook", gVar.getMessage());
        gVar.printStackTrace();
    }

    public final void a(FileNotFoundException fileNotFoundException) {
        Log.e("Facebook", fileNotFoundException.getMessage());
        fileNotFoundException.printStackTrace();
    }

    public final void a(IOException iOException) {
        Log.e("Facebook", iOException.getMessage());
        iOException.printStackTrace();
    }

    public final void a(MalformedURLException malformedURLException) {
        Log.e("Facebook", malformedURLException.getMessage());
        malformedURLException.printStackTrace();
    }

    public final void ay(String str) {
        if (str.equals("logout")) {
            Message message = new Message();
            message.what = 1000;
            this.fP.a(message);
        }
    }
}
