package com.epoint.android.games.mjfgbfree.network;

import android.app.AlertDialog;
import android.view.View;
import android.widget.LinearLayout;
import com.epoint.android.games.mjfgbfree.af;

final class y implements View.OnClickListener {
    private final /* synthetic */ LinearLayout fp;
    final /* synthetic */ TCPLocalHostConnectionActivity hZ;

    y(TCPLocalHostConnectionActivity tCPLocalHostConnectionActivity, LinearLayout linearLayout) {
        this.hZ = tCPLocalHostConnectionActivity;
        this.fp = linearLayout;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.hZ);
        builder.setMessage(String.valueOf(af.aa("確定刪除")) + "?");
        builder.setPositiveButton(af.aa("是"), new af(this, view, this.fp));
        builder.setNegativeButton(af.aa("否"), new ae(this));
        builder.create().show();
    }
}
