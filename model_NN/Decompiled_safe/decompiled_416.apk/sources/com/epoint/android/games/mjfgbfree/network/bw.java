package com.epoint.android.games.mjfgbfree.network;

import android.app.AlertDialog;
import android.view.View;
import android.widget.LinearLayout;
import com.epoint.android.games.mjfgbfree.af;

final class bw implements View.OnClickListener {
    private final /* synthetic */ LinearLayout fp;
    final /* synthetic */ TCPServerConnectionActivity sh;

    bw(TCPServerConnectionActivity tCPServerConnectionActivity, LinearLayout linearLayout) {
        this.sh = tCPServerConnectionActivity;
        this.fp = linearLayout;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.sh);
        builder.setMessage(String.valueOf(af.aa("確定刪除")) + "?");
        builder.setPositiveButton(af.aa("是"), new h(this, view, this.fp));
        builder.setNegativeButton(af.aa("否"), new k(this));
        builder.create().show();
    }
}
