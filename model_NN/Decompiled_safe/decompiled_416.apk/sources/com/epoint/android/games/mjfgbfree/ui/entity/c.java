package com.epoint.android.games.mjfgbfree.ui.entity;

import android.content.ComponentName;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.epoint.android.games.mjfgbfree.C0000R;
import java.util.Hashtable;
import java.util.Vector;

public final class c extends BaseAdapter {
    private LayoutInflater cY;
    private int color = -1;
    private Vector hq;
    private Vector hr;
    private Vector qu = null;
    private Hashtable qv = new Hashtable();
    private b qw = null;
    private int qx = 0;

    public c(Context context, Vector vector, Vector vector2, Vector vector3) {
        this.cY = LayoutInflater.from(context);
        this.hq = vector;
        this.hr = vector2;
        this.qu = vector3;
    }

    public final void cQ() {
        this.color = -16777216;
    }

    public final int getCount() {
        return this.hq.size();
    }

    public final Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        f fVar;
        View view2;
        if (view == null) {
            view2 = this.cY.inflate((int) C0000R.layout.list_item_icon_text, (ViewGroup) null);
            f fVar2 = new f(this);
            fVar2.Bw = (TextView) view2.findViewById(C0000R.id.text);
            fVar2.Bw.setTextColor(this.color);
            if (this.qx != 0) {
                fVar2.Bw.setTextSize(1, (float) this.qx);
            }
            fVar2.Bx = (ImageView) view2.findViewById(C0000R.id.icon);
            view2.setTag(fVar2);
            fVar2.Bw.setText((CharSequence) this.hq.elementAt(i));
            fVar2.Bx.setImageDrawable((Drawable) this.hr.elementAt(i));
            if (this.qw != null) {
                this.hq.elementAt(i);
                fVar = fVar2;
            } else {
                fVar = fVar2;
            }
        } else {
            fVar = (f) view.getTag();
            fVar.Bw.setText((CharSequence) this.hq.elementAt(i));
            fVar.Bx.setImageDrawable((Drawable) this.hr.elementAt(i));
            view2 = view;
        }
        this.qv.put(Integer.valueOf(i), fVar);
        if (this.qu != null) {
            fVar.By = (ComponentName) this.qu.elementAt(i);
        }
        return view2;
    }
}
