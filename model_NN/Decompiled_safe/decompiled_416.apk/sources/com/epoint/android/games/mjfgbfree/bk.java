package com.epoint.android.games.mjfgbfree;

import android.os.Message;

final class bk extends Thread {
    private /* synthetic */ bb gV;
    private int zM;
    private int zN;

    public bk(bb bbVar, int i, int i2) {
        this.gV = bbVar;
        this.zM = i;
        this.zN = i2;
    }

    public final void run() {
        try {
            Thread.sleep((long) this.zM);
            while (this.gV.jq == null) {
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    return;
                }
            }
            if (!this.gV.uH) {
                Message message = new Message();
                message.what = this.zN;
                this.gV.mHandler.sendMessage(message);
            }
        } catch (InterruptedException e2) {
        }
    }
}
