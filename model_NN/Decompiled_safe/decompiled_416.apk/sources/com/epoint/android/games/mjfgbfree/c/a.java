package com.epoint.android.games.mjfgbfree.c;

import b.a.b;
import b.a.f;
import com.epoint.android.games.mjfgbfree.MJ16ViewActivity;
import com.epoint.android.games.mjfgbfree.a.h;
import com.epoint.android.games.mjfgbfree.af;
import com.epoint.android.games.mjfgbfree.ai;
import com.epoint.android.games.mjfgbfree.bb;
import com.epoint.android.games.mjfgbfree.d.g;
import com.epoint.android.games.mjfgbfree.network.aa;
import com.epoint.android.games.mjfgbfree.network.bf;
import com.epoint.android.games.mjfgbfree.network.t;
import java.util.Vector;

public final class a extends d implements t {
    public static final int[] co = {2, 1, 3};
    private String cn;
    public String[] cp = new String[4];
    /* access modifiers changed from: private */
    public boolean[] cq = {true, true, true, true};
    private int cr;
    private Vector cs;
    private MJ16ViewActivity ct;
    /* access modifiers changed from: private */
    public bb cu;
    /* access modifiers changed from: private */
    public Vector cv = new Vector();
    /* access modifiers changed from: private */
    public volatile Thread cw;
    /* access modifiers changed from: private */
    public boolean cx = true;
    private boolean cy = false;
    private a.b.d.a cz;

    public a(Vector vector, MJ16ViewActivity mJ16ViewActivity, String str, boolean z) {
        super(3);
        this.cn = str;
        this.cs = vector;
        this.ct = mJ16ViewActivity;
        if (vector.size() == 0) {
            mJ16ViewActivity.finish();
            return;
        }
        if (z) {
            Object b2 = com.epoint.android.games.mjfgbfree.f.a.b(mJ16ViewActivity, this.qE);
            if (b2 instanceof a.b.d.a) {
                String[] strArr = (String[]) ((a.b.d.a) b2).lq.get("uuid");
                int i = 0;
                for (String str2 : strArr) {
                    if (str2 != null) {
                        i++;
                    }
                }
                if (strArr != null && i == this.cs.size()) {
                    aa[] aaVarArr = new aa[4];
                    int i2 = 0;
                    int i3 = 0;
                    while (i2 < this.cs.size()) {
                        String p = ((aa) this.cs.elementAt(i2)).p();
                        int i4 = 0;
                        while (true) {
                            if (i4 < strArr.length) {
                                if (p != null && p.equals(strArr[i4])) {
                                    i3++;
                                    aaVarArr[i4] = (aa) this.cs.elementAt(i2);
                                    break;
                                }
                                i4++;
                            } else {
                                break;
                            }
                        }
                        i2++;
                        i3 = i3;
                    }
                    if (i3 == this.cs.size()) {
                        this.cs.removeAllElements();
                        for (int i5 = 0; i5 < aaVarArr.length; i5++) {
                            if (aaVarArr[i5] != null) {
                                this.cs.addElement(aaVarArr[i5]);
                            }
                        }
                        this.cz = (a.b.d.a) b2;
                    }
                }
            }
        }
        for (int i6 = 0; i6 < this.cs.size(); i6++) {
            aa aaVar = (aa) this.cs.elementAt(i6);
            this.cp[i6] = aaVar.p();
            aaVar.a(this);
            if (aaVar.n() == 2) {
                this.cr = ((int) (999999.0d * Math.random())) + 3;
                aaVar.d(a(this.cr, ai.nm).toString());
            } else if (aaVar.n() == 0) {
                aaVar.d(a(1, ai.nm).toString());
            } else if (aaVar.m()) {
                mJ16ViewActivity.finish();
                return;
            }
        }
    }

    private void R() {
        this.cu = this.ct.a(this, ai.nr);
        try {
            b bVar = new b();
            b bVar2 = new b();
            for (int i = 0; i < this.cs.size(); i++) {
                aa aaVar = (aa) this.cs.elementAt(i);
                bVar2.c(aaVar.p(), aaVar.n());
            }
            bVar.a("seat_mapping", bVar2);
            b bVar3 = new b();
            b.a.a aVar = new b.a.a();
            aVar.b(this.cn);
            for (String b2 : bb.tH) {
                aVar.b(b2);
            }
            bVar3.a("chara_mapping", aVar);
            b a2 = af.a(this.qC);
            a2.c("tile_color", this.ct.eL().sm);
            a2.c("table_color", this.ct.eL().sn);
            a2.c("rounds", this.cu.cI());
            for (int i2 = 0; i2 < this.cs.size(); i2++) {
                aa aaVar2 = (aa) this.cs.elementAt(i2);
                this.cq[aaVar2.n()] = false;
                if (aaVar2 instanceof bf) {
                    aaVar2.d(bVar.toString());
                }
                aaVar2.d(bVar3.toString());
                a(aaVar2, a2);
            }
        } catch (f e) {
            e.printStackTrace();
        }
    }

    private b a(int i, String str) {
        b bVar = new b();
        try {
            bVar.a("command", "InitHost");
            bVar.c("rank", i);
            bVar.a("name", str);
            bVar.a("chara", this.cn);
        } catch (f e) {
            e.printStackTrace();
        }
        return bVar;
    }

    /* access modifiers changed from: private */
    public static b a(b bVar) {
        b N = bVar.N("mj16gameinfo");
        if (N != null && !N.H("data")) {
            try {
                return new b(h.bi(N.getString("data")));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return bVar;
    }

    private b a(String str, int i) {
        b a2 = a(0, str);
        try {
            a2.c("seat_pos", i);
        } catch (f e) {
            e.printStackTrace();
        }
        return a2;
    }

    private static void a(aa aaVar, b bVar) {
        b bVar2;
        b bVar3;
        try {
            String bh = h.bh(bVar.toString());
            b bVar4 = new b();
            bVar4.a("data", bh);
            bVar3 = new b();
            try {
                bVar3.a("mj16gameinfo", bVar4);
            } catch (Exception e) {
                Exception exc = e;
                bVar2 = bVar3;
                e = exc;
                e.printStackTrace();
                bVar3 = bVar2;
                aaVar.d(bVar3.toString());
            }
        } catch (Exception e2) {
            e = e2;
            bVar2 = bVar;
            e.printStackTrace();
            bVar3 = bVar2;
            aaVar.d(bVar3.toString());
        }
        aaVar.d(bVar3.toString());
    }

    private static b d(String str, String str2) {
        b bVar = new b();
        try {
            bVar.a("command", str);
            bVar.a("data", str2 == null ? "" : str2);
        } catch (f e) {
            e.printStackTrace();
        }
        return bVar;
    }

    public final int N() {
        if (this.qC == null) {
            return -1;
        }
        a.b.c.a.a bP = this.qC.bP();
        int ga = this.qC.bP().kr.ga();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.cs.size()) {
                return super.N();
            }
            if (ga != ((aa) this.cs.elementAt(i2)).n() || (bP.kw && !bP.ku && bP.kv == 0 && bP.kt.size() == 0 && !bP.ks && bP.kx == 0)) {
                i = i2 + 1;
            }
        }
        return -1;
    }

    public final boolean O() {
        if (qB != 0) {
            ((aa) this.cs.firstElement()).d(d("Next", (String) null).toString());
            this.cu.ew().d(false);
            this.cu.er();
        } else {
            this.cu.ew().d(false);
            super.O();
            try {
                b a2 = af.a(this.qC);
                for (int i = 0; i < this.cs.size(); i++) {
                    aa aaVar = (aa) this.cs.elementAt(i);
                    this.cq[aaVar.n()] = false;
                    a(aaVar, a2);
                }
            } catch (f e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public final g P() {
        g P = super.P();
        if (qB == 0 && P.le) {
            this.cu.ew().d(false);
        }
        return P;
    }

    public final boolean Q() {
        for (boolean z : this.cq) {
            if (!z) {
                return false;
            }
        }
        return true;
    }

    public final boolean S() {
        return this.cv.size() > 0;
    }

    public final boolean T() {
        return this.cx;
    }

    public final synchronized void a(aa aaVar) {
        if (this.cs.size() != 0) {
            b d = d("Quit", (String) null);
            for (int i = 0; i < this.cs.size(); i++) {
                aa aaVar2 = (aa) this.cs.elementAt(i);
                if (!aaVar2.m()) {
                    if (qB == 0 || aaVar == null) {
                        aaVar2.d(d.toString());
                    }
                    aaVar2.l();
                }
            }
            this.cs.clear();
            if (this.cu != null) {
                new e(this, d);
            } else {
                this.ct.finish();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x003f A[Catch:{ f -> 0x00e0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0165 A[Catch:{ f -> 0x00e0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.epoint.android.games.mjfgbfree.network.aa r13, java.lang.String r14) {
        /*
            r12 = this;
            r5 = 2
            r11 = -1
            r4 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r10 = 1
            r9 = 0
            b.a.b r1 = new b.a.b     // Catch:{ f -> 0x00e0 }
            r1.<init>(r14)     // Catch:{ f -> 0x00e0 }
            java.lang.String r0 = "command"
            boolean r0 = r1.H(r0)     // Catch:{ f -> 0x00e0 }
            if (r0 != 0) goto L_0x0191
            java.lang.String r0 = "command"
            java.lang.String r0 = r1.getString(r0)     // Catch:{ f -> 0x00e0 }
            java.lang.String r2 = "InitHost"
            boolean r2 = r0.equals(r2)     // Catch:{ f -> 0x00e0 }
            if (r2 == 0) goto L_0x0181
            java.lang.String r0 = "rank"
            int r2 = r1.getInt(r0)     // Catch:{ f -> 0x00e0 }
            java.lang.String r0 = "name"
            java.lang.String r3 = r1.getString(r0)     // Catch:{ f -> 0x00e0 }
            java.lang.String r0 = "chara"
            java.lang.String r4 = r1.getString(r0)     // Catch:{ f -> 0x00e0 }
            if (r2 <= r5) goto L_0x008f
            r0 = r10
        L_0x0036:
            if (r0 == 0) goto L_0x00bd
            int r0 = r12.cr     // Catch:{ f -> 0x00e0 }
            if (r2 <= r0) goto L_0x0091
            r0 = r10
        L_0x003d:
            if (r0 != r10) goto L_0x0165
            a.b.d.a r0 = r12.qC     // Catch:{ f -> 0x00e0 }
            if (r0 != 0) goto L_0x0082
            byte[] r0 = com.epoint.android.games.mjfgbfree.MJ16ViewActivity.wl     // Catch:{ f -> 0x00e0 }
            if (r0 == 0) goto L_0x00c0
            byte[] r0 = com.epoint.android.games.mjfgbfree.MJ16ViewActivity.wl     // Catch:{ f -> 0x00e0 }
            java.lang.Object r0 = com.epoint.android.games.mjfgbfree.a.d.a(r0)     // Catch:{ f -> 0x00e0 }
            a.b.d.a r0 = (a.b.d.a) r0     // Catch:{ f -> 0x00e0 }
            r12.qC = r0     // Catch:{ f -> 0x00e0 }
            a.b.d.a r0 = r12.qC     // Catch:{ f -> 0x00e0 }
            java.util.Hashtable r0 = r0.lq     // Catch:{ f -> 0x00e0 }
            java.lang.String r1 = "AllCardHash"
            java.lang.Object r0 = r0.get(r1)     // Catch:{ f -> 0x00e0 }
            java.util.Hashtable r0 = (java.util.Hashtable) r0     // Catch:{ f -> 0x00e0 }
            a.a.c.mu = r0     // Catch:{ f -> 0x00e0 }
            a.b.d.a r0 = r12.qC     // Catch:{ f -> 0x00e0 }
            java.util.Hashtable r0 = r0.lq     // Catch:{ f -> 0x00e0 }
            java.lang.String r1 = "AllCards"
            java.lang.Object r0 = r0.get(r1)     // Catch:{ f -> 0x00e0 }
            a.a.a[] r0 = (a.a.a[]) r0     // Catch:{ f -> 0x00e0 }
            a.a.c.mv = r0     // Catch:{ f -> 0x00e0 }
            r0 = 1
            r12.qD = r0     // Catch:{ f -> 0x00e0 }
        L_0x0070:
            r0 = 0
            com.epoint.android.games.mjfgbfree.c.a.qB = r0     // Catch:{ f -> 0x00e0 }
            a.b.d.a r0 = r12.qC     // Catch:{ f -> 0x00e0 }
            a.b.c.a.b[] r0 = r0.bN()     // Catch:{ f -> 0x00e0 }
            int r1 = com.epoint.android.games.mjfgbfree.c.a.qB     // Catch:{ f -> 0x00e0 }
            r0 = r0[r1]     // Catch:{ f -> 0x00e0 }
            java.lang.String r1 = com.epoint.android.games.mjfgbfree.ai.nm     // Catch:{ f -> 0x00e0 }
            r0.bc(r1)     // Catch:{ f -> 0x00e0 }
        L_0x0082:
            r1 = r10
            r2 = r9
        L_0x0084:
            java.util.Vector r0 = r12.cs     // Catch:{ f -> 0x00e0 }
            int r0 = r0.size()     // Catch:{ f -> 0x00e0 }
            if (r2 < r0) goto L_0x00f5
            if (r1 != 0) goto L_0x0145
        L_0x008e:
            return
        L_0x008f:
            r0 = r9
            goto L_0x0036
        L_0x0091:
            int r0 = r12.cr     // Catch:{ f -> 0x00e0 }
            if (r2 >= r0) goto L_0x0097
            r0 = r9
            goto L_0x003d
        L_0x0097:
            r5 = 4696837138094751744(0x412e847e00000000, double:999999.0)
            double r7 = java.lang.Math.random()     // Catch:{ f -> 0x00e0 }
            double r5 = r5 * r7
            int r0 = (int) r5     // Catch:{ f -> 0x00e0 }
            int r0 = r0 + 3
            r12.cr = r0     // Catch:{ f -> 0x00e0 }
            java.util.Vector r0 = r12.cs     // Catch:{ f -> 0x00e0 }
            java.lang.Object r0 = r0.firstElement()     // Catch:{ f -> 0x00e0 }
            com.epoint.android.games.mjfgbfree.network.aa r0 = (com.epoint.android.games.mjfgbfree.network.aa) r0     // Catch:{ f -> 0x00e0 }
            int r5 = r12.cr     // Catch:{ f -> 0x00e0 }
            java.lang.String r6 = com.epoint.android.games.mjfgbfree.ai.nm     // Catch:{ f -> 0x00e0 }
            b.a.b r5 = r12.a(r5, r6)     // Catch:{ f -> 0x00e0 }
            java.lang.String r5 = r5.toString()     // Catch:{ f -> 0x00e0 }
            r0.d(r5)     // Catch:{ f -> 0x00e0 }
        L_0x00bd:
            r0 = r2
            goto L_0x003d
        L_0x00c0:
            a.b.d.a r0 = new a.b.d.a     // Catch:{ f -> 0x00e0 }
            int r1 = com.epoint.android.games.mjfgbfree.ai.ns     // Catch:{ f -> 0x00e0 }
            r0.<init>(r1)     // Catch:{ f -> 0x00e0 }
            r12.qC = r0     // Catch:{ f -> 0x00e0 }
            r0 = r9
        L_0x00ca:
            a.b.d.a r1 = r12.qC     // Catch:{ f -> 0x00e0 }
            a.b.c.a.b[] r1 = r1.bN()     // Catch:{ f -> 0x00e0 }
            int r1 = r1.length     // Catch:{ f -> 0x00e0 }
            if (r0 < r1) goto L_0x00e5
            r0 = 4
            int[] r0 = new int[r0]     // Catch:{ f -> 0x00e0 }
            r0 = {500, 500, 500, 500} // fill-array     // Catch:{ f -> 0x00e0 }
            r1 = 4
            int[] r1 = new int[r1]     // Catch:{ f -> 0x00e0 }
            super.a(r0, r1)     // Catch:{ f -> 0x00e0 }
            goto L_0x0070
        L_0x00e0:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x008e
        L_0x00e5:
            a.b.d.a r1 = r12.qC     // Catch:{ f -> 0x00e0 }
            a.b.c.a.b[] r1 = r1.bN()     // Catch:{ f -> 0x00e0 }
            r1 = r1[r0]     // Catch:{ f -> 0x00e0 }
            java.lang.String r2 = ""
            r1.bc(r2)     // Catch:{ f -> 0x00e0 }
            int r0 = r0 + 1
            goto L_0x00ca
        L_0x00f5:
            java.util.Vector r0 = r12.cs     // Catch:{ f -> 0x00e0 }
            java.lang.Object r0 = r0.elementAt(r2)     // Catch:{ f -> 0x00e0 }
            com.epoint.android.games.mjfgbfree.network.aa r0 = (com.epoint.android.games.mjfgbfree.network.aa) r0     // Catch:{ f -> 0x00e0 }
            if (r0 != r13) goto L_0x013d
            int[] r5 = com.epoint.android.games.mjfgbfree.c.a.co     // Catch:{ f -> 0x00e0 }
            r5 = r5[r2]     // Catch:{ f -> 0x00e0 }
            r0.b(r5)     // Catch:{ f -> 0x00e0 }
            r0.e(r3)     // Catch:{ f -> 0x00e0 }
            java.lang.String[] r5 = com.epoint.android.games.mjfgbfree.bb.tH     // Catch:{ f -> 0x00e0 }
            int r6 = r0.n()     // Catch:{ f -> 0x00e0 }
            int r6 = r6 - r10
            r5[r6] = r4     // Catch:{ f -> 0x00e0 }
            a.b.d.a r5 = r12.qC     // Catch:{ f -> 0x00e0 }
            a.b.c.a.b[] r5 = r5.bN()     // Catch:{ f -> 0x00e0 }
            int r6 = r0.n()     // Catch:{ f -> 0x00e0 }
            r5 = r5[r6]     // Catch:{ f -> 0x00e0 }
            java.lang.String r6 = r0.o()     // Catch:{ f -> 0x00e0 }
            r5.bc(r6)     // Catch:{ f -> 0x00e0 }
            java.lang.String r5 = com.epoint.android.games.mjfgbfree.ai.nm     // Catch:{ f -> 0x00e0 }
            int r6 = r0.n()     // Catch:{ f -> 0x00e0 }
            b.a.b r5 = r12.a(r5, r6)     // Catch:{ f -> 0x00e0 }
            java.lang.String r5 = r5.toString()     // Catch:{ f -> 0x00e0 }
            r0.d(r5)     // Catch:{ f -> 0x00e0 }
            r0 = r1
        L_0x0137:
            int r1 = r2 + 1
            r2 = r1
            r1 = r0
            goto L_0x0084
        L_0x013d:
            int r0 = r0.n()     // Catch:{ f -> 0x00e0 }
            if (r0 != r11) goto L_0x023c
            r0 = r9
            goto L_0x0137
        L_0x0145:
            com.epoint.android.games.mjfgbfree.MJ16ViewActivity r0 = r12.ct     // Catch:{ f -> 0x00e0 }
            int r1 = r12.qE     // Catch:{ f -> 0x00e0 }
            r2 = 0
            com.epoint.android.games.mjfgbfree.f.a.a(r0, r1, r2)     // Catch:{ f -> 0x00e0 }
            a.b.d.a r0 = r12.cz     // Catch:{ f -> 0x00e0 }
            if (r0 == 0) goto L_0x0160
            a.b.d.a r0 = r12.cz     // Catch:{ f -> 0x00e0 }
            r12.qC = r0     // Catch:{ f -> 0x00e0 }
            r0 = 1
            r12.qD = r0     // Catch:{ f -> 0x00e0 }
            r0 = 0
            r12.cz = r0     // Catch:{ f -> 0x00e0 }
            r12.R()     // Catch:{ f -> 0x00e0 }
            goto L_0x008e
        L_0x0160:
            r12.R()     // Catch:{ f -> 0x00e0 }
            goto L_0x008e
        L_0x0165:
            if (r0 != 0) goto L_0x008e
            java.util.Vector r0 = r12.cs     // Catch:{ f -> 0x00e0 }
            java.lang.Object r12 = r0.firstElement()     // Catch:{ f -> 0x00e0 }
            com.epoint.android.games.mjfgbfree.network.aa r12 = (com.epoint.android.games.mjfgbfree.network.aa) r12     // Catch:{ f -> 0x00e0 }
            int r0 = com.epoint.android.games.mjfgbfree.c.a.qB     // Catch:{ f -> 0x00e0 }
            if (r0 != r11) goto L_0x017b
            java.lang.String r0 = "seat_pos"
            int r0 = r1.getInt(r0)     // Catch:{ f -> 0x00e0 }
            com.epoint.android.games.mjfgbfree.c.a.qB = r0     // Catch:{ f -> 0x00e0 }
        L_0x017b:
            r0 = 0
            r12.b(r0)     // Catch:{ f -> 0x00e0 }
            goto L_0x008e
        L_0x0181:
            java.lang.String r2 = "data"
            java.lang.String r1 = r1.getString(r2)     // Catch:{ f -> 0x00e0 }
            com.epoint.android.games.mjfgbfree.c.c r2 = new com.epoint.android.games.mjfgbfree.c.c     // Catch:{ f -> 0x00e0 }
            r2.<init>(r12, r13, r0, r1)     // Catch:{ f -> 0x00e0 }
            com.epoint.android.games.mjfgbfree.bb.f(r2)     // Catch:{ f -> 0x00e0 }
            goto L_0x008e
        L_0x0191:
            java.lang.String r0 = "mj16gameinfo"
            boolean r0 = r1.H(r0)     // Catch:{ f -> 0x00e0 }
            if (r0 != 0) goto L_0x01df
            com.epoint.android.games.mjfgbfree.d.g r0 = r12.gO     // Catch:{ f -> 0x00e0 }
            if (r0 != 0) goto L_0x01d8
            b.a.b r0 = a(r1)     // Catch:{ f -> 0x00e0 }
            int r1 = com.epoint.android.games.mjfgbfree.c.d.qB     // Catch:{ f -> 0x00e0 }
            com.epoint.android.games.mjfgbfree.d.g r1 = com.epoint.android.games.mjfgbfree.af.a(r0, r1)     // Catch:{ f -> 0x00e0 }
            r12.gO = r1     // Catch:{ f -> 0x00e0 }
            com.epoint.android.games.mjfgbfree.d.e r1 = new com.epoint.android.games.mjfgbfree.d.e     // Catch:{ f -> 0x00e0 }
            r1.<init>()     // Catch:{ f -> 0x00e0 }
            java.lang.String r2 = "tile_color"
            r3 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            int r2 = r0.b(r2, r3)     // Catch:{ f -> 0x00e0 }
            if (r2 == r4) goto L_0x01ba
            r1.sm = r2     // Catch:{ f -> 0x00e0 }
        L_0x01ba:
            java.lang.String r2 = "table_color"
            r3 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            int r2 = r0.b(r2, r3)     // Catch:{ f -> 0x00e0 }
            if (r2 == r4) goto L_0x01c6
            r1.sn = r2     // Catch:{ f -> 0x00e0 }
        L_0x01c6:
            java.lang.String r1 = "rounds"
            int r2 = com.epoint.android.games.mjfgbfree.ai.nr     // Catch:{ f -> 0x00e0 }
            int r0 = r0.b(r1, r2)     // Catch:{ f -> 0x00e0 }
            com.epoint.android.games.mjfgbfree.MJ16ViewActivity r1 = r12.ct     // Catch:{ f -> 0x00e0 }
            com.epoint.android.games.mjfgbfree.bb r0 = r1.a(r12, r0)     // Catch:{ f -> 0x00e0 }
            r12.cu = r0     // Catch:{ f -> 0x00e0 }
            goto L_0x008e
        L_0x01d8:
            com.epoint.android.games.mjfgbfree.c.e r0 = new com.epoint.android.games.mjfgbfree.c.e     // Catch:{ f -> 0x00e0 }
            r0.<init>(r12, r1)     // Catch:{ f -> 0x00e0 }
            goto L_0x008e
        L_0x01df:
            java.lang.String r0 = "seat_mapping"
            boolean r0 = r1.H(r0)     // Catch:{ f -> 0x00e0 }
            if (r0 != 0) goto L_0x01ff
            java.lang.String r0 = "seat_mapping"
            b.a.b r0 = r1.F(r0)     // Catch:{ f -> 0x00e0 }
            java.lang.String r1 = com.epoint.android.games.mjfgbfree.MJ16Activity.qK     // Catch:{ f -> 0x00e0 }
            boolean r1 = r0.H(r1)     // Catch:{ f -> 0x00e0 }
            if (r1 != 0) goto L_0x008e
            java.lang.String r1 = com.epoint.android.games.mjfgbfree.MJ16Activity.qK     // Catch:{ f -> 0x00e0 }
            int r0 = r0.getInt(r1)     // Catch:{ f -> 0x00e0 }
            com.epoint.android.games.mjfgbfree.c.a.qB = r0     // Catch:{ f -> 0x00e0 }
            goto L_0x008e
        L_0x01ff:
            java.lang.String r0 = "chara_mapping"
            boolean r0 = r1.H(r0)     // Catch:{ f -> 0x00e0 }
            if (r0 != 0) goto L_0x008e
            java.lang.String r0 = "chara_mapping"
            b.a.a r0 = r1.E(r0)     // Catch:{ f -> 0x00e0 }
            java.lang.String[] r1 = com.epoint.android.games.mjfgbfree.bb.tH     // Catch:{ f -> 0x00e0 }
            r2 = 0
            int r3 = com.epoint.android.games.mjfgbfree.c.a.qB     // Catch:{ f -> 0x00e0 }
            int r3 = r3 + 1
            int r3 = r3 % 4
            java.lang.String r3 = r0.getString(r3)     // Catch:{ f -> 0x00e0 }
            r1[r2] = r3     // Catch:{ f -> 0x00e0 }
            java.lang.String[] r1 = com.epoint.android.games.mjfgbfree.bb.tH     // Catch:{ f -> 0x00e0 }
            r2 = 1
            int r3 = com.epoint.android.games.mjfgbfree.c.a.qB     // Catch:{ f -> 0x00e0 }
            int r3 = r3 + 2
            int r3 = r3 % 4
            java.lang.String r3 = r0.getString(r3)     // Catch:{ f -> 0x00e0 }
            r1[r2] = r3     // Catch:{ f -> 0x00e0 }
            java.lang.String[] r1 = com.epoint.android.games.mjfgbfree.bb.tH     // Catch:{ f -> 0x00e0 }
            r2 = 2
            int r3 = com.epoint.android.games.mjfgbfree.c.a.qB     // Catch:{ f -> 0x00e0 }
            int r3 = r3 + 3
            int r3 = r3 % 4
            java.lang.String r0 = r0.getString(r3)     // Catch:{ f -> 0x00e0 }
            r1[r2] = r0     // Catch:{ f -> 0x00e0 }
            goto L_0x008e
        L_0x023c:
            r0 = r1
            goto L_0x0137
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.c.a.a(com.epoint.android.games.mjfgbfree.network.aa, java.lang.String):void");
    }

    public final void a(int[] iArr, int[] iArr2) {
        if (!this.qD) {
            super.a(iArr, iArr2);
        }
    }

    public final void c(String str, String str2) {
        if (str.equals("UpdateSnap")) {
            try {
                b a2 = af.a(this.qC);
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 < this.cs.size()) {
                        a((aa) this.cs.elementAt(i2), a2);
                        i = i2 + 1;
                    } else {
                        return;
                    }
                }
            } catch (f e) {
                e.printStackTrace();
            }
        } else {
            this.cu.ex();
            ((aa) this.cs.firstElement()).d(d(str, str2).toString());
            this.cu.er();
        }
    }

    public final void d(int i) {
        if (i != 7 || e(this.qC.bP().kr.ga())) {
            try {
                b a2 = af.a(this.qC);
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= this.cs.size()) {
                        break;
                    }
                    a((aa) this.cs.elementAt(i3), a2);
                    i2 = i3 + 1;
                }
            } catch (f e) {
                e.printStackTrace();
            }
        }
        super.d(i);
    }

    public final boolean e(int i) {
        if (i == qB) {
            return true;
        }
        return !"".equals(this.gO.vU[i].xx);
    }
}
