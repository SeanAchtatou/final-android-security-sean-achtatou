package com.epoint.android.games.mjfgbfree.network;

import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import java.io.IOException;
import java.io.OutputStream;

public final class bt implements aa, Runnable {
    private int aG;
    private String aH;
    private String aI;
    private Handler handler;
    private Thread iA;
    /* access modifiers changed from: private */
    public t iC;
    /* access modifiers changed from: private */
    public OutputStream ix;
    /* access modifiers changed from: private */
    public int iy;
    private Thread iz;
    private boolean lD;
    private BluetoothSocket wh;

    public bt(BluetoothSocket bluetoothSocket) {
        this.lD = false;
        this.aG = -1;
        this.handler = new al(this);
        this.lD = false;
        this.wh = bluetoothSocket;
        try {
            this.ix = bluetoothSocket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public final void a(t tVar) {
        this.iC = tVar;
        if (this.iz == null) {
            this.iz = new Thread(this);
            this.iz.start();
        }
    }

    public final void b(int i) {
        this.aG = i;
    }

    public final synchronized boolean d(String str) {
        boolean z;
        String str2 = String.valueOf(str) + "\r\n";
        try {
            synchronized (this.ix) {
                this.ix.write(str2.getBytes("UTF-8"));
            }
            if (this.iA != null && this.iA.isAlive()) {
                this.iA.interrupt();
            }
            this.iA = new am(this);
            this.iA.start();
            z = true;
        } catch (IOException e) {
            e.printStackTrace();
            if (this.iA != null && this.iA.isAlive()) {
                this.iA.interrupt();
            }
            z = false;
        }
        return z;
    }

    public final void e(String str) {
        this.aH = str;
    }

    public final void f(String str) {
        this.aI = str;
    }

    public final boolean l() {
        if (this.lD) {
            return false;
        }
        try {
            this.wh.getInputStream().close();
        } catch (Exception e) {
        }
        try {
            this.wh.getOutputStream().close();
        } catch (Exception e2) {
        }
        try {
            this.wh.close();
        } catch (Exception e3) {
        }
        this.lD = true;
        return true;
    }

    public final boolean m() {
        return this.lD;
    }

    public final int n() {
        return this.aG;
    }

    public final String o() {
        return this.aH;
    }

    public final String p() {
        return this.aI;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005a, code lost:
        r2 = "";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r2 = r1.toString("UTF-8");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0084, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0085, code lost:
        r3.printStackTrace();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r5 = this;
            r0 = 0
            android.bluetooth.BluetoothSocket r1 = r5.wh     // Catch:{ IOException -> 0x0049 }
            java.io.InputStream r0 = r1.getInputStream()     // Catch:{ IOException -> 0x0049 }
        L_0x0007:
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r2 = 4096(0x1000, float:5.74E-42)
            r1.<init>(r2)
        L_0x000e:
            r1.reset()     // Catch:{ Exception -> 0x0031 }
        L_0x0011:
            int r2 = r0.read()     // Catch:{ Exception -> 0x0031 }
            r3 = -1
            if (r2 == r3) goto L_0x0052
            r3 = 13
            if (r2 == r3) goto L_0x0011
            r3 = 10
            if (r2 == r3) goto L_0x005a
            int r3 = r1.size()     // Catch:{ Exception -> 0x0031 }
            r4 = 32768(0x8000, float:4.5918E-41)
            if (r3 != r4) goto L_0x004e
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ Exception -> 0x0031 }
            java.lang.String r1 = "Buffer overflow"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0031 }
            throw r0     // Catch:{ Exception -> 0x0031 }
        L_0x0031:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0035:
            boolean r0 = com.epoint.android.games.mjfgbfree.bb.dU()
            if (r0 != 0) goto L_0x0094
            android.os.Message r0 = new android.os.Message
            r0.<init>()
            r1 = 2
            r0.what = r1
            android.os.Handler r1 = r5.handler
            r1.sendMessage(r0)
            return
        L_0x0049:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0007
        L_0x004e:
            r1.write(r2)     // Catch:{ Exception -> 0x0031 }
            goto L_0x0011
        L_0x0052:
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ Exception -> 0x0031 }
            java.lang.String r1 = "Socket read error."
            r0.<init>(r1)     // Catch:{ Exception -> 0x0031 }
            throw r0     // Catch:{ Exception -> 0x0031 }
        L_0x005a:
            java.lang.String r2 = ""
            java.lang.String r3 = "UTF-8"
            java.lang.String r2 = r1.toString(r3)     // Catch:{ UnsupportedEncodingException -> 0x0084 }
        L_0x0062:
            java.lang.String r3 = ""
            boolean r3 = r2.equals(r3)
            if (r3 != 0) goto L_0x000e
        L_0x006a:
            boolean r3 = com.epoint.android.games.mjfgbfree.bb.dU()
            if (r3 != 0) goto L_0x0089
            com.epoint.android.games.mjfgbfree.network.t r3 = r5.iC
            if (r3 == 0) goto L_0x0089
            android.os.Message r3 = new android.os.Message
            r3.<init>()
            r4 = 1
            r3.what = r4
            r3.obj = r2
            android.os.Handler r2 = r5.handler
            r2.sendMessage(r3)
            goto L_0x000e
        L_0x0084:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0062
        L_0x0089:
            r3 = 20
            java.lang.Thread.sleep(r3)     // Catch:{ InterruptedException -> 0x008f }
            goto L_0x006a
        L_0x008f:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x006a
        L_0x0094:
            r0 = 20
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x009a }
            goto L_0x0035
        L_0x009a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.network.bt.run():void");
    }
}
