package com.epoint.android.games.mjfgbfree.challenge;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import b.a.b;
import b.a.f;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.ai;
import com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity;
import com.epoint.android.games.mjfgbfree.baseactivities.e;
import com.epoint.android.games.mjfgbfree.f.a;

public class ChallengeActivity extends BaseActivity implements Handler.Callback {
    private b mt = null;

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 1:
                String str = (String) message.obj;
                if (str != null) {
                    b bVar = (b) ao(str);
                    if (bVar.M("challenge_list") != null) {
                        this.mt = bVar;
                        try {
                            System.out.println(this.mt.bB());
                        } catch (f e) {
                            e.printStackTrace();
                        }
                        if (!this.mt.H("challenge_list")) {
                            a.a(this, "challenge_list", this.mt.toString());
                            break;
                        }
                    }
                }
                break;
        }
        return super.handleMessage(message);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        super.a(bundle, "Challenge");
        requestWindowFeature(5);
        try {
            String b2 = a.b(this, "challenge_list", null);
            if (b2 != null) {
                this.mt = new b(b2);
            }
        } catch (f e) {
            e.printStackTrace();
        }
        setContentView((int) C0000R.layout.challenge_activity);
    }

    public void onWindowFocusChanged(boolean z) {
        if (z && !cC() && !this.oy && !cC()) {
            try {
                a(new e(this, "http://www.mjguy.com:8080/s?req=challenge_list&lang_id=" + ai.nk + (this.mt == null ? "" : "&version=" + this.mt.getInt("version")), 0));
                cD();
            } catch (f e) {
                e.printStackTrace();
            }
        }
        super.onWindowFocusChanged(z);
    }
}
