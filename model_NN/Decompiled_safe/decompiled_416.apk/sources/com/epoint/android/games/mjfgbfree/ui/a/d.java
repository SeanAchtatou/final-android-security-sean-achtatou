package com.epoint.android.games.mjfgbfree.ui.a;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import com.epoint.android.games.mjfgbfree.MJ16ViewActivity;
import com.epoint.android.games.mjfgbfree.af;
import com.epoint.android.games.mjfgbfree.bb;
import com.epoint.android.games.mjfgbfree.e.o;
import com.epoint.android.games.mjfgbfree.ui.a;
import java.util.Vector;

public final class d extends e {
    private Vector gM = new Vector();
    /* access modifiers changed from: private */
    public a hh = new a(100, 60, -65536, -16777216, 160, af.aa("略過"));
    /* access modifiers changed from: private */
    public a hi = new a(100, 60, -65536, -16777216, 160, ">");
    private LinearLayout hj;
    private ImageView hk;
    /* access modifiers changed from: private */
    public TextView hl;
    private h hm;
    /* access modifiers changed from: private */
    public ScrollView hn;
    private o ho = null;
    private int hp = 0;
    private Vector hq = new Vector();
    private Vector hr = new Vector();
    private Handler mHandler = new a(this);
    private int x = 0;

    public d() {
        this.js = new a(420, 250, -1, -12303292, 208, null);
        this.ju = new Rect(this.js.getBounds());
        this.gM.addElement(this.hh);
        this.gM.addElement(this.hi);
    }

    static /* synthetic */ void a(d dVar, String str) {
        Message message = new Message();
        message.what = 0;
        message.obj = str;
        dVar.mHandler.sendMessage(message);
    }

    static /* synthetic */ void e(d dVar) {
        Message message = new Message();
        message.what = 1;
        dVar.mHandler.sendMessage(message);
    }

    public final Object a(int i, int i2, int i3, Object obj) {
        a aVar = null;
        int i4 = 0;
        while (true) {
            a aVar2 = aVar;
            if (i4 >= this.gM.size()) {
                return aVar2;
            }
            aVar = (a) this.gM.elementAt(i4);
            aVar.b(false);
            if (aVar.isVisible() && aVar.bf()) {
                Rect bounds = aVar.getBounds();
                if (i >= bounds.left && i <= bounds.right && i2 >= bounds.top && i2 <= bounds.bottom && (i3 == 0 || (i3 != 0 && obj == aVar))) {
                    aVar.b(true);
                    i4++;
                }
            }
            aVar = aVar2;
            i4++;
        }
    }

    public final void a(Bitmap bitmap) {
        if (bitmap == null) {
            this.hk.setVisibility(8);
            return;
        }
        this.hk.setImageBitmap(bitmap);
        this.hk.setVisibility(0);
    }

    public final void a(Bitmap bitmap, boolean z) {
    }

    public final void a(Canvas canvas) {
        if (this.jt == 1 || this.jt >= 4) {
            int width = this.ju.width();
            int height = this.ju.height();
            this.ju.left = (MJ16Activity.qZ - width) / 2;
            this.ju.top = (bb.ty == 1 ? 0 : bb.dR()) + (((MJ16Activity.ra - bb.dR()) - height) / 2);
            this.ju.right = width + this.ju.left;
            this.ju.bottom = height + this.ju.top;
            int width2 = this.ju.left + (this.ju.width() / 2);
            if (this.hp == 2) {
                this.hh.c((width2 - this.hh.bb()) - 10, (this.ju.bottom - this.hh.ba()) - 10);
                this.hi.c(width2 + 10, (this.ju.bottom - this.hi.ba()) - 10);
            } else if (this.hp == 1 || this.hp == 0) {
                this.hh.c(this.ju.left + 10, (this.ju.bottom - this.hh.ba()) - 10);
                this.hi.c((this.ju.right - this.hi.bb()) - 10, (this.ju.bottom - this.hi.ba()) - 10);
            } else if (this.hp == 3) {
                this.hi.c(width2 - (this.hi.bb() / 2), (this.ju.bottom - this.hi.ba()) - 10);
            }
        }
        super.a(canvas);
    }

    public final void a(MJ16ViewActivity mJ16ViewActivity, bb bbVar) {
        this.jq = mJ16ViewActivity;
        this.jr = bbVar;
        this.hj = (LinearLayout) mJ16ViewActivity.findViewById(C0000R.id.MessageLayout);
        this.hn = (ScrollView) this.hj.findViewById(C0000R.id.MessageScroll);
        this.hl = (TextView) this.hj.findViewById(C0000R.id.text);
        this.hk = (ImageView) this.hj.findViewById(C0000R.id.icon);
        this.hl.setText("");
    }

    public final void a(o oVar) {
        this.ho = oVar;
    }

    public final void a(Object obj) {
        this.hh.d(false);
        this.hi.d(false);
        if (obj == this.hh) {
            this.hh.b(false);
            if (this.ho == null) {
                return;
            }
            if (this.hp == 2) {
                this.ho.a(3, this);
            } else if (this.hp == 1) {
                this.ho.a(1, this);
            }
        } else if (obj == this.hi) {
            this.hi.b(false);
            if (this.ho == null) {
                return;
            }
            if (this.hp == 2) {
                this.ho.a(4, this);
            } else if (this.hp == 1 || this.hp == 0) {
                this.ho.a(2, this);
            } else if (this.hp == 3) {
                this.ho.a(5, this);
            }
        } else {
            a aVar = (a) obj;
            for (int i = 0; i < this.gM.size() - 1; i++) {
                ((a) this.gM.elementAt(i)).c(false);
            }
            aVar.b(false);
        }
    }

    public final void a(Thread thread) {
        super.a((Object) null);
        this.hj.setVisibility(8);
        super.b(thread);
    }

    public final void b(Canvas canvas) {
        canvas.drawARGB(64, 0, 0, 0);
        this.js.draw(canvas);
        int i = this.js.getBounds().top;
        for (int i2 = 0; i2 < this.gM.size(); i2++) {
            if (((a) this.gM.elementAt(i2)).isVisible()) {
                ((a) this.gM.elementAt(i2)).draw(canvas);
            }
        }
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.hj.getLayoutParams();
        layoutParams.bottomMargin = (MJ16Activity.ra - this.hh.getBounds().top) + 4;
        layoutParams.bottomMargin = (layoutParams.bottomMargin * MJ16Activity.qR) / MJ16Activity.ra;
        layoutParams.leftMargin = ((this.js.getBounds().left + 5) * MJ16Activity.qS) / MJ16Activity.qZ;
        layoutParams.rightMargin = layoutParams.leftMargin;
        layoutParams.topMargin = i + 3;
        layoutParams.topMargin = (layoutParams.topMargin * MJ16Activity.qR) / MJ16Activity.ra;
        this.hj.setLayoutParams(layoutParams);
        if (this.hj.getVisibility() == 8) {
            this.jr.er();
            this.hj.setVisibility(0);
            this.hj.bringToFront();
            if (this.ho != null) {
                this.ho.a(6, this);
            }
        }
    }

    public final void bi() {
        a((Thread) null);
    }

    public final void bk() {
        this.hm.interrupt();
        this.hh.b(false);
        this.hi.b(false);
    }

    public final void bo() {
        this.jr.ut = null;
        super.h(false);
    }

    public final void n(int i) {
        this.hh.a(true, false);
        this.hi.a(true, false);
        this.hh.d(false);
        this.hi.d(false);
        this.hp = i;
        switch (i) {
            case 0:
                this.hh.a(false, false);
                this.hi.v(">");
                return;
            case 1:
                this.hh.v(af.aa("略過"));
                this.hi.v(">");
                return;
            case 2:
                this.hh.v(af.aa("是"));
                this.hi.v(af.aa("否"));
                return;
            case 3:
                this.hh.a(false, false);
                this.hi.v(af.aa("好"));
                return;
            default:
                return;
        }
    }

    public final void x(String str) {
        this.hm = new h(this, str);
        this.hm.start();
    }
}
