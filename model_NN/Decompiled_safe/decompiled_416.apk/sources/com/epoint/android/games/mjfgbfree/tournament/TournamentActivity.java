package com.epoint.android.games.mjfgbfree.tournament;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import com.epoint.android.games.mjfgbfree.a.d;
import com.epoint.android.games.mjfgbfree.af;
import com.epoint.android.games.mjfgbfree.ai;
import com.epoint.android.games.mjfgbfree.bb;
import com.epoint.android.games.mjfgbfree.f.a;
import com.epoint.android.games.mjfgbfree.ui.e;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

public class TournamentActivity extends Activity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public LinearLayout hj;
    private LayoutInflater yq;
    private Bitmap yr;
    private Bitmap ys;
    private Bitmap yt;
    private Bitmap yu;
    private Bitmap yv;
    private Hashtable yw;

    private int g(int i, int i2) {
        Integer num = null;
        switch (i) {
            case 0:
                num = (Integer) this.yw.get(Integer.valueOf(100000 + i2));
                break;
            case 1:
                num = (Integer) this.yw.get(Integer.valueOf(100003 + i2));
                break;
            case 2:
                num = (Integer) this.yw.get(Integer.valueOf(100006 + i2));
                break;
            case 3:
                num = (Integer) this.yw.get(Integer.valueOf(100009 + i2));
                break;
            case 4:
                num = (Integer) this.yw.get(Integer.valueOf(100012 + i2));
                break;
            case 5:
                num = (Integer) this.yw.get(Integer.valueOf(100015 + i2));
                break;
        }
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    private RelativeLayout h(String str, int i) {
        boolean z;
        RelativeLayout relativeLayout = (RelativeLayout) this.yq.inflate((int) C0000R.layout.tournament_item, (ViewGroup) null);
        ImageView imageView = (ImageView) relativeLayout.findViewById(C0000R.id.lock);
        imageView.setImageBitmap(this.yr);
        ((TextView) relativeLayout.findViewById(C0000R.id.title)).setText(str);
        ImageView imageView2 = (ImageView) relativeLayout.findViewById(C0000R.id.icon);
        imageView2.setImageBitmap(this.yv);
        if (i == 0 || g(i - 1, 1) > 0) {
            imageView.setVisibility(8);
            z = false;
        } else {
            imageView2.setVisibility(8);
            z = true;
        }
        int g = g(i, 1);
        int g2 = g(i, 2);
        int g3 = g(i, 3);
        ((ImageView) relativeLayout.findViewById(C0000R.id.medal1)).setImageBitmap(this.ys);
        ((TextView) relativeLayout.findViewById(C0000R.id.medal1_count)).setText("x " + g);
        ((ImageView) relativeLayout.findViewById(C0000R.id.medal2)).setImageBitmap(this.yt);
        ((TextView) relativeLayout.findViewById(C0000R.id.medal2_count)).setText("x " + g2);
        ((ImageView) relativeLayout.findViewById(C0000R.id.medal3)).setImageBitmap(this.yu);
        ((TextView) relativeLayout.findViewById(C0000R.id.medal3_count)).setText("x " + g3);
        if (!z) {
            relativeLayout.setBackgroundResource(17301602);
            relativeLayout.setFocusable(true);
            relativeLayout.setClickable(true);
            relativeLayout.setOnClickListener(this);
            relativeLayout.setTag(Integer.valueOf(i));
        }
        this.hj.addView(relativeLayout);
        return relativeLayout;
    }

    public static void s(Context context) {
        int i;
        long time = new Date().getTime();
        if (time > MJ16Activity.rq + 86400000) {
            i = 10;
        } else if (time > MJ16Activity.rq + 43200000) {
            i = 5;
            time = MJ16Activity.rq + 43200000;
        } else {
            i = 0;
        }
        if (i > 0) {
            if (MJ16Activity.rp + i > 999) {
                i = 999 - MJ16Activity.rp;
            }
            a.a(context, 0, i);
            MJ16Activity.rp = i + MJ16Activity.rp;
            a.a(context, "last_ticket_update", String.valueOf(time));
            MJ16Activity.rq = time;
        }
    }

    public static Bitmap t(Context context) {
        Bitmap bitmap;
        Canvas canvas;
        Bitmap c = d.c(context, "images/tour_back.jpg");
        int width = MJ16Activity.db().getWidth() > MJ16Activity.db().getHeight() ? MJ16Activity.db().getWidth() : MJ16Activity.db().getHeight();
        if (c.getWidth() < width) {
            bitmap = Bitmap.createBitmap(width, width, Bitmap.Config.RGB_565);
            Canvas canvas2 = new Canvas(bitmap);
            Rect rect = new Rect();
            rect.left = 0;
            rect.top = 0;
            rect.right = c.getWidth();
            rect.bottom = c.getHeight();
            Rect rect2 = new Rect();
            rect2.left = 0;
            rect2.top = 0;
            rect2.bottom = width;
            rect2.right = width;
            Paint paint = new Paint();
            paint.setDither(true);
            paint.setFilterBitmap(true);
            canvas2.drawBitmap(c, rect, rect2, paint);
            canvas = canvas2;
        } else {
            Bitmap copy = c.copy(Bitmap.Config.RGB_565, true);
            bitmap = copy;
            canvas = new Canvas(copy);
        }
        c.recycle();
        canvas.drawColor(Color.rgb(65, 14, 7), PorterDuff.Mode.SCREEN);
        return bitmap;
    }

    public void onClick(View view) {
        int intValue = ((Integer) ((RelativeLayout) view).getTag()).intValue();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (MJ16Activity.rp >= 2) {
            builder.setMessage(String.valueOf(af.Z("用2張遊戲劵挑戰這比賽")) + "?" + (intValue == 5 ? "\n(" + af.Z("此比賽不許重新挑戰或續關。") + ")" : ""));
            builder.setPositiveButton(af.aa("是"), new h(this, intValue));
            builder.setNegativeButton(af.aa("否"), new e(this));
        } else {
            builder.setMessage(af.Z("需要2張遊戲劵"));
            builder.setPositiveButton(af.aa("好"), new d(this));
        }
        builder.show();
    }

    public void onCreate(Bundle bundle) {
        Button button;
        c cVar;
        super.onCreate(bundle);
        MJ16Activity.dd();
        requestWindowFeature(1);
        setVolumeControlStream(3);
        s(this);
        if (MJ16Activity.rd) {
            setRequestedOrientation(0);
        } else {
            setRequestedOrientation(ai.nf);
        }
        View inflate = LayoutInflater.from(this).inflate((int) C0000R.layout.tournament_activity, (ViewGroup) null);
        setContentView(inflate);
        this.hj = (LinearLayout) inflate.findViewById(C0000R.id.tournament_layout);
        this.yq = LayoutInflater.from(this);
        this.yr = d.c(this, "images/lock.png");
        this.ys = d.c(this, "images/medal1.png");
        this.yt = d.c(this, "images/medal2.png");
        this.yu = d.c(this, "images/medal3.png");
        this.yv = d.c(this, "images/trophy.png");
        this.yw = a.e(this);
        ImageView imageView = (ImageView) inflate.findViewById(C0000R.id.bg);
        if (bb.tE.contains("bkgnd")) {
            ((Bitmap) bb.tE.get("bkgnd")).recycle();
        }
        Bitmap t = t(this);
        bb.tE.put("bkgnd", t);
        imageView.setImageBitmap(t);
        ((ImageView) inflate.findViewById(C0000R.id.tickets)).setImageBitmap(e.e(this, 0));
        ImageView imageView2 = (ImageView) inflate.findViewById(C0000R.id.info);
        imageView2.setImageResource(17301659);
        imageView2.setOnClickListener(new g(this));
        ((TextView) inflate.findViewById(C0000R.id.count)).setText("x " + MJ16Activity.rp);
        ((TextView) inflate.findViewById(C0000R.id.title)).setText("~ " + af.Z("挑戰賽") + " ~");
        Object b2 = a.b(this, 4);
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(C0000R.id.tournament_buttons);
        if (!(b2 instanceof a.b.d.a) || (cVar = (c) ((a.b.d.a) b2).lq.get("tournament_detail")) == null) {
            button = null;
        } else {
            Button button2 = new Button(this);
            button2.setText(String.valueOf(af.aa("繼續遊戲")) + ": " + c.D(cVar.oo) + " - " + cVar.cy());
            button2.setOnClickListener(new f(this));
            linearLayout.addView(button2);
            Button button3 = new Button(this);
            this.hj.addView(button3);
            button = button3;
        }
        Vector vector = new Vector();
        vector.add(h(c.D(0), 0));
        vector.add(h(c.D(1), 1));
        vector.add(h(c.D(2), 2));
        vector.add(h(c.D(3), 3));
        if (g(3, 1) > 0) {
            vector.add(h(c.D(4), 4));
        }
        vector.add(h(c.D(5), 5));
        if (button != null) {
            for (int i = 0; i < vector.size(); i++) {
                ((RelativeLayout) vector.elementAt(i)).setEnabled(false);
                ((RelativeLayout) vector.elementAt(i)).findViewById(C0000R.id.overlay).setVisibility(0);
                ((RelativeLayout) vector.elementAt(i)).findViewById(C0000R.id.overlay).bringToFront();
            }
            button.setText(af.aa("開始新遊戲"));
            button.setOnClickListener(new i(this, vector));
            return;
        }
        this.hj.setBackgroundResource(0);
    }

    public void onDestroy() {
        e.gt();
        super.onDestroy();
    }
}
