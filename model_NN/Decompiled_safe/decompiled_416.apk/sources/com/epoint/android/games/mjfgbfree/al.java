package com.epoint.android.games.mjfgbfree;

import android.content.DialogInterface;
import android.content.Intent;
import com.epoint.android.games.mjfgbfree.admin.MJColorPicker;
import com.epoint.android.games.mjfgbfree.cam.CapturePlayerActivity;
import com.epoint.android.games.mjfgbfree.challenge.ChallengeActivity;
import com.epoint.android.games.mjfgbfree.street.StreetMainActivity;

final class al implements DialogInterface.OnClickListener {
    private /* synthetic */ MJ16Activity ok;

    al(MJ16Activity mJ16Activity) {
        this.ok = mJ16Activity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case 0:
                MJ16Activity.a(this.ok, 0);
                return;
            case 1:
                MJ16Activity.a(this.ok, 2);
                return;
            case 2:
                Intent intent = new Intent(this.ok, MJ16ViewActivity.class);
                intent.putExtra("is_story_game", true);
                intent.putExtra("script_id", 1);
                this.ok.startActivity(intent);
                return;
            case 3:
                Intent intent2 = new Intent(this.ok, MJ16ViewActivity.class);
                intent2.putExtra("is_story_game", true);
                intent2.putExtra("script_id", 2);
                this.ok.startActivity(intent2);
                return;
            case 4:
                Intent intent3 = new Intent(this.ok, MJ16ViewActivity.class);
                intent3.putExtra("is_story_game", true);
                intent3.putExtra("script_id", 3);
                this.ok.startActivity(intent3);
                return;
            case 5:
                this.ok.startActivity(new Intent(this.ok, StreetMainActivity.class));
                return;
            case 6:
                Intent intent4 = new Intent(this.ok, MJ16ViewActivity.class);
                intent4.putExtra("is_network_game", true);
                intent4.putExtra("is_tcp_game", true);
                intent4.putExtra("local_chara", ai.nn);
                this.ok.startActivity(intent4);
                return;
            case 7:
                Intent intent5 = new Intent(this.ok, MJ16ViewActivity.class);
                intent5.putExtra("is_network_game", true);
                intent5.putExtra("is_tcp_localhost_game", true);
                intent5.putExtra("local_chara", ai.nn);
                this.ok.startActivity(intent5);
                return;
            case 8:
                Intent intent6 = new Intent(this.ok, MJ16ViewActivity.class);
                intent6.putExtra("is_network_game", true);
                intent6.putExtra("is_bt_game", true);
                intent6.putExtra("local_chara", ai.nn);
                this.ok.startActivity(intent6);
                return;
            case 9:
                Intent intent7 = new Intent(this.ok, MJ16ViewActivity.class);
                intent7.putExtra("is_network_game", true);
                intent7.putExtra("is_bump_game", true);
                this.ok.startActivity(intent7);
                return;
            case 10:
                this.ok.startActivity(new Intent(this.ok, CapturePlayerActivity.class));
                return;
            case 11:
                this.ok.startActivityForResult(new Intent(this.ok, MJColorPicker.class), 4);
                return;
            case 12:
                this.ok.startActivity(new Intent(this.ok, ChallengeActivity.class));
                return;
            case 13:
                Intent intent8 = new Intent(this.ok, MJ16ViewActivity.class);
                intent8.putExtra("is_local_game", true);
                intent8.putExtra("type", 4);
                intent8.putExtra("is_restore", false);
                this.ok.startActivity(intent8);
                return;
            default:
                return;
        }
    }
}
