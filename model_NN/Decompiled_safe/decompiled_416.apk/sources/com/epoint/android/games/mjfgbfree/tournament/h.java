package com.epoint.android.games.mjfgbfree.tournament;

import android.content.DialogInterface;
import android.content.Intent;

final class h implements DialogInterface.OnClickListener {
    private final /* synthetic */ int hv;
    private /* synthetic */ TournamentActivity su;

    h(TournamentActivity tournamentActivity, int i) {
        this.su = tournamentActivity;
        this.hv = i;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = this.su.getIntent();
        intent.putExtra("tournament_type", this.hv);
        this.su.setResult(-1, intent);
        this.su.finish();
    }
}
