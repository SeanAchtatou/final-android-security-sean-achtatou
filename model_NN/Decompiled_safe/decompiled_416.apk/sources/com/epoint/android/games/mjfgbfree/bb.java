package com.epoint.android.games.mjfgbfree;

import a.a.b;
import a.b.d.a;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Path;
import android.graphics.Picture;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.telephony.TelephonyManager;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.epoint.android.games.mjfgbfree.a.c;
import com.epoint.android.games.mjfgbfree.a.h;
import com.epoint.android.games.mjfgbfree.d.g;
import com.epoint.android.games.mjfgbfree.e.k;
import com.epoint.android.games.mjfgbfree.e.o;
import com.epoint.android.games.mjfgbfree.e.q;
import com.epoint.android.games.mjfgbfree.ui.a.d;
import com.epoint.android.games.mjfgbfree.ui.a.f;
import com.epoint.android.games.mjfgbfree.ui.a.i;
import com.google.ads.AdView;
import com.google.ads.e;
import com.mobclix.android.sdk.ce;
import com.mobclix.android.sdk.l;
import com.wiyun.ad.ak;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

public final class bb extends View implements b, c, e, l, ak, Runnable {
    /* access modifiers changed from: private */
    public static int jQ = 0;
    /* access modifiers changed from: private */
    public static int jR = 0;
    private static boolean jS = false;
    public static Matrix mMatrix = new Matrix();
    private static int tA = 27;
    private static int tB = 21;
    private static int tC = 27;
    public static Hashtable tE = new Hashtable();
    private static Resources tF;
    public static final String[] tG = {"qboy1_h", "qboy2_h", "qboy3_h", "qboy4_h", "qman1_h", "qman2_h", "qman3_h", "qman4_h", "qgirl1_h", "qwoman1_h", "qwoman2_h", "qwoman3_h", "qwoman4_h"};
    public static String[] tH = new String[3];
    public static int ty = MJ16Activity.qJ;
    private static int tz = 21;
    /* access modifiers changed from: private */
    public static volatile Thread uC = null;
    /* access modifiers changed from: private */
    public static int uD = -1;
    /* access modifiers changed from: private */
    public static boolean uE = false;
    /* access modifiers changed from: private */
    public static LinearLayout uF;
    private static final int uz = ("MJ-GB".equals("MJ-16") ? 2 : 3);
    /* access modifiers changed from: private */
    public d fs = null;
    /* access modifiers changed from: private */
    public Vector gM = new Vector();
    public Paint gN;
    /* access modifiers changed from: private */
    public g gO = null;
    private GestureDetector gS;
    /* access modifiers changed from: private */
    public o ho;
    private int jB = -1;
    /* access modifiers changed from: private */
    public Object jF;
    private Object jG = null;
    /* access modifiers changed from: private */
    public MJ16ViewActivity jq = null;
    /* access modifiers changed from: private */
    public Handler mHandler = new h(this);
    private boolean oS = false;
    /* access modifiers changed from: private */
    public a qC = null;
    public int tD = 0;
    private TextPaint tI;
    private Vector tJ = new Vector();
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.ui.a tK;
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.ui.a tL;
    private com.epoint.android.games.mjfgbfree.ui.a tM;
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.ui.a tN;
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.ui.a tO;
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.ui.a tP;
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.ui.a tQ;
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.ui.a tR;
    private com.epoint.android.games.mjfgbfree.ui.a tS;
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.ui.a tT;
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.ui.a tU;
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.ui.a tV;
    private com.epoint.android.games.mjfgbfree.ui.a tW;
    private Vibrator tX;
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.ui.c tY = null;
    /* access modifiers changed from: private */
    public bb tZ = null;
    private bl uA = new bl(this);
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.c.d uB;
    /* access modifiers changed from: private */
    public AdView uG;
    /* access modifiers changed from: private */
    public boolean uH;
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.b.a uI;
    public volatile boolean uJ = false;
    /* access modifiers changed from: private */
    public Vector uK = new Vector();
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.d.d uL = null;
    /* access modifiers changed from: private */
    public int uM;
    private int uN = -1;
    /* access modifiers changed from: private */
    public bk uO;
    /* access modifiers changed from: private */
    public com.wiyun.ad.AdView uP;
    private TelephonyManager uQ;
    public boolean uR = true;
    /* access modifiers changed from: private */
    public String uS;
    private Date uT = null;
    private boolean uU = false;
    /* access modifiers changed from: private */
    public boolean uV = false;
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.ui.a.g ua = null;
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.ui.a.b ub = null;
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.ui.a.c uc = null;
    private f ud = null;
    /* access modifiers changed from: private */
    public i ue = null;
    private com.epoint.android.games.mjfgbfree.ui.a uf;
    private com.epoint.android.games.mjfgbfree.ui.a ug;
    private com.epoint.android.games.mjfgbfree.ui.a uh;
    /* access modifiers changed from: private */
    public com.epoint.android.games.mjfgbfree.ui.a[] ui = new com.epoint.android.games.mjfgbfree.ui.a[4];
    private Bitmap uj;
    private Bitmap uk;
    private Bitmap ul;
    private Bitmap um = null;
    private Canvas un;
    private Bitmap uo;
    private Bitmap up;
    private Bitmap uq;
    private Bitmap ur;
    private int[] us = new int[85];
    public Rect ut = null;
    public volatile boolean uu = true;
    /* access modifiers changed from: private */
    public av uv;
    private Path uw;
    private String ux = "-";
    private boolean uy = true;

    static {
        dV();
    }

    public bb(MJ16ViewActivity mJ16ViewActivity, com.epoint.android.games.mjfgbfree.c.d dVar, LinearLayout linearLayout) {
        super(mJ16ViewActivity);
        try {
            this.uS = h.bi("iN2ZcAKJ0iC5tURuUKUurA==");
        } catch (Exception e) {
            try {
                this.uS = new String(com.epoint.android.games.mjfgbfree.a.e.D("YTE0ZDcwY2MyZjRmNTdh"), "UTF-8");
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
        }
        this.uQ = (TelephonyManager) mJ16ViewActivity.getSystemService("phone");
        this.uM = dVar.cS();
        this.gS = new GestureDetector(new com.epoint.android.games.mjfgbfree.a.g(this));
        uF = linearLayout;
        if (MJ16Activity.qL) {
            dW();
            if (this.uQ == null || !"cn".equals(this.uQ.getNetworkCountryIso())) {
                this.uO = new bk(this, 3000, 11);
            } else {
                this.uO = new bk(this, 3000, 13);
            }
            this.uO.start();
        } else {
            uF.setVisibility(8);
        }
        mJ16ViewActivity.setVolumeControlStream(3);
        this.uB = dVar;
        dVar.a(this);
        this.qC = dVar.cU();
        this.uv = new av(mJ16ViewActivity, this);
        this.uv.setTag(this.uv);
        this.jq = mJ16ViewActivity;
        this.tZ = this;
        this.uw = new Path();
        this.uw.moveTo(44.0f, 0.0f);
        this.uw.lineTo(52.0f, 7.0f);
        this.uw.lineTo(67.0f, 3.0f);
        this.uw.lineTo(67.0f, 9.0f);
        this.uw.lineTo(83.0f, 9.0f);
        this.uw.lineTo(73.0f, 15.0f);
        this.uw.lineTo(88.0f, 17.0f);
        this.uw.lineTo(73.0f, 21.0f);
        this.uw.lineTo(83.0f, 26.0f);
        this.uw.lineTo(66.0f, 26.0f);
        this.uw.lineTo(67.0f, 32.0f);
        this.uw.lineTo(52.0f, 28.0f);
        this.uw.lineTo(45.0f, 35.0f);
        this.uw.lineTo(37.0f, 28.0f);
        this.uw.lineTo(22.0f, 32.0f);
        this.uw.lineTo(23.0f, 26.0f);
        this.uw.lineTo(6.0f, 26.0f);
        this.uw.lineTo(16.0f, 20.0f);
        this.uw.lineTo(0.0f, 17.0f);
        this.uw.lineTo(16.0f, 15.0f);
        this.uw.lineTo(6.0f, 9.0f);
        this.uw.lineTo(23.0f, 10.0f);
        this.uw.lineTo(22.0f, 3.0f);
        this.uw.lineTo(36.0f, 7.0f);
        this.uw.close();
        tF = this.jq.getResources();
        this.uI = new com.epoint.android.games.mjfgbfree.b.a(this.jq, ai.nl);
    }

    private void E(int i) {
        if (this.uN != i) {
            this.uN = i;
            Bitmap c = com.epoint.android.games.mjfgbfree.a.d.c(this.jq, "images/tiles_base.png");
            Bitmap copy = c.copy(Bitmap.Config.RGB_565, true);
            c.recycle();
            Bitmap c2 = com.epoint.android.games.mjfgbfree.a.d.c(this.jq, "images/tiles_" + (i == c.ef ? "cn" : i == c.eg ? "mix" : "en") + ".png");
            new Canvas(copy).drawBitmap(c2, 0.0f, 0.0f, (Paint) null);
            c2.recycle();
            tE.put("tiles_hd", copy);
            d(this.jq, this.jq.eL().sm);
        }
    }

    private synchronized void F(int i) {
        String str;
        boolean z;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        synchronized (this.gO) {
            boolean z2 = (this.gO.aG + 2) % 4 == i;
            this.um.eraseColor(0);
            com.epoint.android.games.mjfgbfree.d.a.a aVar = this.gO.vU[i];
            int width = ((Bitmap) tE.get("bc")).getWidth();
            int height = ((Bitmap) tE.get("bc")).getHeight();
            if (this.gO.lb == i) {
                String str2 = String.valueOf(String.valueOf("") + "[") + af.aa("莊");
                if (this.gO.vL > 0) {
                    str2 = String.valueOf(str2) + this.gO.vL;
                }
                str = String.valueOf(str2) + "]";
            } else {
                str = "";
            }
            this.tI.setTextAlign(Paint.Align.CENTER);
            this.un.save();
            this.un.rotate(180.0f, ((float) this.um.getWidth()) / 2.0f, ((float) this.um.getHeight()) / 2.0f);
            String str3 = String.valueOf(this.uB.e(i) ? String.valueOf(aVar.xx) + " " : "") + "$:" + aVar.gd + "";
            boolean z3 = MJ16Activity.qZ > MJ16Activity.ra;
            if ((this.gO.aG + 1) % 4 == i && MJ16Activity.rd && z3) {
                this.tI.setTextAlign(Paint.Align.LEFT);
                a(this.un, "$:" + aVar.gd + "" + (this.uB.e(i) ? " " + aVar.xx : ""), 20, this.un.getHeight() - 4, this.un.getWidth() - 20);
            } else if ((this.gO.aG + 3) % 4 != i || !MJ16Activity.rd || !z3) {
                a(this.un, str3, this.un.getWidth() / 2, this.un.getHeight() - 4, this.un.getWidth() - 10);
            } else {
                this.tI.setTextAlign(Paint.Align.RIGHT);
                a(this.un, str3, this.un.getWidth() - 20, this.un.getHeight() - 4, this.un.getWidth() - 20);
            }
            this.un.restore();
            this.tI.setTextAlign(Paint.Align.LEFT);
            Rect rect = new Rect();
            rect.left = 0;
            rect.top = 0;
            rect.right = width;
            rect.bottom = height;
            Rect rect2 = new Rect();
            int i9 = 2;
            int i10 = height + 22 + 3;
            int i11 = width * 1;
            if (aVar.fX.size() > 0) {
                int i12 = 0;
                int i13 = 0;
                int i14 = 2;
                int i15 = 0;
                while (i12 < aVar.fX.size()) {
                    String str4 = (String) aVar.fX.elementAt(i12);
                    if (i13 != 0) {
                        rect2.left = i14;
                        rect2.top = 22;
                        rect2.right = rect2.left + (rect.right * 1);
                        rect2.bottom = rect2.top + (rect.bottom * 1);
                        if (str4.length() == 1) {
                            i7 = i14 + 3;
                            i6 = 0;
                            i8 = i13 + 1;
                        } else {
                            int i16 = i15 + 1;
                            if (i16 == 4) {
                                int width2 = rect2.width();
                                int height2 = rect2.height();
                                rect2.left -= (i11 - 1) * 2;
                                rect2.top -= 8;
                                rect2.right = width2 + rect2.left;
                                if (z2) {
                                    rect2.top += 16;
                                }
                                rect2.bottom = rect2.top + height2;
                                if (!z2) {
                                    a(z2, str4, this.un, rect, rect2);
                                    i8 = i13;
                                    i6 = i16;
                                    i7 = i14;
                                } else {
                                    i8 = i13;
                                    i6 = i16;
                                    i7 = i14;
                                }
                            } else {
                                if (!z2) {
                                    a(z2, str4, this.un, rect, rect2);
                                }
                                i6 = i16;
                                i7 = (i11 - 1) + i14;
                                i8 = i13;
                            }
                        }
                    } else if (str4.length() == 1) {
                        i7 = i14;
                        int i17 = i15;
                        i8 = 1;
                        i6 = i17;
                    } else {
                        i6 = i15;
                        i7 = i14;
                        i8 = i13;
                    }
                    i12++;
                    i13 = i8;
                    i14 = i7;
                    i15 = i6;
                }
                i9 = i14 + 5 + i11;
            }
            rect.left = 0;
            rect.top = 0;
            rect.right = width;
            rect.bottom = height;
            if (!z2) {
                for (int i18 = 0; i18 < aVar.fV.size(); i18++) {
                    if (!uE && aVar.fV.size() % 3 == 2 && aVar.fV.size() - 1 == i18 && (this.gO.lh == 0 || this.gO.wf.size() > 0)) {
                        i9 += 5;
                        if (this.gO.wf.size() > 1) {
                            this.gN.setAlpha(160);
                        }
                    }
                    int i19 = i9;
                    rect2.left = i19;
                    rect2.top = 22;
                    rect2.right = rect2.left + (rect.right * 1);
                    rect2.bottom = rect2.top + (rect.bottom * 1);
                    String str5 = (String) aVar.fV.elementAt(i18);
                    if (str5.equals("BK")) {
                        this.un.drawBitmap((Bitmap) tE.get(str5), rect, rect2, this.gN);
                    } else {
                        a(z2, str5, this.un, rect, rect2);
                    }
                    i9 = (width - 1) + i19;
                }
            }
            if (z2) {
                int i20 = 0;
                int i21 = 0;
                while (true) {
                    if (i21 >= aVar.fX.size()) {
                        break;
                    } else if (((String) aVar.fX.elementAt(i21)).length() != 1) {
                        i20++;
                        i21++;
                    } else if (i20 == 4) {
                        z = true;
                    }
                }
            }
            z = true;
            this.gN.setAlpha(255);
            int i22 = 0;
            int i23 = 2;
            while (i22 < aVar.fZ.size()) {
                String str6 = (String) aVar.fZ.elementAt(i22);
                rect2.left = i23;
                rect2.top = i10;
                rect2.right = rect2.left + (rect.right * 1);
                rect2.bottom = rect2.top + (rect.bottom * 1);
                a(z2, str6, this.un, rect, rect2);
                if (str6.equals(this.gO.vO)) {
                    int color = this.gN.getColor();
                    this.gN.setStyle(Paint.Style.STROKE);
                    this.gN.setColor(-65536);
                    rect2.right -= 2;
                    rect2.bottom -= 2;
                    this.un.drawRect(rect2, this.gN);
                    this.gN.setStyle(Paint.Style.FILL);
                    this.gN.setColor(color);
                }
                i22++;
                i23 = (i11 - 1) + i23;
            }
            int i24 = aVar.fZ.size() > 0 ? i23 + 3 : i23;
            if (aVar.fX.size() > 0) {
                int i25 = 0;
                int i26 = i24;
                int i27 = 0;
                while (i25 < aVar.fX.size()) {
                    String str7 = (String) aVar.fX.elementAt(i25);
                    if (str7.length() == 1) {
                        break;
                    }
                    rect2.left = i26;
                    rect2.top = i10;
                    if (z && z2) {
                        rect2.top -= 4;
                    }
                    rect2.right = rect2.left + (rect.right * 1);
                    rect2.bottom = rect2.top + (rect.bottom * 1);
                    int i28 = i27 + 1;
                    if (i28 == 4) {
                        int width3 = rect2.width();
                        int height3 = rect2.height();
                        rect2.left -= (i11 - 1) * 2;
                        rect2.top -= 8;
                        rect2.right = width3 + rect2.left;
                        if (z2) {
                            rect2.top += 14;
                        }
                        rect2.bottom = rect2.top + height3;
                        a(z2, str7, this.un, rect, rect2);
                        i5 = i26;
                    } else {
                        a(z2, str7, this.un, rect, rect2);
                        i5 = (i11 - 1) + i26;
                    }
                    i25++;
                    i26 = i5;
                    i27 = i28;
                }
                i24 = i26 + 3;
            }
            a(this.un, str, i24, (i10 - 12) + height);
            if (z2) {
                int i29 = 2;
                if (aVar.fX.size() > 0) {
                    int i30 = 0;
                    int i31 = 0;
                    int i32 = 2;
                    int i33 = 0;
                    while (i30 < aVar.fX.size()) {
                        String str8 = (String) aVar.fX.elementAt(i30);
                        if (i31 != 0) {
                            rect2.left = i32;
                            rect2.top = 22;
                            rect2.right = rect2.left + (rect.right * 1);
                            rect2.bottom = rect2.top + (rect.bottom * 1);
                            if (str8.length() == 1) {
                                i3 = i32 + 3;
                                i2 = 0;
                                i4 = i31 + 1;
                            } else {
                                int i34 = i33 + 1;
                                if (i34 == 4) {
                                    int width4 = rect2.width();
                                    int height4 = rect2.height();
                                    rect2.left -= (i11 - 1) * 2;
                                    rect2.top -= 8;
                                    rect2.right = width4 + rect2.left;
                                    if (z2) {
                                        rect2.top += 16;
                                    }
                                    rect2.bottom = rect2.top + height4;
                                    a(z2, str8, this.un, rect, rect2);
                                    i4 = i31;
                                    i2 = i34;
                                    i3 = i32;
                                } else {
                                    a(z2, str8, this.un, rect, rect2);
                                    i2 = i34;
                                    i3 = (i11 - 1) + i32;
                                    i4 = i31;
                                }
                            }
                        } else if (str8.length() == 1) {
                            i3 = i32;
                            int i35 = i33;
                            i4 = 1;
                            i2 = i35;
                        } else {
                            i2 = i33;
                            i3 = i32;
                            i4 = i31;
                        }
                        i30++;
                        i31 = i4;
                        i32 = i3;
                        i33 = i2;
                    }
                    i29 = i32 + 5 + i11;
                }
                rect.left = 0;
                rect.top = 0;
                rect.right = width;
                rect.bottom = height;
                for (int i36 = 0; i36 < aVar.fV.size(); i36++) {
                    if (!uE && aVar.fV.size() % 3 == 2 && aVar.fV.size() - 1 == i36 && (this.gO.lh == 0 || this.gO.wf.size() > 0)) {
                        i29 += 5;
                        if (this.gO.wf.size() > 1) {
                            this.gN.setAlpha(160);
                        }
                    }
                    int i37 = i29;
                    rect2.left = i37;
                    rect2.top = 22;
                    rect2.right = rect2.left + (rect.right * 1);
                    rect2.bottom = rect2.top + (rect.bottom * 1);
                    String str9 = (String) aVar.fV.elementAt(i36);
                    if (str9.equals("BK")) {
                        this.un.drawBitmap((Bitmap) tE.get(str9), rect, rect2, this.gN);
                    } else {
                        a(z2, str9, this.un, rect, rect2);
                    }
                    i29 = (width - 1) + i37;
                }
            }
            this.gN.setAlpha(255);
        }
    }

    private com.epoint.android.games.mjfgbfree.d.c G(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.gO.wf.size()) {
                return null;
            }
            com.epoint.android.games.mjfgbfree.d.c cVar = (com.epoint.android.games.mjfgbfree.d.c) this.gO.wf.elementAt(i3);
            if (cVar.lJ == i) {
                return cVar;
            }
            i2 = i3 + 1;
        }
    }

    private boolean H(int i) {
        for (int i2 = 0; i2 < this.gO.wf.size(); i2++) {
            if (((com.epoint.android.games.mjfgbfree.d.c) this.gO.wf.elementAt(i2)).lK == i) {
                return true;
            }
        }
        return false;
    }

    static /* synthetic */ Bitmap a(bb bbVar, int i) {
        Bitmap copy = MJ16Activity.ri.copy(Bitmap.Config.RGB_565, true);
        WebView webView = (WebView) bbVar.jq.findViewById(C0000R.id.ResultView);
        Picture capturePicture = webView.capturePicture();
        Canvas canvas = new Canvas(copy);
        int top = ((LinearLayout) webView.getParent()).getTop();
        int left = ((LinearLayout) webView.getParent()).getLeft();
        Rect rect = new Rect();
        rect.left = left;
        rect.top = top;
        rect.right = rect.left + capturePicture.getWidth();
        rect.bottom = rect.top + Math.min(webView.getHeight() + i, capturePicture.getHeight());
        if (MJ16Activity.rc != 1.0f) {
            float f = ((float) MJ16Activity.qS) / ((float) MJ16Activity.qZ);
            float f2 = ((float) MJ16Activity.qR) / ((float) MJ16Activity.ra);
            rect.left = (int) (((float) rect.left) / f);
            rect.top = (int) (((float) rect.top) / f2);
            rect.right = (int) (((float) rect.right) / f);
            rect.bottom = (int) (((float) rect.bottom) / f2);
        }
        canvas.drawPicture(capturePicture, rect);
        return copy;
    }

    private static void a(Canvas canvas, Bitmap bitmap, int i, Rect rect) {
        Rect rect2 = new Rect();
        bitmap.eraseColor(0);
        canvas.drawBitmap((Bitmap) tE.get("BC_INV"), 0.0f, 0.0f, (Paint) null);
        if (i <= 34) {
            rect2.left = ((i - 1) % 9) * tB;
            rect2.top = ((i - 1) / 9) * tC;
        } else {
            rect2.left = (i - 35) * tB;
            rect2.top = tC * 4;
        }
        rect2.right = rect2.left + tB;
        rect2.bottom = rect2.top + tC;
        canvas.drawBitmap((Bitmap) tE.get("tiles_hd"), rect2, rect, (Paint) null);
        tE.put(a.a.e.nv[i], bitmap.copy(Bitmap.Config.ARGB_8888, false));
    }

    private synchronized void a(Canvas canvas, Rect rect) {
        Vector vector = this.uA.Aw;
        int height = ((Bitmap) tE.get("bc")).getHeight() + 5;
        int width = ((Bitmap) tE.get("bc")).getWidth();
        int i = (vector.size() > 6 ? vector.size() > 14 ? 8 : 7 : 6) * width;
        int i2 = rect.left + ((rect.right - rect.left) / 2) > MJ16Activity.qZ / 2 ? (rect.left - i) - 30 : rect.right + 30;
        Rect bounds = this.uh.getBounds();
        bounds.right = bounds.left + i + 20;
        if (vector.size() <= 7) {
            bounds.bottom = bounds.top + (height * 2) + 20;
        } else {
            bounds.bottom = bounds.top + (height * 3) + 20;
        }
        this.uh.setBounds(bounds);
        this.uh.c(i2 - 10, rect.top - (70 - (vector.size() <= 7 ? height : 0)));
        this.uh.draw(canvas);
        Paint.Align textAlign = this.gN.getTextAlign();
        this.gN.setTextAlign(Paint.Align.CENTER);
        int color = this.gN.getColor();
        this.gN.setColor(-10485760);
        canvas.drawText(af.aa("棄此牌後叫糊"), (float) (this.uh.getBounds().left + ((this.uh.getBounds().right - this.uh.getBounds().left) / 2)), (float) (this.uh.getBounds().top + 30), this.gN);
        this.gN.setColor(color);
        this.gN.setTextAlign(textAlign);
        int size = vector.size() < 6 ? ((i - (vector.size() * width)) / 2) + i2 : i2;
        Rect rect2 = new Rect();
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        for (int i3 = 0; i3 < vector.size(); i3++) {
            if (vector.size() > 14) {
                rect2.top = this.uh.getBounds().top + 50 + ((i3 / 8) * (height + 5));
                rect2.left = ((i3 % 8) * width) + size;
            } else {
                rect2.top = this.uh.getBounds().top + 50 + ((i3 / 7) * (height + 5));
                rect2.left = ((i3 % 7) * width) + size;
            }
            canvas.drawBitmap((Bitmap) tE.get(vector.elementAt(i3)), (float) rect2.left, (float) rect2.top, this.gN);
            rect2.right = rect2.left + ((Bitmap) tE.get(vector.elementAt(i3))).getWidth();
            rect2.bottom = rect2.top + ((Bitmap) tE.get(vector.elementAt(i3))).getHeight();
            if (this.gO.wd[((a.a.a) a.a.c.mu.get(vector.elementAt(i3))).aM()] == 4) {
                paint.setColor(-12303292);
                paint.setAlpha(80);
                MJ16Activity.rj.drawRect(rect2, paint);
                paint.setColor(-16777216);
                paint.setAlpha(255);
                paint.setStrokeWidth(1.0f);
                paint.setAntiAlias(true);
                MJ16Activity.rj.drawLine((float) (rect2.left + 1), (float) (rect2.top + 1), (float) (rect2.right - 1), (float) (rect2.bottom - 1), paint);
                paint.setStrokeWidth(0.0f);
            }
        }
    }

    private void a(Canvas canvas, String str, int i, int i2) {
        a(canvas, str, i, i2, -1);
    }

    private void a(Canvas canvas, String str, int i, int i2, int i3) {
        canvas.drawText(i3 != -1 ? TextUtils.ellipsize(str, this.tI, (float) i3, TextUtils.TruncateAt.MIDDLE).toString() : str, (float) i, (float) i2, this.tI);
    }

    static /* synthetic */ void a(bb bbVar, Object obj) {
        boolean z;
        if (!(obj instanceof com.epoint.android.games.mjfgbfree.ui.c) && (obj instanceof com.epoint.android.games.mjfgbfree.ui.a)) {
            ((com.epoint.android.games.mjfgbfree.ui.a) obj).b(false);
            if (obj != bbVar.tW) {
                if (obj == bbVar.tK) {
                    bbVar.uB.t(bbVar.gO.aG);
                    z = true;
                } else {
                    if (obj == bbVar.tL) {
                        if (bbVar.tY != null) {
                            bbVar.uB.b(bbVar.gO.aG, bbVar.tY.lw);
                            z = true;
                        }
                    } else if (obj == bbVar.tM) {
                        if (bbVar.gO.li == bbVar.gO.aG || bbVar.gO.we.size() <= 0) {
                            bbVar.uB.w(bbVar.gO.aG);
                            z = true;
                        } else {
                            bbVar.gO.we.clear();
                            bbVar.tV.a(false, false);
                            bbVar.tM.a(false, false);
                            bbVar.er();
                            bbVar.en();
                        }
                    } else if (obj == bbVar.tN) {
                        ((com.epoint.android.games.mjfgbfree.ui.a) obj).a(false, false);
                        uC = null;
                        bbVar.eg();
                        bbVar.ue = bbVar.ua;
                        bbVar.ua.a(bbVar.jq, bbVar.tZ);
                        bbVar.ua.d(bbVar.gO.wb, bbVar.gO.vO);
                        if (!bbVar.ua.h(true)) {
                            ((com.epoint.android.games.mjfgbfree.ui.a) obj).a(true, false);
                            bbVar.eg();
                        }
                        z = false;
                    } else if (obj == bbVar.tO) {
                        if (bbVar.gO.vV.size() > 0) {
                            ((com.epoint.android.games.mjfgbfree.ui.a) obj).a(false, false);
                            uC = null;
                            bbVar.eg();
                            bbVar.ue = bbVar.ua;
                            bbVar.ua.a(bbVar.jq, bbVar.tZ);
                            bbVar.ua.h(bbVar.gO.vV);
                            if (bbVar.gO.li == bbVar.gO.aG) {
                                int i = 0;
                                while (true) {
                                    if (i >= bbVar.tJ.size()) {
                                        break;
                                    } else if (((com.epoint.android.games.mjfgbfree.ui.c) bbVar.tJ.elementAt(i)).lw.equals(bbVar.gO.vV.elementAt(0))) {
                                        bbVar.ut = ((com.epoint.android.games.mjfgbfree.ui.c) bbVar.tJ.elementAt(i)).nZ;
                                        break;
                                    } else {
                                        i++;
                                    }
                                }
                            }
                            if (!bbVar.ua.h(true)) {
                                ((com.epoint.android.games.mjfgbfree.ui.a) obj).a(true, false);
                                bbVar.eg();
                            }
                            z = false;
                        }
                    } else if (obj == bbVar.tS) {
                        bbVar.uB.v(bbVar.gO.aG);
                        z = true;
                    } else if (obj == bbVar.tT) {
                        if (bbVar.ho != null) {
                            bbVar.ho.a(0, null);
                            return;
                        } else {
                            bbVar.n(false);
                            return;
                        }
                    } else if (obj == bbVar.tU) {
                        ((com.epoint.android.games.mjfgbfree.ui.a) obj).a(false, false);
                        bbVar.eo();
                        z = false;
                    } else if (obj == bbVar.tV) {
                        ((com.epoint.android.games.mjfgbfree.ui.a) obj).a(false, false);
                        Message message = new Message();
                        message.what = 9;
                        bbVar.mHandler.sendMessage(message);
                        z = false;
                    } else {
                        new j(bbVar, obj).start();
                        return;
                    }
                    z = false;
                }
                uC = null;
                if (z && bbVar.uB.cX()) {
                    bbVar.gO = bbVar.uB.P();
                    bbVar.ek();
                    bbVar.el();
                    if (obj == bbVar.tL) {
                        bbVar.ep();
                        i iVar = new i(bbVar);
                        uC = iVar;
                        iVar.start();
                        return;
                    }
                    bbVar.en();
                }
            } else if (DiscardedTilesActivity.bv() == null) {
                bbVar.ej();
            }
        }
    }

    private synchronized void a(g gVar, int i, com.epoint.android.games.mjfgbfree.d.c cVar, boolean z) {
        Bitmap bitmap = null;
        synchronized (this) {
            this.ui[i].a(false, false);
            this.ui[i].gx = "S";
            if (z) {
                this.ui[i].a(true, false);
                this.ui[i].v(af.aa("出沖"));
                this.ui[i].gx = null;
            } else if (gVar.li == i || cVar != null) {
                if ((gVar.aG + 1) % 4 == i) {
                    bitmap = (Bitmap) tE.get(tH[0]);
                } else if ((gVar.aG + 2) % 4 == i) {
                    bitmap = (Bitmap) tE.get(tH[1]);
                } else if ((gVar.aG + 3) % 4 == i) {
                    bitmap = (Bitmap) tE.get(tH[2]);
                }
                if (cVar != null) {
                    if (gVar.vU[i].fV.size() % 3 == 1) {
                        this.ui[i].v(af.aa("花糊exp"));
                    } else if (cVar.lQ) {
                        this.ui[i].v(af.aa("搶槓"));
                    } else if (cVar.lK == -1) {
                        this.ui[i].v(af.aa("自摸exp"));
                    } else {
                        this.ui[i].v(af.aa("食糊exp"));
                    }
                    this.ui[i].a(true, false);
                } else if (gVar.lh != 0) {
                    switch (gVar.lh) {
                        case 3:
                        case 4:
                            this.uI.a(this.uI.O);
                            com.epoint.android.games.mjfgbfree.ui.b.a(MJ16Activity.rj, this.un, this.um, bitmap, gVar.aG, gVar.li, this.gN);
                            this.ui[i].a(true, false);
                            this.ui[i].v(af.aa("槓"));
                            this.ui[i].gx = null;
                            break;
                    }
                } else if (af.X(gVar.vO) >= 35) {
                    this.uI.a(this.uI.Q);
                    com.epoint.android.games.mjfgbfree.ui.b.a(MJ16Activity.rj, this.un, this.um, bitmap, gVar.aG, gVar.li, this.gN);
                    this.ui[i].a(true, false);
                    this.ui[i].v(af.aa("補花"));
                    this.ui[i].gx = null;
                }
            }
        }
    }

    private void a(String str, f fVar, boolean z, o oVar, boolean z2) {
        while (true) {
            if (!com.epoint.android.games.mjfgbfree.ui.a.e.jz && !this.uu) {
                break;
            }
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
            }
        }
        this.ue = fVar;
        fVar.a(this.jq, this.tZ);
        fVar.a(str, oVar, z);
        fVar.h(z2);
    }

    private void a(boolean z, String str, Canvas canvas, Rect rect, Rect rect2) {
        boolean z2;
        String str2;
        Rect rect3;
        Rect rect4;
        if (str.length() == 3) {
            str2 = str.substring(0, 2);
            z2 = true;
        } else {
            z2 = false;
            str2 = str;
        }
        Bitmap bitmap = (Bitmap) tE.get(str2);
        if (z2) {
            Bitmap b2 = b(bitmap, z ? 270 : 90);
            int height = bitmap.getHeight() - b2.getHeight();
            Rect rect5 = new Rect(rect2);
            Rect rect6 = new Rect(rect);
            rect5.top += height;
            rect5.right += height;
            rect6.bottom -= height;
            rect6.right = height + rect6.right;
            bitmap = b2;
            rect3 = rect5;
            rect4 = rect6;
        } else {
            rect3 = rect2;
            rect4 = rect;
        }
        if (!z || z2) {
            canvas.drawBitmap(bitmap, rect4, rect3, this.gN);
        } else if (str2.equals("BK")) {
            canvas.drawBitmap((Bitmap) tE.get("BK_INV"), rect4, rect3, this.gN);
        } else {
            Rect rect7 = new Rect(rect4);
            Rect rect8 = new Rect(rect3);
            canvas.drawBitmap((Bitmap) tE.get("bc"), rect4, rect3, this.gN);
            rect7.top += 2;
            rect7.left += 2;
            rect7.right -= 2;
            rect7.bottom -= 8;
            rect8.top += 8;
            rect8.bottom -= 2;
            rect8.left += 2;
            rect8.right -= 2;
            canvas.drawBitmap((Bitmap) tE.get(str2), rect7, rect8, this.gN);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private Bitmap b(Bitmap bitmap, int i) {
        Bitmap createBitmap = Bitmap.createBitmap(((Bitmap) tE.get("bc90")).getWidth(), ((Bitmap) tE.get("bc90")).getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap((Bitmap) tE.get("bc90"), 0.0f, 0.0f, this.gN);
        canvas.drawBitmap(bitmap, new Rect(2, 2, 21, 28), new Rect(1, 1, 20, 25), this.gN);
        mMatrix.reset();
        mMatrix.postRotate((float) i);
        Bitmap createBitmap2 = Bitmap.createBitmap(createBitmap, 0, 0, createBitmap.getWidth(), createBitmap.getHeight(), mMatrix, true);
        createBitmap.recycle();
        return createBitmap2;
    }

    private Object b(int i, int i2, int i3) {
        int i4 = 0;
        com.epoint.android.games.mjfgbfree.ui.a aVar = null;
        while (i4 < this.gM.size()) {
            com.epoint.android.games.mjfgbfree.ui.a aVar2 = (com.epoint.android.games.mjfgbfree.ui.a) this.gM.elementAt(i4);
            aVar2.b(false);
            if (aVar2.isVisible() && aVar2.bf()) {
                Rect bounds = aVar2.getBounds();
                if (i >= bounds.left && i <= bounds.right && i2 >= bounds.top && i2 <= bounds.bottom && (i3 == 0 || (i3 != 0 && this.jF == aVar2))) {
                    aVar2.b(true);
                    i4++;
                    aVar = aVar2;
                }
            }
            aVar2 = aVar;
            i4++;
            aVar = aVar2;
        }
        if (aVar != null) {
            return aVar;
        }
        if (this.tL != null && this.tL.isVisible()) {
            for (int i5 = 0; i5 < this.gO.vU[this.gO.aG].fV.size(); i5++) {
                if (i5 < this.tJ.size()) {
                    com.epoint.android.games.mjfgbfree.ui.c cVar = (com.epoint.android.games.mjfgbfree.ui.c) this.tJ.elementAt(i5);
                    if (i >= cVar.nZ.left && i <= cVar.nZ.right && i2 >= cVar.nZ.top && i2 <= cVar.nZ.bottom + 90) {
                        return cVar;
                    }
                }
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static void d(Context context, int i) {
        tE.put("BK", com.epoint.android.games.mjfgbfree.a.d.c(context, "images/bk.gif"));
        tE.put("bc", com.epoint.android.games.mjfgbfree.a.d.c(context, "images/bc.gif"));
        Bitmap createBitmap = Bitmap.createBitmap(((Bitmap) tE.get("BK")).getWidth(), ((Bitmap) tE.get("BK")).getHeight(), Bitmap.Config.ARGB_8888);
        createBitmap.eraseColor(0);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap((Bitmap) tE.get("BK"), 0.0f, 0.0f, (Paint) null);
        canvas.drawColor(i, PorterDuff.Mode.SCREEN);
        canvas.drawBitmap((Bitmap) tE.get("frame_none"), 0.0f, 0.0f, (Paint) null);
        createBitmap.setPixel(0, 0, 0);
        createBitmap.setPixel(createBitmap.getWidth() - 1, 0, 0);
        createBitmap.setPixel(0, createBitmap.getHeight() - 1, 0);
        createBitmap.setPixel(createBitmap.getWidth() - 1, createBitmap.getHeight() - 1, 0);
        tE.put("BK", createBitmap.copy(Bitmap.Config.ARGB_8888, false));
        createBitmap.eraseColor(0);
        canvas.drawBitmap((Bitmap) tE.get("bc"), 0.0f, 0.0f, (Paint) null);
        canvas.drawColor(i, PorterDuff.Mode.SCREEN);
        canvas.drawBitmap((Bitmap) tE.get("frame"), 0.0f, 0.0f, (Paint) null);
        createBitmap.setPixel(0, 0, 0);
        createBitmap.setPixel(createBitmap.getWidth() - 1, 0, 0);
        createBitmap.setPixel(0, createBitmap.getHeight() - 1, 0);
        createBitmap.setPixel(createBitmap.getWidth() - 1, createBitmap.getHeight() - 1, 0);
        tE.put("bc", createBitmap.copy(Bitmap.Config.ARGB_8888, false));
        mMatrix.reset();
        mMatrix.postRotate(180.0f);
        tE.put("BC_INV", Bitmap.createBitmap(createBitmap, 0, 0, ((Bitmap) tE.get("frame")).getWidth(), ((Bitmap) tE.get("frame")).getHeight(), mMatrix, true));
        tE.put("BK_INV", Bitmap.createBitmap((Bitmap) tE.get("BK"), 0, 0, ((Bitmap) tE.get("frame")).getWidth(), ((Bitmap) tE.get("frame")).getHeight(), mMatrix, true));
        Rect rect = new Rect();
        rect.left = 1;
        rect.top = 2;
        rect.right = rect.left + tz;
        rect.bottom = rect.top + tA;
        tB = ((Bitmap) tE.get("tiles_hd")).getWidth() / 9;
        tC = ((Bitmap) tE.get("tiles_hd")).getHeight() / 5;
        canvas.setDrawFilter(new PaintFlagsDrawFilter(0, 6));
        a(canvas, createBitmap, 1, rect);
        a(canvas, createBitmap, 2, rect);
        a(canvas, createBitmap, 3, rect);
        a(canvas, createBitmap, 4, rect);
        a(canvas, createBitmap, 5, rect);
        a(canvas, createBitmap, 6, rect);
        a(canvas, createBitmap, 7, rect);
        a(canvas, createBitmap, 8, rect);
        a(canvas, createBitmap, 9, rect);
        a(canvas, createBitmap, 10, rect);
        a(canvas, createBitmap, 11, rect);
        a(canvas, createBitmap, 12, rect);
        a(canvas, createBitmap, 13, rect);
        a(canvas, createBitmap, 14, rect);
        a(canvas, createBitmap, 15, rect);
        a(canvas, createBitmap, 16, rect);
        a(canvas, createBitmap, 17, rect);
        a(canvas, createBitmap, 18, rect);
        a(canvas, createBitmap, 19, rect);
        a(canvas, createBitmap, 20, rect);
        a(canvas, createBitmap, 21, rect);
        a(canvas, createBitmap, 22, rect);
        a(canvas, createBitmap, 23, rect);
        a(canvas, createBitmap, 24, rect);
        a(canvas, createBitmap, 25, rect);
        a(canvas, createBitmap, 26, rect);
        a(canvas, createBitmap, 27, rect);
        a(canvas, createBitmap, 28, rect);
        a(canvas, createBitmap, 29, rect);
        a(canvas, createBitmap, 30, rect);
        a(canvas, createBitmap, 31, rect);
        a(canvas, createBitmap, 32, rect);
        a(canvas, createBitmap, 33, rect);
        a(canvas, createBitmap, 34, rect);
        a(canvas, createBitmap, 35, rect);
        a(canvas, createBitmap, 36, rect);
        a(canvas, createBitmap, 37, rect);
        a(canvas, createBitmap, 38, rect);
        a(canvas, createBitmap, 39, rect);
        a(canvas, createBitmap, 40, rect);
        a(canvas, createBitmap, 41, rect);
        a(canvas, createBitmap, 42, rect);
        canvas.setDrawFilter(null);
        Bitmap c = com.epoint.android.games.mjfgbfree.a.d.c(context, "images/bc90.gif");
        Bitmap createBitmap2 = Bitmap.createBitmap(c.getWidth(), c.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas2 = new Canvas(createBitmap2);
        createBitmap2.eraseColor(0);
        canvas2.drawBitmap(c, 0.0f, 0.0f, (Paint) null);
        canvas2.drawColor(i, PorterDuff.Mode.SCREEN);
        canvas2.drawBitmap(com.epoint.android.games.mjfgbfree.a.d.c(context, "images/frame90.gif"), 0.0f, 0.0f, (Paint) null);
        createBitmap2.setPixel(0, 0, 0);
        createBitmap2.setPixel(createBitmap2.getWidth() - 1, 0, 0);
        createBitmap2.setPixel(0, createBitmap2.getHeight() - 1, 0);
        createBitmap2.setPixel(createBitmap2.getWidth() - 1, createBitmap2.getHeight() - 1, 0);
        tE.put("bc90", createBitmap2.copy(Bitmap.Config.ARGB_8888, false));
    }

    public static void dQ() {
        uD = -1;
        uE = false;
        jS = false;
        jR = 0;
        jQ = 0;
        uC = null;
        com.epoint.android.games.mjfgbfree.ui.a.e.jz = false;
        com.epoint.android.games.mjfgbfree.c.d.qB = -1;
    }

    public static int dR() {
        if (uF.getVisibility() == 0) {
            return (int) (((float) MJ16ViewActivity.wn) / MJ16Activity.rc);
        }
        return 0;
    }

    public static boolean dU() {
        if (uC == null) {
            return com.epoint.android.games.mjfgbfree.ui.a.e.jz;
        }
        return true;
    }

    public static void dV() {
        if (tH[0] != null) {
            for (int i = 0; i < tH.length; i++) {
                if (tE.containsKey(tH[i])) {
                    ((Bitmap) tE.remove(tH[i])).recycle();
                }
            }
        }
        for (int i2 = 0; i2 < tG.length; i2++) {
            String str = tG[i2];
            int random = (int) (Math.random() * ((double) tG.length));
            tG[i2] = tG[random];
            tG[random] = str;
        }
        for (int i3 = 0; i3 < tH.length; i3++) {
            tH[i3] = tG[i3];
        }
    }

    public static void dW() {
        if (MJ16Activity.qJ != 1) {
            if (MJ16Activity.qZ > MJ16Activity.ra) {
                ty = 0;
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) uF.getLayoutParams();
                layoutParams.addRule(10);
                layoutParams.addRule(12, 0);
                return;
            }
            ty = 1;
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) uF.getLayoutParams();
            layoutParams2.addRule(12);
            layoutParams2.addRule(10, 0);
        }
    }

    private synchronized void e(Thread thread) {
        this.uu = true;
        Message message = new Message();
        message.what = 10;
        message.obj = thread;
        this.mHandler.sendMessage(message);
    }

    public static boolean eA() {
        return uE;
    }

    private synchronized void ee() {
        int i;
        int i2;
        com.epoint.android.games.mjfgbfree.ui.c cVar;
        int size = this.gO.vU[this.gO.aG].fV.size();
        if (MJ16Activity.qZ > MJ16Activity.ra || size < 16) {
            float f = MJ16Activity.qZ >= 800 ? size >= 16 ? 0.05f : 0.1f : 0.0f;
            i = (int) (((double) ((Bitmap) tE.get("bc")).getWidth()) * (((double) f) + 1.4d));
            i2 = ((int) (((double) ((Bitmap) tE.get("bc")).getHeight()) * (((double) f) + 1.4d))) + 1;
        } else {
            i = (int) (((double) ((Bitmap) tE.get("bc")).getWidth()) * 1.13d);
            i2 = ((int) (((double) ((Bitmap) tE.get("bc")).getHeight()) * 1.13d)) + 1;
        }
        com.epoint.android.games.mjfgbfree.ui.c cVar2 = null;
        int i3 = 0;
        while (i3 < size) {
            Bitmap bitmap = (Bitmap) tE.get(this.gO.vU[this.gO.aG].fV.elementAt(i3));
            Bitmap bitmap2 = bitmap == null ? (Bitmap) tE.get("BK") : bitmap;
            if (this.tJ.size() > i3) {
                cVar = (com.epoint.android.games.mjfgbfree.ui.c) this.tJ.elementAt(i3);
            } else {
                com.epoint.android.games.mjfgbfree.ui.c cVar3 = new com.epoint.android.games.mjfgbfree.ui.c(bitmap2, new Rect());
                this.tJ.addElement(cVar3);
                cVar = cVar3;
            }
            cVar.oa = bitmap2;
            cVar.lw = (String) this.gO.vU[this.gO.aG].fV.elementAt(i3);
            cVar.nZ.left = (MJ16Activity.qZ > MJ16Activity.ra || size < 16) ? (((MJ16Activity.qZ - (this.gO.vU[this.gO.aG].fV.size() * (i - 1))) / 2) + ((i - 1) * i3)) - 5 : ((MJ16Activity.qZ - ((this.gO.vU[this.gO.aG].fV.size() + 1) * (i - 1))) / 2) + ((i - 1) * i3);
            cVar.nZ.top = (MJ16Activity.ra - (((Bitmap) tE.get("bc")).getHeight() * 4)) - (ty == 1 ? dR() : 0);
            if (MJ16Activity.qZ > MJ16Activity.ra) {
                cVar.nZ.top += 8;
            } else if (size < 16) {
                cVar.nZ.top += 8;
            } else {
                cVar.nZ.top += 17;
            }
            cVar.nZ.right = cVar.nZ.left + i;
            cVar.nZ.bottom = cVar.nZ.top + i2;
            i3++;
            cVar2 = cVar;
        }
        if ((uC == null || this.gO.wf.size() > 0) && size % 3 == 2) {
            cVar2.nZ.left += 10;
            cVar2.nZ.right += 10;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: private */
    public synchronized void ef() {
        synchronized (this.gO) {
            int i = (this.gO.aG + 1) % 4;
            com.epoint.android.games.mjfgbfree.d.c G = G(i);
            boolean H = H(i);
            if (this.uk != null) {
                this.uk = null;
            }
            F(i);
            a(this.gO, i, G, H);
            if (!H && G == null && this.gO.vU[i].gb) {
                this.ug.draw(this.un);
            }
            mMatrix.reset();
            mMatrix.postRotate(-90.0f);
            this.uk = Bitmap.createBitmap(this.um, 0, 0, this.um.getWidth(), this.um.getHeight(), mMatrix, true);
            int i2 = (i + 1) % 4;
            com.epoint.android.games.mjfgbfree.d.c G2 = G(i2);
            boolean H2 = H(i2);
            if (this.ul != null) {
                this.ul = null;
            }
            F(i2);
            a(this.gO, i2, G2, H2);
            if (!H2 && G2 == null && this.gO.vU[i2].gb) {
                this.ug.draw(this.un);
            }
            mMatrix.reset();
            mMatrix.postRotate(180.0f);
            this.ul = Bitmap.createBitmap(this.um, 0, 0, this.um.getWidth(), this.um.getHeight(), mMatrix, true);
            int i3 = (i2 + 1) % 4;
            com.epoint.android.games.mjfgbfree.d.c G3 = G(i3);
            boolean H3 = H(i3);
            if (this.uj != null) {
                this.uj = null;
            }
            F(i3);
            a(this.gO, i3, G3, H3);
            if (!H3 && G3 == null && this.gO.vU[i3].gb) {
                this.ug.draw(this.un);
            }
            mMatrix.reset();
            mMatrix.postRotate(90.0f);
            this.uj = Bitmap.createBitmap(this.um, 0, 0, this.um.getWidth(), this.um.getHeight(), mMatrix, true);
        }
    }

    private void eh() {
        Canvas canvas;
        Bitmap bitmap;
        if ((this.ho instanceof k) || (this.ho instanceof q) || (this.ho instanceof com.epoint.android.games.mjfgbfree.e.l)) {
            for (int i = 0; i < tH.length; i++) {
                if (!tE.containsKey(tH[i])) {
                    Bitmap d = com.epoint.android.games.mjfgbfree.a.d.d(this.jq, "images/" + tH[i] + ".png");
                    if (d == null) {
                        d = com.epoint.android.games.mjfgbfree.a.d.d(this.jq, "images/" + tG[i] + ".png");
                    }
                    tE.put(tH[i], d);
                }
            }
            if (tE.get("bkgnd") != null) {
                ((Bitmap) tE.remove("bkgnd")).recycle();
                this.ux = "-";
            }
            if (this.jq.eL().so != null) {
                ax(this.jq.eL().so);
            }
            if (tE.get("bkgnd") == null) {
                ax(MJ16Activity.rg.substring(0, MJ16Activity.rg.length() - 2));
            }
            Bitmap bitmap2 = (Bitmap) tE.get("bkgnd");
            int width = MJ16Activity.db().getWidth() > MJ16Activity.db().getHeight() ? MJ16Activity.db().getWidth() : MJ16Activity.db().getHeight();
            if (bitmap2.getWidth() < width) {
                bitmap = Bitmap.createBitmap(width, width, Bitmap.Config.RGB_565);
                Canvas canvas2 = new Canvas(bitmap);
                Rect rect = new Rect();
                rect.left = 0;
                rect.top = 0;
                rect.right = ((Bitmap) tE.get("bkgnd")).getWidth();
                rect.bottom = ((Bitmap) tE.get("bkgnd")).getHeight();
                Rect rect2 = new Rect();
                rect2.left = 0;
                rect2.top = 0;
                rect2.bottom = width;
                rect2.right = width;
                Paint paint = new Paint();
                paint.setDither(true);
                paint.setFilterBitmap(true);
                canvas2.drawBitmap((Bitmap) tE.get("bkgnd"), rect, rect2, paint);
                canvas = canvas2;
            } else {
                Bitmap copy = ((Bitmap) tE.get("bkgnd")).copy(Bitmap.Config.RGB_565, true);
                canvas = new Canvas(copy);
                bitmap = copy;
            }
            ((Bitmap) tE.get("bkgnd")).recycle();
            tE.put("bkgnd", bitmap);
            if (this.jq.eL().sn != -1) {
                canvas.drawColor(this.jq.eL().sn, PorterDuff.Mode.SCREEN);
            }
        }
    }

    /* access modifiers changed from: private */
    public void ek() {
        if (this.gO.we.size() != 0 || this.gO.wa || !this.gO.vU[this.gO.aG].ga || this.gO.vU[this.gO.aG].fV.size() % 3 != 2) {
            this.uA.Av.clear();
        } else {
            this.uA.Av = a.b.a.b.c(this.gO.vU[this.gO.aG].fV);
        }
        this.uA.Aw.clear();
        this.uA.Ax = null;
    }

    /* access modifiers changed from: private */
    public void el() {
        if (this.gO.vU[this.gO.aG].fV.size() % 3 == 2 && this.gO.wf.size() == 0) {
            ee();
            this.tY = (com.epoint.android.games.mjfgbfree.ui.c) this.tJ.elementAt(this.gO.vU[this.gO.aG].fV.size() - 1);
            return;
        }
        this.tY = null;
    }

    /* access modifiers changed from: private */
    public void em() {
        this.tK.a(false, false);
        this.tL.a(false, false);
        this.tM.a(false, false);
        this.tN.a(false, false);
        this.tO.a(false, false);
        this.tP.a(false, false);
        this.tQ.a(false, false);
        this.tV.a(false, false);
        this.tR.a(false, false);
        this.tS.a(false, false);
        this.tT.a(false, false);
    }

    /* access modifiers changed from: private */
    public void en() {
        this.uU = false;
        if ((!this.gO.vU[this.gO.aG].ga || this.gO.li != this.gO.aG) && this.gO.we.size() > 0 && (this.uB instanceof com.epoint.android.games.mjfgbfree.c.f)) {
            this.tV.a(true, false);
            this.tM.a(true, false);
            er();
            if (uC != null) {
                while (this.uu) {
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                    }
                }
            }
            uC = null;
        } else if (this.gO.vU[this.gO.aG].ga || this.gO.le) {
            this.jq.getRequestedOrientation();
            boolean z = ai.ng && this.gO.vU[this.gO.aG].ga && this.gO.vW && !this.gO.vX && this.gO.wb == 0 && this.gO.vV.size() == 0 && !this.gO.wa && !this.gO.vY;
            if (!this.gO.vW || !(this.gO.lh == 4 || this.gO.lh == 3 || this.gO.lh == 2)) {
                this.tK.v(af.aa("摸牌"));
            } else {
                this.tK.v(af.aa("補牌"));
                z = false;
            }
            this.tK.a(this.gO.vW && !z, false);
            this.tL.a(this.gO.vU[this.gO.aG].fV.size() % 3 == 2, false);
            this.tM.a(this.gO.vZ, false);
            this.tN.a(this.gO.wb != 0, false);
            this.tO.a(this.gO.vV.size() != 0, false);
            this.tP.a(this.gO.vX, false);
            this.tQ.a(this.gO.wa && this.gO.li == this.gO.aG, false);
            this.tV.a(this.gO.we.size() > 0, false);
            this.tR.a(this.gO.wa && !this.tQ.isVisible(), false);
            this.tS.a(this.gO.vY, false);
            if (this.gO.le) {
                ep();
                if (uC != null) {
                    while (this.uu) {
                        try {
                            Thread.sleep(20);
                        } catch (InterruptedException e2) {
                        }
                    }
                }
                this.gO.wf.size();
                o oVar = new o(this);
                uC = oVar;
                oVar.start();
            } else if (z) {
                new p(this).start();
            } else {
                if (!(this.uB instanceof com.epoint.android.games.mjfgbfree.c.f)) {
                    ep();
                } else {
                    er();
                }
                if (uC != null) {
                    while (this.uu) {
                        try {
                            Thread.sleep(20);
                        } catch (InterruptedException e3) {
                        }
                    }
                }
            }
            if (!z) {
                uC = null;
            }
        } else {
            Thread.currentThread();
            em();
            er();
            if (uC != null) {
                while (this.uu) {
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e4) {
                    }
                }
            }
            if (this.qC != null) {
                Thread thread = new Thread(this);
                uC = thread;
                thread.start();
                return;
            }
            uC = null;
        }
    }

    /* access modifiers changed from: private */
    public void eo() {
        Message message = new Message();
        message.what = 1;
        this.mHandler.sendMessage(message);
    }

    public static void ez() {
        uC = null;
    }

    public static boolean f(Thread thread) {
        if (uC != null) {
            return false;
        }
        uC = thread;
        if (!thread.isAlive()) {
            uC.start();
        }
        return true;
    }

    public final d a(String str, o oVar) {
        m mVar = new m(this, oVar, str);
        if (this.ue != null) {
            e(mVar);
        } else {
            mVar.run();
        }
        while (true) {
            if (!mVar.isAlive() && !this.uu) {
                break;
            }
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
            }
        }
        return this.fs;
    }

    public final f a(o oVar, com.epoint.android.games.mjfgbfree.d.b bVar) {
        while (true) {
            if (!com.epoint.android.games.mjfgbfree.ui.a.e.jz && !this.uu) {
                break;
            }
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
            }
        }
        this.ue = this.uc;
        com.epoint.android.games.mjfgbfree.ui.a.c cVar = this.uc;
        cVar.a(this.jq, this.tZ);
        StringBuilder sb = new StringBuilder();
        sb.append("<font color='#404040' style='font-size:" + (MJ16Activity.rb + 2) + "px'><table width='100%'>" + "<tr><td colspan='3' align='center'>" + af.aa("績分榜"));
        int cI = cI();
        sb.append(" (" + cI + " " + af.aa("盤") + ")");
        sb.append("</td></tr>");
        if ("".equals(this.gO.vU[com.epoint.android.games.mjfgbfree.c.d.qB].xx)) {
            af.aa("你");
        }
        int i = this.gO.vU[com.epoint.android.games.mjfgbfree.c.d.qB].gd;
        Calendar instance = Calendar.getInstance();
        String str = String.valueOf(instance.get(1)) + "-" + (instance.get(2) + 1) + "-" + instance.get(5);
        Vector c = com.epoint.android.games.mjfgbfree.f.a.c(this.jq, cI);
        int i2 = i;
        for (int i3 = 0; i3 < c.size(); i3++) {
            com.epoint.android.games.mjfgbfree.d.b bVar2 = (com.epoint.android.games.mjfgbfree.d.b) c.elementAt(i3);
            instance.setTime(bVar2.ip);
            String str2 = String.valueOf(instance.get(1)) + "-" + (instance.get(2) + 1) + "-" + instance.get(5);
            if (i2 == bVar2.gH && bVar.io == bVar2.io && str2.equals(str)) {
                i2 = -1;
                sb.append("<tr><td><font color='#800000'>" + (i3 + 1) + ".</font></td><td><font color='#800000'>" + str2 + ": #" + bVar2.io + "</font></td>");
                sb.append("<td align='right'><font color='#800000'>$" + bVar2.gH + "</font></td></tr>");
            } else {
                sb.append("<tr><td>" + (i3 + 1) + ".</td><td>" + str2 + ": #" + bVar2.io + "</td>");
                sb.append("<td align='right'>$" + bVar2.gH + "</td></tr>");
            }
        }
        sb.append("</table></font>");
        cVar.a(sb.toString(), oVar, false);
        cVar.h(false);
        return cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.epoint.android.games.mjfgbfree.av.a(int, boolean, com.epoint.android.games.mjfgbfree.d.g, java.lang.String[]):void
     arg types: [int, int, com.epoint.android.games.mjfgbfree.d.g, java.lang.String[]]
     candidates:
      com.epoint.android.games.mjfgbfree.av.a(int, android.graphics.Canvas, com.epoint.android.games.mjfgbfree.d.g, java.lang.String[]):void
      com.epoint.android.games.mjfgbfree.av.a(int, com.epoint.android.games.mjfgbfree.d.g, java.lang.String[], com.epoint.android.games.mjfgbfree.ui.a):void
      com.epoint.android.games.mjfgbfree.av.a(int, boolean, com.epoint.android.games.mjfgbfree.d.g, java.lang.String[]):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.epoint.android.games.mjfgbfree.bb.b(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.epoint.android.games.mjfgbfree.bb.b(android.graphics.Bitmap, int):android.graphics.Bitmap
      com.epoint.android.games.mjfgbfree.bb.b(int, int):void
      a.a.b.b(int, int):void
      com.epoint.android.games.mjfgbfree.bb.b(boolean, boolean):void */
    public final void a(int i, int i2) {
        if (!this.uv.oR && i2 != 0 && i2 != 7 && i2 != 4 && i2 != 3) {
            this.uv.a(i, true, this.gO, tH);
            if (i2 == 1) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
                this.uI.a(this.uI.P);
                this.uv.a(i, this.gO, tH);
                if ((this.uB instanceof com.epoint.android.games.mjfgbfree.c.f) && this.gO.le) {
                    com.epoint.android.games.mjfgbfree.f.a.c(this.jq, this.gO);
                    this.uK = com.epoint.android.games.mjfgbfree.f.a.b(this.jq, this.gO);
                    this.uL = com.epoint.android.games.mjfgbfree.f.a.a(this.jq, this.gO);
                    b(false, true);
                }
            } else {
                switch (i2) {
                    case 2:
                        this.ui[i].v(af.aa("槓"));
                        this.uI.a(this.uI.O);
                        break;
                    case 5:
                        this.ui[i].v(af.aa("碰"));
                        this.uI.a(this.uI.M);
                        break;
                    case 6:
                        this.ui[i].v(af.aa("上"));
                        this.uI.a(this.uI.N);
                        break;
                }
                this.uv.a(i, this.gO, tH, this.ui[i]);
            }
            while (this.uv.oR) {
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e2) {
                }
            }
            uD = i;
        }
    }

    public final synchronized void a(int i, boolean z) {
        Message message = new Message();
        message.what = 7;
        Bundle bundle = new Bundle();
        bundle.putBoolean("is_share", z);
        bundle.putInt("btn_height", i);
        message.obj = bundle;
        this.mHandler.sendMessage(message);
    }

    public final void a(o oVar) {
        this.ho = oVar;
    }

    public final void a(com.epoint.android.games.mjfgbfree.tournament.c cVar) {
        String str = "tour_bg1_s";
        int rgb = Color.rgb(160, 90, 10);
        switch (cVar.oo) {
            case 1:
                str = "tour_bg2_s";
                break;
            case 2:
                str = "tour_bg3_s";
                break;
            case 3:
                str = "tour_bg4_s";
                break;
            case 4:
                str = "tour_bg5_s";
                break;
            case 5:
                str = "tour_back_s";
                break;
        }
        if (!MJ16Activity.rg.equals(str)) {
            MJ16Activity.rg = str;
            this.jq.eL().sn = rgb;
            this.jq.eL().sm = -7129553;
            eh();
            if (this.uN == ai.nj) {
                d(this.jq, this.jq.eL().sm);
            } else {
                E(ai.nj);
            }
        }
    }

    public final void a(i iVar, int i, String str) {
        k kVar = new k(this, iVar, i, str);
        uC = kVar;
        kVar.start();
    }

    public final void a(ce ceVar) {
        if (!this.uH && (this.uO == null || !this.uO.isAlive())) {
            this.uT = new Date();
            this.uO = new bk(this, 63000, 11);
            this.uO.start();
        }
        ceVar.fM();
        ceVar.b(this);
        uF.removeAllViews();
        uF.addView(ceVar);
    }

    public final void a(ce ceVar, int i) {
        if (!this.uH && (this.uO == null || !this.uO.isAlive())) {
            this.uO = new bk(this, 35000, 11);
            this.uO.start();
        }
        ceVar.fM();
        ceVar.b(this);
        Log.i("MOBCLIX", "onFailedLoad(): " + i);
    }

    public final void a(String str, o oVar, boolean z, boolean z2) {
        a(str, this.ud, z, oVar, z2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.epoint.android.games.mjfgbfree.bb.a(java.lang.String, com.epoint.android.games.mjfgbfree.ui.a.f, boolean, com.epoint.android.games.mjfgbfree.e.o, boolean):void
     arg types: [java.lang.String, com.epoint.android.games.mjfgbfree.ui.a.c, int, ?[OBJECT, ARRAY], int]
     candidates:
      com.epoint.android.games.mjfgbfree.bb.a(android.graphics.Canvas, java.lang.String, int, int, int):void
      com.epoint.android.games.mjfgbfree.bb.a(boolean, java.lang.String, android.graphics.Canvas, android.graphics.Rect, android.graphics.Rect):void
      com.epoint.android.games.mjfgbfree.bb.a(java.lang.String, com.epoint.android.games.mjfgbfree.ui.a.f, boolean, com.epoint.android.games.mjfgbfree.e.o, boolean):void */
    public final void a(Vector vector, com.epoint.android.games.mjfgbfree.d.d dVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("<font color='#404040' style='font-size:" + (MJ16Activity.rb + 1) + "px'><table width='100%'>" + "<tr><td align='center'>" + af.aa("升級了") + "!" + "</td></tr></table>" + "<div style='padding:3px; border-style: dotted;border-width: 3px; border-color: navy;'>" + "<table width='100%'>");
        if (dVar != null) {
            int f = com.epoint.android.games.mjfgbfree.g.a.f(dVar.mp, dVar.mo);
            sb.append("<tr><td>" + ("<img src=\"file:///android_asset/assets/images/score.png\" style='width:" + ((int) (MJ16Activity.rc * 24.0f)) + "px; height:" + ((int) (MJ16Activity.rc * 24.0f)) + "px;' />") + dVar.mm + (dVar.mn == 9999 ? "+" : "-" + dVar.mn) + af.aa("番") + "</td><td align='right' nowrap>");
            for (int i = 0; i < f; i++) {
                sb.append("<img src=\"file:///android_asset/assets/images/star_on.png\" style='width:" + ((int) (MJ16Activity.rc * 16.0f)) + "px; height:" + ((int) (MJ16Activity.rc * 16.0f)) + "px;'>");
            }
            sb.append("</td></tr>");
        }
        for (int i2 = 0; i2 < vector.size(); i2++) {
            com.epoint.android.games.mjfgbfree.d.i iVar = (com.epoint.android.games.mjfgbfree.d.i) vector.elementAt(i2);
            int e = com.epoint.android.games.mjfgbfree.g.a.e(iVar.mp, iVar.mo);
            String d = af.d(iVar.nY, af.A(iVar.AU));
            if (iVar.AV == 998 || iVar.AV == 999) {
                d = "exp" + this.gO.wf.size();
            }
            sb.append("<tr><td>" + (d != null ? "<img src=\"file:///android_asset/assets/images/" + d + ".png\" style='width:" + ((int) (MJ16Activity.rc * 24.0f)) + "px; height:" + ((int) (MJ16Activity.rc * 24.0f)) + "px;' />" : "") + af.ab(iVar.nY) + "</td><td align='right' nowrap>");
            for (int i3 = 0; i3 < e; i3++) {
                sb.append("<img src=\"file:///android_asset/assets/images/star_on.png\" style='width:" + ((int) (MJ16Activity.rc * 16.0f)) + "px; height:" + ((int) (MJ16Activity.rc * 16.0f)) + "px;'>");
            }
            sb.append("</td></tr>");
        }
        sb.append("</table></div></font>");
        a(sb.toString(), (f) this.uc, true, (o) null, false);
    }

    public final boolean a(MotionEvent motionEvent) {
        return false;
    }

    public final void ax(String str) {
        if (!this.ux.equals(str)) {
            if (str.equals("")) {
                Bitmap bitmap = (Bitmap) tE.get("bkgnd");
                if (bitmap != null) {
                    bitmap.recycle();
                }
                tE.remove("bkgnd");
            } else {
                Bitmap c = com.epoint.android.games.mjfgbfree.a.d.c(this.jq, "images/" + str + ".jpg");
                if (c != null) {
                    tE.put("bkgnd", c);
                }
            }
            this.ux = str;
        }
    }

    public final com.epoint.android.games.mjfgbfree.d.b b(o oVar) {
        while (true) {
            if (!com.epoint.android.games.mjfgbfree.ui.a.e.jz && !this.uu) {
                break;
            }
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
            }
        }
        this.ue = this.uc;
        com.epoint.android.games.mjfgbfree.ui.a.c cVar = this.uc;
        cVar.a(this.jq, this.tZ);
        StringBuilder sb = new StringBuilder();
        sb.append("<font color='#404040' style='font-size:" + (MJ16Activity.rb + 1) + "px'><table width='100%'>" + "<tr><td align='center'>" + (oVar instanceof com.epoint.android.games.mjfgbfree.e.l ? af.Z("本階段成積") : af.aa("本局結束")) + "</td></tr></table>" + "<div style='padding:3px; border-style: dotted;border-width: 3px; border-color: navy;'>" + "<table width='100%'>");
        int[] iArr = new int[4];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        String[] strArr = new String[4];
        strArr[com.epoint.android.games.mjfgbfree.c.d.qB] = af.aa("你");
        strArr[(com.epoint.android.games.mjfgbfree.c.d.qB + 1) % 4] = af.aa("下家");
        strArr[(com.epoint.android.games.mjfgbfree.c.d.qB + 2) % 4] = af.aa("對家");
        strArr[(com.epoint.android.games.mjfgbfree.c.d.qB + 3) % 4] = af.aa("上家");
        double[] dArr = {(double) this.gO.vU[0].gd, (double) this.gO.vU[1].gd, (double) this.gO.vU[2].gd, (double) this.gO.vU[3].gd};
        com.epoint.android.games.mjfgbfree.d.b bVar = new com.epoint.android.games.mjfgbfree.d.b();
        bVar.im = "".equals(this.gO.vU[com.epoint.android.games.mjfgbfree.c.d.qB].xx) ? strArr[com.epoint.android.games.mjfgbfree.c.d.qB] : this.gO.vU[com.epoint.android.games.mjfgbfree.c.d.qB].xx;
        bVar.gH = (int) dArr[com.epoint.android.games.mjfgbfree.c.d.qB];
        bVar.in = -1;
        if ((oVar instanceof k) || (oVar instanceof com.epoint.android.games.mjfgbfree.e.l)) {
            int i = com.epoint.android.games.mjfgbfree.c.d.qB;
            dArr[i] = dArr[i] + 0.5d;
            bVar.in = cI();
            if (oVar instanceof k) {
                com.epoint.android.games.mjfgbfree.f.a.a(this.jq, oVar instanceof k ? 0 : oVar instanceof com.epoint.android.games.mjfgbfree.e.l ? 4 : 1, (Object) null);
                this.uR = false;
            }
        }
        for (int i2 = 0; i2 < iArr.length - 1; i2++) {
            for (int i3 = i2 + 1; i3 < iArr.length; i3++) {
                if (dArr[i2] < dArr[i3]) {
                    double d = dArr[i3];
                    dArr[i3] = dArr[i2];
                    dArr[i2] = d;
                    int i4 = iArr[i3];
                    iArr[i3] = iArr[i2];
                    iArr[i2] = i4;
                    String str = strArr[i3];
                    strArr[i3] = strArr[i2];
                    strArr[i2] = str;
                }
            }
        }
        for (int i5 = 0; i5 < this.gO.vU.length; i5++) {
            if (iArr[i5] == com.epoint.android.games.mjfgbfree.c.d.qB) {
                bVar.io = i5 + 1;
                sb.append("<tr><td><font color='#800000'>" + strArr[i5] + "</font></td>");
                sb.append("<td align='right'><font color='#800000'>$" + this.gO.vU[iArr[i5]].gd + "</font></td></tr>");
            } else {
                sb.append("<tr><td>" + ("".equals(this.gO.vU[iArr[i5]].xx) ? strArr[i5] : this.gO.vU[iArr[i5]].xx) + "</td>");
                sb.append("<td align='right'>$" + this.gO.vU[iArr[i5]].gd + "</td></tr>");
            }
        }
        sb.append("</table></div></font>");
        if (bVar.in > 0 && (oVar instanceof k)) {
            com.epoint.android.games.mjfgbfree.f.a.a(this.jq, bVar);
        }
        cVar.a(sb.toString(), oVar, true);
        cVar.h(false);
        return bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.epoint.android.games.mjfgbfree.av.a(int, boolean, com.epoint.android.games.mjfgbfree.d.g, java.lang.String[]):void
     arg types: [int, int, com.epoint.android.games.mjfgbfree.d.g, java.lang.String[]]
     candidates:
      com.epoint.android.games.mjfgbfree.av.a(int, android.graphics.Canvas, com.epoint.android.games.mjfgbfree.d.g, java.lang.String[]):void
      com.epoint.android.games.mjfgbfree.av.a(int, com.epoint.android.games.mjfgbfree.d.g, java.lang.String[], com.epoint.android.games.mjfgbfree.ui.a):void
      com.epoint.android.games.mjfgbfree.av.a(int, boolean, com.epoint.android.games.mjfgbfree.d.g, java.lang.String[]):void */
    public final void b(int i, int i2) {
        if (!this.uv.oR) {
            if (this.gO.vK) {
                this.uI.b(this.gO.V);
            } else if (!this.gO.vK) {
                if (i2 == 0 && uD != -1) {
                    this.uv.a(uD, false, this.gO, tH);
                    while (this.uv.oR) {
                        try {
                            Thread.sleep(20);
                        } catch (InterruptedException e) {
                        }
                    }
                    uD = -1;
                }
                ep();
                if (!this.gO.vU[com.epoint.android.games.mjfgbfree.c.d.qB].ga) {
                    while (this.uu) {
                        try {
                            Thread.sleep(20);
                        } catch (InterruptedException e2) {
                        }
                    }
                }
                if (i2 == 0 && !this.gO.vU[i].fZ.contains(this.gO.vO) && this.gO.vU[i].fV.size() % 3 == 2) {
                    this.uI.a(this.uI.U);
                }
            }
            en();
        }
    }

    public final void b(boolean z, boolean z2) {
        if (!this.uJ && !uE) {
            if (((this.ho instanceof k) || (this.ho instanceof com.epoint.android.games.mjfgbfree.e.l)) && this.uB.cU() != null && this.uR) {
                this.uJ = true;
                if (z2 && this.tT != null) {
                    this.tT.d(false);
                }
                new n(this, z2, z).start();
            }
        }
    }

    public final void bK() {
        if (this.gO != null) {
            this.uv.cG();
            eh();
            this.uB.cW();
        }
    }

    public final int bR() {
        return this.gO.la;
    }

    public final boolean bm() {
        return false;
    }

    public final boolean bn() {
        if (!ai.nq || this.ue != null || this.gO == null || this.gO.le) {
            return false;
        }
        g(this.tL);
        return false;
    }

    public final void c(Thread thread) {
        if (this.ue != null) {
            if (this.ue.by()) {
                this.ue.a(thread);
                this.ue = null;
                return;
            }
            this.ue = null;
        }
        if (thread != null) {
            thread.start();
        }
    }

    public final int cI() {
        if (this.ho instanceof k) {
            return ((k) this.ho).cI();
        }
        if (this.ho instanceof q) {
            return ((q) this.ho).cI();
        }
        if (this.ho instanceof com.epoint.android.games.mjfgbfree.e.l) {
            return ((com.epoint.android.games.mjfgbfree.e.l) this.ho).cI();
        }
        return -1;
    }

    public final void d(Thread thread) {
        this.ue = null;
        if (thread != null) {
            thread.start();
        }
    }

    public final void dL() {
        if (this.uH) {
            return;
        }
        if (this.uO == null || !this.uO.isAlive()) {
            this.uO = new bk(this, 35000, 11);
            this.uO.start();
        }
    }

    public final void dM() {
        if (this.uH) {
            return;
        }
        if (this.uO == null || !this.uO.isAlive()) {
            this.uT = new Date();
            this.uO = new bk(this, 63000, 11);
            this.uO.start();
        }
    }

    public final void dN() {
        if (this.uH) {
            return;
        }
        if (this.uO == null || !this.uO.isAlive()) {
            if (this.uQ == null || !"cn".equals(this.uQ.getNetworkCountryIso())) {
                this.uO = new bk(this, 35000, 11);
            } else {
                this.uO = new bk(this, 35000, 13);
            }
            this.uO.start();
        }
    }

    public final void dO() {
        if (!this.uH && (this.uO == null || !this.uO.isAlive())) {
            this.uT = new Date();
            if (this.uQ == null || !"-cn".equals(this.uQ.getNetworkCountryIso())) {
                this.uO = new bk(this, 63000, 11);
            } else {
                this.uO = new bk(this, 63000, 13);
            }
            this.uO.start();
        }
        uF.removeAllViews();
        new l(this).start();
    }

    public final av dP() {
        return this.uv;
    }

    public final void dS() {
        if (uE) {
            this.uV = true;
        }
        this.uH = true;
        if (this.uO != null && this.uO.isAlive()) {
            this.uO.interrupt();
        }
    }

    public final void dT() {
        if (this.uH) {
            this.uH = false;
            int i = 10000;
            if (this.uT != null) {
                i = (int) (new Date().getTime() - this.uT.getTime());
                if (i < 63000) {
                    i = 63000 - i;
                }
                if (i < 3000 || i >= 63000) {
                    i = 3000;
                }
            }
            if (this.uQ == null || !"cn-".equals(this.uQ.getNetworkCountryIso())) {
                this.uO = new bk(this, i, 11);
            } else {
                this.uO = new bk(this, i, 13);
            }
            this.uO.start();
        }
        dW();
    }

    public final void dX() {
        if (this.ho != null) {
            this.ho.a(8, null);
        }
        dW();
    }

    public final void dY() {
        this.uI.a(ai.nl);
        E(ai.nj);
        if (this.jB != ai.nk) {
            this.jB = ai.nk;
            this.tK.v(af.aa("摸牌"));
            this.tL.v(af.aa("打牌"));
            this.tM.v(af.aa("放棄"));
            this.tN.v(af.aa("上"));
            this.tO.v(af.aa("槓"));
            this.tP.v(af.aa("碰"));
            this.tQ.v(af.aa("自摸"));
            this.tV.v("!");
            this.tR.v(af.aa("食糊"));
            this.tS.v(af.aa("聽牌"));
            this.tT.v(af.aa("繼續"));
            this.tU.v(af.aa("詳情"));
            this.uf.j((ai.nk == 1 || ai.nk == 5) ? 140 : 120);
            for (int i = 0; i < 4; i++) {
                this.ui[i].j((ai.nk == 1 || ai.nk == 5) ? 140 : 120);
            }
            this.ug.v(af.aa("聽牌"));
            this.ug.j((ai.nk == 1 || ai.nk == 5) ? 140 : 120);
        }
        er();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public final void dZ() {
        if (this.tD == 1) {
            ed();
        }
        if (this.tD == 0) {
            RelativeLayout relativeLayout = (RelativeLayout) getParent();
            if (relativeLayout.findViewWithTag(this.uv) == null) {
                relativeLayout.addView(this.uv);
            }
            if (this.gO != null) {
                bK();
            } else if (this.gO == null) {
                MJ16Activity.dd();
                for (int i = 0; i < this.us.length; i++) {
                    this.us[i] = i;
                }
                tE.put("frame", com.epoint.android.games.mjfgbfree.a.d.c(this.jq, "images/frame.gif"));
                tE.put("frame_none", com.epoint.android.games.mjfgbfree.a.d.c(this.jq, "images/frame_none.gif"));
                tE.put("arrow", com.epoint.android.games.mjfgbfree.a.d.c(this.jq, "images/arrow.png"));
                this.gN = new Paint();
                this.gN.setAntiAlias(true);
                this.gN.setStyle(Paint.Style.FILL);
                this.gN.setColor(-1);
                this.gN.setTextSize(22.0f);
                if (!(this.ho instanceof com.epoint.android.games.mjfgbfree.e.l) || ((com.epoint.android.games.mjfgbfree.e.l) this.ho).cM().cz().gF != -1) {
                    eh();
                    E(ai.nj);
                } else {
                    a(((com.epoint.android.games.mjfgbfree.e.l) this.ho).cM());
                }
                this.tK = new com.epoint.android.games.mjfgbfree.ui.a(75, 62, -65536, -16777216, 160, af.aa("摸牌"));
                this.tL = new com.epoint.android.games.mjfgbfree.ui.a(90, 80, -65536, -16777216, 160, af.aa("打牌"));
                this.tM = new com.epoint.android.games.mjfgbfree.ui.a(75, 62, -65536, -16777216, 160, af.aa("放棄"));
                this.tN = new com.epoint.android.games.mjfgbfree.ui.a(75, 62, -65536, -16777216, 160, af.aa("上"));
                this.tO = new com.epoint.android.games.mjfgbfree.ui.a(75, 62, -65536, -16777216, 160, af.aa("槓"));
                this.tP = new com.epoint.android.games.mjfgbfree.ui.a(75, 62, -65536, -16777216, 160, af.aa("碰"));
                this.tQ = new com.epoint.android.games.mjfgbfree.ui.a(75, 62, -65536, -16777216, 160, af.aa("自摸"));
                this.tV = new com.epoint.android.games.mjfgbfree.ui.a(75, 62, -65536, -16777216, 160, "!");
                this.tR = new com.epoint.android.games.mjfgbfree.ui.a(75, 62, -65536, -16777216, 160, af.aa("食糊"));
                this.tS = new com.epoint.android.games.mjfgbfree.ui.a(75, 62, -65536, -16777216, 160, af.aa("聽牌"));
                this.tT = new com.epoint.android.games.mjfgbfree.ui.a(100, 62, -65536, -16777216, 160, af.aa("繼續"));
                this.tU = new com.epoint.android.games.mjfgbfree.ui.a(100, 62, -65536, -16777216, 160, af.aa("詳情"));
                this.tW = new com.epoint.android.games.mjfgbfree.ui.a(70, 70, -65536, -16777216, 160, "");
                this.tW.bg();
                this.uf = new com.epoint.android.games.mjfgbfree.ui.a((ai.nk == 1 || ai.nk == 5) ? 140 : 120, 160, null, Color.rgb(128, 0, 0), this.uw);
                for (int i2 = 0; i2 < 4; i2++) {
                    this.ui[i2] = new com.epoint.android.games.mjfgbfree.ui.a((ai.nk == 1 || ai.nk == 5) ? 140 : 120, 192, null, Color.rgb(128, 0, 0), this.uw);
                }
                this.ug = new com.epoint.android.games.mjfgbfree.ui.a((ai.nk == 1 || ai.nk == 5) ? 140 : 120, 160, af.aa("聽牌"), Color.rgb(128, 0, 0), this.uw);
                this.uh = new com.epoint.android.games.mjfgbfree.ui.a(200, 150, -1, -12303292, 216, null);
                this.gM.add(this.tW);
                this.gM.add(this.tK);
                this.gM.add(this.tL);
                this.gM.add(this.tM);
                this.gM.add(this.tQ);
                this.gM.add(this.tV);
                this.gM.add(this.tR);
                this.gM.add(this.tS);
                this.gM.add(this.tN);
                this.gM.add(this.tO);
                this.gM.add(this.tP);
                this.gM.add(this.tT);
                this.gM.add(this.tU);
                this.um = Bitmap.createBitmap(335, (((Bitmap) tE.get("bc")).getHeight() * 3) - 10, Bitmap.Config.ARGB_8888);
                this.un = new Canvas(this.um);
                this.tI = new TextPaint(this.gN);
                this.tI.setColor(-1);
                this.tI.setShadowLayer(3.0f, 1.0f, 1.0f, -16777216);
                this.un.setDrawFilter(MJ16Activity.rk);
                this.ug.c((this.un.getWidth() - this.ug.bb()) / 2, (this.un.getHeight() - this.ug.ba()) / 2);
                this.tX = (Vibrator) this.jq.getSystemService("vibrator");
            }
            this.tY = null;
            for (int i3 = 0; i3 < this.gM.size(); i3++) {
                ((com.epoint.android.games.mjfgbfree.ui.a) this.gM.elementAt(i3)).b(false);
                ((com.epoint.android.games.mjfgbfree.ui.a) this.gM.elementAt(i3)).a(false, false);
            }
            this.gO = this.uB.P();
            if (!this.uB.cX()) {
                ep();
                ek();
                el();
            }
            if (this.uB.cT()) {
                this.tD = 1;
                ep();
                if (this.gO.vU[com.epoint.android.games.mjfgbfree.c.d.qB].ga) {
                    ek();
                    el();
                }
                en();
                return;
            }
            uC = new ah(this);
            this.tD = 1;
            uC.start();
        }
    }

    public final void ea() {
        n(false);
    }

    public final boolean eb() {
        int cI = cI();
        if (this.gO == null || cI == -1) {
            return false;
        }
        return cI / 4 == this.gO.kZ && (this.gO.lb + 1) % 4 == this.gO.lc;
    }

    public final void ec() {
        this.tD = 0;
    }

    public final void ed() {
        if (this.tD == 1) {
            this.ut = null;
            RelativeLayout relativeLayout = (RelativeLayout) getParent();
            if (relativeLayout.findViewWithTag(this.uv) != null) {
                this.uv.cH();
                relativeLayout.removeView(this.uv);
            }
            this.uu = true;
            if (this.ue != null) {
                this.ue.bi();
                this.ue = null;
            }
            this.uB.ce();
            MJ16Activity.rj.drawColor(-16777216);
            invalidate();
            this.tD = 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:141:0x0714  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x0763  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x07bb  */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x0893  */
    /* JADX WARNING: Removed duplicated region for block: B:215:0x0a09  */
    /* JADX WARNING: Removed duplicated region for block: B:220:0x0a25  */
    /* JADX WARNING: Removed duplicated region for block: B:265:0x0ca8  */
    /* JADX WARNING: Removed duplicated region for block: B:268:0x0cb0  */
    /* JADX WARNING: Removed duplicated region for block: B:274:0x0cc9  */
    /* JADX WARNING: Removed duplicated region for block: B:283:0x0d00  */
    /* JADX WARNING: Removed duplicated region for block: B:289:0x0d63  */
    /* JADX WARNING: Removed duplicated region for block: B:295:0x0d93  */
    /* JADX WARNING: Removed duplicated region for block: B:304:0x0db8  */
    /* JADX WARNING: Removed duplicated region for block: B:325:0x0e1d  */
    /* JADX WARNING: Removed duplicated region for block: B:337:0x0e6f  */
    /* JADX WARNING: Removed duplicated region for block: B:340:0x0ebb  */
    /* JADX WARNING: Removed duplicated region for block: B:467:0x159f  */
    /* JADX WARNING: Removed duplicated region for block: B:476:0x15cc  */
    /* JADX WARNING: Removed duplicated region for block: B:477:0x15d0  */
    /* JADX WARNING: Removed duplicated region for block: B:478:0x15d3  */
    /* JADX WARNING: Removed duplicated region for block: B:482:0x1691  */
    /* JADX WARNING: Removed duplicated region for block: B:486:0x16ac  */
    /* JADX WARNING: Removed duplicated region for block: B:516:0x1829  */
    /* JADX WARNING: Removed duplicated region for block: B:517:0x182c  */
    /* JADX WARNING: Removed duplicated region for block: B:525:0x1852  */
    /* JADX WARNING: Removed duplicated region for block: B:526:0x1857  */
    /* JADX WARNING: Removed duplicated region for block: B:536:0x188c  */
    /* JADX WARNING: Removed duplicated region for block: B:557:0x192c  */
    /* JADX WARNING: Removed duplicated region for block: B:620:0x1b18  */
    /* JADX WARNING: Removed duplicated region for block: B:625:0x1b36  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void eg() {
        /*
            r29 = this;
            monitor-enter(r29)
            android.graphics.Rect r13 = new android.graphics.Rect     // Catch:{ all -> 0x0090 }
            r13.<init>()     // Catch:{ all -> 0x0090 }
            android.graphics.Rect r6 = new android.graphics.Rect     // Catch:{ all -> 0x0090 }
            r6.<init>()     // Catch:{ all -> 0x0090 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0090 }
            java.lang.String r7 = "bkgnd"
            java.lang.Object r5 = r5.get(r7)     // Catch:{ all -> 0x0090 }
            if (r5 == 0) goto L_0x0088
            r5 = 0
            r6.left = r5     // Catch:{ all -> 0x0090 }
            r6.top = r5     // Catch:{ all -> 0x0090 }
            int r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra     // Catch:{ all -> 0x0090 }
            r6.bottom = r5     // Catch:{ all -> 0x0090 }
            int r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0090 }
            r6.right = r5     // Catch:{ all -> 0x0090 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0090 }
            java.lang.String r7 = "bkgnd"
            java.lang.Object r5 = r5.get(r7)     // Catch:{ all -> 0x0090 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0090 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0090 }
            int r7 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra     // Catch:{ all -> 0x0090 }
            int r5 = r5 - r7
            int r5 = r5 / 2
            r13.top = r5     // Catch:{ all -> 0x0090 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0090 }
            java.lang.String r7 = "bkgnd"
            java.lang.Object r5 = r5.get(r7)     // Catch:{ all -> 0x0090 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0090 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0090 }
            int r7 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0090 }
            int r5 = r5 - r7
            int r5 = r5 / 2
            r13.left = r5     // Catch:{ all -> 0x0090 }
            int r5 = r13.top     // Catch:{ all -> 0x0090 }
            int r7 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra     // Catch:{ all -> 0x0090 }
            int r5 = r5 + r7
            r13.bottom = r5     // Catch:{ all -> 0x0090 }
            int r5 = r13.left     // Catch:{ all -> 0x0090 }
            int r7 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0090 }
            int r5 = r5 + r7
            r13.right = r5     // Catch:{ all -> 0x0090 }
            android.graphics.Canvas r7 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0090 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0090 }
            java.lang.String r8 = "bkgnd"
            java.lang.Object r5 = r5.get(r8)     // Catch:{ all -> 0x0090 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0090 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0090 }
            r8 = r0
            r7.drawBitmap(r5, r13, r6, r8)     // Catch:{ all -> 0x0090 }
        L_0x006e:
            r0 = r29
            int r0 = r0.tD     // Catch:{ all -> 0x0090 }
            r5 = r0
            if (r5 != 0) goto L_0x0093
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a.i r0 = r0.ue     // Catch:{ all -> 0x0090 }
            r5 = r0
            if (r5 == 0) goto L_0x0086
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a.i r0 = r0.ue     // Catch:{ all -> 0x0090 }
            r5 = r0
            android.graphics.Canvas r6 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0090 }
            r5.a(r6)     // Catch:{ all -> 0x0090 }
        L_0x0086:
            monitor-exit(r29)
            return
        L_0x0088:
            android.graphics.Canvas r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0090 }
            r7 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r5.drawColor(r7)     // Catch:{ all -> 0x0090 }
            goto L_0x006e
        L_0x0090:
            r5 = move-exception
            monitor-exit(r29)
            throw r5
        L_0x0093:
            r0 = r29
            android.graphics.Bitmap r0 = r0.uj     // Catch:{ all -> 0x0090 }
            r5 = r0
            if (r5 == 0) goto L_0x00a8
            r0 = r29
            android.graphics.Bitmap r0 = r0.uk     // Catch:{ all -> 0x0090 }
            r5 = r0
            if (r5 == 0) goto L_0x00a8
            r0 = r29
            android.graphics.Bitmap r0 = r0.ul     // Catch:{ all -> 0x0090 }
            r5 = r0
            if (r5 != 0) goto L_0x00ab
        L_0x00a8:
            r29.ef()     // Catch:{ all -> 0x0090 }
        L_0x00ab:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0090 }
            r14 = r0
            monitor-enter(r14)     // Catch:{ all -> 0x0090 }
            int r5 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x0ec8 }
            if (r5 != 0) goto L_0x018c
            int r5 = dR()     // Catch:{ all -> 0x0ec8 }
        L_0x00b9:
            int r7 = r5 + 135
            r0 = r29
            android.graphics.Bitmap r0 = r0.uj     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            if (r5 == 0) goto L_0x00f7
            android.graphics.Bitmap r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.uj     // Catch:{ all -> 0x0ec8 }
            r8 = r0
            int r8 = r8.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 - r8
            int r5 = r5 / 2
            int r5 = r5 + 15
            int r8 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra     // Catch:{ all -> 0x0ec8 }
            int r9 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            if (r8 <= r9) goto L_0x0192
            int r8 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x0ec8 }
            r9 = 1
            if (r8 != r9) goto L_0x018f
            int r8 = dR()     // Catch:{ all -> 0x0ec8 }
        L_0x00e5:
            int r5 = r5 - r8
            android.graphics.Canvas r8 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.uj     // Catch:{ all -> 0x0ec8 }
            r9 = r0
            r10 = 0
            float r5 = (float) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            r8.drawBitmap(r9, r10, r5, r11)     // Catch:{ all -> 0x0ec8 }
        L_0x00f7:
            r0 = r29
            android.graphics.Bitmap r0 = r0.uk     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            if (r5 == 0) goto L_0x0143
            android.graphics.Bitmap r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.uk     // Catch:{ all -> 0x0ec8 }
            r8 = r0
            int r8 = r8.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 - r8
            int r5 = r5 / 2
            int r5 = r5 + 15
            int r8 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra     // Catch:{ all -> 0x0ec8 }
            int r9 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            if (r8 <= r9) goto L_0x0197
            int r8 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x0ec8 }
            r9 = 1
            if (r8 != r9) goto L_0x0195
            int r8 = dR()     // Catch:{ all -> 0x0ec8 }
        L_0x0121:
            int r5 = r5 - r8
            android.graphics.Canvas r8 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.uk     // Catch:{ all -> 0x0ec8 }
            r9 = r0
            android.graphics.Bitmap r10 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r10 = r10.getWidth()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.uk     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            int r11 = r11.getWidth()     // Catch:{ all -> 0x0ec8 }
            int r10 = r10 - r11
            float r10 = (float) r10     // Catch:{ all -> 0x0ec8 }
            float r5 = (float) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            r8.drawBitmap(r9, r10, r5, r11)     // Catch:{ all -> 0x0ec8 }
        L_0x0143:
            r0 = r29
            android.graphics.Bitmap r0 = r0.ul     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            if (r5 == 0) goto L_0x0175
            android.graphics.Bitmap r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.ul     // Catch:{ all -> 0x0ec8 }
            r8 = r0
            int r8 = r8.getWidth()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 - r8
            int r5 = r5 / 2
            android.graphics.Canvas r8 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.ul     // Catch:{ all -> 0x0ec8 }
            r9 = r0
            float r5 = (float) r5     // Catch:{ all -> 0x0ec8 }
            int r10 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x0ec8 }
            if (r10 != 0) goto L_0x0199
            int r10 = dR()     // Catch:{ all -> 0x0ec8 }
        L_0x016c:
            float r10 = (float) r10     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            r8.drawBitmap(r9, r5, r10, r11)     // Catch:{ all -> 0x0ec8 }
        L_0x0175:
            r5 = 0
        L_0x0176:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r8 = r0
            int r8 = r8.lc     // Catch:{ all -> 0x0ec8 }
            int r8 = r8 + r5
            int r8 = r8 % 4
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r9 = r0
            int r9 = r9.lb     // Catch:{ all -> 0x0ec8 }
            if (r8 == r9) goto L_0x019b
            int r5 = r5 + 1
            goto L_0x0176
        L_0x018c:
            r5 = 0
            goto L_0x00b9
        L_0x018f:
            r8 = 0
            goto L_0x00e5
        L_0x0192:
            r8 = 0
            goto L_0x00e5
        L_0x0195:
            r8 = 0
            goto L_0x0121
        L_0x0197:
            r8 = 0
            goto L_0x0121
        L_0x0199:
            r10 = 0
            goto L_0x016c
        L_0x019b:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r9 = r0
            int r9 = r9.kZ     // Catch:{ all -> 0x0ec8 }
            r10 = 1
            int r9 = r9 - r10
            int r9 = r9 % 4
            java.lang.String r9 = com.epoint.android.games.mjfgbfree.af.z(r9)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ all -> 0x0ec8 }
            r8.<init>(r9)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r9 = "圈"
            java.lang.String r9 = com.epoint.android.games.mjfgbfree.af.aa(r9)     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0ec8 }
            int r9 = com.epoint.android.games.mjfgbfree.ai.nk     // Catch:{ all -> 0x0ec8 }
            r10 = 4
            if (r9 != r10) goto L_0x0ecb
            int r5 = r5 + 1
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0ec8 }
        L_0x01c8:
            java.lang.StringBuilder r5 = r8.append(r5)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0ec8 }
            java.lang.String r8 = ""
            int r9 = r29.cI()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            int r0 = r0.uM     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r11 = 2
            if (r10 == r11) goto L_0x0217
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ec8 }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ all -> 0x0ec8 }
            r10.<init>(r8)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r8 = "("
            java.lang.StringBuilder r8 = r10.append(r8)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            int r10 = r10.la     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r8 = r8.append(r10)     // Catch:{ all -> 0x0ec8 }
            if (r9 <= 0) goto L_0x0ed1
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ec8 }
            java.lang.String r11 = "/"
            r10.<init>(r11)     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r9 = r10.append(r9)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0ec8 }
        L_0x0209:
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r9 = ")"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0ec8 }
        L_0x0217:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ec8 }
            java.lang.String r10 = "牌牆"
            java.lang.String r10 = com.epoint.android.games.mjfgbfree.af.aa(r10)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ all -> 0x0ec8 }
            r9.<init>(r10)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r10 = ": "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            int r10 = r10.vS     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0ec8 }
            r10 = 0
            int r11 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra     // Catch:{ all -> 0x0ec8 }
            int r12 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            if (r11 <= r12) goto L_0x0ed5
            r0 = r29
            android.text.TextPaint r0 = r0.tI     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            android.graphics.Paint$Align r12 = android.graphics.Paint.Align.CENTER     // Catch:{ all -> 0x0ec8 }
            r11.setTextAlign(r12)     // Catch:{ all -> 0x0ec8 }
            r11 = 10
            int r11 = r7 - r11
            android.graphics.Canvas r12 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ec8 }
            java.lang.String r16 = "["
            r15.<init>(r16)     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r5 = r15.append(r5)     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r8 = "] "
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r5 = r5.append(r9)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0ec8 }
            android.graphics.Canvas r8 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            int r8 = r8.getWidth()     // Catch:{ all -> 0x0ec8 }
            int r8 = r8 / 2
            r0 = r29
            r1 = r12
            r2 = r5
            r3 = r8
            r4 = r11
            r0.a(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.text.TextPaint r0 = r0.tI     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            android.graphics.Paint$Align r8 = android.graphics.Paint.Align.LEFT     // Catch:{ all -> 0x0ec8 }
            r5.setTextAlign(r8)     // Catch:{ all -> 0x0ec8 }
            r8 = r10
        L_0x028b:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            com.epoint.android.games.mjfgbfree.d.a.a[] r5 = r5.vU     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r9 = r0
            int r9 = r9.aG     // Catch:{ all -> 0x0ec8 }
            r15 = r5[r9]     // Catch:{ all -> 0x0ec8 }
            android.graphics.Canvas r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r9 = r7 + 2
            int r5 = r5 - r9
            int r9 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            int r10 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra     // Catch:{ all -> 0x0ec8 }
            if (r9 > r10) goto L_0x0f94
            java.util.Vector r9 = r15.fV     // Catch:{ all -> 0x0ec8 }
            int r9 = r9.size()     // Catch:{ all -> 0x0ec8 }
            r10 = 16
            if (r9 < r10) goto L_0x0f94
            r9 = 0
        L_0x02b5:
            int r5 = r5 - r9
            int r9 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x0ec8 }
            r10 = 1
            if (r9 != r10) goto L_0x0f98
            int r9 = dR()     // Catch:{ all -> 0x0ec8 }
        L_0x02bf:
            int r16 = r5 - r9
            android.graphics.Canvas r9 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.c.d r0 = r0.uB     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            boolean r10 = r10 instanceof com.epoint.android.games.mjfgbfree.c.f     // Catch:{ all -> 0x0ec8 }
            if (r10 == 0) goto L_0x0f9f
            java.lang.String r10 = ""
        L_0x02d0:
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ all -> 0x0ec8 }
            r5.<init>(r10)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r10 = "$:"
            java.lang.StringBuilder r5 = r5.append(r10)     // Catch:{ all -> 0x0ec8 }
            int r10 = r15.gd     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r5 = r5.append(r10)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            int r0 = r0.uM     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r11 = 2
            if (r10 != r11) goto L_0x0fb6
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ec8 }
            java.lang.String r11 = " / "
            r10.<init>(r11)     // Catch:{ all -> 0x0ec8 }
            int r11 = r15.ge     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x0ec8 }
        L_0x02fc:
            java.lang.StringBuilder r10 = r5.append(r10)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            int r0 = r0.uM     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r11 = 4
            if (r5 != r11) goto L_0x0fba
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = " ("
            r11.<init>(r5)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.e.o r0 = r0.ho     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            com.epoint.android.games.mjfgbfree.e.l r5 = (com.epoint.android.games.mjfgbfree.e.l) r5     // Catch:{ all -> 0x0ec8 }
            com.epoint.android.games.mjfgbfree.tournament.c r5 = r5.cM()     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = r5.cy()     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r5 = r11.append(r5)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r11 = ")"
            java.lang.StringBuilder r5 = r5.append(r11)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0ec8 }
        L_0x032c:
            java.lang.StringBuilder r5 = r10.append(r5)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0ec8 }
            int r10 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            int r11 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra     // Catch:{ all -> 0x0ec8 }
            if (r10 <= r11) goto L_0x0fbe
            r10 = 70
        L_0x033c:
            int r10 = r10 + 110
            r0 = r29
            r1 = r9
            r2 = r5
            r3 = r10
            r4 = r16
            r0.a(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            if (r8 == 0) goto L_0x034d
            int r5 = r7 + -30
            r7 = r5
        L_0x034d:
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r17 = r5.getColor()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r8 = -65536(0xffffffffffff0000, float:NaN)
            r5.setColor(r8)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            android.graphics.Paint$Style r8 = android.graphics.Paint.Style.STROKE     // Catch:{ all -> 0x0ec8 }
            r5.setStyle(r8)     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r8 = "bc"
            java.lang.Object r5 = r5.get(r8)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            r8 = 8
            int r9 = r5 - r8
            android.graphics.Bitmap r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            r8 = 220(0xdc, float:3.08E-43)
            int r8 = r5 - r8
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r10 = "bc"
            java.lang.Object r5 = r5.get(r10)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            r10 = 1
            int r5 = r5 - r10
            int r10 = r8 / r5
            r5 = 0
            int r8 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            int r11 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra     // Catch:{ all -> 0x0ec8 }
            if (r8 <= r11) goto L_0x0fc4
            int r8 = dR()     // Catch:{ all -> 0x0ec8 }
            if (r8 <= 0) goto L_0x0fc4
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.util.Vector r5 = r5.vM     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            float r5 = (float) r5     // Catch:{ all -> 0x0ec8 }
            float r8 = (float) r10     // Catch:{ all -> 0x0ec8 }
            float r5 = r5 / r8
            double r11 = (double) r5     // Catch:{ all -> 0x0ec8 }
            double r11 = java.lang.Math.ceil(r11)     // Catch:{ all -> 0x0ec8 }
            int r5 = (int) r11     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tW     // Catch:{ all -> 0x0ec8 }
            r8 = r0
            int r11 = com.epoint.android.games.mjfgbfree.bb.uz     // Catch:{ all -> 0x0ec8 }
            if (r5 <= r11) goto L_0x0fc1
            r11 = 1
        L_0x03c3:
            r12 = 0
            r8.a(r11, r12)     // Catch:{ all -> 0x0ec8 }
            r11 = r5
        L_0x03c8:
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tW     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r8 = 1
            r12 = 0
            r5.a(r8, r12)     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r8 = "tiles_hd"
            java.lang.Object r5 = r5.get(r8)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            int r18 = r5 / 9
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r8 = "tiles_hd"
            java.lang.Object r5 = r5.get(r8)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r19 = r5 / 5
            android.graphics.Point r20 = new android.graphics.Point     // Catch:{ all -> 0x0ec8 }
            r5 = 0
            r8 = 0
            r0 = r20
            r1 = r5
            r2 = r8
            r0.<init>(r1, r2)     // Catch:{ all -> 0x0ec8 }
            r5 = 0
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r8 = r0
            int r8 = r8.aG     // Catch:{ all -> 0x0ec8 }
            int r8 = r8 + 1
            int r8 = r8 % 4
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            int r12 = r12.li     // Catch:{ all -> 0x0ec8 }
            if (r8 != r12) goto L_0x0fd1
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String[] r8 = com.epoint.android.games.mjfgbfree.bb.tH     // Catch:{ all -> 0x0ec8 }
            r12 = 0
            r8 = r8[r12]     // Catch:{ all -> 0x0ec8 }
            java.lang.Object r5 = r5.get(r8)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            r8 = r5
        L_0x0420:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.util.Vector r5 = r5.vM     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r5 <= 0) goto L_0x1b42
            r5 = 0
            int r12 = com.epoint.android.games.mjfgbfree.bb.uz     // Catch:{ all -> 0x0ec8 }
            if (r11 <= r12) goto L_0x1019
            int r5 = com.epoint.android.games.mjfgbfree.bb.uz     // Catch:{ all -> 0x0ec8 }
            int r5 = r11 - r5
            int r5 = r5 * r10
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            java.util.Vector r11 = r11.vM     // Catch:{ all -> 0x0ec8 }
            int r11 = r11.size()     // Catch:{ all -> 0x0ec8 }
            int r11 = r11 - r5
            int[] r11 = new int[r11]     // Catch:{ all -> 0x0ec8 }
            r12 = r11
            r11 = r5
        L_0x0447:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity.b(r5)     // Catch:{ all -> 0x0ec8 }
            int r5 = r12.length     // Catch:{ all -> 0x0ec8 }
            r0 = r5
            int[] r0 = new int[r0]     // Catch:{ all -> 0x0ec8 }
            r21 = r0
            r5 = 0
        L_0x0456:
            r0 = r12
            int r0 = r0.length     // Catch:{ all -> 0x0ec8 }
            r22 = r0
            r0 = r5
            r1 = r22
            if (r0 < r1) goto L_0x102a
            int r5 = r12.length     // Catch:{ all -> 0x0ec8 }
            r22 = 1
            int r5 = r5 - r22
            r22 = r12[r5]     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r23 = "bc"
            r0 = r5
            r1 = r23
            java.lang.Object r5 = r0.get(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            r23 = 1
            int r5 = r5 - r23
            r0 = r22
            r1 = r10
            r2 = r5
            r3 = r9
            r4 = r7
            android.graphics.Point r22 = com.epoint.android.games.mjfgbfree.af.a(r0, r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            r0 = r12
            r1 = r21
            com.epoint.android.games.mjfgbfree.af.b(r0, r1)     // Catch:{ all -> 0x0ec8 }
            r5 = 0
            r23 = r5
        L_0x048e:
            int r5 = r12.length     // Catch:{ all -> 0x0ec8 }
            r0 = r23
            r1 = r5
            if (r0 < r1) goto L_0x103a
            r0 = r22
            int r0 = r0.x     // Catch:{ all -> 0x0ec8 }
            r7 = r0
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r9 = "bc"
            java.lang.Object r5 = r5.get(r9)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + r7
            r0 = r5
            r1 = r20
            r1.x = r0     // Catch:{ all -> 0x0ec8 }
            r0 = r22
            int r0 = r0.y     // Catch:{ all -> 0x0ec8 }
            r7 = r0
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r9 = "bc"
            java.lang.Object r5 = r5.get(r9)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + r7
            r0 = r5
            r1 = r20
            r1.y = r0     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.util.Vector r5 = r5.vM     // Catch:{ all -> 0x0ec8 }
            java.lang.Object r5 = r5.lastElement()     // Catch:{ all -> 0x0ec8 }
            r0 = r5
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.lh     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x04e8
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.lh     // Catch:{ all -> 0x0ec8 }
            r7 = 7
            if (r5 != r7) goto L_0x1b42
        L_0x04e8:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.String r5 = r5.vO     // Catch:{ all -> 0x0ec8 }
            boolean r5 = r12.equals(r5)     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x1b42
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.aG     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6.li     // Catch:{ all -> 0x0ec8 }
            if (r5 == r6) goto L_0x0567
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r6 = -1
            r5.setColor(r6)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.li     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            r1 = r5
            r0.F(r1)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            boolean r5 = r5.le     // Catch:{ all -> 0x0ec8 }
            if (r5 != 0) goto L_0x0545
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            com.epoint.android.games.mjfgbfree.d.a.a[] r5 = r5.vU     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6.li     // Catch:{ all -> 0x0ec8 }
            r5 = r5[r6]     // Catch:{ all -> 0x0ec8 }
            boolean r5 = r5.gb     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x0545
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.ug     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r0 = r29
            android.graphics.Canvas r0 = r0.un     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            r5.draw(r6)     // Catch:{ all -> 0x0ec8 }
        L_0x0545:
            android.graphics.Canvas r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Canvas r0 = r0.un     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            r0 = r29
            android.graphics.Bitmap r0 = r0.um     // Catch:{ all -> 0x0ec8 }
            r7 = r0
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r9 = r0
            int r9 = r9.aG     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            int r10 = r10.li     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            com.epoint.android.games.mjfgbfree.ui.b.a(r5, r6, r7, r8, r9, r10, r11)     // Catch:{ all -> 0x0ec8 }
        L_0x0567:
            android.graphics.Canvas r6 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r0 = r22
            int r0 = r0.x     // Catch:{ all -> 0x0ec8 }
            r7 = r0
            r0 = r22
            int r0 = r0.y     // Catch:{ all -> 0x0ec8 }
            r8 = r0
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r9 = "tiles_hd"
            java.lang.Object r5 = r5.get(r9)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            int r9 = r5 / 9
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r10 = "tiles_hd"
            java.lang.Object r5 = r5.get(r10)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r10 = r5 / 5
            android.graphics.Rect r11 = new android.graphics.Rect     // Catch:{ all -> 0x0ec8 }
            r11.<init>()     // Catch:{ all -> 0x0ec8 }
            r11.top = r8     // Catch:{ all -> 0x0ec8 }
            r11.left = r7     // Catch:{ all -> 0x0ec8 }
            int r7 = r11.top     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r8 = "bc"
            java.lang.Object r5 = r5.get(r8)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + r7
            r11.bottom = r5     // Catch:{ all -> 0x0ec8 }
            int r7 = r11.left     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r8 = "bc"
            java.lang.Object r5 = r5.get(r8)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + r7
            r11.right = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r11.right     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + 20
            r11.right = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r11.bottom     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + 25
            r11.bottom = r5     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r7 = new android.graphics.Rect     // Catch:{ all -> 0x0ec8 }
            r7.<init>()     // Catch:{ all -> 0x0ec8 }
            r5 = 0
            r7.left = r5     // Catch:{ all -> 0x0ec8 }
            r7.top = r5     // Catch:{ all -> 0x0ec8 }
            int r8 = r7.top     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r21 = "bc"
            r0 = r5
            r1 = r21
            java.lang.Object r5 = r0.get(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + r8
            r7.bottom = r5     // Catch:{ all -> 0x0ec8 }
            int r8 = r7.left     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r21 = "bc"
            r0 = r5
            r1 = r21
            java.lang.Object r5 = r0.get(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + r8
            r7.right = r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r8 = r5.getColor()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            android.graphics.Paint$Style r21 = android.graphics.Paint.Style.STROKE     // Catch:{ all -> 0x0ec8 }
            r0 = r5
            r1 = r21
            r0.setStyle(r1)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r21 = -65536(0xffffffffffff0000, float:NaN)
            r0 = r5
            r1 = r21
            r0.setColor(r1)     // Catch:{ all -> 0x0ec8 }
            r5 = 0
            r6.setDrawFilter(r5)     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r21 = "BC_INV"
            r0 = r5
            r1 = r21
            java.lang.Object r5 = r0.get(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r21 = r0
            r0 = r6
            r1 = r5
            r2 = r7
            r3 = r11
            r4 = r21
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Point r5 = com.epoint.android.games.mjfgbfree.af.Y(r12)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r7 = new android.graphics.Rect     // Catch:{ all -> 0x0ec8 }
            r7.<init>()     // Catch:{ all -> 0x0ec8 }
            int r12 = r5.y     // Catch:{ all -> 0x0ec8 }
            int r12 = r12 * r10
            int r12 = r12 + 1
            r7.top = r12     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.x     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 * r9
            r7.left = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r7.left     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + r9
            r7.right = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r7.top     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + r10
            r9 = 1
            int r5 = r5 - r9
            r7.bottom = r5     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r9 = new android.graphics.Rect     // Catch:{ all -> 0x0ec8 }
            r9.<init>(r11)     // Catch:{ all -> 0x0ec8 }
            int r5 = r9.top     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + 2
            r9.top = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r9.left     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + 2
            r9.left = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r9.right     // Catch:{ all -> 0x0ec8 }
            r10 = 3
            int r5 = r5 - r10
            r9.right = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r9.bottom     // Catch:{ all -> 0x0ec8 }
            r10 = 14
            int r5 = r5 - r10
            r9.bottom = r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r10 = 1
            r5.setDither(r10)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r10 = 1
            r5.setFilterBitmap(r10)     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r10 = "tiles_hd"
            java.lang.Object r5 = r5.get(r10)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r6.drawBitmap(r5, r7, r9, r10)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r7 = 0
            r5.setDither(r7)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r7 = 0
            r5.setFilterBitmap(r7)     // Catch:{ all -> 0x0ec8 }
            android.graphics.PaintFlagsDrawFilter r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.rk     // Catch:{ all -> 0x0ec8 }
            r6.setDrawFilter(r5)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r7 = 255(0xff, float:3.57E-43)
            r5.setAlpha(r7)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r6.drawRect(r11, r5)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            android.graphics.Paint$Style r6 = android.graphics.Paint.Style.FILL     // Catch:{ all -> 0x0ec8 }
            r5.setStyle(r6)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r5.setColor(r8)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            boolean r5 = r5.le     // Catch:{ all -> 0x0ec8 }
            if (r5 != 0) goto L_0x06f8
            android.graphics.Rect r5 = new android.graphics.Rect     // Catch:{ all -> 0x0ec8 }
            r5.<init>(r11)     // Catch:{ all -> 0x0ec8 }
            r0 = r5
            r1 = r29
            r1.ut = r0     // Catch:{ all -> 0x0ec8 }
        L_0x06f8:
            r0 = r29
            com.epoint.android.games.mjfgbfree.b.a r0 = r0.uI     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r0 = r29
            com.epoint.android.games.mjfgbfree.b.a r0 = r0.uI     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6.T     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r7 = r0
            r5.a(r6, r7)     // Catch:{ all -> 0x0ec8 }
            r7 = r11
        L_0x070d:
            r0 = r29
            android.graphics.Bitmap r0 = r0.uo     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            if (r5 == 0) goto L_0x075c
            int r5 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x0ec8 }
            if (r5 != 0) goto L_0x071b
            dR()     // Catch:{ all -> 0x0ec8 }
        L_0x071b:
            android.graphics.Bitmap r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            dR()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.uo     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r5 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x0ec8 }
            if (r5 != 0) goto L_0x10f2
            int r5 = dR()     // Catch:{ all -> 0x0ec8 }
        L_0x0733:
            r0 = r29
            android.graphics.Bitmap r0 = r0.ul     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + r6
            android.graphics.Canvas r6 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.uo     // Catch:{ all -> 0x0ec8 }
            r8 = r0
            r0 = r29
            android.graphics.Bitmap r0 = r0.uj     // Catch:{ all -> 0x0ec8 }
            r9 = r0
            int r9 = r9.getWidth()     // Catch:{ all -> 0x0ec8 }
            r10 = 30
            int r9 = r9 - r10
            int r9 = r9 + 10
            float r9 = (float) r9     // Catch:{ all -> 0x0ec8 }
            float r5 = (float) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r6.drawBitmap(r8, r9, r5, r10)     // Catch:{ all -> 0x0ec8 }
        L_0x075c:
            r0 = r29
            android.graphics.Bitmap r0 = r0.up     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            if (r5 == 0) goto L_0x07b4
            int r5 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x0ec8 }
            if (r5 != 0) goto L_0x10f5
            int r5 = dR()     // Catch:{ all -> 0x0ec8 }
        L_0x076b:
            r0 = r29
            android.graphics.Bitmap r0 = r0.ul     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r6 = r6 + r5
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r8 = "bc"
            java.lang.Object r5 = r5.get(r8)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 / 2
            int r5 = r6 - r5
            android.graphics.Canvas r6 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.up     // Catch:{ all -> 0x0ec8 }
            r8 = r0
            android.graphics.Bitmap r9 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r9 = r9.getWidth()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.uk     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            int r10 = r10.getWidth()     // Catch:{ all -> 0x0ec8 }
            int r9 = r9 - r10
            r0 = r29
            android.graphics.Bitmap r0 = r0.up     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            int r10 = r10.getWidth()     // Catch:{ all -> 0x0ec8 }
            int r9 = r9 - r10
            int r9 = r9 + 30
            float r9 = (float) r9     // Catch:{ all -> 0x0ec8 }
            float r5 = (float) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r6.drawBitmap(r8, r9, r5, r10)     // Catch:{ all -> 0x0ec8 }
        L_0x07b4:
            r0 = r29
            android.graphics.Bitmap r0 = r0.uq     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            if (r5 == 0) goto L_0x0812
            android.graphics.Bitmap r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.uq     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6.getWidth()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 - r6
            int r5 = r5 / 2
            android.graphics.Canvas r6 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.ur     // Catch:{ all -> 0x0ec8 }
            r8 = r0
            int r9 = r5 + 10
            float r9 = (float) r9     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.uq     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            int r10 = r10.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r10 = r16 - r10
            int r10 = r10 + 2
            float r10 = (float) r10     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            r6.drawBitmap(r8, r9, r10, r11)     // Catch:{ all -> 0x0ec8 }
            int r6 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x0ec8 }
            if (r6 != 0) goto L_0x10f8
            int r6 = dR()     // Catch:{ all -> 0x0ec8 }
        L_0x07f5:
            r0 = r29
            android.graphics.Bitmap r0 = r0.ul     // Catch:{ all -> 0x0ec8 }
            r8 = r0
            int r8 = r8.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r6 = r6 + r8
            android.graphics.Canvas r8 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.uq     // Catch:{ all -> 0x0ec8 }
            r9 = r0
            int r5 = r5 + 10
            float r5 = (float) r5     // Catch:{ all -> 0x0ec8 }
            float r6 = (float) r6     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r8.drawBitmap(r9, r5, r6, r10)     // Catch:{ all -> 0x0ec8 }
        L_0x0812:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.util.Vector r5 = r5.wf     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x0833
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.util.Vector r5 = r5.wf     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            r6 = 1
            if (r5 <= r6) goto L_0x0833
            r5 = 0
            r0 = r5
            r1 = r29
            r1.ut = r0     // Catch:{ all -> 0x0ec8 }
        L_0x0833:
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            android.graphics.Paint$Style r6 = android.graphics.Paint.Style.FILL     // Catch:{ all -> 0x0ec8 }
            r5.setStyle(r6)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r0 = r5
            r1 = r17
            r0.setColor(r1)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r6 = 255(0xff, float:3.57E-43)
            r5.setAlpha(r6)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r6 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r8 = "bc"
            java.lang.Object r5 = r5.get(r8)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            r8 = 1
            int r5 = r5 - r8
            int r5 = r5 * 17
            int r5 = r6 - r5
            int r8 = r5 / 2
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.aG     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            r1 = r5
            com.epoint.android.games.mjfgbfree.d.c r9 = r0.G(r1)     // Catch:{ all -> 0x0ec8 }
            r29.ee()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r6 = 0
            r5.v(r6)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            java.util.Vector r0 = r0.tJ     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r5 <= 0) goto L_0x09c5
            r0 = r29
            java.util.Vector r0 = r0.tJ     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r6 = 0
            java.lang.Object r5 = r5.elementAt(r6)     // Catch:{ all -> 0x0ec8 }
            com.epoint.android.games.mjfgbfree.ui.c r5 = (com.epoint.android.games.mjfgbfree.ui.c) r5     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r5 = r5.nZ     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.top     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6.ba()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 - r6
            r6 = 10
            int r5 = r5 - r6
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6.aG     // Catch:{ all -> 0x0ec8 }
            int r6 = r6 + 2
            int r6 = r6 % 4
            r0 = r29
            r1 = r6
            com.epoint.android.games.mjfgbfree.d.c r6 = r0.G(r1)     // Catch:{ all -> 0x0ec8 }
            if (r6 == 0) goto L_0x08d6
            int r6 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            int r10 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra     // Catch:{ all -> 0x0ec8 }
            if (r6 <= r10) goto L_0x08d6
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6.ba()     // Catch:{ all -> 0x0ec8 }
            int r6 = r6 + 10
            int r5 = r5 + r6
        L_0x08d6:
            android.graphics.Bitmap r6 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r6 = r6.getWidth()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            int r10 = r10.bb()     // Catch:{ all -> 0x0ec8 }
            int r6 = r6 - r10
            int r6 = r6 / 2
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r10.c(r6, r5)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.li     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6.aG     // Catch:{ all -> 0x0ec8 }
            if (r5 != r6) goto L_0x090a
            r5 = 0
            r6 = r5
        L_0x0902:
            java.util.Vector r5 = r15.fZ     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r6 < r5) goto L_0x10fb
        L_0x090a:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.lh     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x0945
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.lh     // Catch:{ all -> 0x0ec8 }
            r6 = 6
            if (r5 == r6) goto L_0x0945
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.lh     // Catch:{ all -> 0x0ec8 }
            r6 = 5
            if (r5 == r6) goto L_0x0945
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.lh     // Catch:{ all -> 0x0ec8 }
            r6 = 4
            if (r5 == r6) goto L_0x0945
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.lh     // Catch:{ all -> 0x0ec8 }
            r6 = 3
            if (r5 == r6) goto L_0x0945
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.lh     // Catch:{ all -> 0x0ec8 }
            r6 = 2
            if (r5 != r6) goto L_0x0956
        L_0x0945:
            boolean r5 = r15.gb     // Catch:{ all -> 0x0ec8 }
            if (r5 != 0) goto L_0x0956
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.String r5 = r5.bc()     // Catch:{ all -> 0x0ec8 }
            if (r5 != 0) goto L_0x0956
            if (r9 == 0) goto L_0x09c5
        L_0x0956:
            if (r9 == 0) goto L_0x1182
            java.util.Vector r5 = r15.fV     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 % 3
            r6 = 1
            if (r5 != r6) goto L_0x1139
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.String r6 = "花糊exp"
            java.lang.String r6 = com.epoint.android.games.mjfgbfree.af.aa(r6)     // Catch:{ all -> 0x0ec8 }
            r5.v(r6)     // Catch:{ all -> 0x0ec8 }
        L_0x0971:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.util.Vector r5 = r5.wf     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            r6 = 1
            if (r5 <= r6) goto L_0x1172
            r5 = 0
            r0 = r5
            r1 = r29
            r1.ut = r0     // Catch:{ all -> 0x0ec8 }
        L_0x0985:
            boolean r5 = r15.gb     // Catch:{ all -> 0x0ec8 }
            if (r5 != 0) goto L_0x099b
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.li     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6.aG     // Catch:{ all -> 0x0ec8 }
            if (r5 == r6) goto L_0x099b
            if (r9 == 0) goto L_0x09c5
        L_0x099b:
            boolean r5 = r15.gb     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x09c5
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.String r5 = r5.bc()     // Catch:{ all -> 0x0ec8 }
            if (r5 != 0) goto L_0x09c5
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.util.Vector r5 = r5.wf     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r5 > 0) goto L_0x09c5
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.String r6 = "聽牌"
            java.lang.String r6 = com.epoint.android.games.mjfgbfree.af.aa(r6)     // Catch:{ all -> 0x0ec8 }
            r5.v(r6)     // Catch:{ all -> 0x0ec8 }
        L_0x09c5:
            r5 = 2
            r13.top = r5     // Catch:{ all -> 0x0ec8 }
            r5 = 1
            r13.left = r5     // Catch:{ all -> 0x0ec8 }
            r5 = 22
            r13.right = r5     // Catch:{ all -> 0x0ec8 }
            r5 = 29
            r13.bottom = r5     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r10 = new android.graphics.Rect     // Catch:{ all -> 0x0ec8 }
            r10.<init>()     // Catch:{ all -> 0x0ec8 }
            r5 = 0
            r10.left = r5     // Catch:{ all -> 0x0ec8 }
            r5 = 0
            r10.top = r5     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r6 = "bc"
            java.lang.Object r5 = r5.get(r6)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            r10.right = r5     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r6 = "bc"
            java.lang.Object r5 = r5.get(r6)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            r10.bottom = r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            java.util.Vector r0 = r0.tJ     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r5 <= 0) goto L_0x0a13
            r5 = 0
            r11 = r5
        L_0x0a0b:
            java.util.Vector r5 = r15.fV     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r11 < r5) goto L_0x11ce
        L_0x0a13:
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r6 = 255(0xff, float:3.57E-43)
            r5.setAlpha(r6)     // Catch:{ all -> 0x0ec8 }
            r6 = 0
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.c r0 = r0.tY     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            if (r5 == 0) goto L_0x0c70
            android.graphics.Canvas r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r9 = 0
            r5.setDrawFilter(r9)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.c r0 = r0.tY     // Catch:{ all -> 0x0ec8 }
            r9 = r0
            android.graphics.Rect r5 = r9.nZ     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.left     // Catch:{ all -> 0x0ec8 }
            r11 = 20
            int r5 = r5 - r11
            r7.left = r5     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r5 = r9.nZ     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.right     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + 20
            r7.right = r5     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r5 = r9.nZ     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.top     // Catch:{ all -> 0x0ec8 }
            r11 = 85
            int r5 = r5 - r11
            r7.top = r5     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r5 = r9.nZ     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.bottom     // Catch:{ all -> 0x0ec8 }
            r11 = 45
            int r5 = r5 - r11
            r7.bottom = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            int r11 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra     // Catch:{ all -> 0x0ec8 }
            if (r5 > r11) goto L_0x0a63
            java.util.Vector r5 = r15.fV     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            r11 = 16
            if (r5 >= r11) goto L_0x0a71
        L_0x0a63:
            int r5 = r7.top     // Catch:{ all -> 0x0ec8 }
            r11 = 15
            int r5 = r5 - r11
            r7.top = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r7.bottom     // Catch:{ all -> 0x0ec8 }
            r11 = 15
            int r5 = r5 - r11
            r7.bottom = r5     // Catch:{ all -> 0x0ec8 }
        L_0x0a71:
            int r5 = r7.left     // Catch:{ all -> 0x0ec8 }
            if (r5 >= 0) goto L_0x0a83
            int r5 = r7.right     // Catch:{ all -> 0x0ec8 }
            int r11 = r7.left     // Catch:{ all -> 0x0ec8 }
            int r11 = java.lang.Math.abs(r11)     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + r11
            r7.right = r5     // Catch:{ all -> 0x0ec8 }
            r5 = 2
            r7.left = r5     // Catch:{ all -> 0x0ec8 }
        L_0x0a83:
            int r5 = r7.right     // Catch:{ all -> 0x0ec8 }
            int r11 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            if (r5 <= r11) goto L_0x0a9b
            int r5 = r7.right     // Catch:{ all -> 0x0ec8 }
            int r11 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 - r11
            int r5 = r5 + 2
            int r11 = r7.left     // Catch:{ all -> 0x0ec8 }
            int r11 = r11 - r5
            r7.left = r11     // Catch:{ all -> 0x0ec8 }
            int r11 = r7.right     // Catch:{ all -> 0x0ec8 }
            int r5 = r11 - r5
            r7.right = r5     // Catch:{ all -> 0x0ec8 }
        L_0x0a9b:
            android.graphics.Canvas r11 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r12 = "bc"
            java.lang.Object r5 = r5.get(r12)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            r11.drawBitmap(r5, r10, r7, r12)     // Catch:{ all -> 0x0ec8 }
            int r5 = r7.left     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tL     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            int r10 = r10.bb()     // Catch:{ all -> 0x0ec8 }
            int r11 = r7.width()     // Catch:{ all -> 0x0ec8 }
            int r10 = r10 - r11
            int r10 = r10 / 2
            int r5 = r5 - r10
            java.util.Vector r10 = r15.fX     // Catch:{ all -> 0x0ec8 }
            int r10 = r10.size()     // Catch:{ all -> 0x0ec8 }
            if (r10 != 0) goto L_0x0add
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.c r0 = r0.tY     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r0 = r29
            java.util.Vector r0 = r0.tJ     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            r12 = 0
            java.lang.Object r11 = r11.elementAt(r12)     // Catch:{ all -> 0x0ec8 }
            if (r10 != r11) goto L_0x1557
            int r5 = r7.left     // Catch:{ all -> 0x0ec8 }
        L_0x0add:
            r10 = 2
            int r5 = java.lang.Math.max(r5, r10)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tL     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            int r10 = r10.bb()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + r10
            int r10 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            if (r5 <= r10) goto L_0x0af8
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tL     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r5.bb()     // Catch:{ all -> 0x0ec8 }
        L_0x0af8:
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tL     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r5.setBounds(r7)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tL     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r5.bg()     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r10 = new android.graphics.Rect     // Catch:{ all -> 0x0ec8 }
            r10.<init>()     // Catch:{ all -> 0x0ec8 }
            int r5 = r7.left     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + 10
            r10.left = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r7.top     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + 22
            r10.top = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r7.right     // Catch:{ all -> 0x0ec8 }
            r11 = 10
            int r5 = r5 - r11
            r10.right = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r7.bottom     // Catch:{ all -> 0x0ec8 }
            r11 = 7
            int r5 = r5 - r11
            r10.bottom = r5     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = r9.lw     // Catch:{ all -> 0x0ec8 }
            android.graphics.Point r5 = com.epoint.android.games.mjfgbfree.af.Y(r5)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r11 = new android.graphics.Rect     // Catch:{ all -> 0x0ec8 }
            r11.<init>()     // Catch:{ all -> 0x0ec8 }
            int r12 = r5.y     // Catch:{ all -> 0x0ec8 }
            int r12 = r12 * r19
            int r12 = r12 + 1
            r11.top = r12     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.x     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 * r18
            r11.left = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r11.left     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + r18
            r11.right = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r11.top     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + r19
            r12 = 1
            int r5 = r5 - r12
            r11.bottom = r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r12 = 1
            r5.setDither(r12)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r12 = 1
            r5.setFilterBitmap(r12)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Canvas r12 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r13 = "tiles_hd"
            java.lang.Object r5 = r5.get(r13)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r13 = r0
            r12.drawBitmap(r5, r11, r10, r13)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r10 = 0
            r5.setDither(r10)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r10 = 0
            r5.setFilterBitmap(r10)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.b.a r0 = r0.uI     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r0 = r29
            com.epoint.android.games.mjfgbfree.b.a r0 = r0.uI     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            int r10 = r10.U     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            r5.a(r10, r11)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tL     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.String r5 = r5.bc()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            java.util.Vector r10 = r10.wc     // Catch:{ all -> 0x0ec8 }
            java.lang.String r11 = r9.lw     // Catch:{ all -> 0x0ec8 }
            boolean r10 = r10.contains(r11)     // Catch:{ all -> 0x0ec8 }
            if (r10 == 0) goto L_0x0bbf
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tL     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            java.lang.String r11 = "過水"
            java.lang.String r11 = com.epoint.android.games.mjfgbfree.af.aa(r11)     // Catch:{ all -> 0x0ec8 }
            r10.v(r11)     // Catch:{ all -> 0x0ec8 }
        L_0x0bbf:
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tL     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            android.graphics.Canvas r11 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r10.draw(r11)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tL     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r10.v(r5)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Canvas r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            android.graphics.PaintFlagsDrawFilter r10 = com.epoint.android.games.mjfgbfree.MJ16Activity.rk     // Catch:{ all -> 0x0ec8 }
            r5.setDrawFilter(r10)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.bl r0 = r0.uA     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.util.Vector r5 = r5.Av     // Catch:{ all -> 0x0ec8 }
            java.lang.String r10 = r9.lw     // Catch:{ all -> 0x0ec8 }
            boolean r5 = r5.contains(r10)     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x0c70
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.getColor()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            android.graphics.Paint$Style r11 = android.graphics.Paint.Style.FILL     // Catch:{ all -> 0x0ec8 }
            r10.setStyle(r11)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r11 = -65536(0xffffffffffff0000, float:NaN)
            r10.setColor(r11)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r11 = 48
            r10.setAlpha(r11)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Canvas r10 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            r10.drawRect(r7, r11)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r10.setColor(r5)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = r9.lw     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.bl r0 = r0.uA     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            java.lang.String r10 = r10.Ax     // Catch:{ all -> 0x0ec8 }
            boolean r5 = r5.equals(r10)     // Catch:{ all -> 0x0ec8 }
            if (r5 != 0) goto L_0x0c62
            r0 = r29
            com.epoint.android.games.mjfgbfree.bl r0 = r0.uA     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.String r10 = r9.lw     // Catch:{ all -> 0x0ec8 }
            r5.Ax = r10     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.bl r0 = r0.uA     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.util.Vector r5 = r5.Aw     // Catch:{ all -> 0x0ec8 }
            r5.clear()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            com.epoint.android.games.mjfgbfree.d.a.a[] r5 = r5.vU     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            int r10 = r10.aG     // Catch:{ all -> 0x0ec8 }
            r5 = r5[r10]     // Catch:{ all -> 0x0ec8 }
            java.util.Vector r5 = r5.fV     // Catch:{ all -> 0x0ec8 }
            java.lang.String r9 = r9.lw     // Catch:{ all -> 0x0ec8 }
            java.util.Vector r9 = a.b.a.b.a(r5, r9)     // Catch:{ all -> 0x0ec8 }
            r5 = 0
            r10 = r5
        L_0x0c5c:
            int r5 = r9.size()     // Catch:{ all -> 0x0ec8 }
            if (r10 < r5) goto L_0x1584
        L_0x0c62:
            r0 = r29
            com.epoint.android.games.mjfgbfree.bl r0 = r0.uA     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.util.Vector r5 = r5.Aw     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r5 <= 0) goto L_0x0c70
            r6 = r7
        L_0x0c70:
            r9 = 0
            r5 = 0
            r10 = -1
            r11 = 0
            r12 = 0
            r13 = r11
            r11 = r12
            r12 = r10
            r10 = r5
        L_0x0c79:
            java.util.Vector r5 = r15.fX     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r11 < r5) goto L_0x159f
            if (r12 < 0) goto L_0x0c8e
            r5 = 5
            if (r13 != r5) goto L_0x0c89
            r5 = 3
            if (r12 < r5) goto L_0x0c8c
        L_0x0c89:
            r5 = 2
            if (r12 >= r5) goto L_0x0c8e
        L_0x0c8c:
            r5 = 1
            r9 = r5
        L_0x0c8e:
            android.graphics.Bitmap r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r10 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r11 = "bc"
            java.lang.Object r5 = r5.get(r11)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 * 3
            int r5 = r10 - r5
            if (r9 == 0) goto L_0x15cc
            r9 = 37
        L_0x0caa:
            int r5 = r5 + r9
            int r9 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x0ec8 }
            r10 = 1
            if (r9 != r10) goto L_0x15d0
            int r9 = dR()     // Catch:{ all -> 0x0ec8 }
        L_0x0cb4:
            int r9 = r5 - r9
            r5 = 0
            r10 = r5
            r11 = r8
        L_0x0cb9:
            java.util.Vector r5 = r15.fZ     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r10 < r5) goto L_0x15d3
            java.util.Vector r5 = r15.fZ     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r5 <= 0) goto L_0x1b36
            int r5 = r11 + 3
            r7 = r5
        L_0x0ccc:
            r10 = 0
            r11 = 0
            r5 = 0
            r12 = 0
            r13 = 0
            r16 = r5
        L_0x0cd3:
            java.util.Vector r5 = r15.fX     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r13 < r5) goto L_0x1691
            r5 = 0
            r13 = 0
            r17 = r10
            r18 = r7
            r7 = r11
            r10 = r13
            r13 = r12
            r11 = r5
            r12 = r9
        L_0x0ce6:
            java.util.Vector r5 = r15.fX     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r10 < r5) goto L_0x16ac
            java.lang.String r5 = ""
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r7 = r0
            int r7 = r7.lb     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            int r10 = r10.aG     // Catch:{ all -> 0x0ec8 }
            if (r7 != r10) goto L_0x1b18
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ all -> 0x0ec8 }
            r7.<init>(r5)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = "["
            java.lang.StringBuilder r5 = r7.append(r5)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ all -> 0x0ec8 }
            r7.<init>(r5)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = "莊"
            java.lang.String r5 = com.epoint.android.games.mjfgbfree.af.aa(r5)     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r5 = r7.append(r5)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r7 = r0
            int r7 = r7.vL     // Catch:{ all -> 0x0ec8 }
            if (r7 <= 0) goto L_0x0d4b
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ all -> 0x0ec8 }
            r7.<init>(r5)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.vL     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r5 = r7.append(r5)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0ec8 }
        L_0x0d4b:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ all -> 0x0ec8 }
            r7.<init>(r5)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = "] "
            java.lang.StringBuilder r5 = r7.append(r5)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0ec8 }
            r7 = r5
        L_0x0d5f:
            android.graphics.Canvas r10 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            if (r13 == 0) goto L_0x1829
            r11 = r18
        L_0x0d65:
            r5 = 14
            int r9 = r9 - r5
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r12 = "bc"
            java.lang.Object r5 = r5.get(r12)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 * 2
            int r5 = r5 + r9
            r0 = r29
            r1 = r10
            r2 = r7
            r3 = r11
            r4 = r5
            r0.a(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            r5 = 0
            r7 = 0
            r9 = r5
        L_0x0d85:
            r0 = r29
            java.util.Vector r0 = r0.gM     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r7 < r5) goto L_0x182c
            r5 = 5
            if (r9 >= r5) goto L_0x1852
            r5 = 90
            r7 = r5
        L_0x0d96:
            r5 = 0
            r10 = r5
        L_0x0d98:
            r0 = r29
            java.util.Vector r0 = r0.gM     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r10 < r5) goto L_0x1857
            r5 = 0
            r7 = 0
            r10 = r5
        L_0x0da6:
            r0 = r29
            java.util.Vector r0 = r0.gM     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r7 < r5) goto L_0x188c
            r0 = r29
            boolean r0 = r0.uU     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            if (r5 != 0) goto L_0x0e05
            int r5 = com.epoint.android.games.mjfgbfree.ai.ni     // Catch:{ all -> 0x0ec8 }
            int r7 = com.epoint.android.games.mjfgbfree.ac.jT     // Catch:{ all -> 0x0ec8 }
            if (r5 == r7) goto L_0x0dff
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            boolean r5 = r5.wa     // Catch:{ all -> 0x0ec8 }
            if (r5 != 0) goto L_0x0df5
            int r5 = com.epoint.android.games.mjfgbfree.ai.ni     // Catch:{ all -> 0x0ec8 }
            int r7 = com.epoint.android.games.mjfgbfree.ac.jV     // Catch:{ all -> 0x0ec8 }
            if (r5 != r7) goto L_0x0dff
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            boolean r5 = r5.vX     // Catch:{ all -> 0x0ec8 }
            if (r5 != 0) goto L_0x0df5
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.wb     // Catch:{ all -> 0x0ec8 }
            if (r5 != 0) goto L_0x0df5
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.util.Vector r5 = r5.vV     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            if (r5 > 0) goto L_0x0df5
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            boolean r5 = r5.vY     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x0dff
        L_0x0df5:
            r0 = r29
            android.os.Vibrator r0 = r0.tX     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r7 = 40
            r5.vibrate(r7)     // Catch:{ all -> 0x0ec8 }
        L_0x0dff:
            r5 = 1
            r0 = r5
            r1 = r29
            r1.uU = r0     // Catch:{ all -> 0x0ec8 }
        L_0x0e05:
            r5 = 0
            r7 = r5
        L_0x0e07:
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.length     // Catch:{ all -> 0x0ec8 }
            if (r7 < r5) goto L_0x192c
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.util.Vector r5 = r5.wf     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.size()     // Catch:{ all -> 0x0ec8 }
            r7 = 1
            if (r5 <= r7) goto L_0x0e23
            r5 = 0
            r0 = r5
            r1 = r29
            r1.ut = r0     // Catch:{ all -> 0x0ec8 }
        L_0x0e23:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.vT     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r7 = r0
            int r7 = r7.aG     // Catch:{ all -> 0x0ec8 }
            if (r5 == r7) goto L_0x0e43
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.aG     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            r1 = r5
            boolean r5 = r0.H(r1)     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x0e51
        L_0x0e43:
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.String r7 = "出沖"
            java.lang.String r7 = com.epoint.android.games.mjfgbfree.af.aa(r7)     // Catch:{ all -> 0x0ec8 }
            r5.v(r7)     // Catch:{ all -> 0x0ec8 }
        L_0x0e51:
            r0 = r29
            boolean r0 = r0.uy     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            if (r5 == 0) goto L_0x0e6d
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.String r5 = r5.bc()     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x0e6d
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            android.graphics.Canvas r7 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r5.draw(r7)     // Catch:{ all -> 0x0ec8 }
        L_0x0e6d:
            if (r6 == 0) goto L_0x0e78
            android.graphics.Canvas r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            r1 = r5
            r2 = r6
            r0.a(r1, r2)     // Catch:{ all -> 0x0ec8 }
        L_0x0e78:
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tW     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            android.graphics.Rect r5 = r5.getBounds()     // Catch:{ all -> 0x0ec8 }
            android.graphics.Canvas r6 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            int r6 = r6.getWidth()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.um     // Catch:{ all -> 0x0ec8 }
            r7 = r0
            int r7 = r7.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r6 = r6 - r7
            r5.right = r6     // Catch:{ all -> 0x0ec8 }
            r0 = r20
            int r0 = r0.y     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            r5.bottom = r6     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tW     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r0 = r29
            android.graphics.Bitmap r0 = r0.um     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6.getHeight()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Bitmap r0 = r0.um     // Catch:{ all -> 0x0ec8 }
            r7 = r0
            int r7 = r7.getHeight()     // Catch:{ all -> 0x0ec8 }
            r5.c(r6, r7)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a.i r0 = r0.ue     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            if (r5 == 0) goto L_0x0ec5
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a.i r0 = r0.ue     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            android.graphics.Canvas r6 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r5.a(r6)     // Catch:{ all -> 0x0ec8 }
        L_0x0ec5:
            monitor-exit(r14)     // Catch:{ all -> 0x0ec8 }
            goto L_0x0086
        L_0x0ec8:
            r5 = move-exception
            monitor-exit(r14)     // Catch:{ all -> 0x0ec8 }
            throw r5     // Catch:{ all -> 0x0090 }
        L_0x0ecb:
            java.lang.String r5 = com.epoint.android.games.mjfgbfree.af.z(r5)     // Catch:{ all -> 0x0ec8 }
            goto L_0x01c8
        L_0x0ed1:
            java.lang.String r9 = ""
            goto L_0x0209
        L_0x0ed5:
            boolean r10 = com.epoint.android.games.mjfgbfree.MJ16Activity.rd     // Catch:{ all -> 0x0ec8 }
            if (r10 != 0) goto L_0x0edd
            boolean r10 = com.epoint.android.games.mjfgbfree.MJ16Activity.re     // Catch:{ all -> 0x0ec8 }
            if (r10 == 0) goto L_0x0f66
        L_0x0edd:
            boolean r10 = com.epoint.android.games.mjfgbfree.MJ16Activity.qL     // Catch:{ all -> 0x0ec8 }
            if (r10 == 0) goto L_0x0f66
            r0 = r29
            android.text.TextPaint r0 = r0.tI     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            android.graphics.Paint$Align r10 = r10.getTextAlign()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.text.TextPaint r0 = r0.tI     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            android.graphics.Paint$Align r12 = android.graphics.Paint.Align.CENTER     // Catch:{ all -> 0x0ec8 }
            r11.setTextAlign(r12)     // Catch:{ all -> 0x0ec8 }
            boolean r11 = com.epoint.android.games.mjfgbfree.MJ16Activity.rd     // Catch:{ all -> 0x0ec8 }
            if (r11 == 0) goto L_0x0f55
            r11 = 40
        L_0x0efa:
            boolean r12 = com.epoint.android.games.mjfgbfree.MJ16Activity.rd     // Catch:{ all -> 0x0ec8 }
            if (r12 == 0) goto L_0x0f58
            android.graphics.Canvas r9 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            java.lang.String r12 = "牌牆"
            java.lang.String r12 = com.epoint.android.games.mjfgbfree.af.aa(r12)     // Catch:{ all -> 0x0ec8 }
            r15 = 28
            r0 = r29
            r1 = r9
            r2 = r12
            r3 = r11
            r4 = r15
            r0.a(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Canvas r9 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            int r12 = r12.vS     // Catch:{ all -> 0x0ec8 }
            java.lang.String r12 = java.lang.String.valueOf(r12)     // Catch:{ all -> 0x0ec8 }
            r15 = 55
            r0 = r29
            r1 = r9
            r2 = r12
            r3 = r11
            r4 = r15
            r0.a(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
        L_0x0f29:
            android.graphics.Canvas r9 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            int r12 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            int r12 = r12 - r11
            r15 = 28
            r0 = r29
            r1 = r9
            r2 = r5
            r3 = r12
            r4 = r15
            r0.a(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Canvas r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            int r9 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            int r9 = r9 - r11
            r11 = 55
            r0 = r29
            r1 = r5
            r2 = r8
            r3 = r9
            r4 = r11
            r0.a(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.text.TextPaint r0 = r0.tI     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r5.setTextAlign(r10)     // Catch:{ all -> 0x0ec8 }
        L_0x0f51:
            r5 = 1
            r8 = r5
            goto L_0x028b
        L_0x0f55:
            r11 = 60
            goto L_0x0efa
        L_0x0f58:
            android.graphics.Canvas r12 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r15 = 40
            r0 = r29
            r1 = r12
            r2 = r9
            r3 = r11
            r4 = r15
            r0.a(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            goto L_0x0f29
        L_0x0f66:
            android.graphics.Canvas r10 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r11 = 5
            r12 = 28
            r0 = r29
            r1 = r10
            r2 = r9
            r3 = r11
            r4 = r12
            r0.a(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Canvas r9 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ all -> 0x0ec8 }
            r10.<init>(r5)     // Catch:{ all -> 0x0ec8 }
            java.lang.StringBuilder r5 = r10.append(r8)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0ec8 }
            r8 = 5
            r10 = 55
            r0 = r29
            r1 = r9
            r2 = r5
            r3 = r8
            r4 = r10
            r0.a(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            goto L_0x0f51
        L_0x0f94:
            r9 = 10
            goto L_0x02b5
        L_0x0f98:
            int r9 = dR()     // Catch:{ all -> 0x0ec8 }
            int r9 = -r9
            goto L_0x02bf
        L_0x0f9f:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ec8 }
            java.lang.String r11 = r15.xx     // Catch:{ all -> 0x0ec8 }
            java.lang.String r11 = java.lang.String.valueOf(r11)     // Catch:{ all -> 0x0ec8 }
            r10.<init>(r11)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r11 = " "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x0ec8 }
            goto L_0x02d0
        L_0x0fb6:
            java.lang.String r10 = ""
            goto L_0x02fc
        L_0x0fba:
            java.lang.String r5 = ""
            goto L_0x032c
        L_0x0fbe:
            r10 = 0
            goto L_0x033c
        L_0x0fc1:
            r11 = 0
            goto L_0x03c3
        L_0x0fc4:
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tW     // Catch:{ all -> 0x0ec8 }
            r8 = r0
            r11 = 0
            r12 = 0
            r8.a(r11, r12)     // Catch:{ all -> 0x0ec8 }
            r11 = r5
            goto L_0x03c8
        L_0x0fd1:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r8 = r0
            int r8 = r8.aG     // Catch:{ all -> 0x0ec8 }
            int r8 = r8 + 2
            int r8 = r8 % 4
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            int r12 = r12.li     // Catch:{ all -> 0x0ec8 }
            if (r8 != r12) goto L_0x0ff5
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String[] r8 = com.epoint.android.games.mjfgbfree.bb.tH     // Catch:{ all -> 0x0ec8 }
            r12 = 1
            r8 = r8[r12]     // Catch:{ all -> 0x0ec8 }
            java.lang.Object r5 = r5.get(r8)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            r8 = r5
            goto L_0x0420
        L_0x0ff5:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r8 = r0
            int r8 = r8.aG     // Catch:{ all -> 0x0ec8 }
            int r8 = r8 + 3
            int r8 = r8 % 4
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            int r12 = r12.li     // Catch:{ all -> 0x0ec8 }
            if (r8 != r12) goto L_0x1b45
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String[] r8 = com.epoint.android.games.mjfgbfree.bb.tH     // Catch:{ all -> 0x0ec8 }
            r12 = 2
            r8 = r8[r12]     // Catch:{ all -> 0x0ec8 }
            java.lang.Object r5 = r5.get(r8)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            r8 = r5
            goto L_0x0420
        L_0x1019:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            java.util.Vector r11 = r11.vM     // Catch:{ all -> 0x0ec8 }
            int r11 = r11.size()     // Catch:{ all -> 0x0ec8 }
            int[] r11 = new int[r11]     // Catch:{ all -> 0x0ec8 }
            r12 = r11
            r11 = r5
            goto L_0x0447
        L_0x102a:
            r0 = r29
            int[] r0 = r0.us     // Catch:{ all -> 0x0ec8 }
            r22 = r0
            r22 = r22[r5]     // Catch:{ all -> 0x0ec8 }
            r12[r5] = r22     // Catch:{ all -> 0x0ec8 }
            r21[r5] = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + 1
            goto L_0x0456
        L_0x103a:
            r24 = r12[r23]     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r25 = "bc"
            r0 = r5
            r1 = r25
            java.lang.Object r5 = r0.get(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            r25 = 1
            int r5 = r5 - r25
            r0 = r24
            r1 = r10
            r2 = r5
            r3 = r9
            r4 = r7
            android.graphics.Point r24 = com.epoint.android.games.mjfgbfree.af.a(r0, r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.util.Vector r5 = r5.vM     // Catch:{ all -> 0x0ec8 }
            r25 = r21[r23]     // Catch:{ all -> 0x0ec8 }
            int r25 = r25 + r11
            r0 = r5
            r1 = r25
            java.lang.Object r5 = r0.elementAt(r1)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r25 = r0
            r0 = r25
            int r0 = r0.lh     // Catch:{ all -> 0x0ec8 }
            r25 = r0
            if (r25 == 0) goto L_0x1091
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r25 = r0
            r0 = r25
            int r0 = r0.lh     // Catch:{ all -> 0x0ec8 }
            r25 = r0
            r26 = 7
            r0 = r25
            r1 = r26
            if (r0 != r1) goto L_0x10b7
        L_0x1091:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r25 = r0
            r0 = r25
            java.lang.String r0 = r0.vO     // Catch:{ all -> 0x0ec8 }
            r25 = r0
            r0 = r5
            r1 = r25
            boolean r25 = r0.equals(r1)     // Catch:{ all -> 0x0ec8 }
            if (r25 == 0) goto L_0x10b7
            r25 = r21[r23]     // Catch:{ all -> 0x0ec8 }
            r0 = r21
            int r0 = r0.length     // Catch:{ all -> 0x0ec8 }
            r26 = r0
            r27 = 1
            int r26 = r26 - r27
            r0 = r25
            r1 = r26
            if (r0 == r1) goto L_0x10ec
        L_0x10b7:
            android.graphics.Canvas r25 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r26 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            r0 = r26
            r1 = r5
            java.lang.Object r5 = r0.get(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r24
            int r0 = r0.x     // Catch:{ all -> 0x0ec8 }
            r26 = r0
            r0 = r26
            float r0 = (float) r0     // Catch:{ all -> 0x0ec8 }
            r26 = r0
            r0 = r24
            int r0 = r0.y     // Catch:{ all -> 0x0ec8 }
            r24 = r0
            r0 = r24
            float r0 = (float) r0     // Catch:{ all -> 0x0ec8 }
            r24 = r0
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r27 = r0
            r0 = r25
            r1 = r5
            r2 = r26
            r3 = r24
            r4 = r27
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
        L_0x10ec:
            int r5 = r23 + 1
            r23 = r5
            goto L_0x048e
        L_0x10f2:
            r5 = 0
            goto L_0x0733
        L_0x10f5:
            r5 = 0
            goto L_0x076b
        L_0x10f8:
            r6 = 0
            goto L_0x07f5
        L_0x10fb:
            java.util.Vector r5 = r15.fZ     // Catch:{ all -> 0x0ec8 }
            java.lang.Object r5 = r5.elementAt(r6)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            java.lang.String r10 = r10.vO     // Catch:{ all -> 0x0ec8 }
            boolean r5 = r5.equals(r10)     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x1134
            r0 = r29
            com.epoint.android.games.mjfgbfree.b.a r0 = r0.uI     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r0 = r29
            com.epoint.android.games.mjfgbfree.b.a r0 = r0.uI     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6.Q     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r5.a(r6, r10)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.String r6 = "補花"
            java.lang.String r6 = com.epoint.android.games.mjfgbfree.af.aa(r6)     // Catch:{ all -> 0x0ec8 }
            r5.v(r6)     // Catch:{ all -> 0x0ec8 }
            goto L_0x090a
        L_0x1134:
            int r5 = r6 + 1
            r6 = r5
            goto L_0x0902
        L_0x1139:
            boolean r5 = r9.lQ     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x114d
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.String r6 = "搶槓"
            java.lang.String r6 = com.epoint.android.games.mjfgbfree.af.aa(r6)     // Catch:{ all -> 0x0ec8 }
            r5.v(r6)     // Catch:{ all -> 0x0ec8 }
            goto L_0x0971
        L_0x114d:
            int r5 = r9.lK     // Catch:{ all -> 0x0ec8 }
            r6 = -1
            if (r5 == r6) goto L_0x1162
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.String r6 = "食糊exp"
            java.lang.String r6 = com.epoint.android.games.mjfgbfree.af.aa(r6)     // Catch:{ all -> 0x0ec8 }
            r5.v(r6)     // Catch:{ all -> 0x0ec8 }
            goto L_0x0971
        L_0x1162:
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.String r6 = "自摸exp"
            java.lang.String r6 = com.epoint.android.games.mjfgbfree.af.aa(r6)     // Catch:{ all -> 0x0ec8 }
            r5.v(r6)     // Catch:{ all -> 0x0ec8 }
            goto L_0x0971
        L_0x1172:
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            android.graphics.Rect r5 = r5.getBounds()     // Catch:{ all -> 0x0ec8 }
            r0 = r5
            r1 = r29
            r1.ut = r0     // Catch:{ all -> 0x0ec8 }
            goto L_0x0985
        L_0x1182:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.li     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6.aG     // Catch:{ all -> 0x0ec8 }
            if (r5 != r6) goto L_0x0985
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.lh     // Catch:{ all -> 0x0ec8 }
            switch(r5) {
                case 1: goto L_0x0985;
                case 2: goto L_0x119e;
                case 3: goto L_0x119e;
                case 4: goto L_0x119e;
                case 5: goto L_0x11be;
                case 6: goto L_0x11ae;
                default: goto L_0x119c;
            }     // Catch:{ all -> 0x0ec8 }
        L_0x119c:
            goto L_0x0985
        L_0x119e:
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.String r6 = "槓"
            java.lang.String r6 = com.epoint.android.games.mjfgbfree.af.aa(r6)     // Catch:{ all -> 0x0ec8 }
            r5.v(r6)     // Catch:{ all -> 0x0ec8 }
            goto L_0x0985
        L_0x11ae:
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.String r6 = "上"
            java.lang.String r6 = com.epoint.android.games.mjfgbfree.af.aa(r6)     // Catch:{ all -> 0x0ec8 }
            r5.v(r6)     // Catch:{ all -> 0x0ec8 }
            goto L_0x0985
        L_0x11be:
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.uf     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.String r6 = "碰"
            java.lang.String r6 = com.epoint.android.games.mjfgbfree.af.aa(r6)     // Catch:{ all -> 0x0ec8 }
            r5.v(r6)     // Catch:{ all -> 0x0ec8 }
            goto L_0x0985
        L_0x11ce:
            r0 = r29
            java.util.Vector r0 = r0.tJ     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.Object r5 = r5.elementAt(r11)     // Catch:{ all -> 0x0ec8 }
            com.epoint.android.games.mjfgbfree.ui.c r5 = (com.epoint.android.games.mjfgbfree.ui.c) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.c r0 = r0.tY     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            if (r6 != r5) goto L_0x1201
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            java.util.Vector r6 = r6.wf     // Catch:{ all -> 0x0ec8 }
            int r6 = r6.size()     // Catch:{ all -> 0x0ec8 }
            if (r6 != 0) goto L_0x11fb
            boolean r6 = r15.gb     // Catch:{ all -> 0x0ec8 }
            if (r6 == 0) goto L_0x1305
            java.util.Vector r6 = r15.fV     // Catch:{ all -> 0x0ec8 }
            int r6 = r6.size()     // Catch:{ all -> 0x0ec8 }
            r12 = 1
            int r6 = r6 - r12
            if (r11 == r6) goto L_0x1305
        L_0x11fb:
            r6 = 0
            r0 = r6
            r1 = r29
            r1.tY = r0     // Catch:{ all -> 0x0ec8 }
        L_0x1201:
            java.lang.String r6 = "-"
            java.lang.String r12 = r5.lw     // Catch:{ all -> 0x0ec8 }
            boolean r6 = r6.equals(r12)     // Catch:{ all -> 0x0ec8 }
            if (r6 != 0) goto L_0x1216
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            boolean r6 = r6.le     // Catch:{ all -> 0x0ec8 }
            if (r6 != 0) goto L_0x1216
            if (r9 == 0) goto L_0x1320
        L_0x1216:
            java.util.Vector r6 = r15.fV     // Catch:{ all -> 0x0ec8 }
            int r6 = r6.size()     // Catch:{ all -> 0x0ec8 }
            r12 = 1
            int r6 = r6 - r12
            if (r11 != r6) goto L_0x123a
            if (r9 == 0) goto L_0x123a
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            java.util.Vector r6 = r6.wf     // Catch:{ all -> 0x0ec8 }
            int r6 = r6.size()     // Catch:{ all -> 0x0ec8 }
            r12 = 1
            if (r6 <= r12) goto L_0x123a
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            r12 = 160(0xa0, float:2.24E-43)
            r6.setAlpha(r12)     // Catch:{ all -> 0x0ec8 }
        L_0x123a:
            java.lang.String r6 = "-"
            java.lang.String r12 = r5.lw     // Catch:{ all -> 0x0ec8 }
            boolean r6 = r6.equals(r12)     // Catch:{ all -> 0x0ec8 }
            if (r6 != 0) goto L_0x130a
            android.graphics.Canvas r12 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r6 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r16 = "BC_INV"
            r0 = r6
            r1 = r16
            java.lang.Object r6 = r0.get(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r6 = (android.graphics.Bitmap) r6     // Catch:{ all -> 0x0ec8 }
            r0 = r5
            android.graphics.Rect r0 = r0.nZ     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r17 = r0
            r0 = r12
            r1 = r6
            r2 = r10
            r3 = r16
            r4 = r17
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r6 = r5.lw     // Catch:{ all -> 0x0ec8 }
            android.graphics.Point r6 = com.epoint.android.games.mjfgbfree.af.Y(r6)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r12 = new android.graphics.Rect     // Catch:{ all -> 0x0ec8 }
            r12.<init>()     // Catch:{ all -> 0x0ec8 }
            r0 = r6
            int r0 = r0.y     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            int r16 = r16 * r19
            int r16 = r16 + 1
            r0 = r16
            r1 = r12
            r1.top = r0     // Catch:{ all -> 0x0ec8 }
            int r6 = r6.x     // Catch:{ all -> 0x0ec8 }
            int r6 = r6 * r18
            r12.left = r6     // Catch:{ all -> 0x0ec8 }
            int r6 = r12.left     // Catch:{ all -> 0x0ec8 }
            int r6 = r6 + r18
            r12.right = r6     // Catch:{ all -> 0x0ec8 }
            int r6 = r12.top     // Catch:{ all -> 0x0ec8 }
            int r6 = r6 + r19
            r16 = 1
            int r6 = r6 - r16
            r12.bottom = r6     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r6 = new android.graphics.Rect     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r5 = r5.nZ     // Catch:{ all -> 0x0ec8 }
            r6.<init>(r5)     // Catch:{ all -> 0x0ec8 }
            int r5 = r6.top     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + 2
            r6.top = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r6.left     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + 2
            r6.left = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r6.right     // Catch:{ all -> 0x0ec8 }
            r16 = 3
            int r5 = r5 - r16
            r6.right = r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r6.bottom     // Catch:{ all -> 0x0ec8 }
            r16 = 14
            int r5 = r5 - r16
            r6.bottom = r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r16 = 1
            r0 = r5
            r1 = r16
            r0.setDither(r1)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r16 = 1
            r0 = r5
            r1 = r16
            r0.setFilterBitmap(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Canvas r16 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r17 = "tiles_hd"
            r0 = r5
            r1 = r17
            java.lang.Object r5 = r0.get(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r17 = r0
            r0 = r16
            r1 = r5
            r2 = r12
            r3 = r6
            r4 = r17
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r6 = 0
            r5.setDither(r6)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r6 = 0
            r5.setFilterBitmap(r6)     // Catch:{ all -> 0x0ec8 }
        L_0x1305:
            int r5 = r11 + 1
            r11 = r5
            goto L_0x0a0b
        L_0x130a:
            android.graphics.Canvas r6 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r12 = r5.oa     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r5 = r5.nZ     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            r0 = r6
            r1 = r12
            r2 = r10
            r3 = r5
            r4 = r16
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            goto L_0x1305
        L_0x1320:
            android.graphics.Canvas r12 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r6 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r16 = "bc"
            r0 = r6
            r1 = r16
            java.lang.Object r6 = r0.get(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r6 = (android.graphics.Bitmap) r6     // Catch:{ all -> 0x0ec8 }
            r0 = r5
            android.graphics.Rect r0 = r0.nZ     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r17 = r0
            r0 = r12
            r1 = r6
            r2 = r10
            r3 = r16
            r4 = r17
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r6 = r5.nZ     // Catch:{ all -> 0x0ec8 }
            int r6 = r6.left     // Catch:{ all -> 0x0ec8 }
            int r12 = r13.left     // Catch:{ all -> 0x0ec8 }
            int r6 = r6 + r12
            int r6 = r6 + 1
            r7.left = r6     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r6 = r5.nZ     // Catch:{ all -> 0x0ec8 }
            int r6 = r6.top     // Catch:{ all -> 0x0ec8 }
            int r12 = r13.top     // Catch:{ all -> 0x0ec8 }
            int r6 = r6 + r12
            int r6 = r6 + 11
            r7.top = r6     // Catch:{ all -> 0x0ec8 }
            int r6 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            int r12 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra     // Catch:{ all -> 0x0ec8 }
            if (r6 > r12) goto L_0x136a
            java.util.Vector r6 = r15.fV     // Catch:{ all -> 0x0ec8 }
            int r6 = r6.size()     // Catch:{ all -> 0x0ec8 }
            r12 = 16
            if (r6 >= r12) goto L_0x14b5
        L_0x136a:
            r6 = 0
            int r12 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            r16 = 800(0x320, float:1.121E-42)
            r0 = r12
            r1 = r16
            if (r0 < r1) goto L_0x1381
            java.util.Vector r6 = r15.fV     // Catch:{ all -> 0x0ec8 }
            int r6 = r6.size()     // Catch:{ all -> 0x0ec8 }
            r12 = 16
            if (r6 < r12) goto L_0x14b0
            r6 = 1028443341(0x3d4ccccd, float:0.05)
        L_0x1381:
            int r12 = r7.left     // Catch:{ all -> 0x0ec8 }
            r0 = r13
            int r0 = r0.right     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            r0 = r13
            int r0 = r0.left     // Catch:{ all -> 0x0ec8 }
            r17 = r0
            int r16 = r16 - r17
            r0 = r16
            double r0 = (double) r0     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            r21 = 4608983858650965606(0x3ff6666666666666, double:1.4)
            r0 = r6
            double r0 = (double) r0     // Catch:{ all -> 0x0ec8 }
            r23 = r0
            double r21 = r21 + r23
            double r16 = r16 * r21
            r0 = r16
            int r0 = (int) r0     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            int r12 = r12 + r16
            r7.right = r12     // Catch:{ all -> 0x0ec8 }
            int r12 = r7.top     // Catch:{ all -> 0x0ec8 }
            r0 = r13
            int r0 = r0.bottom     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            r0 = r13
            int r0 = r0.top     // Catch:{ all -> 0x0ec8 }
            r17 = r0
            int r16 = r16 - r17
            r0 = r16
            double r0 = (double) r0     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            r21 = 4608983858650965606(0x3ff6666666666666, double:1.4)
            r0 = r6
            double r0 = (double) r0     // Catch:{ all -> 0x0ec8 }
            r23 = r0
            double r21 = r21 + r23
            double r16 = r16 * r21
            r0 = r16
            int r0 = (int) r0     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6 + r12
            r7.bottom = r6     // Catch:{ all -> 0x0ec8 }
        L_0x13d1:
            java.lang.String r6 = r5.lw     // Catch:{ all -> 0x0ec8 }
            android.graphics.Point r6 = com.epoint.android.games.mjfgbfree.af.Y(r6)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r12 = new android.graphics.Rect     // Catch:{ all -> 0x0ec8 }
            r12.<init>()     // Catch:{ all -> 0x0ec8 }
            r0 = r6
            int r0 = r0.y     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            int r16 = r16 * r19
            int r16 = r16 + 1
            r0 = r16
            r1 = r12
            r1.top = r0     // Catch:{ all -> 0x0ec8 }
            int r6 = r6.x     // Catch:{ all -> 0x0ec8 }
            int r6 = r6 * r18
            r12.left = r6     // Catch:{ all -> 0x0ec8 }
            int r6 = r12.left     // Catch:{ all -> 0x0ec8 }
            int r6 = r6 + r18
            r12.right = r6     // Catch:{ all -> 0x0ec8 }
            int r6 = r12.top     // Catch:{ all -> 0x0ec8 }
            int r6 = r6 + r19
            r16 = 1
            int r6 = r6 - r16
            r12.bottom = r6     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            r16 = 1
            r0 = r6
            r1 = r16
            r0.setDither(r1)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            r16 = 1
            r0 = r6
            r1 = r16
            r0.setFilterBitmap(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Canvas r16 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r6 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r17 = "tiles_hd"
            r0 = r6
            r1 = r17
            java.lang.Object r6 = r0.get(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r6 = (android.graphics.Bitmap) r6     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r17 = r0
            r0 = r16
            r1 = r6
            r2 = r12
            r3 = r7
            r4 = r17
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            r12 = 0
            r6.setDither(r12)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            r12 = 0
            r6.setFilterBitmap(r12)     // Catch:{ all -> 0x0ec8 }
            boolean r6 = r15.gb     // Catch:{ all -> 0x0ec8 }
            if (r6 == 0) goto L_0x14fd
            r0 = r29
            java.util.Vector r0 = r0.tJ     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            java.lang.Object r6 = r6.lastElement()     // Catch:{ all -> 0x0ec8 }
            if (r6 == r5) goto L_0x14fd
            java.util.Vector r6 = r15.fV     // Catch:{ all -> 0x0ec8 }
            int r6 = r6.size()     // Catch:{ all -> 0x0ec8 }
            r12 = 14
            if (r6 != r12) goto L_0x14fd
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6.getColor()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            android.graphics.Paint$Style r16 = android.graphics.Paint.Style.FILL     // Catch:{ all -> 0x0ec8 }
            r0 = r12
            r1 = r16
            r0.setStyle(r1)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            r16 = -12303292(0xffffffffff444444, float:-2.6088314E38)
            r0 = r12
            r1 = r16
            r0.setColor(r1)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            r16 = 80
            r0 = r12
            r1 = r16
            r0.setAlpha(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Canvas r12 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r5 = r5.nZ     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            r0 = r12
            r1 = r5
            r2 = r16
            r0.drawRect(r1, r2)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r5.setColor(r6)     // Catch:{ all -> 0x0ec8 }
            goto L_0x1305
        L_0x14b0:
            r6 = 1040522936(0x3e051eb8, float:0.13)
            goto L_0x1381
        L_0x14b5:
            int r6 = r7.top     // Catch:{ all -> 0x0ec8 }
            r12 = 3
            int r6 = r6 - r12
            r7.top = r6     // Catch:{ all -> 0x0ec8 }
            int r6 = r7.left     // Catch:{ all -> 0x0ec8 }
            int r12 = r13.right     // Catch:{ all -> 0x0ec8 }
            r0 = r13
            int r0 = r0.left     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            int r12 = r12 - r16
            r0 = r12
            double r0 = (double) r0     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            r21 = 4607632778762754458(0x3ff199999999999a, double:1.1)
            double r16 = r16 * r21
            r0 = r16
            int r0 = (int) r0     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            int r6 = r6 + r12
            r7.right = r6     // Catch:{ all -> 0x0ec8 }
            int r6 = r7.top     // Catch:{ all -> 0x0ec8 }
            int r12 = r13.bottom     // Catch:{ all -> 0x0ec8 }
            r0 = r13
            int r0 = r0.top     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            int r12 = r12 - r16
            r0 = r12
            double r0 = (double) r0     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            r21 = 4607632778762754458(0x3ff199999999999a, double:1.1)
            double r16 = r16 * r21
            r0 = r16
            int r0 = (int) r0     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            int r6 = r6 + r12
            r7.bottom = r6     // Catch:{ all -> 0x0ec8 }
            int r6 = r7.bottom     // Catch:{ all -> 0x0ec8 }
            int r6 = r6 + 1
            r7.bottom = r6     // Catch:{ all -> 0x0ec8 }
            goto L_0x13d1
        L_0x14fd:
            r0 = r29
            com.epoint.android.games.mjfgbfree.bl r0 = r0.uA     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            java.util.Vector r6 = r6.Av     // Catch:{ all -> 0x0ec8 }
            java.lang.String r12 = r5.lw     // Catch:{ all -> 0x0ec8 }
            boolean r6 = r6.contains(r12)     // Catch:{ all -> 0x0ec8 }
            if (r6 == 0) goto L_0x1305
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r6 = r0
            int r6 = r6.getColor()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            android.graphics.Paint$Style r16 = android.graphics.Paint.Style.FILL     // Catch:{ all -> 0x0ec8 }
            r0 = r12
            r1 = r16
            r0.setStyle(r1)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            r16 = -65536(0xffffffffffff0000, float:NaN)
            r0 = r12
            r1 = r16
            r0.setColor(r1)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            r16 = 48
            r0 = r12
            r1 = r16
            r0.setAlpha(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Canvas r12 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r5 = r5.nZ     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            r0 = r12
            r1 = r5
            r2 = r16
            r0.drawRect(r1, r2)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r5.setColor(r6)     // Catch:{ all -> 0x0ec8 }
            goto L_0x1305
        L_0x1557:
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.c r0 = r0.tY     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r0 = r29
            java.util.Vector r0 = r0.tJ     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            java.util.Vector r12 = r15.fV     // Catch:{ all -> 0x0ec8 }
            int r12 = r12.size()     // Catch:{ all -> 0x0ec8 }
            r13 = 1
            int r12 = r12 - r13
            java.lang.Object r11 = r11.elementAt(r12)     // Catch:{ all -> 0x0ec8 }
            if (r10 != r11) goto L_0x0add
            int r5 = r7.left     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tL     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            int r10 = r10.bb()     // Catch:{ all -> 0x0ec8 }
            int r11 = r7.width()     // Catch:{ all -> 0x0ec8 }
            int r10 = r10 - r11
            int r5 = r5 - r10
            r10 = 1
            int r5 = r5 - r10
            goto L_0x0add
        L_0x1584:
            r0 = r29
            com.epoint.android.games.mjfgbfree.bl r0 = r0.uA     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.util.Vector r11 = r5.Aw     // Catch:{ all -> 0x0ec8 }
            java.lang.Object r5 = r9.elementAt(r10)     // Catch:{ all -> 0x0ec8 }
            a.a.a r5 = (a.a.a) r5     // Catch:{ all -> 0x0ec8 }
            java.lang.String r12 = ""
            java.lang.String r5 = r5.t(r12)     // Catch:{ all -> 0x0ec8 }
            r11.addElement(r5)     // Catch:{ all -> 0x0ec8 }
            int r5 = r10 + 1
            r10 = r5
            goto L_0x0c5c
        L_0x159f:
            java.util.Vector r5 = r15.fX     // Catch:{ all -> 0x0ec8 }
            java.lang.Object r5 = r5.elementAt(r11)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.length()     // Catch:{ all -> 0x0ec8 }
            r16 = 1
            r0 = r5
            r1 = r16
            if (r0 != r1) goto L_0x15c1
            r5 = 0
            int r10 = r13 + 1
            r28 = r12
            r12 = r10
            r10 = r28
        L_0x15ba:
            int r11 = r11 + 1
            r13 = r12
            r12 = r10
            r10 = r5
            goto L_0x0c79
        L_0x15c1:
            r5 = -1
            if (r12 != r5) goto L_0x1b3d
            int r5 = r10 + 1
            r10 = 4
            if (r5 != r10) goto L_0x1b39
            r10 = r13
            r12 = r13
            goto L_0x15ba
        L_0x15cc:
            r9 = 33
            goto L_0x0caa
        L_0x15d0:
            r9 = 0
            goto L_0x0cb4
        L_0x15d3:
            android.graphics.Canvas r12 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.util.Vector r13 = r15.fZ     // Catch:{ all -> 0x0ec8 }
            java.lang.Object r13 = r13.elementAt(r10)     // Catch:{ all -> 0x0ec8 }
            java.lang.Object r5 = r5.get(r13)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            float r13 = (float) r11     // Catch:{ all -> 0x0ec8 }
            r0 = r9
            float r0 = (float) r0     // Catch:{ all -> 0x0ec8 }
            r16 = r0
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r17 = r0
            r0 = r12
            r1 = r5
            r2 = r13
            r3 = r16
            r4 = r17
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            java.util.Vector r5 = r15.fZ     // Catch:{ all -> 0x0ec8 }
            java.lang.Object r5 = r5.elementAt(r10)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            java.lang.String r12 = r12.vO     // Catch:{ all -> 0x0ec8 }
            boolean r5 = r5.equals(r12)     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x167b
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r12 = r5.getColor()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            android.graphics.Paint$Style r13 = android.graphics.Paint.Style.STROKE     // Catch:{ all -> 0x0ec8 }
            r5.setStyle(r13)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r13 = -65536(0xffffffffffff0000, float:NaN)
            r5.setColor(r13)     // Catch:{ all -> 0x0ec8 }
            r7.top = r9     // Catch:{ all -> 0x0ec8 }
            r7.left = r11     // Catch:{ all -> 0x0ec8 }
            int r13 = r7.left     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r16 = "bc"
            r0 = r5
            r1 = r16
            java.lang.Object r5 = r0.get(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + r13
            r13 = 2
            int r5 = r5 - r13
            r7.right = r5     // Catch:{ all -> 0x0ec8 }
            int r13 = r7.top     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r16 = "bc"
            r0 = r5
            r1 = r16
            java.lang.Object r5 = r0.get(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 + r13
            r13 = 8
            int r5 = r5 - r13
            r7.bottom = r5     // Catch:{ all -> 0x0ec8 }
            android.graphics.Canvas r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r13 = r0
            r5.drawRect(r7, r13)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            android.graphics.Paint$Style r13 = android.graphics.Paint.Style.FILL     // Catch:{ all -> 0x0ec8 }
            r5.setStyle(r13)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r5.setColor(r12)     // Catch:{ all -> 0x0ec8 }
        L_0x167b:
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r12 = "bc"
            java.lang.Object r5 = r5.get(r12)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            r12 = 1
            int r5 = r5 - r12
            int r5 = r5 + r11
            int r10 = r10 + 1
            r11 = r5
            goto L_0x0cb9
        L_0x1691:
            java.util.Vector r5 = r15.fX     // Catch:{ all -> 0x0ec8 }
            java.lang.Object r5 = r5.elementAt(r13)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.length()     // Catch:{ all -> 0x0ec8 }
            r17 = 1
            r0 = r5
            r1 = r17
            if (r0 != r1) goto L_0x1b32
            int r5 = r16 + 1
        L_0x16a6:
            int r13 = r13 + 1
            r16 = r5
            goto L_0x0cd3
        L_0x16ac:
            r19 = 0
            java.util.Vector r5 = r15.fX     // Catch:{ all -> 0x0ec8 }
            java.lang.Object r5 = r5.elementAt(r10)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x0ec8 }
            int r21 = r5.length()     // Catch:{ all -> 0x0ec8 }
            r22 = 3
            r0 = r21
            r1 = r22
            if (r0 != r1) goto L_0x1b2d
            r11 = 1
            r19 = 0
            r21 = 2
            r0 = r5
            r1 = r19
            r2 = r21
            java.lang.String r5 = r0.substring(r1, r2)     // Catch:{ all -> 0x0ec8 }
            int r19 = r7 + 1
            r21 = r19
            r19 = r11
            r11 = r5
        L_0x16d7:
            int r5 = r11.length()     // Catch:{ all -> 0x0ec8 }
            r22 = 1
            r0 = r5
            r1 = r22
            if (r0 != r1) goto L_0x172f
            r7 = 0
            r11 = 0
            int r5 = r18 + 3
            int r17 = r17 + 1
            r18 = 2
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x16f8
            r18 = 5
            r0 = r16
            r1 = r18
            if (r0 < r1) goto L_0x1708
        L_0x16f8:
            r18 = 3
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x1b21
            r18 = 5
            r0 = r16
            r1 = r18
            if (r0 != r1) goto L_0x1b21
        L_0x1708:
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r13 = "bc"
            java.lang.Object r5 = r5.get(r13)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            r13 = 5
            int r5 = r5 - r13
            int r5 = r5 + r12
            r12 = 1
            r13 = r17
            r17 = r8
            r28 = r11
            r11 = r5
            r5 = r28
        L_0x1723:
            int r10 = r10 + 1
            r18 = r17
            r17 = r13
            r13 = r12
            r12 = r11
            r11 = r7
            r7 = r5
            goto L_0x0ce6
        L_0x172f:
            int r7 = r7 + 1
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.Object r5 = r5.get(r11)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            if (r19 != 0) goto L_0x174a
            r19 = 2
            r0 = r21
            r1 = r19
            if (r0 != r1) goto L_0x1b1b
            r19 = 4
            r0 = r7
            r1 = r19
            if (r0 != r1) goto L_0x1b1b
        L_0x174a:
            r19 = 90
            r0 = r29
            r1 = r5
            r2 = r19
            android.graphics.Bitmap r19 = r0.b(r1, r2)     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r22 = r19.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 - r22
            int r5 = r5 + r12
            r22 = r5
        L_0x1762:
            r5 = 4
            if (r7 != r5) goto L_0x1808
            int r22 = r22 + -8
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r23 = "bc"
            r0 = r5
            r1 = r23
            java.lang.Object r5 = r0.get(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            r23 = 1
            int r5 = r5 - r23
            int r5 = r5 * 2
            int r5 = r18 - r5
            r23 = r18
            r18 = r22
            r22 = r5
        L_0x1786:
            android.graphics.Canvas r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r0 = r22
            float r0 = (float) r0     // Catch:{ all -> 0x0ec8 }
            r24 = r0
            r0 = r18
            float r0 = (float) r0     // Catch:{ all -> 0x0ec8 }
            r25 = r0
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r26 = r0
            r0 = r5
            r1 = r19
            r2 = r24
            r3 = r25
            r4 = r26
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = "BK"
            boolean r5 = r11.equals(r5)     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x17fd
            java.util.Vector r5 = r15.fY     // Catch:{ all -> 0x0ec8 }
            java.lang.Object r5 = r5.elementAt(r10)     // Catch:{ all -> 0x0ec8 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            int r11 = r11.getAlpha()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r19 = r0
            r24 = 96
            r0 = r19
            r1 = r24
            r0.setAlpha(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Canvas r19 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            java.util.Hashtable r24 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            r0 = r24
            r1 = r5
            java.lang.Object r5 = r0.get(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r22
            float r0 = (float) r0     // Catch:{ all -> 0x0ec8 }
            r22 = r0
            r0 = r18
            float r0 = (float) r0     // Catch:{ all -> 0x0ec8 }
            r18 = r0
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r24 = r0
            r0 = r19
            r1 = r5
            r2 = r22
            r3 = r18
            r4 = r24
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Paint r0 = r0.gN     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r5.setAlpha(r11)     // Catch:{ all -> 0x0ec8 }
        L_0x17fd:
            r5 = r7
            r11 = r12
            r7 = r21
            r12 = r13
            r13 = r17
            r17 = r23
            goto L_0x1723
        L_0x1808:
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r23 = "bc"
            r0 = r5
            r1 = r23
            java.lang.Object r5 = r0.get(r1)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x0ec8 }
            r23 = 1
            int r5 = r5 - r23
            int r5 = r5 + r18
            r23 = r5
            r28 = r18
            r18 = r22
            r22 = r28
            goto L_0x1786
        L_0x1829:
            r11 = r8
            goto L_0x0d65
        L_0x182c:
            r0 = r29
            java.util.Vector r0 = r0.gM     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.Object r5 = r5.elementAt(r7)     // Catch:{ all -> 0x0ec8 }
            com.epoint.android.games.mjfgbfree.ui.a r5 = (com.epoint.android.games.mjfgbfree.ui.a) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tL     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            if (r5 == r10) goto L_0x1b15
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tW     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            if (r5 == r10) goto L_0x1b15
            boolean r5 = r5.isVisible()     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x1b15
            int r5 = r9 + 1
        L_0x184d:
            int r7 = r7 + 1
            r9 = r5
            goto L_0x0d85
        L_0x1852:
            r5 = 85
            r7 = r5
            goto L_0x0d96
        L_0x1857:
            r0 = r29
            java.util.Vector r0 = r0.gM     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.Object r5 = r5.elementAt(r10)     // Catch:{ all -> 0x0ec8 }
            com.epoint.android.games.mjfgbfree.ui.a r5 = (com.epoint.android.games.mjfgbfree.ui.a) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tL     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            if (r5 == r11) goto L_0x1887
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tW     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            if (r5 == r11) goto L_0x1887
            boolean r11 = r5.isVisible()     // Catch:{ all -> 0x0ec8 }
            if (r11 == 0) goto L_0x1887
            int r11 = r5.bb()     // Catch:{ all -> 0x0ec8 }
            r12 = 100
            if (r11 >= r12) goto L_0x1887
            android.graphics.Rect r5 = r5.getBounds()     // Catch:{ all -> 0x0ec8 }
            int r11 = r5.left     // Catch:{ all -> 0x0ec8 }
            int r11 = r11 + r7
            r5.right = r11     // Catch:{ all -> 0x0ec8 }
        L_0x1887:
            int r5 = r10 + 1
            r10 = r5
            goto L_0x0d98
        L_0x188c:
            r0 = r29
            java.util.Vector r0 = r0.gM     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            java.lang.Object r5 = r5.elementAt(r7)     // Catch:{ all -> 0x0ec8 }
            com.epoint.android.games.mjfgbfree.ui.a r5 = (com.epoint.android.games.mjfgbfree.ui.a) r5     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tL     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            if (r5 == r11) goto L_0x18e4
            boolean r11 = r5.isVisible()     // Catch:{ all -> 0x0ec8 }
            if (r11 == 0) goto L_0x18e4
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.tW     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            if (r5 == r11) goto L_0x18df
            r11 = 5
            if (r9 >= r11) goto L_0x18ec
            android.graphics.Bitmap r11 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r11 = r11.getWidth()     // Catch:{ all -> 0x0ec8 }
            int r12 = r5.bb()     // Catch:{ all -> 0x0ec8 }
            int r12 = r12 + 5
            int r12 = r12 * r10
            int r12 = r12 + r8
            int r11 = r11 - r12
            int r12 = r5.bb()     // Catch:{ all -> 0x0ec8 }
            int r11 = r11 - r12
            android.graphics.Bitmap r12 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r12 = r12.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r13 = r5.ba()     // Catch:{ all -> 0x0ec8 }
            int r12 = r12 - r13
            r13 = 21
            int r12 = r12 - r13
            int r13 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x0ec8 }
            r15 = 1
            if (r13 != r15) goto L_0x18ea
            int r13 = dR()     // Catch:{ all -> 0x0ec8 }
        L_0x18d9:
            int r12 = r12 - r13
            r5.c(r11, r12)     // Catch:{ all -> 0x0ec8 }
        L_0x18dd:
            int r10 = r10 + 1
        L_0x18df:
            android.graphics.Canvas r11 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r5.draw(r11)     // Catch:{ all -> 0x0ec8 }
        L_0x18e4:
            r5 = r10
            int r7 = r7 + 1
            r10 = r5
            goto L_0x0da6
        L_0x18ea:
            r13 = 0
            goto L_0x18d9
        L_0x18ec:
            android.graphics.Bitmap r11 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r11 = r11.getWidth()     // Catch:{ all -> 0x0ec8 }
            int r12 = r5.bb()     // Catch:{ all -> 0x0ec8 }
            int r12 = r12 + 5
            int r12 = r12 * r9
            int r11 = r11 - r12
            int r11 = r11 / 2
            android.graphics.Bitmap r12 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r12 = r12.getWidth()     // Catch:{ all -> 0x0ec8 }
            int r11 = r12 - r11
            int r12 = r5.bb()     // Catch:{ all -> 0x0ec8 }
            int r12 = r12 + 5
            int r13 = r10 + 1
            int r12 = r12 * r13
            int r11 = r11 - r12
            android.graphics.Bitmap r12 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r12 = r12.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r13 = r5.ba()     // Catch:{ all -> 0x0ec8 }
            int r12 = r12 - r13
            r13 = 21
            int r12 = r12 - r13
            int r13 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x0ec8 }
            r15 = 1
            if (r13 != r15) goto L_0x192a
            int r13 = dR()     // Catch:{ all -> 0x0ec8 }
        L_0x1925:
            int r12 = r12 - r13
            r5.c(r11, r12)     // Catch:{ all -> 0x0ec8 }
            goto L_0x18dd
        L_0x192a:
            r13 = 0
            goto L_0x1925
        L_0x192c:
            r0 = r29
            r1 = r7
            com.epoint.android.games.mjfgbfree.d.c r5 = r0.G(r1)     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x1a1c
            r5 = 1
            r8 = r5
        L_0x1937:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.aG     // Catch:{ all -> 0x0ec8 }
            if (r5 == r7) goto L_0x195a
            if (r8 == 0) goto L_0x195a
            r0 = r29
            boolean r0 = r0.uy     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            if (r5 == 0) goto L_0x195a
            r0 = r29
            com.epoint.android.games.mjfgbfree.av r0 = r0.uv     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            android.graphics.Canvas r9 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            java.lang.String[] r11 = com.epoint.android.games.mjfgbfree.bb.tH     // Catch:{ all -> 0x0ec8 }
            r5.a(r7, r9, r10, r11)     // Catch:{ all -> 0x0ec8 }
        L_0x195a:
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r5 = r5[r7]     // Catch:{ all -> 0x0ec8 }
            boolean r5 = r5.isVisible()     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x1a17
            java.lang.String r5 = "S"
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r9 = r0
            r9 = r9[r7]     // Catch:{ all -> 0x0ec8 }
            java.lang.String r9 = r9.gx     // Catch:{ all -> 0x0ec8 }
            boolean r5 = r5.equals(r9)     // Catch:{ all -> 0x0ec8 }
            if (r5 == 0) goto L_0x1a20
            java.util.Hashtable r5 = com.epoint.android.games.mjfgbfree.bb.tE     // Catch:{ all -> 0x0ec8 }
            java.lang.String r9 = "bc"
            java.lang.Object r5 = r5.get(r9)     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.getHeight()     // Catch:{ all -> 0x0ec8 }
        L_0x1986:
            int r9 = com.epoint.android.games.mjfgbfree.MJ16Activity.ra     // Catch:{ all -> 0x0ec8 }
            int r10 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            if (r9 <= r10) goto L_0x1a23
            r9 = 1
        L_0x198d:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            int r10 = r10.aG     // Catch:{ all -> 0x0ec8 }
            int r10 = r10 + 1
            int r10 = r10 % 4
            if (r7 != r10) goto L_0x1a2a
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r10 = r10[r7]     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r11 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r11 = r11.getWidth()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Canvas r0 = r0.un     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            int r12 = r12.getHeight()     // Catch:{ all -> 0x0ec8 }
            int r11 = r11 - r12
            boolean r12 = com.epoint.android.games.mjfgbfree.ai.np     // Catch:{ all -> 0x0ec8 }
            if (r12 == 0) goto L_0x1a26
            int r5 = r5 + 5
        L_0x19b7:
            int r5 = r11 - r5
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            r11 = r11[r7]     // Catch:{ all -> 0x0ec8 }
            int r11 = r11.bb()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 - r11
            android.graphics.Bitmap r11 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r11 = r11.getHeight()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            r12 = r12[r7]     // Catch:{ all -> 0x0ec8 }
            int r12 = r12.ba()     // Catch:{ all -> 0x0ec8 }
            int r11 = r11 - r12
            int r11 = r11 / 2
            if (r9 == 0) goto L_0x1a28
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r9 = r0
            r9 = r9[r7]     // Catch:{ all -> 0x0ec8 }
            int r9 = r9.ba()     // Catch:{ all -> 0x0ec8 }
            int r9 = r9 / 2
        L_0x19e8:
            int r9 = r11 - r9
            r10.c(r5, r9)     // Catch:{ all -> 0x0ec8 }
        L_0x19ed:
            r0 = r29
            boolean r0 = r0.uy     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            if (r5 == 0) goto L_0x1a00
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r5 = r5[r7]     // Catch:{ all -> 0x0ec8 }
            android.graphics.Canvas r9 = com.epoint.android.games.mjfgbfree.MJ16Activity.rj     // Catch:{ all -> 0x0ec8 }
            r5.draw(r9)     // Catch:{ all -> 0x0ec8 }
        L_0x1a00:
            if (r8 == 0) goto L_0x1a17
            android.graphics.Rect r5 = new android.graphics.Rect     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r8 = r0
            r8 = r8[r7]     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r8 = r8.getBounds()     // Catch:{ all -> 0x0ec8 }
            r5.<init>(r8)     // Catch:{ all -> 0x0ec8 }
            r0 = r5
            r1 = r29
            r1.ut = r0     // Catch:{ all -> 0x0ec8 }
        L_0x1a17:
            int r5 = r7 + 1
            r7 = r5
            goto L_0x0e07
        L_0x1a1c:
            r5 = 0
            r8 = r5
            goto L_0x1937
        L_0x1a20:
            r5 = -5
            goto L_0x1986
        L_0x1a23:
            r9 = 0
            goto L_0x198d
        L_0x1a26:
            r5 = 0
            goto L_0x19b7
        L_0x1a28:
            r9 = 0
            goto L_0x19e8
        L_0x1a2a:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            int r10 = r10.aG     // Catch:{ all -> 0x0ec8 }
            int r10 = r10 + 2
            int r10 = r10 % 4
            if (r7 != r10) goto L_0x1a74
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r9 = r0
            r9 = r9[r7]     // Catch:{ all -> 0x0ec8 }
            android.graphics.Bitmap r10 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r10 = r10.getWidth()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            r11 = r11[r7]     // Catch:{ all -> 0x0ec8 }
            int r11 = r11.bb()     // Catch:{ all -> 0x0ec8 }
            int r10 = r10 - r11
            int r10 = r10 / 2
            r0 = r29
            android.graphics.Canvas r0 = r0.un     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            int r11 = r11.getHeight()     // Catch:{ all -> 0x0ec8 }
            boolean r12 = com.epoint.android.games.mjfgbfree.ai.np     // Catch:{ all -> 0x0ec8 }
            if (r12 == 0) goto L_0x1a6d
            int r5 = r5 + 10
        L_0x1a61:
            int r5 = r5 + r11
            int r11 = com.epoint.android.games.mjfgbfree.bb.ty     // Catch:{ all -> 0x0ec8 }
            r12 = 1
            if (r11 != r12) goto L_0x1a6f
            r11 = 0
        L_0x1a68:
            int r5 = r5 + r11
            r9.c(r10, r5)     // Catch:{ all -> 0x0ec8 }
            goto L_0x19ed
        L_0x1a6d:
            r5 = 0
            goto L_0x1a61
        L_0x1a6f:
            int r11 = dR()     // Catch:{ all -> 0x0ec8 }
            goto L_0x1a68
        L_0x1a74:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            int r10 = r10.aG     // Catch:{ all -> 0x0ec8 }
            int r10 = r10 + 3
            int r10 = r10 % 4
            if (r7 != r10) goto L_0x1ac5
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r10 = r10[r7]     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            android.graphics.Canvas r0 = r0.un     // Catch:{ all -> 0x0ec8 }
            r11 = r0
            int r11 = r11.getHeight()     // Catch:{ all -> 0x0ec8 }
            boolean r12 = com.epoint.android.games.mjfgbfree.ai.np     // Catch:{ all -> 0x0ec8 }
            if (r12 == 0) goto L_0x1ac1
            int r5 = r5 + 5
        L_0x1a97:
            int r5 = r5 + r11
            android.graphics.Bitmap r11 = com.epoint.android.games.mjfgbfree.MJ16Activity.ri     // Catch:{ all -> 0x0ec8 }
            int r11 = r11.getHeight()     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r12 = r0
            r12 = r12[r7]     // Catch:{ all -> 0x0ec8 }
            int r12 = r12.ba()     // Catch:{ all -> 0x0ec8 }
            int r11 = r11 - r12
            int r11 = r11 / 2
            if (r9 == 0) goto L_0x1ac3
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r9 = r0
            r9 = r9[r7]     // Catch:{ all -> 0x0ec8 }
            int r9 = r9.ba()     // Catch:{ all -> 0x0ec8 }
            int r9 = r9 / 2
        L_0x1abb:
            int r9 = r9 + r11
            r10.c(r5, r9)     // Catch:{ all -> 0x0ec8 }
            goto L_0x19ed
        L_0x1ac1:
            r5 = 0
            goto L_0x1a97
        L_0x1ac3:
            r9 = 0
            goto L_0x1abb
        L_0x1ac5:
            r0 = r29
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            int r5 = r5.aG     // Catch:{ all -> 0x0ec8 }
            if (r7 != r5) goto L_0x19ed
            int r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.qZ     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r9 = r0
            r9 = r9[r7]     // Catch:{ all -> 0x0ec8 }
            int r9 = r9.bb()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 - r9
            int r9 = r5 / 2
            r0 = r29
            java.util.Vector r0 = r0.tJ     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r10 = 0
            java.lang.Object r5 = r5.elementAt(r10)     // Catch:{ all -> 0x0ec8 }
            com.epoint.android.games.mjfgbfree.ui.c r5 = (com.epoint.android.games.mjfgbfree.ui.c) r5     // Catch:{ all -> 0x0ec8 }
            android.graphics.Rect r5 = r5.nZ     // Catch:{ all -> 0x0ec8 }
            int r5 = r5.top     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r10 = r10[r7]     // Catch:{ all -> 0x0ec8 }
            int r10 = r10.ba()     // Catch:{ all -> 0x0ec8 }
            int r5 = r5 - r10
            r10 = 10
            int r5 = r5 - r10
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r10 = r0
            r10 = r10[r7]     // Catch:{ all -> 0x0ec8 }
            r10.c(r9, r5)     // Catch:{ all -> 0x0ec8 }
            r0 = r29
            com.epoint.android.games.mjfgbfree.ui.a[] r0 = r0.ui     // Catch:{ all -> 0x0ec8 }
            r5 = r0
            r5 = r5[r7]     // Catch:{ all -> 0x0ec8 }
            r9 = 0
            r10 = 0
            r5.a(r9, r10)     // Catch:{ all -> 0x0ec8 }
            goto L_0x19ed
        L_0x1b15:
            r5 = r9
            goto L_0x184d
        L_0x1b18:
            r7 = r5
            goto L_0x0d5f
        L_0x1b1b:
            r19 = r5
            r22 = r12
            goto L_0x1762
        L_0x1b21:
            r28 = r11
            r11 = r12
            r12 = r13
            r13 = r17
            r17 = r5
            r5 = r28
            goto L_0x1723
        L_0x1b2d:
            r21 = r11
            r11 = r5
            goto L_0x16d7
        L_0x1b32:
            r5 = r16
            goto L_0x16a6
        L_0x1b36:
            r7 = r11
            goto L_0x0ccc
        L_0x1b39:
            r10 = r12
            r12 = r13
            goto L_0x15ba
        L_0x1b3d:
            r5 = r10
            r10 = r12
            r12 = r13
            goto L_0x15ba
        L_0x1b42:
            r7 = r6
            goto L_0x070d
        L_0x1b45:
            r8 = r5
            goto L_0x0420
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.bb.eg():void");
    }

    public final d ei() {
        this.ue = this.fs;
        return this.fs;
    }

    public final void ej() {
        Intent intent = new Intent(this.jq, DiscardedTilesActivity.class);
        intent.putExtra("gi", com.epoint.android.games.mjfgbfree.a.d.c(this.gO));
        this.jq.startActivity(intent);
    }

    public final synchronized void ep() {
        this.uu = true;
        Message message = new Message();
        message.what = 6;
        this.mHandler.sendMessage(message);
    }

    public final synchronized void eq() {
        this.uu = true;
        Message message = new Message();
        message.what = 8;
        this.mHandler.sendMessage(message);
    }

    public final synchronized void er() {
        this.uu = true;
        Message message = new Message();
        message.what = 0;
        this.mHandler.sendMessage(message);
    }

    public final synchronized void es() {
        this.uu = true;
        Message message = new Message();
        message.what = 3;
        this.mHandler.sendMessage(message);
    }

    public final synchronized void et() {
        this.uu = true;
        Message message = new Message();
        message.what = 4;
        this.mHandler.sendMessage(message);
    }

    public final com.epoint.android.games.mjfgbfree.c.d eu() {
        return this.uB;
    }

    public final void ev() {
        if ((this.gO != null && !this.gO.le) || this.ue == null) {
            this.jq.eM();
        }
    }

    public final com.epoint.android.games.mjfgbfree.ui.a ew() {
        return this.tT;
    }

    public final void ex() {
        em();
    }

    public final void ey() {
        this.gO = this.uB.P();
        ek();
        el();
        ef();
        en();
    }

    public final synchronized void g(Object obj) {
        Message message = new Message();
        message.what = 5;
        message.obj = obj;
        this.mHandler.sendMessage(message);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.epoint.android.games.mjfgbfree.bb.b(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.epoint.android.games.mjfgbfree.bb.b(android.graphics.Bitmap, int):android.graphics.Bitmap
      com.epoint.android.games.mjfgbfree.bb.b(int, int):void
      a.a.b.b(int, int):void
      com.epoint.android.games.mjfgbfree.bb.b(boolean, boolean):void */
    public final void n(boolean z) {
        if (this.ho instanceof com.epoint.android.games.mjfgbfree.e.l) {
            this.uB.cU().s(((com.epoint.android.games.mjfgbfree.e.l) this.ho).cM().cz().gE);
        }
        this.uA.Av.clear();
        this.uy = true;
        uD = -1;
        if (this.tD == 0) {
            dZ();
            return;
        }
        this.tU.a(false, false);
        this.tT.a(false, false);
        if (!((!(this.uB instanceof com.epoint.android.games.mjfgbfree.c.a) || !eb()) ? false : this.uB.cW())) {
            this.uB.O();
        }
        if (z) {
            b(false, false);
        }
        if (this.uB.cX()) {
            ah ahVar = new ah(this);
            uC = ahVar;
            ahVar.start();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.epoint.android.games.mjfgbfree.bb.b(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.epoint.android.games.mjfgbfree.bb.b(android.graphics.Bitmap, int):android.graphics.Bitmap
      com.epoint.android.games.mjfgbfree.bb.b(int, int):void
      a.a.b.b(int, int):void
      com.epoint.android.games.mjfgbfree.bb.b(boolean, boolean):void */
    public final void o(boolean z) {
        b(z, true);
    }

    /* access modifiers changed from: protected */
    public final synchronized void onDraw(Canvas canvas) {
        try {
            if (!this.oS && this.fs != null) {
                this.oS = true;
                RelativeLayout relativeLayout = (RelativeLayout) getParent();
                if (relativeLayout.findViewWithTag(this.uv) == null) {
                    relativeLayout.addView(this.uv);
                }
            }
            if (MJ16ViewActivity.wn == 0) {
                MJ16ViewActivity.wn = uF.getHeight();
                MJ16ViewActivity.wo = uF.getWidth();
            }
            if (MJ16Activity.ri != null) {
                if ((MJ16Activity.qZ > MJ16Activity.ra) == (getWidth() > getHeight())) {
                    if (MJ16Activity.ra == getHeight() && MJ16Activity.qZ == getWidth()) {
                        canvas.drawBitmap(MJ16Activity.ri, 0.0f, 0.0f, (Paint) null);
                    } else {
                        Rect rect = new Rect();
                        Rect rect2 = new Rect();
                        int i = rect2.left;
                        rect2.top = i;
                        rect.left = i;
                        rect.top = i;
                        rect.bottom = MJ16Activity.ri.getHeight();
                        rect.right = MJ16Activity.ri.getWidth();
                        rect2.right = getWidth();
                        rect2.bottom = getHeight();
                        canvas.setDrawFilter(MJ16Activity.rk);
                        canvas.drawBitmap(MJ16Activity.ri, rect, rect2, (Paint) null);
                    }
                }
            }
            this.uu = false;
        } catch (Throwable th) {
            this.uu = false;
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (this.ua == null) {
            this.ua = new com.epoint.android.games.mjfgbfree.ui.a.g();
            this.ua.a(this.jq, this);
            this.ub = new com.epoint.android.games.mjfgbfree.ui.a.b();
            this.ub.a(this.jq, this);
            this.fs = new d();
            this.fs.a(this.jq, this);
            this.uc = new com.epoint.android.games.mjfgbfree.ui.a.c();
            this.uc.a(this.jq, this);
            if (this.ho instanceof com.epoint.android.games.mjfgbfree.e.l) {
                this.ud = new f(Color.rgb(208, 208, 208), -12303292, 207, Color.rgb(208, 208, 208), true);
            } else {
                this.ud = new f();
            }
            this.ud.a(this.jq, this);
            invalidate();
        }
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.uJ) {
            return true;
        }
        if (dU() || this.uv.oR || this.uB.S()) {
            if (motionEvent.getAction() == 0) {
                if (b(af.a((int) motionEvent.getX(), MJ16Activity.ri.getWidth(), getWidth()), af.a((int) motionEvent.getY(), MJ16Activity.ri.getHeight(), getHeight()), motionEvent.getAction()) == this.tW) {
                    g(this.tW);
                } else if (uE) {
                    this.uV = true;
                }
            }
            return true;
        }
        this.gS.onTouchEvent(motionEvent);
        int action = motionEvent.getAction();
        if (!(((int) motionEvent.getX()) == jQ && ((int) motionEvent.getY()) == jR && action != 1) && (action == 0 || action == 1 || action == 2)) {
            jQ = (int) motionEvent.getX();
            jR = (int) motionEvent.getY();
            Object a2 = this.ue != null ? this.ue.a(af.a(jQ, MJ16Activity.ri.getWidth(), getWidth()), af.a(jR, MJ16Activity.ri.getHeight(), getHeight()), action, this.jF) : b(af.a(jQ, MJ16Activity.ri.getWidth(), getWidth()), af.a(jR, MJ16Activity.ri.getHeight(), getHeight()), action);
            if (action == 0) {
                this.jF = a2;
                jS = false;
            } else if (action == 1) {
                if (a2 != null) {
                    if (a2 == this.jF) {
                        g(a2);
                        return true;
                    }
                } else if (a2 == null && this.jF == null && this.tY != null && !jS) {
                    this.tY = null;
                    er();
                } else if (a2 == null && this.gO != null && this.gO.le) {
                    this.uy = !this.uy;
                    er();
                    return true;
                }
            }
            if (a2 != null && (a2 instanceof com.epoint.android.games.mjfgbfree.ui.c) && this.tY != a2) {
                jS = true;
                this.tY = (com.epoint.android.games.mjfgbfree.ui.c) a2;
                this.jG = a2;
                er();
                return true;
            } else if (a2 != this.jG) {
                this.jG = a2;
                er();
            }
        }
        return true;
    }

    public final void run() {
        while (this.uu) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
            }
        }
        if (this.uB.N() == -1) {
            uC = null;
        }
    }
}
