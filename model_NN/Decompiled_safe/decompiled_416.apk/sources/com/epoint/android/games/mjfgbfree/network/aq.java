package com.epoint.android.games.mjfgbfree.network;

import android.content.Intent;
import android.view.View;
import com.epoint.android.games.mjfgbfree.ManualActivity;

final class aq implements View.OnClickListener {
    private /* synthetic */ TCPLocalHostConnectionActivity hZ;

    aq(TCPLocalHostConnectionActivity tCPLocalHostConnectionActivity) {
        this.hZ = tCPLocalHostConnectionActivity;
    }

    public final void onClick(View view) {
        Intent intent = new Intent(this.hZ, ManualActivity.class);
        intent.putExtra("doc", "manual_lan_tcp");
        this.hZ.startActivity(intent);
    }
}
