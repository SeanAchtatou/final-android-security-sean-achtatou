package com.epoint.android.games.mjfgbfree.tournament;

import com.epoint.android.games.mjfgbfree.af;
import java.io.Serializable;
import java.util.Vector;

public final class c implements Serializable {
    public int on = 0;
    public int oo;
    public int op = 1;
    public Vector oq = new Vector();

    public static String D(int i) {
        switch (i) {
            case 0:
                return af.Z("優閒盃");
            case 1:
                return af.Z("初級盃");
            case 2:
                return af.Z("中級盃");
            case 3:
                return af.Z("高級盃");
            case 4:
                return af.Z("大師盃");
            case 5:
                return af.Z("雀友盃");
            default:
                return "";
        }
    }

    public final String C(int i) {
        int size = this.oq.size() - i;
        if (size < 0) {
            return "";
        }
        switch (size) {
            case 0:
                return af.Z("決賽");
            case 1:
                return af.Z("準決賽");
            default:
                return "1/" + ((int) Math.pow(2.0d, (double) size)) + " " + af.Z("賽");
        }
    }

    public final a cA() {
        if (this.op >= this.oq.size()) {
            return null;
        }
        return (a) this.oq.elementAt(this.op);
    }

    public final String cy() {
        return C(this.op);
    }

    public final a cz() {
        if (this.op - 1 >= this.oq.size()) {
            return null;
        }
        return (a) this.oq.elementAt(this.op - 1);
    }
}
