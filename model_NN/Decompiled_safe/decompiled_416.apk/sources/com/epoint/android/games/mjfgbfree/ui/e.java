package com.epoint.android.games.mjfgbfree.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Rect;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import com.epoint.android.games.mjfgbfree.a.d;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

public final class e {
    private static Bitmap Bd;
    private static Canvas Be;
    private static Hashtable yw = new Hashtable();

    private e() {
    }

    private static Bitmap a(Context context, int i, boolean z) {
        int i2;
        Bitmap bitmap;
        if (!yw.containsKey("bc_gold")) {
            yw.put("bc_gold", d.c(context, "images/bc_gold.png"));
            yw.put("stand", d.c(context, "images/stand.png"));
            Bd = Bitmap.createBitmap(((Bitmap) yw.get("stand")).getWidth(), ((((Bitmap) yw.get("stand")).getHeight() / 2) + ((Bitmap) yw.get("bc_gold")).getHeight()) - 4, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(Bd);
            Be = canvas;
            canvas.setDrawFilter(new PaintFlagsDrawFilter(0, 6));
            yw.put("tiles_gold", d.c(context, "images/tiles_gold.png"));
            yw.put("chara_gold", d.c(context, "images/chara_gold.png"));
        }
        if (!yw.containsKey(String.valueOf(String.valueOf(i)) + (z ? "_p" : ""))) {
            if (i == 0) {
                yw.put(String.valueOf(i), d.c(context, "images/tickets.png"));
            } else if ((i > 0 && i <= 59) || i == 99999) {
                if (i < 43) {
                    Bitmap bitmap2 = (Bitmap) yw.get("tiles_gold");
                    i2 = (i >= 35 ? 1 : -1) + i;
                    bitmap = bitmap2;
                } else {
                    i2 = i - 43;
                    bitmap = (Bitmap) yw.get("chara_gold");
                }
                int width = ((Bitmap) yw.get("tiles_gold")).getWidth() / 9;
                int height = ((Bitmap) yw.get("tiles_gold")).getHeight() / 5;
                int i3 = (i2 % 9) * width;
                int i4 = (i2 / 9) * height;
                if (!z) {
                    Bd.eraseColor(0);
                    Be.drawBitmap((Bitmap) yw.get("stand"), 0.0f, (float) (Be.getHeight() - ((Bitmap) yw.get("stand")).getHeight()), (Paint) null);
                }
                if (i != 99999) {
                    int width2 = !z ? ((((Bitmap) yw.get("stand")).getWidth() - ((Bitmap) yw.get("bc_gold")).getWidth()) / 2) + 2 : 0;
                    Rect rect = new Rect(i3, i4, i3 + width, i4 + height);
                    Rect rect2 = new Rect(width2 + 2, 13, width + 2 + width2, height + 13);
                    if (!z) {
                        Be.drawBitmap((Bitmap) yw.get("bc_gold"), (float) width2, 0.0f, (Paint) null);
                        Be.drawBitmap(bitmap, rect, rect2, (Paint) null);
                    } else {
                        Bitmap copy = ((Bitmap) yw.get("bc_gold")).copy(Bitmap.Config.ARGB_8888, true);
                        new Canvas(copy).drawBitmap(bitmap, rect, rect2, (Paint) null);
                        if (MJ16Activity.rc != 1.0f) {
                            Bitmap createScaledBitmap = Bitmap.createScaledBitmap(copy, (int) (((float) copy.getWidth()) * MJ16Activity.rc), (int) (((float) copy.getHeight()) * MJ16Activity.rc), true);
                            copy.recycle();
                            copy = createScaledBitmap;
                        }
                        yw.put(String.valueOf(String.valueOf(i)) + "_p", copy);
                        return copy;
                    }
                }
                if (MJ16Activity.rc != 1.0f) {
                    yw.put(String.valueOf(i), Bitmap.createScaledBitmap(Bd, (int) (((float) Bd.getWidth()) * MJ16Activity.rc), (int) (((float) Bd.getHeight()) * MJ16Activity.rc), true));
                } else {
                    yw.put(String.valueOf(i), Bd.copy(Bitmap.Config.ARGB_8888, false));
                }
            }
        }
        return (Bitmap) yw.get(String.valueOf(i));
    }

    public static Bitmap e(Context context, int i) {
        return a(context, i, false);
    }

    public static String f(Context context, int i) {
        Bitmap a2 = a(context, i, true);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        a2.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        String str = "<img src=\"data:image/png;base64," + com.epoint.android.games.mjfgbfree.a.e.b(byteArrayOutputStream.toByteArray()) + "\" />";
        try {
            byteArrayOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static void gt() {
        Enumeration keys = yw.keys();
        while (keys.hasMoreElements()) {
            ((Bitmap) yw.remove(keys.nextElement())).recycle();
        }
        if (Bd != null) {
            Bd.recycle();
        }
    }

    public static Bitmap y(Context context) {
        if (!yw.containsKey("bkgnd")) {
            yw.put("bkgnd", d.c(context, "images/items_back.jpg"));
        }
        return (Bitmap) yw.get("bkgnd");
    }
}
