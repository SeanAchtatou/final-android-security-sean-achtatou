package com.epoint.android.games.mjfgbfree;

import android.view.View;

final class d implements View.OnClickListener {
    private /* synthetic */ InstructionActivity fU;

    d(InstructionActivity instructionActivity) {
        this.fU = instructionActivity;
    }

    public final void onClick(View view) {
        this.fU.sG.setClickable(false);
        this.fU.sG.setChecked(true);
        this.fU.sI.setClickable(true);
        this.fU.sI.setChecked(false);
        this.fU.sH.setClickable(true);
        this.fU.sH.setChecked(false);
        this.fU.a(this.fU.sJ);
    }
}
