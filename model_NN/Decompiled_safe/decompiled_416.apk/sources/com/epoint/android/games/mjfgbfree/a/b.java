package com.epoint.android.games.mjfgbfree.a;

final class b extends a {
    private static final byte[] cD = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] cE = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static /* synthetic */ boolean cL = (!e.class.desiredAssertionStatus());
    private final byte[] cF = new byte[2];
    private int cG = 0;
    public final boolean cH = true;
    public final boolean cI = true;
    public final boolean cJ = false;
    private final byte[] cK = cD;
    private int count;

    public b() {
        this.n = null;
        this.count = this.cI ? 19 : -1;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0116  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0175  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(byte[] r12, int r13) {
        /*
            r11 = this;
            byte[] r0 = r11.cK
            byte[] r1 = r11.n
            r2 = 0
            int r3 = r11.count
            r4 = 0
            int r5 = r13 + 0
            r6 = -1
            int r7 = r11.cG
            switch(r7) {
                case 0: goto L_0x00c9;
                case 1: goto L_0x00ce;
                case 2: goto L_0x00f4;
                default: goto L_0x0010;
            }
        L_0x0010:
            r10 = r6
            r6 = r4
            r4 = r10
        L_0x0013:
            r7 = -1
            if (r4 == r7) goto L_0x023c
            r7 = 0
            int r2 = r2 + 1
            int r8 = r4 >> 18
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r7] = r8
            r7 = 1
            int r2 = r2 + 1
            int r8 = r4 >> 12
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r7] = r8
            r7 = 2
            int r2 = r2 + 1
            int r8 = r4 >> 6
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r7] = r8
            r7 = 3
            int r2 = r2 + 1
            r4 = r4 & 63
            byte r4 = r0[r4]
            r1[r7] = r4
            int r3 = r3 + -1
            if (r3 != 0) goto L_0x023c
            boolean r3 = r11.cJ
            if (r3 == 0) goto L_0x004f
            r3 = 4
            int r2 = r2 + 1
            r4 = 13
            r1[r3] = r4
        L_0x004f:
            int r3 = r2 + 1
            r4 = 10
            r1[r2] = r4
            r2 = 19
            r4 = r3
            r3 = r2
            r2 = r6
        L_0x005a:
            int r6 = r2 + 3
            if (r6 <= r5) goto L_0x0116
            int r6 = r11.cG
            int r6 = r2 - r6
            r7 = 1
            int r7 = r5 - r7
            if (r6 != r7) goto L_0x0175
            r6 = 0
            int r7 = r11.cG
            if (r7 <= 0) goto L_0x016c
            byte[] r7 = r11.cF
            r8 = 0
            int r6 = r6 + 1
            byte r7 = r7[r8]
            r10 = r7
            r7 = r6
            r6 = r2
            r2 = r10
        L_0x0077:
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r2 = r2 << 4
            int r8 = r11.cG
            int r7 = r8 - r7
            r11.cG = r7
            int r7 = r4 + 1
            int r8 = r2 >> 6
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r4] = r8
            int r4 = r7 + 1
            r2 = r2 & 63
            byte r0 = r0[r2]
            r1[r7] = r0
            boolean r0 = r11.cH
            if (r0 == 0) goto L_0x0236
            int r0 = r4 + 1
            r2 = 61
            r1[r4] = r2
            int r2 = r0 + 1
            r4 = 61
            r1[r0] = r4
            r0 = r2
        L_0x00a4:
            boolean r2 = r11.cI
            if (r2 == 0) goto L_0x0232
            boolean r2 = r11.cJ
            if (r2 == 0) goto L_0x00b3
            int r2 = r0 + 1
            r4 = 13
            r1[r0] = r4
            r0 = r2
        L_0x00b3:
            int r2 = r0 + 1
            r4 = 10
            r1[r0] = r4
            r0 = r6
            r1 = r2
        L_0x00bb:
            boolean r2 = com.epoint.android.games.mjfgbfree.a.b.cL
            if (r2 != 0) goto L_0x0218
            int r2 = r11.cG
            if (r2 == 0) goto L_0x0218
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x00c9:
            r10 = r6
            r6 = r4
            r4 = r10
            goto L_0x0013
        L_0x00ce:
            r7 = 2
            if (r7 > r5) goto L_0x0010
            byte[] r6 = r11.cF
            r7 = 0
            byte r6 = r6[r7]
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 16
            r7 = 0
            int r4 = r4 + 1
            byte r7 = r12[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            int r7 = r7 << 8
            r6 = r6 | r7
            r7 = 1
            int r4 = r4 + 1
            byte r7 = r12[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            r6 = r6 | r7
            r7 = 0
            r11.cG = r7
            r10 = r6
            r6 = r4
            r4 = r10
            goto L_0x0013
        L_0x00f4:
            if (r5 <= 0) goto L_0x0010
            byte[] r6 = r11.cF
            r7 = 0
            byte r6 = r6[r7]
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 16
            byte[] r7 = r11.cF
            r8 = 1
            byte r7 = r7[r8]
            r7 = r7 & 255(0xff, float:3.57E-43)
            int r7 = r7 << 8
            r6 = r6 | r7
            r7 = 0
            int r4 = r4 + 1
            byte r7 = r12[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            r6 = r6 | r7
            r7 = 0
            r11.cG = r7
            goto L_0x0010
        L_0x0116:
            byte r6 = r12[r2]
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 16
            int r7 = r2 + 1
            byte r7 = r12[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            int r7 = r7 << 8
            r6 = r6 | r7
            int r7 = r2 + 2
            byte r7 = r12[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            r6 = r6 | r7
            int r7 = r6 >> 18
            r7 = r7 & 63
            byte r7 = r0[r7]
            r1[r4] = r7
            int r7 = r4 + 1
            int r8 = r6 >> 12
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r7] = r8
            int r7 = r4 + 2
            int r8 = r6 >> 6
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r7] = r8
            int r7 = r4 + 3
            r6 = r6 & 63
            byte r6 = r0[r6]
            r1[r7] = r6
            int r2 = r2 + 3
            int r4 = r4 + 4
            int r3 = r3 + -1
            if (r3 != 0) goto L_0x005a
            boolean r3 = r11.cJ
            if (r3 == 0) goto L_0x0239
            int r3 = r4 + 1
            r6 = 13
            r1[r4] = r6
        L_0x0162:
            int r4 = r3 + 1
            r6 = 10
            r1[r3] = r6
            r3 = 19
            goto L_0x005a
        L_0x016c:
            int r7 = r2 + 1
            byte r2 = r12[r2]
            r10 = r7
            r7 = r6
            r6 = r10
            goto L_0x0077
        L_0x0175:
            int r6 = r11.cG
            int r6 = r2 - r6
            r7 = 2
            int r7 = r5 - r7
            if (r6 != r7) goto L_0x01fa
            r6 = 0
            int r7 = r11.cG
            r8 = 1
            if (r7 <= r8) goto L_0x01ea
            byte[] r7 = r11.cF
            r8 = 0
            int r6 = r6 + 1
            byte r7 = r7[r8]
            r10 = r7
            r7 = r6
            r6 = r2
            r2 = r10
        L_0x018f:
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r2 = r2 << 10
            int r8 = r11.cG
            if (r8 <= 0) goto L_0x01f2
            byte[] r8 = r11.cF
            int r9 = r7 + 1
            byte r7 = r8[r7]
            r8 = r9
            r10 = r6
            r6 = r7
            r7 = r10
        L_0x01a1:
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 2
            r2 = r2 | r6
            int r6 = r11.cG
            int r6 = r6 - r8
            r11.cG = r6
            int r6 = r4 + 1
            int r8 = r2 >> 12
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r4] = r8
            int r4 = r6 + 1
            int r8 = r2 >> 6
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r6] = r8
            int r6 = r4 + 1
            r2 = r2 & 63
            byte r0 = r0[r2]
            r1[r4] = r0
            boolean r0 = r11.cH
            if (r0 == 0) goto L_0x0230
            int r0 = r6 + 1
            r2 = 61
            r1[r6] = r2
        L_0x01d1:
            boolean r2 = r11.cI
            if (r2 == 0) goto L_0x022c
            boolean r2 = r11.cJ
            if (r2 == 0) goto L_0x01e0
            int r2 = r0 + 1
            r4 = 13
            r1[r0] = r4
            r0 = r2
        L_0x01e0:
            int r2 = r0 + 1
            r4 = 10
            r1[r0] = r4
            r0 = r7
            r1 = r2
            goto L_0x00bb
        L_0x01ea:
            int r7 = r2 + 1
            byte r2 = r12[r2]
            r10 = r7
            r7 = r6
            r6 = r10
            goto L_0x018f
        L_0x01f2:
            int r8 = r6 + 1
            byte r6 = r12[r6]
            r10 = r8
            r8 = r7
            r7 = r10
            goto L_0x01a1
        L_0x01fa:
            boolean r0 = r11.cI
            if (r0 == 0) goto L_0x0214
            if (r4 <= 0) goto L_0x0214
            r0 = 19
            if (r3 == r0) goto L_0x0214
            boolean r0 = r11.cJ
            if (r0 == 0) goto L_0x022a
            int r0 = r4 + 1
            r6 = 13
            r1[r4] = r6
        L_0x020e:
            int r4 = r0 + 1
            r6 = 10
            r1[r0] = r6
        L_0x0214:
            r0 = r2
            r1 = r4
            goto L_0x00bb
        L_0x0218:
            boolean r2 = com.epoint.android.games.mjfgbfree.a.b.cL
            if (r2 != 0) goto L_0x0224
            if (r0 == r5) goto L_0x0224
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x0224:
            r11.o = r1
            r11.count = r3
            r0 = 1
            return r0
        L_0x022a:
            r0 = r4
            goto L_0x020e
        L_0x022c:
            r1 = r0
            r0 = r7
            goto L_0x00bb
        L_0x0230:
            r0 = r6
            goto L_0x01d1
        L_0x0232:
            r1 = r0
            r0 = r6
            goto L_0x00bb
        L_0x0236:
            r0 = r4
            goto L_0x00a4
        L_0x0239:
            r3 = r4
            goto L_0x0162
        L_0x023c:
            r4 = r2
            r2 = r6
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.a.b.a(byte[], int):boolean");
    }
}
