package com.epoint.android.games.mjfgbfree.network;

import android.content.Intent;
import android.view.View;
import com.epoint.android.games.mjfgbfree.ManualActivity;

final class cd implements View.OnClickListener {
    private /* synthetic */ TCPServerConnectionActivity sh;

    cd(TCPServerConnectionActivity tCPServerConnectionActivity) {
        this.sh = tCPServerConnectionActivity;
    }

    public final void onClick(View view) {
        Intent intent = new Intent(this.sh, ManualActivity.class);
        intent.putExtra("doc", "manual_tcp_server");
        this.sh.startActivity(intent);
    }
}
