package com.epoint.android.games.mjfgbfree.baseactivities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import b.a.b;
import com.google.android.maps.MapActivity;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public abstract class BaseMapActivity extends MapActivity implements Handler.Callback {
    private static int ot = 0;
    private static volatile Hashtable ox = null;
    private Handler mHandler;
    protected boolean oA = false;
    /* access modifiers changed from: private */
    public volatile Hashtable oB = new Hashtable();
    private volatile Hashtable oC = new Hashtable();
    /* access modifiers changed from: private */
    public volatile Vector oD = new Vector();
    /* access modifiers changed from: private */
    public boolean oE = false;
    private String os;
    private int ou = -1;
    private String ov;
    private ProgressDialog ow;
    private boolean oy = false;
    protected int oz = 0;

    static /* synthetic */ boolean a(BaseMapActivity baseMapActivity, a aVar, b bVar) {
        if (!bVar.H("error")) {
            if (aVar.dM == 2) {
                baseMapActivity.b(aVar.dL, bVar);
            }
            String string = bVar.F("error").getString("message");
            Message message = new Message();
            message.what = 997;
            message.obj = string;
            baseMapActivity.a(message);
            return false;
        }
        baseMapActivity.b(aVar.dL, bVar);
        return true;
    }

    private synchronized void ap(String str) {
        ox.remove(String.valueOf(this.os) + str);
    }

    private synchronized void cB() {
        synchronized (ox) {
            Enumeration keys = ox.keys();
            synchronized (keys) {
                while (keys.hasMoreElements()) {
                    String str = (String) keys.nextElement();
                    if (str.startsWith(this.os)) {
                        ox.remove(str);
                    }
                }
            }
        }
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.content.Context, com.epoint.android.games.mjfgbfree.baseactivities.BaseMapActivity, android.os.Handler$Callback, com.google.android.maps.MapActivity] */
    public final void a(Bundle bundle, String str) {
        if (ox == null) {
            ox = new Hashtable();
        }
        BaseMapActivity.super.onCreate(bundle);
        this.ov = str;
        StringBuilder append = new StringBuilder(String.valueOf(str)).append("#");
        int i = ot + 1;
        ot = i;
        this.os = append.append(i).append("#").toString();
        this.mHandler = new Handler((Handler.Callback) this);
        this.ow = new ProgressDialog(this);
        this.ow.setMessage("Loading...");
        this.ow.setIndeterminate(true);
        this.ow.setCancelable(false);
    }

    /* access modifiers changed from: protected */
    public final void a(Message message) {
        this.mHandler.sendMessage(message);
    }

    /* access modifiers changed from: protected */
    public final void a(a aVar) {
        this.oB.put(aVar.dL, aVar);
    }

    /* access modifiers changed from: protected */
    public final synchronized Object ao(String str) {
        return ox.get(String.valueOf(this.os) + str);
    }

    /* access modifiers changed from: protected */
    public final void b(Exception exc) {
        Message message = new Message();
        message.what = 999;
        message.obj = exc.toString();
        a(message);
    }

    /* access modifiers changed from: protected */
    public final synchronized void b(String str, Object obj) {
        ox.put(String.valueOf(this.os) + str, obj);
    }

    /* access modifiers changed from: protected */
    public final boolean cC() {
        return !this.oD.isEmpty();
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [android.content.Context, com.epoint.android.games.mjfgbfree.baseactivities.BaseMapActivity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected final boolean cD() {
        /*
            r4 = this;
            r3 = 0
            java.util.Hashtable r0 = r4.oB
            int r0 = r0.size()
            if (r0 != 0) goto L_0x000b
            r0 = r3
        L_0x000a:
            return r0
        L_0x000b:
            java.util.Vector r0 = r4.oD
            int r0 = r0.size()
            if (r0 != 0) goto L_0x0060
            r4.oE = r3
            android.os.Message r0 = new android.os.Message
            r0.<init>()
            r0.what = r3
            r4.a(r0)
            java.util.Hashtable r0 = r4.oB
            java.util.Enumeration r1 = r0.elements()
        L_0x0025:
            boolean r0 = r1.hasMoreElements()
            if (r0 != 0) goto L_0x003d
            java.util.Vector r1 = r4.oD
            monitor-enter(r1)
            r2 = r3
        L_0x002f:
            java.util.Vector r0 = r4.oD     // Catch:{ all -> 0x005d }
            int r0 = r0.size()     // Catch:{ all -> 0x005d }
            if (r2 >= r0) goto L_0x003a
            r0 = 5
            if (r2 < r0) goto L_0x004e
        L_0x003a:
            monitor-exit(r1)     // Catch:{ all -> 0x005d }
            r0 = 1
            goto L_0x000a
        L_0x003d:
            com.epoint.android.games.mjfgbfree.baseactivities.d r2 = new com.epoint.android.games.mjfgbfree.baseactivities.d
            java.lang.Object r0 = r1.nextElement()
            com.epoint.android.games.mjfgbfree.baseactivities.a r0 = (com.epoint.android.games.mjfgbfree.baseactivities.a) r0
            r2.<init>(r4, r4, r0)
            java.util.Vector r0 = r4.oD
            r0.addElement(r2)
            goto L_0x0025
        L_0x004e:
            java.util.Vector r0 = r4.oD     // Catch:{ all -> 0x005d }
            java.lang.Object r0 = r0.elementAt(r2)     // Catch:{ all -> 0x005d }
            java.lang.Thread r0 = (java.lang.Thread) r0     // Catch:{ all -> 0x005d }
            r0.start()     // Catch:{ all -> 0x005d }
            int r0 = r2 + 1
            r2 = r0
            goto L_0x002f
        L_0x005d:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x005d }
            throw r0
        L_0x0060:
            r0 = r3
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.baseactivities.BaseMapActivity.cD():boolean");
    }

    /* access modifiers changed from: protected */
    public final void cE() {
        if (!this.ow.isShowing()) {
            Message message = new Message();
            message.what = 996;
            a(message);
        }
    }

    /* access modifiers changed from: protected */
    public final void cF() {
        if (this.ow.isShowing()) {
            Message message = new Message();
            message.what = 995;
            a(message);
        }
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [android.content.Context, com.epoint.android.games.mjfgbfree.baseactivities.BaseMapActivity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public synchronized boolean handleMessage(android.os.Message r4) {
        /*
            r3 = this;
            r2 = 0
            monitor-enter(r3)
            int r0 = r4.what     // Catch:{ all -> 0x000e }
            switch(r0) {
                case 0: goto L_0x0009;
                case 2: goto L_0x003d;
                case 4: goto L_0x003d;
                case 5: goto L_0x0011;
                case 6: goto L_0x0011;
                case 995: goto L_0x004e;
                case 996: goto L_0x0049;
                case 997: goto L_0x0030;
                case 998: goto L_0x0023;
                case 999: goto L_0x0016;
                default: goto L_0x0007;
            }
        L_0x0007:
            monitor-exit(r3)
            return r2
        L_0x0009:
            r0 = 1
            r3.setProgressBarIndeterminateVisibility(r0)     // Catch:{ all -> 0x000e }
            goto L_0x0007
        L_0x000e:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x0011:
            r0 = 0
            r3.setProgressBarIndeterminateVisibility(r0)     // Catch:{ all -> 0x000e }
            goto L_0x0007
        L_0x0016:
            java.lang.Object r0 = r4.obj     // Catch:{ all -> 0x000e }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x000e }
            r1 = 0
            android.widget.Toast r0 = android.widget.Toast.makeText(r3, r0, r1)     // Catch:{ all -> 0x000e }
            r0.show()     // Catch:{ all -> 0x000e }
            goto L_0x0007
        L_0x0023:
            java.lang.Object r0 = r4.obj     // Catch:{ all -> 0x000e }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x000e }
            r1 = 1
            android.widget.Toast r0 = android.widget.Toast.makeText(r3, r0, r1)     // Catch:{ all -> 0x000e }
            r0.show()     // Catch:{ all -> 0x000e }
            goto L_0x0007
        L_0x0030:
            java.lang.Object r0 = r4.obj     // Catch:{ all -> 0x000e }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x000e }
            r1 = 0
            android.widget.Toast r0 = android.widget.Toast.makeText(r3, r0, r1)     // Catch:{ all -> 0x000e }
            r0.show()     // Catch:{ all -> 0x000e }
            goto L_0x0007
        L_0x003d:
            java.lang.Object r0 = r4.obj     // Catch:{ all -> 0x000e }
            if (r0 == 0) goto L_0x0007
            java.lang.Object r0 = r4.obj     // Catch:{ all -> 0x000e }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x000e }
            r3.ap(r0)     // Catch:{ all -> 0x000e }
            goto L_0x0007
        L_0x0049:
            r0 = 1
            r3.showDialog(r0)     // Catch:{ all -> 0x000e }
            goto L_0x0007
        L_0x004e:
            r0 = 1
            r3.dismissDialog(r0)     // Catch:{ all -> 0x000e }
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.baseactivities.BaseMapActivity.handleMessage(android.os.Message):boolean");
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        BaseMapActivity.super.onActivityResult(i, i2, intent);
        if (intent != null) {
            switch (i2) {
                case -1:
                    if (intent.getBooleanExtra("system_back_to_main", false)) {
                        setResult(i2, intent);
                        finish();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        BaseMapActivity.super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 1:
                return this.ow;
            default:
                return null;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return BaseMapActivity.super.onCreateOptionsMenu(menu);
    }

    public void onDestroy() {
        cB();
        ot--;
        BaseMapActivity.super.onDestroy();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 999:
                cB();
                Intent intent = BaseMapActivity.super.getIntent();
                intent.putExtra("system_restart_activity", true);
                setResult(-1, intent);
                finish();
                break;
        }
        return BaseMapActivity.super.onOptionsItemSelected(menuItem);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        return BaseMapActivity.super.onPrepareOptionsMenu(menu);
    }

    public void onWindowFocusChanged(boolean z) {
        if (z) {
            this.oy = true;
        }
        BaseMapActivity.super.onWindowFocusChanged(z);
    }
}
