package com.epoint.android.games.mjfgbfree.b;

import android.content.Context;
import android.media.SoundPool;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.d.g;

public final class a {
    private SoundPool L = new SoundPool(5, 3, 1);
    public int M;
    public int N;
    public int O;
    public int P;
    public int Q;
    public int R;
    public int S;
    public int T;
    public int U;
    private String V = "";
    private boolean W;

    public a(Context context, boolean z) {
        this.M = this.L.load(context, C0000R.raw.pung_f3, 1);
        this.N = this.L.load(context, C0000R.raw.chow_f3, 1);
        this.O = this.L.load(context, C0000R.raw.kung_f3, 1);
        this.P = this.L.load(context, C0000R.raw.won_f3, 1);
        this.Q = this.L.load(context, C0000R.raw.flower_f3, 1);
        this.R = this.L.load(context, C0000R.raw.draw_game_f3, 1);
        this.S = this.L.load(context, C0000R.raw.won_result_f3, 1);
        this.T = this.L.load(context, C0000R.raw.discard, 1);
        this.U = this.L.load(context, C0000R.raw.get, 1);
        this.W = z;
    }

    public final void a(int i) {
        if (this.W) {
            this.L.play(i, 1.0f, 1.0f, 1, 0, 1.0f);
        }
    }

    public final void a(int i, g gVar) {
        if (this.W && !this.V.equals(gVar.V)) {
            this.L.play(i, 1.0f, 1.0f, 1, 0, 1.0f);
            this.V = gVar.V;
        }
    }

    public final void a(boolean z) {
        this.W = z;
    }

    public final void b(String str) {
        this.V = str;
    }
}
