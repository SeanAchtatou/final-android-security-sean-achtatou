package com.epoint.android.games.mjfgbfree.street;

import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ListView;
import b.a.b;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.baseactivities.BaseMapActivity;
import com.epoint.android.games.mjfgbfree.baseactivities.a;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import java.net.URLEncoder;
import java.util.Vector;

public class PlacesMapActivity extends BaseMapActivity implements LocationListener {
    private Vector E = new Vector();
    /* access modifiers changed from: private */
    public LocationManager bw;
    private String fC = "";
    /* access modifiers changed from: private */
    public MapView fD;
    /* access modifiers changed from: private */
    public MapController fE;
    /* access modifiers changed from: private */
    public e fF;
    private ListView fG;
    /* access modifiers changed from: private */
    public double fH = 1001.0d;
    private double fI = 1001.0d;
    private b fJ;

    /* access modifiers changed from: private */
    public void a(GeoPoint geoPoint) {
        this.fF.bl();
        this.fF.a(new OverlayItem(geoPoint, "", ""));
        if (this.fD.getOverlays().isEmpty()) {
            this.fD.getOverlays().add(this.fF);
        }
        this.fD.invalidate();
        this.fE.animateTo(geoPoint);
        if (!cC()) {
            this.fJ = null;
            a("", ((double) ((long) geoPoint.getLatitudeE6())) / 1000000.0d, ((double) ((long) geoPoint.getLongitudeE6())) / 1000000.0d);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, double d, double d2) {
        String trim = str.trim();
        this.fC = trim;
        if (trim.equals("")) {
            trim = "*";
        }
        a(new a(this, "http://ajax.googleapis.com/ajax/services/search/local?rsz=8&sll=" + d + "," + d2 + "&q=" + URLEncoder.encode(trim) + "&v=1.0&hl=zh-HK&key=ABQIAAAA489FGuLAq01AZ2qNQPlBHRTGkedz1DV-efpzbt53LtagN71smhRpWB7Zh9luAJFBcjC8J2fPBel_tQ", 2));
        if (cD()) {
            cE();
        }
    }

    private void aV() {
        ViewGroup.LayoutParams layoutParams = this.fD.getLayoutParams();
        layoutParams.height = ((WindowManager) getSystemService("window")).getDefaultDisplay().getHeight() / 2;
        this.fD.setLayoutParams(layoutParams);
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.content.Context] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean handleMessage(android.os.Message r21) {
        /*
            r20 = this;
            monitor-enter(r20)
            r0 = r21
            int r0 = r0.what     // Catch:{ all -> 0x002a }
            r5 = r0
            switch(r5) {
                case 2: goto L_0x000f;
                default: goto L_0x0009;
            }     // Catch:{ all -> 0x002a }
        L_0x0009:
            boolean r5 = super.handleMessage(r21)     // Catch:{ all -> 0x002a }
            monitor-exit(r20)
            return r5
        L_0x000f:
            r0 = r21
            java.lang.Object r0 = r0.obj     // Catch:{ Exception -> 0x001c }
            r5 = r0
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ Exception -> 0x001c }
            if (r5 != 0) goto L_0x002d
            r20.cF()     // Catch:{ Exception -> 0x001c }
            goto L_0x0009
        L_0x001c:
            r5 = move-exception
            r20.cF()     // Catch:{ all -> 0x002a }
            r5.printStackTrace()     // Catch:{ all -> 0x002a }
            r0 = r20
            r1 = r5
            r0.b(r1)     // Catch:{ all -> 0x002a }
            goto L_0x0009
        L_0x002a:
            r5 = move-exception
            monitor-exit(r20)
            throw r5
        L_0x002d:
            java.lang.String r6 = "http://maps.google.com/maps/api/geocode/json?"
            boolean r6 = r5.startsWith(r6)     // Catch:{ Exception -> 0x001c }
            if (r6 == 0) goto L_0x017c
            r0 = r20
            r1 = r5
            java.lang.Object r5 = r0.ao(r1)     // Catch:{ Exception -> 0x001c }
            b.a.b r5 = (b.a.b) r5     // Catch:{ Exception -> 0x001c }
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            java.lang.String r10 = "results"
            b.a.a r5 = r5.E(r10)     // Catch:{ Exception -> 0x001c }
            r10 = 0
            r18 = r10
            r10 = r6
            r6 = r18
            r19 = r7
            r7 = r9
            r9 = r19
        L_0x0053:
            int r11 = r5.length()     // Catch:{ Exception -> 0x001c }
            if (r6 < r11) goto L_0x00e5
            r0 = r20
            b.a.b r0 = r0.fJ     // Catch:{ Exception -> 0x001c }
            r5 = r0
            java.lang.String r6 = "responseData"
            b.a.b r5 = r5.F(r6)     // Catch:{ Exception -> 0x001c }
            if (r10 == 0) goto L_0x006b
            java.lang.String r6 = "country_code"
            r5.a(r6, r10)     // Catch:{ Exception -> 0x001c }
        L_0x006b:
            if (r9 == 0) goto L_0x0072
            java.lang.String r6 = "admin_area"
            r5.a(r6, r9)     // Catch:{ Exception -> 0x001c }
        L_0x0072:
            if (r8 == 0) goto L_0x0079
            java.lang.String r6 = "sub_admin_area"
            r5.a(r6, r8)     // Catch:{ Exception -> 0x001c }
        L_0x0079:
            if (r7 == 0) goto L_0x0080
            java.lang.String r6 = "locality"
            r5.a(r6, r7)     // Catch:{ Exception -> 0x001c }
        L_0x0080:
            r0 = r20
            b.a.b r0 = r0.fJ     // Catch:{ Exception -> 0x001c }
            r5 = r0
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x001c }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x001c }
            java.lang.String r7 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x001c }
            r6.<init>(r7)     // Catch:{ Exception -> 0x001c }
            java.lang.String r7 = "b8dcb97a5599699ceb13af29b33361c0"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x001c }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x001c }
            java.lang.String r6 = com.epoint.android.games.mjfgbfree.a.d.B(r6)     // Catch:{ Exception -> 0x001c }
            boolean r7 = r20.cC()     // Catch:{ Exception -> 0x001c }
            if (r7 != 0) goto L_0x0177
            com.epoint.android.games.mjfgbfree.baseactivities.a r7 = new com.epoint.android.games.mjfgbfree.baseactivities.a     // Catch:{ Exception -> 0x001c }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x001c }
            java.lang.String r9 = "http://www.mjguy.com:8080/s?req=explore&facebook_access_token="
            r8.<init>(r9)     // Catch:{ Exception -> 0x001c }
            java.lang.String r9 = com.epoint.android.games.mjfgbfree.a.d.iq     // Catch:{ Exception -> 0x001c }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x001c }
            java.lang.String r9 = "&signature="
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x001c }
            java.lang.StringBuilder r6 = r8.append(r6)     // Catch:{ Exception -> 0x001c }
            java.lang.String r8 = "&data="
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ Exception -> 0x001c }
            java.lang.String r5 = java.net.URLEncoder.encode(r5)     // Catch:{ Exception -> 0x001c }
            java.lang.StringBuilder r5 = r6.append(r5)     // Catch:{ Exception -> 0x001c }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x001c }
            r6 = 3
            r0 = r7
            r1 = r20
            r2 = r5
            r3 = r6
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x001c }
            r0 = r20
            r1 = r7
            r0.a(r1)     // Catch:{ Exception -> 0x001c }
            r20.cD()     // Catch:{ Exception -> 0x001c }
            goto L_0x0009
        L_0x00e5:
            b.a.b r11 = r5.m(r6)     // Catch:{ Exception -> 0x001c }
            java.lang.String r12 = "address_components"
            b.a.a r11 = r11.M(r12)     // Catch:{ Exception -> 0x001c }
            if (r11 == 0) goto L_0x0104
            r12 = 0
            r18 = r12
            r12 = r10
            r10 = r9
            r9 = r8
            r8 = r7
            r7 = r18
        L_0x00fa:
            int r13 = r11.length()     // Catch:{ Exception -> 0x001c }
            if (r7 < r13) goto L_0x0108
            r7 = r8
            r8 = r9
            r9 = r10
            r10 = r12
        L_0x0104:
            int r6 = r6 + 1
            goto L_0x0053
        L_0x0108:
            b.a.b r13 = r11.m(r7)     // Catch:{ Exception -> 0x001c }
            java.lang.String r14 = "types"
            b.a.a r14 = r13.M(r14)     // Catch:{ Exception -> 0x001c }
            if (r14 == 0) goto L_0x012a
            r15 = 0
            r18 = r15
            r15 = r12
            r12 = r10
            r10 = r9
            r9 = r8
            r8 = r18
        L_0x011d:
            int r16 = r14.length()     // Catch:{ Exception -> 0x001c }
            r0 = r8
            r1 = r16
            if (r0 < r1) goto L_0x012d
            r8 = r9
            r9 = r10
            r10 = r12
            r12 = r15
        L_0x012a:
            int r7 = r7 + 1
            goto L_0x00fa
        L_0x012d:
            java.lang.String r16 = r14.getString(r8)     // Catch:{ Exception -> 0x001c }
            java.lang.String r17 = "locality"
            boolean r17 = r16.equals(r17)     // Catch:{ Exception -> 0x001c }
            if (r17 == 0) goto L_0x0144
            if (r9 != 0) goto L_0x0141
            java.lang.String r9 = "long_name"
            java.lang.String r9 = r13.O(r9)     // Catch:{ Exception -> 0x001c }
        L_0x0141:
            int r8 = r8 + 1
            goto L_0x011d
        L_0x0144:
            java.lang.String r17 = "administrative_area_level_2"
            boolean r17 = r16.equals(r17)     // Catch:{ Exception -> 0x001c }
            if (r17 == 0) goto L_0x0155
            if (r10 != 0) goto L_0x0141
            java.lang.String r10 = "long_name"
            java.lang.String r10 = r13.O(r10)     // Catch:{ Exception -> 0x001c }
            goto L_0x0141
        L_0x0155:
            java.lang.String r17 = "administrative_area_level_1"
            boolean r17 = r16.equals(r17)     // Catch:{ Exception -> 0x001c }
            if (r17 == 0) goto L_0x0166
            if (r12 != 0) goto L_0x0141
            java.lang.String r12 = "long_name"
            java.lang.String r12 = r13.O(r12)     // Catch:{ Exception -> 0x001c }
            goto L_0x0141
        L_0x0166:
            java.lang.String r17 = "country"
            boolean r16 = r16.equals(r17)     // Catch:{ Exception -> 0x001c }
            if (r16 == 0) goto L_0x0141
            if (r15 != 0) goto L_0x0141
            java.lang.String r15 = "short_name"
            java.lang.String r15 = r13.O(r15)     // Catch:{ Exception -> 0x001c }
            goto L_0x0141
        L_0x0177:
            r20.cF()     // Catch:{ Exception -> 0x001c }
            goto L_0x0009
        L_0x017c:
            java.lang.String r6 = "http://ajax.googleapis.com/ajax/services/search/local?"
            boolean r6 = r5.startsWith(r6)     // Catch:{ Exception -> 0x001c }
            if (r6 == 0) goto L_0x025a
            r0 = r20
            r1 = r5
            java.lang.Object r5 = r0.ao(r1)     // Catch:{ Exception -> 0x001c }
            b.a.b r5 = (b.a.b) r5     // Catch:{ Exception -> 0x001c }
            r0 = r5
            r1 = r20
            r1.fJ = r0     // Catch:{ Exception -> 0x001c }
            r0 = r20
            b.a.b r0 = r0.fJ     // Catch:{ Exception -> 0x001c }
            r5 = r0
            java.lang.String r6 = "responseData"
            b.a.b r5 = r5.F(r6)     // Catch:{ Exception -> 0x001c }
            java.lang.String r6 = "results"
            b.a.a r5 = r5.E(r6)     // Catch:{ Exception -> 0x001c }
            r6 = 4652016104934211584(0x408f480000000000, double:1001.0)
            r8 = 4652016104934211584(0x408f480000000000, double:1001.0)
            int r10 = r5.length()     // Catch:{ Exception -> 0x001c }
            if (r10 <= 0) goto L_0x023d
            r10 = 0
            r18 = r8
            r8 = r6
            r6 = r18
        L_0x01b9:
            int r11 = r5.length()     // Catch:{ Exception -> 0x001c }
            if (r10 < r11) goto L_0x01fa
            boolean r5 = r20.cC()     // Catch:{ Exception -> 0x001c }
            if (r5 != 0) goto L_0x0255
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x001c }
            java.lang.String r10 = "http://maps.google.com/maps/api/geocode/json?latlng="
            r5.<init>(r10)     // Catch:{ Exception -> 0x001c }
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ Exception -> 0x001c }
            java.lang.String r8 = ","
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ Exception -> 0x001c }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x001c }
            java.lang.String r6 = "&sensor=true&language=en"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x001c }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x001c }
            com.epoint.android.games.mjfgbfree.baseactivities.a r6 = new com.epoint.android.games.mjfgbfree.baseactivities.a     // Catch:{ Exception -> 0x001c }
            r7 = 2
            r0 = r6
            r1 = r20
            r2 = r5
            r3 = r7
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x001c }
            r0 = r20
            r1 = r6
            r0.a(r1)     // Catch:{ Exception -> 0x001c }
            r20.cD()     // Catch:{ Exception -> 0x001c }
            goto L_0x0009
        L_0x01fa:
            b.a.b r11 = r5.m(r10)     // Catch:{ Exception -> 0x001c }
            java.lang.String r12 = "url"
            java.lang.String r12 = r11.getString(r12)     // Catch:{ Exception -> 0x001c }
            java.lang.String r13 = "&cid="
            int r13 = r12.indexOf(r13)     // Catch:{ Exception -> 0x001c }
            r14 = -1
            if (r13 == r14) goto L_0x0226
            int r13 = r13 + 5
            java.lang.String r12 = r12.substring(r13)     // Catch:{ Exception -> 0x001c }
            java.lang.String r13 = "&"
            int r13 = r12.indexOf(r13)     // Catch:{ Exception -> 0x001c }
            r14 = -1
            if (r13 == r14) goto L_0x0221
            r14 = 0
            java.lang.String r12 = r12.substring(r14, r13)     // Catch:{ Exception -> 0x001c }
        L_0x0221:
            java.lang.String r13 = "place_guid"
            r11.a(r13, r12)     // Catch:{ Exception -> 0x001c }
        L_0x0226:
            if (r10 != 0) goto L_0x0239
            java.lang.String r6 = "lat"
            double r6 = r11.getDouble(r6)     // Catch:{ Exception -> 0x001c }
            java.lang.String r8 = "lng"
            double r8 = r11.getDouble(r8)     // Catch:{ Exception -> 0x001c }
            r18 = r8
            r8 = r6
            r6 = r18
        L_0x0239:
            int r10 = r10 + 1
            goto L_0x01b9
        L_0x023d:
            r0 = r20
            android.widget.ListView r0 = r0.fG     // Catch:{ Exception -> 0x001c }
            r5 = r0
            r6 = 8
            r5.setVisibility(r6)     // Catch:{ Exception -> 0x001c }
            r5 = 2131361867(0x7f0a004b, float:1.8343498E38)
            r0 = r20
            r1 = r5
            android.view.View r5 = r0.findViewById(r1)     // Catch:{ Exception -> 0x001c }
            r6 = 0
            r5.setVisibility(r6)     // Catch:{ Exception -> 0x001c }
        L_0x0255:
            r20.cF()     // Catch:{ Exception -> 0x001c }
            goto L_0x0009
        L_0x025a:
            java.lang.String r6 = "http://www.mjguy.com:8080/s?req=explore&"
            boolean r6 = r5.startsWith(r6)     // Catch:{ Exception -> 0x001c }
            if (r6 == 0) goto L_0x0009
            r0 = r20
            r1 = r5
            java.lang.Object r5 = r0.ao(r1)     // Catch:{ Exception -> 0x001c }
            b.a.b r5 = (b.a.b) r5     // Catch:{ Exception -> 0x001c }
            java.lang.String r6 = "responseData"
            b.a.b r5 = r5.F(r6)     // Catch:{ Exception -> 0x001c }
            java.lang.String r6 = "results"
            b.a.a r5 = r5.E(r6)     // Catch:{ Exception -> 0x001c }
            r0 = r20
            java.util.Vector r0 = r0.E     // Catch:{ Exception -> 0x001c }
            r6 = r0
            r6.removeAllElements()     // Catch:{ Exception -> 0x001c }
            r6 = 0
        L_0x0280:
            int r7 = r5.length()     // Catch:{ Exception -> 0x001c }
            if (r6 < r7) goto L_0x02cb
            r0 = r20
            android.widget.ListView r0 = r0.fG     // Catch:{ Exception -> 0x001c }
            r5 = r0
            com.epoint.android.games.mjfgbfree.street.b r6 = new com.epoint.android.games.mjfgbfree.street.b     // Catch:{ Exception -> 0x001c }
            r0 = r20
            java.util.Vector r0 = r0.E     // Catch:{ Exception -> 0x001c }
            r7 = r0
            r0 = r6
            r1 = r20
            r2 = r20
            r3 = r7
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x001c }
            r5.setAdapter(r6)     // Catch:{ Exception -> 0x001c }
            r0 = r20
            android.widget.ListView r0 = r0.fG     // Catch:{ Exception -> 0x001c }
            r5 = r0
            r6 = 0
            r5.setVisibility(r6)     // Catch:{ Exception -> 0x001c }
            r0 = r20
            android.widget.ListView r0 = r0.fG     // Catch:{ Exception -> 0x001c }
            r5 = r0
            com.epoint.android.games.mjfgbfree.street.h r6 = new com.epoint.android.games.mjfgbfree.street.h     // Catch:{ Exception -> 0x001c }
            r0 = r6
            r1 = r20
            r0.<init>(r1)     // Catch:{ Exception -> 0x001c }
            r5.setOnItemClickListener(r6)     // Catch:{ Exception -> 0x001c }
            r5 = 2131361867(0x7f0a004b, float:1.8343498E38)
            r0 = r20
            r1 = r5
            android.view.View r5 = r0.findViewById(r1)     // Catch:{ Exception -> 0x001c }
            r6 = 8
            r5.setVisibility(r6)     // Catch:{ Exception -> 0x001c }
            r20.cF()     // Catch:{ Exception -> 0x001c }
            goto L_0x0009
        L_0x02cb:
            b.a.b r7 = r5.m(r6)     // Catch:{ Exception -> 0x001c }
            com.epoint.android.games.mjfgbfree.street.a r8 = new com.epoint.android.games.mjfgbfree.street.a     // Catch:{ Exception -> 0x001c }
            java.lang.String r9 = "titleNoFormatting"
            java.lang.String r9 = r7.getString(r9)     // Catch:{ Exception -> 0x001c }
            java.lang.String r10 = "streetAddress"
            java.lang.String r10 = r7.getString(r10)     // Catch:{ Exception -> 0x001c }
            java.lang.String r11 = "people_count"
            int r7 = r7.getInt(r11)     // Catch:{ Exception -> 0x001c }
            r0 = r8
            r1 = r20
            r2 = r9
            r3 = r10
            r4 = r7
            r0.<init>(r1, r2, r3, r4)     // Catch:{ Exception -> 0x001c }
            r0 = r20
            java.util.Vector r0 = r0.E     // Catch:{ Exception -> 0x001c }
            r7 = r0
            r7.add(r8)     // Catch:{ Exception -> 0x001c }
            int r6 = r6 + 1
            goto L_0x0280
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.street.PlacesMapActivity.handleMessage(android.os.Message):boolean");
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        aV();
    }

    public void onCreate(Bundle bundle) {
        super.a(bundle, "Places Map");
        requestWindowFeature(5);
        setContentView(C0000R.layout.map);
        this.fG = (ListView) findViewById(C0000R.id.place_list);
        this.fD = findViewById(C0000R.id.mapview);
        this.fD.setBuiltInZoomControls(true);
        this.fE = this.fD.getController();
        this.fE.setZoom(17);
        this.fF = new e(this, getResources().getDrawable(C0000R.drawable.marker));
        aV();
        if (this.fH == 1001.0d) {
            this.bw = (LocationManager) getSystemService("location");
            this.bw.requestLocationUpdates("network", 0, 0.0f, this);
            this.bw.requestLocationUpdates("gps", 0, 0.0f, this);
            new g(this).start();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, "Search...").setIcon(17301583);
        return super.onCreateOptionsMenu(menu);
    }

    public void onDestroy() {
        if (this.bw != null) {
            this.bw.removeUpdates(this);
        }
        super.onDestroy();
    }

    public void onLocationChanged(Location location) {
        if (this.fH == 1001.0d) {
            this.fH = (double) ((float) location.getLatitude());
            this.fI = (double) ((float) location.getLongitude());
            this.bw.removeUpdates(this);
            a(new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d)));
        }
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [android.content.Context, com.epoint.android.games.mjfgbfree.street.PlacesMapActivity, com.epoint.android.games.mjfgbfree.baseactivities.BaseMapActivity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean onOptionsItemSelected(android.view.MenuItem r5) {
        /*
            r4 = this;
            int r0 = r5.getItemId()
            switch(r0) {
                case 1: goto L_0x000c;
                default: goto L_0x0007;
            }
        L_0x0007:
            boolean r0 = super.onOptionsItemSelected(r5)
            return r0
        L_0x000c:
            android.view.LayoutInflater r0 = android.view.LayoutInflater.from(r4)
            r1 = 2130903064(0x7f030018, float:1.7412935E38)
            r2 = 0
            android.view.View r1 = r0.inflate(r1, r2)
            r0 = 2131361883(0x7f0a005b, float:1.834353E38)
            android.view.View r0 = r1.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.String r2 = r4.fC
            r0.setText(r2)
            android.app.AlertDialog$Builder r2 = new android.app.AlertDialog$Builder
            r2.<init>(r4)
            r3 = 17301583(0x108004f, float:2.4979476E-38)
            android.app.AlertDialog$Builder r2 = r2.setIcon(r3)
            java.lang.String r3 = "Search place"
            android.app.AlertDialog$Builder r2 = r2.setTitle(r3)
            android.app.AlertDialog$Builder r1 = r2.setView(r1)
            r2 = 17039370(0x104000a, float:2.42446E-38)
            com.epoint.android.games.mjfgbfree.street.i r3 = new com.epoint.android.games.mjfgbfree.street.i
            r3.<init>(r4, r0)
            android.app.AlertDialog$Builder r0 = r1.setPositiveButton(r2, r3)
            r1 = 17039360(0x1040000, float:2.424457E-38)
            com.epoint.android.games.mjfgbfree.street.j r2 = new com.epoint.android.games.mjfgbfree.street.j
            r2.<init>(r4)
            android.app.AlertDialog$Builder r0 = r0.setNegativeButton(r1, r2)
            android.app.AlertDialog r0 = r0.create()
            r0.show()
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.street.PlacesMapActivity.onOptionsItemSelected(android.view.MenuItem):boolean");
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
