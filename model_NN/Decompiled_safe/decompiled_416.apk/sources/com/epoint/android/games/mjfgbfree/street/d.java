package com.epoint.android.games.mjfgbfree.street;

import android.content.Intent;
import android.view.View;

final class d implements View.OnClickListener {
    private /* synthetic */ StreetMainActivity fP;

    d(StreetMainActivity streetMainActivity) {
        this.fP = streetMainActivity;
    }

    public final void onClick(View view) {
        this.fP.startActivity(new Intent(this.fP, PlacesMapActivity.class));
    }
}
