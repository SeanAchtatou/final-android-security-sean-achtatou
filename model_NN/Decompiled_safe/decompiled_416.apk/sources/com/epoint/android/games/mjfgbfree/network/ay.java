package com.epoint.android.games.mjfgbfree.network;

import android.view.View;
import com.epoint.android.games.mjfgbfree.af;

final class ay implements View.OnClickListener {
    private /* synthetic */ BTConnectionActivity pk;

    ay(BTConnectionActivity bTConnectionActivity) {
        this.pk = bTConnectionActivity;
    }

    public final void onClick(View view) {
        if (this.pk.ag.isDiscovering()) {
            this.pk.a(3, this.pk.ao);
            return;
        }
        this.pk.aj.setEnabled(false);
        this.pk.ai.setText(af.aa("停止偵測"));
        BTConnectionActivity.l(this.pk);
    }
}
