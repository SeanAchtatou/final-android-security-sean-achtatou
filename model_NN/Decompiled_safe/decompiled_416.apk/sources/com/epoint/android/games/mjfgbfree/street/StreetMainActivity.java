package com.epoint.android.games.mjfgbfree.street;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import b.a.b;
import com.a.a.a.h;
import com.a.a.a.i;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.a.d;
import com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity;

public class StreetMainActivity extends BaseActivity implements Handler.Callback {
    /* access modifiers changed from: private */
    public static final String[] az = {"email", "publish_stream", "read_stream", "offline_access"};
    /* access modifiers changed from: private */
    public h aA = new h();
    private i aB = new i(this.aA);
    private Button aC;
    private View aD;
    private k aE;

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 2:
                cF();
                String str = (String) message.obj;
                if (str != null) {
                    try {
                        bm("Logged in as: " + ((b) ao(str)).getString("name"));
                        d.iq = this.aA.cO();
                        SharedPreferences.Editor edit = getSharedPreferences("facebook-session", 0).edit();
                        edit.putString("access_token", d.iq);
                        edit.putLong("expires_in", this.aA.cP());
                        edit.commit();
                        this.aC.setVisibility(8);
                        this.aD.setVisibility(0);
                        break;
                    } catch (Exception e) {
                        e.printStackTrace();
                        b(e);
                        break;
                    }
                }
                break;
            case 1000:
                cF();
                this.aC.setVisibility(0);
                this.aD.setVisibility(8);
                break;
        }
        return super.handleMessage(message);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        super.a(bundle, "Street Main");
        requestWindowFeature(5);
        setContentView((int) C0000R.layout.street_main);
        SharedPreferences sharedPreferences = getSharedPreferences("facebook-session", 0);
        this.aA.ar(sharedPreferences.getString("access_token", null));
        this.aA.b(sharedPreferences.getLong("expires_in", 0));
        this.aC = (Button) findViewById(C0000R.id.facebook_button);
        this.aC.setOnClickListener(new c(this));
        this.aD = findViewById(C0000R.id.main_menu_block);
        if (this.aA.cN()) {
            this.aC.setVisibility(8);
            this.aD.setVisibility(0);
            d.iq = this.aA.cO();
        }
        this.aE = new k(this);
        ((Button) findViewById(C0000R.id.explore_button)).setOnClickListener(new d(this));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, "Logout").setIcon(17301580);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 1:
                cE();
                SharedPreferences.Editor edit = getSharedPreferences("facebook-session", 0).edit();
                edit.clear();
                edit.commit();
                this.aB.a(this, this.aE);
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(1).setEnabled(this.aA.cN());
        return super.onPrepareOptionsMenu(menu);
    }
}
