package com.epoint.android.games.mjfgbfree.e;

import android.app.Activity;
import com.epoint.android.games.mjfgbfree.bb;
import com.epoint.android.games.mjfgbfree.d.h;
import com.epoint.android.games.mjfgbfree.ui.a.d;
import com.epoint.android.games.mjfgbfree.ui.a.i;
import java.util.Vector;

public abstract class a implements o {
    protected bb cu;
    protected Vector fq = new Vector();
    protected int fr = -1;
    protected d fs;
    protected boolean ft = true;
    protected Activity fu;

    public a(Activity activity, bb bbVar) {
        this.cu = bbVar;
        this.fu = activity;
    }

    private void aP() {
        this.fr++;
    }

    public void a(int i, i iVar) {
    }

    /* access modifiers changed from: protected */
    public final Object aQ() {
        return this.fq.elementAt(this.fr);
    }

    public void aR() {
        this.fs = this.cu.ei();
        this.fs.a((o) this);
    }

    public void aS() {
        this.fr = this.fq.size();
        this.fs.a((Thread) new p(this));
    }

    /* access modifiers changed from: protected */
    public final void aT() {
        int i = this.fr;
        while (true) {
            int i2 = i;
            if (i2 < this.fq.size()) {
                Object elementAt = this.fq.elementAt(i2);
                if (!(elementAt instanceof com.epoint.android.games.mjfgbfree.d.a) || ((com.epoint.android.games.mjfgbfree.d.a) elementAt).ec != 0) {
                    i = i2 + 1;
                } else {
                    this.fr = i2;
                    return;
                }
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void aU() {
        h hVar;
        if (this.fr == this.fq.size() - 1) {
            aS();
            return;
        }
        aP();
        Object aQ = aQ();
        if (aQ instanceof com.epoint.android.games.mjfgbfree.d.a) {
            com.epoint.android.games.mjfgbfree.d.a aVar = (com.epoint.android.games.mjfgbfree.d.a) aQ;
            if (aVar.ec == 2) {
                hVar = aVar.ed;
                this.fs.n(2);
            } else if (aVar.ec == 1) {
                hVar = aVar.ed;
                this.fs.n(0);
            } else {
                switch (aVar.ec) {
                    case 0:
                        this.fs.n(0);
                        break;
                    case 3:
                        this.cu.ec();
                        break;
                }
                aP();
                hVar = (h) aQ();
            }
        } else {
            hVar = (h) aQ;
        }
        if (hVar.yB != null) {
            this.cu.ax(hVar.yB);
        }
        this.fs.a(hVar.yz);
        this.fs.x(hVar.yA);
    }

    /* access modifiers changed from: protected */
    public final void g(int i) {
        if (i != com.epoint.android.games.mjfgbfree.d.a.dY) {
            this.fr = 0;
            while (this.fr < this.fq.size()) {
                Object elementAt = this.fq.elementAt(this.fr);
                if (!(elementAt instanceof com.epoint.android.games.mjfgbfree.d.a) || ((com.epoint.android.games.mjfgbfree.d.a) elementAt).ea != i) {
                    this.fr++;
                } else {
                    return;
                }
            }
        }
    }
}
