package com.epoint.android.games.mjfgbfree.street;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.TabHost;
import android.widget.TextView;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity;

public class PlaceActivity extends BaseActivity implements Handler.Callback {
    public boolean handleMessage(Message message) {
        return super.handleMessage(message);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        super.a(bundle, "Place");
        requestWindowFeature(5);
        setContentView((int) C0000R.layout.place);
        Bundle extras = getIntent().getExtras();
        ((TextView) findViewById(C0000R.id.place)).setText(extras.getString("place_name"));
        ((TextView) findViewById(C0000R.id.address)).setText(extras.getString("address"));
        ((TextView) findViewById(C0000R.id.message)).setText("People: " + extras.getInt("people_count"));
        TabHost tabHost = (TabHost) findViewById(C0000R.id.tab_host);
        tabHost.setup();
        tabHost.addTab(tabHost.newTabSpec("summary").setIndicator("Summary").setContent((int) C0000R.id.summary));
        tabHost.addTab(tabHost.newTabSpec("top_in_3months").setIndicator("3 months top").setContent((int) C0000R.id.top_in_3months));
        tabHost.addTab(tabHost.newTabSpec("top_all_time").setIndicator("All time top").setContent((int) C0000R.id.top_all_time));
    }
}
