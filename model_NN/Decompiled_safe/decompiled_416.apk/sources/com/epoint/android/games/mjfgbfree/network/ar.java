package com.epoint.android.games.mjfgbfree.network;

import android.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import com.epoint.android.games.mjfgbfree.af;

final class ar implements View.OnClickListener {
    final /* synthetic */ TCPLocalHostConnectionActivity hZ;

    ar(TCPLocalHostConnectionActivity tCPLocalHostConnectionActivity) {
        this.hZ = tCPLocalHostConnectionActivity;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.hZ);
        builder.setTitle(af.aa("設定IP地址"));
        LinearLayout linearLayout = new LinearLayout(this.hZ);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(5, 2, 5, 2);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        LinearLayout linearLayout2 = new LinearLayout(this.hZ);
        linearLayout2.setOrientation(1);
        linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        ScrollView scrollView = new ScrollView(this.hZ);
        scrollView.addView(linearLayout2);
        scrollView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        TCPLocalHostConnectionActivity.a(this.hZ, linearLayout2);
        linearLayout.addView(scrollView);
        EditText editText = new EditText(this.hZ);
        editText.setSingleLine();
        editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        editText.setText(this.hZ.kg.getText());
        linearLayout.addView(editText);
        builder.setView(linearLayout);
        builder.setPositiveButton(af.aa("好"), new aj(this, editText));
        builder.setNegativeButton(af.aa("取消"), new ak(this));
        this.hZ.kk = builder.create();
        this.hZ.kk.show();
    }
}
