package com.epoint.android.games.mjfgbfree;

import android.view.View;

final class be implements View.OnClickListener {
    private /* synthetic */ LocalFanHistoryActivity vh;

    be(LocalFanHistoryActivity localFanHistoryActivity) {
        this.vh = localFanHistoryActivity;
    }

    public final void onClick(View view) {
        this.vh.C.setClickable(false);
        this.vh.C.setChecked(true);
        this.vh.B.setClickable(true);
        this.vh.B.setChecked(false);
        this.vh.A.setClickable(true);
        this.vh.A.setChecked(false);
        this.vh.D.setClickable(true);
        this.vh.D.setChecked(false);
        this.vh.a(this.vh.z);
    }
}
