package com.epoint.android.games.mjfgbfree;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.View;
import com.epoint.android.games.mjfgbfree.ui.a;

final class ae extends View {
    private a js;
    private /* synthetic */ DiscardedTilesActivity mi;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ae(DiscardedTilesActivity discardedTilesActivity, Context context) {
        super(context);
        this.mi = discardedTilesActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x01bf  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0210 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onDraw(android.graphics.Canvas r21) {
        /*
            r20 = this;
            r5 = 128(0x80, float:1.794E-43)
            r6 = 0
            r7 = 0
            r8 = 0
            r0 = r21
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r8
            r0.drawARGB(r1, r2, r3, r4)
            r0 = r20
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.js
            r5 = r0
            r0 = r5
            r1 = r21
            r0.draw(r1)
            java.lang.String r5 = "已打出的牌"
            java.lang.String r5 = com.epoint.android.games.mjfgbfree.af.aa(r5)
            int r6 = r20.getWidth()
            int r6 = r6 / 2
            float r6 = (float) r6
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r7 = r0
            android.graphics.Rect r7 = r7.ji
            int r7 = r7.top
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r8 = r0
            int r8 = r8.jm
            int r7 = r7 + r8
            float r7 = (float) r7
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r8 = r0
            android.graphics.Paint$FontMetrics r8 = r8.jp
            float r8 = r8.descent
            float r7 = r7 - r8
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r8 = r0
            android.graphics.Paint r8 = r8.gN
            r0 = r21
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r8
            r0.drawText(r1, r2, r3, r4)
            android.graphics.PaintFlagsDrawFilter r5 = com.epoint.android.games.mjfgbfree.MJ16Activity.rk
            r0 = r21
            r1 = r5
            r0.setDrawFilter(r1)
            android.graphics.Rect r12 = new android.graphics.Rect
            r12.<init>()
            int r5 = com.epoint.android.games.mjfgbfree.c.d.qB
            int r5 = r5 + 2
            r6 = 4
            int[] r13 = new int[r6]
            r6 = 0
            r13[r6] = r5
            r6 = 1
            int r7 = r5 + 1
            r13[r6] = r7
            r6 = 2
            r7 = 1
            int r7 = r5 - r7
            r13[r6] = r7
            r6 = 3
            int r5 = r5 + 2
            r13[r6] = r5
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r5 = r0
            android.graphics.Rect r5 = r5.ji
            int r5 = r5.top
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r6 = r0
            int r6 = r6.jm
            int r5 = r5 + r6
            r6 = 1077936128(0x40400000, float:3.0)
            float r7 = com.epoint.android.games.mjfgbfree.MJ16Activity.rc
            float r6 = r6 * r7
            int r6 = (int) r6
            int r6 = r6 + r5
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r5 = r0
            java.util.Hashtable r5 = r5.je
            java.lang.String r7 = "arrow"
            java.lang.Object r5 = r5.get(r7)
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5
            android.graphics.Rect r14 = new android.graphics.Rect
            r7 = 0
            r8 = 0
            int r9 = r5.getWidth()
            int r10 = r5.getHeight()
            r14.<init>(r7, r8, r9, r10)
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r7 = r0
            int r15 = r7.jg
            r7 = 0
            r16 = r7
        L_0x00c9:
            com.epoint.android.games.mjfgbfree.d.g r7 = com.epoint.android.games.mjfgbfree.DiscardedTilesActivity.gO
            com.epoint.android.games.mjfgbfree.d.a.a[] r7 = r7.vU
            int r7 = r7.length
            r0 = r16
            r1 = r7
            if (r0 < r1) goto L_0x00dd
            r5 = 0
            r0 = r21
            r1 = r5
            r0.setDrawFilter(r1)
            return
        L_0x00dd:
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r7 = r0
            int r7 = r7.jo
            int r7 = r7 + r15
            r12.left = r7
            r7 = 0
            r17 = r7
            r7 = r6
        L_0x00ed:
            com.epoint.android.games.mjfgbfree.d.g r6 = com.epoint.android.games.mjfgbfree.DiscardedTilesActivity.gO
            com.epoint.android.games.mjfgbfree.d.a.a[] r6 = r6.vU
            r8 = r13[r16]
            int r8 = r8 % 4
            r6 = r6[r8]
            java.util.Vector r6 = r6.fW
            int r6 = r6.size()
            r0 = r17
            r1 = r6
            if (r0 < r1) goto L_0x010a
            int r6 = r16 + 1
            r16 = r6
            r6 = r7
            goto L_0x00c9
        L_0x010a:
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r6 = r0
            java.util.Hashtable r6 = r6.je
            com.epoint.android.games.mjfgbfree.d.g r8 = com.epoint.android.games.mjfgbfree.DiscardedTilesActivity.gO
            com.epoint.android.games.mjfgbfree.d.a.a[] r8 = r8.vU
            r9 = r13[r16]
            int r9 = r9 % 4
            r8 = r8[r9]
            java.util.Vector r8 = r8.fW
            r0 = r8
            r1 = r17
            java.lang.Object r8 = r0.elementAt(r1)
            java.lang.Object r6 = r6.get(r8)
            android.graphics.Bitmap r6 = (android.graphics.Bitmap) r6
            if (r17 == 0) goto L_0x0221
            int r8 = r12.left
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r9 = r0
            int r9 = r9.jf
            r10 = 1
            int r9 = r9 - r10
            int r8 = r8 + r9
            r12.left = r8
            int r8 = r12.left
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r9 = r0
            int r9 = r9.jf
            int r8 = r8 + r9
            int r9 = r20.getWidth()
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r10 = r0
            int r10 = r10.jo
            int r9 = r9 - r10
            if (r8 <= r9) goto L_0x0221
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r8 = r0
            int r8 = r8.jo
            int r8 = r8 + r15
            r12.left = r8
            float r7 = (float) r7
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r8 = r0
            int r8 = r8.jg
            float r8 = (float) r8
            r9 = 1090519040(0x41000000, float:8.0)
            float r10 = com.epoint.android.games.mjfgbfree.MJ16Activity.rc
            float r9 = r9 * r10
            float r8 = r8 - r9
            float r7 = r7 + r8
            int r7 = (int) r7
            r18 = r7
        L_0x017d:
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r7 = r0
            int r7 = r7.jg
            int r7 = r7 + 1
            int r7 = r7 * r16
            int r7 = r7 + r18
            r12.top = r7
            int r7 = r12.left
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r8 = r0
            int r8 = r8.jf
            int r7 = r7 + r8
            r12.right = r7
            int r7 = r12.top
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r8 = r0
            int r8 = r8.jg
            int r7 = r7 + r8
            r12.bottom = r7
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r7 = r0
            android.graphics.Rect r7 = r7.jh
            r8 = 0
            r0 = r21
            r1 = r6
            r2 = r7
            r3 = r12
            r4 = r8
            r0.drawBitmap(r1, r2, r3, r4)
            if (r17 != 0) goto L_0x0210
            android.graphics.Rect r19 = new android.graphics.Rect
            r0 = r19
            r1 = r12
            r0.<init>(r1)
            r0 = r20
            com.epoint.android.games.mjfgbfree.DiscardedTilesActivity r0 = r0.mi
            r6 = r0
            int r6 = r6.jo
            r0 = r6
            r1 = r19
            r1.left = r0
            r0 = r19
            int r0 = r0.left
            r6 = r0
            int r6 = r6 + r15
            r0 = r6
            r1 = r19
            r1.right = r0
            android.graphics.Matrix r6 = com.epoint.android.games.mjfgbfree.bb.mMatrix
            r6.reset()
            r6 = 0
            switch(r16) {
                case 1: goto L_0x0218;
                case 2: goto L_0x021b;
                case 3: goto L_0x021e;
                default: goto L_0x01e9;
            }
        L_0x01e9:
            android.graphics.Matrix r7 = com.epoint.android.games.mjfgbfree.bb.mMatrix
            float r6 = (float) r6
            r7.postRotate(r6)
            r6 = 0
            r7 = 0
            int r8 = r5.getWidth()
            int r9 = r5.getHeight()
            android.graphics.Matrix r10 = com.epoint.android.games.mjfgbfree.bb.mMatrix
            r11 = 1
            android.graphics.Bitmap r6 = android.graphics.Bitmap.createBitmap(r5, r6, r7, r8, r9, r10, r11)
            r7 = 0
            r0 = r21
            r1 = r6
            r2 = r14
            r3 = r19
            r4 = r7
            r0.drawBitmap(r1, r2, r3, r4)
            if (r6 == r5) goto L_0x0210
            r6.recycle()
        L_0x0210:
            int r6 = r17 + 1
            r17 = r6
            r7 = r18
            goto L_0x00ed
        L_0x0218:
            r6 = -90
            goto L_0x01e9
        L_0x021b:
            r6 = 90
            goto L_0x01e9
        L_0x021e:
            r6 = 180(0xb4, float:2.52E-43)
            goto L_0x01e9
        L_0x0221:
            r18 = r7
            goto L_0x017d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.ae.onDraw(android.graphics.Canvas):void");
    }

    public final void onSizeChanged(int i, int i2, int i3, int i4) {
        int i5;
        int max = Math.max((int) (((float) bb.dR()) * MJ16Activity.rc), (int) (30.0f * MJ16Activity.rc));
        this.mi.jp = this.mi.gN.getFontMetrics();
        this.mi.jm = ((int) (Math.abs(this.mi.jp.ascent) + this.mi.jp.descent + this.mi.jp.leading)) + ((int) (3.0f * MJ16Activity.rc));
        DiscardedTilesActivity discardedTilesActivity = this.mi;
        if (i <= i2 || bb.dR() <= 0) {
            i5 = (int) (((float) (i > i2 ? 40 : 70)) * MJ16Activity.rc);
        } else {
            i5 = (int) (15.0f * MJ16Activity.rc);
        }
        discardedTilesActivity.jk = i5;
        this.mi.je = bb.tE;
        this.mi.jf = (int) Math.ceil((double) (((float) ((Bitmap) this.mi.je.get("bc")).getWidth()) * MJ16Activity.rc));
        this.mi.jg = (int) Math.ceil((double) (((float) ((Bitmap) this.mi.je.get("bc")).getHeight()) * MJ16Activity.rc));
        DiscardedTilesActivity discardedTilesActivity2 = this.mi;
        discardedTilesActivity2.jf = (int) (((float) discardedTilesActivity2.jf) * 1.2f);
        DiscardedTilesActivity discardedTilesActivity3 = this.mi;
        discardedTilesActivity3.jg = (int) (((float) discardedTilesActivity3.jg) * 1.2f);
        if (i > i2) {
            int d = (this.mi.jf - 1) * 23;
            this.mi.jl = ((getWidth() - ((this.mi.jf - 1) * 18)) - this.mi.jf) / 2;
            int i6 = 0;
            while (true) {
                if (i6 >= DiscardedTilesActivity.gO.vU.length) {
                    break;
                } else if (DiscardedTilesActivity.gO.vU[i6].fW.size() >= 17) {
                    this.mi.jl = ((getWidth() - d) - this.mi.jf) / 2;
                    break;
                } else {
                    i6++;
                }
            }
        } else {
            this.mi.jl = ((getWidth() - ((this.mi.jf - 1) * 14)) - this.mi.jf) / 2;
        }
        int width = getWidth() - (this.mi.jl * 2);
        int height = (getHeight() - (this.mi.jk * 2)) - max;
        this.js = new a(width, height, -1, Color.rgb(128, 0, 0), 192, null);
        this.js.c((getWidth() - width) / 2, ((getHeight() - height) - (bb.ty == 1 ? max : -max)) / 2);
        this.mi.ji = this.js.getBounds();
        this.mi.jj = (this.js.bb() - this.mi.jf) / (this.mi.jf - 1);
        this.mi.jo = (getWidth() - (this.mi.jj * (this.mi.jf - 1))) / 2;
        this.mi.jh = new Rect();
        Rect i7 = this.mi.jh;
        this.mi.jh.left = 0;
        i7.top = 0;
        this.mi.jh.right = ((Bitmap) this.mi.je.get("bc")).getWidth();
        this.mi.jh.bottom = ((Bitmap) this.mi.je.get("bc")).getHeight();
        super.onSizeChanged(i, i2, i3, i4);
    }
}
