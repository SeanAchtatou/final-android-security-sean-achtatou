package com.epoint.android.games.mjfgbfree.admin;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import com.epoint.android.games.mjfgbfree.bb;

final class b extends View {
    private /* synthetic */ MJColorPicker eh;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    b(MJColorPicker mJColorPicker, Context context) {
        super(context);
        this.eh = mJColorPicker;
    }

    public final void onDraw(Canvas canvas) {
        canvas.drawColor(-1);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < 10) {
                canvas.drawBitmap((Bitmap) bb.tE.get("BK"), (float) (((((Bitmap) bb.tE.get("BK")).getWidth() - 1) * i2) + 20), 100.0f, (Paint) null);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }
}
