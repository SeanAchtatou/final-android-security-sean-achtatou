package com.epoint.android.games.mjfgbfree.network;

import android.content.DialogInterface;
import android.view.View;
import android.widget.LinearLayout;

final class h implements DialogInterface.OnClickListener {
    private /* synthetic */ bw fn;
    private final /* synthetic */ View fo;
    private final /* synthetic */ LinearLayout fp;

    h(bw bwVar, View view, LinearLayout linearLayout) {
        this.fn = bwVar;
        this.fo = view;
        this.fp = linearLayout;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.fn.sh.km.remove(this.fo.getTag());
        this.fn.sh.kl.remove(this.fo.getTag());
        TCPServerConnectionActivity.a(this.fn.sh, this.fp);
        this.fn.sh.f(this.fn.sh.km);
    }
}
