package com.epoint.android.games.mjfgbfree.a;

import android.view.GestureDetector;
import android.view.MotionEvent;

public final class g extends GestureDetector.SimpleOnGestureListener {
    private c sb;

    public g(c cVar) {
        this.sb = cVar;
    }

    public final boolean onDoubleTap(MotionEvent motionEvent) {
        return this.sb.bm();
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        try {
            if (motionEvent.getY() - motionEvent2.getY() <= 35.0f || Math.abs(f2) <= 0.0f) {
                if (motionEvent2.getY() - motionEvent.getY() > 35.0f && Math.abs(f2) > 0.0f) {
                    return Math.abs(motionEvent.getX() - motionEvent2.getX()) > 250.0f ? false : false;
                }
                if (motionEvent.getX() - motionEvent2.getX() > 120.0f && Math.abs(f) > 200.0f) {
                    return Math.abs(motionEvent.getY() - motionEvent2.getY()) > 250.0f ? false : false;
                }
                if (motionEvent2.getX() - motionEvent.getX() > 120.0f && Math.abs(f) > 200.0f) {
                    return Math.abs(motionEvent.getY() - motionEvent2.getY()) > 250.0f ? false : false;
                }
                return false;
            } else if (Math.abs(motionEvent.getX() - motionEvent2.getX()) > 250.0f) {
                return false;
            } else {
                return this.sb.bn();
            }
        } catch (Exception e) {
        }
    }

    public final boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        return this.sb.a(motionEvent);
    }
}
