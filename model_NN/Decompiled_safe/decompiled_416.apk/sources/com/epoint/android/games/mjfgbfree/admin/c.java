package com.epoint.android.games.mjfgbfree.admin;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import com.epoint.android.games.mjfgbfree.ao;

public final class c extends BaseAdapter {
    private /* synthetic */ MJColorPicker eh;
    private Context mContext;
    private int zA;

    public c(MJColorPicker mJColorPicker, Context context) {
        this.eh = mJColorPicker;
        this.mContext = context;
        TypedArray obtainStyledAttributes = mJColorPicker.obtainStyledAttributes(ao.ol);
        this.zA = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
    }

    public final int getCount() {
        return this.eh.jb.size();
    }

    public final Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imageView = new ImageView(this.mContext);
        imageView.setImageBitmap((Bitmap) this.eh.ja.elementAt(i));
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView.setLayoutParams(new Gallery.LayoutParams(110, 100));
        imageView.setBackgroundResource(this.zA);
        return imageView;
    }
}
