package com.epoint.android.games.mjfgbfree.e;

import android.app.Activity;
import com.epoint.android.games.mjfgbfree.bb;
import com.epoint.android.games.mjfgbfree.d.a;
import com.epoint.android.games.mjfgbfree.ui.a.i;

public final class r extends a {
    /* access modifiers changed from: private */
    public bb cu;

    public r(Activity activity, bb bbVar) {
        super(activity, bbVar);
        this.cu = bbVar;
    }

    public final void a(int i, i iVar) {
        switch (i) {
            case 0:
                a aVar = (a) super.aQ();
                if (this.cu.bR() >= aVar.eb) {
                    super.g(aVar.ee);
                    aR();
                    this.fs.bo();
                    break;
                } else {
                    this.cu.ea();
                    break;
                }
            case 1:
                super.aT();
                this.fr--;
                aU();
                break;
            case 2:
                if (!(super.aQ() instanceof a)) {
                    aU();
                    break;
                } else {
                    this.fs.a((Thread) new c(this));
                    break;
                }
            case 3:
                super.g(((a) super.aQ()).ee);
                this.fr--;
                aU();
                break;
            case 4:
                aS();
                break;
            case 6:
                if (this.ft) {
                    this.ft = false;
                }
                aU();
                break;
            case 8:
                if (this.ft) {
                    aR();
                    this.fs.n(1);
                    this.fs.bo();
                    break;
                }
                break;
        }
        super.a(i, iVar);
    }
}
