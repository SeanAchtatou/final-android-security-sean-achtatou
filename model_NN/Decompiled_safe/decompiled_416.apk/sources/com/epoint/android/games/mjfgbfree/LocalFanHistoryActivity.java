package com.epoint.android.games.mjfgbfree;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;
import com.epoint.android.games.mjfgbfree.d.b;
import com.epoint.android.games.mjfgbfree.d.d;
import com.epoint.android.games.mjfgbfree.d.i;
import com.epoint.android.games.mjfgbfree.ui.e;
import com.epoint.android.games.mjfgbfree.ui.entity.a;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Vector;

public class LocalFanHistoryActivity extends Activity {
    /* access modifiers changed from: private */
    public ToggleButton A;
    /* access modifiers changed from: private */
    public ToggleButton B;
    /* access modifiers changed from: private */
    public ToggleButton C;
    /* access modifiers changed from: private */
    public ToggleButton D;
    private Vector E = new Vector();
    private a F;
    private RelativeLayout G;
    private WebView H;
    private RelativeLayout.LayoutParams I;
    private int J;
    private GridView K;
    /* access modifiers changed from: private */
    public String v;
    /* access modifiers changed from: private */
    public String w;
    /* access modifiers changed from: private */
    public String z;

    /* access modifiers changed from: private */
    public void a(String str) {
        if (this.K != null) {
            this.K.setVisibility(8);
        }
        WebView webView = new WebView(this);
        webView.setLayoutParams(this.I);
        webView.setFadingEdgeLength(this.J);
        webView.setVerticalFadingEdgeEnabled(true);
        webView.setBackgroundColor(0);
        webView.setInitialScale(100);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new bi(this));
        if (this.H != null) {
            this.H.clearView();
            this.H.destroy();
            this.G.removeView(this.H);
        }
        this.H = webView;
        this.G.addView(this.H);
        this.H.loadDataWithBaseURL("/", "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"DTD/xhtml1-transitional.dtd\"><html><head><META HTTP-EQUIV=\"content-type\" CONTENT=\"text/html; charset=UTF-8\" /></head><body marginwidth=\"1\" marginheight=\"1\" leftmargin=\"1\" topmargin=\"1\">" + str + "</body>" + "</html>", "text/html", "UTF-8", "/");
    }

    private String d() {
        int i;
        StringBuilder sb = new StringBuilder();
        sb.append("<font color='#404040' style='font-size:" + (MJ16Activity.rb + 2) + "px'><table width='100%' style='text-align:center; padding:3px; border-style: dotted;border-width: 3px; border-color: teal;'>");
        Vector c = com.epoint.android.games.mjfgbfree.f.a.c(this);
        sb.append("<tr><td colspan='3' align='center'>" + af.aa("小統計只限本機遊戲") + "</td></tr>");
        sb.append("<tr>");
        for (int i2 = 0; i2 < c.size(); i2++) {
            d dVar = (d) c.elementAt(i2);
            if (MJ16Activity.qM && dVar.mo > (i = com.epoint.android.games.mjfgbfree.g.a.od[dVar.mp][0])) {
                dVar.mo = i;
            }
            int f = com.epoint.android.games.mjfgbfree.g.a.f(dVar.mp, dVar.mo);
            sb.append("<td width='32%' style='text-align:center; padding:3px; border-style:dotted; border-width:3px; font-size:90%; border-color:" + (dVar.mo == 0 ? "rgb(100,0,0)" : "teal") + ";'>" + dVar.mm + (dVar.mn == 9999 ? "+" : "-" + dVar.mn) + af.aa("番") + "<br>");
            sb.append("<span style='font-size:85%;" + (dVar.mo > 0 ? "color:rgb(10,60,10);" : "") + "'>");
            if (f < com.epoint.android.games.mjfgbfree.g.a.od[dVar.mp].length) {
                sb.append(dVar.mo);
                sb.append("/");
                sb.append(com.epoint.android.games.mjfgbfree.g.a.od[dVar.mp][MJ16Activity.qM ? 0 : f]);
            } else {
                sb.append(String.valueOf(com.epoint.android.games.mjfgbfree.g.a.od[dVar.mp][f - 1]) + "/" + com.epoint.android.games.mjfgbfree.g.a.od[dVar.mp][f - 1]);
            }
            sb.append("</span>");
            if (f > 0) {
                sb.append("<br><span style='width:100%; align=center;'>");
                for (int i3 = 0; i3 < f; i3++) {
                    sb.append("<img src=\"file:///android_asset/assets/images/star_on.png\" style='width:" + ((int) (MJ16Activity.rc * 16.0f)) + "px; height:" + ((int) (MJ16Activity.rc * 16.0f)) + "px;'>");
                }
                sb.append("</span>");
            }
            sb.append("</td>");
            if ((i2 + 1) % 3 == 0) {
                sb.append("</tr><tr>");
            }
        }
        sb.append("</tr>");
        sb.append("<tr><td colspan='3' align='center'>" + (MJ16Activity.qM ? af.aa("Free版只記錄至一星級") : "") + "</td></tr>");
        sb.append("</table></font>");
        return sb.toString();
    }

    private String e() {
        StringBuilder sb = new StringBuilder();
        Vector b2 = com.epoint.android.games.mjfgbfree.f.a.b(this);
        sb.append("<font color='#404040' style='font-size:" + (MJ16Activity.rb + 2) + "px'><table width='100%' style='text-align:center; padding:3px; border-style: dotted;border-width: 3px; border-color: teal;'>");
        if (b2.size() == 0) {
            sb.append("<tr><td colspan='3' align='center'>");
            sb.append(af.aa("沒有紀錄"));
            sb.append("</td></tr>");
        } else {
            sb.append("<tr><td colspan='3' align='center'>" + af.aa("小統計只限本機遊戲") + "</td></tr>");
            for (int i = 0; i < b2.size(); i++) {
                Vector c = com.epoint.android.games.mjfgbfree.f.a.c(this, ((Integer) b2.elementAt(i)).intValue());
                sb.append("<tr><td colspan='3' align='center'>");
                sb.append(" (" + b2.elementAt(i) + " " + af.aa("盤") + ")");
                sb.append("</td></tr>");
                for (int i2 = 0; i2 < c.size(); i2++) {
                    b bVar = (b) c.elementAt(i2);
                    Calendar instance = Calendar.getInstance();
                    instance.setTime(bVar.ip);
                    sb.append("<tr><td>" + (i2 + 1) + ".</td><td>" + (String.valueOf(instance.get(1)) + "-" + (instance.get(2) + 1) + "-" + instance.get(5)) + ": #" + bVar.io + "</td>");
                    sb.append("<td align='right'>$" + bVar.gH + "</td></tr>");
                }
            }
        }
        sb.append("</table></font>");
        return sb.toString();
    }

    static /* synthetic */ void h(LocalFanHistoryActivity localFanHistoryActivity) {
        if (localFanHistoryActivity.H != null) {
            localFanHistoryActivity.H.clearView();
            localFanHistoryActivity.H.destroy();
            localFanHistoryActivity.G.removeView(localFanHistoryActivity.H);
            localFanHistoryActivity.H = null;
        }
        if (localFanHistoryActivity.K == null) {
            localFanHistoryActivity.K = (GridView) localFanHistoryActivity.G.findViewById(C0000R.id.items_grid);
            Hashtable e = com.epoint.android.games.mjfgbfree.f.a.e(localFanHistoryActivity);
            for (int i = 1; i <= 59; i++) {
                if (!e.containsKey(Integer.valueOf(i)) || ((Integer) e.get(Integer.valueOf(i))).intValue() <= 0) {
                    localFanHistoryActivity.E.add(new com.epoint.android.games.mjfgbfree.ui.entity.d(e.e(localFanHistoryActivity, 99999), " ", "", i));
                } else {
                    localFanHistoryActivity.E.add(new com.epoint.android.games.mjfgbfree.ui.entity.d(e.e(localFanHistoryActivity, i), ((Integer) e.get(Integer.valueOf(i))).intValue() > 1 ? " x " + e.get(Integer.valueOf(i)) : " ", "", i));
                }
            }
            localFanHistoryActivity.F = new a(localFanHistoryActivity, localFanHistoryActivity.E);
            localFanHistoryActivity.K.setAdapter((ListAdapter) localFanHistoryActivity.F);
        }
        localFanHistoryActivity.K.setVisibility(0);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        String str;
        int i;
        super.onCreate(bundle);
        MJ16Activity.dd();
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(this).inflate((int) C0000R.layout.local_fan_history_activity, (ViewGroup) null);
        this.H = (WebView) linearLayout.findViewById(C0000R.id.web_view);
        this.I = (RelativeLayout.LayoutParams) this.H.getLayoutParams();
        this.J = this.H.getVerticalFadingEdgeLength();
        this.G = (RelativeLayout) this.H.getParent();
        ((ImageView) linearLayout.findViewById(C0000R.id.bg)).setImageBitmap(e.y(this));
        setContentView(linearLayout);
        this.A = (ToggleButton) findViewById(C0000R.id.btn_type_history);
        this.v = af.aa("牌型紀錄");
        this.A.setText(this.v);
        this.A.setTextOn(this.v);
        this.A.setTextOff(this.v);
        this.A.setChecked(true);
        this.A.setOnClickListener(new bg(this));
        this.C = (ToggleButton) findViewById(C0000R.id.btn_fan_history);
        this.z = af.aa("番數統計");
        this.C.setText(this.z);
        this.C.setTextOn(this.z);
        this.C.setTextOff(this.z);
        this.C.setOnClickListener(new be(this));
        this.B = (ToggleButton) findViewById(C0000R.id.btn_all_time);
        this.w = af.aa("績分榜");
        this.B.setText(this.w);
        this.B.setTextOn(this.w);
        this.B.setTextOff(this.w);
        this.B.setOnClickListener(new bf(this));
        this.D = (ToggleButton) findViewById(C0000R.id.btn_item);
        String Z = af.Z("物品");
        this.D.setText(Z);
        this.D.setTextOn(Z);
        this.D.setTextOff(Z);
        this.D.setOnClickListener(new bh(this));
        if (MJ16Activity.rd) {
            setRequestedOrientation(0);
        } else {
            setRequestedOrientation(ai.nf);
        }
        Vector d = com.epoint.android.games.mjfgbfree.f.a.d(this);
        StringBuilder sb = new StringBuilder();
        int i2 = 1;
        for (int i3 = 0; i3 < d.size(); i3++) {
            i iVar = (i) d.elementAt(i3);
            if (iVar.mp != 0) {
                if (MJ16Activity.qM && iVar.mo > (i = com.epoint.android.games.mjfgbfree.g.a.oc[iVar.mp][0])) {
                    iVar.mo = i;
                }
                int e = com.epoint.android.games.mjfgbfree.g.a.e(iVar.mp, iVar.mo);
                String str2 = iVar.nY;
                int i4 = MJ16Activity.rb;
                String ab = af.ab(iVar.nY);
                int A2 = af.A(iVar.AU);
                if (iVar.AV == 999) {
                    A2 = 9;
                    ab = af.ab(str2);
                    str = String.valueOf("<img src=\"file:///android_asset/assets/images/") + "exp3";
                } else if (iVar.AV == 998) {
                    A2 = 9;
                    ab = af.ab(str2);
                    str = String.valueOf("<img src=\"file:///android_asset/assets/images/") + "exp2";
                } else {
                    String d2 = af.d(str2, A2);
                    str = d2 == null ? "" : String.valueOf("<img src=\"file:///android_asset/assets/images/") + d2;
                }
                if (iVar.mo == 0) {
                    str = "";
                    A2 = 0;
                }
                if (!str.equals("")) {
                    str = String.valueOf(str) + ".png\" style='width:" + ((int) (24.0f * MJ16Activity.rc)) + "px; height:" + ((int) (24.0f * MJ16Activity.rc)) + "px;' />";
                }
                String substring = ab.substring(0, 1);
                String substring2 = ab.substring(1);
                sb.append("<td width='32%' style='text-align:center; padding:3px; border-style:dotted; border-width:3px; font-size:90%; border-color: " + (iVar.mo == 0 ? "rgb(100,0,0)" : "teal") + ";'>" + ((A2 <= 2 || iVar.nY.equals("門清自摸")) ? (iVar.mo <= 0 || (!iVar.nY.equals("自摸") && !iVar.nY.equals("門清自摸") && !iVar.nY.equals("不求人"))) ? "<span style='white-space:nowrap'>" + str + substring + "</span>" + substring2 : "<font color='#800000'><span style='white-space:nowrap'>" + str + substring + "</span>" + substring2 + "</font>" : "<font color='#800000' style='font-size:" + i4 + "px'><span style='white-space:nowrap'>" + str + substring + "</span>" + substring2 + "</font>") + "<br>");
                sb.append("<span style='font-size:85%;" + (iVar.mo > 0 ? "color:rgb(10,60,10);" : "") + "'>");
                if (e < com.epoint.android.games.mjfgbfree.g.a.oc[iVar.mp].length) {
                    sb.append(iVar.mo);
                    sb.append("/");
                    sb.append(com.epoint.android.games.mjfgbfree.g.a.oc[iVar.mp][MJ16Activity.qM ? 0 : e]);
                } else {
                    sb.append(String.valueOf(com.epoint.android.games.mjfgbfree.g.a.oc[iVar.mp][e - 1]) + "/" + com.epoint.android.games.mjfgbfree.g.a.oc[iVar.mp][e - 1]);
                }
                sb.append("</span>");
                if (e > 0) {
                    sb.append("<br><span style='width:100%; align=center;'>");
                    for (int i5 = 0; i5 < e; i5++) {
                        sb.append("<img src=\"file:///android_asset/assets/images/star_on.png\" style='width:" + ((int) (16.0f * MJ16Activity.rc)) + "px; height:" + ((int) (16.0f * MJ16Activity.rc)) + "px;'>");
                    }
                    sb.append("</span>");
                }
                sb.append("</td>");
                int i6 = i2 + 1;
                if (i2 % 3 == 0) {
                    sb.append("</tr><tr>");
                }
                i2 = i6;
            }
        }
        this.v = "<font color='#404040' style='font-size:" + (MJ16Activity.rb + 2) + "px'><table width='100%' style='text-align:center; padding:3px; border-style: dotted;border-width: 3px; border-color: teal;'>" + "<tr><td colspan='3' align='center'>" + af.aa("小統計只限本機遊戲") + "</td></tr>" + "<tr>" + ((Object) sb) + "</tr>" + "<tr><td colspan='3' align='center'>" + (MJ16Activity.qM ? af.aa("Free版只記錄至一星級") : "") + "</td></tr>" + "</table></font>";
        this.z = d();
        this.w = e();
        a(this.v);
    }

    public void onDestroy() {
        e.gt();
        super.onDestroy();
    }
}
