package com.epoint.android.games.mjfgbfree.ui.entity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.preference.ListPreference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.af;
import java.util.Vector;

public class CheckboxListPreference extends ListPreference implements View.OnClickListener {
    private Vector X = new Vector();
    private DialogInterface.OnDismissListener Y = null;
    private CharSequence[] Z;
    private Context aa;

    public CheckboxListPreference(Context context) {
        super(context);
        this.aa = context;
    }

    public CheckboxListPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.aa = context;
    }

    public final void a(DialogInterface.OnDismissListener onDismissListener) {
        this.Y = onDismissListener;
    }

    public final boolean[] f() {
        if (this.X.size() <= 0) {
            return null;
        }
        boolean[] zArr = new boolean[this.X.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.X.size()) {
                return zArr;
            }
            zArr[i2] = ((CheckBox) this.X.elementAt(i2)).isChecked();
            i = i2 + 1;
        }
    }

    public void onClick(View view) {
        CheckBox checkBox = (CheckBox) this.X.elementAt(((Integer) view.getTag()).intValue());
        checkBox.setChecked(!checkBox.isChecked());
    }

    public void onDialogClosed(boolean z) {
        if (z && this.Y != null) {
            this.Y.onDismiss(getDialog());
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        ScrollView scrollView = new ScrollView(this.aa);
        LinearLayout linearLayout = new LinearLayout(this.aa);
        scrollView.addView(linearLayout);
        linearLayout.setOrientation(1);
        this.Z = getEntries();
        this.X.clear();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.Z.length) {
                builder.setView(scrollView);
                builder.setPositiveButton(af.aa("好"), this);
                builder.setNegativeButton(af.aa("取消"), this);
                return;
            }
            TableLayout tableLayout = (TableLayout) LayoutInflater.from(this.aa).inflate((int) C0000R.layout.checkbox_list_element, (ViewGroup) null);
            this.X.add((CheckBox) tableLayout.findViewById(C0000R.id.check_box));
            ((TextView) tableLayout.findViewById(C0000R.id.text)).setText(this.Z[i2]);
            tableLayout.setBackgroundResource(17301602);
            tableLayout.setTag(new Integer(i2));
            tableLayout.setClickable(true);
            tableLayout.setFocusable(true);
            tableLayout.setOnClickListener(this);
            linearLayout.addView(tableLayout);
            i = i2 + 1;
        }
    }
}
