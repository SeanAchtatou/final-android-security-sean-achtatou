package com.epoint.android.games.mjfgbfree;

import android.view.View;

final class bf implements View.OnClickListener {
    private /* synthetic */ LocalFanHistoryActivity vh;

    bf(LocalFanHistoryActivity localFanHistoryActivity) {
        this.vh = localFanHistoryActivity;
    }

    public final void onClick(View view) {
        this.vh.B.setClickable(false);
        this.vh.B.setChecked(true);
        this.vh.C.setClickable(true);
        this.vh.C.setChecked(false);
        this.vh.A.setClickable(true);
        this.vh.A.setChecked(false);
        this.vh.D.setClickable(true);
        this.vh.D.setChecked(false);
        this.vh.a(this.vh.w);
    }
}
