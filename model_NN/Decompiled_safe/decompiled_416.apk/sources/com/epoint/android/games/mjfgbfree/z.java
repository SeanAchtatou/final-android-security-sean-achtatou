package com.epoint.android.games.mjfgbfree;

import a.b.c.a.b;
import a.b.d.a;
import android.content.DialogInterface;
import android.widget.EditText;

final class z implements DialogInterface.OnClickListener {
    private final /* synthetic */ EditText cB;
    private final /* synthetic */ b iM;
    private /* synthetic */ MJ16ViewActivity iu;

    z(MJ16ViewActivity mJ16ViewActivity, EditText editText, b bVar) {
        this.iu = mJ16ViewActivity;
        this.cB = editText;
        this.iM = bVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String editable = this.cB.getText().toString();
        this.iM.gd = Integer.valueOf(editable).intValue();
        a cU = this.iu.cu.eu().cU();
        cU.r(this.iu.cu.cI());
        cU.q((cU.bU() + 3) % 4);
        cU.ce();
        this.iu.cu.er();
    }
}
