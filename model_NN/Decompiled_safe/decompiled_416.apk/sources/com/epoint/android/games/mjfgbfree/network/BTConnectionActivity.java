package com.epoint.android.games.mjfgbfree.network;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ToggleButton;
import b.a.b;
import b.a.f;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import com.epoint.android.games.mjfgbfree.MJ16ViewActivity;
import com.epoint.android.games.mjfgbfree.a.d;
import com.epoint.android.games.mjfgbfree.af;
import com.epoint.android.games.mjfgbfree.ai;
import java.io.IOException;
import java.util.Set;
import java.util.UUID;
import java.util.Vector;

public class BTConnectionActivity extends Activity implements t, Runnable {
    private static final UUID ae = UUID.nameUUIDFromBytes("MJ-GB1.3A".getBytes());
    /* access modifiers changed from: private */
    public int ac;
    /* access modifiers changed from: private */
    public Vector ad;
    private Vector af;
    /* access modifiers changed from: private */
    public BluetoothAdapter ag;
    /* access modifiers changed from: private */
    public BluetoothServerSocket ah;
    /* access modifiers changed from: private */
    public Button ai;
    /* access modifiers changed from: private */
    public Button aj;
    private Button ak;
    /* access modifiers changed from: private */
    public ToggleButton al;
    /* access modifiers changed from: private */
    public LinearLayout am;
    /* access modifiers changed from: private */
    public ScrollView an;
    /* access modifiers changed from: private */
    public Vector ao;
    /* access modifiers changed from: private */
    public ImageView ap;
    private final BroadcastReceiver aq;
    private Handler handler;

    public BTConnectionActivity() {
        this.ac = MJ16Activity.qL ? 1 : 3;
        this.ad = new Vector();
        this.handler = new be(this);
        this.aq = new bd(this);
    }

    /* access modifiers changed from: private */
    public void a(int i, Object obj) {
        Message message = new Message();
        message.what = i;
        message.obj = obj;
        this.handler.sendMessage(message);
    }

    static /* synthetic */ void a(BTConnectionActivity bTConnectionActivity, boolean z) {
        if (bTConnectionActivity.ah != null) {
            String bVar = b("command", "start_game").toString();
            for (int i = 0; i < bTConnectionActivity.ad.size(); i++) {
                ((aa) bTConnectionActivity.ad.elementAt(i)).d(bVar);
                ((aa) bTConnectionActivity.ad.elementAt(i)).a(null);
            }
            MJ16ViewActivity.ad = bTConnectionActivity.ad;
            bTConnectionActivity.ad = null;
            Intent intent = new Intent();
            intent.putExtra("is_restore_game", z);
            bTConnectionActivity.setResult(-1, intent);
            bTConnectionActivity.finish();
            return;
        }
        bTConnectionActivity.a((aa) bTConnectionActivity.ad.elementAt(0));
    }

    private void a(String str, String str2) {
        TextView textView = new TextView(this);
        textView.setTextColor(-1);
        textView.setText(str);
        textView.setTag(str2);
        textView.setBackgroundResource(17301602);
        textView.setClickable(true);
        textView.setFocusable(true);
        textView.setTextSize(20.0f);
        textView.setOnClickListener(new ax(this));
        textView.setPadding(4, 10, 4, 10);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setMinimumHeight(1);
        linearLayout.setBackgroundColor(-12303292);
        this.am.addView(textView);
        this.am.addView(linearLayout);
    }

    /* access modifiers changed from: private */
    public void a(Vector vector) {
        this.am.removeAllViews();
        TextView textView = new TextView(this);
        textView.setText(af.aa("已配對的裝置"));
        textView.setPadding(5, 2, 5, 2);
        textView.setTextColor(-16777216);
        textView.setBackgroundColor(-7829368);
        this.am.addView(textView);
        Set<BluetoothDevice> bondedDevices = this.ag.getBondedDevices();
        this.ap.setVisibility((bondedDevices.size() > 0 || (vector != null && vector.size() > 0)) ? 8 : 0);
        if (bondedDevices.size() > 0) {
            for (BluetoothDevice next : bondedDevices) {
                a(next.getName(), next.getAddress());
            }
        } else {
            TextView textView2 = new TextView(this);
            textView2.setText(af.aa("沒有已配對的裝置"));
            textView2.setPadding(5, 2, 5, 2);
            this.am.addView(textView2);
        }
        if (vector != null) {
            TextView textView3 = new TextView(this);
            textView3.setText(af.aa("新裝置"));
            textView3.setPadding(5, 2, 5, 2);
            textView3.setTextColor(-16777216);
            textView3.setBackgroundColor(-7829368);
            this.am.addView(textView3);
            if (vector.size() > 0) {
                for (int i = 0; i < vector.size(); i++) {
                    String[] split = ((String) vector.elementAt(i)).split("\n");
                    a(split[0], split[1]);
                }
                return;
            }
            TextView textView4 = new TextView(this);
            textView4.setText(af.aa("沒有其他新裝置"));
            textView4.setPadding(5, 2, 5, 2);
            this.am.addView(textView4);
        }
    }

    private void a(boolean z, String str) {
        TextView textView = new TextView(this);
        textView.setTextColor(-1);
        textView.setText(String.valueOf(z ? af.aa("主持") : af.aa("賓客")) + ": " + str);
        textView.setBackgroundResource(17301602);
        textView.setTextSize(20.0f);
        textView.setPadding(4, 10, 4, 10);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setMinimumHeight(1);
        linearLayout.setBackgroundColor(-12303292);
        this.am.addView(textView);
        this.am.addView(linearLayout);
        this.an.post(new aw(this));
    }

    private static b b(String str, String str2) {
        b bVar = new b();
        try {
            bVar.a(str, str2);
        } catch (f e) {
            e.printStackTrace();
        }
        return bVar;
    }

    /* access modifiers changed from: private */
    public boolean c(String str) {
        try {
            if (this.ag.isDiscovering()) {
                this.ag.cancelDiscovery();
            }
            BluetoothSocket createRfcommSocketToServiceRecord = this.ag.getRemoteDevice(str).createRfcommSocketToServiceRecord(ae);
            createRfcommSocketToServiceRecord.connect();
            bt btVar = new bt(createRfcommSocketToServiceRecord);
            btVar.b(0);
            this.ad.add(btVar);
            btVar.a(this);
            b b2 = b("join_game", ai.nm);
            b2.a("uuid", MJ16Activity.qK);
            btVar.d(b2.toString());
            setTitle(af.aa("已連線的玩家"));
            this.aj.setEnabled(false);
            this.ai.setEnabled(false);
            this.al.setEnabled(false);
            this.ap.setVisibility(8);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            a(2, af.aa("連線失敗"));
            a(3, this.ao);
            return true;
        }
    }

    private void g() {
        registerReceiver(this.aq, new IntentFilter("android.bluetooth.device.action.FOUND"));
        registerReceiver(this.aq, new IntentFilter("android.bluetooth.adapter.action.DISCOVERY_FINISHED"));
        registerReceiver(this.aq, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
        registerReceiver(this.aq, new IntentFilter("android.bluetooth.adapter.action.SCAN_MODE_CHANGED"));
        setContentView((int) C0000R.layout.bluetooth_connection_activity);
        this.ak = (Button) findViewById(C0000R.id.help);
        this.ak.setText(af.aa("幫助"));
        this.ak.setOnClickListener(new bb(this));
        this.am = (LinearLayout) findViewById(C0000R.id.devices);
        this.an = (ScrollView) this.am.getParent();
        this.al = (ToggleButton) findViewById(C0000R.id.discoverable);
        this.al.setText(af.aa("使裝置可被偵測"));
        this.al.setTextOff(this.al.getText());
        this.al.setTextOn(this.al.getText());
        this.al.setChecked(this.ag.getScanMode() == 23);
        this.al.setOnClickListener(new ba(this));
        this.ai = (Button) findViewById(C0000R.id.scan);
        this.ai.setText(af.aa("偵測新裝置"));
        this.aj = (Button) findViewById(C0000R.id.host);
        this.aj.setText(af.aa("主持遊戲"));
        this.aj.setOnClickListener(new az(this));
        this.ai.setOnClickListener(new ay(this));
        Bitmap c = d.c(this, "images/connect.png");
        this.ap = (ImageView) findViewById(C0000R.id.bg);
        this.ap.setImageBitmap(c);
        a((Vector) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.a(int, java.lang.Object):void
      com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.BTConnectionActivity, java.util.Vector):void
      com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.BTConnectionActivity, boolean):void
      com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.a(java.lang.String, java.lang.String):void
      com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.BTConnectionActivity, java.lang.String):boolean
      com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.aa, java.lang.String):void
      com.epoint.android.games.mjfgbfree.network.t.a(com.epoint.android.games.mjfgbfree.network.aa, java.lang.String):void
      com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.a(boolean, java.lang.String):void */
    /* access modifiers changed from: private */
    public void h() {
        this.am.removeAllViews();
        b b2 = b("host_name", ai.nm);
        a(true, ai.nm);
        int i = 0;
        while (i < this.ad.size()) {
            try {
                String o = ((aa) this.ad.elementAt(i)).o();
                b2.a("client_name" + i, o);
                a(false, o);
                i++;
            } catch (f e) {
                e.printStackTrace();
            }
        }
        String bVar = b2.toString();
        for (int i2 = 0; i2 < this.ad.size(); i2++) {
            ((aa) this.ad.elementAt(i2)).d(bVar);
        }
        i();
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0084  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void i() {
        /*
            r5 = this;
            r4 = 0
            monitor-enter(r5)
            java.util.Vector r0 = r5.ad     // Catch:{ all -> 0x0089 }
            int r0 = r0.size()     // Catch:{ all -> 0x0089 }
            if (r0 <= 0) goto L_0x0066
            android.bluetooth.BluetoothServerSocket r0 = r5.ah     // Catch:{ all -> 0x0089 }
            if (r0 == 0) goto L_0x0047
            java.util.Vector r0 = r5.af     // Catch:{ all -> 0x0089 }
            int r0 = r0.size()     // Catch:{ all -> 0x0089 }
            java.util.Vector r1 = r5.ad     // Catch:{ all -> 0x0089 }
            int r1 = r1.size()     // Catch:{ all -> 0x0089 }
            if (r0 != r1) goto L_0x0068
            r0 = 1
            r1 = r0
        L_0x001e:
            if (r1 == 0) goto L_0x0029
            r2 = r4
        L_0x0021:
            java.util.Vector r0 = r5.ad     // Catch:{ all -> 0x0089 }
            int r0 = r0.size()     // Catch:{ all -> 0x0089 }
            if (r2 < r0) goto L_0x006a
        L_0x0029:
            r0 = r1
        L_0x002a:
            android.widget.Button r1 = new android.widget.Button     // Catch:{ all -> 0x0089 }
            r1.<init>(r5)     // Catch:{ all -> 0x0089 }
            java.lang.String r2 = "繼續遊戲"
            java.lang.String r2 = com.epoint.android.games.mjfgbfree.af.aa(r2)     // Catch:{ all -> 0x0089 }
            r1.setText(r2)     // Catch:{ all -> 0x0089 }
            if (r0 == 0) goto L_0x0084
            com.epoint.android.games.mjfgbfree.network.bp r0 = new com.epoint.android.games.mjfgbfree.network.bp     // Catch:{ all -> 0x0089 }
            r0.<init>(r5)     // Catch:{ all -> 0x0089 }
            r1.setOnClickListener(r0)     // Catch:{ all -> 0x0089 }
        L_0x0042:
            android.widget.LinearLayout r0 = r5.am     // Catch:{ all -> 0x0089 }
            r0.addView(r1)     // Catch:{ all -> 0x0089 }
        L_0x0047:
            android.widget.Button r0 = new android.widget.Button     // Catch:{ all -> 0x0089 }
            r0.<init>(r5)     // Catch:{ all -> 0x0089 }
            android.bluetooth.BluetoothServerSocket r1 = r5.ah     // Catch:{ all -> 0x0089 }
            if (r1 == 0) goto L_0x008c
            java.lang.String r1 = "開始新遊戲"
            java.lang.String r1 = com.epoint.android.games.mjfgbfree.af.aa(r1)     // Catch:{ all -> 0x0089 }
        L_0x0056:
            r0.setText(r1)     // Catch:{ all -> 0x0089 }
            com.epoint.android.games.mjfgbfree.network.bq r1 = new com.epoint.android.games.mjfgbfree.network.bq     // Catch:{ all -> 0x0089 }
            r1.<init>(r5)     // Catch:{ all -> 0x0089 }
            r0.setOnClickListener(r1)     // Catch:{ all -> 0x0089 }
            android.widget.LinearLayout r1 = r5.am     // Catch:{ all -> 0x0089 }
            r1.addView(r0)     // Catch:{ all -> 0x0089 }
        L_0x0066:
            monitor-exit(r5)
            return
        L_0x0068:
            r1 = r4
            goto L_0x001e
        L_0x006a:
            java.util.Vector r3 = r5.af     // Catch:{ all -> 0x0089 }
            java.util.Vector r0 = r5.ad     // Catch:{ all -> 0x0089 }
            java.lang.Object r0 = r0.elementAt(r2)     // Catch:{ all -> 0x0089 }
            com.epoint.android.games.mjfgbfree.network.aa r0 = (com.epoint.android.games.mjfgbfree.network.aa) r0     // Catch:{ all -> 0x0089 }
            java.lang.String r0 = r0.p()     // Catch:{ all -> 0x0089 }
            boolean r0 = r3.contains(r0)     // Catch:{ all -> 0x0089 }
            if (r0 != 0) goto L_0x0080
            r0 = r4
            goto L_0x002a
        L_0x0080:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0021
        L_0x0084:
            r0 = 0
            r1.setEnabled(r0)     // Catch:{ all -> 0x0089 }
            goto L_0x0042
        L_0x0089:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x008c:
            java.lang.String r1 = "離開"
            java.lang.String r1 = com.epoint.android.games.mjfgbfree.af.aa(r1)     // Catch:{ all -> 0x0089 }
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.i():void");
    }

    static /* synthetic */ void l(BTConnectionActivity bTConnectionActivity) {
        if (bTConnectionActivity.ag.isDiscovering()) {
            bTConnectionActivity.ag.cancelDiscovery();
        }
        bTConnectionActivity.setProgressBarIndeterminateVisibility(true);
        bTConnectionActivity.setTitle(String.valueOf(af.aa("偵測中")) + "...");
        bTConnectionActivity.a((Vector) null);
        if (bTConnectionActivity.ao == null) {
            bTConnectionActivity.ao = new Vector();
        } else {
            bTConnectionActivity.ao.clear();
        }
        bTConnectionActivity.ag.startDiscovery();
    }

    public final synchronized void a(aa aaVar) {
        if (!aaVar.m()) {
            aaVar.l();
            this.ad.remove(aaVar);
            if (this.ah == null) {
                a(3, this.ao);
            } else {
                h();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.a(int, java.lang.Object):void
      com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.BTConnectionActivity, java.util.Vector):void
      com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.BTConnectionActivity, boolean):void
      com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.a(java.lang.String, java.lang.String):void
      com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.BTConnectionActivity, java.lang.String):boolean
      com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.a(com.epoint.android.games.mjfgbfree.network.aa, java.lang.String):void
      com.epoint.android.games.mjfgbfree.network.t.a(com.epoint.android.games.mjfgbfree.network.aa, java.lang.String):void
      com.epoint.android.games.mjfgbfree.network.BTConnectionActivity.a(boolean, java.lang.String):void */
    public final synchronized void a(aa aaVar, String str) {
        try {
            b bVar = new b(str);
            if (!bVar.H("join_game")) {
                aaVar.e(bVar.getString("join_game"));
                aaVar.f(bVar.O("uuid"));
                h();
            } else if (!bVar.H("host_name")) {
                this.am.removeAllViews();
                a(true, bVar.getString("host_name"));
                for (int i = 0; !bVar.H("client_name" + i); i++) {
                    a(false, bVar.getString("client_name" + i));
                }
                i();
            } else if (!bVar.H("command")) {
                String string = bVar.getString("command");
                if (string.equals("start_game")) {
                    MJ16ViewActivity.ad = this.ad;
                    aaVar.a(null);
                    this.ad = null;
                    setResult(-1);
                    finish();
                } else if (string.equals("disconnect")) {
                    a(aaVar);
                    a(2, af.aa("連線失敗"));
                }
            }
        } catch (f e) {
            e.printStackTrace();
        }
        return;
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        switch (i) {
            case 4:
                if (i2 == -1) {
                    g();
                    return;
                }
                setResult(0);
                finish();
                return;
            case 5:
                if (i2 == -1) {
                    this.al.setChecked(true);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.af = d.h(this);
        if (!MJ16Activity.rd || ai.nt) {
            setRequestedOrientation(ai.nf);
        } else {
            setRequestedOrientation(0);
        }
        requestWindowFeature(5);
        setTitle(af.aa("裝置清單"));
        this.ag = BluetoothAdapter.getDefaultAdapter();
        if (this.ag == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(String.valueOf(af.aa("未能啟動藍牙裝置")) + "!");
            builder.setPositiveButton(af.aa("好"), new bc(this));
            builder.setCancelable(false);
            builder.create().show();
        } else if (!this.ag.isEnabled()) {
            startActivityForResult(new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE"), 4);
        } else {
            g();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.ag != null && this.ag.isEnabled()) {
            if (this.ag.isDiscovering()) {
                this.ag.cancelDiscovery();
            }
            unregisterReceiver(this.aq);
        }
        if (this.ah != null) {
            try {
                this.ah.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (this.ad != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.ad.size()) {
                    ((aa) this.ad.elementAt(i2)).l();
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            if (this.ad.size() == 0) {
                setResult(0);
                finish();
            } else if (this.ad.size() > 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(String.valueOf(af.aa("取消連線")) + "?");
                builder.setPositiveButton(af.aa("是"), new bn(this));
                builder.setNegativeButton(af.aa("否"), new bo(this));
                builder.create().show();
            }
            return true;
        } else if (i == 84) {
            return true;
        } else {
            return super.onKeyDown(i, keyEvent);
        }
    }

    public void run() {
        try {
            if (this.ag.isDiscovering()) {
                a(3, this.ao);
            }
            this.ah = this.ag.listenUsingRfcommWithServiceRecord("BTMahjong", ae);
            while (true) {
                BluetoothSocket accept = this.ah.accept();
                if (this.ad.size() < this.ac) {
                    Message message = new Message();
                    message.what = 1;
                    message.obj = accept;
                    this.handler.sendMessage(message);
                } else {
                    accept.getOutputStream().write((b("command", "disconnect") + "\r\n").toString().getBytes());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            if (this.ad != null) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= this.ad.size()) {
                        break;
                    }
                    ((aa) this.ad.elementAt(i2)).l();
                    i = i2 + 1;
                }
                a(2, af.aa("已取消"));
                a(3, this.ao);
            }
            this.ah = null;
        }
    }
}
