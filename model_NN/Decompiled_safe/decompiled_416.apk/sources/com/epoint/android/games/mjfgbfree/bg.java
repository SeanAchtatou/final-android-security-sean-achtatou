package com.epoint.android.games.mjfgbfree;

import android.view.View;

final class bg implements View.OnClickListener {
    private /* synthetic */ LocalFanHistoryActivity vh;

    bg(LocalFanHistoryActivity localFanHistoryActivity) {
        this.vh = localFanHistoryActivity;
    }

    public final void onClick(View view) {
        this.vh.A.setClickable(false);
        this.vh.A.setChecked(true);
        this.vh.C.setClickable(true);
        this.vh.C.setChecked(false);
        this.vh.B.setClickable(true);
        this.vh.B.setChecked(false);
        this.vh.D.setClickable(true);
        this.vh.D.setChecked(false);
        this.vh.a(this.vh.v);
    }
}
