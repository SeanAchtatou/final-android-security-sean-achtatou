package com.epoint.android.games.mjfgbfree.tournament;

import android.app.AlertDialog;
import android.view.View;
import com.epoint.android.games.mjfgbfree.af;

final class g implements View.OnClickListener {
    private /* synthetic */ TournamentActivity su;

    g(TournamentActivity tournamentActivity) {
        this.su = tournamentActivity;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.su);
        builder.setMessage(String.valueOf("1. " + af.Z("以第一名完成賽事後，才可挑戰更高水平賽事。")) + "\n2. " + af.Z("每24小時自動補充10張遊戲劵。"));
        builder.setPositiveButton(af.aa("好"), new b(this));
        builder.show();
    }
}
