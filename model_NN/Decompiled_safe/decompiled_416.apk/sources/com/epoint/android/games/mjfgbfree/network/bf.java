package com.epoint.android.games.mjfgbfree.network;

import b.a.b;
import b.a.f;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import java.net.Socket;
import java.util.Hashtable;

public final class bf extends ab {
    private String aI;
    /* access modifiers changed from: private */
    public Hashtable qy = new Hashtable();

    public bf(Socket socket) {
        super(socket);
    }

    /* access modifiers changed from: protected */
    public final void a(aa aaVar) {
        super.a(aaVar);
    }

    /* access modifiers changed from: protected */
    public final void a(aa aaVar, String str) {
        try {
            if (this.qy.size() > 1) {
                b bVar = new b(str);
                if (!bVar.H("uuid") && (r0 = (aa) this.qy.get(bVar.get("uuid"))) != null) {
                    bVar.P("uuid");
                    super.a(r0, str);
                }
            }
        } catch (f e) {
            e.printStackTrace();
        }
        aa aaVar2 = aaVar;
        super.a(aaVar2, str);
    }

    public final aa at(String str) {
        c cVar = new c(this, str);
        this.qy.put(str, cVar);
        return cVar;
    }

    public final synchronized boolean d(String str) {
        String str2;
        if (MJ16Activity.qK.equals(this.aI)) {
            try {
                b bVar = new b(str);
                bVar.a("uuid", this.aI);
                str2 = bVar.toString();
            } catch (f e) {
                e.printStackTrace();
            }
        }
        str2 = str;
        return super.d(str2);
    }

    public final void f(String str) {
        if (this.aI != null) {
            this.qy.remove(this.aI);
        }
        this.qy.put(str, this);
        this.aI = str;
        super.f(str);
    }

    public final boolean l() {
        this.qy.clear();
        return super.l();
    }
}
