package com.epoint.android.games.mjfgbfree.network;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class bd extends BroadcastReceiver {
    private /* synthetic */ BTConnectionActivity pk;

    bd(BTConnectionActivity bTConnectionActivity) {
        this.pk = bTConnectionActivity;
    }

    public final void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("android.bluetooth.device.action.FOUND".equals(action)) {
            BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            if (bluetoothDevice.getBondState() != 12) {
                this.pk.ao.add(String.valueOf(bluetoothDevice.getName() == null ? bluetoothDevice.getAddress() : bluetoothDevice.getName()) + "\n" + bluetoothDevice.getAddress());
                this.pk.a(this.pk.ao);
            }
        } else if ("android.bluetooth.adapter.action.DISCOVERY_FINISHED".equals(action)) {
            this.pk.a(3, this.pk.ao);
        } else if ("android.bluetooth.adapter.action.STATE_CHANGED".equals(action)) {
            if (intent.getExtras().getInt("android.bluetooth.adapter.extra.STATE") == 10) {
                this.pk.setResult(0);
                this.pk.finish();
            }
        } else if ("android.bluetooth.adapter.action.SCAN_MODE_CHANGED".equals(action)) {
            this.pk.al.setChecked(intent.getExtras().getInt("android.bluetooth.adapter.extra.SCAN_MODE") == 23);
        }
    }
}
