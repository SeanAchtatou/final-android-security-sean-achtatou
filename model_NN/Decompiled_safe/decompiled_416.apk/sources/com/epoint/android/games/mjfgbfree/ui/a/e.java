package com.epoint.android.games.mjfgbfree.ui.a;

import android.graphics.Canvas;
import android.graphics.Rect;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import com.epoint.android.games.mjfgbfree.MJ16ViewActivity;
import com.epoint.android.games.mjfgbfree.ai;
import com.epoint.android.games.mjfgbfree.bb;
import com.epoint.android.games.mjfgbfree.ui.a;

public abstract class e implements i {
    public static volatile boolean jz = false;
    /* access modifiers changed from: private */
    public Thread iz = new Thread();
    private boolean jA = false;
    private int jB = ai.nk;
    protected MJ16ViewActivity jq;
    protected bb jr;
    protected a js;
    protected int jt;
    protected Rect ju;
    private boolean jv = true;
    protected boolean jw = false;
    private Object jx;
    /* access modifiers changed from: private */
    public int jy;

    public e() {
        jz = false;
    }

    public synchronized void a(Canvas canvas) {
        if (this.jB != ai.nk) {
            this.jB = ai.nk;
        }
        if (this.jA) {
            this.jr.dP().a(this.js);
        } else if (this.jt >= 4) {
            this.js.setBounds(this.ju);
            this.js.i(this.jy);
            if (this.jv) {
                this.js.c((MJ16Activity.qZ - this.js.bb()) / 2, (bb.ty == 1 ? 0 : bb.dR()) + (((MJ16Activity.ra - bb.dR()) - this.js.ba()) / 2));
                b(canvas);
                this.jr.dP().cH();
                this.jw = true;
            } else {
                this.jr.dP().cH();
                bk();
                this.jw = false;
            }
        }
    }

    public void a(Object obj) {
        if (obj != null) {
            this.jv = false;
            this.jx = obj;
        }
    }

    /* access modifiers changed from: protected */
    public final synchronized void b(Thread thread) {
        if (!jz) {
            jz = true;
            this.jt = 1;
            this.jA = true;
            this.jr.eg();
            this.jA = false;
            this.jr.es();
            this.iz = new k(this, thread);
            this.iz.start();
        }
    }

    public final boolean by() {
        return this.jw && !jz;
    }

    public final synchronized boolean h(boolean z) {
        boolean z2;
        if (jz) {
            z2 = false;
        } else {
            jz = true;
            this.jt = 1;
            this.jy = this.js.aZ();
            this.jv = true;
            this.iz = new j(this, z);
            this.iz.start();
            z2 = true;
        }
        return z2;
    }
}
