package com.epoint.android.games.mjfgbfree;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import com.epoint.android.games.mjfgbfree.f.a;

final class ar implements DialogInterface.OnClickListener {
    private /* synthetic */ MJ16Activity ok;

    ar(MJ16Activity mJ16Activity) {
        this.ok = mJ16Activity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setData(Uri.parse("market://details?id=com.epoint.android.games.mjfgbfree"));
        this.ok.startActivity(intent);
        a.a(this.ok, "is_wallpaper_unlocked", "Y");
        MJ16Activity.rf = true;
        MJ16Activity.rl.jP.a(false, true);
        MJ16Activity.rl.invalidate();
    }
}
