package com.epoint.android.games.mjfgbfree;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;

final class as implements DialogInterface.OnClickListener {
    private /* synthetic */ MJ16Activity ok;

    as(MJ16Activity mJ16Activity) {
        this.ok = mJ16Activity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case 0:
                MJ16Activity.a(this.ok, 0);
                return;
            case 1:
                MJ16Activity.a(this.ok, 2);
                return;
            case 2:
                Intent intent = new Intent(this.ok, MJ16ViewActivity.class);
                intent.putExtra("is_local_game", true);
                intent.putExtra("type", 4);
                intent.putExtra("is_restore", false);
                this.ok.startActivity(intent);
                return;
            case 3:
                if (Build.VERSION.SDK_INT >= 5) {
                    Intent intent2 = new Intent(this.ok, MJ16ViewActivity.class);
                    intent2.putExtra("is_network_game", true);
                    intent2.putExtra("is_bt_game", true);
                    intent2.putExtra("local_chara", ai.nn);
                    this.ok.startActivity(intent2);
                    return;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(this.ok);
                builder.setMessage(af.aa("此功能需要Android 2.1或以上"));
                builder.setNegativeButton(af.aa("好"), new ad(this));
                builder.setCancelable(true);
                builder.create().show();
                return;
            case 4:
                Intent intent3 = new Intent(this.ok, MJ16ViewActivity.class);
                intent3.putExtra("is_network_game", true);
                intent3.putExtra("is_tcp_localhost_game", true);
                intent3.putExtra("local_chara", ai.nn);
                this.ok.startActivity(intent3);
                return;
            case 5:
                Intent intent4 = new Intent(this.ok, MJ16ViewActivity.class);
                intent4.putExtra("is_network_game", true);
                intent4.putExtra("is_tcp_game", true);
                intent4.putExtra("local_chara", ai.nn);
                this.ok.startActivity(intent4);
                return;
            default:
                return;
        }
    }
}
