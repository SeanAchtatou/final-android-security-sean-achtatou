package com.epoint.android.games.mjfgbfree.network;

final class c implements aa {
    private int aG = -1;
    private String aH;
    private String aI;
    private boolean aJ = false;
    private /* synthetic */ bf aK;

    public c(bf bfVar, String str) {
        this.aK = bfVar;
        this.aI = str;
    }

    public final void a(t tVar) {
    }

    public final void b(int i) {
        this.aG = i;
    }

    public final boolean d(String str) {
        return !this.aJ;
    }

    public final void e(String str) {
        this.aH = str;
    }

    public final void f(String str) {
        this.aI = str;
    }

    public final boolean l() {
        this.aK.qy.remove(this.aI);
        this.aJ = true;
        return true;
    }

    public final boolean m() {
        return this.aJ;
    }

    public final int n() {
        return this.aG;
    }

    public final String o() {
        return this.aH;
    }

    public final String p() {
        return this.aI;
    }
}
