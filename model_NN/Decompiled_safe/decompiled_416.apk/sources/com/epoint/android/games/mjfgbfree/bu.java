package com.epoint.android.games.mjfgbfree;

import android.preference.ListPreference;
import android.preference.Preference;
import com.epoint.android.games.mjfgbfree.a.d;
import com.epoint.android.games.mjfgbfree.ui.entity.IconListPreference;

final class bu implements Preference.OnPreferenceChangeListener {
    private /* synthetic */ PreferencesActivity qn;

    bu(PreferencesActivity preferencesActivity) {
        this.qn = preferencesActivity;
    }

    public final boolean onPreferenceChange(Preference preference, Object obj) {
        preference.setSummary(((ListPreference) preference).getEntries()[Integer.valueOf((String) obj).intValue()]);
        ((IconListPreference) preference).c(d.c(this.qn, "tile_style/" + obj + ".png"));
        return true;
    }
}
