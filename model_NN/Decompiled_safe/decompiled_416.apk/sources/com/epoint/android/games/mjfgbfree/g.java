package com.epoint.android.games.mjfgbfree;

import android.view.View;

final class g implements View.OnClickListener {
    private /* synthetic */ InstructionActivity fU;

    g(InstructionActivity instructionActivity) {
        this.fU = instructionActivity;
    }

    public final void onClick(View view) {
        this.fU.sI.setClickable(false);
        this.fU.sI.setChecked(true);
        this.fU.sG.setClickable(true);
        this.fU.sG.setChecked(false);
        this.fU.sH.setClickable(true);
        this.fU.sH.setChecked(false);
        this.fU.a(this.fU.sL);
    }
}
