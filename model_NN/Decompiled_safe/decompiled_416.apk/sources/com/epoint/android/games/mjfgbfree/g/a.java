package com.epoint.android.games.mjfgbfree.g;

public final class a {
    public static final int[][] oc = new int[11][];
    public static final int[][] od = new int[11][];

    static {
        oc[1] = new int[]{1, 2, 3, 5, 8};
        oc[2] = new int[]{1, 3, 5, 8, 12};
        oc[3] = new int[]{2, 4, 6, 9, 15};
        oc[4] = new int[]{3, 6, 9, 12, 20};
        oc[5] = new int[]{3, 6, 10, 15, 22};
        oc[6] = new int[]{3, 7, 12, 20, 30};
        oc[7] = new int[]{5, 10, 20, 32, 50};
        oc[8] = new int[]{5, 15, 30, 50, 80};
        oc[9] = new int[]{5, 15, 25, 40, 60};
        oc[10] = new int[]{10, 20, 40, 70, 100};
        od[1] = new int[]{1, 2, 3, 5, 8};
        od[2] = new int[]{1, 3, 5, 8, 12};
        od[3] = new int[]{2, 4, 6, 9, 15};
        od[4] = new int[]{3, 6, 9, 12, 20};
        od[5] = new int[]{3, 6, 10, 15, 22};
        od[6] = new int[]{3, 7, 12, 20, 30};
        od[7] = new int[]{5, 10, 20, 32, 50};
        od[8] = new int[]{5, 15, 30, 50, 80};
        od[9] = new int[]{5, 15, 25, 40, 60};
        od[10] = new int[]{10, 20, 40, 70, 100};
    }

    public static int e(int i, int i2) {
        if (i > 0 && i <= 10) {
            int[] iArr = oc[i];
            for (int i3 = 0; i3 < iArr.length - 1; i3++) {
                if (i2 >= iArr[i3] && i2 < iArr[i3 + 1]) {
                    return i3 + 1;
                }
            }
            if (i2 >= iArr[iArr.length - 1]) {
                return iArr.length;
            }
        }
        return 0;
    }

    public static int f(int i, int i2) {
        if (i > 0 && i <= 10) {
            int[] iArr = od[i];
            for (int i3 = 0; i3 < iArr.length - 1; i3++) {
                if (i2 >= iArr[i3] && i2 < iArr[i3 + 1]) {
                    return i3 + 1;
                }
            }
            if (i2 >= iArr[iArr.length - 1]) {
                return iArr.length;
            }
        }
        return 0;
    }
}
