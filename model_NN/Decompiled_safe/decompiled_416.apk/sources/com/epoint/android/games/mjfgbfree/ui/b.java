package com.epoint.android.games.mjfgbfree.ui;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import com.epoint.android.games.mjfgbfree.bb;

public final class b {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static void a(Canvas canvas, Canvas canvas2, Bitmap bitmap, Bitmap bitmap2, int i, int i2, Paint paint) {
        bb.mMatrix.reset();
        int i3 = (i + 1) % 4 == i2 ? 1 : (i + 2) % 4 == i2 ? 2 : (i + 3) % 4 == i2 ? 3 : 0;
        bb.mMatrix.setRotate((float) ((4 - i3) * 90));
        canvas2.drawBitmap(bitmap2, (float) ((canvas2.getWidth() - bitmap2.getWidth()) / 2), (float) (bitmap.getHeight() - ((bitmap.getHeight() / 4) * 1)), paint);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), bb.mMatrix, true);
        int height = (((Bitmap) bb.tE.get("bkgnd")).getHeight() - MJ16Activity.ra) / 2;
        int width = (((Bitmap) bb.tE.get("bkgnd")).getWidth() - MJ16Activity.qZ) / 2;
        Rect rect = new Rect();
        if (i3 == 3) {
            rect.left = 0;
            rect.top = (((canvas.getHeight() - createBitmap.getHeight()) / 2) + 15) - (MJ16Activity.ra > MJ16Activity.qZ ? bb.dR() : 0);
            rect.bottom = rect.top + createBitmap.getHeight();
            rect.right = rect.left + createBitmap.getWidth();
        } else if (i3 == 2) {
            rect.left = (MJ16Activity.ri.getWidth() - createBitmap.getWidth()) / 2;
            rect.top = bb.ty == 0 ? bb.dR() : 0;
            rect.bottom = rect.top + createBitmap.getHeight();
            rect.right = rect.left + createBitmap.getWidth();
        } else if (i3 == 1) {
            rect.left = MJ16Activity.qZ - createBitmap.getWidth();
            rect.top = (((MJ16Activity.ra - createBitmap.getHeight()) / 2) + 15) - (MJ16Activity.ra > MJ16Activity.qZ ? bb.dR() : 0);
            rect.bottom = rect.top + createBitmap.getHeight();
            rect.right = rect.left + createBitmap.getWidth();
        }
        Rect rect2 = new Rect();
        rect2.left = width + rect.left;
        rect2.top = rect.top + height;
        rect2.right = rect2.left + createBitmap.getWidth();
        rect2.bottom = rect2.top + createBitmap.getHeight();
        canvas.drawBitmap((Bitmap) bb.tE.get("bkgnd"), rect2, rect, paint);
        rect2.left = 0;
        rect2.top = 0;
        rect2.right = createBitmap.getWidth();
        rect2.bottom = createBitmap.getHeight();
        canvas.drawBitmap(createBitmap, rect2, rect, paint);
    }
}
