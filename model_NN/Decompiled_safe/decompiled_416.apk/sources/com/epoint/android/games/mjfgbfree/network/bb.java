package com.epoint.android.games.mjfgbfree.network;

import android.content.Intent;
import android.view.View;
import com.epoint.android.games.mjfgbfree.ManualActivity;

final class bb implements View.OnClickListener {
    private /* synthetic */ BTConnectionActivity pk;

    bb(BTConnectionActivity bTConnectionActivity) {
        this.pk = bTConnectionActivity;
    }

    public final void onClick(View view) {
        Intent intent = new Intent(this.pk, ManualActivity.class);
        intent.putExtra("doc", "manual_bluetooth");
        this.pk.startActivity(intent);
    }
}
