package com.epoint.android.games.mjfgbfree.admin;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Gallery;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.a.d;
import com.epoint.android.games.mjfgbfree.ai;
import com.epoint.android.games.mjfgbfree.bb;
import java.util.Vector;

public class MJColorPicker extends Activity implements SeekBar.OnSeekBarChangeListener {
    SeekBar iR;
    SeekBar iS;
    SeekBar iT;
    TextView iU;
    TextView iV;
    TextView iW;
    private Canvas iX;
    private Bitmap iY;
    View iZ;
    Vector ja = new Vector();
    Vector jb = new Vector();
    private Gallery jc;
    int jd = -1;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.iY = Bitmap.createBitmap(((Bitmap) bb.tE.get("BK")).getWidth(), ((Bitmap) bb.tE.get("BK")).getHeight(), Bitmap.Config.ARGB_8888);
        this.iY.eraseColor(0);
        this.iX = new Canvas(this.iY);
        this.iZ = new b(this, this);
        RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(this).inflate((int) C0000R.layout.card_color_dialog, (ViewGroup) null);
        relativeLayout.addView(this.iZ);
        setContentView(relativeLayout);
        int red = Color.red(Integer.valueOf(ai.no).intValue());
        int green = Color.green(Integer.valueOf(ai.no).intValue());
        int blue = Color.blue(Integer.valueOf(ai.no).intValue());
        this.iR = (SeekBar) findViewById(C0000R.id.SeekBarRed);
        this.iR.bringToFront();
        this.iS = (SeekBar) findViewById(C0000R.id.SeekBarGreen);
        this.iS.bringToFront();
        this.iT = (SeekBar) findViewById(C0000R.id.SeekBarBlue);
        this.iT.bringToFront();
        this.iR.setProgress(red);
        this.iS.setProgress(green);
        this.iT.setProgress(blue);
        this.iU = (TextView) findViewById(C0000R.id.TextViewRed);
        this.iV = (TextView) findViewById(C0000R.id.TextViewGreen);
        this.iW = (TextView) findViewById(C0000R.id.TextViewBlue);
        this.iU.bringToFront();
        this.iV.bringToFront();
        this.iW.bringToFront();
        this.iU.setText(String.valueOf(this.iR.getProgress()));
        this.iV.setText(String.valueOf(this.iS.getProgress()));
        this.iW.setText(String.valueOf(this.iT.getProgress()));
        this.iR.setOnSeekBarChangeListener(this);
        this.iS.setOnSeekBarChangeListener(this);
        this.iT.setOnSeekBarChangeListener(this);
        this.jb.addElement(new Integer(Color.rgb(67, 130, 232)));
        this.jb.addElement(new Integer(Color.rgb(73, 177, 28)));
        this.jb.addElement(new Integer(Color.rgb(187, 59, 40)));
        this.jb.addElement(new Integer(Color.rgb(52, 98, 172)));
        this.jb.addElement(new Integer(Color.rgb(52, 126, 27)));
        this.jb.addElement(new Integer(Color.rgb(147, 54, 47)));
        this.jb.addElement(new Integer(Color.rgb(70, 153, 223)));
        this.jb.addElement(new Integer(Color.rgb(107, 198, 88)));
        this.jb.addElement(new Integer(Color.rgb(204, 79, 77)));
        this.jb.addElement(new Integer(Color.rgb(205, 100, 192)));
        this.jb.addElement(new Integer(Color.rgb(137, 78, 194)));
        this.jb.addElement(new Integer(Color.rgb(183, 96, 12)));
        this.jb.addElement(new Integer(Color.rgb(180, 135, 0)));
        this.jb.addElement(new Integer(Color.rgb(175, 168, 0)));
        this.jb.addElement(new Integer(Color.rgb(0, 79, 77)));
        this.jb.addElement(new Integer(Color.rgb(70, 70, 70)));
        this.jb.addElement(new Integer(Color.rgb(205, 100, 0)));
        for (int i = 0; i < this.jb.size(); i++) {
            Vector vector = this.ja;
            int intValue = ((Integer) this.jb.elementAt(i)).intValue();
            this.iX.drawBitmap(d.c(this, "images/bk.gif"), 0.0f, 0.0f, (Paint) null);
            this.iX.drawColor(intValue, PorterDuff.Mode.SCREEN);
            this.iX.drawBitmap((Bitmap) bb.tE.get("frame_none"), 0.0f, 0.0f, (Paint) null);
            this.iY.setPixel(0, 0, 0);
            this.iY.setPixel(this.iY.getWidth() - 1, 0, 0);
            this.iY.setPixel(0, this.iY.getHeight() - 1, 0);
            this.iY.setPixel(this.iY.getWidth() - 1, this.iY.getHeight() - 1, 0);
            vector.addElement(this.iY.copy(Bitmap.Config.ARGB_8888, false));
            if (Color.red(((Integer) this.jb.elementAt(i)).intValue()) == red && Color.green(((Integer) this.jb.elementAt(i)).intValue()) == green && Color.blue(((Integer) this.jb.elementAt(i)).intValue()) == blue) {
                this.jd = i;
            }
        }
        this.jc = (Gallery) findViewById(C0000R.id.gallery);
        this.jc.setOnItemSelectedListener(new a(this));
        this.jc.setAdapter((SpinnerAdapter) new c(this, this));
        if (this.jd != -1) {
            this.jc.setSelection(this.jd);
        }
        this.jc.bringToFront();
    }

    public void onDestroy() {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
        int rgb = Color.rgb(this.iR.getProgress(), this.iS.getProgress(), this.iT.getProgress());
        ai.no = String.valueOf(rgb);
        edit.putString("list_tile_color_preference", String.valueOf(rgb));
        edit.commit();
        super.onDestroy();
        setResult(-1);
        finish();
    }

    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        if (seekBar == this.iR) {
            this.iU.setText(String.valueOf(this.iR.getProgress()));
        } else if (seekBar == this.iS) {
            this.iV.setText(String.valueOf(this.iS.getProgress()));
        } else if (seekBar == this.iT) {
            this.iW.setText(String.valueOf(this.iT.getProgress()));
        }
        if (z) {
            bb.d(this, Color.rgb(Integer.valueOf(this.iU.getText().toString()).intValue(), Integer.valueOf(this.iV.getText().toString()).intValue(), Integer.valueOf(this.iW.getText().toString()).intValue()));
            this.iZ.invalidate();
        }
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}
