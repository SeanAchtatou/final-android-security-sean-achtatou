package com.epoint.android.games.mjfgbfree.network;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.epoint.android.games.mjfgbfree.af;
import java.net.Socket;

final class ce extends Handler {
    private /* synthetic */ TCPServerConnectionActivity sh;

    ce(TCPServerConnectionActivity tCPServerConnectionActivity) {
        this.sh = tCPServerConnectionActivity;
    }

    public final synchronized void handleMessage(Message message) {
        switch (message.what) {
            case 1:
            case 2:
                bf bfVar = new bf((Socket) message.obj);
                this.sh.ad.add(bfVar);
                bfVar.a(this.sh);
                if (message.what == 1) {
                    bfVar.b(0);
                    bfVar.f(this.sh.ka);
                }
                this.sh.ap.setVisibility(8);
                break;
            case 3:
                this.sh.setTitle(String.valueOf(af.aa("等待其他裝置連線")) + "...");
                this.sh.ke.setText(af.aa("停止等候"));
                this.sh.ke.setEnabled(true);
                this.sh.setProgressBarIndeterminateVisibility(false);
                this.sh.jY = (String) message.obj;
                this.sh.kj.setText("Key: " + ((String) message.obj));
                this.sh.kj.setVisibility(0);
                this.sh.kh.setVisibility(0);
                break;
            case 4:
                this.sh.setProgressBarIndeterminateVisibility(false);
                this.sh.setTitle(af.aa("連接伺服器"));
                this.sh.ki.removeAllViews();
                this.sh.ap.setVisibility(0);
                this.sh.kj.setVisibility(8);
                this.sh.kh.setVisibility(8);
                this.sh.kg.setEnabled(true);
                this.sh.kf.setEnabled(true);
                this.sh.ke.setEnabled(true);
                this.sh.ke.setText(af.aa("主持遊戲"));
                break;
            case 5:
                Toast.makeText(this.sh, (String) message.obj, 0).show();
                break;
            case 6:
                this.sh.setProgressBarIndeterminateVisibility(false);
                this.sh.kg.setText((String) message.obj);
                break;
        }
    }
}
