package com.epoint.android.games.mjfgbfree.c;

import b.a.b;

final class e extends Thread {
    private /* synthetic */ a lI;
    private b uY;

    public e(a aVar, b bVar) {
        this.lI = aVar;
        this.uY = bVar;
        synchronized (aVar.cv) {
            aVar.cv.add(this);
            if (aVar.cw == null) {
                aVar.cw = (Thread) aVar.cv.elementAt(0);
                aVar.cw.start();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:?, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0062 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r6 = this;
            r4 = 1
            r3 = 0
        L_0x0002:
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ f -> 0x00cd }
            com.epoint.android.games.mjfgbfree.bb r0 = r0.cu     // Catch:{ f -> 0x00cd }
            int r0 = r0.tD     // Catch:{ f -> 0x00cd }
            if (r0 != r4) goto L_0x00c0
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ f -> 0x00cd }
            com.epoint.android.games.mjfgbfree.bb r0 = r0.cu     // Catch:{ f -> 0x00cd }
            boolean r0 = r0.uu     // Catch:{ f -> 0x00cd }
            if (r0 != 0) goto L_0x00c0
            boolean r0 = com.epoint.android.games.mjfgbfree.bb.dU()     // Catch:{ f -> 0x00cd }
            if (r0 != 0) goto L_0x00c0
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ f -> 0x00cd }
            com.epoint.android.games.mjfgbfree.bb r0 = r0.cu     // Catch:{ f -> 0x00cd }
            com.epoint.android.games.mjfgbfree.av r0 = r0.dP()     // Catch:{ f -> 0x00cd }
            boolean r0 = r0.oR     // Catch:{ f -> 0x00cd }
            if (r0 != 0) goto L_0x00c0
            r0 = 200(0xc8, double:9.9E-322)
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x00d3 }
        L_0x002f:
            com.epoint.android.games.mjfgbfree.bb.f(r6)     // Catch:{ f -> 0x00cd }
            b.a.b r0 = r6.uY     // Catch:{ f -> 0x00cd }
            java.lang.String r1 = "command"
            boolean r0 = r0.H(r1)     // Catch:{ f -> 0x00cd }
            if (r0 != 0) goto L_0x00d9
            b.a.b r0 = r6.uY     // Catch:{ f -> 0x00cd }
            java.lang.String r1 = "command"
            java.lang.String r0 = r0.getString(r1)     // Catch:{ f -> 0x00cd }
            java.lang.String r1 = "Quit"
            boolean r0 = r0.equals(r1)     // Catch:{ f -> 0x00cd }
            if (r0 == 0) goto L_0x0189
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ f -> 0x00cd }
            r0.cx = false     // Catch:{ f -> 0x00cd }
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ f -> 0x00cd }
            com.epoint.android.games.mjfgbfree.bb r0 = r0.cu     // Catch:{ f -> 0x00cd }
            r0.ev()     // Catch:{ f -> 0x00cd }
            r1 = r3
        L_0x005b:
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI
            java.util.Vector r2 = r0.cv
            monitor-enter(r2)
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ all -> 0x016c }
            java.util.Vector r0 = r0.cv     // Catch:{ all -> 0x016c }
            r3 = 0
            r0.remove(r3)     // Catch:{ all -> 0x016c }
            com.epoint.android.games.mjfgbfree.bb.ez()     // Catch:{ all -> 0x016c }
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ all -> 0x016c }
            java.util.Vector r0 = r0.cv     // Catch:{ all -> 0x016c }
            int r0 = r0.size()     // Catch:{ all -> 0x016c }
            if (r0 <= 0) goto L_0x008d
            com.epoint.android.games.mjfgbfree.c.a r3 = r6.lI     // Catch:{ all -> 0x016c }
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ all -> 0x016c }
            java.util.Vector r0 = r0.cv     // Catch:{ all -> 0x016c }
            r4 = 0
            java.lang.Object r0 = r0.elementAt(r4)     // Catch:{ all -> 0x016c }
            java.lang.Thread r0 = (java.lang.Thread) r0     // Catch:{ all -> 0x016c }
            r3.cw = r0     // Catch:{ all -> 0x016c }
        L_0x008d:
            if (r1 == 0) goto L_0x00a9
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ all -> 0x016c }
            java.lang.Thread r0 = r0.cw     // Catch:{ all -> 0x016c }
            boolean r0 = r0.isAlive()     // Catch:{ all -> 0x016c }
            if (r0 == 0) goto L_0x016f
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ all -> 0x016c }
            com.epoint.android.games.mjfgbfree.bb r0 = r0.cu     // Catch:{ all -> 0x016c }
            r1 = 0
            r0.c(r1)     // Catch:{ all -> 0x016c }
        L_0x00a5:
            boolean r0 = com.epoint.android.games.mjfgbfree.ui.a.e.jz     // Catch:{ all -> 0x016c }
            if (r0 != 0) goto L_0x015f
        L_0x00a9:
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ all -> 0x016c }
            java.lang.Thread r0 = r0.cw     // Catch:{ all -> 0x016c }
            boolean r0 = r0.isAlive()     // Catch:{ all -> 0x016c }
            if (r0 != 0) goto L_0x0181
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ all -> 0x016c }
            java.lang.Thread r0 = r0.cw     // Catch:{ all -> 0x016c }
            r0.start()     // Catch:{ all -> 0x016c }
        L_0x00be:
            monitor-exit(r2)     // Catch:{ all -> 0x016c }
        L_0x00bf:
            return
        L_0x00c0:
            r0 = 100
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x00c7 }
            goto L_0x0002
        L_0x00c7:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ f -> 0x00cd }
            goto L_0x0002
        L_0x00cd:
            r0 = move-exception
            r1 = r3
        L_0x00cf:
            r0.printStackTrace()
            goto L_0x005b
        L_0x00d3:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ f -> 0x00cd }
            goto L_0x002f
        L_0x00d9:
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ f -> 0x00cd }
            b.a.b r1 = r6.uY     // Catch:{ f -> 0x00cd }
            b.a.b r1 = com.epoint.android.games.mjfgbfree.c.a.a(r1)     // Catch:{ f -> 0x00cd }
            int r2 = com.epoint.android.games.mjfgbfree.c.d.qB     // Catch:{ f -> 0x00cd }
            com.epoint.android.games.mjfgbfree.d.g r1 = com.epoint.android.games.mjfgbfree.af.a(r1, r2)     // Catch:{ f -> 0x00cd }
            r0.gO = r1     // Catch:{ f -> 0x00cd }
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ f -> 0x00cd }
            com.epoint.android.games.mjfgbfree.d.g r0 = r0.gO     // Catch:{ f -> 0x00cd }
            boolean r0 = r0.le     // Catch:{ f -> 0x00cd }
            if (r0 == 0) goto L_0x0120
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ f -> 0x00cd }
            com.epoint.android.games.mjfgbfree.bb r0 = r0.cu     // Catch:{ f -> 0x00cd }
            com.epoint.android.games.mjfgbfree.ui.a r0 = r0.ew()     // Catch:{ f -> 0x00cd }
            r1 = 1
            r0.d(r1)     // Catch:{ f -> 0x00cd }
            r0 = r3
        L_0x0100:
            com.epoint.android.games.mjfgbfree.c.a r1 = r6.lI     // Catch:{ f -> 0x0145 }
            com.epoint.android.games.mjfgbfree.d.g r1 = r1.gO     // Catch:{ f -> 0x0145 }
            int r1 = r1.li     // Catch:{ f -> 0x0145 }
            com.epoint.android.games.mjfgbfree.c.a r2 = r6.lI     // Catch:{ f -> 0x0145 }
            com.epoint.android.games.mjfgbfree.d.g r2 = r2.gO     // Catch:{ f -> 0x0145 }
            int r2 = r2.aG     // Catch:{ f -> 0x0145 }
            if (r1 == r2) goto L_0x014a
            com.epoint.android.games.mjfgbfree.c.a r1 = r6.lI     // Catch:{ f -> 0x0145 }
            com.epoint.android.games.mjfgbfree.d.g r1 = r1.gO     // Catch:{ f -> 0x0145 }
            int r1 = r1.lh     // Catch:{ f -> 0x0145 }
            switch(r1) {
                case 0: goto L_0x0133;
                case 1: goto L_0x0122;
                case 2: goto L_0x0122;
                case 3: goto L_0x0122;
                case 4: goto L_0x0122;
                case 5: goto L_0x0122;
                case 6: goto L_0x0122;
                case 7: goto L_0x0122;
                default: goto L_0x0117;
            }     // Catch:{ f -> 0x0145 }
        L_0x0117:
            boolean r1 = com.epoint.android.games.mjfgbfree.bb.dU()     // Catch:{ f -> 0x0145 }
            if (r1 != 0) goto L_0x0154
            r1 = r0
            goto L_0x005b
        L_0x0120:
            r0 = r4
            goto L_0x0100
        L_0x0122:
            com.epoint.android.games.mjfgbfree.c.a r1 = r6.lI     // Catch:{ f -> 0x0145 }
            com.epoint.android.games.mjfgbfree.c.a r2 = r6.lI     // Catch:{ f -> 0x0145 }
            com.epoint.android.games.mjfgbfree.d.g r2 = r2.gO     // Catch:{ f -> 0x0145 }
            int r2 = r2.li     // Catch:{ f -> 0x0145 }
            com.epoint.android.games.mjfgbfree.c.a r3 = r6.lI     // Catch:{ f -> 0x0145 }
            com.epoint.android.games.mjfgbfree.d.g r3 = r3.gO     // Catch:{ f -> 0x0145 }
            int r3 = r3.lh     // Catch:{ f -> 0x0145 }
            r1.a(r2, r3)     // Catch:{ f -> 0x0145 }
        L_0x0133:
            com.epoint.android.games.mjfgbfree.c.a r1 = r6.lI     // Catch:{ f -> 0x0145 }
            com.epoint.android.games.mjfgbfree.c.a r2 = r6.lI     // Catch:{ f -> 0x0145 }
            com.epoint.android.games.mjfgbfree.d.g r2 = r2.gO     // Catch:{ f -> 0x0145 }
            int r2 = r2.li     // Catch:{ f -> 0x0145 }
            com.epoint.android.games.mjfgbfree.c.a r3 = r6.lI     // Catch:{ f -> 0x0145 }
            com.epoint.android.games.mjfgbfree.d.g r3 = r3.gO     // Catch:{ f -> 0x0145 }
            int r3 = r3.lh     // Catch:{ f -> 0x0145 }
            r1.b(r2, r3)     // Catch:{ f -> 0x0145 }
            goto L_0x0117
        L_0x0145:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x00cf
        L_0x014a:
            com.epoint.android.games.mjfgbfree.c.a r1 = r6.lI     // Catch:{ f -> 0x0145 }
            com.epoint.android.games.mjfgbfree.bb r1 = r1.cu     // Catch:{ f -> 0x0145 }
            r1.ey()     // Catch:{ f -> 0x0145 }
            goto L_0x0117
        L_0x0154:
            r1 = 20
            java.lang.Thread.sleep(r1)     // Catch:{ InterruptedException -> 0x015a }
            goto L_0x0117
        L_0x015a:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ f -> 0x0145 }
            goto L_0x0117
        L_0x015f:
            r0 = 20
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x0166 }
            goto L_0x00a5
        L_0x0166:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x016c }
            goto L_0x00a5
        L_0x016c:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x016c }
            throw r0
        L_0x016f:
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ all -> 0x016c }
            com.epoint.android.games.mjfgbfree.bb r0 = r0.cu     // Catch:{ all -> 0x016c }
            com.epoint.android.games.mjfgbfree.c.a r1 = r6.lI     // Catch:{ all -> 0x016c }
            java.lang.Thread r1 = r1.cw     // Catch:{ all -> 0x016c }
            r0.c(r1)     // Catch:{ all -> 0x016c }
            monitor-exit(r2)     // Catch:{ all -> 0x016c }
            goto L_0x00bf
        L_0x0181:
            com.epoint.android.games.mjfgbfree.c.a r0 = r6.lI     // Catch:{ all -> 0x016c }
            r1 = 0
            r0.cw = r1     // Catch:{ all -> 0x016c }
            goto L_0x00be
        L_0x0189:
            r1 = r3
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.c.e.run():void");
    }
}
