package com.epoint.android.games.mjfgbfree.network;

import android.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import com.epoint.android.games.mjfgbfree.af;

final class bz implements View.OnClickListener {
    final /* synthetic */ TCPServerConnectionActivity sh;

    bz(TCPServerConnectionActivity tCPServerConnectionActivity) {
        this.sh = tCPServerConnectionActivity;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.sh);
        builder.setTitle(af.aa("伺服器"));
        LinearLayout linearLayout = new LinearLayout(this.sh);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(5, 2, 5, 2);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        LinearLayout linearLayout2 = new LinearLayout(this.sh);
        linearLayout2.setOrientation(1);
        linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        ScrollView scrollView = new ScrollView(this.sh);
        scrollView.addView(linearLayout2);
        scrollView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        TCPServerConnectionActivity.a(this.sh, linearLayout2);
        linearLayout.addView(scrollView);
        EditText editText = new EditText(this.sh);
        editText.setSingleLine();
        editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout.addView(editText);
        builder.setView(linearLayout);
        builder.setPositiveButton(af.aa("好"), new l(this, editText));
        builder.setNegativeButton(af.aa("取消"), new m(this));
        this.sh.kk = builder.create();
        this.sh.kk.show();
    }
}
