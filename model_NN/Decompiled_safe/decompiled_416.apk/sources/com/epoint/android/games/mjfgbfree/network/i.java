package com.epoint.android.games.mjfgbfree.network;

import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

final class i implements DialogInterface.OnClickListener {
    private final /* synthetic */ View fo;
    private final /* synthetic */ LinearLayout fp;
    private /* synthetic */ bu fv;
    private final /* synthetic */ EditText fw;
    private final /* synthetic */ EditText fx;

    i(bu buVar, EditText editText, EditText editText2, View view, LinearLayout linearLayout) {
        this.fv = buVar;
        this.fw = editText;
        this.fx = editText2;
        this.fo = view;
        this.fp = linearLayout;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String replace = this.fw.getText().toString().trim().replace(",", "").replace(";", "");
        String replace2 = this.fx.getText().toString().trim().replace(",", "").replace(";", "");
        String str = (String) this.fv.sh.kl.elementAt(((Integer) this.fo.getTag()).intValue());
        this.fv.sh.kl.remove(str);
        int indexOf = this.fv.sh.km.indexOf(str);
        this.fv.sh.km.remove(str);
        this.fv.sh.km.insertElementAt(String.valueOf(replace2) + ";" + replace, indexOf);
        this.fv.sh.kl.insertElementAt((String) this.fv.sh.km.elementAt(indexOf), ((Integer) this.fo.getTag()).intValue());
        dialogInterface.dismiss();
        this.fv.sh.f(this.fv.sh.km);
        TCPServerConnectionActivity.a(this.fv.sh, this.fp);
    }
}
