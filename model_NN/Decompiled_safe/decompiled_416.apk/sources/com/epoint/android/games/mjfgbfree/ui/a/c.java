package com.epoint.android.games.mjfgbfree.ui.a;

import a.a.g;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import com.epoint.android.games.mjfgbfree.af;
import com.epoint.android.games.mjfgbfree.ai;
import java.util.Vector;

public final class c extends f implements i {
    public final void a(Vector vector, int i) {
        String str;
        String str2;
        int i2;
        String str3;
        String str4;
        String str5;
        String str6 = "";
        int i3 = 0;
        int i4 = 0;
        while (i3 < vector.size()) {
            int i5 = ((g) vector.elementAt(i3)).nV;
            StringBuilder sb = new StringBuilder(String.valueOf(((g) vector.elementAt(i3)).cx()));
            if (i5 > 0) {
                str2 = String.valueOf(i5) + af.aa("番") + (((ai.nk == 1 || ai.nk == 5) && i5 > 1) ? "s" : "");
            } else {
                str2 = "";
            }
            String sb2 = sb.append(str2).toString();
            if (!sb2.startsWith("花牌")) {
                int i6 = i4 + i5;
                int i7 = MJ16Activity.rb;
                StringBuilder sb3 = new StringBuilder(String.valueOf(af.a((g) vector.elementAt(i3))));
                if (i5 > 0) {
                    str4 = String.valueOf(i5) + af.aa("番") + (((ai.nk == 1 || ai.nk == 5) && i5 > 1) ? "s" : "");
                } else {
                    str4 = "";
                }
                String sb4 = sb3.append(str4).toString();
                int A = af.A(((g) vector.elementAt(i3)).nW / ((g) vector.elementAt(i3)).mo);
                if (A >= 4 && A < 8) {
                    i7 = MJ16Activity.rb + 1;
                } else if (A >= 8 && A < 12) {
                    i7 = MJ16Activity.rb + 2;
                }
                if (i3 > 0 && !str6.equals("")) {
                    str6 = String.valueOf(str6) + ", ";
                }
                if (!((g) vector.elementAt(i3)).nX) {
                    String str7 = (sb2.startsWith("自摸") || sb2.startsWith("門清自摸") || sb2.startsWith("不求人")) ? String.valueOf("<img src=\"file:///android_asset/assets/images/") + "chimo" : sb2.startsWith("槓上槓自摸") ? String.valueOf("<img src=\"file:///android_asset/assets/images/") + "chimo2" : (sb2.startsWith("二十二羅漢") || sb2.startsWith("四槓") || sb2.startsWith("十八羅漢") || sb2.startsWith("一色四同順")) ? String.valueOf("<img src=\"file:///android_asset/assets/images/") + "sp1" : ((g) vector.elementAt(i3)).cx().equals("搶槓") ? String.valueOf("<img src=\"file:///android_asset/assets/images/") + "sp2" : (sb2.startsWith("海底撈月") || sb2.startsWith("妙手回春")) ? String.valueOf("<img src=\"file:///android_asset/assets/images/") + "sp3" : (sb2.startsWith("嶺上開花") || sb2.startsWith("槓上開花")) ? String.valueOf("<img src=\"file:///android_asset/assets/images/") + "sp4" : (sb2.startsWith("小花糊") || sb2.startsWith("七隻花糊")) ? String.valueOf("<img src=\"file:///android_asset/assets/images/") + "sp5" : (sb2.startsWith("八仙過海") || sb2.startsWith("八隻花糊")) ? String.valueOf("<img src=\"file:///android_asset/assets/images/") + "sp6" : (sb2.startsWith("雞糊") || sb2.startsWith("無番和")) ? String.valueOf("<img src=\"file:///android_asset/assets/images/") + "sp7" : (sb2.startsWith("天糊") || sb2.startsWith("地糊")) ? String.valueOf("<img src=\"file:///android_asset/assets/images/") + "sp8" : A > 0 ? String.valueOf("<img src=\"file:///android_asset/assets/images/") + "level" + A : null;
                    if (str7 != null) {
                        String str8 = String.valueOf(str7) + ".png\" style='width:" + ((int) (24.0f * MJ16Activity.rc)) + "px; height:" + ((int) (24.0f * MJ16Activity.rc)) + "px;' />";
                        String substring = sb4.substring(0, 1);
                        String substring2 = sb4.substring(1);
                        if (A <= 2 || ((g) vector.elementAt(i3)).cx().equals("門清自摸")) {
                            str5 = (((g) vector.elementAt(i3)).cx().equals("自摸") || ((g) vector.elementAt(i3)).cx().equals("門清自摸") || ((g) vector.elementAt(i3)).cx().equals("不求人")) ? "<font color='#800000'><span style='white-space:nowrap'>" + str8 + substring + "</span>" + substring2 + "</font>" : "<span style='white-space:nowrap'>" + str8 + substring + "</span>" + substring2;
                            int i8 = i6;
                            str3 = String.valueOf(str6) + str5;
                            i2 = i8;
                        } else {
                            str5 = "<font color='#800000' style='font-size:" + i7 + "px'><span style='white-space:nowrap'>" + str8 + substring + "</span>" + substring2 + "</font>";
                            int i82 = i6;
                            str3 = String.valueOf(str6) + str5;
                            i2 = i82;
                        }
                    }
                }
                str5 = sb4;
                int i822 = i6;
                str3 = String.valueOf(str6) + str5;
                i2 = i822;
            } else {
                i2 = i4;
                str3 = str6;
            }
            i3++;
            str6 = str3;
            i4 = i2;
        }
        if (ai.nk == 4) {
            str = "<font color='#800000' style='font-size:" + MJ16Activity.rb + "px'>" + af.aa("不能食糊。糊牌最低要求為").replace("XX", String.valueOf(i) + af.aa("番")) + (((ai.nk == 1 || ai.nk == 5) && i4 > 1) ? "s" : "") + af.aa("(不計花牌番數)") + "</font>" + "<br><font color='#404040' style='font-size:" + MJ16Activity.rb + "px'>" + (String.valueOf(af.aa("雖然糊牌成立，可惜只有").replace("XX", String.valueOf(i4) + af.aa("番"))) + (((ai.nk == 1 || ai.nk == 5) && i4 > 1) ? "s" : "") + "<br>") + "<div style='padding:3px; border-style: dotted;border-width: 3px; border-color: navy;'>" + str6 + "</div></font>";
        } else {
            str = "<font color='#800000' style='font-size:" + MJ16Activity.rb + "px'>" + af.aa("不能食糊。糊牌最低要求為") + i + af.aa("番") + (((ai.nk == 1 || ai.nk == 5) && i4 > 1) ? "s" : "") + af.aa("(不計花牌番數)") + "</font>" + "<br><font color='#404040' style='font-size:" + MJ16Activity.rb + "px'>" + (String.valueOf(af.aa("雖然糊牌成立，可惜只有")) + i4 + af.aa("番") + (((ai.nk == 1 || ai.nk == 5) && i4 > 1) ? "s" : "") + ":<br>") + "<div style='padding:3px; border-style: dotted;border-width: 3px; border-color: navy;'>" + str6 + "</div></font>";
        }
        super.a(str, null, false);
    }
}
