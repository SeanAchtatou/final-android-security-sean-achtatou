package com.epoint.android.games.mjfgbfree.baseactivities;

import android.content.Context;

final class b extends Thread {
    private Context aa;
    private e iv;
    private /* synthetic */ BaseActivity iw;

    public b(BaseActivity baseActivity, Context context, e eVar) {
        this.iw = baseActivity;
        this.aa = context;
        this.iv = eVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:126:0x02ae A[SYNTHETIC, Splitter:B:126:0x02ae] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01e1 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r11 = this;
            r6 = 3
            r5 = 2
            r4 = 4
            r9 = 0
            r8 = 1
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r1 = r11.iw
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv
            java.lang.String r2 = r2.dL
            java.lang.Object r1 = r1.ao(r2)
            android.os.Message r3 = new android.os.Message
            r3.<init>()
            r2 = 0
            r3.obj = r2
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            java.lang.String r2 = r2.dL     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            r3.obj = r2     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            int r2 = r2.dM     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            if (r2 != 0) goto L_0x010c
            r2 = 1
            r3.what = r2     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            if (r1 != 0) goto L_0x03d5
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r2 = r11.iw     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            boolean r2 = r2.oA     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            if (r2 == 0) goto L_0x03d8
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r2 = r11.iw     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            android.content.Intent r2 = r2.getIntent()     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            java.lang.String r4 = "system_restart_activity"
            r5 = 0
            boolean r2 = r2.getBooleanExtra(r4, r5)     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            if (r2 != 0) goto L_0x03d8
            android.content.Context r2 = r11.aa     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            com.epoint.android.games.mjfgbfree.baseactivities.e r4 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            java.lang.String r4 = r4.dL     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            java.lang.String r1 = com.epoint.android.games.mjfgbfree.f.a.b(r2, r4)     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            r2 = r1
        L_0x0048:
            if (r2 != 0) goto L_0x0100
            com.epoint.android.games.mjfgbfree.baseactivities.e r1 = r11.iv     // Catch:{ Exception -> 0x03c3, all -> 0x03a7 }
            java.lang.String r1 = r1.dL     // Catch:{ Exception -> 0x03c3, all -> 0x03a7 }
            b.a.b r2 = com.epoint.android.games.mjfgbfree.a.d.y(r1)     // Catch:{ Exception -> 0x03c3, all -> 0x03a7 }
            if (r2 == 0) goto L_0x0070
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r1 = r11.iw     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            boolean r1 = r1.oA     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            if (r1 == 0) goto L_0x0070
            android.content.Context r4 = r11.aa     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            com.epoint.android.games.mjfgbfree.baseactivities.e r1 = r11.iv     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            java.lang.String r5 = r1.dL     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            r0 = r2
            b.a.b r0 = (b.a.b) r0     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            r1 = r0
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r6 = r11.iw     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            int r6 = r6.oz     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            long r6 = (long) r6     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            com.epoint.android.games.mjfgbfree.f.a.a(r4, r5, r1, r6)     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
        L_0x0070:
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r4 = r11.iw     // Catch:{ Exception -> 0x03c3, all -> 0x03a7 }
            com.epoint.android.games.mjfgbfree.baseactivities.e r5 = r11.iv     // Catch:{ Exception -> 0x03c3, all -> 0x03a7 }
            r0 = r2
            b.a.b r0 = (b.a.b) r0     // Catch:{ Exception -> 0x03c3, all -> 0x03a7 }
            r1 = r0
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity.a(r4, r5, r1)     // Catch:{ Exception -> 0x03c3, all -> 0x03a7 }
            r1 = r2
            r2 = r8
        L_0x007d:
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r4 = r11.iw
            java.util.Vector r4 = r4.oD
            monitor-enter(r4)
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r5 = r11.iw     // Catch:{ all -> 0x036d }
            java.util.Vector r5 = r5.oD     // Catch:{ all -> 0x036d }
            java.lang.Thread r6 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x036d }
            r5.remove(r6)     // Catch:{ all -> 0x036d }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r5 = r11.iw     // Catch:{ all -> 0x036d }
            java.util.Hashtable r5 = r5.oB     // Catch:{ all -> 0x036d }
            com.epoint.android.games.mjfgbfree.baseactivities.e r6 = r11.iv     // Catch:{ all -> 0x036d }
            java.lang.String r6 = r6.dL     // Catch:{ all -> 0x036d }
            r5.remove(r6)     // Catch:{ all -> 0x036d }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r5 = r11.iw     // Catch:{ all -> 0x036d }
            r5.a(r3)     // Catch:{ all -> 0x036d }
            com.epoint.android.games.mjfgbfree.baseactivities.e r3 = r11.iv     // Catch:{ all -> 0x036d }
            int r3 = r3.dM     // Catch:{ all -> 0x036d }
            if (r3 != r8) goto L_0x00d2
            if (r1 == 0) goto L_0x00d2
            if (r2 != 0) goto L_0x00d2
            android.content.Context r2 = r11.aa     // Catch:{ Exception -> 0x0367 }
            com.epoint.android.games.mjfgbfree.baseactivities.e r3 = r11.iv     // Catch:{ Exception -> 0x0367 }
            java.lang.String r3 = r3.dL     // Catch:{ Exception -> 0x0367 }
            android.graphics.Bitmap r1 = (android.graphics.Bitmap) r1     // Catch:{ Exception -> 0x0367 }
            com.epoint.android.games.mjfgbfree.baseactivities.e r5 = r11.iv     // Catch:{ Exception -> 0x0367 }
            int r5 = r5.dN     // Catch:{ Exception -> 0x0367 }
            boolean r1 = com.epoint.android.games.mjfgbfree.f.a.a(r2, r3, r1, r5)     // Catch:{ Exception -> 0x0367 }
            if (r1 == 0) goto L_0x00d2
            android.os.Message r1 = new android.os.Message     // Catch:{ Exception -> 0x0367 }
            r1.<init>()     // Catch:{ Exception -> 0x0367 }
            r2 = 4
            r1.what = r2     // Catch:{ Exception -> 0x0367 }
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv     // Catch:{ Exception -> 0x0367 }
            java.lang.String r2 = r2.dL     // Catch:{ Exception -> 0x0367 }
            r1.obj = r2     // Catch:{ Exception -> 0x0367 }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r2 = r11.iw     // Catch:{ Exception -> 0x0367 }
            r2.a(r1)     // Catch:{ Exception -> 0x0367 }
        L_0x00d2:
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r1 = r11.iw     // Catch:{ all -> 0x036d }
            java.util.Vector r1 = r1.oD     // Catch:{ all -> 0x036d }
            int r1 = r1.size()     // Catch:{ all -> 0x036d }
            if (r1 != 0) goto L_0x0373
            android.os.Message r1 = new android.os.Message     // Catch:{ all -> 0x036d }
            r1.<init>()     // Catch:{ all -> 0x036d }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r2 = r11.iw     // Catch:{ all -> 0x036d }
            boolean r2 = r2.oE     // Catch:{ all -> 0x036d }
            if (r2 == 0) goto L_0x0370
            r2 = 6
        L_0x00ec:
            r1.what = r2     // Catch:{ all -> 0x036d }
            java.lang.Integer r2 = new java.lang.Integer     // Catch:{ all -> 0x036d }
            com.epoint.android.games.mjfgbfree.baseactivities.e r3 = r11.iv     // Catch:{ all -> 0x036d }
            int r3 = r3.dM     // Catch:{ all -> 0x036d }
            r2.<init>(r3)     // Catch:{ all -> 0x036d }
            r1.obj = r2     // Catch:{ all -> 0x036d }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r2 = r11.iw     // Catch:{ all -> 0x036d }
            r2.a(r1)     // Catch:{ all -> 0x036d }
        L_0x00fe:
            monitor-exit(r4)     // Catch:{ all -> 0x036d }
        L_0x00ff:
            return
        L_0x0100:
            b.a.b r4 = new b.a.b     // Catch:{ Exception -> 0x03c3, all -> 0x03a7 }
            r0 = r2
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x03c3, all -> 0x03a7 }
            r1 = r0
            r4.<init>(r1)     // Catch:{ Exception -> 0x03c3, all -> 0x03a7 }
            r2 = r4
            goto L_0x0070
        L_0x010c:
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            int r2 = r2.dM     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            if (r2 == r5) goto L_0x011e
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            int r2 = r2.dM     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            if (r2 == r6) goto L_0x011e
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            int r2 = r2.dM     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            if (r2 != r4) goto L_0x0177
        L_0x011e:
            r2 = 2
            r3.what = r2     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            if (r1 != 0) goto L_0x03d5
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            int r2 = r2.dM     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            if (r2 != r5) goto L_0x0140
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            java.lang.String r2 = r2.dL     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            b.a.b r2 = com.epoint.android.games.mjfgbfree.a.d.y(r2)     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r4 = r11.iw     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            com.epoint.android.games.mjfgbfree.baseactivities.e r5 = r11.iv     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            r0 = r2
            b.a.b r0 = (b.a.b) r0     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            r1 = r0
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity.a(r4, r5, r1)     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            r1 = r2
            r2 = r8
            goto L_0x007d
        L_0x0140:
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            int r2 = r2.dM     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            if (r2 != r6) goto L_0x015d
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            java.lang.String r2 = r2.dL     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            b.a.b r2 = com.epoint.android.games.mjfgbfree.a.d.A(r2)     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r4 = r11.iw     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            com.epoint.android.games.mjfgbfree.baseactivities.e r5 = r11.iv     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            r0 = r2
            b.a.b r0 = (b.a.b) r0     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            r1 = r0
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity.a(r4, r5, r1)     // Catch:{ Exception -> 0x03ca, all -> 0x03ae }
            r1 = r2
            r2 = r8
            goto L_0x007d
        L_0x015d:
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            int r2 = r2.dM     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            if (r2 != r4) goto L_0x03d5
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            java.lang.String r2 = r2.dL     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            java.lang.String r1 = com.epoint.android.games.mjfgbfree.a.d.z(r2)     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r2 = r11.iw     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            com.epoint.android.games.mjfgbfree.baseactivities.e r4 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            java.lang.String r4 = r4.dL     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            r2.b(r4, r1)     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            r2 = r8
            goto L_0x007d
        L_0x0177:
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            int r2 = r2.dM     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            if (r2 != r8) goto L_0x03d5
            r2 = 3
            r3.what = r2     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            if (r1 != 0) goto L_0x03d5
            android.content.Context r2 = r11.aa     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            com.epoint.android.games.mjfgbfree.baseactivities.e r4 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            java.lang.String r4 = r4.dL     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            com.epoint.android.games.mjfgbfree.baseactivities.e r5 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            int r5 = r5.dN     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            android.graphics.Bitmap r1 = com.epoint.android.games.mjfgbfree.f.a.a(r2, r4, r5)     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            if (r1 != 0) goto L_0x03d1
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            java.lang.String r2 = r2.dL     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            com.epoint.android.games.mjfgbfree.baseactivities.e r4 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            int r4 = r4.dN     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            com.epoint.android.games.mjfgbfree.baseactivities.e r5 = r11.iv     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            int r5 = r5.dO     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            android.graphics.Bitmap r1 = com.epoint.android.games.mjfgbfree.a.d.a(r2, r4, r5)     // Catch:{ Exception -> 0x03bf, all -> 0x02a5 }
            r2 = r1
            r1 = r9
        L_0x01a4:
            if (r2 == 0) goto L_0x01b4
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r4 = r11.iw     // Catch:{ Exception -> 0x01cf, all -> 0x03b5 }
            com.epoint.android.games.mjfgbfree.baseactivities.e r5 = r11.iv     // Catch:{ Exception -> 0x01cf, all -> 0x03b5 }
            java.lang.String r5 = r5.dL     // Catch:{ Exception -> 0x01cf, all -> 0x03b5 }
            r4.b(r5, r2)     // Catch:{ Exception -> 0x01cf, all -> 0x03b5 }
            r10 = r1
            r1 = r2
            r2 = r10
            goto L_0x007d
        L_0x01b4:
            com.epoint.android.games.mjfgbfree.baseactivities.f r4 = new com.epoint.android.games.mjfgbfree.baseactivities.f     // Catch:{ Exception -> 0x01cf, all -> 0x03b5 }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r5 = r11.iw     // Catch:{ Exception -> 0x01cf, all -> 0x03b5 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01cf, all -> 0x03b5 }
            java.lang.String r7 = "Unable to load image: "
            r6.<init>(r7)     // Catch:{ Exception -> 0x01cf, all -> 0x03b5 }
            com.epoint.android.games.mjfgbfree.baseactivities.e r7 = r11.iv     // Catch:{ Exception -> 0x01cf, all -> 0x03b5 }
            java.lang.String r7 = r7.dL     // Catch:{ Exception -> 0x01cf, all -> 0x03b5 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x01cf, all -> 0x03b5 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x01cf, all -> 0x03b5 }
            r4.<init>(r5, r6)     // Catch:{ Exception -> 0x01cf, all -> 0x03b5 }
            throw r4     // Catch:{ Exception -> 0x01cf, all -> 0x03b5 }
        L_0x01cf:
            r4 = move-exception
            r10 = r4
            r4 = r1
            r1 = r2
            r2 = r10
        L_0x01d4:
            r2.printStackTrace()     // Catch:{ all -> 0x03bc }
            r5 = 0
            r3.obj = r5     // Catch:{ all -> 0x03bc }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r5 = r11.iw
            java.util.Vector r5 = r5.oD
            monitor-enter(r5)
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r6 = r11.iw     // Catch:{ all -> 0x0268 }
            java.util.Vector r6 = r6.oD     // Catch:{ all -> 0x0268 }
            java.lang.Thread r7 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0268 }
            r6.remove(r7)     // Catch:{ all -> 0x0268 }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r6 = r11.iw     // Catch:{ all -> 0x0268 }
            java.util.Hashtable r6 = r6.oB     // Catch:{ all -> 0x0268 }
            com.epoint.android.games.mjfgbfree.baseactivities.e r7 = r11.iv     // Catch:{ all -> 0x0268 }
            java.lang.String r7 = r7.dL     // Catch:{ all -> 0x0268 }
            r6.remove(r7)     // Catch:{ all -> 0x0268 }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r6 = r11.iw     // Catch:{ all -> 0x0268 }
            r6.a(r3)     // Catch:{ all -> 0x0268 }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r3 = r11.iw     // Catch:{ all -> 0x0268 }
            r3.oE = true     // Catch:{ all -> 0x0268 }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r3 = r11.iw     // Catch:{ all -> 0x0268 }
            r3.b(r2)     // Catch:{ all -> 0x0268 }
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv     // Catch:{ all -> 0x0268 }
            int r2 = r2.dM     // Catch:{ all -> 0x0268 }
            if (r2 != r8) goto L_0x0239
            if (r1 == 0) goto L_0x0239
            if (r4 != 0) goto L_0x0239
            android.content.Context r2 = r11.aa     // Catch:{ Exception -> 0x026b }
            com.epoint.android.games.mjfgbfree.baseactivities.e r3 = r11.iv     // Catch:{ Exception -> 0x026b }
            java.lang.String r3 = r3.dL     // Catch:{ Exception -> 0x026b }
            android.graphics.Bitmap r1 = (android.graphics.Bitmap) r1     // Catch:{ Exception -> 0x026b }
            com.epoint.android.games.mjfgbfree.baseactivities.e r4 = r11.iv     // Catch:{ Exception -> 0x026b }
            int r4 = r4.dN     // Catch:{ Exception -> 0x026b }
            boolean r1 = com.epoint.android.games.mjfgbfree.f.a.a(r2, r3, r1, r4)     // Catch:{ Exception -> 0x026b }
            if (r1 == 0) goto L_0x0239
            android.os.Message r1 = new android.os.Message     // Catch:{ Exception -> 0x026b }
            r1.<init>()     // Catch:{ Exception -> 0x026b }
            r2 = 4
            r1.what = r2     // Catch:{ Exception -> 0x026b }
            com.epoint.android.games.mjfgbfree.baseactivities.e r2 = r11.iv     // Catch:{ Exception -> 0x026b }
            java.lang.String r2 = r2.dL     // Catch:{ Exception -> 0x026b }
            r1.obj = r2     // Catch:{ Exception -> 0x026b }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r2 = r11.iw     // Catch:{ Exception -> 0x026b }
            r2.a(r1)     // Catch:{ Exception -> 0x026b }
        L_0x0239:
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r1 = r11.iw     // Catch:{ all -> 0x0268 }
            java.util.Vector r1 = r1.oD     // Catch:{ all -> 0x0268 }
            int r1 = r1.size()     // Catch:{ all -> 0x0268 }
            if (r1 != 0) goto L_0x0272
            android.os.Message r1 = new android.os.Message     // Catch:{ all -> 0x0268 }
            r1.<init>()     // Catch:{ all -> 0x0268 }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r2 = r11.iw     // Catch:{ all -> 0x0268 }
            boolean r2 = r2.oE     // Catch:{ all -> 0x0268 }
            if (r2 == 0) goto L_0x0270
            r2 = 6
        L_0x0253:
            r1.what = r2     // Catch:{ all -> 0x0268 }
            java.lang.Integer r2 = new java.lang.Integer     // Catch:{ all -> 0x0268 }
            com.epoint.android.games.mjfgbfree.baseactivities.e r3 = r11.iv     // Catch:{ all -> 0x0268 }
            int r3 = r3.dM     // Catch:{ all -> 0x0268 }
            r2.<init>(r3)     // Catch:{ all -> 0x0268 }
            r1.obj = r2     // Catch:{ all -> 0x0268 }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r2 = r11.iw     // Catch:{ all -> 0x0268 }
            r2.a(r1)     // Catch:{ all -> 0x0268 }
        L_0x0265:
            monitor-exit(r5)     // Catch:{ all -> 0x0268 }
            goto L_0x00ff
        L_0x0268:
            r1 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0268 }
            throw r1
        L_0x026b:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x0268 }
            goto L_0x0239
        L_0x0270:
            r2 = 5
            goto L_0x0253
        L_0x0272:
            r2 = r9
        L_0x0273:
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r1 = r11.iw     // Catch:{ all -> 0x0268 }
            java.util.Vector r1 = r1.oD     // Catch:{ all -> 0x0268 }
            int r1 = r1.size()     // Catch:{ all -> 0x0268 }
            if (r2 >= r1) goto L_0x0265
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r1 = r11.iw     // Catch:{ all -> 0x0268 }
            java.util.Vector r1 = r1.oD     // Catch:{ all -> 0x0268 }
            java.lang.Object r1 = r1.elementAt(r2)     // Catch:{ all -> 0x0268 }
            java.lang.Thread r1 = (java.lang.Thread) r1     // Catch:{ all -> 0x0268 }
            boolean r1 = r1.isAlive()     // Catch:{ all -> 0x0268 }
            if (r1 != 0) goto L_0x02a1
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r1 = r11.iw     // Catch:{ all -> 0x0268 }
            java.util.Vector r1 = r1.oD     // Catch:{ all -> 0x0268 }
            java.lang.Object r11 = r1.elementAt(r2)     // Catch:{ all -> 0x0268 }
            java.lang.Thread r11 = (java.lang.Thread) r11     // Catch:{ all -> 0x0268 }
            r11.start()     // Catch:{ all -> 0x0268 }
            goto L_0x0265
        L_0x02a1:
            int r1 = r2 + 1
            r2 = r1
            goto L_0x0273
        L_0x02a5:
            r2 = move-exception
            r4 = r8
        L_0x02a7:
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r5 = r11.iw
            java.util.Vector r5 = r5.oD
            monitor-enter(r5)
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r6 = r11.iw     // Catch:{ all -> 0x032f }
            java.util.Vector r6 = r6.oD     // Catch:{ all -> 0x032f }
            java.lang.Thread r7 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x032f }
            r6.remove(r7)     // Catch:{ all -> 0x032f }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r6 = r11.iw     // Catch:{ all -> 0x032f }
            java.util.Hashtable r6 = r6.oB     // Catch:{ all -> 0x032f }
            com.epoint.android.games.mjfgbfree.baseactivities.e r7 = r11.iv     // Catch:{ all -> 0x032f }
            java.lang.String r7 = r7.dL     // Catch:{ all -> 0x032f }
            r6.remove(r7)     // Catch:{ all -> 0x032f }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r6 = r11.iw     // Catch:{ all -> 0x032f }
            r6.a(r3)     // Catch:{ all -> 0x032f }
            com.epoint.android.games.mjfgbfree.baseactivities.e r3 = r11.iv     // Catch:{ all -> 0x032f }
            int r3 = r3.dM     // Catch:{ all -> 0x032f }
            if (r3 != r8) goto L_0x02fc
            if (r1 == 0) goto L_0x02fc
            if (r4 != 0) goto L_0x02fc
            android.content.Context r3 = r11.aa     // Catch:{ Exception -> 0x032a }
            com.epoint.android.games.mjfgbfree.baseactivities.e r4 = r11.iv     // Catch:{ Exception -> 0x032a }
            java.lang.String r4 = r4.dL     // Catch:{ Exception -> 0x032a }
            android.graphics.Bitmap r1 = (android.graphics.Bitmap) r1     // Catch:{ Exception -> 0x032a }
            com.epoint.android.games.mjfgbfree.baseactivities.e r6 = r11.iv     // Catch:{ Exception -> 0x032a }
            int r6 = r6.dN     // Catch:{ Exception -> 0x032a }
            boolean r1 = com.epoint.android.games.mjfgbfree.f.a.a(r3, r4, r1, r6)     // Catch:{ Exception -> 0x032a }
            if (r1 == 0) goto L_0x02fc
            android.os.Message r1 = new android.os.Message     // Catch:{ Exception -> 0x032a }
            r1.<init>()     // Catch:{ Exception -> 0x032a }
            r3 = 4
            r1.what = r3     // Catch:{ Exception -> 0x032a }
            com.epoint.android.games.mjfgbfree.baseactivities.e r3 = r11.iv     // Catch:{ Exception -> 0x032a }
            java.lang.String r3 = r3.dL     // Catch:{ Exception -> 0x032a }
            r1.obj = r3     // Catch:{ Exception -> 0x032a }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r3 = r11.iw     // Catch:{ Exception -> 0x032a }
            r3.a(r1)     // Catch:{ Exception -> 0x032a }
        L_0x02fc:
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r1 = r11.iw     // Catch:{ all -> 0x032f }
            java.util.Vector r1 = r1.oD     // Catch:{ all -> 0x032f }
            int r1 = r1.size()     // Catch:{ all -> 0x032f }
            if (r1 != 0) goto L_0x0334
            android.os.Message r1 = new android.os.Message     // Catch:{ all -> 0x032f }
            r1.<init>()     // Catch:{ all -> 0x032f }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r3 = r11.iw     // Catch:{ all -> 0x032f }
            boolean r3 = r3.oE     // Catch:{ all -> 0x032f }
            if (r3 == 0) goto L_0x0332
            r3 = 6
        L_0x0316:
            r1.what = r3     // Catch:{ all -> 0x032f }
            java.lang.Integer r3 = new java.lang.Integer     // Catch:{ all -> 0x032f }
            com.epoint.android.games.mjfgbfree.baseactivities.e r4 = r11.iv     // Catch:{ all -> 0x032f }
            int r4 = r4.dM     // Catch:{ all -> 0x032f }
            r3.<init>(r4)     // Catch:{ all -> 0x032f }
            r1.obj = r3     // Catch:{ all -> 0x032f }
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r3 = r11.iw     // Catch:{ all -> 0x032f }
            r3.a(r1)     // Catch:{ all -> 0x032f }
        L_0x0328:
            monitor-exit(r5)     // Catch:{ all -> 0x032f }
            throw r2
        L_0x032a:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x032f }
            goto L_0x02fc
        L_0x032f:
            r1 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x032f }
            throw r1
        L_0x0332:
            r3 = 5
            goto L_0x0316
        L_0x0334:
            r3 = r9
        L_0x0335:
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r1 = r11.iw     // Catch:{ all -> 0x032f }
            java.util.Vector r1 = r1.oD     // Catch:{ all -> 0x032f }
            int r1 = r1.size()     // Catch:{ all -> 0x032f }
            if (r3 >= r1) goto L_0x0328
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r1 = r11.iw     // Catch:{ all -> 0x032f }
            java.util.Vector r1 = r1.oD     // Catch:{ all -> 0x032f }
            java.lang.Object r1 = r1.elementAt(r3)     // Catch:{ all -> 0x032f }
            java.lang.Thread r1 = (java.lang.Thread) r1     // Catch:{ all -> 0x032f }
            boolean r1 = r1.isAlive()     // Catch:{ all -> 0x032f }
            if (r1 != 0) goto L_0x0363
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r1 = r11.iw     // Catch:{ all -> 0x032f }
            java.util.Vector r1 = r1.oD     // Catch:{ all -> 0x032f }
            java.lang.Object r11 = r1.elementAt(r3)     // Catch:{ all -> 0x032f }
            java.lang.Thread r11 = (java.lang.Thread) r11     // Catch:{ all -> 0x032f }
            r11.start()     // Catch:{ all -> 0x032f }
            goto L_0x0328
        L_0x0363:
            int r1 = r3 + 1
            r3 = r1
            goto L_0x0335
        L_0x0367:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x036d }
            goto L_0x00d2
        L_0x036d:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x036d }
            throw r1
        L_0x0370:
            r2 = 5
            goto L_0x00ec
        L_0x0373:
            r2 = r9
        L_0x0374:
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r1 = r11.iw     // Catch:{ all -> 0x036d }
            java.util.Vector r1 = r1.oD     // Catch:{ all -> 0x036d }
            int r1 = r1.size()     // Catch:{ all -> 0x036d }
            if (r2 >= r1) goto L_0x00fe
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r1 = r11.iw     // Catch:{ all -> 0x036d }
            java.util.Vector r1 = r1.oD     // Catch:{ all -> 0x036d }
            java.lang.Object r1 = r1.elementAt(r2)     // Catch:{ all -> 0x036d }
            java.lang.Thread r1 = (java.lang.Thread) r1     // Catch:{ all -> 0x036d }
            boolean r1 = r1.isAlive()     // Catch:{ all -> 0x036d }
            if (r1 != 0) goto L_0x03a3
            com.epoint.android.games.mjfgbfree.baseactivities.BaseActivity r1 = r11.iw     // Catch:{ all -> 0x036d }
            java.util.Vector r1 = r1.oD     // Catch:{ all -> 0x036d }
            java.lang.Object r11 = r1.elementAt(r2)     // Catch:{ all -> 0x036d }
            java.lang.Thread r11 = (java.lang.Thread) r11     // Catch:{ all -> 0x036d }
            r11.start()     // Catch:{ all -> 0x036d }
            goto L_0x00fe
        L_0x03a3:
            int r1 = r2 + 1
            r2 = r1
            goto L_0x0374
        L_0x03a7:
            r1 = move-exception
            r4 = r8
            r10 = r1
            r1 = r2
            r2 = r10
            goto L_0x02a7
        L_0x03ae:
            r1 = move-exception
            r4 = r8
            r10 = r1
            r1 = r2
            r2 = r10
            goto L_0x02a7
        L_0x03b5:
            r4 = move-exception
            r10 = r4
            r4 = r1
            r1 = r2
            r2 = r10
            goto L_0x02a7
        L_0x03bc:
            r2 = move-exception
            goto L_0x02a7
        L_0x03bf:
            r2 = move-exception
            r4 = r8
            goto L_0x01d4
        L_0x03c3:
            r1 = move-exception
            r4 = r8
            r10 = r1
            r1 = r2
            r2 = r10
            goto L_0x01d4
        L_0x03ca:
            r1 = move-exception
            r4 = r8
            r10 = r1
            r1 = r2
            r2 = r10
            goto L_0x01d4
        L_0x03d1:
            r2 = r1
            r1 = r8
            goto L_0x01a4
        L_0x03d5:
            r2 = r8
            goto L_0x007d
        L_0x03d8:
            r2 = r1
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.baseactivities.b.run():void");
    }
}
