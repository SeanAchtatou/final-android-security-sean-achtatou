package com.epoint.android.games.mjfgbfree;

import android.preference.Preference;

final class ba implements Preference.OnPreferenceChangeListener {
    private /* synthetic */ PreferencesActivity qn;

    ba(PreferencesActivity preferencesActivity) {
        this.qn = preferencesActivity;
    }

    public final boolean onPreferenceChange(Preference preference, Object obj) {
        this.qn.setRequestedOrientation(Integer.valueOf((String) obj).intValue());
        return true;
    }
}
