package com.epoint.android.games.mjfgbfree;

import a.a.a;
import a.a.c;
import a.a.d;
import a.a.e;
import a.a.f;
import android.graphics.Point;
import b.a.b;
import com.epoint.android.games.mjfgbfree.d.g;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.Vector;

public final class af {
    private static g gO = new g();
    private static b mj;
    private static final Vector mk;

    static {
        Vector vector = new Vector();
        mk = vector;
        vector.add("currentWind");
        mk.add("currentRound");
        mk.add("minFans");
        mk.add("startHost");
        mk.add("currentHost");
        mk.add("currentCircle");
        mk.add("lastAction");
        mk.add("activeCardOwner");
        mk.add("consecutiveHost");
        mk.add("remainingCardCount");
        mk.add("loser");
        mk.add("isGameOver");
        mk.add("lastActiveCard");
        mk.add("playedCards");
        mk.add("name");
        mk.add("money");
        mk.add("isActive");
        mk.add("tileText");
        mk.add("privateGongTiles");
        mk.add("tileCount");
        mk.add("flowers");
        mk.add("exposedTiles");
        mk.add("listenType");
        mk.add("pseudoActivePlayer");
        mk.add("playerInfo");
        mk.add("unMaskedExposedTiles");
        mk.add("canWu");
        mk.add("canGongTiles");
        mk.add("canPong");
        mk.add("canShungType");
        mk.add("canGetTile");
        mk.add("canGiveup");
        mk.add("canListenType");
        mk.add("prohibitedTiles");
        mk.add("controlBlock");
        mk.add("desc");
        mk.add("fan");
        mk.add("rankingFan");
        mk.add("count");
        mk.add("isCompoundFans");
        mk.add("unqualifiedFanVect");
        mk.add("winningHand");
        mk.add("winner");
        mk.add("loser");
        mk.add("forPlayer");
        mk.add("responsiblePlayer");
        mk.add("money0");
        mk.add("money1");
        mk.add("money2");
        mk.add("money3");
        mk.add("isRobGong");
        mk.add("winningFanVect");
        mk.add("totalFans");
        mk.add("playedTiles");
        mk.add("doraTiles");
        mk.add("setInfo");
    }

    public static int A(int i) {
        if (i >= 88) {
            return 11;
        }
        if (i >= 64) {
            return 10;
        }
        if (i >= 48) {
            return 9;
        }
        if (i >= 32) {
            return 8;
        }
        if (i >= 24) {
            return 7;
        }
        if (i >= 16) {
            return 6;
        }
        if (i >= 12) {
            return 5;
        }
        if (i >= 8) {
            return 4;
        }
        if (i >= 6) {
            return 3;
        }
        if (i >= 4) {
            return 2;
        }
        return i >= 2 ? 1 : 0;
    }

    private static String W(String str) {
        int indexOf = mk.indexOf(str);
        return indexOf == -1 ? str : String.valueOf(indexOf);
    }

    public static int X(String str) {
        if (str.equals("BK") || str.equals("-")) {
            return 0;
        }
        for (int i = 0; i < e.nv.length; i++) {
            if (e.nv[i].equals(str)) {
                return i;
            }
        }
        return -1;
    }

    public static Point Y(String str) {
        a aVar = (a) c.mu.get(str);
        int aN = aVar.aN();
        int i = 0;
        if (aN > 4) {
            aN = 4;
        }
        switch (aN) {
            case 0:
                i = aVar.aM() - 1;
                break;
            case 1:
                i = aVar.aM() - 10;
                break;
            case 2:
                i = aVar.aM() - 19;
                break;
            case 3:
                i = aVar.aM() - 28;
                break;
            case 4:
                i = aVar.aM() - 35;
                break;
        }
        Point point = new Point();
        point.x = i;
        point.y = aN;
        return point;
    }

    public static String Z(String str) {
        if (ai.nk == 2) {
            return str;
        }
        if (ai.nk == 1) {
            if (str.equals("本階段成積")) {
                return "Stage Results";
            }
            if (str.equals("挑戰賽")) {
                return "Tournament";
            }
            if (str.equals("用2張遊戲劵挑戰這比賽")) {
                return "Play this for 2 tickets";
            }
            if (str.equals("需要2張遊戲劵")) {
                return "You need 2 tickets to play this";
            }
            if (str.equals("盤數")) {
                return "Hands";
            }
            if (str.equals("最低晉級排名")) {
                return "Qualifying rank";
            }
            if (str.equals("物品")) {
                return "Items";
            }
            if (str.equals("得到物品")) {
                return "Item obtained";
            }
            if (str.equals("排名")) {
                return "Rank";
            }
            if (str.equals("完成")) {
                return "Finished";
            }
            if (str.equals("挑戰失敗")) {
                return "Game Over";
            }
            if (str.equals("用1張遊戲劵再挑戰")) {
                return "Continue for 1 ticket";
            }
            if (str.equals("剩餘遊戲劵")) {
                return "Tickets left";
            }
            if (str.equals("下一關")) {
                return "Next stage";
            }
            if (str.equals("成功晉級")) {
                return "Stage Cleared";
            }
            if (str.equals("賽")) {
                return "Match";
            }
            if (str.equals("決賽")) {
                return "Final";
            }
            if (str.equals("準決賽")) {
                return "Semi-final";
            }
            if (str.equals("優閒盃")) {
                return "Casual Cup";
            }
            if (str.equals("初級盃")) {
                return "Elementary Cup";
            }
            if (str.equals("中級盃")) {
                return "Intermediate Cup";
            }
            if (str.equals("高級盃")) {
                return "Advanced Cup";
            }
            if (str.equals("大師盃")) {
                return "Master Cup";
            }
            if (str.equals("雀友盃")) {
                return "Friends Cup";
            }
            if (str.equals("起胡番數")) {
                return "Minimum points to win";
            }
            if (str.equals("以第一名完成賽事後，才可挑戰更高水平賽事。")) {
                return "Unlock next level by getting 1st place in previous level.";
            }
            if (str.equals("每24小時自動補充10張遊戲劵。")) {
                return "10 new tickets given daily.";
            }
            if (str.equals("此比賽不許重新挑戰或續關。")) {
                return "Ticket continue or restart is not allowed for this competition.";
            }
            if (str.equals("圈數")) {
                return "Rounds";
            }
            if (str.equals("用2張遊戲劵重新挑戰此階段")) {
                return "Restart current match for 2 tickets";
            }
            if (str.equals("重新挑戰")) {
                return "Restart match";
            }
        } else if (ai.nk == 3) {
            if (str.equals("本階段成積")) {
                return "本阶段成积";
            }
            if (str.equals("挑戰賽")) {
                return "挑战赛";
            }
            if (str.equals("用2張遊戲劵挑戰這比賽")) {
                return "用2张游戏劵挑战这比赛";
            }
            if (str.equals("需要2張遊戲劵")) {
                return "需要2张游戏劵";
            }
            if (str.equals("盤數")) {
                return "盘数";
            }
            if (str.equals("最低晉級排名")) {
                return "最低晋级排名";
            }
            if (str.equals("物品")) {
                return "物品";
            }
            if (str.equals("得到物品")) {
                return "得到物品";
            }
            if (str.equals("排名")) {
                return "排名";
            }
            if (str.equals("完成")) {
                return "完成";
            }
            if (str.equals("挑戰失敗")) {
                return "挑战失败";
            }
            if (str.equals("用1張遊戲劵再挑戰")) {
                return "用1张游戏劵再挑战";
            }
            if (str.equals("剩餘遊戲劵")) {
                return "剩余游戏劵";
            }
            if (str.equals("下一關")) {
                return "下一关";
            }
            if (str.equals("成功晉級")) {
                return "成功晋级";
            }
            if (str.equals("賽")) {
                return "赛";
            }
            if (str.equals("決賽")) {
                return "决赛";
            }
            if (str.equals("準決賽")) {
                return "准决赛";
            }
            if (str.equals("優閒盃")) {
                return "优闲杯";
            }
            if (str.equals("初級盃")) {
                return "初级杯";
            }
            if (str.equals("中級盃")) {
                return "中级杯";
            }
            if (str.equals("高級盃")) {
                return "高级杯";
            }
            if (str.equals("大師盃")) {
                return "大师杯";
            }
            if (str.equals("雀友盃")) {
                return "雀友杯";
            }
            if (str.equals("起胡番數")) {
                return "起胡番数";
            }
            if (str.equals("以第一名完成賽事後，才可挑戰更高水平賽事。")) {
                return "以第一名完成赛事后、才可挑战更高水平赛事。";
            }
            if (str.equals("此比賽不許重新挑戰或續關。")) {
                return "此比赛不许重新挑战或续关。";
            }
            if (str.equals("每24小時自動補充10張遊戲劵。")) {
                return "每24小时自动补充10张游戏劵。";
            }
            if (str.equals("圈數")) {
                return "圈数";
            }
            if (str.equals("用2張遊戲劵重新挑戰此階段")) {
                return "用2张游戏劵重新挑战此阶段";
            }
            if (str.equals("重新挑戰")) {
                return "重新挑战";
            }
        } else if (ai.nk == 4) {
            if (str.equals("本階段成積")) {
                return "ステージの結果";
            }
            if (str.equals("挑戰賽")) {
                return "トーナメント";
            }
            if (str.equals("用2張遊戲劵挑戰這比賽")) {
                return "Play this for 2 tickets";
            }
            if (str.equals("需要2張遊戲劵")) {
                return "You need 2 tickets to play this";
            }
            if (str.equals("盤數")) {
                return "局数";
            }
            if (str.equals("最低晉級排名")) {
                return "予選順位";
            }
            if (str.equals("物品")) {
                return "アイテム";
            }
            if (str.equals("得到物品")) {
                return "アイテムを得た";
            }
            if (str.equals("排名")) {
                return "順位";
            }
            if (str.equals("完成")) {
                return "完成した";
            }
            if (str.equals("挑戰失敗")) {
                return "ゲームオーバー";
            }
            if (str.equals("用1張遊戲劵再挑戰")) {
                return "Continue for 1 ticket";
            }
            if (str.equals("剩餘遊戲劵")) {
                return "残りのチケット";
            }
            if (str.equals("下一關")) {
                return "次のステージ";
            }
            if (str.equals("成功晉級")) {
                return "ステージクリア";
            }
            if (str.equals("賽")) {
                return "試合";
            }
            if (str.equals("決賽")) {
                return "ファイナル";
            }
            if (str.equals("準決賽")) {
                return "準決勝";
            }
            if (str.equals("優閒盃")) {
                return "カジュアルカップ";
            }
            if (str.equals("初級盃")) {
                return "初級カップ";
            }
            if (str.equals("中級盃")) {
                return "中級カップ";
            }
            if (str.equals("高級盃")) {
                return "高級カップ";
            }
            if (str.equals("大師盃")) {
                return "マスターカップ";
            }
            if (str.equals("雀友盃")) {
                return "雀友カップ";
            }
            if (str.equals("起胡番數")) {
                return "和了に必要な翻数";
            }
            if (str.equals("以第一名完成賽事後，才可挑戰更高水平賽事。")) {
                return "Unlock next level by getting 1st place in previous level.";
            }
            if (str.equals("此比賽不許重新挑戰或續關。")) {
                return "Ticket continue or restart is not allowed for this competition.";
            }
            if (str.equals("每24小時自動補充10張遊戲劵。")) {
                return "10 new tickets given daily.";
            }
            if (str.equals("圈數")) {
                return "Rounds";
            }
            if (str.equals("用2張遊戲劵重新挑戰此階段")) {
                return "Restart current match for 2 tickets";
            }
            if (str.equals("重新挑戰")) {
                return "試合を再起動";
            }
        } else if (ai.nk == 5) {
            if (str.equals("本階段成積")) {
                return "Stage Results";
            }
            if (str.equals("挑戰賽")) {
                return "Tournament";
            }
            if (str.equals("用2張遊戲劵挑戰這比賽")) {
                return "Play this for 2 tickets";
            }
            if (str.equals("需要2張遊戲劵")) {
                return "You need 2 tickets to play this";
            }
            if (str.equals("盤數")) {
                return "Hands";
            }
            if (str.equals("最低晉級排名")) {
                return "Qualifying rank";
            }
            if (str.equals("物品")) {
                return "Item";
            }
            if (str.equals("得到物品")) {
                return "Item obtained";
            }
            if (str.equals("排名")) {
                return "Rank";
            }
            if (str.equals("完成")) {
                return "Finished";
            }
            if (str.equals("挑戰失敗")) {
                return "Game Over";
            }
            if (str.equals("用1張遊戲劵再挑戰")) {
                return "Continue for 1 ticket";
            }
            if (str.equals("剩餘遊戲劵")) {
                return "Tickets left";
            }
            if (str.equals("下一關")) {
                return "Next stage";
            }
            if (str.equals("成功晉級")) {
                return "Stage Cleared";
            }
            if (str.equals("賽")) {
                return "Match";
            }
            if (str.equals("決賽")) {
                return "Final";
            }
            if (str.equals("準決賽")) {
                return "Semi-final";
            }
            if (str.equals("優閒盃")) {
                return "Casual Cup";
            }
            if (str.equals("初級盃")) {
                return "Elementary Cup";
            }
            if (str.equals("中級盃")) {
                return "Intermediate Cup";
            }
            if (str.equals("高級盃")) {
                return "Advanced Cup";
            }
            if (str.equals("大師盃")) {
                return "Master Cup";
            }
            if (str.equals("雀友盃")) {
                return "Friends Cup";
            }
            if (str.equals("起胡番數")) {
                return "Minimum points to win";
            }
            if (str.equals("以第一名完成賽事後，才可挑戰更高水平賽事。")) {
                return "Unlock next level by getting 1st place in previous level.";
            }
            if (str.equals("此比賽不許重新挑戰或續關。")) {
                return "Ticket continue or restart is not allowed for this competition.";
            }
            if (str.equals("每24小時自動補充10張遊戲劵。")) {
                return "10 new tickets given daily.";
            }
            if (str.equals("圈數")) {
                return "Rounds";
            }
            if (str.equals("用2張遊戲劵重新挑戰此階段")) {
                return "Restart current match for 2 tickets";
            }
            if (str.equals("重新挑戰")) {
                return "Restart match";
            }
        }
        return str;
    }

    public static int a(int i, int i2, int i3) {
        return (int) ((((float) i2) / ((float) i3)) * ((float) i));
    }

    public static Point a(int i, int i2, int i3, int i4, int i5) {
        Point point = new Point();
        point.x = ((i % i2) * i3) + 110;
        point.y = ((i / i2) * i4) + i5;
        return point;
    }

    public static b a(a.b.d.a aVar) {
        Vector aq;
        boolean z;
        b bVar = new b();
        bVar.c(W("currentWind"), aVar.bS());
        bVar.c(W("currentRound"), aVar.bR());
        bVar.c(W("minFans"), aVar.ca());
        bVar.c(W("startHost"), aVar.bU());
        bVar.c(W("currentHost"), aVar.bV());
        bVar.c(W("currentCircle"), aVar.bT());
        bVar.c(W("lastAction"), aVar.bY());
        bVar.c(W("activeCardOwner"), aVar.bZ());
        bVar.c(W("consecutiveHost"), 0);
        int bZ = (aVar.bZ() < 0 || aVar.bY() != 0 || !aVar.cb().aO()) ? ((aVar.bZ() < 0 || aVar.bY() == 0) && aVar.bO().size() != 0) ? -1 : aVar.bZ() : (aVar.bZ() + 1) % 4;
        bVar.c(W("remainingCardCount"), aVar.bX());
        bVar.c(W("loser"), aVar.bM().size() > 0 ? ((f) aVar.bM().firstElement()).cu() : -1);
        bVar.a(W("isGameOver"), aVar.cd());
        bVar.a(W("lastActiveCard"), aVar.cb().t(""));
        bVar.a(W("playedCards"), aVar.S("."));
        a.b.c.a.a bP = aVar.bP();
        a.b.c.a.b[] bN = aVar.bN();
        b.a.a aVar2 = new b.a.a();
        int i = 0;
        int i2 = bZ;
        while (true) {
            int i3 = i;
            if (i3 >= bN.length) {
                break;
            }
            int i4 = 0;
            while (true) {
                int i5 = i4;
                if (i5 >= aVar.bM().size()) {
                    z = false;
                    break;
                } else if (((f) aVar.bM().elementAt(i5)).cv() == i3) {
                    z = true;
                    break;
                } else {
                    i4 = i5 + 1;
                }
            }
            b bVar2 = new b();
            bVar2.a(W("name"), bN[i3].fZ());
            bVar2.c(W("money"), bN[i3].gd);
            bVar2.c(W("totalFans"), bN[i3].ge);
            bVar2.a(W("isActive"), bP.kr == bN[i3]);
            bVar2.a(W("tileText"), bN[i3].gb().aR("."));
            if (z) {
                bVar2.a(W("privateGongTiles"), bN[i3].gb().aS("."));
            }
            bVar2.a(W("flowers"), bN[i3].gb().aT("."));
            bVar2.a(W("exposedTiles"), bN[i3].gb().b(".", !aVar.cd() && !z));
            bVar2.c(W("listenType"), bN[i3].gb().yQ);
            bVar2.a(W("playedTiles"), bN[i3].gb().fD());
            if (i2 == -1 && bP.kr == bN[i3]) {
                i2 = i3;
            }
            if (bP.kr == bN[i3]) {
                b bVar3 = new b();
                bVar3.a(W("canWu"), bP.ks);
                bVar3.a(W("canGongTiles"), bP.kr.gb().yQ == 0 ? d.e(bP.kt, ".") : "");
                bVar3.a(W("canPong"), bP.ku);
                bVar3.c(W("canShungType"), bP.kv);
                bVar3.a(W("canGetTile"), bP.kw);
                bVar3.a(W("canGiveup"), aVar.bQ() > 1 || (bP.ks && aVar.bX() == 0));
                bVar3.c(W("canListenType"), bP.kx);
                String W = W("prohibitedTiles");
                String str = "";
                Enumeration elements = bN[i3].gb().fF().elements();
                while (elements.hasMoreElements()) {
                    str = String.valueOf(str) + ((a) elements.nextElement()).t(".");
                }
                bVar3.a(W, str);
                bVar2.a(W("controlBlock"), bVar3);
            }
            if (bN[i3].gb().fH() != null) {
                b.a.a aVar3 = new b.a.a();
                Vector fH = bN[i3].gb().fH();
                int i6 = 0;
                while (true) {
                    int i7 = i6;
                    if (i7 >= fH.size()) {
                        break;
                    }
                    a.a.g gVar = (a.a.g) fH.elementAt(i7);
                    b bVar4 = new b();
                    bVar4.a(W("desc"), gVar.cx());
                    bVar4.c(W("count"), gVar.mo);
                    bVar4.c(W("fan"), gVar.nV);
                    bVar4.c(W("rankingFan"), gVar.nW);
                    bVar4.a(W("isCompoundFans"), gVar.nX);
                    aVar3.b(bVar4);
                    i6 = i7 + 1;
                }
                bVar2.a(W("unqualifiedFanVect"), aVar3);
            }
            bVar2.a(W("unMaskedExposedTiles"), bN[i3].gb().b(".", false));
            aVar2.b(bVar2);
            i = i3 + 1;
        }
        bVar.c(W("pseudoActivePlayer"), i2);
        bVar.a(W("playerInfo"), aVar2);
        Vector bM = aVar.bM();
        if (bM.size() > 0) {
            b.a.a aVar4 = new b.a.a();
            int i8 = 0;
            while (true) {
                int i9 = i8;
                if (i9 >= bM.size()) {
                    break;
                }
                b bVar5 = new b();
                a fz = aVar.bN()[((a.b.c.b.a) bM.elementAt(i9)).lJ].gb().fz();
                if (!(fz == null || (aq = ((a.b.c.b.a) bM.elementAt(i9)).aq(fz.t(""))) == null)) {
                    bVar5.a(W("winningHand"), d.d(aq, "."));
                }
                bVar5.c(W("winner"), ((a.b.c.b.a) bM.elementAt(i9)).lJ);
                bVar5.c(W("loser"), ((a.b.c.b.a) bM.elementAt(i9)).lK);
                bVar5.c(W("forPlayer"), ((a.b.c.b.a) bM.elementAt(i9)).lL);
                bVar5.c(W("responsiblePlayer"), aVar.bN()[((a.b.c.b.a) bM.elementAt(i9)).lJ].lM);
                bVar5.c(W("money0"), ((a.b.c.b.a) bM.elementAt(i9)).cw()[0]);
                bVar5.c(W("money1"), ((a.b.c.b.a) bM.elementAt(i9)).cw()[1]);
                bVar5.c(W("money2"), ((a.b.c.b.a) bM.elementAt(i9)).cw()[2]);
                bVar5.c(W("money3"), ((a.b.c.b.a) bM.elementAt(i9)).cw()[3]);
                bVar5.a(W("isRobGong"), ((a.b.c.b.a) bM.elementAt(i9)).lQ);
                b.a.a aVar5 = new b.a.a();
                Vector cJ = ((a.b.c.b.a) bM.elementAt(i9)).cJ();
                int i10 = 0;
                while (true) {
                    int i11 = i10;
                    if (i11 >= cJ.size()) {
                        break;
                    }
                    a.a.g gVar2 = (a.a.g) cJ.elementAt(i11);
                    b bVar6 = new b();
                    bVar6.a(W("desc"), gVar2.cx());
                    bVar6.c(W("count"), gVar2.mo);
                    bVar6.c(W("fan"), gVar2.nV);
                    bVar6.c(W("rankingFan"), gVar2.nW);
                    bVar6.a(W("isCompoundFans"), gVar2.nX);
                    aVar5.b(bVar6);
                    i10 = i11 + 1;
                }
                bVar5.a(W("winningFanVect"), aVar5);
                aVar4.b(bVar5);
                i8 = i9 + 1;
            }
            bVar.a(W("winners"), aVar4);
        }
        b bVar7 = new b();
        bVar7.a("mj16gameinfo", bVar);
        return bVar7;
    }

    public static synchronized g a(int i, a.b.d.a aVar) {
        g gVar;
        synchronized (af.class) {
            try {
                mj = a(aVar);
                synchronized (gO) {
                    a(mj, i);
                }
            } catch (b.a.f e) {
                e.printStackTrace();
            }
            gVar = gO;
        }
        return gVar;
    }

    public static g a(b bVar, int i) {
        b F = bVar.F("mj16gameinfo");
        try {
            gO.V = com.epoint.android.games.mjfgbfree.a.d.B(F.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        for (int i2 = 0; i2 < gO.wd.length; i2++) {
            gO.wd[i2] = 0;
        }
        a(F.getString(W("playedCards")), gO.vM);
        gO.aG = i;
        gO.kY = F.getInt(W("currentWind"));
        gO.la = F.getInt(W("currentRound"));
        gO.kS = F.getInt(W("minFans"));
        gO.lc = F.getInt(W("startHost"));
        gO.lb = F.getInt(W("currentHost"));
        gO.kZ = F.getInt(W("currentCircle"));
        gO.lh = F.getInt(W("lastAction"));
        gO.li = F.getInt(W("activeCardOwner"));
        gO.vO = F.getString(W("lastActiveCard"));
        gO.vL = F.getInt(W("consecutiveHost"));
        gO.vR = F.getInt(W("pseudoActivePlayer"));
        gO.vS = F.getInt(W("remainingCardCount"));
        gO.le = F.getBoolean(W("isGameOver"));
        gO.vT = F.getInt(W("loser"));
        gO.we.clear();
        b.a.a E = F.E(W("playerInfo"));
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= E.length()) {
                break;
            }
            b m = E.m(i4);
            gO.vU[i4].ga = m.getBoolean(W("isActive"));
            gO.vP = true;
            gO.vU[i4].xx = m.getString(W("name"));
            gO.vU[i4].gb = m.getInt(W("listenType")) != 0;
            gO.vU[i4].gd = m.getInt(W("money"));
            gO.vU[i4].ge = m.L(W("totalFans"));
            gO.vU[i4].gf = 0;
            gO.vU[i4].gg = 0;
            gO.vU[i4].xz = false;
            gO.vU[i4].fV.removeAllElements();
            a(m.getString(W("playedTiles")), gO.vU[i4].fW);
            a(m.getString(W("tileText")), gO.vU[i4].fV);
            if (!m.H(W("privateGongTiles"))) {
                a(m.getString(W("privateGongTiles")), gO.vU[i4].gc);
            }
            a(m.getString(W("flowers")), gO.vU[i4].fZ);
            a(m.getString(W("exposedTiles")), gO.vU[i4].fX);
            if (i4 == i) {
                a(m.getString(W("unMaskedExposedTiles")), gO.vU[i4].fY);
                if (!m.H(W("controlBlock"))) {
                    b F2 = m.F(W("controlBlock"));
                    gO.wa = F2.getBoolean(W("canWu"));
                    a(F2.getString(W("canGongTiles")), gO.vV);
                    gO.vX = F2.getBoolean(W("canPong"));
                    gO.wb = F2.getInt(W("canShungType"));
                    gO.vW = F2.getBoolean(W("canGetTile"));
                    gO.vZ = F2.getBoolean(W("canGiveup"));
                    gO.vY = F2.getInt(W("canListenType")) != 0;
                    a(F2.getString(W("prohibitedTiles")), gO.wc);
                } else {
                    gO.vV.removeAllElements();
                    g gVar = gO;
                    g gVar2 = gO;
                    g gVar3 = gO;
                    g gVar4 = gO;
                    gO.vY = false;
                    gVar4.vW = false;
                    gVar3.wa = false;
                    gVar2.vX = false;
                    gVar.vZ = false;
                    gO.wb = 0;
                }
                if (!m.H(W("unqualifiedFanVect"))) {
                    b.a.a E2 = m.E(W("unqualifiedFanVect"));
                    int i5 = 0;
                    while (true) {
                        int i6 = i5;
                        if (i6 >= E2.length()) {
                            break;
                        }
                        b m2 = E2.m(i6);
                        gO.we.add(new a.a.g(m2.getInt(W("fan")), m2.getInt(W("rankingFan")), m2.getString(W("desc")), m2.getBoolean(W("isCompoundFans")), m2.getInt(W("count"))));
                        i5 = i6 + 1;
                    }
                }
            }
            i3 = i4 + 1;
        }
        if (gO.vU[i].fV.size() % 3 == 2) {
            int i7 = 0;
            while (i7 < gO.vU.length) {
                g(i7 == i ? gO.vU[i7].fY : gO.vU[i7].fX);
                i7++;
            }
            g(gO.vU[i].fV);
            g(gO.vM);
        }
        boolean[] zArr = {true, true, true, true};
        zArr[gO.aG] = false;
        gO.wf.clear();
        if (!F.H(W("winners"))) {
            b.a.a E3 = F.E(W("winners"));
            int i8 = 0;
            while (true) {
                int i9 = i8;
                if (i9 >= E3.length()) {
                    break;
                }
                b m3 = E3.m(i9);
                com.epoint.android.games.mjfgbfree.d.c cVar = new com.epoint.android.games.mjfgbfree.d.c();
                cVar.lJ = m3.getInt(W("winner"));
                zArr[cVar.lJ] = false;
                cVar.lK = m3.getInt(W("loser"));
                cVar.lL = m3.getInt(W("forPlayer"));
                cVar.lM = m3.getInt(W("responsiblePlayer"));
                cVar.lO[0] = m3.getInt(W("money0"));
                cVar.lO[1] = m3.getInt(W("money1"));
                cVar.lO[2] = m3.getInt(W("money2"));
                cVar.lO[3] = m3.getInt(W("money3"));
                cVar.lQ = m3.getBoolean(W("isRobGong"));
                if (!m3.H(W("winningHand"))) {
                    a(m3.getString(W("winningHand")), cVar.lP);
                }
                b.a.a E4 = m3.E(W("winningFanVect"));
                int i10 = 0;
                while (true) {
                    int i11 = i10;
                    if (i11 >= E4.length()) {
                        break;
                    }
                    b m4 = E4.m(i11);
                    cVar.lN.add(new a.a.g(m4.getInt(W("fan")), m4.getInt(W("rankingFan")), m4.getString(W("desc")), m4.getBoolean(W("isCompoundFans")), m4.getInt(W("count"))));
                    cVar.ge += m4.getInt(W("fan"));
                    i10 = i11 + 1;
                }
                gO.wf.add(cVar);
                i8 = i9 + 1;
            }
        }
        if (!gO.le) {
            for (int i12 = 0; i12 < gO.vU.length; i12++) {
                if (zArr[i12]) {
                    Vector vector = gO.vU[i12].fV;
                    for (int i13 = 0; i13 < vector.size(); i13++) {
                        vector.add("BK");
                        vector.remove(0);
                    }
                }
            }
        }
        if (gO.wf.size() > 0) {
            gO.lh = 1;
            gO.li = ((com.epoint.android.games.mjfgbfree.d.c) gO.wf.lastElement()).lJ;
        }
        gO.vK = false;
        return gO;
    }

    public static String a(a.a.g gVar) {
        return e(gVar.cx(), gVar.mo);
    }

    private static void a(String str, Vector vector) {
        int length = ".".length();
        Vector vector2 = new Vector();
        int i = 0;
        while (true) {
            int indexOf = str.indexOf(".", i);
            if (indexOf == -1) {
                break;
            }
            vector2.addElement(str.substring(i, indexOf));
            i = indexOf + length;
        }
        String substring = str.substring(i);
        if (!substring.equals("") && !substring.equals("\n")) {
            vector2.addElement(substring);
        }
        String[] strArr = new String[vector2.size()];
        for (int i2 = 0; i2 < strArr.length; i2++) {
            strArr[i2] = (String) vector2.elementAt(i2);
        }
        vector.removeAllElements();
        for (int i3 = 0; i3 < strArr.length; i3++) {
            if (strArr[i3].length() > 0 && strArr[i3].length() <= 3) {
                vector.addElement(strArr[i3]);
            }
        }
    }

    public static String aa(String str) {
        if (ai.nk == 2) {
            return (str.equals("食糊long") || str.equals("食糊exp")) ? "食糊" : (str.equals("自摸long") || str.equals("自摸exp")) ? "自摸" : str.equals("圈long") ? "圈" : str.equals("花糊exp") ? "花糊" : str;
        }
        if (ai.nk == 1) {
            if (str.equals("摸牌")) {
                return "Get";
            }
            if (str.equals("打牌")) {
                return "Discard";
            }
            if (str.equals("放棄")) {
                return "Pass";
            }
            if (str.equals("上")) {
                return "Chow";
            }
            if (str.equals("碰")) {
                return "Pung";
            }
            if (str.equals("槓")) {
                return "Kong";
            }
            if (str.equals("搶槓")) {
                return "Rob";
            }
            if (str.equals("自摸") || str.equals("自摸exp") || str.equals("食糊") || str.equals("食糊exp")) {
                return "Win";
            }
            if (str.equals("聽牌")) {
                return "Ready";
            }
            if (str.equals("繼續")) {
                return "Next";
            }
            if (str.equals("詳情")) {
                return "Details";
            }
            if (str.equals("牌牆")) {
                return "Wall";
            }
            if (str.equals("圈")) {
                return "";
            }
            if (str.equals("東")) {
                return "E.";
            }
            if (str.equals("南")) {
                return "S.";
            }
            if (str.equals("西")) {
                return "W.";
            }
            if (str.equals("北")) {
                return "N.";
            }
            if (str.equals("補牌")) {
                return "Get";
            }
            if (str.equals("補花")) {
                return "Flower";
            }
            if (str.equals("花糊") || str.equals("花糊exp")) {
                return "Win";
            }
            if (str.equals("出沖")) {
                return "Discard";
            }
            if (str.equals("起莊")) {
                return "Start";
            }
            if (str.equals("莊")) {
                return "D.";
            }
            if (str.equals("連")) {
                return "C.";
            }
            if (str.equals("取消")) {
                return "Cancel";
            }
            if (str.equals("請選擇組合")) {
                return "Pick a combination";
            }
            if (str.equals("略過")) {
                return "Skip";
            }
            if (str.equals("是")) {
                return "Yes";
            }
            if (str.equals("否")) {
                return "No";
            }
            if (str.equals("好")) {
                return "OK";
            }
            if (str.equals("抓圖")) {
                return "Capture";
            }
            if (str.equals("關閉")) {
                return "Close";
            }
            if (str.equals("分享")) {
                return "Share";
            }
            if (str.equals("你")) {
                return "You";
            }
            if (str.equals("上家")) {
                return "Left";
            }
            if (str.equals("對家")) {
                return "Opposite";
            }
            if (str.equals("下家")) {
                return "Right";
            }
            if (str.equals("荒牌")) {
                return "Draw game";
            }
            if (str.equals("番")) {
                return " pt";
            }
            if (str.equals("結算")) {
                return "Scoring";
            }
            if (str.equals("食糊long")) {
                return "By-Discard ";
            }
            if (str.equals("自摸long")) {
                return "Self-Drawn ";
            }
            if (str.equals("棄此牌後叫糊")) {
                return "Winning tile(s)";
            }
            if (str.equals("雖然糊牌成立，可惜只有")) {
                return "But your Mahjong Hand scores only ";
            }
            if (str.equals("不能食糊。糊牌最低要求為")) {
                return "Couldn't declare win. Required: ";
            }
            if (str.equals("連線中斷，遊戲結束")) {
                return "Connection lost!";
            }
            if (str.equals("(不計花牌番數)")) {
                return "(Excluding Flower pts.)";
            }
            if (str.equals("繼續遊戲？")) {
                return "Restore saved game?";
            }
            if (str.equals("績分榜")) {
                return "Score";
            }
            if (str.equals("沒有紀錄")) {
                return "No record";
            }
            if (str.equals("盤")) {
                return "Hands";
            }
            if (str.equals("本局結束")) {
                return "Final Results";
            }
            if (str.equals("個")) {
                return " x ";
            }
            if (str.equals("牌型紀錄")) {
                return "Type Record";
            }
            if (str.equals("胡牌番數統計")) {
                return "Score Record";
            }
            if (str.equals("玩家名字")) {
                return "Player's Name";
            }
            if (str.equals("自動摸牌")) {
                return "Auto Get Tile";
            }
            if (str.equals("動畫效果")) {
                return "Animation";
            }
            if (str.equals("音效")) {
                return "Sound Effect";
            }
            if (str.equals("麻雀牌風格")) {
                return "Mahjong Tile Style";
            }
            if (str.equals("頭像")) {
                return "Character";
            }
            if (str.equals("畫面顯示方向")) {
                return "Screen Orientation";
            }
            if (str.equals("語言")) {
                return "Language";
            }
            if (str.equals("雀友麻雀")) {
                return "Mahjong & Friends";
            }
            if (str.equals("繼續遊戲")) {
                return "Continue game";
            }
            if (str.equals("開始新遊戲")) {
                return "Start new game";
            }
            if (str.equals("離開遊戲")) {
                return "Quit game";
            }
            if (str.equals("返回主畫面")) {
                return "Back to title screen";
            }
            if (str.equals("規則")) {
                return "Rules";
            }
            if (str.equals("牌型")) {
                return "Types";
            }
            if (str.equals("關於")) {
                return "About";
            }
            if (str.equals("開始")) {
                return "Play";
            }
            if (str.equals("小統計")) {
                return "Statistics";
            }
            if (str.equals("設定")) {
                return "Settings";
            }
            if (str.equals("遊戲規則")) {
                return "How to play";
            }
            if (str.equals("版本")) {
                return "Version";
            }
            if (str.equals("網路模式")) {
                return "Network mode";
            }
            if (str.equals("裝置清單")) {
                return "Device List";
            }
            if (str.equals("未能啟動藍牙裝置")) {
                return "Unable to initiate Bluetooth device";
            }
            if (str.equals("裝置目前已在可被偵測狀態")) {
                return "Device is already discoverable.";
            }
            if (str.equals("等待其他裝置連線")) {
                return "Waiting for incomming connection";
            }
            if (str.equals("停止等候")) {
                return "Stop hosting";
            }
            if (str.equals("等候狀態中不能主動連接其他裝置")) {
                return "Unable to connect while waiting for incomming connection.";
            }
            if (str.equals("偵測狀能中不能連線")) {
                return "Unable to connect while scanning.";
            }
            if (str.equals("連接到")) {
                return "Connect with";
            }
            if (str.equals("已配對的裝置")) {
                return "Paired devices";
            }
            if (str.equals("連線失敗")) {
                return "Unable to establish connection.";
            }
            if (str.equals("已取消")) {
                return "Aborted.";
            }
            if (str.equals("偵測中")) {
                return "Scanning";
            }
            if (str.equals("停止偵測")) {
                return "Stop scanning";
            }
            if (str.equals("主持")) {
                return "Host";
            }
            if (str.equals("賓客")) {
                return "Guest";
            }
            if (str.equals("開始")) {
                return "Start";
            }
            if (str.equals("離開")) {
                return "Quit";
            }
            if (str.equals("取消連線")) {
                return "Drop connection";
            }
            if (str.equals("使裝置可被偵測")) {
                return "Make device discoverable";
            }
            if (str.equals("偵測新裝置")) {
                return "Scan for new devices";
            }
            if (str.equals("已配對的裝置")) {
                return "Paired devices";
            }
            if (str.equals("沒有已配對的裝置")) {
                return "No paired device";
            }
            if (str.equals("沒有其他新裝置")) {
                return "No new device";
            }
            if (str.equals("主持遊戲")) {
                return "Host a game";
            }
            if (str.equals("新裝置")) {
                return "New devices";
            }
            if (str.equals("連接伺服器")) {
                return "Connect to server";
            }
            if (str.equals("連線")) {
                return "Connect";
            }
            if (str.equals("連線已中斷")) {
                return "Disconnected";
            }
            if (str.equals("已連線的玩家")) {
                return "Connected players";
            }
            if (str.equals("請輸入Game Key")) {
                return "Please enter the Game Key";
            }
            if (str.equals("設立遊戲密碼(可留空)")) {
                return "Password for joining your game. (Optional)";
            }
            if (str.equals("密碼")) {
                return "Password";
            }
            if (str.equals("密碼已傳送至剪貼簿")) {
                return "Password copied to clipboard.";
            }
            if (str.equals("Game Key已傳至剪貼簿")) {
                return "Game Key copied to clipboard.";
            }
            if (str.equals("遊戲密碼(如有)")) {
                return "Host password (If any)";
            }
            if (str.equals("設定IP地址")) {
                return "Setup IP address";
            }
            if (str.equals("錯誤的IP地址")) {
                return "Invalid IP address";
            }
            if (str.equals("沒有可用連線")) {
                return "No connection";
            }
            if (str.equals("IP地址")) {
                return "IP Address";
            }
            if (str.equals("伺服器")) {
                return "Server";
            }
            if (str.equals("遊戲模式")) {
                return "Game mode";
            }
            if (str.equals("本機遊戲")) {
                return "Local game";
            }
            if (str.equals("連線: 藍牙")) {
                return "Network: Bluetooth";
            }
            if (str.equals("連線: 網路")) {
                return "Network: LAN/TCP";
            }
            if (str.equals("連線: 伺服器")) {
                return "Network: Server";
            }
            if (str.equals("分享截圖")) {
                return "Share screen-shot";
            }
            if (str.equals("截圖失敗")) {
                return "Capture failed";
            }
            if (str.equals("太多檔案")) {
                return "Too many files";
            }
            if (str.equals("截圖已儲存至")) {
                return "Captured to";
            }
            if (str.equals("廣告支持版")) {
                return "Ad-supported";
            }
            if (str.equals("牌型紀錄")) {
                return "Types";
            }
            if (str.equals("番數統計")) {
                return "Pts Stat.";
            }
            if (str.equals("幫助")) {
                return "Help";
            }
            if (str.equals("升級了")) {
                return "Level Up";
            }
            if (str.equals("小統計只限本機遊戲")) {
                return "Local game statistics";
            }
            if (str.equals("此功能需要Android 2.1或以上")) {
                return "This feature requires Android 2.1 or later";
            }
            if (str.equals("不要顯示動畫角色")) {
                return "Hide Characters";
            }
            if (str.equals("購買完全版")) {
                return "Buy Full Version";
            }
            if (str.equals("Free版只記錄至一星級")) {
                return "(Data is capped at 1st level in Free version.)";
            }
            if (str.equals("輕掃出牌")) {
                return "Wipe-discard Tile";
            }
            if (str.equals("每局盤數")) {
                return "Hands per Game";
            }
            if (str.equals("起胡番數")) {
                return "Minimum Points to Win";
            }
            if (str.equals("積分模式")) {
                return "Endless Mode";
            }
            if (str.equals("以全屏幕進行游戲")) {
                return "Full Screen Mode";
            }
            if (str.equals("清除游戲記錄")) {
                return "Erase Game Records";
            }
            if (str.equals("記錄清除後將不能復原，你是否要確定")) {
                return "This action cannot be undone, proceed";
            }
            if (str.equals("記錄已清除")) {
                return "Record erased";
            }
            if (str.equals("確定刪除")) {
                return "Confirm delete";
            }
            if (str.equals("設定連線")) {
                return "Edit connection";
            }
            if (str.equals("別名")) {
                return "Alias";
            }
            if (str.equals("儲存及離開")) {
                return "Save and Quit";
            }
            if (str.equals("給我們一個 Facebook\"讚\"？\n(網頁開啟後請按一下頁上的\"讚\"按鈕方為完成)")) {
                return "Give us a \"Like\" on Facebook?\n(Please click the \"Like\" button on our Facebook page to complete action.)";
            }
            if (str.equals("快速指南")) {
                return "Quick Start";
            }
            if (str.equals("圈數")) {
                return "Rounds per Game";
            }
            if (str.equals("過水")) {
                return "Pass";
            }
            if (str.equals("圈long")) {
                return "Rounds";
            }
            if (str.equals("已打出的牌")) {
                return "Discarded Tiles";
            }
            if (str.equals("背景")) {
                return "Wallpaper";
            }
            if (str.equals("發表評分，送你多個新背景！現在就為雀友麻雀評分嗎？")) {
                return "Submit your rating and get extra wallpapers! Proceed?";
            }
            if (str.equals("麻雀牌顏色")) {
                return "Tile Color";
            }
            if (str.equals("遊戲資訊")) {
                return "Game info";
            }
        } else if (ai.nk == 3) {
            if (str.equals("摸牌")) {
                return "摸牌";
            }
            if (str.equals("打牌")) {
                return "打牌";
            }
            if (str.equals("放棄")) {
                return "放弃";
            }
            if (str.equals("上")) {
                return "吃";
            }
            if (str.equals("碰")) {
                return "碰";
            }
            if (str.equals("槓")) {
                return "杠";
            }
            if (str.equals("搶槓")) {
                return "抢杠";
            }
            if (str.equals("自摸") || str.equals("自摸exp")) {
                return "自摸";
            }
            if (str.equals("食糊") || str.equals("食糊exp")) {
                return "食糊";
            }
            if (str.equals("聽牌")) {
                return "听牌";
            }
            if (str.equals("繼續")) {
                return "继续";
            }
            if (str.equals("詳情")) {
                return "详情";
            }
            if (str.equals("牌牆")) {
                return "牌墙";
            }
            if (str.equals("圈")) {
                return "圈";
            }
            if (str.equals("東")) {
                return "东";
            }
            if (str.equals("南")) {
                return "南";
            }
            if (str.equals("西")) {
                return "西";
            }
            if (str.equals("北")) {
                return "北";
            }
            if (str.equals("補牌")) {
                return "补牌";
            }
            if (str.equals("補花")) {
                return "补花";
            }
            if (str.equals("花糊") || str.equals("花糊exp")) {
                return "花糊";
            }
            if (str.equals("出沖")) {
                return "出冲";
            }
            if (str.equals("起莊")) {
                return "起庄";
            }
            if (str.equals("莊")) {
                return "庄";
            }
            if (str.equals("連")) {
                return "连";
            }
            if (str.equals("取消")) {
                return "取消";
            }
            if (str.equals("請選擇組合")) {
                return "请选择组合";
            }
            if (str.equals("略過")) {
                return "略过";
            }
            if (str.equals("是")) {
                return "是";
            }
            if (str.equals("否")) {
                return "否";
            }
            if (str.equals("好")) {
                return "好";
            }
            if (str.equals("抓圖")) {
                return "抓图";
            }
            if (str.equals("關閉")) {
                return "关闭";
            }
            if (str.equals("分享")) {
                return "分享";
            }
            if (str.equals("你")) {
                return "你";
            }
            if (str.equals("上家")) {
                return "上家";
            }
            if (str.equals("對家")) {
                return "对家";
            }
            if (str.equals("下家")) {
                return "下家";
            }
            if (str.equals("荒牌")) {
                return "荒牌";
            }
            if (str.equals("番")) {
                return "番";
            }
            if (str.equals("結算")) {
                return "结算";
            }
            if (str.equals("食糊long")) {
                return "食糊";
            }
            if (str.equals("自摸long")) {
                return "自摸";
            }
            if (str.equals("棄此牌後叫糊")) {
                return "弃此牌後叫糊";
            }
            if (str.equals("雖然糊牌成立，可惜只有")) {
                return "虽然糊牌成立，可惜只有";
            }
            if (str.equals("不能食糊。糊牌最低要求為")) {
                return "不能食糊。糊牌最低要求为";
            }
            if (str.equals("連線中斷，遊戲結束")) {
                return "连线中断，游戏结束";
            }
            if (str.equals("(不計花牌番數)")) {
                return "(不计花牌番数)";
            }
            if (str.equals("繼續遊戲？")) {
                return "继续游戏？";
            }
            if (str.equals("績分榜")) {
                return "绩分榜";
            }
            if (str.equals("沒有紀錄")) {
                return "没有纪录";
            }
            if (str.equals("盤")) {
                return "盘";
            }
            if (str.equals("本局結束")) {
                return "本局结束";
            }
            if (str.equals("個")) {
                return "个";
            }
            if (str.equals("牌型紀錄")) {
                return "牌型纪录";
            }
            if (str.equals("胡牌番數統計")) {
                return "胡牌番数统计";
            }
            if (str.equals("玩家名字")) {
                return "玩家名字";
            }
            if (str.equals("自動摸牌")) {
                return "自动摸牌";
            }
            if (str.equals("動畫效果")) {
                return "动画效果";
            }
            if (str.equals("音效")) {
                return "音效";
            }
            if (str.equals("麻雀牌風格")) {
                return "麻雀牌风格";
            }
            if (str.equals("頭像")) {
                return "头像";
            }
            if (str.equals("畫面顯示方向")) {
                return "画面显示方向";
            }
            if (str.equals("語言")) {
                return "语言";
            }
            if (str.equals("雀友麻雀")) {
                return "雀友麻雀";
            }
            if (str.equals("繼續遊戲")) {
                return "继续游戏";
            }
            if (str.equals("開始新遊戲")) {
                return "开始新游戏";
            }
            if (str.equals("離開遊戲")) {
                return "离开游戏";
            }
            if (str.equals("返回主畫面")) {
                return "返回主画面";
            }
            if (str.equals("規則")) {
                return "规则";
            }
            if (str.equals("牌型")) {
                return "牌型";
            }
            if (str.equals("關於")) {
                return "关於";
            }
            if (str.equals("開始")) {
                return "开始";
            }
            if (str.equals("小統計")) {
                return "小统计";
            }
            if (str.equals("設定")) {
                return "设定";
            }
            if (str.equals("遊戲規則")) {
                return "游戏规则";
            }
            if (str.equals("版本")) {
                return "版本";
            }
            if (str.equals("網路模式")) {
                return "网路模式";
            }
            if (str.equals("裝置清單")) {
                return "装置清单";
            }
            if (str.equals("未能啟動藍牙裝置")) {
                return "未能启动蓝牙装置";
            }
            if (str.equals("裝置目前已在可被偵測狀態")) {
                return "装置目前已在可被侦测状态";
            }
            if (str.equals("等待其他裝置連線")) {
                return "等待其他装置连线";
            }
            if (str.equals("停止等候")) {
                return "停止等候";
            }
            if (str.equals("等候狀態中不能主動連接其他裝置")) {
                return "等候状态中不能主动连接其他装置";
            }
            if (str.equals("偵測狀能中不能連線")) {
                return "侦测状能中不能连线";
            }
            if (str.equals("連接到")) {
                return "连接到";
            }
            if (str.equals("已配對的裝置")) {
                return "已配对的装置";
            }
            if (str.equals("連線失敗")) {
                return "连线失败";
            }
            if (str.equals("已取消")) {
                return "已取消";
            }
            if (str.equals("偵測中")) {
                return "侦测中";
            }
            if (str.equals("停止偵測")) {
                return "停止侦测";
            }
            if (str.equals("主持")) {
                return "主持";
            }
            if (str.equals("賓客")) {
                return "宾客";
            }
            if (str.equals("開始")) {
                return "开始";
            }
            if (str.equals("離開")) {
                return "离开";
            }
            if (str.equals("取消連線")) {
                return "取消连线";
            }
            if (str.equals("使裝置可被偵測")) {
                return "使装置可被侦测";
            }
            if (str.equals("偵測新裝置")) {
                return "侦测新装置";
            }
            if (str.equals("已配對的裝置")) {
                return "已配对的装置";
            }
            if (str.equals("沒有已配對的裝置")) {
                return "没有已配对的装置";
            }
            if (str.equals("沒有其他新裝置")) {
                return "没有其他新装置";
            }
            if (str.equals("主持遊戲")) {
                return "主持游戏";
            }
            if (str.equals("新裝置")) {
                return "新装置";
            }
            if (str.equals("連接伺服器")) {
                return "连接伺服器";
            }
            if (str.equals("連線")) {
                return "连线";
            }
            if (str.equals("連線已中斷")) {
                return "连线已中断";
            }
            if (str.equals("已連線的玩家")) {
                return "已连线的玩家";
            }
            if (str.equals("請輸入Game Key")) {
                return "请输入Game Key";
            }
            if (str.equals("設立遊戲密碼(可留空)")) {
                return "设立游戏密码(可留空)";
            }
            if (str.equals("密碼")) {
                return "密码";
            }
            if (str.equals("密碼已傳送至剪貼簿")) {
                return "密码已传送至剪贴簿";
            }
            if (str.equals("Game Key已傳至剪貼簿")) {
                return "Game Key已传送至剪贴簿";
            }
            if (str.equals("遊戲密碼(如有)")) {
                return "游戏密码(如有)";
            }
            if (str.equals("設定IP地址")) {
                return "设定IP地址";
            }
            if (str.equals("錯誤的IP地址")) {
                return "错误的IP地址";
            }
            if (str.equals("沒有可用連線")) {
                return "没有可用连线";
            }
            if (str.equals("IP地址")) {
                return "IP地址";
            }
            if (str.equals("伺服器")) {
                return "服务器";
            }
            if (str.equals("遊戲模式")) {
                return "游戏模式";
            }
            if (str.equals("本機遊戲")) {
                return "本机游戏";
            }
            if (str.equals("連線: 藍牙")) {
                return "连线: 蓝牙";
            }
            if (str.equals("連線: 網路")) {
                return "连线: 网路";
            }
            if (str.equals("連線: 伺服器")) {
                return "连线: 服务器";
            }
            if (str.equals("分享截圖")) {
                return "分享截图";
            }
            if (str.equals("截圖失敗")) {
                return "截图失败";
            }
            if (str.equals("太多檔案")) {
                return "太多档案";
            }
            if (str.equals("截圖已儲存至")) {
                return "截图已储存至";
            }
            if (str.equals("廣告支持版")) {
                return "广告支持版";
            }
            if (str.equals("牌型紀錄")) {
                return "牌型纪录";
            }
            if (str.equals("番數統計")) {
                return "番数统计";
            }
            if (str.equals("幫助")) {
                return "帮助";
            }
            if (str.equals("升級了")) {
                return "升级了";
            }
            if (str.equals("小統計只限本機遊戲")) {
                return "小统计只限本机游戏";
            }
            if (str.equals("此功能需要Android 2.1或以上")) {
                return "此功能需要Android 2.1或以上";
            }
            if (str.equals("不要顯示動畫角色")) {
                return "不要显示动画角色";
            }
            if (str.equals("購買完全版")) {
                return "购买完全版";
            }
            if (str.equals("Free版只記錄至一星級")) {
                return "Free版只记录至一星级";
            }
            if (str.equals("輕掃出牌")) {
                return "轻扫出牌";
            }
            if (str.equals("每局盤數")) {
                return "每局盘数";
            }
            if (str.equals("起胡番數")) {
                return "起胡番数";
            }
            if (str.equals("積分模式")) {
                return "积分模式";
            }
            if (str.equals("以全屏幕進行游戲")) {
                return "以全屏幕进行游戏";
            }
            if (str.equals("清除游戲記錄")) {
                return "清除游戏记录";
            }
            if (str.equals("記錄清除後將不能復原，你是否要確定")) {
                return "记录清除後将不能复原，你是否要确定";
            }
            if (str.equals("記錄已清除")) {
                return "记录已清除";
            }
            if (str.equals("確定刪除")) {
                return "确认删除";
            }
            if (str.equals("設定連線")) {
                return "设定连线";
            }
            if (str.equals("別名")) {
                return "别名";
            }
            if (str.equals("儲存及離開")) {
                return "储存及离开";
            }
            if (str.equals("給我們一個 Facebook\"讚\"？\n(網頁開啟後請按一下頁上的\"讚\"按鈕方為完成)")) {
                return "给我们一个 Facebook\"赞\"？\n(网页开启后请按一下页上的\"赞\"按钮方为完成)";
            }
            if (str.equals("快速指南")) {
                return "快速指南";
            }
            if (str.equals("圈數")) {
                return "圈数";
            }
            if (str.equals("過水")) {
                return "过水";
            }
            if (str.equals("圈long")) {
                return "圈";
            }
            if (str.equals("已打出的牌")) {
                return "已打出的牌";
            }
            if (str.equals("背景")) {
                return "背景";
            }
            if (str.equals("發表評分，送你3個新背景！現在就為雀友麻雀評分嗎？")) {
                return "发表评分，送你3个新背景！现在就为雀友麻雀评分吗？";
            }
            if (str.equals("麻雀牌顏色")) {
                return "麻雀牌颜色";
            }
            if (str.equals("遊戲資訊")) {
                return "游戏资讯";
            }
        } else if (ai.nk == 4) {
            if (str.equals("摸牌")) {
                return "取得";
            }
            if (str.equals("打牌")) {
                return "捨てる";
            }
            if (str.equals("放棄")) {
                return "パス";
            }
            if (str.equals("上")) {
                return "チー";
            }
            if (str.equals("碰")) {
                return "ポン";
            }
            if (str.equals("槓")) {
                return "カン";
            }
            if (str.equals("搶槓")) {
                return "槍槓";
            }
            if (str.equals("自摸") || str.equals("自摸exp")) {
                return "ツモ";
            }
            if (str.equals("食糊") || str.equals("食糊exp")) {
                return "ロン";
            }
            if (str.equals("聽牌")) {
                return "リーチ";
            }
            if (str.equals("繼續")) {
                return "次へ";
            }
            if (str.equals("詳情")) {
                return "詳細";
            }
            if (str.equals("牌牆")) {
                return "残り";
            }
            if (str.equals("圈")) {
                return "";
            }
            if (str.equals("東")) {
                return "東";
            }
            if (str.equals("南")) {
                return "南";
            }
            if (str.equals("西")) {
                return "西";
            }
            if (str.equals("北")) {
                return "北";
            }
            if (str.equals("補牌")) {
                return "取得";
            }
            if (str.equals("補花")) {
                return "花";
            }
            if (str.equals("花糊") || str.equals("花糊exp")) {
                return "花和";
            }
            if (str.equals("出沖")) {
                return "放銃";
            }
            if (str.equals("起莊")) {
                return "起荘";
            }
            if (str.equals("莊")) {
                return "親";
            }
            if (str.equals("連")) {
                return "連";
            }
            if (str.equals("取消")) {
                return "取消";
            }
            if (str.equals("請選擇組合")) {
                return "組合せを選んでください";
            }
            if (str.equals("略過")) {
                return "スキップ";
            }
            if (str.equals("是")) {
                return "はい";
            }
            if (str.equals("否")) {
                return "いいえ";
            }
            if (str.equals("好")) {
                return "OK";
            }
            if (str.equals("抓圖")) {
                return "キャプ";
            }
            if (str.equals("關閉")) {
                return "閉じる";
            }
            if (str.equals("分享")) {
                return "共有する";
            }
            if (str.equals("你")) {
                return "あなた";
            }
            if (str.equals("上家")) {
                return "上家";
            }
            if (str.equals("對家")) {
                return "対家";
            }
            if (str.equals("下家")) {
                return "下家";
            }
            if (str.equals("荒牌")) {
                return "流局";
            }
            if (str.equals("番")) {
                return "翻";
            }
            if (str.equals("結算")) {
                return "点数計算";
            }
            if (str.equals("食糊long")) {
                return "栄和";
            }
            if (str.equals("自摸long")) {
                return "自摸和";
            }
            if (str.equals("棄此牌後叫糊")) {
                return "和了牌";
            }
            if (str.equals("雖然糊牌成立，可惜只有")) {
                return "和了しましたが、あなたの翻数は XX しかありません。";
            }
            if (str.equals("不能食糊。糊牌最低要求為")) {
                return "和了できません。 XX が必要です。";
            }
            if (str.equals("連線中斷，遊戲結束")) {
                return "接続が切断されました。";
            }
            if (str.equals("(不計花牌番數)")) {
                return "（花牌以外の翻数）";
            }
            if (str.equals("繼續遊戲？")) {
                return "ゲームを続けますか？";
            }
            if (str.equals("績分榜")) {
                return "スコア";
            }
            if (str.equals("沒有紀錄")) {
                return "記録がありません";
            }
            if (str.equals("盤")) {
                return "局";
            }
            if (str.equals("本局結束")) {
                return "結果";
            }
            if (str.equals("個")) {
                return " x ";
            }
            if (str.equals("牌型紀錄")) {
                return "役の記録";
            }
            if (str.equals("胡牌番數統計")) {
                return "スコアの記録";
            }
            if (str.equals("玩家名字")) {
                return "プレイヤーの名前";
            }
            if (str.equals("自動摸牌")) {
                return "自動ツモ";
            }
            if (str.equals("動畫效果")) {
                return "アニメーション";
            }
            if (str.equals("音效")) {
                return "サウンドエフェクト";
            }
            if (str.equals("麻雀牌風格")) {
                return "麻雀牌のスタイル";
            }
            if (str.equals("頭像")) {
                return "キャラクター";
            }
            if (str.equals("畫面顯示方向")) {
                return "画面の方向";
            }
            if (str.equals("語言")) {
                return "言語";
            }
            if (str.equals("雀友麻雀")) {
                return "雀友麻雀";
            }
            if (str.equals("繼續遊戲")) {
                return "ゲームを続ける";
            }
            if (str.equals("開始新遊戲")) {
                return "新しいゲームを開始";
            }
            if (str.equals("離開遊戲")) {
                return "ゲームを終了しますか";
            }
            if (str.equals("返回主畫面")) {
                return "タイトル画面に戻る";
            }
            if (str.equals("規則")) {
                return "ルール";
            }
            if (str.equals("牌型")) {
                return "役";
            }
            if (str.equals("關於")) {
                return "About";
            }
            if (str.equals("開始")) {
                return "スタート";
            }
            if (str.equals("小統計")) {
                return "データ";
            }
            if (str.equals("設定")) {
                return "設定";
            }
            if (str.equals("遊戲規則")) {
                return "遊び方";
            }
            if (str.equals("版本")) {
                return "バージョン";
            }
            if (str.equals("網路模式")) {
                return "ネットワークモード";
            }
            if (str.equals("裝置清單")) {
                return "デバイスリスト";
            }
            if (str.equals("未能啟動藍牙裝置")) {
                return "Bluetoothデバイスを起動できません";
            }
            if (str.equals("裝置目前已在可被偵測狀態")) {
                return "デバイスはすでに発見可能です";
            }
            if (str.equals("等待其他裝置連線")) {
                return "接続を待っています";
            }
            if (str.equals("停止等候")) {
                return "待機を中止する";
            }
            if (str.equals("等候狀態中不能主動連接其他裝置")) {
                return "接続待機中のため他のデバイスは接続できません";
            }
            if (str.equals("偵測狀能中不能連線")) {
                return "スキャン中は接続できません";
            }
            if (str.equals("連接到")) {
                return "XX に接続ですか";
            }
            if (str.equals("已配對的裝置")) {
                return "ペアリングしたデバイス";
            }
            if (str.equals("連線失敗")) {
                return "接続できません";
            }
            if (str.equals("已取消")) {
                return "中止しました";
            }
            if (str.equals("偵測中")) {
                return "検索中";
            }
            if (str.equals("停止偵測")) {
                return "検索を中止";
            }
            if (str.equals("主持")) {
                return "ホスト";
            }
            if (str.equals("賓客")) {
                return "ゲスト";
            }
            if (str.equals("開始")) {
                return "スタート";
            }
            if (str.equals("離開")) {
                return "終了";
            }
            if (str.equals("取消連線")) {
                return "接続を取り消す";
            }
            if (str.equals("使裝置可被偵測")) {
                return "デバイスを発見可能にする";
            }
            if (str.equals("偵測新裝置")) {
                return "新しいデバイスを検索";
            }
            if (str.equals("已配對的裝置")) {
                return "ペアリングしたデバイス";
            }
            if (str.equals("沒有已配對的裝置")) {
                return "ペアリングしたデバイスがありません";
            }
            if (str.equals("沒有其他新裝置")) {
                return "新しいデバイスはありません";
            }
            if (str.equals("主持遊戲")) {
                return "ゲームを主催する";
            }
            if (str.equals("新裝置")) {
                return "新しいデバイス";
            }
            if (str.equals("連接伺服器")) {
                return "サーバーに接続";
            }
            if (str.equals("連線")) {
                return "接続";
            }
            if (str.equals("連線已中斷")) {
                return "接続を中断しました";
            }
            if (str.equals("已連線的玩家")) {
                return "プレイヤーに接続しました";
            }
            if (str.equals("請輸入Game Key")) {
                return "ゲームのKeyを入力してください";
            }
            if (str.equals("設立遊戲密碼(可留空)")) {
                return "ゲームに参加するためのパスワード（省略可）";
            }
            if (str.equals("密碼")) {
                return "パスワード";
            }
            if (str.equals("密碼已傳送至剪貼簿")) {
                return "Password copied to clipboard.";
            }
            if (str.equals("Game Key已傳至剪貼簿")) {
                return "Game Key copied to clipboard.";
            }
            if (str.equals("遊戲密碼(如有)")) {
                return "ホストのパスワード（設定した場合）";
            }
            if (str.equals("設定IP地址")) {
                return "IPアドレスを設定";
            }
            if (str.equals("錯誤的IP地址")) {
                return "IPアドレスが無効です";
            }
            if (str.equals("沒有可用連線")) {
                return "接続先がありません";
            }
            if (str.equals("IP地址")) {
                return "IPアドレス";
            }
            if (str.equals("伺服器")) {
                return "サーバー";
            }
            if (str.equals("遊戲模式")) {
                return "ゲームモード";
            }
            if (str.equals("本機遊戲")) {
                return "オフラインゲーム";
            }
            if (str.equals("連線: 藍牙")) {
                return "ネットワーク: Bluetooth";
            }
            if (str.equals("連線: 網路")) {
                return "ネットワーク: LAN/TCP";
            }
            if (str.equals("連線: 伺服器")) {
                return "ネットワーク: サーバー";
            }
            if (str.equals("分享截圖")) {
                return "スクリーンショットを共有";
            }
            if (str.equals("截圖失敗")) {
                return "キャプチャーに失敗しました";
            }
            if (str.equals("太多檔案")) {
                return "ファイルが多すぎます";
            }
            if (str.equals("截圖已儲存至")) {
                return "XX に保存しました";
            }
            if (str.equals("廣告支持版")) {
                return "Ad-supported";
            }
            if (str.equals("牌型紀錄")) {
                return "役の記録";
            }
            if (str.equals("番數統計")) {
                return "得点データ";
            }
            if (str.equals("幫助")) {
                return "ヘルプ";
            }
            if (str.equals("升級了")) {
                return "レベルアップ";
            }
            if (str.equals("小統計只限本機遊戲")) {
                return "オフラインゲームのデータ";
            }
            if (str.equals("此功能需要Android 2.1或以上")) {
                return "この機能にはAndroid2.1以上が必要です";
            }
            if (str.equals("不要顯示動畫角色")) {
                return "キャラクターを表示しない";
            }
            if (str.equals("購買完全版")) {
                return "フルバージョンを購入";
            }
            if (str.equals("Free版只記錄至一星級")) {
                return "(フリーバージョンではレベル1のデータしか記録できません)";
            }
            if (str.equals("輕掃出牌")) {
                return "ドラッグして捨てる";
            }
            if (str.equals("每局盤數")) {
                return "ゲームの局数";
            }
            if (str.equals("起胡番數")) {
                return "和了に必要な翻数";
            }
            if (str.equals("積分模式")) {
                return "エンドレスモード";
            }
            if (str.equals("以全屏幕進行游戲")) {
                return "フルスクリーンモード";
            }
            if (str.equals("清除游戲記錄")) {
                return "ゲームの記録を消去";
            }
            if (str.equals("記錄清除後將不能復原，你是否要確定")) {
                return "ゲームの記録を消去すると元に戻せません。消去しますか";
            }
            if (str.equals("記錄已清除")) {
                return "消去しました";
            }
            if (str.equals("確定刪除")) {
                return "消去を確定しますか";
            }
            if (str.equals("設定連線")) {
                return "接続を設定する";
            }
            if (str.equals("別名")) {
                return "別名";
            }
            if (str.equals("儲存及離開")) {
                return "保存して終了";
            }
            if (str.equals("給我們一個 Facebook\"讚\"？\n(網頁開啟後請按一下頁上的\"讚\"按鈕方為完成)")) {
                return "Facebookで“いいね！”と紹介してください！\n（Facebookの雀友麻雀のページで“いいね！”をクリックしてください）";
            }
            if (str.equals("快速指南")) {
                return "クイックスタート";
            }
            if (str.equals("圈數")) {
                return "Rounds per Game";
            }
            if (str.equals("過水")) {
                return "パス";
            }
            if (str.equals("圈long")) {
                return "Rounds";
            }
            if (str.equals("已打出的牌")) {
                return "河の牌";
            }
            if (str.equals("背景")) {
                return "壁紙";
            }
            if (str.equals("發表評分，送你多個新背景！現在就為雀友麻雀評分嗎？")) {
                return "Submit your rating and get extra wallpapers! Proceed?";
            }
            if (str.equals("麻雀牌顏色")) {
                return "麻雀牌の顏色";
            }
            if (str.equals("遊戲資訊")) {
                return "Info";
            }
        } else if (ai.nk == 5) {
            if (str.equals("摸牌")) {
                return "Piocher";
            }
            if (str.equals("打牌")) {
                return "Jeter";
            }
            if (str.equals("放棄")) {
                return "Passer";
            }
            if (str.equals("上")) {
                return "Chow";
            }
            if (str.equals("碰")) {
                return "Pung";
            }
            if (str.equals("槓")) {
                return "Kong";
            }
            if (str.equals("搶槓")) {
                return "Voler";
            }
            if (str.equals("自摸") || str.equals("食糊")) {
                return "Gagner";
            }
            if (str.equals("自摸exp") || str.equals("食糊exp")) {
                return "Gagné";
            }
            if (str.equals("聽牌")) {
                return "Ready";
            }
            if (str.equals("繼續")) {
                return "Suivant";
            }
            if (str.equals("詳情")) {
                return "Détails";
            }
            if (str.equals("牌牆")) {
                return "Mur";
            }
            if (str.equals("圈")) {
                return "";
            }
            if (str.equals("東")) {
                return "E.";
            }
            if (str.equals("南")) {
                return "S.";
            }
            if (str.equals("西")) {
                return "O.";
            }
            if (str.equals("北")) {
                return "N.";
            }
            if (str.equals("補牌")) {
                return "Piocher";
            }
            if (str.equals("補花")) {
                return "Fleur";
            }
            if (str.equals("花糊")) {
                return "Gagner";
            }
            if (str.equals("花糊exp")) {
                return "Gagné";
            }
            if (str.equals("出沖")) {
                return "Jeté";
            }
            if (str.equals("起莊")) {
                return "Start";
            }
            if (str.equals("莊")) {
                return "D.";
            }
            if (str.equals("連")) {
                return "C.";
            }
            if (str.equals("取消")) {
                return "Annuler";
            }
            if (str.equals("請選擇組合")) {
                return "Choisir une combinaison";
            }
            if (str.equals("略過")) {
                return "Passer";
            }
            if (str.equals("是")) {
                return "Oui";
            }
            if (str.equals("否")) {
                return "Non";
            }
            if (str.equals("好")) {
                return "Ok";
            }
            if (str.equals("抓圖")) {
                return "Capture";
            }
            if (str.equals("關閉")) {
                return "Fermer";
            }
            if (str.equals("分享")) {
                return "Partager";
            }
            if (str.equals("你")) {
                return "Vous";
            }
            if (str.equals("上家")) {
                return "Gauche";
            }
            if (str.equals("對家")) {
                return "En face";
            }
            if (str.equals("下家")) {
                return "Droite";
            }
            if (str.equals("荒牌")) {
                return "Partie nulle";
            }
            if (str.equals("番")) {
                return " pt";
            }
            if (str.equals("結算")) {
                return "Score";
            }
            if (str.equals("食糊long")) {
                return "Par défausse ";
            }
            if (str.equals("自摸long")) {
                return "Tirée soi-même ";
            }
            if (str.equals("棄此牌後叫糊")) {
                return "Victoire avec";
            }
            if (str.equals("雖然糊牌成立，可惜只有")) {
                return "Mais votre main ne vaut que";
            }
            if (str.equals("不能食糊。糊牌最低要求為")) {
                return "Impossible de déclarer un main gagnante. Requis :";
            }
            if (str.equals("連線中斷，遊戲結束")) {
                return "Connexion perdue !";
            }
            if (str.equals("(不計花牌番數)")) {
                return " (Sans les pts des fleurs)";
            }
            if (str.equals("繼續遊戲？")) {
                return "Reprendre le jeu sauvegarde ?";
            }
            if (str.equals("績分榜")) {
                return "Score";
            }
            if (str.equals("沒有紀錄")) {
                return "Aucun enregistrement";
            }
            if (str.equals("盤")) {
                return "Mains";
            }
            if (str.equals("本局結束")) {
                return "Résultat final";
            }
            if (str.equals("個")) {
                return " x ";
            }
            if (str.equals("牌型紀錄")) {
                return "Combinaisons";
            }
            if (str.equals("胡牌番數統計")) {
                return "Scores";
            }
            if (str.equals("玩家名字")) {
                return "Nom du joueur";
            }
            if (str.equals("自動摸牌")) {
                return "Piocher automatiquement";
            }
            if (str.equals("動畫效果")) {
                return "Animations";
            }
            if (str.equals("音效")) {
                return "Effets sonores";
            }
            if (str.equals("麻雀牌風格")) {
                return "Style des tuiles";
            }
            if (str.equals("頭像")) {
                return "Personnage";
            }
            if (str.equals("畫面顯示方向")) {
                return "Orientation de l'écran";
            }
            if (str.equals("語言")) {
                return "Langue";
            }
            if (str.equals("雀友麻雀")) {
                return "Mahjong & Amis";
            }
            if (str.equals("繼續遊戲")) {
                return "Reprendre le jeu";
            }
            if (str.equals("開始新遊戲")) {
                return "Nouveau le jeu";
            }
            if (str.equals("離開遊戲")) {
                return "Quitter le jeu";
            }
            if (str.equals("返回主畫面")) {
                return "Retour à l'accueil";
            }
            if (str.equals("規則")) {
                return "Règles du jeu";
            }
            if (str.equals("牌型")) {
                return "Combinaisons";
            }
            if (str.equals("關於")) {
                return "A propos de ...";
            }
            if (str.equals("開始")) {
                return "Jouer";
            }
            if (str.equals("小統計")) {
                return "Statistiques";
            }
            if (str.equals("設定")) {
                return "Paramètres";
            }
            if (str.equals("遊戲規則")) {
                return "Comment jouer";
            }
            if (str.equals("版本")) {
                return "Version";
            }
            if (str.equals("網路模式")) {
                return "Mode réseau";
            }
            if (str.equals("裝置清單")) {
                return "Liste des appareils";
            }
            if (str.equals("未能啟動藍牙裝置")) {
                return "Impossible d'initialiser le périphérique Bluetooth";
            }
            if (str.equals("裝置目前已在可被偵測狀態")) {
                return "L'appareil est déjà Identifiable";
            }
            if (str.equals("等待其他裝置連線")) {
                return "Attente de connexion";
            }
            if (str.equals("停止等候")) {
                return "Arrêter l'hébergement";
            }
            if (str.equals("等候狀態中不能主動連接其他裝置")) {
                return "Connexion impossible lors de l'attente de connexion";
            }
            if (str.equals("偵測狀能中不能連線")) {
                return "Connexion impossible lors du balayage";
            }
            if (str.equals("連接到")) {
                return "Connexion avec";
            }
            if (str.equals("已配對的裝置")) {
                return "Appareils appairés";
            }
            if (str.equals("連線失敗")) {
                return "Impossible d'établir une connexion";
            }
            if (str.equals("已取消")) {
                return "Annulé";
            }
            if (str.equals("偵測中")) {
                return "Balayage";
            }
            if (str.equals("停止偵測")) {
                return "Arrêter le balayage";
            }
            if (str.equals("主持")) {
                return "Hôte";
            }
            if (str.equals("賓客")) {
                return "Invité";
            }
            if (str.equals("開始")) {
                return "Démarrer";
            }
            if (str.equals("離開")) {
                return "Quitter";
            }
            if (str.equals("取消連線")) {
                return "Annuler la connexion";
            }
            if (str.equals("使裝置可被偵測")) {
                return "Rendre l'appareil Identifiable";
            }
            if (str.equals("偵測新裝置")) {
                return "Recherche d'appareils";
            }
            if (str.equals("已配對的裝置")) {
                return "Appareils appairés";
            }
            if (str.equals("沒有已配對的裝置")) {
                return "Pas d'appareils appairés";
            }
            if (str.equals("沒有其他新裝置")) {
                return "Pas de nouveaux appareils";
            }
            if (str.equals("主持遊戲")) {
                return "Héberger un jeu";
            }
            if (str.equals("新裝置")) {
                return "Nouveaux appareils";
            }
            if (str.equals("連接伺服器")) {
                return "Connexion au serveur";
            }
            if (str.equals("連線")) {
                return "Se Connecter";
            }
            if (str.equals("連線已中斷")) {
                return "Se Déconnecter";
            }
            if (str.equals("已連線的玩家")) {
                return "Joueurs connectés";
            }
            if (str.equals("請輸入Game Key")) {
                return "Please enter the Game Key";
            }
            if (str.equals("設立遊戲密碼(可留空)")) {
                return "Mot de passe pour rejoindre le jeu (optionnel)";
            }
            if (str.equals("密碼")) {
                return "Mot de passe";
            }
            if (str.equals("密碼已傳送至剪貼簿")) {
                return "Password copied to clipboard.";
            }
            if (str.equals("Game Key已傳至剪貼簿")) {
                return "Game Key copied to clipboard.";
            }
            if (str.equals("遊戲密碼(如有)")) {
                return "Mot de passe de l'hôte (si applicable)";
            }
            if (str.equals("設定IP地址")) {
                return "Saisir l'adresse IP";
            }
            if (str.equals("錯誤的IP地址")) {
                return "Adresse IP invalide";
            }
            if (str.equals("沒有可用連線")) {
                return "Pas de connexion";
            }
            if (str.equals("IP地址")) {
                return "Adresse IP";
            }
            if (str.equals("伺服器")) {
                return "Serveur";
            }
            if (str.equals("遊戲模式")) {
                return "Mode de jeu";
            }
            if (str.equals("本機遊戲")) {
                return "Seul contre l'ordinateur";
            }
            if (str.equals("連線: 藍牙")) {
                return "Réseau Bluetooth";
            }
            if (str.equals("連線: 網路")) {
                return "Réseau TCP";
            }
            if (str.equals("連線: 伺服器")) {
                return "Réseau Serveur";
            }
            if (str.equals("分享截圖")) {
                return "Partager la capture d'écran";
            }
            if (str.equals("截圖失敗")) {
                return "Echec lors de la capture";
            }
            if (str.equals("太多檔案")) {
                return "Trop de fichiers";
            }
            if (str.equals("截圖已儲存至")) {
                return "Capturé en tant que";
            }
            if (str.equals("廣告支持版")) {
                return "Ad-supported";
            }
            if (str.equals("牌型紀錄")) {
                return "Combinaisons";
            }
            if (str.equals("番數統計")) {
                return "Stats. Pts.";
            }
            if (str.equals("幫助")) {
                return "Aide";
            }
            if (str.equals("升級了")) {
                return "Niveau supérieur";
            }
            if (str.equals("小統計只限本機遊戲")) {
                return "Statistiques du jeu en local";
            }
            if (str.equals("此功能需要Android 2.1或以上")) {
                return "Cette fonctionnalité requiert Android 2.1 ou mieux";
            }
            if (str.equals("不要顯示動畫角色")) {
                return "Cacher les personnages";
            }
            if (str.equals("購買完全版")) {
                return "Acheter la version complète";
            }
            if (str.equals("Free版只記錄至一星級")) {
                return "(Les informations sont limités au premier niveau dans la version gratuite)";
            }
            if (str.equals("輕掃出牌")) {
                return "Glisser pour défausser la tuile";
            }
            if (str.equals("每局盤數")) {
                return "Manches par partie";
            }
            if (str.equals("起胡番數")) {
                return "Score minimum pour gagner";
            }
            if (str.equals("積分模式")) {
                return "Mode infini";
            }
            if (str.equals("以全屏幕進行游戲")) {
                return "Mode plein écran";
            }
            if (str.equals("Free Version")) {
                return "Version gratuite";
            }
            if (str.equals("清除游戲記錄")) {
                return "Effacer les données de jeu";
            }
            if (str.equals("記錄清除後將不能復原，你是否要確定")) {
                return "Cette action ne peut pas être annulée, confirmer ";
            }
            if (str.equals("記錄已清除")) {
                return "Données effacées";
            }
            if (str.equals("確定刪除")) {
                return "Confirmer l'effacement ";
            }
            if (str.equals("設定連線")) {
                return "Modifier la connexion";
            }
            if (str.equals("別名")) {
                return "Alias";
            }
            if (str.equals("儲存及離開")) {
                return "Sauver et Quitter";
            }
            if (str.equals("給我們一個 Facebook\"讚\"？\n(網頁開啟後請按一下頁上的\"讚\"按鈕方為完成)")) {
                return "Donnez nous un \"J'aime\" sur Facebook ?\n(Cliquez \"Oui\" puis merci de cliquer le bouton \"J'aime\" sur notre page Facebook)";
            }
            if (str.equals("快速指南")) {
                return "Démarrage rapide";
            }
            if (str.equals("圈數")) {
                return "Rounds per Game";
            }
            if (str.equals("過水")) {
                return "Passer";
            }
            if (str.equals("圈long")) {
                return "Rounds";
            }
            if (str.equals("已打出的牌")) {
                return "Discarded Tiles";
            }
            if (str.equals("背景")) {
                return "Fond d'écran";
            }
            if (str.equals("發表評分，送你多個新背景！現在就為雀友麻雀評分嗎？")) {
                return "Submit your rating and get extra wallpapers! Proceed?";
            }
            if (str.equals("麻雀牌顏色")) {
                return "Couleur des tuiles";
            }
            if (str.equals("遊戲資訊")) {
                return "Game info";
            }
        }
        return str;
    }

    public static String ab(String str) {
        return e(str, 1);
    }

    public static void b(int[] iArr, int[] iArr2) {
        for (int i = 0; i < iArr.length - 1; i++) {
            for (int i2 = i + 1; i2 < iArr.length; i2++) {
                if (iArr[i] > iArr[i2]) {
                    int i3 = iArr[i];
                    iArr[i] = iArr[i2];
                    iArr[i2] = i3;
                    int i4 = iArr2[i];
                    iArr2[i] = iArr2[i2];
                    iArr2[i2] = i4;
                }
            }
        }
    }

    public static String ch() {
        switch (ai.nk) {
            case 2:
                return "_big5";
            case 3:
                return "_gb";
            case 4:
                return "_jp";
            case 5:
                return "_fr";
            default:
                return "";
        }
    }

    public static String d(String str, int i) {
        if (str.startsWith("自摸") || str.startsWith("不求人") || str.startsWith("門清自摸")) {
            return "chimo";
        }
        if (str.startsWith("妙手回春")) {
            return "chimo3";
        }
        if (str.startsWith("槓上槓自摸")) {
            return "chimo2";
        }
        if (str.startsWith("二十二羅漢") || str.startsWith("四槓") || str.startsWith("十八羅漢") || str.startsWith("一色四同順") || str.startsWith("四連太寶")) {
            return "sp1";
        }
        if (str.equals("搶槓")) {
            return "sp2";
        }
        if (str.startsWith("海底撈月")) {
            return "sp3";
        }
        if (str.startsWith("嶺上開花") || str.startsWith("槓上開花")) {
            return "sp4";
        }
        if (str.startsWith("小花糊") || str.startsWith("七隻花糊")) {
            return "sp5";
        }
        if (str.startsWith("八仙過海") || str.startsWith("八隻花糊")) {
            return "sp6";
        }
        if (str.startsWith("雞糊") || str.startsWith("無番和")) {
            return "sp7";
        }
        if (str.startsWith("天糊") || str.startsWith("地糊")) {
            return "sp8";
        }
        if (i > 0) {
            return "level" + i;
        }
        return null;
    }

    private static String e(String str, int i) {
        String str2;
        if (ai.nk == 1) {
            str2 = String.valueOf(str.equals("四風會") ? "Big Four Winds" : str.equals("大三元") ? "Big Three Dragons" : str.equals("綠一色") ? "All Greens" : str.equals("九蓮寶燈") ? "Nine Gates" : str.equals("四槓") ? "Four Kongs" : str.equals("連七對") ? "Seven Shifted Pairs" : str.equals("十三么") ? "Thirteen Orphans" : str.equals("清么九") ? "All Terminals" : str.equals("小四風會") ? "Little Four Winds" : str.equals("小三元") ? "Little Three Dragons" : str.equals("字一色") ? "All Honor" : str.equals("四暗刻") ? "Four Concealed Pungs" : str.equals("一色雙龍會") ? "Pure Terminal Chows" : str.equals("一色四同順") ? "Quadruple Chow" : str.equals("一色四節高") ? "Four Pure Shifted Pungs" : str.equals("一色四步高") ? "Four Pure Shifted Chows" : str.equals("三槓") ? "Three Kongs" : str.equals("混么九") ? "All Terminals and Honors" : str.equals("七對") ? "Seven Pairs" : str.equals("七星不靠") ? "Greater Honors and Knitted Tiles" : str.equals("全雙刻") ? "All Even Pungs" : str.equals("清一色") ? "Full Flush" : str.equals("一色三同順") ? "Pure Triple Chow" : str.equals("一色三節高") ? "Pure Shifted Pungs" : str.equals("全大") ? "Upper Tiles" : str.equals("全中") ? "Middle Tiles" : str.equals("全小") ? "Lower Tiles" : str.equals("清龍") ? "Pure Straight" : str.equals("三色雙龍會") ? "Three-Suited Terminal Chows" : str.equals("一色三步高") ? "Pure Shifted Chows" : str.equals("全帶五") ? "All Fives" : str.equals("三同刻") ? "Triple Pung" : str.equals("三暗刻") ? "Three Concealed Pungs" : str.equals("全不靠") ? "Lesser Honors and Knitted Tiles" : str.equals("組合龍") ? "Knitted Straight" : str.equals("大於五") ? "Upper Five" : str.equals("小於五") ? "Lower Five" : str.equals("三風刻") ? "Big Three Winds" : str.equals("花龍") ? "Mixed Straight" : str.equals("推不倒") ? "Reversible Tiles" : str.equals("三色三同順") ? "Mixed Triple Chow" : str.equals("三色三節高") ? "Mixed Shifted Pungs" : str.equals("無番和") ? "Chicken Hand" : str.equals("妙手回春") ? "Last Tile Draw" : str.equals("海底撈月") ? "Last Tile Claim" : str.equals("槓上開花") ? "Out with Replacement Tile" : str.equals("搶槓") ? "Robbing The Kong" : str.equals("雙暗槓") ? "Two Concealed Kongs" : str.equals("碰碰和") ? "All Pungs" : str.equals("混一色") ? "Half Flush" : str.equals("三色三步高") ? "Mixed Shifted Chows" : str.equals("五門齊") ? "All Types" : str.equals("全求人") ? "Melded Hand" : str.equals("雙箭刻") ? "Two Dragons Pungs" : str.equals("全帶么") ? "Outside Hand" : str.equals("不求人") ? "Fully Concealed Hand" : str.equals("雙槓") ? "Two Melded Kongs" : str.equals("和絕張") ? "Last Tile" : str.equals("箭刻") ? "Dragon Pung" : str.equals("圈風刻") ? "Prevalent Wind" : str.equals("門風刻") ? "Seat Wind" : str.equals("門前清") ? "Concealed Hand" : str.equals("平和") ? "All Chows" : str.equals("四歸一") ? "Tile Hog" : str.equals("雙同刻") ? "Double Pung" : str.equals("雙暗刻") ? "Two Concealed Pungs" : str.equals("暗槓") ? "Concealed Kong" : str.equals("斷么") ? "All Simples" : str.equals("一般高") ? "Pure Double Chow" : str.equals("喜雙逢") ? "Mixed Double Chow" : str.equals("連六") ? "Short Straight" : str.equals("老少副") ? "Two Terminal Chows" : str.equals("么九刻") ? "Pung of Terminals or Honors" : str.equals("明槓") ? "Melded Kong" : str.equals("缺一門") ? "One Voided Suit" : str.equals("無字") ? "No Honors" : str.equals("邊張") ? "Edge Wait" : str.equals("坎張") ? "Closed Wait" : str.equals("單調將") ? "Single Waiting" : str.equals("自摸") ? "Self-Drawn" : str.equals("花牌") ? "Flower Tile" : str.equals("一炮雙響") ? "Double-winner" : str.equals("一炮三響") ? "Triple-winner" : str) + " ";
        } else if (ai.nk == 3) {
            str2 = String.valueOf(str.equals("四風會") ? "四风会" : str.equals("大三元") ? "大三元" : str.equals("綠一色") ? "绿一色" : str.equals("九蓮寶燈") ? "九莲宝灯" : str.equals("四槓") ? "四杠" : str.equals("連七對") ? "连七对" : str.equals("十三么") ? "十三么" : str.equals("清么九") ? "清么九" : str.equals("小四風會") ? "小四风会" : str.equals("小三元") ? "小三元" : str.equals("字一色") ? "字一色" : str.equals("四暗刻") ? "四暗刻" : str.equals("一色雙龍會") ? "一色双龙会" : str.equals("一色四同順") ? "一色四同顺" : str.equals("一色四節高") ? "一色四节高" : str.equals("一色四步高") ? "一色四步高" : str.equals("三槓") ? "三杠" : str.equals("混么九") ? "混么九" : str.equals("七對") ? "七对" : str.equals("七星不靠") ? "七星不靠" : str.equals("全雙刻") ? "全双刻" : str.equals("清一色") ? "清一色" : str.equals("一色三同順") ? "一色三同顺" : str.equals("一色三節高") ? "一色三节高" : str.equals("全大") ? "全大" : str.equals("全中") ? "全中" : str.equals("全小") ? "全小" : str.equals("清龍") ? "清龙" : str.equals("三色雙龍會") ? "三色双龙会" : str.equals("一色三步高") ? "一色三步高" : str.equals("全帶五") ? "全带五" : str.equals("三同刻") ? "三同刻" : str.equals("三暗刻") ? "三暗刻" : str.equals("全不靠") ? "全不靠" : str.equals("組合龍") ? "组合龙" : str.equals("大於五") ? "大於五" : str.equals("小於五") ? "小於五" : str.equals("三風刻") ? "三风刻" : str.equals("花龍") ? "花龙" : str.equals("推不倒") ? "推不倒" : str.equals("三色三同順") ? "三色三同顺" : str.equals("三色三節高") ? "三色三节高" : str.equals("無番和") ? "无番和" : str.equals("妙手回春") ? "妙手回春" : str.equals("海底撈月") ? "海底捞月" : str.equals("槓上開花") ? "杠上开花" : str.equals("搶槓") ? "抢杠" : str.equals("雙暗槓") ? "双暗杠" : str.equals("碰碰和") ? "碰碰和" : str.equals("混一色") ? "混一色" : str.equals("三色三步高") ? "三色三步高" : str.equals("五門齊") ? "五门齐" : str.equals("全求人") ? "全求人" : str.equals("雙箭刻") ? "双箭刻" : str.equals("全帶么") ? "全带么" : str.equals("不求人") ? "不求人" : str.equals("雙槓") ? "双杠" : str.equals("和絕張") ? "和绝张" : str.equals("箭刻") ? "箭刻" : str.equals("圈風刻") ? "圈风刻" : str.equals("門風刻") ? "门风刻" : str.equals("門前清") ? "门前清" : str.equals("平和") ? "平和" : str.equals("四歸一") ? "四归一" : str.equals("雙同刻") ? "双同刻" : str.equals("雙暗刻") ? "双暗刻" : str.equals("暗槓") ? "暗杠" : str.equals("斷么") ? "断么" : str.equals("一般高") ? "一般高" : str.equals("喜雙逢") ? "喜双逢" : str.equals("連六") ? "连六" : str.equals("老少副") ? "老少副" : str.equals("么九刻") ? "么九刻" : str.equals("明槓") ? "明杠" : str.equals("缺一門") ? "缺一门" : str.equals("無字") ? "无字" : str.equals("邊張") ? "边张" : str.equals("坎張") ? "坎张" : str.equals("單調將") ? "单调将" : str.equals("自摸") ? "自摸" : str.equals("花牌") ? "花牌" : str.equals("一炮雙響") ? "一炮双响" : str.equals("一炮三響") ? "一炮三响" : str) + " ";
        } else {
            if (ai.nk == 4) {
                if (str.equals("四風會")) {
                    str2 = "大四喜";
                } else if (str.equals("大三元")) {
                    str2 = "大三元";
                } else if (str.equals("綠一色")) {
                    str2 = "緑一色";
                } else if (str.equals("九蓮寶燈")) {
                    str2 = "九蓮宝燈";
                } else if (str.equals("四槓")) {
                    str2 = "四槓子";
                } else if (str.equals("連七對")) {
                    str2 = "連七対子";
                } else if (str.equals("十三么")) {
                    str2 = "国士無双";
                } else if (str.equals("清么九")) {
                    str2 = "清老頭";
                } else if (str.equals("小四風會")) {
                    str2 = "小四喜";
                } else if (str.equals("小三元")) {
                    str2 = "小三元";
                } else if (str.equals("字一色")) {
                    str2 = "字一色";
                } else if (str.equals("四暗刻")) {
                    str2 = "四暗刻";
                } else if (str.equals("一色雙龍會")) {
                    str2 = "一色双龍會";
                } else if (str.equals("一色四同順")) {
                    str2 = "一色四同順";
                } else if (str.equals("一色四節高")) {
                    str2 = "四連刻";
                } else if (str.equals("一色四步高")) {
                    str2 = "一色四步高";
                } else if (str.equals("三槓")) {
                    str2 = "三槓子";
                } else if (str.equals("混么九")) {
                    str2 = "混老頭";
                } else if (str.equals("七對")) {
                    str2 = "七対子";
                } else if (str.equals("七星不靠")) {
                    str2 = "七星不靠";
                } else if (str.equals("全雙刻")) {
                    str2 = "全双刻";
                } else if (str.equals("清一色")) {
                    str2 = "清一色";
                } else if (str.equals("一色三同順")) {
                    str2 = "一色三同順";
                } else if (str.equals("一色三節高")) {
                    str2 = "三連刻";
                } else if (str.equals("全大")) {
                    str2 = "全大";
                } else if (str.equals("全中")) {
                    str2 = "全中";
                } else if (str.equals("全小")) {
                    str2 = "全小";
                } else if (str.equals("清龍")) {
                    str2 = "一気通貫";
                } else if (str.equals("三色雙龍會")) {
                    str2 = "三色双龍會";
                } else if (str.equals("一色三步高")) {
                    str2 = "一色三步高";
                } else if (str.equals("全帶五")) {
                    str2 = "全帯五";
                } else if (str.equals("三同刻")) {
                    str2 = "三色同刻";
                } else if (str.equals("三暗刻")) {
                    str2 = "三暗刻";
                } else if (str.equals("全不靠")) {
                    str2 = "全不靠";
                } else if (str.equals("組合龍")) {
                    str2 = "組合龍";
                } else if (str.equals("大於五")) {
                    str2 = "大於五";
                } else if (str.equals("小於五")) {
                    str2 = "小於五";
                } else if (str.equals("三風刻")) {
                    str2 = "三風刻";
                } else if (str.equals("花龍")) {
                    str2 = "花龍";
                } else if (str.equals("推不倒")) {
                    str2 = "推不倒";
                } else if (str.equals("三色三同順")) {
                    str2 = "三色同順";
                } else if (str.equals("三色三節高")) {
                    str2 = "三色三節高";
                } else if (str.equals("無番和")) {
                    str2 = "無飜和";
                } else if (str.equals("妙手回春")) {
                    str2 = "海底摸月";
                } else if (str.equals("海底撈月")) {
                    str2 = "河底撈魚";
                } else if (str.equals("槓上開花")) {
                    str2 = "嶺上開花";
                } else if (str.equals("搶槓")) {
                    str2 = "槍槓";
                } else if (str.equals("雙暗槓")) {
                    str2 = "双暗槓";
                } else if (str.equals("碰碰和")) {
                    str2 = "対々和";
                } else if (str.equals("混一色")) {
                    str2 = "混一色";
                } else if (str.equals("三色三步高")) {
                    str2 = "三色三步高";
                } else if (str.equals("五門齊")) {
                    str2 = "五門齊";
                } else if (str.equals("全求人")) {
                    str2 = "全求人";
                } else if (str.equals("雙箭刻")) {
                    str2 = "双箭刻";
                } else if (str.equals("全帶么")) {
                    str2 = "全帯么九";
                } else if (str.equals("不求人")) {
                    str2 = "門前清自摸和";
                } else if (str.equals("雙槓")) {
                    str2 = "双槓子";
                } else if (str.equals("和絕張")) {
                    str2 = "和絕張";
                } else if (str.equals("箭刻")) {
                    str2 = "箭刻";
                } else if (str.equals("圈風刻")) {
                    str2 = "圈風刻";
                } else if (str.equals("門風刻")) {
                    str2 = "門風刻";
                } else if (str.equals("門前清")) {
                    str2 = "門前清";
                } else if (str.equals("平和")) {
                    str2 = "平和";
                } else if (str.equals("四歸一")) {
                    str2 = "四歸一";
                } else if (str.equals("雙同刻")) {
                    str2 = "双同刻";
                } else if (str.equals("雙暗刻")) {
                    str2 = "双暗刻";
                } else if (str.equals("暗槓")) {
                    str2 = "暗槓";
                } else if (str.equals("斷么")) {
                    str2 = "断么九";
                } else if (str.equals("一般高")) {
                    str2 = "一盃口";
                } else if (str.equals("喜雙逢")) {
                    str2 = "喜双逢";
                } else if (str.equals("連六")) {
                    str2 = "連六";
                } else if (str.equals("老少副")) {
                    str2 = "老少副";
                } else if (str.equals("么九刻")) {
                    str2 = "么九刻";
                } else if (str.equals("明槓")) {
                    str2 = "明槓";
                } else if (str.equals("缺一門")) {
                    str2 = "缺一門";
                } else if (str.equals("無字")) {
                    str2 = "無字";
                } else if (str.equals("邊張")) {
                    str2 = "辺張";
                } else if (str.equals("坎張")) {
                    str2 = "嵌張";
                } else if (str.equals("單調將")) {
                    str2 = "単騎將";
                } else if (str.equals("自摸")) {
                    str2 = "自摸";
                } else if (str.equals("花牌")) {
                    str2 = "花牌";
                } else if (str.equals("一炮雙響")) {
                    str2 = "二家和";
                } else if (str.equals("一炮三響")) {
                    str2 = "三家和";
                }
            } else if (ai.nk == 5) {
                str2 = String.valueOf(str.equals("四風會") ? "Quatre grands vents" : str.equals("大三元") ? "Trois grands dragons" : str.equals("綠一色") ? "Main verte" : str.equals("九蓮寶燈") ? "Neuf portes" : str.equals("四槓") ? "Quatre kongs" : str.equals("連七對") ? "Sept paires pures consécutives" : str.equals("十三么") ? "Les treize lanternes merveilleuses" : str.equals("清么九") ? "Tout extrémité" : str.equals("小四風會") ? "Quatre petits vents" : str.equals("小三元") ? "Trois petits dragons" : str.equals("字一色") ? "Tout Honneur" : str.equals("四暗刻") ? "Quatre pungs cachés" : str.equals("一色雙龍會") ? "Deux dragons dans une famille" : str.equals("一色四同順") ? "Quadruple chows purs" : str.equals("一色四節高") ? "Quatre pungs purs consécutifs" : str.equals("一色四步高") ? "Quatre chows purs superposés" : str.equals("三槓") ? "Trois kongs" : str.equals("混么九") ? "Tout honneur et extrémité" : str.equals("七對") ? "Sept paires" : str.equals("七星不靠") ? "Grand serpentin" : str.equals("全雙刻") ? "Tout paire" : str.equals("清一色") ? "Main pure" : str.equals("一色三同順") ? "Triple chows purs" : str.equals("一色三節高") ? "Trois pungs purs consécutifs" : str.equals("全大") ? "Les trois derniers" : str.equals("全中") ? "Les trois milieux" : str.equals("全小") ? "Les trois premiers" : str.equals("清龍") ? "Grande suite pure" : str.equals("三色雙龍會") ? "Deux dragons dans trois familles" : str.equals("一色三步高") ? "Trois chows purs superposés" : str.equals("全帶五") ? "Cinq partout" : str.equals("三同刻") ? "Triple pungs" : str.equals("三暗刻") ? "Trois pungs cachés" : str.equals("全不靠") ? "Petit serpentin" : str.equals("組合龍") ? "Suite serpentin" : str.equals("大於五") ? "Les quatre derniers" : str.equals("小於五") ? "Les quatre premiers" : str.equals("三風刻") ? "Trois petits vents" : str.equals("花龍") ? "Grande suite" : str.equals("推不倒") ? "Symétrie" : str.equals("三色三同順") ? "Triple chows" : str.equals("三色三節高") ? "Trois pungs consécutifs" : str.equals("無番和") ? "Main sans valeur" : str.equals("妙手回春") ? "Dernière tuile tirée" : str.equals("海底撈月") ? "Dernière tuile jetée" : str.equals("槓上開花") ? "Finir sur kong" : str.equals("搶槓") ? "Kong volé" : str.equals("雙暗槓") ? "Deux kongs cachés" : str.equals("碰碰和") ? "Tout pung" : str.equals("混一色") ? "Semi pure" : str.equals("三色三步高") ? "Trois chows superposés" : str.equals("五門齊") ? "Tout type" : str.equals("全求人") ? "Tout exposé" : str.equals("雙箭刻") ? "Deux dragons" : str.equals("全帶么") ? "Extrémité ou honneur partout" : str.equals("不求人") ? "Tout caché tiré" : str.equals("雙槓") ? "Deux kongs exposés" : str.equals("和絕張") ? "Derniere tuile existante" : str.equals("箭刻") ? "Pung de dragons" : str.equals("圈風刻") ? "Vent dominant" : str.equals("門風刻") ? "Vent du joueur" : str.equals("門前清") ? "Tout cache donné" : str.equals("平和") ? "Tout chow" : str.equals("四歸一") ? "Quatre identiques" : str.equals("雙同刻") ? "Double pungs" : str.equals("雙暗刻") ? "Deux pungs cachés" : str.equals("暗槓") ? "Kong caché" : str.equals("斷么") ? "Tout ordinaire" : str.equals("一般高") ? "Double chows purs" : str.equals("喜雙逢") ? "Double chows" : str.equals("連六") ? "Petite suite pure" : str.equals("老少副") ? "Deux chows purs d'extrémités" : str.equals("么九刻") ? "Pung de vents ou d'extrémités" : str.equals("明槓") ? "Kong exposé" : str.equals("缺一門") ? "Une famille absente" : str.equals("無字") ? "Pas d'honneur" : str.equals("邊張") ? "Finir d’un côté" : str.equals("坎張") ? "Finir au milieu" : str.equals("單調將") ? "Finir sur la paire" : str.equals("自摸") ? "Tirer soi-même" : str.equals("花牌") ? "Fleur ou saison" : str.equals("一炮雙響") ? "2 Gagnants" : str.equals("一炮三響") ? "3 Gagnants" : str) + " ";
            }
            str2 = str;
        }
        return i > 1 ? String.valueOf(i) + aa("個") + str2 : str2;
    }

    private static void g(Vector vector) {
        for (int i = 0; i < vector.size(); i++) {
            String str = (String) vector.elementAt(i);
            if (str.length() == 3) {
                str = str.substring(0, 2);
            }
            a aVar = (a) c.mu.get(str);
            if (aVar != null && aVar.aM() <= 34) {
                int[] iArr = gO.wd;
                int aM = aVar.aM();
                iArr[aM] = iArr[aM] + 1;
            }
        }
    }

    public static String z(int i) {
        switch (i) {
            case 0:
                return aa("東");
            case 1:
                return aa("南");
            case 2:
                return aa("西");
            case 3:
                return aa("北");
            default:
                return "";
        }
    }
}
