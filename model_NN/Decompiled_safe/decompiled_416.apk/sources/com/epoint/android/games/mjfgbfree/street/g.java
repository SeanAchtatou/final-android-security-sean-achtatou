package com.epoint.android.games.mjfgbfree.street;

import android.location.Location;

final class g extends Thread {
    private /* synthetic */ PlacesMapActivity aP;

    g(PlacesMapActivity placesMapActivity) {
        this.aP = placesMapActivity;
    }

    public final void run() {
        try {
            Thread.sleep(8000);
            if (this.aP.fH == 1001.0d) {
                Location lastKnownLocation = this.aP.bw.getLastKnownLocation("network");
                if (lastKnownLocation == null) {
                    lastKnownLocation = this.aP.bw.getLastKnownLocation("gps");
                }
                if (lastKnownLocation != null) {
                    this.aP.onLocationChanged(lastKnownLocation);
                }
                this.aP.bw.removeUpdates(this.aP);
            }
        } catch (InterruptedException e) {
        }
    }
}
