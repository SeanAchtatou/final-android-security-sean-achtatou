package com.epoint.android.games.mjfgbfree.street;

import android.content.DialogInterface;
import android.widget.TextView;
import com.google.android.maps.GeoPoint;

final class i implements DialogInterface.OnClickListener {
    private /* synthetic */ PlacesMapActivity aP;
    private final /* synthetic */ TextView uW;

    i(PlacesMapActivity placesMapActivity, TextView textView) {
        this.aP = placesMapActivity;
        this.uW = textView;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        GeoPoint point = this.aP.fF.getItem(0).getPoint();
        this.aP.a(this.uW.getText().toString(), ((double) ((long) point.getLatitudeE6())) / 1000000.0d, ((double) ((long) point.getLongitudeE6())) / 1000000.0d);
    }
}
