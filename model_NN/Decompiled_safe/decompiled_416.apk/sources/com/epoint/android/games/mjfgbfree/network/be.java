package com.epoint.android.games.mjfgbfree.network;

import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.epoint.android.games.mjfgbfree.af;
import java.util.Vector;

final class be extends Handler {
    private /* synthetic */ BTConnectionActivity pk;

    be(BTConnectionActivity bTConnectionActivity) {
        this.pk = bTConnectionActivity;
    }

    public final synchronized void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                if (this.pk.ad.size() < this.pk.ac) {
                    bt btVar = new bt((BluetoothSocket) message.obj);
                    this.pk.ad.add(btVar);
                    btVar.a(this.pk);
                    break;
                }
                break;
            case 2:
                Toast.makeText(this.pk, (String) message.obj, 0).show();
                break;
            case 3:
                if (this.pk.ag.isDiscovering()) {
                    this.pk.ag.cancelDiscovery();
                }
                this.pk.setProgressBarIndeterminateVisibility(false);
                this.pk.setTitle(af.aa("裝置清單"));
                this.pk.a((Vector) message.obj);
                this.pk.ai.setText(af.aa("偵測新裝置"));
                this.pk.ai.setEnabled(true);
                this.pk.aj.setText(af.aa("主持遊戲"));
                this.pk.aj.setEnabled(true);
                this.pk.al.setEnabled(true);
                break;
        }
    }
}
