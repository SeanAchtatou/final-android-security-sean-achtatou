package com.epoint.android.games.mjfgbfree.e;

import android.app.Activity;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import com.epoint.android.games.mjfgbfree.bb;
import com.epoint.android.games.mjfgbfree.d.b;
import com.epoint.android.games.mjfgbfree.f.a;
import com.epoint.android.games.mjfgbfree.tournament.TournamentActivity;
import com.epoint.android.games.mjfgbfree.tournament.c;
import com.epoint.android.games.mjfgbfree.ui.a.i;

public final class l extends a {
    /* access modifiers changed from: private */
    public b pi;
    private int pj = -1;
    /* access modifiers changed from: private */
    public c qj;

    public l(Activity activity, bb bbVar, int i) {
        super(activity, bbVar);
        if (this.qj == null) {
            this.qj = new c();
        }
        this.qj.oo = i;
        MJ16Activity.rp -= 2;
        a.a(this.fu, 0, -2);
        switch (i) {
            case 0:
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(8, 4, 3));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(8, 4, 3));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(16, 4, -1));
                return;
            case 1:
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(8, 4, 3));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(8, 4, 3));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(8, 4, 3));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(16, 4, -1));
                return;
            case 2:
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(8, 6, 3));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(8, 6, 3));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(16, 6, 2));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(16, 6, -1));
                return;
            case 3:
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(8, 6, 3));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(8, 6, 3));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(8, 6, 2));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(16, 6, 2));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(16, 6, -1));
                return;
            case 4:
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(8, 8, 2));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(8, 8, 2));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(8, 8, 2));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(16, 8, 1));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(16, 8, -1));
                return;
            case 5:
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(8, 4, 2));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(8, 6, 2));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(16, 6, 1));
                this.qj.oq.add(new com.epoint.android.games.mjfgbfree.tournament.a(16, 8, -1));
                return;
            default:
                return;
        }
    }

    public l(Activity activity, bb bbVar, c cVar) {
        super(activity, bbVar);
        this.qj = cVar;
    }

    static /* synthetic */ int a(l lVar, int i) {
        switch (lVar.qj.oo) {
            case 0:
                return ((int) (Math.random() * ((double) (i == 1 ? 9 : i == 2 ? 8 : 7)))) + 1;
            case 1:
                return ((int) (Math.random() * ((double) (i == 1 ? 9 : i == 2 ? 8 : 7)))) + 10;
            case 2:
                return ((int) (Math.random() * ((double) (i == 1 ? 9 : i == 2 ? 8 : 7)))) + 19;
            case 3:
                return ((int) (Math.random() * ((double) (i == 1 ? 7 : i == 2 ? 6 : 5)))) + 28;
            case 4:
                return ((int) (Math.random() * ((double) (i == 1 ? 8 : i == 2 ? 7 : 6)))) + 35;
            case 5:
                return ((int) (Math.random() * ((double) (i == 1 ? 17 : i == 2 ? 12 : 7)))) + 43;
            default:
                return 0;
        }
    }

    public final void a(int i, i iVar) {
        switch (i) {
            case 0:
                if (this.cu.eb()) {
                    if (this.pj == -1) {
                        this.pj = 0;
                        new g(this).start();
                        break;
                    }
                } else {
                    this.cu.n(false);
                    break;
                }
                break;
            case 3:
                if (this.pj == 2) {
                    this.pj = -1;
                    this.cu.c(new d(this));
                    break;
                }
                break;
            case 4:
                if (this.pj == 2) {
                    this.pj = -1;
                    a.a(this.fu, this.pi);
                    a.a(this.fu, 4, (Object) null);
                    this.cu.c(new e(this));
                    break;
                }
                break;
            case 5:
                if (!this.cu.eb()) {
                    if (this.pj == -1) {
                        this.cu.d(new f(this));
                        break;
                    }
                } else if (this.pj != 0) {
                    if (this.pj == 2) {
                        a.a(this.fu, this.pi);
                        this.pj = -1;
                        this.cu.c(new i(this, this.qj.cz()));
                        break;
                    }
                } else {
                    this.pj = 2;
                    this.cu.c(new h(this));
                    break;
                }
                break;
            case 8:
                if (this.ft) {
                    super.aR();
                    this.ft = false;
                    if (!bb.tE.contains("bkgnd")) {
                        bb.tE.put("bkgnd", TournamentActivity.t(this.fu));
                    }
                    this.cu.er();
                    new j(this).start();
                    break;
                }
                break;
        }
        super.a(i, iVar);
    }

    public final void aR() {
        super.aR();
    }

    public final void aS() {
        super.aS();
    }

    public final int cI() {
        return this.qj.cz().gD;
    }

    public final c cM() {
        return this.qj;
    }
}
