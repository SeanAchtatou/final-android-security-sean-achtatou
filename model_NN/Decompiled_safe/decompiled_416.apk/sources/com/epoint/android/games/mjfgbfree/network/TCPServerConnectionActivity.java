package com.epoint.android.games.mjfgbfree.network;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import b.a.b;
import b.a.f;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import com.epoint.android.games.mjfgbfree.MJ16ViewActivity;
import com.epoint.android.games.mjfgbfree.af;
import com.epoint.android.games.mjfgbfree.ai;
import com.epoint.android.games.mjfgbfree.c.d;
import com.epoint.android.games.mjfgbfree.f.a;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.Vector;

public class TCPServerConnectionActivity extends Activity implements t {
    /* access modifiers changed from: private */
    public int ac;
    /* access modifiers changed from: private */
    public Vector ad;
    private Vector af;
    /* access modifiers changed from: private */
    public ScrollView an;
    /* access modifiers changed from: private */
    public ImageView ap;
    private Handler handler;
    /* access modifiers changed from: private */
    public String jY = "";
    /* access modifiers changed from: private */
    public String jZ;
    /* access modifiers changed from: private */
    public String ka;
    private Vector kb;
    private ByteArrayOutputStream kc;
    /* access modifiers changed from: private */
    public String kd;
    /* access modifiers changed from: private */
    public Button ke;
    /* access modifiers changed from: private */
    public Button kf;
    /* access modifiers changed from: private */
    public Button kg;
    /* access modifiers changed from: private */
    public ImageButton kh;
    /* access modifiers changed from: private */
    public LinearLayout ki;
    /* access modifiers changed from: private */
    public TextView kj;
    /* access modifiers changed from: private */
    public AlertDialog kk;
    /* access modifiers changed from: private */
    public Vector kl;
    /* access modifiers changed from: private */
    public Vector km;
    private boolean kn;

    public TCPServerConnectionActivity() {
        this.ac = MJ16Activity.qL ? 1 : 3;
        this.ad = new Vector();
        this.kb = new Vector();
        this.kc = new ByteArrayOutputStream(4096);
        this.kl = new Vector();
        this.km = new Vector();
        this.kn = false;
        this.handler = new ce(this);
    }

    /* access modifiers changed from: private */
    public boolean C(String str) {
        String str2;
        String str3;
        try {
            Socket socket = new Socket();
            String charSequence = this.kg.getText().toString();
            socket.connect(new InetSocketAddress(charSequence.equals("<default>") ? "s1.mjguy.com" : charSequence, 7890), 5000);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));
            b bVar = new b();
            bVar.a("identity", "guest");
            bVar.a("name", ai.nm);
            bVar.a("uuid", MJ16Activity.qK);
            bufferedWriter.write(String.valueOf(bVar.toString()) + "\r\n");
            bufferedWriter.flush();
            b bVar2 = new b(bufferedReader.readLine());
            this.jZ = bVar2.getString("identity");
            this.ka = bVar2.getString("uuid");
            b bVar3 = new b();
            bVar3.a("join", "MJ-GB1.3A");
            int lastIndexOf = str.lastIndexOf("@");
            if (lastIndexOf != -1) {
                String substring = str.substring(0, lastIndexOf);
                str3 = str.substring(lastIndexOf + 1);
                str2 = substring;
            } else {
                str2 = "";
                str3 = str;
            }
            bVar3.a("identity", str3);
            bVar3.a("secret", str2);
            bufferedWriter.write(String.valueOf(bVar3.toString()) + "\r\n");
            bufferedWriter.flush();
            setTitle(af.aa("已連線的玩家"));
            this.kg.setEnabled(false);
            this.kf.setEnabled(false);
            this.ke.setEnabled(false);
            if (!b(this.km, charSequence) && !b(this.kl, charSequence)) {
                this.km.add(charSequence);
                this.kl.add(charSequence);
                f(this.km);
            }
            a(1, socket);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            a(5, af.aa("連線失敗"));
            a(4, (Object) null);
            return false;
        }
    }

    /* access modifiers changed from: private */
    public String a(InputStream inputStream) {
        this.kc.reset();
        while (true) {
            int read = inputStream.read();
            if (read == -1) {
                throw new IOException("Socket read error.");
            } else if (read != 13) {
                if (read == 10) {
                    try {
                        return this.kc.toString("UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        throw new IOException("Unsupported Encoding");
                    }
                } else if (this.kc.size() == 32768) {
                    throw new IOException("Buffer overflow");
                } else {
                    this.kc.write(read);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, Object obj) {
        Message message = new Message();
        message.what = i;
        message.obj = obj;
        this.handler.sendMessage(message);
    }

    static /* synthetic */ void a(TCPServerConnectionActivity tCPServerConnectionActivity, LinearLayout linearLayout) {
        linearLayout.removeAllViews();
        for (int i = 0; i < tCPServerConnectionActivity.kl.size(); i++) {
            bv bvVar = new bv(tCPServerConnectionActivity);
            LinearLayout linearLayout2 = (LinearLayout) LayoutInflater.from(tCPServerConnectionActivity).inflate((int) C0000R.layout.ip_list_element, (ViewGroup) null);
            LinearLayout linearLayout3 = (LinearLayout) linearLayout2.findViewById(C0000R.id.address_item);
            TextView textView = (TextView) linearLayout2.findViewById(C0000R.id.text);
            TextView textView2 = (TextView) linearLayout2.findViewById(C0000R.id.alias);
            String[] split = ((String) tCPServerConnectionActivity.kl.elementAt(i)).split(";");
            textView.setText(split[0]);
            linearLayout3.setBackgroundResource(17301602);
            linearLayout3.setClickable(true);
            linearLayout3.setFocusable(true);
            linearLayout3.setOnClickListener(bvVar);
            Button button = (Button) linearLayout2.findViewById(C0000R.id.remove);
            if (!b(tCPServerConnectionActivity.km, textView.getText().toString())) {
                button.setVisibility(8);
                textView2.setVisibility(8);
            } else {
                if (split.length > 1) {
                    textView2.setText(split[1]);
                    float textSize = textView.getTextSize();
                    textView.setTextSize(0, textView2.getTextSize());
                    textView2.setTextSize(0, textSize);
                } else {
                    textView2.setVisibility(8);
                }
                linearLayout3.setTag(Integer.valueOf(i));
                linearLayout3.setLongClickable(true);
                linearLayout3.setOnLongClickListener(new bu(tCPServerConnectionActivity, linearLayout));
                button.setTag(tCPServerConnectionActivity.kl.elementAt(i));
                button.setOnClickListener(new bw(tCPServerConnectionActivity, linearLayout));
            }
            linearLayout.addView(linearLayout2);
            if (i < tCPServerConnectionActivity.kl.size() - 1) {
                LinearLayout linearLayout4 = new LinearLayout(tCPServerConnectionActivity);
                linearLayout4.setMinimumHeight(1);
                linearLayout4.setBackgroundColor(-3355444);
                linearLayout.addView(linearLayout4);
            }
        }
    }

    static /* synthetic */ void a(TCPServerConnectionActivity tCPServerConnectionActivity, boolean z) {
        if (tCPServerConnectionActivity.kj.getVisibility() == 0) {
            String bVar = f("command", "start_game").toString();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= tCPServerConnectionActivity.ad.size()) {
                    break;
                }
                ((aa) tCPServerConnectionActivity.ad.elementAt(i2)).d(bVar);
                ((aa) tCPServerConnectionActivity.ad.elementAt(i2)).a(null);
                i = i2 + 1;
            }
            if (tCPServerConnectionActivity.kb.size() > 0) {
                ((bf) tCPServerConnectionActivity.ad.firstElement()).f((String) tCPServerConnectionActivity.kb.firstElement());
                int i3 = 1;
                while (true) {
                    int i4 = i3;
                    if (i4 >= tCPServerConnectionActivity.kb.size()) {
                        break;
                    }
                    tCPServerConnectionActivity.ad.add(((bf) tCPServerConnectionActivity.ad.firstElement()).at((String) tCPServerConnectionActivity.kb.elementAt(i4)));
                    i3 = i4 + 1;
                }
            }
            MJ16ViewActivity.ad = tCPServerConnectionActivity.ad;
            tCPServerConnectionActivity.ad = null;
            Intent intent = new Intent();
            intent.putExtra("is_restore_game", z);
            tCPServerConnectionActivity.setResult(-1, intent);
            tCPServerConnectionActivity.finish();
            return;
        }
        ((aa) tCPServerConnectionActivity.ad.firstElement()).d(f("command", "quit").toString());
        tCPServerConnectionActivity.a((aa) tCPServerConnectionActivity.ad.firstElement());
    }

    static /* synthetic */ void b(TCPServerConnectionActivity tCPServerConnectionActivity, String str) {
        tCPServerConnectionActivity.setProgressBarIndeterminateVisibility(true);
        tCPServerConnectionActivity.kg.setEnabled(false);
        tCPServerConnectionActivity.kf.setEnabled(false);
        tCPServerConnectionActivity.ke.setEnabled(false);
        new bl(tCPServerConnectionActivity, str).start();
    }

    /* access modifiers changed from: private */
    public static boolean b(Vector vector, String str) {
        for (int i = 0; i < vector.size(); i++) {
            String str2 = (String) vector.elementAt(i);
            if (str2.equals(str) || str2.startsWith(String.valueOf(str) + ";")) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public static b f(String str, String str2) {
        b bVar = new b();
        try {
            bVar.a(str, str2);
        } catch (f e) {
            e.printStackTrace();
        }
        return bVar;
    }

    /* access modifiers changed from: private */
    public void f(Vector vector) {
        String str = "";
        for (int i = 0; i < vector.size(); i++) {
            if (i > 0) {
                str = String.valueOf(str) + ",";
            }
            str = String.valueOf(str) + ((String) vector.elementAt(i));
        }
        a.a(this, "server_ip_str", str);
    }

    private synchronized void i() {
        if (this.ad.size() > 0) {
            if (this.kj.getVisibility() == 0) {
                boolean z = this.af.size() == this.kb.size();
                if (z) {
                    int i = 0;
                    while (true) {
                        if (i >= this.kb.size()) {
                            break;
                        } else if (!this.af.contains(this.kb.elementAt(i))) {
                            z = false;
                            break;
                        } else {
                            i++;
                        }
                    }
                }
                Button button = new Button(this);
                button.setText(af.aa("繼續遊戲"));
                if (z) {
                    button.setOnClickListener(new bi(this));
                } else {
                    button.setEnabled(false);
                }
                this.ki.addView(button);
            }
            Button button2 = new Button(this);
            button2.setText(this.kj.getVisibility() == 0 ? af.aa("開始新遊戲") : af.aa("離開"));
            button2.setOnClickListener(new bj(this));
            this.ki.addView(button2);
        }
    }

    public final synchronized void a(aa aaVar) {
        if (!aaVar.m()) {
            a(5, af.aa("連線已中斷"));
            aaVar.l();
            if (this.ad != null) {
                this.ad.removeAllElements();
                a(4, (Object) null);
            }
        }
    }

    public final synchronized void a(aa aaVar, String str) {
        String str2;
        try {
            b bVar = new b(str);
            if (!bVar.H("group")) {
                this.ki.removeAllViews();
                this.kb.removeAllElements();
                b.a.a E = bVar.E("group");
                int i = 0;
                while (i < E.length()) {
                    b m = E.m(i);
                    if (i > 0) {
                        if (this.jZ.equals(m.getString("identity"))) {
                            d.qB = com.epoint.android.games.mjfgbfree.c.a.co[i - 1];
                        }
                        if (!m.H("uuid")) {
                            this.kb.add(m.getString("uuid"));
                        }
                    } else {
                        this.kn = m.J("is_admin");
                    }
                    boolean z = i == 0;
                    String string = m.getString("name");
                    TextView textView = new TextView(this);
                    textView.setTextColor(-1);
                    textView.setText(String.valueOf(z ? af.aa("主持") : af.aa("賓客")) + ": " + string);
                    textView.setBackgroundResource(17301602);
                    textView.setTextSize(20.0f);
                    textView.setPadding(4, 10, 4, 10);
                    LinearLayout linearLayout = new LinearLayout(this);
                    linearLayout.setMinimumHeight(1);
                    linearLayout.setBackgroundColor(-12303292);
                    this.ki.addView(textView);
                    this.ki.addView(linearLayout);
                    this.an.post(new bm(this));
                    i++;
                }
                if (E.length() > 1) {
                    i();
                }
            } else if (!bVar.H("command")) {
                if (bVar.getString("command").equals("start_game")) {
                    MJ16ViewActivity.ad = this.ad;
                    aaVar.a(null);
                    this.ad = null;
                    setResult(-1);
                    finish();
                }
            } else if (this.kn) {
                String str3 = "";
                if (!bVar.H("details")) {
                    b F = bVar.F("details");
                    Iterator bA = F.bA();
                    while (bA.hasNext()) {
                        String str4 = (String) bA.next();
                        String string2 = F.getString(str4);
                        str3 = String.valueOf((str4.equals("data_out") || str4.equals("data_in")) ? String.valueOf(str3) + str4 + ": " + String.format("%.2f", Double.valueOf((((double) Long.parseLong(string2)) / 1000.0d) / 1000.0d)) + "M" : String.valueOf(str3) + str4 + ": " + string2) + "\n";
                    }
                    str2 = str3;
                } else if (!bVar.H("lp")) {
                    b.a.a E2 = bVar.E("lp");
                    String str5 = str3;
                    for (int i2 = 0; i2 < E2.length(); i2++) {
                        b m2 = E2.m(i2);
                        String str6 = String.valueOf(str5) + m2.getString("name") + ": (in)" + String.format("%.2f", Double.valueOf((((double) m2.getLong("data_in")) / 1000.0d) / 1000.0d)) + "M" + " (out)" + String.format("%.2f", Double.valueOf((((double) m2.getLong("data_out")) / 1000.0d) / 1000.0d)) + "M";
                        if (!m2.H("game")) {
                            str6 = String.valueOf(str6) + " " + m2.getString("game");
                        }
                        str5 = String.valueOf(str6) + "\n";
                    }
                    str2 = str5;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                TextView textView2 = new TextView(this);
                textView2.setText(str2);
                builder.setView(textView2);
                builder.setPositiveButton(af.aa("好"), new bg(this));
                builder.create().show();
            }
        } catch (f e) {
            e.printStackTrace();
        }
        return;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.af = com.epoint.android.games.mjfgbfree.a.d.h(this);
        if (MJ16Activity.rd) {
            setRequestedOrientation(0);
        } else {
            setRequestedOrientation(ai.nf);
        }
        requestWindowFeature(5);
        a(4, (Object) null);
        setContentView((int) C0000R.layout.tcp_connection_activity);
        Bitmap c = com.epoint.android.games.mjfgbfree.a.d.c(this, "images/connect.png");
        this.ap = (ImageView) findViewById(C0000R.id.bg);
        this.ap.setImageBitmap(c);
        try {
            this.kd = getSharedPreferences("facebook-session", 0).getString("access_token", null);
            if (this.kd != null) {
                this.kd = URLDecoder.decode(this.kd, "UTF-8");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        this.kj = (TextView) findViewById(C0000R.id.identity);
        this.kg = (Button) findViewById(C0000R.id.ip_address);
        this.ki = (LinearLayout) findViewById(C0000R.id.players);
        Button button = (Button) findViewById(C0000R.id.help);
        button.setText(af.aa("幫助"));
        button.setOnClickListener(new cd(this));
        Bitmap c2 = com.epoint.android.games.mjfgbfree.a.d.c(this, "images/clipboard.png");
        this.kh = (ImageButton) findViewById(C0000R.id.clipboard_btn);
        this.kh.setImageBitmap(c2);
        this.kh.setOnClickListener(new cc(this));
        this.an = (ScrollView) this.ki.getParent();
        this.ke = (Button) findViewById(C0000R.id.host);
        this.ke.setText(af.aa("主持遊戲"));
        this.ke.setOnClickListener(new cb(this));
        this.kf = (Button) findViewById(C0000R.id.connect);
        this.kf.setText(af.aa("連線"));
        this.kf.setOnClickListener(new ca(this));
        ((TextView) findViewById(C0000R.id.ip_label)).setText(String.valueOf(af.aa("伺服器")) + ":");
        this.kl.add("<default>");
        this.kg.setText((CharSequence) this.kl.firstElement());
        String[] split = a.b(this, "server_ip_str", "").split(",");
        for (int i = 0; i < split.length; i++) {
            if (!split[i].equals("")) {
                this.km.add(split[i]);
                this.kl.add(split[i]);
            }
        }
        String b2 = a.b(this, "def_server_ip", this.kl.size() > 0 ? (String) this.kl.firstElement() : "");
        if (b(this.kl, b2) || b(this.km, b2)) {
            this.kg.setText(b2);
        }
        this.kg.setOnClickListener(new bz(this));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (this.kn) {
            menu.add(0, 0, 0, "Details");
            menu.add(0, 1, 0, "List");
        }
        return super.onCreateOptionsMenu(menu);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.ad != null) {
            String bVar = f("command", "quit").toString();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.ad.size()) {
                    break;
                }
                aa aaVar = (aa) this.ad.elementAt(i2);
                aaVar.d(bVar);
                aaVar.l();
                i = i2 + 1;
            }
        }
        String charSequence = this.kg.getText().toString();
        if (b(this.kl, charSequence)) {
            a.a(this, "def_server_ip", charSequence);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4 && this.ad.size() > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(String.valueOf(af.aa("取消連線")) + "?");
            builder.setPositiveButton(af.aa("是"), new bh(this));
            builder.setNegativeButton(af.aa("否"), new bk(this));
            builder.create().show();
            return true;
        } else if (i == 84) {
            return true;
        } else {
            return super.onKeyDown(i, keyEvent);
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        b bVar = new b();
        String str = "";
        switch (menuItem.getItemId()) {
            case 0:
                str = "details";
                break;
            case 1:
                str = "lp";
                break;
        }
        try {
            bVar.a("command", str);
            ((aa) this.ad.firstElement()).d(bVar.toString());
            return true;
        } catch (f e) {
            e.printStackTrace();
            return true;
        }
    }
}
