package com.epoint.android.games.mjfgbfree.network;

import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.af;

final class bu implements View.OnLongClickListener {
    private final /* synthetic */ LinearLayout fp;
    final /* synthetic */ TCPServerConnectionActivity sh;

    bu(TCPServerConnectionActivity tCPServerConnectionActivity, LinearLayout linearLayout) {
        this.sh = tCPServerConnectionActivity;
        this.fp = linearLayout;
    }

    public final boolean onLongClick(View view) {
        String[] split = ((String) this.sh.kl.elementAt(((Integer) view.getTag()).intValue())).split(";");
        AlertDialog.Builder builder = new AlertDialog.Builder(this.sh);
        builder.setTitle(af.aa("設定連線"));
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(this.sh).inflate((int) C0000R.layout.ip_alias, (ViewGroup) null);
        ((TextView) linearLayout.findViewById(C0000R.id.label_alias)).setText(af.aa("別名"));
        ((TextView) linearLayout.findViewById(C0000R.id.label_ip)).setText(af.aa("IP地址"));
        EditText editText = (EditText) linearLayout.findViewById(C0000R.id.text_alias);
        EditText editText2 = (EditText) linearLayout.findViewById(C0000R.id.text_ip);
        editText2.setText(split[0]);
        builder.setView(linearLayout);
        if (split.length > 1) {
            editText.setText(split[1]);
        }
        builder.setPositiveButton(af.aa("好"), new i(this, editText, editText2, view, this.fp));
        builder.setNegativeButton(af.aa("取消"), new j(this));
        builder.show();
        return false;
    }
}
