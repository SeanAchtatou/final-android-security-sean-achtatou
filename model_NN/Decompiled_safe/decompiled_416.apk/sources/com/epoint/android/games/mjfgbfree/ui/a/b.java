package com.epoint.android.games.mjfgbfree.ui.a;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.epoint.android.games.mjfgbfree.C0000R;
import com.epoint.android.games.mjfgbfree.MJ16Activity;
import com.epoint.android.games.mjfgbfree.MJ16ViewActivity;
import com.epoint.android.games.mjfgbfree.af;
import com.epoint.android.games.mjfgbfree.ai;
import com.epoint.android.games.mjfgbfree.bb;
import com.epoint.android.games.mjfgbfree.d.c;
import com.epoint.android.games.mjfgbfree.d.g;
import com.epoint.android.games.mjfgbfree.ui.a;
import java.util.Hashtable;
import java.util.Vector;

public final class b extends e implements i {
    private a gI = new a(100, 60, -65536, -16777216, 160, af.aa("關閉"));
    private a gJ = new a(100, 60, Color.rgb(205, 100, 0), -16777216, 160, af.aa("抓圖"));
    private a gK = new a(100, 60, Color.rgb(205, 100, 0), -16777216, 160, af.aa("分享"));
    /* access modifiers changed from: private */
    public WebView gL;
    private Vector gM = new Vector();
    private Paint gN;
    private g gO;
    private String gP = "";
    private int gQ;
    private Thread gR = null;
    /* access modifiers changed from: private */
    public Handler mHandler = new l(this);

    public b() {
        this.gI.a(false, false);
        this.gJ.a(false, false);
        this.gK.a(false, false);
        this.gM.addElement(this.gI);
        this.gM.addElement(this.gJ);
        this.gM.addElement(this.gK);
        this.gN = new Paint();
        this.gN.setAlpha(255);
        this.gN.setStyle(Paint.Style.FILL);
        this.gN.setAntiAlias(true);
        this.gN.setTextSize((float) ((ai.nk == 1 || ai.nk == 5) ? 25 : 28));
        this.gN.setColor(-16777216);
    }

    private String a(c cVar) {
        String str;
        String str2;
        String str3;
        String str4;
        String d;
        String str5 = "";
        if (this.gO.wf.size() > 1) {
            String str6 = "<img src=\"file:///android_asset/assets/images/exp" + this.gO.wf.size() + ".png\" style='width:" + ((int) (MJ16Activity.rc * 24.0f)) + "px; height:" + ((int) (MJ16Activity.rc * 24.0f)) + "px;' />";
            str5 = this.gO.wf.size() == 2 ? String.valueOf(str6) + "<font color='#008000'>" + af.ab("一炮雙響").trim() + "</font>, " : String.valueOf(str6) + "<font color='#008000'>" + af.ab("一炮三響").trim() + "</font>, ";
        }
        int i = 0;
        while (true) {
            str = str5;
            if (i >= cVar.lN.size()) {
                break;
            }
            int i2 = ((a.a.g) cVar.lN.elementAt(i)).nV;
            StringBuilder sb = new StringBuilder(String.valueOf(((a.a.g) cVar.lN.elementAt(i)).cx()));
            if (i2 > 0) {
                str2 = String.valueOf(i2) + af.aa("番") + (((ai.nk == 1 || ai.nk == 5) && i2 > 1) ? "s" : "");
            } else {
                str2 = "";
            }
            sb.append(str2).toString();
            int i3 = MJ16Activity.rb;
            StringBuilder sb2 = new StringBuilder(String.valueOf(af.a((a.a.g) cVar.lN.elementAt(i))));
            if (i2 > 0) {
                str3 = String.valueOf(i2) + af.aa("番") + (((ai.nk == 1 || ai.nk == 5) && i2 > 1) ? "s" : "");
            } else {
                str3 = "";
            }
            String sb3 = sb2.append(str3).toString();
            int A = af.A(((a.a.g) cVar.lN.elementAt(i)).nW / ((a.a.g) cVar.lN.elementAt(i)).mo);
            if (A >= 4 && A < 8) {
                i3 = MJ16Activity.rb + 1;
            } else if (A >= 8 && A < 12) {
                i3 = MJ16Activity.rb + 2;
            }
            if (i > 0) {
                str = String.valueOf(str) + ", ";
            }
            if (((a.a.g) cVar.lN.elementAt(i)).nX || (d = af.d(((a.a.g) cVar.lN.elementAt(i)).cx(), A)) == null) {
                str4 = sb3;
            } else {
                String str7 = "<img src=\"file:///android_asset/assets/images/" + d + ".png\" style='width:" + ((int) (MJ16Activity.rc * 24.0f)) + "px; height:" + ((int) (MJ16Activity.rc * 24.0f)) + "px;' />";
                String substring = sb3.substring(0, 1);
                String substring2 = sb3.substring(1);
                str4 = (A <= 2 || ((a.a.g) cVar.lN.elementAt(i)).cx().equals("門清自摸")) ? (((a.a.g) cVar.lN.elementAt(i)).cx().equals("自摸") || ((a.a.g) cVar.lN.elementAt(i)).cx().equals("門清自摸") || ((a.a.g) cVar.lN.elementAt(i)).cx().equals("不求人")) ? "<font color='#800000'><span style='white-space:nowrap'>" + str7 + substring + "</span>" + substring2 + "</font>" : "<span style='white-space:nowrap'>" + str7 + substring + "</span>" + substring2 : "<font color='#800000' style='font-size:" + i3 + "px'><span style='white-space:nowrap'>" + str7 + substring + "</span>" + substring2 + "</font>";
            }
            str5 = String.valueOf(str) + str4;
            i++;
        }
        String str8 = String.valueOf(str) + "<br><div style='padding-top:8px;'><div style='padding:3px; border-style: dotted;border-width: 3px; border-color: teal;'><table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td valign='middle' nowrap><font color='#000080'>&nbsp;" + af.aa("結算") + "</font></td><td align='right'><table border='0' cellpadding='0' cellspacing='0'><tr>";
        int[] iArr = cVar.lO;
        String str9 = str8;
        int i4 = 0;
        for (int i5 = 0; i5 < iArr.length; i5++) {
            if (iArr[i5] != 0) {
                String str10 = "";
                String str11 = this.gO.vU[i5].xx;
                if (i5 == this.gO.aG) {
                    str10 = af.aa("你");
                } else if ((this.gO.aG + 1) % 4 == i5) {
                    str10 = str11.equals("") ? af.aa("下家") : str11;
                } else if ((this.gO.aG + 2) % 4 == i5) {
                    str10 = str11.equals("") ? af.aa("對家") : str11;
                } else if ((this.gO.aG + 3) % 4 == i5) {
                    str10 = str11.equals("") ? af.aa("上家") : str11;
                }
                int i6 = iArr[i5];
                str9 = String.valueOf(str9) + "<td align='right' style=''>" + str10 + ":</td><td align='right' nowrap>" + (i6 > 0 ? "<font color='#008000'>+" + i6 + "&nbsp;</font>" : "<font color='#800000'>" + i6 + "&nbsp;</font></td>");
                if (i4 == 1) {
                    str9 = String.valueOf(str9) + "</tr><tr>";
                }
                i4++;
            }
        }
        return "<font color='#404040' style='font-size:" + MJ16Activity.rb + "px'>" + (String.valueOf(str9) + "</tr></table></td><tr></table></div>") + "</font>";
    }

    private void bh() {
        boolean z = this.gM.size() > 3;
        if (MJ16Activity.ra > MJ16Activity.qZ) {
            this.js = new a(420, 580 - (z ? 0 : ((a) this.gM.firstElement()).ba() + 10), -1, Color.rgb(128, 0, 0), 200, null);
            Rect bounds = this.js.getBounds();
            bounds.top += bb.dR();
            this.js.setBounds(bounds);
            this.js.c((MJ16Activity.qZ - this.js.bb()) / 2, (bb.ty == 1 ? 0 : bb.dR()) + (((MJ16Activity.ra - bb.dR()) - this.js.ba()) / 2));
            this.ju = new Rect(this.js.getBounds());
            return;
        }
        this.js = new a(600 - (z ? 0 : ((a) this.gM.firstElement()).bb() + 10), 420, -1, Color.rgb(128, 0, 0), 200, null);
        Rect bounds2 = this.js.getBounds();
        bounds2.top += bb.dR();
        this.js.setBounds(bounds2);
        this.js.c((MJ16Activity.qZ - this.js.bb()) / 2, (bb.ty == 1 ? 0 : bb.dR()) + (((MJ16Activity.ra - bb.dR()) - this.js.ba()) / 2));
        this.ju = new Rect(this.js.getBounds());
    }

    private void bj() {
        this.gL.clearHistory();
        this.gL.loadDataWithBaseURL("/", "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"DTD/xhtml1-transitional.dtd\"><html><head><META HTTP-EQUIV=\"content-type\" CONTENT=\"text/html; charset=UTF-8\" /></head><body marginwidth=\"0\" marginheight=\"0\" leftmargin=\"0\" topmargin=\"0\">" + this.gP + "<br>" + "</body>" + "</html>", "text/html", "UTF-8", "/");
    }

    private void e(boolean z) {
        this.gJ.b(false);
        this.gK.b(false);
        this.gI.a(z, false);
        this.gJ.a(z, false);
        this.gK.a(z, false);
    }

    private void f(boolean z) {
        e(false);
        this.jr.a((int) (((float) this.gJ.ba()) * MJ16Activity.rc), z);
    }

    public final Object a(int i, int i2, int i3, Object obj) {
        a aVar = null;
        int i4 = 0;
        while (true) {
            a aVar2 = aVar;
            if (i4 >= this.gM.size()) {
                return aVar2;
            }
            aVar = (a) this.gM.elementAt(i4);
            aVar.b(false);
            if (aVar.isVisible() && aVar.bf()) {
                Rect bounds = aVar.getBounds();
                if (i >= bounds.left && i <= bounds.right && i2 >= bounds.top && i2 <= bounds.bottom && (i3 == 0 || (i3 != 0 && obj == aVar))) {
                    aVar.b(true);
                    i4++;
                }
            }
            aVar = aVar2;
            i4++;
        }
    }

    public final void a(Bitmap bitmap, boolean z) {
        e(true);
        this.jr.er();
        this.jq.a(z, bitmap);
    }

    public final synchronized void a(Canvas canvas) {
        int ba;
        int i;
        if (this.jt == 1 || this.jt >= 4) {
            bh();
            this.gI.c((MJ16Activity.qZ - this.gI.bb()) / 2, (this.ju.bottom - this.gI.ba()) - 10);
            this.gJ.c(this.ju.left + 10, (this.ju.bottom - this.gJ.ba()) - 10);
            this.gK.c((this.ju.right - this.gK.bb()) - 10, (this.ju.bottom - this.gK.ba()) - 10);
            Rect rect = this.ju;
            int i2 = rect.top + 10;
            int i3 = 0;
            int i4 = rect.left + 10;
            while (i3 < this.gM.size() - 3) {
                ((a) this.gM.elementAt(i3)).c(i4, i2);
                if (MJ16Activity.ra > MJ16Activity.qZ) {
                    i = ((a) this.gM.elementAt(i3)).bb() + 10 + i4;
                    ba = i2;
                } else {
                    int i5 = i4;
                    ba = ((a) this.gM.elementAt(i3)).ba() + 10 + i2;
                    i = i5;
                }
                i3++;
                i2 = ba;
                i4 = i;
            }
        }
        super.a(canvas);
    }

    public final void a(MJ16ViewActivity mJ16ViewActivity, bb bbVar) {
        this.jq = mJ16ViewActivity;
        this.jr = bbVar;
        bh();
        this.gL = (WebView) this.jq.findViewById(C0000R.id.ResultView);
        this.gL.setBackgroundColor(0);
        this.gL.setInitialScale(100);
        this.gL.setVerticalFadingEdgeEnabled(true);
        this.gL.loadDataWithBaseURL("/", "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"DTD/xhtml1-transitional.dtd\"><html><head><META HTTP-EQUIV=\"content-type\" CONTENT=\"text/html; charset=UTF-8\" /></head><body marginwidth=\"0\" marginheight=\"0\" leftmargin=\"0\" topmargin=\"0\"></body></html>", "text/html", "UTF-8", "/");
    }

    public final void a(g gVar) {
        int i;
        String str;
        c cVar;
        this.gO = gVar;
        this.gM.removeAllElements();
        c cVar2 = null;
        if (gVar.wf.size() > 1) {
            int i2 = 0;
            c cVar3 = null;
            int i3 = -1;
            String str2 = "";
            while (i2 < gVar.wf.size()) {
                int i4 = ((c) gVar.wf.elementAt(i2)).lJ;
                if (i4 == gVar.aG) {
                    String aa = af.aa("你");
                    i = gVar.aG;
                    str = aa;
                } else if ((gVar.aG + 1) % 4 == i4) {
                    i = i3;
                    str = af.aa("下家");
                } else if ((gVar.aG + 2) % 4 == i4) {
                    i = i3;
                    str = af.aa("對家");
                } else if ((gVar.aG + 3) % 4 == i4) {
                    i = i3;
                    str = af.aa("上家");
                } else {
                    i = i3;
                    str = str2;
                }
                a aVar = new a(120, 60, Color.rgb(0, 128, 0), -16777216, 160, str);
                aVar.gx = String.valueOf(i2);
                aVar.a(true, false);
                this.gM.addElement(aVar);
                if (i4 == gVar.aG) {
                    aVar.c(true);
                    cVar = (c) gVar.wf.elementAt(i2);
                } else {
                    cVar = cVar3;
                }
                i2++;
                str2 = str;
                cVar3 = cVar;
                i3 = i;
            }
            if (i3 == -1) {
                ((a) this.gM.firstElement()).c(true);
                cVar2 = (c) gVar.wf.elementAt(Integer.valueOf(((a) this.gM.firstElement()).gx).intValue());
            } else {
                cVar2 = cVar3;
            }
        } else if (gVar.wf.size() == 1) {
            cVar2 = (c) gVar.wf.firstElement();
        }
        if (cVar2 != null) {
            this.gQ = cVar2.ge;
            this.gP = a(cVar2);
        } else {
            this.gQ = -1;
            this.gP = "<div style='text-align:center; font-size:" + (MJ16Activity.rb + 5) + "px'>" + af.aa("荒牌") + "</div>";
            this.jr.ut = null;
        }
        bj();
        this.gI.a(true, false);
        this.gJ.a(true, false);
        this.gK.a(true, false);
        this.gM.addElement(this.gI);
        this.gM.addElement(this.gJ);
        this.gM.addElement(this.gK);
    }

    public final void a(Object obj) {
        if (obj == this.gI) {
            this.gI.b(false);
            super.a(obj);
            this.gL.setVisibility(8);
            super.b(this.gR);
        } else if (obj == this.gJ) {
            f(false);
        } else if (obj == this.gK) {
            f(true);
        } else {
            a aVar = (a) obj;
            for (int i = 0; i < this.gM.size() - 3; i++) {
                ((a) this.gM.elementAt(i)).c(false);
            }
            aVar.b(false);
            aVar.c(true);
            c cVar = (c) this.gO.wf.elementAt(Integer.valueOf(aVar.gx).intValue());
            this.gQ = cVar.ge;
            this.gP = a(cVar);
            bj();
            this.jr.er();
        }
    }

    public final void a(Thread thread) {
        this.gR = thread;
        a(this.gI);
    }

    public final void b(Canvas canvas) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        boolean z;
        int i6;
        c cVar;
        canvas.drawARGB(64, 0, 0, 0);
        this.js.draw(canvas);
        int i7 = this.js.getBounds().top;
        int i8 = 0;
        while (true) {
            int i9 = i8;
            if (i9 >= this.gM.size()) {
                break;
            }
            if (((a) this.gM.elementAt(i9)).isVisible()) {
                ((a) this.gM.elementAt(i9)).draw(canvas);
            }
            i8 = i9 + 1;
        }
        boolean z2 = MJ16Activity.ra > MJ16Activity.qZ;
        boolean z3 = this.gM.size() > 3;
        int i10 = (!z3 || !z2) ? i7 + 50 : i7 + 110;
        Hashtable hashtable = bb.tE;
        if (this.gQ > 0) {
            c cVar2 = (c) this.gO.wf.firstElement();
            int i11 = 0;
            int i12 = this.gO.li;
            while (i11 < this.gM.size() - 3) {
                if (((a) this.gM.elementAt(i11)).be()) {
                    c cVar3 = (c) this.gO.wf.elementAt(Integer.valueOf(((a) this.gM.elementAt(i11)).gx).intValue());
                    cVar = cVar3;
                    i6 = cVar3.lJ;
                } else {
                    i6 = i12;
                    cVar = cVar2;
                }
                i11++;
                cVar2 = cVar;
                i12 = i6;
            }
            Vector vector = this.gO.vU[i12].fV;
            int i13 = (vector.size() == 17 ? -5 : 5) + this.js.getBounds().left + 15;
            if (z3 && !z2) {
                i13 = ((a) this.gM.firstElement()).bb() + 10 + i13;
            }
            String str = "";
            switch (this.gO.wf.size()) {
                case 2:
                    str = " (" + af.ab("一炮雙響").trim() + ")";
                    break;
                case 3:
                    str = " (" + af.ab("一炮三響").trim() + ")";
                    break;
            }
            canvas.drawText(String.valueOf(this.gO.vT == -1 ? af.aa("自摸long") : af.aa("食糊long")) + this.gQ + af.aa("番") + (((ai.nk == 1 || ai.nk == 5) && this.gQ > 1) ? "s" : "") + str, (float) i13, (float) (i10 - 6), this.gN);
            int i14 = i10 + 5;
            if (!(this.gO.vU[i12].fZ.size() == 0 && this.gO.vU[i12].fX.size() == 0)) {
                i14 = ((Bitmap) hashtable.get("bc")).getHeight() + 2 + i14;
            }
            int i15 = 0;
            int i16 = i13;
            while (i15 < this.gO.vU[i12].fZ.size()) {
                canvas.drawBitmap((Bitmap) hashtable.get((String) this.gO.vU[i12].fZ.elementAt(i15)), (float) i16, (float) i14, this.gN);
                i15++;
                i16 = (((Bitmap) hashtable.get("bc")).getWidth() - 1) + i16;
            }
            int i17 = this.gO.vU[i12].fZ.size() > 0 ? i16 + 5 : i16;
            boolean z4 = false;
            int i18 = i14;
            int i19 = 0;
            int i20 = i17;
            int i21 = 0;
            int i22 = 0;
            while (i19 < this.gO.vU[i12].fX.size()) {
                String str2 = (String) this.gO.vU[i12].fX.elementAt(i19);
                if (str2.length() == 3) {
                    str2 = str2.substring(0, 2);
                }
                if (str2.length() == 1) {
                    int i23 = i20 + 5;
                    int i24 = i22 + 1;
                    if (i24 == 2) {
                        i2 = i18 - (((Bitmap) hashtable.get("bc")).getHeight() + 2);
                        i5 = 0;
                        z = true;
                        i3 = i24;
                        i4 = i13;
                    } else {
                        i2 = i18;
                        boolean z5 = z4;
                        i3 = i24;
                        i4 = i23;
                        i5 = 0;
                        z = z5;
                    }
                } else {
                    int i25 = i21 + 1;
                    boolean z6 = i25 == 4 && this.gO.vU[i12].gc.indexOf(str2) >= 0;
                    if (z6) {
                        str2 = (String) this.gO.vU[i12].fX.elementAt(i19 - 1);
                    }
                    canvas.drawBitmap((Bitmap) hashtable.get(str2), (float) i20, (float) i18, this.gN);
                    if (z6) {
                        this.gN.setAlpha(128);
                        canvas.drawBitmap((Bitmap) hashtable.get("BK"), (float) i20, (float) i18, this.gN);
                        this.gN.setAlpha(255);
                    }
                    int width = (((Bitmap) hashtable.get("bc")).getWidth() - 1) + i20;
                    i2 = i18;
                    boolean z7 = z4;
                    i3 = i22;
                    i4 = width;
                    i5 = i25;
                    z = z7;
                }
                i19++;
                i18 = i2;
                i20 = i4;
                i22 = i3;
                z4 = z;
                i21 = i5;
            }
            if (z4 || (this.gO.vU[i12].fZ.size() == 0 && this.gO.vU[i12].fX.size() == 0)) {
                i = i20;
                i10 = i18;
            } else {
                i10 = i18 - (((Bitmap) hashtable.get("bc")).getHeight() + 2);
                i = i13;
            }
            if (cVar2 != null && cVar2.lP.size() > 0) {
                vector = cVar2.lP;
            }
            int i26 = 0;
            while (true) {
                int i27 = i;
                if (i26 < vector.size()) {
                    if (i26 == vector.size() - 1 && vector.size() % 3 == 2) {
                        i27 += 10;
                        canvas.drawBitmap((Bitmap) hashtable.get(vector.elementAt(i26)), (float) i27, (float) i10, this.gN);
                        Rect rect = new Rect();
                        rect.left = i27;
                        rect.top = i10;
                        rect.right = ((Bitmap) hashtable.get("bc")).getWidth() + rect.left;
                        rect.bottom = ((Bitmap) hashtable.get("bc")).getHeight() + rect.top;
                        int color = this.gN.getColor();
                        this.gN.setStyle(Paint.Style.STROKE);
                        this.gN.setColor(-65536);
                        canvas.drawRect(rect, this.gN);
                        this.gN.setStyle(Paint.Style.FILL);
                        this.gN.setColor(color);
                    } else {
                        canvas.drawBitmap((Bitmap) hashtable.get(vector.elementAt(i26)), (float) i27, (float) i10, this.gN);
                    }
                    i = (((Bitmap) hashtable.get("bc")).getWidth() - 1) + i27;
                    i26++;
                } else if (!(this.gO.vU[i12].fZ.size() == 0 && this.gO.vU[i12].fX.size() == 0)) {
                    i10 = ((Bitmap) hashtable.get("bc")).getHeight() + 2 + i10;
                }
            }
        }
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) ((LinearLayout) this.gL.getParent()).getLayoutParams();
        layoutParams.bottomMargin = (MJ16Activity.ra - this.gI.getBounds().top) + 4;
        layoutParams.bottomMargin = (layoutParams.bottomMargin * MJ16Activity.qR) / MJ16Activity.ra;
        layoutParams.leftMargin = ((this.js.getBounds().left + 23) * MJ16Activity.qS) / MJ16Activity.qZ;
        layoutParams.rightMargin = layoutParams.leftMargin;
        layoutParams.topMargin = ((Bitmap) hashtable.get("bc")).getHeight() + i10 + 3;
        layoutParams.topMargin = (layoutParams.topMargin * MJ16Activity.qR) / MJ16Activity.ra;
        if (z3 && !z2) {
            layoutParams.leftMargin = ((((a) this.gM.firstElement()).bb() * MJ16Activity.qS) / MJ16Activity.qZ) + layoutParams.leftMargin;
        }
        ((LinearLayout) this.gL.getParent()).setLayoutParams(layoutParams);
        this.gL.setVisibility(0);
        ((LinearLayout) this.gL.getParent()).bringToFront();
        new m(this).start();
    }

    public final void bi() {
        a((Thread) null);
    }

    public final void bk() {
        this.gI.b(false);
        this.jr.a(this, -1, null);
    }
}
