package com.epoint.android.games.mjfgbfree.network;

import android.content.DialogInterface;
import android.view.View;
import android.widget.LinearLayout;

final class af implements DialogInterface.OnClickListener {
    private final /* synthetic */ View fo;
    private final /* synthetic */ LinearLayout fp;
    private /* synthetic */ y jX;

    af(y yVar, View view, LinearLayout linearLayout) {
        this.jX = yVar;
        this.fo = view;
        this.fp = linearLayout;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.jX.hZ.km.remove(this.fo.getTag());
        this.jX.hZ.kl.remove(this.fo.getTag());
        TCPLocalHostConnectionActivity.a(this.jX.hZ, this.fp);
        this.jX.hZ.f(this.jX.hZ.km);
    }
}
