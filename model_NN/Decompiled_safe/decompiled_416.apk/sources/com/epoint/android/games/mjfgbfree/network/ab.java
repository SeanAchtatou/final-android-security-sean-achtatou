package com.epoint.android.games.mjfgbfree.network;

import android.os.Handler;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class ab implements aa, Runnable {
    private int aG = -1;
    private String aH;
    private String aI;
    private Handler handler = new by(this);
    private Thread iA;
    private Socket iB;
    private t iC;
    /* access modifiers changed from: private */
    public OutputStream ix;
    /* access modifiers changed from: private */
    public int iy;
    private Thread iz;

    public ab(Socket socket) {
        try {
            socket.setSoTimeout(240000);
            this.ix = socket.getOutputStream();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.iB = socket;
    }

    /* access modifiers changed from: protected */
    public void a(aa aaVar) {
        this.iC.a(aaVar);
    }

    /* access modifiers changed from: protected */
    public void a(aa aaVar, String str) {
        this.iC.a(aaVar, str);
    }

    public final void a(t tVar) {
        this.iC = tVar;
        if (this.iz == null) {
            this.iz = new Thread(this);
            this.iz.start();
        }
    }

    public final void b(int i) {
        this.aG = i;
    }

    public synchronized boolean d(String str) {
        boolean z;
        String str2 = String.valueOf(str) + "\r\n";
        try {
            if (!this.iB.isClosed()) {
                synchronized (this.ix) {
                    this.ix.write(str2.getBytes("UTF-8"));
                }
                if (this.iA != null && this.iA.isAlive()) {
                    this.iA.interrupt();
                }
                this.iA = new bx(this);
                this.iA.start();
                z = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
            if (this.iA != null && this.iA.isAlive()) {
                this.iA.interrupt();
            }
        }
        z = false;
        return z;
    }

    public final void e(String str) {
        this.aH = str;
    }

    public void f(String str) {
        this.aI = str;
    }

    public boolean l() {
        if (this.iB.isClosed()) {
            return false;
        }
        try {
            this.iB.getInputStream().close();
        } catch (Exception e) {
        }
        try {
            this.iB.getOutputStream().close();
        } catch (Exception e2) {
        }
        try {
            this.iB.close();
        } catch (Exception e3) {
        }
        return true;
    }

    public final boolean m() {
        return this.iB.isClosed();
    }

    public final int n() {
        return this.aG;
    }

    public final String o() {
        return this.aH;
    }

    public final String p() {
        return this.aI;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0063, code lost:
        r2 = "";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r2 = r1.toString("UTF-8");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x008d, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x008e, code lost:
        r3.printStackTrace();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r5 = this;
            r0 = 0
            java.net.Socket r1 = r5.iB     // Catch:{ IOException -> 0x002a }
            java.io.InputStream r0 = r1.getInputStream()     // Catch:{ IOException -> 0x002a }
        L_0x0007:
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r2 = 4096(0x1000, float:5.74E-42)
            r1.<init>(r2)
        L_0x000e:
            java.net.Socket r2 = r5.iB
            boolean r2 = r2.isClosed()
            if (r2 == 0) goto L_0x002f
        L_0x0016:
            boolean r0 = com.epoint.android.games.mjfgbfree.bb.dU()
            if (r0 != 0) goto L_0x009d
            android.os.Message r0 = new android.os.Message
            r0.<init>()
            r1 = 2
            r0.what = r1
            android.os.Handler r1 = r5.handler
            r1.sendMessage(r0)
            return
        L_0x002a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0007
        L_0x002f:
            r1.reset()     // Catch:{ Exception -> 0x0052 }
        L_0x0032:
            int r2 = r0.read()     // Catch:{ Exception -> 0x0052 }
            r3 = -1
            if (r2 == r3) goto L_0x005b
            r3 = 13
            if (r2 == r3) goto L_0x0032
            r3 = 10
            if (r2 == r3) goto L_0x0063
            int r3 = r1.size()     // Catch:{ Exception -> 0x0052 }
            r4 = 32768(0x8000, float:4.5918E-41)
            if (r3 != r4) goto L_0x0057
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ Exception -> 0x0052 }
            java.lang.String r1 = "Buffer overflow"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0052 }
            throw r0     // Catch:{ Exception -> 0x0052 }
        L_0x0052:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0016
        L_0x0057:
            r1.write(r2)     // Catch:{ Exception -> 0x0052 }
            goto L_0x0032
        L_0x005b:
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ Exception -> 0x0052 }
            java.lang.String r1 = "Socket read error."
            r0.<init>(r1)     // Catch:{ Exception -> 0x0052 }
            throw r0     // Catch:{ Exception -> 0x0052 }
        L_0x0063:
            java.lang.String r2 = ""
            java.lang.String r3 = "UTF-8"
            java.lang.String r2 = r1.toString(r3)     // Catch:{ UnsupportedEncodingException -> 0x008d }
        L_0x006b:
            java.lang.String r3 = ""
            boolean r3 = r2.equals(r3)
            if (r3 != 0) goto L_0x000e
        L_0x0073:
            boolean r3 = com.epoint.android.games.mjfgbfree.bb.dU()
            if (r3 != 0) goto L_0x0092
            com.epoint.android.games.mjfgbfree.network.t r3 = r5.iC
            if (r3 == 0) goto L_0x0092
            android.os.Message r3 = new android.os.Message
            r3.<init>()
            r4 = 1
            r3.what = r4
            r3.obj = r2
            android.os.Handler r2 = r5.handler
            r2.sendMessage(r3)
            goto L_0x000e
        L_0x008d:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x006b
        L_0x0092:
            r3 = 20
            java.lang.Thread.sleep(r3)     // Catch:{ InterruptedException -> 0x0098 }
            goto L_0x0073
        L_0x0098:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0073
        L_0x009d:
            r0 = 20
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x00a4 }
            goto L_0x0016
        L_0x00a4:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.epoint.android.games.mjfgbfree.network.ab.run():void");
    }
}
