package com.epoint.android.games.mjfgbfree.tournament;

import android.view.View;
import android.widget.RelativeLayout;
import com.epoint.android.games.mjfgbfree.C0000R;
import java.util.Vector;

final class i implements View.OnClickListener {
    private /* synthetic */ TournamentActivity su;
    private final /* synthetic */ Vector sy;

    i(TournamentActivity tournamentActivity, Vector vector) {
        this.su = tournamentActivity;
        this.sy = vector;
    }

    public final void onClick(View view) {
        view.setVisibility(8);
        this.su.hj.setBackgroundResource(0);
        for (int i = 0; i < this.sy.size(); i++) {
            ((RelativeLayout) this.sy.elementAt(i)).setEnabled(true);
            ((RelativeLayout) this.sy.elementAt(i)).findViewById(C0000R.id.overlay).setVisibility(8);
            ((RelativeLayout) this.sy.elementAt(i)).setVisibility(0);
        }
    }
}
