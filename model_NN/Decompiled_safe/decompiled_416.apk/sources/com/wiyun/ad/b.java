package com.wiyun.ad;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class b implements Runnable {
    private /* synthetic */ m hf;
    private final /* synthetic */ View hg;

    b(m mVar, View view) {
        this.hf = mVar;
        this.hg = view;
    }

    public final void run() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.hf.iP.getContext().getSystemService("input_method");
        if (inputMethodManager.isActive(this.hg)) {
            inputMethodManager.showSoftInput(this.hg, 0);
        } else {
            this.hg.postDelayed(this, 50);
        }
    }
}
