package com.wiyun.ad;

import android.view.animation.Interpolator;

final class s implements Interpolator {
    private float qz = 0.027777778f;

    public final float getInterpolation(float f) {
        return ((float) ((int) (f / this.qz))) * this.qz;
    }
}
