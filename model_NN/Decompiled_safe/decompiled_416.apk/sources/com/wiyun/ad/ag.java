package com.wiyun.ad;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;

public final class ag {
    private static ag Al;
    private q Am;
    private BroadcastReceiver An = new aq(this);
    /* access modifiers changed from: private */
    public boolean dx;
    private Context rr;

    private ag(Context context) {
        this.rr = context.getApplicationContext();
        this.Am = null;
        this.rr.registerReceiver(this.An, new IntentFilter("android.net.wifi.STATE_CHANGE"));
        this.dx = af.w(context);
    }

    public static void X() {
        if (Al != null) {
            Al.rr.unregisterReceiver(Al.An);
            Al.rr = null;
            Al.Am = null;
            Al = null;
        }
    }

    public static boolean fX() {
        if (Al == null) {
            return false;
        }
        return Al.dx;
    }

    public static void x(Context context) {
        if (Al == null) {
            Al = new ag(context);
        }
    }
}
