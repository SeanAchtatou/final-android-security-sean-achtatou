package com.wiyun.ad;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class aj {
    private static MessageDigest aZ(String str) {
        try {
            return MessageDigest.getInstance(str);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static byte[] ba(String str) {
        return f(ar.m(str, "UTF-8"));
    }

    public static String bb(String str) {
        return an.g(ba(str));
    }

    private static byte[] f(byte[] bArr) {
        return aZ("MD5").digest(bArr);
    }

    public static String g(byte[] bArr) {
        return an.g(f(bArr));
    }
}
