package com.wiyun.ad;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import java.net.URLEncoder;

final class l implements View.OnKeyListener {
    final /* synthetic */ AdView iP;
    private final /* synthetic */ LinearLayout lC;

    l(AdView adView, LinearLayout linearLayout) {
        this.iP = adView;
        this.lC = linearLayout;
    }

    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            if (i == 66) {
                String editable = ((EditText) view).getEditableText().toString();
                if (!TextUtils.isEmpty(editable)) {
                    Context applicationContext = this.iP.getContext().getApplicationContext();
                    new w(this, applicationContext).start();
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.addFlags(268435456);
                    try {
                        intent.setData(Uri.parse(this.iP.kO.kJ.replace("%query%", URLEncoder.encode(editable, "utf-8"))));
                        applicationContext.startActivity(intent);
                    } catch (Exception e) {
                        Log.e("WiYun", "Could not open browser on ad click to " + this.iP.kO.kJ, e);
                    }
                    AdView.a(this.iP, this.lC);
                }
                return true;
            } else if (i == 4) {
                AdView.a(this.iP, this.lC);
                return true;
            }
        }
        return false;
    }
}
