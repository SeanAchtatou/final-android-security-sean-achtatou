package com.wiyun.ad;

import android.os.Handler;
import android.util.Log;

final class i extends Thread {
    final /* synthetic */ AdView iP;

    i(AdView adView) {
        this.iP = adView;
    }

    public final void run() {
        try {
            this.iP.kO = ah.a(this.iP.getContext(), this.iP);
            Handler handler = this.iP.getHandler();
            while (handler == null) {
                Thread.sleep(100);
                handler = this.iP.getHandler();
            }
            handler.post(new k(this));
        } catch (Exception e) {
            Log.e("WiYun", "Unhandled exception requesting a fresh ad.", e);
        }
    }
}
