package com.wiyun.ad;

import android.view.animation.Animation;
import android.widget.EditText;

final class j implements Animation.AnimationListener {
    private /* synthetic */ AdView iP;
    private final /* synthetic */ EditText lA;

    j(AdView adView, EditText editText) {
        this.iP = adView;
        this.lA = editText;
    }

    public final void onAnimationEnd(Animation animation) {
        this.lA.requestFocus();
        this.iP.kN = false;
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
