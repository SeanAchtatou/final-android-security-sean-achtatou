package com.wiyun.ad;

import android.view.animation.Animation;

final class f implements Animation.AnimationListener {
    private final /* synthetic */ ae iN;
    private /* synthetic */ AdView iP;

    f(AdView adView, ae aeVar) {
        this.iP = adView;
        this.iN = aeVar;
    }

    public final void onAnimationEnd(Animation animation) {
        this.iP.ky.clearAnimation();
        this.iP.removeView(this.iP.ky);
        this.iP.bE();
        if (this.iP.ky.fV() != null) {
            this.iP.ky.fV().X();
        }
        this.iP.ky = this.iN;
        this.iP.dx = false;
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
