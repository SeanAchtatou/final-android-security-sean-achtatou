package com.wiyun.ad;

import android.content.Context;
import android.content.res.Configuration;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import java.util.Locale;

final class af {
    private static String cU;
    private static boolean dS = true;
    private static String dz;
    private static String ik;
    private static int is = -1;

    af() {
    }

    static String fO() {
        if (cU == null) {
            StringBuffer stringBuffer = new StringBuffer();
            String str = Build.VERSION.RELEASE;
            if (str.length() > 0) {
                stringBuffer.append(str);
            } else {
                stringBuffer.append("1.0");
            }
            stringBuffer.append("; ");
            Locale locale = Locale.getDefault();
            String language = locale.getLanguage();
            if (language != null) {
                stringBuffer.append(language.toLowerCase());
                String country = locale.getCountry();
                if (country != null) {
                    stringBuffer.append("-");
                    stringBuffer.append(country.toLowerCase());
                }
            } else {
                stringBuffer.append("en");
            }
            String str2 = Build.DEVICE;
            if (str2.length() > 0) {
                stringBuffer.append("; ");
                stringBuffer.append(str2);
            }
            cU = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2", stringBuffer);
        }
        return cU;
    }

    public static String k(Context context) {
        boolean z;
        if (ik == null) {
            if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") != -1) {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                if (telephonyManager != null) {
                    String deviceId = telephonyManager.getDeviceId();
                    ik = deviceId;
                    if (deviceId == null) {
                        z = false;
                    } else {
                        int length = deviceId.length();
                        int i = 0;
                        while (true) {
                            if (i >= length) {
                                z = true;
                                break;
                            } else if (!Character.isDigit(deviceId.charAt(i))) {
                                z = false;
                                break;
                            } else {
                                i++;
                            }
                        }
                    }
                    if (!z) {
                        ik = Settings.Secure.getString(context.getContentResolver(), "android_id");
                    }
                    if (TextUtils.isEmpty(ik)) {
                        ik = "000000000000000";
                    }
                    is = "000000000000000".equals(ik) ? 1 : 0;
                } else {
                    is = 1;
                }
            } else if (dS) {
                k("Cannot get a user ID.  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.READ_PHONE_STATE\" />");
            }
        }
        return ik;
    }

    protected static void k(String str) {
        Log.e("WiYun", str);
        throw new IllegalArgumentException(str);
    }

    public static boolean l(Context context) {
        if (is == -1) {
            k(context);
        }
        if (is == -1) {
            is = "sdk".equalsIgnoreCase(Build.MODEL) ? 1 : 0;
        }
        return is != 0;
    }

    static String o(Context context) {
        if (TextUtils.isEmpty(dz)) {
            Configuration configuration = context.getResources().getConfiguration();
            dz = String.valueOf(configuration.mnc + (configuration.mcc * 100));
        }
        return dz;
    }

    public static int v(Context context) {
        if (w(context)) {
            return 4;
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            return 0;
        }
        int networkType = telephonyManager.getNetworkType();
        return (networkType == 1 || networkType == 2 || networkType == 0) ? 2 : 3;
    }

    static boolean w(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") != 0) {
            return false;
        }
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        if (wifiManager == null) {
            return false;
        }
        WifiInfo connectionInfo = wifiManager.getConnectionInfo();
        return connectionInfo != null && connectionInfo.getSupplicantState() == SupplicantState.COMPLETED;
    }
}
