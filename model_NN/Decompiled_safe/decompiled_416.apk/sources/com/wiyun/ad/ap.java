package com.wiyun.ad;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

final class ap extends Animation {
    private final float BZ;
    private final float Ca;
    private final float Cb;
    private Camera Cc;
    private boolean cS;
    private final boolean dT;
    private final float qz;
    private final float zX;

    public ap(float f, float f2, float f3, float f4, float f5, boolean z, boolean z2) {
        this.qz = f;
        this.BZ = f2;
        this.zX = f3;
        this.Ca = f4;
        this.Cb = f5;
        this.dT = z;
        this.cS = z2;
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f, Transformation transformation) {
        float f2 = this.qz;
        float f3 = f2 + ((this.BZ - f2) * f);
        float f4 = this.zX;
        float f5 = this.Ca;
        Camera camera = this.Cc;
        Matrix matrix = transformation.getMatrix();
        camera.save();
        if (this.dT) {
            camera.translate(0.0f, 0.0f, this.Cb * f);
        } else {
            camera.translate(0.0f, 0.0f, this.Cb * (1.0f - f));
        }
        if (this.cS) {
            camera.rotateY(f3);
        } else {
            camera.rotateX(f3);
        }
        camera.getMatrix(matrix);
        camera.restore();
        matrix.preTranslate(-f4, -f5);
        matrix.postTranslate(f4, f5);
    }

    public final void initialize(int i, int i2, int i3, int i4) {
        super.initialize(i, i2, i3, i4);
        this.Cc = new Camera();
    }
}
