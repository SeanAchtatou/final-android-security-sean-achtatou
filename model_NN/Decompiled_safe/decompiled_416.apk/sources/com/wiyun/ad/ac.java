package com.wiyun.ad;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import java.io.ByteArrayInputStream;
import java.util.Locale;
import java.util.ResourceBundle;

final class ac {
    private static String cU;
    private static String dz = Locale.getDefault().getCountry();

    ac() {
    }

    public static Bitmap a(Resources resources, int i) {
        try {
            return BitmapFactory.decodeResource(resources, i);
        } catch (OutOfMemoryError e) {
            return null;
        }
    }

    public static Drawable a(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        Bitmap bitmap;
        Bitmap bitmap2;
        Bitmap bitmap3;
        Bitmap decodeByteArray;
        try {
            Bitmap decodeByteArray2 = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
            try {
                bitmap2 = BitmapFactory.decodeByteArray(bArr2, 0, bArr2.length);
                try {
                    decodeByteArray = BitmapFactory.decodeByteArray(bArr3, 0, bArr3.length);
                } catch (OutOfMemoryError e) {
                    bitmap = decodeByteArray2;
                    bitmap3 = null;
                }
                try {
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(decodeByteArray2);
                    BitmapDrawable bitmapDrawable2 = new BitmapDrawable(bitmap2);
                    BitmapDrawable bitmapDrawable3 = new BitmapDrawable(decodeByteArray);
                    StateListDrawable stateListDrawable = new StateListDrawable();
                    stateListDrawable.addState(new int[]{16842919}, bitmapDrawable3);
                    stateListDrawable.addState(new int[]{16842908}, bitmapDrawable2);
                    stateListDrawable.addState(new int[]{16842913}, bitmapDrawable2);
                    stateListDrawable.addState(new int[]{16842919, 16842909}, bitmapDrawable3);
                    stateListDrawable.addState(new int[]{16842908, 16842909}, bitmapDrawable2);
                    stateListDrawable.addState(new int[]{16842913, 16842909}, bitmapDrawable2);
                    stateListDrawable.addState(new int[0], bitmapDrawable);
                    return stateListDrawable;
                } catch (OutOfMemoryError e2) {
                    Bitmap bitmap4 = decodeByteArray;
                    bitmap = decodeByteArray2;
                    bitmap3 = bitmap4;
                    bitmap.recycle();
                    bitmap2.recycle();
                    bitmap3.recycle();
                    return null;
                }
            } catch (OutOfMemoryError e3) {
                bitmap2 = null;
                bitmap = decodeByteArray2;
                bitmap3 = null;
                bitmap.recycle();
                bitmap2.recycle();
                bitmap3.recycle();
                return null;
            }
        } catch (OutOfMemoryError e4) {
            bitmap3 = null;
            bitmap2 = null;
            bitmap = null;
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
            }
            if (bitmap2 != null && !bitmap2.isRecycled()) {
                bitmap2.recycle();
            }
            if (bitmap3 != null && !bitmap3.isRecycled()) {
                bitmap3.recycle();
            }
            return null;
        }
    }

    public static String az(String str) {
        return ResourceBundle.getBundle("com.wiyun.ad.sdk").getString(str);
    }

    public static Bitmap d(byte[] bArr, int i) {
        try {
            return BitmapFactory.decodeByteArray(bArr, 0, i);
        } catch (OutOfMemoryError e) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0017, code lost:
        if (r0 != false) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String fO() {
        /*
            java.lang.String r0 = com.wiyun.ad.ac.cU
            if (r0 == 0) goto L_0x0019
            java.util.Locale r0 = java.util.Locale.getDefault()
            java.lang.String r0 = r0.getCountry()
            java.lang.String r1 = com.wiyun.ad.ac.dz
            boolean r1 = r1.equals(r0)
            if (r1 != 0) goto L_0x002a
            com.wiyun.ad.ac.dz = r0
            r0 = 1
        L_0x0017:
            if (r0 == 0) goto L_0x0027
        L_0x0019:
            java.lang.String r0 = "com.wiyun.ad.sdk"
            java.util.ResourceBundle r0 = java.util.ResourceBundle.getBundle(r0)
            java.lang.String r1 = "loading_ad"
            java.lang.String r0 = r0.getString(r1)
            com.wiyun.ad.ac.cU = r0
        L_0x0027:
            java.lang.String r0 = com.wiyun.ad.ac.cU
            return r0
        L_0x002a:
            r0 = 0
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.ad.ac.fO():java.lang.String");
    }

    public static Drawable fP() {
        try {
            return Drawable.createFromStream(new ByteArrayInputStream(ai.Aq), "wy_dialog_bg");
        } catch (Exception e) {
            return null;
        }
    }

    public static Drawable fQ() {
        try {
            return Drawable.createFromStream(new ByteArrayInputStream(ai.Ar), "wy_progress_dialog_bg");
        } catch (Exception e) {
            return null;
        }
    }

    public static Drawable fR() {
        Bitmap bitmap;
        Bitmap bitmap2;
        Bitmap bitmap3;
        try {
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(ai.zQ, 0, ai.zQ.length);
            try {
                bitmap2 = BitmapFactory.decodeByteArray(ai.zP, 0, ai.zP.length);
                try {
                    Bitmap decodeByteArray2 = BitmapFactory.decodeByteArray(ai.zR, 0, ai.zR.length);
                    try {
                        BitmapDrawable bitmapDrawable = new BitmapDrawable(decodeByteArray);
                        BitmapDrawable bitmapDrawable2 = new BitmapDrawable(bitmap2);
                        BitmapDrawable bitmapDrawable3 = new BitmapDrawable(decodeByteArray2);
                        StateListDrawable stateListDrawable = new StateListDrawable();
                        stateListDrawable.addState(new int[]{16842919}, bitmapDrawable3);
                        stateListDrawable.addState(new int[]{16842908}, bitmapDrawable2);
                        stateListDrawable.addState(new int[]{16842913}, bitmapDrawable2);
                        stateListDrawable.addState(new int[]{16842919, 16842909}, bitmapDrawable3);
                        stateListDrawable.addState(new int[]{16842908, 16842909}, bitmapDrawable2);
                        stateListDrawable.addState(new int[]{16842913, 16842909}, bitmapDrawable2);
                        stateListDrawable.addState(new int[0], bitmapDrawable);
                        return stateListDrawable;
                    } catch (OutOfMemoryError e) {
                        Bitmap bitmap4 = decodeByteArray2;
                        bitmap = decodeByteArray;
                        bitmap3 = bitmap4;
                        if (bitmap != null && !bitmap.isRecycled()) {
                            bitmap.recycle();
                        }
                        if (bitmap2 != null && !bitmap2.isRecycled()) {
                            bitmap2.recycle();
                        }
                        if (bitmap3 != null && !bitmap3.isRecycled()) {
                            bitmap3.recycle();
                        }
                        return null;
                    }
                } catch (OutOfMemoryError e2) {
                    bitmap = decodeByteArray;
                    bitmap3 = null;
                }
            } catch (OutOfMemoryError e3) {
                bitmap2 = null;
                bitmap = decodeByteArray;
                bitmap3 = null;
            }
        } catch (OutOfMemoryError e4) {
            bitmap3 = null;
            bitmap2 = null;
            bitmap = null;
        }
    }
}
