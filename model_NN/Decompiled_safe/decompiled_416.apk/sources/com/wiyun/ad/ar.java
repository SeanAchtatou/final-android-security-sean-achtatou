package com.wiyun.ad;

import java.io.UnsupportedEncodingException;

public final class ar {
    public static byte[] m(String str, String str2) {
        if (str == null) {
            return null;
        }
        try {
            return str.getBytes(str2);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(String.valueOf(str2) + ": " + e);
        }
    }
}
