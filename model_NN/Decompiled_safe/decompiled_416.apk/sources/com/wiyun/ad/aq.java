package com.wiyun.ad;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;

final class aq extends BroadcastReceiver {
    private /* synthetic */ ag Al;

    aq(ag agVar) {
        this.Al = agVar;
    }

    public final void onReceive(Context context, Intent intent) {
        NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
        boolean z = networkInfo != null && networkInfo.isConnected();
        if (this.Al.dx != z) {
            this.Al.dx = z;
        }
    }
}
