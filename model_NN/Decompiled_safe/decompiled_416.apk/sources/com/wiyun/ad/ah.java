package com.wiyun.ad;

import android.content.Context;
import android.net.Proxy;
import android.text.TextUtils;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;

final class ah {
    private static final Map Ao = new HashMap();
    static boolean Ap;
    private static int pb = 10000;

    ah() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0067, code lost:
        if (android.text.TextUtils.isEmpty(r0) == false) goto L_0x0069;
     */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x033f A[Catch:{ Throwable -> 0x0369, all -> 0x0530 }] */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x037b  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043 A[Catch:{ Throwable -> 0x0369, all -> 0x0530 }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0051 A[Catch:{ Throwable -> 0x0369, all -> 0x0530 }] */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x0526 A[Catch:{ Throwable -> 0x0369, all -> 0x0530 }] */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x053b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x007c A[Catch:{ Throwable -> 0x0369, all -> 0x0530 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01b1 A[Catch:{ Throwable -> 0x0369, all -> 0x0530 }] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0316  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0339 A[SYNTHETIC, Splitter:B:99:0x0339] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.wiyun.ad.ao a(android.content.Context r13, com.wiyun.ad.AdView r14) {
        /*
            r0 = 0
            java.lang.String r1 = r14.bF()
            java.lang.String r1 = com.wiyun.ad.x.az(r1)
            boolean r2 = r14.bG()
            if (r2 == 0) goto L_0x0016
            int r2 = r14.bH()
            switch(r2) {
                case 1: goto L_0x031e;
                case 2: goto L_0x0327;
                case 3: goto L_0x0330;
                default: goto L_0x0016;
            }
        L_0x0016:
            r2 = r0
        L_0x0017:
            java.lang.String r0 = "android.permission.INTERNET"
            int r0 = r13.checkCallingOrSelfPermission(r0)
            r3 = -1
            if (r0 != r3) goto L_0x0025
            java.lang.String r0 = "Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />"
            com.wiyun.ad.af.k(r0)
        L_0x0025:
            r3 = 0
            org.apache.http.impl.client.DefaultHttpClient r4 = fY()
            java.io.File r5 = new java.io.File     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.io.File r0 = r13.getCacheDir()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = "wiyun_last_ad_json"
            r5.<init>(r0, r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.util.Map r0 = com.wiyun.ad.ah.Ao     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = r14.bF()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.Object r0 = r0.get(r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r0 != 0) goto L_0x0339
            r0 = -1
        L_0x0044:
            android.content.res.Resources r6 = r13.getResources()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            android.content.res.Configuration r6 = r6.getConfiguration()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            int r6 = r6.orientation     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r7 = -1
            if (r0 == r7) goto L_0x033f
            if (r0 == r6) goto L_0x034c
            boolean r0 = r5.exists()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r0 == 0) goto L_0x0555
            byte[] r0 = a(r5)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r0 == 0) goto L_0x0555
            java.lang.String r0 = e(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            boolean r7 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r7 != 0) goto L_0x0555
        L_0x0069:
            java.util.Map r2 = com.wiyun.ad.ah.Ao     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r7 = r14.bF()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r2.put(r7, r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x0076:
            boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r2 == 0) goto L_0x01ab
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = "http://d.wiyun.com/adv/d"
            r2.<init>(r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r6 = 63
            java.lang.StringBuilder r6 = r2.append(r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r7 = "t"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r7 = "="
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r6.append(r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = "a"
            a(r2, r6, r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = "r"
            java.lang.String r7 = r14.bF()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            a(r2, r6, r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = "s"
            java.lang.String r7 = "1.0.11"
            a(r2, r6, r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            android.content.res.Resources r6 = r13.getResources()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            android.util.DisplayMetrics r6 = r6.getDisplayMetrics()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r7 = "h"
            int r8 = r6.heightPixels     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            a(r2, r7, r8)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r7 = "w"
            int r8 = r6.widthPixels     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            a(r2, r7, r8)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            int r7 = com.wiyun.ad.am.h(r14)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r7 > 0) goto L_0x00cf
            int r7 = r6.widthPixels     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x00cf:
            float r8 = (float) r7     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            float r6 = r6.density     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            float r6 = r8 / r6
            int r6 = (int) r6     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r8 = "size"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r9.<init>()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.StringBuilder r6 = r9.append(r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r9 = "x50,"
            java.lang.StringBuilder r6 = r6.append(r9)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r7 = "x75"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            a(r2, r8, r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = "o"
            boolean r7 = com.wiyun.ad.af.l(r13)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r7 == 0) goto L_0x034f
            java.lang.String r7 = "Android Emulator"
        L_0x0101:
            a(r2, r6, r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = "v"
            java.lang.String r7 = android.os.Build.VERSION.RELEASE     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            a(r2, r6, r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = "b"
            java.lang.String r7 = android.os.Build.BRAND     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            a(r2, r6, r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = "m"
            java.lang.String r7 = android.os.Build.MODEL     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            a(r2, r6, r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = "u"
            java.lang.String r7 = com.wiyun.ad.af.k(r13)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            a(r2, r6, r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = "n"
            int r7 = com.wiyun.ad.af.v(r13)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            a(r2, r6, r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = "f"
            r7 = 0
            a(r2, r6, r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = "l"
            java.util.Locale r7 = java.util.Locale.getDefault()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r7 = r7.getLanguage()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            a(r2, r6, r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = "c"
            java.lang.String r7 = com.wiyun.ad.af.o(r13)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            a(r2, r6, r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = "mm"
            java.lang.String r7 = r14.aK()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            a(r2, r6, r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            boolean r6 = r14.bG()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r6 != 0) goto L_0x0166
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r1 != 0) goto L_0x0166
            boolean r1 = com.wiyun.ad.ah.Ap     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r1 != 0) goto L_0x0166
            java.lang.String r1 = "e"
            r6 = 1
            a(r2, r1, r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x0166:
            java.lang.String r1 = r14.bI()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r1 == 0) goto L_0x0179
            java.lang.String r1 = "k"
            java.lang.String r6 = r14.bI()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            a(r2, r1, r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x0179:
            org.apache.http.client.methods.HttpGet r1 = new org.apache.http.client.methods.HttpGet     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r2 = "User-Agent"
            java.lang.String r6 = com.wiyun.ad.af.fO()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r1.setHeader(r2, r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            org.apache.http.HttpResponse r1 = r4.execute(r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            org.apache.http.StatusLine r2 = r1.getStatusLine()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            int r2 = r2.getStatusCode()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r6 = 300(0x12c, float:4.2E-43)
            if (r2 >= r6) goto L_0x0353
            org.apache.http.HttpEntity r0 = r1.getEntity()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = a(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r1 = 1
            com.wiyun.ad.ah.Ap = r1     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x01a6:
            if (r0 == 0) goto L_0x01ab
            a(r5, r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x01ab:
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r1 != 0) goto L_0x0526
            com.wiyun.ad.ab r1 = new com.wiyun.ad.ab     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            b.a.d r2 = new b.a.d     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r2.<init>(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = "p"
            java.lang.String r0 = r1.O(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r2 = "q"
            java.lang.String r2 = r1.O(r2)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r5 = "a"
            int r5 = r1.L(r5)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r6 = "m"
            java.lang.String r6 = r1.O(r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r7 = "z"
            java.lang.String r7 = r1.O(r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r7 = com.wiyun.ad.al.az(r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r8 = "ra"
            java.lang.String r8 = r1.O(r8)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r9 = "c"
            java.lang.String r9 = r1.O(r9)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r9 = com.wiyun.ad.al.az(r9)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            boolean r10 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r10 != 0) goto L_0x054f
            boolean r10 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r10 != 0) goto L_0x054f
            boolean r10 = android.text.TextUtils.isEmpty(r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r10 != 0) goto L_0x054f
            boolean r10 = android.text.TextUtils.isEmpty(r8)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r10 != 0) goto L_0x054f
            boolean r10 = android.text.TextUtils.isEmpty(r9)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r10 != 0) goto L_0x054f
            r10 = 2
            if (r5 == r10) goto L_0x0216
            java.lang.String r10 = "application/x-search"
            boolean r10 = r10.equals(r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r10 != 0) goto L_0x054f
        L_0x0216:
            java.lang.String r10 = "application/x-search"
            boolean r10 = r10.equals(r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r10 == 0) goto L_0x0227
            java.lang.String r10 = "%query%"
            int r10 = r9.indexOf(r10)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r11 = -1
            if (r10 == r11) goto L_0x054f
        L_0x0227:
            java.lang.String r10 = "application/x-map"
            boolean r10 = r10.equals(r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r10 == 0) goto L_0x0247
            java.lang.String r10 = "addr://"
            boolean r10 = r9.startsWith(r10)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r10 != 0) goto L_0x0247
            java.lang.String r10 = "http://"
            boolean r10 = r9.startsWith(r10)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r10 != 0) goto L_0x0247
            java.lang.String r10 = "loc://"
            boolean r10 = r9.startsWith(r10)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r10 == 0) goto L_0x054f
        L_0x0247:
            com.wiyun.ad.ao r10 = new com.wiyun.ad.ao     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.<init>()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.cU = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.dz = r2     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.is = r5     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = "i"
            java.lang.String r0 = r1.O(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.BY = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = "t"
            java.lang.String r0 = r1.O(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.zY = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = "st"
            java.lang.String r0 = r1.O(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.BT = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = "sh"
            java.lang.String r0 = r1.O(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.cR = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.kI = r7     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.kJ = r9     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = "b"
            java.lang.String r0 = r1.O(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.BU = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = "h"
            int r0 = r1.L(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.Ag = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = "w"
            int r0 = r1.L(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.kG = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.BR = r8     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.BV = r6     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = "bc"
            java.lang.String r0 = r1.O(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r2 != 0) goto L_0x02ac
            r2 = 1
            java.lang.String r0 = r0.substring(r2)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r2 = 16
            long r5 = java.lang.Long.parseLong(r0, r2)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            int r0 = (int) r5     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.kE = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x02ac:
            int r0 = r10.kE     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r2 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0 = r0 & r2
            if (r0 != 0) goto L_0x02ba
            int r0 = r10.kE     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r2 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0 = r0 | r2
            r10.kE = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x02ba:
            java.lang.String r0 = "tc"
            java.lang.String r0 = r1.O(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r2 != 0) goto L_0x02d4
            r2 = 1
            java.lang.String r0 = r0.substring(r2)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r2 = 16
            long r5 = java.lang.Long.parseLong(r0, r2)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            int r0 = (int) r5     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.kD = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x02d4:
            int r0 = r10.kD     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r2 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0 = r0 & r2
            if (r0 != 0) goto L_0x02e2
            int r0 = r10.kD     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r2 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0 = r0 | r2
            r10.kD = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x02e2:
            java.lang.String r0 = "ts"
            int r0 = r1.L(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            float r0 = (float) r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r1 = 1095761920(0x41500000, float:13.0)
            float r0 = java.lang.Math.max(r0, r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.BS = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = r14.bF()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.ik = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = r10.zY     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r0 != 0) goto L_0x0307
            java.lang.String r0 = r10.zY     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = com.wiyun.ad.al.az(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.zY = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x0307:
            int r0 = r10.is     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            switch(r0) {
                case 1: goto L_0x0523;
                case 2: goto L_0x0383;
                case 3: goto L_0x048f;
                default: goto L_0x030c;
            }
        L_0x030c:
            r13 = r3
        L_0x030d:
            org.apache.http.conn.ClientConnectionManager r14 = r4.getConnectionManager()
            r14.shutdown()
            if (r13 != 0) goto L_0x031d
            java.lang.String r14 = "WiYun"
            java.lang.String r0 = "Failed to get ad"
            android.util.Log.w(r14, r0)
        L_0x031d:
            return r13
        L_0x031e:
            java.lang.String r0 = "test_text_ad"
            java.lang.String r0 = com.wiyun.ad.ac.az(r0)
            r2 = r0
            goto L_0x0017
        L_0x0327:
            java.lang.String r0 = "test_banner_ad"
            java.lang.String r0 = com.wiyun.ad.ac.az(r0)
            r2 = r0
            goto L_0x0017
        L_0x0330:
            java.lang.String r0 = "test_fullscreen_ad"
            java.lang.String r0 = com.wiyun.ad.ac.az(r0)
            r2 = r0
            goto L_0x0017
        L_0x0339:
            int r0 = r0.intValue()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            goto L_0x0044
        L_0x033f:
            java.util.Map r0 = com.wiyun.ad.ah.Ao     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r7 = r14.bF()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r0.put(r7, r6)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x034c:
            r0 = r2
            goto L_0x0076
        L_0x034f:
            java.lang.String r7 = "Android"
            goto L_0x0101
        L_0x0353:
            java.lang.String r1 = "WiYun"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r7 = "Failed to get ad, statusCode: "
            r6.<init>(r7)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.StringBuilder r2 = r6.append(r2)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            android.util.Log.w(r1, r2)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            goto L_0x01a6
        L_0x0369:
            r13 = move-exception
            r13 = r3
        L_0x036b:
            java.lang.String r14 = "WiYun"
            java.lang.String r0 = "Failed to get ad"
            android.util.Log.w(r14, r0)     // Catch:{ all -> 0x0546 }
            org.apache.http.conn.ClientConnectionManager r14 = r4.getConnectionManager()
            r14.shutdown()
            if (r13 != 0) goto L_0x031d
            java.lang.String r14 = "WiYun"
            java.lang.String r0 = "Failed to get ad"
            android.util.Log.w(r14, r0)
            goto L_0x031d
        L_0x0383:
            boolean r14 = r14.bG()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r14 != 0) goto L_0x0474
            java.lang.String r14 = r10.BY     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            boolean r14 = android.text.TextUtils.isEmpty(r14)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r14 != 0) goto L_0x03f8
            java.io.File r14 = new java.io.File     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            java.io.File r0 = r13.getCacheDir()     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            java.lang.String r1 = r10.BY     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            byte[] r1 = com.wiyun.ad.aj.ba(r1)     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            java.lang.String r1 = com.wiyun.ad.aj.g(r1)     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            r14.<init>(r0, r1)     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            byte[] r14 = a(r14)     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            if (r14 == 0) goto L_0x03b1
            int r0 = r14.length     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            android.graphics.Bitmap r14 = com.wiyun.ad.ac.d(r14, r0)     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            r10.BX = r14     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
        L_0x03b1:
            android.graphics.Bitmap r14 = r10.BX     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            if (r14 != 0) goto L_0x0552
            org.apache.http.client.methods.HttpGet r14 = new org.apache.http.client.methods.HttpGet     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            java.lang.String r0 = r10.BY     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            java.lang.String r0 = az(r0)     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            r14.<init>(r0)     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            org.apache.http.HttpResponse r14 = r4.execute(r14)     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            org.apache.http.StatusLine r0 = r14.getStatusLine()     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            int r0 = r0.getStatusCode()     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            r1 = 300(0x12c, float:4.2E-43)
            if (r0 >= r1) goto L_0x0552
            byte[] r14 = a(r14)     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            int r0 = r14.length     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            android.graphics.Bitmap r0 = com.wiyun.ad.ac.d(r14, r0)     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            r10.BX = r0     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            android.graphics.Bitmap r0 = r10.BX     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            if (r0 == 0) goto L_0x0552
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            java.io.File r13 = r13.getCacheDir()     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            java.lang.String r1 = r10.BY     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            byte[] r1 = com.wiyun.ad.aj.ba(r1)     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            java.lang.String r1 = com.wiyun.ad.aj.g(r1)     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            r0.<init>(r13, r1)     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            a(r0, r14)     // Catch:{ Throwable -> 0x054b, all -> 0x0543 }
            r13 = r10
            goto L_0x030d
        L_0x03f8:
            java.lang.String r14 = r10.BU     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            boolean r14 = android.text.TextUtils.isEmpty(r14)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r14 != 0) goto L_0x054f
            java.io.File r14 = new java.io.File     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.io.File r0 = r13.getCacheDir()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r1 = r10.BU     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            byte[] r1 = com.wiyun.ad.aj.ba(r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r1 = com.wiyun.ad.aj.g(r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r14.<init>(r0, r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            byte[] r14 = a(r14)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r14 == 0) goto L_0x0420
            int r0 = r14.length     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            android.graphics.Bitmap r14 = com.wiyun.ad.ac.d(r14, r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.BW = r14     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x0420:
            android.graphics.Bitmap r14 = r10.BW     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r14 != 0) goto L_0x0464
            org.apache.http.client.methods.HttpGet r14 = new org.apache.http.client.methods.HttpGet     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = r10.BU     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = az(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r14.<init>(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            org.apache.http.HttpResponse r14 = r4.execute(r14)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            org.apache.http.StatusLine r0 = r14.getStatusLine()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            int r0 = r0.getStatusCode()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r1 = 300(0x12c, float:4.2E-43)
            if (r0 >= r1) goto L_0x0464
            byte[] r14 = a(r14)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            int r0 = r14.length     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            android.graphics.Bitmap r0 = com.wiyun.ad.ac.d(r14, r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.BW = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            android.graphics.Bitmap r0 = r10.BW     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r0 == 0) goto L_0x0464
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.io.File r13 = r13.getCacheDir()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r1 = r10.BU     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            byte[] r1 = com.wiyun.ad.aj.ba(r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r1 = com.wiyun.ad.aj.g(r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r0.<init>(r13, r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            a(r0, r14)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x0464:
            android.graphics.Bitmap r13 = r10.BW     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r13 == 0) goto L_0x054f
            r13 = 0
            r10.BX = r13     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r13 = 0
            r10.BY = r13     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r13 = 0
            r10.zY = r13     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r13 = r10
            goto L_0x030d
        L_0x0474:
            android.content.res.Resources r14 = r13.getResources()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = "icon"
            java.lang.String r1 = "drawable"
            java.lang.String r13 = r13.getPackageName()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            int r13 = r14.getIdentifier(r0, r1, r13)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r13 == 0) goto L_0x048c
            android.graphics.Bitmap r13 = com.wiyun.ad.ac.a(r14, r13)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.BX = r13     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x048c:
            r13 = r10
            goto L_0x030d
        L_0x048f:
            boolean r14 = r14.bG()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r14 != 0) goto L_0x0508
            java.lang.String r14 = r10.BU     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            boolean r14 = android.text.TextUtils.isEmpty(r14)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r14 != 0) goto L_0x054f
            java.io.File r14 = new java.io.File     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.io.File r0 = r13.getCacheDir()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r1 = r10.BU     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            byte[] r1 = com.wiyun.ad.aj.ba(r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r1 = com.wiyun.ad.aj.g(r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r14.<init>(r0, r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            byte[] r14 = a(r14)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r14 == 0) goto L_0x04bd
            int r0 = r14.length     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            android.graphics.Bitmap r14 = com.wiyun.ad.ac.d(r14, r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.BW = r14     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x04bd:
            android.graphics.Bitmap r14 = r10.BW     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r14 != 0) goto L_0x0501
            org.apache.http.client.methods.HttpGet r14 = new org.apache.http.client.methods.HttpGet     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = r10.BU     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = az(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r14.<init>(r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            org.apache.http.HttpResponse r14 = r4.execute(r14)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            org.apache.http.StatusLine r0 = r14.getStatusLine()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            int r0 = r0.getStatusCode()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r1 = 300(0x12c, float:4.2E-43)
            if (r0 >= r1) goto L_0x0501
            byte[] r14 = a(r14)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            int r0 = r14.length     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            android.graphics.Bitmap r0 = com.wiyun.ad.ac.d(r14, r0)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.BW = r0     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            android.graphics.Bitmap r0 = r10.BW     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r0 == 0) goto L_0x0501
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.io.File r13 = r13.getCacheDir()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r1 = r10.BU     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            byte[] r1 = com.wiyun.ad.aj.ba(r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r1 = com.wiyun.ad.aj.g(r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r0.<init>(r13, r1)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            a(r0, r14)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x0501:
            android.graphics.Bitmap r13 = r10.BW     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r13 == 0) goto L_0x054f
            r13 = r10
            goto L_0x030d
        L_0x0508:
            android.content.res.Resources r14 = r13.getResources()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            java.lang.String r0 = "icon"
            java.lang.String r1 = "drawable"
            java.lang.String r13 = r13.getPackageName()     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            int r13 = r14.getIdentifier(r0, r1, r13)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            if (r13 == 0) goto L_0x0520
            android.graphics.Bitmap r13 = com.wiyun.ad.ac.a(r14, r13)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r10.BW = r13     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
        L_0x0520:
            r13 = r10
            goto L_0x030d
        L_0x0523:
            r13 = r10
            goto L_0x030d
        L_0x0526:
            java.lang.String r13 = "WiYun"
            java.lang.String r14 = "Failed to get ad"
            android.util.Log.w(r13, r14)     // Catch:{ Throwable -> 0x0369, all -> 0x0530 }
            r13 = r3
            goto L_0x030d
        L_0x0530:
            r13 = move-exception
            r14 = r3
        L_0x0532:
            org.apache.http.conn.ClientConnectionManager r0 = r4.getConnectionManager()
            r0.shutdown()
            if (r14 != 0) goto L_0x0542
            java.lang.String r14 = "WiYun"
            java.lang.String r0 = "Failed to get ad"
            android.util.Log.w(r14, r0)
        L_0x0542:
            throw r13
        L_0x0543:
            r13 = move-exception
            r14 = r10
            goto L_0x0532
        L_0x0546:
            r14 = move-exception
            r12 = r14
            r14 = r13
            r13 = r12
            goto L_0x0532
        L_0x054b:
            r13 = move-exception
            r13 = r10
            goto L_0x036b
        L_0x054f:
            r13 = r3
            goto L_0x030d
        L_0x0552:
            r13 = r10
            goto L_0x030d
        L_0x0555:
            r0 = r2
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.ad.ah.a(android.content.Context, com.wiyun.ad.AdView):com.wiyun.ad.ao");
    }

    private static String a(HttpEntity httpEntity) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr = new byte[2048];
            InputStream content = httpEntity.getContent();
            for (int i = 0; i != -1; i = content.read(bArr)) {
                byteArrayOutputStream.write(bArr, 0, i);
            }
            return e(byteArrayOutputStream.toByteArray());
        } catch (Exception e) {
            return "";
        }
    }

    static void a(Context context, ao aoVar) {
        if (aoVar != null && !TextUtils.isEmpty(aoVar.kI)) {
            if (context.checkCallingOrSelfPermission("android.permission.INTERNET") == -1) {
                af.k("Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />");
            }
            DefaultHttpClient fY = fY();
            try {
                StringBuilder sb = new StringBuilder(aoVar.kI);
                sb.append('?').append("r").append("=").append(aoVar.ik);
                a(sb, "p", aoVar.cU);
                a(sb, "q", aoVar.dz);
                a(sb, "ra", aoVar.BR);
                fY.execute(new HttpGet(sb.toString()));
            } catch (Exception e) {
                Log.w("WiYun", "Failed to record ad click", e);
            } finally {
                fY.getConnectionManager().shutdown();
            }
        }
    }

    private static void a(StringBuilder sb, String str, int i) {
        sb.append("&").append(str).append("=").append(i);
    }

    private static void a(StringBuilder sb, String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            sb.append("&").append(str).append("=").append(URLEncoder.encode(str2, "UTF-8"));
        }
    }

    private static boolean a(File file, String str) {
        try {
            a(file, str.getBytes("utf-8"));
            return true;
        } catch (UnsupportedEncodingException e) {
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0046 A[SYNTHETIC, Splitter:B:19:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0051 A[SYNTHETIC, Splitter:B:25:0x0051] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(java.io.File r5, byte[] r6) {
        /*
            java.io.File r0 = r5.getParentFile()
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x0011
            java.io.File r0 = r5.getParentFile()
            r0.mkdirs()
        L_0x0011:
            r0 = 0
            boolean r1 = r5.exists()     // Catch:{ IOException -> 0x002b, all -> 0x004b }
            if (r1 != 0) goto L_0x001b
            r5.createNewFile()     // Catch:{ IOException -> 0x002b, all -> 0x004b }
        L_0x001b:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x002b, all -> 0x004b }
            r1.<init>(r5)     // Catch:{ IOException -> 0x002b, all -> 0x004b }
            r1.write(r6)     // Catch:{ IOException -> 0x0062, all -> 0x005b }
            r1.flush()     // Catch:{ IOException -> 0x0062, all -> 0x005b }
            r1.close()     // Catch:{ IOException -> 0x0055 }
        L_0x0029:
            r0 = 1
        L_0x002a:
            return r0
        L_0x002b:
            r1 = move-exception
        L_0x002c:
            java.lang.String r1 = "WiYun"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Failed to cache ad to file: "
            r2.<init>(r3)     // Catch:{ all -> 0x005d }
            java.lang.String r3 = r5.getAbsolutePath()     // Catch:{ all -> 0x005d }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x005d }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x005d }
            android.util.Log.w(r1, r2)     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x0049
            r0.close()     // Catch:{ IOException -> 0x0057 }
        L_0x0049:
            r0 = 0
            goto L_0x002a
        L_0x004b:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x004f:
            if (r1 == 0) goto L_0x0054
            r1.close()     // Catch:{ IOException -> 0x0059 }
        L_0x0054:
            throw r0
        L_0x0055:
            r0 = move-exception
            goto L_0x0029
        L_0x0057:
            r0 = move-exception
            goto L_0x0049
        L_0x0059:
            r1 = move-exception
            goto L_0x0054
        L_0x005b:
            r0 = move-exception
            goto L_0x004f
        L_0x005d:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x004f
        L_0x0062:
            r0 = move-exception
            r0 = r1
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.ad.ah.a(java.io.File, byte[]):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0024 A[SYNTHETIC, Splitter:B:20:0x0024] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] a(java.io.File r4) {
        /*
            r2 = 0
            boolean r0 = r4.exists()     // Catch:{ FileNotFoundException -> 0x0017, all -> 0x0020 }
            if (r0 != 0) goto L_0x0009
            r0 = r2
        L_0x0008:
            return r0
        L_0x0009:
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0017, all -> 0x0020 }
            r0.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0017, all -> 0x0020 }
            byte[] r1 = d(r0)     // Catch:{ FileNotFoundException -> 0x0033, all -> 0x002e }
            r0.close()     // Catch:{ IOException -> 0x0028 }
        L_0x0015:
            r0 = r1
            goto L_0x0008
        L_0x0017:
            r0 = move-exception
            r0 = r2
        L_0x0019:
            if (r0 == 0) goto L_0x001e
            r0.close()     // Catch:{ IOException -> 0x002a }
        L_0x001e:
            r0 = r2
            goto L_0x0008
        L_0x0020:
            r0 = move-exception
            r1 = r2
        L_0x0022:
            if (r1 == 0) goto L_0x0027
            r1.close()     // Catch:{ IOException -> 0x002c }
        L_0x0027:
            throw r0
        L_0x0028:
            r0 = move-exception
            goto L_0x0015
        L_0x002a:
            r0 = move-exception
            goto L_0x001e
        L_0x002c:
            r1 = move-exception
            goto L_0x0027
        L_0x002e:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x0022
        L_0x0033:
            r1 = move-exception
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.ad.ah.a(java.io.File):byte[]");
    }

    private static byte[] a(HttpResponse httpResponse) {
        byte[] bArr = new byte[1024];
        InputStream content = httpResponse.getEntity().getContent();
        if (content == null) {
            return null;
        }
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            for (int i = 0; i != -1; i = content.read(bArr)) {
                byteArrayOutputStream.write(bArr, 0, i);
            }
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            if (content == null) {
                return byteArray;
            }
            try {
                content.close();
                return byteArray;
            } catch (IOException e) {
                return byteArray;
            }
        } catch (Exception e2) {
            if (content != null) {
                try {
                    content.close();
                } catch (IOException e3) {
                }
            }
            return null;
        } catch (Throwable th) {
            if (content != null) {
                try {
                    content.close();
                } catch (IOException e4) {
                }
            }
            throw th;
        }
    }

    private static String az(String str) {
        return str.replace(" ", "%20").replace("[", "%5B").replace("]", "%5D").replace("|", "%7C");
    }

    private static byte[] d(InputStream inputStream) {
        int i = 0;
        byte[] bArr = null;
        if (inputStream != null) {
            try {
                byte[] bArr2 = new byte[1024];
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                while (i != -1) {
                    byteArrayOutputStream.write(bArr2, 0, i);
                    i = inputStream.read(bArr2);
                }
                bArr = byteArrayOutputStream.toByteArray();
                byteArrayOutputStream.flush();
                byteArrayOutputStream.close();
            } catch (Throwable th) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                    }
                }
                throw th;
            }
        }
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e2) {
            }
        }
        return bArr;
    }

    private static String e(byte[] bArr) {
        return bArr == null ? "" : e(bArr, bArr.length);
    }

    private static String e(byte[] bArr, int i) {
        if (bArr == null) {
            return "";
        }
        try {
            return new String(bArr, 0, i, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    private static DefaultHttpClient fY() {
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(basicHttpParams, "UTF-8");
        HttpProtocolParams.setUseExpectContinue(basicHttpParams, false);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, pb);
        HttpConnectionParams.setSoTimeout(basicHttpParams, pb);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
        if (!ag.fX()) {
            if (!TextUtils.isEmpty(Proxy.getDefaultHost())) {
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(Proxy.getDefaultHost(), Proxy.getDefaultPort()));
            }
        }
        return defaultHttpClient;
    }
}
