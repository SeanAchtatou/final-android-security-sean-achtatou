package com.wiyun.ad;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

final class am {
    am() {
    }

    public static int h(AdView adView) {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        int i5 = 0;
        View view = adView;
        while (true) {
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams.width < 0) {
                if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                    i5 += marginLayoutParams.leftMargin;
                    i4 += marginLayoutParams.rightMargin;
                }
                ViewParent parent = view.getParent();
                if (parent instanceof View) {
                    view = (View) parent;
                    continue;
                } else {
                    view = null;
                    continue;
                }
                if (view == null) {
                    i = i5;
                    i2 = i4;
                    i3 = 0;
                    break;
                }
            } else {
                i = i5;
                i2 = i4;
                i3 = layoutParams.width;
                break;
            }
        }
        return Math.max(0, (i3 - i) - i2);
    }
}
