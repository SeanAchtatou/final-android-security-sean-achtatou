package com.wiyun.ad;

import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.LinearLayout;

final class n implements Animation.AnimationListener {
    private /* synthetic */ AdView iP;
    private final /* synthetic */ LinearLayout lC;

    n(AdView adView, LinearLayout linearLayout) {
        this.iP = adView;
        this.lC = linearLayout;
    }

    public final void onAnimationEnd(Animation animation) {
        ViewGroup viewGroup = (ViewGroup) this.lC.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(this.lC);
        }
        this.iP.kN = false;
        this.iP.iZ = null;
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
