package com.wiyun.ad;

import android.view.animation.Animation;

final class g implements Animation.AnimationListener {
    private final /* synthetic */ ae iN;
    private final /* synthetic */ int iO;
    private /* synthetic */ AdView iP;

    g(AdView adView, ae aeVar, int i) {
        this.iP = adView;
        this.iN = aeVar;
        this.iO = i;
    }

    public final void onAnimationEnd(Animation animation) {
        this.iP.post(new d(this.iP, this.iN, this.iO));
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
