package com.wiyun.ad;

import android.view.animation.DecelerateInterpolator;

final class d implements Runnable {
    /* access modifiers changed from: private */
    public ae iN;
    private int iO;
    final /* synthetic */ AdView iP;

    public d(AdView adView, ae aeVar, int i) {
        this.iP = adView;
        this.iN = aeVar;
        this.iO = i;
    }

    public final void run() {
        this.iP.ky.setVisibility(8);
        this.iN.setVisibility(0);
        ap apVar = new ap(90.0f, 0.0f, ((float) this.iP.getWidth()) / 2.0f, ((float) this.iP.getHeight()) / 2.0f, -0.4f * ((float) (this.iO == 2 ? this.iP.getHeight() : this.iP.getWidth())), false, this.iO != 2);
        apVar.setDuration(700);
        apVar.setFillAfter(true);
        apVar.setInterpolator(new DecelerateInterpolator());
        apVar.setAnimationListener(new a(this));
        this.iP.startAnimation(apVar);
    }
}
