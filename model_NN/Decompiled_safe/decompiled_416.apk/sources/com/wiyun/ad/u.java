package com.wiyun.ad;

import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

final class u implements View.OnKeyListener {
    private /* synthetic */ ae ky;
    private final /* synthetic */ h pf;

    u(ae aeVar, h hVar) {
        this.ky = aeVar;
        this.pf = hVar;
    }

    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        switch (keyEvent.getAction()) {
            case 0:
                if (i == 4) {
                    return true;
                }
                break;
            case 1:
                if (i == 4) {
                    ((ViewGroup) this.pf.getParent()).removeView(this.pf);
                    this.ky.kK = false;
                    return true;
                }
                break;
        }
        return false;
    }
}
