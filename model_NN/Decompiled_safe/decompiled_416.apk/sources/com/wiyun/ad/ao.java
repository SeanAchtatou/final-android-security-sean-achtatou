package com.wiyun.ad;

import android.graphics.Bitmap;

final class ao {
    int Ag;
    String BR;
    float BS = 13.0f;
    String BT;
    String BU;
    String BV;
    Bitmap BW;
    Bitmap BX;
    String BY;
    String cR;
    String cU;
    String dz;
    String ik;
    int is;
    int kD;
    int kE;
    int kG;
    String kI;
    String kJ;
    String zY;

    ao() {
    }

    public final void X() {
        if (this.BX != null && !this.BX.isRecycled()) {
            this.BX.recycle();
        }
        this.BX = null;
        if (this.BW != null && !this.BW.isRecycled()) {
            this.BW.recycle();
        }
        this.BW = null;
    }

    public final boolean equals(Object obj) {
        return obj instanceof ao ? ((ao) obj).cU.equals(this.cU) && ((ao) obj).dz.equals(this.dz) : super.equals(obj);
    }

    public final int hashCode() {
        return this.cU.hashCode();
    }
}
