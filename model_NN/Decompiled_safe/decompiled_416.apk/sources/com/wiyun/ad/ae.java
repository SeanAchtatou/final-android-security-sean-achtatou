package com.wiyun.ad;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import java.util.LinkedList;

final class ae extends View {
    private static int vx = 40;
    private static final Typeface zZ = Typeface.create(Typeface.SANS_SERIF, 0);
    private Animation Aa;
    private Transformation Ab = new Transformation();
    private Bitmap Ac;
    private Paint Ad;
    /* access modifiers changed from: private */
    public final ao Ae;
    private String[] Af;
    private int Ag;
    private long Ah;
    private boolean Ai;
    private long Aj;
    private ak Ak;
    private long cQ;
    private boolean dS;
    private int iO = -16777216;
    private int is = -1;
    private int kC;
    /* access modifiers changed from: private */
    public boolean kK;

    public ae(Context context, ao aoVar, ak akVar) {
        super(context);
        this.Ae = aoVar;
        this.Ak = akVar;
        if (aoVar != null) {
            setFocusable(true);
            setClickable(true);
        }
        this.Ad = new Paint();
        this.Ad.setAntiAlias(true);
        this.Ad.setTypeface(zZ);
        this.Ad.setTextSize(fU());
    }

    private int J(int i) {
        return (int) TypedValue.applyDimension(1, (float) i, getResources().getDisplayMetrics());
    }

    private static void a(Canvas canvas, Rect rect, int i) {
        Paint paint = new Paint();
        paint.setColor(i);
        paint.setAntiAlias(true);
        canvas.drawRect(rect, paint);
        paint.setShader(new LinearGradient((float) rect.left, (float) rect.top, (float) rect.left, (float) rect.height(), 1728053247, 0, Shader.TileMode.MIRROR));
        canvas.drawRect(rect, paint);
        paint.setShader(null);
    }

    private static void a(Canvas canvas, Rect rect, Bitmap bitmap) {
        Paint paint = new Paint();
        paint.setFilterBitmap(true);
        canvas.drawBitmap(bitmap, new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), rect, paint);
    }

    private static String[] a(Paint paint, String str, float f) {
        if (str == null) {
            return null;
        }
        LinkedList linkedList = new LinkedList();
        int i = 0;
        float measureText = f - paint.measureText("0");
        char[] charArray = str.toCharArray();
        int i2 = 0;
        int i3 = 0;
        boolean z = true;
        for (int i4 = 0; i4 < charArray.length; i4++) {
            boolean z2 = charArray[i4] == '-' || charArray[i4] == '/';
            boolean z3 = charArray[i4] == ' ';
            boolean z4 = charArray[i4] == 10;
            boolean z5 = z4 || (charArray[i4] == 13);
            float measureText2 = paint.measureText(charArray, i2, i4 - i2);
            if (z5 || measureText2 > measureText) {
                int i5 = z5 ? i4 : i > i2 ? i : i4 - 1;
                linkedList.add(c(str.substring(i2, i5), z));
                if (z5) {
                    int i6 = i5 + 1;
                    if (z4) {
                        linkedList.add(null);
                    }
                    i2 = i6;
                    z = true;
                } else {
                    z = false;
                    i2 = i5;
                }
            }
            if (z2) {
                i = i4 + 1;
            }
            if (z3) {
                i = i3 + 1;
            } else {
                i3 = i4;
            }
        }
        linkedList.add(c(str.substring(i2), z));
        String[] strArr = new String[linkedList.size()];
        linkedList.toArray(strArr);
        return strArr;
    }

    private void aa() {
        boolean z;
        View view;
        String substring;
        String substring2;
        if (this.Ae != null) {
            if (isPressed()) {
                setPressed(false);
            }
            boolean equals = "application/x-search".equals(this.Ae.BV);
            Context applicationContext = getContext().getApplicationContext();
            if (!equals) {
                new t(this, applicationContext).start();
            }
            if (equals) {
                ((AdView) getParent()).X();
            } else if ("text/html".equals(this.Ae.BV) || "application/x-app-store".equals(this.Ae.BV)) {
                Uri parse = Uri.parse(this.Ae.kJ);
                if (!Boolean.parseBoolean(parse.getQueryParameter("mini"))) {
                    Intent intent = new Intent("android.intent.action.VIEW", parse);
                    intent.addFlags(268435456);
                    try {
                        applicationContext.startActivity(intent);
                    } catch (Exception e) {
                        Log.e("WiYun", "Could not open viewer on ad click to " + this.Ae.kJ, e);
                    }
                } else if (!this.kK) {
                    this.kK = true;
                    Activity activity = (Activity) getContext();
                    h hVar = new h(activity);
                    View findViewById = activity.findViewById(16908290);
                    if (findViewById == null) {
                        view = activity.getWindow().getDecorView();
                        z = true;
                    } else {
                        View view2 = findViewById;
                        z = false;
                        view = view2;
                    }
                    int width = view.getWidth();
                    int height = view.getHeight();
                    int J = width - J(20);
                    int J2 = height - J((z ? 50 : 0) + 20);
                    int J3 = J - J(20);
                    int J4 = J2 - J(20);
                    FrameLayout frameLayout = new FrameLayout(activity);
                    frameLayout.setBackgroundDrawable(ac.fP());
                    frameLayout.setPadding(J(10), J(10), J(10), J(10));
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(J, J2);
                    layoutParams.gravity = 1;
                    hVar.addView(frameLayout, layoutParams);
                    WebView webView = new WebView(activity);
                    webView.setScrollBarStyle(33554432);
                    webView.setMapTrackballToArrowKeys(false);
                    webView.getSettings().setJavaScriptEnabled(true);
                    webView.getSettings().setBuiltInZoomControls(true);
                    LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -1);
                    layoutParams2.weight = 1.0f;
                    frameLayout.addView(webView, layoutParams2);
                    LinearLayout linearLayout = new LinearLayout(activity);
                    linearLayout.setId(4096);
                    linearLayout.setOrientation(1);
                    linearLayout.setGravity(17);
                    linearLayout.setClickable(true);
                    linearLayout.setBackgroundDrawable(ac.fQ());
                    linearLayout.setPadding(J(30), J(30), J(30), J(30));
                    ProgressBar progressBar = new ProgressBar(activity);
                    progressBar.setIndeterminate(true);
                    linearLayout.addView(progressBar, new LinearLayout.LayoutParams(J(48), J(48)));
                    FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(-1, -1);
                    layoutParams3.gravity = 17;
                    frameLayout.addView(linearLayout, layoutParams3);
                    Button button = new Button(activity);
                    button.setBackgroundDrawable(ac.fR());
                    FrameLayout.LayoutParams layoutParams4 = new FrameLayout.LayoutParams(J(29), J(29));
                    layoutParams4.gravity = 53;
                    hVar.addView(button, layoutParams4);
                    button.setOnClickListener(new p(this, hVar));
                    hVar.setFocusable(true);
                    hVar.setFocusableInTouchMode(true);
                    hVar.setOnKeyListener(new u(this, hVar));
                    hVar.requestFocus();
                    FrameLayout.LayoutParams layoutParams5 = new FrameLayout.LayoutParams(-2, -2);
                    layoutParams5.gravity = 17;
                    TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 2, 1.0f, 1, 0.0f);
                    translateAnimation.setDuration(500);
                    translateAnimation.setInterpolator(new DecelerateInterpolator());
                    activity.addContentView(hVar, layoutParams5);
                    hVar.startAnimation(translateAnimation);
                    webView.setWebViewClient(new r(this, linearLayout));
                    StringBuffer stringBuffer = new StringBuffer(this.Ae.kJ);
                    stringBuffer.append("&embed=true&width=").append(J3).append("&height=").append(J4);
                    webView.loadUrl(stringBuffer.toString());
                }
            } else if ("text/x-phone-number".equals(this.Ae.BV)) {
                Intent intent2 = getContext().checkCallingOrSelfPermission("android.permission.CALL_PHONE") == 0 ? new Intent("android.intent.action.CALL") : new Intent("android.intent.action.DIAL");
                intent2.addFlags(268435456);
                intent2.setData(Uri.parse("tel:" + this.Ae.kJ));
                try {
                    applicationContext.startActivity(intent2);
                } catch (Exception e2) {
                    Log.e("WiYun", "Could not call phone number " + this.Ae.kJ, e2);
                }
            } else if ("text/x-sms-number".equals(this.Ae.BV)) {
                Intent intent3 = new Intent("android.intent.action.SENDTO");
                intent3.addFlags(268435456);
                intent3.setData(Uri.parse("smsto:" + this.Ae.kJ));
                if (!TextUtils.isEmpty(this.Ae.BT)) {
                    intent3.putExtra("sms_body", this.Ae.BT);
                }
                try {
                    applicationContext.startActivity(intent3);
                } catch (Exception e3) {
                    Log.e("WiYun", "Could not send sms to number " + this.Ae.kJ, e3);
                }
            } else if ("audio/mp3".equals(this.Ae.BV) || "video/3gpp".equals(this.Ae.BV) || "video/mp4".equals(this.Ae.BV)) {
                Intent intent4 = new Intent("android.intent.action.VIEW");
                intent4.addFlags(268435456);
                intent4.setDataAndType(Uri.parse(this.Ae.kJ), this.Ae.BV);
                try {
                    applicationContext.startActivity(intent4);
                } catch (Exception e4) {
                    Log.e("WiYun", "Could not open browser on ad click to " + this.Ae.kJ, e4);
                }
            } else if ("application/x-map".equals(this.Ae.BV)) {
                Uri uri = null;
                if (this.Ae.kJ.startsWith("addr://")) {
                    uri = Uri.parse(String.format("geo:0,0?q=%s", this.Ae.kJ.substring("addr://".length())));
                } else if (this.Ae.kJ.startsWith("loc://")) {
                    int lastIndexOf = this.Ae.kJ.lastIndexOf(64);
                    if (lastIndexOf == -1) {
                        substring = this.Ae.kJ.substring("loc://".length());
                        substring2 = null;
                    } else {
                        substring = this.Ae.kJ.substring("loc://".length(), lastIndexOf);
                        substring2 = this.Ae.kJ.substring(lastIndexOf + 1);
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append("http://maps.google.com/maps?q=");
                    sb.append(substring);
                    if (!TextUtils.isEmpty(substring2)) {
                        sb.append('(').append(substring2).append(')');
                    }
                    uri = Uri.parse(sb.toString());
                } else if (this.Ae.kJ.startsWith("http://")) {
                    uri = Uri.parse(this.Ae.kJ);
                }
                if (uri != null) {
                    Intent intent5 = new Intent("android.intent.action.VIEW", uri);
                    try {
                        if (getContext().getPackageManager().getPackageInfo("com.google.android.apps.maps", 0) != null) {
                            intent5.setComponent(new ComponentName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity"));
                        }
                    } catch (PackageManager.NameNotFoundException e5) {
                    }
                    intent5.addFlags(268435456);
                    try {
                        applicationContext.startActivity(intent5);
                    } catch (Exception e6) {
                        Log.e("WiYun", "Could not open google map on ad click to " + this.Ae.kJ, e6);
                    }
                }
            }
            if (!equals && this.Ae.is == 2 && this.Ae.BX != null) {
                this.dS = true;
                setClickable(false);
                this.kC = 0;
                AnimationSet animationSet = new AnimationSet(getContext(), null);
                ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.2f, 1.0f, 1.2f, (float) (vx / 2), (float) (vx / 2));
                scaleAnimation.setDuration(200);
                scaleAnimation.setInterpolator(new DecelerateInterpolator());
                animationSet.addAnimation(scaleAnimation);
                ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.2f, 0.0f, 1.2f, 0.0f, (float) (vx / 2), (float) (vx / 2));
                scaleAnimation2.setDuration(400);
                scaleAnimation2.setInterpolator(new AccelerateInterpolator());
                scaleAnimation2.setStartOffset(200);
                animationSet.addAnimation(scaleAnimation2);
                animationSet.setStartTime(-1);
                this.Aa = animationSet;
                this.cQ = SystemClock.uptimeMillis();
                invalidate();
            }
        }
    }

    private static void b(Canvas canvas, Rect rect) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(-1147097);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2.0f);
        paint.setPathEffect(new CornerPathEffect(3.0f));
        Path path = new Path();
        path.addRoundRect(new RectF(rect), 3.0f, 3.0f, Path.Direction.CW);
        canvas.drawPath(path, paint);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    private void b(Canvas canvas, Rect rect, int i) {
        String str = this.Ae.zY;
        if (str != null) {
            this.Ad.setColor(i);
            if (this.Af == null) {
                this.Af = a(this.Ad, str, (float) (rect.width() - (getHeight() / 2)));
                this.Ag = 0;
                this.Ah = System.currentTimeMillis();
            }
            Paint.FontMetrics fontMetrics = this.Ad.getFontMetrics();
            float f = fontMetrics.descent - fontMetrics.ascent;
            if (this.Af.length < 3) {
                float height = (((((float) rect.height()) - f) - f) / 2.0f) + ((float) (-((int) this.Ad.ascent())));
                for (String drawText : this.Af) {
                    canvas.drawText(drawText, (float) rect.left, height, this.Ad);
                    height += f;
                }
                return;
            }
            long currentTimeMillis = System.currentTimeMillis();
            if (!this.Ai && currentTimeMillis > this.Ah + 5000) {
                this.Ah = currentTimeMillis;
                this.Ai = true;
                this.Aj = currentTimeMillis;
            }
            if (this.Ai) {
                float max = Math.max(0.0f, 1.0f - (((float) (currentTimeMillis - this.Aj)) / 1000.0f));
                this.Ad.setAlpha((int) (255.0f * max));
                int min = Math.min(2, this.Af.length - this.Ag);
                float height2 = ((((float) rect.height()) - (((float) min) * f)) / 2.0f) + (((float) min) * f);
                canvas.drawText(this.Af[this.Ag], (float) rect.left, ((height2 * max) - (((float) min) * f)) - fontMetrics.ascent, this.Ad);
                if (min > 1) {
                    canvas.drawText(this.Af[this.Ag + 1], (float) rect.left, (height2 * max) - fontMetrics.descent, this.Ad);
                }
                int i2 = 0;
                if (this.Af.length - this.Ag >= 2) {
                    i2 = (this.Ag + 2) % this.Af.length;
                }
                int min2 = Math.min(2, this.Af.length - i2);
                float height3 = (((float) rect.height()) - (((float) min2) * f)) / 2.0f;
                this.Ad.setAlpha(255 - this.Ad.getAlpha());
                canvas.drawText(this.Af[i2], (float) rect.left, (((((float) rect.height()) - height3) * max) + height3) - fontMetrics.ascent, this.Ad);
                if (min2 > 1) {
                    canvas.drawText(this.Af[i2 + 1], (float) rect.left, ((f + height3) + ((((float) rect.height()) - height3) * max)) - fontMetrics.ascent, this.Ad);
                }
                if (max == 0.0f) {
                    this.Ai = false;
                    postInvalidateDelayed(3000);
                    this.Ag = i2;
                    this.Ah = currentTimeMillis;
                    return;
                }
                postInvalidateDelayed(50);
                return;
            }
            this.Ad.setAlpha(255);
            int min3 = Math.min(2, this.Af.length - this.Ag);
            float height4 = ((((float) rect.height()) - (((float) min3) * f)) / 2.0f) + ((float) (-((int) this.Ad.ascent())));
            for (int i3 = 0; i3 < min3; i3++) {
                canvas.drawText(this.Af[this.Ag + i3], (float) rect.left, height4, this.Ad);
                height4 += f;
            }
            postInvalidateDelayed(3000);
        }
    }

    private static String c(String str, boolean z) {
        if (!z) {
            return str.trim();
        }
        int length = str.length() - 1;
        while (length >= 0 && str.charAt(length) <= ' ') {
            length--;
        }
        return length <= 0 ? str : str.substring(0, length + 1);
    }

    private void c(Canvas canvas) {
        Bitmap bitmap = this.Ae.BW;
        if (bitmap != null) {
            float width = (float) bitmap.getWidth();
            float height = (float) bitmap.getHeight();
            float width2 = (float) getWidth();
            float height2 = (float) getHeight();
            float max = Math.max(height / height2, width / width2);
            float f = width / max;
            float f2 = height / max;
            int i = (int) ((width2 - f) / 2.0f);
            int i2 = (int) ((height2 - f2) / 2.0f);
            a(canvas, new Rect(i, i2, ((int) f) + i, ((int) f2) + i2), bitmap);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    private void d(Canvas canvas) {
        long currentTimeMillis = System.currentTimeMillis();
        if (!this.Ai && currentTimeMillis > this.Ah + 5000) {
            this.Ah = currentTimeMillis;
            if (this.Af.length > 1) {
                this.Ai = true;
                this.Aj = currentTimeMillis;
            }
        }
        String str = this.Af[this.Ag];
        String str2 = this.Af[this.Ag >= this.Af.length - 1 ? 0 : this.Ag + 1];
        Paint.FontMetrics fontMetrics = this.Ad.getFontMetrics();
        float f = fontMetrics.descent - fontMetrics.ascent;
        float height = (((float) getHeight()) - f) / 2.0f;
        float f2 = f + height;
        this.Ad.setAlpha(255);
        this.Ad.setColor(this.is);
        if (this.Ai) {
            float max = Math.max(0.0f, 1.0f - (((float) (currentTimeMillis - this.Aj)) / 1000.0f));
            this.Ad.setAlpha((int) (255.0f * max));
            canvas.drawText(str, 4.0f, (f2 * max) - fontMetrics.descent, this.Ad);
            this.Ad.setAlpha(255 - this.Ad.getAlpha());
            canvas.drawText(str2, 4.0f, (((((float) getHeight()) - height) * max) + height) - fontMetrics.ascent, this.Ad);
            if (max == 0.0f) {
                this.Ai = false;
                postInvalidateDelayed(3000);
                this.Ag = (this.Ag + 1) % this.Af.length;
                this.Ah = currentTimeMillis;
                return;
            }
            postInvalidateDelayed(50);
            return;
        }
        canvas.drawText(str, 4.0f, height - fontMetrics.ascent, this.Ad);
        postInvalidateDelayed(3000);
    }

    private float fU() {
        if (this.Ae == null || this.Ae.is != 1) {
            return 14.0f;
        }
        return this.Ae.BS;
    }

    private float fW() {
        Paint.FontMetrics fontMetrics = this.Ad.getFontMetrics();
        return fontMetrics.descent - fontMetrics.ascent;
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            setPressed(true);
            return true;
        } else if (action == 2) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int left = getLeft();
            int top = getTop();
            int right = getRight();
            int bottom = getBottom();
            if (x < ((float) left) || x > ((float) right) || y < ((float) top) || y > ((float) bottom)) {
                setPressed(false);
            } else {
                setPressed(true);
            }
            return true;
        } else if (action != 1) {
            return super.dispatchTouchEvent(motionEvent);
        } else {
            if (isPressed()) {
                aa();
            }
            setPressed(false);
            return true;
        }
    }

    public final void f(int i) {
        this.is = i;
        postInvalidate();
    }

    /* access modifiers changed from: protected */
    public final ao fV() {
        return this.Ae;
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        this.dS = false;
        if (this.Ac != null && !this.Ac.isRecycled()) {
            this.Ac.recycle();
        }
        this.Ac = null;
        super.onDetachedFromWindow();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        Rect rect;
        Rect rect2;
        int i;
        int i2;
        try {
            super.onDraw(canvas);
            if (this.Ae != null) {
                switch (this.Ae.is) {
                    case 1:
                        Rect rect3 = new Rect(0, 0, getWidth(), getHeight());
                        if (this.Af == null) {
                            this.Af = a(this.Ad, this.Ae.zY, (((float) (rect3.width() - 25)) - 4.0f) - 4.0f);
                            this.Ag = 0;
                            this.Ah = System.currentTimeMillis();
                        }
                        if (isPressed()) {
                            a(canvas, rect3, -19456);
                            d(canvas);
                            return;
                        }
                        a(canvas, rect3, this.iO);
                        d(canvas);
                        if (hasFocus()) {
                            rect3.top++;
                            b(canvas, rect3);
                            return;
                        }
                        return;
                    case 2:
                        Rect rect4 = new Rect(0, 0, getWidth(), getHeight());
                        Bitmap bitmap = this.Ae.BX;
                        Bitmap bitmap2 = this.Ae.BW;
                        int height = rect4.height();
                        if (height > 0) {
                            if (bitmap != null) {
                                int width = bitmap.getWidth();
                                int height2 = bitmap.getHeight();
                                if (width == height2) {
                                    if (width > vx || (width * 100) / height < 67) {
                                        width = vx;
                                    }
                                    int i3 = (height - width) / 2;
                                    Rect rect5 = new Rect(rect4.left + i3, rect4.top + i3, rect4.left + i3 + width, width + rect4.top + i3);
                                    rect2 = new Rect(i3 + rect5.right, rect4.top, rect4.right - 8, rect4.bottom);
                                    rect = rect5;
                                } else {
                                    boolean z = width > height2;
                                    int i4 = width > height2 ? width : height2;
                                    if (i4 > vx || (i4 * 100) / height < 67) {
                                        int i5 = vx;
                                        if (z) {
                                            i2 = (height2 * i5) / width;
                                            i = i5;
                                        } else {
                                            i = (width * i5) / height2;
                                            i2 = i5;
                                        }
                                    } else {
                                        int i6 = height2;
                                        i = width;
                                        i2 = i6;
                                    }
                                    int i7 = (height - i) / 2;
                                    int i8 = (height - i2) / 2;
                                    Rect rect6 = new Rect(rect4.left + i7, rect4.top + i8, i + rect4.left + i7, i2 + i8 + rect4.top);
                                    rect2 = new Rect(rect6.right + i7, rect4.top, rect4.right - 8, rect4.bottom);
                                    rect = rect6;
                                }
                            } else {
                                rect = null;
                                rect2 = new Rect(rect4.left + 8, rect4.top, rect4.right - 8, rect4.bottom);
                            }
                            if (isPressed()) {
                                a(canvas, rect4, -19456);
                                if (bitmap2 != null) {
                                    c(canvas);
                                } else {
                                    b(canvas, rect2, -16777216);
                                }
                            } else {
                                a(canvas, rect4, this.iO);
                                if (bitmap2 != null) {
                                    c(canvas);
                                } else {
                                    b(canvas, rect2, this.is);
                                }
                                if (hasFocus()) {
                                    rect4.top++;
                                    b(canvas, rect4);
                                    rect4.top--;
                                }
                            }
                            boolean z2 = false;
                            if (bitmap != null) {
                                if (this.dS && SystemClock.uptimeMillis() - this.cQ > 4200) {
                                    this.Aa = null;
                                    this.dS = false;
                                    setClickable(true);
                                }
                                if (this.dS) {
                                    if (!this.Aa.isInitialized()) {
                                        this.Aa.initialize(vx, vx, vx, height);
                                    }
                                    z2 = this.Aa.getTransformation(SystemClock.uptimeMillis(), this.Ab);
                                    if (z2) {
                                        Matrix matrix = canvas.getMatrix();
                                        canvas.save();
                                        matrix.preConcat(this.Ab.getMatrix());
                                        canvas.setMatrix(matrix);
                                        a(canvas, rect, this.kC == 0 ? bitmap : this.Ac);
                                        canvas.restore();
                                    }
                                } else {
                                    a(canvas, rect, bitmap);
                                }
                            }
                            if (bitmap2 != null && isPressed()) {
                                canvas.drawColor(872415231);
                            }
                            Bitmap d = ac.d(ai.pa, ai.pa.length);
                            if (d != null) {
                                a(canvas, new Rect((rect4.right - 2) - d.getWidth(), (rect4.height() - d.getHeight()) / 2, rect4.right - 2, ((rect4.height() - d.getHeight()) / 2) + d.getHeight()), d);
                                d.recycle();
                            }
                            if (bitmap != null && this.dS) {
                                if (!z2) {
                                    switch (this.kC) {
                                        case 0:
                                            if (this.Ac == null) {
                                                try {
                                                    this.Ac = ac.d(z.zR, z.zR.length);
                                                } catch (OutOfMemoryError e) {
                                                }
                                            }
                                            if (this.Ac != null) {
                                                RotateAnimation rotateAnimation = new RotateAnimation(0.0f, 1080.0f, (float) (getHeight() / 2), (float) (getHeight() / 2));
                                                rotateAnimation.setDuration(3600);
                                                rotateAnimation.setInterpolator(new s());
                                                rotateAnimation.setRepeatMode(-1);
                                                rotateAnimation.setStartTime(-1);
                                                this.Aa = rotateAnimation;
                                                this.kC++;
                                                break;
                                            }
                                        case 1:
                                            this.Aa = null;
                                            this.dS = false;
                                            setClickable(true);
                                            break;
                                    }
                                }
                                invalidate();
                                return;
                            }
                            return;
                        }
                        return;
                    case 3:
                        int width2 = getWidth();
                        int height3 = getHeight();
                        c(canvas);
                        if (isPressed()) {
                            canvas.drawColor(872415231);
                        }
                        Bitmap d2 = ac.d(ai.pa, ai.pa.length);
                        if (d2 != null) {
                            int width3 = d2.getWidth();
                            if (width3 > 25) {
                                width3 = 23;
                            }
                            int height4 = d2.getHeight();
                            if (height4 > 25) {
                                height4 = 23;
                            }
                            int i9 = ((height3 - 25) - ((25 - height4) / 2)) - height4;
                            int i10 = (25 - width3) / 2;
                            a(canvas, new Rect(i10, i9, width3 + i10, height4 + i9), d2);
                            d2.recycle();
                        }
                        if (hasFocus()) {
                            b(canvas, new Rect(0, 1, width2, height3));
                            return;
                        }
                        return;
                    default:
                        return;
                }
                Log.e("WiYun", "Exception raised during onDraw.", e);
            }
        } catch (Exception e2) {
            Log.e("WiYun", "Exception raised during onDraw.", e2);
        }
    }

    public final boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 66 || i == 23) {
            setPressed(true);
        }
        return super.onKeyDown(i, keyEvent);
    }

    public final boolean onKeyUp(int i, KeyEvent keyEvent) {
        setPressed(false);
        if (i == 66 || i == 23) {
            aa();
        }
        return super.onKeyUp(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        int applyDimension;
        int i3 = 0;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        switch (mode) {
            case Integer.MIN_VALUE:
                if (this.Ae.is != 3) {
                    if (this.Ae.is != 1) {
                        applyDimension = (int) TypedValue.applyDimension(1, 50.0f, getContext().getResources().getDisplayMetrics());
                        break;
                    } else {
                        this.Ad.setTextSize(fU());
                        applyDimension = Math.min((int) (fW() + 4.0f + 4.0f), size);
                        break;
                    }
                } else {
                    applyDimension = size;
                    break;
                }
            case 1073741824:
                if (this.Ae.is != 2) {
                    applyDimension = size;
                    break;
                }
                applyDimension = (int) TypedValue.applyDimension(1, 50.0f, getContext().getResources().getDisplayMetrics());
                break;
            default:
                if (this.Ae.is != 3) {
                    if (this.Ae.is == 1) {
                        applyDimension = (int) (fW() + 4.0f + 4.0f);
                        break;
                    }
                    applyDimension = (int) TypedValue.applyDimension(1, 50.0f, getContext().getResources().getDisplayMetrics());
                    break;
                } else {
                    applyDimension = size;
                    break;
                }
        }
        if (this.Ae.is == 2) {
            vx = (int) (((float) applyDimension) - ((10.0f * ((float) applyDimension)) / 50.0f));
            int i4 = ((int) (((float) applyDimension) - 16.0f)) / 2;
            this.Ad.setTextSize((float) i4);
            int i5 = i4;
            while (fW() > ((float) i4)) {
                i5 -= 2;
                this.Ad.setTextSize((float) i5);
            }
        }
        if (applyDimension != 0) {
            i3 = View.MeasureSpec.getSize(i);
        }
        setMeasuredDimension(i3, applyDimension);
        if (Log.isLoggable("WiYun", 3)) {
            Log.d("WiYun", "AdContainer.onMeasure() determined the ad to be " + i3 + "x" + applyDimension + " pixels.");
        }
    }

    public final void setBackgroundColor(int i) {
        this.iO = i;
        postInvalidate();
    }

    public final void setPressed(boolean z) {
        if (isPressed() != z) {
            super.setPressed(z);
            invalidate();
        }
    }
}
