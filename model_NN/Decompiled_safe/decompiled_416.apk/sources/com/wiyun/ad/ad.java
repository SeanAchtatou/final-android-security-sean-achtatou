package com.wiyun.ad;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.view.View;

final class ad extends View {
    private long cO = System.currentTimeMillis();
    private boolean dS = false;
    private int kC;
    private Paint zV;
    private Shader zW;
    private float zX = 50.0f;
    private String zY;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, int, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    ad(String str, Context context) {
        super(context);
        this.zY = str;
        this.kC = 1;
        this.zV = new Paint();
        this.zV.setColor(-7829368);
        this.zV.setAntiAlias(true);
        this.zV.setTextSize(26.0f);
        this.zW = new LinearGradient(0.0f, 0.0f, 200.0f, 0.0f, new int[]{Color.argb(255, 120, 120, 120), Color.argb(255, 120, 120, 120), Color.argb(255, 255, 255, 255)}, new float[]{0.0f, 0.7f, 1.0f}, Shader.TileMode.MIRROR);
        this.zV.setShader(this.zW);
    }

    public final void fS() {
        this.zV.setColor(-1);
    }

    public final void fT() {
        this.zV.setTextSize(16.0f);
    }

    public final void j(boolean z) {
        this.dS = z;
        invalidate();
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        long currentTimeMillis = System.currentTimeMillis();
        this.zX = (((float) (currentTimeMillis - this.cO)) / 4.5f) + this.zX;
        Matrix matrix = new Matrix();
        if (this.dS) {
            matrix.setTranslate(this.zX, 0.0f);
            postInvalidateDelayed(50);
        } else {
            matrix.setTranslate(0.0f, 0.0f);
        }
        this.zW.setLocalMatrix(matrix);
        int height = getHeight();
        int width = getWidth();
        Paint.FontMetrics fontMetrics = this.zV.getFontMetrics();
        int i = (height - ((int) (fontMetrics.descent - fontMetrics.ascent))) / 2;
        int measureText = (int) this.zV.measureText(this.zY);
        switch (this.kC) {
            case 1:
                canvas.drawText(this.zY, (float) ((width - measureText) / 2), ((float) i) - fontMetrics.ascent, this.zV);
                break;
            case 2:
                canvas.drawText(this.zY, (float) (width - measureText), ((float) i) - fontMetrics.ascent, this.zV);
                break;
            default:
                canvas.drawText(this.zY, 0.0f, ((float) i) - fontMetrics.ascent, this.zV);
                break;
        }
        this.cO = currentTimeMillis;
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        int min;
        int i3 = 0;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        switch (mode) {
            case Integer.MIN_VALUE:
                min = Math.min(50, size);
                break;
            case 1073741824:
                min = size;
                break;
            default:
                min = Math.max(50, size);
                break;
        }
        if (min != 0) {
            i3 = View.MeasureSpec.getSize(i);
        }
        setMeasuredDimension(i3, min);
    }
}
