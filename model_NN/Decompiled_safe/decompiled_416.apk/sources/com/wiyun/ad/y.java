package com.wiyun.ad;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

class y {
    private final Map dK = new HashMap(252);
    private final String[] zO = new String[3000];

    public y() {
        Properties properties = new Properties();
        InputStream resourceAsStream = y.class.getResourceAsStream("HtmlCharacterEntityReferences.properties");
        if (resourceAsStream == null) {
            throw new IllegalStateException("Cannot find reference definition file [HtmlCharacterEntityReferences.properties] as class path resource");
        }
        try {
            properties.load(resourceAsStream);
            resourceAsStream.close();
            Enumeration<?> propertyNames = properties.propertyNames();
            while (propertyNames.hasMoreElements()) {
                String str = (String) propertyNames.nextElement();
                int parseInt = Integer.parseInt(str);
                int i = parseInt < 1000 ? parseInt : parseInt - 7000;
                String property = properties.getProperty(str);
                this.zO[i] = String.valueOf('&') + property + ';';
                this.dK.put(property, new Character((char) parseInt));
            }
        } catch (IOException e) {
            throw new IllegalStateException("Failed to parse reference definition file [HtmlCharacterEntityReferences.properties]: " + e.getMessage());
        } catch (Throwable th) {
            resourceAsStream.close();
            throw th;
        }
    }

    public final char aY(String str) {
        Character ch = (Character) this.dK.get(str);
        if (ch != null) {
            return ch.charValue();
        }
        return 65535;
    }
}
