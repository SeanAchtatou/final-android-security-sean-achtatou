package com.wiyun.ad;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class c implements Runnable {
    private /* synthetic */ m hf;
    private final /* synthetic */ View hg;

    c(m mVar, View view) {
        this.hf = mVar;
        this.hg = view;
    }

    public final void run() {
        ((InputMethodManager) this.hf.iP.getContext().getSystemService("input_method")).hideSoftInputFromWindow(this.hg.getWindowToken(), 0);
    }
}
