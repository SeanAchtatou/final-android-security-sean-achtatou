package com.wiyun.ad;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import java.io.File;
import java.util.Timer;

public class AdView extends FrameLayout implements View.OnClickListener {
    private boolean bS;
    private boolean cT;
    /* access modifiers changed from: private */
    public boolean dx;
    /* access modifiers changed from: private */
    public View iZ;
    private int is;
    private int kA;
    private Timer kB;
    private int kC;
    private int kD;
    private int kE;
    private boolean kF;
    private int kG;
    private String kH;
    private String kI;
    private String kJ;
    private boolean kK;
    private ak kL;
    private ad kM;
    /* access modifiers changed from: private */
    public boolean kN;
    /* access modifiers changed from: private */
    public ao kO;
    private View kP;
    private Drawable kQ;
    /* access modifiers changed from: private */
    public ae ky;
    private boolean kz;

    public AdView(Context context) {
        this(context, null, 0);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.kC = 0;
        this.kD = 0;
        this.kG = 2;
        this.kH = "*/*";
        setFocusable(true);
        setDescendantFocusability(262144);
        setClickable(true);
        this.is = 0;
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            int attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "textColor", 0);
            int attributeUnsignedIntValue2 = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", 0);
            if (attributeUnsignedIntValue != 0) {
                this.kD = attributeUnsignedIntValue;
                if (this.ky != null) {
                    this.ky.f(attributeUnsignedIntValue);
                }
                invalidate();
            }
            if (attributeUnsignedIntValue2 != 0) {
                setBackgroundColor(attributeUnsignedIntValue2);
            }
            this.kI = attributeSet.getAttributeValue(str, "resId");
            if (this.kI != null) {
                this.kI = this.kI.trim();
            }
            this.kF = attributeSet.getAttributeBooleanValue(str, "testMode", false);
            this.kG = attributeSet.getAttributeIntValue(str, "testAdType", 2);
            this.kK = attributeSet.getAttributeBooleanValue(str, "showLoadingHint", false);
            o(attributeSet.getAttributeIntValue(str, "refreshInterval", 0));
            this.cT = attributeSet.getAttributeBooleanValue(str, "goneIfFail", this.cT);
            this.kE = attributeSet.getAttributeIntValue(str, "transition", 1);
            this.bS = attributeSet.getAttributeBooleanValue(str, "autoStart", false);
        }
        File[] listFiles = getContext().getCacheDir().listFiles();
        if (listFiles != null) {
            long currentTimeMillis = System.currentTimeMillis();
            for (File file : listFiles) {
                if (file.lastModified() - currentTimeMillis > 86400000) {
                    file.delete();
                }
            }
        }
    }

    static /* synthetic */ void a(AdView adView, LinearLayout linearLayout) {
        if (!adView.kN) {
            adView.kN = true;
            TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, 0.0f, 1, 0.0f, 1, -1.0f);
            translateAnimation.setDuration(200);
            translateAnimation.setFillAfter(true);
            translateAnimation.setInterpolator(new DecelerateInterpolator());
            translateAnimation.setAnimationListener(new n(adView, linearLayout));
            linearLayout.startAnimation(translateAnimation);
        }
    }

    private void a(ae aeVar) {
        this.ky = aeVar;
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(350);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        startAnimation(alphaAnimation);
    }

    /* access modifiers changed from: private */
    public void bE() {
        if (this.kO != null && this.kO.is != 3 && this.kP != null) {
            removeView(this.kP);
            this.kP = null;
            if (this.kQ != null) {
                this.kQ.setCallback(null);
                this.kQ = null;
            }
        }
    }

    static /* synthetic */ void d(AdView adView) {
        TranslateAnimation translateAnimation;
        TranslateAnimation translateAnimation2;
        if (!(adView.kM == null || adView.kM.getParent() == null)) {
            adView.kM.j(false);
            adView.removeView(adView.kM);
        }
        if (adView.ky != null) {
            adView.ky.setVisibility(0);
        }
        if (adView.kO != null) {
            synchronized (adView) {
                if (adView.ky == null || !adView.kO.equals(adView.ky.fV())) {
                    boolean z = adView.ky == null;
                    ae aeVar = new ae(adView.getContext(), adView.kO, adView.kL);
                    int i = adView.kC;
                    if (i == 0) {
                        i = adView.kO.kE;
                    }
                    if (i == 0) {
                        i = -16777216;
                    }
                    int i2 = adView.kD;
                    if (i2 == 0) {
                        i2 = adView.kO.kD;
                    }
                    if (i2 == 0) {
                        i2 = -1;
                    }
                    aeVar.setBackgroundColor(i);
                    aeVar.f(i2);
                    int visibility = super.getVisibility();
                    aeVar.setVisibility(visibility);
                    try {
                        if (adView.kL != null) {
                            try {
                                adView.kL.dM();
                            } catch (Exception e) {
                                Log.w("WiYun", "Unhandled exception raised in your AdListener.onAdLoaded().", e);
                            }
                        }
                        adView.addView(aeVar);
                        if (adView.kO.is == 3 && adView.kP == null) {
                            if (adView.kQ == null) {
                                adView.kQ = ac.a(z.zQ, z.pa, z.zP);
                            }
                            Button button = new Button(adView.getContext());
                            button.setBackgroundDrawable(adView.kQ);
                            button.setOnClickListener(adView);
                            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
                            layoutParams.gravity = 53;
                            adView.addView(button, layoutParams);
                            adView.kP = button;
                        }
                        if (visibility == 0) {
                            if (!z) {
                                if (!adView.kK) {
                                    if (!adView.dx) {
                                        adView.dx = true;
                                        int i3 = adView.kE;
                                        int abs = i3 == 0 ? (Math.abs((int) (SystemClock.uptimeMillis() / 1000)) % 7) + 1 : i3;
                                        switch (abs) {
                                            case 3:
                                            case 4:
                                            case 5:
                                            case 6:
                                                switch (abs) {
                                                    case 3:
                                                        translateAnimation = new TranslateAnimation(1, 0.0f, 1, 1.0f, 1, 0.0f, 1, 0.0f);
                                                        translateAnimation2 = new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                                                        break;
                                                    case 4:
                                                        translateAnimation = new TranslateAnimation(1, 0.0f, 1, -1.0f, 1, 0.0f, 1, 0.0f);
                                                        translateAnimation2 = new TranslateAnimation(1, 1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                                                        break;
                                                    case 5:
                                                        translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
                                                        translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                                                        break;
                                                    case 6:
                                                        translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
                                                        translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
                                                        break;
                                                    default:
                                                        translateAnimation = null;
                                                        translateAnimation2 = null;
                                                        break;
                                                }
                                                translateAnimation.setDuration(700);
                                                translateAnimation.setFillAfter(true);
                                                translateAnimation.setInterpolator(new DecelerateInterpolator());
                                                translateAnimation2.setDuration(700);
                                                translateAnimation2.setFillAfter(true);
                                                translateAnimation2.setInterpolator(new DecelerateInterpolator());
                                                translateAnimation2.setAnimationListener(new f(adView, aeVar));
                                                adView.ky.startAnimation(translateAnimation);
                                                aeVar.startAnimation(translateAnimation2);
                                                break;
                                            case 7:
                                                AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
                                                alphaAnimation.setDuration(700);
                                                alphaAnimation.setFillAfter(true);
                                                alphaAnimation.setInterpolator(new AccelerateInterpolator());
                                                AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
                                                alphaAnimation2.setDuration(700);
                                                alphaAnimation2.setFillAfter(true);
                                                alphaAnimation2.setInterpolator(new AccelerateInterpolator());
                                                alphaAnimation2.setAnimationListener(new e(adView, aeVar));
                                                adView.ky.startAnimation(alphaAnimation);
                                                aeVar.startAnimation(alphaAnimation2);
                                                break;
                                            default:
                                                aeVar.setVisibility(8);
                                                ap apVar = new ap(0.0f, -90.0f, ((float) adView.getWidth()) / 2.0f, ((float) adView.getHeight()) / 2.0f, -0.4f * ((float) (abs == 2 ? adView.getHeight() : adView.getWidth())), true, abs != 2);
                                                apVar.setDuration(700);
                                                apVar.setFillAfter(true);
                                                apVar.setInterpolator(new AccelerateInterpolator());
                                                apVar.setAnimationListener(new g(adView, aeVar, abs));
                                                adView.startAnimation(apVar);
                                                break;
                                        }
                                    }
                                } else {
                                    adView.removeView(adView.ky);
                                    adView.bE();
                                    if (adView.ky.fV() != null) {
                                        adView.ky.fV().X();
                                    }
                                    adView.a(aeVar);
                                }
                            } else {
                                adView.a(aeVar);
                            }
                        } else {
                            if (adView.ky != null) {
                                adView.removeView(adView.ky);
                            }
                            adView.bE();
                            if (!(adView.ky == null || adView.ky.fV() == null)) {
                                adView.ky.fV().X();
                            }
                            adView.ky = aeVar;
                        }
                    } catch (Exception e2) {
                        Log.e("WiYun", "Unhandled exception placing AdContainer into AdView.", e2);
                    }
                } else {
                    if (adView.ky.getParent() == null) {
                        adView.addView(adView.ky);
                    }
                    adView.kO.X();
                }
            }
        } else {
            if (adView.ky != null && adView.ky.getParent() == null) {
                adView.addView(adView.ky);
            }
            if (adView.kL != null) {
                try {
                    adView.kL.dL();
                } catch (Exception e3) {
                    Log.w("WiYun", "Unhandled exception raised in your AdListener.onLoadAdFailed().", e3);
                }
            }
        }
        adView.kz = false;
    }

    private void j(boolean z) {
        synchronized (this) {
            if (z) {
                if (this.kA > 0) {
                    if (this.kB == null) {
                        this.kB = new Timer();
                        this.kB.schedule(new o(this), (long) (this.kA * 1000), (long) (this.kA * 1000));
                    }
                }
            }
            if (this.kB != null) {
                this.kB.cancel();
                this.kB = null;
            }
        }
    }

    public final void R(String str) {
        this.kI = str;
    }

    /* access modifiers changed from: package-private */
    public final void X() {
        if (!this.kN) {
            this.kN = true;
            LinearLayout linearLayout = new LinearLayout(getContext());
            linearLayout.setGravity(16);
            linearLayout.setOrientation(0);
            linearLayout.setPadding(5, 5, 0, 0);
            linearLayout.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-3879462, -7167046}));
            EditText editText = new EditText(getContext());
            editText.setTextAppearance(getContext(), 16973895);
            editText.setTypeface(Typeface.DEFAULT_BOLD);
            editText.setSingleLine(true);
            editText.setHint(this.kO.cR);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
            layoutParams.weight = 1.0f;
            editText.setLayoutParams(layoutParams);
            linearLayout.addView(editText);
            editText.setOnKeyListener(new l(this, linearLayout));
            editText.setOnFocusChangeListener(new m(this, linearLayout));
            addView(linearLayout, new FrameLayout.LayoutParams(-1, getHeight()));
            TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, 0.0f, 1, -1.0f, 1, 0.0f);
            translateAnimation.setDuration(200);
            translateAnimation.setFillAfter(true);
            translateAnimation.setInterpolator(new DecelerateInterpolator());
            translateAnimation.setAnimationListener(new j(this, editText));
            editText.startAnimation(translateAnimation);
            this.iZ = editText;
        }
    }

    public final void a(ak akVar) {
        synchronized (this) {
            this.kL = akVar;
        }
    }

    /* access modifiers changed from: package-private */
    public final String aK() {
        return this.kH;
    }

    public final void bD() {
        int currentTimeMillis = (int) (System.currentTimeMillis() / 1000);
        if (!this.kz && !this.dx && currentTimeMillis - this.is >= 30) {
            this.kO = null;
            this.kz = true;
            this.is = currentTimeMillis;
            if (this.kK) {
                if (this.kM == null) {
                    this.kM = new ad(ac.fO(), getContext());
                    this.kM.fS();
                    this.kM.fT();
                }
                if (this.iZ != null) {
                    removeView(this.iZ);
                    this.iZ = null;
                }
                if (this.ky != null) {
                    this.ky.setVisibility(4);
                }
                if (this.kM.getParent() == null) {
                    addView(this.kM, new FrameLayout.LayoutParams(-1, -1));
                }
                this.kM.j(true);
            }
            new i(this).start();
        }
    }

    public final String bF() {
        return this.kI;
    }

    public final boolean bG() {
        return this.kF;
    }

    public final int bH() {
        return this.kG;
    }

    /* access modifiers changed from: package-private */
    public final String bI() {
        return this.kJ;
    }

    public int getVisibility() {
        if (this.ky != null) {
            return super.getVisibility();
        }
        if (!this.cT) {
            return super.getVisibility();
        }
        if (!this.kz || !this.kK) {
            return 8;
        }
        return super.getVisibility();
    }

    public final void o(int i) {
        int i2;
        if (i <= 0) {
            i2 = 0;
        } else {
            if (i < 30) {
                af.k("AdView.setRefreshInterval(" + i + ") seconds must be >= " + 30);
            }
            i2 = i;
        }
        this.kA = i2;
        if (i2 == 0) {
            j(false);
        } else {
            j(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ag.x(getContext());
        if (this.bS) {
            bD();
        }
    }

    public void onClick(View view) {
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        ag.X();
        ah.Ap = false;
        super.onDetachedFromWindow();
    }

    public void onWindowFocusChanged(boolean z) {
        j(z);
    }

    public void setBackgroundColor(int i) {
        super.setBackgroundColor(i);
        this.kC = i;
        if (this.ky != null) {
            this.ky.setBackgroundColor(i);
        }
        invalidate();
    }

    public void setVisibility(int i) {
        if (super.getVisibility() != i) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    getChildAt(i2).setVisibility(i);
                }
                super.setVisibility(i);
            }
        }
    }
}
