package com.wiyun.ad;

import android.view.View;
import android.widget.LinearLayout;

final class m implements View.OnFocusChangeListener {
    final /* synthetic */ AdView iP;
    private final /* synthetic */ LinearLayout lC;

    m(AdView adView, LinearLayout linearLayout) {
        this.iP = adView;
        this.lC = linearLayout;
    }

    public final void onFocusChange(View view, boolean z) {
        if (z) {
            view.post(new b(this, view));
            return;
        }
        view.post(new c(this, view));
        if (view.getParent() != null) {
            AdView.a(this.iP, this.lC);
        }
    }
}
