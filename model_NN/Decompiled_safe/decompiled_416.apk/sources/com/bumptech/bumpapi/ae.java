package com.bumptech.bumpapi;

import com.bumptech.bumpapi.a.c;
import com.bumptech.bumpapi.a.d;
import com.bumptech.bumpapi.a.e;
import com.bumptech.bumpapi.a.f;
import com.bumptech.bumpapi.a.i;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;

final class ae implements d {
    private static int tb = 0;
    private static int tc = 0;
    private static ae td;
    private static float te = 500.0f;
    private static float tf = 800.0f;
    private o r;
    private int tg;
    private int th;
    /* access modifiers changed from: private */
    public int ti;
    private boolean tj;
    private double tk;
    private double tl;
    private Object tm;
    private Timer tn;
    private final String to = "https://www.bumphome.com/servernameA1";
    private int tp;
    private boolean tq;
    private boolean tr;
    private h ts;
    private int tt;

    private ae() {
        tb++;
        this.tj = false;
        this.r = o.cn();
        dC();
        this.tm = new Object();
    }

    private void a(double d) {
        this.tp++;
        double ah = this.r.ah("servertime");
        double d2 = ah - d;
        double currentTimeMillis = ah - (((double) System.currentTimeMillis()) / 1000.0d);
        if (currentTimeMillis > this.r.ah("minoffset") || this.tp == 1) {
            if (this.tp <= 10 || this.r.ah("maxoffset") - currentTimeMillis >= 0.5d * (this.r.ah("maxoffset") - this.r.ah("minoffset"))) {
                this.r.b("minoffset", currentTimeMillis);
                this.r.ag("minoffset");
                this.r.ag("maxoffset");
            } else {
                this.tp = 0;
            }
        }
        if (d2 < this.r.ah("maxoffset") || this.tp == 1) {
            if (this.tp <= 10 || d2 - this.r.ah("minoffset") >= 0.5d * (this.r.ah("maxoffset") - this.r.ah("minoffset"))) {
                this.r.b("maxoffset", d2);
                this.r.ag("minoffset");
                this.r.ag("maxoffset");
            } else {
                this.tp = 0;
            }
        }
        if (this.r.ah("maxoffset") - this.r.ah("minoffset") < 0.0d) {
            this.tp = 0;
        }
    }

    private static byte[] a(InputStream inputStream, int i) {
        byte[] bArr = new byte[i];
        for (int i2 = 0; i2 < bArr.length; i2++) {
            bArr[i2] = 48;
        }
        int i3 = 0;
        int i4 = i;
        do {
            try {
                int read = inputStream.read(bArr, i3, i4);
                i3 += read;
                if (read > 0) {
                    i4 -= read;
                }
                if (read < 0) {
                    break;
                }
            } catch (Exception e) {
            }
        } while (i4 > 0);
        return bArr;
    }

    public static ae dD() {
        if (td == null) {
            td = new ae();
        }
        return td;
    }

    private void dE() {
        this.r.b("handsettime", ((double) System.currentTimeMillis()) / 1000.0d);
        if (!this.tq) {
            if (this.r.ai("goodbye") == 1) {
                this.tq = true;
                this.tk = (double) (System.currentTimeMillis() - 1);
            }
            this.r.ai("hello");
            if (this.ti != 0 && !this.tq) {
                return;
            }
            if (this.r.ak("servername") == 0) {
                this.tp = 0;
                this.ti = 1;
                c cVar = new c("https://www.bumphome.com/servernameA1?hello=1&time=1&bumpid=" + this.r.aj("bumpid") + "&bumpversion=" + "G_API0.4" + "&locale=" + Locale.getDefault().toString() + (this.r.ak("gpsnew") != 0 ? "&gpslong=" + b.format(this.r.ah("gpslong")) : "") + (this.r.ak("gpsnew") != 0 ? "&gpslat=" + b.format(this.r.ah("gpslat")) : "") + (this.r.ak("gpsnew") != 0 ? "&gpsaccuracy=" + b.format(this.r.ah("gpsaccuracy")) : "") + "&timezone=" + (Calendar.getInstance().getTimeZone().getRawOffset() / 3600000), 6000, this);
                cVar.bf("server_name_request_job");
                f.cY().a(cVar);
            } else if (this.tk <= ((double) System.currentTimeMillis())) {
                byte[] cq = this.r.cq();
                this.tk = ((double) System.currentTimeMillis()) + this.tl;
                c cVar2 = new c(this.r.aj("servername"), 60000, this);
                cVar2.bf("server_comms_job");
                cVar2.i(cq);
                cVar2.bg("B879ah3wiuh");
                this.ti = 1;
                f.cY().a(cVar2);
            }
        }
    }

    private void setNetworkAvailable(boolean z) {
        if (this.tj != z) {
            this.tj = z;
            if (this.ts != null) {
                this.ts.g(z);
            }
        }
    }

    public final void a(i iVar, InputStream inputStream) {
        String[] strArr;
        synchronized (this.tm) {
            try {
                if (this.tr) {
                    this.tr = false;
                }
                if (iVar.gm() == "server_name_request_job") {
                    byte[] c = e.c(inputStream);
                    String[] split = b.split((c != null ? new String(c) : null).trim(), ",");
                    if (split.length > 0) {
                        this.r.j("servername", split[0]);
                        this.r.g("servername", 1);
                        if (split.length > 1) {
                            this.r.b("servertime", Double.parseDouble(split[1]));
                            this.r.g("servertime", 1);
                        }
                    }
                    boolean z = iVar.gl() == null || iVar.gl().length < 900;
                    double gq = iVar.gq() / 1000.0d;
                    if (z) {
                        a(gq);
                    }
                    this.tk = (double) System.currentTimeMillis();
                    this.tp = 0;
                    this.r.f("hello", 1);
                    this.r.ag("hello");
                    this.r.ag("bumpid");
                    this.r.ag("hardwareid");
                    this.r.ag("bumpversion");
                    this.r.ag("osid");
                    if (this.r.ak("starttime") != 0) {
                        this.r.ag("starttime");
                    }
                    if (this.r.ak("myname") != 0) {
                        this.r.ag("myname");
                    }
                    if (this.r.ak("gpstime") != 0) {
                        this.r.ag("gpstime");
                    }
                    if (this.r.ak("gpsnew") != 0) {
                        this.r.ag("gpsnew");
                    }
                    if (this.r.ak("gpslong") != 0) {
                        this.r.ag("gpslong");
                    }
                    if (this.r.ak("gpslat") != 0) {
                        this.r.ag("gpslat");
                    }
                    if (this.r.ak("gpsaccuracy") != 0) {
                        this.r.ag("gpsaccuracy");
                    }
                    this.ti = 0;
                    dE();
                } else if (iVar.gm() == "server_comms_job") {
                    if (!new String(a(inputStream, 2), "UTF-8").equals("AA")) {
                        int i = this.tt + 1;
                        this.tt = i;
                        if (i >= 3) {
                            this.r.j("servername", "");
                            this.r.g("servername", 0);
                            this.tp = 0;
                        } else {
                            f.cY().a((c) iVar);
                        }
                        return;
                    }
                    this.tt = 0;
                    int parseInt = Integer.parseInt(new String(a(inputStream, 8), "UTF-8"));
                    inputStream.skip(1);
                    if (parseInt > 0) {
                        String str = new String(a(inputStream, parseInt - 1), "UTF-8");
                        inputStream.skip(1);
                        strArr = b.split(str, ";");
                    } else {
                        strArr = null;
                    }
                    if (strArr != null) {
                        for (String split2 : strArr) {
                            String[] split3 = b.split(split2, ",");
                            String str2 = split3[0];
                            String str3 = "";
                            byte[] a2 = a(inputStream, Integer.parseInt(split3[1]));
                            ao am = this.r.am(str2);
                            if (am != null) {
                                if (!(am.gi() == 5 || am.gi() == 4)) {
                                    str3 = new String(a2, "UTF-8");
                                }
                                if (!am.gd().equals("bumpid")) {
                                    if (am.gi() == 1) {
                                        am.N(Integer.parseInt(str3));
                                        am.P(1);
                                        am.M(1);
                                    }
                                    if (am.AG == 2) {
                                        am.b(Double.parseDouble(str3));
                                        am.P(1);
                                        am.M(1);
                                    }
                                    if (am.AG == 3) {
                                        am.be(str3);
                                        am.P(1);
                                        am.M(1);
                                    }
                                    if (am.AG == 5) {
                                        am.AC = a2;
                                        am.P(1);
                                        am.M(1);
                                    }
                                    if (am.AG == 4) {
                                        am.h(a2);
                                        am.P(1);
                                        am.M(1);
                                    }
                                }
                            }
                        }
                        boolean z2 = iVar.gl() == null || iVar.gl().length < 900;
                        double gq2 = iVar.gq() / 1000.0d;
                        if (z2) {
                            a(gq2);
                        }
                        setNetworkAvailable(true);
                        this.ti = 0;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                this.ti = 0;
                String message = e.getMessage();
                if (message == null) {
                    message = "no exception message.";
                }
                o.cn().an("InetCom_519:error handling server response -- " + message);
            }
        }
    }

    public final void a(h hVar) {
        this.ts = hVar;
        if (this.ts != null) {
            this.ts.g(this.tj);
        }
    }

    public final void a(Exception exc) {
        this.r.j("servername", "");
        this.r.g("servername", 0);
        this.tp = 0;
        setNetworkAvailable(false);
        this.ti = 0;
    }

    public final void dC() {
        this.r.f("goodbye", 0);
        this.tg = 0;
        this.th = 0;
        this.ti = 0;
        setNetworkAvailable(false);
        this.tk = 0.0d;
        this.tl = 1200.0d;
        this.tq = false;
        this.tp = 0;
    }

    public final void dF() {
        dE();
    }

    public final void dG() {
        if (this.tn != null) {
            this.tn.cancel();
            this.tn = null;
        }
        this.tn = new Timer();
        this.tn.scheduleAtFixedRate(new i(this), 0, (long) te);
    }

    public final void dH() {
        if (this.tn != null) {
            this.tn.cancel();
            this.tn = null;
        }
        f.cY().cZ();
        setNetworkAvailable(false);
    }

    public final void dI() {
        this.tr = true;
        this.tk = (double) (System.currentTimeMillis() - 1);
        dE();
    }

    public final boolean dJ() {
        return this.tj;
    }

    public final void dK() {
        this.r.f("goodbye", 1);
        this.r.ag("goodbye");
    }
}
