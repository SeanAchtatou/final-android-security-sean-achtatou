package com.bumptech.bumpapi;

final class n extends a implements v {
    public n(ar arVar) {
        super(arVar);
    }

    public final boolean V(String str) {
        if (!this.u) {
            return false;
        }
        if (str.equals("othername")) {
            String aj = this.r.aj("othername");
            if (aj.equals("")) {
                return false;
            }
            this.t.n(aj);
            return true;
        } else if (!str.equals("otheruserconfirms") || this.r.ai("otheruserconfirms") != 0) {
            return false;
        } else {
            this.t.r(this.r.aj("othername"));
            this.s.a(ad.IDLE_STATE);
            return true;
        }
    }

    public final void c() {
        super.c();
        this.r.a("otheruserconfirms", this);
        if (!V("othername")) {
            this.r.a("othername", this);
        }
    }
}
