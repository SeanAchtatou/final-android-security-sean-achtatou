package com.bumptech.bumpapi;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class BumpAPI extends Activity implements c, y {
    private final Handler handler = new ac(this);
    private final View.OnClickListener q = new ab(this);
    private ViewGroup xA;
    private TextView xB;
    private TextView xC;
    private ProgressBar xD;
    private ImageView xE;
    private ImageView xF;
    private View xG;
    private View xH;
    private View xI;
    private TextView xJ;
    private TextView xK;
    private View xL;
    private boolean xM;
    private Resources xN;
    private String xO;
    private String xP;
    /* access modifiers changed from: private */
    public Thread xQ;
    /* access modifiers changed from: private */
    public t xR;

    static /* synthetic */ void a(BumpAPI bumpAPI, String str) {
        bumpAPI.xB.setText(bumpAPI.xN.getString(k.bump_connecting));
        bumpAPI.xD.setVisibility(8);
        bumpAPI.xE.setAnimation(null);
        bumpAPI.xF.setAnimation(null);
        bumpAPI.xG.setVisibility(0);
        bumpAPI.xH.setBackgroundDrawable(null);
        View inflate = bumpAPI.getLayoutInflater().inflate(e.bump_waiting_view, (ViewGroup) null);
        ((TextView) inflate.findViewById(ah.waiting_prompt)).setText(bumpAPI.xN.getString(k.bump_waiting_for, str));
        bumpAPI.xA.removeAllViews();
        bumpAPI.xA.addView(inflate);
    }

    /* access modifiers changed from: private */
    public void a(CharSequence charSequence) {
        this.xM = true;
        this.handler.removeMessages(9);
        ((TextView) findViewById(ah.notify)).setText(charSequence);
        ViewGroup viewGroup = (ViewGroup) findViewById(ah.notify_bar);
        viewGroup.setVisibility(0);
        viewGroup.startAnimation((AnimationSet) AnimationUtils.loadAnimation(this, f.bump_slide_down));
        this.handler.sendEmptyMessageDelayed(9, 3000);
    }

    private void aP(String str) {
        if (str != null) {
            this.xO = str;
            this.xK.setText(str);
            this.xR.setName(this.xO);
        }
    }

    static /* synthetic */ void b(BumpAPI bumpAPI) {
        bumpAPI.xB.setText(bumpAPI.xN.getString(k.bump_warming_up));
        bumpAPI.xD.setVisibility(0);
        bumpAPI.xE.setAnimation(null);
        bumpAPI.xF.setAnimation(null);
        bumpAPI.xG.setVisibility(4);
        bumpAPI.xH.setBackgroundDrawable(null);
        bumpAPI.xA.removeAllViews();
        bumpAPI.xA.addView(bumpAPI.xI);
        bumpAPI.xR.dm();
    }

    static /* synthetic */ void b(BumpAPI bumpAPI, String str) {
        bumpAPI.xB.setText(k.bump_connecting);
        bumpAPI.xD.setVisibility(8);
        bumpAPI.xE.setAnimation(null);
        bumpAPI.xF.setAnimation(null);
        bumpAPI.xG.setVisibility(0);
        bumpAPI.xH.setBackgroundDrawable(null);
        ((TextView) bumpAPI.xL.findViewById(ah.confirm_prompt)).setText(bumpAPI.xN.getString(k.bump_confirm_connect, str));
        bumpAPI.xA.removeAllViews();
        bumpAPI.xA.addView(bumpAPI.xL);
    }

    static /* synthetic */ void c(BumpAPI bumpAPI) {
        AnimationSet animationSet = (AnimationSet) AnimationUtils.loadAnimation(bumpAPI, f.bump_left_hand);
        new ap(bumpAPI.xF, animationSet, bumpAPI.xE, (AnimationSet) AnimationUtils.loadAnimation(bumpAPI, f.bump_right_hand)).start();
        bumpAPI.xB.setText(bumpAPI.xN.getString(k.bump_to_connect));
        bumpAPI.xD.setVisibility(8);
        bumpAPI.xG.setVisibility(4);
        bumpAPI.xH.setBackgroundDrawable(null);
        bumpAPI.xA.removeAllViews();
        bumpAPI.xA.addView(bumpAPI.xI);
        bumpAPI.xR.dn();
    }

    static /* synthetic */ void c(BumpAPI bumpAPI, String str) {
        bumpAPI.xB.setText(bumpAPI.xN.getString(k.bump_connecting));
        bumpAPI.xD.setVisibility(8);
        bumpAPI.xE.setAnimation(null);
        bumpAPI.xF.setAnimation(null);
        bumpAPI.xG.setVisibility(0);
        bumpAPI.xH.setBackgroundDrawable(null);
        View inflate = bumpAPI.getLayoutInflater().inflate(e.bump_waiting_view, (ViewGroup) null);
        ((TextView) inflate.findViewById(ah.waiting_prompt)).setText(bumpAPI.xN.getString(k.bump_connected, str));
        bumpAPI.xA.removeAllViews();
        bumpAPI.xA.addView(inflate);
    }

    static /* synthetic */ void d(BumpAPI bumpAPI) {
        bumpAPI.xB.setText(bumpAPI.xN.getString(k.bump_connecting));
        bumpAPI.xD.setVisibility(8);
        bumpAPI.xE.setAnimation(null);
        bumpAPI.xF.setAnimation(null);
        bumpAPI.xG.setVisibility(4);
        bumpAPI.xH.setBackgroundResource(f.bump_signal);
        ((AnimationDrawable) bumpAPI.xH.getBackground()).start();
        View inflate = bumpAPI.getLayoutInflater().inflate(e.bump_waiting_view, (ViewGroup) null);
        ((TextView) inflate.findViewById(ah.waiting_prompt)).setText(bumpAPI.xN.getString(k.bump_please_wait));
        bumpAPI.xA.removeAllViews();
        bumpAPI.xA.addView(inflate);
    }

    static /* synthetic */ void e(BumpAPI bumpAPI) {
        if (bumpAPI.xM) {
            bumpAPI.xM = false;
            bumpAPI.handler.removeMessages(9);
            ((ViewGroup) bumpAPI.findViewById(ah.notify_bar)).startAnimation((AnimationSet) AnimationUtils.loadAnimation(bumpAPI, f.bump_slide_up));
        }
    }

    /* access modifiers changed from: private */
    public void fq() {
        if (this.xR.dk()) {
            a(aq.FAIL_USER_CANCELED);
        } else {
            a(aq.FAIL_NETWORK_UNAVAILABLE);
        }
    }

    static /* synthetic */ void g(BumpAPI bumpAPI) {
        if (Settings.System.getString(bumpAPI.getContentResolver(), "android_id") == null || Settings.System.getString(bumpAPI.getContentResolver(), "android_id").equals("9774d56d682e549c")) {
            bumpAPI.xR.dl();
        }
    }

    static /* synthetic */ void i(BumpAPI bumpAPI) {
        Intent intent = new Intent(bumpAPI, EditTextActivity.class);
        intent.putExtra("EXTRA_DEFAULT", bumpAPI.xO);
        intent.putExtra("EXTRA_PROMPT", bumpAPI.xN.getString(k.bump_edit_name));
        bumpAPI.startActivityForResult(intent, 1);
    }

    static /* synthetic */ void j(BumpAPI bumpAPI) {
        bumpAPI.xR.dj();
        bumpAPI.a(bumpAPI.xN.getText(k.bump_you_canceled));
    }

    public final void U() {
        this.handler.sendEmptyMessage(1);
    }

    public final void V() {
        this.handler.sendEmptyMessage(2);
    }

    public final void W() {
        this.handler.sendEmptyMessage(9);
        this.handler.sendEmptyMessage(4);
    }

    public final void a(aq aqVar) {
        Intent intent = new Intent();
        switch (z.sz[aqVar.ordinal()]) {
            case 1:
                intent.putExtra("EXTRA_CONNECTION", this.xR.di());
                setResult(-1, intent);
                break;
            default:
                intent.putExtra("EXTRA_REASON", aqVar);
                setResult(0, intent);
                break;
        }
        try {
            this.xQ.join();
        } catch (InterruptedException e) {
        }
        this.xR.release();
        finish();
    }

    public final void aO(String str) {
        if (this.xC == null) {
            this.xC = new TextView(this);
            this.xC.setTextColor(ColorStateList.valueOf(-65536));
            this.xC.setTypeface(Typeface.DEFAULT_BOLD);
            this.xC.setGravity(1);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams.addRule(3, ah.content_layout);
            ((RelativeLayout) findViewById(ah.api_popup)).addView(this.xC, layoutParams);
        }
        this.xC.setText(str);
    }

    public final void n(String str) {
        this.handler.sendMessage(this.handler.obtainMessage(5, str));
    }

    public final void o(String str) {
        this.handler.sendMessage(this.handler.obtainMessage(3, str));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        switch (i) {
            case 1:
                if (i2 == -1) {
                    aP(intent.getStringExtra("EXTRA_RESULT"));
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.xR == null) {
            this.xR = new t(this, this, this, this.handler);
            this.xQ = new aa(this);
            this.xQ.start();
        }
        setContentView(e.bump_api_popup);
        findViewById(ah.bump_logo).setOnClickListener(this.q);
        findViewById(ah.close_window).setOnClickListener(this.q);
        this.xM = false;
        this.xB = (TextView) findViewById(ah.status);
        this.xD = (ProgressBar) findViewById(ah.progress);
        this.xE = (ImageView) findViewById(ah.bump_hand_right);
        this.xF = (ImageView) findViewById(ah.bump_hand_left);
        this.xG = findViewById(ah.bump_signal);
        this.xH = findViewById(ah.bump_icon);
        this.xA = (ViewGroup) findViewById(ah.content_layout);
        LayoutInflater layoutInflater = getLayoutInflater();
        this.xI = layoutInflater.inflate(e.bump_start_view, (ViewGroup) null);
        this.xI.findViewById(ah.start_edit_name).setOnClickListener(this.q);
        this.xJ = (TextView) this.xI.findViewById(ah.start_prompt);
        this.xK = (TextView) this.xI.findViewById(ah.start_user_name);
        this.xL = layoutInflater.inflate(e.bump_confirm_view, (ViewGroup) null);
        this.xL.findViewById(ah.yes_button).setOnClickListener(this.q);
        this.xL.findViewById(ah.no_button).setOnClickListener(this.q);
        this.xN = getResources();
        Intent intent = getIntent();
        try {
            this.xQ.join();
        } catch (InterruptedException e) {
        }
        aP(intent.getStringExtra("EXTRA_USER_NAME"));
        String stringExtra = intent.getStringExtra("EXTRA_API_KEY");
        if (stringExtra != null) {
            this.xP = stringExtra;
            this.xR.au(this.xP);
        }
        String stringExtra2 = intent.getStringExtra("EXTRA_ACTION_MSG");
        if (stringExtra2 == null) {
            stringExtra2 = this.xN.getString(k.bump_default_action);
        }
        this.xJ.setText(stringExtra2);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
                fq();
                return true;
            default:
                return super.onKeyDown(i, keyEvent);
        }
    }

    public void onPause() {
        this.xR.onPause();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.xR.onResume();
    }

    public void onStop() {
        super.onStop();
    }

    public final void p(String str) {
        this.handler.sendMessage(this.handler.obtainMessage(6, str));
    }

    public final void q(String str) {
        this.handler.sendMessage(this.handler.obtainMessage(7, str));
    }

    public final void r(String str) {
        int i = k.bump_other_canceled;
        this.handler.sendMessage(this.handler.obtainMessage(7, this.xN.getString(i, str)));
    }
}
