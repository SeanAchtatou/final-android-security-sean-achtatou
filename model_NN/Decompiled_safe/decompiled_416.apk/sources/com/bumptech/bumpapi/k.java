package com.bumptech.bumpapi;

public final class k {

    /* renamed from: lR */
    public static final int bump_confirm_connect = 2131165190;

    /* renamed from: lS */
    public static final int bump_connected = 2131165191;

    /* renamed from: lT */
    public static final int bump_connecting = 2131165192;

    /* renamed from: lU */
    public static final int bump_default_action = 2131165215;

    /* renamed from: lV */
    public static final int bump_edit_name = 2131165193;

    /* renamed from: lW */
    public static final int bump_other_canceled = 2131165184;

    /* renamed from: lX */
    public static final int bump_please_wait = 2131165196;

    /* renamed from: lY */
    public static final int bump_to_connect = 2131165187;

    /* renamed from: lZ */
    public static final int bump_waiting_for = 2131165198;

    /* renamed from: ma */
    public static final int bump_warming_up = 2131165199;

    /* renamed from: mb */
    public static final int bump_you_canceled = 2131165200;
}
