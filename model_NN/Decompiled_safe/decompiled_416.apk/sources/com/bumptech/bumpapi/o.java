package com.bumptech.bumpapi;

import android.os.Build;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Vector;

final class o implements v {
    private static o nA;
    private static String nw;
    private Hashtable nx = new Hashtable();
    private x ny;
    private Hashtable nz = new Hashtable();

    public o() {
        b("error", 3, 0);
        b("bumpid", 3, 0);
        b("bumpversion", 3, 0);
        b("handsettime", 2, 0);
        b("starttime", 2, 0);
        b("iconfirm", 1, 0);
        b("myname", 3, 1);
        b("othername", 3, 0);
        b("hardwareid", 3, 0);
        b("osid", 3, 0);
        b("minoffset", 2, 0);
        b("maxoffset", 2, 0);
        b("servertime", 2, 0);
        b("bumpstamp", 3, 0);
        b("bumpstreamreq", 1, 0);
        b("bumpstream", 3, 0);
        b("bumptimes", 3, 0);
        b("bumpdetail", 3, 0);
        b("iphandset", 3, 0);
        b("servername", 3, 0);
        b("servernameka", 3, 0);
        b("gpslong", 2, 1);
        b("gpslat", 2, 1);
        b("gpsaccuracy", 2, 1);
        b("gpstime", 2, 1);
        b("gpsnew", 1, 0);
        b("serverretryidle", 2, 1);
        b("serverretryactive", 2, 1);
        b("commstimeout", 2, 1);
        b("solobumptimeout", 2, 1);
        b("networkbusytimeout", 2, 1);
        b("accelpretime", 2, 1);
        b("accelprethresh", 2, 1);
        b("accelbumpthresh1", 2, 1);
        b("accelbumpthresh2", 2, 1);
        b("hello", 1, 0);
        b("firsthello", 1, 0);
        b("goodbye", 1, 0);
        b("appstatus", 1, 0);
        b("bumpmatched", 1, 0);
        b("bt_bumpmatched", 1, 0);
        b("againmessage", 3, 0);
        b("bt_againmessage", 3, 0);
        b("otheruserconfirms", 1, 0);
        b("mydatadone", 1, 0);
        b("otherdatadone", 1, 0);
        b("servermessage", 3, 0);
        b("requestdata", 3, 0);
        b("serverretryonce", 2, 0);
        b("smsrequest", 3, 0);
        b("mymodeproposed", 1, 0);
        b("mymode", 1, 0);
        b("mymodemeta", 3, 0);
        b("othermode", 1, 0);
        b("othermodemeta", 3, 0);
        b("apikey", 3, 0);
        b("apiauthresponse", 3, 0);
        b("apiauthmessage", 3, 0);
        b("bumpsensitivity", 2, 0);
    }

    private void al(String str) {
        ao aoVar = (ao) this.nx.get(str);
        if (aoVar != null) {
            aoVar.gk();
        }
    }

    private void b(String str, int i, int i2) {
        ao aoVar = new ao();
        aoVar.AI = this;
        aoVar.bd(str);
        aoVar.L(0);
        aoVar.M(0);
        aoVar.h(null);
        aoVar.ge();
        aoVar.be("");
        aoVar.b(0.0d);
        aoVar.N(0);
        aoVar.O(i);
        aoVar.P(0);
        aoVar.Q(i2);
        this.nx.put(str, aoVar);
    }

    public static o cn() {
        if (nA == null) {
            nA = new o();
        }
        return nA;
    }

    public final boolean V(String str) {
        Vector vector = (Vector) this.nz.get(str);
        if (vector == null) {
            return true;
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= vector.size()) {
                return true;
            }
            ((v) vector.get(i2)).V(str);
            i = i2 + 1;
        }
    }

    public final void a(x xVar) {
        this.ny = xVar;
        nw = "G_API0.4";
    }

    public final void a(String str, v vVar) {
        Vector vector = (Vector) this.nz.get(str);
        if (vector == null) {
            Vector vector2 = new Vector();
            vector2.add(vVar);
            this.nz.put(str, vector2);
            return;
        }
        vector.add(vVar);
    }

    public final void ag(String str) {
        ao aoVar = (ao) this.nx.get(str);
        if (aoVar != null) {
            aoVar.L(1);
        }
    }

    public final double ah(String str) {
        ao aoVar = (ao) this.nx.get(str);
        if (aoVar != null) {
            return aoVar.gg();
        }
        return -1.0d;
    }

    public final int ai(String str) {
        ao aoVar = (ao) this.nx.get(str);
        if (aoVar != null) {
            return aoVar.gh();
        }
        return -1;
    }

    public final String aj(String str) {
        ao aoVar = (ao) this.nx.get(str);
        return aoVar != null ? aoVar.gf() : "";
    }

    public final int ak(String str) {
        ao aoVar = (ao) this.nx.get(str);
        if (aoVar != null) {
            return aoVar.gj();
        }
        return -1;
    }

    public final ao am(String str) {
        return (ao) this.nx.get(str);
    }

    public final void an(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("bumpversion=");
        stringBuffer.append(aj("bumpversion"));
        stringBuffer.append("&");
        stringBuffer.append("osid=");
        stringBuffer.append(aj("osid"));
        stringBuffer.append("&");
        stringBuffer.append("hardwareid=");
        stringBuffer.append(aj("hardwareid"));
        stringBuffer.append("&");
        stringBuffer.append("err=");
        stringBuffer.append(str);
        j("error", stringBuffer.toString());
        ag("error");
        g("error", 1);
    }

    public final void b(String str, double d) {
        ao aoVar = (ao) this.nx.get(str);
        if (aoVar != null) {
            aoVar.b(d);
        }
    }

    public final void b(String str, v vVar) {
        ((Vector) this.nz.get(str)).remove(vVar);
    }

    public final void co() {
        this.nz.clear();
    }

    public final void cp() {
        if (this.ny != null) {
            j("hardwareid", Build.MODEL);
            j("bumpversion", nw);
            j("osid", "Android" + "," + Build.VERSION.RELEASE);
            f("gpsnew", 0);
            f("otheruserconfirms", -1);
            al("goodbye");
            j("againmessage", "");
            al("servername");
            if (ak("myname") == 0) {
                g("myname", 1);
                j("myname", "Nobody");
            }
            if (ak("bumpid") == 0) {
                j("bumpid", this.ny.du());
            }
            if (ak("bumpstreamreq") == 0) {
                f("bumpstreamreq", 0);
            }
            if (ak("iphandset") == 0) {
                j("iphandset", "unknown");
            }
            if (ak("serverretryactive") == 0 || ah("serverretryactive") > 15.0d) {
                b("serverretryactive", 0.8d);
            }
            if (ak("serverretryidle") == 0 || ah("serverretryidle") > 15.0d) {
                b("serverretryidle", 1.2d);
            }
            if (ak("commstimeout") == 0 || ah("commstimeout") > 90.0d || ah("commstimeout") < 8.0d) {
                b("commstimeout", 45.0d);
            }
            if (ak("solobumptimeout") == 0 || ah("solobumptimeout") < 3.0d) {
                b("solobumptimeout", 7.0d);
            }
            if (ak("networkbusytimeout") == 0 || ah("networkbusytimeout") < 3.0d) {
                b("networkbusytimeout", 7.0d);
            }
            if (ak("accelpretime") == 0) {
                b("accelpretime", 0.2d);
            }
            if (ak("accelprethresh") == 0) {
                b("accelprethresh", 0.07d);
            }
            if (ak("accelbumpthresh1") == 0) {
                b("accelbumpthresh1", 1.0d);
            }
            if (ak("accelbumpthresh2") == 0) {
                b("accelbumpthresh2", 1.6d);
            }
        }
    }

    public final byte[] cq() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (String str : this.nx.keySet()) {
            ao aoVar = (ao) this.nx.get(str);
            byte[] bArr = null;
            if (aoVar.Ay.equals("error")) {
                if (aoVar.AH != 0) {
                    aoVar.Az = 1;
                }
            } else if (aoVar.Az == 1 || aoVar.Ay.equals("bumpid") || aoVar.Ay.equals("bumpversion") || aoVar.Ay.equals("handsettime")) {
                if (aoVar.AG == 1) {
                    try {
                        bArr = Integer.toString(aoVar.AF).getBytes("UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                if (aoVar.AG == 2) {
                    try {
                        bArr = b.format(aoVar.AE).getBytes("UTF-8");
                    } catch (UnsupportedEncodingException e2) {
                        e2.printStackTrace();
                    }
                }
                if (aoVar.AG == 3 && aoVar.AD != null) {
                    try {
                        bArr = aoVar.AD.getBytes("UTF-8");
                    } catch (UnsupportedEncodingException e3) {
                        e3.printStackTrace();
                    }
                }
                if (aoVar.AG == 5) {
                    bArr = aoVar.AC;
                }
                if (aoVar.AG == 4) {
                    bArr = aoVar.AB;
                }
                if (bArr != null && bArr.length > 0) {
                    String str2 = "Content-Disposition: form-data; name=\"" + aoVar.Ay + "\"\r\n\r\n";
                    try {
                        byteArrayOutputStream.write("\r\n--B879ah3wiuh\r\n".getBytes("UTF-8"));
                        byteArrayOutputStream.write(str2.getBytes("UTF-8"));
                        byteArrayOutputStream.write(bArr);
                    } catch (UnsupportedEncodingException e4) {
                        e4.printStackTrace();
                    } catch (IOException e5) {
                        e5.printStackTrace();
                    }
                }
            }
            aoVar.L(0);
        }
        al("error");
        return byteArrayOutputStream.toByteArray();
    }

    public final void f(String str, int i) {
        ao aoVar = (ao) this.nx.get(str);
        if (aoVar != null) {
            aoVar.N(i);
        }
    }

    public final void g(String str, int i) {
        ao aoVar = (ao) this.nx.get(str);
        if (aoVar != null) {
            aoVar.P(i);
        }
    }

    public final void j(String str, String str2) {
        ao aoVar = (ao) this.nx.get(str);
        if (aoVar != null) {
            aoVar.be(str2);
        }
    }
}
