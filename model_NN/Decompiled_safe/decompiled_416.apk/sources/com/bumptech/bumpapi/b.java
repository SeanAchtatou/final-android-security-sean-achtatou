package com.bumptech.bumpapi;

import java.util.Vector;

final class b {
    b() {
    }

    private static void a(Vector vector, String str, String str2) {
        String str3 = str2;
        String str4 = str;
        Vector vector2 = vector;
        while (true) {
            if (str4 == null) {
                str4 = "";
            }
            int indexOf = str4.indexOf(str3);
            if (indexOf != -1) {
                vector2.addElement(str4.substring(0, indexOf));
                str4 = str4.substring(indexOf + str3.length());
            } else {
                vector2.addElement(str4);
                return;
            }
        }
    }

    public static String format(double d) {
        if (Math.abs(d) < 0.001d) {
            return "0.000";
        }
        String d2 = Double.toString(d > 0.0d ? d + 5.0E-4d : d - 5.0E-4d);
        String[] split = split(d2, "E");
        if (split.length == 2) {
            String str = split[0];
            String str2 = split[1];
            String[] split2 = split(str, ".");
            if (split2.length == 2) {
                int length = split2[0].length();
                int length2 = split2[1].length();
                String str3 = split2[0] + split2[1];
                StringBuffer stringBuffer = new StringBuffer(str3);
                int parseInt = Integer.parseInt(str2);
                if (parseInt > 0) {
                    int i = length2 < parseInt ? parseInt - length2 : 0;
                    for (int i2 = 0; i2 < i; i2++) {
                        stringBuffer.insert(str3.length(), "0");
                    }
                    if (i <= 0 && length2 != parseInt) {
                        stringBuffer.insert(length + parseInt, ".");
                    }
                } else {
                    int abs = Math.abs(parseInt) - length;
                    for (int i3 = 0; i3 < abs; i3++) {
                        stringBuffer.insert(0, "0");
                    }
                    if (abs > 0) {
                        stringBuffer.insert(0, "0.");
                    } else {
                        stringBuffer.insert(length + parseInt, ".");
                    }
                }
                d2 = stringBuffer.toString();
            }
        }
        int indexOf = d2.indexOf(46);
        return indexOf > 0 ? (d2 + "000").substring(0, indexOf + 4) : d2 + ".000";
    }

    public static String[] split(String str, String str2) {
        Vector vector = new Vector();
        a(vector, str, str2);
        if (vector.size() > 0) {
            String[] strArr = new String[vector.size()];
            vector.copyInto(strArr);
            return strArr;
        }
        return new String[]{str};
    }
}
