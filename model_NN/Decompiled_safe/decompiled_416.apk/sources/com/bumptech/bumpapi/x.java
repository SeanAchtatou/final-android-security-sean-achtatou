package com.bumptech.bumpapi;

import android.content.Context;
import android.os.PowerManager;
import android.telephony.TelephonyManager;
import java.lang.ref.WeakReference;
import java.util.Random;

final class x {
    private WeakReference sr;
    private PowerManager.WakeLock ss;
    private boolean st = false;

    public x(Context context) {
        this.sr = new WeakReference(context);
    }

    private synchronized void dv() {
        if (this.st && this.ss != null && this.ss.isHeld()) {
            this.ss.release();
            this.st = false;
        }
    }

    public final String du() {
        String deviceId = ((TelephonyManager) ((Context) this.sr.get()).getSystemService("phone")).getDeviceId();
        if (!deviceId.equals("000000000000000")) {
            return deviceId;
        }
        Random random = new Random();
        return "EMU" + (((long) ((random.nextInt(10000000) * 10000000) + random.nextInt(10000000))) + 100000000000000L);
    }

    public final void finalize() {
        dv();
    }
}
