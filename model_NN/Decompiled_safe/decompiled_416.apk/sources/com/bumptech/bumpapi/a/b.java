package com.bumptech.bumpapi.a;

import java.io.InputStream;

public final class b extends Thread {
    private final g gj;
    private final h gk = new h();

    b(g gVar) {
        this.gj = gVar;
    }

    public final void run() {
        c cVar;
        Exception exc;
        while (true) {
            try {
                c dB = this.gj.dB();
                if (dB != null) {
                    try {
                        if (!dB.gp() && f.da()) {
                            InputStream a2 = h.a(dB);
                            if (dB.bp() != null && !dB.gp()) {
                                dB.bp().a(dB, a2);
                            }
                        }
                    } catch (Exception e) {
                        Exception exc2 = e;
                        cVar = dB;
                        exc = exc2;
                        exc.printStackTrace();
                        if (!(cVar == null || cVar.bp() == null)) {
                            cVar.bp().a(exc);
                        }
                    }
                }
            } catch (Exception e2) {
                Exception exc3 = e2;
                cVar = null;
                exc = exc3;
            }
        }
    }
}
