package com.bumptech.bumpapi.a;

import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public final class h {
    private static int tu = 4;

    public static InputStream a(i iVar) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(iVar.getUrl()).openConnection();
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setReadTimeout(iVar.gn());
            httpURLConnection.setConnectTimeout(iVar.gn());
            httpURLConnection.setUseCaches(iVar.go());
            httpURLConnection.setDefaultUseCaches(true);
            iVar.c((double) System.currentTimeMillis());
            if (iVar.gs()) {
                httpURLConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + iVar.gr());
            }
            if (iVar.gl() != null) {
                httpURLConnection.setDoOutput(true);
                httpURLConnection.connect();
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream(), 1000);
                bufferedOutputStream.write(iVar.gl());
                bufferedOutputStream.write(("\r\n--" + iVar.gr() + "--\r\n").getBytes("UTF-8"));
                bufferedOutputStream.close();
            }
            return httpURLConnection.getInputStream();
        } catch (Exception e) {
            e.printStackTrace();
            throw new a("Exception while doing http request: " + e.getCause());
        }
    }
}
