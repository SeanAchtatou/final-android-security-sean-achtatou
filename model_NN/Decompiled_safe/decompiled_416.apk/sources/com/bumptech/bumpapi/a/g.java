package com.bumptech.bumpapi.a;

import java.util.Vector;

public final class g {
    private static Vector sM = new Vector();
    private static g sN = null;

    public static g dA() {
        if (sN == null) {
            sN = new g();
        }
        return sN;
    }

    public static void removeAllElements() {
        sM.removeAllElements();
    }

    public final synchronized void b(c cVar) {
        sM.addElement(cVar);
        notify();
    }

    public final synchronized c dB() {
        c cVar;
        cVar = null;
        if (sM.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        if (!sM.isEmpty()) {
            cVar = (c) sM.elementAt(0);
            sM.removeElementAt(0);
        }
        return cVar;
    }
}
