package com.bumptech.bumpapi.a;

public final class f {
    private static f qF;
    private static boolean qH = false;
    private Thread[] qG;
    private g qI;

    private f() {
        this.qI = null;
        this.qG = new Thread[5];
        this.qI = g.dA();
        for (int i = 0; i < 5; i++) {
            this.qG[i] = new b(this.qI);
            this.qG[i].start();
        }
    }

    public static f cY() {
        if (qF == null) {
            synchronized (f.class) {
                qF = new f();
            }
        }
        return qF;
    }

    public static boolean da() {
        return qH;
    }

    public final void a(c cVar) {
        if (cVar != null) {
            if (!qH) {
                qH = true;
            }
            this.qI.b(cVar);
        }
    }

    public final void cZ() {
        g.removeAllElements();
        for (int i = 0; i < 5; i++) {
            this.qG[i].interrupt();
        }
        qF = null;
    }
}
