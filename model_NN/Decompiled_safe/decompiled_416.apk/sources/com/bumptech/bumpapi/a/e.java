package com.bumptech.bumpapi.a;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public final class e {
    public static byte[] c(InputStream inputStream) {
        byte[] bArr = null;
        if (inputStream != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                byte[] bArr2 = new byte[8192];
                while (true) {
                    int read = inputStream.read(bArr2);
                    if (read <= 0) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr2, 0, read);
                }
                byteArrayOutputStream.flush();
                bArr = byteArrayOutputStream.toByteArray();
                try {
                    byteArrayOutputStream.close();
                } catch (IOException e) {
                }
            } catch (IOException e2) {
                try {
                    byteArrayOutputStream.close();
                } catch (IOException e3) {
                }
            } catch (Throwable th) {
                try {
                    byteArrayOutputStream.close();
                } catch (IOException e4) {
                }
                throw th;
            }
        }
        return bArr;
    }
}
