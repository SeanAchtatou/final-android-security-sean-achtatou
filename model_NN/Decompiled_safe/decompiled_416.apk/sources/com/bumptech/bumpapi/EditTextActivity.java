package com.bumptech.bumpapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class EditTextActivity extends Activity {
    /* access modifiers changed from: private */
    public EditText p;
    private final View.OnClickListener q = new q(this);

    public final void b() {
        Intent intent = new Intent();
        intent.putExtra("EXTRA_RESULT", this.p.getText().toString());
        setResult(-1, intent);
        finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(e.bump_edit_text);
        findViewById(ah.edit_text_cancel).setOnClickListener(this.q);
        findViewById(ah.edit_text_okay).setOnClickListener(this.q);
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra("EXTRA_PROMPT");
        String stringExtra2 = intent.getStringExtra("EXTRA_DEFAULT");
        ((TextView) findViewById(ah.prompt_edit_text)).setText(stringExtra);
        this.p = (EditText) findViewById(ah.text_edit_text);
        this.p.setText(stringExtra2);
    }

    public void onResume() {
        super.onResume();
        this.p.requestFocus();
        this.p.postDelayed(new s(this), 100);
    }
}
