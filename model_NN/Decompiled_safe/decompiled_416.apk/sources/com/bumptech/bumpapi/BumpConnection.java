package com.bumptech.bumpapi;

import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.bumptech.bumpapi.a.c;
import com.bumptech.bumpapi.a.d;
import com.bumptech.bumpapi.a.f;
import com.bumptech.bumpapi.a.i;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class BumpConnection implements Parcelable, d {
    public static final Parcelable.Creator CREATOR = new al();
    private String Bf;
    private String Bg;
    private String Bh;
    private String Bi;
    private String Bj;
    private String Bk;
    /* access modifiers changed from: private */
    public as Bl;
    private Handler Bm;
    private boolean Bn;
    private f Bo;
    private final String Bp;
    private byte[] Bq;

    /* synthetic */ BumpConnection(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private BumpConnection(Parcel parcel, byte b2) {
        this.Bp = "\r\n--B879ah3wiuh\r\n";
        this.Bf = parcel.readString();
        this.Bg = parcel.readString();
        this.Bh = parcel.readString();
        this.Bi = parcel.readString();
        this.Bj = parcel.readString();
        this.Bk = parcel.readString();
        this.Bn = false;
        this.Bo = f.cY();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byteArrayOutputStream.write(a("bumpid", this.Bf.getBytes("UTF-8")));
            byteArrayOutputStream.write(a("otherbumpid", this.Bi.getBytes("UTF-8")));
            byteArrayOutputStream.write(a("apikey", this.Bg.getBytes("UTF-8")));
            byteArrayOutputStream.write(a("apisession", this.Bj.getBytes("UTF-8")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.Bq = byteArrayOutputStream.toByteArray();
    }

    public BumpConnection(String str, String str2, String str3, String str4, String str5, String str6) {
        this.Bp = "\r\n--B879ah3wiuh\r\n";
        this.Bf = str;
        this.Bg = str2;
        this.Bh = str3;
        this.Bi = str4;
        this.Bj = str5;
        this.Bk = str6;
    }

    private static byte[] a(String str, byte[] bArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        String str2 = "Content-Disposition: form-data; name=\"" + str + "\"\r\n\r\n";
        try {
            byteArrayOutputStream.write("\r\n--B879ah3wiuh\r\n".getBytes("UTF-8"));
            byteArrayOutputStream.write(str2.getBytes("UTF-8"));
            byteArrayOutputStream.write(bArr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream.toByteArray();
    }

    private static byte[] b(InputStream inputStream, int i) {
        byte[] bArr = new byte[i];
        int i2 = i;
        do {
            try {
                i2 -= inputStream.read(bArr, i - i2, i2);
            } catch (Exception e) {
                e.printStackTrace();
                return new byte[0];
            }
        } while (i2 > 0);
        return bArr;
    }

    private void gu() {
        if (this.Bn) {
            this.Bo.cZ();
            return;
        }
        c cVar = new c(this.Bk, 60000, this);
        cVar.bf("api_long_poll");
        cVar.i(this.Bq);
        cVar.bg("B879ah3wiuh");
        this.Bo.a(cVar);
    }

    public final void a(i iVar, InputStream inputStream) {
        if (iVar.gm() != "api_post_data" && new String(b(inputStream, 2)).equals("AA")) {
            try {
                int parseInt = Integer.parseInt(new String(b(inputStream, 8), "UTF-8"));
                try {
                    inputStream.skip(1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String[] split = b.split(new String(b(inputStream, parseInt - 1)), ";");
                if (split == null) {
                    gu();
                    return;
                }
                try {
                    inputStream.skip(1);
                } catch (Exception e2) {
                }
                for (String split2 : split) {
                    String[] split3 = b.split(split2, ",");
                    String str = split3[0];
                    byte[] b2 = b(inputStream, Integer.parseInt(split3[1]));
                    if (str.equals("otherpayload")) {
                        try {
                            if (new String(b2, "ASCII").equals("=+++_>BUMP-GOODBYE-BUMP<_+++=")) {
                                this.Bm.post(new ak(this));
                                return;
                            }
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                        if (this.Bm != null) {
                            this.Bm.post(new aj(this, b2));
                        } else if (this.Bl != null) {
                            this.Bl.c(b2);
                        }
                    } else if (str.equals("othertimedout")) {
                        this.Bm.post(new ai(this));
                        return;
                    }
                }
                gu();
            } catch (Exception e4) {
                e4.printStackTrace();
            }
        }
    }

    public final void a(as asVar) {
        Handler handler = new Handler();
        this.Bl = asVar;
        this.Bm = handler;
        gu();
    }

    public final void a(Exception exc) {
        exc.printStackTrace();
        if (this.Bl != null) {
            this.Bl.cg();
        }
        this.Bo.cZ();
    }

    public int describeContents() {
        return 0;
    }

    public final void disconnect() {
        try {
            j("=+++_>BUMP-GOODBYE-BUMP<_+++=".getBytes("ASCII"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (this.Bl != null) {
            this.Bl.cg();
        }
        this.Bn = true;
    }

    public final void j(byte[] bArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byteArrayOutputStream.write(this.Bq);
            byteArrayOutputStream.write(a("mypayload", bArr));
            byteArrayOutputStream.write(a("nopoll", new String("1").getBytes("UTF-8")));
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("BumpAPI", "Failed to send chunk");
        }
        c cVar = new c(this.Bk, 60000, this);
        cVar.bf("api_post_data");
        cVar.i(byteArrayOutputStream.toByteArray());
        cVar.bg("B879ah3wiuh");
        f.cY().a(cVar);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.Bf);
        parcel.writeString(this.Bg);
        parcel.writeString(this.Bh);
        parcel.writeString(this.Bi);
        parcel.writeString(this.Bj);
        parcel.writeString(this.Bk);
    }
}
