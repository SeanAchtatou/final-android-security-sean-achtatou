package com.bumptech.bumpapi;

import android.view.View;

final class ab implements View.OnClickListener {
    private /* synthetic */ BumpAPI sF;

    ab(BumpAPI bumpAPI) {
        this.sF = bumpAPI;
    }

    public final void onClick(View view) {
        if (view.getId() == ah.bump_logo) {
            BumpAPI.g(this.sF);
        } else if (view.getId() == ah.close_window) {
            this.sF.fq();
        } else if (view.getId() == ah.start_edit_name) {
            BumpAPI.i(this.sF);
        } else if (view.getId() == ah.no_button) {
            BumpAPI.j(this.sF);
        } else if (view.getId() == ah.yes_button) {
            this.sF.xR.confirm();
        }
    }
}
