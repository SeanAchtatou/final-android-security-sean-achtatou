package com.bumptech.bumpapi;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

final class an implements SensorEventListener {
    private /* synthetic */ u yT;

    an(u uVar) {
        this.yT = uVar;
    }

    public final void onAccuracyChanged(Sensor sensor, int i) {
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        float[] fArr = sensorEvent.values;
        int ah = (int) ((this.yT.r.ah("accelpretime") * 89.0d) + 2.0d);
        double ah2 = this.yT.r.ah("accelbumpthresh1");
        double ah3 = this.yT.r.ah("accelbumpthresh2");
        double ah4 = this.yT.r.ah("accelprethresh");
        float f = fArr[0] / 9.80665f;
        float f2 = fArr[1] / 9.80665f;
        float f3 = fArr[2] / 9.80665f;
        double abs = (double) (u.rC * (Math.abs(f - this.yT.rH) + Math.abs(f2 - this.yT.rI) + Math.abs(f3 - this.yT.rJ)));
        float unused = this.yT.rH = f;
        float unused2 = this.yT.rI = f2;
        float unused3 = this.yT.rJ = f3;
        u.rM[u.rW % 100] = (double) this.yT.rH;
        u.rN[u.rW % 100] = (double) this.yT.rI;
        u.rO[u.rW % 100] = (double) this.yT.rJ;
        u.rP[u.rW % 100] = abs;
        u.rR[u.rW % 100] = ((double) System.currentTimeMillis()) / 1000.0d;
        double d = 0.0d;
        double d2 = 0.0d;
        for (int i = -(ah + 2); i < -2; i++) {
            int i2 = (u.rW + i) % 100;
            if (i2 < 0) {
                i2 += 100;
            }
            d += u.rP[i2];
            d2 += 1.0d;
        }
        double d3 = d / d2;
        u.rQ[u.rW % 100] = d3;
        if (((abs > ah2 && d3 < ah4) || abs > ah3) && u.rW > ah + 4 && u.rY > ah + 4 && u.rX > 12) {
            this.yT.r.j("bumptimes", this.yT.r.aj("bumptimes") + " " + b.format(u.rR[u.rW % 100]));
            this.yT.r.ag("bumptimes");
            this.yT.dr();
            u.rS++;
            u.rX = 0;
        }
        u.rW++;
        u.rX++;
        u.rY++;
    }
}
