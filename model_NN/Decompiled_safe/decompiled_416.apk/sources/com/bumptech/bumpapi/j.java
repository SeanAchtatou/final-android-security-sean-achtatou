package com.bumptech.bumpapi;

final class j extends a implements v {
    public j(ar arVar) {
        super(arVar);
    }

    public final boolean V(String str) {
        if (!this.u) {
            return false;
        }
        if (this.r.ai("otheruserconfirms") == 1) {
            this.s.a(ad.MAILBOX_STATE);
            return true;
        } else if (this.r.ai("otheruserconfirms") != 0) {
            return false;
        } else {
            this.t.r(this.r.aj("othername"));
            this.s.a(ad.IDLE_STATE);
            return true;
        }
    }

    public final void c() {
        super.c();
        this.t.o(this.r.aj("othername"));
        if (!V("otheruserconfirms")) {
            this.r.a("otheruserconfirms", this);
        }
    }
}
