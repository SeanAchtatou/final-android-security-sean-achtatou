package com.bumptech.bumpapi;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;

final class ap implements Animation.AnimationListener {
    /* access modifiers changed from: private */
    public AnimationSet AZ;
    /* access modifiers changed from: private */
    public AnimationSet Ba;
    /* access modifiers changed from: private */
    public View Bb;
    /* access modifiers changed from: private */
    public View Bc;

    public ap(View view, AnimationSet animationSet, View view2, AnimationSet animationSet2) {
        this.Bb = view;
        this.AZ = animationSet;
        this.Bc = view2;
        this.Ba = animationSet2;
        this.Ba.setAnimationListener(this);
    }

    public final void onAnimationEnd(Animation animation) {
        if (this.Bc.getAnimation() == this.Ba) {
            this.Bc.postDelayed(new r(this), 1000);
        } else {
            this.Ba.setAnimationListener(null);
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void start() {
        this.Bb.startAnimation(this.AZ);
        this.Bc.startAnimation(this.Ba);
    }
}
