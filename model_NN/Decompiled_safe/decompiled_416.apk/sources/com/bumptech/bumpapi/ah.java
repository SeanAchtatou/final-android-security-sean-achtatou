package com.bumptech.bumpapi;

public final class ah {

    /* renamed from: id */
    public static final int bump_signal = 2131361807;
    public static final int progress = 2131361809;
    public static final int status = 2131361810;

    /* renamed from: xW */
    public static final int api_popup = 2131361799;

    /* renamed from: xX */
    public static final int bump_hand_left = 2131361804;

    /* renamed from: xY */
    public static final int bump_hand_right = 2131361805;

    /* renamed from: xZ */
    public static final int bump_icon = 2131361806;

    /* renamed from: ya */
    public static final int bump_logo = 2131361801;

    /* renamed from: yb */
    public static final int close_window = 2131361802;

    /* renamed from: yc */
    public static final int confirm_prompt = 2131361814;

    /* renamed from: yd */
    public static final int content_layout = 2131361811;

    /* renamed from: ye */
    public static final int edit_text_cancel = 2131361820;

    /* renamed from: yf */
    public static final int edit_text_okay = 2131361821;

    /* renamed from: yg */
    public static final int no_button = 2131361816;

    /* renamed from: yh */
    public static final int notify = 2131361813;

    /* renamed from: yi */
    public static final int notify_bar = 2131361812;

    /* renamed from: yj */
    public static final int prompt_edit_text = 2131361817;

    /* renamed from: yk */
    public static final int start_edit_name = 2131361825;

    /* renamed from: yl */
    public static final int start_prompt = 2131361822;

    /* renamed from: ym */
    public static final int start_user_name = 2131361824;

    /* renamed from: yn */
    public static final int text_edit_text = 2131361818;

    /* renamed from: yo */
    public static final int waiting_prompt = 2131361826;

    /* renamed from: yp */
    public static final int yes_button = 2131361815;
}
