package com.bumptech.bumpapi;

public enum aq {
    FAIL_NONE,
    FAIL_USER_CANCELED,
    FAIL_NETWORK_UNAVAILABLE,
    FAIL_INVALID_AUTHORIZATION
}
