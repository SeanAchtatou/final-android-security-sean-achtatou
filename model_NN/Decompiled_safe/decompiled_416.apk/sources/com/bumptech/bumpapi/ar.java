package com.bumptech.bumpapi;

final class ar implements g, h {
    private a[] BO = new a[6];
    private ad BP;
    private y BQ;
    private c t;

    public ar(c cVar) {
        this.t = cVar;
        this.BO[ad.WARMING_STATE.ordinal()] = new l(this);
        this.BO[ad.IDLE_STATE.ordinal()] = new w(this);
        this.BO[ad.CONNECTING_STATE.ordinal()] = new m(this);
        this.BO[ad.CONFIRM_STATE.ordinal()] = new n(this);
        this.BO[ad.WAITING_STATE.ordinal()] = new j(this);
        this.BO[ad.MAILBOX_STATE.ordinal()] = new d(this);
        a(ad.WARMING_STATE);
    }

    public final void a(ad adVar) {
        if (this.BP != null) {
            this.BO[this.BP.ordinal()].u = false;
        }
        this.BP = adVar;
        this.BO[this.BP.ordinal()].c();
    }

    public final void a(y yVar) {
        this.BQ = yVar;
    }

    public final void b(aq aqVar) {
        if (this.BQ != null) {
            this.BQ.a(aqVar);
        }
    }

    public final void bs() {
        if (this.BP == ad.IDLE_STATE) {
            a(ad.CONNECTING_STATE);
        }
    }

    public final void g(boolean z) {
        if (!z) {
            a(ad.WARMING_STATE);
        } else if (this.BP == ad.WARMING_STATE) {
            a(ad.IDLE_STATE);
        }
    }

    public final c gv() {
        return this.t;
    }

    public final ad gw() {
        return this.BP;
    }
}
