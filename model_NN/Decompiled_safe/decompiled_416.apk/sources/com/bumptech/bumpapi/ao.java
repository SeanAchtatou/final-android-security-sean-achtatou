package com.bumptech.bumpapi;

import java.util.Hashtable;

final class ao {
    private static Hashtable AJ;
    private int AA;
    public byte[] AB;
    public byte[] AC;
    public String AD;
    public double AE;
    public int AF;
    public int AG;
    public int AH;
    public v AI;
    public String Ay;
    public int Az;
    private int persistent;

    static {
        Hashtable hashtable = new Hashtable();
        AJ = hashtable;
        hashtable.put(0, "BT_NONE");
        AJ.put(1, "BT_INT");
        AJ.put(2, "BT_DOUBLE");
        AJ.put(3, "BT_STRING");
        AJ.put(4, "BT_DATA");
        AJ.put(5, "BT_IMAGE");
    }

    ao() {
    }

    public final void L(int i) {
        this.Az = i;
    }

    public final void M(int i) {
        this.AA = i;
    }

    public final void N(int i) {
        this.AF = i;
        this.AI.V(this.Ay);
    }

    public final void O(int i) {
        this.AG = i;
    }

    public final void P(int i) {
        this.AH = i;
    }

    public final void Q(int i) {
        this.persistent = i;
    }

    public final void b(double d) {
        this.AE = d;
        this.AI.V(this.Ay);
    }

    public final void bd(String str) {
        this.Ay = str;
    }

    public final void be(String str) {
        this.AD = str;
        this.AI.V(this.Ay);
    }

    public final String gd() {
        return this.Ay;
    }

    public final void ge() {
        this.AC = null;
        this.AI.V(this.Ay);
    }

    public final String gf() {
        return this.AD;
    }

    public final double gg() {
        return this.AE;
    }

    public final int gh() {
        return this.AF;
    }

    public final int gi() {
        return this.AG;
    }

    public final int gj() {
        return this.AH;
    }

    public final void gk() {
        this.AH = 0;
        this.AA = 0;
        this.Az = 0;
        this.AB = null;
        this.AC = null;
        this.AD = null;
    }

    public final void h(byte[] bArr) {
        this.AB = bArr;
        this.AI.V(this.Ay);
    }
}
