package com.bumptech.bumpapi;

import android.content.Context;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Vibrator;
import java.util.Timer;

public final class u implements v {
    protected static float rC = 0.0f;
    static double[] rM;
    static double[] rN;
    static double[] rO;
    static double[] rP;
    static double[] rQ;
    static double[] rR;
    static int rS = 0;
    private static double rT = 0.0d;
    private static double rU = 0.0d;
    private static double rV = 0.0d;
    static int rW = 0;
    static int rX;
    static int rY;
    /* access modifiers changed from: private */
    public o r = o.cn();
    g rD;
    private boolean rE;
    private final long rF = 30;
    private SensorManager rG;
    /* access modifiers changed from: private */
    public float rH;
    /* access modifiers changed from: private */
    public float rI;
    /* access modifiers changed from: private */
    public float rJ;
    /* access modifiers changed from: private */
    public Vibrator rK;
    /* access modifiers changed from: private */
    public MediaPlayer rL;
    private final SensorEventListener rZ = new an(this);
    private Timer sa;

    public u(Context context) {
        this.rG = (SensorManager) context.getSystemService("sensor");
        this.rK = (Vibrator) context.getSystemService("vibrator");
        this.r.a("bumpsensitivity", this);
        rC = 1.5f;
        this.rL = MediaPlayer.create(context, p.bump_blip);
        this.rE = false;
        m0do();
    }

    /* access modifiers changed from: private */
    public synchronized void dr() {
        if (this.sa != null) {
            this.sa.cancel();
        }
        dq();
        this.sa = new Timer();
        this.sa.schedule(new am(this), 30);
    }

    public final boolean V(String str) {
        rC = (float) this.r.ah("bumpsensitivity");
        return true;
    }

    public final void a(g gVar) {
        this.rD = gVar;
    }

    /* renamed from: do  reason: not valid java name */
    public final void m0do() {
        this.rH = 0.0f;
        this.rI = 0.0f;
        this.rJ = -1.0f;
        rX = 100;
        rY = 0;
        rS = 0;
        rM = new double[100];
        rN = new double[100];
        rO = new double[100];
        rP = new double[100];
        rQ = new double[100];
        rR = new double[100];
    }

    public final void dp() {
        if (!this.rE) {
            this.rE = true;
            m0do();
            this.rG.registerListener(this.rZ, this.rG.getDefaultSensor(1), 0);
        }
    }

    public final void dq() {
        if (this.rE) {
            this.rE = false;
            this.rG.unregisterListener(this.rZ);
        }
    }

    public final void ds() {
        this.r.j("bumptimes", b.format(((double) System.currentTimeMillis()) / 1000.0d));
        this.r.ag("bumptimes");
        dr();
    }
}
