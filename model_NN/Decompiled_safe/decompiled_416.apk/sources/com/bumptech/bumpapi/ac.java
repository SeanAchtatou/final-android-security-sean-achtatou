package com.bumptech.bumpapi;

import android.os.Handler;
import android.os.Message;

final class ac extends Handler {
    private /* synthetic */ BumpAPI sF;

    ac(BumpAPI bumpAPI) {
        this.sF = bumpAPI;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.bumpapi.BumpAPI.a(com.bumptech.bumpapi.BumpAPI, java.lang.CharSequence):void
     arg types: [com.bumptech.bumpapi.BumpAPI, java.lang.String]
     candidates:
      com.bumptech.bumpapi.BumpAPI.a(com.bumptech.bumpapi.BumpAPI, java.lang.String):void
      com.bumptech.bumpapi.BumpAPI.a(com.bumptech.bumpapi.BumpAPI, java.lang.CharSequence):void */
    public final void handleMessage(Message message) {
        try {
            this.sF.xQ.join();
        } catch (InterruptedException e) {
        }
        switch (message.what) {
            case 1:
                BumpAPI.b(this.sF);
                return;
            case 2:
                BumpAPI.c(this.sF);
                return;
            case 3:
                BumpAPI.a(this.sF, (String) message.obj);
                return;
            case 4:
                BumpAPI.d(this.sF);
                return;
            case 5:
                BumpAPI.b(this.sF, (String) message.obj);
                return;
            case 6:
                BumpAPI.c(this.sF, (String) message.obj);
                return;
            case 7:
                this.sF.a((CharSequence) ((String) message.obj));
                return;
            case 8:
                this.sF.aO((String) message.obj);
                return;
            case 9:
                BumpAPI.e(this.sF);
                return;
            default:
                return;
        }
    }
}
