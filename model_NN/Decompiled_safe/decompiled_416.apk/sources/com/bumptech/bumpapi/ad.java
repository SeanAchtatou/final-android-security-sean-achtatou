package com.bumptech.bumpapi;

public enum ad {
    WARMING_STATE,
    IDLE_STATE,
    CONNECTING_STATE,
    CONFIRM_STATE,
    WAITING_STATE,
    MAILBOX_STATE
}
