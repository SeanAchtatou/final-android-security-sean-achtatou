package com.bumptech.bumpapi;

import android.view.View;

final class q implements View.OnClickListener {
    private /* synthetic */ EditTextActivity pl;

    q(EditTextActivity editTextActivity) {
        this.pl = editTextActivity;
    }

    public final void onClick(View view) {
        if (view.getId() == ah.edit_text_cancel) {
            EditTextActivity editTextActivity = this.pl;
            editTextActivity.setResult(0);
            editTextActivity.finish();
        } else if (view.getId() == ah.edit_text_okay) {
            this.pl.b();
        }
    }
}
