package com.bumptech.bumpapi;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import java.util.Hashtable;

final class t implements LocationListener, v {
    private Context aa;
    private Handler handler;
    private o r;
    private int rA = 8;
    private ar ru;
    private ae rv;
    private u rw;
    private Location rx;
    private c ry;
    private y rz;

    public t(Context context, c cVar, y yVar, Handler handler2) {
        this.aa = context;
        this.ry = cVar;
        this.rz = yVar;
        this.handler = handler2;
    }

    public final boolean V(String str) {
        if (this.r.aj("apiauthresponse").equals("fail")) {
            this.rz.a(aq.FAIL_INVALID_AUTHORIZATION);
            return true;
        }
        String aj = this.r.aj("apiauthmessage");
        if (aj.equals("")) {
            this.r.a("apiauthmessage", this);
            return false;
        }
        this.handler.sendMessage(this.handler.obtainMessage(this.rA, aj));
        return true;
    }

    public final void au(String str) {
        if (this.r != null) {
            this.r.j("apikey", str);
        }
    }

    public final void confirm() {
        this.r.f("iconfirm", 1);
        this.r.ag("iconfirm");
        this.ru.a(ad.WAITING_STATE);
    }

    public final void dh() {
        LocationManager locationManager = (LocationManager) this.aa.getSystemService("location");
        if (locationManager.isProviderEnabled("gps")) {
            locationManager.requestLocationUpdates("gps", 250, 10.0f, this, this.aa.getMainLooper());
        }
        if (locationManager.isProviderEnabled("network")) {
            locationManager.requestLocationUpdates("network", 250, 10.0f, this, this.aa.getMainLooper());
        }
        this.r = o.cn();
        this.r.a(new x(this.aa));
        this.r.cp();
        this.r.co();
        this.r.a("apiauthresponse", this);
        this.r.ag("apikey");
        this.r.f("mymodeproposed", 2100);
        this.r.ag("mymodeproposed");
        this.ru = new ar(this.ry);
        this.ru.a(this.rz);
        this.rv = ae.dD();
        this.rv.a(this.ru);
        this.rv.dC();
        this.rv.dG();
        this.rw = new u(this.aa);
        this.rw.a(this.ru);
    }

    public final BumpConnection di() {
        String[] split = this.r.aj("othermodemeta").split("&");
        Hashtable hashtable = new Hashtable();
        for (String split2 : split) {
            String[] split3 = split2.split("=");
            if (split3.length >= 2) {
                hashtable.put(split3[0], split3[1]);
            }
        }
        return new BumpConnection(this.r.aj("bumpid"), this.r.aj("apikey"), this.r.aj("othername"), (String) hashtable.get("otherbumpid"), (String) hashtable.get("session"), (String) hashtable.get("mailboxurl"));
    }

    public final void dj() {
        this.r.f("iconfirm", 0);
        this.r.ag("iconfirm");
        this.ru.a(ad.IDLE_STATE);
    }

    public final boolean dk() {
        return this.rv != null && this.rv.dJ();
    }

    public final void dl() {
        this.rw.ds();
    }

    public final void dm() {
        if (this.rw != null) {
            this.rw.dq();
        }
    }

    public final void dn() {
        if (this.rw != null) {
            this.rw.dp();
        }
    }

    public final void onLocationChanged(Location location) {
        if (location != null) {
            if (this.rx != null) {
                Location location2 = this.rx;
                if (!((location.getLatitude() == location2.getLatitude() && location.getLongitude() == location2.getLongitude() && location.getAccuracy() == location2.getAccuracy() && StrictMath.abs(location.getTime() - location2.getTime()) <= 60000) ? false : true)) {
                    return;
                }
            }
            if (this.r != null) {
                this.r.b("gpslat", location.getLatitude());
                this.r.b("gpslong", location.getLongitude());
                this.r.b("gpsaccuracy", (double) location.getAccuracy());
                this.r.b("gpstime", ((double) location.getTime()) / 1000.0d);
                this.r.f("gpsnew", 1);
                this.r.ag("bumpid");
                this.r.ag("gpslat");
                this.r.ag("gpslong");
                this.r.ag("gpsaccuracy");
                this.r.ag("gpstime");
                this.r.ag("gpsnew");
                this.rx = location;
            }
        }
    }

    public final void onPause() {
        if (this.ru != null && this.ru.gw() == ad.IDLE_STATE) {
            this.rw.dq();
        }
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onResume() {
        if (this.ru != null && this.ru.gw() == ad.IDLE_STATE) {
            this.rw.m0do();
            this.rw.dp();
        }
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }

    public final void release() {
        this.rw.dq();
        this.rw.a((g) null);
        this.rv.dK();
        this.rv.dI();
        this.rv.dH();
        this.rv.a((h) null);
        this.ru.a((y) null);
        this.r.co();
        ((LocationManager) this.aa.getSystemService("location")).removeUpdates(this);
    }

    public final void setName(String str) {
        if (this.r != null) {
            this.r.j("myname", str);
            this.r.ag("myname");
        }
    }
}
