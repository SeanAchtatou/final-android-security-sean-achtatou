package com.bumptech.bumpapi;

import android.os.Parcel;
import android.os.Parcelable;

final class al implements Parcelable.Creator {
    al() {
    }

    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new BumpConnection(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new BumpConnection[i];
    }
}
