package com.mobclix.android.sdk;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.CookieSyncManager;
import b.a.a;
import b.a.b;
import b.a.f;
import java.util.ArrayList;
import java.util.Iterator;

final class ck {
    private b Ce;
    private Bitmap Cf = null;
    private ArrayList Cg = new ArrayList();
    boolean aV = false;
    private String as = "";
    private /* synthetic */ au cV;
    /* access modifiers changed from: private */
    public au fS;
    private ArrayList iG = new ArrayList();
    String lx = "";
    private String ly = "standard";
    String type = "null";

    ck(au auVar, b bVar, au auVar2) {
        this.cV = auVar;
        try {
            this.Ce = bVar;
            this.fS = auVar2;
            this.type = bVar.getString("type");
            if (bVar.G("autoplay")) {
                this.aV = bVar.getBoolean("autoplay");
            }
            try {
                b F = bVar.F("eventUrls");
                a E = F.E("onShow");
                for (int i = 0; i < E.length(); i++) {
                    this.Cg.add(E.getString(i));
                }
                a E2 = F.E("onTouch");
                for (int i2 = 0; i2 < E2.length(); i2++) {
                    this.iG.add(E2.getString(i2));
                }
            } catch (Exception e) {
            }
            if (this.type.equals("url")) {
                try {
                    this.as = bVar.getString("url");
                } catch (f e2) {
                }
                try {
                    this.ly = bVar.getString("browserType");
                } catch (f e3) {
                }
                try {
                    if (bVar.getBoolean("preload")) {
                        this.fS.nH.push(new Thread(new al(this.as, new am(this))));
                    }
                } catch (f e4) {
                }
            } else if (this.type.equals("html")) {
                try {
                    this.as = bVar.getString("baseUrl");
                } catch (f e5) {
                }
                try {
                    this.lx = bVar.getString("html");
                } catch (f e6) {
                }
                try {
                    this.ly = bVar.getString("browserType");
                } catch (f e7) {
                }
            }
        } catch (f e8) {
        }
    }

    ck(au auVar, au auVar2) {
        this.cV = auVar;
        this.fS = auVar2;
    }

    private void gy() {
        try {
            if (!this.as.equals("")) {
                Uri parse = Uri.parse(this.as);
                if (this.as.split("mobclix://").length > 1 || this.as.split("mobclix%3A%2F%2F").length > 1) {
                    this.cV.nS = new Thread(new p(this.cV, this.as));
                    this.cV.nS.start();
                    Iterator it = this.cV.ch.iK.iterator();
                    while (it.hasNext()) {
                        it.next();
                    }
                    return;
                }
                for (int i = 0; i < bw.fm().ff().size(); i++) {
                    if (parse.getHost().equals(bw.fm().ff().get(i))) {
                        this.cV.getContext().startActivity(new Intent("android.intent.action.VIEW", parse));
                        return;
                    }
                }
                if (!this.lx.equals("")) {
                    Intent intent = new Intent();
                    String packageName = this.cV.ch.getContext().getPackageName();
                    try {
                        this.Ce.a("cachedHTML", this.lx);
                    } catch (f e) {
                    }
                    intent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "browser").putExtra(String.valueOf(packageName) + ".data", this.Ce.toString());
                    this.cV.getContext().startActivity(intent);
                } else if (this.ly.equals("minimal")) {
                    Intent intent2 = new Intent();
                    String packageName2 = this.cV.ch.getContext().getPackageName();
                    intent2.setClassName(packageName2, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName2) + ".type", "browser").putExtra(String.valueOf(packageName2) + ".data", this.Ce.toString());
                    this.cV.getContext().startActivity(intent2);
                    return;
                } else {
                    this.cV.getContext().startActivity(new Intent("android.intent.action.VIEW", parse));
                }
                CookieSyncManager.getInstance().sync();
            }
        } catch (Exception e2) {
        }
    }

    public final boolean gx() {
        try {
            Iterator it = this.Cg.iterator();
            while (it.hasNext()) {
                new Thread(new ax((String) it.next(), new bu())).start();
            }
        } catch (Exception e) {
        }
        Iterator it2 = this.cV.ch.iK.iterator();
        while (it2.hasNext()) {
            it2.next();
        }
        au.nP = true;
        if (this.type.equals("url") || this.type.equals("html")) {
            gy();
        } else if (this.type.equals("video")) {
            Intent intent = new Intent();
            String packageName = this.cV.ch.getContext().getPackageName();
            intent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "video").putExtra(String.valueOf(packageName) + ".data", this.Ce.toString());
            this.cV.getContext().startActivity(intent);
        }
        au.nP = false;
        return true;
    }
}
