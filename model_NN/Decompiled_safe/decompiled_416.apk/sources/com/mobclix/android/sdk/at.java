package com.mobclix.android.sdk;

import android.location.Location;
import java.util.TimerTask;

final class at extends TimerTask {
    private /* synthetic */ cg ko;

    at(cg cgVar) {
        this.ko = cgVar;
    }

    public final void run() {
        try {
            Location lastKnownLocation = this.ko.bA ? this.ko.zG.getLastKnownLocation("gps") : null;
            Location lastKnownLocation2 = this.ko.bB ? this.ko.zG.getLastKnownLocation("network") : null;
            if (lastKnownLocation == null || lastKnownLocation2 == null) {
                if (lastKnownLocation != null) {
                    this.ko.zH.a(lastKnownLocation);
                } else if (lastKnownLocation2 != null) {
                    this.ko.zH.a(lastKnownLocation2);
                } else {
                    this.ko.zH.a(null);
                }
            } else if (lastKnownLocation.getTime() > lastKnownLocation2.getTime()) {
                this.ko.zH.a(lastKnownLocation);
            } else {
                this.ko.zH.a(lastKnownLocation2);
            }
        } catch (Exception e) {
        }
    }
}
