package com.mobclix.android.sdk;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

final class s extends ao {
    private /* synthetic */ ap fM;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    s(ap apVar, Object obj) {
        super(obj);
        this.fM = apVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.fM.mB.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse((String) this.mw)));
    }
}
