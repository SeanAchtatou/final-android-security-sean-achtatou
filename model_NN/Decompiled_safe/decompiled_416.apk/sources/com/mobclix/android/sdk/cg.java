package com.mobclix.android.sdk;

import android.content.Context;
import android.location.LocationListener;
import android.location.LocationManager;
import java.util.List;
import java.util.Timer;

final class cg {
    boolean bA = false;
    boolean bB = false;
    Timer zF;
    LocationManager zG;
    z zH;
    LocationListener zI = new af(this);
    LocationListener zJ = new ag(this);

    cg() {
    }

    public final synchronized boolean a(Context context, z zVar) {
        boolean z;
        if (this.zF != null) {
            z = true;
        } else {
            try {
                this.zH = zVar;
                if (this.zG == null) {
                    this.zG = (LocationManager) context.getSystemService("location");
                }
                if (this.zG == null) {
                    z = false;
                } else {
                    if (aX("gps")) {
                        this.bA = this.zG.isProviderEnabled("gps");
                    } else {
                        this.bA = false;
                    }
                    if (aX("network")) {
                        this.bB = this.zG.isProviderEnabled("network");
                    } else {
                        this.bB = false;
                    }
                    if (this.bA || this.bB) {
                        new Thread(new at(this)).run();
                        z = true;
                    } else {
                        z = false;
                    }
                }
            } catch (Exception e) {
            }
        }
        return z;
    }

    public final boolean aX(String str) {
        try {
            List<String> allProviders = this.zG.getAllProviders();
            int i = 0;
            while (i < allProviders.size()) {
                try {
                    if (str.equals(allProviders.get(i))) {
                        return true;
                    }
                    i++;
                } catch (Exception e) {
                    return false;
                }
            }
            return false;
        } catch (Throwable th) {
            return false;
        }
    }

    public final void fN() {
        try {
            if (this.zF != null) {
                this.zF.cancel();
                this.zF.purge();
                this.zF = null;
            }
            if (this.zG != null) {
                if (this.zI != null) {
                    this.zG.removeUpdates(this.zI);
                }
                if (this.zJ != null) {
                    this.zG.removeUpdates(this.zJ);
                }
            }
        } catch (Exception e) {
        }
    }
}
