package com.mobclix.android.sdk;

import android.app.ActivityManager;
import java.util.TimerTask;

final class ay extends TimerTask {
    private /* synthetic */ bw aF;

    /* synthetic */ ay(bw bwVar) {
        this(bwVar, (byte) 0);
    }

    private ay(bw bwVar, byte b2) {
        this.aF = bwVar;
    }

    private boolean cR() {
        try {
            return ((ActivityManager) this.aF.aa.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getPackageName().equals(this.aF.aa.getPackageName());
        } catch (Exception e) {
            return false;
        }
    }

    public final synchronized void run() {
        this.aF.q(cR());
    }
}
