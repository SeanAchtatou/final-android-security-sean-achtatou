package com.mobclix.android.sdk;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import android.widget.RelativeLayout;

final class cb extends x {
    private ImageView pd;

    cb(au auVar) {
        super(auVar);
        try {
            Bitmap decodeStream = BitmapFactory.decodeStream(this.fS.ch.aa.openFileInput(String.valueOf(this.fS.ch.zl) + "_mc_cached_custom_ad.png"));
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            layoutParams.addRule(15);
            this.pd = new ImageView(this.fS.ch.getContext());
            this.pd.setLayoutParams(layoutParams);
            this.pd.setImageBitmap(decodeStream);
            addView(this.pd);
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        } catch (Exception e) {
        }
        aW().nT.sendEmptyMessage(0);
    }
}
