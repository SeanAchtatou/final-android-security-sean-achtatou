package com.mobclix.android.sdk;

import android.view.View;

final class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ y f5a;

    c(y yVar) {
        this.f5a = yVar;
    }

    public final void onClick(View view) {
        if (this.f5a.hS) {
            this.f5a.hQ.setVisibility(8);
            if (!this.f5a.hO.isPlaying()) {
                this.f5a.hO.start();
            }
        }
    }
}
