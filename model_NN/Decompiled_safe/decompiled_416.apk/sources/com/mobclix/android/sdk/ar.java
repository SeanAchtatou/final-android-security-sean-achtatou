package com.mobclix.android.sdk;

import android.text.Html;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import b.a.b;
import b.a.f;

final class ar extends x {
    private int bgColor = -1;
    private String mR = "null";
    private String mS = "null";
    private String mT = "null";
    private String mU = "center";
    private String mV = "";
    private int mW = -16776961;
    private String mX = "center";
    private String mY = "";
    private int mZ = -16776961;
    /* access modifiers changed from: private */
    public ImageView na;
    /* access modifiers changed from: private */
    public ImageView nb;
    private TextView nc;
    private TextView nd;

    ar(b bVar, au auVar) {
        super(auVar);
        try {
            this.bgColor = c(bVar.F("bgColor"));
        } catch (f e) {
        }
        try {
            this.mR = bVar.getString("bgImg");
        } catch (f e2) {
        }
        try {
            this.mS = bVar.getString("leftIcon");
        } catch (f e3) {
        }
        if (this.mS.equals("")) {
            this.mS = "null";
        }
        try {
            this.mT = bVar.getString("rightIcon");
        } catch (f e4) {
        }
        if (this.mT.equals("")) {
            this.mT = "null";
        }
        try {
            b F = bVar.F("headerText");
            try {
                this.mU = F.getString("alignment");
            } catch (f e5) {
            }
            try {
                this.mV = F.getString("text");
            } catch (f e6) {
            }
            if (this.mV.equals("null")) {
                this.mV = "";
            }
            this.mW = c(F.F("color"));
        } catch (f e7) {
        }
        try {
            b F2 = bVar.F("bodyText");
            try {
                this.mX = F2.getString("alignment");
            } catch (f e8) {
            }
            try {
                this.mY = F2.getString("text");
            } catch (f e9) {
            }
            if (this.mY.equals("null")) {
                this.mY = "";
            }
            this.mZ = c(F2.F("color"));
        } catch (f e10) {
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(h(48), h(48));
        layoutParams2.addRule(15);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(h(48), h(48));
        layoutParams3.addRule(15);
        if (this.mS.equals("null") && this.mT.equals("null")) {
            layoutParams.setMargins(h(5), 0, h(5), 0);
        } else if (!this.mS.equals("null") && this.mT.equals("null")) {
            layoutParams.setMargins(h(60), 0, h(5), 0);
            layoutParams2.addRule(9);
            layoutParams2.setMargins(h(5), 0, 0, 0);
        } else if (!this.mS.equals("null") || this.mT.equals("null")) {
            layoutParams.setMargins(h(60), 0, h(60), 0);
            layoutParams2.addRule(9);
            layoutParams2.setMargins(h(5), 0, 0, 0);
            layoutParams3.addRule(11);
            layoutParams3.setMargins(0, 0, h(5), 0);
        } else {
            layoutParams.setMargins(h(5), 0, h(60), 0);
            layoutParams3.addRule(11);
            layoutParams3.setMargins(0, 0, h(5), 0);
        }
        LinearLayout linearLayout = new LinearLayout(this.fS.ch.getContext());
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setGravity(16);
        if (!this.mV.equals("")) {
            this.nc = new TextView(this.fS.ch.getContext());
            this.nc.setGravity(((Integer) this.fR.get(this.mU)).intValue());
            this.nc.setText(Html.fromHtml("<b>" + this.mV + "</b>"));
            this.nc.setTextColor(this.mW);
            linearLayout.addView(this.nc);
        }
        if (!this.mY.equals("")) {
            this.nd = new TextView(this.fS.ch.getContext());
            this.nd.setGravity(((Integer) this.fR.get(this.mX)).intValue());
            this.nd.setText(this.mY);
            this.nd.setTextColor(this.mZ);
            linearLayout.addView(this.nd);
        }
        addView(linearLayout);
        if (!this.mS.equals("null")) {
            this.na = new ImageView(this.fS.ch.getContext());
            this.na.setLayoutParams(layoutParams2);
            addView(this.na);
        }
        if (!this.mT.equals("null")) {
            this.nb = new ImageView(this.fS.ch.getContext());
            this.nb.setLayoutParams(layoutParams3);
            addView(this.nb);
        }
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        setBackgroundColor(this.bgColor);
        if (!this.mS.equals("null")) {
            this.fS.nH.push(new Thread(new ax(this.mS, new bg(this))));
        }
        if (!this.mT.equals("null")) {
            this.fS.nH.push(new Thread(new ax(this.mT, new bh(this))));
        }
        if (!this.mR.equals("null")) {
            this.fS.nH.push(new Thread(new ax(this.mR, new bi(this))));
        }
    }
}
