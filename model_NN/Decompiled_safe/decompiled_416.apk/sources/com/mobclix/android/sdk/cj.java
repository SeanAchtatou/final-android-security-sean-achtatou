package com.mobclix.android.sdk;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

final class cj extends Animation {
    private final float BF;
    private final float BG;
    private final float BH;
    private final float BI;
    private final float BJ = 0.0f;
    private final boolean BK;
    private Camera BL;
    private /* synthetic */ au cV;

    public cj(au auVar, float f, float f2, float f3, float f4, boolean z) {
        this.cV = auVar;
        this.BF = f;
        this.BG = f2;
        this.BH = f3;
        this.BI = f4;
        this.BK = z;
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f, Transformation transformation) {
        float f2 = this.BF;
        float f3 = f2 + ((this.BG - f2) * f);
        float f4 = this.BH;
        float f5 = this.BI;
        Camera camera = this.BL;
        Matrix matrix = transformation.getMatrix();
        camera.save();
        if (this.BK) {
            camera.translate(0.0f, 0.0f, this.BJ * f);
        } else {
            camera.translate(0.0f, 0.0f, this.BJ * (1.0f - f));
        }
        camera.rotateY(f3);
        camera.getMatrix(matrix);
        camera.restore();
        matrix.preTranslate(-f4, -f5);
        matrix.postTranslate(f4, f5);
    }

    public final void initialize(int i, int i2, int i3, int i4) {
        super.initialize(i, i2, i3, i4);
        this.BL = new Camera();
    }
}
