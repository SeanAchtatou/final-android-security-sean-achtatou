package com.mobclix.android.sdk;

import android.app.Activity;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.ArcShape;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import b.a.b;
import b.a.f;

final class ak extends RelativeLayout {
    private String as;
    /* access modifiers changed from: private */
    public Activity av;
    final /* synthetic */ MobclixBrowserActivity cC;
    /* access modifiers changed from: private */
    public WebView hc;
    private String lx = "";
    private String ly = "standard";

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ak(MobclixBrowserActivity mobclixBrowserActivity, Activity activity, String str) {
        super(activity);
        this.cC = mobclixBrowserActivity;
        this.av = activity;
        try {
            b bVar = new b(str);
            try {
                this.as = bVar.getString("url");
            } catch (Exception e) {
                this.as = "http://www.mobclix.com";
            }
            try {
                this.lx = bVar.getString("cachedHTML");
            } catch (Exception e2) {
                this.lx = "";
            }
            try {
                this.ly = bVar.getString("browserType");
            } catch (Exception e3) {
                this.ly = "standard";
            }
        } catch (f e4) {
            this.as = "http://www.mobclix.com";
        }
        this.hc = new WebView(activity);
        mobclixBrowserActivity.setProgressBarVisibility(true);
        this.hc.getSettings().setJavaScriptEnabled(true);
        this.hc.setWebViewClient(new u(this));
        this.hc.setWebChromeClient(new v(this));
        this.hc.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        if (this.lx.equals("")) {
            this.hc.loadUrl(this.as);
        } else {
            this.hc.loadDataWithBaseURL(this.as, this.lx, "text/html", "utf-8", null);
        }
        addView(this.hc);
        if (this.ly.equals("minimal")) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(MobclixBrowserActivity.a(mobclixBrowserActivity, 30), MobclixBrowserActivity.a(mobclixBrowserActivity, 30));
            layoutParams.setMargins(MobclixBrowserActivity.a(mobclixBrowserActivity, 5), MobclixBrowserActivity.a(mobclixBrowserActivity, 5), 0, 0);
            ImageView imageView = new ImageView(mobclixBrowserActivity);
            imageView.setLayoutParams(layoutParams);
            imageView.setImageResource(17301594);
            ShapeDrawable shapeDrawable = new ShapeDrawable(new ArcShape(0.0f, 360.0f));
            shapeDrawable.setPadding(MobclixBrowserActivity.a(mobclixBrowserActivity, -7), MobclixBrowserActivity.a(mobclixBrowserActivity, -7), MobclixBrowserActivity.a(mobclixBrowserActivity, -7), MobclixBrowserActivity.a(mobclixBrowserActivity, -7));
            imageView.setBackgroundDrawable(shapeDrawable);
            imageView.setOnClickListener(new w(this));
            addView(imageView);
        }
    }
}
