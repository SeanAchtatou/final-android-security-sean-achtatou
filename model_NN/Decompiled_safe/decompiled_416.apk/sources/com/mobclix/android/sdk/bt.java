package com.mobclix.android.sdk;

import android.app.ActivityManager;
import android.os.Handler;
import java.net.URLEncoder;
import java.util.Iterator;

final class bt extends al {
    private /* synthetic */ ce ab;
    String wr = "";

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    bt(ce ceVar, Handler handler) {
        super("", handler);
        this.ab = ceVar;
    }

    private String eN() {
        String i = this.ab.mz.i(this.ab.mz.h(this.ab.zd, aq.mD), "build_request");
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        String str = "";
        StringBuffer stringBuffer3 = new StringBuffer();
        try {
            String i2 = this.ab.mz.i(i, "ad_feed_id_params");
            stringBuffer.append(this.ab.ar.fe());
            stringBuffer.append("?p=android");
            if (this.ab.zp.equals("")) {
                stringBuffer.append("&i=").append(URLEncoder.encode(this.ab.ar.eO(), "UTF-8"));
                stringBuffer.append("&s=").append(URLEncoder.encode(this.ab.zl, "UTF-8"));
            } else {
                stringBuffer.append("&a=").append(URLEncoder.encode(this.ab.zp, "UTF-8"));
            }
            String i3 = this.ab.mz.i(this.ab.mz.af(i2), "software_env");
            stringBuffer.append("&av=").append(URLEncoder.encode(this.ab.ar.eR(), "UTF-8"));
            stringBuffer.append("&u=").append(URLEncoder.encode(this.ab.ar.getDeviceId(), "UTF-8"));
            stringBuffer.append("&andid=").append(URLEncoder.encode(this.ab.ar.eS(), "UTF-8"));
            stringBuffer.append("&v=").append(URLEncoder.encode(bw.fc()));
            stringBuffer.append("&ct=").append(URLEncoder.encode(this.ab.ar.eV()));
            i = this.ab.mz.i(this.ab.mz.af(i3), "hardware_env");
            stringBuffer.append("&dm=").append(URLEncoder.encode(this.ab.ar.eT(), "UTF-8"));
            stringBuffer.append("&hwdm=").append(URLEncoder.encode(this.ab.ar.eU(), "UTF-8"));
            stringBuffer.append("&sv=").append(URLEncoder.encode(this.ab.ar.eQ(), "UTF-8"));
            stringBuffer.append("&ua=").append(URLEncoder.encode(this.ab.ar.fh(), "UTF-8"));
            if (this.ab.ar.fi()) {
                if (this.ab.ar.fj()) {
                    stringBuffer.append("&jb=1");
                } else {
                    stringBuffer.append("&jb=0");
                }
            }
            String i4 = this.ab.mz.i(this.ab.mz.af(i), "ad_view_state_id_params");
            stringBuffer.append("&o=").append(this.ab.zt);
            this.ab.zt++;
            if (this.ab.zg == 1) {
                stringBuffer.append("&ap=1");
            } else {
                stringBuffer.append("&ap=0");
            }
            if (this.ab.zo != null && !this.ab.zo.equals("")) {
                stringBuffer.append("&as=").append(URLEncoder.encode(this.ab.zo));
            }
            if (this.ab.zf) {
                stringBuffer.append("&t=1");
            }
            String i5 = this.ab.mz.i(this.ab.mz.af(i4), "geo_lo");
            if (!this.ab.ar.eY().equals("null")) {
                stringBuffer.append("&ll=").append(URLEncoder.encode(this.ab.ar.eY(), "UTF-8"));
            }
            stringBuffer.append("&l=").append(URLEncoder.encode(this.ab.ar.eZ(), "UTF-8"));
            String i6 = this.ab.mz.i(this.ab.mz.af(i5), "keywords");
            try {
                Iterator it = this.ab.iK.iterator();
                while (it.hasNext()) {
                    String str2 = ((l) it.next()) != null ? null : str;
                    if (str2 == null) {
                        str2 = "";
                    }
                    if (!str2.equals("")) {
                        if (stringBuffer2.length() == 0) {
                            stringBuffer2.append("&k=").append(URLEncoder.encode(str2, "UTF-8"));
                        } else {
                            stringBuffer2.append("%2C").append(URLEncoder.encode(str2, "UTF-8"));
                        }
                    }
                    if (!"".equals("")) {
                        if (stringBuffer3.length() == 0) {
                            stringBuffer3.append("&q=").append(URLEncoder.encode("", "UTF-8"));
                            str = str2;
                        } else {
                            stringBuffer3.append("%2B").append(URLEncoder.encode("", "UTF-8"));
                        }
                    }
                    str = str2;
                }
                if (stringBuffer2.length() > 0) {
                    stringBuffer.append(stringBuffer2);
                }
                String i7 = this.ab.mz.i(this.ab.mz.af(i6), "query");
                if (stringBuffer3.length() > 0) {
                    stringBuffer.append(stringBuffer3);
                }
                String i8 = this.ab.mz.i(this.ab.mz.af(i7), "additional_params");
                if (this.ab.zk != null && !this.ab.zk.equals("")) {
                    stringBuffer.append("&adurl=").append(URLEncoder.encode(this.ab.zk, "UTF-8"));
                }
                if (!this.wr.equals("")) {
                    stringBuffer.append(this.wr);
                }
                this.wr = "";
                String af = this.ab.mz.af(this.ab.mz.af(i8));
                this.ab.mz.a(stringBuffer.toString(), "request_url", this.ab.zd);
                return stringBuffer.toString();
            } catch (Exception e) {
                i = i6;
                this.ab.mz.af(this.ab.mz.af(i));
                this.ab.mz.ae(this.ab.zd);
                return "";
            }
        } catch (Exception e2) {
            this.ab.mz.af(this.ab.mz.af(i));
            this.ab.mz.ae(this.ab.zd);
            return "";
        }
    }

    public final void run() {
        try {
            if (!((ActivityManager) this.ab.aa.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getPackageName().equals(this.ab.aa.getPackageName())) {
                this.ab.zt = 0;
                this.ab.fK();
                this.ab.ar.xe.fN();
                return;
            }
        } catch (Exception e) {
        }
        setUrl(eN());
        super.run();
    }
}
