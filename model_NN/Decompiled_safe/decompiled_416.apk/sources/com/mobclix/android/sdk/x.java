package com.mobclix.android.sdk;

import android.graphics.Color;
import android.widget.RelativeLayout;
import b.a.b;
import b.a.f;
import java.util.HashMap;

class x extends RelativeLayout {
    protected HashMap fR = new HashMap();
    protected au fS = null;

    x(au auVar) {
        super(auVar.getContext());
        this.fR.put("center", 17);
        this.fR.put("left", 19);
        this.fR.put("right", 21);
        this.fS = auVar;
    }

    public static int c(b bVar) {
        try {
            return Color.argb(bVar.getInt("a"), bVar.getInt("r"), bVar.getInt("g"), bVar.getInt("b"));
        } catch (f e) {
            return 0;
        }
    }

    public final au aW() {
        return this.fS;
    }

    /* access modifiers changed from: package-private */
    public final int h(int i) {
        return (int) (this.fS.ch.c * ((float) i));
    }
}
