package com.mobclix.android.sdk;

import android.app.Activity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import java.util.Iterator;

final class bs extends r {
    private /* synthetic */ MobclixBrowserActivity cC;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    bs(MobclixBrowserActivity mobclixBrowserActivity, Activity activity) {
        super(mobclixBrowserActivity, activity);
        this.cC = mobclixBrowserActivity;
        ((Activity) this.aQ.getContext()).getWindow().setFlags(1024, 1024);
        this.fy.removeView(this.aQ);
        this.aQ.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.aQ.ck.aT = true;
        addView(this.aQ);
    }

    public final synchronized void exit(int i) {
        if (bw.fm().aQ != null) {
            this.aQ.ck.aS = null;
            bw.fm().aQ = null;
            this.fA = false;
            Window window = ((Activity) this.aQ.getContext()).getWindow();
            WindowManager.LayoutParams attributes = window.getAttributes();
            attributes.flags = this.fz;
            window.setAttributes(attributes);
            this.cC.unregisterReceiver(this.cC.k);
            if (this.aQ.ci != null) {
                Iterator it = this.aQ.ci.iK.iterator();
                while (it.hasNext()) {
                    it.next();
                }
            }
            this.aQ.ck.x();
            this.av.finish();
        }
    }
}
