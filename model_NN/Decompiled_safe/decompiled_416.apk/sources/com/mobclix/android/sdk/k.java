package com.mobclix.android.sdk;

import android.app.Activity;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import b.a.b;
import b.a.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class k implements LocationListener {
    private static String TAG = "MobclixJavascriptInterface";
    /* access modifiers changed from: private */
    public n aQ;
    private Activity aR;
    r aS;
    boolean aT = false;
    private boolean aU = false;
    private boolean aV = true;
    private boolean aW = false;
    private boolean aX = false;
    private String aY = null;
    private HashMap aZ = new HashMap();
    private bw ar = bw.fm();
    private boolean bA = false;
    private boolean bB = false;
    private final int bC = 75;
    private boolean bD = false;
    private String bE = null;
    private String bF = null;
    private String bG = null;
    private String bH = null;
    private String bI = null;
    private final int ba = 0;
    private final int bb = 1;
    private final int bc = 2;
    private final int bd = 3;
    private final cc be = cc.fs();
    private Camera bf = null;
    private String bg = "null";
    private boolean bh = false;
    int bi;
    int bj;
    private String bk;
    Thread bl;
    /* access modifiers changed from: private */
    public String bm = "";
    private String bn = "";
    private String bo = "";
    private String bp = "";
    private final int bq = 1337;
    private final int br = 1338;
    private SensorManager bs;
    private List bt = new ArrayList();
    private HashMap bu = new HashMap();
    private HashMap bv = new HashMap();
    private LocationManager bw = null;
    private float bx = 0.0f;
    private String by = "";
    private boolean bz = false;
    private int duration = 500;

    k(n nVar, boolean z) {
        this.aQ = nVar;
        this.aR = (Activity) this.aQ.getContext();
        this.aU = z;
        try {
            this.aW = this.aQ.ch.fJ();
            this.aV = this.aQ.ch.fI();
        } catch (Exception e) {
        }
        if (z) {
            this.aV = true;
            this.aW = false;
        }
        this.bs = (SensorManager) this.aR.getSystemService("sensor");
        for (Sensor type : this.bs.getSensorList(-1)) {
            this.bt.add(Integer.valueOf(type.getType()));
        }
        this.aZ.put(0, -1);
        this.aZ.put(1, -1);
        this.aZ.put(2, -1);
        this.aZ.put(3, -1);
        this.bw = (LocationManager) this.aR.getSystemService("location");
        if (this.ar.xe.aX("gps")) {
            this.bA = this.bw.isProviderEnabled("gps");
        } else {
            this.bA = false;
        }
        if (this.ar.xe.aX("network")) {
            this.bB = this.bw.isProviderEnabled("network");
        } else {
            this.bB = false;
        }
    }

    static String a(String str, String str2, boolean z) {
        StringBuilder sb = new StringBuilder("javascript:");
        sb.append(str);
        if (z) {
            sb.append("(\"");
        } else {
            sb.append("(");
        }
        sb.append(str2);
        if (z) {
            sb.append("\");");
        } else {
            sb.append(");");
        }
        return sb.toString();
    }

    private void a(Location location) {
        try {
            if (!this.by.equals("")) {
                b bVar = new b();
                bVar.a("altitude", location.getAltitude());
                b bVar2 = new b();
                bVar2.a("latitude", location.getLatitude());
                bVar2.a("longitude", location.getLongitude());
                bVar.a("coordinate", bVar2);
                bVar.a("course", (double) location.getBearing());
                bVar.a("speed", (double) location.getSpeed());
                bVar.a("timestamp", location.getTime());
                bVar.a("horizontalAccuracy", (double) location.getAccuracy());
                bVar.a("verticalAccuracy", (double) location.getAccuracy());
                this.aQ.loadUrl(a(this.by, "eval('(" + bVar.toString() + ")')", false));
            }
        } catch (f e) {
        }
    }

    private synchronized void s() {
        if (!this.ar.xq.containsKey("android.permission.CAMERA")) {
            throw new Exception("Application does not have the CAMERA permission.");
        }
        if (this.bf != null) {
            this.bf.release();
        }
        this.bf = Camera.open();
        Camera.Parameters parameters = this.bf.getParameters();
        if (this.bg.equals("null")) {
            try {
                this.bg = (String) Camera.Parameters.class.getMethod("getFlashMode", new Class[0]).invoke(parameters, new Object[0]);
            } catch (Exception e) {
            }
        }
        try {
            Camera.Parameters.class.getMethod("setFlashMode", String.class).invoke(parameters, "torch");
        } catch (Exception e2) {
        }
        this.bf.setParameters(parameters);
        this.bh = true;
    }

    private synchronized void t() {
        if (!this.ar.xq.containsKey("android.permission.CAMERA")) {
            throw new Exception("Application does not have the CAMERA permission.");
        } else if (this.bf == null) {
            throw new Exception("Flashlight isn't on.");
        } else {
            Camera.Parameters parameters = this.bf.getParameters();
            String str = "auto";
            if (!this.bg.equals("null")) {
                str = this.bg;
            }
            try {
                Camera.Parameters.class.getMethod("setFlashMode", String.class).invoke(parameters, str);
            } catch (Exception e) {
            }
            this.bf.setParameters(parameters);
            this.bf.release();
            this.bf = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void A() {
        if (this.bI != null) {
            this.aQ.loadUrl(a(this.bI, "", false));
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void g(String str) {
        if (this.bm != null) {
            this.aQ.loadUrl(a(this.bm, str, true));
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void h(String str) {
        if (this.bn != null) {
            this.aQ.loadUrl(a(this.bn, str, true));
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void i(String str) {
        if (this.bp != null) {
            this.aQ.loadUrl(a(this.bp, str, true));
        }
    }

    /* access modifiers changed from: package-private */
    public final void j(String str) {
        if (this.bl == null) {
            this.bl = new Thread(new aa(this, this.bk, str));
            this.bl.run();
        } else if (this.bn != null) {
            this.aQ.loadUrl(a(this.bn, "Image already being sent.", true));
        }
    }

    public final void onLocationChanged(Location location) {
        Location lastKnownLocation = this.bA ? this.bw.getLastKnownLocation("gps") : null;
        Location lastKnownLocation2 = this.bB ? this.bw.getLastKnownLocation("network") : null;
        if (lastKnownLocation == null || lastKnownLocation2 == null) {
            if (lastKnownLocation != null) {
                a(lastKnownLocation);
            } else if (lastKnownLocation2 != null) {
                a(lastKnownLocation2);
            }
        } else if (lastKnownLocation.getTime() > lastKnownLocation2.getTime()) {
            a(lastKnownLocation);
        } else {
            a(lastKnownLocation2);
        }
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }

    /* access modifiers changed from: package-private */
    public final synchronized void q() {
        if (!this.bD) {
            for (Map.Entry value : this.bu.entrySet()) {
                try {
                    this.bs.unregisterListener((SensorEventListener) value.getValue());
                } catch (Exception e) {
                }
            }
            try {
                this.bw.removeUpdates(this);
            } catch (Exception e2) {
            }
            try {
                t();
            } catch (Exception e3) {
            }
            this.bD = true;
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void r() {
        if (this.bD) {
            for (Map.Entry entry : this.bu.entrySet()) {
                try {
                    this.bs.unregisterListener((SensorEventListener) entry.getValue());
                } catch (Exception e) {
                }
                try {
                    if (((Integer) entry.getKey()).intValue() == 1337) {
                        this.bs.registerListener((SensorEventListener) entry.getValue(), this.bs.getDefaultSensor(1), ((Integer) this.bv.get(entry.getKey())).intValue());
                    } else {
                        this.bs.registerListener((SensorEventListener) entry.getValue(), this.bs.getDefaultSensor(((Integer) entry.getKey()).intValue()), ((Integer) this.bv.get(entry.getKey())).intValue());
                    }
                } catch (Exception e2) {
                }
            }
            try {
                if (this.bz) {
                    if (this.bA) {
                        this.bw.requestLocationUpdates("gps", 0, this.bx, this);
                    }
                    if (this.bB) {
                        this.bw.requestLocationUpdates("network", 0, this.bx, this);
                    }
                }
            } catch (Exception e3) {
            }
            try {
                if (this.bh) {
                    s();
                }
            } catch (Exception e4) {
            }
            this.bD = false;
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void u() {
        if (this.bo != null) {
            this.aQ.loadUrl(a(this.bo, "", false));
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void v() {
        Activity activity = this.aR;
        if (this.aS != null) {
            this.aS.fB = true;
        }
        cc ccVar = this.be;
        activity.getContentResolver();
        b fv = ccVar.fv();
        if (fv == null && this.bp != null) {
            this.aQ.loadUrl(a(this.bp, "Error getting contact.", true));
        } else if (this.bo != null) {
            this.aQ.loadUrl(a(this.bo, "eval('(" + bz.aN(fv.toString()) + ")')", false));
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void w() {
        if (this.bE != null) {
            this.aQ.loadUrl(a(this.bE, "", false));
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void x() {
        if (this.bF != null) {
            this.aQ.loadUrl(a(this.bF, "", false));
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void y() {
        if (this.bG != null) {
            this.aQ.loadUrl(a(this.bG, "", false));
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void z() {
        if (this.bH != null) {
            this.aQ.loadUrl(a(this.bH, "", false));
        }
    }
}
