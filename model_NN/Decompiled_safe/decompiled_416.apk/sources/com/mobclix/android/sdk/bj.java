package com.mobclix.android.sdk;

import android.content.DialogInterface;
import android.webkit.JsPromptResult;
import android.widget.EditText;

final class bj extends ao {
    private /* synthetic */ ba si;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    bj(ba baVar, Object obj, Object obj2) {
        super(obj, obj2);
        this.si = baVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        ((JsPromptResult) this.mw).confirm(((EditText) this.mx).getText().toString());
    }
}
