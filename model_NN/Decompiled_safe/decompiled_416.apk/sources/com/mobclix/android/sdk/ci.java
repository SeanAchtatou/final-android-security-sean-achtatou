package com.mobclix.android.sdk;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

final class ci implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ y f6a;
    private Context aa;
    private String as;

    public ci(y yVar, Context context, String str) {
        this.f6a = yVar;
        this.aa = context;
        this.as = str;
    }

    public final void onClick(View view) {
        this.aa.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.as)));
    }
}
