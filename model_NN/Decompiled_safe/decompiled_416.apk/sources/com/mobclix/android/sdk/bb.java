package com.mobclix.android.sdk;

import android.widget.RelativeLayout;
import b.a.b;
import java.util.Iterator;

final class bb extends x {
    String at = null;
    RelativeLayout rs;
    private String rt = "openadmob";

    bb(b bVar, au auVar) {
        super(auVar);
        try {
            this.rt = bVar.getString("network");
        } catch (Exception e) {
        }
        try {
            StringBuffer stringBuffer = new StringBuffer();
            b F = bVar.F("params");
            Iterator bA = F.bA();
            while (bA.hasNext()) {
                String obj = bA.next().toString();
                String obj2 = F.get(obj).toString();
                stringBuffer.append("&").append(obj);
                stringBuffer.append("=").append(obj2);
            }
            this.at = stringBuffer.toString();
        } catch (Exception e2) {
        }
        char c = this.rt.equals("openadmob") ? 64786 : this.rt.equals("opengoogle") ? (char) 55436 : 64530;
        Iterator it = this.fS.ch.iK.iterator();
        while (it.hasNext()) {
            it.next();
        }
        if (c == 64786) {
            dg();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x007d A[Catch:{ Exception -> 0x0115 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a0 A[Catch:{ Exception -> 0x0115 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00e5 A[SYNTHETIC, Splitter:B:23:0x00e5] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void dg() {
        /*
            r7 = this;
            r6 = 0
            java.lang.String r0 = "com.admob.android.ads.AdView"
            java.lang.Class r1 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x0122 }
            r0 = 1
            java.lang.Class[] r0 = new java.lang.Class[r0]     // Catch:{ Exception -> 0x0122 }
            r2 = 0
            java.lang.Class<android.app.Activity> r3 = android.app.Activity.class
            r0[r2] = r3     // Catch:{ Exception -> 0x0122 }
            java.lang.reflect.Constructor r2 = r1.getConstructor(r0)     // Catch:{ Exception -> 0x0122 }
            r0 = 1
            java.lang.Object[] r3 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x0122 }
            r4 = 0
            com.mobclix.android.sdk.au r0 = r7.fS     // Catch:{ Exception -> 0x0122 }
            android.content.Context r0 = r0.getContext()     // Catch:{ Exception -> 0x0122 }
            android.app.Activity r0 = (android.app.Activity) r0     // Catch:{ Exception -> 0x0122 }
            r3[r4] = r0     // Catch:{ Exception -> 0x0122 }
            java.lang.Object r0 = r2.newInstance(r3)     // Catch:{ Exception -> 0x0122 }
            android.widget.RelativeLayout r0 = (android.widget.RelativeLayout) r0     // Catch:{ Exception -> 0x0122 }
            r7.rs = r0     // Catch:{ Exception -> 0x0122 }
            java.lang.String r0 = "com.admob.android.ads.AdListener"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x00d7 }
            com.mobclix.android.sdk.br r2 = new com.mobclix.android.sdk.br     // Catch:{ Exception -> 0x0120 }
            r2.<init>(r7)     // Catch:{ Exception -> 0x0120 }
            java.lang.ClassLoader r3 = r0.getClassLoader()     // Catch:{ Exception -> 0x0120 }
            r4 = 1
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x0120 }
            r5 = 0
            r4[r5] = r0     // Catch:{ Exception -> 0x0120 }
            java.lang.Object r2 = java.lang.reflect.Proxy.newProxyInstance(r3, r4, r2)     // Catch:{ Exception -> 0x0120 }
            java.lang.Object r2 = r0.cast(r2)     // Catch:{ Exception -> 0x0120 }
        L_0x0046:
            java.lang.String r3 = "setAdListener"
            r4 = 1
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x0115 }
            r5 = 0
            r4[r5] = r0     // Catch:{ Exception -> 0x0115 }
            java.lang.reflect.Method r0 = r1.getMethod(r3, r4)     // Catch:{ Exception -> 0x0115 }
            android.widget.RelativeLayout r3 = r7.rs     // Catch:{ Exception -> 0x0115 }
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0115 }
            r5 = 0
            r4[r5] = r2     // Catch:{ Exception -> 0x0115 }
            r0.invoke(r3, r4)     // Catch:{ Exception -> 0x0115 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0115 }
            r2.<init>()     // Catch:{ Exception -> 0x0115 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0115 }
            r3.<init>()     // Catch:{ Exception -> 0x0115 }
            com.mobclix.android.sdk.au r0 = r7.fS     // Catch:{ Exception -> 0x0115 }
            com.mobclix.android.sdk.ce r0 = r0.ch     // Catch:{ Exception -> 0x0115 }
            java.util.HashSet r0 = r0.iK     // Catch:{ Exception -> 0x0115 }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ Exception -> 0x0115 }
        L_0x0071:
            boolean r0 = r4.hasNext()     // Catch:{ Exception -> 0x0115 }
            if (r0 != 0) goto L_0x00e5
            int r0 = r2.length()     // Catch:{ Exception -> 0x0115 }
            if (r0 <= 0) goto L_0x009a
            java.lang.String r0 = "setKeywords"
            r4 = 1
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x0115 }
            r5 = 0
            java.lang.Class<java.lang.String> r6 = java.lang.String.class
            r4[r5] = r6     // Catch:{ Exception -> 0x0115 }
            java.lang.reflect.Method r0 = r1.getMethod(r0, r4)     // Catch:{ Exception -> 0x0115 }
            android.widget.RelativeLayout r4 = r7.rs     // Catch:{ Exception -> 0x0115 }
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0115 }
            r6 = 0
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0115 }
            r5[r6] = r2     // Catch:{ Exception -> 0x0115 }
            r0.invoke(r4, r5)     // Catch:{ Exception -> 0x0115 }
        L_0x009a:
            int r0 = r3.length()     // Catch:{ Exception -> 0x0115 }
            if (r0 <= 0) goto L_0x00bd
            java.lang.String r0 = "setSearchQuery"
            r2 = 1
            java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x0115 }
            r4 = 0
            java.lang.Class<java.lang.String> r5 = java.lang.String.class
            r2[r4] = r5     // Catch:{ Exception -> 0x0115 }
            java.lang.reflect.Method r0 = r1.getMethod(r0, r2)     // Catch:{ Exception -> 0x0115 }
            android.widget.RelativeLayout r1 = r7.rs     // Catch:{ Exception -> 0x0115 }
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0115 }
            r4 = 0
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0115 }
            r2[r4] = r3     // Catch:{ Exception -> 0x0115 }
            r0.invoke(r1, r2)     // Catch:{ Exception -> 0x0115 }
        L_0x00bd:
            android.widget.RelativeLayout r0 = r7.rs
            com.mobclix.android.sdk.bn r1 = new com.mobclix.android.sdk.bn
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            android.widget.RelativeLayout r0 = r7.rs
            r1 = 4
            r0.setVisibility(r1)
            com.mobclix.android.sdk.au r0 = r7.fS
            com.mobclix.android.sdk.ce r0 = r0.ch
            android.widget.RelativeLayout r1 = r7.rs
            r0.addView(r1)
        L_0x00d6:
            return
        L_0x00d7:
            r0 = move-exception
            r0 = r6
        L_0x00d9:
            com.mobclix.android.sdk.au r2 = r7.fS
            com.mobclix.android.sdk.ce r2 = r2.ch
            java.lang.String r3 = r7.at
            r2.aV(r3)
            r2 = r6
            goto L_0x0046
        L_0x00e5:
            java.lang.Object r0 = r4.next()     // Catch:{ Exception -> 0x0115 }
            com.mobclix.android.sdk.l r0 = (com.mobclix.android.sdk.l) r0     // Catch:{ Exception -> 0x0115 }
            if (r0 == 0) goto L_0x0071
            java.lang.String r0 = ""
            java.lang.String r5 = ""
            boolean r5 = r0.equals(r5)     // Catch:{ Exception -> 0x0115 }
            if (r5 != 0) goto L_0x0100
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x0115 }
            java.lang.String r5 = " "
            r0.append(r5)     // Catch:{ Exception -> 0x0115 }
        L_0x0100:
            java.lang.String r0 = ""
            java.lang.String r5 = ""
            boolean r5 = r0.equals(r5)     // Catch:{ Exception -> 0x0115 }
            if (r5 != 0) goto L_0x0071
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x0115 }
            java.lang.String r5 = " "
            r0.append(r5)     // Catch:{ Exception -> 0x0115 }
            goto L_0x0071
        L_0x0115:
            r0 = move-exception
            com.mobclix.android.sdk.au r0 = r7.fS
            com.mobclix.android.sdk.ce r0 = r0.ch
            java.lang.String r1 = r7.at
            r0.aV(r1)
            goto L_0x00bd
        L_0x0120:
            r2 = move-exception
            goto L_0x00d9
        L_0x0122:
            r0 = move-exception
            goto L_0x00d6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.bb.dg():void");
    }
}
