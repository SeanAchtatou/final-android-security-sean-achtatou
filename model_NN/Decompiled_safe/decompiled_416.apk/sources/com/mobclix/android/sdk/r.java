package com.mobclix.android.sdk;

import android.app.Activity;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.widget.LinearLayout;

class r extends LinearLayout {
    n aQ;
    Activity av;
    private /* synthetic */ MobclixBrowserActivity cC;
    boolean fA = true;
    boolean fB = false;
    ViewGroup fy;
    int fz;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    r(MobclixBrowserActivity mobclixBrowserActivity, Activity activity) {
        super(activity);
        this.cC = mobclixBrowserActivity;
        this.av = activity;
        this.aQ = bw.fm().aQ;
        this.aQ.ck.aS = this;
        this.fz = ((Activity) this.aQ.getContext()).getWindow().getAttributes().flags;
        this.av.getWindowManager().getDefaultDisplay().getMetrics(new DisplayMetrics());
        this.av.setRequestedOrientation((this.av.getWindowManager().getDefaultDisplay().getOrientation() + 1) % 2);
        setBackgroundColor(Color.argb(128, 256, 256, 256));
        setOrientation(1);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.fy = (ViewGroup) this.aQ.getParent();
    }

    public synchronized void exit(int i) {
    }
}
