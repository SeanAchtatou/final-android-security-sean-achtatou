package com.mobclix.android.sdk;

import android.widget.ImageView;
import android.widget.RelativeLayout;

final class aw extends x {
    private String pc;
    /* access modifiers changed from: private */
    public ImageView pd = new ImageView(this.fS.ch.getContext());

    aw(String str, au auVar) {
        super(auVar);
        this.pc = str;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(15);
        this.pd.setLayoutParams(layoutParams);
        addView(this.pd);
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.fS.nH.push(new Thread(new ax(this.pc, new bv(this))));
    }
}
