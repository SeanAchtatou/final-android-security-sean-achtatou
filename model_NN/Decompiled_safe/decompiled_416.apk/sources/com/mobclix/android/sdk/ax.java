package com.mobclix.android.sdk;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import org.apache.http.HttpEntity;

final class ax implements Runnable {
    private bu qk;
    private String ql;
    private Bitmap qm;

    ax(String str, bu buVar) {
        this.ql = str;
        this.qk = buVar;
    }

    public final void run() {
        try {
            HttpEntity entity = new as(this.ql).cm().getEntity();
            this.qm = BitmapFactory.decodeStream(entity.getContent());
            entity.consumeContent();
            this.qk.setBitmap(this.qm);
        } catch (Throwable th) {
        }
        this.qk.sendEmptyMessage(0);
    }
}
