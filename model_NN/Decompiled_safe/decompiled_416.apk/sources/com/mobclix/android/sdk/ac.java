package com.mobclix.android.sdk;

import android.os.Handler;
import android.os.Message;

final class ac extends Handler {
    private /* synthetic */ au cV;

    ac(au auVar) {
        this.cV = auVar;
    }

    public final void handleMessage(Message message) {
        int a2 = this.cV.nL + 1;
        if (a2 >= this.cV.nK) {
            if (!this.cV.nN) {
                this.cV.nR.cancel();
                return;
            }
            a2 = 0;
        }
        this.cV.nL = a2;
        this.cV.showNext();
    }
}
