package com.mobclix.android.sdk;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import b.a.b;
import b.a.f;

final class bq extends r implements Animation.AnimationListener {
    private /* synthetic */ MobclixBrowserActivity cC;
    private View vB;
    private View vC;
    private int vD = 0;
    private int vE = 0;
    private int vF = 0;
    private int vG = 0;
    private int vH = 0;
    private int vI = 0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    bq(MobclixBrowserActivity mobclixBrowserActivity, Activity activity, String str) {
        super(mobclixBrowserActivity, activity);
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        this.cC = mobclixBrowserActivity;
        try {
            b bVar = new b(str);
            try {
                this.vH = bVar.getInt("statusBarHeight");
            } catch (Exception e) {
            }
            try {
                this.vD = bVar.getInt("topMargin");
            } catch (Exception e2) {
            }
            try {
                this.vE = bVar.getInt("leftMargin");
            } catch (Exception e3) {
            }
            try {
                i9 = bVar.getInt("fTopMargin");
            } catch (Exception e4) {
                i9 = 0;
            }
            try {
                i2 = bVar.getInt("fLeftMargin");
            } catch (Exception e5) {
                i2 = 0;
            } catch (f e6) {
                i4 = 0;
                i2 = 0;
                i = i9;
                i3 = 0;
                this.av.finish();
                i5 = i4;
                i6 = i3;
                i7 = 500;
                i8 = i2;
                this.vD -= this.vH;
                setOnClickListener(new q(this));
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(1, this.vD);
                this.vB = new View(this.av);
                this.vB.setLayoutParams(layoutParams);
                LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
                LinearLayout linearLayout = new LinearLayout(this.av);
                linearLayout.setLayoutParams(layoutParams2);
                LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(this.vE, 1);
                this.vC = new View(this.av);
                this.vC.setLayoutParams(layoutParams3);
                this.fy = (ViewGroup) this.aQ.getParent();
                this.fy.removeView(this.aQ);
                this.vF = this.aQ.getWidth();
                this.vG = this.aQ.getHeight();
                this.aQ.setLayoutParams(new LinearLayout.LayoutParams(this.vF, this.vG));
                addView(this.vB);
                addView(linearLayout);
                linearLayout.addView(this.vC);
                linearLayout.addView(this.aQ);
                a(this.vE, i8, this.vD, i, i6, i5, i7);
            }
            try {
                i10 = bVar.getInt("fWidth");
            } catch (Exception e7) {
                i10 = 0;
            } catch (f e8) {
                i4 = 0;
                i = i9;
                i3 = 0;
                this.av.finish();
                i5 = i4;
                i6 = i3;
                i7 = 500;
                i8 = i2;
                this.vD -= this.vH;
                setOnClickListener(new q(this));
                LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(1, this.vD);
                this.vB = new View(this.av);
                this.vB.setLayoutParams(layoutParams4);
                LinearLayout.LayoutParams layoutParams22 = new LinearLayout.LayoutParams(-2, -2);
                LinearLayout linearLayout2 = new LinearLayout(this.av);
                linearLayout2.setLayoutParams(layoutParams22);
                LinearLayout.LayoutParams layoutParams32 = new LinearLayout.LayoutParams(this.vE, 1);
                this.vC = new View(this.av);
                this.vC.setLayoutParams(layoutParams32);
                this.fy = (ViewGroup) this.aQ.getParent();
                this.fy.removeView(this.aQ);
                this.vF = this.aQ.getWidth();
                this.vG = this.aQ.getHeight();
                this.aQ.setLayoutParams(new LinearLayout.LayoutParams(this.vF, this.vG));
                addView(this.vB);
                addView(linearLayout2);
                linearLayout2.addView(this.vC);
                linearLayout2.addView(this.aQ);
                a(this.vE, i8, this.vD, i, i6, i5, i7);
            }
            try {
                i11 = bVar.getInt("fHeight");
            } catch (Exception e9) {
                i11 = 0;
            } catch (f e10) {
                i4 = 0;
                int i12 = i10;
                i = i9;
                i3 = i12;
                this.av.finish();
                i5 = i4;
                i6 = i3;
                i7 = 500;
                i8 = i2;
                this.vD -= this.vH;
                setOnClickListener(new q(this));
                LinearLayout.LayoutParams layoutParams42 = new LinearLayout.LayoutParams(1, this.vD);
                this.vB = new View(this.av);
                this.vB.setLayoutParams(layoutParams42);
                LinearLayout.LayoutParams layoutParams222 = new LinearLayout.LayoutParams(-2, -2);
                LinearLayout linearLayout22 = new LinearLayout(this.av);
                linearLayout22.setLayoutParams(layoutParams222);
                LinearLayout.LayoutParams layoutParams322 = new LinearLayout.LayoutParams(this.vE, 1);
                this.vC = new View(this.av);
                this.vC.setLayoutParams(layoutParams322);
                this.fy = (ViewGroup) this.aQ.getParent();
                this.fy.removeView(this.aQ);
                this.vF = this.aQ.getWidth();
                this.vG = this.aQ.getHeight();
                this.aQ.setLayoutParams(new LinearLayout.LayoutParams(this.vF, this.vG));
                addView(this.vB);
                addView(linearLayout22);
                linearLayout22.addView(this.vC);
                linearLayout22.addView(this.aQ);
                a(this.vE, i8, this.vD, i, i6, i5, i7);
            }
            try {
                i5 = i11;
                i7 = bVar.getInt("duration");
                i6 = i10;
                i = i9;
                i8 = i2;
            } catch (Exception e11) {
                i5 = i11;
                i7 = 500;
                i6 = i10;
                i = i9;
                i8 = i2;
            } catch (f e12) {
                i4 = i11;
                int i13 = i10;
                i = i9;
                i3 = i13;
                this.av.finish();
                i5 = i4;
                i6 = i3;
                i7 = 500;
                i8 = i2;
            }
        } catch (f e13) {
            i4 = 0;
            i3 = 0;
            i2 = 0;
            i = 0;
            this.av.finish();
            i5 = i4;
            i6 = i3;
            i7 = 500;
            i8 = i2;
            this.vD -= this.vH;
            setOnClickListener(new q(this));
            LinearLayout.LayoutParams layoutParams422 = new LinearLayout.LayoutParams(1, this.vD);
            this.vB = new View(this.av);
            this.vB.setLayoutParams(layoutParams422);
            LinearLayout.LayoutParams layoutParams2222 = new LinearLayout.LayoutParams(-2, -2);
            LinearLayout linearLayout222 = new LinearLayout(this.av);
            linearLayout222.setLayoutParams(layoutParams2222);
            LinearLayout.LayoutParams layoutParams3222 = new LinearLayout.LayoutParams(this.vE, 1);
            this.vC = new View(this.av);
            this.vC.setLayoutParams(layoutParams3222);
            this.fy = (ViewGroup) this.aQ.getParent();
            this.fy.removeView(this.aQ);
            this.vF = this.aQ.getWidth();
            this.vG = this.aQ.getHeight();
            this.aQ.setLayoutParams(new LinearLayout.LayoutParams(this.vF, this.vG));
            addView(this.vB);
            addView(linearLayout222);
            linearLayout222.addView(this.vC);
            linearLayout222.addView(this.aQ);
            a(this.vE, i8, this.vD, i, i6, i5, i7);
        }
        this.vD -= this.vH;
        setOnClickListener(new q(this));
        LinearLayout.LayoutParams layoutParams4222 = new LinearLayout.LayoutParams(1, this.vD);
        this.vB = new View(this.av);
        this.vB.setLayoutParams(layoutParams4222);
        LinearLayout.LayoutParams layoutParams22222 = new LinearLayout.LayoutParams(-2, -2);
        LinearLayout linearLayout2222 = new LinearLayout(this.av);
        linearLayout2222.setLayoutParams(layoutParams22222);
        LinearLayout.LayoutParams layoutParams32222 = new LinearLayout.LayoutParams(this.vE, 1);
        this.vC = new View(this.av);
        this.vC.setLayoutParams(layoutParams32222);
        this.fy = (ViewGroup) this.aQ.getParent();
        this.fy.removeView(this.aQ);
        this.vF = this.aQ.getWidth();
        this.vG = this.aQ.getHeight();
        this.aQ.setLayoutParams(new LinearLayout.LayoutParams(this.vF, this.vG));
        addView(this.vB);
        addView(linearLayout2222);
        linearLayout2222.addView(this.vC);
        linearLayout2222.addView(this.aQ);
        a(this.vE, i8, this.vD, i, i6, i5, i7);
    }

    private void a(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16 = i7 < 0 ? 0 : i7;
        int i17 = i16 > 3000 ? 3000 : i16;
        if (this.fA) {
            int i18 = i3 < 0 ? 0 : i3;
            int i19 = i < 0 ? 0 : i;
            int i20 = i4 < 0 ? 0 : i4;
            int i21 = i2 < 0 ? 0 : i2;
            int i22 = i5 < 0 ? 1 : i5;
            int i23 = i6 < 0 ? 1 : i6;
            DisplayMetrics displayMetrics = new DisplayMetrics();
            this.cC.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int i24 = displayMetrics.widthPixels;
            int i25 = displayMetrics.heightPixels;
            if (i22 > i24) {
                i14 = 0;
                i15 = i24;
            } else if (i22 + i21 > i24) {
                int i26 = i22;
                i14 = i24 - i22;
                i15 = i26;
            } else {
                int i27 = i22;
                i14 = i21;
                i15 = i27;
            }
            if (i23 > i25) {
                i8 = i25;
                i9 = i15;
                i11 = i18;
                i12 = i14;
                i13 = i19;
                i10 = 0;
            } else if (i23 + i20 > i25) {
                i8 = i23;
                i9 = i15;
                i10 = i25 - i23;
                i12 = i14;
                i13 = i19;
                i11 = i18;
            } else {
                i8 = i23;
                i9 = i15;
                i10 = i20;
                i12 = i14;
                i13 = i19;
                i11 = i18;
            }
        } else {
            i8 = i6;
            i9 = i5;
            i10 = i4;
            i11 = i3;
            i12 = i2;
            i13 = i;
        }
        ah ahVar = new ah(this.cC, this.vB, 1.0f, 1.0f, (float) i11, (float) i10);
        ahVar.setDuration((long) i17);
        ah ahVar2 = new ah(this.cC, this.vC, (float) i13, (float) i12, 1.0f, 1.0f);
        ahVar2.setDuration((long) i17);
        ah ahVar3 = new ah(this.cC, this.aQ, (float) this.aQ.getWidth(), (float) i9, (float) this.aQ.getHeight(), (float) i8);
        ahVar3.setDuration((long) i17);
        ahVar3.setAnimationListener(this);
        this.vB.startAnimation(ahVar);
        this.vC.startAnimation(ahVar2);
        this.aQ.startAnimation(ahVar3);
    }

    public final synchronized void exit(int i) {
        if (bw.fm().aQ != null) {
            this.aQ.ck.aS = null;
            bw.fm().aQ = null;
            this.fA = false;
            Window window = ((Activity) this.aQ.getContext()).getWindow();
            WindowManager.LayoutParams attributes = window.getAttributes();
            attributes.flags = this.fz;
            window.setAttributes(attributes);
            this.aQ.ck.x();
            a(this.vC.getWidth(), this.vE, this.vB.getHeight(), this.vD, this.vF, this.vG, i);
        }
    }

    public final void onAnimationEnd(Animation animation) {
        if (!this.fA) {
            this.aQ.ck.aT = false;
            ((ViewGroup) this.aQ.getParent()).removeView(this.aQ);
            this.fy.addView(this.aQ);
            this.cC.unregisterReceiver(this.cC.k);
            this.av.finish();
            return;
        }
        if (this.vB.getHeight() <= this.vH) {
            ((Activity) this.aQ.getContext()).getWindow().setFlags(1024, 1024);
        }
        this.aQ.ck.r();
        this.aQ.ck.w();
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
