package com.mobclix.android.sdk;

import android.content.DialogInterface;

final class by implements DialogInterface.OnCancelListener {
    private /* synthetic */ MobclixBrowserActivity cC;

    by(MobclixBrowserActivity mobclixBrowserActivity) {
        this.cC = mobclixBrowserActivity;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.cC.finish();
    }
}
