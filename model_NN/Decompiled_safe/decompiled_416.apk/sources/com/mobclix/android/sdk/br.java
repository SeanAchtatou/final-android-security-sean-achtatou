package com.mobclix.android.sdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public final class br implements InvocationHandler {
    private /* synthetic */ bb uX;

    public br(bb bbVar) {
        this.uX = bbVar;
    }

    public final Object invoke(Object obj, Method method, Object[] objArr) {
        this.uX.fS.ch.removeView(this.uX.rs);
        if (method.getName().equals("onReceiveAd")) {
            this.uX.addView(this.uX.rs);
            this.uX.rs.setVisibility(0);
            this.uX.aW().nT.sendEmptyMessage(0);
            return null;
        }
        if (this.uX.at == null) {
            this.uX.at = "";
        }
        this.uX.fS.ch.aV(this.uX.at);
        return null;
    }
}
