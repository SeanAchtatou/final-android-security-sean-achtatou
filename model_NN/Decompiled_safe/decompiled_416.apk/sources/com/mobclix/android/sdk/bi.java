package com.mobclix.android.sdk;

import android.graphics.drawable.BitmapDrawable;
import android.os.Message;

final class bi extends bu {
    private /* synthetic */ ar sj;

    bi(ar arVar) {
        this.sj = arVar;
    }

    public final void handleMessage(Message message) {
        if (this.qm != null) {
            this.sj.setBackgroundDrawable(new BitmapDrawable(this.qm));
        }
        this.sj.aW().nT.sendEmptyMessage(0);
    }
}
