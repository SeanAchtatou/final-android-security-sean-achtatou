package com.mobclix.android.sdk;

import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ViewFlipper;
import b.a.a;
import b.a.b;
import b.a.f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;
import java.util.Timer;

final class au extends ViewFlipper {
    /* access modifiers changed from: private */
    public static boolean nP = false;
    ce ch;
    private ArrayList iF = new ArrayList();
    private ArrayList iG = new ArrayList();
    private aq mz = aq.cl();
    private String nF = "";
    /* access modifiers changed from: private */
    public ck nG;
    /* access modifiers changed from: private */
    public Stack nH = new Stack();
    private boolean nI = false;
    boolean nJ = false;
    /* access modifiers changed from: private */
    public int nK = 1;
    /* access modifiers changed from: private */
    public int nL = 0;
    private String nM = "none";
    /* access modifiers changed from: private */
    public boolean nN = true;
    private boolean nO = false;
    private int nQ = 3000;
    /* access modifiers changed from: private */
    public Timer nR = null;
    /* access modifiers changed from: private */
    public Thread nS;
    final cd nT = new cd(this);
    final ac nU = new ac(this);
    private String type = "";

    au(ce ceVar, b bVar, boolean z) {
        super(ceVar.getContext());
        this.ch = ceVar;
        String i = this.mz.i(this.mz.h(this.ch.zd, aq.mD), "handle_response");
        requestDisallowInterceptTouchEvent(true);
        if (bVar == null) {
            addView(new cb(this));
            this.nK = 1;
            this.type = "customAd";
            this.nI = true;
            return;
        }
        String i2 = this.mz.i(i, "b_build_models");
        try {
            b F = bVar.F("eventUrls");
            try {
                a E = F.E("onShow");
                for (int i3 = 0; i3 < E.length(); i3++) {
                    this.iF.add(E.getString(i3));
                }
            } catch (Exception e) {
            }
            a E2 = F.E("onTouch");
            for (int i4 = 0; i4 < E2.length(); i4++) {
                this.iG.add(E2.getString(i4));
            }
        } catch (Exception e2) {
        }
        try {
            i = this.mz.i(this.mz.i(this.mz.af(i2), "c_build_creative"), "a_determine_type");
            b F2 = bVar.F("props");
            try {
                this.nF = bVar.getString("id");
            } catch (f e3) {
            }
            this.type = bVar.getString("type");
            String af = this.mz.af(i);
            try {
                this.nO = z;
                this.nG = new ck(this, bVar.F("action"), this);
            } catch (Exception e4) {
                this.nG = new ck(this, this);
            }
            i = this.mz.i(af, "b_get_view");
            if (this.type.equals("html")) {
                addView(new av(F2.getString("html"), this));
                this.nK = 1;
                this.nI = true;
            } else if (this.type.equals("openallocation")) {
                addView(new bb(F2, this));
                this.nK = 1;
                this.nI = true;
            } else {
                try {
                    this.nM = F2.getString("transitionType");
                } catch (f e5) {
                }
                a(this, this.nM);
                try {
                    this.nQ = (int) (F2.getDouble("transitionTime") * 1000.0d);
                } catch (f e6) {
                }
                if (this.nQ == 0) {
                    this.nQ = 3000;
                }
                try {
                    this.nN = F2.getBoolean("loop");
                } catch (f e7) {
                }
                if (this.type.equals("image")) {
                    a E3 = F2.E("images");
                    this.nK = E3.length();
                    for (int i5 = 0; i5 < E3.length(); i5++) {
                        addView(new aw(E3.getString(i5), this));
                    }
                } else if (this.type.equals("text")) {
                    a E4 = F2.E("texts");
                    this.nK = E4.length();
                    for (int i6 = 0; i6 < E4.length(); i6++) {
                        addView(new ar(E4.m(i6), this));
                    }
                }
                String i7 = this.mz.i(this.mz.af(i), "f_load_ad_creative");
                a();
                String af2 = this.mz.af(i7);
                this.nI = true;
            }
        } catch (f e8) {
            this.mz.af(this.mz.af(this.mz.af(i)));
            this.mz.ae(this.ch.zd);
        }
    }

    private void a(ViewFlipper viewFlipper, String str) {
        Animation cjVar;
        Animation cjVar2;
        if (str != null) {
            if (str.equals("fade")) {
                cjVar = new AlphaAnimation(1.0f, 0.0f);
                cjVar2 = new AlphaAnimation(0.0f, 1.0f);
            } else if (str.equals("slideRight")) {
                cjVar = new TranslateAnimation(2, 0.0f, 2, 1.0f, 2, 0.0f, 2, 0.0f);
                cjVar2 = new TranslateAnimation(2, -1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
            } else if (str.equals("slideLeft")) {
                cjVar = new TranslateAnimation(2, 0.0f, 2, -1.0f, 2, 0.0f, 2, 0.0f);
                cjVar2 = new TranslateAnimation(2, 1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
            } else if (str.equals("slideUp")) {
                cjVar = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 0.0f, 2, -1.0f);
                cjVar2 = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 1.0f, 2, 0.0f);
            } else if (str.equals("slideDown")) {
                cjVar = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 0.0f, 2, 1.0f);
                cjVar2 = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, -1.0f, 2, 0.0f);
            } else if (str.equals("flipRight")) {
                cjVar = new cj(this, 0.0f, 90.0f, ((float) this.ch.getWidth()) / 2.0f, ((float) this.ch.getHeight()) / 2.0f, true);
                cjVar2 = new cj(this, -90.0f, 0.0f, ((float) this.ch.getWidth()) / 2.0f, ((float) this.ch.getHeight()) / 2.0f, false);
                cjVar2.setStartOffset(300);
            } else if (str.equals("flipLeft")) {
                cjVar = new cj(this, 0.0f, -90.0f, ((float) this.ch.getWidth()) / 2.0f, ((float) this.ch.getHeight()) / 2.0f, true);
                cjVar2 = new cj(this, 90.0f, 0.0f, ((float) this.ch.getWidth()) / 2.0f, ((float) this.ch.getHeight()) / 2.0f, false);
                cjVar2.setStartOffset(300);
            } else {
                return;
            }
            cjVar.setDuration(300);
            cjVar2.setDuration(300);
            viewFlipper.setOutAnimation(cjVar);
            viewFlipper.setInAnimation(cjVar2);
        }
    }

    public final void a() {
        if (!this.nH.isEmpty()) {
            ((Thread) this.nH.pop()).start();
            return;
        }
        String i = this.mz.i(this.mz.h(this.ch.zd, aq.mD), "handle_response");
        this.nL = 0;
        String i2 = this.mz.i(this.mz.i(this.mz.i(i, "c_build_creative"), "b_get_view"), "c_deque_view");
        while (this.ch.getChildCount() > 1) {
            if (this.ch.getChildAt(0) == this.ch.zv) {
                this.ch.removeViewAt(1);
            } else {
                this.ch.removeViewAt(0);
            }
            System.gc();
        }
        String i3 = this.mz.i(this.mz.af(this.mz.af(i2)), "e_add_view");
        this.ch.addView(this);
        String i4 = this.mz.i(this.mz.af(this.mz.af(i3)), "d_bring_onscreen");
        if (this.ch.zv != null) {
            this.ch.zv.onStop();
            if (this.ch.ze) {
                a(this.ch, "flipRight");
            }
        }
        this.ch.showNext();
        String af = this.mz.af(i4);
        if (this.nK > 1) {
            onPause();
            this.nR = new Timer();
            this.nR.scheduleAtFixedRate(new cf(this), (long) this.nQ, (long) this.nQ);
        }
        String i5 = this.mz.i(af, "e_trigger_events");
        if (this.ch.getVisibility() == 0) {
            cr();
        }
        String i6 = this.mz.i(this.mz.af(i5), "f_notify_delegates");
        Iterator it = this.ch.iK.iterator();
        while (it.hasNext()) {
            l lVar = (l) it.next();
            if (lVar != null) {
                lVar.a(this.ch);
            }
        }
        String i7 = this.mz.i(this.mz.af(i6), "h_handle_autoplay");
        if (this.nG != null && this.nG.aV && this.ch.fI() && !this.nO && !nP && ((Long) ce.zj.get(this.ch.zl)).longValue() + bw.aG(this.ch.zl).longValue() < System.currentTimeMillis()) {
            this.nO = true;
            this.nG.gx();
            ce.zj.put(this.ch.zl, Long.valueOf(System.currentTimeMillis()));
        }
        this.mz.af(this.mz.af(i7));
        this.mz.ae(this.ch.zd);
    }

    /* access modifiers changed from: package-private */
    public final void cr() {
        Iterator it = this.iF.iterator();
        while (it.hasNext()) {
            new Thread(new ax((String) it.next(), new bu())).start();
        }
        this.nJ = true;
    }

    public final boolean isInitialized() {
        return this.nI;
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 7) {
                try {
                    super.onDetachedFromWindow();
                    super.stopFlipping();
                } catch (IllegalArgumentException e) {
                    Log.w("MobclixCreative", "Android project  issue 6191  workaround.");
                    super.stopFlipping();
                } catch (Throwable th) {
                    super.stopFlipping();
                    throw th;
                }
            } else {
                super.onDetachedFromWindow();
            }
        } catch (Exception e2) {
            super.onDetachedFromWindow();
        }
    }

    public final void onPause() {
        synchronized (this) {
            if (this.nR != null) {
                this.nR.cancel();
                this.nR.purge();
            }
        }
        try {
            if (getCurrentView().getClass() == av.class) {
                k kVar = ((av) getCurrentView()).aQ.ck;
                kVar.q();
                if (!kVar.aT) {
                    kVar.z();
                }
            }
        } catch (Exception e) {
        }
    }

    public final void onResume() {
        synchronized (this) {
            if (this.nR != null) {
                this.nR.cancel();
                this.nR.purge();
                this.nR = new Timer();
                this.nR.scheduleAtFixedRate(new cf(this), (long) this.nQ, (long) this.nQ);
            }
        }
        try {
            if (getCurrentView().getClass() == av.class) {
                k kVar = ((av) getCurrentView()).aQ.ck;
                kVar.r();
                if (!kVar.aT && kVar.aS == null) {
                    kVar.A();
                }
                kVar.aT = false;
            }
        } catch (Exception e) {
        }
    }

    public final void onStop() {
        onPause();
        try {
            if (getCurrentView().getClass() == av.class) {
                ((av) getCurrentView()).aQ.ck.y();
            }
        } catch (Exception e) {
        }
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        try {
            if (this.type.equals("html")) {
                return false;
            }
            if (motionEvent.getAction() == 0) {
                Iterator it = this.iG.iterator();
                while (it.hasNext()) {
                    new Thread(new ax((String) it.next(), new bu())).start();
                }
                return this.nG.gx();
            }
            return false;
        } catch (Exception e) {
        }
    }
}
