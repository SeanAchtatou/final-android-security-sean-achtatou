package com.mobclix.android.sdk;

import android.content.Intent;
import android.net.Uri;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class az extends WebViewClient {
    private /* synthetic */ av qA;

    az(av avVar) {
        this.qA = avVar;
    }

    public final void onPageFinished(WebView webView, String str) {
        CookieSyncManager.getInstance().sync();
        if (!this.qA.aQ.cj && this.qA.fS != null) {
            this.qA.aW().nT.sendEmptyMessage(0);
        }
        if (!this.qA.aQ.cj && this.qA.ci != null) {
            this.qA.ci.a(this.qA.aQ);
        }
        this.qA.aQ.cj = true;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        Uri parse = Uri.parse(str);
        try {
            if (parse.getScheme().equals("tel") || parse.getScheme().equals("mailto")) {
                if (this.qA.fS != null) {
                    this.qA.fS.nG.type = "null";
                    this.qA.fS.nG.gx();
                }
                if (this.qA.aQ.ck.aS != null) {
                    this.qA.aQ.ck.aS.fB = true;
                }
                this.qA.getContext().startActivity(new Intent("android.intent.action.VIEW", parse));
                return true;
            } else if (parse.getScheme().equals("sms")) {
                if (this.qA.fS != null) {
                    this.qA.fS.nG.type = "null";
                    this.qA.fS.nG.gx();
                }
                if (this.qA.aQ.ck.aS != null) {
                    this.qA.aQ.ck.aS.fB = true;
                }
                String[] split = str.split(":");
                String str2 = String.valueOf(split[0]) + "://";
                for (int i = 1; i < split.length; i++) {
                    str2 = String.valueOf(str2) + split[i];
                }
                String queryParameter = Uri.parse(str2).getQueryParameter("body");
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str.split("\\?")[0]));
                intent.putExtra("sms_body", queryParameter);
                this.qA.getContext().startActivity(intent);
                return true;
            } else {
                String queryParameter2 = parse.getQueryParameter("shouldOpenInNewWindow");
                if (queryParameter2 == null) {
                    queryParameter2 = "";
                } else {
                    queryParameter2.toLowerCase();
                }
                if ((this.qA.aQ.ck.aS != null || queryParameter2.equals("no")) && !queryParameter2.equals("yes")) {
                    this.qA.aQ.loadUrl(str);
                } else {
                    if (this.qA.aQ.ck.aS != null) {
                        this.qA.aQ.ck.aS.fB = true;
                    }
                    this.qA.getContext().startActivity(new Intent("android.intent.action.VIEW", parse));
                    if (this.qA.fS != null) {
                        this.qA.fS.nG.type = "null";
                        this.qA.fS.nG.gx();
                    }
                }
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }
}
