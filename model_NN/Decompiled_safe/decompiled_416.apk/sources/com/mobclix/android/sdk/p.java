package com.mobclix.android.sdk;

import java.net.HttpURLConnection;
import java.net.URL;

final class p implements Runnable {
    private String as;
    private /* synthetic */ au cV;

    p(au auVar, String str) {
        this.cV = auVar;
        this.as = str;
    }

    public final void run() {
        HttpURLConnection httpURLConnection;
        HttpURLConnection httpURLConnection2 = null;
        try {
            httpURLConnection = (HttpURLConnection) new URL(this.as).openConnection();
            try {
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setRequestProperty("User-Agent", this.cV.ch.ar.fh());
                httpURLConnection.connect();
                httpURLConnection.disconnect();
            } catch (Exception e) {
                httpURLConnection.disconnect();
            } catch (Throwable th) {
                Throwable th2 = th;
                httpURLConnection2 = httpURLConnection;
                th = th2;
                httpURLConnection2.disconnect();
                throw th;
            }
        } catch (Exception e2) {
            httpURLConnection = null;
            httpURLConnection.disconnect();
        } catch (Throwable th3) {
            th = th3;
            httpURLConnection2.disconnect();
            throw th;
        }
    }
}
