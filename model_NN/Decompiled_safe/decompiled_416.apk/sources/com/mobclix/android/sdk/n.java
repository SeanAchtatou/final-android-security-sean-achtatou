package com.mobclix.android.sdk;

import android.content.Context;
import android.webkit.WebView;

final class n extends WebView {
    ce ch = null;
    ab ci = null;
    boolean cj = false;
    k ck = null;
    private String cl = null;

    public n(ab abVar) {
        super(abVar.getActivity());
        this.ci = abVar;
    }

    public n(ce ceVar) {
        super(ceVar.getContext());
        this.ch = ceVar;
    }

    public final Context L() {
        Context context = getContext();
        return (this.ck == null || this.ck.aS == null) ? context : this.ck.aS.getContext();
    }

    public final void M() {
        try {
            loadDataWithBaseURL(null, this.cl, "text/html", "utf-8", null);
        } catch (Exception e) {
        }
    }

    public final void m(String str) {
        this.cl = str;
    }
}
