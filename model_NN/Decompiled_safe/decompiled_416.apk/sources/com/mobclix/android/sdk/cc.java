package com.mobclix.android.sdk;

import android.content.Intent;
import android.os.Build;
import b.a.b;

public abstract class cc {
    private static cc xV;

    public static cc fs() {
        if (xV == null) {
            try {
                xV = (cc) Class.forName(Integer.parseInt(Build.VERSION.SDK) < 5 ? "com.mobclix.android.sdk.MobclixContactsSdk3_4" : "com.mobclix.android.sdk.MobclixContactsSdk5").asSubclass(cc.class).newInstance();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return xV;
    }

    public abstract Intent ft();

    public abstract Intent fu();

    public abstract b fv();
}
