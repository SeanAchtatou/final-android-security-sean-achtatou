package com.mobclix.android.sdk;

import b.a.b;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public final class aq {
    private static String TAG = "MobclixInstrumentation";
    static String mC = "startup";
    static String mD = "adview";
    static final String[] mE = {mC, mD};
    private static final aq mJ = new aq();
    private bw ar = bw.fm();
    private b mF = new b();
    private HashMap mG = new HashMap();
    private ArrayList mH = new ArrayList();
    private HashMap mI = new HashMap();

    private aq() {
    }

    private boolean ad(String str) {
        String[] split = str.split("/");
        return this.mF.G(split.length == 1 ? str : split[0]);
    }

    static aq cl() {
        return mJ;
    }

    /* access modifiers changed from: package-private */
    public final void a(Object obj, String str, String str2) {
        if (obj != null && str != null && !str.equals("") && str2 != null && !str2.equals("")) {
            try {
                this.mF.F(str2).F("data").a(str, obj);
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final String ac(String str) {
        return h(str, null);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x006e A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void ae(java.lang.String r8) {
        /*
            r7 = this;
            r5 = 0
            if (r8 == 0) goto L_0x0011
            java.lang.String r0 = ""
            boolean r0 = r8.equals(r0)
            if (r0 != 0) goto L_0x0011
            boolean r0 = r7.ad(r8)
            if (r0 != 0) goto L_0x0012
        L_0x0011:
            return
        L_0x0012:
            if (r8 == 0) goto L_0x001c
            java.lang.String r0 = ""
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x002d
        L_0x001c:
            r0 = r5
        L_0x001d:
            if (r0 != 0) goto L_0x0096
            java.util.ArrayList r0 = r7.mH
            boolean r0 = r0.contains(r8)
            if (r0 != 0) goto L_0x0011
            java.util.ArrayList r0 = r7.mH
            r0.add(r8)
            goto L_0x0011
        L_0x002d:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = java.lang.String.valueOf(r8)
            r0.<init>(r1)
            java.lang.String r1 = "/"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            java.lang.String r0 = com.mobclix.android.sdk.aq.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Current benchmarks: "
            r2.<init>(r3)
            java.util.HashMap r3 = r7.mG
            java.util.Set r3 = r3.keySet()
            java.lang.String r3 = r3.toString()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.v(r0, r2)
            java.util.HashMap r0 = r7.mG
            java.util.Set r0 = r0.keySet()
            java.util.Iterator r2 = r0.iterator()
        L_0x0068:
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x0070
            r0 = 1
            goto L_0x001d
        L_0x0070:
            java.lang.Object r0 = r2.next()
            java.lang.String r0 = (java.lang.String) r0
            int r3 = r0.length()
            int r4 = r1.length()
            if (r3 < r4) goto L_0x008e
            int r3 = r1.length()
            java.lang.String r3 = r0.substring(r5, r3)
            boolean r3 = r3.equals(r1)
            if (r3 != 0) goto L_0x0094
        L_0x008e:
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x0068
        L_0x0094:
            r0 = r5
            goto L_0x001d
        L_0x0096:
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x017f }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ Exception -> 0x017f }
            b.a.b r1 = r7.mF     // Catch:{ Exception -> 0x017f }
            b.a.b r1 = r1.F(r8)     // Catch:{ Exception -> 0x017f }
            b.a.b r2 = r7.mF     // Catch:{ Exception -> 0x017f }
            r2.P(r8)     // Catch:{ Exception -> 0x017f }
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat     // Catch:{ Exception -> 0x017f }
            java.lang.String r3 = "yyyy-MM-dd'T'hh:mmZ"
            r2.<init>(r3)     // Catch:{ Exception -> 0x017f }
            java.lang.String r3 = "endDate"
            java.util.Date r4 = new java.util.Date     // Catch:{ Exception -> 0x017f }
            long r5 = r0.longValue()     // Catch:{ Exception -> 0x017f }
            r4.<init>(r5)     // Catch:{ Exception -> 0x017f }
            java.lang.String r0 = r2.format(r4)     // Catch:{ Exception -> 0x017f }
            r1.a(r3, r0)     // Catch:{ Exception -> 0x017f }
            java.lang.String r0 = "totalElapsedTime"
            long r2 = java.lang.System.nanoTime()     // Catch:{ Exception -> 0x017f }
            java.lang.String r4 = "startDateNanoTime"
            long r4 = r1.getLong(r4)     // Catch:{ Exception -> 0x017f }
            long r2 = r2 - r4
            double r2 = (double) r2     // Catch:{ Exception -> 0x017f }
            r4 = 4741671816366391296(0x41cdcd6500000000, double:1.0E9)
            double r2 = r2 / r4
            r1.a(r0, r2)     // Catch:{ Exception -> 0x017f }
            java.lang.String r0 = "startDateNanoTime"
            r1.P(r0)     // Catch:{ Exception -> 0x017f }
            b.a.b r0 = new b.a.b     // Catch:{ Exception -> 0x017f }
            r0.<init>()     // Catch:{ Exception -> 0x017f }
            java.lang.String r2 = "app_id"
            com.mobclix.android.sdk.bw r3 = r7.ar     // Catch:{ Exception -> 0x017f }
            java.lang.String r3 = r3.eO()     // Catch:{ Exception -> 0x017f }
            r0.a(r2, r3)     // Catch:{ Exception -> 0x017f }
            java.lang.String r2 = "platform"
            com.mobclix.android.sdk.bw r3 = r7.ar     // Catch:{ Exception -> 0x017f }
            java.lang.String r3 = r3.eP()     // Catch:{ Exception -> 0x017f }
            r0.a(r2, r3)     // Catch:{ Exception -> 0x017f }
            java.lang.String r2 = "sdk_ver"
            java.lang.String r3 = com.mobclix.android.sdk.bw.fc()     // Catch:{ Exception -> 0x017f }
            r0.a(r2, r3)     // Catch:{ Exception -> 0x017f }
            java.lang.String r2 = "app_ver"
            com.mobclix.android.sdk.bw r3 = r7.ar     // Catch:{ Exception -> 0x017f }
            java.lang.String r3 = r3.eR()     // Catch:{ Exception -> 0x017f }
            r0.a(r2, r3)     // Catch:{ Exception -> 0x017f }
            java.lang.String r2 = "udid"
            com.mobclix.android.sdk.bw r3 = r7.ar     // Catch:{ Exception -> 0x017f }
            java.lang.String r3 = r3.getDeviceId()     // Catch:{ Exception -> 0x017f }
            r0.a(r2, r3)     // Catch:{ Exception -> 0x017f }
            java.lang.String r2 = "dev_model"
            com.mobclix.android.sdk.bw r3 = r7.ar     // Catch:{ Exception -> 0x017f }
            java.lang.String r3 = r3.eT()     // Catch:{ Exception -> 0x017f }
            r0.a(r2, r3)     // Catch:{ Exception -> 0x017f }
            java.lang.String r2 = "dev_vers"
            com.mobclix.android.sdk.bw r3 = r7.ar     // Catch:{ Exception -> 0x017f }
            java.lang.String r3 = r3.eQ()     // Catch:{ Exception -> 0x017f }
            r0.a(r2, r3)     // Catch:{ Exception -> 0x017f }
            java.lang.String r2 = "hw_dev_model"
            com.mobclix.android.sdk.bw r3 = r7.ar     // Catch:{ Exception -> 0x017f }
            java.lang.String r3 = r3.eU()     // Catch:{ Exception -> 0x017f }
            r0.a(r2, r3)     // Catch:{ Exception -> 0x017f }
            java.lang.String r2 = "conn"
            com.mobclix.android.sdk.bw r3 = r7.ar     // Catch:{ Exception -> 0x017f }
            java.lang.String r3 = r3.eV()     // Catch:{ Exception -> 0x017f }
            r0.a(r2, r3)     // Catch:{ Exception -> 0x017f }
            java.lang.String r2 = "environment"
            r1.a(r2, r0)     // Catch:{ Exception -> 0x017f }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x017f }
            java.lang.String r2 = "cat="
            r0.<init>(r2)     // Catch:{ Exception -> 0x017f }
            java.lang.StringBuilder r2 = r0.append(r8)     // Catch:{ Exception -> 0x017f }
            java.lang.String r3 = "&payload="
            r2.append(r3)     // Catch:{ Exception -> 0x017f }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x017f }
            java.lang.String r2 = "UTF-8"
            java.lang.String r1 = java.net.URLEncoder.encode(r1, r2)     // Catch:{ Exception -> 0x017f }
            r0.append(r1)     // Catch:{ Exception -> 0x017f }
            java.lang.Thread r1 = new java.lang.Thread     // Catch:{ Exception -> 0x017f }
            com.mobclix.android.sdk.g r2 = new com.mobclix.android.sdk.g     // Catch:{ Exception -> 0x017f }
            com.mobclix.android.sdk.bw r3 = r7.ar     // Catch:{ Exception -> 0x017f }
            java.lang.String r3 = r3.fd()     // Catch:{ Exception -> 0x017f }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x017f }
            r2.<init>(r3, r0)     // Catch:{ Exception -> 0x017f }
            r1.<init>(r2)     // Catch:{ Exception -> 0x017f }
            r1.run()     // Catch:{ Exception -> 0x017f }
            goto L_0x0011
        L_0x017f:
            r0 = move-exception
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.aq.ae(java.lang.String):void");
    }

    /* access modifiers changed from: package-private */
    public final String af(String str) {
        if (str == null || str.equals("") || !ad(str)) {
            return null;
        }
        try {
            String[] split = str.split("/");
            if (split.length == 1) {
                return null;
            }
            Long valueOf = Long.valueOf(System.nanoTime());
            String str2 = split[0];
            this.mF.F(str2).F("benchmarks").a(str, ((double) (valueOf.longValue() - ((Long) this.mG.get(str)).longValue())) / 1.0E9d);
            this.mG.remove(str);
            if (this.mH.contains(str2)) {
                ae(str2);
            }
            if (split.length <= 1) {
                return null;
            }
            StringBuilder sb = new StringBuilder(split[0]);
            for (int i = 1; i < split.length - 1; i++) {
                sb.append("/").append(split[i]);
            }
            String replace = sb.toString().replace(" ", "").replace("\\r", "").replace("\\n", "");
            if (replace.length() == 0) {
                return null;
            }
            if (replace.equals("/")) {
                return null;
            }
            return replace;
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final String h(String str, String str2) {
        int i;
        if (str == null || str.equals("")) {
            return null;
        }
        if (this.mF.G(str)) {
            return str;
        }
        String str3 = (str2 == null || str2.equals("")) ? str : str2;
        try {
            this.ar = bw.fm();
            try {
                i = ((Integer) this.mI.get(str3)).intValue();
            } catch (Exception e) {
                i = 0;
            }
            if (bw.aC(str3) == null) {
                return null;
            }
            int parseInt = Integer.parseInt(bw.aC(str3));
            this.mI.put(str3, Integer.valueOf(i + 1));
            if (parseInt < 0 || (parseInt != 0 && i % parseInt != 0)) {
                return null;
            }
            b bVar = new b();
            bVar.a("benchmarks", new b());
            bVar.a("data", new b());
            bVar.a("startDate", new SimpleDateFormat("yyyy-MM-dd'T'hh:mmZ").format(new Date(System.currentTimeMillis())));
            bVar.a("startDateNanoTime", System.nanoTime());
            this.mF.a(str, bVar);
            return str;
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final String i(String str, String str2) {
        if (str2 == null || str2.equals("") || str == null || str.equals("") || !ad(str)) {
            return null;
        }
        String str3 = String.valueOf(str) + "/" + str2;
        this.mG.put(str3, Long.valueOf(System.nanoTime()));
        return str3;
    }
}
