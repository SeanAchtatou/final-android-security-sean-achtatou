package com.mobclix.android.sdk;

import android.os.Handler;
import android.os.Message;
import java.util.Iterator;

final class ad extends Handler {
    private /* synthetic */ ce ab;

    /* synthetic */ ad(ce ceVar) {
        this(ceVar, (byte) 0);
    }

    private ad(ce ceVar, byte b2) {
        this.ab = ceVar;
    }

    public final void handleMessage(Message message) {
        if (!this.ab.zi) {
            if (bw.aD(this.ab.zl)) {
                this.ab.zc = true;
                if (this.ab.zg == -1) {
                    this.ab.r(bw.aF(this.ab.zl));
                }
                if (this.ab.zh == -1) {
                    this.ab.s(bw.aH(this.ab.zl));
                }
                if (this.ab.zs != 0) {
                    this.ab.c(this.ab.zs);
                } else if (bw.aE(this.ab.zl) >= 15000) {
                    this.ab.c(bw.aE(this.ab.zl));
                }
                this.ab.fL();
                return;
            }
            Iterator it = this.ab.iK.iterator();
            while (it.hasNext()) {
                l lVar = (l) it.next();
                if (lVar != null) {
                    lVar.a(this.ab, -999999);
                }
            }
        }
    }
}
