package com.mobclix.android.sdk;

import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class u extends WebViewClient {
    private /* synthetic */ ak fO;

    u(ak akVar) {
        this.fO = akVar;
    }

    public final void onPageFinished(WebView webView, String str) {
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        Uri parse = Uri.parse(str);
        if (parse.getScheme().equals("http") || parse.getScheme().equals("https") || parse.getScheme() == null) {
            webView.loadUrl(str);
            return true;
        }
        this.fO.cC.startActivity(new Intent("android.intent.action.VIEW", parse));
        return true;
    }
}
