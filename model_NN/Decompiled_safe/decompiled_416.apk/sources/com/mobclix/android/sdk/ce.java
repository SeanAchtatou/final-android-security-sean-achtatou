package com.mobclix.android.sdk;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewFlipper;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Timer;

public abstract class ce extends ViewFlipper {
    private static int iH = 0;
    static HashMap zj = new HashMap();
    private String TAG = "MobclixAdView";
    Context aa;
    bw ar = null;
    float c = 1.0f;
    private float height;
    HashSet iK = new HashSet();
    private Object lock = new Object();
    /* access modifiers changed from: private */
    public aq mz;
    private float width;
    private ae za = new ae(this);
    final ad zb = new ad(this);
    boolean zc = false;
    String zd = null;
    boolean ze = true;
    boolean zf = false;
    int zg = -1;
    int zh = -1;
    boolean zi = false;
    String zk = null;
    String zl;
    private int zm = 0;
    private Thread zn;
    /* access modifiers changed from: private */
    public String zo = "";
    String zp = "";
    private Timer zq = null;
    private boolean zr = false;
    /* access modifiers changed from: private */
    public long zs = 0;
    int zt = 0;
    au zu = null;
    au zv = null;
    String zw = null;
    bm zx = null;

    ce(Context context, String str) {
        super(context);
        this.aa = context;
        this.zl = str;
        try {
            f((Activity) context);
        } catch (ca e) {
            throw e;
        } catch (Exception e2) {
        }
    }

    ce(Context context, String str, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.aa = context;
        this.zl = str;
        String attributeValue = attributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "background");
        if (attributeValue != null) {
            this.zm = Color.parseColor(attributeValue);
        }
        try {
            f((Activity) context);
        } catch (ca e) {
            throw e;
        } catch (Exception e2) {
        }
    }

    private void aW(String str) {
        if (this.zc) {
            if (this.zn != null) {
                this.zn.interrupt();
                this.zn = null;
            }
            String i = this.mz.i(this.mz.h(this.zd, aq.mD), "start_request");
            bt btVar = new bt(this, this.za);
            if (str != null) {
                btVar.wr = str == null ? "" : str;
            }
            this.zn = new Thread(btVar);
            this.zn.start();
            this.mz.af(i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ea, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00eb, code lost:
        throw r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00ea A[ExcHandler: ca (r0v2 'e' com.mobclix.android.sdk.ca A[CUSTOM_DECLARE]), Splitter:B:5:0x003d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void f(android.app.Activity r9) {
        /*
            r8 = this;
            r7 = 1
            r4 = 0
            java.lang.Object r0 = r8.lock
            monitor-enter(r0)
            com.mobclix.android.sdk.aq r1 = com.mobclix.android.sdk.aq.cl()     // Catch:{ all -> 0x00de }
            r8.mz = r1     // Catch:{ all -> 0x00de }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00de }
            java.lang.String r2 = com.mobclix.android.sdk.aq.mD     // Catch:{ all -> 0x00de }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ all -> 0x00de }
            r1.<init>(r2)     // Catch:{ all -> 0x00de }
            java.lang.String r2 = "_"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00de }
            java.lang.String r2 = r8.zl     // Catch:{ all -> 0x00de }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00de }
            java.lang.String r2 = "_"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00de }
            int r2 = com.mobclix.android.sdk.ce.iH     // Catch:{ all -> 0x00de }
            int r2 = r2 + 1
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00de }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00de }
            r8.zd = r1     // Catch:{ all -> 0x00de }
            int r1 = com.mobclix.android.sdk.ce.iH     // Catch:{ all -> 0x00de }
            int r1 = r1 + 1
            com.mobclix.android.sdk.ce.iH = r1     // Catch:{ all -> 0x00de }
            monitor-exit(r0)     // Catch:{ all -> 0x00de }
            com.mobclix.android.sdk.bw.d(r9)     // Catch:{ ca -> 0x00ea, Exception -> 0x0109 }
            com.mobclix.android.sdk.bw r0 = com.mobclix.android.sdk.bw.fm()     // Catch:{ ca -> 0x00ea, Exception -> 0x0109 }
            r8.ar = r0     // Catch:{ ca -> 0x00ea, Exception -> 0x0109 }
            com.mobclix.android.sdk.bw r0 = r8.ar     // Catch:{ ca -> 0x00ea, Exception -> 0x0109 }
            java.lang.String r0 = r0.fh()     // Catch:{ ca -> 0x00ea, Exception -> 0x0109 }
            java.lang.String r1 = "null"
            boolean r0 = r0.equals(r1)     // Catch:{ ca -> 0x00ea, Exception -> 0x0109 }
            if (r0 == 0) goto L_0x007c
            android.webkit.WebView r0 = new android.webkit.WebView     // Catch:{ ca -> 0x00ea, Exception -> 0x0109 }
            android.content.Context r1 = r8.getContext()     // Catch:{ ca -> 0x00ea, Exception -> 0x0109 }
            r0.<init>(r1)     // Catch:{ ca -> 0x00ea, Exception -> 0x0109 }
            android.webkit.WebSettings r0 = r0.getSettings()     // Catch:{ ca -> 0x00ea, Exception -> 0x0109 }
            java.lang.Class r1 = r0.getClass()     // Catch:{ Exception -> 0x00e1, ca -> 0x00ea }
            java.lang.String r2 = "getUserAgentString"
            r3 = 0
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x00e1, ca -> 0x00ea }
            java.lang.reflect.Method r1 = r1.getDeclaredMethod(r2, r3)     // Catch:{ Exception -> 0x00e1, ca -> 0x00ea }
            com.mobclix.android.sdk.bw r2 = r8.ar     // Catch:{ Exception -> 0x00e1, ca -> 0x00ea }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x00e1, ca -> 0x00ea }
            java.lang.Object r0 = r1.invoke(r0, r3)     // Catch:{ Exception -> 0x00e1, ca -> 0x00ea }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00e1, ca -> 0x00ea }
            r2.setUserAgent(r0)     // Catch:{ Exception -> 0x00e1, ca -> 0x00ea }
        L_0x007c:
            java.lang.String[] r0 = com.mobclix.android.sdk.bw.wt
            int r1 = r0.length
            r2 = r4
        L_0x0080:
            if (r2 < r1) goto L_0x00ec
            r8.requestDisallowInterceptTouchEvent(r7)
            int r0 = r8.zm
            r8.setBackgroundColor(r0)
            android.webkit.CookieManager r0 = android.webkit.CookieManager.getInstance()
            r0.setAcceptCookie(r7)
            java.lang.Object r0 = r8.getTag()     // Catch:{ Exception -> 0x0107 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0107 }
            r8.zo = r0     // Catch:{ Exception -> 0x0107 }
        L_0x009b:
            java.lang.String r0 = "com.admob.android.ads.AdManager"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x0105 }
            java.lang.String r1 = "setTestDevices"
            r2 = 1
            java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x0105 }
            r3 = 0
            java.lang.Class<java.lang.String[]> r4 = java.lang.String[].class
            r2[r3] = r4     // Catch:{ Exception -> 0x0105 }
            java.lang.reflect.Method r1 = r0.getMethod(r1, r2)     // Catch:{ Exception -> 0x0105 }
            java.lang.String r2 = "TEST_EMULATOR"
            java.lang.reflect.Field r0 = r0.getField(r2)     // Catch:{ Exception -> 0x0105 }
            r2 = 0
            java.lang.Object r0 = r0.get(r2)     // Catch:{ Exception -> 0x0105 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0105 }
            r2 = 0
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0105 }
            r4 = 0
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Exception -> 0x0105 }
            r6 = 0
            r5[r6] = r0     // Catch:{ Exception -> 0x0105 }
            r3[r4] = r5     // Catch:{ Exception -> 0x0105 }
            r1.invoke(r2, r3)     // Catch:{ Exception -> 0x0105 }
        L_0x00cc:
            com.mobclix.android.sdk.bw r0 = r8.ar
            if (r0 == 0) goto L_0x00dd
            java.lang.Thread r0 = new java.lang.Thread
            com.mobclix.android.sdk.f r1 = new com.mobclix.android.sdk.f
            r1.<init>(r8)
            r0.<init>(r1)
            r0.start()
        L_0x00dd:
            return
        L_0x00de:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00de }
            throw r1
        L_0x00e1:
            r0 = move-exception
            com.mobclix.android.sdk.bw r0 = r8.ar     // Catch:{ ca -> 0x00ea, Exception -> 0x0109 }
            java.lang.String r1 = "Mozilla/5.0 (Linux; U; Android 1.1; en-us; dream) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2"
            r0.setUserAgent(r1)     // Catch:{ ca -> 0x00ea, Exception -> 0x0109 }
            goto L_0x007c
        L_0x00ea:
            r0 = move-exception
            throw r0
        L_0x00ec:
            r3 = r0[r2]
            java.util.HashMap r4 = com.mobclix.android.sdk.ce.zj
            boolean r4 = r4.containsKey(r3)
            if (r4 != 0) goto L_0x0101
            java.util.HashMap r4 = com.mobclix.android.sdk.ce.zj
            r5 = 0
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r4.put(r3, r5)
        L_0x0101:
            int r2 = r2 + 1
            goto L_0x0080
        L_0x0105:
            r0 = move-exception
            goto L_0x00cc
        L_0x0107:
            r0 = move-exception
            goto L_0x009b
        L_0x0109:
            r0 = move-exception
            goto L_0x007c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.ce.f(android.app.Activity):void");
    }

    /* access modifiers changed from: private */
    public void fK() {
        synchronized (this) {
            if (this.zq != null) {
                this.zq.cancel();
                this.zq.purge();
                this.zq = null;
                System.gc();
            }
        }
    }

    public final boolean a(l lVar) {
        if (lVar == null) {
            return false;
        }
        return this.iK.add(lVar);
    }

    public final void aV(String str) {
        String str2 = str;
        while (true) {
            this.zu = null;
            if (this.zx == null || this.zx.sp == null) {
                aW(str2);
            } else {
                bm bmVar = this.zx;
                bmVar.sq++;
                if (!(bmVar.sq < bmVar.sp.length())) {
                    aW(str2);
                    return;
                }
                this.zu = new au(this, this.zx.dt(), false);
                if (!this.zu.isInitialized()) {
                    str2 = "";
                } else {
                    return;
                }
            }
        }
        aW(str2);
    }

    public final boolean b(l lVar) {
        return this.iK.remove(lVar);
    }

    public final void c(long j) {
        fK();
        this.zs = j;
        if (this.zs >= 0) {
            if (this.zs < 15000) {
                this.zs = 15000;
            }
            try {
                this.zq = new Timer();
                this.zq.scheduleAtFixedRate(new bt(this, this.za), this.zs, this.zs);
            } catch (Exception e) {
            }
        }
    }

    public final boolean fI() {
        return this.zg == 1;
    }

    public final boolean fJ() {
        return this.zh == 1;
    }

    public final void fL() {
        aW(null);
    }

    public final void fM() {
        synchronized (this) {
            if (this.zn != null) {
                this.zn.interrupt();
            }
        }
        fK();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        fK();
        if (this.zu != null) {
            this.zu.onStop();
        }
        super.finalize();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 7) {
                try {
                    super.onDetachedFromWindow();
                    super.stopFlipping();
                } catch (IllegalArgumentException e) {
                    Log.w(this.TAG, "Android project  issue 6191  workaround.");
                    super.stopFlipping();
                } catch (Throwable th) {
                    super.stopFlipping();
                    throw th;
                }
            } else {
                super.onDetachedFromWindow();
            }
        } catch (Exception e2) {
            super.onDetachedFromWindow();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        super.onRestoreInstanceState(View.BaseSavedState.EMPTY_STATE);
        try {
            this.zw = ((Bundle) parcelable).getString("response");
            if (!this.zw.equals("")) {
                try {
                    this.zx = new bm(this.zw, ((Bundle) parcelable).getInt("nCreative"));
                } catch (Exception e) {
                }
                try {
                    bw.d((Activity) getContext());
                } catch (Exception e2) {
                }
                try {
                    if (this.zn != null) {
                        this.zn.interrupt();
                    }
                    if (bw.aD(this.zl)) {
                        this.zu = new au(this, this.zx.dt(), true);
                        c(bw.aE(this.zl));
                    } else {
                        Iterator it = this.iK.iterator();
                        while (it.hasNext()) {
                            l lVar = (l) it.next();
                            if (lVar != null) {
                                lVar.a(this, -999999);
                            }
                        }
                    }
                } catch (Exception e3) {
                }
                this.zi = true;
            }
        } catch (Exception e4) {
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        super.onSaveInstanceState();
        fK();
        if (this.zu == null) {
            return null;
        }
        this.zu.onStop();
        Bundle bundle = new Bundle();
        bundle.putString("response", this.zw);
        bundle.putInt("nCreative", this.zx.sq);
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i) {
        if (i == 0 && this.zu != null && !this.zu.nJ) {
            this.zu.cr();
        }
    }

    public void onWindowFocusChanged(boolean z) {
        if (!z) {
            try {
                fK();
            } catch (Exception e) {
            }
            try {
                this.ar.xe.fN();
            } catch (Exception e2) {
            }
            try {
                if (this.zu != null) {
                    this.zu.onPause();
                }
            } catch (Exception e3) {
            }
        } else if (this.zu != null) {
            if (!this.zr) {
                if (bw.aD(this.zl)) {
                    if (this.zs != 0) {
                        c(this.zs);
                    } else {
                        c(bw.aE(this.zl));
                    }
                }
                this.zr = false;
            }
            if (this.zu != null) {
                this.zu.onResume();
            }
        }
    }

    public final void r(boolean z) {
        this.zg = z ? 1 : 0;
    }

    public final void s(boolean z) {
        this.zh = z ? 1 : 0;
    }

    public void setLayoutParams(ViewGroup.LayoutParams layoutParams) {
        int parseInt = Integer.parseInt(this.zl.split("x")[0]);
        int parseInt2 = Integer.parseInt(this.zl.split("x")[1]);
        if (layoutParams.width == parseInt && layoutParams.height == parseInt2) {
            this.width = (float) parseInt;
            this.height = (float) parseInt2;
        } else {
            this.width = TypedValue.applyDimension(1, (float) parseInt, getResources().getDisplayMetrics());
            this.height = TypedValue.applyDimension(1, (float) parseInt2, getResources().getDisplayMetrics());
            this.c = getResources().getDisplayMetrics().density;
        }
        layoutParams.width = (int) this.width;
        layoutParams.height = (int) this.height;
        super.setLayoutParams(layoutParams);
    }
}
