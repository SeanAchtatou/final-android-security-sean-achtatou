package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public final class ab {
    private static int iH = 0;
    private String TAG;
    private bw ar;
    private Activity av;
    private boolean iD;
    private boolean iE;
    private ArrayList iF;
    private ArrayList iG;
    int iI;
    int iJ;
    HashSet iK;
    private n iL;

    private boolean bt() {
        if (!this.iD || this.iL == null) {
            Log.e(this.TAG, "FullScreen Ad did not display, ad not yet loaded.");
            return false;
        }
        try {
            if (!((ActivityManager) this.av.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getPackageName().equals(this.av.getPackageName())) {
                this.ar.xe.fN();
                return false;
            }
        } catch (Exception e) {
        }
        bw.fm().aQ = this.iL;
        Intent intent = new Intent();
        String packageName = this.av.getPackageName();
        intent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "fullscreen");
        this.av.startActivity(intent);
        Iterator it = this.iK.iterator();
        while (it.hasNext()) {
            it.next();
        }
        Iterator it2 = this.iF.iterator();
        while (it2.hasNext()) {
            new Thread(new ax((String) it2.next(), new bu())).start();
        }
        this.iL = null;
        this.iD = false;
        this.iE = false;
        this.iF = null;
        this.iG = null;
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void a(n nVar) {
        this.iD = true;
        this.iL = nVar;
        Iterator it = this.iK.iterator();
        while (it.hasNext()) {
            it.next();
        }
        if (this.iE) {
            bt();
        }
    }

    /* access modifiers changed from: package-private */
    public final Activity getActivity() {
        return this.av;
    }
}
