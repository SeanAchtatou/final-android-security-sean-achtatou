package com.mobclix.android.sdk;

import android.app.AlertDialog;
import android.content.Context;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.EditText;

final class ba extends WebChromeClient {
    private /* synthetic */ av qA;

    ba(av avVar) {
        this.qA = avVar;
    }

    public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.qA.aQ.L());
        builder.setMessage(str2).setCancelable(false);
        builder.setPositiveButton(17039370, new bc(this, jsResult));
        builder.create().show();
        return true;
    }

    public final boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.qA.aQ.L());
        builder.setMessage(str2).setTitle("Confirm Navigation").setCancelable(false);
        builder.setPositiveButton("Leave this Page", new bf(this, jsResult));
        builder.setNegativeButton("Stay on this Page", new bk(this, jsResult));
        builder.create().show();
        return true;
    }

    public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.qA.aQ.L());
        builder.setMessage(str2).setCancelable(false);
        builder.setPositiveButton(17039370, new bd(this, jsResult));
        builder.setNegativeButton(17039360, new be(this, jsResult));
        builder.create().show();
        return true;
    }

    public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        Context L = this.qA.aQ.L();
        AlertDialog.Builder builder = new AlertDialog.Builder(L);
        EditText editText = new EditText(L);
        if (str3 != null) {
            editText.setText(str3);
        }
        builder.setMessage(str2).setView(editText).setCancelable(false).setPositiveButton(17039370, new bj(this, jsPromptResult, editText)).setNegativeButton(17039360, new bl(this, jsPromptResult));
        builder.create().show();
        return true;
    }
}
