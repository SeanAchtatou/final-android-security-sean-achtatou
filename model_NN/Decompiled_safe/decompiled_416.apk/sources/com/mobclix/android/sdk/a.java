package com.mobclix.android.sdk;

import android.os.Message;
import android.widget.ImageView;
import android.widget.LinearLayout;

final class a extends bu {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ y f3a;

    a(y yVar) {
        this.f3a = yVar;
    }

    public final void handleMessage(Message message) {
        if (this.qm != null) {
            int width = this.qm.getWidth();
            int height = this.qm.getHeight();
            this.f3a.hN.setLayoutParams(new LinearLayout.LayoutParams(MobclixBrowserActivity.a(this.f3a.cC, width), MobclixBrowserActivity.a(this.f3a.cC, height)));
            this.f3a.hN.setScaleType(ImageView.ScaleType.FIT_CENTER);
            this.f3a.hN.setImageBitmap(this.qm);
        }
        this.f3a.cC.d.sendEmptyMessage(0);
    }
}
