package com.mobclix.android.sdk;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;

final class aa implements Runnable {
    private String hV;
    private String hW;
    private /* synthetic */ k hX;

    aa(k kVar, String str, String str2) {
        this.hX = kVar;
        this.hW = str2;
        this.hV = str;
    }

    public final void run() {
        try {
            Bitmap decodeFile = BitmapFactory.decodeFile(new File(this.hV).getAbsolutePath());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            decodeFile.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.hW).openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();
            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            byteArrayOutputStream.writeTo(dataOutputStream);
            dataOutputStream.flush();
            dataOutputStream.close();
            byteArrayOutputStream.close();
            System.gc();
            if (this.hX.bm != null) {
                this.hX.aQ.loadUrl(k.a(this.hX.bm, "", false));
            }
        } catch (Exception e) {
            bw.fm().xv.ck.h(e.toString());
            bw.fm().xv.ck.h("Error processing photo.");
        }
        this.hX.bl = null;
    }
}
