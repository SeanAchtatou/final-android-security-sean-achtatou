package com.mobclix.android.sdk;

import android.app.Activity;

final class g implements Runnable {
    private bw ar = bw.fm();
    private String as;
    private String at;
    private bx au;
    private Activity av;

    g(String str, String str2) {
        this.as = str;
        this.at = str2;
        this.av = null;
        this.au = null;
    }

    private void j() {
        if (this.au != null && this.av != null) {
            try {
                this.av.runOnUiThread(new ai(this));
            } catch (Exception e) {
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r1.flush();
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0072, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0073, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0067 A[SYNTHETIC, Splitter:B:28:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0072 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x0024] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r5 = this;
            r1 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x007a, all -> 0x0064 }
            java.lang.String r2 = r5.as     // Catch:{ Exception -> 0x007a, all -> 0x0064 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x007a, all -> 0x0064 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x007a, all -> 0x0064 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x007a, all -> 0x0064 }
            java.lang.String r2 = "POST"
            r0.setRequestMethod(r2)     // Catch:{ Exception -> 0x007a, all -> 0x0064 }
            r2 = 1
            r0.setDoInput(r2)     // Catch:{ Exception -> 0x007a, all -> 0x0064 }
            r2 = 1
            r0.setDoOutput(r2)     // Catch:{ Exception -> 0x007a, all -> 0x0064 }
            java.io.DataOutputStream r2 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x007a, all -> 0x0064 }
            java.io.OutputStream r3 = r0.getOutputStream()     // Catch:{ Exception -> 0x007a, all -> 0x0064 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x007a, all -> 0x0064 }
            java.lang.String r1 = r5.at     // Catch:{ Exception -> 0x0054, all -> 0x0072 }
            r2.writeBytes(r1)     // Catch:{ Exception -> 0x0054, all -> 0x0072 }
            int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x0054, all -> 0x0072 }
            r3 = 200(0xc8, float:2.8E-43)
            if (r1 != r3) goto L_0x0050
            com.mobclix.android.sdk.bx r1 = r5.au     // Catch:{ Exception -> 0x0054, all -> 0x0072 }
            if (r1 == 0) goto L_0x0039
            android.app.Activity r1 = r5.av     // Catch:{ Exception -> 0x0054, all -> 0x0072 }
            if (r1 != 0) goto L_0x0043
        L_0x0039:
            r0.disconnect()     // Catch:{ Exception -> 0x0054, all -> 0x0072 }
            r2.flush()     // Catch:{ Exception -> 0x006e }
            r2.close()     // Catch:{ Exception -> 0x006e }
        L_0x0042:
            return
        L_0x0043:
            android.app.Activity r1 = r5.av     // Catch:{ Exception -> 0x004e, all -> 0x0072 }
            com.mobclix.android.sdk.aj r3 = new com.mobclix.android.sdk.aj     // Catch:{ Exception -> 0x004e, all -> 0x0072 }
            r3.<init>(r5)     // Catch:{ Exception -> 0x004e, all -> 0x0072 }
            r1.runOnUiThread(r3)     // Catch:{ Exception -> 0x004e, all -> 0x0072 }
            goto L_0x0039
        L_0x004e:
            r1 = move-exception
            goto L_0x0039
        L_0x0050:
            r5.j()     // Catch:{ Exception -> 0x0054, all -> 0x0072 }
            goto L_0x0039
        L_0x0054:
            r0 = move-exception
            r0 = r2
        L_0x0056:
            r5.j()     // Catch:{ all -> 0x0075 }
            if (r0 == 0) goto L_0x0042
            r0.flush()     // Catch:{ Exception -> 0x0062 }
            r0.close()     // Catch:{ Exception -> 0x0062 }
            goto L_0x0042
        L_0x0062:
            r0 = move-exception
            goto L_0x0042
        L_0x0064:
            r0 = move-exception
        L_0x0065:
            if (r1 == 0) goto L_0x006d
            r1.flush()     // Catch:{ Exception -> 0x0070 }
            r1.close()     // Catch:{ Exception -> 0x0070 }
        L_0x006d:
            throw r0
        L_0x006e:
            r0 = move-exception
            goto L_0x0042
        L_0x0070:
            r0 = move-exception
            goto L_0x0042
        L_0x0072:
            r0 = move-exception
            r1 = r2
            goto L_0x0065
        L_0x0075:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0065
        L_0x007a:
            r0 = move-exception
            r0 = r1
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.g.run():void");
    }
}
