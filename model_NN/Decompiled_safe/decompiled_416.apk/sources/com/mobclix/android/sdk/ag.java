package com.mobclix.android.sdk;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

final class ag implements LocationListener {
    private /* synthetic */ cg ko;

    ag(cg cgVar) {
        this.ko = cgVar;
    }

    public final void onLocationChanged(Location location) {
        try {
            if (this.ko.zF != null) {
                this.ko.zF.cancel();
                this.ko.zF.purge();
                this.ko.zF = null;
            }
            this.ko.zH.a(location);
        } catch (Exception e) {
        }
        try {
            this.ko.zG.removeUpdates(this);
            this.ko.zG.removeUpdates(this.ko.zI);
        } catch (Exception e2) {
        }
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
