package com.mobclix.android.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.location.Criteria;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import b.a.b;
import b.a.f;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;

public final class bw {
    /* access modifiers changed from: private */
    public static final bw ar = new bw();
    static HashMap wA = new HashMap();
    static HashMap wB = new HashMap();
    static HashMap wC = new HashMap();
    static HashMap wD = new HashMap();
    static final String[] wt = {"320x50", "300x250"};
    static HashMap ww = new HashMap();
    static HashMap wx = new HashMap();
    static HashMap wy = new HashMap();
    static HashMap wz = new HashMap();
    private static boolean xw = false;
    n aQ;
    /* access modifiers changed from: private */
    public Context aa;
    private Handler mHandler;
    private aq mz = aq.cl();
    String wE = "http://ads.mobclix.com/";
    String wF = "http://data.mobclix.com/post/config";
    String wG = "http://data.mobclix.com/post/sendData";
    String wH = "http://vc.mobclix.com";
    String wI = "http://data.mobclix.com/post/feedback";
    String wJ = "http://data.mobclix.com/post/debug";
    List wK = new ArrayList();
    int wL = 30000;
    int wM = 120000;
    int wN = 0;
    boolean wO = false;
    private boolean wP = false;
    private boolean wQ = false;
    private long wR = 0;
    private long wS = 0;
    private long wT = 0;
    private Timer wU = new Timer();
    private String wV = "android";
    private String wW = "null";
    private String wX = "null";
    String wY = "null";
    private String wZ = "null";
    private SharedPreferences wu = null;
    private b wv = new b();
    private String xa = "null";
    private String xb = "null";
    private String xc = "null";
    private Handler xd;
    cg xe = new cg();
    /* access modifiers changed from: private */
    public String xf = "null";
    /* access modifiers changed from: private */
    public String xg = "null";
    private String xh = "null";
    private String xi = "null";
    private String xj = "null";
    private String xk = "null";
    private String xl = "null";
    private int xm = -1;
    /* access modifiers changed from: private */
    public int xn = -1;
    private String xo = "null";
    private int xp = 16;
    HashMap xq = new HashMap();
    private boolean xr = false;
    private Criteria xs;
    String xt = null;
    boolean xu = false;
    n xv;

    private bw() {
    }

    static void a(CookieStore cookieStore, String str) {
        try {
            CookieManager instance = CookieManager.getInstance();
            List cookies = cookieStore.getCookies();
            StringBuffer stringBuffer = new StringBuffer();
            if (!cookies.isEmpty()) {
                for (int i = 0; i < cookies.size(); i++) {
                    Cookie cookie = (Cookie) cookies.get(i);
                    stringBuffer.append(cookie.getName()).append("=").append(cookie.getValue());
                    if (cookie.getExpiryDate() != null) {
                        stringBuffer.append("; expires=").append(new SimpleDateFormat("E, dd-MMM-yyyy HH:mm:ss").format(cookie.getExpiryDate())).append(" GMT");
                    }
                    if (cookie.getPath() != null) {
                        stringBuffer.append("; path=").append(cookie.getPath());
                    }
                    if (cookie.getDomain() != null) {
                        stringBuffer.append("; domain=").append(cookie.getDomain());
                    }
                    instance.setCookie(str, stringBuffer.toString());
                }
                CookieSyncManager.getInstance().sync();
                CookieSyncManager.getInstance().stopSync();
            }
        } catch (Exception e) {
        }
    }

    static String aC(String str) {
        try {
            return (String) ww.get(str);
        } catch (Exception e) {
            return "";
        }
    }

    static boolean aD(String str) {
        try {
            return ((Boolean) wx.get(str)).booleanValue();
        } catch (Exception e) {
            return true;
        }
    }

    static long aE(String str) {
        try {
            return ((Long) wy.get(str)).longValue();
        } catch (Exception e) {
            return -1;
        }
    }

    static boolean aF(String str) {
        try {
            return ((Boolean) wz.get(str)).booleanValue();
        } catch (Exception e) {
            return false;
        }
    }

    static Long aG(String str) {
        try {
            return (Long) wA.get(str);
        } catch (Exception e) {
            return 0L;
        }
    }

    static boolean aH(String str) {
        try {
            return ((Boolean) wB.get(str)).booleanValue();
        } catch (Exception e) {
            return true;
        }
    }

    static String aI(String str) {
        try {
            return ar.wu.getString(str, "");
        } catch (Exception e) {
            return "";
        }
    }

    static boolean aJ(String str) {
        try {
            return ar.wu.contains(str);
        } catch (Exception e) {
            return false;
        }
    }

    static void aK(String str) {
        try {
            SharedPreferences.Editor edit = ar.wu.edit();
            edit.remove(str);
            edit.commit();
        } catch (Exception e) {
        }
    }

    private static String aL(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(str.getBytes(), 0, str.length());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                stringBuffer.append(Integer.toHexString(b2 & 255));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    static String aM(String str) {
        try {
            return CookieManager.getInstance().getCookie(str);
        } catch (Exception e) {
            return "";
        }
    }

    private static synchronized void c(Activity activity) {
        synchronized (bw.class) {
            if (activity == null) {
                throw new Resources.NotFoundException("Activity not provided.");
            }
            try {
                ar.aa = activity.getApplicationContext();
            } catch (Exception e) {
            }
            if (!xw) {
                try {
                    ar.e(activity);
                } catch (ca e2) {
                    throw e2;
                } catch (Exception e3) {
                }
            }
            if (xw) {
                ar.q(true);
            }
        }
        return;
    }

    static void c(Map map) {
        try {
            SharedPreferences.Editor edit = ar.wu.edit();
            for (Map.Entry entry : map.entrySet()) {
                edit.putString((String) entry.getKey(), (String) entry.getValue());
            }
            edit.commit();
        } catch (Exception e) {
        }
    }

    public static final synchronized void d(Activity activity) {
        synchronized (bw.class) {
            c(activity);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:48:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0141  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void e(android.app.Activity r11) {
        /*
            r10 = this;
            r9 = 2
            r8 = 0
            r2 = 0
            r7 = 1
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = r11.getPackageName()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.<init>(r1)
            java.lang.String r1 = ".MCConfig"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.content.SharedPreferences r0 = r11.getSharedPreferences(r0, r2)
            r10.wu = r0
            java.lang.String[] r0 = com.mobclix.android.sdk.aq.mE
            int r1 = r0.length
        L_0x0024:
            if (r2 < r1) goto L_0x006a
            com.mobclix.android.sdk.aq r0 = r10.mz
            if (r0 != 0) goto L_0x0030
            com.mobclix.android.sdk.aq r0 = com.mobclix.android.sdk.aq.cl()
            r10.mz = r0
        L_0x0030:
            com.mobclix.android.sdk.aq r0 = r10.mz
            java.lang.String r1 = com.mobclix.android.sdk.aq.mC
            java.lang.String r0 = r0.ac(r1)
            com.mobclix.android.sdk.aq r1 = r10.mz
            java.lang.String r2 = "init"
            java.lang.String r0 = r1.i(r0, r2)
            com.mobclix.android.sdk.aq r1 = r10.mz
            java.lang.String r2 = "environment"
            java.lang.String r1 = r1.i(r0, r2)
            java.lang.String r0 = ""
            java.lang.String r0 = r11.getPackageName()     // Catch:{ Exception -> 0x03e9 }
        L_0x004e:
            android.content.pm.PackageManager r2 = r11.getPackageManager()     // Catch:{ NameNotFoundException -> 0x009c }
            r3 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r0 = r2.getApplicationInfo(r0, r3)     // Catch:{ NameNotFoundException -> 0x009c }
        L_0x0058:
            android.os.Bundle r2 = r0.metaData     // Catch:{ NullPointerException -> 0x00a6 }
            java.lang.String r3 = "com.mobclix.APPLICATION_ID"
            java.lang.String r2 = r2.getString(r3)     // Catch:{ NullPointerException -> 0x00a6 }
            if (r2 != 0) goto L_0x00af
            android.content.res.Resources$NotFoundException r0 = new android.content.res.Resources$NotFoundException
            java.lang.String r1 = "com.mobclix.APPLICATION_ID not found in the Android Manifest xml."
            r0.<init>(r1)
            throw r0
        L_0x006a:
            r3 = r0[r2]
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "debug_"
            r4.<init>(r5)
            java.lang.StringBuilder r4 = r4.append(r3)
            java.lang.String r4 = r4.toString()
            boolean r4 = aJ(r4)
            if (r4 == 0) goto L_0x0099
            java.util.HashMap r4 = com.mobclix.android.sdk.bw.ww
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "debug_"
            r5.<init>(r6)
            java.lang.StringBuilder r5 = r5.append(r3)
            java.lang.String r5 = r5.toString()
            java.lang.String r5 = aI(r5)
            r4.put(r3, r5)
        L_0x0099:
            int r2 = r2 + 1
            goto L_0x0024
        L_0x009c:
            r0 = move-exception
            java.lang.String r0 = "mobclix-controller"
            java.lang.String r2 = "Application Key Started"
            android.util.Log.e(r0, r2)
            r0 = r8
            goto L_0x0058
        L_0x00a6:
            r0 = move-exception
            android.content.res.Resources$NotFoundException r0 = new android.content.res.Resources$NotFoundException
            java.lang.String r1 = "com.mobclix.APPLICATION_ID not found in the Android Manifest xml."
            r0.<init>(r1)
            throw r0
        L_0x00af:
            r10.xo = r2
            android.os.Bundle r0 = r0.metaData     // Catch:{ Exception -> 0x0111 }
            java.lang.String r2 = "com.mobclix.LOG_LEVEL"
            java.lang.String r0 = r0.getString(r2)     // Catch:{ Exception -> 0x0111 }
        L_0x00b9:
            r2 = 16
            if (r0 == 0) goto L_0x0401
            java.lang.String r3 = "debug"
            boolean r3 = r0.equalsIgnoreCase(r3)
            if (r3 == 0) goto L_0x0114
            r0 = r7
        L_0x00c6:
            r10.xp = r0
            r10.aa = r11
            java.lang.String r0 = r10.xo
            if (r0 == 0) goto L_0x00d8
            java.lang.String r0 = r10.xo
            java.lang.String r2 = ""
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x00dc
        L_0x00d8:
            java.lang.String r0 = "null"
            r10.xo = r0
        L_0x00dc:
            java.lang.String r0 = android.os.Build.VERSION.RELEASE
            r10.wW = r0
            java.lang.String r0 = r10.wW
            if (r0 == 0) goto L_0x00ee
            java.lang.String r0 = r10.wW
            java.lang.String r2 = ""
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x00f2
        L_0x00ee:
            java.lang.String r0 = "null"
            r10.wW = r0
        L_0x00f2:
            java.lang.String r0 = ""
            android.content.Context r2 = r10.aa
            android.content.pm.PackageManager r2 = r2.getPackageManager()
            android.content.Context r3 = r10.aa     // Catch:{ Exception -> 0x013e }
            java.lang.String r0 = r3.getPackageName()     // Catch:{ Exception -> 0x013e }
            r3 = r0
        L_0x0101:
            java.lang.String r0 = "android.permission.GET_TASKS"
            int r0 = r2.checkPermission(r0, r3)
            if (r0 == 0) goto L_0x0141
            com.mobclix.android.sdk.ca r0 = new com.mobclix.android.sdk.ca
            java.lang.String r1 = "Missing required permission GET_TASKS."
            r0.<init>(r10, r1)
            throw r0
        L_0x0111:
            r0 = move-exception
            r0 = r8
            goto L_0x00b9
        L_0x0114:
            java.lang.String r3 = "info"
            boolean r3 = r0.equalsIgnoreCase(r3)
            if (r3 == 0) goto L_0x011e
            r0 = r9
            goto L_0x00c6
        L_0x011e:
            java.lang.String r3 = "warn"
            boolean r3 = r0.equalsIgnoreCase(r3)
            if (r3 == 0) goto L_0x0128
            r0 = 4
            goto L_0x00c6
        L_0x0128:
            java.lang.String r3 = "error"
            boolean r3 = r0.equalsIgnoreCase(r3)
            if (r3 == 0) goto L_0x0133
            r0 = 8
            goto L_0x00c6
        L_0x0133:
            java.lang.String r3 = "fatal"
            boolean r0 = r0.equalsIgnoreCase(r3)
            if (r0 == 0) goto L_0x0401
            r0 = 16
            goto L_0x00c6
        L_0x013e:
            r3 = move-exception
            r3 = r0
            goto L_0x0101
        L_0x0141:
            java.util.HashMap r0 = r10.xq
            java.lang.String r4 = "android.permission.GET_TASKS"
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r7)
            r0.put(r4, r5)
            java.lang.String r0 = "android.permission.INTERNET"
            int r0 = r2.checkPermission(r0, r3)
            if (r0 == 0) goto L_0x015c
            com.mobclix.android.sdk.ca r0 = new com.mobclix.android.sdk.ca
            java.lang.String r1 = "Missing required permission INTERNET."
            r0.<init>(r10, r1)
            throw r0
        L_0x015c:
            java.util.HashMap r0 = r10.xq
            java.lang.String r4 = "android.permission.INTERNET"
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r7)
            r0.put(r4, r5)
            java.lang.String r0 = "android.permission.READ_PHONE_STATE"
            int r0 = r2.checkPermission(r0, r3)
            if (r0 == 0) goto L_0x0177
            com.mobclix.android.sdk.ca r0 = new com.mobclix.android.sdk.ca
            java.lang.String r1 = "Missing required permission READ_PHONE_STATE."
            r0.<init>(r10, r1)
            throw r0
        L_0x0177:
            java.util.HashMap r0 = r10.xq
            java.lang.String r4 = "android.permission.READ_PHONE_STATE"
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r7)
            r0.put(r4, r5)
            java.lang.String r0 = "android.permission.BATTERY_STATS"
            int r0 = r2.checkPermission(r0, r3)     // Catch:{ Exception -> 0x03fe }
            if (r0 != 0) goto L_0x019f
            android.content.Context r0 = r10.aa     // Catch:{ Exception -> 0x03fe }
            android.content.Context r0 = r0.getApplicationContext()     // Catch:{ Exception -> 0x03fe }
            com.mobclix.android.sdk.i r4 = new com.mobclix.android.sdk.i     // Catch:{ Exception -> 0x03fe }
            r4.<init>(r10)     // Catch:{ Exception -> 0x03fe }
            android.content.IntentFilter r5 = new android.content.IntentFilter     // Catch:{ Exception -> 0x03fe }
            java.lang.String r6 = "android.intent.action.BATTERY_CHANGED"
            r5.<init>(r6)     // Catch:{ Exception -> 0x03fe }
            r0.registerReceiver(r4, r5)     // Catch:{ Exception -> 0x03fe }
        L_0x019f:
            java.util.HashMap r0 = r10.xq     // Catch:{ Exception -> 0x03fe }
            java.lang.String r4 = "android.permission.BATTERY_STATS"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x03fe }
            r0.put(r4, r5)     // Catch:{ Exception -> 0x03fe }
        L_0x01ab:
            java.lang.String r0 = "android.permission.CAMERA"
            int r0 = r2.checkPermission(r0, r3)
            if (r0 != 0) goto L_0x01be
            java.util.HashMap r0 = r10.xq
            java.lang.String r4 = "android.permission.CAMERA"
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r7)
            r0.put(r4, r5)
        L_0x01be:
            java.lang.String r0 = "android.permission.READ_CALENDAR"
            int r0 = r2.checkPermission(r0, r3)
            if (r0 != 0) goto L_0x01d1
            java.util.HashMap r0 = r10.xq
            java.lang.String r4 = "android.permission.READ_CALENDAR"
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r7)
            r0.put(r4, r5)
        L_0x01d1:
            java.lang.String r0 = "android.permission.WRITE_CALENDAR"
            int r0 = r2.checkPermission(r0, r3)
            if (r0 != 0) goto L_0x01e4
            java.util.HashMap r0 = r10.xq
            java.lang.String r4 = "android.permission.WRITE_CALENDAR"
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r7)
            r0.put(r4, r5)
        L_0x01e4:
            java.lang.String r0 = "android.permission.READ_CONTACTS"
            int r0 = r2.checkPermission(r0, r3)
            if (r0 != 0) goto L_0x01f7
            java.util.HashMap r0 = r10.xq
            java.lang.String r4 = "android.permission.READ_CONTACTS"
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r7)
            r0.put(r4, r5)
        L_0x01f7:
            java.lang.String r0 = "android.permission.WRITE_CONTACTS"
            int r0 = r2.checkPermission(r0, r3)
            if (r0 != 0) goto L_0x020a
            java.util.HashMap r0 = r10.xq
            java.lang.String r4 = "android.permission.WRITE_CONTACTS"
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r7)
            r0.put(r4, r5)
        L_0x020a:
            java.lang.String r0 = "android.permission.GET_ACCOUNTS"
            int r0 = r2.checkPermission(r0, r3)
            if (r0 != 0) goto L_0x021d
            java.util.HashMap r0 = r10.xq
            java.lang.String r4 = "android.permission.GET_ACCOUNTS"
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r7)
            r0.put(r4, r5)
        L_0x021d:
            java.lang.String r0 = "android.permission.VIBRATE"
            int r0 = r2.checkPermission(r0, r3)
            if (r0 != 0) goto L_0x0230
            java.util.HashMap r0 = r10.xq
            java.lang.String r4 = "android.permission.VIBRATE"
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r7)
            r0.put(r4, r5)
        L_0x0230:
            java.lang.String r0 = "android.permission.ACCESS_FINE_LOCATION"
            int r0 = r2.checkPermission(r0, r3)     // Catch:{ Exception -> 0x03cb }
            if (r0 != 0) goto L_0x03a5
            android.location.Criteria r0 = new android.location.Criteria     // Catch:{ Exception -> 0x03cb }
            r0.<init>()     // Catch:{ Exception -> 0x03cb }
            r10.xs = r0     // Catch:{ Exception -> 0x03cb }
            android.location.Criteria r0 = r10.xs     // Catch:{ Exception -> 0x03cb }
            r4 = 1
            r0.setAccuracy(r4)     // Catch:{ Exception -> 0x03cb }
            r0 = 1
            r10.xr = r0     // Catch:{ Exception -> 0x03cb }
            java.util.HashMap r0 = r10.xq     // Catch:{ Exception -> 0x03cb }
            java.lang.String r4 = "android.permission.ACCESS_FINE_LOCATION"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x03cb }
            r0.put(r4, r5)     // Catch:{ Exception -> 0x03cb }
        L_0x0254:
            java.lang.String r0 = "android.permission.ACCESS_NETWORK_STATE"
            int r0 = r2.checkPermission(r0, r3)     // Catch:{ Exception -> 0x03cb }
            if (r0 != 0) goto L_0x0268
            java.util.HashMap r0 = r10.xq     // Catch:{ Exception -> 0x03cb }
            java.lang.String r4 = "android.permission.ACCESS_NETWORK_STATE"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x03cb }
            r0.put(r4, r5)     // Catch:{ Exception -> 0x03cb }
        L_0x0268:
            android.content.Context r0 = r10.aa     // Catch:{ Exception -> 0x03d3 }
            java.lang.String r4 = "phone"
            java.lang.Object r0 = r0.getSystemService(r4)     // Catch:{ Exception -> 0x03d3 }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Exception -> 0x03d3 }
        L_0x0272:
            r4 = 0
            android.content.pm.PackageInfo r2 = r2.getPackageInfo(r3, r4)     // Catch:{ Exception -> 0x03fb }
            java.lang.String r2 = r2.versionName     // Catch:{ Exception -> 0x03fb }
            r10.wX = r2     // Catch:{ Exception -> 0x03fb }
        L_0x027b:
            java.lang.String r2 = r10.wX
            if (r2 == 0) goto L_0x0289
            java.lang.String r2 = r10.wX
            java.lang.String r3 = ""
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x028d
        L_0x0289:
            java.lang.String r2 = "null"
            r10.wX = r2
        L_0x028d:
            android.content.ContentResolver r2 = r11.getContentResolver()     // Catch:{ Exception -> 0x03d7 }
            java.lang.String r3 = "android_id"
            java.lang.String r2 = android.provider.Settings.System.getString(r2, r3)     // Catch:{ Exception -> 0x03d7 }
            r10.wZ = r2     // Catch:{ Exception -> 0x03d7 }
        L_0x0299:
            java.lang.String r2 = r10.wZ
            if (r2 == 0) goto L_0x02a7
            java.lang.String r2 = r10.wZ
            java.lang.String r3 = ""
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x02ab
        L_0x02a7:
            java.lang.String r2 = "null"
            r10.wZ = r2
        L_0x02ab:
            java.lang.String r2 = r0.getDeviceId()     // Catch:{ Exception -> 0x03f8 }
            r10.wY = r2     // Catch:{ Exception -> 0x03f8 }
        L_0x02b1:
            java.lang.String r2 = r10.wY
            if (r2 == 0) goto L_0x02bf
            java.lang.String r2 = r10.wY
            java.lang.String r3 = ""
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x02c3
        L_0x02bf:
            java.lang.String r2 = r10.wZ
            r10.wY = r2
        L_0x02c3:
            java.lang.String r2 = android.os.Build.MODEL     // Catch:{ Exception -> 0x03f5 }
            r10.xa = r2     // Catch:{ Exception -> 0x03f5 }
        L_0x02c7:
            java.lang.String r2 = r10.xa
            if (r2 == 0) goto L_0x02d5
            java.lang.String r2 = r10.xa
            java.lang.String r3 = ""
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x02d9
        L_0x02d5:
            java.lang.String r2 = "null"
            r10.xa = r2
        L_0x02d9:
            java.lang.String r2 = android.os.Build.DEVICE     // Catch:{ Exception -> 0x03f2 }
            r10.xb = r2     // Catch:{ Exception -> 0x03f2 }
        L_0x02dd:
            java.lang.String r2 = r10.xb
            if (r2 == 0) goto L_0x02eb
            java.lang.String r2 = r10.xb
            java.lang.String r3 = ""
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x02ef
        L_0x02eb:
            java.lang.String r2 = "null"
            r10.xb = r2
        L_0x02ef:
            java.lang.String r0 = r0.getNetworkOperator()     // Catch:{ Exception -> 0x03ef }
            if (r0 == 0) goto L_0x0304
            r2 = 0
            r3 = 3
            java.lang.String r2 = r0.substring(r2, r3)     // Catch:{ Exception -> 0x03ef }
            r10.xj = r2     // Catch:{ Exception -> 0x03ef }
            r2 = 3
            java.lang.String r0 = r0.substring(r2)     // Catch:{ Exception -> 0x03ef }
            r10.xk = r0     // Catch:{ Exception -> 0x03ef }
        L_0x0304:
            java.lang.String r0 = r10.xj
            if (r0 == 0) goto L_0x0312
            java.lang.String r0 = r10.xj
            java.lang.String r2 = ""
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0316
        L_0x0312:
            java.lang.String r0 = "null"
            r10.xj = r0
        L_0x0316:
            java.lang.String r0 = r10.xk
            if (r0 == 0) goto L_0x0324
            java.lang.String r0 = r10.xk
            java.lang.String r2 = ""
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0328
        L_0x0324:
            java.lang.String r0 = "null"
            r10.xk = r0
        L_0x0328:
            java.util.Locale r0 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x03ec }
            java.lang.String r2 = r0.toString()     // Catch:{ Exception -> 0x03ec }
            r10.xh = r2     // Catch:{ Exception -> 0x03ec }
            java.lang.String r0 = r0.getLanguage()     // Catch:{ Exception -> 0x03ec }
            r10.xi = r0     // Catch:{ Exception -> 0x03ec }
        L_0x0338:
            java.lang.String r0 = r10.xh
            if (r0 == 0) goto L_0x0346
            java.lang.String r0 = r10.xh
            java.lang.String r2 = ""
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x034a
        L_0x0346:
            java.lang.String r0 = "null"
            r10.xh = r0
        L_0x034a:
            java.lang.String r0 = r10.xi
            if (r0 == 0) goto L_0x0358
            java.lang.String r0 = r10.xi
            java.lang.String r2 = ""
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x035c
        L_0x0358:
            java.lang.String r0 = "null"
            r10.xi = r0
        L_0x035c:
            android.content.Context r0 = r10.aa
            android.webkit.CookieSyncManager.createInstance(r0)
            com.mobclix.android.sdk.j r0 = new com.mobclix.android.sdk.j
            r0.<init>(r10)
            r10.xd = r0
            com.mobclix.android.sdk.aq r0 = r10.mz
            java.lang.String r0 = r0.af(r1)
            com.mobclix.android.sdk.aq r1 = r10.mz
            java.lang.String r2 = "session"
            java.lang.String r6 = r1.i(r0, r2)
            com.mobclix.android.sdk.an r0 = new com.mobclix.android.sdk.an
            r0.<init>(r10)
            r10.mHandler = r0
            int r0 = r10.wL
            int r1 = r10.wM
            int r0 = java.lang.Math.min(r0, r1)
            r10.wL = r0
            java.util.Timer r0 = r10.wU
            com.mobclix.android.sdk.ay r1 = new com.mobclix.android.sdk.ay
            r1.<init>(r10)
            int r2 = r10.wL
            long r2 = (long) r2
            int r4 = r10.wL
            long r4 = (long) r4
            r0.scheduleAtFixedRate(r1, r2, r4)
            com.mobclix.android.sdk.bw.xw = r7
            com.mobclix.android.sdk.aq r0 = r10.mz
            java.lang.String r0 = r0.af(r6)
            com.mobclix.android.sdk.aq r1 = r10.mz
            r1.af(r0)
            return
        L_0x03a5:
            java.lang.String r0 = "android.permission.ACCESS_COARSE_LOCATION"
            int r0 = r2.checkPermission(r0, r3)     // Catch:{ Exception -> 0x03cb }
            if (r0 != 0) goto L_0x03ce
            android.location.Criteria r0 = new android.location.Criteria     // Catch:{ Exception -> 0x03cb }
            r0.<init>()     // Catch:{ Exception -> 0x03cb }
            r10.xs = r0     // Catch:{ Exception -> 0x03cb }
            android.location.Criteria r0 = r10.xs     // Catch:{ Exception -> 0x03cb }
            r4 = 2
            r0.setAccuracy(r4)     // Catch:{ Exception -> 0x03cb }
            r0 = 1
            r10.xr = r0     // Catch:{ Exception -> 0x03cb }
            java.util.HashMap r0 = r10.xq     // Catch:{ Exception -> 0x03cb }
            java.lang.String r4 = "android.permission.ACCESS_COARSE_LOCATION"
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x03cb }
            r0.put(r4, r5)     // Catch:{ Exception -> 0x03cb }
            goto L_0x0254
        L_0x03cb:
            r0 = move-exception
            goto L_0x0268
        L_0x03ce:
            r0 = 0
            r10.xr = r0     // Catch:{ Exception -> 0x03cb }
            goto L_0x0254
        L_0x03d3:
            r0 = move-exception
            r0 = r8
            goto L_0x0272
        L_0x03d7:
            r2 = move-exception
            android.content.ContentResolver r2 = r11.getContentResolver()     // Catch:{ Exception -> 0x03e6 }
            java.lang.String r3 = "android_id"
            java.lang.String r2 = android.provider.Settings.System.getString(r2, r3)     // Catch:{ Exception -> 0x03e6 }
            r10.wZ = r2     // Catch:{ Exception -> 0x03e6 }
            goto L_0x0299
        L_0x03e6:
            r2 = move-exception
            goto L_0x0299
        L_0x03e9:
            r2 = move-exception
            goto L_0x004e
        L_0x03ec:
            r0 = move-exception
            goto L_0x0338
        L_0x03ef:
            r0 = move-exception
            goto L_0x0304
        L_0x03f2:
            r2 = move-exception
            goto L_0x02dd
        L_0x03f5:
            r2 = move-exception
            goto L_0x02c7
        L_0x03f8:
            r2 = move-exception
            goto L_0x02b1
        L_0x03fb:
            r2 = move-exception
            goto L_0x027b
        L_0x03fe:
            r0 = move-exception
            goto L_0x01ab
        L_0x0401:
            r0 = r2
            goto L_0x00c6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.bw.e(android.app.Activity):void");
    }

    private String eW() {
        return this.xf == null ? "null" : this.xf;
    }

    private String eX() {
        return this.xg == null ? "null" : this.xg;
    }

    static String fc() {
        return "2.3.0";
    }

    public static bw fm() {
        return ar;
    }

    /* access modifiers changed from: private */
    public synchronized void fn() {
        String i = this.mz.i(this.mz.i(this.mz.ac(aq.mC), "session"), "init");
        long currentTimeMillis = System.currentTimeMillis();
        String aL = aL(String.valueOf(this.wY) + currentTimeMillis);
        this.wQ = true;
        this.wR = currentTimeMillis;
        this.wS = 0;
        this.wT = 0;
        this.wP = true;
        try {
            this.wv.a("id", URLEncoder.encode(aL, "UTF-8"));
        } catch (Exception e) {
        }
        String i2 = this.mz.i(this.mz.af(i), "config");
        this.wN = 0;
        new ap().execute(new String[0]);
        this.mz.af(this.mz.af(i2));
    }

    private void fo() {
        try {
            if (this.wP) {
                long j = this.wS - this.wR;
                if (aJ("totalSessionTime")) {
                    try {
                        j += Long.parseLong(aI("totalSessionTime"));
                    } catch (Exception e) {
                    }
                }
                if (aJ("totalIdleTime")) {
                    try {
                        this.wT += Long.parseLong(aI("totalIdleTime"));
                    } catch (Exception e2) {
                    }
                }
                HashMap hashMap = new HashMap();
                hashMap.put("totalSessionTime", Long.toString(j));
                hashMap.put("totalIdleTime", Long.toString(this.wT));
                if (this.wO) {
                    int i = 1;
                    if (aJ("offlineSessions")) {
                        try {
                            i = Integer.parseInt(aI("offlineSessions")) + 1;
                        } catch (Exception e3) {
                        }
                    }
                    hashMap.put("offlineSessions", Long.toString((long) i));
                }
                c(hashMap);
                this.wP = false;
                this.wQ = false;
                this.wR = 0;
                this.wS = 0;
                this.wT = 0;
            }
        } catch (Exception e4) {
        }
    }

    static void k(String str, String str2) {
        try {
            SharedPreferences.Editor edit = ar.wu.edit();
            edit.putString(str, str2);
            edit.commit();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void q(boolean r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            if (r7 == 0) goto L_0x0029
            boolean r2 = r6.wQ     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            if (r2 == 0) goto L_0x000d
        L_0x000b:
            monitor-exit(r6)
            return
        L_0x000d:
            boolean r2 = r6.wP     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            if (r2 != 0) goto L_0x001a
            android.os.Handler r0 = r6.mHandler     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            r1 = 0
            r0.sendEmptyMessage(r1)     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            goto L_0x000b
        L_0x0018:
            r0 = move-exception
            goto L_0x000b
        L_0x001a:
            long r2 = r6.wT     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            long r4 = r6.wS     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            long r0 = r0 - r4
            long r0 = r0 + r2
            r6.wT = r0     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            r0 = 1
            r6.wQ = r0     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            goto L_0x000b
        L_0x0026:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x0029:
            boolean r2 = r6.wQ     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            if (r2 != 0) goto L_0x003f
            long r2 = r6.wS     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            long r0 = r0 - r2
            int r2 = r6.wM     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            long r2 = (long) r2     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x000b
            boolean r0 = r6.wP     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            if (r0 == 0) goto L_0x000b
            r6.fo()     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            goto L_0x000b
        L_0x003f:
            r6.wS = r0     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            r0 = 0
            r6.wQ = r0     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            com.mobclix.android.sdk.cg r0 = r6.xe     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            r0.fN()     // Catch:{ Exception -> 0x0018, all -> 0x0026 }
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.bw.q(boolean):void");
    }

    public static final void sync() {
        if (!xw) {
            Log.v("mobclix-controller", "sync failed - You must initialize Mobclix by calling Mobclix.onCreate(this).");
        } else if (m.D() == m.ce) {
            new Thread(new bp()).start();
        }
    }

    public final String eO() {
        return this.xo == null ? "null" : this.xo;
    }

    /* access modifiers changed from: package-private */
    public final String eP() {
        return this.wV == null ? "null" : this.wV;
    }

    /* access modifiers changed from: package-private */
    public final String eQ() {
        return this.wW == null ? "null" : this.wW;
    }

    /* access modifiers changed from: package-private */
    public final String eR() {
        return this.wX == null ? "null" : this.wX;
    }

    /* access modifiers changed from: package-private */
    public final String eS() {
        return this.wZ == null ? "null" : this.wZ;
    }

    /* access modifiers changed from: package-private */
    public final String eT() {
        return this.xa == null ? "null" : this.xa;
    }

    /* access modifiers changed from: package-private */
    public final String eU() {
        return this.xb == null ? "null" : this.xb;
    }

    /* access modifiers changed from: package-private */
    public final String eV() {
        return this.xc == null ? "null" : this.xc;
    }

    /* access modifiers changed from: package-private */
    public final String eY() {
        return (eW().equals("null") || eX().equals("null")) ? "null" : String.valueOf(eW()) + "," + eX();
    }

    /* access modifiers changed from: package-private */
    public final String eZ() {
        return this.xh == null ? "null" : this.xh;
    }

    /* access modifiers changed from: package-private */
    public final String fa() {
        return this.xj == null ? "null" : this.xj;
    }

    /* access modifiers changed from: package-private */
    public final String fb() {
        return this.xk == null ? "null" : this.xk;
    }

    /* access modifiers changed from: package-private */
    public final String fd() {
        return this.wJ;
    }

    /* access modifiers changed from: package-private */
    public final String fe() {
        return this.wE;
    }

    /* access modifiers changed from: package-private */
    public final List ff() {
        return this.wK;
    }

    /* access modifiers changed from: package-private */
    public final int fg() {
        return this.wN;
    }

    /* access modifiers changed from: package-private */
    public final String fh() {
        if (this.xl.equals("null") && aJ("UserAgent")) {
            this.xl = aI("UserAgent");
        }
        return this.xl;
    }

    /* access modifiers changed from: package-private */
    public final boolean fi() {
        return this.xm != -1;
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        fo();
    }

    public final boolean fj() {
        if (this.xm != -1) {
            return this.xm == 1;
        }
        try {
            Runtime.getRuntime().exec("su");
            this.xm = 1;
        } catch (Exception e) {
            this.xm = 0;
        }
        return this.xm == 1;
    }

    /* access modifiers changed from: package-private */
    public final void fk() {
        NetworkInfo activeNetworkInfo;
        try {
            String typeName = (!((Boolean) this.xq.get("android.permission.ACCESS_NETWORK_STATE")).booleanValue() || (activeNetworkInfo = ((ConnectivityManager) this.aa.getSystemService("connectivity")).getActiveNetworkInfo()) == null) ? "u" : activeNetworkInfo.getTypeName();
            if (typeName.equals("WI_FI") || typeName.equals("WIFI")) {
                this.xc = "wifi";
            } else if (typeName.equals("MOBILE")) {
                this.xc = Integer.toString(((TelephonyManager) this.aa.getSystemService("phone")).getNetworkType());
            } else {
                this.xc = "null";
            }
            if (this.xc == null) {
                this.xc = "null";
            }
        } catch (Exception e) {
            this.xc = "null";
        }
        if (this.xr) {
            this.xd.sendEmptyMessage(0);
        }
        try {
            this.wv.a("ts", System.currentTimeMillis());
            String eY = eY();
            if (!eY.equals("null")) {
                this.wv.a("ll", eY);
            } else {
                this.wv.P("ll");
            }
            this.wv.a("g", this.xc);
        } catch (f e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public final void fl() {
        this.xe.a(this.aa, new h(this));
    }

    /* access modifiers changed from: package-private */
    public final Context getContext() {
        return this.aa;
    }

    /* access modifiers changed from: package-private */
    public final String getDeviceId() {
        return this.wY == null ? "null" : this.wY;
    }

    /* access modifiers changed from: package-private */
    public final void setUserAgent(String str) {
        this.xl = str;
        k("UserAgent", str);
    }
}
