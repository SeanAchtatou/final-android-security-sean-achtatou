package com.mobclix.android.sdk;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

final class bp implements Runnable {
    bp() {
    }

    public final synchronized void run() {
        m.cd = m.cf;
        try {
            File file = new File(String.valueOf(m.ar.getContext().getDir(m.bU, 0).getAbsolutePath()) + "/" + m.bV);
            file.mkdir();
            File[] listFiles = file.listFiles();
            int i = 0;
            while (true) {
                if (i >= listFiles.length) {
                    break;
                }
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("p=android");
                stringBuffer.append("&a=").append(URLEncoder.encode(m.ar.eO(), "UTF-8"));
                stringBuffer.append("&m=").append(URLEncoder.encode(bw.fc()));
                stringBuffer.append("&d=").append(URLEncoder.encode(m.ar.getDeviceId(), "UTF-8"));
                stringBuffer.append("&v=").append(URLEncoder.encode(m.ar.eR(), "UTF-8"));
                stringBuffer.append("&j=");
                FileInputStream fileInputStream = new FileInputStream(listFiles[i]);
                BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
                DataInputStream dataInputStream = new DataInputStream(bufferedInputStream);
                while (dataInputStream.available() != 0) {
                    stringBuffer.append(dataInputStream.readLine());
                }
                dataInputStream.close();
                bufferedInputStream.close();
                fileInputStream.close();
                stringBuffer.append("]}]");
                m.cc = stringBuffer.toString();
                try {
                    HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(m.ar.wG).openConnection();
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    httpURLConnection.setUseCaches(false);
                    httpURLConnection.setRequestMethod("POST");
                    PrintWriter printWriter = new PrintWriter(httpURLConnection.getOutputStream());
                    printWriter.print(m.cc);
                    printWriter.flush();
                    printWriter.close();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    String str = null;
                    while (true) {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        } else if (str == null) {
                            str = readLine;
                        }
                    }
                    if (!str.equals("1")) {
                        m.cd = m.cg;
                    }
                    bufferedReader.close();
                } catch (Exception e) {
                    m.cd = m.cg;
                }
                if (m.cd == m.cg) {
                    m.cd = m.ce;
                    break;
                } else {
                    listFiles[i].delete();
                    i++;
                }
            }
        } catch (Exception e2) {
        }
        m.cc = null;
        m.cb = 0;
        m.bZ = false;
        m.cd = m.ce;
    }
}
