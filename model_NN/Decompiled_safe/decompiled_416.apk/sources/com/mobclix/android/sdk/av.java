package com.mobclix.android.sdk;

import android.widget.RelativeLayout;

class av extends x {
    /* access modifiers changed from: private */
    public n aQ;
    /* access modifiers changed from: private */
    public ab ci = null;
    private k ck;
    private String cl;

    av(String str, au auVar) {
        super(auVar);
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        requestDisallowInterceptTouchEvent(true);
        this.cl = str;
        try {
            if (this.ci == null) {
                this.aQ = new n(this.fS.ch);
                this.ck = new k(this.aQ, false);
            } else {
                this.aQ = new n(this.ci);
                this.ck = new k(this.aQ, true);
            }
            this.aQ.requestDisallowInterceptTouchEvent(true);
            this.aQ.setLayoutParams(this.ci == null ? new RelativeLayout.LayoutParams(-1, -1) : new RelativeLayout.LayoutParams(this.ci.iI, this.ci.iJ));
            this.aQ.setScrollBarStyle(33554432);
            this.aQ.addJavascriptInterface(this.ck, "MOBCLIX");
            this.aQ.ck = this.ck;
            this.aQ.getSettings().setJavaScriptEnabled(true);
            this.aQ.setWebViewClient(new az(this));
            this.aQ.setWebChromeClient(new ba(this));
            this.aQ.m(this.cl);
            this.aQ.M();
            this.aQ.setFocusable(true);
            addView(this.aQ);
        } catch (Exception e) {
        }
    }

    public void onDetachedFromWindow() {
        this.ck.q();
    }
}
