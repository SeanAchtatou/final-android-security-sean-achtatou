package com.mobclix.android.sdk;

import android.os.AsyncTask;
import java.net.URLEncoder;

final class ap extends AsyncTask {
    private static String TAG = "MobclixConfig";
    private static boolean mA = false;
    private String as;
    bw mB = bw.fm();
    private aq mz = aq.cl();

    ap() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:158:0x06f7, code lost:
        r2 = r4;
        r4 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x071a, code lost:
        r2 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x071b, code lost:
        r3 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:?, code lost:
        com.mobclix.android.sdk.bw.wC.put(r12, "");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x0780, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:?, code lost:
        android.util.Log.v(com.mobclix.android.sdk.ap.TAG, "ERROR: " + r2.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x07c1, code lost:
        r5 = r4;
        r4 = r2;
        r2 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:215:0x0809, code lost:
        r5 = r2;
        r2 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:217:0x080d, code lost:
        r4 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:220:0x0814, code lost:
        r17 = r4;
        r4 = r2;
        r2 = r17;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x05b7 A[LOOP:1: B:22:0x00f3->B:136:0x05b7, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x071a A[ExcHandler: all (th java.lang.Throwable), Splitter:B:85:0x0396] */
    /* JADX WARNING: Removed duplicated region for block: B:228:0x0825 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private b.a.a ck() {
        /*
            r18 = this;
            boolean r2 = com.mobclix.android.sdk.ap.mA
            if (r2 == 0) goto L_0x0006
            r2 = 0
        L_0x0005:
            return r2
        L_0x0006:
            r2 = 1
            com.mobclix.android.sdk.ap.mA = r2
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r2 = r0
            if (r2 != 0) goto L_0x0019
            com.mobclix.android.sdk.aq r2 = com.mobclix.android.sdk.aq.cl()
            r0 = r2
            r1 = r18
            r1.mz = r0
        L_0x0019:
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r2 = r0
            java.lang.String r3 = com.mobclix.android.sdk.aq.mC
            java.lang.String r2 = r2.ac(r3)
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r3 = r0
            java.lang.String r4 = "config"
            java.lang.String r2 = r3.i(r2, r4)
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r3 = r0
            java.lang.String r4 = "update_session"
            java.lang.String r2 = r3.i(r2, r4)
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB
            r3 = r0
            r3.fk()
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r3 = r0
            java.lang.String r2 = r3.af(r2)
            r3 = 0
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r4 = r0
            java.lang.String r5 = "update"
            java.lang.String r2 = r4.i(r2, r5)
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r4 = r0
            java.lang.String r5 = "parse_cache"
            java.lang.String r2 = r4.i(r2, r5)
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r4 = r0
            java.lang.String r5 = "load_misc_settings"
            java.lang.String r2 = r4.i(r2, r5)
            java.lang.String r4 = "deviceId"
            boolean r4 = com.mobclix.android.sdk.bw.aJ(r4)     // Catch:{ Exception -> 0x01ec }
            if (r4 == 0) goto L_0x01d6
            java.lang.String r4 = "deviceId"
            java.lang.String r4 = com.mobclix.android.sdk.bw.aI(r4)     // Catch:{ Exception -> 0x01ec }
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x01ec }
            r5 = r0
            java.lang.String r5 = r5.wY     // Catch:{ Exception -> 0x01ec }
            boolean r5 = r4.equals(r5)     // Catch:{ Exception -> 0x01ec }
            if (r5 != 0) goto L_0x008f
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x01ec }
            r5 = r0
            r5.xt = r4     // Catch:{ Exception -> 0x01ec }
        L_0x008f:
            java.lang.String r4 = "idleTimeout"
            boolean r4 = com.mobclix.android.sdk.bw.aJ(r4)
            if (r4 == 0) goto L_0x00a8
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB
            r4 = r0
            java.lang.String r5 = "idleTimeout"
            java.lang.String r5 = com.mobclix.android.sdk.bw.aI(r5)
            int r5 = java.lang.Integer.parseInt(r5)
            r4.wM = r5
        L_0x00a8:
            java.lang.String r4 = "pollTime"
            boolean r4 = com.mobclix.android.sdk.bw.aJ(r4)
            if (r4 == 0) goto L_0x00c1
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB
            r4 = r0
            java.lang.String r5 = "pollTime"
            java.lang.String r5 = com.mobclix.android.sdk.bw.aI(r5)
            int r5 = java.lang.Integer.parseInt(r5)
            r4.wL = r5
        L_0x00c1:
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r4 = r0
            java.lang.String r2 = r4.af(r2)
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r4 = r0
            java.lang.String r5 = "load_adunits"
            java.lang.String r2 = r4.i(r2, r5)
            java.lang.String[] r4 = com.mobclix.android.sdk.bw.wt
            int r5 = r4.length
            r6 = 0
        L_0x00d9:
            if (r6 < r5) goto L_0x01ef
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r4 = r0
            java.lang.String r2 = r4.af(r2)
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r4 = r0
            java.lang.String r2 = r4.af(r2)
            r4 = 1
            r17 = r4
            r4 = r3
            r3 = r17
        L_0x00f3:
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB
            r5 = r0
            int r5 = r5.wN
            r6 = 1
            if (r5 != r6) goto L_0x031e
            r3 = r2
            r2 = r4
        L_0x00ff:
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB
            r4 = r0
            int r4 = r4.wN
            r5 = 1
            if (r4 == r5) goto L_0x01aa
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r4 = r0
            java.lang.String r5 = "parse_cache"
            java.lang.String r3 = r4.i(r3, r5)
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r4 = r0
            java.lang.String r5 = "load_urls"
            java.lang.String r3 = r4.i(r3, r5)
            java.lang.String r4 = "ConfigServer"
            boolean r4 = com.mobclix.android.sdk.bw.aJ(r4)
            if (r4 == 0) goto L_0x0134
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB
            r4 = r0
            java.lang.String r5 = "ConfigServer"
            java.lang.String r5 = com.mobclix.android.sdk.bw.aI(r5)
            r4.wF = r5
        L_0x0134:
            java.lang.String r4 = "AdServer"
            boolean r4 = com.mobclix.android.sdk.bw.aJ(r4)
            if (r4 == 0) goto L_0x0149
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB
            r4 = r0
            java.lang.String r5 = "AdServer"
            java.lang.String r5 = com.mobclix.android.sdk.bw.aI(r5)
            r4.wE = r5
        L_0x0149:
            java.lang.String r4 = "AnalyticsServer"
            boolean r4 = com.mobclix.android.sdk.bw.aJ(r4)
            if (r4 == 0) goto L_0x015e
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB
            r4 = r0
            java.lang.String r5 = "AnalyticsServer"
            java.lang.String r5 = com.mobclix.android.sdk.bw.aI(r5)
            r4.wG = r5
        L_0x015e:
            java.lang.String r4 = "VcServer"
            boolean r4 = com.mobclix.android.sdk.bw.aJ(r4)
            if (r4 == 0) goto L_0x0173
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB
            r4 = r0
            java.lang.String r5 = "VcServer"
            java.lang.String r5 = com.mobclix.android.sdk.bw.aI(r5)
            r4.wH = r5
        L_0x0173:
            java.lang.String r4 = "FeedbackServer"
            boolean r4 = com.mobclix.android.sdk.bw.aJ(r4)
            if (r4 == 0) goto L_0x0188
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB
            r4 = r0
            java.lang.String r5 = "FeedbackServer"
            java.lang.String r5 = com.mobclix.android.sdk.bw.aI(r5)
            r4.wI = r5
        L_0x0188:
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB
            r4 = r0
            r5 = 1
            r4.wN = r5
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB
            r4 = r0
            r5 = 1
            r4.wO = r5
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r4 = r0
            java.lang.String r3 = r4.af(r3)
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r4 = r0
            java.lang.String r3 = r4.af(r3)
        L_0x01aa:
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r4 = r0
            java.lang.String r3 = r4.af(r3)
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r4 = r0
            java.lang.String r3 = r4.af(r3)
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r4 = r0
            r4.af(r3)
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r3 = r0
            java.lang.String r4 = com.mobclix.android.sdk.aq.mC
            r3.ae(r4)
            com.mobclix.android.sdk.bw.sync()
            r3 = 0
            com.mobclix.android.sdk.ap.mA = r3
            goto L_0x0005
        L_0x01d6:
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x01ec }
            r4 = r0
            r5 = 1
            r4.xu = r5     // Catch:{ Exception -> 0x01ec }
            java.lang.String r4 = "deviceId"
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x01ec }
            r5 = r0
            java.lang.String r5 = r5.wY     // Catch:{ Exception -> 0x01ec }
            com.mobclix.android.sdk.bw.k(r4, r5)     // Catch:{ Exception -> 0x01ec }
            goto L_0x008f
        L_0x01ec:
            r4 = move-exception
            goto L_0x008f
        L_0x01ef:
            r7 = r4[r6]
            boolean r8 = com.mobclix.android.sdk.bw.aJ(r7)
            if (r8 == 0) goto L_0x02de
            java.lang.String r8 = com.mobclix.android.sdk.bw.aI(r7)
            java.lang.String r9 = ","
            java.lang.String[] r8 = r8.split(r9)
            java.util.HashMap r9 = com.mobclix.android.sdk.bw.wx     // Catch:{ Exception -> 0x029a }
            r10 = 0
            r10 = r8[r10]     // Catch:{ Exception -> 0x029a }
            java.lang.String r11 = "true"
            boolean r10 = r10.equals(r11)     // Catch:{ Exception -> 0x029a }
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r10)     // Catch:{ Exception -> 0x029a }
            r9.put(r7, r10)     // Catch:{ Exception -> 0x029a }
        L_0x0213:
            java.util.HashMap r9 = com.mobclix.android.sdk.bw.wy     // Catch:{ Exception -> 0x02a7 }
            r10 = 1
            r10 = r8[r10]     // Catch:{ Exception -> 0x02a7 }
            long r10 = java.lang.Long.parseLong(r10)     // Catch:{ Exception -> 0x02a7 }
            java.lang.Long r10 = java.lang.Long.valueOf(r10)     // Catch:{ Exception -> 0x02a7 }
            r9.put(r7, r10)     // Catch:{ Exception -> 0x02a7 }
        L_0x0223:
            java.util.HashMap r9 = com.mobclix.android.sdk.bw.wz     // Catch:{ Exception -> 0x02b5 }
            r10 = 2
            r10 = r8[r10]     // Catch:{ Exception -> 0x02b5 }
            java.lang.String r11 = "true"
            boolean r10 = r10.equals(r11)     // Catch:{ Exception -> 0x02b5 }
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r10)     // Catch:{ Exception -> 0x02b5 }
            r9.put(r7, r10)     // Catch:{ Exception -> 0x02b5 }
        L_0x0235:
            java.util.HashMap r9 = com.mobclix.android.sdk.bw.wA     // Catch:{ Exception -> 0x02c2 }
            r10 = 3
            r10 = r8[r10]     // Catch:{ Exception -> 0x02c2 }
            long r10 = java.lang.Long.parseLong(r10)     // Catch:{ Exception -> 0x02c2 }
            java.lang.Long r10 = java.lang.Long.valueOf(r10)     // Catch:{ Exception -> 0x02c2 }
            r9.put(r7, r10)     // Catch:{ Exception -> 0x02c2 }
        L_0x0245:
            java.util.HashMap r9 = com.mobclix.android.sdk.bw.wB     // Catch:{ Exception -> 0x02d1 }
            r10 = 4
            r8 = r8[r10]     // Catch:{ Exception -> 0x02d1 }
            java.lang.String r10 = "true"
            boolean r8 = r8.equals(r10)     // Catch:{ Exception -> 0x02d1 }
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)     // Catch:{ Exception -> 0x02d1 }
            r9.put(r7, r8)     // Catch:{ Exception -> 0x02d1 }
        L_0x0257:
            java.util.HashMap r8 = com.mobclix.android.sdk.bw.wD
            r9 = 0
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)
            r8.put(r7, r9)
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = java.lang.String.valueOf(r7)
            r8.<init>(r9)
            java.lang.String r9 = "CustomAdUrl"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r8 = r8.toString()
            boolean r8 = com.mobclix.android.sdk.bw.aJ(r8)
            if (r8 == 0) goto L_0x0315
            java.util.HashMap r8 = com.mobclix.android.sdk.bw.wC
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = java.lang.String.valueOf(r7)
            r9.<init>(r10)
            java.lang.String r10 = "CustomAdUrl"
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r9 = r9.toString()
            java.lang.String r9 = com.mobclix.android.sdk.bw.aI(r9)
            r8.put(r7, r9)
        L_0x0296:
            int r6 = r6 + 1
            goto L_0x00d9
        L_0x029a:
            r9 = move-exception
            java.util.HashMap r9 = com.mobclix.android.sdk.bw.wx
            r10 = 1
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r10)
            r9.put(r7, r10)
            goto L_0x0213
        L_0x02a7:
            r9 = move-exception
            java.util.HashMap r9 = com.mobclix.android.sdk.bw.wy
            r10 = 30000(0x7530, double:1.4822E-319)
            java.lang.Long r10 = java.lang.Long.valueOf(r10)
            r9.put(r7, r10)
            goto L_0x0223
        L_0x02b5:
            r9 = move-exception
            java.util.HashMap r9 = com.mobclix.android.sdk.bw.wz
            r10 = 0
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r10)
            r9.put(r7, r10)
            goto L_0x0235
        L_0x02c2:
            r9 = move-exception
            java.util.HashMap r9 = com.mobclix.android.sdk.bw.wA
            r10 = 120000(0x1d4c0, double:5.9288E-319)
            java.lang.Long r10 = java.lang.Long.valueOf(r10)
            r9.put(r7, r10)
            goto L_0x0245
        L_0x02d1:
            r8 = move-exception
            java.util.HashMap r8 = com.mobclix.android.sdk.bw.wB
            r9 = 1
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)
            r8.put(r7, r9)
            goto L_0x0257
        L_0x02de:
            java.util.HashMap r8 = com.mobclix.android.sdk.bw.wx
            r9 = 1
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)
            r8.put(r7, r9)
            java.util.HashMap r8 = com.mobclix.android.sdk.bw.wy
            r9 = 30000(0x7530, double:1.4822E-319)
            java.lang.Long r9 = java.lang.Long.valueOf(r9)
            r8.put(r7, r9)
            java.util.HashMap r8 = com.mobclix.android.sdk.bw.wz
            r9 = 0
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)
            r8.put(r7, r9)
            java.util.HashMap r8 = com.mobclix.android.sdk.bw.wA
            r9 = 120000(0x1d4c0, double:5.9288E-319)
            java.lang.Long r9 = java.lang.Long.valueOf(r9)
            r8.put(r7, r9)
            java.util.HashMap r8 = com.mobclix.android.sdk.bw.wB
            r9 = 1
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)
            r8.put(r7, r9)
            goto L_0x0257
        L_0x0315:
            java.util.HashMap r8 = com.mobclix.android.sdk.bw.wC
            java.lang.String r9 = ""
            r8.put(r7, r9)
            goto L_0x0296
        L_0x031e:
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r5 = r0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "attempt_"
            r6.<init>(r7)
            java.lang.StringBuilder r6 = r6.append(r3)
            java.lang.String r6 = r6.toString()
            java.lang.String r2 = r5.i(r2, r6)
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r5 = r0
            java.lang.String r6 = "build_request"
            java.lang.String r2 = r5.i(r2, r6)
            r5 = 1
            if (r3 != r5) goto L_0x05c0
            r5 = 1
        L_0x0345:
            r0 = r18
            r1 = r5
            java.lang.String r5 = r0.l(r1)
            r0 = r5
            r1 = r18
            r1.as = r0
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r5 = r0
            java.lang.String r2 = r5.af(r2)
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r5 = r0
            java.lang.String r6 = "send_request"
            java.lang.String r2 = r5.i(r2, r6)
            java.lang.String r5 = ""
            r6 = 0
            com.mobclix.android.sdk.as r7 = new com.mobclix.android.sdk.as     // Catch:{ Exception -> 0x0804, all -> 0x0800 }
            r0 = r18
            java.lang.String r0 = r0.as     // Catch:{ Exception -> 0x0804, all -> 0x0800 }
            r8 = r0
            r7.<init>(r8)     // Catch:{ Exception -> 0x0804, all -> 0x0800 }
            org.apache.http.HttpResponse r7 = r7.cm()     // Catch:{ Exception -> 0x0804, all -> 0x0800 }
            org.apache.http.HttpEntity r8 = r7.getEntity()     // Catch:{ Exception -> 0x0804, all -> 0x0800 }
            org.apache.http.StatusLine r7 = r7.getStatusLine()     // Catch:{ Exception -> 0x0804, all -> 0x0800 }
            int r7 = r7.getStatusCode()     // Catch:{ Exception -> 0x0804, all -> 0x0800 }
            r9 = 200(0xc8, float:2.8E-43)
            if (r7 != r9) goto L_0x07b4
            java.io.BufferedReader r7 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0804, all -> 0x0800 }
            java.io.InputStreamReader r9 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0804, all -> 0x0800 }
            java.io.InputStream r10 = r8.getContent()     // Catch:{ Exception -> 0x0804, all -> 0x0800 }
            r9.<init>(r10)     // Catch:{ Exception -> 0x0804, all -> 0x0800 }
            r10 = 8000(0x1f40, float:1.121E-41)
            r7.<init>(r9, r10)     // Catch:{ Exception -> 0x0804, all -> 0x0800 }
            java.lang.String r6 = r7.readLine()     // Catch:{ Exception -> 0x0808, all -> 0x071a }
            r17 = r6
            r6 = r5
            r5 = r17
        L_0x039f:
            if (r5 != 0) goto L_0x05c3
            r8.consumeContent()     // Catch:{ Exception -> 0x0808, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz     // Catch:{ Exception -> 0x0808, all -> 0x071a }
            r5 = r0
            java.lang.String r2 = r5.af(r2)     // Catch:{ Exception -> 0x0808, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz     // Catch:{ Exception -> 0x0808, all -> 0x071a }
            r5 = r0
            java.lang.String r8 = "handle_response"
            java.lang.String r2 = r5.i(r2, r8)     // Catch:{ Exception -> 0x0808, all -> 0x071a }
            java.lang.String r5 = ""
            boolean r5 = r6.equals(r5)     // Catch:{ Exception -> 0x0808, all -> 0x071a }
            if (r5 != 0) goto L_0x0821
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r5 = r0
            java.lang.String r8 = "decode_json"
            java.lang.String r2 = r5.i(r2, r8)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            b.a.b r5 = new b.a.b     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8 = r0
            java.lang.String r2 = r8.af(r2)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8 = r0
            java.lang.String r9 = "save_json"
            java.lang.String r2 = r8.i(r2, r9)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8 = r0
            java.lang.String r9 = "raw_config_json"
            java.lang.String r10 = com.mobclix.android.sdk.aq.mC     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8.a(r6, r9, r10)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r6 = r0
            java.lang.String r8 = "decoded_config_json"
            java.lang.String r9 = com.mobclix.android.sdk.aq.mC     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r6.a(r5, r8, r9)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r6 = r0
            java.lang.String r2 = r6.af(r2)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r6 = r0
            java.lang.String r8 = "load_config"
            java.lang.String r2 = r6.i(r2, r8)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            java.lang.String r6 = "urls"
            b.a.b r6 = r5.F(r6)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8 = r0
            java.lang.String r9 = "config"
            java.lang.String r9 = r6.getString(r9)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8.wF = r9     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8 = r0
            java.lang.String r9 = "ads"
            java.lang.String r9 = r6.getString(r9)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8.wE = r9     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8 = r0
            java.lang.String r9 = "analytics"
            java.lang.String r9 = r6.getString(r9)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8.wG = r9     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8 = r0
            java.lang.String r9 = "vc"
            java.lang.String r9 = r6.getString(r9)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8.wH = r9     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8 = r0
            java.lang.String r9 = "feedback"
            java.lang.String r9 = r6.getString(r9)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8.wI = r9     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8 = r0
            java.lang.String r9 = "debug"
            java.lang.String r6 = r6.getString(r9)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8.wJ = r6     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r6 = r0
            java.lang.String r8 = "idle_timeout"
            int r8 = r5.getInt(r8)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            int r8 = r8 * 1000
            r6.wM = r8     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x081e, all -> 0x071a }
            r6 = r0
            java.lang.String r8 = "poll_time"
            int r8 = r5.getInt(r8)     // Catch:{ Exception -> 0x081e, all -> 0x071a }
            int r8 = r8 * 1000
            r6.wL = r8     // Catch:{ Exception -> 0x081e, all -> 0x071a }
        L_0x0482:
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r6 = r0
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r8 = r0
            int r8 = r8.wL     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r9 = r0
            int r9 = r9.wM     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            int r8 = java.lang.Math.min(r8, r9)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r6.wL = r8     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            r6 = r0
            java.lang.String r8 = "set_default_values"
            java.lang.String r6 = r6.i(r2, r8)     // Catch:{ Exception -> 0x0813, all -> 0x071a }
            java.util.HashMap r8 = new java.util.HashMap     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r8.<init>()     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = "ConfigServer"
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r9 = r0
            java.lang.String r9 = r9.wF     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r8.put(r2, r9)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = "AdServer"
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r9 = r0
            java.lang.String r9 = r9.wE     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r8.put(r2, r9)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = "AnalyticsServer"
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r9 = r0
            java.lang.String r9 = r9.wG     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r8.put(r2, r9)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = "VcServer"
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r9 = r0
            java.lang.String r9 = r9.wH     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r8.put(r2, r9)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = "FeedbackServer"
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r9 = r0
            java.lang.String r9 = r9.wI     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r8.put(r2, r9)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = "idleTimeout"
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r9 = r0
            int r9 = r9.wM     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r9 = java.lang.Integer.toString(r9)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r8.put(r2, r9)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = "pollTime"
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r9 = r0
            int r9 = r9.wL     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r9 = java.lang.Integer.toString(r9)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r8.put(r2, r9)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = "ad_units"
            b.a.a r9 = r5.E(r2)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r2 = 0
            r10 = r2
        L_0x050f:
            int r2 = r9.length()     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            if (r10 < r2) goto L_0x05df
            java.lang.String r2 = "debug_config"
            b.a.b r9 = r5.F(r2)     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            java.lang.String[] r2 = com.mobclix.android.sdk.aq.mE     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            int r10 = r2.length     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            r11 = 0
        L_0x051f:
            if (r11 < r10) goto L_0x0745
            java.util.Iterator r10 = r9.bA()     // Catch:{ Exception -> 0x079b, all -> 0x071a }
        L_0x0525:
            boolean r2 = r10.hasNext()     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            if (r2 != 0) goto L_0x075d
        L_0x052b:
            java.lang.String r2 = "app_alerts"
            b.a.a r2 = r5.E(r2)     // Catch:{ Exception -> 0x079e, all -> 0x071a }
        L_0x0531:
            java.lang.String r4 = "native_urls"
            b.a.a r4 = r5.E(r4)     // Catch:{ Exception -> 0x081b, all -> 0x071a }
            r5 = 0
        L_0x0538:
            int r9 = r4.length()     // Catch:{ Exception -> 0x081b, all -> 0x071a }
            if (r5 < r9) goto L_0x07a2
        L_0x053e:
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x080c, all -> 0x071a }
            r4 = r0
            java.lang.String r4 = r4.xt     // Catch:{ Exception -> 0x080c, all -> 0x071a }
            if (r4 == 0) goto L_0x0553
            java.lang.String r4 = "deviceId"
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x080c, all -> 0x071a }
            r5 = r0
            java.lang.String r5 = r5.wY     // Catch:{ Exception -> 0x080c, all -> 0x071a }
            com.mobclix.android.sdk.bw.k(r4, r5)     // Catch:{ Exception -> 0x080c, all -> 0x071a }
        L_0x0553:
            java.lang.String r4 = "offlineSessions"
            java.lang.String r5 = "0"
            r8.put(r4, r5)     // Catch:{ Exception -> 0x080c, all -> 0x071a }
            java.lang.String r4 = "totalSessionTime"
            java.lang.String r5 = "0"
            r8.put(r4, r5)     // Catch:{ Exception -> 0x080c, all -> 0x071a }
            java.lang.String r4 = "totalIdleTime"
            java.lang.String r5 = "0"
            r8.put(r4, r5)     // Catch:{ Exception -> 0x080c, all -> 0x071a }
            com.mobclix.android.sdk.bw.c(r8)     // Catch:{ Exception -> 0x080c, all -> 0x071a }
            java.lang.String r4 = "MCReferralData"
            boolean r4 = com.mobclix.android.sdk.bw.aJ(r4)     // Catch:{ Exception -> 0x080c, all -> 0x071a }
            if (r4 == 0) goto L_0x0578
            java.lang.String r4 = "MCReferralData"
            com.mobclix.android.sdk.bw.aK(r4)     // Catch:{ Exception -> 0x080c, all -> 0x071a }
        L_0x0578:
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz     // Catch:{ Exception -> 0x080c, all -> 0x071a }
            r4 = r0
            java.lang.String r4 = r4.af(r6)     // Catch:{ Exception -> 0x080c, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz     // Catch:{ Exception -> 0x0810, all -> 0x071a }
            r5 = r0
            java.lang.String r4 = r5.af(r4)     // Catch:{ Exception -> 0x0810, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x0810, all -> 0x071a }
            r5 = r0
            r6 = 0
            r5.wO = r6     // Catch:{ Exception -> 0x0810, all -> 0x071a }
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x0810, all -> 0x071a }
            r5 = r0
            r6 = 1
            r5.wN = r6     // Catch:{ Exception -> 0x0810, all -> 0x071a }
            r5 = r4
            r4 = r2
            r2 = r7
        L_0x059d:
            r2.close()     // Catch:{ Exception -> 0x07eb }
            r2 = r4
            r4 = r5
        L_0x05a2:
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r5 = r0
            java.lang.String r4 = r5.af(r4)
            r0 = r18
            com.mobclix.android.sdk.aq r0 = r0.mz
            r5 = r0
            java.lang.String r4 = r5.af(r4)
            r5 = 1
            if (r3 > r5) goto L_0x0825
            int r3 = r3 + 1
            r17 = r2
            r2 = r4
            r4 = r17
            goto L_0x00f3
        L_0x05c0:
            r5 = 0
            goto L_0x0345
        L_0x05c3:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0808, all -> 0x071a }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x0808, all -> 0x071a }
            r9.<init>(r6)     // Catch:{ Exception -> 0x0808, all -> 0x071a }
            java.lang.StringBuilder r5 = r9.append(r5)     // Catch:{ Exception -> 0x0808, all -> 0x071a }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0808, all -> 0x071a }
            java.lang.String r6 = r7.readLine()     // Catch:{ Exception -> 0x0808, all -> 0x071a }
            r17 = r6
            r6 = r5
            r5 = r17
            goto L_0x039f
        L_0x05df:
            b.a.b r11 = r9.m(r10)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = "size"
            java.lang.String r12 = r11.getString(r2)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.util.HashMap r2 = com.mobclix.android.sdk.bw.wx     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r13 = "enabled"
            boolean r13 = r11.getBoolean(r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.Boolean r13 = java.lang.Boolean.valueOf(r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r2.put(r12, r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = "refresh"
            long r13 = r11.getLong(r2)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r15 = -1
            int r2 = (r13 > r15 ? 1 : (r13 == r15 ? 0 : -1))
            if (r2 != 0) goto L_0x06e2
            java.util.HashMap r2 = com.mobclix.android.sdk.bw.wy     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r13 = -1
            java.lang.Long r13 = java.lang.Long.valueOf(r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r2.put(r12, r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
        L_0x060f:
            java.util.HashMap r2 = com.mobclix.android.sdk.bw.wz     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r13 = "autoplay"
            boolean r13 = r11.getBoolean(r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.Boolean r13 = java.lang.Boolean.valueOf(r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r2.put(r12, r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = "autoplay_interval"
            long r13 = r11.getLong(r2)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r15 = -1
            int r2 = (r13 > r15 ? 1 : (r13 == r15 ? 0 : -1))
            if (r2 != 0) goto L_0x0706
            java.util.HashMap r2 = com.mobclix.android.sdk.bw.wA     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r13 = -1
            java.lang.Long r13 = java.lang.Long.valueOf(r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r2.put(r12, r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
        L_0x0635:
            java.util.HashMap r2 = com.mobclix.android.sdk.bw.wB     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r13 = "autoplay"
            boolean r13 = r11.getBoolean(r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.Boolean r13 = java.lang.Boolean.valueOf(r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r2.put(r12, r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.util.HashMap r2 = com.mobclix.android.sdk.bw.wx     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.Object r2 = r2.get(r12)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.Boolean r2 = (java.lang.Boolean) r2     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            boolean r2 = r2.booleanValue()     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = java.lang.Boolean.toString(r2)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r13.<init>(r2)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = ","
            java.lang.StringBuilder r13 = r13.append(r2)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.util.HashMap r2 = com.mobclix.android.sdk.bw.wy     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.Object r2 = r2.get(r12)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.StringBuilder r2 = r13.append(r2)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r13 = ","
            java.lang.StringBuilder r13 = r2.append(r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.util.HashMap r2 = com.mobclix.android.sdk.bw.wz     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.Object r2 = r2.get(r12)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.Boolean r2 = (java.lang.Boolean) r2     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            boolean r2 = r2.booleanValue()     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = java.lang.Boolean.toString(r2)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.StringBuilder r2 = r13.append(r2)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r13 = ","
            java.lang.StringBuilder r13 = r2.append(r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.util.HashMap r2 = com.mobclix.android.sdk.bw.wA     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.Object r2 = r2.get(r12)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.StringBuilder r2 = r13.append(r2)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r13 = ","
            java.lang.StringBuilder r13 = r2.append(r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.util.HashMap r2 = com.mobclix.android.sdk.bw.wB     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.Object r2 = r2.get(r12)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.Boolean r2 = (java.lang.Boolean) r2     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            boolean r2 = r2.booleanValue()     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = java.lang.Boolean.toString(r2)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.StringBuilder r2 = r13.append(r2)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r8.put(r12, r2)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r2 = "customAdUrl"
            java.lang.String r2 = r11.getString(r2)     // Catch:{ Exception -> 0x073c, all -> 0x071a }
            java.util.HashMap r11 = com.mobclix.android.sdk.bw.wC     // Catch:{ Exception -> 0x073c, all -> 0x071a }
            java.lang.Object r11 = r11.get(r12)     // Catch:{ Exception -> 0x073c, all -> 0x071a }
            boolean r11 = r2.equals(r11)     // Catch:{ Exception -> 0x073c, all -> 0x071a }
            if (r11 == 0) goto L_0x0720
            java.util.HashMap r2 = com.mobclix.android.sdk.bw.wC     // Catch:{ Exception -> 0x073c, all -> 0x071a }
            java.lang.String r11 = ""
            r2.put(r12, r11)     // Catch:{ Exception -> 0x073c, all -> 0x071a }
        L_0x06dd:
            int r2 = r10 + 1
            r10 = r2
            goto L_0x050f
        L_0x06e2:
            java.util.HashMap r2 = com.mobclix.android.sdk.bw.wy     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r13 = "refresh"
            long r13 = r11.getLong(r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r15 = 1000(0x3e8, double:4.94E-321)
            long r13 = r13 * r15
            java.lang.Long r13 = java.lang.Long.valueOf(r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r2.put(r12, r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            goto L_0x060f
        L_0x06f6:
            r2 = move-exception
            r2 = r4
            r4 = r6
        L_0x06f9:
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x07c0, all -> 0x071a }
            r5 = r0
            r6 = -1
            r5.wN = r6     // Catch:{ Exception -> 0x07c0, all -> 0x071a }
            r5 = r4
            r4 = r2
            r2 = r7
            goto L_0x059d
        L_0x0706:
            java.util.HashMap r2 = com.mobclix.android.sdk.bw.wA     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r13 = "autoplay_interval"
            long r13 = r11.getLong(r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r15 = 1000(0x3e8, double:4.94E-321)
            long r13 = r13 * r15
            java.lang.Long r13 = java.lang.Long.valueOf(r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            r2.put(r12, r13)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            goto L_0x0635
        L_0x071a:
            r2 = move-exception
            r3 = r7
        L_0x071c:
            r3.close()     // Catch:{ Exception -> 0x07e0 }
        L_0x071f:
            throw r2
        L_0x0720:
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x073c, all -> 0x071a }
            java.lang.String r13 = java.lang.String.valueOf(r12)     // Catch:{ Exception -> 0x073c, all -> 0x071a }
            r11.<init>(r13)     // Catch:{ Exception -> 0x073c, all -> 0x071a }
            java.lang.String r13 = "CustomAdUrl"
            java.lang.StringBuilder r11 = r11.append(r13)     // Catch:{ Exception -> 0x073c, all -> 0x071a }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x073c, all -> 0x071a }
            com.mobclix.android.sdk.bw.aK(r11)     // Catch:{ Exception -> 0x073c, all -> 0x071a }
            java.util.HashMap r11 = com.mobclix.android.sdk.bw.wC     // Catch:{ Exception -> 0x073c, all -> 0x071a }
            r11.put(r12, r2)     // Catch:{ Exception -> 0x073c, all -> 0x071a }
            goto L_0x06dd
        L_0x073c:
            r2 = move-exception
            java.util.HashMap r2 = com.mobclix.android.sdk.bw.wC     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            java.lang.String r11 = ""
            r2.put(r12, r11)     // Catch:{ Exception -> 0x06f6, all -> 0x071a }
            goto L_0x06dd
        L_0x0745:
            r12 = r2[r11]     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            java.lang.String r14 = "debug_"
            r13.<init>(r14)     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            java.lang.StringBuilder r12 = r13.append(r12)     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            com.mobclix.android.sdk.bw.aK(r12)     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            int r11 = r11 + 1
            goto L_0x051f
        L_0x075d:
            java.lang.Object r2 = r10.next()     // Catch:{ Exception -> 0x0780, all -> 0x071a }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0780, all -> 0x071a }
            java.lang.String r11 = r9.getString(r2)     // Catch:{ Exception -> 0x0780, all -> 0x071a }
            java.util.HashMap r12 = com.mobclix.android.sdk.bw.ww     // Catch:{ Exception -> 0x0780, all -> 0x071a }
            r12.put(r2, r11)     // Catch:{ Exception -> 0x0780, all -> 0x071a }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0780, all -> 0x071a }
            java.lang.String r13 = "debug_"
            r12.<init>(r13)     // Catch:{ Exception -> 0x0780, all -> 0x071a }
            java.lang.StringBuilder r2 = r12.append(r2)     // Catch:{ Exception -> 0x0780, all -> 0x071a }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0780, all -> 0x071a }
            com.mobclix.android.sdk.bw.k(r2, r11)     // Catch:{ Exception -> 0x0780, all -> 0x071a }
            goto L_0x0525
        L_0x0780:
            r2 = move-exception
            java.lang.String r11 = com.mobclix.android.sdk.ap.TAG     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            java.lang.String r13 = "ERROR: "
            r12.<init>(r13)     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            java.lang.StringBuilder r2 = r12.append(r2)     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            android.util.Log.v(r11, r2)     // Catch:{ Exception -> 0x079b, all -> 0x071a }
            goto L_0x0525
        L_0x079b:
            r2 = move-exception
            goto L_0x052b
        L_0x079e:
            r2 = move-exception
            r2 = r4
            goto L_0x0531
        L_0x07a2:
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x081b, all -> 0x071a }
            r9 = r0
            java.util.List r9 = r9.wK     // Catch:{ Exception -> 0x081b, all -> 0x071a }
            java.lang.String r10 = r4.getString(r5)     // Catch:{ Exception -> 0x081b, all -> 0x071a }
            r9.add(r10)     // Catch:{ Exception -> 0x081b, all -> 0x071a }
            int r5 = r5 + 1
            goto L_0x0538
        L_0x07b4:
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x0804, all -> 0x0800 }
            r5 = r0
            r7 = -1
            r5.wN = r7     // Catch:{ Exception -> 0x0804, all -> 0x0800 }
            r5 = r2
            r2 = r6
            goto L_0x059d
        L_0x07c0:
            r5 = move-exception
            r5 = r4
            r4 = r2
            r2 = r7
        L_0x07c4:
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ all -> 0x07f8 }
            r6 = r0
            r7 = -1
            r6.wN = r7     // Catch:{ all -> 0x07f8 }
            r2.close()     // Catch:{ Exception -> 0x07d3 }
            r2 = r4
            r4 = r5
            goto L_0x05a2
        L_0x07d3:
            r2 = move-exception
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB
            r2 = r0
            r6 = -1
            r2.wN = r6
            r2 = r4
            r4 = r5
            goto L_0x05a2
        L_0x07e0:
            r3 = move-exception
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB
            r3 = r0
            r4 = -1
            r3.wN = r4
            goto L_0x071f
        L_0x07eb:
            r2 = move-exception
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB
            r2 = r0
            r6 = -1
            r2.wN = r6
            r2 = r4
            r4 = r5
            goto L_0x05a2
        L_0x07f8:
            r3 = move-exception
            r17 = r3
            r3 = r2
            r2 = r17
            goto L_0x071c
        L_0x0800:
            r2 = move-exception
            r3 = r6
            goto L_0x071c
        L_0x0804:
            r5 = move-exception
            r5 = r2
            r2 = r6
            goto L_0x07c4
        L_0x0808:
            r5 = move-exception
            r5 = r2
            r2 = r7
            goto L_0x07c4
        L_0x080c:
            r4 = move-exception
            r4 = r6
            goto L_0x06f9
        L_0x0810:
            r5 = move-exception
            goto L_0x06f9
        L_0x0813:
            r5 = move-exception
            r17 = r4
            r4 = r2
            r2 = r17
            goto L_0x06f9
        L_0x081b:
            r4 = move-exception
            goto L_0x053e
        L_0x081e:
            r6 = move-exception
            goto L_0x0482
        L_0x0821:
            r5 = r2
            r2 = r7
            goto L_0x059d
        L_0x0825:
            r3 = r4
            goto L_0x00ff
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.ap.ck():b.a.a");
    }

    private String l(boolean z) {
        String str = this.mB.wF;
        StringBuffer stringBuffer = new StringBuffer();
        if (bw.aJ("ConfigServer") && z) {
            str = bw.aI("ConfigServer");
        }
        stringBuffer.append(str);
        stringBuffer.append("?p=android");
        stringBuffer.append("&a=").append(URLEncoder.encode(this.mB.eO(), "UTF-8"));
        stringBuffer.append("&m=").append(URLEncoder.encode(bw.fc()));
        stringBuffer.append("&v=").append(URLEncoder.encode(this.mB.eR(), "UTF-8"));
        stringBuffer.append("&d=").append(URLEncoder.encode(this.mB.getDeviceId(), "UTF-8"));
        stringBuffer.append("&dm=").append(URLEncoder.encode(this.mB.eT(), "UTF-8"));
        stringBuffer.append("&dv=").append(URLEncoder.encode(this.mB.eQ(), "UTF-8"));
        stringBuffer.append("&hwdm=").append(URLEncoder.encode(this.mB.eU(), "UTF-8"));
        stringBuffer.append("&g=").append(URLEncoder.encode(this.mB.eV(), "UTF-8"));
        if (!this.mB.eY().equals("null")) {
            stringBuffer.append("&ll=").append(URLEncoder.encode(this.mB.eY(), "UTF-8"));
        }
        if (bw.aJ("offlineSessions")) {
            try {
                stringBuffer.append("&off=").append(bw.aI("offlineSessions"));
            } catch (Exception e) {
            }
        }
        if (bw.aJ("totalSessionTime")) {
            try {
                stringBuffer.append("&st=").append(bw.aI("totalSessionTime"));
            } catch (Exception e2) {
            }
        }
        if (bw.aJ("totalIdleTime")) {
            try {
                stringBuffer.append("&it=").append(bw.aI("totalIdleTime"));
            } catch (Exception e3) {
            }
        }
        try {
            if (this.mB.xt != null) {
                stringBuffer.append("&pd=").append(URLEncoder.encode(this.mB.xt, "UTF-8"));
            }
            stringBuffer.append("&mcc=").append(URLEncoder.encode(this.mB.fa(), "UTF-8"));
            stringBuffer.append("&mnc=").append(URLEncoder.encode(this.mB.fb(), "UTF-8"));
            if (this.mB.xu) {
                stringBuffer.append("&new=true");
            }
            try {
                if (bw.aJ("MCReferralData")) {
                    String aI = bw.aI("MCReferralData");
                    if (!aI.equals("")) {
                        stringBuffer.append("&r=").append(aI);
                    }
                }
            } catch (Exception e4) {
            }
            return stringBuffer.toString();
        } catch (Exception e5) {
            return "";
        }
    }

    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        return ck();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0119, code lost:
        if (r4.equals("null") == false) goto L_0x011c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x01a0 A[SYNTHETIC, Splitter:B:103:0x01a0] */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x0023 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00ca  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* bridge */ /* synthetic */ void onPostExecute(java.lang.Object r19) {
        /*
            r18 = this;
            b.a.a r19 = (b.a.a) r19
            if (r19 == 0) goto L_0x000b
            r3 = 0
        L_0x0005:
            int r4 = r19.length()
            if (r3 < r4) goto L_0x000c
        L_0x000b:
            return
        L_0x000c:
            r0 = r19
            r1 = r3
            b.a.b r4 = r0.m(r1)     // Catch:{ Exception -> 0x0195 }
            java.lang.String r5 = "id"
            java.lang.String r5 = r4.getString(r5)     // Catch:{ Exception -> 0x0195 }
            if (r5 == 0) goto L_0x0023
            java.lang.String r6 = ""
            boolean r6 = r5.equals(r6)     // Catch:{ Exception -> 0x0195 }
            if (r6 == 0) goto L_0x0026
        L_0x0023:
            int r3 = r3 + 1
            goto L_0x0005
        L_0x0026:
            r6 = 0
            r7 = 0
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0195 }
            java.lang.String r10 = "MCAppAlert"
            r9.<init>(r10)     // Catch:{ Exception -> 0x0195 }
            java.lang.StringBuilder r9 = r9.append(r5)     // Catch:{ Exception -> 0x0195 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0195 }
            java.lang.String r9 = com.mobclix.android.sdk.bw.aI(r9)     // Catch:{ Exception -> 0x0195 }
            java.lang.String r10 = ""
            boolean r10 = r9.equals(r10)     // Catch:{ Exception -> 0x0195 }
            if (r10 != 0) goto L_0x0199
            java.lang.String r10 = ","
            java.lang.String[] r9 = r9.split(r10)     // Catch:{ Exception -> 0x0195 }
            r10 = 0
            r10 = r9[r10]     // Catch:{ Exception -> 0x01e2 }
            int r6 = java.lang.Integer.parseInt(r10)     // Catch:{ Exception -> 0x01e2 }
        L_0x0051:
            r10 = 1
            r9 = r9[r10]     // Catch:{ Exception -> 0x0198 }
            long r7 = java.lang.Long.parseLong(r9)     // Catch:{ Exception -> 0x0198 }
            r16 = r7
            r8 = r6
            r6 = r16
        L_0x005d:
            java.lang.String r9 = "title"
            java.lang.String r9 = r4.getString(r9)     // Catch:{ Exception -> 0x0195 }
            if (r9 == 0) goto L_0x0023
            java.lang.String r10 = ""
            boolean r10 = r9.equals(r10)     // Catch:{ Exception -> 0x0195 }
            if (r10 != 0) goto L_0x0023
            java.lang.String r10 = "null"
            boolean r10 = r9.equals(r10)     // Catch:{ Exception -> 0x0195 }
            if (r10 != 0) goto L_0x0023
            r10 = 0
            java.lang.String r11 = "message"
            java.lang.String r10 = r4.getString(r11)     // Catch:{ Exception -> 0x01df }
            if (r10 == 0) goto L_0x008e
            java.lang.String r11 = ""
            boolean r11 = r10.equals(r11)     // Catch:{ Exception -> 0x01df }
            if (r11 != 0) goto L_0x008e
            java.lang.String r11 = "null"
            boolean r11 = r10.equals(r11)     // Catch:{ Exception -> 0x01df }
            if (r11 == 0) goto L_0x008f
        L_0x008e:
            r10 = 0
        L_0x008f:
            r11 = 0
            java.lang.String r12 = "max_displays"
            int r11 = r4.getInt(r12)     // Catch:{ Exception -> 0x01dc }
        L_0x0096:
            if (r11 == 0) goto L_0x009a
            if (r8 >= r11) goto L_0x0023
        L_0x009a:
            r11 = 0
            java.lang.String r13 = "display_interval"
            int r11 = r4.getInt(r13)     // Catch:{ Exception -> 0x01d9 }
            long r11 = (long) r11
            r13 = 1000(0x3e8, double:4.94E-321)
            long r11 = r11 * r13
        L_0x00a6:
            r13 = 0
            int r13 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r13 == 0) goto L_0x00b5
            long r6 = r6 + r11
            long r11 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0195 }
            int r6 = (r6 > r11 ? 1 : (r6 == r11 ? 0 : -1))
            if (r6 >= 0) goto L_0x0023
        L_0x00b5:
            java.lang.String r6 = "target_versions"
            b.a.a r6 = r4.E(r6)     // Catch:{ Exception -> 0x0195 }
            r7 = 0
            r11 = 0
            r16 = r11
            r11 = r7
            r7 = r16
        L_0x00c2:
            int r12 = r6.length()     // Catch:{ Exception -> 0x0195 }
            if (r7 < r12) goto L_0x01a0
            if (r11 == 0) goto L_0x0023
            r6 = 0
            java.lang.String r7 = "action_button"
            java.lang.String r6 = r4.getString(r7)     // Catch:{ Exception -> 0x01d4 }
            if (r6 == 0) goto L_0x00e3
            java.lang.String r7 = ""
            boolean r7 = r6.equals(r7)     // Catch:{ Exception -> 0x01d4 }
            if (r7 != 0) goto L_0x00e3
            java.lang.String r7 = "null"
            boolean r7 = r6.equals(r7)     // Catch:{ Exception -> 0x01d4 }
            if (r7 == 0) goto L_0x00e4
        L_0x00e3:
            r6 = 0
        L_0x00e4:
            r7 = 0
            java.lang.String r11 = "action_url"
            java.lang.String r7 = r4.getString(r11)     // Catch:{ Exception -> 0x01d1 }
            if (r7 == 0) goto L_0x00fd
            java.lang.String r11 = ""
            boolean r11 = r7.equals(r11)     // Catch:{ Exception -> 0x01d1 }
            if (r11 != 0) goto L_0x00fd
            java.lang.String r11 = "null"
            boolean r11 = r7.equals(r11)     // Catch:{ Exception -> 0x01d1 }
            if (r11 == 0) goto L_0x00fe
        L_0x00fd:
            r7 = 0
        L_0x00fe:
            if (r7 == 0) goto L_0x0102
            if (r6 == 0) goto L_0x0023
        L_0x0102:
            r11 = 0
            java.lang.String r12 = "dismiss_button"
            java.lang.String r4 = r4.getString(r12)     // Catch:{ Exception -> 0x01ca }
            if (r4 == 0) goto L_0x011b
            java.lang.String r11 = ""
            boolean r11 = r4.equals(r11)     // Catch:{ Exception -> 0x01ce }
            if (r11 != 0) goto L_0x011b
            java.lang.String r11 = "null"
            boolean r11 = r4.equals(r11)     // Catch:{ Exception -> 0x01ce }
            if (r11 == 0) goto L_0x011c
        L_0x011b:
            r4 = 0
        L_0x011c:
            if (r7 != 0) goto L_0x0120
            if (r4 == 0) goto L_0x0023
        L_0x0120:
            android.app.AlertDialog$Builder r11 = new android.app.AlertDialog$Builder     // Catch:{ Exception -> 0x0195 }
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x0195 }
            r12 = r0
            android.content.Context r12 = r12.getContext()     // Catch:{ Exception -> 0x0195 }
            r11.<init>(r12)     // Catch:{ Exception -> 0x0195 }
            r11.setTitle(r9)     // Catch:{ Exception -> 0x0195 }
            r9 = 0
            r11.setCancelable(r9)     // Catch:{ Exception -> 0x0195 }
            if (r10 == 0) goto L_0x013a
            r11.setMessage(r10)     // Catch:{ Exception -> 0x0195 }
        L_0x013a:
            if (r7 == 0) goto L_0x0148
            com.mobclix.android.sdk.s r9 = new com.mobclix.android.sdk.s     // Catch:{ Exception -> 0x0195 }
            r0 = r9
            r1 = r18
            r2 = r7
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0195 }
            r11.setPositiveButton(r6, r9)     // Catch:{ Exception -> 0x0195 }
        L_0x0148:
            if (r4 == 0) goto L_0x0155
            com.mobclix.android.sdk.t r6 = new com.mobclix.android.sdk.t     // Catch:{ Exception -> 0x0195 }
            r0 = r6
            r1 = r18
            r0.<init>(r1)     // Catch:{ Exception -> 0x0195 }
            r11.setNegativeButton(r4, r6)     // Catch:{ Exception -> 0x0195 }
        L_0x0155:
            android.app.AlertDialog r4 = r11.create()     // Catch:{ Exception -> 0x0195 }
            r4.show()     // Catch:{ Exception -> 0x0195 }
            int r4 = r8 + 1
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0195 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0195 }
            java.lang.String r9 = "MCAppAlert"
            r8.<init>(r9)     // Catch:{ Exception -> 0x0195 }
            java.lang.StringBuilder r5 = r8.append(r5)     // Catch:{ Exception -> 0x0195 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0195 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0195 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x0195 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x0195 }
            r8.<init>(r4)     // Catch:{ Exception -> 0x0195 }
            java.lang.String r4 = ","
            java.lang.StringBuilder r4 = r8.append(r4)     // Catch:{ Exception -> 0x0195 }
            java.lang.String r6 = java.lang.Long.toString(r6)     // Catch:{ Exception -> 0x0195 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x0195 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0195 }
            com.mobclix.android.sdk.bw.k(r5, r4)     // Catch:{ Exception -> 0x0195 }
            goto L_0x0023
        L_0x0195:
            r4 = move-exception
            goto L_0x0023
        L_0x0198:
            r9 = move-exception
        L_0x0199:
            r16 = r7
            r8 = r6
            r6 = r16
            goto L_0x005d
        L_0x01a0:
            java.lang.String r12 = r6.getString(r7)     // Catch:{ Exception -> 0x01d7 }
            java.lang.String r13 = "\\*"
            java.lang.String[] r12 = r12.split(r13)     // Catch:{ Exception -> 0x01d7 }
            r13 = 0
            r12 = r12[r13]     // Catch:{ Exception -> 0x01d7 }
            r0 = r18
            com.mobclix.android.sdk.bw r0 = r0.mB     // Catch:{ Exception -> 0x01d7 }
            r13 = r0
            java.lang.String r13 = r13.eR()     // Catch:{ Exception -> 0x01d7 }
            r14 = 0
            int r15 = r12.length()     // Catch:{ Exception -> 0x01d7 }
            java.lang.String r13 = r13.substring(r14, r15)     // Catch:{ Exception -> 0x01d7 }
            boolean r12 = r13.equals(r12)     // Catch:{ Exception -> 0x01d7 }
            if (r12 == 0) goto L_0x01c6
            r11 = 1
        L_0x01c6:
            int r7 = r7 + 1
            goto L_0x00c2
        L_0x01ca:
            r4 = move-exception
            r4 = r11
            goto L_0x011c
        L_0x01ce:
            r11 = move-exception
            goto L_0x011c
        L_0x01d1:
            r11 = move-exception
            goto L_0x00fe
        L_0x01d4:
            r7 = move-exception
            goto L_0x00e4
        L_0x01d7:
            r12 = move-exception
            goto L_0x01c6
        L_0x01d9:
            r13 = move-exception
            goto L_0x00a6
        L_0x01dc:
            r12 = move-exception
            goto L_0x0096
        L_0x01df:
            r11 = move-exception
            goto L_0x008f
        L_0x01e2:
            r10 = move-exception
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.ap.onPostExecute(java.lang.Object):void");
    }
}
