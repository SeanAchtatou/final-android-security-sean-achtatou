package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import b.a.a;
import b.a.b;
import java.util.ArrayList;
import java.util.Iterator;

final class y extends RelativeLayout implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    private Activity av;
    final /* synthetic */ MobclixBrowserActivity cC;
    private String hC = "";
    private String hD;
    private String hE = "";
    private String hF = "";
    private ArrayList hG = new ArrayList();
    private ArrayList hH = new ArrayList();
    /* access modifiers changed from: private */
    public ArrayList hI = new ArrayList();
    private ArrayList hJ = new ArrayList();
    private LinearLayout hK = null;
    private LinearLayout hL = null;
    /* access modifiers changed from: private */
    public ProgressDialog hM = null;
    /* access modifiers changed from: private */
    public ImageView hN = null;
    /* access modifiers changed from: private */
    public VideoView hO;
    private MediaController hP;
    /* access modifiers changed from: private */
    public ImageView hQ;
    /* access modifiers changed from: private */
    public boolean hR = false;
    /* access modifiers changed from: private */
    public boolean hS = false;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    y(MobclixBrowserActivity mobclixBrowserActivity, Activity activity, String str) {
        super(activity);
        this.cC = mobclixBrowserActivity;
        this.av = activity;
        try {
            b bVar = new b(str);
            try {
                this.hD = bVar.getString("videoUrl");
            } catch (Exception e) {
            }
            try {
                this.hC = bVar.getString("landingUrl");
            } catch (Exception e2) {
            }
            try {
                this.hE = bVar.getString("tagline");
            } catch (Exception e3) {
            }
            try {
                this.hF = bVar.getString("taglineImageUrl");
            } catch (Exception e4) {
            }
            try {
                a E = bVar.E("buttons");
                for (int i = 0; i < E.length(); i++) {
                    this.hH.add(E.m(i).getString("imageUrl"));
                    this.hJ.add(E.m(i).getString("url"));
                }
            } catch (Exception e5) {
            }
            a E2 = bVar.E("trackingUrls");
            for (int i2 = 0; i2 < E2.length(); i2++) {
                this.hG.add(E2.getString(i2));
            }
        } catch (Exception e6) {
        }
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        bq();
    }

    private void bq() {
        this.cC.getWindow().setFlags(1024, 1024);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        this.hO = new VideoView(this.av);
        this.hO.setId(1337);
        this.hO.setLayoutParams(layoutParams);
        this.hP = new MediaController(this.av);
        this.hP.setAnchorView(this.hO);
        this.hO.setVideoURI(Uri.parse(this.hD));
        this.hO.setMediaController(this.hP);
        this.hO.setOnPreparedListener(this);
        this.hO.setOnErrorListener(this);
        this.hO.setOnCompletionListener(this);
        this.hO.setVisibility(4);
        addView(this.hO);
        this.hQ = new ImageView(this.av);
        this.hQ.setLayoutParams(layoutParams);
        this.hQ.setOnClickListener(new c(this));
        addView(this.hQ);
        if ((!this.hF.equals("null") && !this.hF.equals("")) || !this.hE.equals("")) {
            LinearLayout linearLayout = new LinearLayout(this.av);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams2.addRule(11);
            linearLayout.setLayoutParams(layoutParams2);
            linearLayout.setBackgroundColor(Color.parseColor("#CC666666"));
            linearLayout.setPadding(MobclixBrowserActivity.a(this.cC, 4), MobclixBrowserActivity.a(this.cC, 4), MobclixBrowserActivity.a(this.cC, 4), MobclixBrowserActivity.a(this.cC, 4));
            if (this.hF.equals("null") || this.hF.equals("")) {
                TextView textView = new TextView(this.av);
                textView.setText(this.hE);
                linearLayout.addView(textView);
            } else {
                this.hN = new ImageView(this.av);
                linearLayout.addView(this.hN);
                this.cC.e.add(new Thread(new ax(this.hF, new a(this))));
            }
            addView(linearLayout);
        }
        this.cC.setContentView(this);
        Iterator it = this.hH.iterator();
        while (it.hasNext()) {
            this.cC.e.add(new Thread(new ax((String) it.next(), new b(this))));
        }
        if (!this.hC.equals("")) {
            this.cC.e.add(new Thread(new ax(this.hC, new d(this))));
        }
        Iterator it2 = this.hG.iterator();
        while (it2.hasNext()) {
            this.cC.e.add(new Thread(new ax((String) it2.next(), new e(this))));
        }
    }

    public final void br() {
        if (this.hI.size() != 0) {
            this.hK = new LinearLayout(this.av);
            this.hK.setOrientation(1);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams.addRule(12);
            this.hK.setLayoutParams(layoutParams);
            this.hK.setBackgroundColor(Color.parseColor("#CC666666"));
            ImageView imageView = new ImageView(this.av);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            imageView.setBackgroundResource(17301524);
            this.hK.addView(imageView);
            this.hL = new LinearLayout(this.av);
            this.hL.setPadding(0, MobclixBrowserActivity.a(this.cC, 4), 0, 0);
            for (int i = 0; i < this.hI.size(); i++) {
                ImageView imageView2 = new ImageView(this.av);
                Bitmap bitmap = (Bitmap) this.hI.get(i);
                LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(MobclixBrowserActivity.a(this.cC, bitmap.getWidth()), MobclixBrowserActivity.a(this.cC, bitmap.getHeight()));
                layoutParams2.weight = 1.0f;
                imageView2.setLayoutParams(layoutParams2);
                imageView2.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageView2.setImageBitmap(bitmap);
                imageView2.setOnClickListener(new ci(this, this.av, (String) this.hJ.get(i)));
                this.hL.addView(imageView2);
            }
            this.hK.addView(this.hL);
            addView(this.hK);
        }
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        this.hQ.setVisibility(0);
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        this.hM.dismiss();
        removeView(this.hO);
        return true;
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        this.hS = true;
        this.hQ.setVisibility(8);
        this.hM.dismiss();
    }
}
