package com.mobclix.android.sdk;

import android.os.Handler;
import android.os.Message;
import java.util.Iterator;

final class ae extends Handler {
    private /* synthetic */ ce ab;

    /* synthetic */ ae(ce ceVar) {
        this(ceVar, (byte) 0);
    }

    private ae(ce ceVar, byte b2) {
        this.ab = ceVar;
    }

    public final void handleMessage(Message message) {
        String string = message.getData().getString("type");
        if (string.equals("success")) {
            String i = this.ab.mz.i(this.ab.mz.h(this.ab.zd, aq.mD), "handle_response");
            if (this.ab.zu != null) {
                this.ab.zv = this.ab.zu;
            }
            try {
                String i2 = this.ab.mz.i(i, "a_decode_json");
                this.ab.zw = message.getData().getString("response");
                this.ab.mz.a(this.ab.zw, "raw_json", this.ab.zd);
                this.ab.zx = new bm(this.ab.zw, 0);
                if (this.ab.zx.sp.length() > 0) {
                    this.ab.mz.a(this.ab.zx.dt(), "decoded_json", this.ab.zd);
                    this.ab.zu = new au(this.ab, this.ab.zx.dt(), false);
                    if (!this.ab.zu.isInitialized()) {
                        this.ab.aV("");
                    }
                }
                this.ab.mz.af(i2);
            } catch (Exception e) {
                this.ab.mz.af(this.ab.mz.af(i));
                this.ab.mz.ae(this.ab.zd);
                this.ab.aV("");
            }
        } else if (string.equals("failure")) {
            int i3 = message.getData().getInt("errorCode");
            Iterator it = this.ab.iK.iterator();
            while (it.hasNext()) {
                l lVar = (l) it.next();
                if (lVar != null) {
                    lVar.a(this.ab, i3);
                }
            }
        }
    }
}
