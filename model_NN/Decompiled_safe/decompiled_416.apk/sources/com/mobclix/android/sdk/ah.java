package com.mobclix.android.sdk;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

final class ah extends Animation {
    private /* synthetic */ MobclixBrowserActivity cC;
    private float height;
    private float ls;
    private float lt;
    private View view;
    private float width;

    ah(MobclixBrowserActivity mobclixBrowserActivity, View view2, float f, float f2, float f3, float f4) {
        this.cC = mobclixBrowserActivity;
        this.view = view2;
        this.height = f3;
        this.ls = f4 - this.height;
        this.width = f;
        this.lt = f2 - this.width;
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f, Transformation transformation) {
        super.applyTransformation(f, transformation);
        ViewGroup.LayoutParams layoutParams = this.view.getLayoutParams();
        layoutParams.height = (int) (this.height + (this.ls * f));
        layoutParams.width = (int) (this.width + (this.lt * f));
        this.view.setLayoutParams(layoutParams);
    }
}
