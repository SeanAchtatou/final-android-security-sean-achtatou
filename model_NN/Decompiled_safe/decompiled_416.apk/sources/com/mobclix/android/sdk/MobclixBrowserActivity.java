package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Browser;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TableLayout;
import b.a.b;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.LinkedList;

public class MobclixBrowserActivity extends Activity {
    private String TAG = "mobclix-browser";

    /* renamed from: b  reason: collision with root package name */
    private String f2b = "";
    private float c = 1.0f;
    /* access modifiers changed from: private */
    public o d = new o(this);
    /* access modifiers changed from: private */
    public LinkedList e = new LinkedList();
    private Intent f = null;
    private final int g = 0;
    private final int h = 1;
    private final int i = 2;
    private FrameLayout j;
    ScreenReceiver k;
    private boolean l = true;
    private Uri m;
    private int type;
    /* access modifiers changed from: private */
    public View view;

    public class ScreenReceiver extends BroadcastReceiver {
        private boolean yS = false;

        public ScreenReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
                ((r) MobclixBrowserActivity.this.view).aQ.ck.z();
            } else {
                intent.getAction().equals("android.intent.action.SCREEN_ON");
            }
        }
    }

    static /* synthetic */ int a(MobclixBrowserActivity mobclixBrowserActivity, int i2) {
        return (int) (mobclixBrowserActivity.c * ((float) i2));
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x004c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.io.File a(android.net.Uri r8, android.app.Activity r9) {
        /*
            r6 = 0
            r0 = 3
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ all -> 0x0048 }
            r0 = 0
            java.lang.String r1 = "_data"
            r2[r0] = r1     // Catch:{ all -> 0x0048 }
            r0 = 1
            java.lang.String r1 = "_id"
            r2[r0] = r1     // Catch:{ all -> 0x0048 }
            r0 = 2
            java.lang.String r1 = "orientation"
            r2[r0] = r1     // Catch:{ all -> 0x0048 }
            r3 = 0
            r4 = 0
            r5 = 0
            r0 = r9
            r1 = r8
            android.database.Cursor r0 = r0.managedQuery(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0048 }
            java.lang.String r1 = "_data"
            int r1 = r0.getColumnIndexOrThrow(r1)     // Catch:{ all -> 0x0050 }
            java.lang.String r2 = "orientation"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ all -> 0x0050 }
            boolean r3 = r0.moveToFirst()     // Catch:{ all -> 0x0050 }
            if (r3 == 0) goto L_0x0041
            r0.getString(r2)     // Catch:{ all -> 0x0050 }
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x0050 }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ all -> 0x0050 }
            r2.<init>(r1)     // Catch:{ all -> 0x0050 }
            if (r0 == 0) goto L_0x003f
            r0.close()
        L_0x003f:
            r0 = r2
        L_0x0040:
            return r0
        L_0x0041:
            if (r0 == 0) goto L_0x0046
            r0.close()
        L_0x0046:
            r0 = r6
            goto L_0x0040
        L_0x0048:
            r0 = move-exception
            r1 = r6
        L_0x004a:
            if (r1 == 0) goto L_0x004f
            r1.close()
        L_0x004f:
            throw r0
        L_0x0050:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixBrowserActivity.a(android.net.Uri, android.app.Activity):java.io.File");
    }

    private String a(Uri uri) {
        Cursor managedQuery = managedQuery(uri, new String[]{"_data"}, null, null, null);
        if (managedQuery == null) {
            return null;
        }
        int columnIndexOrThrow = managedQuery.getColumnIndexOrThrow("_data");
        managedQuery.moveToFirst();
        return managedQuery.getString(columnIndexOrThrow);
    }

    public final void a() {
        if (!this.e.isEmpty()) {
            ((Thread) this.e.removeFirst()).start();
            return;
        }
        switch (this.type) {
            case 0:
                ((y) this.view).hR = true;
                ((y) this.view).hM.dismiss();
                ((y) this.view).br();
                ((y) this.view).hO.setVisibility(0);
                ((y) this.view).hO.start();
                return;
            default:
                return;
        }
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        if (bw.fm().xv == null) {
            finish();
        }
        if (i2 == 4) {
            if (i3 == -1) {
                try {
                    Bitmap decodeFile = BitmapFactory.decodeFile(a(this.m, this).getAbsolutePath());
                    int i4 = bw.fm().xv.ck.bj;
                    int i5 = bw.fm().xv.ck.bi;
                    if (!(i4 == 0 || i5 == 0)) {
                        decodeFile = Bitmap.createScaledBitmap(decodeFile, bw.fm().xv.ck.bi, bw.fm().xv.ck.bj, true);
                    }
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    decodeFile.compress(Bitmap.CompressFormat.JPEG, 10, byteArrayOutputStream);
                    bw.fm().xv.ck.g(bo.d(byteArrayOutputStream.toByteArray()));
                    byteArrayOutputStream.close();
                    System.gc();
                } catch (Exception e2) {
                    bw.fm().xv.ck.h(e2.toString());
                    bw.fm().xv.ck.h("Error processing photo.");
                }
            } else if (i3 == 0) {
                bw.fm().xv.ck.h("User canceled.");
            }
        } else if (i2 == 5) {
            if (i3 == -1) {
                try {
                    Uri data = intent.getData();
                    String path = data.getPath();
                    String a2 = a(data);
                    if (a2 == null) {
                        a2 = path;
                    }
                    Uri.parse(a2);
                    File file = new File(a2);
                    Bitmap decodeFile2 = BitmapFactory.decodeFile(file.getAbsolutePath());
                    int i6 = bw.fm().xv.ck.bj;
                    int i7 = bw.fm().xv.ck.bi;
                    if (!(i6 == 0 || i7 == 0)) {
                        decodeFile2 = Bitmap.createScaledBitmap(decodeFile2, bw.fm().xv.ck.bi, bw.fm().xv.ck.bj, true);
                    }
                    ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                    decodeFile2.compress(Bitmap.CompressFormat.JPEG, 10, byteArrayOutputStream2);
                    bw.fm().xv.ck.g(bo.d(byteArrayOutputStream2.toByteArray()));
                    byteArrayOutputStream2.close();
                    System.gc();
                    file.delete();
                } catch (Exception e3) {
                    bw.fm().xv.ck.h(e3.toString());
                    bw.fm().xv.ck.h("Error processing photo.");
                }
            } else if (i3 == 0) {
                bw.fm().xv.ck.h("User canceled.");
            }
        } else if (i2 == 6) {
            if (i3 == -1) {
                try {
                    Uri data2 = intent.getData();
                    String path2 = data2.getPath();
                    String a3 = a(data2);
                    if (a3 == null) {
                        a3 = path2;
                    }
                    bw.fm().xv.ck.j(a3);
                } catch (Exception e4) {
                    bw.fm().xv.ck.h(e4.toString());
                    bw.fm().xv.ck.h("Error processing photo.");
                }
            } else if (i3 == 0) {
                bw.fm().xv.ck.h("User canceled.");
            }
        } else if (i2 == 7) {
            if (i3 == -1) {
                try {
                    k kVar = bw.fm().xv.ck;
                    intent.getData();
                    kVar.v();
                } catch (Exception e5) {
                    bw.fm().xv.ck.i("Error getting contact.");
                }
            } else if (i3 == 0) {
                bw.fm().xv.ck.i("User canceled.");
            }
        } else if (i2 == 8) {
            if (i3 == -1) {
                try {
                    bw.fm().xv.ck.u();
                } catch (Exception e6) {
                    bw.fm().xv.ck.i("Error getting contact.");
                }
            } else if (i3 == 0) {
                bw.fm().xv.ck.i("User canceled.");
            }
        }
        bw.fm().xv = null;
        finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.c = getResources().getDisplayMetrics().density;
        Bundle extras = getIntent().getExtras();
        this.f2b = extras.getString(String.valueOf(getPackageName()) + ".data");
        if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("video")) {
            this.type = 0;
            requestWindowFeature(1);
            setRequestedOrientation(0);
            this.view = new y(this, this, this.f2b);
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("browser")) {
            this.type = 2;
            requestWindowFeature(2);
            this.view = new ak(this, this, this.f2b);
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("expander")) {
            this.type = 3;
            requestWindowFeature(1);
            getWindow().setFlags(1024, 1024);
            this.view = new bq(this, this, this.f2b);
            IntentFilter intentFilter = new IntentFilter("android.intent.action.SCREEN_ON");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            this.k = new ScreenReceiver();
            registerReceiver(this.k, intentFilter);
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("fullscreen")) {
            this.type = 9;
            requestWindowFeature(1);
            getWindow().setFlags(1024, 1024);
            this.view = new bs(this, this);
            IntentFilter intentFilter2 = new IntentFilter("android.intent.action.SCREEN_ON");
            intentFilter2.addAction("android.intent.action.SCREEN_OFF");
            this.k = new ScreenReceiver();
            registerReceiver(this.k, intentFilter2);
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("camera")) {
            this.type = 4;
            ContentValues contentValues = new ContentValues();
            contentValues.put("title", "camera.jpg");
            contentValues.put("description", "Image capture by camera");
            try {
                this.m = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                this.f = new Intent("android.media.action.IMAGE_CAPTURE");
                this.f.putExtra("output", this.m);
                this.f.putExtra("android.intent.extra.videoQuality", 0);
                return;
            } catch (Exception e2) {
                return;
            }
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("gallery")) {
            this.type = 5;
            this.f = new Intent();
            this.f.setType("image/*");
            this.f.setAction("android.intent.action.GET_CONTENT");
            return;
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("sendToServer")) {
            this.type = 6;
            this.f = new Intent();
            this.f.setType("image/*");
            this.f.setAction("android.intent.action.GET_CONTENT");
            return;
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("contact")) {
            this.type = 7;
            return;
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("addContact")) {
            this.type = 8;
            try {
                new b(extras.getString(String.valueOf(getPackageName()) + ".data"));
                this.f = cc.fs().fu();
                return;
            } catch (Exception e3) {
                bw.fm().xv.ck.i("Error getting contact.");
                finish();
                return;
            }
        }
        this.j = new FrameLayout(this);
        this.j.setLayoutParams(new TableLayout.LayoutParams(-1, -1));
        setContentView(this.j);
        this.j.addView(this.view);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        switch (this.type) {
            case 2:
            case 9:
                super.onCreateOptionsMenu(menu);
                menu.add(0, 0, 0, "Bookmark").setIcon(17301555);
                menu.add(0, 1, 0, "Forward").setIcon(17301565);
                menu.add(0, 2, 0, "Close").setIcon(17301560);
                return true;
            default:
                return false;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(this.k);
        } catch (Exception e2) {
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        switch (this.type) {
            case 0:
                if (i2 == 4) {
                    finish();
                }
                return super.onKeyDown(i2, keyEvent);
            case 2:
                if (i2 != 4 || !((ak) this.view).hc.canGoBack()) {
                    return super.onKeyDown(i2, keyEvent);
                }
                ((ak) this.view).hc.goBack();
                return true;
            case 3:
                if (i2 != 4) {
                    return super.onKeyDown(i2, keyEvent);
                }
                if (((bq) this.view).aQ.canGoBack()) {
                    ((bq) this.view).aQ.M();
                }
                ((bq) this.view).exit(500);
                return true;
            case 9:
                if (i2 != 4) {
                    return super.onKeyDown(i2, keyEvent);
                }
                if (!((bs) this.view).aQ.canGoBack()) {
                    ((bs) this.view).exit(0);
                } else if (((bs) this.view).aQ.canGoBackOrForward(-2)) {
                    ((bs) this.view).aQ.goBack();
                } else {
                    ((bs) this.view).aQ.M();
                    ((bs) this.view).aQ.clearHistory();
                }
                return true;
            default:
                return super.onKeyDown(i2, keyEvent);
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (this.type) {
            case 2:
                switch (menuItem.getItemId()) {
                    case 0:
                        Browser.saveBookmark(this, ((ak) this.view).hc.getTitle(), ((ak) this.view).hc.getUrl());
                        return true;
                    case 1:
                        if (((ak) this.view).hc.canGoForward()) {
                            ((ak) this.view).hc.goForward();
                        }
                        return true;
                    case 2:
                        finish();
                        return true;
                    default:
                        return super.onContextItemSelected(menuItem);
                }
            case 9:
                switch (menuItem.getItemId()) {
                    case 0:
                        Browser.saveBookmark(this, ((r) this.view).aQ.getTitle(), ((r) this.view).aQ.getUrl());
                        return true;
                    case 1:
                        if (((r) this.view).aQ.canGoForward()) {
                            ((r) this.view).aQ.goForward();
                        }
                        return true;
                    case 2:
                        ((r) this.view).exit(0);
                        return true;
                    default:
                        return super.onContextItemSelected(menuItem);
                }
            default:
                return false;
        }
    }

    public void onPause() {
        super.onPause();
        switch (this.type) {
            case 0:
                ((y) this.view).hM.dismiss();
                return;
            case 1:
            case 2:
            default:
                return;
            case 3:
                ((bq) this.view).aQ.ck.q();
                return;
        }
    }

    public void onRestoreInstanceState(Bundle bundle) {
        if (bundle.containsKey("imageUri")) {
            this.m = (Uri) bundle.getParcelable("imageUri");
        }
    }

    public void onResume() {
        super.onResume();
        switch (this.type) {
            case 3:
            case 9:
                ((r) this.view).aQ.ck.r();
                ((r) this.view).fB = false;
                if (!this.l) {
                    ((r) this.view).aQ.ck.A();
                }
                this.l = false;
                return;
            case 4:
                if (bw.fm().xv == null) {
                    finish();
                    return;
                } else if (this.f != null) {
                    startActivityForResult(this.f, this.type);
                    return;
                } else {
                    finish();
                    return;
                }
            case 5:
                if (this.f != null) {
                    startActivityForResult(Intent.createChooser(this.f, "Select Picture"), this.type);
                    return;
                } else {
                    finish();
                    return;
                }
            case 6:
                if (this.f != null) {
                    startActivityForResult(Intent.createChooser(this.f, "Select Picture"), this.type);
                    return;
                } else {
                    finish();
                    return;
                }
            case 7:
                startActivityForResult(cc.fs().ft(), this.type);
                return;
            case 8:
                try {
                    startActivityForResult(this.f, this.type);
                    return;
                } catch (Exception e2) {
                    bw.fm().xv.ck.i("Error getting contact.");
                    finish();
                    return;
                }
            default:
                return;
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        if (this.m != null) {
            bundle.putParcelable("imageUri", this.m);
        }
        super.onSaveInstanceState(bundle);
    }

    public void onStart() {
        super.onStart();
        switch (this.type) {
            case 0:
                if (!((y) this.view).hR) {
                    a();
                    ((y) this.view).hM = ProgressDialog.show(this, "", "Loading...", true, true);
                    ((y) this.view).hM.setOnCancelListener(new by(this));
                    return;
                }
                ((y) this.view).hQ.setVisibility(0);
                ((y) this.view).hO.start();
                return;
            default:
                return;
        }
    }

    public void onStop() {
        super.onStop();
        switch (this.type) {
            case 3:
                if (((r) this.view).fB) {
                    ((r) this.view).aQ.ck.z();
                    return;
                } else if (((r) this.view).fA) {
                    ((r) this.view).exit(1);
                    ((r) this.view).aQ.ck.z();
                    return;
                } else {
                    return;
                }
            case 9:
                if (((r) this.view).fB) {
                    ((r) this.view).aQ.ck.z();
                    return;
                } else if (((r) this.view).fA) {
                    ((r) this.view).aQ.ck.z();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }
}
