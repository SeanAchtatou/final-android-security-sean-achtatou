package com.mobclix.android.sdk;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.TimerTask;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

class al extends TimerTask implements Runnable {
    private String as;
    private Handler handler;

    al(String str, Handler handler2) {
        this.as = str;
        this.handler = handler2;
    }

    private void y(int i) {
        Message message = new Message();
        Bundle bundle = new Bundle();
        bundle.putString("type", "failure");
        bundle.putInt("errorCode", i);
        message.setData(bundle);
        this.handler.sendMessage(message);
    }

    public void run() {
        BufferedReader bufferedReader;
        int parseInt;
        BufferedReader bufferedReader2;
        bw.ar.fk();
        if (this.as.equals("")) {
            y(-503);
        }
        BufferedReader bufferedReader3 = null;
        try {
            HttpResponse cm = new as(this.as).cm();
            HttpEntity entity = cm.getEntity();
            int statusCode = cm.getStatusLine().getStatusCode();
            if ((statusCode == 200 || statusCode == 251) && entity != null) {
                BufferedReader bufferedReader4 = new BufferedReader(new InputStreamReader(entity.getContent()), 8000);
                try {
                    String str = "";
                    for (String readLine = bufferedReader4.readLine(); readLine != null; readLine = bufferedReader4.readLine()) {
                        str = String.valueOf(str) + readLine;
                    }
                    entity.consumeContent();
                    if (!str.equals("")) {
                        Message message = new Message();
                        Bundle bundle = new Bundle();
                        bundle.putString("type", "success");
                        bundle.putString("response", str);
                        message.setData(bundle);
                        this.handler.sendMessage(message);
                        bufferedReader2 = bufferedReader4;
                    } else {
                        bufferedReader2 = bufferedReader4;
                    }
                } catch (Throwable th) {
                    th = th;
                    bufferedReader3 = bufferedReader4;
                    bufferedReader3.close();
                    throw th;
                }
            } else {
                switch (statusCode) {
                    case 251:
                        String value = cm.getFirstHeader("X-Mobclix-Suballocation").getValue();
                        if (value != null) {
                            parseInt = Integer.parseInt(value);
                            break;
                        }
                    default:
                        parseInt = -503;
                        break;
                }
                y(parseInt);
                bufferedReader2 = null;
            }
            try {
                bufferedReader2.close();
            } catch (Exception e) {
            }
        } catch (Throwable th2) {
            th = th2;
            bufferedReader3.close();
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public final void setUrl(String str) {
        this.as = str;
    }
}
