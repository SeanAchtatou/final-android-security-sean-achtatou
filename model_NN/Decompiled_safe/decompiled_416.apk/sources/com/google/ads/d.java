package com.google.ads;

import android.content.Context;
import android.location.Location;
import com.google.ads.util.AdUtil;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class d {
    private static String sS = AdUtil.az("emulator");
    private boolean dT = false;
    private String dz = null;
    private boolean oZ = false;
    private a sO = null;
    private Set sP = null;
    private Map sQ = null;
    private Location sR = null;
    private Set sT = null;

    public final void aw(String str) {
        if (this.sP == null) {
            this.sP = new HashSet();
        }
        this.sP.add(str);
    }

    public final Map j(Context context) {
        String k;
        HashMap hashMap = new HashMap();
        if (this.sP != null) {
            hashMap.put("kw", this.sP);
        }
        if (this.sO != null) {
            hashMap.put("cust_gender", this.sO.toString());
        }
        if (this.dz != null) {
            hashMap.put("cust_age", this.dz);
        }
        if (this.sR != null) {
            hashMap.put("uule", AdUtil.b(this.sR));
        }
        if (this.dT) {
            hashMap.put("testing", 1);
        }
        if ((this.sT == null || (k = AdUtil.k(context)) == null || !this.sT.contains(k)) ? false : true) {
            hashMap.put("adtest", "on");
        } else if (!this.oZ) {
            com.google.ads.util.d.bj("To get test ads on this device, call adRequest.addTestDevice(" + (AdUtil.eJ() ? "AdRequest.TEST_EMULATOR" : "\"" + AdUtil.k(context) + "\"") + ");");
            this.oZ = true;
        }
        if (this.sQ != null) {
            hashMap.put("extras", this.sQ);
        }
        return hashMap;
    }
}
