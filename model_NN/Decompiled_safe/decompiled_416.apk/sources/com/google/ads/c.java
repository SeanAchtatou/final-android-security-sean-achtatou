package com.google.ads;

public enum c {
    INVALID_REQUEST("Invalid Google Ad request."),
    NO_FILL("Ad request successful, but no ad returned due to lack of ad inventory."),
    NETWORK_ERROR("A network error occurred."),
    INTERNAL_ERROR("There was an internal error.");
    
    private String cU;

    private c(String str) {
        this.cU = str;
    }

    public final String toString() {
        return this.cU;
    }
}
