package com.google.ads.util;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.location.Location;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import b.a.a;
import b.a.b;
import b.a.f;
import com.google.ads.AdActivity;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URLDecoder;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public final class AdUtil {
    private static boolean dT = true;
    private static String dy;
    private static String ik = null;
    private static boolean oZ = false;
    private static String vA = null;
    private static int vx;
    private static Boolean vy = null;
    private static AudioManager vz;

    public class UserActivityReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.intent.action.USER_PRESENT")) {
                AdUtil.j(true);
            } else if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
                AdUtil.j(false);
            }
        }
    }

    static {
        int i;
        try {
            i = Integer.parseInt(Build.VERSION.SDK);
        } catch (NumberFormatException e) {
            d.bl("The Android SDK version couldn't be parsed to an int: " + Build.VERSION.SDK);
            d.bl("Defaulting to Android SDK version 3.");
            i = 3;
        }
        vx = i;
    }

    private AdUtil() {
    }

    public static int a(Context context, DisplayMetrics displayMetrics) {
        return vx >= 4 ? f.a(context, displayMetrics) : displayMetrics.heightPixels;
    }

    private static a a(Set set) {
        a aVar = new a();
        if (set != null && !set.isEmpty()) {
            for (Object next : set) {
                if ((next instanceof String) || (next instanceof Integer) || (next instanceof Double) || (next instanceof Long) || (next instanceof Float)) {
                    aVar.b(next);
                } else if (next instanceof Map) {
                    try {
                        aVar.b(b((Map) next));
                    } catch (ClassCastException e) {
                        d.b("Unknown map type in json serialization: ", e);
                    }
                } else if (next instanceof Set) {
                    try {
                        aVar.b(a((Set) next));
                    } catch (ClassCastException e2) {
                        d.b("Unknown map type in json serialization: ", e2);
                    }
                } else {
                    d.bl("Unknown value in json serialization: " + next);
                }
            }
        }
        return aVar;
    }

    public static String a(Map map) {
        try {
            return b(map).toString();
        } catch (f e) {
            d.b("JsonException in serialization: ", e);
            return null;
        }
    }

    public static void a(WebView webView) {
        webView.getSettings().setUserAgentString(r(webView.getContext().getApplicationContext()));
    }

    public static void a(HttpURLConnection httpURLConnection, Context context) {
        httpURLConnection.setRequestProperty("User-Agent", r(context));
    }

    private static String aA(String str) {
        try {
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(1, new SecretKeySpec(new byte[]{10, 55, -112, -47, -6, 7, 11, 75, -7, -121, 121, 69, 80, -61, 15, 5}, "AES"));
            byte[] iv = instance.getIV();
            byte[] doFinal = instance.doFinal(str.getBytes());
            byte[] bArr = new byte[(iv.length + doFinal.length)];
            System.arraycopy(iv, 0, bArr, 0, iv.length);
            System.arraycopy(doFinal, 0, bArr, iv.length, doFinal.length);
            return e.e(bArr);
        } catch (GeneralSecurityException e) {
            return null;
        }
    }

    public static String az(String str) {
        if (str == null || str.length() <= 0) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes(), 0, str.length());
            return String.format("%032X", new BigInteger(1, instance.digest()));
        } catch (NoSuchAlgorithmException e) {
            return str.substring(0, 32);
        }
    }

    public static int b(Context context, DisplayMetrics displayMetrics) {
        return vx >= 4 ? f.b(context, displayMetrics) : displayMetrics.widthPixels;
    }

    public static DisplayMetrics b(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    private static b b(Map map) {
        b bVar = new b();
        if (map == null || map.isEmpty()) {
            return bVar;
        }
        for (String str : map.keySet()) {
            Object obj = map.get(str);
            if ((obj instanceof String) || (obj instanceof Integer) || (obj instanceof Double) || (obj instanceof Long) || (obj instanceof Float)) {
                bVar.a(str, obj);
            } else if (obj instanceof Map) {
                try {
                    bVar.a(str, b((Map) obj));
                } catch (ClassCastException e) {
                    d.b("Unknown map type in json serialization: ", e);
                }
            } else if (obj instanceof Set) {
                try {
                    bVar.a(str, a((Set) obj));
                } catch (ClassCastException e2) {
                    d.b("Unknown map type in json serialization: ", e2);
                }
            } else {
                d.bl("Unknown value in json serialization: " + obj);
            }
        }
        return bVar;
    }

    public static String b(Location location) {
        if (location == null) {
            return null;
        }
        return "e1+" + aA(String.format("role: 6 producer: 24 historical_role: 1 historical_producer: 12 timestamp: %d latlng < latitude_e7: %d longitude_e7: %d> radius: %d", Long.valueOf(location.getTime() * 1000), Long.valueOf((long) (location.getLatitude() * 1.0E7d)), Long.valueOf((long) (location.getLongitude() * 1.0E7d)), Long.valueOf((long) (location.getAccuracy() * 1000.0f))));
    }

    public static boolean b(Uri uri) {
        String scheme = uri.getScheme();
        return "http".equalsIgnoreCase(scheme) || "https".equalsIgnoreCase(scheme);
    }

    public static HashMap e(Uri uri) {
        if (uri == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        String encodedQuery = uri.getEncodedQuery();
        if (encodedQuery == null) {
            return hashMap;
        }
        for (String str : encodedQuery.split("&")) {
            int indexOf = str.indexOf(61);
            if (indexOf == -1) {
                return null;
            }
            hashMap.put(URLDecoder.decode(str.substring(0, indexOf)), URLDecoder.decode(str.substring(indexOf + 1)));
        }
        return hashMap;
    }

    public static int eH() {
        return vx >= 9 ? 6 : 0;
    }

    public static int eI() {
        return vx >= 9 ? 7 : 1;
    }

    public static boolean eJ() {
        return "unknown".equals(Build.BOARD) && "generic".equals(Build.DEVICE) && "generic".equals(Build.BRAND);
    }

    public static boolean eK() {
        return dT;
    }

    public static void j(boolean z) {
        dT = z;
    }

    public static String k(Context context) {
        if (ik == null) {
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            String az = (string == null || eJ()) ? az("emulator") : az(string);
            if (az == null) {
                return null;
            }
            ik = az.toUpperCase(Locale.US);
        }
        return ik;
    }

    public static boolean l(Context context) {
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();
        if (packageManager.checkPermission("android.permission.INTERNET", packageName) == -1) {
            d.s("INTERNET permissions must be enabled in AndroidManifest.xml.");
            return false;
        } else if (packageManager.checkPermission("android.permission.ACCESS_NETWORK_STATE", packageName) != -1) {
            return true;
        } else {
            d.s("ACCESS_NETWORK_STATE permissions must be enabled in AndroidManifest.xml.");
            return false;
        }
    }

    public static boolean m(Context context) {
        boolean z;
        if (vy != null) {
            return vy.booleanValue();
        }
        ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(new Intent(context, AdActivity.class), 65536);
        boolean z2 = true;
        if (resolveActivity == null || resolveActivity.activityInfo == null) {
            d.s("Could not find com.google.ads.AdActivity, please make sure it is registered in AndroidManifest.xml.");
            z = false;
        } else {
            if ((resolveActivity.activityInfo.configChanges & 16) == 0) {
                d.s("The android:configChanges value of the com.google.ads.AdActivity must include keyboard.");
                z2 = false;
            }
            if ((resolveActivity.activityInfo.configChanges & 32) == 0) {
                d.s("The android:configChanges value of the com.google.ads.AdActivity must include keyboardHidden.");
                z2 = false;
            }
            if ((resolveActivity.activityInfo.configChanges & 128) == 0) {
                d.s("The android:configChanges value of the com.google.ads.AdActivity must include orientation.");
                z = false;
            } else {
                z = z2;
            }
        }
        vy = Boolean.valueOf(z);
        return z;
    }

    public static String n(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return null;
        }
        switch (activeNetworkInfo.getType()) {
            case 0:
                return "ed";
            case 1:
                return "wi";
            default:
                return "unknown";
        }
    }

    public static String o(Context context) {
        if (dy == null) {
            StringBuilder sb = new StringBuilder();
            PackageManager packageManager = context.getPackageManager();
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse("geo:0,0?q=donuts")), 65536);
            if (queryIntentActivities == null || queryIntentActivities.size() == 0) {
                sb.append("m");
            }
            List<ResolveInfo> queryIntentActivities2 = packageManager.queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:com.google")), 65536);
            if (queryIntentActivities2 == null || queryIntentActivities2.size() == 0) {
                if (sb.length() > 0) {
                    sb.append(",");
                }
                sb.append("a");
            }
            List<ResolveInfo> queryIntentActivities3 = packageManager.queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse("tel://6509313940")), 65536);
            if (queryIntentActivities3 == null || queryIntentActivities3.size() == 0) {
                if (sb.length() > 0) {
                    sb.append(",");
                }
                sb.append("t");
            }
            dy = sb.toString();
        }
        return dy;
    }

    public static a p(Context context) {
        if (vz == null) {
            vz = (AudioManager) context.getSystemService("audio");
        }
        int mode = vz.getMode();
        if (eJ()) {
            return a.EMULATOR;
        }
        if (vz.isMusicActive() || vz.isSpeakerphoneOn() || mode == 2 || mode == 1) {
            return a.VIBRATE;
        }
        int ringerMode = vz.getRingerMode();
        return (ringerMode == 0 || ringerMode == 1) ? a.VIBRATE : a.SPEAKER;
    }

    public static void q(Context context) {
        if (!oZ) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.USER_PRESENT");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            context.registerReceiver(new UserActivityReceiver(), intentFilter);
            oZ = true;
        }
    }

    private static String r(Context context) {
        if (vA == null) {
            String userAgentString = new WebView(context).getSettings().getUserAgentString();
            if (userAgentString == null || userAgentString.length() == 0 || userAgentString.equals("Java0")) {
                String property = System.getProperty("os.name", "Linux");
                String str = "Android " + Build.VERSION.RELEASE;
                Locale locale = Locale.getDefault();
                String lowerCase = locale.getLanguage().toLowerCase(Locale.US);
                if (lowerCase.length() == 0) {
                    lowerCase = "en";
                }
                String lowerCase2 = locale.getCountry().toLowerCase(Locale.US);
                userAgentString = "Mozilla/5.0 (" + property + "; U; " + str + "; " + (lowerCase2.length() > 0 ? lowerCase + "-" + lowerCase2 : lowerCase) + "; " + (Build.MODEL + " Build/" + Build.ID) + ") AppleWebKit/0.0 (KHTML, like " + "Gecko) Version/0.0 Mobile Safari/0.0";
            }
            vA = userAgentString + " (Mobile; " + "afma-sdk-a-v" + "4.1.1" + ")";
        }
        return vA;
    }
}
