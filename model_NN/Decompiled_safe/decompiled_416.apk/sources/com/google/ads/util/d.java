package com.google.ads.util;

import android.util.Log;

public final class d {
    private d() {
    }

    public static void a(String str, Throwable th) {
        if (j("Ads", 6)) {
            Log.e("Ads", str, th);
        }
    }

    public static void b(String str, Throwable th) {
        if (j("Ads", 5)) {
            Log.w("Ads", str, th);
        }
    }

    public static void bj(String str) {
        if (j("Ads", 4)) {
            Log.i("Ads", str);
        }
    }

    public static void bk(String str) {
        if (j("Ads", 2)) {
            Log.v("Ads", str);
        }
    }

    public static void bl(String str) {
        if (j("Ads", 5)) {
            Log.w("Ads", str);
        }
    }

    private static boolean j(String str, int i) {
        return (i >= 5) || Log.isLoggable(str, i);
    }

    public static void k(String str) {
        if (j("Ads", 3)) {
            Log.d("Ads", str);
        }
    }

    public static void s(String str) {
        if (j("Ads", 6)) {
            Log.e("Ads", str);
        }
    }
}
