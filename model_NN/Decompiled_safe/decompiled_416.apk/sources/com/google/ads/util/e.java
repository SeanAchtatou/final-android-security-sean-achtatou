package com.google.ads.util;

import java.io.UnsupportedEncodingException;

public class e {
    private static /* synthetic */ boolean Ap = (!e.class.desiredAssertionStatus());

    private e() {
    }

    public static String e(byte[] bArr) {
        try {
            int length = bArr.length;
            b bVar = new b();
            int i = (length / 3) * 4;
            if (!bVar.dR) {
                switch (length % 3) {
                    case 1:
                        i += 2;
                        break;
                    case 2:
                        i += 3;
                        break;
                }
            } else if (length % 3 > 0) {
                i += 4;
            }
            if (bVar.dS && length > 0) {
                i += (((length - 1) / 57) + 1) * (bVar.dT ? 2 : 1);
            }
            bVar.pa = new byte[i];
            bVar.b(bArr, length);
            if (Ap || bVar.pb == i) {
                return new String(bVar.pa, "US-ASCII");
            }
            throw new AssertionError();
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }
}
