package com.google.ads;

public final class g {
    public static final g Br = new g(320, 50, "320x50_mb");
    public static final g Bs = new g(300, 250, "300x250_as");
    public static final g Bt = new g(468, 60, "468x60_as");
    public static final g Bu = new g(728, 90, "728x90_as");
    private String ik;
    private int pb;
    private int vx;

    private g(int i, int i2, String str) {
        this.vx = i;
        this.pb = i2;
        this.ik = str;
    }

    public final int getHeight() {
        return this.pb;
    }

    public final int getWidth() {
        return this.vx;
    }

    public final String toString() {
        return this.ik;
    }
}
