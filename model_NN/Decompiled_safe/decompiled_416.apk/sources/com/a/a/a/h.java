package com.a.a.a;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

public final class h {
    private static String qo = "https://graph.facebook.com/oauth/authorize";
    private static String qp = "https://www.facebook.com/connect/uiserver.php";
    private static String qq = "https://graph.facebook.com/";
    private static String qr = "https://api.facebook.com/restserver.php";
    private String qs = null;
    private long qt = 0;

    public final void a(Context context, String str, String[] strArr, a aVar) {
        String str2;
        Bundle bundle = new Bundle();
        bundle.putString("client_id", str);
        if (strArr.length > 0) {
            bundle.putString("scope", TextUtils.join(",", strArr));
        }
        CookieSyncManager.createInstance(context);
        b bVar = new b(this, aVar);
        if ("login".equals("login")) {
            str2 = qo;
            bundle.putString("type", "user_agent");
            bundle.putString("redirect_uri", "fbconnect://success");
        } else {
            String str3 = qp;
            bundle.putString("method", "login");
            bundle.putString("next", "fbconnect://success");
            str2 = str3;
        }
        bundle.putString("display", "touch");
        if (cN()) {
            bundle.putString("access_token", this.qs);
        }
        String str4 = String.valueOf(str2) + "?" + e.b(bundle);
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") != 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Error");
            builder.setMessage("Application requires permission to access the Internet");
            builder.create().show();
            return;
        }
        new c(context, str4, bVar).show();
    }

    public final void ar(String str) {
        this.qs = str;
    }

    public final void as(String str) {
        if (str != null && !str.equals("0")) {
            this.qt = System.currentTimeMillis() + ((long) (Integer.parseInt(str) * 1000));
        }
    }

    public final void b(long j) {
        this.qt = j;
    }

    public final boolean cN() {
        return this.qs != null && (this.qt == 0 || System.currentTimeMillis() < this.qt);
    }

    public final String cO() {
        return this.qs;
    }

    public final long cP() {
        return this.qt;
    }

    public final String i(Context context) {
        CookieSyncManager.createInstance(context);
        CookieManager.getInstance().removeAllCookie();
        Bundle bundle = new Bundle();
        bundle.putString("method", "auth.expireSession");
        if (!bundle.containsKey("method")) {
            throw new IllegalArgumentException("API method must be specified. (parameters must contain key \"method\" and value). See http://developers.facebook.com/docs/reference/rest/");
        }
        bundle.putString("format", "json");
        if (cN()) {
            bundle.putString("access_token", this.qs);
        }
        String a2 = e.a(qr, "GET", bundle);
        this.qs = null;
        this.qt = 0;
        return a2;
    }
}
