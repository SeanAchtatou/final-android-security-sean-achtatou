package com.a.a.a;

import android.os.Bundle;
import android.util.Log;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public final class e {
    private static Bundle T(String str) {
        Bundle bundle = new Bundle();
        if (str != null) {
            for (String split : str.split("&")) {
                String[] split2 = split.split("=");
                bundle.putString(split2[0], split2[1]);
            }
        }
        return bundle;
    }

    public static Bundle U(String str) {
        try {
            URL url = new URL(str.replace("fbconnect", "http"));
            Bundle T = T(url.getQuery());
            T.putAll(T(url.getRef()));
            return T;
        } catch (MalformedURLException e) {
            return new Bundle();
        }
    }

    public static String a(String str, String str2, Bundle bundle) {
        String str3 = str2.equals("GET") ? String.valueOf(str) + "?" + b(bundle) : str;
        Log.d("Facebook-Util", String.valueOf(str2) + " URL: " + str3);
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str3).openConnection();
        httpURLConnection.setRequestProperty("User-Agent", String.valueOf(System.getProperties().getProperty("http.agent")) + " FacebookAndroidSDK");
        if (!str2.equals("GET")) {
            bundle.putString("method", str2);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.getOutputStream().write(b(bundle).getBytes("UTF-8"));
        }
        try {
            return b(httpURLConnection.getInputStream());
        } catch (FileNotFoundException e) {
            return b(httpURLConnection.getErrorStream());
        }
    }

    public static String b(Bundle bundle) {
        if (bundle == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (String next : bundle.keySet()) {
            if (z) {
                z = false;
            } else {
                sb.append("&");
            }
            sb.append(String.valueOf(next) + "=" + bundle.getString(next));
        }
        return sb.toString();
    }

    private static String b(InputStream inputStream) {
        StringBuilder sb = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 1000);
        for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
            sb.append(readLine);
        }
        inputStream.close();
        return sb.toString();
    }
}
