package com.a.a.a;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class k extends WebViewClient {
    private /* synthetic */ c Ch;

    /* synthetic */ k(c cVar) {
        this(cVar, (byte) 0);
    }

    private k(c cVar, byte b2) {
        this.Ch = cVar;
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        String title = this.Ch.hc.getTitle();
        if (title != null && title.length() > 0) {
            this.Ch.he.setText(title);
        }
        this.Ch.hb.dismiss();
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        Log.d("Facebook-WebView", "Webview loading URL: " + str);
        super.onPageStarted(webView, str, bitmap);
        this.Ch.hb.show();
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
        this.Ch.ha.a(new d(str, i, str2));
        this.Ch.dismiss();
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        Log.d("Facebook-WebView", "Redirect URL: " + str);
        if (str.startsWith("fbconnect://success")) {
            Bundle U = e.U(str);
            String string = U.getString("error_reason");
            if (string == null) {
                this.Ch.ha.a(U);
            } else {
                this.Ch.ha.a(new g(string));
            }
            this.Ch.dismiss();
            return true;
        } else if (str.startsWith("fbconnect:cancel")) {
            this.Ch.ha.onCancel();
            this.Ch.dismiss();
            return true;
        } else if (str.contains("touch")) {
            return false;
        } else {
            this.Ch.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        }
    }
}
