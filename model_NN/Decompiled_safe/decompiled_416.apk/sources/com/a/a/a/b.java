package com.a.a.a;

import android.os.Bundle;
import android.util.Log;
import android.webkit.CookieSyncManager;

final class b implements a {
    private /* synthetic */ h gh;
    private final /* synthetic */ a gi;

    b(h hVar, a aVar) {
        this.gh = hVar;
        this.gi = aVar;
    }

    public final void a(Bundle bundle) {
        CookieSyncManager.getInstance().sync();
        this.gh.ar(bundle.getString("access_token"));
        this.gh.as(bundle.getString("expires_in"));
        if (this.gh.cN()) {
            Log.d("Facebook-authorize", "Login Success! access_token=" + this.gh.cO() + " expires=" + this.gh.cP());
            this.gi.a(bundle);
            return;
        }
        a(new g("failed to receive access_token"));
    }

    public final void a(d dVar) {
        Log.d("Facebook-authorize", "Login failed: " + dVar);
        this.gi.a(dVar);
    }

    public final void a(g gVar) {
        Log.d("Facebook-authorize", "Login failed: " + gVar);
        this.gi.a(gVar);
    }

    public final void onCancel() {
        Log.d("Facebook-authorize", "Login cancelled");
        this.gi.onCancel();
    }
}
