package com.a.a.a;

import android.content.Context;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

final class f extends Thread {
    private /* synthetic */ i nC;
    private final /* synthetic */ Context nD;
    private final /* synthetic */ j nE;

    f(i iVar, Context context, j jVar) {
        this.nC = iVar;
        this.nD = context;
        this.nE = jVar;
    }

    public final void run() {
        try {
            String i = this.nC.rB.i(this.nD);
            if (i.length() == 0 || i.equals("false")) {
                this.nE.a(new g("auth.expireSession failed"));
            } else {
                this.nE.ay("logout");
            }
        } catch (FileNotFoundException e) {
            this.nE.a(e);
        } catch (MalformedURLException e2) {
            this.nE.a(e2);
        } catch (IOException e3) {
            this.nE.a(e3);
        }
    }
}
