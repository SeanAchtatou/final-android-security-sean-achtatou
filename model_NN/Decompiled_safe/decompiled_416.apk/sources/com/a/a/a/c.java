package com.a.a.a;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.epoint.android.games.mjfgbfree.C0000R;

public final class c extends Dialog {
    private static float[] gW = {460.0f, 260.0f};
    private static float[] gX = {280.0f, 420.0f};
    private static FrameLayout.LayoutParams gY = new FrameLayout.LayoutParams(-1, -1);
    private String gZ;
    /* access modifiers changed from: private */
    public a ha;
    /* access modifiers changed from: private */
    public ProgressDialog hb;
    /* access modifiers changed from: private */
    public WebView hc;
    private LinearLayout hd;
    /* access modifiers changed from: private */
    public TextView he;

    public c(Context context, String str, a aVar) {
        super(context);
        this.gZ = str;
        this.ha = aVar;
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.hb = new ProgressDialog(getContext());
        this.hb.requestWindowFeature(1);
        this.hb.setMessage("Loading...");
        this.hd = new LinearLayout(getContext());
        this.hd.setOrientation(1);
        requestWindowFeature(1);
        Drawable drawable = getContext().getResources().getDrawable(C0000R.drawable.facebook_icon);
        this.he = new TextView(getContext());
        this.he.setText("Facebook");
        this.he.setTextColor(-1);
        this.he.setTypeface(Typeface.DEFAULT_BOLD);
        this.he.setBackgroundColor(-9599820);
        this.he.setPadding(6, 4, 4, 4);
        this.he.setCompoundDrawablePadding(6);
        this.he.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
        this.hd.addView(this.he);
        this.hc = new WebView(getContext());
        this.hc.setVerticalScrollBarEnabled(false);
        this.hc.setHorizontalScrollBarEnabled(false);
        this.hc.setWebViewClient(new k(this));
        this.hc.getSettings().setJavaScriptEnabled(true);
        this.hc.loadUrl(this.gZ);
        this.hc.setLayoutParams(gY);
        this.hd.addView(this.hc);
        Display defaultDisplay = getWindow().getWindowManager().getDefaultDisplay();
        float f = getContext().getResources().getDisplayMetrics().density;
        float[] fArr = defaultDisplay.getWidth() < defaultDisplay.getHeight() ? gX : gW;
        addContentView(this.hd, new FrameLayout.LayoutParams((int) ((fArr[0] * f) + 0.5f), (int) ((fArr[1] * f) + 0.5f)));
    }
}
