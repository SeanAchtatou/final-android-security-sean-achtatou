package a.a;

import java.io.Serializable;

public final class a implements Serializable {
    private int dU = 0;
    private int dV;
    private String dW;

    public a(int i) {
        this.dU = i;
        this.dV = d.B(this.dU);
        this.dW = d.a(this, "");
    }

    public final int aM() {
        return this.dU;
    }

    public final int aN() {
        return this.dV;
    }

    public final boolean aO() {
        return this.dU > 0 && this.dU < 35;
    }

    public final String t(String str) {
        return String.valueOf(this.dW) + str;
    }
}
