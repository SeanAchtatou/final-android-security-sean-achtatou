package a.a;

import java.util.Vector;

public final class d {
    public static int B(int i) {
        if (i > 0) {
            if (i <= 9) {
                return 0;
            }
            if (i <= 18) {
                return 1;
            }
            if (i <= 27) {
                return 2;
            }
            if (i <= 34) {
                return 3;
            }
            if (i <= 38) {
                return 4;
            }
            if (i <= 42) {
                return 5;
            }
        }
        return -1;
    }

    public static int a(a aVar, a aVar2) {
        a aVar3;
        a aVar4;
        if (B(aVar.aM()) == B(aVar2.aM())) {
            if (aVar.aM() > aVar2.aM()) {
                aVar3 = aVar;
                aVar4 = aVar2;
            } else {
                aVar3 = aVar2;
                aVar4 = aVar;
            }
            if (aVar4.aM() == aVar3.aM()) {
                return 2;
            }
            if (aVar4.aN() != 3) {
                if (aVar4.aM() + 1 == aVar3.aM()) {
                    return 2;
                }
                if (aVar4.aM() + 2 == aVar3.aM()) {
                    return 1;
                }
            }
        }
        return 0;
    }

    public static String a(a aVar, String str) {
        if (aVar == null) {
            return "-" + str;
        }
        return String.valueOf(e.nv[aVar.aM()]) + str;
    }

    public static String d(Vector vector, String str) {
        return e(vector, str);
    }

    public static String e(Vector vector, String str) {
        String str2 = "";
        int i = 0;
        while (true) {
            int i2 = i;
            String str3 = str2;
            if (i2 >= vector.size()) {
                return str3;
            }
            Object elementAt = vector.elementAt(i2);
            str2 = elementAt instanceof a ? String.valueOf(str3) + a((a) elementAt, String.valueOf("") + str) : String.valueOf(str3) + a((a) null, str);
            i = i2 + 1;
        }
    }
}
