package a.a;

import java.io.Serializable;

public final class g implements Serializable {
    public int mo;
    public int nV;
    public int nW;
    public boolean nX;
    private String nY;

    public g(int i, int i2, String str, boolean z, int i3) {
        this.nX = false;
        this.nW = i2;
        this.nV = i;
        this.nY = str;
        this.nX = z;
        this.mo = i3;
    }

    public g(String str) {
        this(0, 0, str, false, 1);
    }

    public final String cx() {
        return this.nY;
    }
}
