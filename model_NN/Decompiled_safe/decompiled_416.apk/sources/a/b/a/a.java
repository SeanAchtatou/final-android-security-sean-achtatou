package a.b.a;

import java.io.Serializable;

public final class a implements Serializable {
    public boolean eA = false;
    public boolean eB = false;
    public int eC = 0;
    public int eD = 0;
    public boolean eE = false;
    public boolean eF = false;
    public boolean eG = false;
    public boolean eH = false;
    public boolean eI = false;
    public boolean eJ = false;
    public int eK = 0;
    public int eL = 0;
    public boolean eM = false;
    public boolean eN = false;
    public boolean eO = false;
    public boolean eP = false;
    public boolean eQ = false;
    public boolean eR = false;
    public boolean eS = false;
    public boolean eT = false;
    public boolean eU = false;
    public boolean eV = false;
    public int eW = 0;
    public boolean eX = false;
    public boolean eY = false;
    public boolean eZ = false;
    public int ep = 0;
    public int eq = 0;
    public int er = 0;
    public int es = 0;
    public int et = 0;
    public boolean eu = false;
    public boolean ev = false;
    public int ew = 0;
    public int ex = 0;
    public int ey = 0;
    public int ez = 0;
    public boolean fa = false;
    public int fb = 0;
    public int fc = 0;
    public boolean fd = false;
    private boolean fe = false;
    public int ff = 0;
    public boolean fg = false;
    public boolean fh = false;
    public boolean fi = false;
    public boolean fj = false;
    public int fk = 0;
    public int fl = 0;
    private String fm;

    public final int a(a.b.c.b.a aVar) {
        int i = 0;
        this.fm = "";
        if (this.fc == 3) {
            i = aVar.a(88, 64, "連七對") + 0;
        } else if (this.fc == 2) {
            i = aVar.e(24, "七對") + 0;
        }
        if (this.fb == 3) {
            i += aVar.e(24, "七星不靠");
        } else if (this.fb == 2 || this.fb == 4) {
            i += aVar.e(12, "全不靠");
        }
        if (this.fd) {
            i += aVar.e(12, "組合龍");
        }
        if (!aVar.pu) {
            int i2 = this.ew + this.ex;
            int i3 = this.ey + this.ex;
            if (i3 == 4) {
                i += aVar.e(64, "四暗刻");
            } else if (i2 == 3) {
                i += aVar.e(32, "三槓");
                if (this.ex == 3) {
                    i += aVar.e(16, "三暗刻");
                }
            } else if (i3 == 3) {
                i += aVar.e(16, "三暗刻");
            } else if (i2 == 2) {
                i = this.ex == 2 ? i + aVar.e(8, "雙暗槓") : i + aVar.e(4, "雙槓");
            } else {
                if (i3 == 2) {
                    i += aVar.e(2, "雙暗刻");
                }
                if (this.ew == 1) {
                    i += aVar.e(1, "明槓");
                }
            }
        }
        if (this.ex > 0) {
            i += aVar.a(this.ex * 2, "暗槓", this.ex);
        }
        if (this.eX) {
            i += aVar.e(64, "一色雙龍會");
        } else if (this.eY) {
            i += aVar.e(16, "三色雙龍會");
        }
        if (this.ep == 4) {
            i += aVar.a(48, 99, "一色四同順");
        } else if (this.ep == 3) {
            i += aVar.e(24, "一色三同順");
        }
        if (this.er == 4) {
            i += aVar.a(48, 64, "一色四節高");
        } else if (this.er == 3) {
            i += aVar.e(24, "一色三節高");
        }
        if (this.et == 4) {
            i += aVar.e(32, "一色四步高");
        } else if (this.et == 3) {
            i += aVar.e(16, "一色三步高");
        }
        if (this.eV) {
            i += aVar.e(24, "全雙刻");
        }
        if (this.eO) {
            i += aVar.e(24, "全大");
        } else if (this.eN) {
            i += aVar.e(24, "全中");
        } else if (this.eM) {
            i += aVar.e(24, "全小");
        } else if (this.eP) {
            i += aVar.e(12, "大於五");
        } else if (this.eQ) {
            i += aVar.e(12, "小於五");
        }
        if (this.eR) {
            i += aVar.e(16, "全帶五");
        }
        if (this.eu) {
            i += aVar.e(16, "清龍");
        }
        if (this.ev) {
            i += aVar.e(8, "花龍");
        }
        if (this.eA) {
            i += aVar.e(16, "三同刻");
        }
        if (this.eL == 3) {
            i += aVar.e(12, "三風刻");
        }
        if (this.eS) {
            i += aVar.e(8, "推不倒");
        }
        if (this.eB) {
            i += aVar.e(8, "三色三同順");
        }
        if (this.eT) {
            i += aVar.e(8, "三色三節高");
        }
        if (this.eJ) {
            i += aVar.e(6, "碰碰和");
        }
        if (this.eU) {
            i += aVar.e(6, "三色三步高");
        }
        if (this.eZ) {
            i += aVar.e(6, "五門齊");
        }
        if (this.fa) {
            i += aVar.e(1, "缺一門");
        }
        if (this.eE) {
            i += aVar.e(6, "全求人");
        }
        if (this.fk == 2 || this.fk == 3) {
            i += aVar.e(4, "全帶么");
        } else if (this.fk == 1) {
            i += aVar.e(2, "斷么");
        }
        if (this.eI) {
            i += aVar.e(2, "平和");
        }
        if (this.ez > 0) {
            i += aVar.a(this.ez * 2, "四歸一", this.ez);
        } else if (this.eC > 0) {
            i += aVar.a(this.eC * 2, "雙同刻", this.eC);
        }
        if (this.eq > 0) {
            i += aVar.a(this.eq, "一般高", this.eq);
        }
        if (this.eD > 0) {
            i += aVar.a(this.eD, "喜雙逢", this.eD);
        }
        if (this.eW > 0) {
            i += aVar.a(this.eW, "連六", this.eW);
        }
        if (this.fl > 0) {
            i += aVar.a(this.fl, "老少副", this.fl);
        }
        if (this.eK > 0) {
            i += aVar.a(this.eK, "么九刻", this.eK);
        }
        if (aVar.pY == 0) {
            i += aVar.e(1, "無字");
        }
        if (this.ff > 0) {
            i += aVar.b(this.ff, this.ff, "花牌");
        }
        if (this.fg) {
            i += aVar.e(1, "邊張");
        } else if (this.fh) {
            i += aVar.e(1, "坎張");
        } else if (this.fi) {
            i += aVar.e(1, "單調將");
        }
        return this.fj ? i + aVar.e(4, "和絕張") : i;
    }
}
