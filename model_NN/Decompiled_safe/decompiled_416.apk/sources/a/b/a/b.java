package a.b.a;

import a.a.a;
import a.a.d;
import a.a.e;
import a.a.f;
import a.b.c.b.c;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public final class b {
    private static Hashtable iQ = null;

    public static Integer a(a aVar, a aVar2, a aVar3) {
        if (d.B(aVar.aM()) == 3) {
            if (aVar == aVar2 && aVar2 == aVar3) {
                return c.yG;
            }
        } else if (d.B(aVar.aM()) >= 4) {
            return null;
        } else {
            if (aVar.aN() == aVar2.aN() && aVar2.aN() == aVar3.aN()) {
                if (aVar == aVar2 && aVar2 == aVar3) {
                    return c.yG;
                }
                if (aVar.aM() + 1 == aVar2.aM() && aVar2.aM() + 1 == aVar3.aM()) {
                    return c.yF;
                }
            }
        }
        return null;
    }

    private static String a(Vector vector, a aVar, a aVar2, a aVar3) {
        String str = "";
        a aVar4 = aVar3;
        a aVar5 = aVar2;
        a aVar6 = aVar;
        for (int i = 0; i < vector.size(); i++) {
            if (aVar6 != null && vector.elementAt(i) == aVar6) {
                aVar6 = null;
            } else if (aVar5 != null && vector.elementAt(i) == aVar5) {
                aVar5 = null;
            } else if (aVar4 == null || vector.elementAt(i) != aVar4) {
                str = String.valueOf(str) + ((a) vector.elementAt(i)).t("");
            } else {
                aVar4 = null;
            }
        }
        return str;
    }

    public static Vector a(a.b.d.a aVar, a.b.c.a.b bVar, a aVar2, a.b.c.b.a aVar3) {
        return a(aVar, bVar, aVar2, new Hashtable(), aVar3);
    }

    private static Vector a(a.b.d.a aVar, a.b.c.a.b bVar, a aVar2, Hashtable hashtable, a.b.c.b.a aVar3) {
        a[] aVarArr;
        a aVar4;
        boolean z;
        Integer num;
        boolean z2;
        int i;
        c gb = bVar.gb();
        Vector b2 = b(gb.fw());
        if (aVar2 != null) {
            int i2 = 0;
            while (true) {
                i = i2;
                if (i < b2.size() && ((a) b2.elementAt(i)).aM() <= aVar2.aM()) {
                    i2 = i + 1;
                }
            }
            b2.insertElementAt(aVar2, i);
            if (b(b2, null)) {
                aVar3.pt = true;
            }
        }
        if (!aVar3.pt) {
            a(gb, b2, aVar3, aVar.bS(), bVar.gc());
            if (aVar2 != null) {
                Vector fx = gb.fx();
                hashtable.clear();
                hashtable.put(e.nv[11], a.a.c.mu.get(e.nv[11]));
                hashtable.put(e.nv[12], a.a.c.mu.get(e.nv[12]));
                hashtable.put(e.nv[13], a.a.c.mu.get(e.nv[13]));
                hashtable.put(e.nv[15], a.a.c.mu.get(e.nv[15]));
                hashtable.put(e.nv[17], a.a.c.mu.get(e.nv[17]));
                hashtable.put(e.nv[33], a.a.c.mu.get(e.nv[33]));
                int i3 = 0;
                while (true) {
                    if (i3 < fx.size()) {
                        if ((fx.elementAt(i3) instanceof a) && !hashtable.containsValue(fx.elementAt(i3))) {
                            z = false;
                            break;
                        }
                        i3++;
                    } else {
                        z = true;
                        break;
                    }
                }
                if (z) {
                    int i4 = 0;
                    while (true) {
                        if (i4 >= b2.size()) {
                            break;
                        } else if (!hashtable.containsValue(b2.elementAt(i4))) {
                            z = false;
                            break;
                        } else {
                            i4++;
                        }
                    }
                }
                if (z) {
                    aVar3.pQ = true;
                }
                aVar3.pz = gb.fx().size() == 0;
                if (aVar3.pT == 0) {
                    hashtable.clear();
                    aVar3.eJ = a(b2, c.yG, hashtable);
                }
                if (!aVar3.eJ && aVar3.pS == 0 && aVar3.pU == 0) {
                    hashtable.clear();
                    if (aVar3.pY == 0) {
                        aVar3.eI = a(b2, c.yF, hashtable);
                    }
                }
                int i5 = aVar3.pV + aVar3.pW + aVar3.pX;
                if (aVar3.eJ) {
                    Vector fw = gb.fw();
                    Vector fx2 = gb.fx();
                    Hashtable bu = bu();
                    int i6 = 0;
                    while (true) {
                        if (i6 >= fw.size()) {
                            int i7 = 0;
                            while (true) {
                                if (i7 < fx2.size()) {
                                    if ((fx2.elementAt(i7) instanceof a) && !bu.containsValue(fx2.elementAt(i7))) {
                                        z2 = false;
                                        break;
                                    }
                                    i7++;
                                } else {
                                    z2 = true;
                                    break;
                                }
                            }
                        } else if (!bu.containsValue(fw.elementAt(i6))) {
                            z2 = false;
                            break;
                        } else {
                            i6++;
                        }
                    }
                    if (z2) {
                        if (aVar3.pY == 0) {
                            aVar3.pB = true;
                            aVar3.eJ = false;
                        } else if (i5 != 0) {
                            aVar3.pA = true;
                        }
                    }
                    if (gb.fx().size() == 0 && bVar.gb().fz() == aVar2) {
                        aVar3.pP = true;
                        aVar3.eJ = false;
                    }
                }
                if (aVar3.pY == 0) {
                    aVar3.pn = aVar3.pV == i5 || aVar3.pW == i5 || aVar3.pX == i5;
                    if (aVar3.pn && aVar3.pz) {
                        hashtable.clear();
                        int i8 = 0;
                        while (true) {
                            int i9 = i8;
                            if (i9 >= b2.size()) {
                                break;
                            }
                            Integer num2 = (Integer) hashtable.get(b2.elementAt(i9));
                            if (num2 == null) {
                                num2 = new Integer(0);
                            }
                            hashtable.put(b2.elementAt(i9), Integer.valueOf(num2.intValue() + 1));
                            i8 = i9 + 1;
                        }
                        int i10 = aVar3.pV > 0 ? 1 : aVar3.pW > 0 ? 10 : 19;
                        Integer num3 = (Integer) hashtable.get(a.a.c.mu.get(e.nv[i10]));
                        boolean z3 = true;
                        if (num3 != null && num3.intValue() >= 3) {
                            int i11 = i10 + 1;
                            while (i11 < i10 + 8) {
                                boolean z4 = ((Integer) hashtable.get(a.a.c.mu.get(e.nv[i11]))) == null ? false : z3;
                                i11++;
                                z3 = z4;
                            }
                            if (z3 && (num = (Integer) hashtable.get(a.a.c.mu.get(e.nv[i11]))) != null && num.intValue() >= 3 && (bVar.gb().fz() == aVar2 || ((Integer) hashtable.get(aVar2)).intValue() > 1)) {
                                aVar3.pR = true;
                                aVar3.pn = false;
                                aVar3.pz = false;
                            }
                        }
                    }
                } else {
                    if (i5 == 0) {
                        aVar3.pv = true;
                        aVar3.eJ = false;
                    } else {
                        aVar3.po = aVar3.pV == i5 || aVar3.pW == i5 || aVar3.pX == i5;
                    }
                    int min = Math.min(aVar3.pZ, 3) + Math.min(aVar3.qa, 3) + Math.min(aVar3.qb, 3);
                    if (min == 9) {
                        aVar3.pp = true;
                    } else if (min == 8) {
                        aVar3.pq = true;
                    } else {
                        int min2 = Math.min(aVar3.qc[0], 3) + Math.min(aVar3.qc[1], 3) + Math.min(aVar3.qc[2], 3) + Math.min(aVar3.qc[3], 3);
                        if (min2 == 12) {
                            aVar3.pr = true;
                            aVar3.eJ = false;
                        } else if (min2 == 11) {
                            aVar3.ps = true;
                        }
                    }
                }
                if (aVar3.pU == 4) {
                    aVar3.pu = true;
                    aVar3.eJ = false;
                }
            }
        }
        a aVar5 = new a();
        aVar5.ff = gb.fy().size();
        Vector vector = new Vector();
        hashtable.clear();
        a(b2, hashtable, vector);
        a[] aVarArr2 = new a[vector.size()];
        aVar5.fj = aVar.cc()[aVar2.aM()] == 4;
        a aVar6 = aVar5;
        int i12 = 0;
        while (i12 < vector.size()) {
            aVarArr2[i12] = new a();
            aVarArr2[i12].ff = gb.fy().size();
            aVarArr2[i12].fj = aVar6.fj;
            c.a(gb.fx(), (Vector) vector.elementAt(i12), aVar2, gb.fz() != null, aVarArr2[i12], hashtable, gb.fE().size() == 1);
            if (i12 == 0 || aVar6.a(aVar3) < aVarArr2[i12].a(aVar3)) {
                aVar3.qi = i12;
                aVar4 = aVarArr2[i12];
            } else {
                aVar4 = aVar6;
            }
            i12++;
            aVar6 = aVar4;
        }
        int c = c.c(b2, null);
        if (c >= 2) {
            aVar6.fb = c;
            if (c == 4) {
                aVar6.fd = true;
                aVarArr = aVarArr2;
            }
            aVarArr = aVarArr2;
        } else {
            int d = c.d(b2, null);
            if (d == 3 || d == 2) {
                aVar6.fd = true;
                c.a(gb.fx(), b2, aVar2, gb.fz() != null, aVar6, hashtable, gb.fE().size() == 1);
                if (d == 2) {
                    aVar6.eI = true;
                } else if (gb.fx().size() != 0 && ((Integer) gb.fx().lastElement()) == c.yF) {
                    aVar6.eI = true;
                }
                vector.addElement(b2);
                aVarArr = new a[]{aVar6};
                aVar3.qi = 0;
            } else {
                int e = c.e(b2, null);
                if (e == 3 || (e == 2 && aVar6.ep != 4 && !aVar6.eX)) {
                    if (vector.size() == 0) {
                        c.a(gb.fx(), b2, aVar2, gb.fz() != null, aVar6, hashtable, gb.fE().size() == 1);
                    }
                    aVar3.qi = -1;
                    aVar6.fc = e;
                }
                aVarArr = aVarArr2;
            }
        }
        if (aVar3.pY > 0) {
            aVar6.eI = false;
        }
        aVar3.qg = aVarArr;
        aVar3.qh = vector;
        aVar3.a(aVar6);
        aVar3.eJ = aVar6.eJ;
        aVar3.eI = aVar6.eI;
        aVar3.pz = aVar3.pS == 0 && aVar3.pT == 0 && aVar6.ew == 0;
        aVar3.py = aVar.bW().size() == 0;
        return b2;
    }

    private static Vector a(Vector vector, a aVar) {
        Hashtable hashtable = a.b.d.a.lo;
        Vector vector2 = (Vector) vector.clone();
        a aVar2 = (a) vector2.remove(vector2.size() - 1);
        int i = 0;
        while (i < vector2.size() && ((a) vector2.elementAt(i)).aM() <= aVar2.aM()) {
            i++;
        }
        vector2.insertElementAt(aVar2, i);
        int i2 = 0;
        while (true) {
            if (i2 >= vector2.size()) {
                break;
            } else if (vector2.elementAt(i2) == aVar) {
                vector2.removeElementAt(i2);
                break;
            } else {
                i2++;
            }
        }
        Hashtable hashtable2 = new Hashtable();
        b(vector2, hashtable2);
        if (hashtable2.size() == 0) {
            hashtable.clear();
            a(vector2, hashtable2, hashtable);
            c.e(vector2, hashtable2);
            if (hashtable2.size() == 0) {
                c.c(vector2, hashtable2);
                if (hashtable2.size() == 0) {
                    c.d(vector2, hashtable2);
                }
            }
        }
        vector2.clear();
        if (hashtable2.size() > 0) {
            Enumeration elements = hashtable2.elements();
            while (elements.hasMoreElements()) {
                a aVar3 = (a) elements.nextElement();
                int i3 = 0;
                while (i3 < vector2.size() && ((a) vector2.elementAt(i3)).aM() <= aVar3.aM()) {
                    i3++;
                }
                vector2.insertElementAt(aVar3, i3);
            }
        }
        return vector2;
    }

    public static Vector a(Vector vector, String str) {
        Vector vector2 = new Vector();
        a aVar = (a) a.a.c.mu.get(str);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= vector.size()) {
                return a(vector2, aVar);
            }
            vector2.add((a) a.a.c.mu.get(vector.elementAt(i2)));
            i = i2 + 1;
        }
    }

    private static void a(int i, int i2, Vector vector, a.b.c.b.a aVar) {
        for (int i3 = 0; i3 < vector.size(); i3++) {
            if (vector.elementAt(i3) instanceof a) {
                a aVar2 = (a) vector.elementAt(i3);
                switch (aVar2.aN()) {
                    case 0:
                        aVar.pV++;
                        continue;
                    case 1:
                        aVar.pW++;
                        continue;
                    case 2:
                        aVar.pX++;
                        continue;
                    case 3:
                        aVar.pY++;
                        switch (aVar2.aM()) {
                            case 28:
                                int[] iArr = aVar.qc;
                                iArr[0] = iArr[0] + 1;
                                if (aVar.qc[0] == 3) {
                                    if (i == 0) {
                                        aVar.pF = true;
                                    }
                                    if (i2 != 0) {
                                        break;
                                    } else {
                                        aVar.pG = true;
                                        break;
                                    }
                                } else {
                                    continue;
                                    continue;
                                }
                            case 29:
                                int[] iArr2 = aVar.qc;
                                iArr2[1] = iArr2[1] + 1;
                                if (aVar.qc[1] == 3) {
                                    if (i == 1) {
                                        aVar.pF = true;
                                    }
                                    if (i2 != 1) {
                                        break;
                                    } else {
                                        aVar.pG = true;
                                        break;
                                    }
                                } else {
                                    continue;
                                }
                            case 30:
                                int[] iArr3 = aVar.qc;
                                iArr3[2] = iArr3[2] + 1;
                                if (aVar.qc[2] == 3) {
                                    if (i == 2) {
                                        aVar.pF = true;
                                    }
                                    if (i2 != 2) {
                                        break;
                                    } else {
                                        aVar.pG = true;
                                        break;
                                    }
                                } else {
                                    continue;
                                }
                            case 31:
                                int[] iArr4 = aVar.qc;
                                iArr4[3] = iArr4[3] + 1;
                                if (aVar.qc[3] == 3) {
                                    if (i == 3) {
                                        aVar.pF = true;
                                    }
                                    if (i2 != 3) {
                                        break;
                                    } else {
                                        aVar.pG = true;
                                        break;
                                    }
                                } else {
                                    continue;
                                }
                            case 32:
                                aVar.pZ++;
                                continue;
                            case 33:
                                aVar.qa++;
                                continue;
                            case 34:
                                aVar.qb++;
                                continue;
                        }
                }
            }
        }
    }

    public static void a(c cVar, Vector vector, a.b.c.b.a aVar, int i, int i2) {
        if (cVar.fx().size() > 0) {
            int i3 = 0;
            while (i3 < cVar.fx().size()) {
                if (!(cVar.fx().elementAt(i3 + 3) instanceof a)) {
                    if (cVar.fx().elementAt(i3) == cVar.fx().elementAt(i3 + 1)) {
                        aVar.pS++;
                    } else {
                        aVar.pT++;
                    }
                    i3 += 4;
                } else {
                    aVar.pU++;
                    i3 += 5;
                }
            }
        }
        for (int i4 = 0; i4 < cVar.fy().size(); i4++) {
            int aM = ((a) cVar.fy().elementAt(i4)).aM() - 35;
            if (aM >= 4) {
                if (aM % 4 == i2) {
                    aVar.pE = true;
                }
            } else if (aM == i2) {
                aVar.pD = true;
            }
        }
        aVar.pL = cVar.fy().size() == 0;
        a(i, i2, cVar.fx(), aVar);
        a(i, i2, vector, aVar);
        aVar.pI = aVar.pZ >= 3;
        aVar.pJ = aVar.qa >= 3;
        aVar.pK = aVar.qb >= 3;
    }

    private static void a(Vector vector, Hashtable hashtable, Vector vector2) {
        a(vector, hashtable, (Vector) null, vector2);
    }

    private static void a(Vector vector, Hashtable hashtable, Vector vector2, Vector vector3) {
        int i;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= vector.size() - 2) {
                break;
            }
            a aVar = (a) vector.elementAt(i3);
            int i4 = i3 + 1;
            while (true) {
                int i5 = i4;
                if (i5 < vector.size() - 1) {
                    a aVar2 = (a) vector.elementAt(i5);
                    if (aVar2.aN() != aVar.aN()) {
                        break;
                    }
                    int i6 = i5 + 1;
                    while (true) {
                        int i7 = i6;
                        if (i7 < vector.size()) {
                            a aVar3 = (a) vector.elementAt(i7);
                            if (aVar3.aN() != aVar2.aN()) {
                                break;
                            }
                            Integer a2 = a(aVar, aVar2, aVar3);
                            if (a2 != null) {
                                Vector b2 = b(vector2);
                                int i8 = 0;
                                while (true) {
                                    i = i8;
                                    if (i < b2.size() / 4) {
                                        int i9 = i * 4;
                                        if (((a) b2.elementAt(i9 + 2)).aM() + (((a) b2.elementAt(i9)).aM() * 10000) + (((a) b2.elementAt(i9 + 1)).aM() * 100) > (aVar.aM() * 10000) + (aVar2.aM() * 100) + aVar3.aM()) {
                                            break;
                                        }
                                        i8 = i + 1;
                                    } else {
                                        break;
                                    }
                                }
                                b2.insertElementAt(a2, i * 4);
                                b2.insertElementAt(aVar3, i * 4);
                                b2.insertElementAt(aVar2, i * 4);
                                b2.insertElementAt(aVar, i * 4);
                                if (hashtable.get(String.valueOf(d.d(b2, "")) + a(vector, aVar, aVar2, aVar3)) == null) {
                                    Vector b3 = b(vector);
                                    b3.removeElementAt(i7);
                                    b3.removeElementAt(i5);
                                    b3.removeElementAt(i3);
                                    hashtable.put(String.valueOf(d.d(b2, "")) + d.d(b3, ""), b3);
                                    a(b3, hashtable, b2, vector3);
                                }
                            }
                            i6 = i7 + 1;
                        } else {
                            break;
                        }
                    }
                    i4 = i5 + 1;
                } else {
                    break;
                }
            }
            i2 = i3 + 1;
        }
        if (vector.size() == 2 && ((a) vector.elementAt(0)).aM() == ((a) vector.elementAt(1)).aM()) {
            Vector vector4 = vector2 == null ? new Vector() : vector2;
            vector4.addElement(vector.elementAt(0));
            vector4.addElement(vector.elementAt(1));
            vector3.addElement(vector4);
        }
    }

    private static boolean a(Vector vector, Integer num, Hashtable hashtable) {
        for (int i = 0; i < vector.size() - 2; i++) {
            a aVar = (a) vector.elementAt(i);
            int i2 = i + 1;
            while (true) {
                int i3 = i2;
                if (i3 < vector.size() - 1) {
                    a aVar2 = (a) vector.elementAt(i3);
                    if (aVar2.aN() != aVar.aN()) {
                        continue;
                        break;
                    }
                    int i4 = i3 + 1;
                    while (true) {
                        int i5 = i4;
                        if (i5 < vector.size()) {
                            a aVar3 = (a) vector.elementAt(i5);
                            if (aVar3.aN() != aVar2.aN()) {
                                continue;
                                break;
                            }
                            if (a(aVar, aVar2, aVar3) == num) {
                                Vector b2 = b(vector);
                                b2.removeElementAt(i5);
                                b2.removeElementAt(i3);
                                b2.removeElementAt(i);
                                if (hashtable.get(d.e(b2, "")) == null) {
                                    hashtable.put(d.e(b2, ""), b2);
                                    if (a(b2, num, hashtable)) {
                                        return true;
                                    }
                                } else {
                                    continue;
                                }
                            }
                            i4 = i5 + 1;
                        } else {
                            break;
                        }
                    }
                    i2 = i3 + 1;
                } else {
                    break;
                }
            }
        }
        return vector.size() == 2 && vector.elementAt(0) == vector.elementAt(1);
    }

    public static boolean a(Vector vector, Hashtable hashtable) {
        a aVar;
        for (int i = 0; i < vector.size() - 2; i++) {
            a aVar2 = (a) vector.elementAt(i);
            int i2 = i + 1;
            while (true) {
                int i3 = i2;
                if (i3 < vector.size() - 1) {
                    a aVar3 = (a) vector.elementAt(i3);
                    if (aVar3.aN() != aVar2.aN()) {
                        continue;
                        break;
                    }
                    int i4 = i3 + 1;
                    while (true) {
                        int i5 = i4;
                        if (i5 < vector.size()) {
                            a aVar4 = (a) vector.elementAt(i5);
                            if (aVar4.aN() != aVar3.aN()) {
                                continue;
                                break;
                            }
                            if (a(aVar2, aVar3, aVar4) != null && hashtable.get(a(vector, aVar2, aVar3, aVar4)) == null) {
                                Vector b2 = b(vector);
                                b2.removeElementAt(i5);
                                b2.removeElementAt(i3);
                                b2.removeElementAt(i);
                                hashtable.put(d.e(b2, ""), b2);
                                if (a(b2, hashtable)) {
                                    return true;
                                }
                            }
                            i4 = i5 + 1;
                        } else {
                            break;
                        }
                    }
                    i2 = i3 + 1;
                } else {
                    break;
                }
            }
        }
        if (vector.size() == 5) {
            a aVar5 = (a) vector.elementAt(0);
            a aVar6 = (a) vector.elementAt(1);
            a aVar7 = (a) vector.elementAt(2);
            a aVar8 = (a) vector.elementAt(3);
            a aVar9 = (a) vector.elementAt(4);
            if (aVar6.aM() == aVar7.aM()) {
                aVar6 = aVar8;
                aVar7 = aVar5;
                aVar = aVar9;
            } else if (aVar7.aM() == aVar8.aM()) {
                aVar7 = aVar5;
                aVar = aVar9;
            } else if (aVar8.aM() == aVar9.aM()) {
                a aVar10 = aVar7;
                aVar7 = aVar5;
                aVar = aVar10;
            } else if (aVar5.aM() != aVar6.aM()) {
                return false;
            } else {
                aVar = aVar9;
                aVar6 = aVar8;
            }
            if (d.a(aVar7, aVar6) != 0) {
                return true;
            }
            if (d.a(aVar6, aVar) != 0) {
                return true;
            }
        } else if (vector.size() == 2) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a.b.a(java.util.Vector, java.util.Hashtable, java.util.Hashtable, boolean):boolean
     arg types: [java.util.Vector, java.util.Hashtable, java.util.Hashtable, int]
     candidates:
      a.b.a.b.a(java.util.Vector, a.a.a, a.a.a, a.a.a):java.lang.String
      a.b.a.b.a(a.b.d.a, a.b.c.a.b, a.a.a, a.b.c.b.a):java.util.Vector
      a.b.a.b.a(int, int, java.util.Vector, a.b.c.b.a):void
      a.b.a.b.a(java.util.Vector, java.util.Hashtable, java.util.Vector, java.util.Vector):void
      a.b.a.b.a(java.util.Vector, java.util.Hashtable, java.util.Hashtable, boolean):boolean */
    public static boolean a(Vector vector, Hashtable hashtable, Hashtable hashtable2) {
        return a(vector, hashtable, hashtable2, false);
    }

    private static boolean a(Vector vector, Hashtable hashtable, Hashtable hashtable2, boolean z) {
        a aVar;
        a aVar2;
        if (z && hashtable.size() > 0) {
            return true;
        }
        for (int i = 0; i < vector.size() - 2; i++) {
            a aVar3 = (a) vector.elementAt(i);
            int i2 = i + 1;
            while (true) {
                int i3 = i2;
                if (i3 < vector.size() - 1) {
                    a aVar4 = (a) vector.elementAt(i3);
                    if (aVar4.aN() != aVar3.aN()) {
                        continue;
                        break;
                    }
                    int i4 = i3 + 1;
                    while (true) {
                        int i5 = i4;
                        if (i5 < vector.size()) {
                            a aVar5 = (a) vector.elementAt(i5);
                            if (aVar5.aN() != aVar4.aN()) {
                                continue;
                                break;
                            }
                            if (a(aVar3, aVar4, aVar5) != null && hashtable2.get(a(vector, aVar3, aVar4, aVar5)) == null) {
                                Vector b2 = b(vector);
                                b2.removeElementAt(i5);
                                b2.removeElementAt(i3);
                                b2.removeElementAt(i);
                                if (a(b2, hashtable, hashtable2, z)) {
                                    return true;
                                }
                                hashtable2.put(d.e(b2, ""), b2);
                            }
                            i4 = i5 + 1;
                        } else {
                            break;
                        }
                    }
                    i2 = i3 + 1;
                } else {
                    break;
                }
            }
        }
        if (vector.size() == 4) {
            a aVar6 = (a) vector.elementAt(0);
            a aVar7 = (a) vector.elementAt(1);
            a aVar8 = (a) vector.elementAt(2);
            a aVar9 = (a) vector.elementAt(3);
            if ((aVar6 == null && aVar7 == null) || aVar6 == aVar7) {
                aVar2 = aVar8;
                aVar = aVar9;
            } else if (aVar8 == aVar9) {
                aVar = aVar7;
                aVar2 = aVar6;
            }
            if (aVar2 == aVar) {
                if (aVar6 != null) {
                    hashtable.put(aVar6.t(""), aVar6);
                }
                hashtable.put(aVar8.t(""), aVar8);
            } else if (d.B(aVar2.aM()) == d.B(aVar.aM()) && d.B(aVar2.aM()) < 3) {
                if (aVar2.aM() + 1 == aVar.aM()) {
                    a aVar10 = (a) a.a.c.mu.get(e.nv[aVar2.aM() - 1]);
                    if (aVar10 != null && aVar10.aN() == aVar.aN()) {
                        hashtable.put(aVar10.t(""), aVar10);
                    }
                    a aVar11 = (a) a.a.c.mu.get(e.nv[aVar.aM() + 1]);
                    if (aVar11 != null && aVar11.aN() == aVar2.aN()) {
                        hashtable.put(aVar11.t(""), aVar11);
                    }
                } else if (aVar2.aM() + 2 == aVar.aM()) {
                    a aVar12 = (a) a.a.c.mu.get(e.nv[aVar2.aM() + 1]);
                    hashtable.put(aVar12.t(""), aVar12);
                }
            }
        } else if (vector.size() == 1) {
            a aVar13 = (a) vector.elementAt(0);
            hashtable.put(aVar13.t(""), aVar13);
        } else if (vector.size() == 2 && ((a) vector.elementAt(0)).aM() == ((a) vector.elementAt(1)).aM()) {
            return true;
        }
        return false;
    }

    public static Vector b(Vector vector) {
        Vector vector2 = new Vector();
        if (vector != null) {
            for (int i = 0; i < vector.size(); i++) {
                vector2.addElement(vector.elementAt(i));
            }
        }
        return vector2;
    }

    public static boolean b(Vector vector, Hashtable hashtable) {
        if (vector.size() < 13) {
            return false;
        }
        Hashtable bu = bu();
        for (int i = 0; i < vector.size(); i++) {
            if (!bu.containsValue(vector.elementAt(i))) {
                return false;
            }
        }
        int i2 = 0;
        while (true) {
            if (i2 >= vector.size() - 1) {
                break;
            } else if (vector.elementAt(i2) != vector.elementAt(i2 + 1)) {
                i2++;
            } else if (!bu.containsValue(vector.elementAt(i2))) {
                return false;
            }
        }
        for (int i3 = 0; i3 < vector.size(); i3++) {
            bu.remove(((a) vector.elementAt(i3)).t(""));
        }
        if (bu.size() > 1) {
            return false;
        }
        if (bu.size() == 1 && vector.size() != 14) {
            Enumeration elements = bu.elements();
            while (elements.hasMoreElements()) {
                if (hashtable != null) {
                    a aVar = (a) elements.nextElement();
                    hashtable.put(aVar.t(""), aVar);
                }
            }
        } else if (bu.size() == 0) {
            if (hashtable != null) {
                hashtable.put(e.nv[1], a.a.c.mu.get(e.nv[1]));
                hashtable.put(e.nv[9], a.a.c.mu.get(e.nv[9]));
                hashtable.put(e.nv[10], a.a.c.mu.get(e.nv[10]));
                hashtable.put(e.nv[18], a.a.c.mu.get(e.nv[18]));
                hashtable.put(e.nv[19], a.a.c.mu.get(e.nv[19]));
                hashtable.put(e.nv[27], a.a.c.mu.get(e.nv[27]));
                hashtable.put(e.nv[28], a.a.c.mu.get(e.nv[28]));
                hashtable.put(e.nv[29], a.a.c.mu.get(e.nv[29]));
                hashtable.put(e.nv[30], a.a.c.mu.get(e.nv[30]));
                hashtable.put(e.nv[31], a.a.c.mu.get(e.nv[31]));
                hashtable.put(e.nv[32], a.a.c.mu.get(e.nv[32]));
                hashtable.put(e.nv[33], a.a.c.mu.get(e.nv[33]));
                hashtable.put(e.nv[34], a.a.c.mu.get(e.nv[34]));
            }
            if (vector.size() == 14) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.b.a.b.a(java.util.Vector, java.util.Hashtable, java.util.Hashtable, boolean):boolean
     arg types: [java.util.Vector, java.util.Hashtable, java.util.Hashtable, int]
     candidates:
      a.b.a.b.a(java.util.Vector, a.a.a, a.a.a, a.a.a):java.lang.String
      a.b.a.b.a(a.b.d.a, a.b.c.a.b, a.a.a, a.b.c.b.a):java.util.Vector
      a.b.a.b.a(int, int, java.util.Vector, a.b.c.b.a):void
      a.b.a.b.a(java.util.Vector, java.util.Hashtable, java.util.Vector, java.util.Vector):void
      a.b.a.b.a(java.util.Vector, java.util.Hashtable, java.util.Hashtable, boolean):boolean */
    private static boolean b(Vector vector, Hashtable hashtable, Hashtable hashtable2) {
        boolean a2 = a(vector, hashtable, hashtable2, true);
        if (hashtable.size() > 0) {
            Enumeration keys = hashtable.keys();
            while (keys.hasMoreElements()) {
                String str = (String) keys.nextElement();
                a aVar = (a) hashtable.get(str);
                int i = 0;
                for (int i2 = 0; i2 < vector.size(); i2++) {
                    if (vector.elementAt(i2) != aVar) {
                        if (i != 0) {
                            break;
                        }
                    } else {
                        i++;
                        if (i == 4) {
                            hashtable.remove(str);
                        }
                    }
                }
            }
        }
        return a2;
    }

    public static Hashtable bu() {
        if (iQ == null) {
            iQ = new Hashtable();
        } else {
            iQ.clear();
        }
        iQ.put(e.nv[1], a.a.c.mu.get(e.nv[1]));
        iQ.put(e.nv[9], a.a.c.mu.get(e.nv[9]));
        iQ.put(e.nv[10], a.a.c.mu.get(e.nv[10]));
        iQ.put(e.nv[18], a.a.c.mu.get(e.nv[18]));
        iQ.put(e.nv[19], a.a.c.mu.get(e.nv[19]));
        iQ.put(e.nv[27], a.a.c.mu.get(e.nv[27]));
        iQ.put(e.nv[28], a.a.c.mu.get(e.nv[28]));
        iQ.put(e.nv[29], a.a.c.mu.get(e.nv[29]));
        iQ.put(e.nv[30], a.a.c.mu.get(e.nv[30]));
        iQ.put(e.nv[31], a.a.c.mu.get(e.nv[31]));
        iQ.put(e.nv[32], a.a.c.mu.get(e.nv[32]));
        iQ.put(e.nv[33], a.a.c.mu.get(e.nv[33]));
        iQ.put(e.nv[34], a.a.c.mu.get(e.nv[34]));
        return iQ;
    }

    public static Vector c(Vector vector) {
        Vector vector2 = new Vector();
        for (int i = 0; i < vector.size(); i++) {
            vector2.add((a) a.a.c.mu.get(vector.elementAt(i)));
        }
        Vector d = d(vector2);
        for (int i2 = 0; i2 < d.size(); i2++) {
            d.set(i2, ((a) d.elementAt(i2)).t(""));
        }
        return d;
    }

    private static Vector d(Vector vector) {
        Hashtable hashtable = a.b.d.a.lo;
        Vector vector2 = new Vector();
        Vector vector3 = new Vector();
        Vector b2 = b(vector);
        a aVar = (a) b2.remove(b2.size() - 1);
        int i = 0;
        while (i < b2.size() && ((a) b2.elementAt(i)).aM() <= aVar.aM()) {
            i++;
        }
        b2.insertElementAt(aVar, i);
        for (int i2 = 0; i2 < b2.size(); i2++) {
            a aVar2 = (a) b2.elementAt(i2);
            if (!vector3.contains(aVar2)) {
                vector3.addElement(aVar2);
            }
        }
        Hashtable hashtable2 = new Hashtable();
        for (int i3 = 0; i3 < vector3.size(); i3++) {
            hashtable2.clear();
            hashtable.clear();
            a aVar3 = (a) vector3.elementAt(i3);
            int indexOf = b2.indexOf(aVar3);
            b2.remove(aVar3);
            b(b2, hashtable2);
            if (hashtable2.size() > 0) {
                vector2.addElement(aVar3);
            } else {
                b(b2, hashtable2, hashtable);
                c.e(b2, hashtable2);
                if (hashtable2.size() == 0) {
                    c.c(b2, hashtable2);
                    if (hashtable2.size() == 0) {
                        c.d(b2, hashtable2);
                    }
                }
                if (hashtable2.size() > 0) {
                    vector2.addElement(aVar3);
                }
            }
            b2.insertElementAt(aVar3, indexOf);
        }
        return vector2;
    }

    public static void e(Vector vector) {
        int i;
        int i2;
        for (int i3 = 0; i3 < vector.size(); i3++) {
            a.b.c.b.a aVar = (a.b.c.b.a) vector.elementAt(i3);
            int cs = aVar.cs();
            if (aVar.lK == -1) {
                i2 = (cs + 8) * 2;
                i = i2;
            } else {
                i = (cs + 8) * 2;
                i2 = 16;
            }
            int[] cw = aVar.cw();
            for (int i4 = 0; i4 < cw.length; i4++) {
                if (aVar.lJ == i4) {
                    cw[i4] = (i2 * 2) + i;
                } else if (aVar.lK == i4) {
                    cw[i4] = -i;
                } else {
                    cw[i4] = -i2;
                }
            }
        }
        if (vector.size() > 1) {
            int[] iArr = new int[4];
            for (int i5 = 0; i5 < vector.size(); i5++) {
                f fVar = (f) vector.elementAt(i5);
                for (int i6 = 0; i6 < iArr.length; i6++) {
                    iArr[i6] = iArr[i6] + fVar.cw()[i6];
                }
            }
            for (int i7 = 0; i7 < vector.size(); i7++) {
                f fVar2 = (f) vector.elementAt(i7);
                for (int i8 = 0; i8 < fVar2.cw().length; i8++) {
                    fVar2.cw()[i8] = iArr[i8];
                }
            }
        }
    }
}
