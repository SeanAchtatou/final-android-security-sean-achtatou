package a.b.c.b;

import a.a.a;
import a.a.d;
import a.b.c.a.b;
import java.io.Serializable;
import java.util.Hashtable;
import java.util.Vector;

public final class c implements Serializable {
    private static Integer yC = new Integer(2);
    private static Integer yD = new Integer(3);
    private static Integer yE = new Integer(4);
    public static final Integer yF = new Integer(6);
    public static final Integer yG = new Integer(5);
    private Vector mr = new Vector();
    private a yH;
    private Hashtable yI = new Hashtable();
    private Hashtable yJ = new Hashtable();
    private Vector yK = new Vector();
    private Vector yL = new Vector();
    private Vector yM = new Vector();
    private Vector yN = new Vector();
    private a yO;
    private boolean yP;
    public int yQ = 0;
    private Vector yR = null;

    private static int a(b bVar, b bVar2) {
        if ((bVar.ga() + 1) % 4 == bVar2.ga()) {
            return 3;
        }
        if ((bVar.ga() + 2) % 4 == bVar2.ga()) {
            return 2;
        }
        return (bVar.ga() + 3) % 4 == bVar2.ga() ? 1 : 0;
    }

    private static boolean a(a aVar, Vector vector) {
        int i;
        int i2 = 0;
        int i3 = 0;
        while (i2 < vector.size()) {
            if (!(vector.elementAt(i2) instanceof a) || aVar.aM() != ((a) vector.elementAt(i2)).aM()) {
                i = 0;
            } else {
                i = i3 + 1;
                if (i == 3) {
                    return true;
                }
            }
            i2++;
            i3 = i;
        }
        return false;
    }

    public final boolean a(a aVar) {
        int aM = aVar.aM();
        if (aVar.aM() >= 35) {
            int i = 0;
            while (i < this.yL.size() && aM >= ((a) this.yL.elementAt(i)).aM()) {
                i++;
            }
            this.yL.insertElementAt(aVar, i);
            return false;
        }
        int i2 = 0;
        while (i2 < this.mr.size() && ((a) this.mr.elementAt(i2)).aM() <= aVar.aM()) {
            i2++;
        }
        this.mr.insertElementAt(aVar, i2);
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d8 A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(a.a.a r14, int r15) {
        /*
            r13 = this;
            a.a.a r0 = r13.yH
            if (r0 == 0) goto L_0x0006
            r0 = 0
        L_0x0005:
            return r0
        L_0x0006:
            java.util.Vector r1 = r13.mr
            java.util.Vector r2 = r13.yM
            r0 = 1
            if (r15 != r0) goto L_0x0088
            int r0 = r14.aM()
            r3 = 2
            int r0 = r0 - r3
            int r3 = r14.aM()
            r4 = 1
            int r3 = r3 - r4
            r4 = r0
        L_0x001a:
            r0 = 0
            r5 = 0
            r6 = -1
            r7 = -1
            r8 = 0
            r9 = r0
            r11 = r7
            r7 = r6
            r6 = r11
            r12 = r5
            r5 = r8
            r8 = r12
        L_0x0026:
            int r0 = r1.size()
            if (r5 < r0) goto L_0x00ad
            r0 = -1
            if (r7 == r0) goto L_0x00d6
            r0 = -1
            if (r6 == r0) goto L_0x00d6
            r0 = 3
            if (r15 != r0) goto L_0x0038
            r2.addElement(r14)
        L_0x0038:
            java.lang.Object r0 = r1.elementAt(r7)
            r2.addElement(r0)
            r0 = 2
            if (r15 != r0) goto L_0x0045
            r2.addElement(r14)
        L_0x0045:
            java.lang.Object r0 = r1.elementAt(r6)
            r2.addElement(r0)
            r0 = 1
            if (r15 != r0) goto L_0x0052
            r2.addElement(r14)
        L_0x0052:
            java.lang.Integer r0 = a.b.c.b.c.yF
            r2.addElement(r0)
            r1.removeElementAt(r6)
            r1.removeElementAt(r7)
            r0 = 1
        L_0x005e:
            if (r0 == 0) goto L_0x00d8
            r0 = 0
            r13.yO = r0
            java.util.Vector r0 = r13.mr
            int r0 = r0.size()
            r1 = 1
            int r0 = r0 - r1
            java.util.Vector r1 = r13.mr
            java.lang.Object r15 = r1.elementAt(r0)
            a.a.a r15 = (a.a.a) r15
            r13.yH = r15
            java.util.Vector r1 = r13.mr
            r1.removeElementAt(r0)
            java.util.Vector r0 = r13.yN
            a.b.c.b.b r1 = new a.b.c.b.b
            r2 = 0
            r1.<init>(r2, r14)
            r0.add(r1)
            r0 = 1
            goto L_0x0005
        L_0x0088:
            r0 = 2
            if (r15 != r0) goto L_0x0099
            int r0 = r14.aM()
            r3 = 1
            int r0 = r0 - r3
            int r3 = r14.aM()
            int r3 = r3 + 1
            r4 = r0
            goto L_0x001a
        L_0x0099:
            r0 = 3
            if (r15 != r0) goto L_0x00ab
            int r0 = r14.aM()
            int r0 = r0 + 1
            int r3 = r14.aM()
            int r3 = r3 + 2
            r4 = r0
            goto L_0x001a
        L_0x00ab:
            r0 = 0
            goto L_0x005e
        L_0x00ad:
            java.lang.Object r0 = r1.elementAt(r5)
            a.a.a r0 = (a.a.a) r0
            if (r9 != 0) goto L_0x00c8
            int r10 = r0.aM()
            if (r10 != r4) goto L_0x00c8
            r0 = 1
            r7 = r8
            r8 = r0
            r0 = r6
            r6 = r5
        L_0x00c0:
            int r5 = r5 + 1
            r9 = r8
            r8 = r7
            r7 = r6
            r6 = r0
            goto L_0x0026
        L_0x00c8:
            if (r8 != 0) goto L_0x00db
            int r0 = r0.aM()
            if (r0 != r3) goto L_0x00db
            r0 = 1
            r6 = r7
            r8 = r9
            r7 = r0
            r0 = r5
            goto L_0x00c0
        L_0x00d6:
            r0 = 0
            goto L_0x005e
        L_0x00d8:
            r0 = 0
            goto L_0x0005
        L_0x00db:
            r0 = r6
            r6 = r7
            r7 = r8
            r8 = r9
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: a.b.c.b.c.a(a.a.a, int):boolean");
    }

    public final boolean a(b bVar, b bVar2, a aVar) {
        int i;
        int i2;
        boolean z;
        if (this.yH != null) {
            return false;
        }
        Vector vector = this.mr;
        Vector vector2 = this.yM;
        int i3 = 0;
        int i4 = -1;
        int i5 = -1;
        while (i3 < vector.size()) {
            if (aVar.aM() == ((a) vector.elementAt(i3)).aM()) {
                if (i5 >= 0) {
                    if (i4 >= 0) {
                        break;
                    }
                    i = i3;
                    i2 = i5;
                } else {
                    i = i4;
                    i2 = i3;
                }
            } else {
                i = i4;
                i2 = i5;
            }
            i3++;
            i5 = i2;
            i4 = i;
        }
        if (i5 == -1 || i4 == -1) {
            z = false;
        } else {
            vector2.addElement(vector.elementAt(i5));
            vector2.addElement(vector.elementAt(i4));
            vector2.addElement(aVar);
            vector2.addElement(yG);
            vector.removeElementAt(i4);
            vector.removeElementAt(i5);
            z = true;
        }
        if (!z) {
            return false;
        }
        this.yO = null;
        int size = this.mr.size() - 1;
        this.yH = (a) this.mr.elementAt(size);
        this.mr.removeElementAt(size);
        this.yN.add(new b(a(bVar, bVar2), aVar));
        return true;
    }

    public final a aQ(String str) {
        a aVar;
        int i = 0;
        while (true) {
            if (i < this.mr.size()) {
                if (((a) this.mr.elementAt(i)).t("").equals(str)) {
                    break;
                }
                i++;
            } else {
                i = -1;
                break;
            }
        }
        if (i == -1) {
            aVar = (this.yH == null || !this.yH.t("").equals(str)) ? null : this.yH;
        } else {
            aVar = (a) this.mr.elementAt(i);
            this.mr.removeElementAt(i);
            a(this.yH);
        }
        if (this.yO == null || !this.yO.t("").equals(str)) {
            fB();
            this.yO = null;
        }
        this.yH = null;
        this.yP = false;
        return aVar;
    }

    public final String aR(String str) {
        return String.valueOf(d.d(this.mr, str)) + (this.yH == null ? "" : this.yH.t(str));
    }

    public final String aS(String str) {
        Vector vector = this.yM;
        String str2 = "";
        int size = vector.size() - 1;
        while (size >= 0) {
            Object elementAt = vector.elementAt(size);
            if ((elementAt instanceof Integer) && ((Integer) elementAt).intValue() == 3) {
                size--;
                str2 = String.valueOf(d.a((a) vector.elementAt(size), str)) + str2;
            }
            size--;
        }
        return str2;
    }

    public final String aT(String str) {
        return d.e(this.yL, str);
    }

    public final int b(b bVar, b bVar2, a aVar) {
        int i;
        int i2;
        int i3;
        int i4;
        Vector vector = this.mr;
        Vector vector2 = this.yM;
        int i5 = -1;
        int i6 = -1;
        int i7 = 0;
        int i8 = -1;
        while (i7 < vector.size()) {
            if (aVar.aM() == ((a) vector.elementAt(i7)).aM()) {
                if (i6 >= 0) {
                    if (i8 >= 0) {
                        if (i5 >= 0) {
                            break;
                        }
                        i = i7;
                        i2 = i8;
                        i3 = i6;
                    } else {
                        i = i5;
                        i3 = i6;
                        i2 = i7;
                    }
                } else {
                    i = i5;
                    i2 = i8;
                    i3 = i7;
                }
            } else {
                i = i5;
                i2 = i8;
                i3 = i6;
            }
            i7++;
            i6 = i3;
            i8 = i2;
            i5 = i;
        }
        if (i6 == -1 || i8 == -1 || i5 == -1) {
            int i9 = 0;
            int i10 = 0;
            while (true) {
                int i11 = i10;
                int i12 = i9;
                if (i11 >= vector2.size()) {
                    i4 = 0;
                    break;
                }
                if (vector2.elementAt(i11) instanceof a) {
                    i9 = ((a) vector2.elementAt(i11)).aM() == aVar.aM() ? i12 + 1 : i12;
                } else if (i12 == 3) {
                    vector2.removeElementAt(i11);
                    vector2.insertElementAt(yE, i11);
                    vector2.insertElementAt(aVar, i11);
                    i4 = 4;
                    break;
                } else {
                    i9 = 0;
                }
                i10 = i11 + 1;
            }
        } else {
            vector2.addElement(vector.elementAt(i6));
            vector2.addElement(vector.elementAt(i8));
            vector2.addElement(vector.elementAt(i5));
            vector2.addElement(aVar);
            vector2.addElement(yC);
            vector.removeElementAt(i5);
            vector.removeElementAt(i8);
            vector.removeElementAt(i6);
            i4 = 2;
        }
        if (i4 != 0) {
            this.yO = null;
            if (aQ(aVar.t("")) == null) {
                this.yP = false;
                fB();
            } else if (i4 == 2) {
                i4 = 3;
            }
            switch (i4) {
                case 2:
                    this.yM.removeElementAt(this.yM.size() - 1);
                    this.yM.addElement(yC);
                    this.yN.add(new b(a(bVar, bVar2), aVar));
                    break;
                case 3:
                    this.yM.removeElementAt(this.yM.size() - 1);
                    this.yM.addElement(yD);
                    this.yN.add(new b(a(bVar, bVar2), aVar));
                    break;
            }
        }
        return i4;
    }

    public final String b(String str, boolean z) {
        if (!z) {
            return d.e(this.yM, str);
        }
        Vector vector = this.yM;
        String str2 = "";
        Integer num = null;
        for (int size = vector.size() - 1; size >= 0; size--) {
            Object elementAt = vector.elementAt(size);
            if (elementAt instanceof Integer) {
                str2 = "-" + str + str2;
                num = (Integer) elementAt;
            } else {
                str2 = num.intValue() == 3 ? "BK" + str + str2 : String.valueOf(d.a((a) elementAt, String.valueOf("") + str)) + str2;
            }
        }
        return str2;
    }

    public final boolean b(a aVar) {
        int aM = aVar.aM();
        if (aM >= 35) {
            int i = 0;
            while (i < this.yL.size() && aM >= ((a) this.yL.elementAt(i)).aM()) {
                i++;
            }
            this.yL.insertElementAt(aVar, i);
            return false;
        }
        this.yO = aVar;
        this.yH = aVar;
        return true;
    }

    public final void bC() {
        this.yO = null;
        this.yH = null;
        this.mr.removeAllElements();
        this.yI.clear();
        this.yJ.clear();
        this.yK.removeAllElements();
        this.yL.removeAllElements();
        this.yM.removeAllElements();
        this.yN.removeAllElements();
        this.yP = true;
        this.yQ = 0;
        this.yR = null;
    }

    public final boolean c(a aVar) {
        Vector vector = this.mr;
        int i = 0;
        for (int i2 = 0; i2 < vector.size(); i2++) {
            if (aVar.aM() == ((a) vector.elementAt(i2)).aM()) {
                i++;
            }
        }
        return i >= 2;
    }

    public final boolean d(a aVar) {
        return a(aVar, this.mr);
    }

    public final int e(a aVar) {
        Vector vector = this.mr;
        if (aVar.aN() < 3) {
            int aM = aVar.aM() - 2;
            int aM2 = aVar.aM() - 1;
            int aM3 = aVar.aM() + 1;
            int aM4 = aVar.aM() + 2;
            if (d.B(aM) != aVar.aN()) {
                aM = 0;
            }
            if (d.B(aM2) != aVar.aN()) {
                aM2 = 0;
            }
            if (d.B(aM3) != aVar.aN()) {
                aM3 = 0;
            }
            if (d.B(aM4) != aVar.aN()) {
                aM4 = 0;
            }
            boolean z = false;
            boolean z2 = false;
            boolean z3 = false;
            boolean z4 = false;
            for (int i = 0; i < vector.size(); i++) {
                a aVar2 = (a) vector.elementAt(i);
                if (aM == aVar2.aM()) {
                    z4 = true;
                } else if (aM2 == aVar2.aM()) {
                    z3 = true;
                } else if (aM3 == aVar2.aM()) {
                    z2 = true;
                } else if (aM4 == aVar2.aM()) {
                    z = true;
                }
            }
            boolean z5 = z4 && z3;
            boolean z6 = z3 && z2;
            boolean z7 = z2 && z;
            if (z5 && z6 && z7) {
                return 15;
            }
            if (z5 && z6) {
                return 11;
            }
            if (z6 && z7) {
                return 13;
            }
            if (z5) {
                return 10;
            }
            if (z6) {
                return 12;
            }
            if (z7) {
                return 14;
            }
        }
        return 0;
    }

    public final boolean f(a aVar) {
        if (aVar == null) {
            return false;
        }
        return this.yI.containsValue(aVar);
    }

    public final int fA() {
        return this.mr.size() + (this.yH == null ? 0 : 1);
    }

    public final void fB() {
        this.yI.clear();
        a.b.a.b.b(this.mr, this.yI);
        if (this.yI.size() == 0) {
            a.b.d.a.lo.clear();
            a.b.a.b.a(this.mr, this.yI, a.b.d.a.lo);
            a.b.a.c.e(this.mr, this.yI);
            if (this.yI.size() == 0) {
                a.b.a.c.c(this.mr, this.yI);
                if (this.yI.size() == 0) {
                    a.b.a.c.d(this.mr, this.yI);
                    this.yI.size();
                }
            }
        }
    }

    public final Vector fC() {
        return this.yK;
    }

    public final String fD() {
        return d.e(this.yK, ".");
    }

    public final Hashtable fE() {
        return this.yI;
    }

    public final Hashtable fF() {
        return this.yJ;
    }

    public final boolean fG() {
        return f(this.yH);
    }

    public final Vector fH() {
        return this.yR;
    }

    public final Vector fw() {
        return this.mr;
    }

    public final Vector fx() {
        return this.yM;
    }

    public final Vector fy() {
        return this.yL;
    }

    public final a fz() {
        return this.yH;
    }

    public final void i(Vector vector) {
        if (this.yH != null) {
            for (int i = 0; i < this.mr.size(); i++) {
                a aVar = (a) this.mr.elementAt(i);
                if (a(aVar, this.yM)) {
                    vector.addElement(aVar);
                }
            }
            a aVar2 = (a) this.mr.elementAt(0);
            int i2 = 1;
            int i3 = 1;
            while (true) {
                a aVar3 = aVar2;
                if (i2 >= this.mr.size()) {
                    break;
                }
                if (((a) this.mr.elementAt(i2)).aM() == aVar3.aM()) {
                    int i4 = i3 + 1;
                    if (i4 == 4) {
                        vector.addElement(aVar3);
                        i3 = i4;
                    } else {
                        i3 = i4;
                    }
                } else {
                    i3 = 1;
                }
                aVar2 = (a) this.mr.elementAt(i2);
                i2++;
            }
            if (a(this.yH, this.yM) || a(this.yH, this.mr)) {
                vector.addElement(this.yH);
            }
        }
    }

    public final void j(Vector vector) {
        this.yR = vector;
    }
}
