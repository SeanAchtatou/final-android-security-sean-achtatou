package a.b.c.b;

import a.a.f;
import a.a.g;
import a.b.c.a.b;
import java.io.Serializable;
import java.util.Vector;

public final class a implements f, Serializable, Cloneable {
    public boolean eI = false;
    public boolean eJ = false;
    private int ge = -1;
    public int lJ;
    public int lK;
    public int lL;
    public boolean lQ = false;
    public boolean pA = false;
    public boolean pB = false;
    public boolean pC = false;
    public boolean pD = false;
    public boolean pE = false;
    public boolean pF = false;
    public boolean pG = false;
    private boolean pH = false;
    public boolean pI = false;
    public boolean pJ = false;
    public boolean pK = false;
    public boolean pL = false;
    public int pM = 0;
    public boolean pN = false;
    public boolean pO = false;
    public boolean pP = false;
    public boolean pQ = false;
    public boolean pR = false;
    public int pS;
    public int pT;
    public int pU;
    public int pV;
    public int pW;
    public int pX;
    public int pY;
    public int pZ;
    public boolean pn = false;
    public boolean po = false;
    public boolean pp = false;
    public boolean pq = false;
    public boolean pr = false;
    public boolean ps = false;
    public boolean pt = false;
    public boolean pu = false;
    public boolean pv = false;
    public boolean pw = false;
    public boolean px = false;
    public boolean py = false;
    public boolean pz = false;
    public int qa;
    public int qb;
    public int[] qc = new int[4];
    private int[] qd = new int[4];
    private Vector qe = new Vector();
    private a.b.a.a qf = null;
    public a.b.a.a[] qg = null;
    public Vector qh = null;
    public int qi = -1;

    private int a(int i, int i2, String str, boolean z, int i3) {
        int i4;
        int i5 = 0;
        while (true) {
            i4 = i5;
            if (i4 < this.qe.size() && ((g) this.qe.elementAt(i4)).nW >= i2) {
                i5 = i4 + 1;
            }
        }
        this.qe.insertElementAt(new g(i, i2, str, z, i3), i4);
        return i;
    }

    public final int a(int i, int i2, String str) {
        return a(i, i2, str, false, 1);
    }

    public final int a(int i, String str, int i2) {
        return a(i, i, str, false, i2);
    }

    public final void a(a.b.a.a aVar) {
        this.qf = aVar;
    }

    public final void a(a.b.d.a aVar, b bVar, a.a.a aVar2) {
        bC();
        this.lL = bVar.ga();
        a.b.a.b.a(aVar, bVar, aVar2, this);
    }

    public final Vector aq(String str) {
        if (this.qi == -1) {
            return null;
        }
        Vector vector = (Vector) ((Vector) this.qh.elementAt(this.qi)).clone();
        for (int size = vector.size() - 1; size >= 0; size--) {
            if (!(vector.elementAt(size) instanceof a.a.a)) {
                vector.removeElementAt(size);
            }
        }
        for (int size2 = vector.size() - 1; size2 >= 0; size2--) {
            if (str.equals(((a.a.a) vector.elementAt(size2)).t(""))) {
                vector.addElement((a.a.a) vector.remove(size2));
                return vector;
            }
        }
        return vector;
    }

    public final int b(int i, int i2, String str) {
        return a(i, i2, str, true, 1);
    }

    public final void bC() {
        this.qf = null;
        this.qg = null;
        this.qh = null;
        this.qi = -1;
        this.eJ = false;
        this.pn = false;
        this.po = false;
        this.pp = false;
        this.pq = false;
        this.pr = false;
        this.ps = false;
        this.pt = false;
        this.pu = false;
        this.pv = false;
        this.eI = false;
        this.pw = false;
        this.px = false;
        this.py = false;
        this.lQ = false;
        this.pz = false;
        this.pA = false;
        this.pB = false;
        this.pC = false;
        this.pD = false;
        this.pE = false;
        this.pF = false;
        this.pG = false;
        this.pH = false;
        this.pI = false;
        this.pJ = false;
        this.pK = false;
        this.pL = false;
        this.pM = 0;
        this.pN = false;
        this.pO = false;
        this.pP = false;
        this.pQ = false;
        this.pR = false;
        this.pY = 0;
        this.pX = 0;
        this.pW = 0;
        this.pV = 0;
        this.pU = 0;
        this.pT = 0;
        this.pS = 0;
        this.qb = 0;
        this.qa = 0;
        this.pZ = 0;
        this.qc[0] = 0;
        this.qc[1] = 0;
        this.qc[2] = 0;
        this.qc[3] = 0;
        this.lK = -1;
        this.lJ = -1;
        this.lL = -1;
        for (int i = 0; i < this.qd.length; i++) {
            this.qd[i] = 0;
        }
        this.ge = -1;
    }

    public final Vector cJ() {
        return this.qe;
    }

    /* renamed from: cK */
    public final a clone() {
        a aVar = (a) super.clone();
        aVar.qc = new int[this.qc.length];
        for (int i = 0; i < this.qc.length; i++) {
            aVar.qc[i] = this.qc[i];
        }
        aVar.qd = new int[this.qd.length];
        for (int i2 = 0; i2 < this.qd.length; i2++) {
            aVar.qd[i2] = this.qd[i2];
        }
        aVar.qe = new Vector();
        return aVar;
    }

    public final a.b.a.a cL() {
        return this.qf;
    }

    public final int cs() {
        if (this.ge != -1) {
            return this.ge;
        }
        this.qe.clear();
        a.b.a.a aVar = this.qf;
        if (this.pr) {
            this.pG = false;
            this.pF = false;
            aVar.eJ = false;
            aVar.eK = 0;
        } else if (this.pR) {
            this.pY = 14;
            aVar.eK = 0;
            if (!this.pw) {
                this.pz = false;
            }
        } else if (this.pu) {
            aVar.eJ = false;
            aVar.fi = false;
        } else if (aVar.fc == 3 || aVar.fc == 2) {
            if (aVar.fc == 3) {
                this.pY = 14;
                this.pn = false;
            }
            aVar.eI = false;
            if (!this.pw) {
                this.pz = false;
            }
            aVar.eq = 0;
            aVar.eW = 0;
            aVar.fl = 0;
            aVar.eD = 0;
            aVar.fi = false;
            aVar.fh = false;
            aVar.fg = false;
        } else if (this.pt) {
            this.pY = 14;
            if (!this.pw) {
                this.pz = false;
            }
        } else if (this.pB) {
            this.pY = 14;
            aVar.eJ = false;
            aVar.eK = 0;
        } else if (this.ps) {
            aVar.eL = 0;
            aVar.eK -= 3;
        } else if (this.pq) {
            this.pK = false;
            this.pJ = false;
            this.pI = false;
        }
        if (this.pv) {
            aVar.eJ = false;
            aVar.eK = 0;
        }
        if (aVar.ey + aVar.ex == 4) {
            aVar.eJ = false;
            if (!this.pw) {
                this.pz = false;
            }
        } else if (aVar.eX) {
            this.pY = 14;
            this.pn = false;
            aVar.eI = false;
            aVar.fl = 0;
            aVar.eq = 0;
            aVar.fc = 0;
        } else if (aVar.ep == 4) {
            aVar.eq = 0;
            aVar.ez = 0;
        } else if (aVar.er == 4) {
            aVar.ep = 0;
            aVar.eJ = false;
        } else if (aVar.et == 4) {
            aVar.eW = 0;
            aVar.fl = 0;
        }
        if (this.pA) {
            aVar.eJ = false;
            aVar.eK = 0;
        }
        if (aVar.fb >= 2 && !this.pw) {
            this.pz = false;
        }
        if (aVar.eV) {
            this.pY = 14;
            aVar.eJ = false;
            aVar.fk = 0;
        }
        if (this.pn) {
            this.pY = 14;
        }
        if (aVar.ep == 3) {
            aVar.eq = 0;
            aVar.er = 0;
        }
        if (aVar.er == 3) {
            aVar.eq = 0;
            aVar.ep = 0;
        }
        if (aVar.eO) {
            this.pY = 14;
        } else if (aVar.eN) {
            this.pY = 14;
            aVar.fk = 0;
        } else if (aVar.eM) {
            this.pY = 14;
        }
        if (aVar.eY) {
            this.pY = 14;
            aVar.eI = false;
            aVar.fl = 0;
            aVar.eD = 0;
        } else if (aVar.eR) {
            this.pY = 14;
            aVar.fk = 0;
        }
        if (aVar.eP || aVar.eQ) {
            this.pY = 14;
        }
        if (aVar.eS) {
            aVar.fa = false;
        }
        if (aVar.eE) {
            aVar.fi = false;
        }
        if (aVar.eL == 3) {
            aVar.eK -= 3;
        }
        if (aVar.eI) {
            this.pY = 14;
        }
        if (this.pF || this.pG) {
            aVar.eK--;
        }
        int a2 = this.qf.a(this) + 0;
        if (this.pt) {
            a2 += a(88, 48, "十三么");
        } else {
            if (this.pu) {
                a2 += a(88, 99, "四槓");
            }
            if (this.pR) {
                a2 += e(88, "九蓮寶燈");
            }
            if (this.pQ) {
                a2 += e(88, "綠一色");
            }
            if (this.pr) {
                a2 += e(88, "四風會");
            }
            if (this.pp) {
                a2 += a(88, 32, "大三元");
            }
            if (this.pB) {
                a2 += e(64, "清么九");
            }
            if (this.ps) {
                a2 += e(64, "小四風會");
            }
            if (this.pq) {
                a2 += a(64, 24, "小三元");
            }
            if (this.pv) {
                a2 += e(64, "字一色");
            }
            if (this.pA) {
                a2 += e(32, "混么九");
            }
            if (this.pn) {
                a2 += e(24, "清一色");
            }
            if (this.lQ) {
                a2 += e(8, "搶槓");
            }
            if (this.po) {
                a2 += e(6, "混一色");
            }
            if (!this.pp && !this.pq) {
                int i = (this.pI ? 1 : 0) + (this.pJ ? 1 : 0) + (this.pK ? 1 : 0);
                if (i == 2) {
                    a2 += e(6, "雙箭刻");
                } else if (i == 1) {
                    a2 += e(2, "箭刻");
                }
            }
            if (this.pF) {
                a2 += e(2, "圈風刻");
            }
            if (this.pG) {
                a2 += e(2, "門風刻");
            }
            if (this.px) {
                a2 += e(8, "槓上開花");
            }
        }
        if (this.pz) {
            a2 = this.pw ? a2 + e(4, "不求人") : a2 + e(2, "門前清");
        } else if (this.pw && !this.py) {
            a2 += e(1, "自摸");
        }
        if (this.py && this.pw) {
            a2 += e(8, "妙手回春");
        } else if (this.py) {
            a2 += e(8, "海底撈月");
        }
        if (a2 - this.qf.ff == 0) {
            a2 += e(8, "無番和");
        }
        this.ge = a2;
        return a2;
    }

    public final int ct() {
        return this.lJ;
    }

    public final int cu() {
        return this.lK;
    }

    public final int cv() {
        return this.lL;
    }

    public final int[] cw() {
        return this.qd;
    }

    public final int e(int i, String str) {
        return a(i, i, str, false, 1);
    }
}
