package a.b.d;

import a.a.c;
import a.a.d;
import a.a.e;
import a.b.c.a.b;
import java.io.Serializable;
import java.util.Hashtable;
import java.util.Vector;

public final class a implements Serializable {
    public static Hashtable lo = new Hashtable();
    private boolean kR = true;
    private int kS;
    private Vector kT = new Vector();
    private Vector kU = new Vector();
    private Vector kV = new Vector();
    private Vector kW;
    private b[] kX = new b[4];
    private int kY;
    private int kZ;
    private int la;
    private int lb;
    private int lc;
    private int ld;
    private boolean le = true;
    private boolean lf = false;
    private Vector lg = new Vector();
    private int lh;
    private int li;
    private a.a.a lj;
    private a.b.c.b.a lk = new a.b.c.b.a();
    private Vector ll = new Vector();
    private int[] lm = new int[35];
    private a.b.c.b.a ln = new a.b.c.b.a();
    private transient a.a.b lp = null;
    public Hashtable lq = new Hashtable();

    public a() {
        p(0);
    }

    public a(int i) {
        p(i);
    }

    private void a(b bVar, boolean z, a.a.a aVar, boolean z2, int i, boolean z3) {
        a.b.c.a.a aVar2 = null;
        if (this.lg.size() > 0) {
            aVar2 = (a.b.c.a.a) this.lg.elementAt(this.lg.size() - 1);
        }
        if (aVar2 == null || aVar2.kr != bVar) {
            aVar2 = new a.b.c.a.a();
            aVar2.kr = bVar;
            this.lg.addElement(aVar2);
        }
        if (z) {
            aVar2.ks = z;
        }
        if (aVar != null) {
            aVar2.kt.addElement(aVar);
        }
        if (z2) {
            aVar2.ku = true;
        }
        if (i != 0) {
            aVar2.kv = i;
        }
        if (z3) {
            aVar2.kw = z3;
        }
    }

    private void cf() {
        for (b gb : this.kX) {
            gb.gb().j(null);
        }
    }

    private boolean k(boolean z) {
        if (!this.le) {
            return false;
        }
        this.kT.removeAllElements();
        this.kU.removeAllElements();
        synchronized (c.mv) {
            for (int i = 0; i < c.mv.length; i++) {
                int random = (int) (Math.random() * ((double) c.mv.length));
                a.a.a aVar = c.mv[random];
                c.mv[random] = c.mv[i];
                c.mv[i] = aVar;
            }
            for (a.a.a addElement : c.mv) {
                this.kT.addElement(addElement);
            }
        }
        if (!z) {
            int i2 = (this.lb + 1) % 4;
            this.lb = i2;
            this.ld = i2;
            if (this.lc == this.lb) {
                this.kY = (this.kY + 1) % 4;
                this.kZ++;
            }
        }
        this.kX[this.lb].K(0);
        this.kX[(this.lb + 1) % 4].K(1);
        this.kX[(this.lb + 2) % 4].K(2);
        this.kX[(this.lb + 3) % 4].K(3);
        for (b gb : this.kX) {
            gb.gb().bC();
        }
        for (int i3 = 0; i3 < this.lm.length; i3++) {
            this.lm[i3] = 0;
        }
        this.lg.removeAllElements();
        for (int i4 = 0; i4 < this.kX.length; i4++) {
            int i5 = (this.lb + i4) % 4;
            this.kX[i5].lM = -1;
            do {
                this.kX[i5].gb().a((a.a.a) this.kT.elementAt(0));
                this.kT.removeElementAt(0);
            } while (this.kX[i5].gb().fA() < 13);
            this.kX[i5].gb().fB();
        }
        a(this.kX[this.lb], false, null, false, 0, true);
        this.li = this.lb;
        this.la++;
        this.le = false;
        this.lf = false;
        this.lh = 0;
        this.lj = (a.a.a) c.mu.get(e.nv[0]);
        this.lk.bC();
        this.ll.removeAllElements();
        this.kV.removeAllElements();
        this.kW = this.kX[this.lb].gb().fC();
        return true;
    }

    private void p(int i) {
        this.kS = i;
        this.lq.put("AllCardHash", c.mu);
        this.lq.put("AllCards", c.mv);
        this.kX[0] = new b(0, "@Alice");
        this.kX[1] = new b(1, "@Brian");
        this.kX[2] = new b(2, "@Chris");
        this.kX[3] = new b(3, "@Doris");
        bK();
    }

    public final boolean O() {
        return k(false);
    }

    public final String S(String str) {
        return d.e(this.kV, str);
    }

    public final boolean b(int i, String str) {
        a.a.a aQ;
        int e;
        a.b.c.a.a aVar = (a.b.c.a.a) this.lg.elementAt(0);
        if (this.le || i != aVar.kr.ga() || aVar.kr.gb().fz() == null) {
            return false;
        }
        if ((aVar.kr.gb().yQ != 0 && !aVar.kr.gb().fz().t("").equals(str)) || (aQ = aVar.kr.gb().aQ(str)) == null) {
            return false;
        }
        cf();
        this.lg.removeAllElements();
        this.lk.px = false;
        this.lk.pC = false;
        this.kW = this.kX[i].gb().fC();
        this.kW.addElement(aQ);
        this.li = i;
        this.lj = aQ;
        this.kV.addElement(aQ);
        int[] iArr = this.lm;
        int aM = aQ.aM();
        iArr[aM] = iArr[aM] + 1;
        this.lh = 0;
        int i2 = 1;
        while (true) {
            int i3 = i2;
            if (i3 >= 4) {
                break;
            }
            int i4 = (i + i3) % 4;
            if (this.kX[i4].gb().f(aQ)) {
                this.lk.a(this, this.kX[i4], aQ);
                if (this.lk.cs() - this.kX[i4].gb().fy().size() >= this.kS) {
                    a(this.kX[i4], true, null, false, 0, false);
                } else {
                    this.kX[i4].gb().j((Vector) this.lk.cJ().clone());
                }
            }
            i2 = i3 + 1;
        }
        int i5 = (i + 1) % 4;
        if (this.kT.size() > 0) {
            int i6 = 1;
            while (true) {
                int i7 = i6;
                if (i7 >= 4) {
                    break;
                }
                int i8 = (i + i7) % 4;
                if (this.kX[i8].gb().yQ == 0) {
                    if (this.kX[i8].gb().d(aQ)) {
                        a(this.kX[i8], false, aQ, true, 0, false);
                    } else if (this.kX[i8].gb().c(aQ)) {
                        a(this.kX[i8], false, null, true, 0, false);
                    }
                }
                i6 = i7 + 1;
            }
            if (this.kX[i5].gb().yQ == 0 && (e = this.kX[i5].gb().e(aQ)) != 0) {
                a(this.kX[i5], false, null, false, e, false);
            }
        } else if (this.lg.size() == 0) {
            this.le = true;
        }
        a(this.kX[i5], false, null, false, 0, this.kT.size() > 0);
        return true;
    }

    public final int bJ() {
        this.ln.bC();
        lo.clear();
        return a.b.b.a.a(this, this.ln, lo, this.lp);
    }

    public final void bK() {
        int random = (int) (Math.random() * 4.0d);
        this.lb = random;
        this.lc = random;
        this.ld = random;
        this.kZ = 1;
        this.kY = 0;
        k(true);
        this.la = 1;
    }

    public final a.b.c.b.a bL() {
        return this.lk;
    }

    public final Vector bM() {
        return this.ll;
    }

    public final b[] bN() {
        return this.kX;
    }

    public final Vector bO() {
        return this.kV;
    }

    public final a.b.c.a.a bP() {
        return (a.b.c.a.a) this.lg.elementAt(0);
    }

    public final int bQ() {
        return this.lg.size();
    }

    public final int bR() {
        return this.la;
    }

    public final int bS() {
        return this.kY;
    }

    public final int bT() {
        return this.kZ;
    }

    public final int bU() {
        return this.lc;
    }

    public final int bV() {
        return this.lb;
    }

    public final Vector bW() {
        return this.kT;
    }

    public final int bX() {
        return this.kT.size();
    }

    public final int bY() {
        return this.lh;
    }

    public final int bZ() {
        return this.li;
    }

    public final boolean c(int i, String str) {
        String str2;
        a.a.a aVar;
        a.b.c.a.a aVar2 = (a.b.c.a.a) this.lg.elementAt(0);
        if (this.le || i != aVar2.kr.ga()) {
            return false;
        }
        if (aVar2.kt.size() == 0) {
            return false;
        }
        if (aVar2.kr.gb().fz() != null) {
            str2 = str;
        } else if (this.kW.size() == 0) {
            return false;
        } else {
            str2 = ((a.a.a) this.kW.elementAt(this.kW.size() - 1)).t("");
        }
        int i2 = 0;
        while (true) {
            if (i2 >= aVar2.kt.size()) {
                aVar = null;
                break;
            } else if (((a.a.a) aVar2.kt.elementAt(i2)).t("").equals(str2)) {
                aVar = (a.a.a) aVar2.kt.elementAt(i2);
                break;
            } else {
                i2++;
            }
        }
        if (aVar == null) {
            return false;
        }
        int b2 = aVar2.kr.gb().b(aVar2.kr, this.kX[this.li], aVar);
        if (b2 == 0) {
            return false;
        }
        cf();
        aVar2.bC();
        this.lm[aVar.aM()] = 4;
        if (b2 == 2) {
            if (aVar2.kr.gb().fA() == 1 && aVar2.kr.lM == -1) {
                aVar2.kr.lM = this.li;
            }
            this.kW.removeElementAt(this.kW.size() - 1);
            this.kV.removeElementAt(this.kV.size() - 1);
        }
        this.lh = b2;
        this.li = i;
        this.lj = aVar;
        this.lg.removeAllElements();
        if (b2 != 2) {
            for (int i3 = 1; i3 < 4; i3++) {
                int i4 = (i + i3) % 4;
                if (this.kX[i4].gb().f(aVar)) {
                    this.lk.a(this, this.kX[i4], aVar);
                    if (b2 == 4) {
                        this.lk.lQ = true;
                        int size = this.kX[i4].gb().fy().size();
                        this.lk.cL().fj = false;
                        if (this.lk.cs() - size >= this.kS) {
                            a(this.kX[i4], true, null, false, 0, false);
                        } else {
                            this.kX[i4].gb().j((Vector) this.lk.cJ().clone());
                        }
                    } else if (b2 == 3 && !this.lk.pt) {
                    }
                }
            }
        }
        this.lg.addElement(aVar2);
        if (this.kT.size() != 0) {
            aVar2.kw = true;
            this.lk.px = true;
        } else if (this.lg.size() == 1) {
            this.le = true;
            ((a.b.c.a.a) this.lg.firstElement()).bC();
        } else {
            this.lg.removeElementAt(this.lg.size() - 1);
        }
        return true;
    }

    public final int ca() {
        return this.kS;
    }

    public final a.a.a cb() {
        return this.lj;
    }

    public final int[] cc() {
        return this.lm;
    }

    public final boolean cd() {
        return this.le;
    }

    public final void ce() {
        this.lf = true;
        this.le = true;
    }

    public final boolean d(int i, int i2) {
        a.b.c.a.a aVar = (a.b.c.a.a) this.lg.elementAt(0);
        if (this.le || i != aVar.kr.ga() || aVar.kr.gb().fz() != null) {
            return false;
        }
        if (aVar.kv == 0) {
            return false;
        }
        a.a.a aVar2 = (a.a.a) this.kW.elementAt(this.kW.size() - 1);
        if (!aVar.kr.gb().a(aVar2, i2)) {
            return false;
        }
        cf();
        if (aVar.kr.gb().fA() == 2 && aVar.kr.lM == -1) {
            aVar.kr.lM = this.li;
        }
        this.kW.removeElementAt(this.kW.size() - 1);
        this.kV.removeElementAt(this.kV.size() - 1);
        aVar.bC();
        if (i2 == 1) {
            int[] iArr = this.lm;
            int aM = aVar2.aM() - 2;
            iArr[aM] = iArr[aM] + 1;
            int[] iArr2 = this.lm;
            int aM2 = aVar2.aM() - 1;
            iArr2[aM2] = iArr2[aM2] + 1;
        } else if (i2 == 2) {
            int[] iArr3 = this.lm;
            int aM3 = aVar2.aM() - 1;
            iArr3[aM3] = iArr3[aM3] + 1;
            int[] iArr4 = this.lm;
            int aM4 = aVar2.aM() + 1;
            iArr4[aM4] = iArr4[aM4] + 1;
        } else if (i2 == 3) {
            int[] iArr5 = this.lm;
            int aM5 = aVar2.aM() + 1;
            iArr5[aM5] = iArr5[aM5] + 1;
            int[] iArr6 = this.lm;
            int aM6 = aVar2.aM() + 2;
            iArr6[aM6] = iArr6[aM6] + 1;
        }
        this.lh = 6;
        this.li = i;
        return true;
    }

    public final void q(int i) {
        this.lb = i;
    }

    public final void r(int i) {
        this.la = i;
        this.kZ = i / 4;
    }

    public final void s(int i) {
        this.kS = i;
    }

    public final boolean t(int i) {
        a.b.c.a.a aVar = (a.b.c.a.a) this.lg.elementAt(0);
        if (this.le || i != aVar.kr.ga() || aVar.kr.gb().fz() != null) {
            return false;
        }
        if (this.lg.size() > 1 || this.kT.size() == 0) {
            return false;
        }
        cf();
        a.a.a aVar2 = (a.a.a) this.kT.elementAt(0);
        this.kT.removeElementAt(0);
        aVar.bC();
        if (aVar.kr.gb().b(aVar2)) {
            aVar.ks = aVar.kr.gb().fG();
            if (aVar.ks) {
                boolean z = this.lk.px;
                boolean z2 = this.lk.pC;
                this.lk.a(this, this.kX[i], aVar2);
                if (!this.lk.pN && !this.lk.pO) {
                    this.lk.px = z;
                    this.lk.pC = z2;
                    this.lk.pw = true;
                }
                if (this.lk.cs() - aVar.kr.gb().fy().size() < this.kS) {
                    aVar.ks = false;
                    aVar.kr.gb().j((Vector) this.lk.cJ().clone());
                }
            }
            aVar.kr.gb().i(aVar.kt);
            if ((this.lh == 4 || this.lh == 2 || this.lh == 3) && aVar.kt.size() != 0) {
                this.lk.pC = true;
            }
            this.lj = (a.a.a) c.mu.get(e.nv[0]);
        } else {
            this.lk.px = false;
            if (this.kT.size() == 0) {
                this.le = true;
            } else {
                aVar.kw = true;
            }
            this.lj = aVar2;
            if (this.kS >= 0) {
                aVar.ks = false;
            } else {
                aVar.ks = true;
            }
        }
        this.lh = 0;
        this.li = i;
        this.lk.lQ = false;
        return true;
    }

    public final boolean u(int i) {
        a.b.c.a.a aVar = (a.b.c.a.a) this.lg.elementAt(0);
        if (this.le || i != aVar.kr.ga() || aVar.kr.gb().fz() != null) {
            return false;
        }
        if (!aVar.ku) {
            return false;
        }
        a.a.a aVar2 = (a.a.a) this.kW.elementAt(this.kW.size() - 1);
        if (!aVar.kr.gb().a(aVar.kr, this.kX[this.li], aVar2)) {
            return false;
        }
        cf();
        if (aVar.kr.gb().fA() == 2 && aVar.kr.lM == -1) {
            aVar.kr.lM = this.li;
        }
        this.kW.removeElementAt(this.kW.size() - 1);
        this.kV.removeElementAt(this.kV.size() - 1);
        aVar.bC();
        int[] iArr = this.lm;
        int aM = aVar2.aM();
        iArr[aM] = iArr[aM] + 2;
        this.lh = 5;
        this.li = i;
        this.lg.removeAllElements();
        this.lg.addElement(aVar);
        return true;
    }

    public final boolean v(int i) {
        a.b.c.a.a aVar = (a.b.c.a.a) this.lg.elementAt(0);
        if (this.le || i != aVar.kr.ga() || aVar.kr.gb().fz() != null) {
            return false;
        }
        if (aVar.kx == 0) {
            return false;
        }
        cf();
        aVar.kr.gb().yQ = aVar.kx;
        this.lg.removeElementAt(0);
        this.lh = 7;
        return true;
    }

    public final boolean w(int i) {
        a.b.c.a.a aVar = (a.b.c.a.a) this.lg.elementAt(0);
        if (this.le || i != aVar.kr.ga()) {
            return false;
        }
        if (this.lg.size() != 1) {
            this.lg.removeElementAt(0);
            cf();
            return true;
        } else if (this.kT.size() != 0) {
            return false;
        } else {
            this.le = true;
            ((a.b.c.a.a) this.lg.firstElement()).bC();
            return true;
        }
    }

    public final boolean x(int i) {
        a.b.c.b.a aVar;
        a.b.c.a.a aVar2 = (a.b.c.a.a) this.lg.elementAt(0);
        if (this.le || i != aVar2.kr.ga()) {
            return false;
        }
        if (!aVar2.ks) {
            return false;
        }
        cf();
        try {
            aVar = this.lk.lL != i ? this.lk.clone() : this.lk;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            aVar = null;
        }
        if (aVar2.kr.gb().fz() == null) {
            if (aVar.lQ) {
                if (aVar.lL != i) {
                    aVar.a(this, this.kX[i], this.lj);
                }
                aVar.lQ = true;
                aVar.cL().fj = false;
                aVar.lK = this.li;
                aVar2.kr.gb().b(this.lj);
            } else if (this.li == i || this.kW.size() == 0) {
                aVar.bC();
                aVar.pM = -1;
                aVar.lL = i;
            } else {
                a.a.a aVar3 = (a.a.a) this.kW.elementAt(this.kW.size() - 1);
                if (aVar.lL != i) {
                    aVar.a(this, this.kX[i], aVar3);
                }
                aVar2.kr.gb().b(aVar3);
                aVar.lK = this.li;
            }
        }
        this.lg.removeElementAt(0);
        if (this.ll.size() == 0) {
            for (int size = this.lg.size() - 1; size >= 0; size--) {
                aVar2 = (a.b.c.a.a) this.lg.elementAt(size);
                if (aVar2.ku || aVar2.kt.size() > 0 || aVar2.kv != 0) {
                    if (aVar2.ks || aVar2.kw) {
                        aVar2.ku = false;
                        aVar2.kt.removeAllElements();
                        aVar2.kv = 0;
                    } else {
                        this.lg.removeElementAt(size);
                    }
                }
            }
        }
        a.b.c.a.a aVar4 = aVar2;
        aVar.lJ = i;
        this.ll.add(aVar);
        if (!this.kR || this.lg.size() == 0 || ((this.lg.size() == 1 && !((a.b.c.a.a) this.lg.elementAt(0)).ks) || aVar.pM > 0)) {
            a.b.a.b.e(this.ll);
            this.lk = (a.b.c.b.a) this.ll.elementAt(0);
            if (this.lk.lK != -1 && !this.lk.lQ && this.lk.pM <= 0) {
                this.kW.removeElementAt(this.kW.size() - 1);
                this.kV.removeElementAt(this.kV.size() - 1);
            } else if (this.lk.lQ) {
                Vector fx = this.kX[this.li].gb().fx();
                int i2 = 0;
                while (true) {
                    if (i2 >= fx.size()) {
                        break;
                    } else if (fx.elementAt(i2) == this.lj) {
                        fx.removeElementAt(i2);
                        break;
                    } else {
                        i2++;
                    }
                }
            }
            this.lg.removeAllElements();
            aVar4.bC();
            this.lg.addElement(aVar4);
            this.lh = 1;
            int i3 = 0;
            while (true) {
                if (i3 >= this.ll.size()) {
                    break;
                } else if (((a.b.c.b.a) this.ll.elementAt(i3)).lL == this.ld) {
                    this.lk = (a.b.c.b.a) this.ll.elementAt(i3);
                    break;
                } else {
                    i3++;
                }
            }
            this.li = this.lk.lL;
            this.ld = this.lk.lL;
            this.le = true;
        }
        return true;
    }
}
