package defpackage;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import com.google.ads.AdActivity;
import com.google.ads.AdView;
import com.google.ads.b;
import com.google.ads.d;
import com.google.ads.e;
import com.google.ads.f;
import com.google.ads.g;
import com.google.ads.util.AdUtil;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: c  reason: default package */
public final class c {
    private static final Object bJ = new Object();
    private WeakReference cZ;
    private b da;
    private e db = null;
    private f dc = null;
    private d dd = null;
    private g de;
    private a df = new a();
    private String dg;
    private b dh;
    private n di;
    private Handler dj = new Handler();
    private long dk;
    private boolean dl = false;
    private boolean dm = false;
    private SharedPreferences dn;

    /* renamed from: do  reason: not valid java name */
    private long f0do = 0;
    private x dp;
    private boolean dq = false;
    private LinkedList dr;
    private LinkedList ds;
    private int dt;

    public c(Activity activity, b bVar, g gVar, String str) {
        this.cZ = new WeakReference(activity);
        this.da = bVar;
        this.de = gVar;
        this.dg = str;
        synchronized (bJ) {
            this.dn = activity.getApplicationContext().getSharedPreferences("GoogleAdMobAdsPrefs", 0);
            this.dk = 60000;
        }
        this.dp = new x(this);
        this.dr = new LinkedList();
        this.ds = new LinkedList();
        X();
        AdUtil.q(activity.getApplicationContext());
    }

    private synchronized void aF() {
        Activity activity = (Activity) this.cZ.get();
        if (activity == null) {
            com.google.ads.util.d.bl("activity was null while trying to ping tracking URLs.");
        } else {
            Iterator it = this.dr.iterator();
            while (it.hasNext()) {
                new Thread(new p((String) it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    private synchronized void aH() {
        Activity activity = (Activity) this.cZ.get();
        if (activity == null) {
            com.google.ads.util.d.bl("activity was null while trying to ping click tracking URLs.");
        } else {
            Iterator it = this.ds.iterator();
            while (it.hasNext()) {
                new Thread(new p((String) it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    private synchronized boolean ay() {
        return this.dc != null;
    }

    public final synchronized void X() {
        Activity ao = ao();
        if (ao == null) {
            com.google.ads.util.d.k("activity was null while trying to create an AdWebView.");
        } else {
            this.dh = new b(ao.getApplicationContext(), this.de);
            this.dh.setVisibility(8);
            if (this.da instanceof AdView) {
                this.di = new n(this, g.dK, true, false);
            } else {
                this.di = new n(this, g.dK, true, true);
            }
            this.dh.setWebViewClient(this.di);
        }
    }

    public final synchronized void Z() {
        if (this.dm) {
            com.google.ads.util.d.k("Disabling refreshing.");
            this.dj.removeCallbacks(this.dp);
            this.dm = false;
        } else {
            com.google.ads.util.d.k("Refreshing is already disabled.");
        }
    }

    public final synchronized void a(float f) {
        this.f0do = (long) (1000.0f * f);
    }

    public final void a(long j) {
        synchronized (bJ) {
            SharedPreferences.Editor edit = this.dn.edit();
            edit.putLong("Timeout" + this.dg, j);
            edit.commit();
            if (this.dq) {
                this.dk = j;
            }
        }
    }

    public final synchronized void a(com.google.ads.c cVar) {
        this.dc = null;
        if (this.da instanceof f) {
            if (cVar == com.google.ads.c.NO_FILL) {
                this.df.ak();
            } else if (cVar == com.google.ads.c.NETWORK_ERROR) {
                this.df.ai();
            }
        }
        com.google.ads.util.d.bj("onFailedToReceiveAd(" + cVar + ")");
        if (this.db != null) {
            this.db.dN();
        }
    }

    public final synchronized void a(d dVar) {
        if (ay()) {
            com.google.ads.util.d.bl("loadAd called while the ad is already loading, so aborting.");
        } else if (AdActivity.isShowing()) {
            com.google.ads.util.d.bl("loadAd called while an interstitial or landing page is displayed, so aborting");
        } else {
            Activity ao = ao();
            if (ao == null) {
                com.google.ads.util.d.bl("activity is null while trying to load an ad.");
            } else if (AdUtil.m(ao.getApplicationContext()) && AdUtil.l(ao.getApplicationContext())) {
                this.dl = false;
                this.dr.clear();
                this.dd = dVar;
                this.dc = new f(this);
                this.dc.a(dVar);
            }
        }
    }

    public final synchronized void a(e eVar) {
        this.db = eVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(Runnable runnable) {
        this.dj.post(runnable);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(LinkedList linkedList) {
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            com.google.ads.util.d.k("Adding a click tracking URL: " + ((String) it.next()));
        }
        this.ds = linkedList;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void aA() {
        this.dc = null;
        this.dl = true;
        this.dh.setVisibility(0);
        this.df.Z();
        if (this.da instanceof AdView) {
            aF();
        }
        com.google.ads.util.d.bj("onReceiveAd()");
        if (this.db != null) {
            this.db.dO();
        }
    }

    public final synchronized void aB() {
        this.df.al();
        com.google.ads.util.d.bj("onDismissScreen()");
    }

    public final synchronized void aC() {
        com.google.ads.util.d.bj("onPresentScreen()");
    }

    public final synchronized void aD() {
        com.google.ads.util.d.bj("onLeaveApplication()");
    }

    public final void aE() {
        this.df.Y();
        aH();
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean aG() {
        return !this.ds.isEmpty();
    }

    public final synchronized void aI() {
        if (this.dd == null) {
            com.google.ads.util.d.k("Tried to refresh before calling loadAd().");
        } else if (this.da instanceof AdView) {
            if (!((AdView) this.da).isShown() || !AdUtil.eK()) {
                com.google.ads.util.d.k("Not refreshing because the ad is not visible.");
            } else {
                com.google.ads.util.d.bj("Refreshing ad.");
                a(this.dd);
            }
            this.dj.postDelayed(this.dp, this.f0do);
        } else {
            com.google.ads.util.d.k("Tried to refresh an ad that wasn't an AdView.");
        }
    }

    public final synchronized void aa() {
        if (!(this.da instanceof AdView)) {
            com.google.ads.util.d.k("Tried to enable refreshing on something other than an AdView.");
        } else if (!this.dm) {
            com.google.ads.util.d.k("Enabling refreshing every " + this.f0do + " milliseconds.");
            this.dj.postDelayed(this.dp, this.f0do);
            this.dm = true;
        } else {
            com.google.ads.util.d.k("Refreshing is already enabled.");
        }
    }

    public final Activity ao() {
        return (Activity) this.cZ.get();
    }

    public final b ap() {
        return this.da;
    }

    public final synchronized f aq() {
        return this.dc;
    }

    /* access modifiers changed from: package-private */
    public final String ar() {
        return this.dg;
    }

    public final synchronized b as() {
        return this.dh;
    }

    /* access modifiers changed from: package-private */
    public final synchronized n at() {
        return this.di;
    }

    public final g au() {
        return this.de;
    }

    public final a av() {
        return this.df;
    }

    public final synchronized int aw() {
        return this.dt;
    }

    public final long ax() {
        return this.dk;
    }

    public final synchronized boolean az() {
        return this.dm;
    }

    public final synchronized void f(int i) {
        this.dt = i;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void k(String str) {
        com.google.ads.util.d.k("Adding a tracking URL: " + str);
        this.dr.add(str);
    }
}
