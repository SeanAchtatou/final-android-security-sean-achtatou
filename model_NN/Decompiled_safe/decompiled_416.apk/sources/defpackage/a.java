package defpackage;

import android.os.SystemClock;
import com.google.ads.util.d;
import java.util.LinkedList;

/* renamed from: a  reason: default package */
public final class a {
    private static long cQ = 0;
    private LinkedList cM = new LinkedList();
    private long cN;
    private long cO;
    private LinkedList cP = new LinkedList();
    private String cR;
    private boolean cS = false;
    private boolean cT = false;
    private String cU;

    a() {
        X();
    }

    static long af() {
        return cQ;
    }

    /* access modifiers changed from: package-private */
    public final void X() {
        this.cM.clear();
        this.cN = 0;
        this.cO = 0;
        this.cP.clear();
        this.cR = null;
        this.cS = false;
        this.cT = false;
    }

    /* access modifiers changed from: package-private */
    public final void Y() {
        d.bk("Ad clicked.");
        this.cM.add(Long.valueOf(SystemClock.elapsedRealtime()));
    }

    /* access modifiers changed from: package-private */
    public final void Z() {
        d.bk("Ad request loaded.");
        this.cN = SystemClock.elapsedRealtime();
    }

    /* access modifiers changed from: package-private */
    public final void aa() {
        d.bk("Ad request started.");
        this.cO = SystemClock.elapsedRealtime();
        cQ++;
    }

    /* access modifiers changed from: package-private */
    public final long ab() {
        if (this.cM.size() != this.cP.size()) {
            return -1;
        }
        return (long) this.cM.size();
    }

    /* access modifiers changed from: package-private */
    public final String ac() {
        if (this.cM.isEmpty() || this.cM.size() != this.cP.size()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.cM.size()) {
                return sb.toString();
            }
            if (i2 != 0) {
                sb.append(",");
            }
            sb.append(Long.toString(((Long) this.cP.get(i2)).longValue() - ((Long) this.cM.get(i2)).longValue()));
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public final String ad() {
        if (this.cM.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.cM.size()) {
                return sb.toString();
            }
            if (i2 != 0) {
                sb.append(",");
            }
            sb.append(Long.toString(((Long) this.cM.get(i2)).longValue() - this.cN));
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public final long ae() {
        return this.cN - this.cO;
    }

    /* access modifiers changed from: package-private */
    public final String ag() {
        return this.cR;
    }

    /* access modifiers changed from: package-private */
    public final boolean ah() {
        return this.cS;
    }

    /* access modifiers changed from: package-private */
    public final void ai() {
        d.bk("Interstitial network error.");
        this.cS = true;
    }

    /* access modifiers changed from: package-private */
    public final boolean aj() {
        return this.cT;
    }

    /* access modifiers changed from: package-private */
    public final void ak() {
        d.bk("Interstitial no fill.");
        this.cT = true;
    }

    public final void al() {
        d.bk("Landing page dismissed.");
        this.cP.add(Long.valueOf(SystemClock.elapsedRealtime()));
    }

    /* access modifiers changed from: package-private */
    public final String am() {
        return this.cU;
    }

    public final void k(String str) {
        d.bk("Prior ad identifier = " + str);
        this.cR = str;
    }

    public final void s(String str) {
        d.bk("Prior impression ticket = " + str);
        this.cU = str;
    }
}
