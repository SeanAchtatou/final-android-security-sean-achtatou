package defpackage;

import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.google.ads.g;
import com.google.ads.util.AdUtil;
import com.google.ads.util.d;

/* renamed from: b  reason: default package */
public final class b extends WebView {
    private AdActivity cW;
    private g cX;

    public b(Context context, g gVar) {
        super(context);
        this.cX = gVar;
        setBackgroundColor(0);
        AdUtil.a(this);
        getSettings().setJavaScriptEnabled(true);
        setScrollBarStyle(0);
    }

    public final void X() {
        if (this.cW != null) {
            this.cW.finish();
        }
    }

    public final void a(AdActivity adActivity) {
        this.cW = adActivity;
    }

    public final AdActivity an() {
        return this.cW;
    }

    public final void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        try {
            super.loadDataWithBaseURL(str, str2, str3, str4, str5);
        } catch (Exception e) {
            d.a("An error occurred while loading data in AdWebView:", e);
        }
    }

    public final void loadUrl(String str) {
        try {
            super.loadUrl(str);
        } catch (Exception e) {
            d.a("An error occurred while loading a URL in AdWebView:", e);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        if (isInEditMode()) {
            super.onMeasure(i, i2);
        } else if (this.cX == null) {
            super.onMeasure(i, i2);
        } else {
            int mode = View.MeasureSpec.getMode(i);
            int size = View.MeasureSpec.getSize(i);
            int mode2 = View.MeasureSpec.getMode(i2);
            int size2 = View.MeasureSpec.getSize(i2);
            float f = getContext().getResources().getDisplayMetrics().density;
            int width = (int) (((float) this.cX.getWidth()) * f);
            int height = (int) (((float) this.cX.getHeight()) * f);
            if (mode == 0 || mode2 == 0) {
                super.onMeasure(i, i2);
            } else if (((float) width) - (6.0f * f) > ((float) size) || height > size2) {
                d.bl("Not enough space to show ad! Wants: <" + width + ", " + height + ">, Has: <" + size + ", " + size2 + ">");
                setVisibility(8);
                setMeasuredDimension(0, 0);
            } else {
                super.onMeasure(i, i2);
            }
        }
    }
}
