package defpackage;

import com.google.ads.util.d;
import java.lang.ref.WeakReference;

/* renamed from: x  reason: default package */
public final class x implements Runnable {
    private WeakReference eo;

    public x(c cVar) {
        this.eo = new WeakReference(cVar);
    }

    public final void run() {
        c cVar = (c) this.eo.get();
        if (cVar == null) {
            d.k("The ad must be gone, so cancelling the refresh timer.");
        } else {
            cVar.aI();
        }
    }
}
