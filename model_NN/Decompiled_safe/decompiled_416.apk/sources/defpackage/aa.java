package defpackage;

import android.webkit.WebView;

/* renamed from: aa  reason: default package */
final class aa implements Runnable {
    private /* synthetic */ f dv;
    private final String dz;
    private final String ik;
    private final WebView il;

    public aa(f fVar, WebView webView, String str, String str2) {
        this.dv = fVar;
        this.il = webView;
        this.dz = str;
        this.ik = str2;
    }

    public final void run() {
        this.il.loadDataWithBaseURL(this.dz, this.ik, "text/html", "utf-8", null);
    }
}
