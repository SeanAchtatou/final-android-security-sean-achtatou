package defpackage;

import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.ads.AdActivity;
import com.google.ads.util.AdUtil;
import com.google.ads.util.d;
import java.util.HashMap;
import java.util.Map;

/* renamed from: n  reason: default package */
public final class n extends WebViewClient {
    private Map dK;
    private c dQ;
    private boolean dR;
    private boolean dS = false;
    private boolean dT = false;
    private boolean dx;

    public n(c cVar, Map map, boolean z, boolean z2) {
        this.dQ = cVar;
        this.dK = map;
        this.dx = z;
        this.dR = z2;
    }

    public final void X() {
        this.dS = true;
    }

    public final void Y() {
        this.dT = true;
    }

    public final void onPageFinished(WebView webView, String str) {
        if (this.dS) {
            f aq = this.dQ.aq();
            if (aq != null) {
                aq.Y();
            } else {
                d.k("adLoader was null while trying to setFinishedLoadingHtml().");
            }
            this.dS = false;
        }
        if (this.dT) {
            g.a(webView);
            this.dT = false;
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        d.k("shouldOverrideUrlLoading(\"" + str + "\")");
        Uri parse = Uri.parse(str);
        HashMap e = AdUtil.e(parse);
        if (e == null) {
            d.bl("An error occurred while parsing the url parameters.");
            return true;
        }
        String str2 = (String) e.get("ai");
        if (str2 != null) {
            this.dQ.av().k(str2);
        }
        if (g.b(parse)) {
            g.a(this.dQ, this.dK, parse, webView);
            return true;
        } else if (this.dR) {
            if (AdUtil.b(parse)) {
                return super.shouldOverrideUrlLoading(webView, str);
            }
            HashMap hashMap = new HashMap();
            hashMap.put("u", str);
            AdActivity.a(this.dQ, new d("intent", hashMap));
            return true;
        } else if (this.dx) {
            String str3 = (!this.dQ.aG() || !AdUtil.b(parse)) ? "intent" : "webapp";
            HashMap hashMap2 = new HashMap();
            hashMap2.put("u", parse.toString());
            AdActivity.a(this.dQ, new d(str3, hashMap2));
            return true;
        } else {
            d.bl("URL is not a GMSG and can't handle URL: " + str);
            return true;
        }
    }
}
