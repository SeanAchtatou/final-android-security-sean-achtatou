package defpackage;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.google.ads.c;
import com.google.ads.d;
import com.google.ads.g;
import com.google.ads.util.AdUtil;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

/* renamed from: f  reason: default package */
public final class f implements Runnable {
    private String cR = null;
    private volatile boolean cT;
    private String cU;
    private e dA;
    private c dB;
    private d dC;
    private WebView dD;
    private LinkedList dE = new LinkedList();
    private c dF = null;
    private boolean dG = false;
    private int dH = -1;
    private Thread dI;
    private String dz = null;

    public f(c cVar) {
        this.dB = cVar;
        Activity ao = cVar.ao();
        if (ao != null) {
            this.dD = new b(ao.getApplicationContext(), null);
            this.dD.setWebViewClient(new n(cVar, g.dJ, false, false));
            this.dD.setVisibility(8);
            this.dD.setWillNotDraw(true);
            this.dA = new e(this, cVar);
            return;
        }
        this.dD = null;
        this.dA = null;
        com.google.ads.util.d.bl("activity was null while trying to create an AdLoader.");
    }

    private String a(d dVar, Activity activity) {
        Context applicationContext = activity.getApplicationContext();
        Map j = dVar.j(applicationContext);
        a av = this.dB.av();
        long ae = av.ae();
        if (ae > 0) {
            j.put("prl", Long.valueOf(ae));
        }
        String ad = av.ad();
        if (ad != null) {
            j.put("ppcl", ad);
        }
        String ac = av.ac();
        if (ac != null) {
            j.put("pcl", ac);
        }
        long ab = av.ab();
        if (ab > 0) {
            j.put("pcc", Long.valueOf(ab));
        }
        j.put("preqs", Long.valueOf(a.af()));
        String ag = av.ag();
        if (ag != null) {
            j.put("pai", ag);
        }
        if (av.ah()) {
            j.put("aoi_timeout", "true");
        }
        if (av.aj()) {
            j.put("aoi_nofill", "true");
        }
        String am = av.am();
        if (am != null) {
            j.put("pit", am);
        }
        av.X();
        av.aa();
        if (this.dB.ap() instanceof com.google.ads.f) {
            j.put("format", "interstitial_mb");
        } else {
            g au = this.dB.au();
            String gVar = au.toString();
            if (gVar != null) {
                j.put("format", gVar);
            } else {
                HashMap hashMap = new HashMap();
                hashMap.put("w", Integer.valueOf(au.getWidth()));
                hashMap.put("h", Integer.valueOf(au.getHeight()));
                j.put("ad_frame", hashMap);
            }
        }
        j.put("slotname", this.dB.ar());
        j.put("js", "afma-sdk-a-v4.1.1");
        try {
            int i = applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 0).versionCode;
            j.put("msid", applicationContext.getPackageName());
            j.put("app_name", i + ".android." + applicationContext.getPackageName());
            j.put("isu", AdUtil.k(applicationContext));
            String n = AdUtil.n(applicationContext);
            if (n == null) {
                throw new ab(this, "NETWORK_ERROR");
            }
            j.put("net", n);
            String o = AdUtil.o(applicationContext);
            if (!(o == null || o.length() == 0)) {
                j.put("cap", o);
            }
            j.put("u_audio", Integer.valueOf(AdUtil.p(applicationContext).ordinal()));
            DisplayMetrics b2 = AdUtil.b(activity);
            j.put("u_sd", Float.valueOf(b2.density));
            j.put("u_h", Integer.valueOf(AdUtil.a(applicationContext, b2)));
            j.put("u_w", Integer.valueOf(AdUtil.b(applicationContext, b2)));
            j.put("hl", Locale.getDefault().getLanguage());
            if (AdUtil.eJ()) {
                j.put("simulator", 1);
            }
            String str = "<html><head><script src=\"http://www.gstatic.com/afma/sdk-core-v40.js\"></script><script>AFMA_buildAdURL(" + AdUtil.a(j) + ");" + "</script></head><body></body></html>";
            com.google.ads.util.d.bj("adRequestUrlHtml: " + str);
            return str;
        } catch (PackageManager.NameNotFoundException e) {
            throw new z(this, "NameNotFoundException");
        }
    }

    private void a(c cVar, boolean z) {
        this.dA.X();
        this.dB.a(new y(this, this.dB, this.dD, this.dA, cVar, z));
    }

    /* access modifiers changed from: package-private */
    public final synchronized void Y() {
        this.dG = true;
        notify();
    }

    public final synchronized void a(c cVar) {
        this.dF = cVar;
        notify();
    }

    /* access modifiers changed from: package-private */
    public final void a(d dVar) {
        this.dC = dVar;
        this.cT = false;
        this.dI = new Thread(this);
        this.dI.start();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void e(String str, String str2) {
        this.cU = str2;
        this.dz = str;
        notify();
    }

    public final synchronized void f(int i) {
        this.dH = i;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void k(String str) {
        this.dE.add(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: f.a(com.google.ads.c, boolean):void
     arg types: [com.google.ads.c, int]
     candidates:
      f.a(com.google.ads.d, android.app.Activity):java.lang.String
      f.a(com.google.ads.c, boolean):void */
    public final void run() {
        synchronized (this) {
            if (this.dD == null || this.dA == null) {
                com.google.ads.util.d.bl("adRequestWebView was null while trying to load an ad.");
                a(c.INTERNAL_ERROR, false);
                return;
            }
            Activity ao = this.dB.ao();
            if (ao == null) {
                com.google.ads.util.d.bl("activity was null while forming an ad request.");
                a(c.INTERNAL_ERROR, false);
                return;
            }
            try {
                this.dB.a(new aa(this, this.dD, null, a(this.dC, ao)));
                long ax = this.dB.ax();
                long elapsedRealtime = SystemClock.elapsedRealtime();
                if (ax > 0) {
                    try {
                        wait(ax);
                    } catch (InterruptedException e) {
                        com.google.ads.util.d.k("AdLoader InterruptedException while getting the URL: " + e);
                        return;
                    }
                }
                try {
                    if (!this.cT) {
                        if (this.dF != null) {
                            a(this.dF, false);
                            return;
                        } else if (this.cR == null) {
                            com.google.ads.util.d.bj("AdLoader timed out after " + ax + "ms while getting the URL.");
                            a(c.NETWORK_ERROR, false);
                            return;
                        } else {
                            this.dA.k(this.cR);
                            long elapsedRealtime2 = ax - (SystemClock.elapsedRealtime() - elapsedRealtime);
                            if (elapsedRealtime2 > 0) {
                                try {
                                    wait(elapsedRealtime2);
                                } catch (InterruptedException e2) {
                                    com.google.ads.util.d.k("AdLoader InterruptedException while getting the HTML: " + e2);
                                    return;
                                }
                            }
                            if (!this.cT) {
                                if (this.dF != null) {
                                    a(this.dF, false);
                                    return;
                                } else if (this.dz == null) {
                                    com.google.ads.util.d.bj("AdLoader timed out after " + ax + "ms while getting the HTML.");
                                    a(c.NETWORK_ERROR, false);
                                    return;
                                } else {
                                    b as = this.dB.as();
                                    this.dB.at().X();
                                    this.dB.a(new aa(this, as, this.cU, this.dz));
                                    long elapsedRealtime3 = ax - (SystemClock.elapsedRealtime() - elapsedRealtime);
                                    if (elapsedRealtime3 > 0) {
                                        try {
                                            wait(elapsedRealtime3);
                                        } catch (InterruptedException e3) {
                                            com.google.ads.util.d.k("AdLoader InterruptedException while loading the HTML: " + e3);
                                            return;
                                        }
                                    }
                                    if (this.dG) {
                                        this.dB.a(new ac(this, this.dB, this.dE, this.dH));
                                    } else {
                                        com.google.ads.util.d.bj("AdLoader timed out after " + ax + "ms while loading the HTML.");
                                        a(c.NETWORK_ERROR, true);
                                    }
                                    return;
                                }
                            } else {
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                } catch (Exception e4) {
                    com.google.ads.util.d.a("An unknown error occurred in AdLoader.", e4);
                    a(c.INTERNAL_ERROR, true);
                }
            } catch (ab e5) {
                com.google.ads.util.d.bj("Unable to connect to network: " + e5);
                a(c.NETWORK_ERROR, false);
                return;
            } catch (z e6) {
                com.google.ads.util.d.bj("Caught internal exception: " + e6);
                a(c.INTERNAL_ERROR, false);
                return;
            }
        }
    }

    public final synchronized void s(String str) {
        this.cR = str;
        notify();
    }
}
