package defpackage;

import android.webkit.WebView;
import com.google.ads.util.d;
import java.util.HashMap;

/* renamed from: j  reason: default package */
public final class j implements o {
    public final void a(c cVar, HashMap hashMap, WebView webView) {
        if (webView instanceof b) {
            ((b) webView).X();
        } else {
            d.s("Trying to close WebView that isn't an AdWebView");
        }
    }
}
