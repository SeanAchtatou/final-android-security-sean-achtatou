package defpackage;

import android.net.Uri;
import android.webkit.WebView;
import b.a.b;
import com.google.ads.util.AdUtil;
import com.google.ads.util.d;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: g  reason: default package */
public final class g {
    public static final Map dJ;
    public static final Map dK;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put("/invalidRequest", new i());
        hashMap.put("/loadAdURL", new w());
        dJ = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("/open", new u());
        hashMap2.put("/canOpenURLs", new l());
        hashMap2.put("/close", new j());
        hashMap2.put("/evalInOpener", new k());
        hashMap2.put("/log", new v());
        hashMap2.put("/click", new m());
        hashMap2.put("/httpTrack", new h());
        hashMap2.put("/reloadRequest", new t());
        hashMap2.put("/settings", new s());
        hashMap2.put("/touch", new r());
        hashMap2.put("/video", new q());
        dK = Collections.unmodifiableMap(hashMap2);
    }

    private g() {
    }

    public static void a(WebView webView) {
        d.bk("Calling onshow.");
        a(webView, "onshow", "{'version': 'afma-sdk-a-v4.1.1'}");
    }

    public static void a(WebView webView, String str) {
        webView.loadUrl("javascript:" + str);
    }

    public static void a(WebView webView, String str, String str2) {
        if (str2 != null) {
            a(webView, "AFMA_ReceiveMessage" + "('" + str + "', " + str2 + ");");
        } else {
            a(webView, "AFMA_ReceiveMessage" + "('" + str + "');");
        }
    }

    public static void a(WebView webView, Map map) {
        a(webView, "openableURLs", new b(map).toString());
    }

    static void a(c cVar, Map map, Uri uri, WebView webView) {
        String str;
        HashMap e = AdUtil.e(uri);
        if (e == null) {
            d.bl("An error occurred while parsing the message parameters.");
            return;
        }
        if (d(uri)) {
            String host = uri.getHost();
            if (host == null) {
                d.bl("An error occurred while parsing the AMSG parameters.");
                str = null;
            } else if (host.equals("launch")) {
                e.put("a", "intent");
                e.put("u", e.get("url"));
                e.remove("url");
                str = "/open";
            } else if (host.equals("closecanvas")) {
                str = "/close";
            } else if (host.equals("log")) {
                str = "/log";
            } else {
                d.bl("An error occurred while parsing the AMSG: " + uri.toString());
                str = null;
            }
        } else if (c(uri)) {
            str = uri.getPath();
        } else {
            d.bl("Message was neither a GMSG nor an AMSG.");
            str = null;
        }
        if (str == null) {
            d.bl("An error occurred while parsing the message.");
            return;
        }
        o oVar = (o) map.get(str);
        if (oVar == null) {
            d.bl("No AdResponse found, <message: " + str + ">");
        } else {
            oVar.a(cVar, e, webView);
        }
    }

    public static void b(WebView webView) {
        d.bk("Calling onhide.");
        a(webView, "onhide", null);
    }

    public static boolean b(Uri uri) {
        if (uri == null || !uri.isHierarchical()) {
            return false;
        }
        return c(uri) || d(uri);
    }

    private static boolean c(Uri uri) {
        String scheme = uri.getScheme();
        if (scheme == null || !scheme.equals("gmsg")) {
            return false;
        }
        String authority = uri.getAuthority();
        return authority != null && authority.equals("mobileads.google.com");
    }

    private static boolean d(Uri uri) {
        String scheme = uri.getScheme();
        return scheme != null && scheme.equals("admob");
    }
}
