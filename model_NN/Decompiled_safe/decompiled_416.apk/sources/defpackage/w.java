package defpackage;

import android.webkit.WebView;
import com.google.ads.util.d;
import java.util.HashMap;

/* renamed from: w  reason: default package */
public final class w implements o {
    public final void a(c cVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("url");
        d.bj("Received ad url: <\"url\": \"" + str + "\", \"afmaNotifyDt\": \"" + ((String) hashMap.get("afma_notify_dt")) + "\">");
        f aq = cVar.aq();
        if (aq != null) {
            aq.s(str);
        }
    }
}
