package defpackage;

import android.os.Bundle;
import java.io.Serializable;
import java.util.HashMap;

/* renamed from: d  reason: default package */
public final class d {
    private String cU;
    private HashMap du;

    public d(Bundle bundle) {
        this.cU = bundle.getString("action");
        Serializable serializable = bundle.getSerializable("params");
        this.du = serializable instanceof HashMap ? (HashMap) serializable : null;
    }

    private d(String str) {
        this.cU = str;
    }

    public d(String str, HashMap hashMap) {
        this(str);
        this.du = hashMap;
    }

    public final Bundle aJ() {
        Bundle bundle = new Bundle();
        bundle.putString("action", this.cU);
        bundle.putSerializable("params", this.du);
        return bundle;
    }

    public final String aK() {
        return this.cU;
    }

    public final HashMap aL() {
        return this.du;
    }
}
