package defpackage;

import android.app.Activity;
import com.google.ads.c;
import com.google.ads.util.AdUtil;
import com.google.ads.util.d;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.StringTokenizer;

/* renamed from: e  reason: default package */
public final class e implements Runnable {
    private f dv;
    private c dw;
    private volatile boolean dx;
    private String dy;

    e(f fVar, c cVar) {
        this.dv = fVar;
        this.dw = cVar;
    }

    private void a(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.dw.k(stringTokenizer.nextToken());
            }
        }
        b(httpURLConnection);
        String headerField2 = httpURLConnection.getHeaderField("X-Afma-Refresh-Rate");
        if (headerField2 != null) {
            try {
                this.dw.a(Float.parseFloat(headerField2));
                if (!this.dw.az()) {
                    this.dw.aa();
                }
            } catch (NumberFormatException e) {
                d.b("Could not get refresh value: " + headerField2, e);
            }
        }
        String headerField3 = httpURLConnection.getHeaderField("X-Afma-Interstitial-Timeout");
        if (headerField3 != null) {
            try {
                this.dw.a((long) (Float.parseFloat(headerField3) * 1000.0f));
            } catch (NumberFormatException e2) {
                d.b("Could not get timeout value: " + headerField3, e2);
            }
        }
        String headerField4 = httpURLConnection.getHeaderField("X-Afma-Orientation");
        if (headerField4 == null) {
            return;
        }
        if (headerField4.equals("portrait")) {
            this.dv.f(AdUtil.eI());
        } else if (headerField4.equals("landscape")) {
            this.dv.f(AdUtil.eH());
        }
    }

    private void b(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Click-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.dv.k(stringTokenizer.nextToken());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void X() {
        this.dx = true;
    }

    /* access modifiers changed from: package-private */
    public final void k(String str) {
        this.dy = str;
        this.dx = false;
        new Thread(this).start();
    }

    public final void run() {
        HttpURLConnection httpURLConnection;
        String readLine;
        while (!this.dx) {
            try {
                httpURLConnection = (HttpURLConnection) new URL(this.dy).openConnection();
                Activity ao = this.dw.ao();
                if (ao == null) {
                    d.bj("activity was null in AdHtmlLoader.");
                    this.dv.a(c.INTERNAL_ERROR);
                    httpURLConnection.disconnect();
                    return;
                }
                AdUtil.a(httpURLConnection, ao.getApplicationContext());
                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.connect();
                int responseCode = httpURLConnection.getResponseCode();
                if (300 <= responseCode && responseCode < 400) {
                    String headerField = httpURLConnection.getHeaderField("Location");
                    if (headerField == null) {
                        d.bj("Could not get redirect location from a " + responseCode + " redirect.");
                        this.dv.a(c.INTERNAL_ERROR);
                        httpURLConnection.disconnect();
                        return;
                    }
                    a(httpURLConnection);
                    this.dy = headerField;
                    httpURLConnection.disconnect();
                } else if (responseCode == 200) {
                    a(httpURLConnection);
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()), 4096);
                    StringBuilder sb = new StringBuilder();
                    while (!this.dx && (readLine = bufferedReader.readLine()) != null) {
                        sb.append(readLine);
                        sb.append("\n");
                    }
                    String sb2 = sb.toString();
                    d.k("Response content is: " + sb2);
                    if (sb2 == null || sb2.trim().length() <= 0) {
                        d.k("Response message is null or zero length: " + sb2);
                        this.dv.a(c.NO_FILL);
                        httpURLConnection.disconnect();
                        return;
                    }
                    this.dv.e(sb2, this.dy);
                    httpURLConnection.disconnect();
                    return;
                } else if (responseCode == 400) {
                    d.bj("Bad request");
                    this.dv.a(c.INVALID_REQUEST);
                    httpURLConnection.disconnect();
                    return;
                } else {
                    d.bj("Invalid response code: " + responseCode);
                    this.dv.a(c.INTERNAL_ERROR);
                    httpURLConnection.disconnect();
                    return;
                }
            } catch (MalformedURLException e) {
                d.a("Received malformed ad url from javascript.", e);
                this.dv.a(c.INTERNAL_ERROR);
                return;
            } catch (IOException e2) {
                d.b("IOException connecting to ad url.", e2);
                this.dv.a(c.NETWORK_ERROR);
                return;
            } catch (Exception e3) {
                d.a("An unknown error occurred in AdHtmlLoader.", e3);
                this.dv.a(c.INTERNAL_ERROR);
                return;
            } catch (Throwable th) {
                httpURLConnection.disconnect();
                throw th;
            }
        }
    }
}
