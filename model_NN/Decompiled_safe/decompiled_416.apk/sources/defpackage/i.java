package defpackage;

import android.webkit.WebView;
import com.google.ads.c;
import com.google.ads.util.d;
import java.util.HashMap;

/* renamed from: i  reason: default package */
public final class i implements o {
    public final void a(c cVar, HashMap hashMap, WebView webView) {
        d.bl("Invalid " + ((String) hashMap.get("type")) + " request error: " + ((String) hashMap.get("errors")));
        f aq = cVar.aq();
        if (aq != null) {
            aq.a(c.INVALID_REQUEST);
        }
    }
}
