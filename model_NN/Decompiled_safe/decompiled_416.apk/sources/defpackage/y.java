package defpackage;

import android.webkit.WebView;
import com.google.ads.c;

/* renamed from: y  reason: default package */
final class y implements Runnable {
    private final boolean dT;
    private /* synthetic */ f dv;
    private final c dw;
    private final WebView ih;
    private final e ii;
    private final c ij;

    public y(f fVar, c cVar, WebView webView, e eVar, c cVar2, boolean z) {
        this.dv = fVar;
        this.dw = cVar;
        this.ih = webView;
        this.ii = eVar;
        this.ij = cVar2;
        this.dT = z;
    }

    public final void run() {
        this.ih.stopLoading();
        this.ih.destroy();
        this.ii.X();
        if (this.dT) {
            b as = this.dw.as();
            as.stopLoading();
            as.setVisibility(8);
        }
        this.dw.a(this.ij);
    }
}
