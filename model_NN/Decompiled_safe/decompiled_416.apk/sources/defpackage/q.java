package defpackage;

import android.app.Activity;
import android.webkit.WebView;
import android.widget.VideoView;
import com.google.ads.AdActivity;
import com.google.ads.util.d;
import java.util.HashMap;

/* renamed from: q  reason: default package */
public final class q implements o {
    public final void a(c cVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("action");
        if (webView instanceof b) {
            AdActivity an = ((b) webView).an();
            if (an == null) {
                d.k("Could not get adActivity to create the video.");
            } else if (str.equals("load")) {
                String str2 = (String) hashMap.get("url");
                Activity ao = cVar.ao();
                if (ao == null) {
                    d.bl("activity was null while loading a video.");
                    return;
                }
                VideoView videoView = new VideoView(ao);
                videoView.setVideoPath(str2);
                an.a(videoView);
            } else {
                VideoView B = an.B();
                if (B == null) {
                    d.bl("Could not get the VideoView for a video GMSG.");
                } else if (str.equals("play")) {
                    B.setVisibility(0);
                    B.start();
                    d.bk("Video is now playing.");
                    g.a(webView, "onVideoEvent", "{'event': 'playing'}");
                } else if (str.equals("pause")) {
                    B.pause();
                } else if (str.equals("stop")) {
                    B.stopPlayback();
                } else if (str.equals("remove")) {
                    B.setVisibility(8);
                } else if (str.equals("replay")) {
                    B.setVisibility(0);
                    B.seekTo(0);
                    B.start();
                } else if (str.equals("currentTime")) {
                    String str3 = (String) hashMap.get("time");
                    if (str3 == null) {
                        d.bl("No \"time\" parameter!");
                    } else {
                        B.seekTo((int) (Float.valueOf(str3).floatValue() * 1000.0f));
                    }
                } else if (!str.equals("position")) {
                    d.bl("Unknown movie action: " + str);
                }
            }
        } else {
            d.k("Could not get adWebView to create the video.");
        }
    }
}
