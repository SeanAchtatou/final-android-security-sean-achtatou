package com.scoreloop.client.android.core.ui;

import android.app.Activity;
import android.util.Log;
import com.a.a.a;
import com.a.a.d;
import com.a.a.e;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.model.FacebookSocialProvider;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import java.util.HashMap;

public class FacebookAuthViewController extends AuthViewController {

    /* renamed from: a  reason: collision with root package name */
    public FacebookPermissionDialog f106a;
    private FacebookLoginDialog b;
    private Activity c;
    /* access modifiers changed from: private */
    public boolean d;
    private e e;
    private d f;
    private h g;

    public FacebookAuthViewController(SocialProviderControllerObserver socialProviderControllerObserver) {
        super(socialProviderControllerObserver);
    }

    private void e() {
        this.g = new h(this, null);
        FacebookSocialProvider.c().d().add(this.g);
    }

    public void a(Activity activity) {
        this.d = false;
        this.c = activity;
        e();
        ((FacebookSocialProvider) SocialProvider.getSocialProviderForIdentifier(FacebookSocialProvider.IDENTIFIER)).d();
        this.f = new g(this, null);
        this.e = new n(this, null);
        this.b = new FacebookLoginDialog(activity, b());
        this.b.show();
        this.b.setOnCancelListener(new b(this));
    }

    public void a(boolean z) {
        if (!z) {
            this.d = true;
        }
        if (this.f106a != null) {
            this.f106a.dismiss();
        }
        this.f106a = null;
        if (this.b != null) {
            this.b.dismiss();
        }
        this.b = null;
    }

    /* access modifiers changed from: package-private */
    public d b() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        a a2 = m.a(this.e);
        HashMap hashMap = new HashMap();
        hashMap.put("user_id", Session.getCurrentSession().getUser().getIdentifier());
        Log.d("inform", "STARTING REQUEST ------------------------------------------------------------------");
        a2.b("http://apps.facebook.com/scoreloop/scoreloop/sdk", hashMap);
    }

    public Activity d() {
        return this.c;
    }
}
