package com.scoreloop.client.android.core.controller;

import android.app.Activity;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.ui.AuthViewController;
import java.lang.reflect.InvocationTargetException;

public abstract class SocialProviderController {

    /* renamed from: a  reason: collision with root package name */
    U f47a;
    RequestControllerObserver b;
    private Activity c;
    private SocialProviderControllerObserver d;
    private SocialProvider e;

    SocialProviderController(Session session, SocialProviderControllerObserver socialProviderControllerObserver) {
        this.d = socialProviderControllerObserver;
    }

    public static SocialProviderController getSocialProviderController(Session session, SocialProviderControllerObserver socialProviderControllerObserver, SocialProvider socialProvider) {
        Class a2 = socialProvider.a();
        if (a2 == null) {
            throw new IllegalArgumentException();
        }
        try {
            try {
                SocialProviderController socialProviderController = (SocialProviderController) a2.getConstructor(Session.class, SocialProviderControllerObserver.class).newInstance(session, socialProviderControllerObserver);
                socialProviderController.a(socialProvider);
                return socialProviderController;
            } catch (IllegalArgumentException e2) {
                throw new IllegalStateException(e2);
            } catch (InstantiationException e3) {
                throw new IllegalStateException(e3);
            } catch (IllegalAccessException e4) {
                throw new IllegalStateException(e4);
            } catch (InvocationTargetException e5) {
                throw new IllegalStateException(e5);
            }
        } catch (SecurityException e6) {
            throw new IllegalStateException(e6);
        } catch (NoSuchMethodException e7) {
            throw new IllegalStateException(e7);
        }
    }

    public static SocialProviderController getSocialProviderController(String str, SocialProviderControllerObserver socialProviderControllerObserver) {
        return getSocialProviderController(null, socialProviderControllerObserver, SocialProvider.getSocialProviderForIdentifier(str));
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity, AuthViewController authViewController) {
        if (activity == null || authViewController == null) {
            throw new IllegalArgumentException();
        }
        authViewController.a(activity);
    }

    /* access modifiers changed from: package-private */
    public void a(SocialProvider socialProvider) {
        this.e = socialProvider;
    }

    /* access modifiers changed from: package-private */
    public abstract void b();

    /* access modifiers changed from: package-private */
    public Activity c() {
        return this.c;
    }

    public final void connect(Activity activity) {
        this.c = activity;
        this.b = new A(this);
        if (e() == null || !e().isAuthenticated()) {
            if (this.f47a == null) {
                this.f47a = new U(e(), this.b);
            }
            this.f47a.i();
            return;
        }
        g();
    }

    /* access modifiers changed from: package-private */
    public SocialProviderControllerObserver d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public Session e() {
        return Session.getCurrentSession();
    }

    /* access modifiers changed from: package-private */
    public SocialProvider f() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void g() {
        if (e().getUser().isConnectedToSocialProviderWithIdentifier(f().getIdentifier())) {
            d().didSucceed();
        } else {
            b();
        }
    }
}
