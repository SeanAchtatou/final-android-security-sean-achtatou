package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.scoreloop.client.android.core.controller.a  reason: case insensitive filesystem */
class C0002a extends I {
    private String c;
    private String e;

    public C0002a(RequestCompletionCallback requestCompletionCallback, Game game, User user) {
        super(requestCompletionCallback, game, user);
        this.c = user.getLogin();
        this.e = user.getEmailAddress();
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            this.b.setLogin(this.c);
            this.b.setEmailAddress(this.e);
            jSONObject.put("user", this.b.d());
            return jSONObject;
        } catch (JSONException e2) {
            throw new IllegalStateException("Invalid user data", e2);
        }
    }

    public RequestMethod b() {
        return RequestMethod.PUT;
    }
}
