package com.scoreloop.client.android.core.ui;

import com.scoreloop.client.android.core.ui.MyspaceCredentialsDialog;
import java.net.MalformedURLException;

class d implements MyspaceCredentialsDialog.MyspaceCredentialsListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MySpaceAuthViewController f119a;

    d(MySpaceAuthViewController mySpaceAuthViewController) {
        this.f119a = mySpaceAuthViewController;
    }

    public void a() {
        this.f119a.e.dismiss();
        this.f119a.a().userDidCancel();
    }

    public void a(String str, String str2) {
        try {
            this.f119a.a(str, str2, this.f119a.b("https://roa.myspace.com/roa/09/messaging/login"));
        } catch (MalformedURLException e) {
            throw new IllegalStateException(e);
        }
    }
}
