package com.scoreloop.client.android.core.model;

public class Game implements MessageTarget {
    public static final String CONTEXT_KEY_LEVEL = "SLContextKeyLevel";
    public static final String CONTEXT_KEY_MINOR_RESULT = "SLContextKeyMinorResult";
    public static final String CONTEXT_KEY_MODE = "SLContextKeyMode";

    /* renamed from: a  reason: collision with root package name */
    private String f75a;
    private String b;
    private Integer c;
    private Integer d;
    private Integer e;
    private Integer f;
    private String g;
    private final String h;
    private String i;

    Game(String str, String str2) {
        this.b = str;
        this.h = str2;
    }

    public String a() {
        return this.h;
    }

    public void a(String str) {
        this.f75a = str;
    }

    public String b() {
        return "game";
    }

    public String getIdentifier() {
        return this.b;
    }

    public Integer getLevelCount() {
        if (!hasLevels()) {
            return 1;
        }
        return Integer.valueOf(getMaxLevel().intValue() - getMinLevel().intValue());
    }

    public Integer getMaxLevel() {
        return this.c;
    }

    public Integer getMaxMode() {
        return this.d;
    }

    public Integer getMinLevel() {
        return this.e;
    }

    public Integer getMinMode() {
        return this.f;
    }

    public Integer getModeCount() {
        if (!hasModes()) {
            return 1;
        }
        return Integer.valueOf(getMaxMode().intValue() - getMinMode().intValue());
    }

    public String getName() {
        return this.g;
    }

    public String getVersion() {
        return this.i;
    }

    public boolean hasLevels() {
        return (getMinLevel() == null || getMaxLevel() == null) ? false : true;
    }

    public boolean hasModes() {
        return (getMinMode() == null || getMaxMode() == null) ? false : true;
    }

    public void setMaxLevel(Integer num) {
        this.c = num;
    }

    public void setMaxMode(Integer num) {
        this.d = num;
    }

    public void setMinLevel(Integer num) {
        this.e = num;
    }

    public void setMinMode(Integer num) {
        this.f = num;
    }

    public void setName(String str) {
        this.g = str;
    }

    public void setVersion(String str) {
        this.i = str;
    }
}
