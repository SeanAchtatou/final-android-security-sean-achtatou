package com.scoreloop.client.android.core.ui;

import android.content.DialogInterface;
import com.scoreloop.client.android.core.utils.Logger;

class i implements DialogInterface.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TwitterAuthViewController f124a;

    i(TwitterAuthViewController twitterAuthViewController) {
        this.f124a = twitterAuthViewController;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        Logger.a("twitter auth view controller", "dialog dismissed");
    }
}
