package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import org.json.JSONException;
import org.json.JSONObject;

class D extends Request {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UsersController f34a;
    private final Game b;
    private final Integer c;
    private final boolean e;
    private Integer f;
    private Integer g;
    private User h;
    private JSONObject i;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public D(UsersController usersController, RequestCompletionCallback requestCompletionCallback, Game game, boolean z, Integer num) {
        super(requestCompletionCallback);
        this.f34a = usersController;
        this.b = game;
        this.e = z;
        this.c = num;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("search_list", this.i);
            jSONObject.put("offset", this.g);
            jSONObject.put("per_page", this.f);
            jSONObject.put("limit", this.c);
            if (this.h != null) {
                jSONObject.put("reference_user_id", this.h.getIdentifier());
            }
            return jSONObject;
        } catch (JSONException e2) {
            throw new IllegalStateException("Invalid data", e2);
        }
    }

    public void a(User user) {
        this.h = user;
    }

    public void a(Integer num) {
        this.f = num;
    }

    public RequestMethod b() {
        return RequestMethod.GET;
    }

    public void b(Integer num) {
        this.g = num;
    }

    public void b(JSONObject jSONObject) {
        this.i = jSONObject;
    }

    public String c() {
        if (this.e) {
            return "/service/users";
        }
        return String.format("/service/games/%s/users", this.b.getIdentifier());
    }
}
