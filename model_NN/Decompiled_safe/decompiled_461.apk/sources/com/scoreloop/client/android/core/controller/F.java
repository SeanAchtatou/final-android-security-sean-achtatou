package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.utils.JSONUtils;
import org.json.JSONException;
import org.json.JSONObject;

class F extends I {
    private final Game c;

    public F(RequestCompletionCallback requestCompletionCallback, User user, Game game) {
        super(requestCompletionCallback, game, user);
        this.c = game;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.b != null) {
                jSONObject.put("context", JSONUtils.a(this.b.getContext()));
                jSONObject.put("version", this.b.g());
            }
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException("Invalid user data");
        }
    }

    public RequestMethod b() {
        return RequestMethod.POST;
    }

    public String c() {
        return String.format("/service/games/%s/users/%s/context", this.c.getIdentifier(), this.b.getIdentifier());
    }
}
