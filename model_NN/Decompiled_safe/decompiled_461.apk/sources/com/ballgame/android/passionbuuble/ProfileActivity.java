package com.ballgame.android.passionbuuble;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.ballgame.android.passionbuuble.BaseActivity;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;

public class ProfileActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public EditText emailView;
    /* access modifiers changed from: private */
    public EditText loginView;
    private String oldEmail;
    private String oldLogin;
    /* access modifiers changed from: private */
    public UserController userController;

    private class UserUpdateObserver extends BaseActivity.UserGenericObserver {
        private UserUpdateObserver() {
            super();
        }

        /* synthetic */ UserUpdateObserver(ProfileActivity profileActivity, UserUpdateObserver userUpdateObserver) {
            this();
        }

        public void onEmailAlreadyTaken(UserController controller) {
            super.onEmailAlreadyTaken(controller);
            ProfileActivity.this.setSessionUserToOldData();
            ProfileActivity.this.setTextsToSessionUser();
        }

        public void onEmailInvalidFormat(UserController controller) {
            super.onEmailInvalidFormat(controller);
            ProfileActivity.this.setSessionUserToOldData();
            ProfileActivity.this.setTextsToSessionUser();
        }

        public void onUsernameAlreadyTaken(UserController controller) {
            super.onUsernameAlreadyTaken(controller);
            ProfileActivity.this.setSessionUserToOldData();
            ProfileActivity.this.setTextsToSessionUser();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            super.requestControllerDidFail(requestController, exception);
            ProfileActivity.this.setSessionUserToOldData();
            ProfileActivity.this.setTextsToSessionUser();
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            super.requestControllerDidReceiveResponse(requestController);
            ProfileActivity.this.setTextsToSessionUser();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.profile);
        this.userController = new UserController(new UserUpdateObserver(this, null));
        this.loginView = (EditText) findViewById(R.id.name);
        this.emailView = (EditText) findViewById(R.id.email);
        ((Button) findViewById(R.id.update_profile_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ProfileActivity.this.setOldDataToSessionUser();
                String login = ProfileActivity.this.loginView.getText().toString().trim();
                String email = ProfileActivity.this.emailView.getText().toString().trim();
                ProfileActivity.this.loginView.setText(login);
                ProfileActivity.this.emailView.setText(email);
                User user = Session.getCurrentSession().getUser();
                user.setLogin(login);
                user.setEmailAddress(email);
                ProfileActivity.this.userController.updateUser();
                ProfileActivity.this.showProgressIndicator();
            }
        });
    }

    /* access modifiers changed from: private */
    public void setOldDataToSessionUser() {
        User user = Session.getCurrentSession().getUser();
        this.oldLogin = user.getLogin();
        this.oldEmail = user.getEmailAddress();
    }

    /* access modifiers changed from: private */
    public void setSessionUserToOldData() {
        User user = Session.getCurrentSession().getUser();
        user.setLogin(this.oldLogin);
        user.setEmailAddress(this.oldEmail);
    }

    /* access modifiers changed from: private */
    public void setTextsToSessionUser() {
        User user = Session.getCurrentSession().getUser();
        this.loginView.setText(user.getLogin());
        this.emailView.setText(user.getEmailAddress());
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        setOldDataToSessionUser();
        this.userController.updateUser();
        showProgressIndicator();
    }
}
