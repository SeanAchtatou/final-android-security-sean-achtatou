package com.ballgame.android.passionbuuble;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TabHost;
import android.widget.TextView;
import com.ballgame.android.passionbuuble.utils.ExecutableItem;
import com.scoreloop.client.android.core.controller.ChallengeController;
import com.scoreloop.client.android.core.controller.ChallengeControllerObserver;
import com.scoreloop.client.android.core.controller.RequestCancelledException;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.controller.UserControllerObserver;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.Session;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

abstract class BaseActivity extends ActivityGroup {
    static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private static final int DEFAULT_SEARCH_LISTS_SELECTION = 0;
    static final int DIALOG_ERROR_CANNOT_ACCEPT_CHALLENGE = 5;
    static final int DIALOG_ERROR_CANNOT_CHALLENGE_YOURSELF = 4;
    static final int DIALOG_ERROR_CANNOT_REJECT_CHALLENGE = 6;
    static final int DIALOG_ERROR_CHALLENGE_UPLOAD = 7;
    static final int DIALOG_ERROR_EMAIL_ALREADY_TAKEN = 8;
    static final int DIALOG_ERROR_INSUFFICIENT_BALANCE = 9;
    static final int DIALOG_ERROR_INVALID_EMAIL_FORMAT = 10;
    static final int DIALOG_ERROR_NAME_ALREADY_TAKEN = 11;
    static final int DIALOG_ERROR_NETWORK = 3;
    static final int DIALOG_ERROR_NOT_ON_HIGHSCORE_LIST = 1;
    static final int DIALOG_ERROR_REQUEST_CANCELLED = 2;
    static final String EMPTY_ENTRY = "---";
    private int progressCounter;
    private ProgressDialog progressDialog;

    BaseActivity() {
    }

    class UserGenericObserver implements UserControllerObserver {
        UserGenericObserver() {
        }

        public void onEmailAlreadyTaken(UserController controller) {
            BaseActivity.this.hideProgressIndicatorAndShowDialog(BaseActivity.DIALOG_ERROR_EMAIL_ALREADY_TAKEN);
        }

        public void onEmailInvalidFormat(UserController controller) {
            BaseActivity.this.hideProgressIndicatorAndShowDialog(BaseActivity.DIALOG_ERROR_INVALID_EMAIL_FORMAT);
        }

        public void onUsernameAlreadyTaken(UserController controller) {
            BaseActivity.this.hideProgressIndicatorAndShowDialog(BaseActivity.DIALOG_ERROR_NAME_ALREADY_TAKEN);
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            BaseActivity.this.hideProgressIndicator();
            if (!BaseActivity.this.isRequestCancellation(exception)) {
                BaseActivity.this.showDialog(3);
            }
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            BaseActivity.this.hideProgressIndicator();
        }
    }

    private class UserUpdateObserver extends UserGenericObserver {
        private UserUpdateObserver() {
            super();
        }

        /* synthetic */ UserUpdateObserver(BaseActivity baseActivity, UserUpdateObserver userUpdateObserver) {
            this();
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            BaseActivity.this.hideProgressIndicator();
            BaseActivity.this.onSearchListsAvailable();
        }
    }

    class ChallengeGenericObserver implements ChallengeControllerObserver {
        ChallengeGenericObserver() {
        }

        public void onCannotAcceptChallenge(ChallengeController challengeController) {
            BaseActivity.this.hideProgressIndicatorAndShowDialog(5);
        }

        public void onCannotRejectChallenge(ChallengeController challengeController) {
            BaseActivity.this.hideProgressIndicatorAndShowDialog(6);
        }

        public void onInsufficientBalance(ChallengeController challengeController) {
            BaseActivity.this.hideProgressIndicatorAndShowDialog(BaseActivity.DIALOG_ERROR_INSUFFICIENT_BALANCE);
        }

        public void requestControllerDidFail(RequestController aRequestController, Exception anException) {
            BaseActivity.this.hideProgressIndicator();
            if (!BaseActivity.this.isRequestCancellation(anException)) {
                BaseActivity.this.showDialog(3);
            }
        }

        public void requestControllerDidReceiveResponse(RequestController aRequestController) {
            BaseActivity.this.hideProgressIndicator();
        }
    }

    class MenuListAdapter extends ArrayAdapter<ExecutableItem> {
        public MenuListAdapter(Context context, int resource, List<ExecutableItem> objects) {
            super(context, resource, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = BaseActivity.this.getLayoutInflater().inflate((int) R.layout.menu_item, (ViewGroup) null);
            }
            final ExecutableItem item = (ExecutableItem) getItem(position);
            ((TextView) view.findViewById(R.id.label)).setText(item.getLabelId());
            if (item.getListener() != null) {
                view.setOnClickListener(item.getListener());
            } else {
                view.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        BaseActivity.this.menuItemOnClick(item.getTargetClass());
                    }
                });
            }
            return view;
        }
    }

    /* access modifiers changed from: package-private */
    public void onSearchListsAvailable() {
    }

    private Dialog createErrorDialog(int resId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(resId).setCancelable(true);
        Dialog dialog = builder.create();
        dialog.getWindow().requestFeature(1);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    /* access modifiers changed from: package-private */
    public String formatMoney(Money money) {
        return String.format(getString(R.string.money_format), Integer.valueOf(money.getAmount().intValue() / 100), Integer.valueOf(money.getAmount().intValue() % 100));
    }

    /* access modifiers changed from: private */
    public void hideProgressIndicatorAndShowDialog(int dialogId) {
        hideProgressIndicator();
        showDialog(dialogId);
    }

    /* access modifiers changed from: package-private */
    public void showInfoDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message).setCancelable(true);
        Dialog dialog = builder.create();
        dialog.getWindow().requestFeature(1);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    private void initMenuView(ListView menuList, ExecutableItem[] items) {
        menuList.setAdapter((ListAdapter) new MenuListAdapter(this, R.layout.menu, Arrays.asList(items)));
    }

    private void initProgressIndicator() {
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setCancelable(false);
        this.progressCounter = 0;
    }

    private TabHost.TabSpec initTabSpec(TabHost tabHost, String tag, int labelId) {
        TabHost.TabSpec spec = tabHost.newTabSpec(tag);
        spec.setIndicator(getString(labelId));
        return spec;
    }

    /* access modifiers changed from: private */
    public void menuItemOnClick(Class<? extends Activity> targetClass) {
        startActivity(new Intent(this, targetClass));
    }

    /* access modifiers changed from: package-private */
    public void hideProgressIndicator() {
        synchronized (this.progressDialog) {
            this.progressCounter--;
            if (this.progressCounter == 0) {
                this.progressDialog.hide();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void initMenu(ExecutableItem[] items) {
        initMenuView((ListView) findViewById(R.id.menu), items);
    }

    /* access modifiers changed from: package-private */
    public void initMenu(ExecutableItem[] items, View layout) {
        initMenuView((ListView) layout.findViewById(R.id.menu), items);
    }

    /* access modifiers changed from: package-private */
    public void initTab(TabHost tabHost, String tag, Intent intent, int labelId) {
        TabHost.TabSpec spec = initTabSpec(tabHost, tag, labelId);
        spec.setContent(intent);
        tabHost.addTab(spec);
    }

    /* access modifiers changed from: package-private */
    public void initTab(TabHost tabHost, String tag, TabHost.TabContentFactory contentFactory, int labelId) {
        TabHost.TabSpec spec = initTabSpec(tabHost, tag, labelId);
        spec.setContent(contentFactory);
        tabHost.addTab(spec);
    }

    /* access modifiers changed from: package-private */
    public <T> boolean isEmpty(T t) {
        return t == null || "".equals(t.toString().trim());
    }

    /* access modifiers changed from: package-private */
    public boolean isRequestCancellation(Exception e) {
        if (!(e instanceof RequestCancelledException)) {
            return false;
        }
        showDialog(2);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        initProgressIndicator();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return createErrorDialog(R.string.error_message_not_on_highscore_list);
            case 2:
                return createErrorDialog(R.string.error_message_request_cancelled);
            case 3:
                return createErrorDialog(R.string.error_message_network);
            case 4:
                return createErrorDialog(R.string.error_message_self_challenge);
            case 5:
                return createErrorDialog(R.string.error_message_cannot_accept_challenge);
            case 6:
                return createErrorDialog(R.string.error_message_cannot_reject_challenge);
            case 7:
                return createErrorDialog(R.string.error_message_challenge_upload);
            case DIALOG_ERROR_EMAIL_ALREADY_TAKEN /*8*/:
                return createErrorDialog(R.string.error_message_email_already_taken);
            case DIALOG_ERROR_INSUFFICIENT_BALANCE /*9*/:
                return createErrorDialog(R.string.error_message_insufficient_balance);
            case DIALOG_ERROR_INVALID_EMAIL_FORMAT /*10*/:
                return createErrorDialog(R.string.error_message_invalid_email_format);
            case DIALOG_ERROR_NAME_ALREADY_TAKEN /*11*/:
                return createErrorDialog(R.string.error_message_name_already_taken);
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.progressDialog.dismiss();
    }

    /* access modifiers changed from: package-private */
    public void requestSearchLists() {
        new UserController(new UserUpdateObserver(this, null)).updateUser();
        showProgressIndicator();
    }

    /* access modifiers changed from: package-private */
    public void showProgressIndicator() {
        showProgressIndicator(R.string.progress_message_default);
    }

    /* access modifiers changed from: package-private */
    public void showProgressIndicator(int messageId) {
        synchronized (this.progressDialog) {
            this.progressCounter++;
            this.progressDialog.setMessage(getString(messageId));
            this.progressDialog.show();
        }
    }

    /* access modifiers changed from: package-private */
    public Spinner getGameModeChooser(Integer selectedMode, boolean enabled) {
        ArrayAdapter<?> adapter = ArrayAdapter.createFromResource(this, R.array.game_modes, 17367048);
        adapter.setDropDownViewResource(17367049);
        Spinner s = (Spinner) findViewById(R.id.game_mode);
        s.setAdapter((SpinnerAdapter) adapter);
        s.setSelection(selectedMode != null ? selectedMode.intValue() : 0);
        s.setEnabled(enabled);
        return s;
    }

    /* access modifiers changed from: package-private */
    public void initSearchListChooser(AdapterView.OnItemSelectedListener listener) {
        Spinner s = (Spinner) findViewById(R.id.search_list_spinner);
        ArrayAdapter<SearchList> adapter = new ArrayAdapter<>(this, 17367048, Session.getCurrentSession().getScoreSearchLists());
        adapter.setDropDownViewResource(17367049);
        s.setAdapter((SpinnerAdapter) adapter);
        s.setSelection(0);
        if (listener != null) {
            s.setOnItemSelectedListener(listener);
        }
    }
}
