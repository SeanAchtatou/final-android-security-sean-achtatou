package com.ballgame.android.passionbuuble;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import com.ballgame.android.passionbuuble.MyTimer;
import java.text.DecimalFormat;

public class BBView extends View implements MyTimer.CallBack {
    public static int mDiplayHeight = 60;
    public static int mScore;
    public static long mStartTime;
    public static int mViewHeight;
    public static int mViewWidth;
    public static int mXOffset;
    public static int mYOffset;
    public boolean bSound = true;
    public int hitOkSfx;
    public int longhit;
    public BBMatrix mBBMatrix;
    public Bitmap mBitmap;
    public Bitmap mBitmap1;
    public Bitmap mBitmap2;
    public Bitmap[] mBubbleArray;
    public Canvas mCanvas = new Canvas();
    public int mMode = 1;
    public View mOverView;
    public final Paint mPaint = new Paint();
    public Bubble mRefMain;
    private ScoreBubble mScoreBubble;
    public Bitmap mSelectmp;
    public TextView mStatusText;
    public MyTimer mTimer = null;
    public Paint mpt;
    public SoundPool snd = null;
    public int streamVolume;

    public void timerCallBack() {
        if (2 == this.mMode) {
            refreshPlayground();
            invalidate();
        }
    }

    public void RefeshDrawScore(Canvas pCan) {
        pCan.drawBitmap(this.mBitmap1, 0.0f, 0.0f, this.mPaint);
        this.mpt.setColor(-1);
        this.mpt.setTextSize(14.0f);
        pCan.drawText(new StringBuilder().append(mScore).toString(), 17.0f, 37.0f, this.mpt);
        DecimalFormat format = new DecimalFormat("00");
        int lsl = (int) ((System.currentTimeMillis() - mStartTime) / 1000);
        pCan.drawText(format.format((long) (lsl / 60)) + ":" + format.format((long) (lsl % 60)), 272.0f, 37.0f, this.mpt);
    }

    public void RefeshDrawTime() {
        if (2 == this.mMode) {
            this.mBitmap1 = Bitmap.createBitmap(mViewWidth, mDiplayHeight, Bitmap.Config.ARGB_8888);
            this.mCanvas.setBitmap(this.mBitmap1);
            if (this.mBitmap1 != null) {
                this.mCanvas.drawBitmap(this.mBitmap1, 0.0f, 0.0f, (Paint) null);
            }
            Paint pt = new Paint();
            pt.setColor(-65536);
            pt.setTextSize(14.0f);
            this.mCanvas.drawText("Time:" + ((System.currentTimeMillis() - mStartTime) / 1000) + " second", 5.0f, 40.0f, pt);
            invalidate();
        }
    }

    public void RefeshDrawPush(Canvas pCan) {
        this.mpt.setARGB(77, 77, 128, 179);
        pCan.drawRect(1.0f, (float) ((mViewHeight - mDiplayHeight) + 10), (float) mViewWidth, (float) mViewHeight, this.mpt);
    }

    public BBView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initBBView();
    }

    public BBView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initBBView();
    }

    public void initBBView() {
        setFocusable(true);
        Resources r = getContext().getResources();
        resetBubbles(5);
        loadBubble(0, r.getDrawable(R.drawable.skin1_block_1a));
        loadBubble(1, r.getDrawable(R.drawable.skin1_block_2a));
        loadBubble(2, r.getDrawable(R.drawable.skin1_block_3a));
        loadBubble(3, r.getDrawable(R.drawable.skin1_block_4a));
        loadBubble(4, r.getDrawable(R.drawable.skin1_block_5a));
        this.snd = null;
        this.mpt = new Paint();
        this.mpt.setFlags(1);
        this.mpt.setTextSize(20.0f);
        this.mpt.setColor(-16711936);
        this.mTimer = new MyTimer(30, this);
        this.mTimer.startTimer();
        this.mBitmap = null;
        this.mBitmap1 = null;
        this.mBitmap2 = null;
        this.mScoreBubble = new ScoreBubble(320, 480);
        this.mScoreBubble.setFont(Typeface.createFromAsset(getContext().getAssets(), "fonts/ARCENA.ttf"));
    }

    public void clearsound() {
        if (this.snd != null) {
            this.snd.release();
            this.snd = null;
        }
    }

    public void playsound(int samenum) {
        if (this.bSound) {
            if (this.snd == null) {
                this.snd = new SoundPool(5, 3, 100);
                this.streamVolume = ((AudioManager) getContext().getSystemService("audio")).getStreamVolume(3);
                this.hitOkSfx = this.snd.load(getContext(), R.raw.click, 1);
                this.longhit = this.snd.load(getContext(), R.raw.lclick, 1);
            }
            if (this.snd == null) {
                return;
            }
            if (samenum < 6 && samenum > 0) {
                this.snd.play(this.hitOkSfx, (float) this.streamVolume, (float) this.streamVolume, 1, 0, 1.0f);
            } else if (samenum >= 6) {
                this.snd.play(this.longhit, (float) this.streamVolume, (float) this.streamVolume, 1, 0, 1.0f);
            }
        }
    }

    public void setMode(int newMode) {
        int oldMode = this.mMode;
        this.mMode = newMode;
        if ((newMode == 2) && (oldMode != 2)) {
            this.mStatusText.setVisibility(4);
            invalidate();
            return;
        }
        Resources res = getContext().getResources();
        CharSequence str = "";
        if (newMode == 0) {
            str = res.getText(R.string.mode_pause);
        }
        if (newMode == 1) {
            str = res.getText(R.string.mode_ready);
        }
        if (newMode == 3) {
            str = String.valueOf(res.getString(R.string.mode_lose_prefix)) + mScore + res.getString(R.string.mode_lose_suffix);
        }
        if (newMode == 3) {
            Drawable drawable = getContext().getResources().getDrawable(R.drawable.icon);
            new AlertDialog.Builder(getContext()).setMessage("Score:  " + mScore).setTitle("Game Over").setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                    BBView.this.mRefMain.stopMusic();
                    BBView.this.mRefMain.ToMainView(BBView.mScore);
                }
            }).setNeutralButton("Replay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                    BBView.this.newGame();
                    BBView.this.setMode(2);
                }
            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                    BBView.this.mRefMain.stopMusic();
                    BBView.this.mRefMain.ToFirstView();
                }
            }).create().show();
            return;
        }
        this.mStatusText.setText(str);
        this.mStatusText.setVisibility(0);
    }

    public void freeAlloc() {
        if (!this.mBitmap2.isRecycled()) {
            this.mBitmap2.recycle();
        }
        if (!this.mBitmap.isRecycled()) {
            this.mBitmap.recycle();
        }
        if (!this.mBitmap1.isRecycled()) {
            this.mBitmap1.recycle();
        }
    }

    public void newGame() {
        mScore = 0;
        this.mBBMatrix = new BBMatrix(mViewWidth, mViewHeight - (mDiplayHeight * 2));
        mXOffset = 0;
        mYOffset = mDiplayHeight + 1;
        mStartTime = System.currentTimeMillis();
        if (this.mBitmap2 == null) {
            Drawable bubble = getContext().getResources().getDrawable(R.drawable.gmbk);
            Bitmap bitmap = Bitmap.createBitmap(mViewWidth, mViewHeight, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            bubble.setBounds(0, 0, mViewWidth, mViewHeight);
            bubble.draw(canvas);
            this.mBitmap2 = bitmap;
        }
        if (this.mBitmap == null) {
            this.mBitmap = Bitmap.createBitmap(mViewWidth, mViewHeight, Bitmap.Config.ARGB_8888);
        }
        if (this.mBitmap1 == null) {
            Drawable bubble2 = getContext().getResources().getDrawable(R.drawable.top);
            Bitmap bitmap2 = Bitmap.createBitmap(mViewWidth, 43, Bitmap.Config.ARGB_8888);
            Canvas canvas2 = new Canvas(bitmap2);
            bubble2.setBounds(0, 0, mViewWidth, 43);
            bubble2.draw(canvas2);
            this.mBitmap1 = bitmap2;
        }
        this.mCanvas.setBitmap(this.mBitmap);
        refreshPlayground();
        playsound(-1);
        this.mRefMain.playMusic();
    }

    public void loadBubble(int key, Drawable bubble) {
        Bitmap bitmap = Bitmap.createBitmap(30, 30, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        bubble.setBounds(0, 0, 30, 30);
        bubble.draw(canvas);
        this.mBubbleArray[key] = bitmap;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        int i;
        mViewWidth = this.mBitmap != null ? this.mBitmap.getWidth() : 0;
        if (this.mBitmap != null) {
            i = this.mBitmap.getHeight();
        } else {
            i = 0;
        }
        mViewHeight = i;
        if (mViewWidth < w || mViewHeight < h) {
            if (mViewWidth < w) {
                mViewWidth = w;
            }
            if (mViewHeight < h) {
                mViewHeight = h;
            }
            newGame();
            setMode(2);
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        Log.d("bacon", "onTouchEvent X=" + event.getX() + " Y=" + event.getY() + " action=" + action);
        if (action != 1 || this.mMode != 2) {
            return true;
        }
        int curX = (int) Math.floor((double) event.getX());
        int curY = (int) Math.floor((double) event.getY());
        int bubbleX = (curX - mXOffset) / 32;
        int bubbleY = (curY - mYOffset) / 32;
        this.mBBMatrix.mSameBubbleCount = 0;
        this.mBBMatrix.findSameBubble(bubbleX, bubbleY);
        if (this.mBBMatrix.mSameBubbleCount > 1) {
            playsound(this.mBBMatrix.mSameBubbleCount);
            int hitCount = this.mBBMatrix.mSameBubbleCount;
            int nowScore = calculateScore(hitCount);
            this.mScoreBubble.Add(curX, curY, hitCount, nowScore);
            Log.v("BBView", "Add Score Bubble");
            mScore += nowScore;
            this.mBBMatrix.removeMarkedBubbles();
            refreshPlayground();
            this.mBBMatrix.removeMark();
            invalidate();
        }
        if (!this.mBBMatrix.isBBMatrixSolvable()) {
            setMode(3);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (2 == this.mMode && this.mBitmap != null) {
            canvas.drawBitmap(this.mBitmap, 0.0f, 0.0f, (Paint) null);
        }
    }

    public void refreshPlayground() {
        this.mCanvas.drawBitmap(this.mBitmap2, 0.0f, 0.0f, this.mPaint);
        RefeshDrawScore(this.mCanvas);
        for (int i = 0; i < this.mBBMatrix.mXBubbleCount; i++) {
            for (int j = 0; j < this.mBBMatrix.mYBubbleCount; j++) {
                if (this.mBBMatrix.mBubbleGrid[i][j] != -1) {
                    this.mCanvas.drawBitmap(this.mBubbleArray[this.mBBMatrix.mBubbleGrid[i][j]], (float) (mXOffset + (i * 32)), (float) (mYOffset + (j * 32)), this.mPaint);
                }
            }
        }
        this.mScoreBubble.show(this.mCanvas, this.mPaint);
    }

    public int calculateScore(int pSameBubbleCount) {
        int result = 1;
        if (pSameBubbleCount <= 1) {
            return 0;
        }
        for (int i = 2; i <= pSameBubbleCount; i++) {
            result += i * 2;
        }
        return result;
    }

    public void resetBubbles(int pBubbleCount) {
        this.mBubbleArray = new Bitmap[pBubbleCount];
    }

    public void setTextView(TextView newView) {
        this.mStatusText = newView;
    }

    public void setMain(Bubble newMain) {
        this.mRefMain = newMain;
    }
}
