package com.ballgame.android.passionbuuble.utils;

import android.app.Activity;
import android.view.View;

public class ExecutableItem {
    private final int labelId;
    private final View.OnClickListener listener;
    private final Class<? extends Activity> targetClass;

    public ExecutableItem(int labelId2, Class<? extends Activity> targetClass2) {
        this(labelId2, targetClass2, null);
    }

    public ExecutableItem(int labelId2, View.OnClickListener listener2) {
        this(labelId2, null, listener2);
    }

    private ExecutableItem(int labelId2, Class<? extends Activity> targetClass2, View.OnClickListener listener2) {
        this.labelId = labelId2;
        this.targetClass = targetClass2;
        this.listener = listener2;
    }

    public int getLabelId() {
        return this.labelId;
    }

    public View.OnClickListener getListener() {
        return this.listener;
    }

    public Class<? extends Activity> getTargetClass() {
        return this.targetClass;
    }
}
