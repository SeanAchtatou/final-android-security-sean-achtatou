package com.ballgame.android.passionbuuble;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class BBMain extends Activity implements View.OnClickListener {
    static final int RG_REQUEST = 0;
    private BBView mBBView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        ((Button) findViewById(R.id.Button01)).setOnClickListener(this);
        ((Button) findViewById(R.id.Button02)).setOnClickListener(this);
    }

    public void ToGameView() {
        setContentView((int) R.layout.bb_layout);
        this.mBBView = (BBView) findViewById(R.id.bubblebreaker);
        this.mBBView.setTextView((TextView) findViewById(R.id.text));
        this.mBBView.setMode(1);
    }

    public void ToMainView() {
        setContentView((int) R.layout.main);
    }

    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.Button01 /*2131099703*/:
                startActivityForResult(new Intent(this, Bubble.class), 0);
                return;
            case R.id.Button02 /*2131099704*/:
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != 0) {
            return;
        }
        if (resultCode == 0) {
            setTitle("Canceled...");
        } else if (resultCode == -1) {
            setTitle("game over...");
        }
    }
}
