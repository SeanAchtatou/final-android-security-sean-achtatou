package com.ballgame.android.passionbuuble;

import android.app.Application;

public class SLDemoApplication extends Application {
    private static final String GAME_ID = "ff143c51-87bd-4a40-8e27-71afb8c265fa";
    static final int GAME_MODE_COUNT = 1;
    static final int GAME_MODE_MIN = 0;
    private static final String GAME_SECRET = "G3WroMSW/0Ggow1lkE7hw5pgWp+UX0ZG46lIkALSFAXM5gSn3AlxmA==";

    public void onCreate() {
        super.onCreate();
        ScoreloopManager.init(this, GAME_ID, GAME_SECRET);
    }
}
