package com.ballgame.android.passionbuuble;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import com.ballgame.android.passionbuuble.utils.ExecutableItem;
import com.scoreloop.client.android.core.controller.ChallengesController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import java.util.List;

public class ChallengesActivity extends BaseActivity {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$ballgame$android$passionbuuble$ChallengesActivity$Tabs = null;
    static final int DEFAULT_SCORE_VALUE = 0;
    static final String EXTRA_HIGH_SCORES_SELECTION_MODE = "HIGH_SCORES_SELECTION_MODE";
    /* access modifiers changed from: private */
    public ChallengesController challengesController;
    /* access modifiers changed from: private */
    public ListView historyListView;
    private boolean initialized;
    private Tabs lastTab;
    /* access modifiers changed from: private */
    public ListView openListView;
    private TabHost tabHost;

    enum NewChallengeModes {
        NONE,
        OPEN,
        DIRECT
    }

    enum Tabs {
        HISTORY,
        NEW,
        OPEN
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$ballgame$android$passionbuuble$ChallengesActivity$Tabs() {
        int[] iArr = $SWITCH_TABLE$com$ballgame$android$passionbuuble$ChallengesActivity$Tabs;
        if (iArr == null) {
            iArr = new int[Tabs.values().length];
            try {
                iArr[Tabs.HISTORY.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Tabs.NEW.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Tabs.OPEN.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$ballgame$android$passionbuuble$ChallengesActivity$Tabs = iArr;
        }
        return iArr;
    }

    private class ChallengesControllerObserver implements RequestControllerObserver {
        private ChallengesControllerObserver() {
        }

        /* synthetic */ ChallengesControllerObserver(ChallengesActivity challengesActivity, ChallengesControllerObserver challengesControllerObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            ChallengesActivity.this.hideProgressIndicator();
            if (!ChallengesActivity.this.isRequestCancellation(exception)) {
                ChallengesActivity.this.showDialog(3);
            }
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            if (ChallengesActivity.this.isCurrentTabOpen() && ChallengesActivity.this.openListView != null) {
                ChallengesActivity.this.openListView.setAdapter((ListAdapter) new OpenChallengeViewAdapter(ChallengesActivity.this, R.layout.challenges_open_list, ChallengesActivity.this.challengesController.getChallenges()));
            } else if (ChallengesActivity.this.isCurrentTabHistory() && ChallengesActivity.this.historyListView != null) {
                ChallengesActivity.this.historyListView.setAdapter((ListAdapter) new HistoryChallengeViewAdapter(ChallengesActivity.this, R.layout.challenges_history_list, ChallengesActivity.this.challengesController.getChallenges()));
            }
            ChallengesActivity.this.hideProgressIndicator();
        }
    }

    private class HistoryChallengeViewAdapter extends ArrayAdapter<Challenge> {
        public HistoryChallengeViewAdapter(Context context, int resource, List<Challenge> objects) {
            super(context, resource, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = ChallengesActivity.this.getLayoutInflater().inflate((int) R.layout.challenges_history_list_item, (ViewGroup) null);
            }
            Challenge challenge = (Challenge) getItem(position);
            setUsernameForRow(view, challenge);
            setStakeTextForRow(view, challenge);
            setStatusTextForRow(view, challenge);
            return view;
        }

        private void setStakeTextForRow(View rowView, Challenge challenge) {
            ((TextView) rowView.findViewById(R.id.stake)).setText(ChallengesActivity.this.formatMoney(challenge.getStake()));
        }

        private void setStatusTextForRow(View rowView, Challenge challenge) {
            TextView challengeStatus = (TextView) rowView.findViewById(R.id.status);
            String status = ChallengesActivity.this.getString(R.string.challenge_other_cap);
            if (challenge.isOpen()) {
                status = ChallengesActivity.this.getString(R.string.challenge_pending_cap);
            }
            if (challenge.isAssigned()) {
                status = ChallengesActivity.this.getString(R.string.challenge_pending_cap);
            }
            if (challenge.isRejected()) {
                status = ChallengesActivity.this.getString(R.string.challenge_rejected_cap);
            }
            if (challenge.isComplete()) {
                if (challenge.isWinner(Session.getCurrentSession().getUser())) {
                    status = ChallengesActivity.this.getString(R.string.won);
                } else {
                    status = ChallengesActivity.this.getString(R.string.lost);
                }
            }
            challengeStatus.setText(status);
        }

        private void setUsernameForRow(View rowView, Challenge challenge) {
            TextView challengeName = (TextView) rowView.findViewById(R.id.name);
            String name = ChallengesActivity.this.getString(R.string.challenge_invalid_cap);
            User contender = challenge.getContender();
            User contestant = challenge.getContestant();
            User currentUser = Session.getCurrentSession().getUser();
            if (contestant == null) {
                name = ChallengesActivity.this.getString(R.string.challenge_anyone_cap);
            }
            if (currentUser.equals(contender) && contestant != null) {
                name = contestant.getLogin();
            }
            if (currentUser.equals(contestant) && contender != null) {
                name = contender.getLogin();
            }
            challengeName.setText(name);
        }
    }

    private class OnHistoryChallengeClickListener implements AdapterView.OnItemClickListener {
        static final /* synthetic */ boolean $assertionsDisabled = (!ChallengesActivity.class.desiredAssertionStatus());

        private OnHistoryChallengeClickListener() {
        }

        /* synthetic */ OnHistoryChallengeClickListener(ChallengesActivity challengesActivity, OnHistoryChallengeClickListener onHistoryChallengeClickListener) {
            this();
        }

        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            Challenge challenge = (Challenge) adapter.getItemAtPosition(position);
            if (!$assertionsDisabled && challenge == null) {
                throw new AssertionError();
            } else if ($assertionsDisabled || challenge.getContender() != null) {
                String string = ChallengesActivity.this.getString(R.string.history_challenge_info_format);
                Object[] objArr = new Object[2];
                objArr[0] = ChallengesActivity.DEFAULT_DATE_FORMAT.format(challenge.getCreatedAt());
                objArr[1] = ChallengesActivity.this.isEmpty(challenge.getContestantSkill()) ? "---" : challenge.getContestantSkill();
                ChallengesActivity.this.showInfoDialog(String.format(string, objArr));
            } else {
                throw new AssertionError();
            }
        }
    }

    private class OnOpenChallengeClickListener implements AdapterView.OnItemClickListener {
        static final /* synthetic */ boolean $assertionsDisabled = (!ChallengesActivity.class.desiredAssertionStatus());

        private OnOpenChallengeClickListener() {
        }

        /* synthetic */ OnOpenChallengeClickListener(ChallengesActivity challengesActivity, OnOpenChallengeClickListener onOpenChallengeClickListener) {
            this();
        }

        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            Challenge tappedChallenge = (Challenge) adapter.getItemAtPosition(position);
            if (!$assertionsDisabled && tappedChallenge == null) {
                throw new AssertionError();
            } else if ($assertionsDisabled || tappedChallenge.getContender() != null) {
                Session session = Session.getCurrentSession();
                User currentUser = session.getUser();
                User possibleContender = tappedChallenge.getContender();
                if (possibleContender.equals(currentUser)) {
                    ChallengesActivity.this.showDialog(4);
                } else if (session.getBalance().compareTo(tappedChallenge.getStake()) < 0) {
                    ChallengesActivity.this.showDialog(9);
                } else {
                    tappedChallenge.setContestant(currentUser);
                    tappedChallenge.setContender(possibleContender);
                    ScoreloopManager.setCurrentChallenge(tappedChallenge);
                    ChallengesActivity.this.startActivity(new Intent(ChallengesActivity.this, ChallengeConfirmActivity.class));
                }
            } else {
                throw new AssertionError();
            }
        }
    }

    private class OpenChallengeViewAdapter extends ArrayAdapter<Challenge> {
        public OpenChallengeViewAdapter(Context context, int resource, List<Challenge> objects) {
            super(context, resource, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = ChallengesActivity.this.getLayoutInflater().inflate((int) R.layout.challenges_open_list_item, (ViewGroup) null);
            }
            Challenge challenge = (Challenge) getItem(position);
            ((TextView) view.findViewById(R.id.name)).setText(challenge.getContender().getLogin());
            ((TextView) view.findViewById(R.id.stake)).setText(ChallengesActivity.this.formatMoney(challenge.getStake()));
            return view;
        }
    }

    private void clearListViews() {
        if (this.openListView != null) {
            this.openListView.setAdapter((ListAdapter) null);
        }
        if (this.historyListView != null) {
            this.historyListView.setAdapter((ListAdapter) null);
        }
    }

    /* access modifiers changed from: private */
    public View initHistoryChallengesLayout() {
        View layout = getLayoutInflater().inflate((int) R.layout.challenges_history_list, (ViewGroup) null);
        this.historyListView = (ListView) layout.findViewById(R.id.challenges_list);
        this.historyListView.setOnItemClickListener(new OnHistoryChallengeClickListener(this, null));
        return layout;
    }

    /* access modifiers changed from: private */
    public View initNewChallengeLayout() {
        View layout = getLayoutInflater().inflate((int) R.layout.chmenu, (ViewGroup) null);
        initMenu(new ExecutableItem[]{new ExecutableItem((int) R.string.challenge_new_menu_item_open, OpenChallengeActivity.class), new ExecutableItem((int) R.string.challenge_new_menu_item_direct, DirectChallengeActivity.class)}, layout);
        return layout;
    }

    /* access modifiers changed from: private */
    public View initOpenChallengesLayout() {
        View layout = getLayoutInflater().inflate((int) R.layout.challenges_open_list, (ViewGroup) null);
        this.openListView = (ListView) layout.findViewById(R.id.challenges_list);
        this.openListView.setOnItemClickListener(new OnOpenChallengeClickListener(this, null));
        return layout;
    }

    private void initTabs() {
        this.tabHost = (TabHost) findViewById(R.id.tabhost);
        this.tabHost.setup();
        this.tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            public void onTabChanged(String tabId) {
                ChallengesActivity.this.tabChanged();
            }
        });
        initTab(this.tabHost, Tabs.NEW.name(), new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return ChallengesActivity.this.initNewChallengeLayout();
            }
        }, (int) R.string.challenge_tab_new);
        initTab(this.tabHost, Tabs.OPEN.name(), new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return ChallengesActivity.this.initOpenChallengesLayout();
            }
        }, (int) R.string.challenge_tab_open);
        initTab(this.tabHost, Tabs.HISTORY.name(), new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return ChallengesActivity.this.initHistoryChallengesLayout();
            }
        }, (int) R.string.challenge_tab_history);
    }

    /* access modifiers changed from: private */
    public boolean isCurrentTabHistory() {
        return this.tabHost.getCurrentTabTag().equals(Tabs.HISTORY.name());
    }

    /* access modifiers changed from: private */
    public boolean isCurrentTabOpen() {
        return this.tabHost.getCurrentTabTag().equals(Tabs.OPEN.name());
    }

    private void onTabHistory() {
        showProgressIndicator(R.string.progress_message_default);
        clearListViews();
        this.challengesController.requestChallengeHistory();
    }

    private void onTabOpen() {
        showProgressIndicator(R.string.progress_message_default);
        clearListViews();
        this.challengesController.requestOpenChallenges();
    }

    private void restoreTab() {
        if (this.initialized) {
            this.tabHost.setCurrentTabByTag(this.lastTab.name());
            tabChanged();
        }
    }

    /* access modifiers changed from: private */
    public void tabChanged() {
        Tabs currentTab = Tabs.valueOf(this.tabHost.getCurrentTabTag());
        switch ($SWITCH_TABLE$com$ballgame$android$passionbuuble$ChallengesActivity$Tabs()[currentTab.ordinal()]) {
            case 1:
                onTabHistory();
                break;
            case 2:
                break;
            case 3:
                onTabOpen();
                break;
            default:
                throw new IllegalStateException("Unhandled tab tag: " + currentTab.name());
        }
        this.lastTab = currentTab;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.challenges);
        this.challengesController = new ChallengesController(new ChallengesControllerObserver(this, null));
        initTabs();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        restoreTab();
        this.initialized = true;
    }
}
