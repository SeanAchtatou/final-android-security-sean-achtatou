package com.ballgame.android.passionbuuble;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class MenuActivity extends Activity implements View.OnClickListener {
    private final String SL_GAME_ID = "ff143c51-87bd-4a40-8e27-71afb8c265fa";
    private final String SL_GAME_SECRET = "G3WroMSW/0Ggow1lkE7hw5pgWp+UX0ZG46lIkALSFAXM5gSn3AlxmA==";
    private Window window;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GameAd.init(0);
        requestWindowFeature(1);
        this.window = getWindow();
        this.window.setFlags(1024, 1024);
        setContentView((int) R.layout.splash);
        ((Button) findViewById(R.id.start_game)).setOnClickListener(this);
        ((Button) findViewById(R.id.score_board)).setOnClickListener(this);
        ((Button) findViewById(R.id.profile)).setOnClickListener(this);
        ((Button) findViewById(R.id.exit)).setOnClickListener(this);
        initScoreLoop();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        startActivity(new Intent(this, AdSplash.class));
        finish();
        return true;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_game /*2131099726*/:
                startActivity(new Intent(this, Bubble.class));
                return;
            case R.id.score_board /*2131099727*/:
                startActivity(new Intent(this, HighscoresActivity.class));
                return;
            case R.id.profile /*2131099728*/:
                startActivity(new Intent(this, ProfileActivity.class));
                return;
            case R.id.exit /*2131099729*/:
                startActivity(new Intent(this, AdSplash.class));
                finish();
                return;
            default:
                return;
        }
    }

    private void initScoreLoop() {
        ScoreloopManager.init(this, "ff143c51-87bd-4a40-8e27-71afb8c265fa", "G3WroMSW/0Ggow1lkE7hw5pgWp+UX0ZG46lIkALSFAXM5gSn3AlxmA==");
        ScoreloopManager.setNumberOfModes(2);
    }
}
