package defpackage;

import com.google.ads.util.a;
import java.lang.ref.WeakReference;

/* renamed from: x  reason: default package */
public final class x implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference<d> f141a;

    public x(d dVar) {
        this.f141a = new WeakReference<>(dVar);
    }

    public final void run() {
        d dVar = this.f141a.get();
        if (dVar == null) {
            a.a("The ad must be gone, so cancelling the refresh timer.");
        } else {
            dVar.x();
        }
    }
}
