package defpackage;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.InterstitialAd;
import com.google.ads.util.AdUtil;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

/* renamed from: c  reason: default package */
public final class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private String f3a;
    private String b = null;
    private b c;
    private d d;
    private AdRequest e;
    private WebView f;
    private String g = null;
    private LinkedList<String> h = new LinkedList<>();
    private volatile boolean i;
    private AdRequest.ErrorCode j = null;
    private boolean k = false;
    private int l = -1;
    private Thread m;

    /* renamed from: c$a */
    private class a implements Runnable {
        private final d b;
        private final WebView c;
        private final b d;
        private final AdRequest.ErrorCode e;
        private final boolean f;

        public a(d dVar, WebView webView, b bVar, AdRequest.ErrorCode errorCode, boolean z) {
            this.b = dVar;
            this.c = webView;
            this.d = bVar;
            this.e = errorCode;
            this.f = z;
        }

        public final void run() {
            this.c.stopLoading();
            this.c.destroy();
            this.d.a();
            if (this.f) {
                g i = this.b.i();
                i.stopLoading();
                i.setVisibility(8);
            }
            this.b.a(this.e);
        }
    }

    /* renamed from: c$b */
    private class b extends Exception {
        public b(String str) {
            super(str);
        }
    }

    /* renamed from: c$c  reason: collision with other inner class name */
    private class C0000c implements Runnable {
        private final String b;
        private final String c;
        private final WebView d;

        public C0000c(WebView webView, String str, String str2) {
            this.d = webView;
            this.b = str;
            this.c = str2;
        }

        public final void run() {
            this.d.loadDataWithBaseURL(this.b, this.c, "text/html", "utf-8", null);
        }
    }

    /* renamed from: c$d */
    private class d extends Exception {
        public d(String str) {
            super(str);
        }
    }

    /* renamed from: c$e */
    private class e implements Runnable {
        private final d b;
        private final LinkedList<String> c;
        private final int d;

        public e(d dVar, LinkedList<String> linkedList, int i) {
            this.b = dVar;
            this.c = linkedList;
            this.d = i;
        }

        public final void run() {
            this.b.a(this.c);
            this.b.a(this.d);
            this.b.q();
        }
    }

    public c(d dVar) {
        this.d = dVar;
        Activity e2 = dVar.e();
        if (e2 != null) {
            this.f = new g(e2.getApplicationContext(), null);
            this.f.setWebViewClient(new h(dVar, a.f0a, false, false));
            this.f.setVisibility(8);
            this.f.setWillNotDraw(true);
            this.c = new b(this, dVar);
            return;
        }
        this.f = null;
        this.c = null;
        com.google.ads.util.a.e("activity was null while trying to create an AdLoader.");
    }

    private String a(AdRequest adRequest, Activity activity) throws b, d {
        Context applicationContext = activity.getApplicationContext();
        Map<String, Object> requestMap = adRequest.getRequestMap(applicationContext);
        f l2 = this.d.l();
        long h2 = l2.h();
        if (h2 > 0) {
            requestMap.put("prl", Long.valueOf(h2));
        }
        String g2 = l2.g();
        if (g2 != null) {
            requestMap.put("ppcl", g2);
        }
        String f2 = l2.f();
        if (f2 != null) {
            requestMap.put("pcl", f2);
        }
        long e2 = l2.e();
        if (e2 > 0) {
            requestMap.put("pcc", Long.valueOf(e2));
        }
        requestMap.put("preqs", Long.valueOf(f.i()));
        String j2 = l2.j();
        if (j2 != null) {
            requestMap.put("pai", j2);
        }
        if (l2.k()) {
            requestMap.put("aoi_timeout", "true");
        }
        if (l2.m()) {
            requestMap.put("aoi_nofill", "true");
        }
        String p = l2.p();
        if (p != null) {
            requestMap.put("pit", p);
        }
        l2.a();
        l2.d();
        if (this.d.f() instanceof InterstitialAd) {
            requestMap.put("format", "interstitial_mb");
        } else {
            AdSize k2 = this.d.k();
            String adSize = k2.toString();
            if (adSize != null) {
                requestMap.put("format", adSize);
            } else {
                HashMap hashMap = new HashMap();
                hashMap.put("w", Integer.valueOf(k2.getWidth()));
                hashMap.put("h", Integer.valueOf(k2.getHeight()));
                requestMap.put("ad_frame", hashMap);
            }
        }
        requestMap.put("slotname", this.d.h());
        requestMap.put("js", "afma-sdk-a-v4.1.1");
        try {
            int i2 = applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 0).versionCode;
            requestMap.put("msid", applicationContext.getPackageName());
            requestMap.put("app_name", i2 + ".android." + applicationContext.getPackageName());
            requestMap.put("isu", AdUtil.a(applicationContext));
            String d2 = AdUtil.d(applicationContext);
            if (d2 == null) {
                throw new d("NETWORK_ERROR");
            }
            requestMap.put("net", d2);
            String e3 = AdUtil.e(applicationContext);
            if (!(e3 == null || e3.length() == 0)) {
                requestMap.put("cap", e3);
            }
            requestMap.put("u_audio", Integer.valueOf(AdUtil.f(applicationContext).ordinal()));
            DisplayMetrics a2 = AdUtil.a(activity);
            requestMap.put("u_sd", Float.valueOf(a2.density));
            requestMap.put("u_h", Integer.valueOf(AdUtil.a(applicationContext, a2)));
            requestMap.put("u_w", Integer.valueOf(AdUtil.b(applicationContext, a2)));
            requestMap.put("hl", Locale.getDefault().getLanguage());
            if (AdUtil.c()) {
                requestMap.put("simulator", 1);
            }
            String str = "<html><head><script src=\"http://www.gstatic.com/afma/sdk-core-v40.js\"></script><script>AFMA_buildAdURL(" + AdUtil.a(requestMap) + ");" + "</script></head><body></body></html>";
            com.google.ads.util.a.c("adRequestUrlHtml: " + str);
            return str;
        } catch (PackageManager.NameNotFoundException e4) {
            throw new b("NameNotFoundException");
        }
    }

    private void a(AdRequest.ErrorCode errorCode, boolean z) {
        this.c.a();
        this.d.a(new a(this.d, this.f, this.c, errorCode, z));
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        com.google.ads.util.a.a("AdLoader cancelled.");
        this.f.stopLoading();
        this.f.destroy();
        if (this.m != null) {
            this.m.interrupt();
            this.m = null;
        }
        this.c.a();
        this.i = true;
    }

    public final synchronized void a(int i2) {
        this.l = i2;
    }

    public final synchronized void a(AdRequest.ErrorCode errorCode) {
        this.j = errorCode;
        notify();
    }

    /* access modifiers changed from: package-private */
    public final void a(AdRequest adRequest) {
        this.e = adRequest;
        this.i = false;
        this.m = new Thread(this);
        this.m.start();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        this.h.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str, String str2) {
        this.f3a = str2;
        this.b = str;
        notify();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void b() {
        this.k = true;
        notify();
    }

    public final synchronized void b(String str) {
        this.g = str;
        notify();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.a(com.google.ads.AdRequest$ErrorCode, boolean):void
     arg types: [com.google.ads.AdRequest$ErrorCode, int]
     candidates:
      c.a(com.google.ads.AdRequest, android.app.Activity):java.lang.String
      c.a(java.lang.String, java.lang.String):void
      c.a(com.google.ads.AdRequest$ErrorCode, boolean):void */
    public final void run() {
        synchronized (this) {
            if (this.f == null || this.c == null) {
                com.google.ads.util.a.e("adRequestWebView was null while trying to load an ad.");
                a(AdRequest.ErrorCode.INTERNAL_ERROR, false);
                return;
            }
            Activity e2 = this.d.e();
            if (e2 == null) {
                com.google.ads.util.a.e("activity was null while forming an ad request.");
                a(AdRequest.ErrorCode.INTERNAL_ERROR, false);
                return;
            }
            try {
                this.d.a(new C0000c(this.f, null, a(this.e, e2)));
                long n = this.d.n();
                long elapsedRealtime = SystemClock.elapsedRealtime();
                if (n > 0) {
                    try {
                        wait(n);
                    } catch (InterruptedException e3) {
                        com.google.ads.util.a.a("AdLoader InterruptedException while getting the URL: " + e3);
                        return;
                    }
                }
                try {
                    if (!this.i) {
                        if (this.j != null) {
                            a(this.j, false);
                            return;
                        } else if (this.g == null) {
                            com.google.ads.util.a.c("AdLoader timed out after " + n + "ms while getting the URL.");
                            a(AdRequest.ErrorCode.NETWORK_ERROR, false);
                            return;
                        } else {
                            this.c.a(this.g);
                            long elapsedRealtime2 = n - (SystemClock.elapsedRealtime() - elapsedRealtime);
                            if (elapsedRealtime2 > 0) {
                                try {
                                    wait(elapsedRealtime2);
                                } catch (InterruptedException e4) {
                                    com.google.ads.util.a.a("AdLoader InterruptedException while getting the HTML: " + e4);
                                    return;
                                }
                            }
                            if (!this.i) {
                                if (this.j != null) {
                                    a(this.j, false);
                                    return;
                                } else if (this.b == null) {
                                    com.google.ads.util.a.c("AdLoader timed out after " + n + "ms while getting the HTML.");
                                    a(AdRequest.ErrorCode.NETWORK_ERROR, false);
                                    return;
                                } else {
                                    g i2 = this.d.i();
                                    this.d.j().a();
                                    this.d.a(new C0000c(i2, this.f3a, this.b));
                                    long elapsedRealtime3 = n - (SystemClock.elapsedRealtime() - elapsedRealtime);
                                    if (elapsedRealtime3 > 0) {
                                        try {
                                            wait(elapsedRealtime3);
                                        } catch (InterruptedException e5) {
                                            com.google.ads.util.a.a("AdLoader InterruptedException while loading the HTML: " + e5);
                                            return;
                                        }
                                    }
                                    if (this.k) {
                                        this.d.a(new e(this.d, this.h, this.l));
                                    } else {
                                        com.google.ads.util.a.c("AdLoader timed out after " + n + "ms while loading the HTML.");
                                        a(AdRequest.ErrorCode.NETWORK_ERROR, true);
                                    }
                                    return;
                                }
                            } else {
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                } catch (Exception e6) {
                    com.google.ads.util.a.a("An unknown error occurred in AdLoader.", e6);
                    a(AdRequest.ErrorCode.INTERNAL_ERROR, true);
                }
            } catch (d e7) {
                com.google.ads.util.a.c("Unable to connect to network: " + e7);
                a(AdRequest.ErrorCode.NETWORK_ERROR, false);
                return;
            } catch (b e8) {
                com.google.ads.util.a.c("Caught internal exception: " + e8);
                a(AdRequest.ErrorCode.INTERNAL_ERROR, false);
                return;
            }
        }
    }
}
