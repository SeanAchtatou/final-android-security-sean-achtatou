package com.woodlawn.defense.objects;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.woodlawn.squarepop.Manager;

public class UnpoppableSquare extends Square {
    public UnpoppableSquare(RectF bounds, boolean money, boolean health, boolean isBomb, Bitmap bomb, Bitmap transparency, int color, Manager manager) {
        super(bounds, money, health, isBomb, bomb, transparency, color, manager);
    }

    public void draw(Canvas c, Paint p) {
        int oldColor = p.getColor();
        p.setColor(this.color);
        if (this.movingDirection == RIGHT) {
            c.drawRect(this.bounds.left, this.bounds.top, this.bounds.right - this.offsetX, this.bounds.bottom, p);
        } else if (this.movingDirection == LEFT) {
            c.drawRect(this.offsetX + this.bounds.left, this.bounds.top, this.bounds.right, this.bounds.bottom, p);
        } else if (this.movingDirection == UP) {
            c.drawRect(this.bounds.left, this.offsetY + this.bounds.top, this.bounds.right, this.bounds.bottom, p);
        } else if (this.movingDirection == DOWN) {
            c.drawRect(this.bounds.left, this.bounds.top, this.bounds.right, this.bounds.bottom - this.offsetY, p);
        }
        p.setColor(oldColor);
    }
}
