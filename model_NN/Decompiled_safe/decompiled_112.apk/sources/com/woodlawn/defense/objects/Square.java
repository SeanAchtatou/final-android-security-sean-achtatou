package com.woodlawn.defense.objects;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.woodlawn.squarepop.Manager;
import java.util.Random;

public class Square {
    protected static int DOWN = 4;
    protected static int LEFT = 1;
    protected static int RIGHT = 2;
    protected static int UP = 3;
    protected int alpha;
    protected int alphaDelta;
    protected Bitmap bomb;
    protected RectF bounds;
    protected int color;
    protected boolean comingOnScreen;
    protected boolean expired;
    protected int expiredCountdown;
    protected boolean health;
    protected boolean isBomb;
    protected Manager manager;
    protected boolean money;
    protected int movingDirection;
    protected float offsetX;
    protected float offsetY;
    protected Random rand = new Random();
    private boolean sound = false;
    protected Bitmap transparency;

    public Square(RectF bounds2, boolean money2, boolean health2, boolean isBomb2, Bitmap bomb2, Bitmap transparency2, int color2, Manager manager2) {
        this.bounds = bounds2;
        this.transparency = transparency2;
        this.color = color2;
        this.money = money2;
        this.health = health2;
        this.isBomb = isBomb2;
        this.bomb = bomb2;
        this.comingOnScreen = true;
        this.movingDirection = this.rand.nextInt(4) + 1;
        if (this.movingDirection == LEFT || this.movingDirection == RIGHT) {
            this.offsetX = (float) Math.round(bounds2.right - bounds2.left);
            this.offsetY = 0.0f;
        } else {
            this.offsetY = (float) Math.round(bounds2.bottom - bounds2.top);
            this.offsetX = 0.0f;
        }
        this.alpha = 0;
        this.expired = false;
        this.expiredCountdown = 15;
        if (money2 || health2) {
            this.alphaDelta = 2;
        } else {
            this.alphaDelta = 1;
        }
        this.manager = manager2;
    }

    public void update() {
        if (this.comingOnScreen) {
            if (this.offsetX > 0.0f) {
                this.offsetX -= 3.0f;
            } else {
                this.offsetX = 0.0f;
            }
            if (this.offsetY > 0.0f) {
                this.offsetY -= 3.0f;
            } else {
                this.offsetY = 0.0f;
            }
            if (this.offsetY == 0.0f && this.offsetX == 0.0f) {
                this.comingOnScreen = false;
                return;
            }
            return;
        }
        if (this.alpha < 255) {
            this.alpha += this.alphaDelta;
        } else if (!this.expired) {
            this.expiredCountdown--;
        }
        if (this.expiredCountdown < 1) {
            this.expired = true;
        }
    }

    public boolean expired() {
        return this.expired;
    }

    public void draw(Canvas c, Paint p) {
        int oldColor = p.getColor();
        p.setColor(this.color);
        float rx = 0.0f;
        float ry = 0.0f;
        if (this.alpha > 190 && this.alpha < 220 && this.alpha % 3 == 0) {
            rx = this.rand.nextFloat() * 1.5f;
            ry = this.rand.nextFloat() * 1.5f;
            if (this.rand.nextInt() % 2 == 0) {
                rx *= -1.0f;
            }
            if (this.rand.nextInt() % 2 == 0) {
                ry *= -1.0f;
            }
            if (!this.sound) {
                this.manager.playSound(Manager.SOUND_BUZZ);
                this.sound = true;
            }
        } else if (this.alpha > 220) {
            float rx2 = this.rand.nextFloat() * 2.0f;
            float ry2 = this.rand.nextFloat() * 2.0f;
            if (this.rand.nextInt() % 2 == 0) {
                rx2 *= -1.0f;
            }
            if (this.rand.nextInt() % 2 == 0) {
                ry2 *= -1.0f;
            }
            if (this.sound) {
                this.manager.playSound(Manager.SOUND_BUZZ);
                this.sound = false;
            }
        }
        if (this.movingDirection == RIGHT) {
            c.drawRect(this.bounds.left + rx, this.bounds.top + ry, (this.bounds.right - this.offsetX) + rx, this.bounds.bottom + ry, p);
        } else if (this.movingDirection == LEFT) {
            c.drawRect(this.bounds.left + this.offsetX + rx, this.bounds.top + ry, this.bounds.right + rx, this.bounds.bottom + ry, p);
        } else if (this.movingDirection == UP) {
            c.drawRect(this.bounds.left + rx, this.bounds.top + this.offsetY + ry, this.bounds.right + rx, this.bounds.bottom + ry, p);
        } else if (this.movingDirection == DOWN) {
            c.drawRect(this.bounds.left + rx, this.bounds.top + ry, this.bounds.right + rx, (this.bounds.bottom - this.offsetY) + ry, p);
        }
        if (!this.comingOnScreen) {
            int oldAlpha = p.getAlpha();
            p.setAlpha(this.alpha);
            c.drawBitmap(this.transparency, (Rect) null, new RectF(this.bounds.left + rx, this.bounds.top + ry, this.bounds.right + rx, this.bounds.bottom + ry), p);
            p.setAlpha(oldAlpha);
            if (this.money) {
                p.setTextAlign(Paint.Align.CENTER);
                p.setColor(-16777216);
                float oldTextSize = p.getTextSize();
                p.setTextSize((this.bounds.right - this.bounds.left) * 0.9f);
                Rect textBounds = new Rect();
                p.getTextBounds("$", 0, 1, textBounds);
                float textOffset = ((this.bounds.right - this.bounds.left) - ((float) (textBounds.right - textBounds.left))) / 2.0f;
                c.drawText("$", this.bounds.left + ((this.bounds.right - this.bounds.left) / 2.0f) + 1.0f + rx, (this.bounds.bottom - textOffset) + 1.0f + ry, p);
                p.setColor(Color.parseColor("#59D832"));
                c.drawText("$", this.bounds.left + ((this.bounds.right - this.bounds.left) / 2.0f) + rx, (this.bounds.bottom - textOffset) + ry, p);
                p.setTextAlign(Paint.Align.LEFT);
                p.setTextSize(oldTextSize);
            } else if (this.health) {
                p.setTextAlign(Paint.Align.CENTER);
                p.setColor(-16777216);
                float oldTextSize2 = p.getTextSize();
                p.setTextSize(this.bounds.right - this.bounds.left);
                Rect textBounds2 = new Rect();
                p.getTextBounds("$", 0, 1, textBounds2);
                float textOffset2 = ((this.bounds.right - this.bounds.left) - ((float) (textBounds2.right - textBounds2.left))) / 2.0f;
                c.drawText("+", this.bounds.left + ((this.bounds.right - this.bounds.left) / 2.0f) + 1.0f + rx, (this.bounds.bottom - textOffset2) + 1.0f + ry, p);
                p.setColor(Color.parseColor("#F36363"));
                c.drawText("+", this.bounds.left + ((this.bounds.right - this.bounds.left) / 2.0f) + rx, (this.bounds.bottom - textOffset2) + ry, p);
                p.setTextAlign(Paint.Align.LEFT);
                p.setTextSize(oldTextSize2);
            } else if (this.isBomb) {
                int oldAlpha2 = p.getAlpha();
                p.setAlpha(150);
                c.drawBitmap(this.bomb, (Rect) null, this.bounds, p);
                p.setAlpha(oldAlpha2);
            }
        }
        p.setColor(oldColor);
    }

    public boolean clicked(float x, float y, float width) {
        RectF click = new RectF(x - width, y - width, x + width, y + width);
        return !this.comingOnScreen && (RectF.intersects(this.bounds, click) || this.bounds.contains(click));
    }

    public int color() {
        return this.color;
    }

    public boolean comingOnScreen() {
        return this.comingOnScreen;
    }

    public boolean money() {
        return this.money;
    }

    public boolean health() {
        return this.health;
    }

    public boolean bomb() {
        return this.isBomb;
    }

    public RectF bounds() {
        return this.bounds;
    }
}
