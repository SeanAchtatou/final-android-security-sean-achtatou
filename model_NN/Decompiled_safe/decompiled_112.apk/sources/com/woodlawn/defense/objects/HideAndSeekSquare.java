package com.woodlawn.defense.objects;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.woodlawn.squarepop.Manager;

public class HideAndSeekSquare extends Square {
    private Bitmap face;

    public HideAndSeekSquare(RectF bounds, boolean money, boolean health, boolean isBomb, Bitmap bomb, Bitmap transparency, int color, Manager manager, Bitmap face2) {
        super(bounds, money, health, isBomb, bomb, transparency, color, manager);
        this.face = face2;
    }

    public void draw(Canvas c, Paint p) {
        int oldColor = p.getColor();
        p.setColor(this.color);
        if (this.movingDirection == RIGHT) {
            c.drawRect(this.bounds.left, this.bounds.top, this.bounds.right - this.offsetX, this.bounds.bottom, p);
        } else if (this.movingDirection == LEFT) {
            c.drawRect(this.offsetX + this.bounds.left, this.bounds.top, this.bounds.right, this.bounds.bottom, p);
        } else if (this.movingDirection == UP) {
            c.drawRect(this.bounds.left, this.offsetY + this.bounds.top, this.bounds.right, this.bounds.bottom, p);
        } else if (this.movingDirection == DOWN) {
            c.drawRect(this.bounds.left, this.bounds.top, this.bounds.right, this.bounds.bottom - this.offsetY, p);
        }
        if (!this.comingOnScreen) {
            int oldAlpha = p.getAlpha();
            p.setAlpha(this.alpha);
            c.drawBitmap(this.transparency, (Rect) null, new RectF(this.bounds.left, this.bounds.top, this.bounds.right, this.bounds.bottom), p);
            p.setAlpha(oldAlpha);
            c.drawBitmap(this.face, (Rect) null, new RectF(this.bounds.left, this.bounds.top, this.bounds.right, this.bounds.bottom), p);
        }
        p.setColor(oldColor);
    }
}
