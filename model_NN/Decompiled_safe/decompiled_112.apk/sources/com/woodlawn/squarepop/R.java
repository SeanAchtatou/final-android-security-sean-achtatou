package com.woodlawn.squarepop;

public final class R {

    public static final class anim {
        public static final int loading = 2130968576;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int bomb = 2130837504;
        public static final int brick = 2130837505;
        public static final int face_mean = 2130837506;
        public static final int icon = 2130837507;
        public static final int poptrans = 2130837508;
        public static final int spacerbottomcorner = 2130837509;
        public static final int spacerbottomleft = 2130837510;
        public static final int spacerbottomright = 2130837511;
        public static final int spacerhorizontal = 2130837512;
        public static final int spacerhorizontalbottom = 2130837513;
        public static final int spacertopleft = 2130837514;
        public static final int spacertopright = 2130837515;
        public static final int spacervertical = 2130837516;
        public static final int spacerverticalright = 2130837517;
        public static final int squareblue = 2130837518;
        public static final int squaregreen = 2130837519;
        public static final int squareorange = 2130837520;
        public static final int squarepurple = 2130837521;
        public static final int squarered = 2130837522;
        public static final int squaretope = 2130837523;
        public static final int squareyellow = 2130837524;
        public static final int treelogo32 = 2130837525;
    }

    public static final class id {
        public static final int AnimationView = 2131230720;
        public static final int ad = 2131230771;
        public static final int close_button_first_time = 2131230753;
        public static final int close_stats_button = 2131230749;
        public static final int first_time_layout = 2131230750;
        public static final int first_time_txt_1 = 2131230751;
        public static final int first_time_txt_6 = 2131230752;
        public static final int gallery_wrapper = 2131230755;
        public static final int highleveltext = 2131230760;
        public static final int highscoretext = 2131230759;
        public static final int layout_root = 2131230754;
        public static final int loading_icon = 2131230722;
        public static final int loading_layout = 2131230721;
        public static final int loading_text = 2131230723;
        public static final int losing_face0 = 2131230763;
        public static final int losing_face1 = 2131230764;
        public static final int losing_face2 = 2131230765;
        public static final int losing_face3 = 2131230766;
        public static final int losing_face4 = 2131230767;
        public static final int losing_face5 = 2131230768;
        public static final int losing_face6 = 2131230769;
        public static final int losing_text = 2131230770;
        public static final int menutitle = 2131230756;
        public static final int play_levels_button = 2131230757;
        public static final int play_marathon_button = 2131230758;
        public static final int quit_button = 2131230762;
        public static final int request = 2131230772;
        public static final int search = 2131230773;
        public static final int stat_button = 2131230761;
        public static final int stats_0 = 2131230736;
        public static final int stats_1 = 2131230738;
        public static final int stats_2 = 2131230740;
        public static final int stats_3 = 2131230742;
        public static final int stats_4 = 2131230744;
        public static final int stats_5 = 2131230746;
        public static final int stats_6 = 2131230748;
        public static final int stats_accuracy_text = 2131230732;
        public static final int stats_avg_score_text = 2131230727;
        public static final int stats_face0 = 2131230735;
        public static final int stats_face1 = 2131230737;
        public static final int stats_face2 = 2131230739;
        public static final int stats_face3 = 2131230741;
        public static final int stats_face4 = 2131230743;
        public static final int stats_face5 = 2131230745;
        public static final int stats_face6 = 2131230747;
        public static final int stats_games_completed_text = 2131230729;
        public static final int stats_games_played_text = 2131230728;
        public static final int stats_inner_layout = 2131230734;
        public static final int stats_layout = 2131230724;
        public static final int stats_misses_text = 2131230731;
        public static final int stats_pops_text = 2131230730;
        public static final int stats_popstitle_text = 2131230733;
        public static final int stats_total_score_text = 2131230726;
        public static final int statstitle = 2131230725;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class menu {
        public static final int options_menu = 2131165184;
    }

    public static final class raw {
        public static final int bad_pop = 2131034112;
        public static final int buzz = 2131034113;
        public static final int highscore = 2131034114;
        public static final int pop = 2131034115;
        public static final int squarepop = 2131034116;
        public static final int unpoppable = 2131034117;
    }

    public static final class string {
        public static final int app_name = 2131099648;
    }

    public static final class styleable {
        public static final int[] HelloGallery = {16842828};
        public static final int HelloGallery_android_galleryItemBackground = 0;
    }
}
