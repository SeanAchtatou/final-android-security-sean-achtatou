package com.woodlawn.squarepop;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.SoundPool;
import android.view.MotionEvent;
import com.woodlawn.defense.objects.HideAndSeekSquare;
import com.woodlawn.defense.objects.Square;
import com.woodlawn.defense.objects.UnpoppableSquare;
import com.woodlawn.squarepop.workers.LevelsWorker;
import com.woodlawn.squarepop.workers.MarathonWorker;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Random;

public class Manager {
    public static int LEVELS = 2;
    public static int MARATHON = 1;
    public static String SOUND_BAD_POP = "bad_pop";
    public static String SOUND_BUZZ = "buzz";
    public static String SOUND_POP = "pop";
    public static String SOUND_SQUARE_POP = "squarepop";
    public static String SOUND_UNPOP = "sound_unpop";
    protected int backgroundColor;
    protected int[] backgroundColors;
    protected Bitmap bomb;
    protected int bombInterval;
    protected int bombIntervalMin = 90;
    protected int bombLastCheck;
    protected Bitmap brickOverlay;
    protected boolean click = false;
    protected float clickX;
    protected float clickY;
    protected int[] colors;
    protected Bitmap faceMean;
    private int gameType;
    protected Bitmap grid;
    protected int gridSize = 9;
    protected int healthInterval;
    protected int healthIntervalMin = 1550;
    protected int healthLastCheck;
    protected int healthPops;
    protected float heightMargin;
    private int individualSquareCount;
    private int level;
    private int levelColorSetIndex;
    protected boolean levelComplete = false;
    private int levelIndividualSquareCountNumber;
    private int[] levelSquareCount;
    private int levelSquareCountNumber;
    private int[][] levelsColorSets;
    private LevelsWorker levelsWorker;
    private MarathonWorker marathonWorker;
    protected float marginPercent = 0.0125f;
    protected int misses;
    protected int moneyInterval;
    protected int moneyIntervalMin = 800;
    protected int moneyLastCheck;
    protected int newHighScore = -1;
    protected long pause;
    protected int playerHealth;
    protected int playerScore;
    protected boolean playerWon;
    protected int popColor0;
    protected int popColor1;
    protected int popColor2;
    protected int popColor3;
    protected int popColor4;
    protected int popColor5;
    protected int popColor6;
    protected int pops;
    protected float puzzleWidth;
    protected Random rand = new Random();
    protected int scorePops;
    protected HashMap<String, Integer> soundMap;
    protected SoundPool soundPool;
    protected Bitmap spacerBottomLeft;
    protected Bitmap spacerBottomRight;
    protected Bitmap spacerCorner;
    protected Bitmap spacerHorizontal;
    protected Bitmap spacerHorizontalBottom;
    protected float spacerMargin;
    protected Bitmap spacerTopLeft;
    protected Bitmap spacerTopRight;
    protected Bitmap spacerVertical;
    protected Bitmap spacerVerticalRight;
    private int squareCount;
    protected float squareSize;
    protected Square[][] squares;
    protected Bitmap transparency;
    protected int updateLength;
    protected int updateLimit;
    protected int updates = 0;
    protected float widthMargin;
    protected float x_max = 0.0f;
    protected float y_max = 0.0f;

    public Manager(float width, float height) {
        this.x_max = width;
        this.y_max = height;
    }

    public void setBrickOverlay(Bitmap brickOverlay2) {
        this.brickOverlay = brickOverlay2;
    }

    public void setBomb(Bitmap bomb2) {
        this.bomb = bomb2;
    }

    public void setSoundPool(SoundPool soundPool2) {
        this.soundPool = soundPool2;
    }

    public void setSoundMap(HashMap<String, Integer> soundMap2) {
        this.soundMap = soundMap2;
    }

    public void setSpacerCorner(Bitmap spacerCorner2) {
        this.spacerCorner = spacerCorner2;
    }

    public void setSpacerBottomLeft(Bitmap spacerBottomLeft2) {
        this.spacerBottomLeft = spacerBottomLeft2;
    }

    public void setSpacerBottomRight(Bitmap spacerBottomRight2) {
        this.spacerBottomRight = spacerBottomRight2;
    }

    public void setSpacerTopRight(Bitmap spacerTopRight2) {
        this.spacerTopRight = spacerTopRight2;
    }

    public void setSpacerTopLeft(Bitmap spacerTopLeft2) {
        this.spacerTopLeft = spacerTopLeft2;
    }

    public void setTransparency(Bitmap transparency2) {
        this.transparency = transparency2;
    }

    public void setSpacerVertical(Bitmap spacerVertical2) {
        this.spacerVertical = spacerVertical2;
    }

    public void setSpacerHorizontal(Bitmap spacerHorizontal2) {
        this.spacerHorizontal = spacerHorizontal2;
    }

    public void setSpacerVerticalRight(Bitmap spacerVerticalRight2) {
        this.spacerVerticalRight = spacerVerticalRight2;
    }

    public void setSpacerHorizontalBottom(Bitmap spacerHorizontalBottom2) {
        this.spacerHorizontalBottom = spacerHorizontalBottom2;
    }

    public void setFaceMean(Bitmap faceMean2) {
        this.faceMean = faceMean2;
    }

    public void setBackground(Bitmap background) {
    }

    public String whoWon() {
        if (gameOver() && this.playerWon) {
            return "Player";
        }
        if (!gameOver() || this.playerWon) {
            return "No One";
        }
        return "Computer";
    }

    public int getNewHighScore() {
        return this.newHighScore;
    }

    public void saveScore() {
        int[] scores = new int[5];
        int x = 0;
        while (x < scores.length) {
            if (scores[x] < this.playerScore) {
                this.newHighScore = this.playerScore;
                for (int y = scores.length - 1; y > x; y--) {
                    scores[y] = scores[y - 1];
                }
                scores[x] = this.playerScore;
                x = scores.length;
            }
            x++;
        }
    }

    public boolean gameOver() {
        return this.playerHealth < 1;
    }

    public void draw(Canvas c, Paint paint) {
        if (this.gameType == MARATHON) {
            this.marathonWorker.draw(c, paint, this.x_max, this.y_max, this.backgroundColor, this.playerHealth, this.spacerMargin, this.widthMargin, this.heightMargin, this.squareSize, this.bomb, this.playerScore, this.puzzleWidth, this.grid, this.squares, this.gridSize, this.clickX, this.clickY);
        } else if (this.gameType == LEVELS) {
            this.levelsWorker.draw(c, paint, this.x_max, this.y_max, this.backgroundColor, this.playerHealth, this.spacerMargin, this.widthMargin, this.heightMargin, this.squareSize, this.bomb, this.playerScore, this.puzzleWidth, this.grid, this.squares, this.gridSize, this.clickX, this.clickY, this.levelSquareCount, this.levelColorSetIndex, this.levelsColorSets, this.level);
        }
    }

    public void init(int gameType2) {
        if (gameType2 == LEVELS) {
            this.levelsWorker = new LevelsWorker();
        } else if (gameType2 == MARATHON) {
            this.marathonWorker = new MarathonWorker();
        }
        this.squares = (Square[][]) Array.newInstance(Square.class, this.gridSize, this.gridSize);
        this.playerHealth = 15;
        this.playerScore = 0;
        this.updateLength = 30;
        this.updateLimit = 90;
        this.healthInterval = this.healthIntervalMin;
        this.moneyInterval = this.moneyIntervalMin;
        this.bombInterval = this.bombIntervalMin;
        this.moneyLastCheck = 0;
        this.healthLastCheck = 0;
        this.bombLastCheck = 0;
        this.updates = 0;
        this.pops = 0;
        this.misses = 0;
        this.healthPops = 0;
        this.scorePops = 0;
        this.popColor0 = 0;
        this.popColor1 = 0;
        this.popColor2 = 0;
        this.popColor3 = 0;
        this.popColor4 = 0;
        this.popColor5 = 0;
        this.popColor6 = 0;
        this.newHighScore = -1;
        this.puzzleWidth = this.x_max * (1.0f - (this.marginPercent * 2.0f));
        this.widthMargin = this.x_max * this.marginPercent;
        this.heightMargin = (this.y_max - this.puzzleWidth) / 2.0f;
        this.spacerMargin = this.puzzleWidth * 0.03f;
        this.squareSize = (this.puzzleWidth * 0.7f) / 9.0f;
        this.grid = Bitmap.createBitmap((int) Math.ceil((double) this.x_max), (int) Math.ceil((double) this.y_max), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(this.grid);
        Paint paint = new Paint();
        c.drawBitmap(this.spacerTopLeft, (Rect) null, new RectF(this.widthMargin, this.heightMargin, this.widthMargin + this.spacerMargin, this.heightMargin + this.spacerMargin), paint);
        c.drawBitmap(this.spacerTopRight, (Rect) null, new RectF((this.widthMargin + this.puzzleWidth) - this.spacerMargin, this.heightMargin, this.widthMargin + this.puzzleWidth, this.heightMargin + this.spacerMargin), paint);
        c.drawBitmap(this.spacerBottomRight, (Rect) null, new RectF((this.widthMargin + this.puzzleWidth) - this.spacerMargin, (this.heightMargin + this.puzzleWidth) - this.spacerMargin, this.widthMargin + this.puzzleWidth, this.heightMargin + this.puzzleWidth), paint);
        c.drawBitmap(this.spacerBottomLeft, (Rect) null, new RectF(this.widthMargin, (this.heightMargin + this.puzzleWidth) - this.spacerMargin, this.widthMargin + this.spacerMargin, this.heightMargin + this.puzzleWidth), paint);
        c.drawBitmap(this.spacerVertical, (Rect) null, new RectF(this.widthMargin, this.heightMargin + this.spacerMargin, this.widthMargin + this.spacerMargin, (this.heightMargin + this.puzzleWidth) - this.spacerMargin), paint);
        c.drawBitmap(this.spacerVerticalRight, (Rect) null, new RectF(this.widthMargin + (9.0f * (this.spacerMargin + this.squareSize)), this.heightMargin + this.spacerMargin, this.widthMargin + this.spacerMargin + (9.0f * (this.spacerMargin + this.squareSize)), (this.heightMargin + this.puzzleWidth) - this.spacerMargin), paint);
        for (int j = 0; j < this.gridSize; j++) {
            for (int i = 1; i < this.gridSize; i++) {
                c.drawBitmap(this.spacerVertical, (Rect) null, new RectF(this.widthMargin + (((float) i) * (this.spacerMargin + this.squareSize)), this.heightMargin + this.spacerMargin + (((float) j) * (this.spacerMargin + this.squareSize)), this.widthMargin + this.spacerMargin + (((float) i) * (this.spacerMargin + this.squareSize)), this.heightMargin + this.spacerMargin + (((float) j) * (this.spacerMargin + this.squareSize)) + this.squareSize), paint);
            }
        }
        c.drawBitmap(this.spacerHorizontal, (Rect) null, new RectF(this.widthMargin + this.spacerMargin, this.heightMargin, (this.widthMargin + this.puzzleWidth) - this.spacerMargin, this.heightMargin + this.spacerMargin), paint);
        c.drawBitmap(this.spacerHorizontalBottom, (Rect) null, new RectF(this.widthMargin + this.spacerMargin, (this.heightMargin + this.puzzleWidth) - this.spacerMargin, (this.widthMargin + this.puzzleWidth) - this.spacerMargin, this.heightMargin + this.puzzleWidth), paint);
        for (int i2 = 0; i2 < this.gridSize; i2++) {
            for (int j2 = 1; j2 < this.gridSize; j2++) {
                c.drawBitmap(this.spacerHorizontal, (Rect) null, new RectF(this.widthMargin + this.spacerMargin + (((float) i2) * (this.spacerMargin + this.squareSize)), this.heightMargin + (((float) j2) * (this.spacerMargin + this.squareSize)), this.widthMargin + this.spacerMargin + (((float) i2) * (this.spacerMargin + this.squareSize)) + this.squareSize, this.heightMargin + this.spacerMargin + (((float) j2) * (this.spacerMargin + this.squareSize))), paint);
            }
        }
        for (int i3 = 0; i3 < this.gridSize - 1; i3++) {
            for (int j3 = 0; j3 < this.gridSize - 1; j3++) {
                c.drawBitmap(this.spacerCorner, (Rect) null, new RectF(this.widthMargin + this.spacerMargin + this.squareSize + (((float) i3) * (this.spacerMargin + this.squareSize)), this.heightMargin + this.spacerMargin + this.squareSize + (((float) j3) * (this.spacerMargin + this.squareSize)), this.widthMargin + this.spacerMargin + this.squareSize + (((float) i3) * (this.spacerMargin + this.squareSize)) + this.spacerMargin, this.heightMargin + this.spacerMargin + this.squareSize + (((float) j3) * (this.spacerMargin + this.squareSize)) + this.spacerMargin), paint);
            }
        }
        String[] rgbColors = {"#fce94f", "#fcaf3e", "#e9b96e", "#8ae234", "#729fcf", "#ad7fa8", "#ef2929"};
        this.colors = new int[rgbColors.length];
        for (int i4 = 0; i4 < rgbColors.length; i4++) {
            this.colors[i4] = Color.parseColor(rgbColors[i4]);
        }
        String[] rgbColors2 = {"#c4a000", "#ce5c00", "#8f5902", "#4e9a06", "#204a87", "#5c3566", "#a40000"};
        this.backgroundColors = new int[rgbColors2.length];
        for (int i5 = 0; i5 < rgbColors2.length; i5++) {
            this.backgroundColors[i5] = Color.parseColor(rgbColors2[i5]);
        }
        this.backgroundColor = this.backgroundColors[this.rand.nextInt(this.backgroundColors.length)];
        this.individualSquareCount = 3;
        this.squareCount = 3;
        if (gameType2 == MARATHON) {
            initMarathon();
        } else if (gameType2 == LEVELS) {
            this.level = 1;
            initLevels();
        }
        this.gameType = gameType2;
    }

    private void initMarathon() {
    }

    private void initLevels() {
        String[][] rawColors = {new String[]{"#CED9DD", "#FF9182", "#DEBB17", "#FFA75D", "#4ACBD1", "#95B138"}, new String[]{"#9A0000", "#CE0000", "#FF5400", "#FE7F88", "#FFCDCC", "#39AAD2"}, new String[]{"#19AA5F", "#00917E", "#FF9E29", "#FE5915", "#E21C3F", "#A41950"}, new String[]{"#1F6D00", "#179F00", "#97B515", "#6F460E", "#3BA5BF", "#BCD4D4"}, new String[]{"#97C429", "#FCD4D4", "#E1212C", "#9BEDC7", "#EE9714", "#31A26A"}, new String[]{"#613437", "#DA2127", "#FECD4C", "#F98321", "#3BB548", "#F9BEB8"}, new String[]{"#C097D9", "#03064D", "#3D41A4", "#98A9B9", "#5D144C", "#D19499"}, new String[]{"#FAA429", "#8ED4CC", "#6C9E92", "#B78139", "#693331", "#483D45"}, new String[]{"#FC2568", "#5C80D8", "#126A6C", "#E6BA33", "#FF735A", "#4B382A"}, new String[]{"#5A4324", "#7E5D32", "#496968", "#567B96", "#5B9944", "#D3C469"}, new String[]{"#E4BF0F", "#F9440B", "#A84D32", "#03702C", "#279966", "#C0B75C"}, new String[]{"#DBBD45", "#02AFBD", "#C894BA", "#ACDE4B", "#E37278", "#F5B317"}, new String[]{"#6E202E", "#F07531", "#547442", "#922856", "#DE2F2C", "#CCB21D"}, new String[]{"#CFCE40", "#76B04C", "#8FC043", "#8AC7B8", "#F56409", "#F2C61F"}, new String[]{"#971038", "#930279", "#3D0045", "#66AFC2", "#3D988F", "#1D4039"}, new String[]{"#A41A6F", "#680971", "#3B1167", "#235395", "#141474", "#F7014C"}};
        this.levelsColorSets = (int[][]) Array.newInstance(Integer.TYPE, rawColors.length, 6);
        for (int i = 0; i < rawColors.length; i++) {
            for (int j = 0; j < rawColors[i].length; j++) {
                this.levelsColorSets[i][j] = Color.parseColor(rawColors[i][j]);
            }
        }
        Random r = new Random();
        this.levelColorSetIndex = r.nextInt(this.levelsColorSets.length);
        this.levelSquareCount = new int[this.levelsColorSets[this.levelColorSetIndex].length];
        for (int i2 = 0; i2 < this.levelsColorSets[this.levelColorSetIndex].length; i2++) {
            this.levelSquareCount[i2] = 0;
        }
        int tempSquareCount = 0;
        this.levelIndividualSquareCountNumber = this.individualSquareCount;
        this.levelSquareCountNumber = this.squareCount;
        while (tempSquareCount < this.levelSquareCountNumber) {
            int t = r.nextInt(this.levelSquareCount.length);
            if (this.levelSquareCount[t] == 0) {
                tempSquareCount++;
                this.levelSquareCount[t] = r.nextInt(this.levelIndividualSquareCountNumber) + 1;
            }
        }
        this.colors = this.levelsColorSets[this.levelColorSetIndex];
    }

    public void reset() {
    }

    public void setXMax(float x_max2) {
        this.x_max = x_max2;
    }

    public void setYMax(float y_max2) {
        this.y_max = y_max2;
    }

    /* access modifiers changed from: protected */
    public void popSquare(int i, int j) {
        if (this.squares[i][j] instanceof UnpoppableSquare) {
            playSound(SOUND_UNPOP);
            return;
        }
        int money = 0;
        int health = 0;
        if (this.squares[i][j].money()) {
            money = 0 + 100;
            this.scorePops++;
        } else if (this.squares[i][j].health()) {
            health = 0 + 1;
            this.healthPops++;
        }
        this.pops++;
        if (this.squares[i][j].color() == this.colors[0]) {
            this.popColor0++;
        } else if (this.squares[i][j].color() == this.colors[1]) {
            this.popColor1++;
        } else if (this.squares[i][j].color() == this.colors[2]) {
            this.popColor2++;
        } else if (this.squares[i][j].color() == this.colors[3]) {
            this.popColor3++;
        } else if (this.squares[i][j].color() == this.colors[4]) {
            this.popColor4++;
        } else if (this.squares[i][j].color() == this.colors[5]) {
            this.popColor5++;
        } else if (this.squares[i][j].color() == this.colors[6]) {
            this.popColor6++;
        }
        this.squares[i][j] = null;
        this.playerScore = this.playerScore + 10 + money;
        this.playerHealth += health;
        playSound(SOUND_POP);
    }

    public void update(long now) {
        boolean health;
        this.updates = this.updates + 1;
        if (this.click) {
            boolean popped = false;
            int i = 0;
            while (i < this.gridSize) {
                int j = 0;
                while (j < this.gridSize) {
                    if (this.squares[i][j] != null && this.squares[i][j].clicked(this.clickX, this.clickY, this.squareSize * 0.75f) && !(this.squares[i][j] instanceof UnpoppableSquare)) {
                        int scoreFactor = 1;
                        int health2 = 0;
                        int money = 0;
                        if (this.squares[i][j].bomb()) {
                            if (i - 1 >= 0 && j - 1 >= 0 && this.squares[i - 1][j - 1] != null) {
                                popSquare(i - 1, j - 1);
                            }
                            if (i - 1 >= 0 && this.squares[i - 1][j] != null) {
                                popSquare(i - 1, j);
                            }
                            if (i - 1 >= 0 && j + 1 < this.gridSize && this.squares[i - 1][j + 1] != null) {
                                popSquare(i - 1, j + 1);
                            }
                            if (j + 1 < this.gridSize && this.squares[i][j + 1] != null) {
                                popSquare(i, j + 1);
                            }
                            if (i + 1 < this.gridSize && j + 1 < this.gridSize && this.squares[i + 1][j + 1] != null) {
                                popSquare(i + 1, j + 1);
                            }
                            if (i + 1 < this.gridSize && this.squares[i + 1][j] != null) {
                                popSquare(i + 1, j);
                            }
                            if (i + 1 < this.gridSize && j - 1 >= 0 && this.squares[i + 1][j - 1] != null) {
                                popSquare(i + 1, j - 1);
                            }
                            if (j - 1 >= 0 && this.squares[i][j - 1] != null) {
                                popSquare(i, j - 1);
                            }
                        }
                        boolean badPop = false;
                        if (i != this.gridSize - 1 && this.squares[i + 1][j] != null && !this.squares[i + 1][j].comingOnScreen() && this.squares[i + 1][j].color() == this.squares[i][j].color()) {
                            scoreFactor = 1 + 2;
                            if (this.squares[i + 1][j].money()) {
                                money = 0 + 100;
                                this.scorePops = this.scorePops + 1;
                            } else if (this.squares[i + 1][j].health()) {
                                health2 = 0 + 1;
                                this.healthPops = this.healthPops + 1;
                            } else if (this.squares[i + 1][j] instanceof HideAndSeekSquare) {
                                this.playerHealth = this.playerHealth - 1;
                                badPop = true;
                            }
                            this.pops = this.pops + 1;
                            if (this.squares[i + 1][j].color() == this.colors[0]) {
                                this.popColor0 = this.popColor0 + 1;
                            } else if (this.squares[i + 1][j].color() == this.colors[1]) {
                                this.popColor1 = this.popColor1 + 1;
                            } else if (this.squares[i + 1][j].color() == this.colors[2]) {
                                this.popColor2 = this.popColor2 + 1;
                            } else if (this.squares[i + 1][j].color() == this.colors[3]) {
                                this.popColor3 = this.popColor3 + 1;
                            } else if (this.squares[i + 1][j].color() == this.colors[4]) {
                                this.popColor4 = this.popColor4 + 1;
                            } else if (this.squares[i + 1][j].color() == this.colors[5]) {
                                this.popColor5 = this.popColor5 + 1;
                            } else if (this.squares[i + 1][j].color() == this.colors[6]) {
                                this.popColor6 = this.popColor6 + 1;
                            }
                            if (this.gameType == LEVELS) {
                                for (int k = 0; k < this.levelSquareCount.length; k++) {
                                    if (this.levelsColorSets[this.levelColorSetIndex][k] == this.squares[i + 1][j].color() && this.levelSquareCount[k] > 0) {
                                        int[] iArr = this.levelSquareCount;
                                        iArr[k] = iArr[k] - 1;
                                    }
                                }
                            }
                            this.squares[i + 1][j] = null;
                        }
                        if (i != 0 && this.squares[i - 1][j] != null && !this.squares[i - 1][j].comingOnScreen() && this.squares[i - 1][j].color() == this.squares[i][j].color()) {
                            scoreFactor += 2;
                            if (this.squares[i - 1][j].money()) {
                                money += 100;
                                this.scorePops = this.scorePops + 1;
                            } else if (this.squares[i - 1][j].health()) {
                                health2++;
                                this.healthPops = this.healthPops + 1;
                            } else if (this.squares[i - 1][j] instanceof HideAndSeekSquare) {
                                this.playerHealth = this.playerHealth - 1;
                                badPop = true;
                            }
                            this.pops = this.pops + 1;
                            if (this.squares[i - 1][j].color() == this.colors[0]) {
                                this.popColor0 = this.popColor0 + 1;
                            } else if (this.squares[i - 1][j].color() == this.colors[1]) {
                                this.popColor1 = this.popColor1 + 1;
                            } else if (this.squares[i - 1][j].color() == this.colors[2]) {
                                this.popColor2 = this.popColor2 + 1;
                            } else if (this.squares[i - 1][j].color() == this.colors[3]) {
                                this.popColor3 = this.popColor3 + 1;
                            } else if (this.squares[i - 1][j].color() == this.colors[4]) {
                                this.popColor4 = this.popColor4 + 1;
                            } else if (this.squares[i - 1][j].color() == this.colors[5]) {
                                this.popColor5 = this.popColor5 + 1;
                            } else if (this.squares[i - 1][j].color() == this.colors[6]) {
                                this.popColor6 = this.popColor6 + 1;
                            }
                            if (this.gameType == LEVELS) {
                                for (int k2 = 0; k2 < this.levelSquareCount.length; k2++) {
                                    if (this.levelsColorSets[this.levelColorSetIndex][k2] == this.squares[i - 1][j].color() && this.levelSquareCount[k2] > 0) {
                                        int[] iArr2 = this.levelSquareCount;
                                        iArr2[k2] = iArr2[k2] - 1;
                                    }
                                }
                            }
                            this.squares[i - 1][j] = null;
                        }
                        if (j != this.gridSize - 1 && this.squares[i][j + 1] != null && !this.squares[i][j + 1].comingOnScreen() && this.squares[i][j + 1].color() == this.squares[i][j].color()) {
                            scoreFactor += 2;
                            if (this.squares[i][j + 1].money()) {
                                money += 100;
                                this.scorePops = this.scorePops + 1;
                            } else if (this.squares[i][j + 1].health()) {
                                health2++;
                                this.healthPops = this.healthPops + 1;
                            } else if (this.squares[i][j + 1] instanceof HideAndSeekSquare) {
                                this.playerHealth = this.playerHealth - 1;
                                badPop = true;
                            }
                            this.pops = this.pops + 1;
                            if (this.squares[i][j + 1].color() == this.colors[0]) {
                                this.popColor0 = this.popColor0 + 1;
                            } else if (this.squares[i][j + 1].color() == this.colors[1]) {
                                this.popColor1 = this.popColor1 + 1;
                            } else if (this.squares[i][j + 1].color() == this.colors[2]) {
                                this.popColor2 = this.popColor2 + 1;
                            } else if (this.squares[i][j + 1].color() == this.colors[3]) {
                                this.popColor3 = this.popColor3 + 1;
                            } else if (this.squares[i][j + 1].color() == this.colors[4]) {
                                this.popColor4 = this.popColor4 + 1;
                            } else if (this.squares[i][j + 1].color() == this.colors[5]) {
                                this.popColor5 = this.popColor5 + 1;
                            } else if (this.squares[i][j + 1].color() == this.colors[6]) {
                                this.popColor6 = this.popColor6 + 1;
                            }
                            if (this.gameType == LEVELS) {
                                for (int k3 = 0; k3 < this.levelSquareCount.length; k3++) {
                                    if (this.levelsColorSets[this.levelColorSetIndex][k3] == this.squares[i][j + 1].color() && this.levelSquareCount[k3] > 0) {
                                        int[] iArr3 = this.levelSquareCount;
                                        iArr3[k3] = iArr3[k3] - 1;
                                    }
                                }
                            }
                            this.squares[i][j + 1] = null;
                        }
                        if (j != 0 && this.squares[i][j - 1] != null && !this.squares[i][j - 1].comingOnScreen() && this.squares[i][j - 1].color() == this.squares[i][j].color()) {
                            scoreFactor += 2;
                            if (this.squares[i][j - 1].money()) {
                                money += 100;
                                this.scorePops = this.scorePops + 1;
                            } else if (this.squares[i][j - 1].health()) {
                                health2++;
                                this.healthPops = this.healthPops + 1;
                            } else if (this.squares[i][j - 1] instanceof HideAndSeekSquare) {
                                this.playerHealth = this.playerHealth - 1;
                                badPop = true;
                            }
                            this.pops = this.pops + 1;
                            if (this.squares[i][j - 1].color() == this.colors[0]) {
                                this.popColor0 = this.popColor0 + 1;
                            } else if (this.squares[i][j - 1].color() == this.colors[1]) {
                                this.popColor1 = this.popColor1 + 1;
                            } else if (this.squares[i][j - 1].color() == this.colors[2]) {
                                this.popColor2 = this.popColor2 + 1;
                            } else if (this.squares[i][j - 1].color() == this.colors[3]) {
                                this.popColor3 = this.popColor3 + 1;
                            } else if (this.squares[i][j - 1].color() == this.colors[4]) {
                                this.popColor4 = this.popColor4 + 1;
                            } else if (this.squares[i][j - 1].color() == this.colors[5]) {
                                this.popColor5 = this.popColor5 + 1;
                            } else if (this.squares[i][j - 1].color() == this.colors[6]) {
                                this.popColor6 = this.popColor6 + 1;
                            }
                            if (this.gameType == LEVELS) {
                                for (int k4 = 0; k4 < this.levelSquareCount.length; k4++) {
                                    if (this.levelsColorSets[this.levelColorSetIndex][k4] == this.squares[i][j - 1].color() && this.levelSquareCount[k4] > 0) {
                                        int[] iArr4 = this.levelSquareCount;
                                        iArr4[k4] = iArr4[k4] - 1;
                                    }
                                }
                            }
                            this.squares[i][j - 1] = null;
                        }
                        if (this.squares[i][j].money()) {
                            money += 100;
                            this.scorePops = this.scorePops + 1;
                        } else if (this.squares[i][j].health()) {
                            health2++;
                            this.healthPops = this.healthPops + 1;
                        } else if (this.squares[i][j] instanceof HideAndSeekSquare) {
                            badPop = true;
                            this.playerHealth = this.playerHealth - 1;
                        }
                        this.pops = this.pops + 1;
                        if (this.squares[i][j].color() == this.colors[0]) {
                            this.popColor0 = this.popColor0 + 1;
                        } else if (this.squares[i][j].color() == this.colors[1]) {
                            this.popColor1 = this.popColor1 + 1;
                        } else if (this.squares[i][j].color() == this.colors[2]) {
                            this.popColor2 = this.popColor2 + 1;
                        } else if (this.squares[i][j].color() == this.colors[3]) {
                            this.popColor3 = this.popColor3 + 1;
                        } else if (this.squares[i][j].color() == this.colors[4]) {
                            this.popColor4 = this.popColor4 + 1;
                        } else if (this.squares[i][j].color() == this.colors[5]) {
                            this.popColor5 = this.popColor5 + 1;
                        } else if (this.squares[i][j].color() == this.colors[6]) {
                            this.popColor6 = this.popColor6 + 1;
                        }
                        if (badPop) {
                            playSound(SOUND_BAD_POP);
                        } else {
                            playSound(SOUND_POP);
                        }
                        if (this.gameType == LEVELS) {
                            for (int k5 = 0; k5 < this.levelSquareCount.length; k5++) {
                                if (this.levelsColorSets[this.levelColorSetIndex][k5] == this.squares[i][j].color() && this.levelSquareCount[k5] > 0) {
                                    int[] iArr5 = this.levelSquareCount;
                                    iArr5[k5] = iArr5[k5] - 1;
                                }
                            }
                        }
                        this.squares[i][j] = null;
                        this.playerScore = this.playerScore + (scoreFactor * 10) + money;
                        this.playerHealth = this.playerHealth + health2;
                        popped = true;
                        j = this.squares[i].length;
                        i = this.squares.length;
                    } else if (this.squares[i][j] != null && this.squares[i][j].clicked(this.clickX, this.clickY, this.squareSize * 0.75f) && (this.squares[i][j] instanceof UnpoppableSquare)) {
                        playSound(SOUND_UNPOP);
                    }
                    j++;
                }
                i++;
            }
            if (!popped) {
                this.misses = this.misses + 1;
            }
            this.click = false;
        }
        if (this.updates % this.updateLength == 0) {
            this.updateLength = this.rand.nextInt(this.updateLimit) + 1;
            if (this.updateLimit > 40) {
                this.updateLimit = this.updateLimit - 1;
            }
            int seedSize = this.rand.nextInt((this.updates / 3000) + 1) + 1;
            for (int k6 = 0; k6 < seedSize; k6++) {
                int startX = this.rand.nextInt(this.gridSize);
                int startY = this.rand.nextInt(this.gridSize);
                int i2 = 0;
                while (i2 < this.gridSize) {
                    int j2 = 0;
                    while (j2 < this.gridSize) {
                        if (this.squares[startX][startY] == null) {
                            boolean bombsaway = this.updates - this.bombLastCheck > this.bombInterval;
                            boolean money2 = !bombsaway && this.updates - this.moneyLastCheck > this.moneyInterval;
                            if (bombsaway || money2 || this.updates - this.healthLastCheck <= this.healthInterval) {
                                health = false;
                            } else {
                                health = true;
                            }
                            if (this.rand.nextInt(15) == 5) {
                                this.squares[startX][startY] = new HideAndSeekSquare(new RectF(this.widthMargin + this.spacerMargin + (((float) startX) * (this.spacerMargin + this.squareSize)), this.heightMargin + this.spacerMargin + (((float) startY) * (this.spacerMargin + this.squareSize)), this.widthMargin + this.spacerMargin + (((float) startX) * (this.spacerMargin + this.squareSize)) + this.squareSize, this.heightMargin + this.spacerMargin + this.squareSize + (((float) startY) * (this.spacerMargin + this.squareSize))), false, false, false, this.bomb, this.transparency, this.colors[this.rand.nextInt(this.colors.length)], this, this.faceMean);
                            } else if (this.rand.nextInt(15) == 6) {
                                this.squares[startX][startY] = new UnpoppableSquare(new RectF(this.widthMargin + this.spacerMargin + (((float) startX) * (this.spacerMargin + this.squareSize)), this.heightMargin + this.spacerMargin + (((float) startY) * (this.spacerMargin + this.squareSize)), this.widthMargin + this.spacerMargin + (((float) startX) * (this.spacerMargin + this.squareSize)) + this.squareSize, this.heightMargin + this.spacerMargin + this.squareSize + (((float) startY) * (this.spacerMargin + this.squareSize))), false, false, false, this.bomb, this.transparency, -7829368, this);
                            } else {
                                this.squares[startX][startY] = new Square(new RectF(this.widthMargin + this.spacerMargin + (((float) startX) * (this.spacerMargin + this.squareSize)), this.heightMargin + this.spacerMargin + (((float) startY) * (this.spacerMargin + this.squareSize)), this.widthMargin + this.spacerMargin + (((float) startX) * (this.spacerMargin + this.squareSize)) + this.squareSize, this.heightMargin + this.spacerMargin + this.squareSize + (((float) startY) * (this.spacerMargin + this.squareSize))), money2, health, bombsaway, this.bomb, this.transparency, this.colors[this.rand.nextInt(this.colors.length)], this);
                            }
                            j2 = this.gridSize;
                            i2 = this.gridSize;
                            if (bombsaway) {
                                this.bombInterval = this.rand.nextInt(60) + this.bombIntervalMin;
                                this.bombLastCheck = this.updates;
                            } else if (money2) {
                                this.moneyInterval = this.rand.nextInt(340) + this.moneyIntervalMin;
                                this.moneyLastCheck = this.updates;
                            } else if (health) {
                                this.healthInterval = this.rand.nextInt(340) + this.healthIntervalMin;
                                this.healthLastCheck = this.updates;
                            }
                        } else {
                            startX++;
                            startY++;
                            if (startX == this.gridSize) {
                                startX = 0;
                            }
                            if (startY == this.gridSize) {
                                startY = 0;
                            }
                        }
                        j2++;
                    }
                    i2++;
                }
            }
        }
        for (int i3 = 0; i3 < this.gridSize; i3++) {
            for (int j3 = 0; j3 < this.gridSize; j3++) {
                if (this.squares[i3][j3] != null) {
                    this.squares[i3][j3].update();
                    if (this.squares[i3][j3].expired()) {
                        if ((this.squares[i3][j3] instanceof HideAndSeekSquare) || (this.squares[i3][j3] instanceof UnpoppableSquare)) {
                            this.squares[i3][j3] = null;
                        } else {
                            this.playerHealth = this.playerHealth - 1;
                            if (i3 != this.gridSize - 1 && this.squares[i3 + 1][j3] != null && !this.squares[i3 + 1][j3].comingOnScreen() && this.squares[i3 + 1][j3].color() == this.squares[i3][j3].color()) {
                                this.playerHealth = this.playerHealth - 1;
                                this.squares[i3 + 1][j3] = null;
                            }
                            if (i3 != 0 && this.squares[i3 - 1][j3] != null && !this.squares[i3 - 1][j3].comingOnScreen() && this.squares[i3 - 1][j3].color() == this.squares[i3][j3].color()) {
                                this.playerHealth = this.playerHealth - 1;
                                this.squares[i3 - 1][j3] = null;
                            }
                            if (j3 != this.gridSize - 1 && this.squares[i3][j3 + 1] != null && !this.squares[i3][j3 + 1].comingOnScreen() && this.squares[i3][j3 + 1].color() == this.squares[i3][j3].color()) {
                                this.playerHealth = this.playerHealth - 1;
                                this.squares[i3][j3 + 1] = null;
                            }
                            if (j3 != 0 && this.squares[i3][j3 - 1] != null && !this.squares[i3][j3 - 1].comingOnScreen() && this.squares[i3][j3 - 1].color() == this.squares[i3][j3].color()) {
                                this.playerHealth = this.playerHealth - 1;
                                this.squares[i3][j3 - 1] = null;
                            }
                            this.squares[i3][j3] = null;
                            playSound(SOUND_SQUARE_POP);
                        }
                    }
                }
            }
        }
        if (this.gameType == MARATHON) {
            updateMarathon();
        } else if (this.gameType == LEVELS) {
            updateLevels();
        }
    }

    private void updateMarathon() {
    }

    private void updateLevels() {
        boolean newLevel = true;
        for (int i : this.levelSquareCount) {
            if (i > 0) {
                newLevel = false;
            }
        }
        if (newLevel) {
            this.level++;
            this.individualSquareCount++;
            if (this.individualSquareCount % 3 == 0 && this.squareCount < this.levelSquareCount.length) {
                this.squareCount++;
            }
            this.squares = (Square[][]) Array.newInstance(Square.class, this.gridSize, this.gridSize);
            this.updateLength = 30;
            this.updateLimit = 90;
            this.healthInterval = this.healthIntervalMin;
            this.moneyInterval = this.moneyIntervalMin;
            this.bombInterval = this.bombIntervalMin;
            this.moneyLastCheck = 0;
            this.healthLastCheck = 0;
            this.bombLastCheck = 0;
            this.updates = 0;
            initLevels();
        }
    }

    public void levelCompleteOver() {
        this.levelComplete = false;
    }

    public boolean levelComplete() {
        return this.levelComplete;
    }

    public void handleInput(MotionEvent event) {
        if (event.getAction() == 0) {
            this.click = true;
            this.clickX = event.getX();
            this.clickY = event.getY();
        } else if (event.getAction() == 1) {
            this.click = false;
        }
    }

    public int getScore() {
        int s = this.playerScore;
        this.playerScore = 0;
        return s;
    }

    public void pauseOn() {
        this.pause = System.currentTimeMillis();
    }

    public void pauseOff() {
        this.pause = System.currentTimeMillis() - this.pause;
    }

    public void recycleBitmaps() {
    }

    public static String commas(int number) {
        String text = String.valueOf(number);
        for (int x = text.length() - 3; x > 0; x -= 3) {
            text = String.valueOf(text.substring(0, x)) + "," + text.substring(x, text.length());
        }
        return text;
    }

    public void playSound(String sound) {
        this.soundPool.play(this.soundMap.get(sound).intValue(), 1.0f, 1.0f, 0, 0, 1.0f);
    }

    public void saveStats(SharedPreferences preferences) {
        if ((this.pops != 0 || this.misses != 0) && this.gameType != LEVELS) {
            SharedPreferences.Editor edit = preferences.edit();
            if (this.playerHealth < 1) {
                edit.putInt(SquarePopActivity.PREFERENCE_STAT_GAMES_COMPLETED, preferences.getInt(SquarePopActivity.PREFERENCE_STAT_GAMES_COMPLETED, 0) + 1);
            }
            edit.putInt(SquarePopActivity.PREFERENCE_STAT_GAMES_PLAYED, preferences.getInt(SquarePopActivity.PREFERENCE_STAT_GAMES_PLAYED, 0) + 1);
            edit.putInt(SquarePopActivity.PREFERENCE_STAT_POPS, preferences.getInt(SquarePopActivity.PREFERENCE_STAT_POPS, 0) + this.pops);
            edit.putInt(SquarePopActivity.PREFERENCE_STAT_MISSES, preferences.getInt(SquarePopActivity.PREFERENCE_STAT_MISSES, 0) + this.misses);
            edit.putInt(SquarePopActivity.PREFERENCE_STAT_POP_COLOR_0, preferences.getInt(SquarePopActivity.PREFERENCE_STAT_POP_COLOR_0, 0) + this.popColor0);
            edit.putInt(SquarePopActivity.PREFERENCE_STAT_POP_COLOR_1, preferences.getInt(SquarePopActivity.PREFERENCE_STAT_POP_COLOR_1, 0) + this.popColor1);
            edit.putInt(SquarePopActivity.PREFERENCE_STAT_POP_COLOR_2, preferences.getInt(SquarePopActivity.PREFERENCE_STAT_POP_COLOR_2, 0) + this.popColor2);
            edit.putInt(SquarePopActivity.PREFERENCE_STAT_POP_COLOR_3, preferences.getInt(SquarePopActivity.PREFERENCE_STAT_POP_COLOR_3, 0) + this.popColor3);
            edit.putInt(SquarePopActivity.PREFERENCE_STAT_POP_COLOR_4, preferences.getInt(SquarePopActivity.PREFERENCE_STAT_POP_COLOR_4, 0) + this.popColor4);
            edit.putInt(SquarePopActivity.PREFERENCE_STAT_POP_COLOR_5, preferences.getInt(SquarePopActivity.PREFERENCE_STAT_POP_COLOR_5, 0) + this.popColor5);
            edit.putInt(SquarePopActivity.PREFERENCE_STAT_POP_COLOR_6, preferences.getInt(SquarePopActivity.PREFERENCE_STAT_POP_COLOR_6, 0) + this.popColor6);
            edit.putInt(SquarePopActivity.PREFERENCE_STAT_HEALTH_POPS, preferences.getInt(SquarePopActivity.PREFERENCE_STAT_HEALTH_POPS, 0) + this.healthPops);
            edit.putInt(SquarePopActivity.PREFERENCE_STAT_SCORE_POPS, preferences.getInt(SquarePopActivity.PREFERENCE_STAT_SCORE_POPS, 0) + this.scorePops);
            edit.putInt(SquarePopActivity.PREFERENCE_STAT_TOTAL_SCORE, preferences.getInt(SquarePopActivity.PREFERENCE_STAT_TOTAL_SCORE, 0) + this.playerScore);
            edit.commit();
            this.pops = 0;
            this.misses = 0;
            this.healthPops = 0;
            this.scorePops = 0;
            this.popColor0 = 0;
            this.popColor1 = 0;
            this.popColor2 = 0;
            this.popColor3 = 0;
            this.popColor4 = 0;
            this.popColor5 = 0;
            this.popColor6 = 0;
        }
    }

    public int getType() {
        return this.gameType;
    }

    public int getLevel() {
        return this.level;
    }
}
