package com.woodlawn.squarepop;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.SoundPool;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.HashMap;

public class AnimationView extends SurfaceView implements SurfaceHolder.Callback {
    private static String TAG = "AnimationView";
    /* access modifiers changed from: private */
    public SquarePopActivity myActivity;
    public Dialog playAgainDialog;
    public Dialog playAgainLostDialog;
    private Manager qb;
    private AnimationThread thread;

    public AnimationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        if (!isInEditMode()) {
            this.qb = new Manager((float) getWidth(), (float) getHeight());
            this.thread = new AnimationThread(holder, this.qb, new Handler() {
                public void handleMessage(Message m) {
                    AnimationView.this.pause();
                    AnimationView.this.myActivity.handleMessage(m);
                }
            }, this);
        }
        setFocusable(true);
        setResources(context);
        Log.d(TAG, "@@@ done creating view!");
    }

    public void setResources(Context context) {
        Resources r = getContext().getResources();
        Bitmap spacerHorizontal = BitmapFactory.decodeResource(r, R.drawable.spacerhorizontal);
        Bitmap spacerVertical = BitmapFactory.decodeResource(r, R.drawable.spacervertical);
        Bitmap transparency = BitmapFactory.decodeResource(r, R.drawable.poptrans);
        Bitmap spacerHorizontalBottom = BitmapFactory.decodeResource(r, R.drawable.spacerhorizontalbottom);
        Bitmap spacerVerticalRight = BitmapFactory.decodeResource(r, R.drawable.spacerverticalright);
        Bitmap spacerBottomRight = BitmapFactory.decodeResource(r, R.drawable.spacerbottomright);
        Bitmap spacerBottomLeft = BitmapFactory.decodeResource(r, R.drawable.spacerbottomleft);
        Bitmap spacerTopRight = BitmapFactory.decodeResource(r, R.drawable.spacertopright);
        Bitmap spacerTopLeft = BitmapFactory.decodeResource(r, R.drawable.spacertopleft);
        Bitmap spacerCorner = BitmapFactory.decodeResource(r, R.drawable.spacerbottomcorner);
        this.qb.setSpacerBottomLeft(spacerBottomLeft);
        this.qb.setSpacerBottomRight(spacerBottomRight);
        this.qb.setSpacerTopLeft(spacerTopLeft);
        this.qb.setSpacerTopRight(spacerTopRight);
        this.qb.setSpacerCorner(spacerCorner);
        this.qb.setSpacerHorizontal(spacerHorizontal);
        this.qb.setSpacerVertical(spacerVertical);
        this.qb.setTransparency(transparency);
        this.qb.setSpacerHorizontalBottom(spacerHorizontalBottom);
        this.qb.setSpacerVerticalRight(spacerVerticalRight);
        this.qb.setBomb(BitmapFactory.decodeResource(r, R.drawable.bomb));
        this.qb.setFaceMean(BitmapFactory.decodeResource(r, R.drawable.face_mean));
        this.qb.setBrickOverlay(BitmapFactory.decodeResource(r, R.drawable.brick));
        SoundPool soundPool = new SoundPool(7, 3, 0);
        HashMap<String, Integer> soundMap = new HashMap<>();
        soundMap.put(Manager.SOUND_BUZZ, Integer.valueOf(soundPool.load(context, R.raw.buzz, 1)));
        soundMap.put(Manager.SOUND_POP, Integer.valueOf(soundPool.load(context, R.raw.pop, 1)));
        soundMap.put(Manager.SOUND_SQUARE_POP, Integer.valueOf(soundPool.load(context, R.raw.squarepop, 1)));
        soundMap.put(Manager.SOUND_BAD_POP, Integer.valueOf(soundPool.load(context, R.raw.bad_pop, 1)));
        soundMap.put(Manager.SOUND_UNPOP, Integer.valueOf(soundPool.load(context, R.raw.unpoppable, 1)));
        this.qb.setSoundPool(soundPool);
        this.qb.setSoundMap(soundMap);
    }

    public void setTypeface(Typeface typeface) {
        this.thread.setFont(typeface);
    }

    public void setActivity(SquarePopActivity activity) {
        this.myActivity = activity;
    }

    public void surfaceChanged(SurfaceHolder arg0, int format, int width, int height) {
        this.qb.setXMax((float) width);
        this.qb.setYMax((float) height);
        this.thread.setSurfaceSize(width, height);
    }

    public void surfaceCreated(SurfaceHolder arg0) {
        Log.d(TAG, "@@@ surfaceCreated called!");
        this.thread.setRunning(true);
    }

    public void newGame(boolean firsttime, int gameType) {
        this.qb.init(gameType);
        this.thread.play();
        if (firsttime) {
            this.thread.start();
        }
    }

    public void saveScore() {
        this.qb.saveScore();
    }

    public int getScore() {
        return this.qb.getScore();
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
        boolean retry = true;
        this.thread.setRunning(false);
        while (retry) {
            try {
                this.thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        this.qb.handleInput(event);
        if (event.getAction() != 1) {
            return true;
        }
        return false;
    }

    public void restartThread() {
        this.thread = new AnimationThread(getHolder(), this.qb, new Handler() {
            public void handleMessage(Message m) {
                AnimationView.this.myActivity.handleMessage(m);
            }
        }, this);
    }

    public void pause() {
        this.thread.pause();
    }

    public void recycleBitmaps() {
        this.qb.recycleBitmaps();
    }

    public int getNewHighScore() {
        return this.qb.getNewHighScore();
    }

    public void saveStats(SharedPreferences preferences) {
        this.qb.saveStats(preferences);
    }

    public int getLevel() {
        return this.qb.getLevel();
    }

    public int getType() {
        return this.qb.getType();
    }
}
