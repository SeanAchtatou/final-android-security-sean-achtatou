package com.woodlawn.squarepop;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;

public class AnimationThread extends Thread {
    private static int STATE_LOSE = 3;
    private static int STATE_PAUSE = 4;
    private static int STATE_PLAY = 2;
    private static int STATE_RUNNING = 1;
    private static String TAG = "AnimationThread";
    private boolean initialized = false;
    public Handler mHandler;
    private final Paint mPaint = new Paint();
    private boolean onHold = false;
    private Manager quarterback;
    private boolean run;
    private SurfaceHolder surfaceHolder;
    private int threadState;

    public AnimationThread(SurfaceHolder surfaceHolder2, Manager quarterback2, Handler handler, View parentView) {
        this.surfaceHolder = surfaceHolder2;
        this.quarterback = quarterback2;
        this.mHandler = handler;
    }

    public void setFont(Typeface typeface) {
        this.mPaint.setTypeface(typeface);
    }

    public void restart() {
        if (!isAlive()) {
            start();
        }
    }

    public void setManager(Manager quarterback2) {
        this.quarterback = quarterback2;
    }

    public void pause() {
        if (this.threadState != STATE_PAUSE) {
            this.threadState = STATE_PAUSE;
            this.quarterback.pauseOn();
            return;
        }
        this.threadState = STATE_RUNNING;
        this.quarterback.pauseOff();
    }

    private void updateGameState(long now) {
        this.quarterback.update(now);
    }

    private void doDraw(Canvas c) {
        this.quarterback.draw(c, this.mPaint);
    }

    public void play() {
        this.threadState = STATE_RUNNING;
        this.onHold = false;
    }

    public void run() {
        Log.d("THREAD", "running sucka - start");
        long timer = System.currentTimeMillis();
        while (this.run) {
            Canvas c = null;
            long now = System.currentTimeMillis();
            boolean frame = false;
            if (now - timer >= 34) {
                frame = true;
            }
            if (this.threadState == STATE_RUNNING) {
                if (frame) {
                    updateGameState(now);
                }
                this.onHold = false;
            } else if ((this.threadState != STATE_PLAY || this.initialized) && this.threadState == STATE_LOSE) {
                this.initialized = false;
            }
            if (frame) {
                try {
                    c = this.surfaceHolder.lockCanvas(null);
                    doDraw(c);
                    timer = now;
                } finally {
                    if (c != null) {
                        this.surfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
            if (this.quarterback.gameOver() && !this.onHold) {
                this.threadState = STATE_LOSE;
                Message msg = this.mHandler.obtainMessage();
                Bundle b = new Bundle();
                b.putInt("outcome", 1);
                msg.setData(b);
                this.mHandler.sendMessage(msg);
                this.onHold = true;
            } else if (this.quarterback.levelComplete()) {
                Message msg2 = this.mHandler.obtainMessage();
                Bundle b2 = new Bundle();
                b2.putInt("outcome", 2);
                msg2.setData(b2);
                this.mHandler.sendMessage(msg2);
                this.quarterback.levelCompleteOver();
            }
        }
        Log.d("THREAD", "running sucka - end");
    }

    public void setSurfaceSize(int width, int height) {
        synchronized (this.surfaceHolder) {
        }
    }

    public void setRunning(boolean run2) {
        Log.e(TAG, "setRunning called, run = " + run2);
        this.run = run2;
    }
}
