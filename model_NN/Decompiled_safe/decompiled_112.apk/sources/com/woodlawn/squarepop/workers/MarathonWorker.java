package com.woodlawn.squarepop.workers;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.woodlawn.defense.objects.Square;
import com.woodlawn.squarepop.Manager;

public class MarathonWorker {
    public void draw(Canvas c, Paint paint, float x_max, float y_max, int backgroundColor, int playerHealth, float spacerMargin, float widthMargin, float heightMargin, float squareSize, Bitmap bomb, int playerScore, float puzzleWidth, Bitmap grid, Square[][] squares, int gridSize, float clickX, float clickY) {
        if (c != null) {
            paint.setColor(backgroundColor);
            c.drawRect(new RectF(0.0f, 0.0f, x_max, y_max), paint);
            paint.setColor(-1);
            for (int i = 0; i < playerHealth; i++) {
                c.drawRect(((float) (i * 12)) + spacerMargin + widthMargin, heightMargin - (3.0f * spacerMargin), 10.0f + spacerMargin + widthMargin + ((float) (i * 12)), heightMargin - spacerMargin, paint);
            }
            int alpha = paint.getAlpha();
            float oldTextSize = paint.getTextSize();
            paint.setTextSize(24.0f);
            Rect bounds = new Rect();
            paint.getTextBounds("S", 0, 1, bounds);
            paint.setTextAlign(Paint.Align.CENTER);
            c.drawText("Score: " + Manager.commas(playerScore), x_max / 2.0f, heightMargin + puzzleWidth + ((float) (bounds.bottom - bounds.top)) + (spacerMargin / 2.0f), paint);
            paint.setTextAlign(Paint.Align.LEFT);
            paint.setTextSize(oldTextSize);
            c.drawBitmap(grid, (Rect) null, new RectF(0.0f, 0.0f, x_max, y_max), paint);
            for (Square[] row : squares) {
                for (Square square : squares[r6]) {
                    if (square != null) {
                        square.draw(c, paint);
                    }
                }
            }
        }
    }
}
