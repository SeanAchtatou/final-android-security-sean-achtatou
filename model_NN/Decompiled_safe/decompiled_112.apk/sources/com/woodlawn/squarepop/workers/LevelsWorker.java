package com.woodlawn.squarepop.workers;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.woodlawn.defense.objects.Square;
import com.woodlawn.squarepop.Manager;

public class LevelsWorker {
    public void draw(Canvas c, Paint paint, float x_max, float y_max, int backgroundColor, int playerHealth, float spacerMargin, float widthMargin, float heightMargin, float squareSize, Bitmap bomb, int playerScore, float puzzleWidth, Bitmap grid, Square[][] squares, int gridSize, float clickX, float clickY, int[] levelSquareCount, int levelColorSetIndex, int[][] levelsColorSets, int level) {
        if (c != null) {
            paint.setColor(backgroundColor);
            c.drawRect(new RectF(0.0f, 0.0f, x_max, y_max), paint);
            paint.setColor(-1);
            Rect bounds = new Rect();
            String levelText = "Level " + level;
            float oldTextSize = paint.getTextSize();
            paint.setTextSize(1.5f * spacerMargin);
            paint.getTextBounds(levelText, 0, levelText.length(), bounds);
            c.drawText(levelText, (x_max - spacerMargin) - ((float) (bounds.right - bounds.left)), ((float) (bounds.bottom - bounds.top)) + spacerMargin, paint);
            paint.setTextSize(oldTextSize);
            float m = 0.0f;
            for (int i = 0; i < levelsColorSets[levelColorSetIndex].length; i++) {
                for (int j = levelSquareCount[i] - 1; j > 0; j--) {
                    paint.setStyle(Paint.Style.FILL);
                    paint.setColor(levelsColorSets[levelColorSetIndex][i]);
                    c.drawRect(((float) (j * 2)) + m + widthMargin + 0.0f + ((float) (i * 5)) + (((float) i) * spacerMargin), ((float) (j * 2)) + 0.0f + spacerMargin, ((float) (j * 2)) + m + widthMargin + spacerMargin + ((float) (i * 5)) + (((float) i) * spacerMargin), ((float) (j * 2)) + spacerMargin + spacerMargin, paint);
                    paint.setColor(-16777216);
                    paint.setStyle(Paint.Style.STROKE);
                    c.drawRect(((float) (j * 2)) + m + widthMargin + 0.0f + ((float) (i * 5)) + (((float) i) * spacerMargin), ((float) (j * 2)) + 0.0f + spacerMargin, ((float) (j * 2)) + m + widthMargin + spacerMargin + ((float) (i * 5)) + (((float) i) * spacerMargin), ((float) (j * 2)) + spacerMargin + spacerMargin, paint);
                }
                paint.setColor(levelsColorSets[levelColorSetIndex][i]);
                paint.setStyle(Paint.Style.FILL);
                if (levelSquareCount[i] == 0) {
                    paint.setAlpha(45);
                }
                c.drawRect(((float) (i * 5)) + (((float) i) * spacerMargin) + m + widthMargin + 0.0f, 0.0f + spacerMargin, ((float) (i * 5)) + (((float) i) * spacerMargin) + m + widthMargin + spacerMargin, spacerMargin + spacerMargin, paint);
                if (levelSquareCount[i] == 0) {
                    paint.setAlpha(255);
                } else {
                    paint.setStyle(Paint.Style.STROKE);
                    paint.setColor(-16777216);
                    c.drawRect(((float) (i * 5)) + (((float) i) * spacerMargin) + m + widthMargin + 0.0f, 0.0f + spacerMargin, ((float) (i * 5)) + (((float) i) * spacerMargin) + m + widthMargin + spacerMargin, spacerMargin + spacerMargin, paint);
                    paint.setStyle(Paint.Style.FILL);
                }
                m += (float) (levelSquareCount[i] * 4);
            }
            paint.setColor(-1);
            for (int i2 = 0; i2 < playerHealth; i2++) {
                c.drawRect(((float) (i2 * 12)) + spacerMargin + widthMargin, heightMargin - (3.0f * spacerMargin), 10.0f + spacerMargin + widthMargin + ((float) (i2 * 12)), heightMargin - spacerMargin, paint);
            }
            float oldTextSize2 = paint.getTextSize();
            paint.setTextSize(24.0f);
            paint.getTextBounds("S", 0, 1, bounds);
            paint.setTextAlign(Paint.Align.CENTER);
            c.drawText("Score: " + Manager.commas(playerScore), x_max / 2.0f, heightMargin + puzzleWidth + ((float) (bounds.bottom - bounds.top)) + (spacerMargin / 2.0f), paint);
            paint.setTextAlign(Paint.Align.LEFT);
            paint.setTextSize(oldTextSize2);
            c.drawBitmap(grid, (Rect) null, new RectF(0.0f, 0.0f, x_max, y_max), paint);
            for (Square[] row : squares) {
                for (Square square : squares[r6]) {
                    if (square != null) {
                        square.draw(c, paint);
                    }
                }
            }
        }
    }
}
