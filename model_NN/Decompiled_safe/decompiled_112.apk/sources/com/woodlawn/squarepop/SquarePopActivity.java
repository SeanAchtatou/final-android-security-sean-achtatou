package com.woodlawn.squarepop;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.io.InputStream;
import java.util.Random;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class SquarePopActivity extends Activity implements AdListener {
    private static final int ABANDON_GAME_DIALOG = 6;
    private static final int GAME_LOST_DIALOG = 4;
    private static final int GAME_QUIT_DIALOG = 5;
    public static final String PREFERENCE_GAME_TYPE = "GAME_TYPE";
    public static String PREFERENCE_HIGH_LEVEL = "HIGH_LEVEL";
    public static String PREFERENCE_HIGH_SCORE = "HIGH_SCORE_";
    public static String PREFERENCE_LAST_HEALTH = "LAST_HEALTH";
    public static String PREFERENCE_LAST_LEVEL = "LAST_LEVEL";
    public static String PREFERENCE_LAST_SCORE = "LAST_SCORE";
    public static final String PREFERENCE_STAT_GAMES_COMPLETED = "STAT_GAMES_COMPLETED";
    public static final String PREFERENCE_STAT_GAMES_PLAYED = "STAT_GAMES_PLAYED";
    public static final String PREFERENCE_STAT_HEALTH_POPS = "STAT_HEALTH_POPS";
    public static final String PREFERENCE_STAT_MISSES = "STAT_MISSES";
    public static final String PREFERENCE_STAT_POPS = "STAT_POPS";
    public static final String PREFERENCE_STAT_POP_COLOR_0 = "STAT_POP_COLOR_0";
    public static final String PREFERENCE_STAT_POP_COLOR_1 = "STAT_POP_COLOR_1";
    public static final String PREFERENCE_STAT_POP_COLOR_2 = "STAT_POP_COLOR_2";
    public static final String PREFERENCE_STAT_POP_COLOR_3 = "STAT_POP_COLOR_3";
    public static final String PREFERENCE_STAT_POP_COLOR_4 = "STAT_POP_COLOR_4";
    public static final String PREFERENCE_STAT_POP_COLOR_5 = "STAT_POP_COLOR_5";
    public static final String PREFERENCE_STAT_POP_COLOR_6 = "STAT_POP_COLOR_6";
    public static final String PREFERENCE_STAT_SCORE_POPS = "STAT_SCORE_POPS";
    public static final String PREFERENCE_STAT_TOTAL_SCORE = "STAT_TOTAL_SCORE";
    public static final String PREFERENCE_UPDATE = "PREFERENCE_UPDATE";
    private static final int UPDATE_DIALOG = 10;
    private boolean comingFromStats = false;
    private boolean firstLoad = true;
    private boolean firsttime = true;
    private boolean loading;
    private boolean over = false;
    private MediaPlayer trumpet;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = getPreferences(0);
        setVolumeControlStream(3);
        setContentView(R.layout.main);
        this.loading = true;
        ((TextView) findViewById(R.id.highscoretext)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((TextView) findViewById(R.id.highleveltext)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        TextView title = (TextView) findViewById(R.id.menutitle);
        title.setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        title.setTextSize(title.getTextSize() * 2.0f);
        ((TextView) findViewById(R.id.losing_text)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        Button quitButton = (Button) findViewById(R.id.quit_button);
        quitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SquarePopActivity.this.finish();
            }
        });
        quitButton.setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        Button playButton = (Button) findViewById(R.id.play_levels_button);
        playButton.setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        playButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferences.Editor edit = SquarePopActivity.this.getPreferences(0).edit();
                edit.putInt(SquarePopActivity.PREFERENCE_GAME_TYPE, 1);
                edit.commit();
                SquarePopActivity.this.hideMainMenu();
                SquarePopActivity.this.initNewGame(Manager.LEVELS);
            }
        });
        Button playMarathonButton = (Button) findViewById(R.id.play_marathon_button);
        playMarathonButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferences.Editor edit = SquarePopActivity.this.getPreferences(0).edit();
                edit.putInt(SquarePopActivity.PREFERENCE_GAME_TYPE, 2);
                edit.commit();
                SquarePopActivity.this.hideMainMenu();
                SquarePopActivity.this.initNewGame(Manager.MARATHON);
            }
        });
        playMarathonButton.setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        Button statsButton = (Button) findViewById(R.id.stat_button);
        statsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SquarePopActivity.this.hideMainMenu();
                SquarePopActivity.this.showStats();
            }
        });
        statsButton.setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        Button closeStatsButton = (Button) findViewById(R.id.close_stats_button);
        closeStatsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SquarePopActivity.this.hideStats();
                SquarePopActivity.this.showMainMenu();
            }
        });
        closeStatsButton.setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        TextView tv = (TextView) findViewById(R.id.statstitle);
        tv.setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        tv.setTextSize(tv.getTextSize() * 2.0f);
        ((TextView) findViewById(R.id.stats_total_score_text)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((TextView) findViewById(R.id.stats_avg_score_text)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((TextView) findViewById(R.id.stats_games_played_text)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((TextView) findViewById(R.id.stats_games_completed_text)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((TextView) findViewById(R.id.stats_pops_text)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((TextView) findViewById(R.id.stats_misses_text)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((TextView) findViewById(R.id.stats_accuracy_text)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((TextView) findViewById(R.id.stats_0)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((TextView) findViewById(R.id.stats_1)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((TextView) findViewById(R.id.stats_2)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((TextView) findViewById(R.id.stats_3)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((TextView) findViewById(R.id.stats_4)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((TextView) findViewById(R.id.stats_5)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((TextView) findViewById(R.id.stats_6)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((TextView) findViewById(R.id.stats_popstitle_text)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((TextView) findViewById(R.id.loading_text)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((ImageView) findViewById(R.id.loading_icon)).startAnimation(AnimationUtils.loadAnimation(this, R.anim.loading));
        View a = findViewById(R.id.ad);
        AdRequest adRequest = new AdRequest();
        AdView adView = (AdView) a;
        adView.setAdListener(this);
        adView.loadAd(adRequest);
        AnimationView view = (AnimationView) findViewById(R.id.AnimationView);
        view.setActivity(this);
        view.setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        setupFirstTime();
        refreshHighScore();
        this.trumpet = MediaPlayer.create(this, R.raw.highscore);
        if (preferences.getInt(PREFERENCE_UPDATE, 1) == 1) {
            new UpdateTask(this, null).execute(new String[0]);
        }
        hideMainMenu();
        refreshHighScore();
    }

    private void setupFirstTime() {
        ((Button) findViewById(R.id.close_button_first_time)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferences preferences = SquarePopActivity.this.getPreferences(0);
                int type = preferences.getInt(SquarePopActivity.PREFERENCE_GAME_TYPE, -1);
                if (type == -1) {
                    SharedPreferences.Editor edit = preferences.edit();
                    edit.putInt(SquarePopActivity.PREFERENCE_GAME_TYPE, 1);
                    edit.commit();
                    type = 1;
                }
                SquarePopActivity.this.initNewGame(type);
                SquarePopActivity.this.hideFirstTime();
            }
        });
    }

    public void showFirstTime() {
        findViewById(R.id.first_time_layout).setVisibility(0);
    }

    public void hideFirstTime() {
        findViewById(R.id.first_time_layout).setVisibility(8);
    }

    public void refreshHighScore() {
        TextView highScoreTxt = (TextView) findViewById(R.id.highscoretext);
        int[] scores = getHighScore();
        String text = "High Score";
        if (scores[0] == 0) {
            text = "Not Played Yet";
        } else {
            for (int i = 0; i < 1; i++) {
                text = String.valueOf(text) + ": " + Manager.commas(scores[i]);
            }
        }
        highScoreTxt.setText(text);
        ((TextView) findViewById(R.id.highleveltext)).setText("Highest Level: " + getPreferences(0).getInt(PREFERENCE_HIGH_LEVEL, 0));
    }

    public void showMainMenu() {
        if (this.comingFromStats) {
            findViewById(R.id.losing_face0).setVisibility(0);
            findViewById(R.id.losing_face1).setVisibility(8);
            findViewById(R.id.losing_face2).setVisibility(8);
            findViewById(R.id.losing_face3).setVisibility(8);
            findViewById(R.id.losing_face4).setVisibility(8);
            findViewById(R.id.losing_face5).setVisibility(8);
            findViewById(R.id.losing_face6).setVisibility(8);
            TextView losingText = (TextView) findViewById(R.id.losing_text);
            String[] taunts = {"Those stats are pathetic!", "Pumped up now? I wouldn't be after looking at those stats.", "My schnauzer has better stats."};
            losingText.setText(taunts[new Random().nextInt(taunts.length)]);
            losingText.setVisibility(0);
            this.comingFromStats = false;
        } else if (!this.firstLoad) {
            findViewById(R.id.losing_face0).setVisibility(8);
            findViewById(R.id.losing_face1).setVisibility(8);
            findViewById(R.id.losing_face2).setVisibility(8);
            findViewById(R.id.losing_face3).setVisibility(8);
            findViewById(R.id.losing_face4).setVisibility(8);
            findViewById(R.id.losing_face5).setVisibility(8);
            findViewById(R.id.losing_face6).setVisibility(8);
            Random rand = new Random();
            int index = rand.nextInt(7);
            if (index == 0) {
                findViewById(R.id.losing_face0).setVisibility(0);
            } else if (index == 1) {
                findViewById(R.id.losing_face1).setVisibility(0);
            } else if (index == 2) {
                findViewById(R.id.losing_face2).setVisibility(0);
            } else if (index == 3) {
                findViewById(R.id.losing_face3).setVisibility(0);
            } else if (index == GAME_LOST_DIALOG) {
                findViewById(R.id.losing_face4).setVisibility(0);
            } else if (index == GAME_QUIT_DIALOG) {
                findViewById(R.id.losing_face5).setVisibility(0);
            } else if (index == ABANDON_GAME_DIALOG) {
                findViewById(R.id.losing_face6).setVisibility(0);
            }
            TextView losingText2 = (TextView) findViewById(R.id.losing_text);
            String[] taunts2 = new String[8];
            taunts2[0] = "Better luck next time.";
            taunts2[1] = "It's not if you when or lose, it's how you play the game, right?";
            taunts2[2] = "Why waste my time?";
            taunts2[3] = "Buh-bye.";
            taunts2[GAME_LOST_DIALOG] = "Tough luck bub, better try again.";
            taunts2[GAME_QUIT_DIALOG] = "Try again?";
            taunts2[ABANDON_GAME_DIALOG] = "Okay, practice is over, how about playing for real?";
            taunts2[7] = "Do or do not, there is no try.";
            losingText2.setText(taunts2[rand.nextInt(taunts2.length)]);
            losingText2.setVisibility(0);
        } else {
            this.firstLoad = false;
        }
        if (this.loading) {
            ImageView loadingIcon = (ImageView) findViewById(R.id.loading_icon);
            loadingIcon.clearAnimation();
            loadingIcon.setVisibility(8);
            findViewById(R.id.loading_layout).setVisibility(8);
            this.loading = false;
        }
        findViewById(R.id.layout_root).setVisibility(0);
        refreshHighScore();
        View a = findViewById(R.id.ad);
        a.setVisibility(0);
        a.setClickable(true);
    }

    public void hideMainMenu() {
        findViewById(R.id.layout_root).setVisibility(8);
        View a = findViewById(R.id.ad);
        a.setVisibility(GAME_LOST_DIALOG);
        a.setClickable(false);
        ((AdView) a).loadAd(new AdRequest());
    }

    public void showStats() {
        SharedPreferences preferences = getPreferences(0);
        int gamesCompleted = preferences.getInt(PREFERENCE_STAT_GAMES_COMPLETED, 0);
        int gamesPlayed = preferences.getInt(PREFERENCE_STAT_GAMES_PLAYED, 0);
        int i = preferences.getInt(PREFERENCE_STAT_HEALTH_POPS, 0);
        int misses = preferences.getInt(PREFERENCE_STAT_MISSES, 0);
        int color0 = preferences.getInt(PREFERENCE_STAT_POP_COLOR_0, 0);
        int color1 = preferences.getInt(PREFERENCE_STAT_POP_COLOR_1, 0);
        int color2 = preferences.getInt(PREFERENCE_STAT_POP_COLOR_2, 0);
        int color3 = preferences.getInt(PREFERENCE_STAT_POP_COLOR_3, 0);
        int color4 = preferences.getInt(PREFERENCE_STAT_POP_COLOR_4, 0);
        int color5 = preferences.getInt(PREFERENCE_STAT_POP_COLOR_5, 0);
        int color6 = preferences.getInt(PREFERENCE_STAT_POP_COLOR_6, 0);
        int pops = preferences.getInt(PREFERENCE_STAT_POPS, 0);
        int i2 = preferences.getInt(PREFERENCE_STAT_SCORE_POPS, 0);
        int totalScore = preferences.getInt(PREFERENCE_STAT_TOTAL_SCORE, 0);
        ((TextView) findViewById(R.id.stats_total_score_text)).setText("Total Score: " + totalScore);
        TextView tv = (TextView) findViewById(R.id.stats_avg_score_text);
        float averageScore = 0.0f;
        if (gamesPlayed != 0) {
            averageScore = ((float) totalScore) / ((float) gamesPlayed);
        }
        tv.setText("Average Score: " + Math.round(averageScore));
        ((TextView) findViewById(R.id.stats_games_played_text)).setText("Games Played: " + gamesPlayed);
        ((TextView) findViewById(R.id.stats_games_completed_text)).setText("Games Completed: " + gamesCompleted);
        ((TextView) findViewById(R.id.stats_pops_text)).setText("Pops: " + pops);
        ((TextView) findViewById(R.id.stats_misses_text)).setText("Misses: " + misses);
        TextView tv2 = (TextView) findViewById(R.id.stats_accuracy_text);
        float accuracy = 0.0f;
        if (misses == 0 && pops > 0) {
            accuracy = 1.0f;
        } else if (!(misses == 0 || pops == 0)) {
            accuracy = ((float) pops) / (((float) misses) + ((float) pops));
        }
        tv2.setText("Accuracy: " + ((int) (accuracy * 100.0f)) + "%");
        ((TextView) findViewById(R.id.stats_0)).setText(" - " + color0);
        ((TextView) findViewById(R.id.stats_1)).setText(" - " + color1);
        ((TextView) findViewById(R.id.stats_2)).setText(" - " + color2);
        ((TextView) findViewById(R.id.stats_3)).setText(" - " + color3);
        ((TextView) findViewById(R.id.stats_4)).setText(" - " + color4);
        ((TextView) findViewById(R.id.stats_5)).setText(" - " + color5);
        ((TextView) findViewById(R.id.stats_6)).setText(" - " + color6);
        findViewById(R.id.stats_layout).setVisibility(0);
    }

    public void hideStats() {
        findViewById(R.id.stats_layout).setVisibility(8);
        this.comingFromStats = true;
    }

    private int[] getHighScore() {
        SharedPreferences preferences = getPreferences(0);
        int[] scores = new int[GAME_QUIT_DIALOG];
        for (int i = 0; i < scores.length; i++) {
            scores[i] = preferences.getInt(String.valueOf(PREFERENCE_HIGH_SCORE) + "_" + (i + 1), 0);
        }
        return scores;
    }

    private void saveHighScore(int[] scores) {
        SharedPreferences.Editor edit = getPreferences(0).edit();
        for (int i = 0; i < scores.length; i++) {
            edit.putInt(String.valueOf(PREFERENCE_HIGH_SCORE) + "_" + (i + 1), scores[i]);
        }
        edit.commit();
    }

    /* access modifiers changed from: private */
    public void saveHighLevel(int level) {
        SharedPreferences preferences = getPreferences(0);
        if (preferences.getInt(PREFERENCE_HIGH_LEVEL, 0) < level) {
            SharedPreferences.Editor edit = preferences.edit();
            edit.putInt(PREFERENCE_HIGH_LEVEL, level);
            edit.commit();
        }
    }

    public void saveHighScore(int newScore) {
        int[] scores = getHighScore();
        int i = 0;
        while (true) {
            if (i >= scores.length) {
                break;
            } else if (newScore > scores[i]) {
                if (i == 0) {
                    Toast.makeText(this, "New High Score!", 1).show();
                    this.trumpet.start();
                }
                for (int j = scores.length - 1; j > i; j--) {
                    scores[j] = scores[j - 1];
                }
                scores[i] = newScore;
            } else {
                i++;
            }
        }
        saveHighScore(scores);
    }

    public void onPause() {
        AnimationView view = (AnimationView) findViewById(R.id.AnimationView);
        view.pause();
        view.saveStats(getPreferences(0));
        if (view.getType() == Manager.LEVELS) {
            saveHighLevel(view.getLevel());
        }
        saveHighScore(view.getScore());
        this.firstLoad = true;
        super.onPause();
    }

    public void onSaveInstanceState(Bundle outState) {
        AnimationView view = (AnimationView) findViewById(R.id.AnimationView);
        view.saveStats(getPreferences(0));
        if (view.getType() == Manager.LEVELS) {
            saveHighLevel(view.getLevel());
        }
        saveHighScore(view.getScore());
        super.onSaveInstanceState(outState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        SharedPreferences preferences = getPreferences(0);
        int type = preferences.getInt(PREFERENCE_GAME_TYPE, -1);
        if (type == -1) {
            SharedPreferences.Editor edit = preferences.edit();
            edit.putInt(PREFERENCE_GAME_TYPE, 1);
            edit.commit();
            type = 1;
        }
        initNewGame(type);
        super.onRestoreInstanceState(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        SharedPreferences preferences = getPreferences(0);
        int type = preferences.getInt(PREFERENCE_GAME_TYPE, -1);
        if (type == -1) {
            SharedPreferences.Editor edit = preferences.edit();
            edit.putInt(PREFERENCE_GAME_TYPE, 1);
            edit.commit();
            type = 1;
        }
        initNewGame(type);
        super.onRestart();
    }

    public void onResume() {
        Log.d("DEFENSEACTIVITY", "onResume sucka");
        AnimationView view = (AnimationView) findViewById(R.id.AnimationView);
        view.pause();
        findViewById(R.id.losing_face0).setVisibility(8);
        findViewById(R.id.losing_face1).setVisibility(8);
        findViewById(R.id.losing_face2).setVisibility(8);
        findViewById(R.id.losing_face3).setVisibility(8);
        findViewById(R.id.losing_face4).setVisibility(8);
        findViewById(R.id.losing_face5).setVisibility(8);
        findViewById(R.id.losing_face6).setVisibility(8);
        findViewById(R.id.losing_text).setVisibility(8);
        if (!this.firsttime) {
            view.restartThread();
            this.firsttime = true;
            view.setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
            showMainMenu();
        }
        super.onResume();
    }

    public void onBackPressed() {
        AnimationView view = (AnimationView) findViewById(R.id.AnimationView);
        if (findViewById(R.id.layout_root).getVisibility() == 0 || this.loading) {
            showDialog(GAME_QUIT_DIALOG);
            return;
        }
        view.pause();
        showDialog(ABANDON_GAME_DIALOG);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        AnimationView view = (AnimationView) findViewById(R.id.AnimationView);
        if (findViewById(R.id.layout_root).getVisibility() != 0) {
            view.pause();
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public void onOptionsMenuClosed(Menu item) {
        AnimationView view = (AnimationView) findViewById(R.id.AnimationView);
        if (findViewById(R.id.layout_root).getVisibility() != 0) {
            view.pause();
        }
        super.onOptionsMenuClosed(item);
    }

    public void initNewGame(int type) {
        findViewById(R.id.layout_root).setVisibility(8);
        View a = findViewById(R.id.ad);
        a.setVisibility(GAME_LOST_DIALOG);
        a.setClickable(false);
        if (firstTime()) {
            showFirstTime();
            return;
        }
        this.over = false;
        AnimationView view = (AnimationView) findViewById(R.id.AnimationView);
        if (this.firsttime) {
            view.newGame(this.firsttime, type);
            this.firsttime = false;
            return;
        }
        view.newGame(this.firsttime, type);
    }

    public void gameOver() {
        if (!this.over) {
            this.over = true;
            showDialog(GAME_LOST_DIALOG);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        new AlertDialog.Builder(this);
        AnimationView animationView = (AnimationView) findViewById(R.id.AnimationView);
        SharedPreferences preferences = getPreferences(0);
        switch (id) {
            case GAME_LOST_DIALOG /*4*/:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                SharedPreferences preferences2 = getPreferences(0);
                builder.setMessage("Game Over!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AnimationView view = (AnimationView) SquarePopActivity.this.findViewById(R.id.AnimationView);
                        view.saveStats(SquarePopActivity.this.getPreferences(0));
                        if (view.getType() == Manager.LEVELS) {
                            SquarePopActivity.this.saveHighLevel(view.getLevel());
                        }
                        SquarePopActivity.this.saveHighScore(view.getScore());
                        SquarePopActivity.this.showMainMenu();
                    }
                });
                return builder.create();
            case GAME_QUIT_DIALOG /*5*/:
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setMessage("  Quit?  ").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AnimationView view = (AnimationView) SquarePopActivity.this.findViewById(R.id.AnimationView);
                        view.saveStats(SquarePopActivity.this.getPreferences(0));
                        if (view.getType() == Manager.LEVELS) {
                            SquarePopActivity.this.saveHighLevel(view.getLevel());
                        }
                        SquarePopActivity.this.saveHighScore(view.getScore());
                        SquarePopActivity.this.finish();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                Dialog dialog = builder2.create();
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        ((AnimationView) SquarePopActivity.this.findViewById(R.id.AnimationView)).pause();
                    }
                });
                return dialog;
            case ABANDON_GAME_DIALOG /*6*/:
                AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
                builder3.setMessage("Abandon Game?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AnimationView view = (AnimationView) SquarePopActivity.this.findViewById(R.id.AnimationView);
                        view.saveStats(SquarePopActivity.this.getPreferences(0));
                        if (view.getType() == Manager.LEVELS) {
                            SquarePopActivity.this.saveHighLevel(view.getLevel());
                        }
                        SquarePopActivity.this.saveHighScore(view.getScore());
                        SquarePopActivity.this.showMainMenu();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SquarePopActivity.this.dismissDialog(SquarePopActivity.ABANDON_GAME_DIALOG);
                        ((AnimationView) SquarePopActivity.this.findViewById(R.id.AnimationView)).pause();
                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        ((AnimationView) SquarePopActivity.this.findViewById(R.id.AnimationView)).pause();
                    }
                });
                return builder3.create();
            case 7:
            case 8:
            case 9:
            default:
                return null;
            case UPDATE_DIALOG /*10*/:
                AlertDialog.Builder builder4 = new AlertDialog.Builder(this);
                builder4.setMessage("Update Available").setPositiveButton("Update?", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent market = new Intent("android.intent.action.VIEW");
                        market.setData(Uri.parse("market://details?id=com.woodlawn.squarepop"));
                        SquarePopActivity.this.startActivity(market);
                    }
                }).setNegativeButton("No Thanks", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SquarePopActivity.this.noUpdates();
                        SquarePopActivity.this.dismissDialog(SquarePopActivity.UPDATE_DIALOG);
                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                    }
                });
                return builder4.create();
        }
    }

    public boolean firstTime() {
        SharedPreferences preferences = getPreferences(0);
        if (preferences.getInt(PREFERENCE_LAST_LEVEL, -1) != -1) {
            return false;
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(PREFERENCE_LAST_LEVEL, 1);
        editor.commit();
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        AnimationView animationView = (AnimationView) findViewById(R.id.AnimationView);
        switch (item.getItemId()) {
            case R.id.request:
                Intent email = new Intent("android.intent.action.SEND");
                email.setType("plain/text");
                email.putExtra("android.intent.extra.EMAIL", new String[]{"feedback@100woodlawn.com"});
                email.putExtra("android.intent.extra.SUBJECT", "Square Pop Request/Feedback");
                startActivity(Intent.createChooser(email, "Request a Feature"));
                return true;
            case R.id.search:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:\"100 Woodlawn\"")));
                finish();
                return true;
            default:
                return false;
        }
    }

    public void noUpdates() {
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.putInt(PREFERENCE_UPDATE, 2);
        edit.commit();
    }

    public void onDestroy() {
        ((AnimationView) findViewById(R.id.AnimationView)).recycleBitmaps();
        super.onDestroy();
    }

    public void handleMessage(Message msg) {
        if (msg.getData().getInt("outcome") == 1) {
            gameOver();
        }
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
        if (this.loading) {
            showMainMenu();
        }
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
    }

    public void onReceiveAd(Ad arg0) {
        if (this.loading) {
            showMainMenu();
        }
    }

    private class UpdateTask extends AsyncTask<String, String, Boolean> {
        private UpdateTask() {
        }

        /* synthetic */ UpdateTask(SquarePopActivity squarePopActivity, UpdateTask updateTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... arg0) {
            PackageInfo info;
            try {
                InputStream stream = new DefaultHttpClient().execute(new HttpGet("http://www.100woodlawn.com/u/sp.upd")).getEntity().getContent();
                String rawVersion = "";
                for (int c = stream.read(); c != -1; c = stream.read()) {
                    rawVersion = String.valueOf(rawVersion) + ((char) c);
                }
                stream.close();
                try {
                    info = SquarePopActivity.this.getPackageManager().getPackageInfo(SquarePopActivity.this.getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    info = null;
                }
                String version = "";
                if (info != null) {
                    version = info.versionName;
                }
                if (!rawVersion.equals(version)) {
                    return true;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            return false;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean results) {
            if (results.booleanValue()) {
                SquarePopActivity.this.showDialog(SquarePopActivity.UPDATE_DIALOG);
            }
        }
    }
}
