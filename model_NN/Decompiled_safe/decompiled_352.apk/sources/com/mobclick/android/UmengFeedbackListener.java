package com.mobclick.android;

public interface UmengFeedbackListener {
    void onFeedbackReturned(int i);
}
