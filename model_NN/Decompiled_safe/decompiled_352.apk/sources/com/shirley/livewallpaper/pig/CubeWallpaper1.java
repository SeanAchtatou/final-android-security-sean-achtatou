package com.shirley.livewallpaper.pig;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.SystemClock;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import java.util.ArrayList;
import java.util.List;

public class CubeWallpaper1 extends WallpaperService {
    public static final String PATTERN_WALLPAPER_PREF_NAME = "my";
    public int height = 0;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public int offx;
    public int width = 0;

    public void onCreate() {
        super.onCreate();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public WallpaperService.Engine onCreateEngine() {
        return new CubeEngine();
    }

    class CubeEngine extends WallpaperService.Engine implements SharedPreferences.OnSharedPreferenceChangeListener {
        Bitmap back1;
        Bitmap back2;
        Bitmap back3;
        Bitmap back4;
        int i = 0;
        private List image;
        private boolean isLive = false;
        private float mCenterX;
        private float mCenterY;
        private final Runnable mDrawCube = new Runnable() {
            public void run() {
                CubeEngine.this.drawFrame();
            }
        };
        private float mOffset;
        private final Paint mPaint = new Paint();
        private SharedPreferences mPreferences;
        private long mStartTime;
        private float mTouchX = -1.0f;
        private float mTouchY = -1.0f;
        private boolean mVisible;

        CubeEngine() {
            super(CubeWallpaper1.this);
            Paint paint = this.mPaint;
            paint.setColor(-3599559);
            paint.setAntiAlias(true);
            paint.setStrokeWidth(2.0f);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setStyle(Paint.Style.STROKE);
            this.mStartTime = SystemClock.elapsedRealtime();
            this.mPreferences = CubeWallpaper1.this.getSharedPreferences(CubeWallpaper1.PATTERN_WALLPAPER_PREF_NAME, 0);
            this.mPreferences.registerOnSharedPreferenceChangeListener(this);
            onSharedPreferenceChanged(this.mPreferences, null);
            this.back1 = BitmapFactory.decodeResource(CubeWallpaper1.this.getResources(), R.drawable.back1);
            this.back2 = BitmapFactory.decodeResource(CubeWallpaper1.this.getResources(), R.drawable.back2);
            this.back3 = BitmapFactory.decodeResource(CubeWallpaper1.this.getResources(), R.drawable.back3);
            this.back4 = BitmapFactory.decodeResource(CubeWallpaper1.this.getResources(), R.drawable.back4);
            this.image = new ArrayList();
            this.image.add(this.back1);
            this.image.add(this.back2);
            this.image.add(this.back3);
            this.image.add(this.back4);
        }

        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);
            setTouchEventsEnabled(true);
        }

        public void onDestroy() {
            super.onDestroy();
            CubeWallpaper1.this.mHandler.removeCallbacks(this.mDrawCube);
        }

        public void onVisibilityChanged(boolean visible) {
            this.mVisible = visible;
            if (visible) {
                drawFrame();
            } else {
                CubeWallpaper1.this.mHandler.removeCallbacks(this.mDrawCube);
            }
        }

        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);
            this.mCenterX = ((float) width) / 2.0f;
            this.mCenterY = ((float) height) / 2.0f;
            drawFrame();
        }

        public void onSurfaceCreated(SurfaceHolder holder) {
            super.onSurfaceCreated(holder);
        }

        public void onSurfaceDestroyed(SurfaceHolder holder) {
            super.onSurfaceDestroyed(holder);
            this.mVisible = false;
            CubeWallpaper1.this.mHandler.removeCallbacks(this.mDrawCube);
        }

        public void onOffsetsChanged(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {
            this.mOffset = xOffset;
            CubeWallpaper1.this.offx = -xPixelOffset;
            super.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep, xPixelOffset, yPixelOffset);
            drawFrame();
        }

        public void onTouchEvent(MotionEvent event) {
            if (event.getAction() == 2) {
                this.mTouchX = event.getX();
                this.mTouchY = event.getY();
            } else {
                this.mTouchX = -1.0f;
                this.mTouchY = -1.0f;
            }
            super.onTouchEvent(event);
        }

        /* access modifiers changed from: package-private */
        public void drawFrame() {
            SurfaceHolder holder = getSurfaceHolder();
            Rect frame = holder.getSurfaceFrame();
            CubeWallpaper1.this.width = frame.width();
            CubeWallpaper1.this.height = frame.height();
            Canvas c = null;
            try {
                c = holder.lockCanvas();
                if (c != null) {
                    drawGif(c, (Bitmap) this.image.get(2));
                }
                drawTouchPoint(c);
                if (this.isLive) {
                    drawGif(c, (Bitmap) this.image.get(this.i));
                    this.i++;
                    if (this.i == 4) {
                        this.i = 0;
                    }
                }
                CubeWallpaper1.this.mHandler.removeCallbacks(this.mDrawCube);
                if (this.mVisible) {
                    CubeWallpaper1.this.mHandler.postDelayed(this.mDrawCube, 3000);
                }
            } finally {
                if (c != null) {
                    holder.unlockCanvasAndPost(c);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void drawGif(Canvas c, Bitmap gif) {
            c.drawBitmap(gif, new Rect(CubeWallpaper1.this.offx, 0, CubeWallpaper1.this.offx + 480, 800), new Rect(0, 0, CubeWallpaper1.this.width, CubeWallpaper1.this.height), (Paint) null);
        }

        /* access modifiers changed from: package-private */
        public void drawCube(Canvas c) {
            c.save();
            c.translate(this.mCenterX, this.mCenterY);
            drawLine(c, -400, -400, -400, 400, -400, -400);
            drawLine(c, 400, -400, -400, 400, 400, -400);
            drawLine(c, 400, 400, -400, -400, 400, -400);
            drawLine(c, -400, 400, -400, -400, -400, -400);
            drawLine(c, -400, -400, 400, 400, -400, 400);
            drawLine(c, 400, -400, 400, 400, 400, 400);
            drawLine(c, 400, 400, 400, -400, 400, 400);
            drawLine(c, -400, 400, 400, -400, -400, 400);
            drawLine(c, -400, -400, 400, -400, -400, -400);
            drawLine(c, 400, -400, 400, 400, -400, -400);
            drawLine(c, 400, 400, 400, 400, 400, -400);
            drawLine(c, -400, 400, 400, -400, 400, -400);
            c.restore();
        }

        /* access modifiers changed from: package-private */
        public void drawLine(Canvas c, int x1, int y1, int z1, int x2, int y2, int z2) {
            float xrot = ((float) (SystemClock.elapsedRealtime() - this.mStartTime)) / 1000.0f;
            float yrot = (0.5f - this.mOffset) * 2.0f;
            float newy1 = (float) ((Math.sin((double) xrot) * ((double) z1)) + (Math.cos((double) xrot) * ((double) y1)));
            float newy2 = (float) ((Math.sin((double) xrot) * ((double) z2)) + (Math.cos((double) xrot) * ((double) y2)));
            float newz1 = (float) ((Math.cos((double) xrot) * ((double) z1)) - (Math.sin((double) xrot) * ((double) y1)));
            float newz2 = (float) ((Math.cos((double) xrot) * ((double) z2)) - (Math.sin((double) xrot) * ((double) y2)));
            float newx1 = (float) ((Math.sin((double) yrot) * ((double) newz1)) + (Math.cos((double) yrot) * ((double) x1)));
            float newx2 = (float) ((Math.sin((double) yrot) * ((double) newz2)) + (Math.cos((double) yrot) * ((double) x2)));
            float newz12 = (float) ((Math.cos((double) yrot) * ((double) newz1)) - (Math.sin((double) yrot) * ((double) x1)));
            float newz22 = (float) ((Math.cos((double) yrot) * ((double) newz2)) - (Math.sin((double) yrot) * ((double) x2)));
            c.drawLine(newx1 / (4.0f - (newz12 / 400.0f)), newy1 / (4.0f - (newz12 / 400.0f)), newx2 / (4.0f - (newz22 / 400.0f)), newy2 / (4.0f - (newz22 / 400.0f)), this.mPaint);
        }

        /* access modifiers changed from: package-private */
        public void drawTouchPoint(Canvas c) {
            if (this.mTouchX >= 0.0f && this.mTouchY >= 0.0f) {
                c.drawCircle(this.mTouchX, this.mTouchY, 30.0f, this.mPaint);
            }
        }

        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            this.isLive = sharedPreferences.getBoolean("livewallpaper_movement", false);
            Log.d("isLive", new StringBuilder(String.valueOf(this.isLive)).toString());
        }
    }
}
