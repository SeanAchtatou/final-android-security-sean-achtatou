package com.shirley.livewallpaper.pig;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import com.jeef.wapsConnection.wapsConnection;
import com.waps.AppConnect;

public class LiveWallpaperSetting extends PreferenceActivity {
    CheckBoxPreference checkBoxPreference;
    public Context context;
    private SharedPreferences.Editor editor;
    int jinbi;
    private SharedPreferences sharedPreferences;
    /* access modifiers changed from: private */
    public wapsConnection waps;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        getPreferenceManager().setSharedPreferencesName(CubeWallpaper1.PATTERN_WALLPAPER_PREF_NAME);
        addPreferencesFromResource(R.layout.livewallpaper_settings);
        this.checkBoxPreference = (CheckBoxPreference) findPreference("livewallpaper_movement");
        wapsConnection.Initialize(this, "62bfbc965c16d513135ca2c85fe09749", "hiapk", 50, true);
        this.waps = new wapsConnection(this, "dangerb_angle");
        this.checkBoxPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                return LiveWallpaperSetting.this.waps.isValid();
            }
        });
        AppConnect.getInstance(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public void onPause() {
        super.onPause();
        this.waps.onPause();
    }

    public void onResume() {
        super.onResume();
        this.waps.onResume();
    }
}
