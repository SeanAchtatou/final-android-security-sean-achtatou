package com.jeef.wapsConnection;

import android.content.Context;
import android.content.SharedPreferences;

public class ShareData {
    private SharedPreferences hander;

    public ShareData(Context context, String name) {
        this.hander = context.getSharedPreferences(name, 0);
    }

    public SharedPreferences.Editor edit() {
        return this.hander.edit();
    }

    public SharedPreferences get() {
        return this.hander;
    }
}
