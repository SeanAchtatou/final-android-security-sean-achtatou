package com.waps;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class r extends WebViewClient {
    String a;
    final /* synthetic */ OffersWebView b;

    private r(OffersWebView offersWebView) {
        this.b = offersWebView;
        this.a = "";
    }

    /* synthetic */ r(OffersWebView offersWebView, q qVar) {
        this(offersWebView);
    }

    public void onPageFinished(WebView webView, String str) {
        this.b.d.setVisibility(8);
        super.onPageFinished(webView, str);
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        this.b.d.setVisibility(0);
        super.onPageStarted(webView, str, bitmap);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        webView.setScrollBarStyle(0);
        webView.loadDataWithBaseURL("", "<html><body bgcolor=\"000000\" align=\"center\"><br/><font color=\"ffffff\">网络链接失败，请检查网络。</font><br/></body></html>", "text/html", "utf-8", "");
        boolean unused = this.b.l = false;
        super.onReceivedError(webView, i, str, str2);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        String substring;
        Intent intent;
        String unused = this.b.k = str;
        if (str.startsWith("load://")) {
            if (str.contains(";http://")) {
                substring = str.substring("load://".length(), str.indexOf(";"));
                this.a = str.substring(str.indexOf(";") + 1);
            } else {
                substring = str.substring("load://".length());
            }
            if (substring != "") {
                try {
                    Intent launchIntentForPackage = this.b.getPackageManager().getLaunchIntentForPackage(substring);
                    try {
                        AppConnect.getInstanceNoConnect(this.b).package_receiver(substring, 1);
                        intent = launchIntentForPackage;
                    } catch (Exception e) {
                        intent = launchIntentForPackage;
                    }
                } catch (Exception e2) {
                    intent = null;
                }
                if (intent != null) {
                    this.b.startActivity(intent);
                    return true;
                }
                if (str.contains("market")) {
                    try {
                        this.b.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + this.a.split("id=")[1])));
                    } catch (Exception e3) {
                        Dialog unused2 = this.b.e = new AlertDialog.Builder(this.b).setTitle("").setMessage("Android market is unavailable at this device. To view this link install market.").setPositiveButton("确定", new s(this)).create();
                        try {
                            this.b.e.show();
                        } catch (Exception e4) {
                            e4.printStackTrace();
                        }
                    }
                } else {
                    webView.loadUrl(this.a);
                }
                return true;
            }
        }
        return false;
    }
}
