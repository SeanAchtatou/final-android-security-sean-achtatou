package com.waps.ads.a;

import com.waps.ads.AdGroupLayout;

class d implements Runnable {
    private b a;

    public d(b bVar) {
        this.a = bVar;
    }

    public void run() {
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.a.c.get();
        if (adGroupLayout != null) {
            adGroupLayout.e = adGroupLayout.j.getCustom(this.a.d.a);
            if (adGroupLayout.e == null) {
                adGroupLayout.rotateThreadedNow();
            } else {
                adGroupLayout.b.post(new c(this.a));
            }
        }
    }
}
