package com.waps.ads.adapters;

import android.graphics.Color;
import android.view.ViewGroup;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.AdGroupTargeting;
import com.waps.ads.a.a;
import com.waps.ads.b.b;
import com.waps.ads.b.c;
import com.waps.ads.f;
import com.wiyun.ad.AdView;

public class WiyunAdapter extends a {
    public WiyunAdapter(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void
     arg types: [com.waps.ads.AdGroupLayout, com.wiyun.ad.AdView]
     candidates:
      com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.View):void
      com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void */
    public void handle() {
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            b bVar = adGroupLayout.d;
            int rgb = Color.rgb(bVar.e, bVar.f, bVar.g);
            int rgb2 = Color.rgb(bVar.a, bVar.b, bVar.c);
            AdView adView = new AdView(adGroupLayout.getContext());
            adView.setBackgroundColor(rgb);
            adView.setTextColor(rgb2);
            adView.setResId(this.d.e);
            adView.setTestMode(AdGroupTargeting.getTestMode());
            adView.requestAd();
            adGroupLayout.j.resetRollover();
            adGroupLayout.b.post(new f(adGroupLayout, (ViewGroup) adView));
            adGroupLayout.rotateThreadedDelayed();
        }
    }
}
