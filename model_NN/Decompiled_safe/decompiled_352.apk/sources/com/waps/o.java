package com.waps;

import android.view.View;

class o implements View.OnClickListener {
    final /* synthetic */ DisplayAd a;

    o(DisplayAd displayAd) {
        this.a = displayAd;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onClick(android.view.View r8) {
        /*
            r7 = this;
            r5 = 1
            java.lang.String r0 = ""
            java.lang.String r0 = ""
            com.waps.DisplayAd r1 = r7.a
            android.content.Context r1 = r1.g
            android.content.pm.PackageManager r1 = r1.getPackageManager()
            com.waps.DisplayAd r2 = r7.a     // Catch:{ NameNotFoundException -> 0x00b1 }
            android.content.Context r2 = r2.g     // Catch:{ NameNotFoundException -> 0x00b1 }
            java.lang.String r2 = r2.getPackageName()     // Catch:{ NameNotFoundException -> 0x00b1 }
            r3 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r1 = r1.getApplicationInfo(r2, r3)     // Catch:{ NameNotFoundException -> 0x00b1 }
            com.waps.DisplayAd r2 = r7.a     // Catch:{ NameNotFoundException -> 0x00b1 }
            android.content.Context r2 = r2.g     // Catch:{ NameNotFoundException -> 0x00b1 }
            java.lang.String r0 = r2.getPackageName()     // Catch:{ NameNotFoundException -> 0x00b1 }
            android.os.Bundle r1 = r1.metaData     // Catch:{ NameNotFoundException -> 0x00b1 }
            java.lang.String r2 = "CLIENT_PACKAGE"
            java.lang.String r1 = r1.getString(r2)     // Catch:{ NameNotFoundException -> 0x00b1 }
            if (r1 == 0) goto L_0x003c
            java.lang.String r2 = ""
            boolean r2 = r1.equals(r2)     // Catch:{ NameNotFoundException -> 0x00b1 }
            if (r2 != 0) goto L_0x003c
            r0 = r1
        L_0x003c:
            java.lang.String r1 = com.waps.DisplayAd.l
            if (r1 == 0) goto L_0x00f3
            java.lang.String r1 = ""
            java.lang.String r2 = com.waps.DisplayAd.l
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x00f3
            com.waps.DisplayAd r1 = r7.a
            android.content.Context r1 = r1.g
            android.content.pm.PackageManager r1 = r1.getPackageManager()
            r2 = 0
            java.lang.String r3 = com.waps.DisplayAd.l     // Catch:{ Exception -> 0x00ba }
            android.content.Intent r1 = r1.getLaunchIntentForPackage(r3)     // Catch:{ Exception -> 0x00ba }
            com.waps.DisplayAd r2 = r7.a     // Catch:{ Exception -> 0x00ee }
            android.content.Context r2 = r2.g     // Catch:{ Exception -> 0x00ee }
            com.waps.AppConnect r2 = com.waps.AppConnect.getInstanceNoConnect(r2)     // Catch:{ Exception -> 0x00ee }
            java.lang.String r3 = com.waps.DisplayAd.l     // Catch:{ Exception -> 0x00ee }
            r4 = 2
            r2.package_receiver(r3, r4)     // Catch:{ Exception -> 0x00ee }
        L_0x0073:
            if (r1 == 0) goto L_0x00c0
            com.waps.DisplayAd r2 = r7.a
            android.content.Context r2 = r2.g
            r2.startActivity(r1)
            r1 = 0
        L_0x007f:
            if (r1 == 0) goto L_0x00b0
            java.lang.String r1 = com.waps.DisplayAd.m
            if (r1 == 0) goto L_0x00c2
            java.lang.String r1 = ""
            java.lang.String r2 = com.waps.DisplayAd.m
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x00c2
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "android.intent.action.VIEW"
            java.lang.String r2 = com.waps.DisplayAd.k
            android.net.Uri r2 = android.net.Uri.parse(r2)
            r0.<init>(r1, r2)
            r1 = 268435456(0x10000000, float:2.5243549E-29)
            r0.setFlags(r1)
            com.waps.DisplayAd r1 = r7.a
            android.content.Context r1 = r1.g
            r1.startActivity(r0)
        L_0x00b0:
            return
        L_0x00b1:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            r0.printStackTrace()
            r0 = r1
            goto L_0x003c
        L_0x00ba:
            r1 = move-exception
        L_0x00bb:
            r1.printStackTrace()
            r1 = r2
            goto L_0x0073
        L_0x00c0:
            r1 = r5
            goto L_0x007f
        L_0x00c2:
            android.content.Intent r1 = new android.content.Intent
            com.waps.DisplayAd r2 = r7.a
            android.content.Context r2 = r2.g
            java.lang.Class<com.waps.OffersWebView> r3 = com.waps.OffersWebView.class
            r1.<init>(r2, r3)
            java.lang.String r2 = "URL"
            java.lang.String r3 = com.waps.DisplayAd.k
            r1.putExtra(r2, r3)
            java.lang.String r2 = "isFinshClose"
            java.lang.String r3 = "true"
            r1.putExtra(r2, r3)
            java.lang.String r2 = "CLIENT_PACKAGE"
            r1.putExtra(r2, r0)
            com.waps.DisplayAd r0 = r7.a
            android.content.Context r0 = r0.g
            r0.startActivity(r1)
            goto L_0x00b0
        L_0x00ee:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x00bb
        L_0x00f3:
            r1 = r5
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.waps.o.onClick(android.view.View):void");
    }
}
