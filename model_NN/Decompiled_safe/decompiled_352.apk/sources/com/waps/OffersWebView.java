package com.waps;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class OffersWebView extends Activity {
    k a;
    /* access modifiers changed from: private */
    public WebView b = null;
    private String c = null;
    /* access modifiers changed from: private */
    public ProgressBar d;
    /* access modifiers changed from: private */
    public Dialog e = null;
    private String f = "";
    private String g = "";
    private String h = "";
    private String i = "";
    /* access modifiers changed from: private */
    public String j = "false";
    /* access modifiers changed from: private */
    public String k;
    /* access modifiers changed from: private */
    public boolean l = true;

    private void initMetaData(Bundle bundle) {
        if (bundle != null) {
            if (bundle.getString("Offers_URL") != null) {
                this.g = bundle.getString("Offers_URL");
            }
            if (bundle.getString("URL") != null) {
                this.g = bundle.getString("URL");
            }
            this.h = bundle.getString("URL_PARAMS");
            this.f = bundle.getString("CLIENT_PACKAGE");
            this.i = bundle.getString("USER_ID");
            this.j = bundle.getString("isFinshClose");
            this.h += "&publisher_user_id=" + this.i;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        initMetaData(getIntent().getExtras());
        if (this.g.indexOf("?") > -1) {
            this.c = this.g + this.h;
        } else {
            this.c = this.g + "?a=1" + this.h;
        }
        this.c = this.c.replaceAll(" ", "%20");
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView(getResources().getIdentifier("offers_web_view", "layout", this.f));
        this.b = (WebView) findViewById(getResources().getIdentifier("offersWebView", "id", this.f));
        this.b.setWebViewClient(new r(this, null));
        this.b.getSettings().setJavaScriptEnabled(true);
        WebView webView = this.b;
        WebView.enablePlatformNotifications();
        this.b.setHttpAuthUsernamePassword("10.0.0.172", "", "", "");
        this.d = (ProgressBar) findViewById(getResources().getIdentifier("OfferProgressBar", "id", this.f));
        this.d.setVisibility(0);
        if (this.j != null && "true".equals(this.j)) {
            Toast.makeText(this, "加载中,请稍候...", 0).show();
        }
        this.b.loadUrl(this.c);
        this.b.setDownloadListener(new q(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.b.destroy();
        finish();
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || !this.b.canGoBack()) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (!this.l) {
            finish();
            this.l = true;
        }
        this.b.goBack();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (!(this.c == null || this.b == null)) {
            this.b.loadUrl(this.c);
        }
        super.onResume();
    }
}
