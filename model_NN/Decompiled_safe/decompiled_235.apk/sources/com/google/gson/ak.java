package com.google.gson;

import java.util.Collection;
import java.util.HashSet;

final class ak implements ExclusionStrategy {
    private final Collection<Integer> a = new HashSet();

    public ak(int... iArr) {
        if (iArr != null) {
            for (int valueOf : iArr) {
                this.a.add(Integer.valueOf(valueOf));
            }
        }
    }

    public final boolean shouldSkipClass(Class<?> cls) {
        return false;
    }

    public final boolean shouldSkipField(FieldAttributes f) {
        for (Integer intValue : this.a) {
            if (f.hasModifier(intValue.intValue())) {
                return true;
            }
        }
        return false;
    }
}
