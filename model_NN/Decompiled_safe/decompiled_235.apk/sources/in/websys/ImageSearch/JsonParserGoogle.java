package in.websys.ImageSearch;

import android.util.Log;
import android.util.Xml;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;

public class JsonParserGoogle extends BaseFeedParser {
    public JsonParserGoogle(String feedUrl) {
        super(feedUrl);
    }

    public List<Message> parse() {
        JSONException e;
        IOException e2;
        ClientProtocolException e3;
        List<Message> messages = null;
        XmlPullParser newPullParser = Xml.newPullParser();
        Message currentMessage = null;
        try {
            HttpResponse res = new DefaultHttpClient().execute(new HttpGet(getFeedUrl().toString()));
            if (res.getStatusLine().getStatusCode() != 200) {
                return null;
            }
            JSONObject jsonObj = new JSONObject(EntityUtils.toString(res.getEntity()));
            String responseStatus = jsonObj.getString("responseStatus");
            Log.v("ImageSearch", "responseStatus :" + responseStatus);
            if (!responseStatus.equals("200")) {
                return null;
            }
            JSONArray jsonObjResultArray = jsonObj.getJSONObject("responseData").getJSONArray("results");
            List<Message> messages2 = new ArrayList<>();
            int i = 0;
            while (i < jsonObjResultArray.length()) {
                try {
                    JSONObject jsonObjResult = jsonObjResultArray.getJSONObject(i);
                    currentMessage = new Message();
                    try {
                        currentMessage.setImage(jsonObjResult.getString("unescapedUrl"));
                        currentMessage.setThumbnail(jsonObjResult.getString("tbUrl"));
                        currentMessage.setLink(jsonObjResult.getString("originalContextUrl"));
                        messages2.add(currentMessage);
                        i++;
                    } catch (ClientProtocolException e4) {
                        e3 = e4;
                        messages = messages2;
                        e3.printStackTrace();
                        return messages;
                    } catch (IOException e5) {
                        e2 = e5;
                        messages = messages2;
                        e2.printStackTrace();
                        return messages;
                    } catch (JSONException e6) {
                        e = e6;
                        messages = messages2;
                        e.printStackTrace();
                        return messages;
                    }
                } catch (ClientProtocolException e7) {
                    e3 = e7;
                    messages = messages2;
                    e3.printStackTrace();
                    return messages;
                } catch (IOException e8) {
                    e2 = e8;
                    messages = messages2;
                    e2.printStackTrace();
                    return messages;
                } catch (JSONException e9) {
                    e = e9;
                    messages = messages2;
                    e.printStackTrace();
                    return messages;
                }
            }
            messages = messages2;
            return messages;
        } catch (ClientProtocolException e10) {
            e3 = e10;
            e3.printStackTrace();
            return messages;
        } catch (IOException e11) {
            e2 = e11;
            e2.printStackTrace();
            return messages;
        } catch (JSONException e12) {
            e = e12;
            e.printStackTrace();
            return messages;
        }
    }
}
