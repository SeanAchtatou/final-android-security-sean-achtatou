package in.websys.ImageSearch;

import android.util.Log;
import android.util.Xml;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

public class XmlPullFeedParserFlickr extends BaseFeedParser {
    static final String CHANNEL = "channel";
    static final String DESCRIPTION = "description";
    static final String IMAGE = "content";
    static final String ITEM = "item";
    static final String LINK = "link";
    static final String PHOTO = "photo";
    static final String PHOTOSET = "photoset";
    static final String PHOTO_PARENT = "photo";
    static final String TAG = "tag";
    static final String THUMBNAIL = "thumbnail";
    static final String TITLE = "photo-caption";

    public XmlPullFeedParserFlickr(String feedUrl) {
        super(feedUrl);
    }

    public List<Message> parse() {
        Exception e;
        List<Message> list;
        List<Message> messages = null;
        XmlPullParser parser = Xml.newPullParser();
        try {
            parser.setInput(getInputStream(), null);
            int eventType = parser.getEventType();
            while (true) {
                list = messages;
                if (eventType == 1 || 0 != 0) {
                    return list;
                }
                switch (eventType) {
                    case R.styleable.com_google_ads_AdView_adSize:
                        try {
                            messages = new ArrayList<>();
                            break;
                        } catch (Exception e2) {
                            e = e2;
                            Log.e("ImageSearch::PullFeedParser", e.getMessage(), e);
                            throw new RuntimeException(e);
                        }
                    case 2:
                        if (parser.getName().equalsIgnoreCase("photo")) {
                            Message currentMessage = new Message();
                            String strOwner = parser.getAttributeValue(null, "owner");
                            String strFarm = parser.getAttributeValue(null, "farm");
                            String strServer = parser.getAttributeValue(null, "server");
                            String strId = parser.getAttributeValue(null, "id");
                            String strSecret = parser.getAttributeValue(null, "secret");
                            currentMessage.setImage("http://farm" + strFarm + ".static.flickr.com/" + strServer + "/" + strId + "_" + strSecret + ".jpg");
                            currentMessage.setThumbnail("http://farm" + strFarm + ".static.flickr.com/" + strServer + "/" + strId + "_" + strSecret + "_s.jpg");
                            currentMessage.setLink("http://www.flickr.com/photos/" + strOwner + "/" + strId + "/");
                            list.add(currentMessage);
                            messages = list;
                            break;
                        }
                    case R.styleable.com_google_ads_AdView_adUnitId:
                    default:
                        messages = list;
                        break;
                }
                eventType = parser.next();
            }
            return list;
        } catch (Exception e3) {
            e = e3;
            Log.e("ImageSearch::PullFeedParser", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
}
