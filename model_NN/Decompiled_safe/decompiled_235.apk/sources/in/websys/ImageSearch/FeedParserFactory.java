package in.websys.ImageSearch;

import java.net.URLEncoder;

public abstract class FeedParserFactory {
    private static /* synthetic */ int[] $SWITCH_TABLE$in$websys$ImageSearch$FromType;
    static String encoding = "UTF-8";

    static /* synthetic */ int[] $SWITCH_TABLE$in$websys$ImageSearch$FromType() {
        int[] iArr = $SWITCH_TABLE$in$websys$ImageSearch$FromType;
        if (iArr == null) {
            iArr = new int[FromType.values().length];
            try {
                iArr[FromType.FLICKR.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[FromType.GOOGLE.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[FromType.HATENA_KEYWORD.ordinal()] = 5;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[FromType.HATENA_TAG.ordinal()] = 6;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[FromType.PHOTOZOU.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[FromType.PICASA.ordinal()] = 2;
            } catch (NoSuchFieldError e6) {
            }
            $SWITCH_TABLE$in$websys$ImageSearch$FromType = iArr;
        }
        return iArr;
    }

    public static FeedParser getParser(String keyword, long page, int num) {
        switch ($SWITCH_TABLE$in$websys$ImageSearch$FromType()[MyMessage.getInstance().getFrom().ordinal()]) {
            case R.styleable.com_google_ads_AdView_adUnitId /*1*/:
                String url = String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf("http://api.flickr.com/services/rest/?") + "api_key=df1408826504884233a6518effc71be8") + "&method=flickr.photos.search") + "&format=rest") + "&per_page=20") + "&page=" + String.valueOf(page)) + "&sort=interestingness-desc";
                try {
                    url = String.valueOf(url) + "&text=" + URLEncoder.encode(keyword, encoding);
                } catch (Exception e) {
                }
                return new XmlPullFeedParserFlickr(url);
            case 2:
                String url2 = String.valueOf("http://picasaweb.google.com/data/feed/base/all?alt=rss&kind=photo&max-results=20") + "&start-index=" + String.valueOf(((page - 1) * ((long) num)) + 1);
                try {
                    url2 = String.valueOf(url2) + "&q=" + URLEncoder.encode(keyword, encoding);
                } catch (Exception e2) {
                }
                return new XmlPullFeedParserPicasa(url2);
            case 3:
                String url3 = String.valueOf(String.valueOf(String.valueOf("http://api.photozou.jp/rest/search_public?type=photo") + "&limit=" + String.valueOf((((page - 1) * 20) + 20) - 1)) + "&offset=" + String.valueOf((page - 1) * 20)) + "&order_type=favorite";
                try {
                    url3 = String.valueOf(url3) + "&keyword=" + URLEncoder.encode(keyword, encoding);
                } catch (Exception e3) {
                }
                return new XmlPullFeedParserPhotozou(url3);
            case 4:
                String url4 = String.valueOf("http://ajax.googleapis.com/ajax/services/search/images?rsz=large&v=1.0") + "&start=" + String.valueOf((page - 1) * ((long) num));
                if (!MyMessage.getInstance().getSafeSearch()) {
                    url4 = String.valueOf(url4) + "&safe=off";
                }
                try {
                    url4 = String.valueOf(url4) + "&q=" + URLEncoder.encode(keyword, encoding);
                } catch (Exception e4) {
                }
                return new JsonParserGoogle(url4);
            case 5:
                String url5 = "http://f.hatena.ne.jp/keyword/";
                try {
                    url5 = String.valueOf(url5) + URLEncoder.encode(keyword, encoding);
                } catch (Exception e5) {
                }
                return new XmlPullFeedParserHatena(String.valueOf(String.valueOf(url5) + "?mode=rss") + "&page=" + String.valueOf(page));
            case 6:
                String url6 = "http://f.hatena.ne.jp/t/";
                try {
                    url6 = String.valueOf(url6) + URLEncoder.encode(keyword, encoding);
                } catch (Exception e6) {
                }
                return new XmlPullFeedParserHatena(String.valueOf(String.valueOf(url6) + "?mode=rss") + "&page=" + String.valueOf(page));
            default:
                return null;
        }
    }
}
