package in.websys.ImageSearch;

public final class R {

    public static final class anim {
        public static final int fade_in = 2130968576;
        public static final int fade_out = 2130968577;
        public static final int move_in_left = 2130968578;
        public static final int move_in_right = 2130968579;
        public static final int move_out_left = 2130968580;
        public static final int move_out_right = 2130968581;
        public static final int set_position = 2130968582;
    }

    public static final class array {
        public static final int numEntries = 2131034112;
        public static final int numEntryValues = 2131034113;
    }

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class drawable {
        public static final int bak_notfound = 2130837504;
        public static final int blank = 2130837505;
        public static final int ic_menu_allfriends = 2130837506;
        public static final int ic_menu_friendslist = 2130837507;
        public static final int ic_menu_refresh = 2130837508;
        public static final int ic_menu_start_conversation = 2130837509;
        public static final int icon = 2130837510;
        public static final int notfound = 2130837511;
        public static final int x = 2130837512;
    }

    public static final class id {
        public static final int BANNER = 2131099648;
        public static final int ButtonRefresh = 2131099662;
        public static final int EditText01 = 2131099656;
        public static final int GridImageThumb = 2131099680;
        public static final int GridWaitBar = 2131099679;
        public static final int GridviewWait = 2131099678;
        public static final int IAB_BANNER = 2131099650;
        public static final int IAB_LEADERBOARD = 2131099651;
        public static final int IAB_MRECT = 2131099649;
        public static final int IdMain = 2131099660;
        public static final int IdPage = 2131099661;
        public static final int ImageButton01 = 2131099657;
        public static final int ImageGrid = 2131099665;
        public static final int KeywordText = 2131099683;
        public static final int LayoutId = 2131099664;
        public static final int LayoutId2 = 2131099666;
        public static final int ListKeyword = 2131099658;
        public static final int SlidePage = 2131099668;
        public static final int TextView01 = 2131099684;
        public static final int ad = 2131099659;
        public static final int btnVoice = 2131099655;
        public static final int dialog_edittext = 2131099682;
        public static final int dialog_textview = 2131099681;
        public static final int flipper = 2131099669;
        public static final int flipperId = 2131099663;
        public static final int image_image_img = 2131099677;
        public static final int linearLayout1 = 2131099654;
        public static final int list_id = 2131099676;
        public static final int seekbar = 2131099685;
        public static final int slideshow_frm = 2131099667;
        public static final int slideshow_frm1 = 2131099670;
        public static final int slideshow_frm2 = 2131099673;
        public static final int slideshow_img1 = 2131099672;
        public static final int slideshow_img2 = 2131099675;
        public static final int slideshow_progress1 = 2131099671;
        public static final int slideshow_progress2 = 2131099674;
        public static final int spnFrom = 2131099653;
        public static final int textView1 = 2131099652;
    }

    public static final class layout {
        public static final int act_condition = 2130903040;
        public static final int act_gridview = 2130903041;
        public static final int act_slideshow = 2130903042;
        public static final int frame_line = 2130903043;
        public static final int id_row = 2130903044;
        public static final int image = 2130903045;
        public static final int image_grid_rowdata = 2130903046;
        public static final int input_dialog = 2130903047;
        public static final int keyword_row = 2130903048;
        public static final int row = 2130903049;
        public static final int setting = 2130903050;
    }

    public static final class string {
        public static final int app_name = 2131165184;
        public static final int back = 2131165200;
        public static final int cannot_get_image = 2131165212;
        public static final int delete = 2131165193;
        public static final int delete2 = 2131165199;
        public static final int error = 2131165201;
        public static final int error_load_data = 2131165202;
        public static final int error_no_Image = 2131165203;
        public static final int from_flickr = 2131165186;
        public static final int from_google = 2131165190;
        public static final int from_hatena = 2131165188;
        public static final int from_hatena_keyword = 2131165191;
        public static final int from_hatena_tag = 2131165192;
        public static final int from_photozou = 2131165189;
        public static final int from_picasa = 2131165187;
        public static final int input_text = 2131165194;
        public static final int keyword = 2131165196;
        public static final int loading = 2131165185;
        public static final int m_image = 2131165210;
        public static final int mgs_delete_all = 2131165204;
        public static final int no = 2131165206;
        public static final int no_keyword = 2131165195;
        public static final int open_page = 2131165209;
        public static final int refresh = 2131165198;
        public static final int safesearch_preferences = 2131165216;
        public static final int seekbar_summary = 2131165224;
        public static final int seekbar_title = 2131165223;
        public static final int setting = 2131165213;
        public static final int setting_safesearch_off = 2131165219;
        public static final int setting_safesearch_on = 2131165218;
        public static final int setting_safesearch_title = 2131165217;
        public static final int setting_second_description = 2131165222;
        public static final int setting_second_title = 2131165221;
        public static final int settings_label = 2131165214;
        public static final int settings_title = 2131165215;
        public static final int slideshow = 2131165197;
        public static final int slideshow_preferences = 2131165220;
        public static final int start = 2131165207;
        public static final int stop = 2131165208;
        public static final int wallpaper = 2131165211;
        public static final int yes = 2131165205;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }
}
