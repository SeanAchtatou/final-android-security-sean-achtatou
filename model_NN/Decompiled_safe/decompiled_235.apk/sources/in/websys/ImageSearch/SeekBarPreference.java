package in.websys.ImageSearch;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

public class SeekBarPreference extends DialogPreference implements SeekBar.OnSeekBarChangeListener {
    private static final int LAYOUT_PADDING = 10;
    private static final int MIN_SEC = 1;
    private static final int OPT_SEEKBAR_DEF = 3;
    private static final String OPT_SEEKBAR_KEY = "CHANGE_SEC";
    private static SharedPreferences sp;
    private SeekBar bar;
    private boolean blnInit = false;
    private Context mContext;
    private TextView text;

    public SeekBarPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        sp = PreferenceManager.getDefaultSharedPreferences(this.mContext);
    }

    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (this.text != null && this.bar != null && this.blnInit) {
            String sec = " " + String.valueOf(this.bar.getProgress() + 1);
            this.text.setText(this.mContext.getString(R.string.seekbar_summary).replace("###", sec.substring(sec.length() - 2, sec.length())));
        }
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    /* access modifiers changed from: protected */
    public View onCreateDialogView() {
        LinearLayout layout = new LinearLayout(this.mContext);
        layout.setPadding(LAYOUT_PADDING, LAYOUT_PADDING, LAYOUT_PADDING, LAYOUT_PADDING);
        layout.setOrientation(1);
        this.text = new TextView(this.mContext);
        layout.addView(this.text, new LinearLayout.LayoutParams(-2, -2));
        this.bar = new SeekBar(this.mContext);
        this.bar.setOnSeekBarChangeListener(this);
        this.bar.setMax(59);
        this.bar.setProgress(getValue());
        layout.addView(this.bar, new LinearLayout.LayoutParams(-1, -2));
        String sec = " " + String.valueOf(this.bar.getProgress() + 1);
        this.text.setText(this.mContext.getString(R.string.seekbar_summary).replace("###", sec.substring(sec.length() - 2, sec.length())));
        this.blnInit = true;
        return layout;
    }

    /* access modifiers changed from: protected */
    public void onBindDialogView(View v) {
        super.onBindDialogView(v);
    }

    /* access modifiers changed from: protected */
    public void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            setValue(this.bar.getProgress() + 1);
        }
    }

    private void setValue(int value) {
        SharedPreferences.Editor ed = sp.edit();
        ed.putInt(OPT_SEEKBAR_KEY, value);
        ed.commit();
    }

    private int getValue() {
        return sp.getInt(OPT_SEEKBAR_KEY, OPT_SEEKBAR_DEF) - 1;
    }
}
