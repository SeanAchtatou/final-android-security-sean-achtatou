package in.websys.ImageSearch;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityCondition extends Activity {
    private static /* synthetic */ int[] $SWITCH_TABLE$in$websys$ImageSearch$ActivityCondition$ConMenuKeyword;
    private static /* synthetic */ int[] $SWITCH_TABLE$in$websys$ImageSearch$ActivityCondition$MenuType2;
    static String encoding = "UTF-8";
    private final int CUSTOM_DIALOG = 1;
    private final int GRIDVIEW_ACTIVITY = 2;
    private final int VOICE_DIALOG = 0;
    private DatabaseHelper helper;
    /* access modifiers changed from: private */
    public Context mContext;
    private String mDelId;
    /* access modifiers changed from: private */
    public EditText mEditText01;
    /* access modifiers changed from: private */
    public ImageButton mImageButton01;
    private List<String> mListId = new ArrayList(0);
    private ListView mListView;
    private ImageButton mbtnVoice;
    private Spinner mspnFrom;

    private enum ConMenuKeyword {
        DELETE
    }

    private enum MenuType2 {
        DELETE,
        SETTING
    }

    static /* synthetic */ int[] $SWITCH_TABLE$in$websys$ImageSearch$ActivityCondition$ConMenuKeyword() {
        int[] iArr = $SWITCH_TABLE$in$websys$ImageSearch$ActivityCondition$ConMenuKeyword;
        if (iArr == null) {
            iArr = new int[ConMenuKeyword.values().length];
            try {
                iArr[ConMenuKeyword.DELETE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            $SWITCH_TABLE$in$websys$ImageSearch$ActivityCondition$ConMenuKeyword = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$in$websys$ImageSearch$ActivityCondition$MenuType2() {
        int[] iArr = $SWITCH_TABLE$in$websys$ImageSearch$ActivityCondition$MenuType2;
        if (iArr == null) {
            iArr = new int[MenuType2.values().length];
            try {
                iArr[MenuType2.DELETE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[MenuType2.SETTING.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$in$websys$ImageSearch$ActivityCondition$MenuType2 = iArr;
        }
        return iArr;
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        this.mContext = this;
        getWindow().setSoftInputMode(3);
        setContentView((int) R.layout.act_condition);
        this.mspnFrom = (Spinner) findViewById(R.id.spnFrom);
        this.mEditText01 = (EditText) findViewById(R.id.EditText01);
        this.mbtnVoice = (ImageButton) findViewById(R.id.btnVoice);
        this.mImageButton01 = (ImageButton) findViewById(R.id.ImageButton01);
        this.mListView = (ListView) findViewById(R.id.ListKeyword);
        dispId();
        this.mspnFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                MyMessage.getInstance().setFrom(FromType.values()[position]);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        setSpinnerFrom();
        this.mEditText01.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 0 || keyCode != 66) {
                    return false;
                }
                ActivityCondition.this.mImageButton01.performClick();
                return true;
            }
        });
        this.mbtnVoice.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Intent intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
                    intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
                    ActivityCondition.this.startActivityForResult(intent, 0);
                } catch (ActivityNotFoundException e) {
                }
            }
        });
        this.mImageButton01.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ((InputMethodManager) ActivityCondition.this.getSystemService("input_method")).hideSoftInputFromWindow(ActivityCondition.this.mEditText01.getWindowToken(), 0);
                String keyword = ActivityCondition.this.mEditText01.getText().toString().trim();
                if (keyword.equals("")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(ActivityCondition.this.mContext);
                    alert.setTitle((int) R.string.input_text);
                    alert.setMessage((int) R.string.no_keyword);
                    alert.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
                    alert.show();
                    return;
                }
                MyMessage.getInstance().setKeyword(keyword);
                MyMessage.getInstance().setPage(1);
                ActivityCondition.this.showImageSearchActivity();
            }
        });
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                ActivityCondition.this.mEditText01.setText(((TextView) view.findViewWithTag("text")).getText().toString());
                ActivityCondition.this.mImageButton01.performClick();
            }
        });
        registerForContextMenu(this.mListView);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == -1) {
            this.mEditText01.setText(data.getStringArrayListExtra("android.speech.extra.RESULTS").get(0));
        }
        if (requestCode == 2) {
            dispId();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, MenuType2.DELETE.ordinal(), MenuType2.DELETE.ordinal(), (int) R.string.delete).setIcon(17301564);
        menu.add(0, MenuType2.SETTING.ordinal(), MenuType2.SETTING.ordinal(), (int) R.string.setting).setIcon(17301577);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch ($SWITCH_TABLE$in$websys$ImageSearch$ActivityCondition$MenuType2()[MenuType2.values()[item.getItemId()].ordinal()]) {
            case R.styleable.com_google_ads_AdView_adUnitId /*1*/:
                new AlertDialog.Builder(this).setIcon(17301564).setTitle((int) R.string.delete).setMessage((int) R.string.mgs_delete_all).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ActivityCondition.this.deleteId(null);
                        ActivityCondition.this.dispId();
                    }
                }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).show();
                return true;
            case 2:
                startActivity(new Intent(this, Settings.class));
                return true;
            default:
                return true;
        }
    }

    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo info) {
        super.onCreateContextMenu(menu, view, info);
        this.mDelId = this.mListId.get(((AdapterView.AdapterContextMenuInfo) info).position);
        menu.setHeaderTitle(this.mDelId);
        menu.add(0, ConMenuKeyword.DELETE.ordinal(), ConMenuKeyword.DELETE.ordinal(), (int) R.string.delete2);
    }

    public boolean onContextItemSelected(MenuItem item) {
        super.onContextItemSelected(item);
        switch ($SWITCH_TABLE$in$websys$ImageSearch$ActivityCondition$ConMenuKeyword()[ConMenuKeyword.values()[item.getItemId()].ordinal()]) {
            case R.styleable.com_google_ads_AdView_adUnitId /*1*/:
                deleteId(this.mDelId);
                dispId();
                return true;
            default:
                return true;
        }
    }

    private void setSpinnerFrom() {
        try {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, 17367048);
            adapter.add(getString(R.string.from_flickr));
            adapter.add(getString(R.string.from_picasa));
            adapter.add(getString(R.string.from_photozou));
            adapter.add(getString(R.string.from_google));
            adapter.add(getString(R.string.from_hatena_keyword));
            adapter.add(getString(R.string.from_hatena_tag));
            adapter.setDropDownViewResource(17367049);
            this.mspnFrom.setAdapter((SpinnerAdapter) adapter);
        } catch (Throwable th) {
            finish();
        }
    }

    public void dispId() {
        String[] from_template = {"url"};
        int[] to_template = {R.id.list_id};
        ListView list_group = (ListView) findViewById(R.id.ListKeyword);
        list_group.setAdapter((ListAdapter) null);
        ArrayList<Map<String, Object>> data = new ArrayList<>();
        this.helper = new DatabaseHelper(this);
        Cursor c = this.helper.getReadableDatabase().rawQuery("select keyword from keyword_cache order by crtdate desc ;", null);
        c.moveToFirst();
        int cnt = c.getCount();
        this.mListId.clear();
        for (int i = 0; i < cnt; i++) {
            this.mListId.add(c.getString(0));
            Map<String, Object> map = new HashMap<>();
            map.put("url", c.getString(0));
            data.add(map);
            c.moveToNext();
        }
        c.close();
        ListTemplateAdapter adapter = new ListTemplateAdapter(this, data, R.layout.act_gridview, from_template, to_template);
        if (data != null && data.size() > 0) {
            adapter.setListData(data);
            list_group.setFocusable(false);
            list_group.setAdapter((ListAdapter) adapter);
            list_group.setScrollingCacheEnabled(false);
            list_group.setTextFilterEnabled(true);
        }
    }

    public void showImageSearchActivity() {
        Intent intent = new Intent(this, ActivityGridView.class);
        CharSequence text = MyMessage.getInstance().getKeyword();
        insKeyword(text.toString());
        intent.putExtra("MAIN_FORM_TEXT", text);
        startActivityForResult(intent, 2);
    }

    public void insKeyword(String keyword) {
        SQLiteDatabase db = new DatabaseHelper(this).getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete("keyword_cache", "keyword = ?", new String[]{keyword});
            SQLiteStatement stmt = db.compileStatement("insert into keyword_cache values (?, datetime('now', 'localtime'));");
            stmt.bindString(1, keyword);
            stmt.executeInsert();
            Cursor c = db.rawQuery("select keyword from keyword_cache order by crtdate desc limit 1 offset 50;", null);
            c.moveToFirst();
            int cnt = c.getCount();
            for (int j = 0; j < cnt; j++) {
                db.delete("keyword_cache", "keyword = ?", new String[]{c.getString(0)});
                c.moveToNext();
            }
            c.close();
            db.setTransactionSuccessful();
        } finally {
            if (db != null) {
                db.endTransaction();
                db.close();
            }
        }
    }

    public void deleteId(String id) {
        SQLiteDatabase db = this.helper.getWritableDatabase();
        db.beginTransaction();
        if (id == null) {
            try {
                int delete = db.delete("keyword_cache", null, null);
            } catch (Throwable th) {
                db.endTransaction();
                throw th;
            }
        } else {
            int delete2 = db.delete("keyword_cache", "keyword = ?", new String[]{id});
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    private class ListTemplateAdapter extends SimpleAdapter {
        private Context context;
        private LayoutInflater inflater;
        private ArrayList<Map<String, Object>> items;

        public ListTemplateAdapter(Context context2, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context2, data, resource, from, to);
            this.context = context2;
            this.inflater = (LayoutInflater) context2.getSystemService("layout_inflater");
        }

        /* access modifiers changed from: private */
        public void setListData(ArrayList<Map<String, Object>> data) {
            this.items = data;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = this.inflater.inflate((int) R.layout.id_row, (ViewGroup) null);
            }
            ((TextView) v.findViewById(R.id.list_id)).setText(this.items.get(position).get("url").toString());
            return v;
        }
    }
}
