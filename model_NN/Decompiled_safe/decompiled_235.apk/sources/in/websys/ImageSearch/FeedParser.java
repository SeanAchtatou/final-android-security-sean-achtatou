package in.websys.ImageSearch;

import java.util.List;

public interface FeedParser {
    List<Message> parse();
}
