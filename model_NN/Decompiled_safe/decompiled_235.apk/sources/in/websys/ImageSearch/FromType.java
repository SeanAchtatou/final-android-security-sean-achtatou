package in.websys.ImageSearch;

public enum FromType {
    FLICKR,
    PICASA,
    PHOTOZOU,
    GOOGLE,
    HATENA_KEYWORD,
    HATENA_TAG
}
