package in.websys.ImageSearch;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public abstract class BaseFeedParser implements FeedParser {
    private final URL feedUrl;

    protected BaseFeedParser(String feedUrl2) {
        try {
            this.feedUrl = new URL(feedUrl2);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    /* access modifiers changed from: protected */
    public InputStream getInputStream() {
        try {
            return this.feedUrl.openConnection().getInputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /* access modifiers changed from: protected */
    public URL getFeedUrl() {
        return this.feedUrl;
    }
}
