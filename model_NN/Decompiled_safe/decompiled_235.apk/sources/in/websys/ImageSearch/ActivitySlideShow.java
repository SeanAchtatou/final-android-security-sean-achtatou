package in.websys.ImageSearch;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class ActivitySlideShow extends Activity implements View.OnTouchListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$in$websys$ImageSearch$ActivitySlideShow$ConMenuKeyword = null;
    private static /* synthetic */ int[] $SWITCH_TABLE$in$websys$ImageSearch$ActivitySlideShow$SlideshowMenuType = null;
    private static final int FADE_TIME = 1000;
    private static final int MM_SEC = 1000;
    private static final int WAIT_TIME = 100;
    private AlphaAnimation alphaDisp;
    private AlphaAnimation alphaDispSoon;
    private AlphaAnimation alphaHide;
    private AlphaAnimation alphaHideSoon;
    private SharedPreferences config;
    private float firstTouch;
    private boolean isFlip = false;
    private FrameLayout mDispFrm;
    private ImageView mDispImg;
    private LinearLayout mDispProgress;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    private FrameLayout mHideFrm;
    private ImageView mHideImg;
    private LinearLayout mHideProgress;
    private Bitmap mNotFoundImage;
    private int mNowPos = 0;
    private int mPos = 0;
    private LinearLayout mProgress1;
    private LinearLayout mProgress2;
    private FrameLayout mSlideshow_frm1;
    private FrameLayout mSlideshow_frm2;
    private ImageView mSlideshow_img1;
    private ImageView mSlideshow_img2;
    private LinearLayout mSlideshow_linear;
    private TextView mTextPage;
    long m_start;
    /* access modifiers changed from: private */
    public Bitmap mbitNextImage;
    private boolean mblnChange;
    private boolean mblnExec = false;
    private boolean mblnFirstLoad = false;
    private boolean mblnLoad;
    private boolean mblnLoadOnly;
    private boolean mblnNotFound;
    private boolean mblnSlideshow = false;
    private boolean mblnStop = false;
    private List<Message> messages;
    private int mintChangeSec;
    private int mintChangeTime;
    DownloadTask task;
    private ViewFlipper viewFlipper;
    private WindowManager wm;

    private enum ConMenuKeyword {
        PAGE,
        M_IMAGE,
        WALLPAPER
    }

    private enum SlideshowMenuType {
        BACK,
        START,
        STOP,
        SETTING
    }

    static /* synthetic */ int[] $SWITCH_TABLE$in$websys$ImageSearch$ActivitySlideShow$ConMenuKeyword() {
        int[] iArr = $SWITCH_TABLE$in$websys$ImageSearch$ActivitySlideShow$ConMenuKeyword;
        if (iArr == null) {
            iArr = new int[ConMenuKeyword.values().length];
            try {
                iArr[ConMenuKeyword.M_IMAGE.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ConMenuKeyword.PAGE.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ConMenuKeyword.WALLPAPER.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$in$websys$ImageSearch$ActivitySlideShow$ConMenuKeyword = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$in$websys$ImageSearch$ActivitySlideShow$SlideshowMenuType() {
        int[] iArr = $SWITCH_TABLE$in$websys$ImageSearch$ActivitySlideShow$SlideshowMenuType;
        if (iArr == null) {
            iArr = new int[SlideshowMenuType.values().length];
            try {
                iArr[SlideshowMenuType.BACK.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[SlideshowMenuType.SETTING.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[SlideshowMenuType.START.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[SlideshowMenuType.STOP.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$in$websys$ImageSearch$ActivitySlideShow$SlideshowMenuType = iArr;
        }
        return iArr;
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        String pos = getIntent().getStringExtra("POS");
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.act_slideshow);
        this.wm = (WindowManager) getSystemService("window");
        this.messages = MyMessage.getInstance().getMessages();
        this.mSlideshow_img1 = (ImageView) findViewById(R.id.slideshow_img1);
        this.mSlideshow_img2 = (ImageView) findViewById(R.id.slideshow_img2);
        this.mSlideshow_linear = (LinearLayout) findViewById(R.id.slideshow_frm);
        this.mSlideshow_frm1 = (FrameLayout) findViewById(R.id.slideshow_frm1);
        this.mSlideshow_frm2 = (FrameLayout) findViewById(R.id.slideshow_frm2);
        this.mintChangeSec = PreferenceManager.getDefaultSharedPreferences(this).getInt("CHANGE_SEC", 3);
        this.mintChangeTime = this.mintChangeSec * 1000;
        this.viewFlipper = (ViewFlipper) findViewById(R.id.flipper);
        this.mSlideshow_frm1.setOnTouchListener(this);
        this.mSlideshow_frm2.setOnTouchListener(this);
        this.mProgress1 = (LinearLayout) findViewById(R.id.slideshow_progress1);
        this.mProgress2 = (LinearLayout) findViewById(R.id.slideshow_progress2);
        this.mNotFoundImage = BitmapFactory.decodeResource(getResources(), R.drawable.blank);
        this.mTextPage = (TextView) findViewById(R.id.SlidePage);
        registerForContextMenu(this.mSlideshow_frm1);
        registerForContextMenu(this.mSlideshow_frm2);
        if (pos == null) {
            startSlideshow();
            return;
        }
        this.viewFlipper.setInAnimation(null);
        this.viewFlipper.setOutAnimation(null);
        dispImagePos2(new Integer(pos).intValue());
        this.viewFlipper.setDisplayedChild(new Integer(pos).intValue() % 2);
        this.mNowPos = new Integer(pos).intValue();
        this.mblnFirstLoad = true;
    }

    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo info) {
        if (!this.mblnExec) {
            super.onCreateContextMenu(menu, view, info);
            AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) info;
            this.mblnStop = true;
            this.mblnExec = true;
            menu.add(0, ConMenuKeyword.PAGE.ordinal(), ConMenuKeyword.PAGE.ordinal(), (int) R.string.open_page);
            menu.add(0, ConMenuKeyword.M_IMAGE.ordinal(), ConMenuKeyword.M_IMAGE.ordinal(), (int) R.string.m_image);
            menu.add(0, ConMenuKeyword.WALLPAPER.ordinal(), ConMenuKeyword.WALLPAPER.ordinal(), (int) R.string.wallpaper);
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        super.onContextItemSelected(item);
        switch ($SWITCH_TABLE$in$websys$ImageSearch$ActivitySlideShow$ConMenuKeyword()[ConMenuKeyword.values()[item.getItemId()].ordinal()]) {
            case R.styleable.com_google_ads_AdView_adUnitId /*1*/:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.messages.get(this.mNowPos).getLink().toString())));
                return true;
            case 2:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.messages.get(this.mNowPos).getImage())));
                return true;
            case 3:
                Bitmap bit = getBlob(this.messages.get(this.mNowPos).getImage());
                if (bit == null) {
                    return true;
                }
                try {
                    setWallpaper(bit);
                    return true;
                } catch (IOException e) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setMessage((int) R.string.cannot_get_image);
                    alert.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
                    alert.show();
                    return true;
                }
            default:
                return true;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r13, android.view.MotionEvent r14) {
        /*
            r12 = this;
            r10 = 4600877379321698714(0x3fd999999999999a, double:0.4)
            r9 = 0
            r8 = 0
            r7 = 1
            android.view.WindowManager r3 = r12.wm
            android.view.Display r3 = r3.getDefaultDisplay()
            int r0 = r3.getWidth()
            float r3 = r14.getRawX()
            int r2 = (int) r3
            int r3 = r13.getId()
            switch(r3) {
                case 2131099670: goto L_0x001f;
                case 2131099671: goto L_0x001e;
                case 2131099672: goto L_0x001e;
                case 2131099673: goto L_0x001f;
                default: goto L_0x001e;
            }
        L_0x001e:
            return r9
        L_0x001f:
            int r3 = r14.getAction()
            switch(r3) {
                case 0: goto L_0x0027;
                case 1: goto L_0x0117;
                case 2: goto L_0x0035;
                default: goto L_0x0026;
            }
        L_0x0026:
            goto L_0x001e
        L_0x0027:
            long r3 = r14.getDownTime()
            r12.m_start = r3
            float r3 = r14.getRawX()
            r12.firstTouch = r3
            r12.mblnExec = r9
        L_0x0035:
            boolean r3 = r12.isFlip
            if (r3 != 0) goto L_0x001e
            boolean r3 = r12.mblnExec
            if (r3 != 0) goto L_0x001e
            float r3 = (float) r2
            float r4 = r12.firstTouch
            float r3 = r3 - r4
            double r3 = (double) r3
            double r5 = (double) r0
            double r5 = r5 * r10
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x00a9
            r12.mblnExec = r7
            r12.isFlip = r7
            int r3 = r12.mNowPos
            int r1 = r3 - r7
            if (r1 >= 0) goto L_0x0081
            java.util.List<in.websys.ImageSearch.Message> r3 = r12.messages
            int r3 = r3.size()
            int r1 = r3 - r7
            int r3 = r1 % 2
            if (r3 != 0) goto L_0x0081
            android.widget.ImageView r3 = r12.mSlideshow_img2
            android.widget.ImageView r4 = r12.mSlideshow_img1
            android.graphics.drawable.Drawable r4 = r4.getDrawable()
            r3.setImageDrawable(r4)
            android.widget.ImageView r3 = r12.mSlideshow_img1
            r3.setImageBitmap(r8)
            android.widget.ViewFlipper r3 = r12.viewFlipper
            r3.setInAnimation(r8)
            android.widget.ViewFlipper r3 = r12.viewFlipper
            r3.setOutAnimation(r8)
            android.widget.ViewFlipper r3 = r12.viewFlipper
            int r4 = r1 % 2
            int r4 = r4 + 1
            r3.setDisplayedChild(r4)
        L_0x0081:
            r12.dispImagePos2(r1)
            android.widget.ViewFlipper r3 = r12.viewFlipper
            r4 = 2130968578(0x7f040002, float:1.7545814E38)
            android.view.animation.Animation r4 = android.view.animation.AnimationUtils.loadAnimation(r12, r4)
            r3.setInAnimation(r4)
            android.widget.ViewFlipper r3 = r12.viewFlipper
            r4 = 2130968581(0x7f040005, float:1.754582E38)
            android.view.animation.Animation r4 = android.view.animation.AnimationUtils.loadAnimation(r12, r4)
            r3.setOutAnimation(r4)
            android.widget.ViewFlipper r3 = r12.viewFlipper
            int r4 = r1 % 2
            r3.setDisplayedChild(r4)
            r12.mNowPos = r1
            r12.mblnFirstLoad = r7
            goto L_0x001e
        L_0x00a9:
            float r3 = r12.firstTouch
            float r4 = (float) r2
            float r3 = r3 - r4
            double r3 = (double) r3
            double r5 = (double) r0
            double r5 = r5 * r10
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x001e
            r12.mblnExec = r7
            r12.isFlip = r7
            int r3 = r12.mNowPos
            int r1 = r3 + 1
            java.util.List<in.websys.ImageSearch.Message> r3 = r12.messages
            int r3 = r3.size()
            int r3 = r3 - r7
            if (r1 <= r3) goto L_0x00ef
            r1 = 0
            int r3 = r12.mNowPos
            int r3 = r3 % 2
            if (r3 != 0) goto L_0x00ef
            android.widget.ImageView r3 = r12.mSlideshow_img2
            android.widget.ImageView r4 = r12.mSlideshow_img1
            android.graphics.drawable.Drawable r4 = r4.getDrawable()
            r3.setImageDrawable(r4)
            android.widget.ImageView r3 = r12.mSlideshow_img1
            r3.setImageBitmap(r8)
            android.widget.ViewFlipper r3 = r12.viewFlipper
            r3.setInAnimation(r8)
            android.widget.ViewFlipper r3 = r12.viewFlipper
            r3.setOutAnimation(r8)
            android.widget.ViewFlipper r3 = r12.viewFlipper
            int r4 = r1 % 2
            int r4 = r4 + 1
            r3.setDisplayedChild(r4)
        L_0x00ef:
            r12.dispImagePos2(r1)
            android.widget.ViewFlipper r3 = r12.viewFlipper
            r4 = 2130968579(0x7f040003, float:1.7545816E38)
            android.view.animation.Animation r4 = android.view.animation.AnimationUtils.loadAnimation(r12, r4)
            r3.setInAnimation(r4)
            android.widget.ViewFlipper r3 = r12.viewFlipper
            r4 = 2130968580(0x7f040004, float:1.7545818E38)
            android.view.animation.Animation r4 = android.view.animation.AnimationUtils.loadAnimation(r12, r4)
            r3.setOutAnimation(r4)
            android.widget.ViewFlipper r3 = r12.viewFlipper
            int r4 = r1 % 2
            r3.setDisplayedChild(r4)
            r12.mNowPos = r1
            r12.mblnFirstLoad = r7
            goto L_0x001e
        L_0x0117:
            r12.isFlip = r9
            r3 = 0
            r12.m_start = r3
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: in.websys.ImageSearch.ActivitySlideShow.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mblnStop = true;
        this.mblnSlideshow = false;
    }

    public void startSlideshow() {
        this.mblnLoad = true;
        this.mblnLoadOnly = false;
        this.mblnChange = false;
        this.mblnStop = false;
        this.mblnSlideshow = true;
        this.mintChangeTime = 0;
        this.alphaHideSoon = null;
        this.alphaHideSoon = new AlphaAnimation(0.0f, 0.0f);
        this.alphaHideSoon.setDuration(1);
        this.alphaHideSoon.setFillAfter(true);
        this.alphaHideSoon.setFillEnabled(true);
        this.mSlideshow_frm1.startAnimation(this.alphaHideSoon);
        this.mSlideshow_frm2.startAnimation(this.alphaHideSoon);
        dispImage();
    }

    public void restartSlideshow() {
        this.mblnLoad = true;
        this.mblnLoadOnly = false;
        this.mblnChange = false;
        this.mblnStop = false;
        this.mblnSlideshow = true;
        this.mPos = this.mNowPos + 1;
        if (this.mPos > this.messages.size() - 1) {
            this.mPos = 0;
        }
        dispImage();
    }

    public void stopSlideshow() {
        this.mblnStop = true;
    }

    public void dispImagePos2(int pos) {
        this.mblnLoad = true;
        this.mblnLoadOnly = true;
        this.mblnChange = false;
        this.mblnStop = false;
        this.mblnSlideshow = false;
        this.mintChangeTime = 0;
        this.mPos = pos;
        this.alphaDispSoon = null;
        this.alphaDispSoon = new AlphaAnimation(1.0f, 1.0f);
        this.alphaDispSoon.setDuration(1);
        this.alphaDispSoon.setFillAfter(true);
        this.alphaDispSoon.setFillEnabled(true);
        this.mSlideshow_frm1.startAnimation(this.alphaDispSoon);
        this.mSlideshow_frm2.startAnimation(this.alphaDispSoon);
        dispImage();
    }

    public void dispImage() {
        this.mTextPage.setText(String.valueOf(String.valueOf(this.mNowPos)) + " " + String.valueOf(this.mPos));
        boolean blnWait = false;
        if (this.mblnStop) {
            this.mblnSlideshow = false;
        } else if (this.mblnLoad) {
            if (this.mblnSlideshow && this.mPos == 0 && this.messages.size() % 2 == 1) {
                this.mSlideshow_img2.setImageDrawable(this.mSlideshow_img1.getDrawable());
                this.alphaDispSoon = null;
                this.alphaDispSoon = new AlphaAnimation(1.0f, 1.0f);
                this.alphaDispSoon.setDuration(1);
                this.alphaDispSoon.setFillAfter(true);
                this.alphaDispSoon.setFillEnabled(true);
                this.mSlideshow_frm2.startAnimation(this.alphaDispSoon);
                this.mSlideshow_frm2.setVisibility(0);
                this.alphaHideSoon = null;
                this.alphaHideSoon = new AlphaAnimation(0.0f, 0.0f);
                this.alphaHideSoon.setDuration(1);
                this.alphaHideSoon.setFillAfter(true);
                this.alphaHideSoon.setFillEnabled(true);
                this.mSlideshow_frm1.startAnimation(this.alphaHideSoon);
                this.mSlideshow_img1.setImageBitmap(null);
            }
            if (this.mPos % 2 == 0) {
                this.mDispImg = this.mSlideshow_img2;
                this.mHideImg = this.mSlideshow_img1;
                this.mDispFrm = (FrameLayout) findViewById(R.id.slideshow_frm2);
                this.mHideFrm = (FrameLayout) findViewById(R.id.slideshow_frm1);
                this.mDispProgress = this.mProgress2;
                this.mHideProgress = this.mProgress1;
            } else {
                this.mDispImg = this.mSlideshow_img1;
                this.mHideImg = this.mSlideshow_img2;
                this.mDispFrm = (FrameLayout) findViewById(R.id.slideshow_frm1);
                this.mHideFrm = (FrameLayout) findViewById(R.id.slideshow_frm2);
                this.mDispProgress = this.mProgress1;
                this.mHideProgress = this.mProgress2;
            }
            this.mHideImg.setImageBitmap(null);
            this.mbitNextImage = null;
            this.mHideImg.setVisibility(8);
            this.mHideProgress.setVisibility(0);
            String path = this.messages.get(this.mPos).getImage();
            Bitmap bit = getBlob(path);
            if (bit != null) {
                this.mHideImg.setImageBitmap(bit);
                this.mbitNextImage = bit;
                this.mHideImg.setVisibility(0);
                this.mHideProgress.setVisibility(8);
            } else {
                if (this.task != null) {
                    this.task.cancel(true);
                    this.task = null;
                }
                this.task = new DownloadTask(this);
                this.task.setImageView(this.mHideImg);
                this.task.setProgress(this.mHideProgress);
                this.task.setNotFoundImage(this.mNotFoundImage);
                this.task.execute(path);
            }
            if (this.mblnSlideshow) {
                this.mPos++;
                if (this.mPos > this.messages.size() - 1) {
                    this.mPos = 0;
                }
            }
            this.mblnLoad = false;
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    ActivitySlideShow.this.mHandler.post(new Runnable() {
                        public void run() {
                            ActivitySlideShow.this.dispImage();
                        }
                    });
                }
            }, 100);
        } else {
            if (this.mHideImg != null) {
                BitmapDrawable bd = (BitmapDrawable) this.mHideImg.getDrawable();
                if (bd == null) {
                    blnWait = true;
                } else if (bd.getBitmap() == null) {
                    blnWait = true;
                }
            }
            if (this.mblnChange) {
                if (this.mblnLoadOnly) {
                    this.mblnStop = true;
                    return;
                }
                this.mblnChange = false;
                this.alphaHide = null;
                this.alphaHide = new AlphaAnimation(1.0f, 0.0f);
                this.alphaHide.setDuration(1000);
                this.alphaHide.setFillAfter(true);
                this.alphaHide.setFillEnabled(true);
                this.alphaDisp = null;
                this.alphaDisp = new AlphaAnimation(0.0f, 1.0f);
                this.alphaDisp.setDuration(1000);
                this.alphaDisp.setFillAfter(true);
                this.alphaDisp.setFillEnabled(true);
                this.viewFlipper.setInAnimation(null);
                this.viewFlipper.setOutAnimation(null);
                if (this.mblnSlideshow) {
                    this.mNowPos = this.mPos - 1;
                    this.mblnFirstLoad = true;
                } else {
                    this.mNowPos = this.mPos;
                    this.mblnFirstLoad = true;
                }
                this.viewFlipper.setDisplayedChild(this.mNowPos);
                this.mHideFrm.setVisibility(0);
                this.mDispFrm.setVisibility(0);
                this.mDispFrm.startAnimation(this.alphaHide);
                this.mHideFrm.startAnimation(this.alphaDisp);
                this.mTextPage.setText(String.valueOf(String.valueOf(this.mNowPos)) + " " + String.valueOf(this.mPos));
                this.mblnLoad = true;
                if (!this.mblnSlideshow) {
                    this.mblnStop = true;
                }
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        ActivitySlideShow.this.mHandler.post(new Runnable() {
                            public void run() {
                                ActivitySlideShow.this.dispImage();
                            }
                        });
                    }
                }, 1000);
            } else if (blnWait) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        ActivitySlideShow.this.mHandler.post(new Runnable() {
                            public void run() {
                                ActivitySlideShow.this.dispImage();
                            }
                        });
                    }
                }, 100);
            } else {
                this.mblnChange = true;
                Handler handler = new Handler();
                if (this.mblnNotFound) {
                    this.mintChangeTime = 0;
                    this.mblnNotFound = false;
                }
                if (this.mbitNextImage != null && this.mbitNextImage == this.mNotFoundImage) {
                    this.mblnNotFound = true;
                }
                handler.postDelayed(new Runnable() {
                    public void run() {
                        ActivitySlideShow.this.mHandler.post(new Runnable() {
                            public void run() {
                                ActivitySlideShow.this.dispImage();
                            }
                        });
                    }
                }, (long) this.mintChangeTime);
                this.mintChangeSec = PreferenceManager.getDefaultSharedPreferences(this).getInt("CHANGE_SEC", 3);
                this.mintChangeTime = this.mintChangeSec * 1000;
            }
        }
    }

    private static byte[] bmp2data(Bitmap src) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(Bitmap.CompressFormat.JPEG, WAIT_TIME, os);
        return os.toByteArray();
    }

    private static Bitmap data2bmp(byte[] data) {
        return BitmapFactory.decodeByteArray(data, 0, data.length);
    }

    private Bitmap getBlob(String url) {
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            db = new DatabaseHelper(this).getReadableDatabase();
            cursor = db.rawQuery(String.format("select %s from %s where %s = ?", "bitmap", "image_cache", "image_url"), new String[]{url});
            if (cursor.moveToFirst()) {
                Bitmap data2bmp = data2bmp(cursor.getBlob(0));
            }
            if (cursor != null) {
                cursor.close();
            }
            if (db != null) {
                db.close();
            }
            return null;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            if (db != null) {
                db.close();
            }
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();
        menu.add(0, SlideshowMenuType.BACK.ordinal(), SlideshowMenuType.BACK.ordinal(), (int) R.string.back).setIcon(17301580);
        if (!this.mblnSlideshow) {
            menu.add(0, SlideshowMenuType.START.ordinal(), SlideshowMenuType.START.ordinal(), (int) R.string.start).setIcon(17301540);
        } else {
            menu.add(0, SlideshowMenuType.STOP.ordinal(), SlideshowMenuType.STOP.ordinal(), (int) R.string.stop).setIcon(17301539);
        }
        menu.add(0, SlideshowMenuType.SETTING.ordinal(), SlideshowMenuType.SETTING.ordinal(), (int) R.string.setting).setIcon(17301577);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch ($SWITCH_TABLE$in$websys$ImageSearch$ActivitySlideShow$SlideshowMenuType()[SlideshowMenuType.values()[item.getItemId()].ordinal()]) {
            case R.styleable.com_google_ads_AdView_adUnitId /*1*/:
                this.mblnStop = true;
                finish();
                break;
            case 2:
                restartSlideshow();
                break;
            case 3:
                stopSlideshow();
                break;
            case 4:
                startActivity(new Intent(this, Settings.class));
                break;
        }
        return true;
    }

    public void onDestroy() {
        super.onDestroy();
    }

    private class DownloadTask extends AsyncTask<String, Integer, Bitmap> {
        private HttpClient mClient = new DefaultHttpClient();
        private Context mContext;
        private HttpGet mGetMethod = new HttpGet();
        private ImageView mImageView;
        private ListData mListData;
        private Bitmap mNotFoundImage;
        private LinearLayout mProgress;
        private int mRetry = 0;

        public DownloadTask(Context context) {
            this.mContext = context;
        }

        /* access modifiers changed from: package-private */
        public void setImageView(ImageView pImageView) {
            this.mImageView = pImageView;
        }

        /* access modifiers changed from: package-private */
        public void setProgress(LinearLayout pImageProgress) {
            this.mProgress = pImageProgress;
        }

        /* access modifiers changed from: package-private */
        public void setListData(ListData pListData) {
            this.mListData = pListData;
        }

        /* access modifiers changed from: package-private */
        public void setNotFoundImage(Bitmap bit) {
            this.mNotFoundImage = bit;
        }

        /* access modifiers changed from: package-private */
        public Bitmap downloadImage(String uri) {
            try {
                this.mRetry++;
                this.mGetMethod.setURI(new URI(uri));
                HttpResponse resp = this.mClient.execute(this.mGetMethod);
                if (resp.getStatusLine().getStatusCode() < 400) {
                    InputStream is = resp.getEntity().getContent();
                    Bitmap bit = createBitmap(is);
                    is.close();
                    return bit;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        private Bitmap createBitmap(InputStream is) {
            try {
                return resizeBitmap(BitmapFactory.decodeStream(new PatchInputStream(is)));
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
         arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
         candidates:
          ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
          ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
        private Bitmap resizeBitmap(Bitmap bmp) {
            float d_size;
            float bmp_size;
            Bitmap scaledBitmap;
            try {
                Display disp = ((WindowManager) ActivitySlideShow.this.getSystemService("window")).getDefaultDisplay();
                int width = disp.getWidth();
                int height = disp.getHeight();
                if (width > height) {
                    d_size = (float) width;
                } else {
                    d_size = (float) height;
                }
                if (bmp.getWidth() > bmp.getHeight()) {
                    bmp_size = (float) bmp.getWidth();
                } else {
                    bmp_size = (float) bmp.getHeight();
                }
                float rate = d_size / bmp_size;
                if (rate < 1.0f) {
                    Matrix matrix = new Matrix();
                    matrix.postScale(rate, rate);
                    scaledBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
                } else {
                    scaledBitmap = bmp;
                }
                return scaledBitmap;
            } catch (Exception e) {
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public Bitmap doInBackground(String... params) {
            String uri = params[0];
            Bitmap bit = null;
            if (isCancelled()) {
                return null;
            }
            while (this.mRetry <= 3 && bit == null) {
                if (isCancelled()) {
                    return null;
                }
                bit = downloadImage(uri);
            }
            if (isCancelled()) {
                return null;
            }
            if (bit != null) {
                setBlob(uri, bit);
            }
            if (bit == null && this.mNotFoundImage != null) {
                bit = this.mNotFoundImage;
            }
            return bit;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Bitmap result) {
            this.mImageView.setImageBitmap(result);
            ActivitySlideShow.this.mbitNextImage = result;
            this.mImageView.setVisibility(0);
            if (this.mProgress != null) {
                this.mProgress.setVisibility(8);
            }
            if (this.mListData != null) {
                this.mListData.bitmap = result;
            }
        }

        private byte[] bmp2data(Bitmap src) {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            src.compress(Bitmap.CompressFormat.JPEG, ActivitySlideShow.WAIT_TIME, os);
            return os.toByteArray();
        }

        private void setBlob(String url, Bitmap bit) {
            SQLiteDatabase db = null;
            try {
                byte[] blob = bmp2data(bit);
                db = new DatabaseHelper(this.mContext).getWritableDatabase();
                db.beginTransaction();
                db.delete("image_cache", "image_url = ?", new String[]{url});
                ContentValues values = new ContentValues();
                values.put("image_url", url);
                values.put("bitmap", blob);
                db.insert("image_cache", null, values);
                db.setTransactionSuccessful();
                if (db != null) {
                    db.endTransaction();
                    db.close();
                }
            } catch (SQLiteException e) {
                if (db != null) {
                    db.endTransaction();
                    db.close();
                }
            } catch (Throwable th) {
                if (db != null) {
                    db.endTransaction();
                    db.close();
                }
                throw th;
            }
        }
    }
}
