package in.websys.ImageSearch;

public class Message implements Comparable<Message> {
    private String image;
    private String link;
    private String thumbnail;

    public String getThumbnail() {
        return this.thumbnail;
    }

    public void setThumbnail(String thumbnail2) {
        this.thumbnail = thumbnail2.trim();
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String l_image) {
        this.image = l_image.trim();
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String link2) {
        this.link = link2.trim();
    }

    public int compareTo(Message another) {
        if (another == null) {
            return 1;
        }
        return another.image.compareTo(this.image);
    }
}
