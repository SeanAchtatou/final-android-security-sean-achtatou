package in.websys.ImageSearch;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.ViewFlipper;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityGridView extends Activity implements View.OnTouchListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$in$websys$ImageSearch$ActivityGridView$MenuType = null;
    static final int REQUEST_OK = 1;
    private static final int SHOW_IMG_FORM = 1;
    /* access modifiers changed from: private */
    public static Context mContext;
    ListTemplateAdapter adapter;
    private float firstTouch;
    /* access modifiers changed from: private */
    public final Handler handler = new Handler();
    private boolean isFlip = false;
    /* access modifiers changed from: private */
    public ActivityGridView mActivity;
    /* access modifiers changed from: private */
    public ImageButton mImageButton;
    private GridView mImageGrid;
    private LinearLayout mLayoutId;
    /* access modifiers changed from: private */
    public EditText mPageId;
    private List<Message> messages;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog;
    private Runnable runnable = new Runnable() {
        public void run() {
            try {
                MyMessage.getInstance().loadFeed();
                try {
                    ActivityGridView.this.handler.post(new Runnable() {
                        public void run() {
                            ActivityGridView.this.showFeed();
                        }
                    });
                } catch (Exception e) {
                    ActivityGridView.this.progressDialog.dismiss();
                    System.out.println(e);
                }
                ActivityGridView.this.progressDialog.dismiss();
            } catch (Exception e2) {
                ActivityGridView.this.progressDialog.dismiss();
                System.out.println(e2);
                AlertDialog.Builder alert = new AlertDialog.Builder(ActivityGridView.this.mActivity);
                alert.setTitle((int) R.string.error);
                alert.setMessage((int) R.string.error_load_data);
                alert.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
                alert.show();
            }
        }
    };
    private ViewFlipper viewFlipper;
    private WindowManager wm;

    private enum MenuType {
        KEYWORD,
        SLIDESHOW,
        SETTING
    }

    static /* synthetic */ int[] $SWITCH_TABLE$in$websys$ImageSearch$ActivityGridView$MenuType() {
        int[] iArr = $SWITCH_TABLE$in$websys$ImageSearch$ActivityGridView$MenuType;
        if (iArr == null) {
            iArr = new int[MenuType.values().length];
            try {
                iArr[MenuType.KEYWORD.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[MenuType.SETTING.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[MenuType.SLIDESHOW.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$in$websys$ImageSearch$ActivityGridView$MenuType = iArr;
        }
        return iArr;
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        getWindow().setSoftInputMode(3);
        setContentView((int) R.layout.act_gridview);
        this.mActivity = this;
        this.mPageId = (EditText) findViewById(R.id.IdPage);
        this.mImageButton = (ImageButton) findViewById(R.id.ButtonRefresh);
        this.mImageGrid = (GridView) findViewById(R.id.ImageGrid);
        mContext = getApplicationContext();
        this.viewFlipper = (ViewFlipper) findViewById(R.id.flipperId);
        this.mLayoutId = (LinearLayout) findViewById(R.id.LayoutId);
        this.mLayoutId.setOnTouchListener(this);
        this.mImageGrid.setOnTouchListener(this);
        this.mPageId = (EditText) findViewById(R.id.IdPage);
        this.mImageGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                ImageCache.clearCache();
                Intent intent = new Intent(ActivityGridView.mContext, ActivitySlideShow.class);
                intent.putExtra("POS", String.valueOf(position));
                ActivityGridView.this.startActivityForResult(intent, 1);
            }
        });
        this.mPageId.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 0 || keyCode != 66) {
                    return false;
                }
                ActivityGridView.this.mImageButton.performClick();
                return true;
            }
        });
        this.mImageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                long lPage;
                ((InputMethodManager) ActivityGridView.this.getSystemService("input_method")).hideSoftInputFromWindow(ActivityGridView.this.mPageId.getWindowToken(), 0);
                try {
                    lPage = (long) Integer.parseInt(ActivityGridView.this.mPageId.getText().toString());
                } catch (NumberFormatException e) {
                    lPage = 0;
                }
                if (lPage == 0) {
                    lPage = MyMessage.getInstance().getPage();
                }
                MyMessage.getInstance().setPage(lPage);
                ActivityGridView.this.dispList();
            }
        });
        EditText et = new EditText(this) {
            /* access modifiers changed from: protected */
            public void onSizeChanged(int w, int h, int oldw, int oldh) {
                if (ActivityGridView.this.mPageId != null) {
                    ActivityGridView.this.mPageId.setWidth(w);
                }
            }
        };
        this.wm = (WindowManager) getSystemService("window");
        et.setText("99999");
        ((LinearLayout) findViewById(R.id.IdMain)).addView(et, new LinearLayout.LayoutParams(-2, 0));
        dispList();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.adapter != null) {
            this.adapter.clearTask();
        }
        deleteImageCache(null);
        return super.onKeyDown(keyCode, event);
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r11, android.view.MotionEvent r12) {
        /*
            r10 = this;
            android.view.WindowManager r4 = r10.wm
            android.view.Display r4 = r4.getDefaultDisplay()
            int r0 = r4.getWidth()
            float r4 = r12.getRawX()
            int r3 = (int) r4
            int r4 = r12.getAction()
            switch(r4) {
                case 0: goto L_0x0018;
                case 1: goto L_0x00bc;
                case 2: goto L_0x001e;
                default: goto L_0x0016;
            }
        L_0x0016:
            r4 = 0
        L_0x0017:
            return r4
        L_0x0018:
            float r4 = r12.getRawX()
            r10.firstTouch = r4
        L_0x001e:
            boolean r4 = r10.isFlip
            if (r4 != 0) goto L_0x0016
            float r4 = (float) r3
            float r5 = r10.firstTouch
            float r4 = r4 - r5
            double r4 = (double) r4
            double r6 = (double) r0
            r8 = 4600877379321698714(0x3fd999999999999a, double:0.4)
            double r6 = r6 * r8
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 <= 0) goto L_0x0073
            in.websys.ImageSearch.MyMessage r4 = in.websys.ImageSearch.MyMessage.getInstance()
            long r1 = r4.getPage()
            r4 = 1
            int r4 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r4 > 0) goto L_0x0042
            r4 = 1
            goto L_0x0017
        L_0x0042:
            r4 = 1
            r10.isFlip = r4
            android.widget.ViewFlipper r4 = r10.viewFlipper
            r5 = 2130968578(0x7f040002, float:1.7545814E38)
            android.view.animation.Animation r5 = android.view.animation.AnimationUtils.loadAnimation(r10, r5)
            r4.setInAnimation(r5)
            android.widget.ViewFlipper r4 = r10.viewFlipper
            r5 = 2130968581(0x7f040005, float:1.754582E38)
            android.view.animation.Animation r5 = android.view.animation.AnimationUtils.loadAnimation(r10, r5)
            r4.setOutAnimation(r5)
            in.websys.ImageSearch.MyMessage r4 = in.websys.ImageSearch.MyMessage.getInstance()
            r5 = 1
            long r5 = r1 - r5
            r4.setPage(r5)
            android.widget.ViewFlipper r4 = r10.viewFlipper
            r5 = 0
            r4.setDisplayedChild(r5)
            r10.dispList()
            r4 = 1
            goto L_0x0017
        L_0x0073:
            float r4 = r10.firstTouch
            float r5 = (float) r3
            float r4 = r4 - r5
            double r4 = (double) r4
            double r6 = (double) r0
            r8 = 4600877379321698714(0x3fd999999999999a, double:0.4)
            double r6 = r6 * r8
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 <= 0) goto L_0x0016
            r4 = 1
            r10.isFlip = r4
            android.widget.ViewFlipper r4 = r10.viewFlipper
            r5 = 2130968579(0x7f040003, float:1.7545816E38)
            android.view.animation.Animation r5 = android.view.animation.AnimationUtils.loadAnimation(r10, r5)
            r4.setInAnimation(r5)
            android.widget.ViewFlipper r4 = r10.viewFlipper
            r5 = 2130968580(0x7f040004, float:1.7545818E38)
            android.view.animation.Animation r5 = android.view.animation.AnimationUtils.loadAnimation(r10, r5)
            r4.setOutAnimation(r5)
            in.websys.ImageSearch.MyMessage r4 = in.websys.ImageSearch.MyMessage.getInstance()
            long r1 = r4.getPage()
            in.websys.ImageSearch.MyMessage r4 = in.websys.ImageSearch.MyMessage.getInstance()
            r5 = 1
            long r5 = r5 + r1
            r4.setPage(r5)
            android.widget.ViewFlipper r4 = r10.viewFlipper
            r5 = 0
            r4.setDisplayedChild(r5)
            r10.dispList()
            r4 = 1
            goto L_0x0017
        L_0x00bc:
            r4 = 0
            r10.isFlip = r4
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: in.websys.ImageSearch.ActivityGridView.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();
        menu.add(0, MenuType.KEYWORD.ordinal(), MenuType.KEYWORD.ordinal(), (int) R.string.keyword).setIcon(17301583);
        menu.add(0, MenuType.SLIDESHOW.ordinal(), MenuType.SLIDESHOW.ordinal(), (int) R.string.slideshow).setIcon(17301587);
        menu.add(0, MenuType.SETTING.ordinal(), MenuType.SETTING.ordinal(), (int) R.string.setting).setIcon(17301577);
        if (MyMessage.getInstance() == null) {
            menu.getItem(MenuType.SLIDESHOW.ordinal()).setEnabled(false);
            return true;
        } else if (MyMessage.getInstance().getMessages() == null) {
            menu.getItem(MenuType.SLIDESHOW.ordinal()).setEnabled(false);
            return true;
        } else if (MyMessage.getInstance().getMessages().size() > 0) {
            return true;
        } else {
            menu.getItem(MenuType.SLIDESHOW.ordinal()).setEnabled(false);
            return true;
        }
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        super.onMenuItemSelected(featureId, item);
        switch ($SWITCH_TABLE$in$websys$ImageSearch$ActivityGridView$MenuType()[MenuType.values()[item.getItemId()].ordinal()]) {
            case R.styleable.com_google_ads_AdView_adUnitId /*1*/:
                if (this.adapter != null) {
                    this.adapter.clearTask();
                }
                deleteImageCache(null);
                setResult(-1, new Intent(this, ActivityCondition.class));
                finish();
                return true;
            case 2:
                Intent intent = new Intent(this, ActivitySlideShow.class);
                intent.putExtra("MAIN_FORM_TEXT", MyMessage.getInstance().getKeyword());
                startActivity(intent);
                return true;
            case 3:
                startActivity(new Intent(this, Settings.class));
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: private */
    public void showFeed() {
        int intCnt = 0;
        String strImage = "";
        try {
            if (this.adapter != null) {
                this.adapter.clearTask();
            }
            ImageCache.clearCache();
            this.messages = MyMessage.getInstance().getMessages();
            this.mPageId.setText(String.valueOf(MyMessage.getInstance().getPage()));
            if (this.messages == null) {
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle((int) R.string.error);
                alert.setMessage((int) R.string.error_no_Image);
                alert.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
                alert.show();
                return;
            }
            this.adapter = null;
            String[] from_template = {"url", "viewImage"};
            int[] to_template = {R.id.list_id, R.id.GridImageThumb};
            GridView list_group = (GridView) findViewById(R.id.ImageGrid);
            ArrayList<Map<String, Object>> data = new ArrayList<>();
            for (Message msg : this.messages) {
                if (strImage == "") {
                    strImage = msg.getThumbnail();
                }
                Map<String, Object> map = new HashMap<>();
                map.put("url", msg.getThumbnail());
                map.put("viewImage", msg.getThumbnail());
                data.add(map);
                intCnt++;
            }
            if (intCnt == 0) {
                AlertDialog.Builder alert2 = new AlertDialog.Builder(this);
                alert2.setTitle((int) R.string.error);
                alert2.setMessage((int) R.string.error_no_Image);
                alert2.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
                alert2.show();
                return;
            }
            this.adapter = new ListTemplateAdapter(this, data, R.layout.act_gridview, from_template, to_template);
            if (data != null && data.size() > 0) {
                this.adapter.setListData(data);
                list_group.setFocusable(false);
                list_group.setAdapter((ListAdapter) this.adapter);
                list_group.setScrollingCacheEnabled(false);
                list_group.setTextFilterEnabled(true);
                AlphaAnimation alphaDispSoon = new AlphaAnimation(1.0f, 1.0f);
                alphaDispSoon.setDuration(1);
                alphaDispSoon.setFillAfter(true);
                alphaDispSoon.setFillEnabled(true);
                list_group.startAnimation(alphaDispSoon);
            }
        } catch (Throwable th) {
            moveTaskToBack(true);
        }
    }

    public void dispList() {
        this.messages = null;
        AlphaAnimation alphaHideSoon = new AlphaAnimation(0.0f, 0.0f);
        alphaHideSoon.setDuration(1);
        alphaHideSoon.setFillAfter(true);
        alphaHideSoon.setFillEnabled(true);
        this.mImageGrid.startAnimation(alphaHideSoon);
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setProgressStyle(0);
        this.progressDialog.setMessage(getString(R.string.loading));
        this.progressDialog.setCancelable(true);
        this.progressDialog.show();
        MyMessage.getInstance().setSafeSearch(PreferenceManager.getDefaultSharedPreferences(this).getBoolean("safesearch", true));
        new Thread(this.runnable).start();
        deleteImageCache(null);
    }

    private void deleteImageCache(String url) {
        SQLiteDatabase db = null;
        try {
            db = new DatabaseHelper(this).getWritableDatabase();
            if (url == null) {
                db.delete("image_cache", null, null);
            } else {
                db.delete("image_cache", "image_url = ?", new String[]{url});
            }
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    private class ListTemplateAdapter extends SimpleAdapter {
        /* access modifiers changed from: private */
        public Context context;
        private LayoutInflater inflater;
        private ArrayList<Map<String, Object>> items;
        private List<ImageGetTask> tasks = new ArrayList();

        public ListTemplateAdapter(Context context2, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context2, data, resource, from, to);
            this.context = context2;
            this.inflater = (LayoutInflater) context2.getSystemService("layout_inflater");
        }

        /* access modifiers changed from: private */
        public void setListData(ArrayList<Map<String, Object>> data) {
            this.items = data;
        }

        public void clearTask() {
            for (ImageGetTask task : this.tasks) {
                if (task != null) {
                    task.cancel(true);
                }
            }
            this.tasks.clear();
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = this.inflater.inflate((int) R.layout.image_grid_rowdata, (ViewGroup) null);
            }
            ImageView imageView = (ImageView) v.findViewById(R.id.GridImageThumb);
            LinearLayout waitBar = (LinearLayout) v.findViewById(R.id.GridWaitBar);
            waitBar.setVisibility(0);
            imageView.setVisibility(8);
            String thumbUrl = this.items.get(position).get("viewImage").toString();
            imageView.setImageDrawable(this.context.getResources().getDrawable(R.drawable.blank));
            try {
                imageView.setTag(thumbUrl);
                ImageGetTask task = new ImageGetTask(imageView, waitBar);
                this.tasks.add(task);
                task.execute(thumbUrl);
            } catch (Exception e) {
                imageView.setImageDrawable(this.context.getResources().getDrawable(R.drawable.notfound));
                waitBar.setVisibility(8);
                imageView.setVisibility(0);
            }
            return v;
        }

        class ImageGetTask extends AsyncTask<String, Void, Bitmap> {
            private ImageView image;
            private int mRetry;
            private LinearLayout progress;
            private String tag = this.image.getTag().toString();

            public ImageGetTask(ImageView _image, LinearLayout _progress) {
                this.image = _image;
                this.progress = _progress;
            }

            /* access modifiers changed from: package-private */
            public Bitmap downloadImage(String uri) {
                try {
                    this.mRetry++;
                    Bitmap image2 = ImageCache.getImage(uri);
                    if (image2 == null && (image2 = BitmapFactory.decodeStream(new PatchInputStream(new URL(uri).openStream()))) != null) {
                        ImageCache.setImage(uri, image2);
                    }
                    return image2;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            /* access modifiers changed from: protected */
            public Bitmap doInBackground(String... params) {
                synchronized (ListTemplateAdapter.this.context) {
                    try {
                        String uri = params[0];
                        if (isCancelled()) {
                            return null;
                        }
                        Bitmap bit = downloadImage(uri);
                        if (isCancelled()) {
                            return null;
                        }
                        if (bit == null) {
                            bit = BitmapFactory.decodeResource(ActivityGridView.this.getResources(), R.drawable.notfound);
                        }
                        if (isCancelled()) {
                            return null;
                        }
                        return bit;
                    } catch (Exception e) {
                        Exception exc = e;
                        return null;
                    }
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Bitmap result) {
                if (this.tag.equals(this.image.getTag())) {
                    if (result != null) {
                        this.image.setImageBitmap(result);
                    } else {
                        this.image.setImageDrawable(ListTemplateAdapter.this.context.getResources().getDrawable(R.drawable.notfound));
                    }
                    this.progress.setVisibility(8);
                    this.image.setVisibility(0);
                }
            }
        }
    }
}
