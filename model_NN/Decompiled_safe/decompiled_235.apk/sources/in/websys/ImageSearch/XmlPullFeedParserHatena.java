package in.websys.ImageSearch;

import android.util.Log;
import android.util.Xml;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

public class XmlPullFeedParserHatena extends BaseFeedParser {
    static final String IMAGE = "imageurl";
    static final String ITEM = "item";
    static final String LINK = "link";
    static final String START = "RDF";
    static final String THUMBNAIL = "imageurlmedium";

    public XmlPullFeedParserHatena(String feedUrl) {
        super(feedUrl);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public List<Message> parse() {
        Exception e;
        List<Message> list;
        List<Message> messages = null;
        XmlPullParser parser = Xml.newPullParser();
        try {
            parser.setInput(getInputStream(), null);
            int eventType = parser.getEventType();
            Message currentMessage = null;
            boolean done = false;
            while (true) {
                list = messages;
                if (eventType == 1 || done) {
                    return list;
                }
                switch (eventType) {
                    case R.styleable.com_google_ads_AdView_adSize:
                        try {
                            messages = new ArrayList<>();
                            break;
                        } catch (Exception e2) {
                            e = e2;
                            Log.e("ImageSearch::PullFeedParser", e.getMessage(), e);
                            throw new RuntimeException(e);
                        }
                    case R.styleable.com_google_ads_AdView_adUnitId:
                    default:
                        messages = list;
                        break;
                    case 2:
                        String name = parser.getName();
                        if (!name.equalsIgnoreCase(ITEM)) {
                            if (currentMessage != null) {
                                if (!name.equalsIgnoreCase(LINK)) {
                                    if (!name.equalsIgnoreCase(IMAGE)) {
                                        if (name.equalsIgnoreCase(THUMBNAIL)) {
                                            currentMessage.setThumbnail(parser.nextText());
                                            messages = list;
                                            break;
                                        }
                                    } else {
                                        currentMessage.setImage(parser.nextText());
                                        messages = list;
                                        break;
                                    }
                                } else {
                                    currentMessage.setLink(parser.nextText());
                                    messages = list;
                                    break;
                                }
                            }
                            messages = list;
                            break;
                        } else {
                            currentMessage = new Message();
                            messages = list;
                            break;
                        }
                    case 3:
                        String name2 = parser.getName();
                        if (name2.equalsIgnoreCase(ITEM) && currentMessage != null) {
                            list.add(currentMessage);
                            messages = list;
                            break;
                        } else {
                            if (name2.equalsIgnoreCase(START)) {
                                done = true;
                                messages = list;
                                break;
                            }
                            messages = list;
                            break;
                        }
                }
                eventType = parser.next();
            }
            return list;
        } catch (Exception e3) {
            e = e3;
            Log.e("ImageSearch::PullFeedParser", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
}
