package in.websys.ImageSearch;

import android.graphics.Bitmap;

public final class ListData {
    public Bitmap bitmap;
    public String url;
}
