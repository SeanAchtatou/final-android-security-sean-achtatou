package in.websys.ImageSearch;

import android.util.Log;
import java.util.List;

public class MyMessage {
    private static MyMessage instance = new MyMessage();
    private FromType _from;
    private String _keyword;
    private List<Message> _messages;
    private int _num = 20;
    private long _page = 1;
    private boolean _safesearch;

    private MyMessage() {
    }

    public static MyMessage getInstance() {
        return instance;
    }

    public void setSafeSearch(boolean safesearch) {
        this._safesearch = safesearch;
    }

    public boolean getSafeSearch() {
        return this._safesearch;
    }

    public void setFrom(FromType from) {
        this._from = from;
    }

    public FromType getFrom() {
        return this._from;
    }

    public void setKeyword(String keyword) {
        this._keyword = keyword;
    }

    public String getKeyword() {
        return this._keyword;
    }

    public void setPage(long page) {
        this._page = page;
    }

    public long getPage() {
        return this._page;
    }

    public void setNum(int num) {
        this._num = num;
    }

    public long getNum() {
        return (long) this._num;
    }

    public List<Message> getMessages() {
        return this._messages;
    }

    public void loadFeed() {
        try {
            this._messages = null;
            this._messages = FeedParserFactory.getParser(this._keyword, this._page, this._num).parse();
        } catch (Throwable th) {
            Throwable t = th;
            Log.e("MyMessage", t.getMessage(), t);
        }
    }
}
