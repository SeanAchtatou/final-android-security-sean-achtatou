package in.websys.ImageSearch;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class PatchInputStream extends FilterInputStream {
    public PatchInputStream(InputStream in2) {
        super(in2);
    }

    public long skip(long n) throws IOException {
        long m = 0;
        while (m < n) {
            long _m = this.in.skip(n - m);
            if (_m == 0) {
                break;
            }
            m += _m;
        }
        return m;
    }
}
