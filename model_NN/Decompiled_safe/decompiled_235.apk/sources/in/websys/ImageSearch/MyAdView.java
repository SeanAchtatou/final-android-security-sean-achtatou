package in.websys.ImageSearch;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class MyAdView extends AdView {
    static final String MY_AD_UNIT_ID = "a14e2acb22e39cf";

    public MyAdView(Context context, AttributeSet attrs) {
        super((Activity) context, AdSize.BANNER, MY_AD_UNIT_ID);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.loadAd(new AdRequest());
    }
}
