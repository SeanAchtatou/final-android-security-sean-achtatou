package in.websys.ImageSearch;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TABLE_FILE_NAME = "in.websys.ImageSearch";

    public DatabaseHelper(Context context) {
        super(context, TABLE_FILE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table keyword_cache (keyword text primary key,crtdate date not null);");
        db.execSQL("create table image_cache (image_url text, bitmap blob);");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
