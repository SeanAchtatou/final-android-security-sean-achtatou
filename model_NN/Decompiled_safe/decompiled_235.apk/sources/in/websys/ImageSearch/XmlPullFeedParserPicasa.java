package in.websys.ImageSearch;

import android.util.Log;
import android.util.Xml;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

public class XmlPullFeedParserPicasa extends BaseFeedParser {
    static final String CHANNEL = "channel";
    static final String DESCRIPTION = "description";
    static final String IMAGE = "content";
    static final String ITEM = "item";
    static final String LINK = "link";
    static final String PHOTO = "photo-url";
    static final String PHOTOSET = "photoset";
    static final String PHOTO_PARENT = "photo";
    static final String TAG = "tag";
    static final String THUMBNAIL = "thumbnail";
    static final String TITLE = "photo-caption";

    public XmlPullFeedParserPicasa(String feedUrl) {
        super(feedUrl);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public List<Message> parse() {
        Exception e;
        List<Message> list;
        List<Message> messages = null;
        int iPhotoCnt = 0;
        XmlPullParser parser = Xml.newPullParser();
        try {
            parser.setInput(getInputStream(), null);
            int eventType = parser.getEventType();
            Message currentMessage = null;
            boolean done = false;
            while (true) {
                list = messages;
                if (eventType == 1 || done) {
                    return list;
                }
                switch (eventType) {
                    case R.styleable.com_google_ads_AdView_adSize:
                        try {
                            messages = new ArrayList<>();
                            iPhotoCnt = 0;
                            break;
                        } catch (Exception e2) {
                            e = e2;
                            Log.e("ImageSearch::PullFeedParser", e.getMessage(), e);
                            throw new RuntimeException(e);
                        }
                    case R.styleable.com_google_ads_AdView_adUnitId:
                    default:
                        messages = list;
                        break;
                    case 2:
                        String name = parser.getName();
                        if (name.equalsIgnoreCase(ITEM)) {
                            currentMessage = new Message();
                            messages = list;
                            break;
                        } else if (name == LINK) {
                            if (currentMessage != null) {
                                currentMessage.setLink(parser.nextText());
                                messages = list;
                                break;
                            }
                            messages = list;
                            break;
                        } else {
                            if (currentMessage != null) {
                                if (!name.equalsIgnoreCase(IMAGE)) {
                                    if (name.equalsIgnoreCase(THUMBNAIL)) {
                                        String strText = parser.getAttributeValue(0);
                                        iPhotoCnt++;
                                        switch (iPhotoCnt) {
                                            case R.styleable.com_google_ads_AdView_adUnitId:
                                                currentMessage.setThumbnail(strText);
                                                messages = list;
                                                break;
                                            default:
                                                messages = list;
                                                break;
                                        }
                                    }
                                } else {
                                    currentMessage.setImage(parser.getAttributeValue(0));
                                    messages = list;
                                    break;
                                }
                            }
                            messages = list;
                        }
                    case 3:
                        String name2 = parser.getName();
                        if (name2.equalsIgnoreCase(ITEM) && currentMessage != null) {
                            list.add(currentMessage);
                            iPhotoCnt = 0;
                            messages = list;
                            break;
                        } else {
                            if (name2.equalsIgnoreCase(CHANNEL)) {
                                done = true;
                                messages = list;
                                break;
                            }
                            messages = list;
                            break;
                        }
                }
                eventType = parser.next();
            }
            return list;
        } catch (Exception e3) {
            e = e3;
            Log.e("ImageSearch::PullFeedParser", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
}
