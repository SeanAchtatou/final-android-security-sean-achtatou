package android.content.pm;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class e {

    /* renamed from: a  reason: collision with root package name */
    public int f31a = 1;
    private Class b = null;
    private Constructor c = null;
    private Method d = null;
    private Resources e = null;
    private AssetManager f = null;

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        if (this.f != null) {
            this.f.close();
        }
    }

    public PackageInfo a(String str, int i) {
        Object obj;
        ArrayList arrayList;
        this.f31a = 1;
        if (this.b == null) {
            this.b = Class.forName("android.content.pm.PackageParser");
        }
        if (this.c == null) {
            this.c = this.b.getConstructor(String.class);
        }
        if (this.d == null) {
            this.d = this.b.getDeclaredMethod("parsePackage", File.class, String.class, DisplayMetrics.class, Integer.TYPE);
        }
        Object newInstance = this.c.newInstance(str);
        Method method = this.b.getMethod("getParseError", new Class[0]);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        displayMetrics.setToDefaults();
        try {
            obj = this.d.invoke(newInstance, new File(str), str, displayMetrics, 0);
        } catch (Exception e2) {
            e2.printStackTrace();
            obj = null;
        }
        this.f31a = ((Integer) method.invoke(newInstance, new Object[0])).intValue();
        if (obj == null) {
            return null;
        }
        Class<?> cls = obj.getClass();
        Field declaredField = cls.getDeclaredField("applicationInfo");
        Field declaredField2 = cls.getDeclaredField("mVersionName");
        Field declaredField3 = cls.getDeclaredField("mVersionCode");
        ApplicationInfo applicationInfo = (ApplicationInfo) declaredField.get(obj);
        int i2 = declaredField3.getInt(obj);
        PackageInfo packageInfo = new PackageInfo();
        packageInfo.applicationInfo = applicationInfo;
        packageInfo.versionName = (String) declaredField2.get(obj);
        packageInfo.versionCode = i2;
        packageInfo.packageName = applicationInfo.packageName;
        if ((i & 4096) > 0 && (arrayList = (ArrayList) cls.getDeclaredField("requestedPermissions").get(obj)) != null) {
            int size = arrayList.size();
            if (size > 0) {
                packageInfo.requestedPermissions = new String[size];
            }
            for (int i3 = 0; i3 < size; i3++) {
                packageInfo.requestedPermissions[i3] = (String) arrayList.get(i3);
            }
        }
        return packageInfo;
    }

    public String a(Context context, String str, int i) {
        if (this.e == null) {
            a(context, str);
        }
        if (this.e == null) {
            return null;
        }
        return this.e.getString(i);
    }

    public void a() {
        if (this.e != null) {
            this.e = null;
        }
        if (this.f != null) {
            this.f.close();
            this.f = null;
        }
    }

    public Bitmap b(Context context, String str, int i) {
        if (this.e == null) {
            a(context, str);
        }
        if (this.e == null) {
            return null;
        }
        return ((BitmapDrawable) this.e.getDrawable(i)).getBitmap();
    }

    public byte[] c(Context context, String str, int i) {
        Bitmap b2 = b(context, str, i);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        b2.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        try {
            byteArrayOutputStream.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        b2.recycle();
        return byteArray;
    }

    private void a(Context context, String str) {
        if (this.e == null) {
            Resources resources = context.getResources();
            if (this.f == null) {
                this.f = AssetManager.class.newInstance();
                AssetManager.class.getDeclaredMethod("addAssetPath", String.class).invoke(this.f, str);
            }
            this.e = new Resources(this.f, resources.getDisplayMetrics(), resources.getConfiguration());
        }
    }

    public static Drawable d(Context context, String str, int i) {
        if (i == 0) {
            return null;
        }
        Resources resources = context.getResources();
        AssetManager newInstance = AssetManager.class.newInstance();
        AssetManager.class.getDeclaredMethod("addAssetPath", String.class).invoke(newInstance, str);
        return new Resources(newInstance, resources.getDisplayMetrics(), resources.getConfiguration()).getDrawable(i);
    }
}
