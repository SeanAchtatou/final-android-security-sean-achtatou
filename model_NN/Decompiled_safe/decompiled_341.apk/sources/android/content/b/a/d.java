package android.content.b.a;

import java.io.InputStream;

/* compiled from: ProGuard */
public class d {
    public static short a(InputStream inputStream) {
        byte[] bArr = new byte[2];
        if (inputStream.read(bArr) < 2) {
            throw new Exception("XXXX2");
        }
        return (short) ((bArr[0] & 255) | (bArr[1] << 8));
    }

    public static void a(InputStream inputStream, int i) {
        inputStream.skip((long) i);
    }

    public static int b(InputStream inputStream) {
        byte[] bArr = new byte[4];
        if (inputStream.read(bArr) < 4) {
            throw new Exception("XXXX4");
        }
        return (bArr[0] & 255) | ((bArr[3] & 255) << 24) | ((bArr[2] & 255) << 16) | ((bArr[1] & 255) << 8);
    }

    public static byte c(InputStream inputStream) {
        byte[] bArr = new byte[1];
        inputStream.read(bArr);
        return bArr[0];
    }

    public static int[] b(InputStream inputStream, int i) {
        int[] iArr = new int[i];
        for (int i2 = 0; i2 < i; i2++) {
            iArr[i2] = b(inputStream);
        }
        return iArr;
    }

    public static void a(InputStream inputStream, byte[] bArr) {
        inputStream.read(bArr);
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x0013  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.io.InputStream r3, int r4, boolean r5) {
        /*
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r0 = 16
            r1.<init>(r0)
        L_0x0007:
            int r0 = r4 + -1
            if (r4 == 0) goto L_0x0011
            short r2 = a(r3)
            if (r2 != 0) goto L_0x001d
        L_0x0011:
            if (r5 == 0) goto L_0x0018
            int r0 = r0 * 2
            a(r3, r0)
        L_0x0018:
            java.lang.String r0 = r1.toString()
            return r0
        L_0x001d:
            char r2 = (char) r2
            r1.append(r2)
            r4 = r0
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: android.content.b.a.d.a(java.io.InputStream, int, boolean):java.lang.String");
    }
}
