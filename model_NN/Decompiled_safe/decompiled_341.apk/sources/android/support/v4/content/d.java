package android.support.v4.content;

import android.os.Process;

/* compiled from: ProGuard */
class d extends h<Params, Result> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ModernAsyncTask f73a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    d(ModernAsyncTask modernAsyncTask) {
        super(null);
        this.f73a = modernAsyncTask;
    }

    public Result call() {
        this.f73a.i.set(true);
        Process.setThreadPriority(10);
        return this.f73a.d(this.f73a.a(this.b));
    }
}
