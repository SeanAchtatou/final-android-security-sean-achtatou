package android.support.v4.a;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
class d<T> implements Parcelable.ClassLoaderCreator<T> {

    /* renamed from: a  reason: collision with root package name */
    private final c<T> f37a;

    public d(c<T> cVar) {
        this.f37a = cVar;
    }

    public T createFromParcel(Parcel parcel) {
        return this.f37a.a(parcel, null);
    }

    public T createFromParcel(Parcel parcel, ClassLoader classLoader) {
        return this.f37a.a(parcel, classLoader);
    }

    public T[] newArray(int i) {
        return this.f37a.a(i);
    }
}
