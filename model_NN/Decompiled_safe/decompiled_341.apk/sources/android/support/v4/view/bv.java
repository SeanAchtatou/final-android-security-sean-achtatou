package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.a;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: ProGuard */
class bv extends AccessibilityDelegateCompat {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ViewPager f88a;

    bv(ViewPager viewPager) {
        this.f88a = viewPager;
    }

    public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(view, accessibilityEvent);
        accessibilityEvent.setClassName(ViewPager.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(View view, a aVar) {
        boolean z = true;
        super.onInitializeAccessibilityNodeInfo(view, aVar);
        aVar.a(ViewPager.class.getName());
        if (this.f88a.mAdapter == null || this.f88a.mAdapter.getCount() <= 1) {
            z = false;
        }
        aVar.a(z);
        if (this.f88a.mAdapter != null && this.f88a.mCurItem >= 0 && this.f88a.mCurItem < this.f88a.mAdapter.getCount() - 1) {
            aVar.a(4096);
        }
        if (this.f88a.mAdapter != null && this.f88a.mCurItem > 0 && this.f88a.mCurItem < this.f88a.mAdapter.getCount()) {
            aVar.a(8192);
        }
    }

    public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
        if (super.performAccessibilityAction(view, i, bundle)) {
            return true;
        }
        switch (i) {
            case 4096:
                if (this.f88a.mAdapter == null || this.f88a.mCurItem < 0 || this.f88a.mCurItem >= this.f88a.mAdapter.getCount() - 1) {
                    return false;
                }
                this.f88a.setCurrentItem(this.f88a.mCurItem + 1);
                return true;
            case 8192:
                if (this.f88a.mAdapter == null || this.f88a.mCurItem <= 0 || this.f88a.mCurItem >= this.f88a.mAdapter.getCount()) {
                    return false;
                }
                this.f88a.setCurrentItem(this.f88a.mCurItem - 1);
                return true;
            default:
                return false;
        }
    }
}
