package android.support.v4.view.a;

import android.os.Build;

/* compiled from: ProGuard */
public class o {

    /* renamed from: a  reason: collision with root package name */
    private static final r f80a;
    private final Object b;

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f80a = new s();
        } else if (Build.VERSION.SDK_INT >= 15) {
            f80a = new q();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f80a = new p();
        } else {
            f80a = new t();
        }
    }

    public o(Object obj) {
        this.b = obj;
    }

    public static o a() {
        return new o(f80a.a());
    }

    public void a(boolean z) {
        f80a.a(this.b, z);
    }

    public void a(int i) {
        f80a.b(this.b, i);
    }

    public void b(int i) {
        f80a.a(this.b, i);
    }

    public void c(int i) {
        f80a.c(this.b, i);
    }

    public int hashCode() {
        if (this.b == null) {
            return 0;
        }
        return this.b.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        o oVar = (o) obj;
        if (this.b == null) {
            if (oVar.b != null) {
                return false;
            }
            return true;
        } else if (!this.b.equals(oVar.b)) {
            return false;
        } else {
            return true;
        }
    }
}
