package android.support.v4.view;

import android.os.Build;
import android.view.MotionEvent;

/* compiled from: ProGuard */
public class MotionEventCompat {
    public static final int ACTION_HOVER_ENTER = 9;
    public static final int ACTION_HOVER_EXIT = 10;
    public static final int ACTION_HOVER_MOVE = 7;
    public static final int ACTION_MASK = 255;
    public static final int ACTION_POINTER_DOWN = 5;
    public static final int ACTION_POINTER_INDEX_MASK = 65280;
    public static final int ACTION_POINTER_INDEX_SHIFT = 8;
    public static final int ACTION_POINTER_UP = 6;
    public static final int ACTION_SCROLL = 8;
    static final aj IMPL;

    static {
        if (Build.VERSION.SDK_INT >= 5) {
            IMPL = new ai();
        } else {
            IMPL = new ah();
        }
    }

    public static int getActionMasked(MotionEvent motionEvent) {
        return motionEvent.getAction() & 255;
    }

    public static int getActionIndex(MotionEvent motionEvent) {
        return (motionEvent.getAction() & ACTION_POINTER_INDEX_MASK) >> 8;
    }

    public static int findPointerIndex(MotionEvent motionEvent, int i) {
        return IMPL.a(motionEvent, i);
    }

    public static int getPointerId(MotionEvent motionEvent, int i) {
        return IMPL.b(motionEvent, i);
    }

    public static float getX(MotionEvent motionEvent, int i) {
        return IMPL.c(motionEvent, i);
    }

    public static float getY(MotionEvent motionEvent, int i) {
        return IMPL.d(motionEvent, i);
    }

    public static int getPointerCount(MotionEvent motionEvent) {
        return IMPL.a(motionEvent);
    }
}
