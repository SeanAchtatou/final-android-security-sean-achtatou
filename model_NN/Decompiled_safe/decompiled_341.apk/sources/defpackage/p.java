package defpackage;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;

/* renamed from: p  reason: default package */
public final class p extends JceStruct implements Cloneable {
    static final /* synthetic */ boolean v = (!p.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    public String f3939a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public String d = Constants.STR_EMPTY;
    public String e = Constants.STR_EMPTY;
    public String f = Constants.STR_EMPTY;
    public String g = Constants.STR_EMPTY;
    public int h = 0;
    public String i = Constants.STR_EMPTY;
    public String j = Constants.STR_EMPTY;
    public String k = Constants.STR_EMPTY;
    public String l = Constants.STR_EMPTY;
    public int m = 0;
    public String n = Constants.STR_EMPTY;
    public int o = 0;
    public String p = Constants.STR_EMPTY;
    public short q = 0;
    public String r = Constants.STR_EMPTY;
    public String s = Constants.STR_EMPTY;
    public String t = Constants.STR_EMPTY;
    public String u = Constants.STR_EMPTY;

    public p() {
        a(this.f3939a);
        b(this.b);
        c(this.c);
        d(this.d);
        e(this.e);
        f(this.f);
        g(this.g);
        a(this.h);
        h(this.i);
        i(this.j);
        j(this.k);
        k(this.l);
        b(this.m);
        l(this.n);
        c(this.o);
        m(this.p);
        a(this.q);
        n(this.r);
        o(this.s);
        p(this.t);
        q(this.u);
    }

    public void a(int i2) {
        this.h = i2;
    }

    public void a(String str) {
        this.f3939a = str;
    }

    public void a(short s2) {
        this.q = s2;
    }

    public void b(int i2) {
        this.m = i2;
    }

    public void b(String str) {
        this.b = str;
    }

    public void c(int i2) {
        this.o = i2;
    }

    public void c(String str) {
        this.c = str;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (v) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void d(String str) {
        this.d = str;
    }

    public void display(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.display(this.f3939a, "lc");
        jceDisplayer.display(this.b, "name");
        jceDisplayer.display(this.c, "version");
        jceDisplayer.display(this.d, "imei");
        jceDisplayer.display(this.e, "imsi");
        jceDisplayer.display(this.f, "qq");
        jceDisplayer.display(this.g, "ip");
        jceDisplayer.display(this.h, SocialConstants.PARAM_TYPE);
        jceDisplayer.display(this.i, "osversion");
        jceDisplayer.display(this.j, "machineuid");
        jceDisplayer.display(this.k, "machineconf");
        jceDisplayer.display(this.l, "phone");
        jceDisplayer.display(this.m, "subplatform");
        jceDisplayer.display(this.n, "channelid");
        jceDisplayer.display(this.o, "isbuildin");
        jceDisplayer.display(this.p, "uuid");
        jceDisplayer.display(this.q, "lang");
        jceDisplayer.display(this.r, "guid");
        jceDisplayer.display(this.s, "sdk");
        jceDisplayer.display(this.t, "sid");
        jceDisplayer.display(this.u, "newguid");
    }

    public void e(String str) {
        this.e = str;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        p pVar = (p) obj;
        return JceUtil.equals(this.f3939a, pVar.f3939a) && JceUtil.equals(this.b, pVar.b) && JceUtil.equals(this.c, pVar.c) && JceUtil.equals(this.d, pVar.d) && JceUtil.equals(this.e, pVar.e) && JceUtil.equals(this.f, pVar.f) && JceUtil.equals(this.g, pVar.g) && JceUtil.equals(this.h, pVar.h) && JceUtil.equals(this.i, pVar.i) && JceUtil.equals(this.j, pVar.j) && JceUtil.equals(this.k, pVar.k) && JceUtil.equals(this.l, pVar.l) && JceUtil.equals(this.m, pVar.m) && JceUtil.equals(this.n, pVar.n) && JceUtil.equals(this.o, pVar.o) && JceUtil.equals(this.p, pVar.p) && JceUtil.equals(this.q, pVar.q) && JceUtil.equals(this.r, pVar.r) && JceUtil.equals(this.s, pVar.s) && JceUtil.equals(this.t, pVar.t) && JceUtil.equals(this.u, pVar.u);
    }

    public void f(String str) {
        this.f = str;
    }

    public void g(String str) {
        this.g = str;
    }

    public void h(String str) {
        this.i = str;
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public void i(String str) {
        this.j = str;
    }

    public void j(String str) {
        this.k = str;
    }

    public void k(String str) {
        this.l = str;
    }

    public void l(String str) {
        this.n = str;
    }

    public void m(String str) {
        this.p = str;
    }

    public void n(String str) {
        this.r = str;
    }

    public void o(String str) {
        this.s = str;
    }

    public void p(String str) {
        this.t = str;
    }

    public void q(String str) {
        this.u = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
     arg types: [short, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short */
    public void readFrom(JceInputStream jceInputStream) {
        a(jceInputStream.readString(0, true));
        b(jceInputStream.readString(1, true));
        c(jceInputStream.readString(2, true));
        d(jceInputStream.readString(3, true));
        e(jceInputStream.readString(4, true));
        f(jceInputStream.readString(5, false));
        g(jceInputStream.readString(6, false));
        a(jceInputStream.read(this.h, 7, false));
        h(jceInputStream.readString(8, false));
        i(jceInputStream.readString(9, false));
        j(jceInputStream.readString(10, false));
        k(jceInputStream.readString(11, false));
        b(jceInputStream.read(this.m, 12, false));
        l(jceInputStream.readString(13, false));
        c(jceInputStream.read(this.o, 14, false));
        m(jceInputStream.readString(15, false));
        a(jceInputStream.read(this.q, 16, false));
        n(jceInputStream.readString(17, false));
        o(jceInputStream.readString(18, false));
        p(jceInputStream.readString(19, false));
        q(jceInputStream.readString(20, false));
    }

    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.f3939a, 0);
        jceOutputStream.write(this.b, 1);
        jceOutputStream.write(this.c, 2);
        jceOutputStream.write(this.d, 3);
        jceOutputStream.write(this.e, 4);
        if (this.f != null) {
            jceOutputStream.write(this.f, 5);
        }
        if (this.g != null) {
            jceOutputStream.write(this.g, 6);
        }
        jceOutputStream.write(this.h, 7);
        if (this.i != null) {
            jceOutputStream.write(this.i, 8);
        }
        if (this.j != null) {
            jceOutputStream.write(this.j, 9);
        }
        if (this.k != null) {
            jceOutputStream.write(this.k, 10);
        }
        if (this.l != null) {
            jceOutputStream.write(this.l, 11);
        }
        jceOutputStream.write(this.m, 12);
        if (this.n != null) {
            jceOutputStream.write(this.n, 13);
        }
        jceOutputStream.write(this.o, 14);
        if (this.p != null) {
            jceOutputStream.write(this.p, 15);
        }
        jceOutputStream.write(this.q, 16);
        if (this.r != null) {
            jceOutputStream.write(this.r, 17);
        }
        if (this.s != null) {
            jceOutputStream.write(this.s, 18);
        }
        if (this.t != null) {
            jceOutputStream.write(this.t, 19);
        }
        if (this.u != null) {
            jceOutputStream.write(this.u, 20);
        }
    }
}
