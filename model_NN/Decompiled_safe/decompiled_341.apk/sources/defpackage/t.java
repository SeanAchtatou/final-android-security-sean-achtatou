package defpackage;

import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import com.tencent.connect.common.Constants;

/* renamed from: t  reason: default package */
public final class t extends JceStruct implements Cloneable {
    static final /* synthetic */ boolean e = (!t.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    public String f3943a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public int c = 0;
    public int d = 0;

    public t() {
        a(this.f3943a);
        b(this.b);
        a(this.c);
        b(this.d);
    }

    public String a() {
        return this.f3943a;
    }

    public void a(int i) {
        this.c = i;
    }

    public void a(String str) {
        this.f3943a = str;
    }

    public String b() {
        return this.b;
    }

    public void b(int i) {
        this.d = i;
    }

    public void b(String str) {
        this.b = str;
    }

    public int c() {
        return this.c;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (e) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public int d() {
        return this.d;
    }

    public void display(StringBuilder sb, int i) {
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        t tVar = (t) obj;
        return JceUtil.equals(this.f3943a, tVar.f3943a) && JceUtil.equals(this.b, tVar.b) && JceUtil.equals(this.c, tVar.c) && JceUtil.equals(this.d, tVar.d);
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    public void readFrom(JceInputStream jceInputStream) {
        a(jceInputStream.readString(0, true));
        b(jceInputStream.readString(1, true));
        a(jceInputStream.read(this.c, 2, true));
        b(jceInputStream.read(this.d, 3, true));
    }

    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.f3943a, 0);
        jceOutputStream.write(this.b, 1);
        jceOutputStream.write(this.c, 2);
        jceOutputStream.write(this.d, 3);
    }
}
