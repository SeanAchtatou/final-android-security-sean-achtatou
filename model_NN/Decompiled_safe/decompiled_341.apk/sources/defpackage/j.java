package defpackage;

import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import com.tencent.connect.common.Constants;

/* renamed from: j  reason: default package */
public final class j extends JceStruct implements Cloneable {
    static final /* synthetic */ boolean b = (!j.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    public String f3933a = Constants.STR_EMPTY;

    public j() {
        a(this.f3933a);
    }

    public String a() {
        return this.f3933a;
    }

    public void a(String str) {
        this.f3933a = str;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            if (b) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void display(StringBuilder sb, int i) {
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        return JceUtil.equals(this.f3933a, ((j) obj).f3933a);
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void readFrom(JceInputStream jceInputStream) {
        a(jceInputStream.readString(0, true));
    }

    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.f3933a, 0);
    }
}
