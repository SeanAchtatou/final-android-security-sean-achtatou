package defpackage;

import android.content.Context;
import android.content.SharedPreferences;
import com.tencent.securemodule.service.ProductInfo;

/* renamed from: as  reason: default package */
public class as {
    public static int a(Context context, int i, int i2) {
        return context.getSharedPreferences("secure_config", 0).getInt(a(i), i2);
    }

    private static String a(int i) {
        return String.format("%5d", Integer.valueOf(i));
    }

    public static String a(Context context, int i, String str) {
        return context.getSharedPreferences("secure_config", 0).getString(a(i), str);
    }

    public static void a(Context context, ProductInfo productInfo) {
        SharedPreferences.Editor edit = context.getSharedPreferences("secure_config", 0).edit();
        edit.putInt(a(30003), productInfo.getProductId());
        edit.putString(a(30001), productInfo.getVersion());
        edit.putInt(a(30004), productInfo.getBuildNo());
        edit.putInt(a(30005), productInfo.getSubPlatformId());
        edit.putString(a(30002), productInfo.getChannelId());
        edit.putString(a(30006), productInfo.getQq());
        edit.commit();
    }

    public static boolean a(Context context, int i, boolean z) {
        return context.getSharedPreferences("secure_config", 0).getBoolean(a(i), z);
    }

    public static void b(Context context, int i, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences("secure_config", 0).edit();
        edit.putString(a(i), str);
        edit.commit();
    }

    public static void b(Context context, int i, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences("secure_config", 0).edit();
        edit.putBoolean(a(i), z);
        edit.commit();
    }
}
