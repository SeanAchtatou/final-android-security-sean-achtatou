package defpackage;

import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import com.tencent.connect.common.Constants;

/* renamed from: b  reason: default package */
public final class b extends JceStruct implements Cloneable {
    static final /* synthetic */ boolean e = (!b.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    public int f109a = 0;
    public int b = 0;
    public String c = Constants.STR_EMPTY;
    public String d = Constants.STR_EMPTY;

    public b() {
        a(this.f109a);
        b(this.b);
        a(this.c);
        b(this.d);
    }

    public b(int i, int i2, String str, String str2) {
        a(i);
        b(i2);
        a(str);
        b(str2);
    }

    public void a(int i) {
        this.f109a = i;
    }

    public void a(String str) {
        this.c = str;
    }

    public void b(int i) {
        this.b = i;
    }

    public void b(String str) {
        this.d = str;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (e) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void display(StringBuilder sb, int i) {
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        b bVar = (b) obj;
        return JceUtil.equals(this.f109a, bVar.f109a) && JceUtil.equals(this.b, bVar.b) && JceUtil.equals(this.c, bVar.c) && JceUtil.equals(this.d, bVar.d);
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    public void readFrom(JceInputStream jceInputStream) {
        a(jceInputStream.read(this.f109a, 0, true));
        b(jceInputStream.read(this.b, 1, true));
        a(jceInputStream.readString(2, false));
        b(jceInputStream.readString(3, false));
    }

    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.f109a, 0);
        jceOutputStream.write(this.b, 1);
        if (this.c != null) {
            jceOutputStream.write(this.c, 2);
        }
        if (this.d != null) {
            jceOutputStream.write(this.d, 3);
        }
    }
}
