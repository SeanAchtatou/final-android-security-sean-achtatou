package com.qq.c;

import com.tencent.connect.common.Constants;
import java.io.File;

/* compiled from: ProGuard */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static String f265a = null;
    private static String b = null;
    private static String c = null;
    private static String d = null;

    public static String a() {
        if (f265a == null || f265a.length() == 0) {
            f265a = c();
        }
        return f265a;
    }

    public static String b() {
        if (c == null || c.length() == 0) {
            c = d();
        }
        return c;
    }

    private static String c() {
        if (new File("/system/bin/ls").exists()) {
            return "/system/bin/ls";
        }
        if (new File("/system/xbin/ls").exists()) {
            return "/system/xbin/ls";
        }
        if (!new File("/system/xbin/busybox").exists()) {
            return Constants.STR_EMPTY;
        }
        return "/system/xbin/busybox" + " ls ";
    }

    private static String d() {
        if (new File("/system/bin/chmod").exists()) {
            return "/system/bin/chmod";
        }
        if (new File("/system/xbin/chmod").exists()) {
            return "/system/xbin/chmod";
        }
        if (!new File("/system/xbin/busybox").exists()) {
            return Constants.STR_EMPTY;
        }
        return "/system/xbin/busybox" + " chmod ";
    }
}
