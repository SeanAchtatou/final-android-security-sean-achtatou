package com.qq.h;

import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.wcs.jce.ServerInfo;
import com.tencent.wcs.jce.ServerList;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class b implements NetworkMonitor.ConnectivityChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private static b f295a;
    private a b = new a();
    private ArrayList<ServerInfo> c = new ArrayList<>();
    private ArrayList<ServerInfo> d = new ArrayList<>();
    private int e = 0;
    private int f = 0;
    private int g = 0;

    private b() {
        m();
        l();
        cq.a().a(this);
    }

    private void l() {
        if (!this.d.isEmpty()) {
            this.d.clear();
        }
        Iterator<String> it = this.b.b.iterator();
        while (it.hasNext()) {
            String next = it.next();
            ServerInfo serverInfo = new ServerInfo();
            serverInfo.f3883a = next.substring(0, next.indexOf(58));
            serverInfo.b = Integer.parseInt(next.substring(next.indexOf(58) + 1));
            this.d.add(serverInfo);
        }
    }

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            if (f295a == null) {
                f295a = new b();
            }
            bVar = f295a;
        }
        return bVar;
    }

    public boolean b() {
        if (!this.c.isEmpty()) {
            return true;
        }
        return false;
    }

    private void m() {
        ServerList serverList = null;
        try {
            serverList = i();
        } catch (Throwable th) {
            th.printStackTrace();
        }
        if (serverList != null && serverList.f3884a != null && !serverList.f3884a.isEmpty()) {
            if (!this.c.isEmpty()) {
                this.c.clear();
            }
            ArrayList<ServerInfo> arrayList = serverList.f3884a;
            if (arrayList != null && !arrayList.isEmpty()) {
                this.c.addAll(arrayList);
            }
            ArrayList<ServerInfo> arrayList2 = serverList.b;
            if (arrayList2 != null && !arrayList2.isEmpty()) {
                this.c.addAll(arrayList2);
            }
            ArrayList<ServerInfo> arrayList3 = serverList.c;
            if (arrayList3 != null && !arrayList3.isEmpty()) {
                this.c.addAll(arrayList3);
            }
            this.e = 0;
        }
    }

    public synchronized void a(ServerList serverList) {
        if (serverList != null) {
            b(serverList);
            m();
        }
    }

    public ServerInfo c() {
        if (this.f < this.d.size()) {
            return this.d.get(this.f);
        }
        d();
        return null;
    }

    public void d() {
        this.f = 0;
    }

    public void e() {
        this.f++;
    }

    public String f() {
        ServerInfo g2 = g();
        if (g2 == null) {
            g2 = n();
        }
        return g2.f3883a + ":" + g2.b;
    }

    private ServerInfo n() {
        if (this.g >= this.d.size()) {
            this.g = 0;
        }
        ServerInfo serverInfo = this.d.get(this.g);
        this.g++;
        return serverInfo;
    }

    public ServerInfo g() {
        if (this.c.isEmpty()) {
            c.a();
            return null;
        }
        if (this.e >= this.c.size()) {
            this.e = 0;
        }
        return this.c.get(this.e);
    }

    public void h() {
        this.e++;
    }

    public void b(ServerList serverList) {
        if (serverList != null && serverList.f3884a != null && !serverList.f3884a.isEmpty()) {
            as.w().a(serverList);
        }
    }

    public ServerList i() {
        return as.w().x();
    }

    public void onConnected(APN apn) {
        if (apn == APN.WIFI && !c.k() && !d.f296a) {
            a().d();
            TemporaryThreadManager.get().start(new d());
        }
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        if (apn != APN.WIFI && apn2 == APN.WIFI && !c.k() && !d.f296a) {
            a().d();
            TemporaryThreadManager.get().start(new d());
        }
    }

    public String j() {
        if (!a.f294a) {
            return "http://112.90.140.215:10001/";
        }
        return "http://" + a().f() + "/";
    }

    public void k() {
        if (a.f294a) {
            a().h();
        }
    }
}
