package com.qq.a.a;

import android.net.Uri;

/* compiled from: ProGuard */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    public static final Uri f259a = Uri.parse("content://sms/");
    public static final Uri b = Uri.parse("content://sms/inbox");
    public static final Uri c = Uri.parse("content://sms/sent");
    public static final Uri d = Uri.parse("content://sms/draft");
    public static final Uri e = Uri.parse("content://sms/outbox");
    public static final Uri f = Uri.parse("content://sms/failed");
    public static final Uri g = Uri.parse("content://sms/queued");
}
