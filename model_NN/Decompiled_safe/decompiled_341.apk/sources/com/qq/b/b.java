package com.qq.b;

import com.qq.AppService.AuthorReceiver;
import com.qq.AppService.s;
import java.util.ArrayList;

/* compiled from: ProGuard */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    public int f262a = 0;
    public String b = null;
    public int c = 0;
    public ArrayList<String> d = null;

    public void a(String str) {
        if (this.d == null) {
            this.d = new ArrayList<>();
        }
        this.d.add(str);
    }

    public static b a(int i) {
        b bVar = new b();
        bVar.f262a = i;
        return bVar;
    }

    public static b b(int i) {
        b bVar = new b();
        bVar.f262a = 1;
        bVar.b = AuthorReceiver.b(i);
        return bVar;
    }

    public static byte[] a(b bVar) {
        if (bVar == null) {
            return null;
        }
        if (bVar.f262a == 1 || bVar.f262a == 7) {
            byte[] a2 = s.a(bVar.b);
            int length = a2.length + 8;
            byte[] bArr = new byte[(length + 4)];
            System.arraycopy(s.a(length), 0, bArr, 0, 4);
            System.arraycopy(s.a(bVar.f262a), 0, bArr, 4, 4);
            System.arraycopy(s.a(a2.length), 0, bArr, 8, 4);
            System.arraycopy(a2, 0, bArr, 12, a2.length);
            return bArr;
        } else if (bVar.f262a == 2 || bVar.f262a == 10 || bVar.f262a == 11 || bVar.f262a == 2 || bVar.f262a == 12) {
            byte[] bArr2 = new byte[8];
            System.arraycopy(s.a(4), 0, bArr2, 0, 4);
            System.arraycopy(s.a(bVar.f262a), 0, bArr2, 4, 4);
            return bArr2;
        } else if (bVar.f262a == 3 || bVar.f262a == 5 || bVar.f262a == 4 || bVar.f262a == 6 || bVar.f262a == 107 || bVar.f262a == 108) {
            byte[] a3 = s.a(bVar.b);
            int length2 = a3.length + 8;
            ArrayList arrayList = new ArrayList();
            if (bVar.d != null) {
                int size = bVar.d.size();
                int i = length2;
                for (int i2 = 0; i2 < size; i2++) {
                    byte[] a4 = s.a(bVar.d.get(i2));
                    arrayList.add(a4);
                    i = i + 4 + a4.length;
                }
                length2 = i;
            }
            byte[] bArr3 = new byte[(length2 + 4)];
            System.arraycopy(s.a(length2), 0, bArr3, 0, 4);
            System.arraycopy(s.a(bVar.f262a), 0, bArr3, 4, 4);
            System.arraycopy(s.a(a3.length), 0, bArr3, 8, 4);
            System.arraycopy(a3, 0, bArr3, 12, a3.length);
            int size2 = arrayList.size();
            int length3 = a3.length + 12;
            for (int i3 = 0; i3 < size2; i3++) {
                byte[] bArr4 = (byte[]) arrayList.get(i3);
                System.arraycopy(s.a(bArr4.length), 0, bArr3, length3, 4);
                int i4 = length3 + 4;
                System.arraycopy(bArr4, 0, bArr3, i4, bArr4.length);
                length3 = i4 + bArr4.length;
            }
            return bArr3;
        } else if (bVar.f262a != 103 && bVar.f262a != 104 && bVar.f262a != 105 && bVar.f262a != 106 && bVar.f262a != 111) {
            return null;
        } else {
            byte[] a5 = s.a(bVar.b);
            int length4 = a5.length + 8 + 4;
            ArrayList arrayList2 = new ArrayList();
            if (bVar.d != null) {
                int size3 = bVar.d.size();
                int i5 = length4;
                for (int i6 = 0; i6 < size3; i6++) {
                    byte[] a6 = s.a(bVar.d.get(i6));
                    arrayList2.add(a6);
                    i5 = i5 + 4 + a6.length;
                }
                length4 = i5;
            }
            byte[] bArr5 = new byte[(length4 + 4)];
            System.arraycopy(s.a(length4), 0, bArr5, 0, 4);
            System.arraycopy(s.a(bVar.f262a), 0, bArr5, 4, 4);
            System.arraycopy(s.a(bVar.c), 0, bArr5, 8, 4);
            System.arraycopy(s.a(a5.length), 0, bArr5, 12, 4);
            System.arraycopy(a5, 0, bArr5, 16, a5.length);
            int size4 = arrayList2.size();
            int length5 = a5.length + 16;
            for (int i7 = 0; i7 < size4; i7++) {
                byte[] bArr6 = (byte[]) arrayList2.get(i7);
                System.arraycopy(s.a(bArr6.length), 0, bArr5, length5, 4);
                int i8 = length5 + 4;
                System.arraycopy(bArr6, 0, bArr5, i8, bArr6.length);
                length5 = i8 + bArr6.length;
            }
            return bArr5;
        }
    }
}
