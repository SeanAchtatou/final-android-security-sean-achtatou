package com.qq.j;

import android.content.Context;
import android.util.Log;
import com.qq.util.x;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static Class f299a = null;

    public static long a(Context context, String str) {
        HashSet hashSet = new HashSet();
        hashSet.add(str);
        return a(context, hashSet);
    }

    public static long a(Context context, Set<String> set) {
        long j;
        boolean z;
        try {
            j = x.a(context, set);
            z = false;
        } catch (Exception e) {
            e.printStackTrace();
            j = 0;
            z = true;
        }
        if (z) {
            j = b(context, set);
            if (j == -1) {
                throw new IllegalArgumentException("Unable to find or allocate a thread ID.");
            }
        }
        return j;
    }

    public static long b(Context context, Set<String> set) {
        Class<?> cls;
        Method method;
        Object obj;
        if (f299a == null) {
            try {
                cls = Class.forName("android.provider.Telephony$Threads");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                cls = null;
            }
            if (cls == null) {
                Log.d("com.qq.connect", "could not find android.provider.Telephony$Threads");
                return -1;
            }
            f299a = cls;
        } else {
            cls = f299a;
        }
        try {
            method = cls.getMethod("getOrCreateThreadId", Context.class, Set.class);
        } catch (SecurityException e2) {
            e2.printStackTrace();
            method = null;
        } catch (NoSuchMethodException e3) {
            e3.printStackTrace();
            method = null;
        }
        if (method == null) {
            Log.d("com.qq.connect", " not method getOrCreateThreadId");
            return -1;
        }
        try {
            obj = method.invoke(null, context, set);
        } catch (IllegalArgumentException e4) {
            e4.printStackTrace();
            obj = null;
        } catch (IllegalAccessException e5) {
            e5.printStackTrace();
            obj = null;
        } catch (InvocationTargetException e6) {
            e6.printStackTrace();
            obj = null;
        }
        if (obj != null) {
            return ((Long) obj).longValue();
        }
        Log.d("com.qq.connect", "android.provider.Telephony$Threads.getOrCreateThreadId == null");
        return -1;
    }
}
