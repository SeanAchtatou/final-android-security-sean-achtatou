package com.qq.m;

import android.content.Context;
import com.qq.AppService.AppService;
import com.qq.i.a;
import com.tencent.wcs.jce.PingPCRequest;

/* compiled from: ProGuard */
public class d extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private a f314a = null;
    private volatile boolean b = false;
    private Context c = null;
    private PingPCRequest d = null;
    private e e = null;
    private String f = null;

    public d(Context context, e eVar) {
        this.c = context;
        this.d = new PingPCRequest();
        this.d.a(AppService.v());
        this.d.a((short) 13092);
        this.d.c("random" + System.currentTimeMillis());
        this.e = eVar;
    }

    public d(Context context, long j, e eVar) {
        this.c = context;
        this.d = new PingPCRequest();
        this.d.a(AppService.v());
        this.d.a((short) 13092);
        this.d.c("random" + System.currentTimeMillis());
        this.d.a(j);
        this.e = eVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ba  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r14 = this;
            r13 = 4
            r0 = 1
            r6 = 0
            r9 = 0
            super.run()
            r14.b = r0
            java.lang.String r0 = "com.qq.connect"
            java.lang.String r1 = "PingPCThread Thread start"
            android.util.Log.d(r0, r1)
            android.content.Context r0 = r14.c
            java.lang.String r1 = "connectivity"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0
            android.net.NetworkInfo r11 = r0.getActiveNetworkInfo()
            if (r11 == 0) goto L_0x0026
            boolean r0 = r11.isConnected()
            if (r0 != 0) goto L_0x0028
        L_0x0026:
            r14.b = r6
        L_0x0028:
            boolean r0 = r14.b
            if (r0 != 0) goto L_0x0033
            java.lang.String r0 = "com.qq.connect"
            java.lang.String r1 = "PingPCThread Thread check network failed::"
            android.util.Log.d(r0, r1)
        L_0x0033:
            com.tencent.assistant.st.a r0 = com.tencent.assistant.st.a.a()     // Catch:{ Throwable -> 0x024b }
            r0.b()     // Catch:{ Throwable -> 0x024b }
            r10 = r6
            r7 = r6
            r8 = r9
        L_0x003d:
            if (r10 > 0) goto L_0x00ac
            boolean r0 = r14.b     // Catch:{ Throwable -> 0x01f9 }
            if (r0 == 0) goto L_0x00ac
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f9 }
            if (r0 == 0) goto L_0x004c
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f9 }
            r0.f()     // Catch:{ Throwable -> 0x01f9 }
        L_0x004c:
            com.qq.h.b r0 = com.qq.h.b.a()     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r1 = r0.j()     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r0 = "com.qq.connect"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01f9 }
            r2.<init>()     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r3 = "PingPCThread url: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x01f9 }
            android.util.Log.d(r0, r2)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01f9 }
            r0.<init>()     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r2 = "PingPCThread url: "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x01f9 }
            com.tencent.wcs.c.b.a(r0)     // Catch:{ Throwable -> 0x01f9 }
            com.qq.i.a r0 = new com.qq.i.a     // Catch:{ Throwable -> 0x01f9 }
            r2 = 1
            r3 = 0
            r4 = 0
            r5 = 0
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x01f9 }
            r14.f314a = r0     // Catch:{ Throwable -> 0x01f9 }
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f9 }
            if (r0 == 0) goto L_0x00ac
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f9 }
            r1 = 10000(0x2710, float:1.4013E-41)
            r0.a(r1)     // Catch:{ Throwable -> 0x01f9 }
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f9 }
            r1 = 15000(0x3a98, float:2.102E-41)
            r0.b(r1)     // Catch:{ Throwable -> 0x01f9 }
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f9 }
            r0.b()     // Catch:{ Throwable -> 0x01f9 }
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f9 }
            if (r0 == 0) goto L_0x00ac
            boolean r0 = r14.b     // Catch:{ Throwable -> 0x01f9 }
            if (r0 != 0) goto L_0x00c0
        L_0x00ac:
            com.tencent.assistant.st.a r0 = com.tencent.assistant.st.a.a()
            r0.c()
            r0 = r6
            r1 = r7
            r2 = r8
        L_0x00b6:
            com.qq.m.e r3 = r14.e
            if (r3 == 0) goto L_0x00bf
            com.qq.m.e r3 = r14.e
            r3.a(r1, r0, r2)
        L_0x00bf:
            return
        L_0x00c0:
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f9 }
            java.net.InetAddress r0 = r0.e()     // Catch:{ Throwable -> 0x01f9 }
            if (r0 == 0) goto L_0x00ac
            com.tencent.wcs.jce.PingPCRequest r1 = r14.d     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r2 = r0.getHostAddress()     // Catch:{ Throwable -> 0x01f9 }
            r1.b(r2)     // Catch:{ Throwable -> 0x01f9 }
            com.qq.g.a r1 = new com.qq.g.a     // Catch:{ Throwable -> 0x01f9 }
            r1.<init>()     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r2 = com.qq.AppService.AppService.i     // Catch:{ Throwable -> 0x01f9 }
            r1.n = r2     // Catch:{ Throwable -> 0x01f9 }
            android.content.Context r2 = r14.c     // Catch:{ Throwable -> 0x01f9 }
            r1.a(r2)     // Catch:{ Throwable -> 0x01f9 }
            r1.a(r11)     // Catch:{ Throwable -> 0x01f9 }
            android.content.Context r2 = r14.c     // Catch:{ Throwable -> 0x01f9 }
            r1.a(r2, r0)     // Catch:{ Throwable -> 0x01f9 }
            byte[] r0 = com.qq.g.a.a(r1)     // Catch:{ Throwable -> 0x01f9 }
            com.tencent.wcs.jce.PingPCRequest r2 = r14.d     // Catch:{ Throwable -> 0x01f9 }
            r2.a(r0)     // Catch:{ Throwable -> 0x01f9 }
            com.tencent.wcs.jce.PingPCRequest r0 = r14.d     // Catch:{ Throwable -> 0x01f9 }
            byte[] r0 = r0.toByteArray()     // Catch:{ Throwable -> 0x01f9 }
            r2 = 1007(0x3ef, float:1.411E-42)
            byte[] r2 = com.qq.AppService.s.a(r2)     // Catch:{ Throwable -> 0x01f9 }
            byte[] r3 = com.qq.AppService.s.a(r2, r0)     // Catch:{ Throwable -> 0x01f9 }
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f9 }
            if (r0 == 0) goto L_0x0109
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f9 }
            r0.a(r3)     // Catch:{ Throwable -> 0x01f9 }
        L_0x0109:
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f9 }
            if (r0 == 0) goto L_0x0116
            boolean r0 = r14.b     // Catch:{ Throwable -> 0x01f9 }
            if (r0 == 0) goto L_0x0116
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f3 }
            r0.c()     // Catch:{ Throwable -> 0x01f3 }
        L_0x0116:
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f9 }
            if (r0 == 0) goto L_0x0158
            boolean r0 = r14.b     // Catch:{ Throwable -> 0x01f9 }
            if (r0 == 0) goto L_0x0158
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x020a }
            r2 = 1
            byte[] r0 = r0.a(r2)     // Catch:{ Throwable -> 0x020a }
        L_0x0125:
            if (r0 == 0) goto L_0x0158
            int r2 = r0.length     // Catch:{ Throwable -> 0x01f9 }
            if (r2 <= r13) goto L_0x0158
            int r2 = com.qq.AppService.s.a(r0)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r4 = "com.qq.connect"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01f9 }
            r5.<init>()     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r12 = "PingPCThread ...."
            java.lang.StringBuilder r5 = r5.append(r12)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x01f9 }
            android.util.Log.d(r4, r2)     // Catch:{ Throwable -> 0x01f9 }
            com.qq.taf.jce.JceInputStream r4 = new com.qq.taf.jce.JceInputStream     // Catch:{ Throwable -> 0x01f9 }
            r2 = 4
            r4.<init>(r0, r2)     // Catch:{ Throwable -> 0x01f9 }
            com.tencent.wcs.jce.PingPCSResponse r2 = new com.tencent.wcs.jce.PingPCSResponse     // Catch:{ Throwable -> 0x01f9 }
            r2.<init>()     // Catch:{ Throwable -> 0x01f9 }
            r0 = -1
            r2.b = r0     // Catch:{ Throwable -> 0x0251 }
            r2.readFrom(r4)     // Catch:{ Exception -> 0x0211 }
            r8 = r2
        L_0x0158:
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f9 }
            int r7 = r0.d()     // Catch:{ Throwable -> 0x01f9 }
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f9 }
            int r6 = r0.a()     // Catch:{ Throwable -> 0x01f9 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01f9 }
            r0.<init>()     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r2 = "PingPCThread response "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r2 = " err "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r2 = " response "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x01f9 }
            com.tencent.wcs.c.b.a(r0)     // Catch:{ Throwable -> 0x01f9 }
            if (r8 == 0) goto L_0x0218
            if (r3 == 0) goto L_0x0218
            java.lang.String r0 = "com.qq.connect"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01f9 }
            r2.<init>()     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r4 = "PingPCThread response: "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r4 = " err: "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r4 = " postData: "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Throwable -> 0x01f9 }
            int r3 = r3.length     // Catch:{ Throwable -> 0x01f9 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r3 = " obj.password: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r1 = r1.h     // Catch:{ Throwable -> 0x01f9 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r2 = " response ... "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x01f9 }
            int r2 = r8.b     // Catch:{ Throwable -> 0x01f9 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r2 = " PCList: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x01f9 }
            java.util.ArrayList r2 = r8.a()     // Catch:{ Throwable -> 0x01f9 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x01f9 }
            android.util.Log.d(r0, r1)     // Catch:{ Throwable -> 0x01f9 }
        L_0x01e5:
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f9 }
            if (r0 == 0) goto L_0x01ee
            com.qq.i.a r0 = r14.f314a     // Catch:{ Throwable -> 0x01f9 }
            r0.f()     // Catch:{ Throwable -> 0x01f9 }
        L_0x01ee:
            int r0 = r10 + 1
            r10 = r0
            goto L_0x003d
        L_0x01f3:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x01f9 }
            goto L_0x0116
        L_0x01f9:
            r0 = move-exception
            r3 = r0
            r1 = r7
            r2 = r8
            r0 = r6
        L_0x01fe:
            r3.printStackTrace()     // Catch:{ all -> 0x0242 }
            com.tencent.assistant.st.a r3 = com.tencent.assistant.st.a.a()
            r3.c()
            goto L_0x00b6
        L_0x020a:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x01f9 }
            r0 = r9
            goto L_0x0125
        L_0x0211:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x0251 }
            r8 = r2
            goto L_0x0158
        L_0x0218:
            java.lang.String r0 = "com.qq.connect"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01f9 }
            r1.<init>()     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r2 = "PingPCThread response:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r2 = " err:"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Throwable -> 0x01f9 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x01f9 }
            android.util.Log.d(r0, r1)     // Catch:{ Throwable -> 0x01f9 }
            com.qq.h.b r0 = com.qq.h.b.a()     // Catch:{ Throwable -> 0x01f9 }
            r0.k()     // Catch:{ Throwable -> 0x01f9 }
            goto L_0x01e5
        L_0x0242:
            r0 = move-exception
            com.tencent.assistant.st.a r1 = com.tencent.assistant.st.a.a()
            r1.c()
            throw r0
        L_0x024b:
            r0 = move-exception
            r3 = r0
            r1 = r6
            r2 = r9
            r0 = r6
            goto L_0x01fe
        L_0x0251:
            r0 = move-exception
            r3 = r0
            r1 = r7
            r0 = r6
            goto L_0x01fe
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.m.d.run():void");
    }
}
