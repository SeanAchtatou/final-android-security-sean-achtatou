package com.qq.m;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.pm.e;
import android.os.Environment;
import android.util.Log;
import com.qq.AppService.AstApp;
import com.qq.AppService.s;
import com.qq.ndk.FileNative;
import com.qq.ndk.Native;
import com.qq.ndk.a;
import com.qq.provider.n;
import com.qq.util.aa;
import com.qq.util.m;
import com.tencent.connect.common.Constants;
import java.io.File;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

/* compiled from: ProGuard */
public final class h extends Thread implements a {

    /* renamed from: a  reason: collision with root package name */
    int f316a = 0;
    int b = 0;
    private String c = null;
    private i d = null;
    private ArrayList<String> e = null;
    /* access modifiers changed from: private */
    public long f = 0;
    private ArrayList<j> g = null;

    public static String a(PackageInfo packageInfo) {
        Signature[] signatureArr;
        X509Certificate a2;
        byte[] bArr;
        if (packageInfo == null || (signatureArr = packageInfo.signatures) == null || signatureArr.length <= 0 || signatureArr[0] == null || (a2 = aa.a(signatureArr[0])) == null) {
            return null;
        }
        try {
            bArr = a2.getEncoded();
        } catch (CertificateEncodingException e2) {
            e2.printStackTrace();
            bArr = null;
        }
        if (bArr != null) {
            return m.b(bArr);
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0010  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(android.graphics.drawable.Drawable r4) {
        /*
            r1 = 0
            if (r4 == 0) goto L_0x0046
            boolean r0 = r4 instanceof android.graphics.drawable.BitmapDrawable
            if (r0 == 0) goto L_0x0012
            android.graphics.drawable.BitmapDrawable r4 = (android.graphics.drawable.BitmapDrawable) r4
            android.graphics.Bitmap r0 = r4.getBitmap()
            r2 = r0
        L_0x000e:
            if (r2 != 0) goto L_0x0022
            r0 = r1
        L_0x0011:
            return r0
        L_0x0012:
            boolean r0 = r4 instanceof android.graphics.drawable.StateListDrawable
            if (r0 == 0) goto L_0x0046
            android.graphics.drawable.Drawable r0 = r4.getCurrent()
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0
            android.graphics.Bitmap r0 = r0.getBitmap()
            r2 = r0
            goto L_0x000e
        L_0x0022:
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            android.graphics.Bitmap$CompressFormat r0 = android.graphics.Bitmap.CompressFormat.PNG
            r3 = 80
            r2.compress(r0, r3, r1)
            r1.flush()     // Catch:{ IOException -> 0x003c }
        L_0x0031:
            byte[] r0 = r1.toByteArray()
            r1.close()     // Catch:{ IOException -> 0x0041 }
        L_0x0038:
            r2.recycle()
            goto L_0x0011
        L_0x003c:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0031
        L_0x0041:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0038
        L_0x0046:
            r2 = r1
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.m.h.a(android.graphics.drawable.Drawable):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001f  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0044  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(android.content.pm.PackageInfo r4, android.content.pm.PackageManager r5, boolean r6) {
        /*
            r1 = 0
            android.content.pm.ApplicationInfo r0 = r4.applicationInfo
            if (r0 == 0) goto L_0x0032
            android.content.pm.ApplicationInfo r0 = r4.applicationInfo
            int r0 = r0.icon
            if (r0 <= 0) goto L_0x0032
            if (r6 == 0) goto L_0x0017
            java.lang.String r0 = android.os.Build.MODEL
            java.lang.String r2 = "Lenovo A820"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0032
        L_0x0017:
            android.content.pm.ApplicationInfo r0 = r4.applicationInfo     // Catch:{ Throwable -> 0x002e }
            android.graphics.drawable.Drawable r0 = r0.loadIcon(r5)     // Catch:{ Throwable -> 0x002e }
        L_0x001d:
            if (r0 == 0) goto L_0x006a
            boolean r2 = r0 instanceof android.graphics.drawable.BitmapDrawable
            if (r2 == 0) goto L_0x0034
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0
            android.graphics.Bitmap r0 = r0.getBitmap()
            r2 = r0
        L_0x002a:
            if (r2 != 0) goto L_0x0044
            r0 = r1
        L_0x002d:
            return r0
        L_0x002e:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0032:
            r0 = r1
            goto L_0x001d
        L_0x0034:
            boolean r2 = r0 instanceof android.graphics.drawable.StateListDrawable
            if (r2 == 0) goto L_0x006a
            android.graphics.drawable.Drawable r0 = r0.getCurrent()
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0
            android.graphics.Bitmap r0 = r0.getBitmap()
            r2 = r0
            goto L_0x002a
        L_0x0044:
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            android.graphics.Bitmap$CompressFormat r0 = android.graphics.Bitmap.CompressFormat.PNG
            r3 = 80
            r2.compress(r0, r3, r1)
            r1.flush()     // Catch:{ IOException -> 0x0060 }
        L_0x0053:
            byte[] r0 = r1.toByteArray()
            r1.close()     // Catch:{ IOException -> 0x0065 }
        L_0x005a:
            if (r6 != 0) goto L_0x002d
            r2.recycle()
            goto L_0x002d
        L_0x0060:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0053
        L_0x0065:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005a
        L_0x006a:
            r2 = r1
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.m.h.a(android.content.pm.PackageInfo, android.content.pm.PackageManager, boolean):byte[]");
    }

    public static j a(Context context, String str, int i, long j) {
        PackageInfo packageInfo;
        if (s.b(str) || context == null) {
            return null;
        }
        File file = new File(str);
        if (!file.exists()) {
            return null;
        }
        boolean z = (i & 1) > 0;
        boolean z2 = (i & 2) > 0;
        boolean z3 = (i & 4) > 0;
        boolean z4 = (i & 8) > 0;
        boolean z5 = (i & 16) > 0;
        j jVar = new j();
        jVar.f318a = j;
        jVar.b = 2;
        jVar.c = str;
        jVar.d = file.length();
        jVar.e = file.lastModified();
        if (!z) {
            return jVar;
        }
        int i2 = 1;
        if (z2) {
            i2 = 4097;
        }
        if (z3) {
            i2 += 64;
        }
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo2 = null;
        try {
            packageInfo2 = packageManager.getPackageArchiveInfo(str, i2);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        Log.d("com.qq.connect", "parse::" + packageInfo2);
        if (packageInfo2 != null) {
            jVar.f = packageInfo2.packageName;
            if (jVar.g == null && packageInfo2.applicationInfo != null) {
                jVar.g = packageInfo2.applicationInfo.loadLabel(packageManager).toString();
            }
            if (!((jVar.g != null && (packageInfo2.packageName == null || !jVar.g.equals(packageInfo2.packageName))) || packageInfo2.applicationInfo == null || packageInfo2.applicationInfo.labelRes == 0)) {
                e eVar = new e();
                String str2 = null;
                try {
                    str2 = eVar.a(context, str, packageInfo2.applicationInfo.labelRes);
                } catch (Throwable th2) {
                    th2.printStackTrace();
                }
                eVar.a();
                jVar.g = str2;
            }
            jVar.h = packageInfo2.versionCode;
            jVar.i = packageInfo2.versionName;
            jVar.l = Constants.STR_EMPTY;
            if (z4) {
                if (jVar.j == null) {
                    jVar.j = a(packageInfo2, packageManager, true);
                }
                if (jVar.j == null && packageInfo2.applicationInfo != null) {
                    Log.d("com.qq.connect", jVar.f + packageInfo2.applicationInfo.icon + "+++");
                    try {
                        jVar.j = a(e.d(context, str, packageInfo2.applicationInfo.icon));
                    } catch (Throwable th3) {
                    }
                    Log.d("com.qq.connect", jVar.f + packageInfo2.applicationInfo.icon + ">>>>>>>>>");
                }
                if (packageInfo2.applicationInfo == null || jVar.j == null) {
                    Log.d("com.qq.connect", jVar.f + "   0 ");
                } else {
                    Log.d("com.qq.connect", jVar.f + Constants.STR_EMPTY + jVar.j.length);
                }
            }
            if (z5) {
                PackageInfo packageInfo3 = null;
                try {
                    if (jVar.f != null) {
                        packageInfo3 = packageManager.getPackageInfo(jVar.f, i2);
                    }
                    packageInfo = packageInfo3;
                } catch (Throwable th4) {
                    packageInfo = null;
                }
                if (packageInfo != null) {
                    if (packageInfo.applicationInfo != null) {
                        jVar.m = packageInfo.applicationInfo.sourceDir;
                    }
                    if (!s.b(jVar.m)) {
                        File file2 = new File(jVar.m);
                        if (file2.exists()) {
                            jVar.o = file2.lastModified();
                            jVar.n = file2.length();
                        }
                    }
                    jVar.p = packageInfo.versionCode;
                    jVar.q = packageInfo.versionName;
                    if (packageInfo.applicationInfo != null) {
                        jVar.s = packageInfo.applicationInfo.flags % 2 > 0;
                    }
                    if (z3) {
                        jVar.r = a(packageInfo);
                    }
                    if (z4 && jVar.j == null && packageInfo.applicationInfo != null) {
                        jVar.j = a(packageInfo, packageManager, false);
                    }
                }
            }
            if (z2 && packageInfo2.permissions != null) {
                for (int i3 = 0; i3 < packageInfo2.permissions.length; i3++) {
                    if (packageInfo2.permissions[i3] != null) {
                        if (i3 != 0) {
                            jVar.l += ";";
                        }
                        jVar.l += packageInfo2.permissions[i3].name;
                    }
                }
            }
            if (z3 && packageInfo2.signatures != null) {
                jVar.k = a(packageInfo2);
            }
        }
        if (jVar.j != null && jVar.j.length > 62464) {
            jVar.j = null;
        }
        return jVar;
    }

    public h(String str, int i, int i2, long j) {
        this.f = j;
        this.c = str;
        this.f316a = i;
        this.b = i2;
        if ((this.f316a & 1) > 0 && this.b == 3) {
            this.d = new i(this, i);
            this.d.start();
        }
    }

    public void a(String str) {
        a(AstApp.i(), str, this.f316a, this.f);
    }

    public void b(String str) {
        if (!s.b(str)) {
            if (this.g == null) {
                this.g = new ArrayList<>();
            }
            j a2 = a(AstApp.i(), str, this.f316a, this.f);
            if (a2 != null) {
                this.g.add(a2);
            }
        }
    }

    public ArrayList<byte[]> a() {
        ArrayList<byte[]> arrayList = new ArrayList<>(1024);
        if (this.g == null) {
            arrayList.add(s.a(0));
            return arrayList;
        }
        int size = this.g.size();
        arrayList.add(s.a(size));
        for (int i = 0; i < size; i++) {
            this.g.get(i).a(arrayList);
        }
        return arrayList;
    }

    public void run() {
        boolean z;
        boolean z2;
        boolean z3;
        int i = 0;
        super.run();
        Native nativeR = new Native();
        if (this.f != 0) {
            com.qq.provider.h.a(0, this.f);
        }
        if ((this.f316a & 64) > 0) {
            this.c = Environment.getExternalStorageDirectory().getAbsolutePath();
            z = true;
        } else {
            z = false;
        }
        if ((this.f316a & 32) <= 0) {
            z2 = false;
        } else if (this.c.equals("/") || this.c.equals("/data")) {
            int filePermission = nativeR.getFilePermission("/data");
            int filePermission2 = nativeR.getFilePermission("/data/local");
            if (filePermission <= 0 || filePermission2 <= 0) {
                z3 = true;
            } else if (filePermission % 10 < 5 || filePermission2 % 10 < 5) {
                z3 = true;
            } else {
                z3 = false;
            }
            z2 = z3;
        } else {
            z2 = !this.c.equals("/data/local");
        }
        Log.w("com.qq.connect", this.c + " " + z + "." + z2);
        FileNative fileNative = new FileNative(this);
        try {
            nativeR.findFileCallback(fileNative, "com/qq/ndk/FileNative", "dumpFiles", this.c, ".apk", 2);
            if (z2) {
                nativeR.findFileCallback(fileNative, "com/qq/ndk/FileNative", "dumpFiles", "/data/local/tmp", ".apk", 2);
            }
            if (z) {
                String c2 = n.c();
                if (c2 != null && n.a(c2, this.c)) {
                    c2 = null;
                }
                if (c2 != null) {
                    nativeR.findFileCallback(fileNative, "com/qq/ndk/FileNative", "dumpFiles", c2, ".apk", 2);
                }
            }
            if (this.f != 0) {
                if (this.e != null && this.e.size() > 0) {
                    while (i < this.e.size()) {
                        a(this.e.get(i));
                        i++;
                    }
                }
            } else if (this.e != null && this.e.size() > 0) {
                while (i < this.e.size()) {
                    b(this.e.get(i));
                    i++;
                }
            }
            if (this.d != null) {
                this.d.a();
                this.d = null;
            }
        } catch (Throwable th) {
            if (this.d != null) {
                this.d.a();
                this.d = null;
            }
            this.e = null;
            throw th;
        }
        this.e = null;
        if (this.f != 0 && this.b != 3) {
            com.qq.provider.h.a(1, this.f);
        }
    }

    public void a(String[] strArr) {
        int i = 0;
        if (strArr != null && strArr.length >= 1) {
            if (this.b == 1) {
                if (this.e == null) {
                    this.e = new ArrayList<>();
                }
                while (i < strArr.length) {
                    if (strArr[i] != null) {
                        this.e.add(strArr[i]);
                    }
                    i++;
                }
            } else if (this.b == 2) {
                while (i < strArr.length) {
                    a(strArr[i]);
                    i++;
                }
            } else if (this.b == 3 && this.d != null) {
                while (i < strArr.length) {
                    if (strArr[i] != null) {
                        this.d.a(strArr[i]);
                    }
                    i++;
                }
            }
        }
    }
}
