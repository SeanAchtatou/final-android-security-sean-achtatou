package com.qq.d.g;

import android.os.SystemProperties;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import java.io.File;
import java.util.Map;

/* compiled from: ProGuard */
public class i extends r {
    public boolean a(Map<String, String> map, boolean z, s sVar) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean exists = new File("/system/framework/framework-miui-res.apk").exists();
        boolean z5 = !TextUtils.isEmpty(SystemProperties.get("ro.miui.ui.version.code")) || !TextUtils.isEmpty(SystemProperties.get("ro.miui.ui.version.name"));
        boolean contains = sVar.b().toLowerCase().contains("miui");
        String str = SystemProperties.get("ro.config.ringtone");
        if (TextUtils.isEmpty(str) || !str.equalsIgnoreCase("mi.ogg")) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (exists || contains || z2 || z5) {
            z3 = true;
        } else {
            z3 = false;
        }
        if (!z && z3) {
            a(map, "rombrand", "miui");
            a(map, "rombranch", Constants.STR_EMPTY);
            a(map, "romversion", sVar.a());
            a(map, "romsubbranch", a());
        }
        if (this.b == null) {
            return z3;
        }
        r rVar = this.b;
        if (z || z3) {
            z4 = true;
        } else {
            z4 = false;
        }
        boolean a2 = rVar.a(map, z4, sVar);
        if (z3 || a2) {
            return true;
        }
        return false;
    }

    private String a() {
        String str = SystemProperties.get("ro.miui.ui.version.code");
        String str2 = SystemProperties.get("ro.miui.ui.version.name");
        if (!TextUtils.isEmpty(str) || !TextUtils.isEmpty(str2)) {
            return "v5";
        }
        return "v4";
    }
}
