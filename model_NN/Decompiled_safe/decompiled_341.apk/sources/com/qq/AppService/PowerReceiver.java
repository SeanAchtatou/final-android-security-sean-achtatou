package com.qq.AppService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
public class PowerReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    public static String f207a = null;
    public static volatile int b = 1;
    public static volatile int c = 0;
    private static volatile int d = 0;

    public IBinder peekService(Context context, Intent intent) {
        return super.peekService(context, intent);
    }

    public void onReceive(Context context, Intent intent) {
        String action;
        if (AppService.c && intent != null && (action = intent.getAction()) != null) {
            if (action.equals("android.hardware.usb.action.USB_STATE")) {
                try {
                    ad.a(context, ad.a(context, intent.getExtras().getBoolean("connected")));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if ("android.intent.action.BATTERY_CHANGED".equals(action)) {
                int intExtra = intent.getIntExtra("level", 0);
                int intExtra2 = intent.getIntExtra("scale", 100);
                if (intExtra2 != 0) {
                    int i = (intExtra * 100) / intExtra2;
                    while (i > 100) {
                        i /= 10;
                    }
                    f207a = String.valueOf(i) + "%";
                    int intExtra3 = intent.getIntExtra("status", 1);
                    b = intExtra3;
                    synchronized (this) {
                        c = intent.getIntExtra("plugged", 0);
                    }
                    if (c == 0) {
                        d = 0;
                    } else {
                        d++;
                    }
                    IPCService g = AppService.g();
                    if (g != null && g.b != null) {
                        TemporaryThreadManager.get().start(new ao(this, g, intExtra3));
                    }
                }
            }
        }
    }
}
