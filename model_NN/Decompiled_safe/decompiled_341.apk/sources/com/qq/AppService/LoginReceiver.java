package com.qq.AppService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/* compiled from: ProGuard */
public class LoginReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Log.d("com.qq.connect", "in login Receiver");
        if (intent != null) {
            String stringExtra = intent.getStringExtra("name");
            String stringExtra2 = intent.getStringExtra("ip");
            String stringExtra3 = intent.getStringExtra("key");
            String stringExtra4 = intent.getStringExtra("qq");
            String stringExtra5 = intent.getStringExtra("pc");
            if (stringExtra == null) {
                return;
            }
            if (stringExtra4 != null || stringExtra5 == null) {
                try {
                    ad.a(context, ad.a(context, stringExtra, stringExtra2, stringExtra3, null, null));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    ad.a(context, ad.a(context, stringExtra, stringExtra2, stringExtra3, null, null));
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }
}
