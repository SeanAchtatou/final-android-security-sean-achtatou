package com.qq.AppService;

import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.qq.l.k;
import com.qq.l.p;
import com.tencent.connector.m;

/* compiled from: ProGuard */
public class t {

    /* renamed from: a  reason: collision with root package name */
    private static t f252a = null;
    private static String c = null;
    private Context b = null;
    private int d = 0;
    private String e = null;
    private String f = null;
    private u g = null;

    public static void a(String str) {
        if (str != null) {
            c = m.d(str);
            p.m().a(c);
        }
    }

    public static void b(String str) {
        if (str != null) {
            c = m.e(str);
        }
    }

    public static void a() {
        c = null;
    }

    public static boolean c(String str) {
        if (str != null && str.equals(c)) {
            return true;
        }
        return false;
    }

    public static t a(u uVar, String str, String str2, String str3, Context context, String str4, String str5, String str6) {
        if (f252a == null) {
            f252a = new t(context, str4, 0);
        } else {
            b();
            f252a = new t(context, str4, 0);
        }
        if (f252a.g != null) {
            if (f252a.f == null && WifiPage.c != null) {
                Message message = new Message();
                message.what = 1;
                WifiPage.c.sendMessage(message);
            }
            f252a.g.a(-1);
        }
        f252a.g = uVar;
        f252a.a(str, str2, str3, str4, str5, str6, 0);
        return f252a;
    }

    public static t a(u uVar, String str, String str2, String str3, Context context, String str4) {
        if (f252a == null) {
            f252a = new t(context, str4);
        } else {
            b();
            f252a = new t(context, str4);
        }
        if (f252a.g != null) {
            if (f252a.e == null && WifiPage.c != null) {
                Message message = new Message();
                message.what = 1;
                WifiPage.c.sendMessage(message);
            }
            f252a.g.a(-1);
        }
        f252a.g = uVar;
        f252a.a(str, str2, str3, str4);
        return f252a;
    }

    public static void b() {
        f252a = null;
    }

    public static void a(Context context) {
        if (f252a != null) {
            f252a.d = 0;
            if (f252a != null) {
                f252a.g = null;
            }
            if (WifiPage.c != null) {
                Message message = new Message();
                message.what = 1;
                WifiPage.c.sendMessage(message);
            }
            try {
                ad.a(context, ad.h(context));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public static void a(int i) {
        if (f252a != null && f252a.g != null) {
            f252a.g.a(i);
        }
    }

    private t(Context context, String str) {
        this.b = context;
        this.e = str;
    }

    private t(Context context, String str, int i) {
        this.b = context;
        this.f = str;
    }

    public void a(String str, String str2, String str3, String str4, String str5, String str6, int i) {
        this.d = 2;
        Log.d("com.qq.connect", "in show Dailog_ex!");
        IPCService g2 = AppService.g();
        if (!(g2 == null || g2.b == null)) {
            try {
                g2.b.a(str2, str, str3);
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
        p.m().a(new k());
        p.m().a(1, 2, -1);
        p.m().b(704, 0, -1);
        try {
            ad.a(this.b, ad.a(this.b, str, str2, str3, str5, str6));
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public void a(String str, String str2, String str3, String str4) {
        this.d = 2;
        Log.d("com.qq.connect", "in show Dailog!");
        IPCService g2 = AppService.g();
        if (!(g2 == null || g2.b == null)) {
            try {
                g2.b.a(str2, str, str3);
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
        Intent intent = new Intent();
        intent.setClass(this.b, LoginReceiver.class);
        intent.putExtra("name", str);
        intent.putExtra("ip", str2);
        intent.putExtra("key", str3);
        intent.putExtra("qq", str4);
        this.b.sendBroadcast(intent);
    }
}
