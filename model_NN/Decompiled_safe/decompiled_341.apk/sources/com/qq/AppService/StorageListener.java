package com.qq.AppService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.qq.provider.h;

/* compiled from: ProGuard */
public class StorageListener extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action;
        if (context != null && intent != null && (action = intent.getAction()) != null) {
            if (action.equals("android.intent.action.MEDIA_MOUNTED")) {
                h.d(0);
            } else if (action.equals("android.intent.action.MEDIA_SHARED")) {
                h.d(1);
            } else if (action.equals("android.intent.action.MEDIA_BAD_REMOVAL")) {
                h.d(2);
            } else if (action.equals("android.intent.action.MEDIA_REMOVED")) {
                h.d(3);
            } else if (action.equals("android.intent.action.MEDIA_UNMOUNTED")) {
                h.d(3);
            } else {
                Log.d("com.qq.connect", action);
            }
        }
    }
}
