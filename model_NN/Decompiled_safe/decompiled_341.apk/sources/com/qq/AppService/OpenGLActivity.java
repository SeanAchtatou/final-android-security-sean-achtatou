package com.qq.AppService;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;

/* compiled from: ProGuard */
public class OpenGLActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    public static volatile String f205a = null;
    public static volatile am b = null;
    public static final Object c = new Object();
    private GLSurfaceView d;

    public static void a() {
        synchronized (c) {
            f205a = null;
            b = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.d = new GLSurfaceView(this);
        this.d.setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        this.d.setRenderer(new al(this));
        this.d.getHolder().setFormat(-3);
        setContentView(this.d);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.d.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.d.onPause();
    }
}
