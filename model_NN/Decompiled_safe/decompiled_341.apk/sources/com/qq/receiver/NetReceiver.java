package com.qq.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/* compiled from: ProGuard */
public class NetReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    public static NetworkInfo f359a = null;
    public a b = null;

    public static void a(Context context, NetReceiver netReceiver) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        context.registerReceiver(netReceiver, intentFilter);
    }

    public static void b(Context context, NetReceiver netReceiver) {
        if (context != null && netReceiver != null) {
            context.unregisterReceiver(netReceiver);
        }
    }

    public void onReceive(Context context, Intent intent) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (this.b != null) {
                this.b.a(activeNetworkInfo);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
