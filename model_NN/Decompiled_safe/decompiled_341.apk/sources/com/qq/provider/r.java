package com.qq.provider;

import android.os.RemoteException;
import android.util.Log;

/* compiled from: ProGuard */
class r extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f350a;

    r(q qVar) {
        this.f350a = qVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.provider.q.b(com.qq.provider.q, boolean):boolean
     arg types: [com.qq.provider.q, int]
     candidates:
      com.qq.provider.q.b(android.content.Context, com.qq.g.c):void
      com.qq.provider.q.b(com.qq.provider.q, boolean):boolean */
    public void run() {
        super.run();
        try {
            boolean unused = this.f350a.d = this.f350a.b.askForRoot();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        boolean unused2 = this.f350a.e = false;
        synchronized (this.f350a) {
            this.f350a.notifyAll();
        }
        Log.d("com.qq.connect", "notify  root " + this.f350a.d);
    }
}
