package com.qq.provider;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import com.qq.AppService.AppService;
import com.tencent.tmsecurelite.commom.ServiceManager;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;

/* compiled from: ProGuard */
final class t implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f352a;
    private int b;
    private volatile boolean c;

    private t(q qVar) {
        this.f352a = qVar;
        this.b = 0;
        this.c = false;
    }

    /* synthetic */ t(q qVar, r rVar) {
        this(qVar);
    }

    public boolean a() {
        return this.c;
    }

    public void a(int i) {
        if (i > 0) {
            this.b = i;
        } else {
            this.b = 0;
        }
    }

    public void b() {
        synchronized (this.f352a) {
            this.f352a.notifyAll();
        }
    }

    public int b(int i) {
        if (i == 0) {
            Log.d("com.qq.connect", "no wait");
            return 0;
        }
        this.f352a.a(true, null);
        if (i > 0 && this.f352a.b == null) {
            this.c = true;
            synchronized (this.f352a) {
                try {
                    this.f352a.wait((long) this.b);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            this.c = false;
        }
        if (this.f352a.b == null) {
            try {
                Thread.sleep(850);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
        if (this.f352a.b == null) {
            return -5;
        }
        return 1;
    }

    public void onServiceDisconnected(ComponentName componentName) {
        Log.d("com.qq.connect", "onServiceDisconnected");
        ISystemOptimize unused = this.f352a.b = (ISystemOptimize) null;
        b();
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        Log.d("com.qq.connect", "onServiceConnected " + componentName);
        try {
            ISystemOptimize unused = this.f352a.b = (ISystemOptimize) ServiceManager.getInterface(0, iBinder);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (this.f352a.b == null) {
            Context applicationContext = AppService.a().getApplicationContext();
            if (applicationContext != null) {
                applicationContext.unbindService(this);
                this.f352a.b(applicationContext);
                return;
            }
            return;
        }
        b();
    }
}
