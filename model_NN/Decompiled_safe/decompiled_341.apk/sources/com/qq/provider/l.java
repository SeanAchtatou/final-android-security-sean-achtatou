package com.qq.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import com.qq.AppService.s;
import com.qq.d.c.a;
import com.qq.g.c;
import com.qq.ndk.Native;
import com.qq.provider.ebooks3.EbooksProvider;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;

/* compiled from: ProGuard */
public class l {

    /* renamed from: a  reason: collision with root package name */
    private String[] f344a;

    private void a(a aVar, ArrayList<byte[]> arrayList) {
        if (arrayList != null && aVar != null) {
            arrayList.add(s.a(aVar.f278a));
            arrayList.add(s.a(aVar.b));
            arrayList.add(s.a(aVar.c));
            arrayList.add(s.a(aVar.e));
            arrayList.add(s.a(aVar.d));
            arrayList.add(s.a(aVar.f));
            arrayList.add(s.a(aVar.g));
            arrayList.add(s.a(aVar.h));
            arrayList.add(s.a(aVar.i));
            arrayList.add(s.a(aVar.j));
            arrayList.add(s.a(aVar.k));
        }
    }

    private l() {
        this.f344a = null;
        this.f344a = new String[16];
        this.f344a[0] = ".txt";
        this.f344a[1] = ".chm";
        this.f344a[2] = ".pdf";
        this.f344a[3] = ".ceb";
        this.f344a[4] = ".umd";
        this.f344a[5] = ".pdg";
        this.f344a[6] = ".jar";
        this.f344a[7] = ".pdb";
        this.f344a[8] = ".stk";
        this.f344a[9] = ".epub";
        this.f344a[10] = ".pmlz";
        this.f344a[11] = ".rb";
        this.f344a[12] = ".doc";
        this.f344a[13] = ".docx";
        this.f344a[14] = ".rtf";
        this.f344a[15] = ".tcr";
    }

    public static l a() {
        return new l();
    }

    private boolean a(String str) {
        for (String endsWith : this.f344a) {
            if (str.endsWith(endsWith)) {
                return true;
            }
        }
        return false;
    }

    private String b(String str) {
        if (str != null && str.contains(".")) {
            return str.substring(str.lastIndexOf("."));
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private int a(Context context, String str) {
        if (str == null) {
            return -1;
        }
        Native nativeR = new Native();
        String replace = str.replace("'", "''");
        Cursor query = context.getContentResolver().query(EbooksProvider.f339a, null, "kind > -1 and data like '" + replace + "'", null, null);
        if (query == null) {
            return -1;
        }
        if (query.moveToFirst()) {
            int i = query.getInt(query.getColumnIndex("_id"));
            query.close();
            return i;
        }
        query.close();
        File file = new File(replace);
        if (!file.exists() || !file.isFile()) {
            return -2;
        }
        ContentValues contentValues = new ContentValues();
        String name = file.getName();
        if (!s.b(name)) {
            int lastIndexOf = name.lastIndexOf(46);
            if (lastIndexOf > 0) {
                name = name.substring(0, lastIndexOf);
            }
            contentValues.put("name", name);
        }
        contentValues.put("data", replace);
        String b = b(replace);
        if (b != null) {
            contentValues.put(SocialConstants.PARAM_TYPE, b);
        }
        contentValues.put("size", Long.valueOf(file.length()));
        contentValues.put("kind", (Integer) 5);
        contentValues.put("date_add", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("date_modify", Long.valueOf(file.lastModified()));
        contentValues.put("date_last_read", Integer.valueOf(nativeR.getLastAccessTime(name)));
        contentValues.put("read_times", (Integer) 0);
        return Integer.parseInt(context.getContentResolver().insert(EbooksProvider.f339a, contentValues).getLastPathSegment());
    }

    private int a(Context context, int i) {
        File file;
        Uri withAppendedPath = Uri.withAppendedPath(EbooksProvider.f339a, Constants.STR_EMPTY + i);
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            return 1;
        }
        for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
            String string = query.getString(query.getColumnIndex("data"));
            if (!(string == null || (file = new File(string)) == null || !file.exists())) {
                file.delete();
            }
        }
        query.close();
        context.getContentResolver().delete(withAppendedPath, null, null);
        return 0;
    }

    public void a(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h <= 0) {
            cVar.a(1);
        } else if (cVar.e() < h + 1) {
            cVar.a(1);
        } else {
            for (int i = 0; i < h; i++) {
                int h2 = cVar.h();
                if (h2 > 0) {
                    a(context, h2);
                }
            }
            cVar.a(0);
        }
    }

    public void b(Context context, c cVar) {
        Cursor query = context.getContentResolver().query(EbooksProvider.f339a, new String[]{"size", "kind"}, "kind > -1", null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        long j = 0;
        for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
            j += query.getLong(query.getColumnIndex("size"));
        }
        query.close();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(j));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void c(Context context, c cVar) {
        Cursor query = context.getContentResolver().query(EbooksProvider.f339a, new String[]{"kind"}, "kind > -1", null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        query.close();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(count));
        cVar.a(0);
        cVar.a(arrayList);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void d(Context context, c cVar) {
        if (cVar.e() < 4) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        String j2 = cVar.j();
        int h = cVar.h();
        String j3 = cVar.j();
        if (s.b(j2) || !a(j2)) {
            cVar.a(1);
            return;
        }
        File file = new File(j2);
        if (!file.exists()) {
            cVar.a(6);
            return;
        }
        String replace = j2.replace("'", "''");
        Cursor query = context.getContentResolver().query(EbooksProvider.f339a, null, "data like '" + replace + "'", null, null);
        if (query == null) {
            cVar.a(8);
        } else if (query.moveToFirst()) {
            int i = query.getInt(query.getColumnIndex("_id"));
            ArrayList arrayList = new ArrayList();
            arrayList.add(s.a(i));
            cVar.a(0);
            cVar.a(arrayList);
            query.close();
        } else {
            query.close();
            Native nativeR = new Native();
            ContentValues contentValues = new ContentValues();
            if (!s.b(j)) {
                contentValues.put("name", j);
            }
            contentValues.put("data", replace);
            String b = b(replace);
            if (b != null) {
                contentValues.put(SocialConstants.PARAM_TYPE, b);
            }
            contentValues.put("size", Long.valueOf(file.length()));
            contentValues.put("kind", Integer.valueOf(h));
            if (!s.b(j3)) {
                contentValues.put("label", j3);
            }
            contentValues.put("date_add", Long.valueOf(System.currentTimeMillis()));
            contentValues.put("date_modify", Long.valueOf(file.lastModified()));
            contentValues.put("date_last_read", Integer.valueOf(nativeR.getLastAccessTime(j)));
            contentValues.put("read_times", (Integer) 0);
            int parseInt = Integer.parseInt(context.getContentResolver().insert(EbooksProvider.f339a, contentValues).getLastPathSegment());
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(s.a(parseInt));
            cVar.a(0);
            cVar.a(arrayList2);
        }
    }

    public void e(Context context, c cVar) {
        Cursor query = context.getContentResolver().query(EbooksProvider.f339a, null, "kind > -1", null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(count));
        boolean moveToFirst = query.moveToFirst();
        for (int i = 0; i < count && moveToFirst; i++) {
            arrayList.add(s.a(query.getInt(query.getColumnIndex("_id"))));
            moveToFirst = query.moveToNext();
        }
        query.close();
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void f(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        Cursor query = context.getContentResolver().query(Uri.withAppendedPath(EbooksProvider.f339a, Constants.STR_EMPTY + cVar.h()), null, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            cVar.a(1);
            query.close();
        } else {
            a aVar = new a(query);
            aVar.d = aVar.d.replace("''", "'");
            ArrayList arrayList = new ArrayList();
            a(aVar, arrayList);
            cVar.a(arrayList);
            cVar.a(0);
            query.close();
        }
    }

    public void g(Context context, c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (h2 < 0) {
            cVar.a(1);
            return;
        }
        Cursor query = context.getContentResolver().query(EbooksProvider.f339a, null, "_id > " + h + " and " + "kind" + " > " + -1, null, "_id");
        if (query == null) {
            cVar.a(8);
            return;
        }
        boolean moveToFirst = query.moveToFirst();
        int count = query.getCount();
        if (count > h2) {
            count = h2;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(count));
        boolean z = moveToFirst;
        int i = 0;
        for (int i2 = 0; i2 < count && z; i2++) {
            a aVar = new a(query);
            if (!(aVar == null || aVar.d == null)) {
                aVar.d = aVar.d.replace("''", "'");
                if (new File(aVar.d).exists()) {
                    a(aVar, arrayList);
                    i++;
                } else {
                    Log.d("com.qq.connect", "delete book" + aVar.d);
                    context.getContentResolver().delete(EbooksProvider.f339a, "_id = " + aVar.f278a, null);
                }
            }
            z = query.moveToNext();
        }
        arrayList.set(0, s.a(i));
        query.close();
        cVar.a(0);
        cVar.a(arrayList);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void h(Context context, c cVar) {
        Log.d("com.qq.connect", "remove ebook!");
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        Uri withAppendedPath = Uri.withAppendedPath(EbooksProvider.f339a, Constants.STR_EMPTY + h);
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            cVar.a(1);
            query.close();
        } else {
            String string = query.getString(query.getColumnIndex("data"));
            query.close();
            if (h2 > 0) {
                context.getContentResolver().delete(withAppendedPath, null, null);
                if (string != null) {
                    File file = new File(string);
                    if (file.exists()) {
                        file.delete();
                    }
                }
            } else {
                ContentValues contentValues = new ContentValues();
                contentValues.put("kind", (Integer) -1);
                context.getContentResolver().update(withAppendedPath, contentValues, null, null);
            }
            cVar.a(0);
        }
    }

    public void i(Context context, c cVar) {
        if (cVar.e() < 11) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        cVar.g();
        cVar.g();
        int h2 = cVar.h();
        String j3 = cVar.j();
        String j4 = cVar.j();
        String j5 = cVar.j();
        String j6 = cVar.j();
        int h3 = cVar.h();
        Uri withAppendedPath = Uri.withAppendedPath(EbooksProvider.f339a, Constants.STR_EMPTY + h);
        ContentValues contentValues = new ContentValues();
        if (!s.b(j)) {
            contentValues.put("name", j);
        } else {
            contentValues.putNull("name");
        }
        if (!s.b(j2)) {
            contentValues.put(SocialConstants.PARAM_TYPE, j2);
        } else {
            contentValues.putNull(SocialConstants.PARAM_TYPE);
        }
        contentValues.put("kind", Integer.valueOf(h2));
        if (s.b(j3) || h2 != 0) {
            contentValues.putNull("label");
        } else {
            contentValues.put("kind", j3);
        }
        Date c = s.c(j4);
        if (!s.b(j4)) {
            contentValues.put("date_add", Long.valueOf(c.getTime()));
        } else {
            contentValues.putNull("date_add");
        }
        Date c2 = s.c(j5);
        if (!s.b(j5)) {
            contentValues.put("date_modify", Long.valueOf(c2.getTime()));
        } else {
            contentValues.putNull("date_modify");
        }
        Date c3 = s.c(j6);
        if (!s.b(j6)) {
            contentValues.put("date_last_read", Long.valueOf(c3.getTime()));
        } else {
            contentValues.putNull("date_last_read");
        }
        contentValues.put("read_times", Integer.valueOf(h3));
        contentValues.put("_id", Integer.valueOf(h));
        if (context.getContentResolver().update(withAppendedPath, contentValues, null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void j(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (s.b(j)) {
            cVar.a(1);
        } else if (!new File(j).exists()) {
            cVar.a(0);
        } else {
            long currentTimeMillis = System.currentTimeMillis();
            String[] findMutiFiles = new Native().findMutiFiles(j, this.f344a, 2);
            if (findMutiFiles == null) {
                cVar.a(6);
                return;
            }
            for (int i = 0; i < findMutiFiles.length; i++) {
                Log.d("get file", findMutiFiles[i]);
                a(context, findMutiFiles[i]);
            }
            Log.e("com.qq.connect.ebookscan", "time spent:" + (System.currentTimeMillis() - currentTimeMillis));
            cVar.a(0);
        }
    }

    public void k(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        Uri withAppendedPath = Uri.withAppendedPath(EbooksProvider.f339a, Constants.STR_EMPTY + cVar.h());
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            cVar.a(1);
            query.close();
        } else {
            int i = query.getInt(query.getColumnIndex("read_times"));
            query.close();
            if (i < 0) {
                i = 0;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("read_times", Integer.valueOf(i + 1));
            contentValues.put("date_last_read", Long.valueOf(System.currentTimeMillis()));
            if (context.getContentResolver().update(withAppendedPath, contentValues, null, null) > 0) {
                cVar.a(0);
            } else {
                cVar.a(1);
            }
        }
    }

    public void l(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        Uri withAppendedPath = Uri.withAppendedPath(EbooksProvider.f339a, Constants.STR_EMPTY + cVar.h());
        ContentValues contentValues = new ContentValues();
        contentValues.put("date_modify", Long.valueOf(new Date().getTime()));
        if (context.getContentResolver().update(withAppendedPath, contentValues, null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }
}
