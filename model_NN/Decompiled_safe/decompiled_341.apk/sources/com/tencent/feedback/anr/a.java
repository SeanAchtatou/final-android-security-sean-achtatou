package com.tencent.feedback.anr;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
final class a implements Parcelable.Creator<ANRHandleServiceTask> {
    a() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new ANRHandleServiceTask(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new ANRHandleServiceTask[i];
    }
}
