package com.tencent.feedback.proguard;

import android.content.Context;
import com.tencent.feedback.a.i;
import com.tencent.feedback.common.b;
import com.tencent.feedback.common.c;
import com.tencent.feedback.common.e;
import com.tencent.feedback.common.g;
import java.util.Calendar;
import java.util.Date;

/* compiled from: ProGuard */
public final class aw implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private static aw f3734a;
    /* access modifiers changed from: private */
    public Context b;
    private c c;
    private i d;
    private boolean e = false;
    private long f = 60000;
    private int g = 10;
    private boolean h = true;
    private final String i;
    private int j = 0;
    private long k = 0;

    private aw(Context context, c cVar, i iVar) {
        this.b = context;
        this.c = cVar;
        this.d = iVar;
        this.i = b.h(this.b);
        this.k = d();
    }

    public static synchronized aw a(Context context, c cVar, i iVar) {
        aw awVar;
        synchronized (aw.class) {
            if (f3734a == null) {
                f3734a = new aw(context.getApplicationContext(), cVar, iVar);
            }
            awVar = f3734a;
        }
        return awVar;
    }

    public static synchronized aw a() {
        aw awVar;
        synchronized (aw.class) {
            awVar = f3734a;
        }
        return awVar;
    }

    public final synchronized boolean b() {
        return this.e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.common.c.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.tencent.feedback.common.c.a(java.lang.Runnable, long):boolean
      com.tencent.feedback.common.c.a(int, boolean):boolean */
    public final synchronized boolean a(long j2) {
        boolean z = false;
        synchronized (this) {
            if (this.c != null) {
                long j3 = 60000 > 30000 ? 60000 : 30000;
                if (!this.e) {
                    this.f = j3;
                    this.e = true;
                    this.c.a(19, this, 0, this.f);
                    long e2 = e();
                    long time = new Date().getTime();
                    if (e2 <= time) {
                        b(d());
                        e2 = e();
                    }
                    if (e2 > time) {
                        this.c.a(new ax(this), e2 - time);
                        g.b("rqdp{ next day %d}", Long.valueOf(e2 - time));
                    }
                } else if (j3 != this.f) {
                    this.f = j3;
                    this.c.a(19, true);
                    this.c.a(19, this, 0, this.f);
                }
                z = true;
            }
        }
        return z;
    }

    private synchronized boolean h() {
        boolean z = false;
        synchronized (this) {
            if (!a(2)) {
                g.c("rqdp{ resume record fail}", new Object[0]);
            } else {
                g.e("rqdp{ resume state record true}", new Object[0]);
                z = true;
            }
        }
        return z;
    }

    private synchronized boolean i() {
        boolean a2;
        a2 = a(1);
        g.e("rqdp{ launch state record %b}", Boolean.valueOf(a2));
        return a2;
    }

    public final synchronized boolean c() {
        boolean a2;
        a2 = a(3);
        g.e("rqdp{ next day state record %b}", Boolean.valueOf(a2));
        return a2;
    }

    /* access modifiers changed from: protected */
    public final synchronized boolean a(int i2) {
        boolean z = true;
        synchronized (this) {
            e a2 = e.a(this.b);
            if (a2 != null) {
                q qVar = new q();
                qVar.a(i2);
                qVar.a(new Date().getTime());
                qVar.a(this.i);
                qVar.b(a2.i());
                if (ac.a(this.b, new q[]{qVar}) > 0) {
                    if (b(this.i) >= this.g) {
                        g.e("rqdp{ state max upload}", new Object[0]);
                        c f2 = f();
                        if (f2 != null) {
                            if (0 <= 0) {
                                f2.a(new ay(this, (byte) 2));
                            } else if (0 > 0) {
                                f2.a(new az(this, (byte) 2), 0);
                            }
                        }
                    }
                }
            }
            z = false;
        }
        return z;
    }

    public final synchronized q[] a(String str) {
        return ac.a(this.b, str);
    }

    public final synchronized int a(q[] qVarArr) {
        return ac.b(this.b, qVarArr);
    }

    public final synchronized int b(String str) {
        return ac.b(this.b, str);
    }

    public final synchronized long d() {
        long time;
        try {
            Calendar instance = Calendar.getInstance();
            instance.set(11, 0);
            instance.set(12, 0);
            instance.set(13, 0);
            instance.add(6, 1);
            time = instance.getTime().getTime();
        } catch (Throwable th) {
            th.printStackTrace();
            time = new Date().getTime() + TesDownloadConfig.TES_CONFIG_CHECK_PERIOD;
        }
        return time;
    }

    private synchronized int j() {
        return this.j;
    }

    private synchronized void b(int i2) {
        this.j = i2;
    }

    public final synchronized long e() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public final synchronized void b(long j2) {
        this.k = j2;
    }

    public final synchronized c f() {
        return this.c;
    }

    public final synchronized i g() {
        return this.d;
    }

    public final void run() {
        boolean z;
        int j2 = j() + 1;
        b(j2);
        if (j2 == 1) {
            this.h = b.b(this.b, this.i);
            i();
            return;
        }
        boolean b2 = b.b(this.b, this.i);
        synchronized (this) {
            if (b2 != this.h) {
                this.h = b2;
                if (b2) {
                    z = true;
                }
            }
            z = false;
        }
        if (z) {
            g.b("process:%s is resumed!", this.i);
            h();
        }
    }
}
