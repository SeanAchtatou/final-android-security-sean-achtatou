package com.tencent.feedback.proguard;

import java.io.Serializable;

/* compiled from: ProGuard */
public class q implements Serializable, Comparable<q> {

    /* renamed from: a  reason: collision with root package name */
    private long f3742a;
    private long b;
    private int c;
    private String d;
    private String e;

    /* access modifiers changed from: private */
    /* renamed from: a */
    public synchronized int compareTo(q qVar) {
        return (int) (this.b - qVar.b);
    }

    public final synchronized long a() {
        return this.b;
    }

    public final synchronized void a(long j) {
        this.b = j;
    }

    public final synchronized int b() {
        return this.c;
    }

    public final synchronized void a(int i) {
        this.c = i;
    }

    public final synchronized String c() {
        return this.d;
    }

    public final synchronized void a(String str) {
        this.d = str;
    }

    public final synchronized long d() {
        return this.f3742a;
    }

    public final synchronized void b(long j) {
        this.f3742a = j;
    }

    public final synchronized String e() {
        return this.e;
    }

    public final synchronized void b(String str) {
        this.e = str;
    }
}
