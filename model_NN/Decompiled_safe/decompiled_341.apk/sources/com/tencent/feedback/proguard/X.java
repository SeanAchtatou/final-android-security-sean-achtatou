package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;

public final class X extends C0008j {

    /* renamed from: a  reason: collision with root package name */
    private long f3717a = 0;
    private byte b = 0;
    private String c = Constants.STR_EMPTY;
    private String d = Constants.STR_EMPTY;

    public X() {
    }

    public X(long j, byte b2, String str, String str2) {
        this.f3717a = j;
        this.b = b2;
        this.c = str;
        this.d = str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte */
    public final void a(ag agVar) {
        this.f3717a = agVar.a(this.f3717a, 0, true);
        this.b = agVar.a(this.b, 1, true);
        this.c = agVar.b(2, false);
        this.d = agVar.b(3, false);
    }

    public final void a(ah ahVar) {
        ahVar.a(this.f3717a, 0);
        ahVar.a(this.b, 1);
        if (this.c != null) {
            ahVar.a(this.c, 2);
        }
        if (this.d != null) {
            ahVar.a(this.d, 3);
        }
    }

    public final void a(StringBuilder sb, int i) {
    }
}
