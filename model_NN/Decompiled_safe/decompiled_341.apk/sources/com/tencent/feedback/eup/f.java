package com.tencent.feedback.eup;

import android.content.Context;
import android.os.Environment;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.common.PlugInInfo;
import com.tencent.feedback.common.b;
import com.tencent.feedback.common.e;
import com.tencent.feedback.common.g;
import com.tencent.feedback.proguard.ac;
import com.tencent.feedback.proguard.aj;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/* compiled from: ProGuard */
public class f {

    /* renamed from: a  reason: collision with root package name */
    public byte f3693a;
    public int b;

    public static boolean a(Context context, e eVar) {
        String str;
        boolean z;
        g.b("rqdp{  EUPDAO.insertEUP() start}", new Object[0]);
        if (context == null || eVar == null) {
            g.c("rqdp{  EUPDAO.insertEUP() have null args}", new Object[0]);
            return false;
        }
        try {
            aj a2 = a(eVar);
            if (context == null || a2 == null) {
                g.a("rqdp{  AnalyticsDAO.insert() have null args}", new Object[0]);
                z = false;
            } else {
                ArrayList arrayList = new ArrayList();
                arrayList.add(a2);
                z = aj.a(context, arrayList);
            }
            if (!z) {
                return false;
            }
            eVar.a(a2.a());
            g.b("rqdp{  EUPDAO.insertEUP() end}", new Object[0]);
            return true;
        } catch (Throwable th) {
            th.printStackTrace();
            g.d("rqdp{  insert fail!}", new Object[0]);
            return false;
        } finally {
            str = "rqdp{  EUPDAO.insertEUP() end}";
            g.b(str, new Object[0]);
        }
    }

    public static int a(Context context, List<e> list) {
        int i = 0;
        g.b("rqdp{  EUPDAO.deleteEupList() start}", new Object[0]);
        if (context == null) {
            g.c("rqdp{  deleteEupList() have null args!}", new Object[0]);
            return -1;
        } else if (list.size() <= 0) {
            return 0;
        } else {
            Long[] lArr = new Long[list.size()];
            while (true) {
                int i2 = i;
                if (i2 >= list.size()) {
                    return aj.a(context, lArr);
                }
                lArr[i2] = Long.valueOf(list.get(i2).a());
                i = i2 + 1;
            }
        }
    }

    public static int b(Context context) {
        g.b("rqdp{  EUPDAO.querySum() start}", new Object[0]);
        if (context == null) {
            g.c("rqdp{  querySum() context is null arg}", new Object[0]);
            return -1;
        }
        return aj.a(context, new int[]{2, 1}, -1, Long.MAX_VALUE, null);
    }

    /* JADX INFO: finally extract failed */
    public static boolean b(Context context, List<e> list) {
        g.b("rqdp{  EUPDAO.insertOrUpdateEupList() start}", new Object[0]);
        if (context == null || list == null || list.size() <= 0) {
            g.c("rqdp{  context == null ||| list == null || list.size() <= 0,pls check}", new Object[0]);
            return false;
        }
        try {
            ArrayList arrayList = new ArrayList();
            for (e a2 : list) {
                aj a3 = a(a2);
                if (a3 != null) {
                    arrayList.add(a3);
                }
            }
            boolean b2 = aj.b(context, arrayList);
            g.b("rqdp{  EUPDAO.insertOrUpdateEupList() end}", new Object[0]);
            return b2;
        } catch (Throwable th) {
            g.b("rqdp{  EUPDAO.insertOrUpdateEupList() end}", new Object[0]);
            throw th;
        }
    }

    protected static boolean a(Context context, e eVar, d dVar) {
        if (dVar == null || !dVar.h()) {
            return false;
        }
        try {
            g.b("save eup logs", new Object[0]);
            e a2 = e.a(context);
            String c = a2.c();
            String d = a2.d();
            String s = eVar.s();
            Locale locale = Locale.US;
            Object[] objArr = new Object[9];
            objArr[0] = c;
            objArr[1] = d;
            objArr[2] = a2.f();
            objArr[3] = s;
            Date date = new Date(eVar.i());
            objArr[4] = date == null ? null : new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(date);
            objArr[5] = eVar.e();
            objArr[6] = eVar.f();
            objArr[7] = eVar.h();
            objArr[8] = eVar.x();
            String format = String.format(locale, "#--------\npackage:%s\nversion:%s\nsdk:%s\nprocess:%s\ndate:%s\ntype:%s\nmessage:%s\nstack:\n%s\neupID:%s\n", objArr);
            if (dVar.n() != null) {
                File file = new File(dVar.n());
                if (!file.isFile()) {
                    file = file.getParentFile();
                }
                ac.a(new File(file, "euplog.txt").getAbsolutePath(), format, dVar.i());
                return true;
            } else if (!b.f(context)) {
                return false;
            } else {
                int i = dVar.i();
                g.b("rqdp{  sv sd start}", new Object[0]);
                if (format != null && format.trim().length() > 0) {
                    if (Environment.getExternalStorageState().equals("mounted")) {
                        ac.a(new File(Environment.getExternalStorageDirectory(), "/Tencent/" + b.b(context) + "/euplog.txt").getAbsolutePath(), format, i);
                    }
                    g.b("rqdp{  sv sd end}", new Object[0]);
                }
                return true;
            }
        } catch (Throwable th) {
            g.c("rqdp{  save error} %s", th.toString());
            th.printStackTrace();
            return false;
        }
    }

    protected static aj a(e eVar) {
        int i;
        if (eVar == null) {
            return null;
        }
        try {
            aj ajVar = new aj(eVar.b() ? 1 : 2, 0, eVar.i(), ac.a(eVar));
            ajVar.b(eVar.l());
            ajVar.a(eVar.o());
            ajVar.a(eVar.q());
            ajVar.a(eVar.a());
            if (eVar.y()) {
                i = 1;
            } else {
                i = 0;
            }
            ajVar.c(i);
            return ajVar;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    private static void a(ArrayList<String> arrayList, Throwable th, int i, int i2, int i3) {
        while (arrayList != null && th != null && i <= i2 && arrayList.size() <= i3) {
            i++;
            StackTraceElement[] stackTrace = th.getStackTrace();
            if (stackTrace != null) {
                for (StackTraceElement stackTraceElement : stackTrace) {
                    arrayList.add(stackTraceElement.toString());
                }
            }
            if (th.getCause() != null) {
                arrayList.add("cause by:");
                arrayList.add(th.getCause().getClass().getName() + ": " + th.getCause().getMessage());
                th = th.getCause();
            } else {
                return;
            }
        }
    }

    protected static String a(Throwable th, d dVar) {
        int i = 100;
        int i2 = 3;
        if (dVar != null) {
            i2 = Math.max(3, dVar.o());
            i = Math.max(100, dVar.p());
            g.b("change frame:%d  line:%d", Integer.valueOf(i2), Integer.valueOf(i));
        }
        ArrayList arrayList = new ArrayList();
        a(arrayList, th, 0, i2, i);
        if (arrayList.size() <= 0) {
            return Constants.STR_EMPTY;
        }
        StringBuilder sb = new StringBuilder(arrayList.size());
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            sb.append((String) it.next()).append("\n");
        }
        return sb.toString();
    }

    public static List<e> a(Context context, int i, String str, int i2, String str2, int i3, int i4, int i5, int i6, long j, long j2, Boolean bool) {
        g.b("rqdp{  EUPDAO.queryEupRecent() start}", new Object[0]);
        if (context == null || i == 0 || ((j2 > 0 && j > j2) || (i4 > 0 && i3 > i4))) {
            g.c("rqdp{  context == null || limitNum == 0 || (timeEnd > 0 && timeStart > timeEnd) || (maxCount > 0 && miniCount > maxCount ,pls check}", new Object[0]);
            return null;
        }
        int i7 = "asc".equals(str) ? 1 : 2;
        int[] iArr = null;
        if (i2 == 2) {
            iArr = new int[]{2};
        } else if (i2 == 1) {
            iArr = new int[]{1};
        } else if (i2 < 0) {
            iArr = new int[]{1, 2};
        } else {
            g.c("rqdp{  queryEupRecent() seletedRecordType unaccepted}", new Object[0]);
        }
        List<aj> a2 = aj.a(context, iArr, -1, i7, -1, i, str2, i3, i4, i5, i6, j, j2, bool == null ? -1 : bool.booleanValue() ? 1 : 0);
        if (a2 == null || a2.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Iterator<aj> it = a2.iterator();
        while (it.hasNext()) {
            aj next = it.next();
            try {
                Object b2 = ac.b(next.b());
                if (b2 != null && e.class.isInstance(b2)) {
                    e cast = e.class.cast(b2);
                    cast.a(next.a());
                    arrayList.add(cast);
                    it.remove();
                }
            } catch (Throwable th) {
                th.printStackTrace();
                g.d("rqdp{  query have error!}", new Object[0]);
            }
        }
        if (a2.size() > 0) {
            g.b("rqdp{  there are error datas ,should be remove }" + a2.size(), new Object[0]);
            Long[] lArr = new Long[a2.size()];
            int i8 = 0;
            while (true) {
                int i9 = i8;
                if (i9 >= a2.size()) {
                    break;
                }
                lArr[i9] = Long.valueOf(a2.get(i9).a());
                i8 = i9 + 1;
            }
            aj.a(context, lArr);
        }
        g.b("rqdp{  EUPDAO.queryEupRecent() end}", new Object[0]);
        return arrayList;
    }

    public static Map<String, String> a() {
        Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
        if (allStackTraces == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        StringBuilder sb = new StringBuilder();
        try {
            for (Map.Entry next : allStackTraces.entrySet()) {
                sb.setLength(0);
                for (StackTraceElement stackTraceElement : (StackTraceElement[]) next.getValue()) {
                    sb.append(stackTraceElement.toString()).append("\n");
                }
                hashMap.put(((Thread) next.getKey()).getName(), sb.toString());
            }
        } catch (Throwable th) {
            g.d("add all thread error", new Object[0]);
            th.printStackTrace();
        }
        return hashMap;
    }

    public static boolean a(Context context) {
        if (ac.b() < 0) {
            g.e("rqdp{  today fail?}", new Object[0]);
            new Date().getTime();
        }
        List<e> a2 = l.a(context).a(context, 1);
        if (a2 == null || a2.size() <= 0) {
            return false;
        }
        return true;
    }

    public static e a(Context context, String str, String str2, long j, Map<String, PlugInInfo> map, String str3, String str4, String str5, String str6, String str7, String str8, long j2, String str9, byte[] bArr) {
        e eVar = new e();
        eVar.i(str3);
        eVar.j(str4);
        eVar.b(j2 + j);
        if (str9 != null && str9.length() > 10000) {
            try {
                str9 = str9.substring(str9.length() - 10000, str9.length());
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        if (bArr != null && bArr.length > 10000) {
            try {
                byte[] bArr2 = new byte[10000];
                int length = bArr2.length - 1;
                int length2 = bArr.length - 1;
                while (length >= 0 && length2 >= 0) {
                    bArr2[length] = bArr[length2];
                    length--;
                    length2--;
                }
                bArr = bArr2;
            } catch (Throwable th2) {
                th2.printStackTrace();
            }
        }
        eVar.k(str9);
        eVar.b(bArr);
        eVar.c(str5);
        if (str7 == null || str7.trim().length() == 0) {
            str7 = "empty message";
        } else if (str7.length() > 1000) {
            str7 = str7.substring(0, 1000);
        }
        eVar.b(str7);
        eVar.a(str6);
        if (str8 == null || str8.trim().length() == 0) {
            str8 = "empty stack";
        }
        eVar.d(str8);
        eVar.a(-1.0f);
        com.tencent.feedback.common.f a2 = com.tencent.feedback.common.f.a(context);
        eVar.c(com.tencent.feedback.common.f.i());
        eVar.e(a2.k());
        eVar.d(com.tencent.feedback.common.f.g());
        eVar.i(com.tencent.feedback.common.f.n());
        eVar.j(com.tencent.feedback.common.f.o());
        g.b("avram:%d,avsd:%d,avrom:%d,avstack:%d,avheap:%d", Long.valueOf(eVar.I()), Long.valueOf(eVar.K()), Long.valueOf(eVar.J()), Long.valueOf(eVar.O()), Long.valueOf(eVar.P()));
        e a3 = e.a(context);
        eVar.f(a3.u());
        eVar.g(a3.t());
        eVar.h(a3.v());
        eVar.r(a3.d());
        eVar.s(a3.z());
        g.b("tram:%d,trom:%d,tsd:%d,v:%s,cn:%s", Long.valueOf(eVar.L()), Long.valueOf(eVar.M()), Long.valueOf(eVar.N()), eVar.Q(), eVar.R());
        eVar.e(str);
        eVar.q(str2);
        eVar.a(map);
        eVar.l(ac.c());
        eVar.m(ac.b("ro.build.fingerprint"));
        g.b("record id:%s", eVar.x());
        g.b("rom id %s", eVar.A());
        return eVar;
    }
}
