package com.tencent.feedback.eup.jni;

import com.tencent.feedback.common.g;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: ProGuard */
public class NativeExceptionUpload {
    public static final int ANDROID_LOG_DEBUG = 3;
    public static final int ANDROID_LOG_ERROR = 6;
    public static final int ANDROID_LOG_INFO = 4;
    public static final int ANDROID_LOG_WARN = 5;
    public static final int JAR_JNI_VERSION = 1;

    /* renamed from: a  reason: collision with root package name */
    private static AtomicBoolean f3696a = new AtomicBoolean(false);
    private static NativeExceptionHandler b = null;

    protected static native void doNativeCrashForTest();

    protected static native void enableHandler(boolean z);

    protected static native boolean registNativeExceptionHandler(String str, String str2, int i);

    protected static native String registNativeExceptionHandler2(String str, String str2, int i, int i2);

    protected static native void setLogMode(int i);

    public static synchronized boolean loadRQDNativeLib(File file) {
        boolean z = true;
        synchronized (NativeExceptionUpload.class) {
            if (file != null) {
                if (file.exists() && file.canRead()) {
                    try {
                        g.b("load %s", file.getAbsolutePath());
                        System.load(file.getAbsolutePath());
                        f3696a.set(true);
                    } catch (Throwable th) {
                        th.printStackTrace();
                        g.d("rqdp{  load library fail! see detail ,will turn off native eup function!}", new Object[0]);
                        z = false;
                    }
                }
            }
            z = false;
        }
        return z;
    }

    public static synchronized boolean loadRQDNativeLib(List<File> list) {
        File file;
        boolean z;
        synchronized (NativeExceptionUpload.class) {
            if (list != null) {
                Iterator<File> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    file = it.next();
                    if (file.exists() && file.isFile() && file.getName().equals("libNativeRQD.so")) {
                        break;
                    }
                }
            }
            file = null;
            if (file != null) {
                z = loadRQDNativeLib(file);
            } else {
                z = false;
            }
        }
        return z;
    }

    public static synchronized boolean loadRQDNativeLib() {
        boolean z = true;
        synchronized (NativeExceptionUpload.class) {
            try {
                System.loadLibrary("NativeRQD");
                f3696a.set(true);
            } catch (Throwable th) {
                th.printStackTrace();
                g.d("rqdp{  load library fail! see detail ,will turn off native eup function!}", new Object[0]);
                z = false;
            }
        }
        return z;
    }

    public static synchronized NativeExceptionHandler getmHandler() {
        NativeExceptionHandler nativeExceptionHandler;
        synchronized (NativeExceptionUpload.class) {
            nativeExceptionHandler = b;
        }
        return nativeExceptionHandler;
    }

    public static synchronized void setmHandler(NativeExceptionHandler nativeExceptionHandler) {
        synchronized (NativeExceptionUpload.class) {
            b = nativeExceptionHandler;
        }
    }

    public static boolean registEUP(String str, String str2, int i) {
        if (!f3696a.get()) {
            g.c("rqdp{  nreg disable!}", new Object[0]);
            return false;
        } else if (str == null || str.trim().length() <= 0) {
            g.c("rqdp{  nreg param!}", new Object[0]);
            return false;
        } else {
            try {
                g.a("jarV:%d nativeV:%s", 1, registNativeExceptionHandler2(str, str2, i, 1));
                return true;
            } catch (Throwable th) {
                g.c("regist fail:%s , try old regist", th.getMessage());
                th.printStackTrace();
                th.printStackTrace();
                return false;
            }
        }
    }

    public static void enableNativeEUP(boolean z) {
        if (!f3696a.get()) {
            g.c("rqdp{  n enable disable!!}", new Object[0]);
            return;
        }
        try {
            enableHandler(z);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void testNativeCrash() {
        if (!f3696a.get()) {
            g.c("rqdp{  n testNC disable!!}", new Object[0]);
        } else {
            doNativeCrashForTest();
        }
    }

    public static void setNativeLogMode(int i) {
        if (!f3696a.get()) {
            g.c("rqdp{  n sNL disable!!}", new Object[0]);
            return;
        }
        try {
            setLogMode(i);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
