package com.tencent.feedback.eup;

import com.tencent.assistant.st.STConst;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.common.PlugInInfo;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/* compiled from: ProGuard */
public class e implements Serializable {
    private long A = -1;
    private long B = -1;
    private long C = -1;
    private long D = -1;
    private String E;
    private byte[] F;
    private Map<String, PlugInInfo> G;
    private String H;
    private boolean I;
    private String J;
    private String K = Constants.STR_EMPTY;
    private String L = Constants.STR_EMPTY;
    private String M = Constants.STR_EMPTY;
    private String N = "unknwon";
    private String O = Constants.STR_EMPTY;
    private String P = Constants.STR_EMPTY;
    private String Q = Constants.STR_EMPTY;
    private final Map<String, String> R = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    private long f3692a = -1;
    private boolean b = true;
    private boolean c = false;
    private boolean d = false;
    private boolean e = false;
    private boolean f = false;
    private int g = 0;
    private String h = Constants.STR_EMPTY;
    private String i;
    private String j;
    private String k;
    private String l;
    private long m;
    private String n;
    private int o;
    private byte[] p;
    private String q;
    private String r;
    private String s;
    private String t;
    private float u = -1.0f;
    private float v = -1.0f;
    private long w = -1;
    private long x = -1;
    private long y = -1;
    private long z = -1;

    public e() {
        try {
            this.H = UUID.randomUUID().toString();
        } catch (Throwable th) {
            th.printStackTrace();
            this.H = STConst.ST_INSTALL_FAIL_STR_UNKNOWN;
        }
    }

    public final synchronized long a() {
        return this.f3692a;
    }

    public final synchronized void a(long j2) {
        this.f3692a = j2;
    }

    public final synchronized boolean b() {
        return this.b;
    }

    public final synchronized void a(boolean z2) {
        this.b = z2;
    }

    public final synchronized boolean c() {
        return this.c;
    }

    public final synchronized void b(boolean z2) {
        this.c = true;
    }

    public final synchronized boolean d() {
        return this.d;
    }

    public final synchronized void c(boolean z2) {
        this.d = true;
    }

    public final synchronized String e() {
        return this.i;
    }

    public final synchronized void a(String str) {
        this.i = str;
    }

    public final synchronized String f() {
        return this.j;
    }

    public final synchronized void b(String str) {
        this.j = str;
    }

    public final synchronized String g() {
        return this.k;
    }

    public final synchronized void c(String str) {
        this.k = str;
    }

    public final synchronized String h() {
        return this.l;
    }

    public final synchronized void d(String str) {
        this.l = str;
    }

    public final synchronized long i() {
        return this.m;
    }

    public final synchronized void b(long j2) {
        this.m = j2;
    }

    public final synchronized float j() {
        return this.u;
    }

    public final synchronized void a(float f2) {
        this.u = -1.0f;
    }

    public final synchronized float k() {
        return this.v;
    }

    public final synchronized int l() {
        return this.g;
    }

    public final synchronized void a(int i2) {
        this.g = i2;
    }

    public final synchronized String m() {
        return this.h;
    }

    public final synchronized void e(String str) {
        this.h = str;
    }

    public final synchronized String n() {
        return this.n;
    }

    public final synchronized void f(String str) {
        this.n = str;
    }

    public final synchronized int o() {
        return this.o;
    }

    public final synchronized void b(int i2) {
        this.o = i2;
    }

    public final synchronized byte[] p() {
        return this.p;
    }

    public final synchronized void a(byte[] bArr) {
        this.p = bArr;
    }

    public final synchronized String q() {
        return this.q;
    }

    public final synchronized void g(String str) {
        this.q = str;
    }

    public final synchronized String r() {
        return this.r;
    }

    public final synchronized void h(String str) {
        this.r = str;
    }

    public final synchronized String s() {
        return this.s;
    }

    public final synchronized void i(String str) {
        this.s = str;
    }

    public final synchronized String t() {
        return this.t;
    }

    public final synchronized void j(String str) {
        this.t = str;
    }

    public final synchronized String u() {
        return this.E;
    }

    public final synchronized void k(String str) {
        this.E = str;
    }

    public final synchronized byte[] v() {
        return this.F;
    }

    public final synchronized void b(byte[] bArr) {
        this.F = bArr;
    }

    public final Map<String, PlugInInfo> w() {
        return this.G;
    }

    public final void a(Map<String, PlugInInfo> map) {
        this.G = map;
    }

    public final String x() {
        return this.H;
    }

    public final void l(String str) {
        this.H = str;
    }

    public final boolean y() {
        return this.I;
    }

    public final void d(boolean z2) {
        this.I = true;
    }

    public final boolean z() {
        return this.e;
    }

    public final void e(boolean z2) {
        this.e = true;
    }

    public final String A() {
        return this.J;
    }

    public final void m(String str) {
        this.J = str;
    }

    public final String B() {
        return this.K;
    }

    public final String C() {
        return this.L;
    }

    public final void n(String str) {
        this.L = str;
    }

    public final String D() {
        return this.M;
    }

    public final void o(String str) {
        this.M = str;
    }

    public final String E() {
        return this.N;
    }

    public final void p(String str) {
        this.N = str;
    }

    public final Map<String, String> F() {
        return this.R;
    }

    public final String G() {
        return this.O;
    }

    public final void q(String str) {
        this.O = str;
    }

    public final boolean H() {
        return this.f;
    }

    public final void f(boolean z2) {
        this.f = true;
    }

    public final long I() {
        return this.w;
    }

    public final void c(long j2) {
        this.w = j2;
    }

    public final long J() {
        return this.x;
    }

    public final void d(long j2) {
        this.x = j2;
    }

    public final long K() {
        return this.y;
    }

    public final void e(long j2) {
        this.y = j2;
    }

    public final long L() {
        return this.z;
    }

    public final void f(long j2) {
        this.z = j2;
    }

    public final long M() {
        return this.A;
    }

    public final void g(long j2) {
        this.A = j2;
    }

    public final long N() {
        return this.B;
    }

    public final void h(long j2) {
        this.B = j2;
    }

    public final long O() {
        return this.C;
    }

    public final void i(long j2) {
        this.C = j2;
    }

    public final long P() {
        return this.D;
    }

    public final void j(long j2) {
        this.D = j2;
    }

    public final String Q() {
        return this.P;
    }

    public final void r(String str) {
        this.P = str;
    }

    public final String R() {
        return this.Q;
    }

    public final void s(String str) {
        this.Q = str;
    }
}
