package com.tencent.feedback.eup;

import android.content.Context;
import com.tencent.feedback.a.h;
import com.tencent.feedback.common.g;
import com.tencent.feedback.proguard.U;
import com.tencent.feedback.proguard.ac;
import com.tencent.feedback.proguard.ag;

/* compiled from: ProGuard */
public final class j implements h {

    /* renamed from: a  reason: collision with root package name */
    private Context f3695a;

    public j(Context context) {
        this.f3695a = context;
    }

    public final void a(int i, byte[] bArr, boolean z) {
        d dVar;
        boolean z2;
        d j;
        if (i == 302 && bArr != null) {
            try {
                k k = k.k();
                if (k == null) {
                    g.c("rqdp{  imposiable handle response ,but no eup instance!}", new Object[0]);
                    return;
                }
                U u = new U();
                u.a(new ag(bArr));
                d o = k.o();
                if (o == null) {
                    d n = k.n();
                    if (n == null) {
                        g.f("rqdp{  init eup sStrategy by default}", new Object[0]);
                        j = new d();
                    } else {
                        g.f("rqdp{  init eup sStrategy by uStrategy}", new Object[0]);
                        j = n.clone();
                    }
                    k.a(j);
                    dVar = j;
                } else {
                    dVar = o;
                }
                if (u == null || dVar == null) {
                    z2 = false;
                } else {
                    if (dVar.e() != u.f3714a) {
                        g.h("rqdp{  is merged changed} %b", Boolean.valueOf(u.f3714a));
                        dVar.a(u.f3714a);
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                    if (dVar.k() != u.b) {
                        dVar.d(u.b);
                        g.h("rqdp{ Assert enable changed: } %s", Boolean.valueOf(u.b));
                        z2 = true;
                    }
                    if (dVar.l() != u.c) {
                        dVar.f(u.c);
                        g.h("rqdp{ Assert task interval changed: } %s", Integer.valueOf(u.c));
                        z2 = true;
                    }
                    if (dVar.m() != u.d) {
                        dVar.g(u.d);
                        g.h("rqdp{ Assert limit count changed: } %s", Integer.valueOf(u.d));
                        z2 = true;
                    }
                }
                if (z2 && z) {
                    g.f("rqdp{  save eup strategy}", new Object[0]);
                    ac.a(this.f3695a, i, bArr);
                }
                g.h("rqdp{  crashStrategy}[%s]", u);
            } catch (Throwable th) {
                th.printStackTrace();
                g.d("rqdp{  process crash strategy error} %s", th.toString());
            }
        }
    }
}
