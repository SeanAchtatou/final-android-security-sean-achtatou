package com.tencent.connect.auth;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.connect.a.a;
import com.tencent.connect.common.AssistActivity;
import com.tencent.connect.common.BaseApi;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import com.tencent.open.a.n;
import com.tencent.open.b.d;
import com.tencent.open.utils.Global;
import com.tencent.open.utils.HttpUtils;
import com.tencent.open.utils.ServerSetting;
import com.tencent.open.utils.SystemUtils;
import com.tencent.open.utils.ThreadManager;
import com.tencent.open.utils.Util;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class AuthAgent extends BaseApi {
    public static final String SECURE_LIB_FILE_NAME = "libwbsafeedit";
    public static final String SECURE_LIB_NAME = "libwbsafeedit.so";

    /* renamed from: a  reason: collision with root package name */
    private IUiListener f3476a;
    private String b;
    /* access modifiers changed from: private */
    public Activity c;

    public AuthAgent(QQToken qQToken) {
        super(qQToken);
    }

    /* compiled from: ProGuard */
    class TokenListener implements IUiListener {
        private final IUiListener b;
        private final boolean c;
        private final Context d;

        public TokenListener(Context context, IUiListener iUiListener, boolean z, boolean z2) {
            this.d = context;
            this.b = iUiListener;
            this.c = z;
            n.b(n.d, "OpenUi, TokenListener()");
        }

        public void onComplete(Object obj) {
            n.b(n.d, "OpenUi, TokenListener() onComplete");
            JSONObject jSONObject = (JSONObject) obj;
            try {
                String string = jSONObject.getString(Constants.PARAM_ACCESS_TOKEN);
                String string2 = jSONObject.getString(Constants.PARAM_EXPIRES_IN);
                String string3 = jSONObject.getString("openid");
                if (!(string == null || AuthAgent.this.mToken == null || string3 == null)) {
                    AuthAgent.this.mToken.setAccessToken(string, string2);
                    AuthAgent.this.mToken.setOpenId(string3);
                    a.d(this.d, AuthAgent.this.mToken);
                }
                String string4 = jSONObject.getString(Constants.PARAM_PLATFORM_ID);
                if (string4 != null) {
                    try {
                        this.d.getSharedPreferences(Constants.PREFERENCE_PF, 0).edit().putString(Constants.PARAM_PLATFORM_ID, string4).commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                        n.b(n.d, "OpenUi, TokenListener() onComplete error", e);
                    }
                }
                if (this.c) {
                    CookieSyncManager.getInstance().sync();
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
                n.b(n.d, "OpenUi, TokenListener() onComplete error", e2);
            }
            this.b.onComplete(jSONObject);
            AuthAgent.this.releaseResource();
            n.b();
        }

        public void onError(UiError uiError) {
            n.b(n.d, "OpenUi, TokenListener() onError");
            this.b.onError(uiError);
            n.b();
        }

        public void onCancel() {
            n.b(n.d, "OpenUi, TokenListener() onCancel");
            this.b.onCancel();
            n.b();
        }
    }

    public int doLogin(Activity activity, String str, IUiListener iUiListener) {
        return doLogin(activity, str, iUiListener, false, null);
    }

    public int doLogin(Activity activity, String str, IUiListener iUiListener, boolean z, Fragment fragment) {
        this.b = str;
        this.c = activity;
        this.f3476a = iUiListener;
        if (a(activity, fragment, z)) {
            n.c(n.d, "OpenUi, showUi, return Constants.UI_ACTIVITY");
            d.a().a(this.mToken.getOpenId(), this.mToken.getAppId(), "2", "1", "5", "0", "0", "0");
            return 1;
        }
        d.a().a(this.mToken.getOpenId(), this.mToken.getAppId(), "2", "1", "5", "1", "0", "0");
        n.d(n.d, "startActivity fail show dialog.");
        this.f3476a = new FeedConfirmListener(this.f3476a);
        return a(z, this.f3476a);
    }

    public void releaseResource() {
        this.c = null;
        this.f3476a = null;
    }

    private int a(boolean z, IUiListener iUiListener) {
        n.c(n.d, "OpenUi, showDialog -- start");
        CookieSyncManager.createInstance(Global.getContext());
        Bundle composeCGIParams = composeCGIParams();
        if (z) {
            composeCGIParams.putString("isadd", "1");
        }
        composeCGIParams.putString(Constants.PARAM_SCOPE, this.b);
        composeCGIParams.putString(Constants.PARAM_CLIENT_ID, this.mToken.getAppId());
        if (isOEM) {
            composeCGIParams.putString(Constants.PARAM_PLATFORM_ID, "desktop_m_qq-" + installChannel + "-" + "android" + "-" + registerChannel + "-" + businessId);
        } else {
            composeCGIParams.putString(Constants.PARAM_PLATFORM_ID, Constants.DEFAULT_PF);
        }
        String str = (System.currentTimeMillis() / 1000) + Constants.STR_EMPTY;
        composeCGIParams.putString("sign", SystemUtils.getAppSignatureMD5(Global.getContext(), str));
        composeCGIParams.putString("time", str);
        composeCGIParams.putString("display", "mobile");
        composeCGIParams.putString("response_type", "token");
        composeCGIParams.putString("redirect_uri", ServerSetting.DEFAULT_REDIRECT_URI);
        composeCGIParams.putString("cancel_display", "1");
        composeCGIParams.putString("switch", "1");
        composeCGIParams.putString("status_userip", Util.getUserIp());
        final String str2 = ServerSetting.getInstance().getEnvUrl(Global.getContext(), ServerSetting.DEFAULT_CGI_AUTHORIZE) + Util.encodeUrl(composeCGIParams);
        final TokenListener tokenListener = new TokenListener(Global.getContext(), iUiListener, true, false);
        n.b(n.d, "OpenUi, showDialog TDialog");
        ThreadManager.executeOnSubThread(new Runnable() {
            public void run() {
                SystemUtils.extractSecureLib(AuthAgent.SECURE_LIB_FILE_NAME, AuthAgent.SECURE_LIB_NAME, 2);
                AuthAgent.this.c.runOnUiThread(new Runnable() {
                    public void run() {
                        new AuthDialog(AuthAgent.this.c, SystemUtils.ACTION_LOGIN, str2, tokenListener, AuthAgent.this.mToken).show();
                    }
                });
            }
        });
        n.c(n.d, "OpenUi, showDialog -- end");
        return 2;
    }

    private boolean a(Activity activity, Fragment fragment, boolean z) {
        n.c(n.d, "startActionActivity() -- start");
        Intent targetActivityIntent = getTargetActivityIntent("com.tencent.open.agent.AgentActivity");
        if (targetActivityIntent != null) {
            Bundle composeCGIParams = composeCGIParams();
            if (z) {
                composeCGIParams.putString("isadd", "1");
            }
            composeCGIParams.putString(Constants.PARAM_SCOPE, this.b);
            composeCGIParams.putString(Constants.PARAM_CLIENT_ID, this.mToken.getAppId());
            if (isOEM) {
                composeCGIParams.putString(Constants.PARAM_PLATFORM_ID, "desktop_m_qq-" + installChannel + "-" + "android" + "-" + registerChannel + "-" + businessId);
            } else {
                composeCGIParams.putString(Constants.PARAM_PLATFORM_ID, Constants.DEFAULT_PF);
            }
            composeCGIParams.putString("need_pay", "1");
            composeCGIParams.putString(Constants.KEY_APP_NAME, SystemUtils.getAppName(Global.getContext()));
            targetActivityIntent.putExtra(Constants.KEY_ACTION, SystemUtils.ACTION_LOGIN);
            targetActivityIntent.putExtra(Constants.KEY_PARAMS, composeCGIParams);
            this.mActivityIntent = targetActivityIntent;
            if (hasActivityForIntent()) {
                this.f3476a = new FeedConfirmListener(this.f3476a);
                if (fragment != null) {
                    n.b("AuthAgent", "startAssitActivity fragment");
                    startAssitActivity(fragment, this.f3476a);
                } else {
                    n.b("AuthAgent", "startAssitActivity activity");
                    startAssitActivity(activity, this.f3476a);
                }
                n.c(n.d, "startActionActivity() -- end");
                d.a().a(0, "LOGIN_CHECK_SDK", Constants.DEFAULT_UIN, this.mToken.getAppId(), Constants.STR_EMPTY, Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, Constants.STR_EMPTY);
                return true;
            }
        }
        d.a().a(1, "LOGIN_CHECK_SDK", Constants.DEFAULT_UIN, this.mToken.getAppId(), Constants.STR_EMPTY, Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "startActionActivity fail");
        n.c(n.d, "startActionActivity() -- end");
        return false;
    }

    /* access modifiers changed from: protected */
    public void a(IUiListener iUiListener) {
        n.c(n.d, "reportDAU() -- start");
        String accessToken = this.mToken.getAccessToken();
        String openId = this.mToken.getOpenId();
        String appId = this.mToken.getAppId();
        String str = Constants.STR_EMPTY;
        if (!TextUtils.isEmpty(accessToken) && !TextUtils.isEmpty(openId) && !TextUtils.isEmpty(appId)) {
            str = Util.encrypt("tencent&sdk&qazxc***14969%%" + accessToken + appId + openId + "qzone3.4");
        }
        if (TextUtils.isEmpty(str)) {
            n.e(n.d, "reportDAU -- encrytoken is null");
            return;
        }
        Bundle composeCGIParams = composeCGIParams();
        composeCGIParams.putString("encrytoken", str);
        HttpUtils.requestAsync(this.mToken, Global.getContext(), "https://openmobile.qq.com/user/user_login_statis", composeCGIParams, Constants.HTTP_POST, null);
        n.c(n.d, "reportDAU() -- end");
    }

    /* access modifiers changed from: protected */
    public void b(IUiListener iUiListener) {
        Bundle composeCGIParams = composeCGIParams();
        composeCGIParams.putString("reqType", "checkLogin");
        HttpUtils.requestAsync(this.mToken, Global.getContext(), "https://openmobile.qq.com/v3/user/get_info", composeCGIParams, Constants.HTTP_GET, new BaseApi.TempRequestListener(new CheckLoginListener(iUiListener)));
    }

    public void onActivityResult(Activity activity, int i, int i2, Intent intent) {
        IUiListener iUiListener;
        ThreadManager.executeOnSubThread(new Runnable() {
            public void run() {
                Global.saveVersionCode();
            }
        });
        Iterator it = this.mTaskList.iterator();
        while (true) {
            if (!it.hasNext()) {
                iUiListener = null;
                break;
            }
            BaseApi.ApiTask apiTask = (BaseApi.ApiTask) it.next();
            if (apiTask.mRequestCode == i) {
                IUiListener iUiListener2 = apiTask.mListener;
                this.mTaskList.remove(apiTask);
                iUiListener = iUiListener2;
                break;
            }
        }
        if (intent != null) {
            a(intent.getStringExtra(Constants.KEY_RESPONSE));
            if (iUiListener == null) {
                AssistActivity.setResultDataForLogin(activity, intent);
                return;
            }
            if (i2 == -1) {
                handleDataToListener(intent, iUiListener);
            } else {
                n.b(n.d, "OpenUi, onActivityResult, Constants.ACTIVITY_CANCEL");
                iUiListener.onCancel();
            }
            releaseResource();
            n.b();
        } else if (iUiListener != null) {
            iUiListener.onCancel();
        }
    }

    private void a(String str) {
        try {
            JSONObject parseJson = Util.parseJson(str);
            String string = parseJson.getString(Constants.PARAM_ACCESS_TOKEN);
            String string2 = parseJson.getString(Constants.PARAM_EXPIRES_IN);
            String string3 = parseJson.getString("openid");
            if (!TextUtils.isEmpty(string) && !TextUtils.isEmpty(string2) && !TextUtils.isEmpty(string3)) {
                this.mToken.setAccessToken(string, string2);
                this.mToken.setOpenId(string3);
            }
        } catch (Exception e) {
        }
    }

    /* compiled from: ProGuard */
    class CheckLoginListener implements IUiListener {

        /* renamed from: a  reason: collision with root package name */
        IUiListener f3480a;

        public CheckLoginListener(IUiListener iUiListener) {
            this.f3480a = iUiListener;
        }

        public void onComplete(Object obj) {
            if (obj == null) {
                n.e("CheckLoginListener", "response data is null");
                return;
            }
            JSONObject jSONObject = (JSONObject) obj;
            try {
                int i = jSONObject.getInt("ret");
                String string = i == 0 ? "success" : jSONObject.getString(SocialConstants.PARAM_SEND_MSG);
                if (this.f3480a != null) {
                    this.f3480a.onComplete(new JSONObject().put("ret", i).put(SocialConstants.PARAM_SEND_MSG, string));
                }
            } catch (JSONException e) {
                e.printStackTrace();
                n.e("CheckLoginListener", "response data format error");
            }
        }

        public void onError(UiError uiError) {
            if (this.f3480a != null) {
                this.f3480a.onError(uiError);
            }
        }

        public void onCancel() {
            if (this.f3480a != null) {
                this.f3480a.onCancel();
            }
        }
    }

    /* compiled from: ProGuard */
    class FeedConfirmListener implements IUiListener {

        /* renamed from: a  reason: collision with root package name */
        IUiListener f3481a;
        private final String c = "sendinstall";
        private final String d = "installwording";
        private final String e = "http://appsupport.qq.com/cgi-bin/qzapps/mapp_addapp.cgi";

        public FeedConfirmListener(IUiListener iUiListener) {
            this.f3481a = iUiListener;
        }

        public void onComplete(Object obj) {
            JSONObject jSONObject;
            boolean z;
            String str;
            boolean z2 = false;
            if (obj != null && (jSONObject = (JSONObject) obj) != null) {
                try {
                    if (jSONObject.getInt("sendinstall") == 1) {
                        z2 = true;
                    }
                    str = jSONObject.getString("installwording");
                    z = z2;
                } catch (JSONException e2) {
                    n.d("FeedConfirm", "There is no value for sendinstall.");
                    z = false;
                    str = Constants.STR_EMPTY;
                }
                String decode = URLDecoder.decode(str);
                n.b("TAG", " WORDING = " + decode + "xx");
                if (z && !TextUtils.isEmpty(decode)) {
                    a(decode, this.f3481a, obj);
                } else if (this.f3481a != null) {
                    this.f3481a.onComplete(obj);
                }
            }
        }

        /* compiled from: ProGuard */
        abstract class ButtonListener implements View.OnClickListener {
            Dialog d;

            ButtonListener(Dialog dialog) {
                this.d = dialog;
            }
        }

        private void a(String str, final IUiListener iUiListener, final Object obj) {
            PackageInfo packageInfo;
            Drawable drawable = null;
            Dialog dialog = new Dialog(AuthAgent.this.c);
            dialog.requestWindowFeature(1);
            PackageManager packageManager = AuthAgent.this.c.getPackageManager();
            try {
                packageInfo = packageManager.getPackageInfo(AuthAgent.this.c.getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
                packageInfo = null;
            }
            if (packageInfo != null) {
                drawable = packageInfo.applicationInfo.loadIcon(packageManager);
            }
            AnonymousClass1 r4 = new ButtonListener(dialog) {
                public void onClick(View view) {
                    FeedConfirmListener.this.a();
                    if (this.d != null && this.d.isShowing()) {
                        this.d.dismiss();
                    }
                    if (iUiListener != null) {
                        iUiListener.onComplete(obj);
                    }
                }
            };
            AnonymousClass2 r5 = new ButtonListener(dialog) {
                public void onClick(View view) {
                    if (this.d != null && this.d.isShowing()) {
                        this.d.dismiss();
                    }
                    if (iUiListener != null) {
                        iUiListener.onComplete(obj);
                    }
                }
            };
            ColorDrawable colorDrawable = new ColorDrawable();
            colorDrawable.setAlpha(0);
            dialog.getWindow().setBackgroundDrawable(colorDrawable);
            dialog.setContentView(a(AuthAgent.this.c, drawable, str, r4, r5));
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialogInterface) {
                    if (iUiListener != null) {
                        iUiListener.onComplete(obj);
                    }
                }
            });
            if (AuthAgent.this.c != null && !AuthAgent.this.c.isFinishing()) {
                dialog.show();
            }
        }

        private Drawable a(String str, Context context) {
            IOException e2;
            Drawable drawable;
            try {
                InputStream open = context.getApplicationContext().getAssets().open(str);
                if (open == null) {
                    return null;
                }
                if (str.endsWith(".9.png")) {
                    Bitmap decodeStream = BitmapFactory.decodeStream(open);
                    if (decodeStream == null) {
                        return null;
                    }
                    byte[] ninePatchChunk = decodeStream.getNinePatchChunk();
                    NinePatch.isNinePatchChunk(ninePatchChunk);
                    return new NinePatchDrawable(decodeStream, ninePatchChunk, new Rect(), null);
                }
                drawable = Drawable.createFromStream(open, str);
                try {
                    open.close();
                    return drawable;
                } catch (IOException e3) {
                    e2 = e3;
                }
            } catch (IOException e4) {
                IOException iOException = e4;
                drawable = null;
                e2 = iOException;
            }
            e2.printStackTrace();
            return drawable;
        }

        private View a(Context context, Drawable drawable, String str, View.OnClickListener onClickListener, View.OnClickListener onClickListener2) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            float f = displayMetrics.density;
            RelativeLayout relativeLayout = new RelativeLayout(context);
            ImageView imageView = new ImageView(context);
            imageView.setImageDrawable(drawable);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setId(1);
            int i = (int) (14.0f * f);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (60.0f * f), (int) (60.0f * f));
            layoutParams.addRule(9);
            layoutParams.setMargins(0, (int) (18.0f * f), (int) (6.0f * f), (int) (18.0f * f));
            relativeLayout.addView(imageView, layoutParams);
            TextView textView = new TextView(context);
            textView.setText(str);
            textView.setTextSize(14.0f);
            textView.setGravity(3);
            textView.setIncludeFontPadding(false);
            textView.setPadding(0, 0, 0, 0);
            textView.setLines(2);
            textView.setId(5);
            textView.setMinWidth((int) (185.0f * f));
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams2.addRule(1, 1);
            layoutParams2.addRule(6, 1);
            int i2 = (int) (10.0f * f);
            layoutParams2.setMargins(0, 0, (int) (5.0f * f), 0);
            relativeLayout.addView(textView, layoutParams2);
            View view = new View(context);
            view.setBackgroundColor(Color.rgb(214, 214, 214));
            view.setId(3);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, 2);
            layoutParams3.addRule(3, 1);
            layoutParams3.addRule(5, 1);
            layoutParams3.addRule(7, 5);
            layoutParams3.setMargins(0, 0, 0, (int) (12.0f * f));
            relativeLayout.addView(view, layoutParams3);
            LinearLayout linearLayout = new LinearLayout(context);
            RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams4.addRule(5, 1);
            layoutParams4.addRule(7, 5);
            layoutParams4.addRule(3, 3);
            Button button = new Button(context);
            button.setText("跳过");
            button.setBackgroundDrawable(a("buttonNegt.png", context));
            button.setTextColor(Color.rgb(36, 97, 131));
            button.setTextSize(20.0f);
            button.setOnClickListener(onClickListener2);
            button.setId(4);
            LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(0, (int) (45.0f * f));
            layoutParams5.rightMargin = (int) (14.0f * f);
            layoutParams5.leftMargin = (int) (4.0f * f);
            layoutParams5.weight = 1.0f;
            linearLayout.addView(button, layoutParams5);
            Button button2 = new Button(context);
            button2.setText("确定");
            button2.setTextSize(20.0f);
            button2.setTextColor(Color.rgb(255, 255, 255));
            button2.setBackgroundDrawable(a("buttonPost.png", context));
            button2.setOnClickListener(onClickListener);
            LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(0, (int) (45.0f * f));
            layoutParams6.weight = 1.0f;
            layoutParams6.rightMargin = (int) (4.0f * f);
            linearLayout.addView(button2, layoutParams6);
            relativeLayout.addView(linearLayout, layoutParams4);
            FrameLayout.LayoutParams layoutParams7 = new FrameLayout.LayoutParams((int) (279.0f * f), (int) (163.0f * f));
            relativeLayout.setPadding((int) (14.0f * f), 0, (int) (12.0f * f), (int) (12.0f * f));
            relativeLayout.setLayoutParams(layoutParams7);
            relativeLayout.setBackgroundColor(Color.rgb(247, 251, 247));
            PaintDrawable paintDrawable = new PaintDrawable(Color.rgb(247, 251, 247));
            paintDrawable.setCornerRadius(f * 5.0f);
            relativeLayout.setBackgroundDrawable(paintDrawable);
            return relativeLayout;
        }

        /* access modifiers changed from: protected */
        public void a() {
            HttpUtils.requestAsync(AuthAgent.this.mToken, AuthAgent.this.c, "http://appsupport.qq.com/cgi-bin/qzapps/mapp_addapp.cgi", AuthAgent.this.composeActivityParams(), Constants.HTTP_POST, null);
        }

        public void onError(UiError uiError) {
            if (this.f3481a != null) {
                this.f3481a.onError(uiError);
            }
        }

        public void onCancel() {
            if (this.f3481a != null) {
                this.f3481a.onCancel();
            }
        }
    }
}
