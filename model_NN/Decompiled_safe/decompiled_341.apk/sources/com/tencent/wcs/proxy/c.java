package com.tencent.wcs.proxy;

import android.content.Context;
import android.content.IntentFilter;
import com.tencent.wcs.agent.a;
import com.tencent.wcs.agent.e;
import com.tencent.wcs.b.b;
import com.tencent.wcs.proxy.a.d;
import com.tencent.wcs.proxy.c.f;
import com.tencent.wcs.proxy.heartbeat.HeartBeatService;

/* compiled from: ProGuard */
public class c implements b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f3906a;
    /* access modifiers changed from: private */
    public l b;
    /* access modifiers changed from: private */
    public HeartBeatService c;
    /* access modifiers changed from: private */
    public b d;
    /* access modifiers changed from: private */
    public a e;
    /* access modifiers changed from: private */
    public e f;
    private com.tencent.wcs.a.b g;
    private com.tencent.wcs.agent.config.a h;
    private com.tencent.wcs.agent.config.a i;
    /* access modifiers changed from: private */
    public com.tencent.wcs.e.a j;
    /* access modifiers changed from: private */
    public volatile boolean k = false;
    /* access modifiers changed from: private */
    public final Object l = new Object();
    /* access modifiers changed from: private */
    public volatile boolean m = false;
    private volatile boolean n = false;
    /* access modifiers changed from: private */
    public volatile int o = 0;
    /* access modifiers changed from: private */
    public volatile com.tencent.wcs.proxy.d.a p;
    private j q;
    private com.tencent.wcs.proxy.a.a r;
    private d s;
    private com.tencent.wcs.proxy.a.b t;
    private com.tencent.wcs.d.a u;

    static /* synthetic */ int j(c cVar) {
        int i2 = cVar.o;
        cVar.o = i2 + 1;
        return i2;
    }

    public c(Context context) {
        this.f3906a = context;
        this.u = new com.tencent.wcs.d.a(this.f3906a);
        this.b = new l(this.f3906a, this);
        this.c = new HeartBeatService(this.f3906a, this.b);
        this.d = new b(this.f3906a, this.b);
        this.h = new com.tencent.wcs.agent.config.b();
        this.i = new com.tencent.wcs.agent.config.c();
        this.g = new com.tencent.wcs.a.b();
        this.j = new com.tencent.wcs.e.a(this.b);
        this.e = new a(this.b, this.g, this.h, this.j);
        this.f = new e(this.b, this.g, this.i);
        this.r = new com.tencent.wcs.proxy.a.a();
        this.s = new d(this.f3906a);
        this.t = new com.tencent.wcs.proxy.a.b();
        this.q = new j(this, null);
        x();
    }

    public void a(com.tencent.wcs.proxy.d.a aVar) {
        this.p = aVar;
        if (this.b != null) {
            this.b.a(aVar);
        }
    }

    /* access modifiers changed from: private */
    public void q() {
        if (this.b != null && this.f3906a != null) {
            if (this.s != null && this.s.a(this.f3906a)) {
                this.b.a(this.s);
            } else if (this.t == null || !this.t.a(this.f3906a)) {
                this.b.a(this.r);
            } else {
                this.b.a(this.t);
                this.e.c();
                this.f.a();
            }
        }
    }

    public void a() {
        q();
        s();
        if (!e()) {
            b();
        }
    }

    public void b() {
        com.tencent.wcs.c.b.a("WanServiceManager start wireless service");
        com.tencent.wcs.proxy.e.a.a().a(new d(this));
    }

    public void c() {
        if (this.m) {
            com.tencent.wcs.c.b.a("WanServiceManager stop wireless service begin...");
            this.m = false;
            com.tencent.wcs.proxy.e.a.a().a(new g(this));
        }
    }

    public void d() {
        t();
        c();
    }

    /* access modifiers changed from: private */
    public void r() {
        com.tencent.wcs.proxy.e.a.a().a(new i(this));
    }

    public boolean e() {
        return this.m;
    }

    private void s() {
        if (this.f3906a != null && this.q != null && !this.n) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            this.f3906a.registerReceiver(this.q, intentFilter);
            this.n = true;
        }
    }

    private void t() {
        if (this.f3906a != null && this.q != null && this.n) {
            this.f3906a.unregisterReceiver(this.q);
            this.n = false;
        }
    }

    public void a(f fVar) {
        if (this.e != null) {
            this.e.c();
        }
        if (this.f != null) {
            this.f.a();
        }
    }

    public void f() {
        if (this.b != null) {
            this.b.f3923a = false;
        }
        u();
        w();
    }

    public void g() {
        v();
    }

    private void u() {
        if (this.c != null) {
            this.c.a();
        }
    }

    private void v() {
        if (this.c != null) {
            this.c.b();
        }
    }

    private void w() {
        if (this.d != null) {
            this.d.a();
        }
    }

    private void x() {
        if (this.d != null) {
            this.d.b();
        }
    }

    public void h() {
        if (this.g != null) {
            this.g.b();
        }
    }

    public void i() {
        if (this.g != null) {
            this.g.a();
        }
    }

    public void j() {
        if (this.b != null) {
            this.b.k();
        }
    }

    public void k() {
        if (this.b != null) {
            this.b.l();
        }
    }

    public void l() {
        if (this.b != null) {
            this.b.m();
        }
    }

    public void m() {
        if (this.b != null) {
            this.b.n();
        }
    }

    public void n() {
        if (this.b != null) {
            this.b.o();
        }
    }

    public boolean o() {
        if (this.b != null) {
            return this.b.b();
        }
        return false;
    }

    public boolean p() {
        if (this.b != null) {
            return this.b.e();
        }
        return false;
    }
}
