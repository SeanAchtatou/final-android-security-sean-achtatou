package com.tencent.wcs.proxy.c;

import com.qq.AppService.s;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.protocol.l;

/* compiled from: ProGuard */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private int f3910a;
    private JceStruct b;
    private byte[] c;

    public int a() {
        if (this.c != null) {
            return this.c.length + 4;
        }
        return 0;
    }

    public int b() {
        return this.f3910a;
    }

    public JceStruct c() {
        return this.b;
    }

    public byte[] d() {
        return s.a(this.f3910a);
    }

    public byte[] e() {
        if (this.c != null) {
            return this.c;
        }
        return new byte[0];
    }

    public void a(int i) {
        this.f3910a = i;
    }

    public void a(JceStruct jceStruct) {
        this.b = jceStruct;
        if (jceStruct != null) {
            this.c = l.b(jceStruct);
        } else {
            this.c = null;
        }
    }
}
