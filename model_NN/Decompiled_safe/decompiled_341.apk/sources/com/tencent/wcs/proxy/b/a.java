package com.tencent.wcs.proxy.b;

import java.lang.ref.SoftReference;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public abstract class a<T, P> {

    /* renamed from: a  reason: collision with root package name */
    private int f3897a;
    private Queue<SoftReference<T>> b;

    /* access modifiers changed from: protected */
    public abstract T a(Object... objArr);

    /* access modifiers changed from: protected */
    public abstract boolean a(Object obj);

    /* access modifiers changed from: protected */
    public abstract boolean b(Object... objArr);

    /* access modifiers changed from: protected */
    public abstract void c(Object obj);

    protected a() {
        if (this.b == null) {
            this.b = new ConcurrentLinkedQueue();
        }
        this.f3897a = 0;
    }

    public T c(Object... objArr) {
        T t;
        if (b(objArr)) {
            return a(objArr);
        }
        if (this.b == null || this.b.isEmpty()) {
            return a(objArr);
        }
        SoftReference poll = this.b.poll();
        if (poll != null) {
            t = poll.get();
        } else {
            t = null;
        }
        if (t == null) {
            return a(objArr);
        }
        return t;
    }

    public synchronized void b(Object obj) {
        c(obj);
        if (this.b != null && this.f3897a < 50 && a(obj)) {
            this.b.offer(new SoftReference(obj));
            this.f3897a++;
        }
    }

    public void a() {
        if (this.b != null) {
            this.b.clear();
        }
    }
}
