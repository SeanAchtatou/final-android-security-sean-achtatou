package com.tencent.wcs.c;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static volatile a f3848a = null;

    public static void a(String str) {
        if (f3848a == null) {
            f3848a = new a();
            f3848a.setDaemon(true);
            f3848a.start();
        }
        f3848a.a(str);
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0024 A[SYNTHETIC, Splitter:B:15:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0031 A[SYNTHETIC, Splitter:B:23:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:41:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.Exception r4) {
        /*
            r1 = 0
            java.io.StringWriter r2 = new java.io.StringWriter     // Catch:{ Exception -> 0x002d, all -> 0x0020 }
            r2.<init>()     // Catch:{ Exception -> 0x002d, all -> 0x0020 }
            java.io.PrintWriter r0 = new java.io.PrintWriter     // Catch:{ Exception -> 0x004d, all -> 0x0046 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x004d, all -> 0x0046 }
            r4.printStackTrace(r0)     // Catch:{ Exception -> 0x0051, all -> 0x0048 }
            java.lang.String r1 = r2.toString()     // Catch:{ Exception -> 0x0051, all -> 0x0048 }
            a(r1)     // Catch:{ Exception -> 0x0051, all -> 0x0048 }
            if (r2 == 0) goto L_0x001a
            r2.close()     // Catch:{ IOException -> 0x0041 }
        L_0x001a:
            if (r0 == 0) goto L_0x001f
        L_0x001c:
            r0.close()
        L_0x001f:
            return
        L_0x0020:
            r0 = move-exception
            r2 = r1
        L_0x0022:
            if (r2 == 0) goto L_0x0027
            r2.close()     // Catch:{ IOException -> 0x003c }
        L_0x0027:
            if (r1 == 0) goto L_0x002c
            r1.close()
        L_0x002c:
            throw r0
        L_0x002d:
            r0 = move-exception
            r0 = r1
        L_0x002f:
            if (r1 == 0) goto L_0x0034
            r1.close()     // Catch:{ IOException -> 0x0037 }
        L_0x0034:
            if (r0 == 0) goto L_0x001f
            goto L_0x001c
        L_0x0037:
            r1 = move-exception
            r4.printStackTrace()
            goto L_0x0034
        L_0x003c:
            r2 = move-exception
            r4.printStackTrace()
            goto L_0x0027
        L_0x0041:
            r1 = move-exception
            r4.printStackTrace()
            goto L_0x001a
        L_0x0046:
            r0 = move-exception
            goto L_0x0022
        L_0x0048:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x0022
        L_0x004d:
            r0 = move-exception
            r0 = r1
            r1 = r2
            goto L_0x002f
        L_0x0051:
            r1 = move-exception
            r1 = r2
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wcs.c.b.a(java.lang.Exception):void");
    }
}
