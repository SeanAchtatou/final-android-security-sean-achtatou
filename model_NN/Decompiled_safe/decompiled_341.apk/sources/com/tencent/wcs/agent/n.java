package com.tencent.wcs.agent;

import com.tencent.wcs.agent.config.RemoteType;
import com.tencent.wcs.b.c;
import com.tencent.wcs.c.b;
import java.io.IOException;

/* compiled from: ProGuard */
class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f3841a;

    n(l lVar) {
        this.f3841a = lVar;
    }

    public void run() {
        try {
            if (this.f3841a.k.b() == RemoteType.WEB) {
                this.f3841a.g();
            } else if (this.f3841a.k.b() == RemoteType.PC) {
                this.f3841a.f();
            }
        } catch (IOException e) {
            e.printStackTrace();
            if (!this.f3841a.g) {
                if (c.f3846a) {
                    b.a("AgentSessionOriginal read input exception channelId " + this.f3841a.m + " messageId " + this.f3841a.n + " port " + this.f3841a.q + " remote " + this.f3841a.k.a() + ", close it now");
                }
                this.f3841a.a(null, l.h(this.f3841a), 2);
                if (this.f3841a.u != null) {
                    this.f3841a.u.a(this.f3841a.n);
                }
            }
        }
    }
}
