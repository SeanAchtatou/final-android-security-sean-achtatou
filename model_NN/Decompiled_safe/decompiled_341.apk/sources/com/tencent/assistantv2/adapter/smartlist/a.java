package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.util.Pair;
import android.view.View;
import com.tencent.assistant.model.e;

/* compiled from: ProGuard */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    protected Context f2908a;
    protected ab b;

    public abstract Pair<View, Object> a();

    public abstract void a(View view, Object obj, int i, e eVar);

    public a(Context context, ab abVar) {
        this.f2908a = context;
        this.b = abVar;
        if (this.b == null) {
            this.b = new ab();
        }
    }
}
