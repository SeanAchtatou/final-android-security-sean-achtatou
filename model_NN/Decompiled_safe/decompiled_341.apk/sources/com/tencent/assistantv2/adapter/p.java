package com.tencent.assistantv2.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class p extends PagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<View> f2900a = new ArrayList<>();

    public int getCount() {
        return this.f2900a.size();
    }

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    public void a(List<View> list) {
        if (list != null) {
            this.f2900a.clear();
            this.f2900a.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView(this.f2900a.get(i));
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        try {
            viewGroup.addView(this.f2900a.get(i));
        } catch (Exception e) {
        }
        return this.f2900a.get(i);
    }
}
