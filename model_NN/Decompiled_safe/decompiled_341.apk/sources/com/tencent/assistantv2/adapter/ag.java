package com.tencent.assistantv2.adapter;

import android.widget.ImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistantv2.component.y;

/* compiled from: ProGuard */
class ag extends y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankNormalListAdapter f2881a;

    ag(RankNormalListAdapter rankNormalListAdapter) {
        this.f2881a = rankNormalListAdapter;
    }

    public void a(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            a.a().a(downloadInfo);
            com.tencent.assistant.utils.a.a((ImageView) this.f2881a.i.findViewWithTag(downloadInfo.downloadTicket));
        }
    }
}
