package com.tencent.assistantv2.adapter.smartlist;

import android.view.View;
import android.widget.TextView;
import com.tencent.assistant.adapter.a.c;
import com.tencent.assistant.component.HorizonMultiImageView;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.ListItemRelateNewsView;
import com.tencent.assistantv2.component.ListRecommendReasonView;

/* compiled from: ProGuard */
public class v {
    public TextView j;
    public TXAppIconView k;
    public TextView l;
    public DownloadButton m;
    public ListItemInfoView n;
    public TextView o;
    public ListRecommendReasonView p;
    public HorizonMultiImageView q;
    public View r;
    public c s;
    public TextView t;
    public TextView u;
    public ListItemRelateNewsView v;
}
