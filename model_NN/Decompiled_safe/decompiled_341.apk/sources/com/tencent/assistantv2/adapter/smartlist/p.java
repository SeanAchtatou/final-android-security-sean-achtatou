package com.tencent.assistantv2.adapter.smartlist;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.ListRecommendReasonView;

/* compiled from: ProGuard */
class p {

    /* renamed from: a  reason: collision with root package name */
    TextView f2924a;
    TXAppIconView b;
    TextView c;
    DownloadButton d;
    ListItemInfoView e;
    TextView f;
    ListRecommendReasonView g;
    View h;
    ImageView i;
    View j;
    final /* synthetic */ n k;

    private p(n nVar) {
        this.k = nVar;
    }
}
