package com.tencent.assistantv2.adapter.smartlist;

import android.widget.TextView;
import com.tencent.assistant.adapter.a.c;
import com.tencent.assistant.adapter.a.d;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class t implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f2927a;

    t(s sVar) {
        this.f2927a = sVar;
    }

    public c a(Object obj) {
        if (obj != null && (obj instanceof v)) {
            v vVar = (v) obj;
            if (vVar.s != null) {
                return vVar.s;
            }
        }
        return null;
    }

    public c b(Object obj) {
        if (obj == null || !(obj instanceof v)) {
            return null;
        }
        return ((v) obj).s;
    }

    public TextView c(Object obj) {
        if (obj != null && (obj instanceof v)) {
            v vVar = (v) obj;
            if (vVar.s != null) {
                return vVar.s.h;
            }
        }
        return null;
    }

    public TextView d(Object obj) {
        if (obj == null || !(obj instanceof v)) {
            return null;
        }
        return ((v) obj).t;
    }

    public String a() {
        if (this.f2927a.b == null || this.f2927a.b.e() == null) {
            return Constants.STR_EMPTY;
        }
        return this.f2927a.b.e().contentId;
    }
}
