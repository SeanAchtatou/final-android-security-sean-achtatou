package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.e;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.model.SimpleVideoModel;

/* compiled from: ProGuard */
public class ah extends a {
    private final String c = "VideoNormalItemView";
    private IViewInvalidater d;
    private ab e;

    public ah(Context context, ab abVar, IViewInvalidater iViewInvalidater) {
        super(context, abVar);
        this.d = iViewInvalidater;
        this.e = abVar;
    }

    public Pair<View, Object> a() {
        View inflate = LayoutInflater.from(this.f2908a).inflate((int) R.layout.video_normal_universal_item, (ViewGroup) null);
        ak akVar = new ak();
        akVar.f2915a = (TXAppIconView) inflate.findViewById(R.id.video_icon);
        akVar.f2915a.setInvalidater(this.d);
        akVar.b = (TextView) inflate.findViewById(R.id.video_title);
        akVar.c = (TextView) inflate.findViewById(R.id.video_update_info);
        akVar.d = (TextView) inflate.findViewById(R.id.video_view_count);
        akVar.e = (TextView) inflate.findViewById(R.id.video_desc);
        return Pair.create(inflate, akVar);
    }

    public void a(View view, Object obj, int i, e eVar) {
        SimpleVideoModel simpleVideoModel;
        ak akVar = (ak) obj;
        if (eVar != null) {
            simpleVideoModel = eVar.d();
        } else {
            simpleVideoModel = null;
        }
        view.setOnClickListener(new aj(this, this.f2908a, i, simpleVideoModel, this.b));
        a(akVar, simpleVideoModel, i);
    }

    private void a(ak akVar, SimpleVideoModel simpleVideoModel, int i) {
        if (simpleVideoModel != null && akVar != null) {
            akVar.f2915a.updateImageView(simpleVideoModel.c, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            akVar.b.setText(simpleVideoModel.b);
            if (!TextUtils.isEmpty(ct.f(simpleVideoModel.i))) {
                akVar.c.setPadding(0, 0, df.a(this.f2908a, 10.0f), 0);
                akVar.c.setText(ct.f(simpleVideoModel.i));
            } else {
                akVar.c.setPadding(0, 0, 0, 0);
            }
            akVar.d.setText(simpleVideoModel.h);
            akVar.e.setText(ct.e(simpleVideoModel.f));
        }
    }
}
