package com.tencent.assistantv2.component.search;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.protocol.jce.ExplicitHotWord;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.component.CycleShowView;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/* compiled from: ProGuard */
public class SearchTxtCycleShowView extends CycleShowView {

    /* renamed from: a  reason: collision with root package name */
    private final String f3232a;
    private List<ExplicitHotWord> b;
    private List<ExplicitHotWord> c;
    private ExplicitHotWord d;
    private volatile boolean e;
    /* access modifiers changed from: private */
    public volatile boolean f;
    private int g;
    private volatile int h;
    private ExplicitHotWord i;
    private boolean j;
    private boolean k;
    private final long l;
    private String m;
    private b n;
    private int o;
    private Runnable p;
    private Runnable q;
    private boolean r;

    public SearchTxtCycleShowView(Context context) {
        this(context, null);
    }

    public SearchTxtCycleShowView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3232a = "SearchTxtCycleShowView";
        this.e = true;
        this.f = false;
        this.h = -1;
        this.i = null;
        this.j = true;
        this.k = false;
        this.l = 5000;
        this.m = Constants.STR_EMPTY;
        this.n = new b();
        this.o = -9999;
        this.p = new q(this);
        this.q = new r(this);
        this.r = true;
        m();
        this.f = false;
        this.h = -1;
        this.j = true;
        this.e = true;
        this.d = new ExplicitHotWord();
        this.d.b = getResources().getString(R.string.search_explicit_word_default_copywriting);
        this.d.f2067a = null;
        this.c = new ArrayList();
        this.c.add(this.d);
    }

    public void a(List<ExplicitHotWord> list, String str) {
        this.m = str;
        if (this.b == null || !this.b.equals(list)) {
            this.k = false;
            XLog.d("SearchTxtCycleShowView", "setContent:" + (list == null ? 0 : list.size()));
            this.b = list;
            if (this.b == null || this.b.isEmpty()) {
                this.e = true;
                this.c.clear();
                this.c.add(this.d);
                return;
            }
            this.e = false;
            this.c = this.b;
            return;
        }
        XLog.d("SearchTxtCycleShowView", "updateData same datas, ingore");
        this.k = true;
    }

    public void b(List<ExplicitHotWord> list, String str) {
        a(list, str);
        l();
    }

    public int c() {
        if (this.c == null) {
            return 0;
        }
        return this.c.size();
    }

    public Object a(int i2) {
        if (this.c == null || i2 < 0 || i2 > this.c.size() - 1) {
            return null;
        }
        return this.c.get(i2);
    }

    public int d() {
        return this.g;
    }

    public Object e() {
        return this.i;
    }

    /* access modifiers changed from: private */
    public void k() {
        if (this.h == 0) {
            if (this.j || c() > 1) {
                ExplicitHotWord explicitHotWord = (ExplicitHotWord) a(i());
                if (explicitHotWord != null) {
                    View b2 = b();
                    if (b2 instanceof TextView) {
                        this.r = false;
                        ((TextView) b2).setText(explicitHotWord.b);
                        this.r = true;
                    }
                } else {
                    XLog.d("SearchTxtCycleShowView", "word == null");
                }
                this.i = explicitHotWord;
                a();
                this.j = false;
            }
            if (!this.e) {
                a(this.i, 100);
            }
            postDelayed(this.q, 5000);
        }
    }

    public void a(long j2) {
        m();
        this.f = true;
        this.h = 0;
        this.j = true;
        postDelayed(this.p, j2);
    }

    public void f() {
        XLog.d("SearchTxtCycleShowView", "onResume:isCycling:" + this.f + "  mStatus:" + this.h);
        if (this.f && this.h != 0) {
            this.h = 0;
            post(this.q);
        }
    }

    public void g() {
        XLog.d("SearchTxtCycleShowView", "onPause");
        this.h = 1;
        if (this.p != null) {
            removeCallbacks(this.p);
        }
        if (this.q != null) {
            removeCallbacks(this.q);
        }
    }

    public void h() {
        XLog.d("SearchTxtCycleShowView", "onDestory");
        m();
        this.f = false;
        this.h = -1;
        if (this.p != null) {
            removeCallbacks(this.p);
        }
        if (this.q != null) {
            removeCallbacks(this.q);
        }
    }

    public int i() {
        if (c() <= 0) {
            return 0;
        }
        int i2 = this.g + 1;
        this.g = i2;
        int c2 = i2 % c();
        this.g %= c();
        if (c2 >= 0) {
            return c2;
        }
        return 0;
    }

    private void l() {
        this.f = false;
        this.h = -1;
        if (this.p != null) {
            removeCallbacks(this.p);
        }
        if (this.q != null) {
            removeCallbacks(this.q);
        }
        if (!this.k) {
            m();
        }
        if (c() > 1) {
            ExplicitHotWord explicitHotWord = (ExplicitHotWord) a((this.g + 1) % c());
            if (this.i != null && !TextUtils.isEmpty(this.i.b) && explicitHotWord != null && this.i.b.equals(explicitHotWord.b)) {
                this.g++;
            }
        }
        this.f = true;
        this.h = 0;
        if (c() != 1 || this.i == null || TextUtils.isEmpty(this.i.b) || this.c.get(0) == null || !this.i.b.equals(this.c.get(0).b)) {
            this.j = true;
        } else {
            this.i = this.c.get(0);
            this.j = false;
            this.g = 0;
        }
        k();
    }

    private void a(ExplicitHotWord explicitHotWord, int i2) {
        STInfoV2 buildSTInfo;
        if (explicitHotWord != null && (getContext() instanceof BaseActivity) && (buildSTInfo = STInfoBuilder.buildSTInfo(getContext(), i2)) != null) {
            buildSTInfo.slotId = a.a("01", "001");
            buildSTInfo.extraData = explicitHotWord.b + ";" + explicitHotWord.f2067a + ";" + (this.g + 1);
            buildSTInfo.searchPreId = this.m;
            if (i2 == 100) {
                if (this.n == null) {
                    this.n = new b();
                }
                if (this.o != buildSTInfo.scene) {
                    this.o = buildSTInfo.scene;
                    this.n.reset();
                }
                this.n.exposureHotWord(buildSTInfo);
                return;
            }
            k.a(buildSTInfo);
        }
    }

    public String j() {
        return this.m;
    }

    public void requestLayout() {
        if (this.r) {
            super.requestLayout();
        }
    }

    private void m() {
        if (c() > 0) {
            this.g = new Random().nextInt(c()) - 1;
        } else {
            this.g = -1;
        }
    }
}
