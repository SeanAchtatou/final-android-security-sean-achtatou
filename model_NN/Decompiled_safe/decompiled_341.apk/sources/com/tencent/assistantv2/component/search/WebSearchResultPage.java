package com.tencent.assistantv2.component.search;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.manager.u;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.b;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.component.TxWebViewContainer;
import com.tencent.assistantv2.component.dz;
import com.tencent.assistantv2.component.ek;
import com.tencent.assistantv2.component.search.ISearchResultPage;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class WebSearchResultPage extends ISearchResultPage {
    private TxWebViewContainer h;
    private String i;
    private String j;
    private dz k;
    private ek l;

    public WebSearchResultPage(Context context) {
        this(context, null);
    }

    public WebSearchResultPage(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public WebSearchResultPage(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.l = new s(this);
    }

    /* access modifiers changed from: protected */
    public void a(Context context) {
        LayoutInflater.from(context).inflate((int) R.layout.search_result_web_page, this);
        this.h = (TxWebViewContainer) findViewById(R.id.result_web);
        this.h.n();
        this.h.a(this.l);
        this.h.b(getResources().getColor(R.color.apk_list_bg));
        this.k = new dz();
        if (!TextUtils.isEmpty(this.i)) {
            int i2 = 0;
            if (this.i.contains("accelerate=1")) {
                i2 = 1;
            }
            if (this.i.contains("accelerate=0")) {
                i2 = 2;
            }
            if (i2 != this.k.b) {
                this.k.b = i2;
            }
        }
        this.h.a(this.k);
        l();
    }

    private void l() {
        this.j = m();
    }

    private String m() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("qua", Global.getQUA());
            jSONObject.put("osVer", Build.VERSION.SDK_INT);
            b h2 = c.h();
            if (h2.f1925a == APN.UN_DETECT) {
                c.j();
            }
            jSONObject.put("apn", h2.f1925a);
            jSONObject.put("isWap", h2.d ? 1 : 0);
            jSONObject.put("resolution", t.b + "x" + t.c);
            jSONObject.put("androidId", t.l());
            jSONObject.put("macAdress", t.k());
            jSONObject.put("imsi", t.h());
            jSONObject.put("imei", t.g());
            jSONObject.put(CommentDetailTabView.PARAMS_VERSION_CODE, Global.getAppVersionCode());
            jSONObject.put("phoneGuid", Global.getPhoneGuid());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        XLog.d("WebSearchResultPage", jSONObject.toString());
        return jSONObject.toString();
    }

    private String n() {
        UnsupportedEncodingException e;
        String str;
        if (TextUtils.isEmpty(this.i)) {
            return this.i;
        }
        try {
            String str2 = (("keyword=" + URLEncoder.encode(this.f3224a, "utf-8")) + "&" + SocialConstants.PARAM_TYPE + "=" + b()) + "&" + "srcscene" + "=" + this.d;
            if (this.e != null) {
                str2 = str2 + "&" + "appid" + "=" + this.e.f1634a;
            }
            str = ((str2 + "&" + "onemoreapp" + "=" + u.a().a("oneMoreApp", 0)) + "&" + "mobileinfo" + "=" + URLEncoder.encode(this.j, "utf-8")) + "&" + "opentime" + "=" + System.currentTimeMillis();
            try {
                XLog.d("WebSearchResultPage", "searchtype:" + b() + " params:" + str);
            } catch (UnsupportedEncodingException e2) {
                e = e2;
                e.printStackTrace();
                XLog.d("WebSearchResultPage", String.format(this.i, str));
                return String.format(this.i, str);
            }
        } catch (UnsupportedEncodingException e3) {
            UnsupportedEncodingException unsupportedEncodingException = e3;
            str = Constants.STR_EMPTY;
            e = unsupportedEncodingException;
            e.printStackTrace();
            XLog.d("WebSearchResultPage", String.format(this.i, str));
            return String.format(this.i, str);
        }
        XLog.d("WebSearchResultPage", String.format(this.i, str));
        return String.format(this.i, str);
    }

    public void a(String str, int i2, SimpleAppModel simpleAppModel) {
        a();
        o();
        d();
        this.f = 0;
        this.e = simpleAppModel;
        this.f3224a = str;
        this.h.a("about:blank");
        this.h.a(n());
    }

    public void a(String str, int i2) {
        a();
        if (d(str)) {
            o();
            d();
            this.f = 0;
            this.f3224a = str;
            this.h.a("about:blank");
            this.h.a(n());
        }
    }

    private boolean d(String str) {
        return !TextUtils.isEmpty(str) && !str.equals(this.f3224a);
    }

    public void a(String str) {
    }

    public void b(String str) {
    }

    public ISearchResultPage.PageType h() {
        return ISearchResultPage.PageType.WEB;
    }

    private void o() {
    }

    /* access modifiers changed from: private */
    public void p() {
    }

    public void d(int i2) {
        switch (i2) {
            case 0:
                return;
            default:
                if (this.h != null) {
                    this.h.a("about:blank");
                    return;
                }
                return;
        }
    }

    public void i() {
        if (this.h != null) {
            this.h.a();
        }
    }

    public void j() {
        if (this.h != null) {
            this.h.b();
        }
    }

    public void k() {
        if (this.h != null) {
            this.h.c();
        }
    }

    public void c(String str) {
        this.i = str;
        int i2 = 0;
        if (!TextUtils.isEmpty(str)) {
            if (str.contains("accelerate=1")) {
                i2 = 1;
            }
            if (str.contains("accelerate=0")) {
                i2 = 2;
            }
        }
        if (this.h != null && this.k != null && i2 != this.k.b) {
            this.k.b = i2;
            this.h.a(this.k);
        }
    }
}
