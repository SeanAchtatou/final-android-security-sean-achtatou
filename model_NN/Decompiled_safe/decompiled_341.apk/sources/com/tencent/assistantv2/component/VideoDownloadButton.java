package com.tencent.assistantv2.component;

import android.content.Context;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.mediadownload.o;
import com.tencent.assistantv2.model.AbstractDownloadInfo;
import com.tencent.assistantv2.model.h;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class VideoDownloadButton extends Button implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f3025a;
    /* access modifiers changed from: private */
    public h b;

    public VideoDownloadButton(Context context) {
        this(context, null);
    }

    public VideoDownloadButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3025a = context;
        b();
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_HOST_GET_FAIL, this);
    }

    private void b() {
        setHeight(this.f3025a.getResources().getDimensionPixelSize(R.dimen.download_button_height));
        setMinWidth(this.f3025a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
        setGravity(17);
        setTextSize(0, (float) this.f3025a.getResources().getDimensionPixelSize(R.dimen.download_button_font_size));
        setSingleLine(true);
    }

    public void a(h hVar, STInfoV2 sTInfoV2) {
        this.b = hVar;
        if (this.b != null) {
            a();
            a(sTInfoV2);
        }
    }

    public void a(STInfoV2 sTInfoV2) {
        if (this.b != null) {
            setOnClickListener(new en(this, sTInfoV2));
        }
    }

    public void a() {
        if (this.b != null) {
            a(this.b.i);
        }
    }

    private void a(AbstractDownloadInfo.DownState downState) {
        et b2 = b(downState);
        setTextColor(this.f3025a.getResources().getColor(b2.b));
        a(this.f3025a.getResources().getString(b2.f3206a));
        setBackgroundDrawable(this.f3025a.getResources().getDrawable(b2.c));
    }

    private et b(AbstractDownloadInfo.DownState downState) {
        et etVar = new et(null);
        etVar.c = R.drawable.state_bg_common_selector;
        etVar.b = R.color.state_normal;
        etVar.f3206a = R.string.appbutton_unknown;
        switch (eq.f3204a[downState.ordinal()]) {
            case 1:
                etVar.f3206a = R.string.appbutton_download;
                break;
            case 2:
                etVar.f3206a = R.string.downloading_display_pause;
                break;
            case 3:
                etVar.f3206a = R.string.queuing;
                break;
            case 4:
            case 5:
            case 6:
                etVar.f3206a = R.string.appbutton_continuing;
                etVar.b = R.color.state_install;
                etVar.c = R.drawable.state_bg_install_selector;
                break;
            case 7:
                etVar.f3206a = R.string.videobutton_play;
                break;
            default:
                etVar.f3206a = R.string.appbutton_unknown;
                break;
        }
        return etVar;
    }

    private void a(String str) {
        if (str.length() == 4) {
            setMinWidth(this.f3025a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_4));
        } else {
            setMinWidth(this.f3025a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
        }
        setText(str);
    }

    public void handleUIEvent(Message message) {
        if (message.what == 1166) {
            String str = (String) message.obj;
            if (!TextUtils.isEmpty(str) && this.b != null && str.equals(this.b.r)) {
                Context context = getContext();
                String string = getContext().getResources().getString(R.string.video_down_get_fail);
                Object[] objArr = new Object[1];
                objArr[0] = TextUtils.isEmpty(this.b.v) ? getContext().getResources().getString(R.string.video_down_tips_unknow_player) : this.b.v;
                Toast.makeText(context, String.format(string, objArr), 0).show();
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(h hVar, String str) {
        if (hVar != null) {
            switch (eq.f3204a[hVar.i.ordinal()]) {
                case 1:
                case 4:
                case 6:
                    o.c().a(hVar);
                    return;
                case 2:
                case 3:
                    o.c().a(hVar.c);
                    return;
                case 5:
                default:
                    return;
                case 7:
                    int a2 = o.c().a(getContext(), hVar);
                    if (a2 == -2 || a2 == -3) {
                        v.a(new er(getContext(), hVar, a2, null));
                        return;
                    } else if (a2 == -4) {
                        eo eoVar = new eo(this, hVar);
                        eoVar.titleRes = getContext().getResources().getString(R.string.video_play_tips);
                        eoVar.contentRes = String.format(getContext().getResources().getString(R.string.video_play_tips_content), new Object[0]);
                        eoVar.lBtnTxtRes = getContext().getResources().getString(R.string.video_down_cancel);
                        eoVar.rBtnTxtRes = getContext().getResources().getString(R.string.video_play_reload);
                        v.a(eoVar);
                        return;
                    } else {
                        return;
                    }
            }
        }
    }
}
