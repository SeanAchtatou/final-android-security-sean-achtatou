package com.tencent.assistantv2.component.banner.floatheader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.Banner;
import com.tencent.assistantv2.component.banner.f;

/* compiled from: ProGuard */
public class a extends f {
    public a(Banner banner) {
        super(banner);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View a(Context context, ViewGroup viewGroup, int i, long j, int i2) {
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.banner_float_node_view, viewGroup, false);
        inflate.setBackgroundColor(c());
        ((TXImageView) inflate.findViewById(R.id.title_img)).updateImageView(this.f3103a.c, 0, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        ((TextView) inflate.findViewById(R.id.title)).setText(this.f3103a.b);
        inflate.setOnClickListener(new b(this, context, i2));
        return inflate;
    }

    public int a() {
        return 1;
    }
}
