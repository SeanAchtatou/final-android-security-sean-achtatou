package com.tencent.assistantv2.component.categorydetail;

import com.tencent.assistant.model.e;
import com.tencent.assistant.module.callback.c;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.protocol.jce.TagGroup;
import com.tencent.assistantv2.component.banner.f;
import java.util.List;

/* compiled from: ProGuard */
class h implements c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameCategoryDetailListView f3146a;

    h(GameCategoryDetailListView gameCategoryDetailListView) {
        this.f3146a = gameCategoryDetailListView;
    }

    public void a(int i, int i2, boolean z, List<e> list, List<TagGroup> list2) {
        if (list != null) {
            if (this.f3146a.b == null) {
                this.f3146a.e();
            }
            this.f3146a.b.a(z, list, (List<f>) null, (List<ColorCardItem>) null);
        }
        if (!this.f3146a.e.a()) {
            if (list2 != null && !list2.isEmpty()) {
                this.f3146a.e.a(list2, this.f3146a.f);
                this.f3146a.a(list2);
            }
            this.f3146a.b(i, i2, z);
            return;
        }
        this.f3146a.a(i, i2, z);
    }
}
