package com.tencent.assistantv2.component.search;

import android.content.Context;
import android.support.v4.view.ViewPager;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistantv2.adapter.al;
import com.tencent.assistantv2.component.TabBarView;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public abstract class SearchResultTabPagesBase {

    /* renamed from: a  reason: collision with root package name */
    protected ViewPager f3230a;
    protected List<k> b = new ArrayList();
    protected al c = null;
    protected int d = 0;
    protected ViewPageScrollListener e = new p(this);
    private final String f = "SearchResultTabPagesBase";
    /* access modifiers changed from: private */
    public TabBarView g;
    private boolean h = true;
    private Context i;
    private int j = 8;
    private c k = new o(this);

    /* compiled from: ProGuard */
    public enum TabType {
        NONE,
        b,
        APP,
        VIDEO,
        EBOOK,
        MUSIC
    }

    /* access modifiers changed from: protected */
    public abstract void a(int i2);

    /* access modifiers changed from: protected */
    public abstract int[] a();

    public SearchResultTabPagesBase(Context context, TabBarView tabBarView, ViewPager viewPager) {
        this.i = context;
        this.g = tabBarView;
        this.f3230a = viewPager;
        b();
        c();
    }

    public void g() {
        if (this.b != null) {
            for (k next : this.b) {
                if (!(next == null || next.a() == null)) {
                    next.a().j();
                }
            }
        }
    }

    public void h() {
        if (this.b != null) {
            for (k next : this.b) {
                if (!(next == null || next.a() == null)) {
                    next.a().i();
                }
            }
        }
    }

    public void i() {
        this.f3230a = null;
        this.c = null;
        if (this.b != null) {
            for (k next : this.b) {
                if (!(next == null || next.a() == null)) {
                    next.a().k();
                }
            }
            this.b.clear();
            this.b = null;
        }
    }

    public void e(int i2) {
        this.j = i2;
        if (this.g != null) {
            this.g.setVisibility(i2);
        }
        if (this.f3230a != null) {
            this.f3230a.setVisibility(i2);
        }
        if (this.b != null) {
            for (k next : this.b) {
                if (!(next == null || next.a() == null)) {
                    next.a().d(i2);
                }
            }
        }
    }

    public int j() {
        return this.j;
    }

    private void b() {
        if (this.g != null) {
            this.g.a(a());
            this.g.a(this.d);
            this.g.a(this.k);
        }
    }

    private void c() {
        if (this.f3230a != null) {
            this.c = new al(this.b);
            this.f3230a.setAdapter(this.c);
            this.f3230a.setCurrentItem(this.d);
            this.f3230a.setOnPageChangeListener(this.e);
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, boolean z) {
        if (this.g != null) {
            this.g.b(i2, z);
            if (this.h) {
                this.g.c(i2);
            }
            this.h = false;
        }
    }

    /* access modifiers changed from: protected */
    public void b(int i2, boolean z) {
        if (this.g != null) {
            this.g.b(i2, z);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, int i2) {
        if (k() instanceof BaseActivity) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(k(), i2);
            buildSTInfo.slotId = a.a(str, str2);
            k.a(buildSTInfo);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, int i2, int i3) {
        if (k() instanceof BaseActivity) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(k(), i3);
            buildSTInfo.slotId = a.a(str, i2);
            k.a(buildSTInfo);
        }
    }

    public Context k() {
        return this.i;
    }
}
