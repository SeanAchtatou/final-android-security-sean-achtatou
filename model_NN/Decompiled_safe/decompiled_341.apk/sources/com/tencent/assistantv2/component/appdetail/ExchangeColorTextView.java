package com.tencent.assistantv2.component.appdetail;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.tencent.android.qqdownloader.R;
import com.tencent.connect.common.Constants;
import com.tencent.smtt.sdk.WebView;

/* compiled from: ProGuard */
public class ExchangeColorTextView extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    float f3054a;
    private LinearGradient b;
    private Paint c;
    private float d;
    private String e;
    private boolean f;

    public ExchangeColorTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = 0.0f;
        this.e = Constants.STR_EMPTY;
        this.f = false;
        this.c = new Paint(1);
    }

    public ExchangeColorTextView(Context context) {
        this(context, null);
    }

    public void a(float f2) {
        this.f3054a = f2;
        if (((double) this.f3054a) <= 0.05d) {
            this.f3054a = 0.05f;
        }
        invalidate();
    }

    public void a(boolean z) {
        this.f = z;
    }

    public void a(String str) {
        this.e = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, int, float, int, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f) {
            this.b = new LinearGradient(0.0f, 0.0f, ((float) getWidth()) * this.f3054a, 0.0f, new int[]{-1, getResources().getColor(R.color.appdetail_tag_text_color_blue_n)}, new float[]{0.99f, 1.0f}, Shader.TileMode.CLAMP);
        } else {
            this.b = new LinearGradient(0.0f, 0.0f, ((float) getWidth()) * this.f3054a, 0.0f, new int[]{-1, getResources().getColor(R.color.appdetail_btn_text_color_to_install)}, new float[]{0.99f, 1.0f}, Shader.TileMode.CLAMP);
        }
        if (Build.VERSION.SDK_INT > 7) {
            this.c.setShader(this.b);
        } else {
            this.c.setColor((int) WebView.NIGHT_MODE_COLOR);
        }
        this.c.setTextSize((float) getResources().getDimensionPixelSize(R.dimen.app_detail_text_size_large));
        float measureText = this.c.measureText(this.e);
        canvas.drawText(this.e, (float) ((getWidth() - ((int) measureText)) >> 1), (float) getResources().getDimensionPixelSize(R.dimen.appdetail_procesd_exchange_padding_top), this.c);
        canvas.restore();
    }
}
