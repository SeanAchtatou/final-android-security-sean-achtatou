package com.tencent.assistantv2.component.topbanner;

import com.tencent.assistant.db.table.ae;
import com.tencent.assistant.model.r;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static b f3252a;
    /* access modifiers changed from: private */
    public ae b;
    /* access modifiers changed from: private */
    public r c;

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            if (f3252a == null) {
                f3252a = new b();
            }
            bVar = f3252a;
        }
        return bVar;
    }

    public b() {
        this.b = null;
        this.b = new ae();
    }

    public void a(r rVar) {
        TemporaryThreadManager.get().start(new c(this, rVar));
    }

    public void b(r rVar) {
        TemporaryThreadManager.get().start(new d(this, rVar));
    }

    public void c(r rVar) {
        a(rVar);
    }

    public boolean d(r rVar) {
        this.c = rVar;
        if (!a(rVar.f1671a) || !b(rVar.f1671a)) {
            return false;
        }
        return true;
    }

    private boolean a(int i) {
        long currentTimeMillis = System.currentTimeMillis();
        if (this.c == null || this.c.f1671a != i || currentTimeMillis >= this.c.c * 1000) {
            return false;
        }
        return true;
    }

    private boolean b(int i) {
        if (this.c == null || this.c.f1671a != i || this.c.k >= this.c.j) {
            return false;
        }
        return true;
    }

    public int e(r rVar) {
        if (this.c == null || this.c.f1671a != rVar.f1671a) {
            this.c = this.b.b(rVar.f1671a);
        }
        if (this.c.m == 0 || this.c.m == rVar.m) {
            return this.c.k;
        }
        TemporaryThreadManager.get().start(new e(this));
        return 0;
    }

    public void f(r rVar) {
        rVar.k = 0;
        TemporaryThreadManager.get().start(new f(this, rVar));
    }

    public void g(r rVar) {
        b(rVar);
        this.c = null;
    }
}
