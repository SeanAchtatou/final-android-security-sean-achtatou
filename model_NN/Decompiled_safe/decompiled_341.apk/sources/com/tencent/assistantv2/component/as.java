package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.protocol.jce.HotWordItem;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class as extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HotWordItem f3091a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ HotwordsCardView d;

    as(HotwordsCardView hotwordsCardView, HotWordItem hotWordItem, int i, int i2) {
        this.d = hotwordsCardView;
        this.f3091a = hotWordItem;
        this.b = i;
        this.c = i2;
    }

    public void onTMAClick(View view) {
        b.a(this.d.getContext(), "tmast://search?selflink=1&key=" + this.f3091a.a());
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.d.getContext(), 200);
        if (this.b == 1) {
            buildSTInfo.slotId = a.a("04", this.c);
            buildSTInfo.status = "18";
            return buildSTInfo;
        } else if (this.b != 2) {
            return null;
        } else {
            buildSTInfo.scene = this.d.g;
            buildSTInfo.slotId = a.a("03", this.c);
            buildSTInfo.status = "01";
            return buildSTInfo;
        }
    }
}
