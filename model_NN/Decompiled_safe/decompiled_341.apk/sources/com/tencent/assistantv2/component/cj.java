package com.tencent.assistantv2.component;

import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.cy;
import com.tencent.assistant.component.SwitchButton;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.m;
import com.tencent.assistantv2.manager.i;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class cj extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankNormalListView f3156a;

    cj(RankNormalListView rankNormalListView) {
        this.f3156a = rankNormalListView;
    }

    public void onTMAClick(View view) {
        boolean z = true;
        this.f3156a.mClickCountNum++;
        if (this.f3156a.mClickCountNum < 10) {
            int i = this.f3156a.mClickCountNum - 1;
            this.f3156a.mClickCount[i] = true;
            if (i > 0) {
                this.f3156a.mClickCount[i - 1] = false;
            }
            this.f3156a.mHideInstalledAppBtn.postDelayed(new ck(this, i), 2000);
            if (this.f3156a.mClickCountNum == 5) {
                this.f3156a.mHideInstalledAppArea.setBackgroundResource(R.color.apk_list_bg);
            }
            if (this.f3156a.mClickCountNum > 5) {
                this.f3156a.mHideInstalledAppTips.setText(RankNormalListView.mDefaultTips[this.f3156a.mClickCountNum % 5]);
                this.f3156a.mHideInstalledAppTips.setVisibility(0);
                return;
            }
            SwitchButton switchButton = this.f3156a.mHideInstalledAppBtn;
            if (this.f3156a.mHideInstalledAppBtn.getSwitchState()) {
                z = false;
            }
            switchButton.updateSwitchStateWithAnim(z);
            m.a().s(this.f3156a.mHideInstalledAppBtn.getSwitchState());
            i.a().b().a(this.f3156a.mHideInstalledAppBtn.getSwitchState());
            if (this.f3156a.mHideInstalledAppBtn.getSwitchState()) {
                Toast.makeText(this.f3156a.getContext(), (int) R.string.hide_installed_apps_in_list, 4).show();
            } else if (this.f3156a.isGameRank) {
                Toast.makeText(this.f3156a.getContext(), (int) R.string.hide_installed_apps_in_list_cancel_rank, 4).show();
            } else {
                Toast.makeText(this.f3156a.getContext(), (int) R.string.hide_installed_apps_in_list_cancel, 4).show();
            }
            this.f3156a.mAdapter.c();
            if (this.f3156a.mAdapter.getCount() > 0 && this.f3156a.mAppEngine != null && !this.f3156a.mAppEngine.h()) {
                this.f3156a.updateFootViewText();
            }
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 sTInfoV2 = new STInfoV2(this.f3156a.mFragment.J(), RankNormalListView.ST_HIDE_INSTALLED_APPS, this.f3156a.mFragment.J(), RankNormalListView.ST_HIDE_INSTALLED_APPS, 200);
        if (!this.f3156a.isGameRank || !(this.f3156a.mFragment instanceof cy)) {
            sTInfoV2.slotId = a.a(RankNormalListView.ST_HIDE_INSTALLED_APPS, 0);
        } else {
            sTInfoV2.slotId = a.a(((cy) this.f3156a.mFragment).K(), 0);
        }
        if (!i.a().b().c()) {
            sTInfoV2.status = "01";
        } else {
            sTInfoV2.status = "02";
        }
        sTInfoV2.actionId = 200;
        return sTInfoV2;
    }
}
