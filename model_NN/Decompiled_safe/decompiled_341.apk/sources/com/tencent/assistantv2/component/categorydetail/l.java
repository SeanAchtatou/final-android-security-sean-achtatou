package com.tencent.assistantv2.component.categorydetail;

import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AbsListView;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private View f3148a;
    private int b;
    private int c;
    private long d;
    private Interpolator e = new AccelerateDecelerateInterpolator();
    private long f = -1;
    private k g;
    private boolean h = false;

    public l(View view, int i, int i2, long j) {
        this.f3148a = view;
        this.b = i;
        this.c = i2;
        this.d = j;
    }

    public void a(k kVar) {
        this.g = kVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public void run() {
        if (this.d <= 0) {
            c();
        } else if (this.f <= 0) {
            this.f = System.currentTimeMillis();
            b();
            if (this.f3148a != null) {
                this.f3148a.postDelayed(this, 10);
            }
        } else if (System.currentTimeMillis() - this.f >= this.d) {
            c();
        } else {
            int round = this.b - Math.round(this.e.getInterpolation(((float) Math.max(Math.min(((System.currentTimeMillis() - this.f) * 1000) / this.d, 1000L), 0L)) / 1000.0f) * ((float) (this.b - this.c)));
            XLog.d("yanhui-fth", "height:" + round + " newHeight:" + this.c);
            if (round <= this.c) {
                c();
                return;
            }
            b(round);
            a(round);
            if (this.f3148a != null) {
                this.f3148a.postDelayed(this, 10);
            }
        }
    }

    private void a(int i) {
        if (this.f3148a != null) {
            this.f3148a.setLayoutParams(new AbsListView.LayoutParams(-1, i));
            this.f3148a.postInvalidate();
        }
    }

    private void b() {
        this.h = true;
        if (this.g != null) {
            this.g.b();
        }
    }

    private void b(int i) {
        this.h = true;
        if (this.g != null) {
            this.g.a(i);
        }
    }

    private void c() {
        XLog.d("yanhui-fth", "stop:" + this.c);
        this.h = false;
        a(this.c);
        if (this.g != null) {
            this.g.a();
        }
    }

    public boolean a() {
        return this.h;
    }
}
