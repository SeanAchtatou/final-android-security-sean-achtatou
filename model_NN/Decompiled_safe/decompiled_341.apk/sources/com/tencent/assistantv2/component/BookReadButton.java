package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistantv2.model.b;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class BookReadButton extends Button {

    /* renamed from: a  reason: collision with root package name */
    private Context f2980a;
    /* access modifiers changed from: private */
    public b b;
    /* access modifiers changed from: private */
    public STInfoV2 c;

    public BookReadButton(Context context) {
        this(context, null);
    }

    public BookReadButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2980a = context;
        b();
    }

    private void b() {
        setHeight(this.f2980a.getResources().getDimensionPixelSize(R.dimen.download_button_height));
        setMinWidth(this.f2980a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
        setGravity(17);
        setTextSize(0, (float) this.f2980a.getResources().getDimensionPixelSize(R.dimen.download_button_font_size));
        setSingleLine(true);
        c();
        a();
    }

    public void a(b bVar, STInfoV2 sTInfoV2) {
        this.b = bVar;
        this.c = sTInfoV2;
    }

    public void a() {
        setOnClickListener(new l(this));
    }

    private void c() {
        m d = d();
        setTextColor(this.f2980a.getResources().getColor(d.b));
        a(this.f2980a.getResources().getString(d.f3220a));
        setBackgroundDrawable(this.f2980a.getResources().getDrawable(d.c));
    }

    private m d() {
        m mVar = new m(null);
        mVar.c = R.drawable.state_bg_common_selector;
        mVar.b = R.color.state_normal;
        mVar.f3220a = R.string.bookbutton_play;
        return mVar;
    }

    private void a(String str) {
        if (str.length() == 4) {
            setMinWidth(this.f2980a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_4));
        } else {
            setMinWidth(this.f2980a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
        }
        setText(str);
    }
}
