package com.tencent.assistantv2.component.appdetail;

import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.RecommendAppInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class aa extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    int f3063a = this.b;
    final /* synthetic */ int b;
    final /* synthetic */ RecommendAppViewV5 c;
    private byte d = 0;

    aa(RecommendAppViewV5 recommendAppViewV5, int i) {
        this.c = recommendAppViewV5;
        this.b = i;
    }

    public void onTMAClick(View view) {
        SimpleAppModel simpleAppModel = (SimpleAppModel) this.c.o.get(this.f3063a);
        if (simpleAppModel == null || simpleAppModel.b <= 0 || TextUtils.isEmpty(simpleAppModel.i)) {
            SimpleAppModel a2 = this.c.a((RecommendAppInfo) this.c.m.get(this.f3063a));
            if (a2 != null) {
                this.c.k[this.f3063a].setTag(Integer.valueOf(this.c.v.a(a2, this.d).b));
                this.c.k[this.f3063a].a();
                return;
            }
            return;
        }
        this.c.a((SimpleAppModel) this.c.o.get(this.f3063a), view, this.f3063a);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c.c, (SimpleAppModel) this.c.o.get(this.f3063a), this.c.c(this.f3063a), 200, a.a(u.d((SimpleAppModel) this.c.o.get(this.f3063a)), (SimpleAppModel) this.c.o.get(this.f3063a)));
        buildSTInfo.actionId = a.a(u.d((SimpleAppModel) this.c.o.get(this.f3063a)));
        buildSTInfo.recommendId = ((RecommendAppInfo) this.c.m.get(this.f3063a)).g;
        buildSTInfo.isImmediately = ((RecommendAppInfo) this.c.m.get(this.f3063a)).n == 1;
        return buildSTInfo;
    }
}
