package com.tencent.assistantv2.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.q;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
class bv implements q {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankCustomizeListView f3129a;

    bv(RankCustomizeListView rankCustomizeListView) {
        this.f3129a = rankCustomizeListView;
    }

    public void a(int i, int i2, boolean z, Map<AppGroupInfo, ArrayList<SimpleAppModel>> map, ArrayList<Long> arrayList, boolean z2) {
        if (this.f3129a.p != null) {
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1, null, this.f3129a.A);
            viewInvalidateMessage.arg1 = i2;
            viewInvalidateMessage.arg2 = i;
            HashMap hashMap = new HashMap();
            hashMap.put("isFirstPage", Boolean.valueOf(z));
            hashMap.put("hasNext", Boolean.valueOf(z2));
            hashMap.put("key_Appid", arrayList);
            hashMap.put("key_data", map);
            viewInvalidateMessage.params = hashMap;
            this.f3129a.p.sendMessage(viewInvalidateMessage);
        }
    }
}
