package com.tencent.assistantv2.component;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.style.DynamicDrawableSpan;
import android.view.View;
import android.widget.FrameLayout;

/* compiled from: ProGuard */
public class eu extends DynamicDrawableSpan {

    /* renamed from: a  reason: collision with root package name */
    private final Context f3207a;
    private final FrameLayout b;
    private final Resources c = this.f3207a.getResources();

    public eu(Context context, View view) {
        this.f3207a = context;
        this.b = new FrameLayout(context);
        this.b.addView(view);
        this.b.setDrawingCacheEnabled(true);
    }

    public Drawable getDrawable() {
        this.b.measure(0, 0);
        this.b.layout(0, 0, this.b.getMeasuredWidth(), this.b.getMeasuredHeight());
        BitmapDrawable bitmapDrawable = new BitmapDrawable(this.c, this.b.getDrawingCache());
        bitmapDrawable.setBounds(0, 0, bitmapDrawable.getIntrinsicWidth(), bitmapDrawable.getIntrinsicHeight());
        return bitmapDrawable;
    }
}
