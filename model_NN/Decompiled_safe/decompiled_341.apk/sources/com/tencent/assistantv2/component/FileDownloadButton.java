package com.tencent.assistantv2.component;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.util.AttributeSet;
import android.widget.Button;
import com.qq.AppService.AstApp;
import com.qq.ndk.NativeFileObject;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.mediadownload.FileOpenSelector;
import com.tencent.assistantv2.mediadownload.c;
import com.tencent.assistantv2.mediadownload.m;
import com.tencent.assistantv2.model.AbstractDownloadInfo;
import com.tencent.assistantv2.model.d;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.List;

/* compiled from: ProGuard */
public class FileDownloadButton extends Button {

    /* renamed from: a  reason: collision with root package name */
    private Context f2990a;
    /* access modifiers changed from: private */
    public d b;

    public FileDownloadButton(Context context) {
        this(context, null);
    }

    public FileDownloadButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2990a = context;
        b();
    }

    private void b() {
        setHeight(this.f2990a.getResources().getDimensionPixelSize(R.dimen.download_button_height));
        setMinWidth(this.f2990a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
        setGravity(17);
        setTextSize(0, (float) this.f2990a.getResources().getDimensionPixelSize(R.dimen.download_button_font_size));
        setSingleLine(true);
    }

    public void a(d dVar, STInfoV2 sTInfoV2) {
        this.b = dVar;
        if (this.b != null) {
            a();
            a(sTInfoV2);
        }
    }

    public void a(STInfoV2 sTInfoV2) {
        if (this.b != null) {
            setOnClickListener(new ah(this, sTInfoV2));
        }
    }

    public void a() {
        if (this.b != null) {
            a(this.b.i);
        }
    }

    private void a(AbstractDownloadInfo.DownState downState) {
        ak b2 = b(downState);
        setTextColor(this.f2990a.getResources().getColor(b2.b));
        a(this.f2990a.getResources().getString(b2.f3037a));
        try {
            setBackgroundDrawable(this.f2990a.getResources().getDrawable(b2.c));
        } catch (Throwable th) {
        }
    }

    private ak b(AbstractDownloadInfo.DownState downState) {
        ak akVar = new ak(null);
        akVar.c = R.drawable.state_bg_common_selector;
        akVar.b = R.color.state_normal;
        akVar.f3037a = R.string.appbutton_unknown;
        switch (aj.f3036a[downState.ordinal()]) {
            case 1:
                akVar.f3037a = R.string.appbutton_download;
                break;
            case 2:
                akVar.f3037a = R.string.downloading_display_pause;
                break;
            case 3:
                akVar.f3037a = R.string.queuing;
                break;
            case 4:
            case 5:
            case 6:
                akVar.f3037a = R.string.appbutton_continuing;
                akVar.b = R.color.state_install;
                akVar.c = R.drawable.state_bg_install_selector;
                break;
            case 7:
                akVar.f3037a = R.string.filebutton_open;
                break;
            default:
                akVar.f3037a = R.string.appbutton_unknown;
                break;
        }
        return akVar;
    }

    private void a(String str) {
        if (str.length() == 4) {
            setMinWidth(this.f2990a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_4));
        } else {
            setMinWidth(this.f2990a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
        }
        setText(str);
    }

    /* access modifiers changed from: private */
    public void b(d dVar, STInfoV2 sTInfoV2) {
        if (dVar != null) {
            switch (aj.f3036a[dVar.i.ordinal()]) {
                case 1:
                case 4:
                case 6:
                    c.c().a(dVar);
                    return;
                case 2:
                case 3:
                    c.c().a(dVar.c);
                    return;
                case 5:
                default:
                    return;
                case 7:
                    m b2 = FileOpenSelector.b(dVar.h);
                    if (b2 != null) {
                        Intent intent = b2.f3295a;
                        List<ResolveInfo> queryIntentActivities = getContext().getPackageManager().queryIntentActivities(intent, NativeFileObject.S_IFIFO);
                        if ((!b2.c || queryIntentActivities == null || queryIntentActivities.size() <= 0) && b2.c) {
                            ai aiVar = new ai(this, b2);
                            aiVar.titleRes = getContext().getString(R.string.file_down_tips);
                            aiVar.contentRes = getContext().getString(R.string.file_down_tips_content);
                            aiVar.lBtnTxtRes = getContext().getString(R.string.file_down_cancel);
                            aiVar.rBtnTxtRes = getContext().getString(R.string.file_down_search);
                            v.a(aiVar);
                            k.a(new STInfoV2(STConst.ST_PAGE_DOWNLOAD_FILE_CANT_OPEN, a.a(STConst.ST_DEFAULT_SLOT, 0), AstApp.m() != null ? AstApp.m().f() : 2000, STConst.ST_DEFAULT_SLOT, 100));
                            return;
                        }
                        getContext().startActivity(intent);
                        return;
                    }
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2, String str, int i3) {
        k.a(new STInfoV2(i, str, i2, STConst.ST_DEFAULT_SLOT, i3));
    }
}
