package com.tencent.assistantv2.component;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.protocol.jce.NewsInfo;

/* compiled from: ProGuard */
class ay extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ListItemRelateNewsView f3096a;
    private Context b;
    private NewsInfo c;

    public ay(ListItemRelateNewsView listItemRelateNewsView, Context context, NewsInfo newsInfo) {
        this.f3096a = listItemRelateNewsView;
        this.b = context;
        this.c = newsInfo;
    }

    public void onTMAClick(View view) {
        if (this.c != null) {
            b.a(this.f3096a.getContext(), this.c.f);
        }
    }
}
