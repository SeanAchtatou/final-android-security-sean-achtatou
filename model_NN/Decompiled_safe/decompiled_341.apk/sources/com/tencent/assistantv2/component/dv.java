package com.tencent.assistantv2.component;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

/* compiled from: ProGuard */
class dv extends ClickableSpan implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TxManagerCommContainView f3187a;
    private final View.OnClickListener b;

    public dv(TxManagerCommContainView txManagerCommContainView, View.OnClickListener onClickListener) {
        this.f3187a = txManagerCommContainView;
        this.b = onClickListener;
    }

    public void onClick(View view) {
        this.b.onClick(view);
    }

    public void updateDrawState(TextPaint textPaint) {
        textPaint.setUnderlineText(false);
    }
}
