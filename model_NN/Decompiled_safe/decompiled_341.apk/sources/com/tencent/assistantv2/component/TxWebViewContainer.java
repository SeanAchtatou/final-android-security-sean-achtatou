package com.tencent.assistantv2.component;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.webkit.ValueCallback;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.js.JsBridge;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.XLog;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;

/* compiled from: ProGuard */
public class TxWebViewContainer extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference<Activity> f3021a;
    /* access modifiers changed from: private */
    public TxWebView b;
    private FrameLayout c;
    /* access modifiers changed from: private */
    public ProgressBar d;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage e;
    private JsBridge f;
    /* access modifiers changed from: private */
    public dz g;
    /* access modifiers changed from: private */
    public ek h;

    public TxWebViewContainer(Context context) {
        this(context, null);
    }

    public TxWebViewContainer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3021a = new WeakReference<>((Activity) context);
        q();
    }

    private void q() {
        XLog.i("TxWebViewContainer", ">>init>>");
        LayoutInflater.from(t()).inflate((int) R.layout.txwebview_layout, this);
        r();
    }

    private void r() {
        this.c = (FrameLayout) findViewById(R.id.webview_container);
        this.b = new TxWebView(t());
        this.c.addView(this.b);
        this.d = (ProgressBar) findViewById(R.id.loading_view);
        this.f = new JsBridge(t(), this.b, this);
        s();
    }

    private void s() {
        this.e = (NormalErrorRecommendPage) findViewById(R.id.error_page_view);
        this.e.setButtonClickListener(new eh(this));
        this.e.setIsAutoLoading(true);
    }

    public void a(dz dzVar) {
        this.g = dzVar;
        ei eiVar = new ei(this);
        ej ejVar = new ej(this);
        if (this.g != null && this.g.c) {
            this.d.setVisibility(4);
        }
        this.b.a(this.f, eiVar, ejVar, dzVar);
    }

    public void a() {
        if (this.b != null) {
            this.b.c();
        }
        if (this.f != null) {
            this.f.onResume();
        }
    }

    public void b() {
        if (this.b != null) {
            this.b.b();
        }
        if (this.f != null) {
            this.f.onPause();
        }
    }

    public void c() {
        if (this.f != null) {
            this.f.recycle();
            this.f = null;
        }
        if (this.e != null) {
            this.e.destory();
        }
        if (this.b != null) {
            this.b.clearCache(false);
            this.b.destroy();
        }
    }

    public void d() {
        try {
            if (this.b != null) {
                this.c.removeAllViews();
                this.b.stopLoading();
                this.b.a();
                this.b = null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean e() {
        return this.b.canGoBack();
    }

    public boolean f() {
        return this.b.canGoForward();
    }

    public void g() {
        a(true);
        this.b.goBack();
    }

    public void h() {
        a(true);
        this.b.goForward();
    }

    public void i() {
        a(true);
        this.b.reload();
    }

    public void a(String str) {
        a(true);
        try {
            if (TextUtils.isEmpty(str) || !str.trim().toLowerCase().startsWith("file:")) {
                this.b.loadUrl(str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public ValueCallback<Uri> j() {
        return this.b.d();
    }

    public void k() {
        this.b.e();
    }

    public void a(int i) {
        this.d.setProgress(i);
    }

    public void l() {
        this.f.clickCallback();
    }

    public void m() {
        if (this.f != null) {
            this.f.updateStartLoadTime();
        }
    }

    public void a(String str, String str2) {
        this.f.responseFileChooser(str, str2);
    }

    public void showErrorPage(boolean z) {
        if (!z) {
            a(true);
        } else if (!c.a()) {
            c(30);
        } else {
            c(20);
        }
    }

    /* access modifiers changed from: private */
    public void c(int i) {
        a(false);
        this.e.setErrorType(i);
        this.e.setVisibility(0);
    }

    public void a(boolean z) {
        int i = 0;
        if (this.e != null) {
            this.e.setVisibility(z ? 4 : 0);
        }
        if (this.b != null) {
            TxWebView txWebView = this.b;
            if (!z) {
                i = 4;
            }
            txWebView.setVisibility(i);
        }
    }

    public void n() {
        try {
            this.b.getClass().getMethod("setOverScrollMode", Integer.TYPE).invoke(this.b, 2);
        } catch (NoSuchMethodException e2) {
            e2.printStackTrace();
        } catch (IllegalArgumentException e3) {
            e3.printStackTrace();
        } catch (IllegalAccessException e4) {
            e4.printStackTrace();
        } catch (InvocationTargetException e5) {
            e5.printStackTrace();
        }
    }

    public boolean b(boolean z) {
        if (this.b != null) {
            return this.b.setVideoFullScreen(z);
        }
        return false;
    }

    public void o() {
        if (this.b != null) {
            Bundle bundle = new Bundle();
            bundle.putInt("DefaultVideoScreen", 2);
            bundle.putBoolean("supportLiteWnd", false);
            if (this.b.getX5WebViewExtension() != null) {
                this.b.getX5WebViewExtension().invokeMiscMethod("setVideoParams", bundle);
            }
        }
    }

    public TxWebView p() {
        return this.b;
    }

    public void a(ek ekVar) {
        this.h = ekVar;
    }

    public void b(int i) {
        if (this.b != null) {
            this.b.setBackgroundColor(i);
        }
    }

    public void a(ea eaVar) {
        if (this.b != null && eaVar != null) {
            this.b.a(eaVar);
        }
    }

    /* access modifiers changed from: private */
    public Activity t() {
        Activity activity = this.f3021a.get();
        if (activity == null) {
            return (Activity) getContext();
        }
        return activity;
    }
}
