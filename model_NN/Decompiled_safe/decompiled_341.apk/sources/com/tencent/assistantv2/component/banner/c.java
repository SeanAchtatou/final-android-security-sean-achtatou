package com.tencent.assistantv2.component.banner;

import com.tencent.assistant.manager.as;
import com.tencent.assistant.module.ce;
import com.tencent.assistant.protocol.jce.GftGetGameGiftFlagResponse;
import com.tencent.assistant.protocol.jce.GftReportGameGiftCheckedRequest;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f3101a;

    c(b bVar) {
        this.f3101a = bVar;
    }

    public void run() {
        ce.a().a(new GftReportGameGiftCheckedRequest());
        GftGetGameGiftFlagResponse r = as.w().r();
        if (r != null && r.f2196a == 0) {
            r.b = 0;
            as.w().a(r);
        }
    }
}
