package com.tencent.assistantv2.component;

import android.animation.ValueAnimator;
import android.widget.RelativeLayout;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
class dn implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TxManagerCommContainView f3179a;

    dn(TxManagerCommContainView txManagerCommContainView) {
        this.f3179a = txManagerCommContainView;
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.f3179a.h.getLayoutParams();
        layoutParams.height = (int) ((1.0f - floatValue) * ((float) (this.f3179a.getMeasuredHeight() - this.f3179a.j.getMeasuredHeight())));
        this.f3179a.a(floatValue / TxManagerCommContainView.k);
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.f3179a.i.getLayoutParams();
        layoutParams2.height = (this.f3179a.getMeasuredHeight() - this.f3179a.j.getMeasuredHeight()) - layoutParams.height;
        float measuredHeight = (float) this.f3179a.h.getMeasuredHeight();
        XLog.d("Donaldxuuu", "topHeight = " + measuredHeight + " marginTop = ");
        if ((measuredHeight - ((float) df.a(this.f3179a.getContext(), 144.0f))) / 2.0f < ((float) df.a(this.f3179a.getContext(), 140.0f))) {
            if (this.f3179a.y) {
                RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) this.f3179a.x.getLayoutParams();
                layoutParams3.addRule(13);
                layoutParams3.width = df.a(this.f3179a.getContext(), 144.0f);
                layoutParams3.height = df.a(this.f3179a.getContext(), 144.0f);
                layoutParams3.topMargin = 0;
                layoutParams3.bottomMargin = 0;
                this.f3179a.x.setLayoutParams(layoutParams3);
                boolean unused = this.f3179a.y = false;
            }
            float c = 1.0f - ((floatValue / TxManagerCommContainView.k) * (1.0f - TxManagerCommContainView.l));
            this.f3179a.x.setScaleX(c);
            this.f3179a.x.setScaleY(c);
        }
        this.f3179a.h.setLayoutParams(layoutParams);
        this.f3179a.i.setLayoutParams(layoutParams2);
    }
}
