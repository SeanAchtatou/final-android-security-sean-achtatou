package com.tencent.assistantv2.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class dc extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleViewV5 f3172a;

    dc(SecondNavigationTitleViewV5 secondNavigationTitleViewV5) {
        this.f3172a = secondNavigationTitleViewV5;
    }

    public void onTMAClick(View view) {
        this.f3172a.b.startActivity(new Intent(this.f3172a.b, MainActivity.class));
    }

    public STInfoV2 getStInfo() {
        return this.f3172a.e(a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QQ, "001"));
    }
}
