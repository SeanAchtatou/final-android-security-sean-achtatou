package com.tencent.assistantv2.component;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.m;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.k;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ProGuard */
class by extends ViewInvalidateMessageHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankCustomizeListView f3132a;

    by(RankCustomizeListView rankCustomizeListView) {
        this.f3132a = rankCustomizeListView;
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        if (viewInvalidateMessage.what == 1) {
            int i = viewInvalidateMessage.arg1;
            int i2 = viewInvalidateMessage.arg2;
            Map map = (Map) viewInvalidateMessage.params;
            boolean booleanValue = ((Boolean) map.get("isFirstPage")).booleanValue();
            boolean booleanValue2 = ((Boolean) map.get("hasNext")).booleanValue();
            Map map2 = (Map) map.get("key_data");
            if (!(map2 == null || map2.size() <= 0 || this.f3132a.m == null)) {
                long j = -1;
                if (this.f3132a.l != null) {
                    j = this.f3132a.l.c();
                }
                if (booleanValue) {
                    this.f3132a.m.a(map2, j);
                    k.a((int) STConst.ST_PAGE_RANK_RECOMMEND, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                    int i3 = 0;
                    for (AppGroupInfo appGroupInfo : map2.keySet()) {
                        Iterator it = ((ArrayList) map2.get(appGroupInfo)).iterator();
                        while (it.hasNext()) {
                            if (((SimpleAppModel) it.next()).t() == AppConst.AppState.INSTALLED) {
                                i3++;
                            }
                        }
                    }
                    if ((i3 < 5 || !m.a().ai()) && ((!this.f3132a.i && !m.a().ak()) || (this.f3132a.i && !m.a().al()))) {
                        this.f3132a.e = false;
                        this.f3132a.b.setVisibility(8);
                        this.f3132a.b.postDelayed(new bz(this), 200);
                    } else {
                        this.f3132a.e = true;
                        this.f3132a.e();
                        m.a().w(false);
                        if (this.f3132a.i) {
                            m.a().z(false);
                        } else {
                            m.a().y(false);
                        }
                    }
                } else {
                    this.f3132a.m.b(map2, j);
                }
                for (int i4 = 0; i4 < this.f3132a.m.getGroupCount(); i4++) {
                    this.f3132a.expandGroup(i4);
                }
            }
            this.f3132a.a(i2, i, booleanValue, booleanValue2);
            return;
        }
        this.f3132a.n.b();
        if (this.f3132a.m != null) {
            this.f3132a.m.notifyDataSetChanged();
        }
    }
}
