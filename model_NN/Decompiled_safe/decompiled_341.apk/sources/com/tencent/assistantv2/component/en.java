package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class en extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f3201a;
    final /* synthetic */ VideoDownloadButton b;

    en(VideoDownloadButton videoDownloadButton, STInfoV2 sTInfoV2) {
        this.b = videoDownloadButton;
        this.f3201a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.b.a(this.b.b, Constants.STR_EMPTY);
    }

    public STInfoV2 getStInfo() {
        if (this.f3201a != null) {
            this.f3201a.actionId = a.a(this.b.b.i);
            this.f3201a.status = a.c(this.b.b.i);
        }
        return this.f3201a;
    }
}
