package com.tencent.assistantv2.component.appdetail;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.AppdetailViewPager;
import com.tencent.assistant.component.appdetail.HorizonScrollPicViewer;
import com.tencent.assistant.component.appdetail.IAppdetailView;
import com.tencent.assistant.component.appdetail.IInnerScrollListener;
import com.tencent.assistant.component.appdetail.InnerScrollView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.c;
import com.tencent.assistant.protocol.jce.AppHotFriend;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class AppdetailTabViewV5 extends LinearLayout implements IAppdetailView {

    /* renamed from: a  reason: collision with root package name */
    private InnerScrollView f3048a;
    private AppDetailViewV5 b;
    private AppdetailRelatedViewV5 c;
    private b d;

    public AppdetailTabViewV5(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AppdetailTabViewV5(Context context) {
        super(context);
    }

    private void a(Context context) {
        if (this.c == null) {
            View inflate = LayoutInflater.from(context).inflate((int) R.layout.appdetail_view_layout_v5, this);
            this.f3048a = (InnerScrollView) inflate.findViewById(R.id.inner_scrollview);
            this.b = (AppDetailViewV5) inflate.findViewById(R.id.detail_view);
            this.c = (AppdetailRelatedViewV5) this.b.findViewById(R.id.recommend_view);
        }
    }

    public IInnerScrollListener getInnerScrollView() {
        return this.f3048a;
    }

    public AppdetailRelatedViewV5 a() {
        return this.c;
    }

    public AppDetailViewV5 b() {
        return this.b;
    }

    public InnerScrollView c() {
        return this.f3048a;
    }

    public void a(c cVar, SimpleAppModel simpleAppModel) {
        a(getContext());
        this.b.b(cVar, simpleAppModel);
        this.c = (AppdetailRelatedViewV5) this.b.findViewById(R.id.recommend_view);
    }

    public void a(HorizonScrollPicViewer.IPicViewerListener iPicViewerListener) {
        this.b.a(iPicViewerListener);
    }

    public void a(HorizonScrollPicViewer.IShowPictureListener iShowPictureListener) {
        this.b.a(iShowPictureListener);
    }

    public void d() {
        if (this.b != null) {
            this.b.a();
        }
    }

    public void e() {
        if (this.b != null) {
            this.b.b();
        }
    }

    public AppdetailViewPager.IHorizonScrollPicViewer f() {
        return this.b.c();
    }

    public void a(b bVar) {
        if (this.b != null) {
            this.b.a(bVar);
        }
        this.d = bVar;
    }

    public void a(String str, ArrayList<AppHotFriend> arrayList) {
        this.b.a(str, arrayList);
    }
}
