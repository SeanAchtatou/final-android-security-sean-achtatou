package com.tencent.assistantv2.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistantv2.db.a.a;
import com.tencent.assistantv2.db.a.b;
import com.tencent.assistantv2.db.a.d;

/* compiled from: ProGuard */
public class MediaDbHelper extends SqliteHelper {
    private static final String DB_NAME = "mobile_media.db";
    private static final int DB_VERSION = 2;
    private static final Class<?>[] TABLESS = {d.class, b.class, a.class};
    private static volatile SqliteHelper instance;

    public static synchronized SqliteHelper get(Context context) {
        SqliteHelper sqliteHelper;
        synchronized (MediaDbHelper.class) {
            if (instance == null) {
                instance = new MediaDbHelper(context, DB_NAME, null, 2);
            }
            sqliteHelper = instance;
        }
        return sqliteHelper;
    }

    public MediaDbHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, DB_NAME, null, i);
    }

    public Class<?>[] getTables() {
        return TABLESS;
    }

    public int getDBVersion() {
        return 2;
    }
}
