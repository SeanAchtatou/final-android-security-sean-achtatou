package com.tencent.assistantv2.st.business;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class p extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f3339a;

    p(o oVar) {
        this.f3339a = oVar;
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        if (localApkInfo != null && i == 1) {
            TemporaryThreadManager.get().start(new q(this, localApkInfo));
        }
    }
}
