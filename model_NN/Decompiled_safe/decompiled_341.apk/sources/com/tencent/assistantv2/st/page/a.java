package com.tencent.assistantv2.st.page;

import android.content.Context;
import android.text.TextUtils;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.model.AbstractDownloadInfo;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.model.StatInfo;

/* compiled from: ProGuard */
public class a {
    public static int a(AppConst.AppState appState) {
        if (appState == null) {
            return -1;
        }
        switch (b.f3359a[appState.ordinal()]) {
            case 1:
                return 900;
            case 2:
                return STConstAction.ACTION_HIT_UPDATE;
            case 3:
                return STConstAction.ACTION_HIT_INSTALL;
            case 4:
                return STConstAction.ACTION_HIT_OPEN;
            default:
                return 200;
        }
    }

    public static int a(AbstractDownloadInfo.DownState downState) {
        if (downState == null) {
            return -1;
        }
        switch (b.b[downState.ordinal()]) {
            case 1:
                return 900;
            case 2:
                return STConstAction.ACTION_HIT_OPEN;
            default:
                return 200;
        }
    }

    public static int b(AbstractDownloadInfo.DownState downState) {
        if (downState == null) {
            return -1;
        }
        switch (b.b[downState.ordinal()]) {
            case 1:
                return 900;
            case 2:
                return STConstAction.ACTION_HIT_OPEN;
            default:
                return 200;
        }
    }

    public static String a(AppConst.AppState appState, SimpleAppModel simpleAppModel) {
        if (s.a(simpleAppModel, appState)) {
            return "02";
        }
        if (appState == null) {
            return STConst.ST_STATUS_DEFAULT;
        }
        switch (b.f3359a[appState.ordinal()]) {
            case 4:
                if (simpleAppModel == null || TextUtils.isEmpty(simpleAppModel.aD)) {
                    return STConst.ST_STATUS_DEFAULT;
                }
                return "01";
            case 5:
            case 6:
                return "04";
            case 7:
                return "05";
            default:
                return STConst.ST_STATUS_DEFAULT;
        }
    }

    public static String a(AppConst.AppState appState, boolean z) {
        if (z) {
            return "02";
        }
        if (appState == null) {
            return STConst.ST_STATUS_DEFAULT;
        }
        switch (b.f3359a[appState.ordinal()]) {
            case 5:
            case 6:
                return "04";
            case 7:
                return "05";
            default:
                return STConst.ST_STATUS_DEFAULT;
        }
    }

    public static String b(AppConst.AppState appState) {
        if (appState == null) {
            return STConst.ST_STATUS_DEFAULT;
        }
        switch (b.f3359a[appState.ordinal()]) {
            case 2:
                return "01";
            case 3:
                return "02";
            case 4:
            default:
                return STConst.ST_STATUS_DEFAULT;
            case 5:
            case 6:
                return "04";
            case 7:
                return "05";
        }
    }

    public static String c(AbstractDownloadInfo.DownState downState) {
        if (downState == null) {
            return STConst.ST_STATUS_DEFAULT;
        }
        switch (b.b[downState.ordinal()]) {
            case 3:
            case 4:
                return "04";
            case 5:
                return "05";
            default:
                return STConst.ST_STATUS_DEFAULT;
        }
    }

    public static String d(AbstractDownloadInfo.DownState downState) {
        if (downState == null) {
            return STConst.ST_STATUS_DEFAULT;
        }
        switch (b.b[downState.ordinal()]) {
            case 3:
            case 4:
                return "04";
            case 5:
                return "05";
            default:
                return STConst.ST_STATUS_DEFAULT;
        }
    }

    public static STPageInfo a(Context context, String str) {
        if (context != null && (context instanceof BaseActivity)) {
            STPageInfo q = ((BaseActivity) context).q();
            if (TextUtils.isEmpty(str)) {
                return q;
            }
            q.b = str;
            return q;
        } else if (context == null || !(context instanceof PluginActivity)) {
            return null;
        } else {
            STPageInfo stPageInfo = ((PluginActivity) context).getStPageInfo();
            if (TextUtils.isEmpty(str)) {
                return stPageInfo;
            }
            stPageInfo.b = str;
            return stPageInfo;
        }
    }

    public static STPageInfo a(int i, String str) {
        STPageInfo sTPageInfo = new STPageInfo();
        sTPageInfo.f3358a = i;
        sTPageInfo.b = str;
        return sTPageInfo;
    }

    public static String a(String str, int i) {
        if (TextUtils.isEmpty(str) || str.equalsIgnoreCase(STConst.ST_DEFAULT_SLOT)) {
            return STConst.ST_DEFAULT_SLOT;
        }
        return str + "_" + ct.a(i + 1);
    }

    public static String a(String str, String str2) {
        if (TextUtils.isEmpty(str) || str.equalsIgnoreCase(STConst.ST_DEFAULT_SLOT)) {
            return STConst.ST_DEFAULT_SLOT;
        }
        return str + "_" + str2;
    }

    public static String a(String str, int i, int i2) {
        if (TextUtils.isEmpty(str) || str.equalsIgnoreCase(STConst.ST_DEFAULT_SLOT)) {
            return STConst.ST_DEFAULT_SLOT;
        }
        return str + "_" + String.format("%02d", Integer.valueOf(i + 1)) + "_" + ct.a(i2 + 1);
    }

    public static String b(String str, String str2) {
        if (TextUtils.isEmpty(str) || str.equalsIgnoreCase(STConst.ST_DEFAULT_SLOT)) {
            return STConst.ST_DEFAULT_SLOT;
        }
        if (TextUtils.isEmpty(str2)) {
            str2 = STConst.ST_STATUS_DEFAULT;
        }
        StringBuffer stringBuffer = new StringBuffer(str);
        if (str.indexOf("|") > 0) {
            stringBuffer.insert(str.indexOf("|"), "_" + str2);
        } else {
            stringBuffer.append("_" + str2);
        }
        return stringBuffer.toString();
    }

    public static String a(STCommonInfo.ContentIdType contentIdType, String str) {
        return contentIdType.ordinal() + "_" + str;
    }

    public static StatInfo a(STCommonInfo sTCommonInfo) {
        return new StatInfo(sTCommonInfo);
    }
}
