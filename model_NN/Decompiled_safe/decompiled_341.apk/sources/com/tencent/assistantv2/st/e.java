package com.tencent.assistantv2.st;

import com.tencent.assistant.protocol.jce.StatReportItem;
import com.tencent.assistant.usagestats.UsagestatsSTManager;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f3351a;

    e(b bVar) {
        this.f3351a = bVar;
    }

    public void run() {
        ArrayList arrayList = new ArrayList();
        List<Long> a2 = this.f3351a.d.a((ArrayList<StatReportItem>) arrayList);
        if (arrayList != null && arrayList.size() > 0) {
            XLog.d("logReport", "非实时日志的上报");
            this.f3351a.b.put(Integer.valueOf(this.f3351a.e.a(arrayList)), a2);
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            if (((StatReportItem) it.next()).a() == 17) {
                UsagestatsSTManager.a().a("app_usage_n_report_all", null, null, UsagestatsSTManager.ReportType.normal);
                return;
            }
        }
    }
}
