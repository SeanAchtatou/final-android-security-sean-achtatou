package com.tencent.assistantv2.b;

import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.GameRankTabType;
import com.tencent.assistant.protocol.jce.GetNavigationResponse;
import com.tencent.assistant.protocol.jce.NavigationNode;
import com.tencent.assistant.protocol.jce.SubNavigationNode;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.manager.MainTabType;
import com.tencent.assistantv2.manager.RankTabType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class ak {

    /* renamed from: a  reason: collision with root package name */
    public long f2949a;
    public List<al> b;
    public int c;

    private ak() {
    }

    private ak(int i) {
        this.c = i;
        switch (i) {
            case 0:
                this.b = new ArrayList(ae.e.length);
                this.b.add(new al(ae.e[0], MainTabType.DISCOVER.ordinal(), 0, null));
                this.b.add(new al(ae.e[1], MainTabType.HOT.ordinal(), 0, null));
                this.b.add(new al(ae.e[2], MainTabType.APP.ordinal(), 0, null));
                this.b.add(new al(ae.e[3], MainTabType.GAME.ordinal(), 0, null));
                this.b.add(new al(ae.e[4], MainTabType.VIDEO.ordinal(), 0, null));
                this.b.add(new al(ae.e[5], MainTabType.EBOOK.ordinal(), 0, null));
                return;
            case 1:
                this.b = new ArrayList(4);
                this.b.add(new al("飙升榜", RankTabType.APPLIST.ordinal(), 0, null, 0, 6, 20, (byte) 1));
                this.b.add(new al("下载榜", RankTabType.APPLIST.ordinal(), 0, null, 0, 5, 20, (byte) 1));
                this.b.add(new al("个性榜", RankTabType.LISTGROUP.ordinal(), 0, null, 0, 2, 20, (byte) 0));
                this.b.add(new al("好友榜", RankTabType.APPLIST.ordinal(), 0, null, 0, 99, 20, (byte) 0));
                return;
            case 2:
                this.b = new ArrayList(4);
                this.b.add(new al("新游", GameRankTabType.NEWGAME.ordinal(), 0, null, -2, 11, 20, (byte) 0));
                this.b.add(new al("单机", GameRankTabType.ONEPC.ordinal(), 0, null, -2, 8, 20, (byte) 0));
                this.b.add(new al("网游", GameRankTabType.NETGAME.ordinal(), 0, null, -2, 9, 20, (byte) 0));
                this.b.add(new al("人气", GameRankTabType.RENQI.ordinal(), 0, null, -2, 10, 20, (byte) 0));
                return;
            default:
                return;
        }
    }

    public static ak a(int i, GetNavigationResponse getNavigationResponse) {
        ak akVar = null;
        try {
            akVar = b(i, getNavigationResponse);
        } catch (Exception e) {
            XLog.e("GetNavigationEngine", "parse tab container fail,type:" + i + ".ex:" + e);
            e.printStackTrace();
        }
        if (akVar == null) {
            return new ak(i);
        }
        return akVar;
    }

    public static ak b(int i, GetNavigationResponse getNavigationResponse) {
        al alVar;
        switch (i) {
            case 0:
                if (!(getNavigationResponse == null || getNavigationResponse.b() == null || getNavigationResponse.b().size() <= 0)) {
                    ak akVar = new ak();
                    akVar.c = i;
                    akVar.f2949a = getNavigationResponse.a();
                    ArrayList<NavigationNode> arrayList = getNavigationResponse.c;
                    ArrayList<NavigationNode> arrayList2 = new ArrayList<>();
                    ArrayList arrayList3 = new ArrayList(getNavigationResponse.b().size());
                    Iterator<NavigationNode> it = arrayList.iterator();
                    while (it.hasNext()) {
                        NavigationNode next = it.next();
                        switch (aj.f2948a[MainTabType.values()[next.b].ordinal()]) {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                                arrayList2.add(next);
                                break;
                        }
                    }
                    if (arrayList2.size() > 0) {
                        Iterator<NavigationNode> it2 = arrayList2.iterator();
                        while (it2.hasNext()) {
                            arrayList.remove(it2.next());
                        }
                        arrayList2.addAll(arrayList);
                        getNavigationResponse.c = arrayList2;
                        Iterator<NavigationNode> it3 = arrayList2.iterator();
                        while (it3.hasNext()) {
                            NavigationNode next2 = it3.next();
                            if (MainTabType.values()[next2.b] == MainTabType.TREASURY) {
                                alVar = new al(MainActivity.i().getResources().getString(R.string.navigation_treasure_default_txt), next2.b, next2.c, next2.d);
                            } else {
                                alVar = new al(next2.f2240a.replace("电子书", "图书").substring(0, 2), next2.b, next2.c, next2.d);
                            }
                            arrayList3.add(alVar);
                        }
                        akVar.b = arrayList3;
                    } else {
                        akVar.b = new ak(0).b;
                    }
                    return akVar;
                }
                break;
            default:
                if (!(getNavigationResponse == null || getNavigationResponse.c() == null || getNavigationResponse.c().size() <= 0)) {
                    ak akVar2 = new ak();
                    akVar2.c = i;
                    akVar2.f2949a = getNavigationResponse.a();
                    ArrayList<SubNavigationNode> c2 = getNavigationResponse.c();
                    ArrayList arrayList4 = new ArrayList(getNavigationResponse.c().size());
                    Iterator<SubNavigationNode> it4 = c2.iterator();
                    while (it4.hasNext()) {
                        SubNavigationNode next3 = it4.next();
                        arrayList4.add(new al(next3.f2383a, next3.b, next3.c, next3.d, next3.e, next3.f, next3.g, next3.h));
                    }
                    akVar2.b = arrayList4;
                    return akVar2;
                }
        }
        return null;
    }

    public al a(int i) {
        if (this.b == null || i < 0 || i >= this.b.size()) {
            return null;
        }
        return this.b.get(i);
    }
}
