package com.tencent.assistantv2.b;

import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.d.b;
import com.tencent.assistant.d.e;
import com.tencent.assistant.h.c;
import com.tencent.assistant.login.d;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.Banner;
import com.tencent.assistant.protocol.jce.EntranceTemplate;
import com.tencent.assistant.protocol.jce.GetDiscoverRequest;
import com.tencent.assistant.protocol.jce.GetDiscoverResponse;
import com.tencent.assistant.protocol.jce.GetSmartCardsRequest;
import com.tencent.assistant.protocol.jce.GetSmartCardsResponse;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.assistant.protocol.jce.SmartCardWrapper;
import com.tencent.assistant.protocol.jce.TopBanner;
import com.tencent.assistant.protocol.scu.RequestResponePair;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.model.a.g;
import com.tencent.assistantv2.st.business.StartUpCostTimeSTManager;
import com.tencent.assistantv2.st.k;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class r extends BaseEngine<g> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public b f2968a;
    /* access modifiers changed from: private */
    public LbsData b;
    /* access modifiers changed from: private */
    public ArrayList<Banner> c;
    private ArrayList<SimpleAppModel> d;
    /* access modifiers changed from: private */
    public TopBanner e;
    /* access modifiers changed from: private */
    public long f;
    /* access modifiers changed from: private */
    public c g;
    private int h;
    private int i;
    /* access modifiers changed from: private */
    public ad j;
    private com.tencent.assistant.model.b k;
    private long l;
    private ab m;
    private long n;
    private EntranceTemplate o;
    private long p;
    private e q;

    public r() {
        this.f2968a = null;
        this.c = new ArrayList<>();
        this.d = new ArrayList<>();
        this.e = null;
        this.g = new com.tencent.assistant.h.b();
        this.h = -1;
        this.j = new ad();
        this.k = new com.tencent.assistant.model.b();
        this.l = -1;
        this.m = new ab(this, null);
        this.o = null;
        this.q = new aa(this);
        this.f2968a = new b(AstApp.i().getApplicationContext(), this.q);
        TemporaryThreadManager.get().start(new s(this));
    }

    /* access modifiers changed from: private */
    public int b(ad adVar) {
        if (adVar == null || adVar.f2942a == null || adVar.f2942a.length == 0) {
            adVar = new ad();
            this.j = adVar;
        }
        if (adVar.f2942a == null || adVar.f2942a.length == 0) {
            TemporaryThreadManager.get().start(new t(this));
        }
        GetSmartCardsRequest getSmartCardsRequest = new GetSmartCardsRequest();
        getSmartCardsRequest.f2167a = 0;
        getSmartCardsRequest.b = this.b;
        getSmartCardsRequest.e = 40;
        getSmartCardsRequest.d = adVar.b;
        GetDiscoverRequest getDiscoverRequest = new GetDiscoverRequest();
        getDiscoverRequest.f2115a = 20;
        getDiscoverRequest.b = adVar.f2942a;
        getDiscoverRequest.d = d.a().n();
        ArrayList arrayList = new ArrayList(2);
        arrayList.add(getSmartCardsRequest);
        arrayList.add(getDiscoverRequest);
        this.h = send(arrayList);
        XLog.d("GetHomeEngine", "getHomeEngine sendRequest:" + adVar + ",requestid:" + this.h);
        return this.h;
    }

    public int a() {
        if (this.h > 0 && this.h != this.m.c) {
            return this.h;
        }
        this.i = 0;
        k.a(StartUpCostTimeSTManager.TIMETYPE.B6_TIME, System.currentTimeMillis(), 0);
        this.h = b((ad) null);
        return this.h;
    }

    public int a(ad adVar) {
        if (adVar == null || adVar.f2942a == null) {
            return -1;
        }
        int b2 = this.m.b();
        if (this.m.d()) {
            this.m.a();
            return b2;
        } else if (this.m.e() == null || adVar.f2942a != this.m.e().f2942a || this.m.g() == null || this.m.g().size() == 0) {
            XLog.d("GetHomeEngine", "get next page from network.pagercontext:" + adVar);
            return b(adVar);
        } else if (this.l != this.m.c()) {
            XLog.d("GetHomeEngine", "version not same:" + this.l + ",refresh.");
            return a();
        } else {
            boolean b3 = this.m.e;
            ArrayList<SimpleAppModel> g2 = this.m.g();
            this.j = this.m.f();
            XLog.d("GetHomeEngine", "get data from nextPagerLoader.has next:" + b3 + ",query pagerContext:" + adVar + ",next pager context:" + this.j);
            this.d.addAll(g2);
            this.g.b(this.m.h());
            this.l = this.m.c();
            this.k.b(this.l);
            notifyDataChangedInMainThread(new u(this, b3, b2, g2));
            if (!b3) {
                return b2;
            }
            XLog.d("GetHomeEngine", "nextPageLoader.exec,pagerContext:" + this.j);
            this.m.a(this.j);
            return b2;
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, List<RequestResponePair> list) {
        int i3;
        boolean z;
        GetSmartCardsResponse getSmartCardsResponse;
        GetDiscoverRequest getDiscoverRequest;
        GetDiscoverResponse getDiscoverResponse;
        boolean z2 = false;
        byte[] bArr = null;
        this.h = -1;
        RequestResponePair requestResponePair = null;
        GetSmartCardsResponse getSmartCardsResponse2 = null;
        GetDiscoverResponse getDiscoverResponse2 = null;
        GetDiscoverRequest getDiscoverRequest2 = null;
        for (RequestResponePair next : list) {
            if (next.request instanceof GetDiscoverRequest) {
                getDiscoverResponse = (GetDiscoverResponse) next.response;
                getDiscoverRequest = (GetDiscoverRequest) next.request;
                getSmartCardsResponse = getSmartCardsResponse2;
            } else if (next.request instanceof GetSmartCardsRequest) {
                GetSmartCardsRequest getSmartCardsRequest = (GetSmartCardsRequest) next.request;
                getSmartCardsResponse = (GetSmartCardsResponse) next.response;
                getDiscoverRequest = getDiscoverRequest2;
                next = requestResponePair;
                getDiscoverResponse = getDiscoverResponse2;
            } else {
                next = requestResponePair;
                getSmartCardsResponse = getSmartCardsResponse2;
                getDiscoverRequest = getDiscoverRequest2;
                getDiscoverResponse = getDiscoverResponse2;
            }
            getSmartCardsResponse2 = getSmartCardsResponse;
            getDiscoverResponse2 = getDiscoverResponse;
            getDiscoverRequest2 = getDiscoverRequest;
            requestResponePair = next;
        }
        if (getDiscoverRequest2 == null || getDiscoverResponse2 == null || requestResponePair == null || getDiscoverResponse2.b == null || getDiscoverResponse2.b.size() == 0) {
            if (requestResponePair != null) {
                i3 = requestResponePair.errorCode;
            } else {
                i3 = -841;
            }
            onRequestFailed(i2, i3, list);
            return;
        }
        System.currentTimeMillis();
        k.a(StartUpCostTimeSTManager.TIMETYPE.B7_TIME, System.currentTimeMillis(), 0);
        ArrayList<SimpleAppModel> b2 = u.b(getDiscoverResponse2.a());
        ArrayList<SmartCardWrapper> arrayList = getSmartCardsResponse2 != null ? getSmartCardsResponse2.b : null;
        ad adVar = new ad();
        adVar.f2942a = getDiscoverResponse2.d;
        if (getDiscoverResponse2.f == 1) {
            z = true;
        } else {
            z = false;
        }
        adVar.c = z;
        adVar.b = this.j.b;
        adVar.a();
        if (i2 == this.m.b()) {
            XLog.d("GetHomeEngine", "getHomeEngine.onRequestSuccessed save nextpagerloader data.pager context" + adVar);
            as w = as.w();
            if (this.m.e() != null) {
                bArr = this.m.e().f2942a;
            }
            w.a(bArr, getDiscoverResponse2);
            this.m.a(getDiscoverResponse2.c(), b2, arrayList, adVar.c, adVar);
            return;
        }
        if (getDiscoverRequest2.b.length == 0) {
            z2 = true;
        }
        this.l = getDiscoverResponse2.c();
        this.k.b(this.l);
        if (z2) {
            this.c.clear();
            this.c.addAll(getDiscoverResponse2.b());
            this.n = getDiscoverResponse2.d();
            this.d.clear();
            this.g.b();
        }
        if (!(b2 == null || b2.size() == 0)) {
            this.d.addAll(b2);
        }
        System.currentTimeMillis();
        this.g.b(arrayList);
        this.e = getDiscoverResponse2.e();
        this.f = getDiscoverResponse2.f();
        this.o = getDiscoverResponse2.j;
        this.p = getDiscoverResponse2.k;
        XLog.v("home_entry", "getDiscoverResponse --onSuccess--ret= " + getDiscoverResponse2.f2116a + "--mEntranceRevision = " + this.p + "--mHomeEntryData= " + this.o);
        this.j = adVar;
        this.mCallbacks.broadcast(new v(this, adVar, i2, z2, b2));
        if (adVar.c) {
            XLog.d("GetHomeEngine", "getHomeEngine.onRequestSuccessed has Next!next pager loader exec load next.");
            this.m.a(adVar);
        }
        as.w().a(getDiscoverRequest2.b, getDiscoverResponse2);
        if (z2) {
            as.w().a(getSmartCardsResponse2);
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, List<RequestResponePair> list) {
        GetDiscoverRequest getDiscoverRequest;
        this.h = -1;
        XLog.d("GetHomeEngine", "on request fail,seq:" + i2);
        if (i2 == this.m.b()) {
            this.m.i();
            return;
        }
        this.i = i3;
        GetDiscoverRequest getDiscoverRequest2 = null;
        for (RequestResponePair next : list) {
            if (next.request instanceof GetDiscoverRequest) {
                getDiscoverRequest = (GetDiscoverRequest) next.request;
            } else {
                getDiscoverRequest = getDiscoverRequest2;
            }
            getDiscoverRequest2 = getDiscoverRequest;
        }
        if (getDiscoverRequest2.b.length == 0) {
            TemporaryThreadManager.get().start(new w(this, i3, i2));
        } else {
            notifyDataChangedInMainThread(new y(this, i2, i3));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
    }

    /* access modifiers changed from: private */
    public boolean a(int i2) {
        boolean z = false;
        k.a(StartUpCostTimeSTManager.TIMETYPE.B7_TIME, System.currentTimeMillis(), 1);
        GetDiscoverResponse a2 = as.w().a((byte[]) null);
        GetSmartCardsResponse a3 = as.w().a();
        if (!(a2 == null || a3 == null)) {
            ArrayList<Banner> b2 = a2.b();
            ArrayList<SimpleAppModel> b3 = u.b(a2.b);
            ArrayList<SmartCardWrapper> arrayList = a3.b;
            if (!(b3 == null || b3.size() == 0 || b2 == null || b2.size() == 0)) {
                ad adVar = this.j;
                if (1 == a2.f) {
                    z = true;
                }
                adVar.c = z;
                this.j.f2942a = a2.d;
                this.j.a();
                if (this.c == null) {
                    this.c = new ArrayList<>();
                }
                this.c.clear();
                this.c.addAll(b2);
                if (this.d == null) {
                    this.d = new ArrayList<>();
                }
                this.d.clear();
                this.d.addAll(b3);
                this.g.a(arrayList);
                this.l = a2.c();
                this.k.b(this.l);
                this.e = a2.e();
                this.f = a2.i;
                this.o = a2.g();
                this.p = a2.h();
                XLog.v("home_entry", "getDiscoverResponse --load cache--mEntranceRevision= " + this.p + "--mHomeEntryData= " + this.o);
                notifyDataChangedInMainThread(new z(this, i2, b3));
                if (!this.j.c) {
                    return true;
                }
                this.m.a(this.j);
                return true;
            }
        }
        return false;
    }

    public int b() {
        return this.i;
    }

    public boolean c() {
        return this.d != null && this.d.size() > 0;
    }

    public ad d() {
        return this.j;
    }

    public c e() {
        return this.g;
    }

    public ArrayList<SimpleAppModel> f() {
        return this.d;
    }

    public ArrayList<Banner> g() {
        return this.c;
    }

    public com.tencent.assistant.model.b h() {
        return this.k;
    }

    public long i() {
        return this.n;
    }

    public EntranceTemplate j() {
        return this.o;
    }

    public long k() {
        return this.p;
    }
}
