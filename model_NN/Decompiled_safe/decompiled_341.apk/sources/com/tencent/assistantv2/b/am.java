package com.tencent.assistantv2.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListRequest;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.model.a.i;
import java.text.SimpleDateFormat;

/* compiled from: ProGuard */
public class am extends BaseEngine<i> {

    /* renamed from: a  reason: collision with root package name */
    SimpleDateFormat f2951a = new SimpleDateFormat("yyyy-MM-dd");
    GetPhoneUserAppListRequest b = new GetPhoneUserAppListRequest();

    public int a() {
        this.b.f2151a = t.x() / 1000;
        XLog.e("zhangyuanchao", "-------------phoneAppListReqeust.sysFileCreateTime:" + this.f2951a.format(Long.valueOf(this.b.f2151a)) + "," + this.b.f2151a);
        return send(this.b);
    }

    public void a(boolean z) {
        this.b.b = z;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        GetPhoneUserAppListResponse getPhoneUserAppListResponse = (GetPhoneUserAppListResponse) jceStruct2;
        XLog.e("zhangyuanchao", "-------------onRequestSuccessed:" + getPhoneUserAppListResponse.b + "," + this.f2951a.format(Long.valueOf(getPhoneUserAppListResponse.e)) + ",Time:" + getPhoneUserAppListResponse.e + "," + getPhoneUserAppListResponse.d + "," + getPhoneUserAppListResponse.c.size());
        XLog.e("zhangyuanchao", "---------notifyDataChangedInMainThread---Successed ---");
        notifyDataChangedInMainThread(new an(this, i, getPhoneUserAppListResponse));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.e("zhangyuanchao", "-------------onRequestFailed:" + i2);
        notifyDataChangedInMainThread(new ao(this, i, i2));
    }
}
