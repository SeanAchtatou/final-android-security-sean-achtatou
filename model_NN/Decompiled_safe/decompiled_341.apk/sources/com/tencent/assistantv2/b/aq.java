package com.tencent.assistantv2.b;

import com.tencent.assistant.protocol.jce.VideoInfo;
import com.tencent.assistantv2.model.SimpleVideoModel;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class aq {
    public static SimpleVideoModel a(VideoInfo videoInfo) {
        if (videoInfo == null) {
            return null;
        }
        SimpleVideoModel simpleVideoModel = new SimpleVideoModel();
        simpleVideoModel.f3312a = videoInfo.f2422a;
        simpleVideoModel.b = videoInfo.e;
        simpleVideoModel.c = videoInfo.b;
        simpleVideoModel.d = videoInfo.f;
        simpleVideoModel.e = videoInfo.d;
        simpleVideoModel.f = videoInfo.c;
        simpleVideoModel.g = videoInfo.g;
        simpleVideoModel.h = videoInfo.i;
        simpleVideoModel.i = videoInfo.j;
        simpleVideoModel.k = videoInfo.h;
        if (videoInfo.l != null) {
            simpleVideoModel.l = videoInfo.l.f1970a;
        }
        if (videoInfo.k == 0) {
            simpleVideoModel.j = SimpleVideoModel.CARD_TYPE.NORMAL;
            return simpleVideoModel;
        } else if (videoInfo.k == 1) {
            simpleVideoModel.j = SimpleVideoModel.CARD_TYPE.RICH;
            return simpleVideoModel;
        } else {
            simpleVideoModel.j = SimpleVideoModel.CARD_TYPE.NORMAL;
            return simpleVideoModel;
        }
    }

    public static List<SimpleVideoModel> a(List<VideoInfo> list) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (VideoInfo a2 : list) {
            arrayList.add(a(a2));
        }
        return arrayList;
    }
}
