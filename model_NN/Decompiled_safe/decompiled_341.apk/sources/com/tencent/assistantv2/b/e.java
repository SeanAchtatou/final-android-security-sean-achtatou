package com.tencent.assistantv2.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.m;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.GftGetNavigationRequest;
import com.tencent.assistant.protocol.jce.GftGetNavigationResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bh;
import com.tencent.assistantv2.model.a.a;

/* compiled from: ProGuard */
public class e extends BaseEngine<a> {

    /* renamed from: a  reason: collision with root package name */
    private static e f2956a = null;
    private static h b = null;
    private static final Object c = new Object();

    public static synchronized e a() {
        e eVar;
        synchronized (e.class) {
            if (f2956a == null) {
                f2956a = new e();
            }
            eVar = f2956a;
        }
        return eVar;
    }

    private e() {
        b();
        TemporaryThreadManager.get().start(new f(this));
    }

    private h c() {
        GftGetNavigationResponse gftGetNavigationResponse;
        byte[] K = m.a().K();
        if (K != null && K.length > 0) {
            try {
                gftGetNavigationResponse = (GftGetNavigationResponse) bh.b(K, GftGetNavigationResponse.class);
            } catch (Exception e) {
                XLog.e("GameGetNavigationEngine", "response to navi object fail.typeid:.ex:" + e);
            }
            return h.a(gftGetNavigationResponse);
        }
        gftGetNavigationResponse = null;
        return h.a(gftGetNavigationResponse);
    }

    public h b() {
        if (b == null) {
            synchronized (c) {
                if (b == null) {
                    b = c();
                }
            }
        }
        return b;
    }

    /* access modifiers changed from: private */
    public int a(long j, int i) {
        GftGetNavigationRequest gftGetNavigationRequest = new GftGetNavigationRequest();
        gftGetNavigationRequest.f2197a = j;
        gftGetNavigationRequest.b = i;
        return send(gftGetNavigationRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        GftGetNavigationResponse gftGetNavigationResponse = (GftGetNavigationResponse) jceStruct2;
        GftGetNavigationRequest gftGetNavigationRequest = (GftGetNavigationRequest) jceStruct;
        if (gftGetNavigationResponse == null || gftGetNavigationRequest == null || gftGetNavigationResponse.b == gftGetNavigationRequest.a()) {
            XLog.e("GameGetNavigationEngine", "GetNavigationEngine has null value.seq:" + i + ",request:" + jceStruct + ",response:" + gftGetNavigationResponse);
            return;
        }
        int b2 = gftGetNavigationRequest.b();
        h b3 = h.b(gftGetNavigationResponse);
        if (b3 != null) {
            byte[] a2 = bh.a(gftGetNavigationResponse);
            if (a2 != null) {
                m.a().f(a2);
            }
            h hVar = b;
            if (hVar != null && b3.b != null && b3.b.size() > 0) {
                notifyDataChangedInMainThread(new g(this, i, b2, b3));
                b = hVar;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.e("GameGetNavigationEngine", "GetNavigationEngine error code:" + i2 + ".seq:" + i + ",request:" + jceStruct + ",response:" + jceStruct2);
    }
}
