package com.tencent.assistantv2.mediadownload;

import android.text.TextUtils;
import com.tencent.assistant.Global;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.model.AbstractDownloadInfo;
import com.tencent.assistantv2.model.a;
import com.tencent.assistantv2.model.d;
import com.tencent.downloadsdk.ae;
import java.io.File;

/* compiled from: ProGuard */
class h implements ae {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f3290a;

    h(c cVar) {
        this.f3290a = cVar;
    }

    public void a(int i, String str, String str2) {
        d dVar = (d) this.f3290a.f3285a.get(str);
        if (dVar != null) {
            dVar.g = str2;
            dVar.h = str2;
            dVar.i = AbstractDownloadInfo.DownState.SUCC;
            dVar.j = 0;
            dVar.f = System.currentTimeMillis();
            this.f3290a.c.sendMessage(this.f3290a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC, dVar));
            this.f3290a.b.a(dVar);
        }
    }

    public void a(int i, String str) {
        d dVar = (d) this.f3290a.f3285a.get(str);
        if (dVar != null) {
            dVar.i = AbstractDownloadInfo.DownState.DOWNLOADING;
            dVar.j = 0;
            this.f3290a.c.sendMessage(this.f3290a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START, dVar));
            this.f3290a.b.a(dVar);
        }
    }

    public void a(int i, String str, long j, String str2, String str3) {
        d dVar = (d) this.f3290a.f3285a.get(str);
        if (dVar != null) {
            if (dVar.k == null) {
                dVar.k = new a();
            }
            dVar.k.f3314a = j;
            dVar.d = j;
            if (!TextUtils.isEmpty(str2)) {
                dVar.h = str2;
                dVar.f3307a = new File(str2).getName();
            }
            dVar.m = str3;
            this.f3290a.c.sendMessage(this.f3290a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FILENAME, dVar));
            this.f3290a.b.a(dVar);
        }
    }

    public void a(int i, String str, long j, long j2, double d) {
        d dVar = (d) this.f3290a.f3285a.get(str);
        if (dVar != null) {
            if (dVar.i != AbstractDownloadInfo.DownState.DOWNLOADING) {
                this.f3290a.c.sendMessage(this.f3290a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START, dVar));
            }
            dVar.i = AbstractDownloadInfo.DownState.DOWNLOADING;
            dVar.j = 0;
            dVar.k.b = j2;
            dVar.k.f3314a = j;
            dVar.k.c = ct.a(d);
            this.f3290a.c.sendMessage(this.f3290a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING, dVar));
        }
    }

    public void a(int i, String str, String str2, String str3) {
        d dVar = (d) this.f3290a.f3285a.get(str);
        if (dVar != null) {
            dVar.h = str2;
            if (!TextUtils.isEmpty(str2)) {
                dVar.f3307a = new File(str2).getName();
            }
            dVar.m = str3;
            dVar.i = AbstractDownloadInfo.DownState.SUCC;
            dVar.j = 0;
            dVar.f = System.currentTimeMillis();
            dVar.k.b = dVar.k.f3314a;
            this.f3290a.c.sendMessage(this.f3290a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC, dVar));
            this.f3290a.b.a(dVar);
        }
    }

    public void a(int i, String str, int i2, byte[] bArr, String str2) {
        d dVar = (d) this.f3290a.f3285a.get(str);
        if (dVar != null) {
            dVar.i = AbstractDownloadInfo.DownState.FAIL;
            dVar.j = i2;
            this.f3290a.c.sendMessage(this.f3290a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FAIL, dVar));
            this.f3290a.b.a(dVar);
        }
        if (i2 == -12) {
            TemporaryThreadManager.get().start(new i(this));
        } else if (i2 == -11) {
            ba.a().post(new j(this));
        }
        FileUtil.tryRefreshPath(i2);
    }

    public void b(int i, String str) {
        if (Global.isDev()) {
            XLog.d("FileDownTag", "onTaskPaused.type:" + i + ",ticketid:" + str);
        }
        d dVar = (d) this.f3290a.f3285a.get(str);
        if (dVar == null) {
            return;
        }
        if (dVar.i == AbstractDownloadInfo.DownState.QUEUING || dVar.i == AbstractDownloadInfo.DownState.DOWNLOADING) {
            dVar.i = AbstractDownloadInfo.DownState.PAUSED;
            dVar.j = 0;
            this.f3290a.c.sendMessage(this.f3290a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_PAUSE, dVar));
            this.f3290a.b.a(dVar);
        }
    }

    public void b(int i, String str, String str2) {
    }
}
