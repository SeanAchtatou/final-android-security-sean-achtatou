package com.tencent.assistantv2.activity;

import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
class e extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f2847a;

    e(AppDetailActivityV5 appDetailActivityV5) {
        this.f2847a = appDetailActivityV5;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.AppDetailActivityV5.a(com.tencent.assistantv2.activity.AppDetailActivityV5, boolean):boolean
     arg types: [com.tencent.assistantv2.activity.AppDetailActivityV5, int]
     candidates:
      com.tencent.assistantv2.activity.AppDetailActivityV5.a(com.tencent.assistantv2.activity.AppDetailActivityV5, int):int
      com.tencent.assistantv2.activity.AppDetailActivityV5.a(com.tencent.assistantv2.activity.AppDetailActivityV5, com.tencent.assistant.localres.model.LocalApkInfo):com.tencent.assistant.localres.model.LocalApkInfo
      com.tencent.assistantv2.activity.AppDetailActivityV5.a(com.tencent.assistant.model.c, com.tencent.assistant.model.SimpleAppModel):com.tencent.assistant.model.ShareAppModel
      com.tencent.assistantv2.activity.AppDetailActivityV5.a(com.tencent.assistantv2.activity.AppDetailActivityV5, com.tencent.assistant.model.c):com.tencent.assistant.model.c
      com.tencent.assistantv2.activity.AppDetailActivityV5.a(int, com.tencent.assistant.protocol.jce.GetRecommendAppListResponse):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.AppDetailActivityV5.a(com.tencent.assistantv2.activity.AppDetailActivityV5, boolean):boolean */
    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                if (this.f2847a.H != null) {
                    this.f2847a.H.e();
                    boolean unused = this.f2847a.ar = true;
                    return;
                }
                return;
            case 2:
                if (this.f2847a.Q()) {
                    this.f2847a.A.getInnerScrollView().scrollDeltaY(-message.arg1);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
