package com.tencent.assistantv2.activity;

import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.b.ad;
import com.tencent.assistantv2.st.business.StartUpCostTimeSTManager;
import com.tencent.assistantv2.st.k;

/* compiled from: ProGuard */
class be implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bd f2776a;

    be(bd bdVar) {
        this.f2776a = bdVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.bd.a(com.tencent.assistantv2.activity.bd, boolean):void
     arg types: [com.tencent.assistantv2.activity.bd, int]
     candidates:
      com.tencent.assistantv2.activity.bd.a(com.tencent.assistantv2.activity.bd, com.tencent.assistantv2.b.ad):com.tencent.assistantv2.b.ad
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(int, android.support.v4.app.Fragment):void
      android.support.v4.app.Fragment.a(android.content.Intent, int):void
      android.support.v4.app.Fragment.a(android.view.Menu, android.view.MenuInflater):void
      android.support.v4.app.Fragment.a(android.view.View, android.os.Bundle):void
      com.tencent.assistantv2.activity.bd.a(com.tencent.assistantv2.activity.bd, boolean):void */
    public void run() {
        this.f2776a.Z.a(true, this.f2776a.Y.f(), this.f2776a.Y.h(), this.f2776a.Y.i(), null, this.f2776a.Y.n());
        k.a(StartUpCostTimeSTManager.TIMETYPE.END, System.currentTimeMillis(), 1);
        XLog.d("qt4atest", "TIMETYPE.END");
        ad unused = this.f2776a.aa = this.f2776a.Y.d();
        this.f2776a.U.onRefreshComplete(this.f2776a.Y.e());
        this.f2776a.U.setVisibility(0);
        if (this.f2776a.X != null) {
            this.f2776a.X.setVisibility(8);
        }
        this.f2776a.e(true);
    }
}
