package com.tencent.assistantv2.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public abstract class dx extends ay {
    FrameLayout P;
    View S;
    boolean T = true;
    private final String U = "TreasuryTabActivity:";
    private int V = 5;

    public abstract View A();

    public void d(Bundle bundle) {
        super.d(bundle);
        d((int) R.layout.act6);
        this.P = (FrameLayout) e((int) R.id.container);
    }

    public void d(boolean z) {
        if (this.T) {
            this.S = A();
            this.P.addView(this.S, new FrameLayout.LayoutParams(-1, -1));
            this.T = false;
        }
        if (this.S == null) {
            return;
        }
        if (this.S instanceof VideoTabActivity) {
            ((VideoTabActivity) this.S).a();
        } else if (this.S instanceof EBookTabActivity) {
            ((EBookTabActivity) this.S).a();
        }
    }

    public dx() {
        super(MainActivity.i());
    }

    public void k() {
        super.k();
        if (this.S == null) {
            return;
        }
        if (this.S instanceof VideoTabActivity) {
            ((VideoTabActivity) this.S).b();
        } else if (this.S instanceof EBookTabActivity) {
            ((EBookTabActivity) this.S).b();
        }
    }

    public void C() {
    }

    public int D() {
        return 0;
    }

    public int E() {
        return this.V;
    }

    public int J() {
        if (!(this.P == null || this.S == null)) {
            if (this.S instanceof VideoTabActivity) {
                return ((VideoTabActivity) this.S).c();
            }
            if (this.S instanceof EBookTabActivity) {
                return ((EBookTabActivity) this.S).c();
            }
        }
        return 2000;
    }

    public void I() {
        if (this.P == null || this.P.getChildAt(0) == null || this.S != null) {
            M();
        }
    }
}
