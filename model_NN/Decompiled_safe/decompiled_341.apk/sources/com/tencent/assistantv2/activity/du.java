package com.tencent.assistantv2.activity;

import android.text.TextUtils;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.DownloadButton;

/* compiled from: ProGuard */
class du implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f2844a;

    du(SearchActivity searchActivity) {
        this.f2844a = searchActivity;
    }

    public void onClick(View view) {
        String str = (String) view.getTag(R.id.search_key_word);
        SimpleAppModel simpleAppModel = null;
        if (view instanceof DownloadButton) {
            simpleAppModel = (SimpleAppModel) view.getTag(R.id.search_app_info);
            AppConst.AppState d = u.d(simpleAppModel);
            if (d == AppConst.AppState.DOWNLOAD || d == AppConst.AppState.UPDATE) {
                int unused = this.f2844a.K = (int) STConst.ST_PAGE_SEARCH_DIRECT;
            } else {
                return;
            }
        } else {
            int unused2 = this.f2844a.K = (int) STConst.ST_PAGE_SEARCH_SUGGEST;
        }
        this.f2844a.H.removeMessages(0);
        this.f2844a.D.a(this.f2844a.F);
        this.f2844a.A();
        this.f2844a.n = true;
        if (!TextUtils.isEmpty(str)) {
            String obj = this.f2844a.u.a().getText().toString();
            if (str.length() > SearchActivity.I) {
                str = str.substring(0, SearchActivity.I);
            }
            try {
                this.f2844a.u.a().setText(str);
                this.f2844a.u.a().setSelection(str.length());
            } catch (Throwable th) {
            }
            this.f2844a.c(8);
            this.f2844a.y.b(obj);
            this.f2844a.a(simpleAppModel);
        }
    }
}
