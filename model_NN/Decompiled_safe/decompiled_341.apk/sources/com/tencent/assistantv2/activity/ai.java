package com.tencent.assistantv2.activity;

import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.ApkMgrActivity;
import com.tencent.assistant.activity.SpaceCleanActivity;
import com.tencent.assistant.securescan.StartScanActivity;
import com.tencent.assistant.utils.br;
import com.tencent.assistant.utils.bt;
import com.tencent.assistantv2.model.e;
import com.tencent.connect.common.Constants;
import java.util.Random;

/* compiled from: ProGuard */
class ai implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AssistantTabActivity f2756a;

    ai(AssistantTabActivity assistantTabActivity) {
        this.f2756a = assistantTabActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.AssistantTabActivity.b(com.tencent.assistantv2.activity.AssistantTabActivity, boolean):boolean
     arg types: [com.tencent.assistantv2.activity.AssistantTabActivity, int]
     candidates:
      com.tencent.assistantv2.activity.AssistantTabActivity.b(com.tencent.assistantv2.activity.AssistantTabActivity, int):java.lang.String
      com.tencent.assistantv2.activity.AssistantTabActivity.b(com.tencent.assistantv2.activity.AssistantTabActivity, java.lang.String):void
      com.tencent.assistantv2.activity.AssistantTabActivity.b(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.AssistantTabActivity.b(com.tencent.assistantv2.activity.AssistantTabActivity, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.AssistantTabActivity.c(com.tencent.assistantv2.activity.AssistantTabActivity, boolean):void
     arg types: [com.tencent.assistantv2.activity.AssistantTabActivity, int]
     candidates:
      com.tencent.assistantv2.activity.AssistantTabActivity.c(com.tencent.assistantv2.activity.AssistantTabActivity, int):int
      com.tencent.assistantv2.activity.AssistantTabActivity.c(com.tencent.assistantv2.activity.AssistantTabActivity, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.AssistantTabActivity.d(com.tencent.assistantv2.activity.AssistantTabActivity, boolean):boolean
     arg types: [com.tencent.assistantv2.activity.AssistantTabActivity, int]
     candidates:
      com.tencent.assistantv2.activity.AssistantTabActivity.d(com.tencent.assistantv2.activity.AssistantTabActivity, int):int
      com.tencent.assistantv2.activity.AssistantTabActivity.d(com.tencent.assistantv2.activity.AssistantTabActivity, boolean):boolean */
    public void run() {
        String string;
        this.f2756a.bg.removeMessages(7);
        boolean unused = this.f2756a.be = false;
        this.f2756a.c(true);
        if (this.f2756a.ak > 0) {
            if (this.f2756a.ak > 0) {
                string = this.f2756a.getString(R.string.manage_toast_apkDel, new Object[]{Integer.valueOf(this.f2756a.ak), 10});
            } else {
                string = this.f2756a.getString(R.string.manage_toast_apkDel_1, new Object[]{10});
            }
            e eVar = new e();
            eVar.b = string;
            eVar.f3320a = ApkMgrActivity.class;
            eVar.c = 10;
            this.f2756a.ag.add(eVar);
        }
        if (!br.c() || (!this.f2756a.y().c() && !this.f2756a.aH)) {
            String string2 = this.f2756a.getString(R.string.manage_toast_volClean, new Object[]{10});
            e eVar2 = new e();
            eVar2.b = string2;
            eVar2.f3320a = SpaceCleanActivity.class;
            eVar2.c = 10;
            this.f2756a.ag.add(eVar2);
        }
        if (this.f2756a.al > 0) {
            String string3 = this.f2756a.getString(R.string.manage_toast_safeScan, new Object[]{Integer.valueOf(this.f2756a.al), 10});
            e eVar3 = new e();
            eVar3.b = string3;
            eVar3.f3320a = StartScanActivity.class;
            eVar3.c = 10;
            this.f2756a.ag.add(eVar3);
        }
        if (this.f2756a.ag.size() <= 0) {
            int unused2 = this.f2756a.aC = 100;
            boolean unused3 = this.f2756a.M();
        } else if (!this.f2756a.aG || this.f2756a.E.b() != 0) {
            int unused4 = this.f2756a.ah = new Random().nextInt(this.f2756a.ag.size());
            int i = ((e) this.f2756a.ag.get(this.f2756a.ah)).c;
            String str = "05_001";
            Class unused5 = this.f2756a.af = ((e) this.f2756a.ag.get(this.f2756a.ah)).f3320a;
            br.a(i, this.f2756a.af);
            int unused6 = this.f2756a.aC = 100 - i;
            if (this.f2756a.af == ApkMgrActivity.class) {
                this.f2756a.E.setTag(R.id.tma_st_slot_tag, "05_002");
                str = "05_002";
            } else if (this.f2756a.af == StartScanActivity.class) {
                this.f2756a.E.setTag(R.id.tma_st_slot_tag, "05_003");
                str = "05_003";
            } else if (this.f2756a.af == SpaceCleanActivity.class) {
                this.f2756a.E.setTag(R.id.tma_st_slot_tag, "05_001");
                str = "05_001";
            }
            this.f2756a.c(((e) this.f2756a.ag.get(this.f2756a.ah)).b);
            this.f2756a.b(str, Constants.STR_EMPTY);
        }
        if (this.f2756a.aG) {
            String string4 = this.f2756a.getString(R.string.manage_need_more);
            if (this.f2756a.aC == 100) {
                string4 = this.f2756a.getString(R.string.manage_great);
            }
            this.f2756a.b(string4);
        } else {
            boolean unused7 = this.f2756a.aG = true;
            String string5 = this.f2756a.getString(R.string.manage_opt_100);
            String str2 = "04_001";
            if (this.f2756a.ai > 0) {
                string5 = this.f2756a.getString(R.string.manage_tips_2, new Object[]{bt.c(this.f2756a.ai)});
                str2 = "04_001";
            }
            if (this.f2756a.aj > 0) {
                string5 = this.f2756a.getString(R.string.manage_tips_3, new Object[]{bt.c(this.f2756a.aj)});
            }
            if (this.f2756a.ai > 0 && this.f2756a.aj > 0) {
                str2 = "04_002";
                string5 = this.f2756a.getString(R.string.manage_tips_1, new Object[]{bt.c(this.f2756a.ai), bt.c(this.f2756a.aj)});
            }
            this.f2756a.b(string5);
            this.f2756a.b(str2, Constants.STR_EMPTY);
        }
        this.f2756a.D();
    }
}
