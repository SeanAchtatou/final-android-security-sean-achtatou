package com.tencent.assistantv2.activity;

import android.widget.AbsListView;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistantv2.component.RankCustomizeListPage;
import com.tencent.assistantv2.component.RankNormalListPage;

/* compiled from: ProGuard */
class ci extends ListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    boolean f2805a = false;
    int b;
    boolean c = false;
    final /* synthetic */ cc d;

    ci(cc ccVar) {
        this.d = ccVar;
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        super.onScroll(absListView, i, i2, i3);
        if (i != 0) {
            this.c = true;
        }
        if (i == 2) {
            this.f2805a = true;
        } else if (i == 1) {
            this.f2805a = false;
        }
        this.b = i;
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
        if (this.d.ae != null) {
            this.d.ae.b(i);
        }
        if (i == 1) {
            this.c = false;
        }
        if (i != 0) {
            return;
        }
        if ((this.d.P.get(this.d.ab) instanceof RankNormalListPage) && this.f2805a) {
            ((RankNormalListPage) this.d.P.get(this.d.ab)).showHideInstalledAppArea(false);
        } else if (!(this.d.P.get(this.d.ab) instanceof RankCustomizeListPage)) {
        } else {
            if (this.b != 0) {
                ((RankCustomizeListPage) this.d.P.get(this.d.ab)).a(false);
            } else if (this.c) {
                ((RankCustomizeListPage) this.d.P.get(this.d.ab)).a(true);
            } else {
                ((RankCustomizeListPage) this.d.P.get(this.d.ab)).h();
            }
        }
    }
}
