package com.tencent.assistantv2.activity;

import android.widget.CompoundButton;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class bw implements CompoundButton.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GuideActivity f2793a;

    bw(GuideActivity guideActivity) {
        this.f2793a = guideActivity;
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        XLog.i(this.f2793a.u, "id=" + compoundButton.getId() + " equal=" + (compoundButton == this.f2793a.B));
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2793a, 200);
        buildSTInfo.scene = STConst.ST_PAGE_GUIDE_RESULT;
        if (compoundButton == this.f2793a.B) {
            buildSTInfo.slotId = "03_002";
            buildSTInfo.status = z ? "001" : "002";
        } else if (compoundButton == this.f2793a.C) {
            buildSTInfo.slotId = "03_003";
            buildSTInfo.status = z ? "001" : "002";
        }
        k.a(buildSTInfo);
    }
}
