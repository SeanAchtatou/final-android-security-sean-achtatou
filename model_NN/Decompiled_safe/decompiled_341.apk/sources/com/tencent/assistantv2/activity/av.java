package com.tencent.assistantv2.activity;

import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistantv2.component.cu;

/* compiled from: ProGuard */
class av implements cu {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AssistantTabActivity f2769a;

    av(AssistantTabActivity assistantTabActivity) {
        this.f2769a = assistantTabActivity;
    }

    public void a() {
        if (this.f2769a.F != null) {
            this.f2769a.F.setVisibility(this.f2769a.E.b() == 0 ? 4 : 0);
        }
        this.f2769a.v.b(true);
        this.f2769a.v.b();
    }

    public void b() {
        if (this.f2769a.F != null) {
            this.f2769a.F.setVisibility(0);
            if (this.f2769a.E.b() == 0) {
                this.f2769a.F.startAnimation(AnimationUtils.loadAnimation(this.f2769a.u, R.anim.mgr_fade_in));
            } else {
                this.f2769a.b(this.f2769a.d(this.f2769a.aC));
            }
        }
        this.f2769a.v.smoothScrollTo(0, 0);
        this.f2769a.v.c();
        this.f2769a.v.b(false);
    }

    public void a(float f, Transformation transformation) {
        this.f2769a.v.smoothScrollTo(0, 0);
    }
}
