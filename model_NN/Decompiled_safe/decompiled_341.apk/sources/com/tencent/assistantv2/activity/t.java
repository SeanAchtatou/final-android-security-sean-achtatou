package com.tencent.assistantv2.activity;

import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.a;
import com.tencent.assistant.module.callback.f;
import com.tencent.assistant.module.callback.z;
import com.tencent.assistant.protocol.jce.AppHotFriend;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.protocol.jce.GetAppTagInfoListResponse;
import com.tencent.assistant.protocol.jce.GetRecommendAppListResponse;
import com.tencent.assistantv2.component.appdetail.AppDetailViewV5;
import com.tencent.assistantv2.model.a.c;
import com.tencent.assistantv2.model.a.d;
import com.tencent.connect.common.Constants;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
class t implements a, f, z, c, d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f2862a;

    private t(AppDetailActivityV5 appDetailActivityV5) {
        this.f2862a = appDetailActivityV5;
    }

    /* synthetic */ t(AppDetailActivityV5 appDetailActivityV5, b bVar) {
        this(appDetailActivityV5);
    }

    public void a(int i, int i2, com.tencent.assistant.model.c cVar, int i3) {
        this.f2862a.a(i, i2, cVar, i3);
    }

    public void a(int i, int i2, boolean z, List<SimpleAppModel> list, boolean z2, byte[] bArr) {
        if ((this.f2862a.aV & 2) == 0) {
            this.f2862a.a(i, i2, z, list, z2, bArr);
        }
    }

    public void a(int i, int i2, String str, GetRecommendAppListResponse getRecommendAppListResponse) {
        if (this.f2862a.v[1] == i && (this.f2862a.aV & 1) == 0) {
            this.f2862a.a(i2, getRecommendAppListResponse);
        }
    }

    public void a(int i, int i2, String str, ArrayList<AppHotFriend> arrayList) {
        if (i2 != 0 || i != this.f2862a.v[7]) {
            return;
        }
        if (this.f2862a.aW) {
            this.f2862a.H.a(str, arrayList);
            return;
        }
        HashMap hashMap = new HashMap();
        this.f2862a.u[4] = new ViewInvalidateMessage(4);
        hashMap.put("hasNext", str);
        hashMap.put("pageContext", arrayList);
        this.f2862a.u[4].params = hashMap;
    }

    public void a(int i, int i2, ArrayList<CommentDetail> arrayList, ArrayList<CommentTagInfo> arrayList2, ArrayList<CommentTagInfo> arrayList3) {
        if (i2 == 0 && i == this.f2862a.v[7]) {
            this.f2862a.H.b().a(this.f2862a.bc);
            this.f2862a.x.clear();
            this.f2862a.x.addAll(arrayList3);
            if (this.f2862a.aW) {
                this.f2862a.H.b().a(arrayList, arrayList2);
                return;
            }
            this.f2862a.u[5] = new ViewInvalidateMessage(5);
            HashMap hashMap = new HashMap();
            hashMap.put("selectedCommentList", arrayList);
            hashMap.put("goodTagList", arrayList2);
            this.f2862a.u[5].params = hashMap;
        }
    }

    public void a(int i, int i2, GetAppTagInfoListResponse getAppTagInfoListResponse) {
        if (i2 == 0 && i == this.f2862a.v[6]) {
            if (this.f2862a.ao) {
                getAppTagInfoListResponse.c.clear();
            }
            if (this.f2862a.aW) {
                this.f2862a.H.b().a(getAppTagInfoListResponse.b, this.f2862a.ac.f1634a, this.f2862a.ac.c);
                if (getAppTagInfoListResponse.c.size() >= 2) {
                    this.f2862a.I.a(getAppTagInfoListResponse.e, getAppTagInfoListResponse.c, this.f2862a.aV);
                    return;
                }
                return;
            }
            this.f2862a.u[6] = new ViewInvalidateMessage(6);
            HashMap hashMap = new HashMap();
            hashMap.put("hasNext", getAppTagInfoListResponse.b);
            hashMap.put("pageContext", getAppTagInfoListResponse.c);
            hashMap.put("pageText", getAppTagInfoListResponse.e);
            this.f2862a.u[6].params = hashMap;
        }
    }

    public void a(int i, int i2, int i3, int i4) {
        String str;
        if (i2 != 0 || i != this.f2862a.v[7] || i3 != 0) {
            return;
        }
        if (this.f2862a.aW) {
            if (AppDetailActivityV5.a(this.f2862a.ae) && AppDetailViewV5.a(this.f2862a.ae, this.f2862a.ac) == AppDetailViewV5.APPDETAIL_MODE.NEED_UPDATE && this.f2862a.ae.f1660a.f1993a.b.get(0).F > 10000) {
                BigDecimal scale = new BigDecimal(((double) this.f2862a.ae.f1660a.f1993a.b.get(0).F) / 10000.0d).setScale(1, 4);
                str = String.format(this.f2862a.getResources().getString(R.string.appdetail_friends_update), scale);
            } else if (i4 > 0) {
                str = String.format(this.f2862a.getResources().getString(R.string.appdetail_friends_using), Integer.valueOf(i4));
            } else {
                str = Constants.STR_EMPTY;
            }
            this.f2862a.F.a(str);
            return;
        }
        this.f2862a.u[7] = new ViewInvalidateMessage(7);
        this.f2862a.u[7].params = Integer.valueOf(i4);
    }
}
