package com.tencent.assistantv2.activity;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class ax implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AssistantTabActivity f2771a;

    ax(AssistantTabActivity assistantTabActivity) {
        this.f2771a = assistantTabActivity;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f2771a.I.setVisibility(8);
        if (this.f2771a.bf > 0) {
            AssistantTabActivity.n(this.f2771a);
            this.f2771a.G();
        }
    }
}
