package com.tencent.assistantv2.activity;

import android.view.View;
import android.widget.AbsListView;
import com.tencent.assistant.component.invalidater.TXRefreshGetMoreListViewScrollListener;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;

/* compiled from: ProGuard */
class bu extends TXRefreshGetMoreListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bo f2791a;

    private bu(bo boVar) {
        this.f2791a = boVar;
    }

    /* synthetic */ bu(bo boVar, bp bpVar) {
        this(boVar);
    }

    public void onScroll(View view, int i, int i2, int i3) {
        boolean z = true;
        super.onScroll(view, i, i2, i3);
        if (this.f2791a.aa != null) {
            SmartListAdapter.BannerType unused = this.f2791a.ab = this.f2791a.aa.p();
            boolean z2 = this.f2791a.ab == SmartListAdapter.BannerType.None;
            if (this.f2791a.ab != SmartListAdapter.BannerType.HomePage) {
                z = false;
            }
            if ((!z2 && !z) && this.f2791a.ac != null) {
                this.f2791a.ac.a((AbsListView) view, i, i2, i3);
            }
        }
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
        if (this.f2791a.ac != null) {
            this.f2791a.ac.b(absListView, i);
        }
    }
}
