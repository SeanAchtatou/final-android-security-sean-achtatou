package com.tencent.assistantv2.activity;

import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

/* compiled from: ProGuard */
class dc implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public boolean f2826a = false;
    long b = 0;
    int c = 20;
    int d = 250;
    int e;
    final /* synthetic */ MainActivity f;
    private long g = -1;
    private int h = 0;
    private Interpolator i = new DecelerateInterpolator();

    public dc(MainActivity mainActivity) {
        this.f = mainActivity;
    }

    public void a(int i2, int i3) {
        this.h = i2;
        this.e = i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public void run() {
        int round;
        if (this.g == -1) {
            this.f2826a = true;
            this.g = System.currentTimeMillis();
            round = 0;
        } else {
            this.b = ((System.currentTimeMillis() - this.g) * 1000) / ((long) this.d);
            this.b = Math.max(Math.min(this.b, 1000L), 0L);
            round = Math.round(((float) this.e) * this.i.getInterpolation(((float) this.b) / 1000.0f));
            if (Math.abs(round - this.e) < 4) {
                round = this.e;
            }
            this.f.M.scrollTo(this.h + round, 0);
        }
        if (this.b >= 1000 || round == this.e) {
            this.f2826a = false;
        } else if (this.f2826a) {
            this.f.M.postDelayed(this, (long) this.c);
        }
    }
}
