package com.tencent.assistantv2.activity;

import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.model.c;
import com.tencent.assistant.module.t;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f2773a;

    b(AppDetailActivityV5 appDetailActivityV5) {
        this.f2773a = appDetailActivityV5;
    }

    public void run() {
        LocalApkInfo unused = this.f2773a.aQ = ApkResourceManager.getInstance().getInstalledApkInfo(this.f2773a.ac.c, true);
        if ("4".equals(this.f2773a.aM)) {
            this.f2773a.ac.ac = Constants.STR_EMPTY;
        }
        t a2 = this.f2773a.Z.a(this.f2773a.ac, this.f2773a.aQ, this.f2773a.aL, this.f2773a.aN, this.f2773a.aO, this.f2773a.aP);
        if (a2 != null) {
            c unused2 = this.f2773a.ae = a2.f1857a;
            this.f2773a.v[3] = a2.b;
        }
    }
}
