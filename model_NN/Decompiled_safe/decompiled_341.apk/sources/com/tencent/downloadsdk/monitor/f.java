package com.tencent.downloadsdk.monitor;

import android.net.NetworkInfo;
import com.tencent.downloadsdk.utils.NetInfo;
import com.tencent.downloadsdk.utils.k;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private static f f3615a = null;
    private b b = new b();
    private d c = new d();
    private a d = new a();

    private f() {
    }

    public static synchronized f a() {
        f fVar;
        synchronized (f.class) {
            if (f3615a == null) {
                f3615a = new f();
            }
            fVar = f3615a;
        }
        return fVar;
    }

    public void a(NetworkInfo networkInfo) {
        if (networkInfo != null) {
            NetInfo.APN f = k.f();
            k.g();
            NetInfo.APN f2 = k.f();
            if (f == f2) {
                return;
            }
            if (f == NetInfo.APN.NO_NETWORK) {
                this.b.a(f2);
            } else if (f2 == NetInfo.APN.NO_NETWORK) {
                this.b.b(f);
            } else {
                this.b.a(f, f2);
            }
        }
    }

    public void a(boolean z) {
        if (z) {
            this.c.b();
        } else {
            this.c.a();
        }
    }
}
