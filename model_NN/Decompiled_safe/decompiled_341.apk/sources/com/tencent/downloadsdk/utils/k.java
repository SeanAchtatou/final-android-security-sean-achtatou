package com.tencent.downloadsdk.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.downloadsdk.utils.NetInfo;

public class k {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f3651a = true;
    private static NetInfo b = new NetInfo();
    private static boolean c = false;

    public static int a(String str) {
        if (!TextUtils.isEmpty(str)) {
            if (str.equals("46000") || str.equals("46002") || str.equals("46007")) {
                return 0;
            }
            if (str.equals("46001")) {
                return 1;
            }
            if (str.equals("46003")) {
                return 2;
            }
        }
        return -1;
    }

    public static NetworkInfo a(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager != null) {
            try {
                return connectivityManager.getActiveNetworkInfo();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static boolean a() {
        if (b.f3645a == NetInfo.APN.UN_DETECT) {
            g();
        }
        return f3651a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001b, code lost:
        if (r0.isAvailable() == false) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.tencent.downloadsdk.utils.NetInfo b(android.content.Context r4) {
        /*
            r3 = 1
            com.tencent.downloadsdk.utils.NetInfo r1 = new com.tencent.downloadsdk.utils.NetInfo
            r1.<init>()
            r2 = 0
            java.lang.String r0 = "connectivity"
            java.lang.Object r0 = r4.getSystemService(r0)
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0
            if (r0 == 0) goto L_0x003f
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()     // Catch:{ Exception -> 0x0026 }
        L_0x0015:
            if (r0 == 0) goto L_0x001d
            boolean r2 = r0.isAvailable()     // Catch:{ Exception -> 0x003d }
            if (r2 != 0) goto L_0x0028
        L_0x001d:
            r2 = 0
            com.tencent.downloadsdk.utils.k.f3651a = r2     // Catch:{ Exception -> 0x003d }
            com.tencent.downloadsdk.utils.NetInfo$APN r2 = com.tencent.downloadsdk.utils.NetInfo.APN.NO_NETWORK     // Catch:{ Exception -> 0x003d }
            r1.f3645a = r2     // Catch:{ Exception -> 0x003d }
            r0 = r1
        L_0x0025:
            return r0
        L_0x0026:
            r0 = move-exception
            r0 = r2
        L_0x0028:
            com.tencent.downloadsdk.utils.k.f3651a = r3
            if (r0 == 0) goto L_0x0038
            int r0 = r0.getType()
            if (r0 != r3) goto L_0x0038
            com.tencent.downloadsdk.utils.NetInfo$APN r0 = com.tencent.downloadsdk.utils.NetInfo.APN.WIFI
            r1.f3645a = r0
            r0 = r1
            goto L_0x0025
        L_0x0038:
            com.tencent.downloadsdk.utils.NetInfo r0 = c(r4)
            goto L_0x0025
        L_0x003d:
            r2 = move-exception
            goto L_0x0028
        L_0x003f:
            r0 = r2
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.utils.k.b(android.content.Context):com.tencent.downloadsdk.utils.NetInfo");
    }

    public static boolean b() {
        return !TextUtils.isEmpty(Proxy.getDefaultHost());
    }

    private static NetInfo c(Context context) {
        NetInfo netInfo = new NetInfo();
        boolean b2 = b();
        netInfo.d = b2;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String networkOperator = telephonyManager.getNetworkOperator();
        netInfo.b = networkOperator;
        int networkType = telephonyManager.getNetworkType();
        netInfo.c = networkType;
        switch (a(networkOperator)) {
            case 0:
                switch (networkType) {
                    case 1:
                    case 2:
                        if (b2) {
                            netInfo.f3645a = NetInfo.APN.CMWAP;
                        } else {
                            netInfo.f3645a = NetInfo.APN.CMNET;
                        }
                        return netInfo;
                    default:
                        if (b2) {
                            netInfo.f3645a = NetInfo.APN.UNKNOW_WAP;
                        } else {
                            netInfo.f3645a = NetInfo.APN.UNKNOWN;
                        }
                        return netInfo;
                }
            case 1:
                switch (networkType) {
                    case 1:
                    case 2:
                        if (b2) {
                            netInfo.f3645a = NetInfo.APN.UNIWAP;
                        } else {
                            netInfo.f3645a = NetInfo.APN.UNINET;
                        }
                        return netInfo;
                    case 3:
                    case 8:
                    case 10:
                    case 15:
                        if (b2) {
                            netInfo.f3645a = NetInfo.APN.WAP3G;
                        } else {
                            netInfo.f3645a = NetInfo.APN.NET3G;
                        }
                        return netInfo;
                    default:
                        if (b2) {
                            netInfo.f3645a = NetInfo.APN.UNKNOW_WAP;
                        } else {
                            netInfo.f3645a = NetInfo.APN.UNKNOWN;
                        }
                        return netInfo;
                }
            case 2:
                if (b2) {
                    netInfo.f3645a = NetInfo.APN.CTWAP;
                } else {
                    netInfo.f3645a = NetInfo.APN.CTNET;
                }
                return netInfo;
            default:
                if (b2) {
                    netInfo.f3645a = NetInfo.APN.UNKNOW_WAP;
                } else {
                    netInfo.f3645a = NetInfo.APN.UNKNOWN;
                }
                return netInfo;
        }
    }

    public static boolean c() {
        NetInfo.APN f = f();
        return f == NetInfo.APN.CMNET || f == NetInfo.APN.CMWAP || f == NetInfo.APN.UNINET || f == NetInfo.APN.UNIWAP;
    }

    public static boolean d() {
        NetInfo.APN f = f();
        return f == NetInfo.APN.CTWAP || f == NetInfo.APN.CTNET || f == NetInfo.APN.WAP3G || f == NetInfo.APN.NET3G;
    }

    public static NetInfo e() {
        if (b.f3645a == NetInfo.APN.UN_DETECT) {
            g();
        }
        return b;
    }

    public static NetInfo.APN f() {
        return e().f3645a;
    }

    public static void g() {
        b = b(DownloadManager.a().b());
    }
}
