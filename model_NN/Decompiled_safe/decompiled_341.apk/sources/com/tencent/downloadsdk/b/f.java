package com.tencent.downloadsdk.b;

import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class f {

    /* renamed from: a  reason: collision with root package name */
    public String f3602a;
    public long b;
    public long c;
    public String d;
    public long e;
    public long f;
    public long g;
    public long h;
    public int i;
    public long j;
    public String k;
    public int l;
    public int m;
    public int n;
    public HashMap<Long, a> o;
    public ArrayList<String> p;
    public Map<String, String> q;
    public boolean r = false;
    public String s = Constants.STR_EMPTY;

    public f(String str) {
        this.f3602a = str;
        this.o = new HashMap<>();
        this.q = new HashMap();
        this.p = new ArrayList<>();
    }

    public void a(int i2) {
        this.p.add(i2 + Constants.STR_EMPTY);
    }

    public void a(String str, String str2) {
        a(str, str2, false);
    }

    public void a(String str, String str2, boolean z) {
        String str3;
        if (z && (str3 = this.q.get(str)) != null) {
            str2 = str3 + "+" + str2;
        }
        this.q.put(str, str2);
    }
}
