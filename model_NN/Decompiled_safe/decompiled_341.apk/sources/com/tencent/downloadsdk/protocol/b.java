package com.tencent.downloadsdk.protocol;

public class b {
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0030 A[SYNTHETIC, Splitter:B:12:0x0030] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(byte[] r5) {
        /*
            r2 = 0
            java.util.zip.Deflater r3 = new java.util.zip.Deflater
            r3.<init>()
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x004b }
            int r0 = r5.length     // Catch:{ all -> 0x004b }
            r1.<init>(r0)     // Catch:{ all -> 0x004b }
            r0 = 9
            r3.setLevel(r0)     // Catch:{ all -> 0x002a }
            r3.setInput(r5)     // Catch:{ all -> 0x002a }
            r3.finish()     // Catch:{ all -> 0x002a }
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r0]     // Catch:{ all -> 0x002a }
        L_0x001b:
            boolean r2 = r3.finished()     // Catch:{ all -> 0x002a }
            if (r2 != 0) goto L_0x0034
            int r2 = r3.deflate(r0)     // Catch:{ all -> 0x002a }
            r4 = 0
            r1.write(r0, r4, r2)     // Catch:{ all -> 0x002a }
            goto L_0x001b
        L_0x002a:
            r0 = move-exception
        L_0x002b:
            r3.end()
            if (r1 == 0) goto L_0x0033
            r1.close()     // Catch:{ IOException -> 0x0046 }
        L_0x0033:
            throw r0
        L_0x0034:
            r3.end()
            if (r1 == 0) goto L_0x003c
            r1.close()     // Catch:{ IOException -> 0x0041 }
        L_0x003c:
            byte[] r0 = r1.toByteArray()
            return r0
        L_0x0041:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x003c
        L_0x0046:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0033
        L_0x004b:
            r0 = move-exception
            r1 = r2
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.protocol.b.a(byte[]):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002b A[SYNTHETIC, Splitter:B:14:0x002b] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0044 A[SYNTHETIC, Splitter:B:26:0x0044] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] b(byte[] r6) {
        /*
            java.util.zip.Inflater r2 = new java.util.zip.Inflater
            r2.<init>()
            r1 = 0
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ DataFormatException -> 0x0051, all -> 0x003e }
            int r3 = r6.length     // Catch:{ DataFormatException -> 0x0051, all -> 0x003e }
            r0.<init>(r3)     // Catch:{ DataFormatException -> 0x0051, all -> 0x003e }
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]     // Catch:{ DataFormatException -> 0x0022 }
            r2.setInput(r6)     // Catch:{ DataFormatException -> 0x0022 }
        L_0x0013:
            boolean r3 = r2.finished()     // Catch:{ DataFormatException -> 0x0022 }
            if (r3 != 0) goto L_0x0033
            int r3 = r2.inflate(r1)     // Catch:{ DataFormatException -> 0x0022 }
            r4 = 0
            r0.write(r1, r4, r3)     // Catch:{ DataFormatException -> 0x0022 }
            goto L_0x0013
        L_0x0022:
            r1 = move-exception
        L_0x0023:
            r1.printStackTrace()     // Catch:{ all -> 0x004c }
            r2.end()
            if (r0 == 0) goto L_0x002e
            r0.close()     // Catch:{ IOException -> 0x0048 }
        L_0x002e:
            byte[] r0 = r0.toByteArray()
            return r0
        L_0x0033:
            r2.end()
            if (r0 == 0) goto L_0x002e
            r0.close()     // Catch:{ IOException -> 0x003c }
            goto L_0x002e
        L_0x003c:
            r1 = move-exception
            goto L_0x002e
        L_0x003e:
            r0 = move-exception
        L_0x003f:
            r2.end()
            if (r1 == 0) goto L_0x0047
            r1.close()     // Catch:{ IOException -> 0x004a }
        L_0x0047:
            throw r0
        L_0x0048:
            r1 = move-exception
            goto L_0x002e
        L_0x004a:
            r1 = move-exception
            goto L_0x0047
        L_0x004c:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x003f
        L_0x0051:
            r0 = move-exception
            r5 = r0
            r0 = r1
            r1 = r5
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.protocol.b.b(byte[]):byte[]");
    }
}
