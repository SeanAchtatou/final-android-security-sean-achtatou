package com.tencent.downloadsdk.protocol.jce;

import java.io.Serializable;

public final class ResponseHeadErrorCode implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final ResponseHeadErrorCode f3631a = new ResponseHeadErrorCode(0, -10, "EC_SERVER_INTERNAL_ERROR");
    public static final ResponseHeadErrorCode b = new ResponseHeadErrorCode(1, -11, "EC_BUSINESS_ERROR");
    static final /* synthetic */ boolean c = (!ResponseHeadErrorCode.class.desiredAssertionStatus());
    private static ResponseHeadErrorCode[] d = new ResponseHeadErrorCode[2];
    private int e;
    private String f = new String();

    private ResponseHeadErrorCode(int i, int i2, String str) {
        this.f = str;
        this.e = i2;
        d[i] = this;
    }

    public String toString() {
        return this.f;
    }
}
