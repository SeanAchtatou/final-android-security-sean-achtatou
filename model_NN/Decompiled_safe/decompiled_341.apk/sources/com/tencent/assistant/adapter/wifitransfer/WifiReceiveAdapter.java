package com.tencent.assistant.adapter.wifitransfer;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.DownloadProgressBar;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.localres.LocalMediaLoader;
import com.tencent.assistant.localres.WiFiReceiveItem;
import com.tencent.assistant.localres.WiFiReceiveLoader;
import com.tencent.assistant.utils.df;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class WifiReceiveAdapter extends BaseAdapter implements LocalMediaLoader.ILocalMediaLoaderListener<WiFiReceiveItem> {

    /* renamed from: a  reason: collision with root package name */
    protected WiFiReceiveLoader f811a;
    private boolean b;
    private Context c;
    private LayoutInflater d;
    /* access modifiers changed from: private */
    public int e;
    private SparseArray<ArrayList<WiFiReceiveItem>> f;
    private ArrayList<Integer> g;
    private int h;
    private int i;
    private int j;
    /* access modifiers changed from: private */
    public f k;

    private synchronized void a(ArrayList<WiFiReceiveItem> arrayList, boolean z) {
        if (arrayList != null) {
            if (arrayList.size() > 0) {
                this.f.put(1, arrayList);
                if (this.g.size() == 0) {
                    this.g.add(1);
                } else if (!this.g.contains(1)) {
                    if (this.g.contains(2)) {
                        this.g.add(this.g.indexOf(2), 1);
                    } else {
                        this.g.add(1);
                    }
                }
            }
        }
        ArrayList<WiFiReceiveItem> longerList = this.f811a.getLongerList();
        if (longerList != null && longerList.size() > 0) {
            this.f.put(2, longerList);
            if (!this.g.contains(2)) {
                this.g.add(2);
            }
        }
        if (this.b) {
            notifyDataSetChanged();
        }
        a();
    }

    public int getCount() {
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int i4 = i2;
            if (i3 >= this.f.size()) {
                return i4;
            }
            ArrayList arrayList = this.f.get(this.g.get(i3).intValue());
            if (arrayList == null) {
                i2 = i4;
            } else if (this.g.get(i3).intValue() == 0) {
                i2 = a(arrayList.size()) + i4;
            } else {
                i2 = a(arrayList.size()) + 1 + i4;
            }
            i3++;
        }
    }

    public Object getItem(int i2) {
        int i3 = 0;
        int i4 = 0;
        while (true) {
            int i5 = i3;
            int i6 = i3;
            if (i4 >= this.g.size()) {
                return null;
            }
            ArrayList arrayList = this.f.get(this.g.get(i4).intValue());
            if (arrayList == null) {
                return null;
            }
            if (this.g.get(i4).intValue() == 0) {
                i3 = a(arrayList.size()) + i5;
                if (i2 < i3 && i2 >= i6) {
                    return a(arrayList, i2 - i6);
                }
            } else {
                i3 = a(arrayList.size()) + 1 + i5;
                if (i2 == i6) {
                    return this.g.get(i4);
                }
                if (i2 < i3 && i2 > i6) {
                    return a(arrayList, (i2 - i6) - 1);
                }
            }
            i4++;
        }
    }

    private int a(int i2) {
        return (int) Math.ceil(((double) i2) / ((double) this.e));
    }

    private List<WiFiReceiveItem> a(List<WiFiReceiveItem> list, int i2) {
        int i3 = this.e + (this.e * i2);
        int size = list.size();
        if (i3 <= size) {
            size = i3;
        }
        return list.subList(this.e * i2, size);
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    public int getItemViewType(int i2) {
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 < this.g.size()) {
            if (this.g.get(i3).intValue() == 0) {
                i4 += a(this.f.get(this.g.get(i3).intValue()).size());
                if (i2 < i4 && i2 >= i5) {
                    return 0;
                }
            } else {
                i4 += a(this.f.get(this.g.get(i3).intValue()).size()) + 1;
                if (i2 == i5) {
                    return 1;
                }
                if (i2 < i4 && i2 > i5) {
                    return 0;
                }
            }
            i3++;
            i5 = i4;
        }
        return 0;
    }

    public int getViewTypeCount() {
        return 2;
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        g gVar;
        TextView textView;
        boolean z = true;
        if (getItemViewType(i2) == 1) {
            if (view == null || ((h) view.getTag()).b == null) {
                view = this.d.inflate((int) R.layout.wifi_receive_group, (ViewGroup) null);
                TextView textView2 = (TextView) view.findViewById(R.id.group_title);
                h hVar = new h(this, null);
                hVar.b = textView2;
                view.setTag(hVar);
                textView = textView2;
            } else {
                textView = ((h) view.getTag()).b;
            }
            if (((Integer) getItem(i2)).intValue() == 2) {
                textView.setText(this.c.getText(R.string.older));
            } else {
                textView.setText(this.c.getText(R.string.one_week));
            }
        } else {
            List list = (List) getItem(i2);
            if (view == null || ((h) view.getTag()).f819a == null) {
                g gVar2 = new g(this);
                view = this.d.inflate((int) R.layout.wifi_receive_list_item, (ViewGroup) null);
                LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.line);
                linearLayout.setWeightSum((float) this.e);
                int b2 = df.b(3.0f);
                linearLayout.setPadding(b2, 0, 0, b2);
                if (!(i2 == 0 || getItemViewType(i2 - 1) == 1)) {
                    z = false;
                }
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1, 1.0f);
                if (z) {
                    layoutParams.setMargins(0, b2, b2, 0);
                } else {
                    layoutParams.setMargins(0, 0, b2, 0);
                }
                for (int i3 = 0; i3 < this.e; i3++) {
                    View inflate = this.d.inflate((int) R.layout.wifitransfer_receive_gridview_item, (ViewGroup) null);
                    e eVar = gVar2.f818a.get(i3);
                    eVar.f817a = inflate;
                    eVar.b = (TXImageView) inflate.findViewById(R.id.gridview_item_imageview);
                    eVar.c = (TextView) inflate.findViewById(R.id.gridview_item_label);
                    eVar.d = (DownloadProgressBar) inflate.findViewById(R.id.gridview_item_progressbar);
                    eVar.e = (ImageView) inflate.findViewById(R.id.type_icon);
                    eVar.f = (ImageView) inflate.findViewById(R.id.type_bg);
                    linearLayout.addView(inflate);
                    inflate.setLayoutParams(layoutParams);
                }
                h hVar2 = new h(this, null);
                hVar2.f819a = gVar2;
                view.setTag(hVar2);
                gVar = gVar2;
            } else {
                gVar = ((h) view.getTag()).f819a;
            }
            for (int i4 = 0; i4 < this.e; i4++) {
                e eVar2 = gVar.f818a.get(i4);
                if (list == null || i4 >= list.size()) {
                    eVar2.d.setVisibility(8);
                    eVar2.b.setVisibility(8);
                    eVar2.c.setVisibility(8);
                    eVar2.f.setVisibility(8);
                    eVar2.e.setVisibility(8);
                    eVar2.f817a.setOnClickListener(new b(this));
                    eVar2.f817a.setBackgroundColor(this.c.getResources().getColor(17170445));
                } else {
                    a(eVar2, (WiFiReceiveItem) list.get(i4), i2, i4);
                }
            }
        }
        return view;
    }

    private void a(e eVar, WiFiReceiveItem wiFiReceiveItem, int i2, int i3) {
        int i4 = 0;
        eVar.b.setVisibility(0);
        eVar.c.setVisibility(0);
        eVar.b.updateImageView(wiFiReceiveItem.mFilePath, a(wiFiReceiveItem.mImageType), wiFiReceiveItem.mImageType);
        eVar.c.setText(wiFiReceiveItem.mFileName);
        if (wiFiReceiveItem.mReceiveState == 0) {
            eVar.d.setVisibility(0);
            eVar.f817a.setBackgroundResource(R.drawable.bg_card_gray_selector);
        } else {
            eVar.d.setVisibility(8);
            eVar.f817a.setBackgroundResource(R.drawable.bg_card_selector);
        }
        synchronized (this) {
            eVar.d.setTag(wiFiReceiveItem.mFileId);
        }
        if (wiFiReceiveItem.mFileSize != 0) {
            i4 = (int) (((((float) wiFiReceiveItem.mReceiveSize) * 1.0f) / ((float) wiFiReceiveItem.mFileSize)) * 1.0f * 100.0f);
        }
        eVar.d.setProgressAndDownloading(i4, DownloadProgressBar.ProgressDownloadType.DOWNLOADING);
        a(wiFiReceiveItem.mFileType, eVar);
        eVar.f817a.setOnClickListener(new c(this, wiFiReceiveItem, i2, i3));
    }

    /* access modifiers changed from: protected */
    public int a(TXImageView.TXImageViewType tXImageViewType) {
        switch (d.f816a[tXImageViewType.ordinal()]) {
            case 1:
            default:
                return R.drawable.pic_default_unknow;
            case 2:
                return R.drawable.pic_default_music;
            case 3:
                return R.drawable.pic_default_video;
            case 4:
                return R.drawable.pic_default_pic;
            case 5:
                return R.drawable.pic_default_app;
        }
    }

    private void a(String str, e eVar) {
        if (str.equals("jpg") || str.equals("gif") || str.equals("png") || str.equals("jpeg") || str.equals("bmp")) {
            eVar.e.setImageResource(R.drawable.icon_pic);
            eVar.e.setVisibility(0);
            eVar.f.setVisibility(0);
        } else if (str.equals("m4a") || str.equals("mp3") || str.equals("mid") || str.equals("xmf") || str.equals("ogg") || str.equals("wav")) {
            eVar.e.setImageResource(R.drawable.icon_music);
            eVar.e.setVisibility(0);
            eVar.f.setVisibility(0);
        } else if (str.equals("3gp") || str.equals("mp4")) {
            eVar.e.setImageResource(R.drawable.icon_video);
            eVar.e.setVisibility(0);
            eVar.f.setVisibility(0);
        } else {
            eVar.e.setVisibility(8);
            eVar.f.setVisibility(8);
        }
    }

    public void onLocalMediaLoaderFinish(LocalMediaLoader<WiFiReceiveItem> localMediaLoader, ArrayList<WiFiReceiveItem> arrayList, boolean z) {
        a(arrayList, z);
        if (this.k != null && this.b) {
            this.k.a(this, getCount());
        }
    }

    public void a() {
        ArrayList arrayList = this.f.get(0);
        if (arrayList != null) {
            this.h = a(arrayList.size());
        } else {
            this.h = 0;
        }
        ArrayList arrayList2 = this.f.get(1);
        if (arrayList2 != null) {
            this.i = a(arrayList2.size()) + 1;
        } else {
            this.i = 0;
        }
        ArrayList arrayList3 = this.f.get(2);
        if (arrayList3 != null) {
            this.j = a(arrayList3.size()) + 1;
        } else {
            this.j = 0;
        }
    }

    public void onLocalMediaLoaderOver() {
    }
}
