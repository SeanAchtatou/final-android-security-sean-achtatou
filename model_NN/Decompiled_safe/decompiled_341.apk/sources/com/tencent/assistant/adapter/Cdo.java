package com.tencent.assistant.adapter;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.SwitchButton;
import com.tencent.assistant.component.appdetail.HorizonImageListView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.floatingwindow.j;
import com.tencent.assistant.k;
import com.tencent.assistant.login.a.a;
import com.tencent.assistant.login.a.b;
import com.tencent.assistant.login.d;
import com.tencent.assistant.m;
import com.tencent.assistant.module.fh;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.manager.i;
import com.tencent.assistantv2.model.ItemElement;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.tencent.assistant.adapter.do  reason: invalid class name */
/* compiled from: ProGuard */
public class Cdo extends BaseExpandableListAdapter {

    /* renamed from: a  reason: collision with root package name */
    protected STPageInfo f774a = null;
    public boolean b = false;
    public boolean c = false;
    /* access modifiers changed from: private */
    public Context d;
    private LayoutInflater e;
    private Map<String, List<ItemElement>> f = new HashMap();
    private Map<String, Integer> g = new HashMap();
    private List<String> h = new ArrayList();
    private STInfoV2 i;
    /* access modifiers changed from: private */
    public volatile boolean j = false;
    /* access modifiers changed from: private */
    public Handler k = new ds(this);

    public Cdo(Context context) {
        this.d = context;
        if (this.d instanceof BaseActivity) {
            this.f774a = ((BaseActivity) this.d).q();
        } else {
            this.f774a = new STPageInfo();
        }
        this.e = LayoutInflater.from(context);
    }

    public void a(String str, int i2, int i3, List<ItemElement> list) {
        if (i3 <= this.h.size() && i2 >= 0) {
            if (i3 == -1) {
                this.h.add(str);
            } else {
                this.h.add(i3, str);
            }
            this.g.put(str, Integer.valueOf(i2));
            this.f.put(str, list);
        }
    }

    public void a() {
        if (this.h != null && this.h.size() > 0) {
            this.h.clear();
        }
        if (this.f != null && this.f.size() > 0) {
            this.f.clear();
        }
    }

    public ItemElement a(int i2, int i3) {
        List list;
        String str = this.h.get(i2);
        if (TextUtils.isEmpty(str) || (list = this.f.get(str)) == null || i3 >= list.size()) {
            return null;
        }
        return (ItemElement) list.get(i3);
    }

    public int b(int i2, int i3) {
        ItemElement itemElement;
        List list;
        String str = this.h.get(i2);
        if (TextUtils.isEmpty(str) || (list = this.f.get(str)) == null || i3 >= list.size()) {
            itemElement = null;
        } else {
            itemElement = (ItemElement) list.get(i3);
        }
        if (itemElement != null) {
            return itemElement.c;
        }
        return 0;
    }

    public ItemElement c(int i2, int i3) {
        List list;
        String str = this.h.get(i2);
        if (TextUtils.isEmpty(str) || (list = this.f.get(str)) == null || i3 >= list.size()) {
            return null;
        }
        return (ItemElement) list.get(i3);
    }

    public View getChildView(int i2, int i3, boolean z, View view, ViewGroup viewGroup) {
        ItemElement c2 = c(i2, i3);
        if (c2 != null) {
            switch (b(i2, i3)) {
                case 0:
                    view = a(c2, view, i2, i3, z);
                    break;
                case 1:
                    view = b(c2, view, i2, i3, z);
                    break;
            }
            view.setOnClickListener(new dp(this, i2, i3));
            if (!z || i2 != getGroupCount() - 1) {
                view.setPadding(0, 0, 0, 0);
            } else {
                view.setPadding(0, 0, 0, df.a(this.d, 5.0f));
            }
        }
        return view;
    }

    private View a(ItemElement itemElement, View view, int i2, int i3, boolean z) {
        dt dtVar;
        if (view == null || !(view.getTag() instanceof dt)) {
            view = this.e.inflate((int) R.layout.setting_item_arrow, (ViewGroup) null);
            dtVar = new dt(this, null);
            dtVar.f779a = view.findViewById(R.id.item_layout);
            dtVar.b = view.findViewById(R.id.top_margin);
            dtVar.c = (TextView) view.findViewById(R.id.item_title);
            dtVar.d = (TextView) view.findViewById(R.id.item_description);
            dtVar.e = (LinearLayout) view.findViewById(R.id.item_left_layout);
            dtVar.f = view.findViewById(R.id.item_line);
            dtVar.g = view.findViewById(R.id.bottom_margin);
            view.setTag(dtVar);
        } else {
            dtVar = (dt) view.getTag();
        }
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) dtVar.f779a.getLayoutParams();
        marginLayoutParams.setMargins(marginLayoutParams.leftMargin, df.b((float) itemElement.e), marginLayoutParams.rightMargin, marginLayoutParams.bottomMargin);
        dtVar.f779a.setLayoutParams(marginLayoutParams);
        dtVar.c.setText(itemElement.f3309a);
        if (itemElement.b != null) {
            dtVar.d.setText(itemElement.b);
            dtVar.d.setVisibility(0);
            dtVar.e.setPadding(0, df.a(this.d, 20.0f), 0, df.a(this.d, 17.0f));
        } else {
            dtVar.e.setPadding(0, 0, 0, 0);
            dtVar.d.setVisibility(8);
        }
        dtVar.f779a.setBackgroundResource(R.drawable.helper_cardbg_all_selector_new);
        if (z) {
            dtVar.f.setVisibility(8);
            dtVar.b.setVisibility(8);
            dtVar.g.setVisibility(0);
        } else if (i3 == 0) {
            dtVar.f.setVisibility(0);
            dtVar.b.setVisibility(0);
            dtVar.g.setVisibility(8);
        } else {
            dtVar.f.setVisibility(0);
            dtVar.b.setVisibility(8);
            dtVar.g.setVisibility(8);
        }
        return view;
    }

    private View b(ItemElement itemElement, View view, int i2, int i3, boolean z) {
        dv dvVar;
        String string;
        int i4 = itemElement.d;
        if (view == null || !(view.getTag() instanceof dv)) {
            view = this.e.inflate((int) R.layout.setting_item_switch, (ViewGroup) null);
            dvVar = new dv(this);
            dvVar.f780a = view.findViewById(R.id.item_layout);
            dvVar.b = view.findViewById(R.id.top_margin);
            dvVar.c = (TextView) view.findViewById(R.id.item_title);
            dvVar.d = (TextView) view.findViewById(R.id.item_description);
            dvVar.e = (TextView) view.findViewById(R.id.item_sub_description);
            dvVar.f = (LinearLayout) view.findViewById(R.id.item_mid_layout);
            dvVar.h = view.findViewById(R.id.item_line);
            dvVar.i = view.findViewById(R.id.bottom_margin);
            dvVar.g = (SwitchButton) view.findViewById(R.id.item_switch);
            dvVar.g.setTitlesOfSwitch(this.d.getString(R.string.setting_left_title), this.d.getString(R.string.setting_right_title));
            view.setTag(dvVar);
        } else {
            dvVar = (dv) view.getTag();
        }
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) dvVar.f780a.getLayoutParams();
        marginLayoutParams.setMargins(marginLayoutParams.leftMargin, df.b((float) itemElement.e), marginLayoutParams.rightMargin, marginLayoutParams.bottomMargin);
        dvVar.f780a.setLayoutParams(marginLayoutParams);
        dvVar.c.setText(itemElement.f3309a);
        dvVar.f780a.setBackgroundResource(R.drawable.helper_cardbg_all_selector_new);
        if (itemElement.b != null) {
            dvVar.d.setText(itemElement.b);
            dvVar.d.setVisibility(0);
            dvVar.f.setPadding(0, df.a(this.d, 20.0f), 0, df.a(this.d, 17.0f));
        } else {
            dvVar.d.setVisibility(8);
            dvVar.f.setPadding(0, 0, 0, 0);
        }
        dvVar.g.setOnSwitchListener(new dq(this, i4, i3, i2));
        if (i4 == 8) {
            dvVar.e.setVisibility(0);
            if (d.a().j()) {
                b f2 = a.f();
                dvVar.g.setSwitchState((f2.c & 1) == 1);
                if ((f2.c & 1) == 1) {
                    string = this.d.getString(R.string.setting_privacy_protection_item_sub_des_on);
                } else {
                    string = this.d.getString(R.string.setting_privacy_protection_item_sub_des_off);
                }
                if (d.a().k()) {
                    dvVar.e.setText(string + "   " + this.d.getString(R.string.setting_privacy_protection_item_sub_des_qq) + d.a().p());
                }
                if (d.a().l()) {
                    dvVar.e.setText(string + "   " + this.d.getString(R.string.setting_privacy_protection_item_sub_des_wx) + d.a().q());
                }
            } else {
                dvVar.e.setText(this.d.getString(R.string.setting_privacy_protection_item_sub_des));
                dvVar.g.setSwitchState(false);
            }
        } else {
            dvVar.e.setVisibility(8);
            dvVar.g.setSwitchState(k.a(itemElement.d));
        }
        if (z) {
            dvVar.h.setVisibility(8);
            dvVar.b.setVisibility(8);
            dvVar.i.setVisibility(0);
        } else if (i3 == 0) {
            dvVar.h.setVisibility(0);
            dvVar.b.setVisibility(0);
            dvVar.i.setVisibility(8);
        } else {
            dvVar.h.setVisibility(0);
            dvVar.b.setVisibility(8);
            dvVar.i.setVisibility(8);
        }
        return view;
    }

    public void a(int i2, View view, int i3, boolean z) {
        switch (i2) {
            case 1:
                k.a(i2, z);
                AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_AUTO_UPDATE_SWITCH_VALUE_CHANGE);
                fh.a().a(z, AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE);
                return;
            case 2:
                k.a(i2, z);
                AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_AUTO_NEW_DOWNLOAD_SWITCH_VALUE_CHANGE);
                fh.a().a(z, AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD);
                return;
            case 3:
                a(view);
                return;
            case 6:
                k.a(i2, z);
                i.a().b().a(z);
                return;
            case 8:
                if (d.a().j()) {
                    fh.a().a(z, 4);
                    return;
                }
                this.b = z;
                b();
                return;
            case 16:
                k.a(i2, z);
                a(z);
                return;
            default:
                k.a(i2, z);
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    private void a(boolean z) {
        if (z) {
            if (!m.a().a("key_has_show_float_window_create_tips", false)) {
                m.a().b("key_has_show_float_window_create_tips", (Object) true);
            }
            j.a().b();
            return;
        }
        j.a().c();
    }

    private void a(View view) {
        View view2;
        if (view instanceof RelativeLayout) {
            view2 = view.findViewById(R.id.item_switch);
        } else {
            view2 = view;
        }
        SwitchButton switchButton = (SwitchButton) view2;
        if (m.a().j()) {
            k.a(3, false);
        } else if (!this.j) {
            this.c = true;
            TemporaryThreadManager.get().start(new dr(this, switchButton));
        }
    }

    public Object getChild(int i2, int i3) {
        List list;
        String str = this.h.get(i2);
        if (TextUtils.isEmpty(str) || (list = this.f.get(str)) == null || i3 >= list.size()) {
            return null;
        }
        return (ItemElement) list.get(i3);
    }

    public long getChildId(int i2, int i3) {
        return (long) i3;
    }

    public int getChildrenCount(int i2) {
        List list;
        String str = this.h.get(i2);
        if (TextUtils.isEmpty(str) || (list = this.f.get(str)) == null) {
            return 0;
        }
        return list.size();
    }

    public Object getGroup(int i2) {
        return this.h.get(i2);
    }

    public int getGroupCount() {
        return this.f.size();
    }

    public long getGroupId(int i2) {
        return (long) i2;
    }

    public View getGroupView(int i2, boolean z, View view, ViewGroup viewGroup) {
        if (this.e == null) {
            this.e = LayoutInflater.from(this.d);
        }
        View inflate = this.e.inflate((int) R.layout.setting_group_item, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.group_title)).setText(this.d.getResources().getString(this.g.get(this.h.get(i2)).intValue()));
        return inflate;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int i2, int i3) {
        return true;
    }

    private void b() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.KEY_LOGIN_TYPE, 2);
        bundle.putInt(AppConst.KEY_FROM_TYPE, 15);
        d.a().a(AppConst.IdentityType.MOBILEQ, bundle);
    }

    /* access modifiers changed from: private */
    public String d(int i2, int i3) {
        switch (i2) {
            case 0:
                return "03_" + ct.a(i3 + 1);
            case 1:
                return "04_" + ct.a(i3 + 1);
            case 2:
                return HorizonImageListView.TMA_ST_HORIZON_IMAGE_TAG + ct.a(i3 + 1);
            case 3:
                return "06_" + ct.a(i3 + 1);
            default:
                return null;
        }
    }

    /* access modifiers changed from: private */
    public String b(boolean z) {
        if (z) {
            return "01";
        }
        return "02";
    }

    /* access modifiers changed from: private */
    public STInfoV2 a(String str, String str2, int i2) {
        if (this.i == null) {
            this.i = new STInfoV2(this.f774a.f3358a, str, this.f774a.c, this.f774a.d, i2);
        }
        this.i.status = str2;
        this.i.slotId = str;
        this.i.actionId = i2;
        return this.i;
    }
}
