package com.tencent.assistant.adapter;

import com.tencent.assistant.model.SimpleAppModel;
import java.util.Comparator;

/* compiled from: ProGuard */
class ad implements Comparator<SimpleAppModel> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ac f690a;

    ad(ac acVar) {
        this.f690a = acVar;
    }

    /* renamed from: a */
    public int compare(SimpleAppModel simpleAppModel, SimpleAppModel simpleAppModel2) {
        return a(simpleAppModel.f1634a) - a(simpleAppModel2.f1634a);
    }

    private int a(long j) {
        if (this.f690a.n == null || !this.f690a.n.containsKey(Long.valueOf(j))) {
            return 100;
        }
        return ((Integer) this.f690a.n.get(Long.valueOf(j))).intValue();
    }
}
