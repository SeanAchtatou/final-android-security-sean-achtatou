package com.tencent.assistant.adapter;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;

/* compiled from: ProGuard */
class at extends AppConst.OneBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f706a;
    final /* synthetic */ DownloadInfoMultiAdapter b;

    at(DownloadInfoMultiAdapter downloadInfoMultiAdapter, DownloadInfo downloadInfo) {
        this.b = downloadInfoMultiAdapter;
        this.f706a = downloadInfo;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>):void
      com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void */
    public void onBtnClick() {
        DownloadProxy.a().b(this.f706a.downloadTicket, false);
    }

    public void onCancell() {
    }
}
