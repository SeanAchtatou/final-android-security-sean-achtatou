package com.tencent.assistant.adapter;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class be extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bd f718a;

    be(bd bdVar) {
        this.f718a = bdVar;
    }

    public void onLeftBtnClick() {
    }

    public void onRightBtnClick() {
        TemporaryThreadManager.get().start(new bf(this));
    }

    public void onCancell() {
    }
}
