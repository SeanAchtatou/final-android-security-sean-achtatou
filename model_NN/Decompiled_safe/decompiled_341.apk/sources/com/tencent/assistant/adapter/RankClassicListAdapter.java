package com.tencent.assistant.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Message;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.AppIconView;
import com.tencent.assistant.component.RatingView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.b;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.u;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.List;

/* compiled from: ProGuard */
public class RankClassicListAdapter extends BaseAdapter implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    protected static int f676a = 0;
    protected static int b = (f676a + 1);
    protected static int c = (b + 1);
    protected ListType d;
    protected Context e;
    protected LayoutInflater f;
    protected List<SimpleAppModel> g;
    protected View h;
    protected int i;
    protected long j;
    protected long k;
    protected b l;
    private boolean m;

    /* compiled from: ProGuard */
    public enum ListType {
        LISTTYPENORMAL,
        LISTTYPEGAMESORT
    }

    public int getCount() {
        if (this.g == null) {
            return 0;
        }
        return this.g.size();
    }

    /* renamed from: a */
    public SimpleAppModel getItem(int i2) {
        if (this.g == null) {
            return null;
        }
        return this.g.get(i2);
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        SimpleAppModel simpleAppModel;
        if (this.g == null || i2 >= this.g.size()) {
            simpleAppModel = null;
        } else {
            simpleAppModel = this.g.get(i2);
        }
        if (f676a == getItemViewType(i2)) {
            return a(view, simpleAppModel, i2);
        }
        if (b == getItemViewType(i2)) {
            return b(view, simpleAppModel, i2);
        }
        return view;
    }

    public int getItemViewType(int i2) {
        if (!a()) {
            return f676a;
        }
        SimpleAppModel.CARD_TYPE card_type = this.g.get(i2).U;
        if (SimpleAppModel.CARD_TYPE.NORMAL == card_type) {
            return f676a;
        }
        if (SimpleAppModel.CARD_TYPE.QUALITY == card_type) {
            return b;
        }
        return f676a;
    }

    public int getViewTypeCount() {
        return b + 1;
    }

    /* access modifiers changed from: private */
    public String b(int i2) {
        return "03_" + ct.a(i2 + 1);
    }

    /* access modifiers changed from: protected */
    public View a(View view, SimpleAppModel simpleAppModel, int i2) {
        cv cvVar;
        int i3;
        if (view == null || ((cu) view.getTag()).f753a == null) {
            cu cuVar = new cu(this, null);
            cvVar = new cv(this, null);
            view = this.f.inflate((int) R.layout.app_item_no_desc, (ViewGroup) null);
            cvVar.f754a = view.findViewById(R.id.app_item);
            cvVar.c = (TextView) view.findViewById(R.id.text_sort);
            cvVar.d = (AppIconView) view.findViewById(R.id.app_icon_img);
            cvVar.e = (TextView) view.findViewById(R.id.app_name_txt);
            cvVar.h = (DownloadButton) view.findViewById(R.id.state_app_btn);
            cvVar.f = (RatingView) view.findViewById(R.id.app_ratingview);
            cvVar.g = (TextView) view.findViewById(R.id.download_times_txt);
            cvVar.i = view.findViewById(R.id.app_updatesizeinfo);
            cvVar.j = (TextView) view.findViewById(R.id.app_size_sumsize);
            cvVar.k = (TextView) view.findViewById(R.id.app_score_truesize);
            cvVar.l = (TextView) view.findViewById(R.id.app_size_text);
            cvVar.b = (ImageView) view.findViewById(R.id.sort_num_image);
            cvVar.m = (ImageView) view.findViewById(R.id.last_line);
            cuVar.f753a = cvVar;
            view.setTag(cuVar);
        } else {
            cvVar = ((cu) view.getTag()).f753a;
        }
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) cvVar.e.getLayoutParams();
        if (simpleAppModel == null || (!simpleAppModel.c() && !simpleAppModel.h())) {
            layoutParams.rightMargin = df.b(72.0f);
        } else {
            layoutParams.rightMargin = df.b(101.0f);
        }
        cvVar.e.setLayoutParams(layoutParams);
        cvVar.f754a.setOnClickListener(new cn(this, simpleAppModel, i2));
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) cvVar.d.getLayoutParams();
        if (ListType.LISTTYPEGAMESORT == this.d) {
            layoutParams2.leftMargin = 0;
            try {
                cvVar.c.setVisibility(0);
                cvVar.c.setText((CharSequence) null);
                if (i2 <= 2) {
                    if (i2 == 0) {
                        i3 = R.drawable.sort01;
                    } else if (1 == i2) {
                        i3 = R.drawable.sort02;
                    } else {
                        i3 = R.drawable.sort03;
                    }
                    ImageSpan imageSpan = new ImageSpan(this.e, BitmapFactory.decodeResource(this.e.getResources(), i3));
                    SpannableString spannableString = new SpannableString("icon");
                    spannableString.setSpan(imageSpan, 0, 4, 33);
                    cvVar.c.setText(spannableString);
                } else {
                    cvVar.c.setBackgroundResource(0);
                    cvVar.c.setTextColor(this.e.getResources().getColor(R.color.rank_sort_txt));
                    cvVar.c.setText(String.valueOf(i2 + 1));
                }
            } catch (Throwable th) {
                cq.a().b();
            }
        } else {
            layoutParams2.leftMargin = df.a(this.e, 9.0f);
            cvVar.c.setVisibility(8);
        }
        cvVar.d.setLayoutParams(layoutParams2);
        if (i2 == 0) {
            cvVar.f754a.setBackgroundResource(R.drawable.bg_card_download_selector);
            cvVar.m.setVisibility(0);
        } else if (i2 == getCount() - 1) {
            cvVar.f754a.setBackgroundResource(R.drawable.bg_card_download_down_selector);
            cvVar.m.setVisibility(8);
        } else {
            cvVar.f754a.setBackgroundResource(R.drawable.bg_card_download_middel_selector);
            cvVar.m.setVisibility(0);
        }
        a(cvVar, simpleAppModel, i2);
        return view;
    }

    private void a(cv cvVar, SimpleAppModel simpleAppModel, int i2) {
        if (simpleAppModel != null && cvVar != null) {
            cvVar.e.setText(simpleAppModel.d);
            if (this.m) {
                if (1 == (((int) (simpleAppModel.B >> 2)) & 3)) {
                    Drawable drawable = this.e.getResources().getDrawable(R.drawable.appdownload_icon_original);
                    drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                    cvVar.e.setCompoundDrawablePadding(df.b(6.0f));
                    cvVar.e.setCompoundDrawables(null, null, drawable, null);
                } else {
                    cvVar.e.setCompoundDrawables(null, null, null, null);
                }
            }
            cvVar.d.setSimpleAppModel(simpleAppModel, new StatInfo(simpleAppModel.b, this.i, this.l.d(), this.l.b(), this.l.a(), b(i2)), this.j);
            cvVar.h.a(simpleAppModel);
            cvVar.g.setText(ct.a(simpleAppModel.p, 0));
            cvVar.f.setRating(simpleAppModel.q);
            if (simpleAppModel.a()) {
                cvVar.i.setVisibility(0);
                cvVar.l.setVisibility(8);
                cvVar.j.setText(bt.a(simpleAppModel.k));
                cvVar.k.setText(bt.a(simpleAppModel.v));
            } else {
                cvVar.i.setVisibility(8);
                cvVar.l.setVisibility(0);
                cvVar.l.setText(bt.a(simpleAppModel.k));
            }
            if (a(simpleAppModel)) {
                cvVar.h.setClickable(false);
            } else {
                cvVar.h.setClickable(true);
                STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.e, simpleAppModel, b(i2), 200, null);
                cvVar.h.a(buildSTInfo, new co(this), (d) null, cvVar.h);
            }
            cvVar.b.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public View b(View view, SimpleAppModel simpleAppModel, int i2) {
        ct ctVar;
        if (view == null || ((cu) view.getTag()).b == null) {
            cu cuVar = new cu(this, null);
            ct ctVar2 = new ct(this, null);
            view = this.f.inflate((int) R.layout.competitive_card, (ViewGroup) null);
            ctVar2.f752a = (TextView) view.findViewById(R.id.title);
            ctVar2.b = (TXImageView) view.findViewById(R.id.pic);
            ctVar2.d = (ImageView) view.findViewById(R.id.vedio);
            ctVar2.e = (AppIconView) view.findViewById(R.id.icon);
            ctVar2.f = (DownloadButton) view.findViewById(R.id.download_soft_btn);
            ctVar2.g = (TextView) view.findViewById(R.id.name);
            ctVar2.h = (TextView) view.findViewById(R.id.app_size_sumsize);
            ctVar2.i = (ImageView) view.findViewById(R.id.app_size_redline);
            ctVar2.j = (TextView) view.findViewById(R.id.app_score_truesize);
            ctVar2.k = (TextView) view.findViewById(R.id.app_size_text);
            ctVar2.c = (ImageView) view.findViewById(R.id.sort_num_image);
            ctVar2.l = (TextView) view.findViewById(R.id.description);
            cuVar.b = ctVar2;
            view.setTag(cuVar);
            ctVar = ctVar2;
        } else {
            ctVar = ((cu) view.getTag()).b;
        }
        view.setOnClickListener(new cp(this, simpleAppModel, i2));
        a(ctVar, simpleAppModel, i2);
        return view;
    }

    private void a(ct ctVar, SimpleAppModel simpleAppModel, int i2) {
        if (simpleAppModel != null && ctVar != null) {
            if (!TextUtils.isEmpty(simpleAppModel.Y)) {
                ctVar.b.updateImageView(simpleAppModel.Y, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            } else {
                ctVar.b.setImageResource(R.drawable.pic_defaule);
            }
            if (simpleAppModel.Z == null || simpleAppModel.Z.length() == 0) {
                ctVar.d.setVisibility(8);
                ctVar.b.setDuplicateParentStateEnabled(true);
                ctVar.b.setClickable(false);
            } else {
                ctVar.d.setVisibility(0);
                ctVar.b.setDuplicateParentStateEnabled(false);
                ctVar.b.setClickable(true);
                ctVar.b.setOnClickListener(new cq(this, simpleAppModel));
            }
            ctVar.e.setSimpleAppModel(simpleAppModel, new StatInfo(simpleAppModel.b, this.i, this.l.d(), this.l.b(), this.l.a(), b(i2)), this.j);
            ctVar.f.a(simpleAppModel);
            ctVar.g.setText(simpleAppModel.d);
            if (simpleAppModel.a()) {
                ctVar.i.setVisibility(0);
                ctVar.h.setVisibility(0);
                ctVar.j.setVisibility(0);
                ctVar.k.setVisibility(8);
                ctVar.h.setText(bt.a(simpleAppModel.k));
                ctVar.j.setText(bt.a(simpleAppModel.v));
            } else {
                ctVar.i.setVisibility(8);
                ctVar.h.setVisibility(8);
                ctVar.j.setVisibility(8);
                ctVar.k.setVisibility(0);
                ctVar.k.setText(bt.a(simpleAppModel.k));
            }
            if (TextUtils.isEmpty(simpleAppModel.X)) {
                ctVar.l.setVisibility(8);
            } else {
                ctVar.l.setVisibility(0);
                ctVar.l.setText(simpleAppModel.X);
            }
            if (a(simpleAppModel)) {
                ctVar.f.setClickable(false);
            } else {
                ctVar.f.setClickable(true);
                STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.e, simpleAppModel, b(i2), 200, null);
                ctVar.f.a(buildSTInfo, new cr(this), (d) null, ctVar.f);
            }
            ctVar.c.setVisibility(8);
        }
    }

    public void handleUIEvent(Message message) {
        DownloadInfo downloadInfo;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE /*1009*/:
                if (message.obj instanceof DownloadInfo) {
                    downloadInfo = (DownloadInfo) message.obj;
                } else {
                    downloadInfo = null;
                }
                if (downloadInfo != null && !TextUtils.isEmpty(downloadInfo.downloadTicket)) {
                    for (SimpleAppModel q : this.g) {
                        if (q.q().equals(downloadInfo.downloadTicket)) {
                            b();
                            return;
                        }
                    }
                    return;
                }
                return;
            case 1016:
                u.g(this.g);
                b();
                return;
            case EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY /*1038*/:
                b();
                return;
            default:
                return;
        }
    }

    private void b() {
        ba.a().post(new cs(this));
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        if ((!c.d() || c.k()) && m.a().p()) {
            return false;
        }
        return true;
    }

    private boolean a(SimpleAppModel simpleAppModel) {
        return b(simpleAppModel) || c(simpleAppModel);
    }

    private boolean b(SimpleAppModel simpleAppModel) {
        if (simpleAppModel == null) {
            return false;
        }
        DownloadInfo d2 = DownloadProxy.a().d(simpleAppModel.q());
        if ((d2 == null || d2.versionCode != simpleAppModel.g) && simpleAppModel.d() && (simpleAppModel.P & 3) > 0 && !e.a(simpleAppModel.c, simpleAppModel.g, simpleAppModel.ad)) {
            return true;
        }
        return false;
    }

    private boolean c(SimpleAppModel simpleAppModel) {
        if (simpleAppModel == null) {
            return false;
        }
        DownloadInfo d2 = DownloadProxy.a().d(simpleAppModel.q());
        if ((d2 == null || d2.versionCode != simpleAppModel.g) && simpleAppModel.e() && (simpleAppModel.P & 3) > 0 && !e.a(simpleAppModel.c, simpleAppModel.g, simpleAppModel.ad)) {
            return true;
        }
        return false;
    }
}
