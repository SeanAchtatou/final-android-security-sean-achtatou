package com.tencent.assistant.adapter;

import android.content.Intent;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.model.StatInfo;

/* compiled from: ProGuard */
class dh implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f767a;
    final /* synthetic */ SearchMatchAdapter b;

    dh(SearchMatchAdapter searchMatchAdapter, SimpleAppModel simpleAppModel) {
        this.b = searchMatchAdapter;
        this.f767a = simpleAppModel;
    }

    public void onClick(View view) {
        int i = 0;
        try {
            i = ((Integer) view.getTag(R.id.list_item_pos)).intValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.b.a(this.f767a, i, "01", 200, SearchMatchAdapter.f678a);
        Intent intent = new Intent(this.b.c, AppDetailActivityV5.class);
        intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.f767a);
        StatInfo statInfo = new StatInfo();
        statInfo.sourceScene = STConst.ST_PAGE_SEARCH_SUGGEST;
        statInfo.f3356a = 1;
        statInfo.extraData = this.b.i;
        intent.putExtra("statInfo", statInfo);
        if (this.b.c instanceof BaseActivity) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.b.c).f());
        }
        this.b.c.startActivity(intent);
    }
}
