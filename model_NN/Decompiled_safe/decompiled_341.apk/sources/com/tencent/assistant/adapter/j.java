package com.tencent.assistant.adapter;

import android.graphics.drawable.Drawable;

/* compiled from: ProGuard */
class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Drawable f800a;
    final /* synthetic */ i b;

    j(i iVar, Drawable drawable) {
        this.b = iVar;
        this.f800a = drawable;
    }

    public void run() {
        if (this.b.b.c.getTag() != null && this.b.b.c.getTag().equals(this.b.f799a.mLocalFilePath)) {
            this.b.b.c.setImageDrawable(this.f800a);
        }
    }
}
