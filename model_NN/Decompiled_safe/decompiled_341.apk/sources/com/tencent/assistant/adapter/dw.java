package com.tencent.assistant.adapter;

import android.widget.TextView;
import com.tencent.assistant.adapter.a.c;
import com.tencent.assistant.adapter.a.d;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class dw implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpecailTopicDetailAdapter f781a;

    dw(SpecailTopicDetailAdapter specailTopicDetailAdapter) {
        this.f781a = specailTopicDetailAdapter;
    }

    public c a(Object obj) {
        if (obj != null && (obj instanceof dx)) {
            dx dxVar = (dx) obj;
            if (dxVar.f782a != null) {
                return dxVar.f782a.b;
            }
        }
        return null;
    }

    public c b(Object obj) {
        if (obj == null || !(obj instanceof dy)) {
            return null;
        }
        return ((dy) obj).b;
    }

    public TextView c(Object obj) {
        if (obj != null && (obj instanceof dx)) {
            dx dxVar = (dx) obj;
            if (dxVar.f782a != null) {
                return dxVar.f782a.f783a;
            }
        }
        return null;
    }

    public TextView d(Object obj) {
        if (obj == null || !(obj instanceof dy)) {
            return null;
        }
        return ((dy) obj).f783a;
    }

    public String a() {
        return a.a(STCommonInfo.ContentIdType.SPECIAL, String.valueOf(this.f781a.i));
    }
}
