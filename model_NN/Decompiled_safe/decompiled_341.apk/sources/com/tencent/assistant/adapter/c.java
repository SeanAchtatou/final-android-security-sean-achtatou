package com.tencent.assistant.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.AppdetailFlagView;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.cs;
import com.tencent.assistant.utils.df;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class c {
    public static void a(Context context, SimpleAppModel simpleAppModel, TextView textView, boolean z) {
        Drawable drawable;
        Drawable drawable2 = null;
        boolean z2 = true;
        if (simpleAppModel != null && textView != null) {
            try {
                ArrayList arrayList = new ArrayList(3);
                if (!AppdetailFlagView.getFlag(simpleAppModel, 1) || !z) {
                    z2 = false;
                }
                if (z2) {
                    arrayList.add(context.getResources().getDrawable(R.drawable.common_tag_profesional));
                }
                if (simpleAppModel.h() || simpleAppModel.d()) {
                    drawable = context.getResources().getDrawable(R.drawable.common_tag_test);
                } else if (simpleAppModel.c()) {
                    drawable = context.getResources().getDrawable(R.drawable.common_tag_first);
                } else if (simpleAppModel.f()) {
                    drawable = context.getResources().getDrawable(R.drawable.common_tag_gift);
                } else if (simpleAppModel.l()) {
                    drawable = context.getResources().getDrawable(R.drawable.common_tag_gamenodel);
                } else if (simpleAppModel.k()) {
                    drawable = context.getResources().getDrawable(R.drawable.common_tag_newping);
                } else if (simpleAppModel.o()) {
                    drawable = context.getResources().getDrawable(R.drawable.common_redflower);
                } else {
                    drawable = null;
                }
                if (drawable != null) {
                    arrayList.add(drawable);
                }
                if (simpleAppModel.m()) {
                    drawable2 = context.getResources().getDrawable(R.drawable.common_tag_video);
                } else if (simpleAppModel.n()) {
                    drawable2 = context.getResources().getDrawable(R.drawable.common_tag_gamebook);
                }
                if (drawable2 != null) {
                    arrayList.add(drawable2);
                }
                a(arrayList, textView);
            } catch (OutOfMemoryError e) {
                cq.a().b();
            } catch (Throwable th) {
            }
        }
    }

    public static void b(Context context, SimpleAppModel simpleAppModel, TextView textView, boolean z) {
        Drawable drawable;
        Drawable drawable2 = null;
        boolean z2 = true;
        if (simpleAppModel != null && textView != null) {
            try {
                ArrayList arrayList = new ArrayList(3);
                if (!AppdetailFlagView.getFlag(simpleAppModel, 1) || !z) {
                    z2 = false;
                }
                if (z2) {
                    arrayList.add(context.getResources().getDrawable(R.drawable.common_tag_profesional));
                }
                if (simpleAppModel.p()) {
                    drawable = context.getResources().getDrawable(R.drawable.common_tag_promote);
                } else if (simpleAppModel.h() || simpleAppModel.d()) {
                    drawable = context.getResources().getDrawable(R.drawable.common_tag_test);
                } else if (simpleAppModel.c()) {
                    drawable = context.getResources().getDrawable(R.drawable.common_tag_first);
                } else if (simpleAppModel.f()) {
                    drawable = context.getResources().getDrawable(R.drawable.common_tag_gift);
                } else if (simpleAppModel.l()) {
                    drawable = context.getResources().getDrawable(R.drawable.common_tag_gamenodel);
                } else if (simpleAppModel.k()) {
                    drawable = context.getResources().getDrawable(R.drawable.common_tag_newping);
                } else if (simpleAppModel.o()) {
                    drawable = context.getResources().getDrawable(R.drawable.common_redflower);
                } else {
                    drawable = null;
                }
                if (drawable != null) {
                    arrayList.add(drawable);
                }
                if (simpleAppModel.m()) {
                    drawable2 = context.getResources().getDrawable(R.drawable.common_tag_video);
                } else if (simpleAppModel.n()) {
                    drawable2 = context.getResources().getDrawable(R.drawable.common_tag_gamebook);
                }
                if (drawable2 != null) {
                    arrayList.add(drawable2);
                }
                a(arrayList, textView);
            } catch (OutOfMemoryError e) {
                cq.a().b();
            } catch (Throwable th) {
            }
        }
    }

    private static void a(List<Drawable> list, TextView textView) {
        cs csVar;
        if (list == null || list.size() == 0) {
            textView.setCompoundDrawables(null, null, null, null);
            return;
        }
        textView.setCompoundDrawablePadding(df.b(5.0f));
        if (list.size() == 1) {
            Drawable drawable = list.get(0);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            textView.setCompoundDrawables(null, null, drawable, null);
            return;
        }
        try {
            csVar = new cs((Drawable[]) list.toArray(new Drawable[0]), df.b(6.0f));
        } catch (Exception e) {
            e.printStackTrace();
            csVar = null;
        }
        textView.setCompoundDrawables(null, null, csVar, null);
    }
}
