package com.tencent.assistant.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import com.tencent.assistant.adapter.wifitransfer.ImageGridViewAdapter;
import com.tencent.assistant.adapter.wifitransfer.i;
import com.tencent.assistant.localres.model.LocalImage;

/* compiled from: ProGuard */
public class PhotoBackupGridViewAdapter extends ImageGridViewAdapter {
    private SparseBooleanArray e = new SparseBooleanArray();

    public PhotoBackupGridViewAdapter(Context context) {
        super(context);
    }

    public void a(int i) {
        this.e.put(i, true);
        notifyDataSetChanged();
    }

    public void b(int i) {
        this.e.delete(i);
        notifyDataSetChanged();
    }

    public boolean c(int i) {
        return this.e.get(i);
    }

    public int a() {
        return this.e.size();
    }

    public int[] b() {
        int a2 = a();
        int[] iArr = new int[a2];
        for (int i = 0; i < a2; i++) {
            iArr[i] = this.e.keyAt(i);
        }
        return iArr;
    }

    public void a(int[] iArr) {
        if (iArr != null && iArr.length != 0) {
            for (int put : iArr) {
                this.e.put(put, true);
            }
            notifyDataSetChanged();
        }
    }

    public void c() {
        this.e.clear();
        notifyDataSetChanged();
    }

    public void d() {
        if (this.f812a != null) {
            this.f812a.unregisterListener(this);
        }
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = e();
        }
        LocalImage localImage = (LocalImage) this.b.get(i);
        i iVar = (i) view.getTag();
        iVar.c.setVisibility(8);
        iVar.d.setVisibility(0);
        if (iVar != null) {
            iVar.d.setSelected(c(localImage.id));
            if (iVar.d.getCompoundDrawables()[2] != null) {
                iVar.d.getCompoundDrawables()[2].setAlpha(204);
            }
        }
        a(view, localImage, i);
        return view;
    }
}
