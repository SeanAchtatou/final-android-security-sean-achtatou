package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class af extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f692a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ STInfoV2 d;
    final /* synthetic */ ac e;

    af(ac acVar, SimpleAppModel simpleAppModel, int i, int i2, STInfoV2 sTInfoV2) {
        this.e = acVar;
        this.f692a = simpleAppModel;
        this.b = i;
        this.c = i2;
        this.d = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.e.a(this.f692a, this.b, this.c);
    }

    public STInfoV2 getStInfo() {
        this.d.actionId = 200;
        this.d.status = "03";
        return this.d;
    }
}
