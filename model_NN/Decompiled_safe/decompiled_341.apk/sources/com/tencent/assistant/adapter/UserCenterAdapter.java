package com.tencent.assistant.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.usercenter.model.a;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: ProGuard */
public class UserCenterAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private static final String f683a = UserCenterAdapter.class.getSimpleName();
    private Context b = null;
    private LayoutInflater c = null;
    private List<Object> d = new ArrayList();
    private HashMap<String, ArrayList<Object>> e = new HashMap<>();
    private Set<String> f = new HashSet();
    private int g = 0;
    private b h = null;

    public UserCenterAdapter(Context context) {
        this.b = context;
        this.c = LayoutInflater.from(this.b);
    }

    public synchronized List<Object> a(HashMap<String, ArrayList<Object>> hashMap) {
        a aVar;
        if (hashMap != null) {
            if (hashMap.size() > 0) {
                this.d.clear();
                this.f.clear();
                this.e.clear();
                this.e.putAll(hashMap);
                this.g = com.tencent.assistant.usercenter.b.f();
                if (this.e.keySet() != null) {
                    this.f = this.e.keySet();
                }
                for (Map.Entry next : this.e.entrySet()) {
                    if (next != null) {
                        String str = (String) next.getKey();
                        XLog.i(f683a, "strGroupName = " + str);
                        this.d.add(str);
                        ArrayList arrayList = (ArrayList) next.getValue();
                        if (arrayList != null) {
                            int size = arrayList.size();
                            for (int i = 0; i < size; i++) {
                                if ((arrayList.get(i) instanceof a) && (aVar = (a) arrayList.get(i)) != null) {
                                    aVar.g = a(size, aVar.c);
                                    if (1 != aVar.e) {
                                        int d2 = com.tencent.assistant.usercenter.b.d(aVar.e, aVar.c);
                                        if (d2 > 0) {
                                            aVar.f = d2;
                                            aVar.f2611a = false;
                                            aVar.j = Constants.STR_EMPTY;
                                        } else {
                                            aVar.f = 0;
                                            boolean c2 = com.tencent.assistant.usercenter.b.c(aVar.e, aVar.c);
                                            if (this.g <= 0 || !c2) {
                                                aVar.f2611a = false;
                                                aVar.j = Constants.STR_EMPTY;
                                            } else {
                                                aVar.f2611a = true;
                                                this.g--;
                                            }
                                        }
                                        this.d.add(aVar);
                                    } else if (com.tencent.assistant.usercenter.b.c(aVar.e, i + 1)) {
                                        this.d.add(aVar);
                                    } else {
                                        this.d.remove(str);
                                    }
                                }
                            }
                            continue;
                        } else {
                            this.d.remove(str);
                        }
                    }
                }
            }
        }
        return this.d;
    }

    public List<Object> a() {
        return this.d;
    }

    public int getCount() {
        if (this.d == null) {
            return 0;
        }
        return this.d.size();
    }

    public Object getItem(int i) {
        if (this.d == null) {
            return null;
        }
        return this.d.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public boolean isEnabled(int i) {
        if (this.f == null || !this.f.contains(getItem(i))) {
            return super.isEnabled(i);
        }
        return false;
    }

    public int getItemViewType(int i) {
        if (this.f != null && this.f.contains(getItem(i))) {
            return 0;
        }
        Object item = getItem(i);
        if (!(item instanceof a)) {
            return 0;
        }
        if (1 == ((a) item).e) {
            return 1;
        }
        return 2;
    }

    public int getViewTypeCount() {
        return 3;
    }

    public void notifyDataSetChanged() {
        this.g = com.tencent.assistant.usercenter.b.f();
        XLog.i(f683a, "[UserCenterAdapter] ---> notifyDataSetChanged : mMaxRedDotCount = " + this.g);
        super.notifyDataSetChanged();
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        eg egVar;
        ei eiVar;
        if (this.c == null) {
            return null;
        }
        if (this.f == null || !this.f.contains(getItem(i))) {
            Object item = getItem(i);
            if (!(item instanceof a)) {
                return view;
            }
            a aVar = (a) item;
            if (1 == aVar.e) {
                if (view == null) {
                    view = this.c.inflate((int) R.layout.uc_operation_task_view, (ViewGroup) null);
                    ei eiVar2 = new ei(this);
                    eiVar2.f792a = (LinearLayout) view.findViewById(R.id.layout_operation_task);
                    eiVar2.b = (TXImageView) view.findViewById(R.id.tiv_task);
                    eiVar2.c = (TextView) view.findViewById(R.id.tv_task_title);
                    eiVar2.d = (TextView) view.findViewById(R.id.tv_task_content);
                    view.setTag(eiVar2);
                    eiVar = eiVar2;
                } else {
                    eiVar = (ei) view.getTag();
                }
                a(aVar, eiVar);
                return view;
            }
            if (view == null) {
                view = this.c.inflate((int) R.layout.item_user_center, (ViewGroup) null);
                eg egVar2 = new eg(this);
                egVar2.f790a = (RelativeLayout) view.findViewById(R.id.item_layout);
                egVar2.b = (ImageView) view.findViewById(R.id.iv_divider);
                egVar2.c = (TXImageView) view.findViewById(R.id.iv_icon);
                egVar2.d = (ImageView) view.findViewById(R.id.iv_promot);
                egVar2.e = (TextView) view.findViewById(R.id.tv_name);
                egVar2.f = (TextView) view.findViewById(R.id.tv_number);
                egVar2.g = (TextView) view.findViewById(R.id.tv_extend);
                view.setTag(egVar2);
                egVar = egVar2;
            } else {
                egVar = (eg) view.getTag();
            }
            a(aVar, egVar);
            return view;
        } else if (view == null) {
            View inflate = this.c.inflate((int) R.layout.uc_list_group_space, (ViewGroup) null);
            eh ehVar = new eh(this);
            ehVar.f791a = inflate.findViewById(R.id.id_list_group_space);
            inflate.setTag(ehVar);
            return inflate;
        } else {
            eh ehVar2 = (eh) view.getTag();
            return view;
        }
    }

    private void a(a aVar, ei eiVar) {
        if (aVar != null && eiVar != null) {
            eiVar.a(aVar.g);
            eiVar.a(aVar.h, R.drawable.uc_qq_coin, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            eiVar.a(aVar.i);
            eiVar.b(aVar.j);
            b(aVar);
        }
    }

    private void a(a aVar, eg egVar) {
        if (aVar != null && egVar != null) {
            egVar.c(aVar.g);
            egVar.b(aVar.g);
            egVar.a(aVar.h, aVar.d, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            egVar.a(aVar.i);
            egVar.b(aVar.j);
            egVar.a(aVar.f);
            egVar.a(aVar.f2611a);
            b(aVar);
        }
    }

    private int a(int i, int i2) {
        if (1 == i) {
            return 0;
        }
        if (2 == i) {
            if (1 == i2) {
                return 1;
            }
            if (2 == i2) {
                return 3;
            }
        } else if (i >= 3) {
            if (1 == i2) {
                return 1;
            }
            if (i2 == i) {
                return 3;
            }
            return 2;
        }
        return -1;
    }

    private STInfoV2 b(a aVar) {
        if (aVar != null) {
            String a2 = a(aVar);
            if (this.b instanceof BaseActivity) {
                if (this.h == null) {
                    this.h = new b();
                }
                STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b, null, a2, 100, null);
                buildSTInfo.extraData = aVar.i;
                this.h.exposure(buildSTInfo);
                return buildSTInfo;
            }
        }
        return STInfoBuilder.buildSTInfo(this.b, 100);
    }

    public String a(a aVar) {
        if (aVar == null) {
            return STConst.ST_DEFAULT_SLOT;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%02d_", Integer.valueOf(aVar.e + 2)));
        sb.append(String.format("%03d_", Integer.valueOf(aVar.c)));
        if (aVar.f2611a || aVar.f > 0) {
            sb.append("01");
        } else {
            sb.append("02");
        }
        return sb.toString();
    }
}
