package com.tencent.assistant.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class an extends PagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List<View> f700a = new ArrayList();
    private int b = 0;

    public an(List<View> list) {
        if (list != null) {
            this.f700a.addAll(list);
        }
    }

    public void a(List<View> list) {
        this.f700a.clear();
        this.f700a.addAll(list);
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        if (((this.b >> i) & 1) != 1) {
            viewGroup.addView(this.f700a.get(i));
            this.b += 1 << i;
        }
        return this.f700a.get(i);
    }

    public int getCount() {
        return this.f700a.size();
    }

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    public View a(int i) {
        return this.f700a.get(i);
    }
}
