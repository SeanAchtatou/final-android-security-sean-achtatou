package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class y extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f822a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ AppUpdateIgnoreListAdapter c;

    y(AppUpdateIgnoreListAdapter appUpdateIgnoreListAdapter, SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2) {
        this.c = appUpdateIgnoreListAdapter;
        this.f822a = simpleAppModel;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.c.c(this.f822a);
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.status = "01";
        }
        return this.b;
    }
}
