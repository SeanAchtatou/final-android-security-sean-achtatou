package com.tencent.assistant.adapter;

import android.text.TextUtils;
import com.tencent.assistant.component.ApkMetaInfoLoader;

/* compiled from: ProGuard */
class bk implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkMetaInfoLoader.MetaInfo f724a;
    final /* synthetic */ bj b;

    bk(bj bjVar, ApkMetaInfoLoader.MetaInfo metaInfo) {
        this.b = bjVar;
        this.f724a = metaInfo;
    }

    public void run() {
        if (this.b.b.getTag() != null && this.b.b.getTag().equals(this.b.f723a.c) && this.f724a != null) {
            if (this.f724a.icon != null) {
                this.b.b.setImageDrawable(this.f724a.icon);
            }
            if (!TextUtils.isEmpty(this.f724a.name)) {
                this.b.c.setText(this.f724a.name);
            }
        }
    }
}
