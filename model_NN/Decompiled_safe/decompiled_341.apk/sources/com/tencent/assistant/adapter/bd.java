package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.model.h;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class bd extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f717a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ DownloadInfoMultiAdapter c;

    bd(DownloadInfoMultiAdapter downloadInfoMultiAdapter, h hVar, STInfoV2 sTInfoV2) {
        this.c = downloadInfoMultiAdapter;
        this.f717a = hVar;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        if (this.f717a != null) {
            be beVar = new be(this);
            beVar.titleRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm_title);
            beVar.contentRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm);
            beVar.rBtnTxtRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm_btn);
            v.a(beVar);
        }
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.status = Constants.VIA_REPORT_TYPE_MAKE_FRIEND;
        }
        return this.b;
    }
}
