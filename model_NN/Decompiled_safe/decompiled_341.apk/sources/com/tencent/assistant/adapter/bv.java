package com.tencent.assistant.adapter;

import com.tencent.assistant.download.DownloadInfoWrapper;
import java.util.Comparator;

/* compiled from: ProGuard */
public class bv implements Comparator<DownloadInfoWrapper> {
    /* renamed from: a */
    public int compare(DownloadInfoWrapper downloadInfoWrapper, DownloadInfoWrapper downloadInfoWrapper2) {
        boolean c = downloadInfoWrapper.c();
        boolean c2 = downloadInfoWrapper2.c();
        if ((!c && !c2) || c == c2) {
            return (int) (downloadInfoWrapper2.b() - downloadInfoWrapper.b());
        }
        if (c) {
            return -1;
        }
        return 1;
    }
}
