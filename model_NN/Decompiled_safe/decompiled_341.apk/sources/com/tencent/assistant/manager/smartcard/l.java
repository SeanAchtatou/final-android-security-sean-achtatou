package com.tencent.assistant.manager.smartcard;

import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.n;
import com.tencent.assistant.model.a.q;
import com.tencent.assistant.model.a.r;
import com.tencent.assistant.model.a.s;
import java.util.List;

/* compiled from: ProGuard */
public class l extends y {
    public boolean a(i iVar, List<Long> list) {
        if (iVar == null || iVar.i != 17) {
            return false;
        }
        return a((n) iVar, (r) this.f1610a.get(Integer.valueOf(iVar.k())), (s) this.b.get(Integer.valueOf(iVar.k())), list);
    }

    private boolean a(n nVar, r rVar, s sVar, List<Long> list) {
        if (sVar == null) {
            return false;
        }
        nVar.a(list);
        if (nVar.v) {
            return false;
        }
        if (rVar == null) {
            rVar = new r();
            rVar.f = nVar.j;
            rVar.e = nVar.i;
            this.f1610a.put(Integer.valueOf(nVar.k()), rVar);
        }
        if (rVar.b >= sVar.c) {
            a(nVar.s, nVar.j + "||" + nVar.i + "|" + 1, nVar.i);
            return false;
        } else if (rVar.f1651a < sVar.f1652a) {
            return true;
        } else {
            a(nVar.s, nVar.j + "||" + nVar.i + "|" + 2, nVar.i);
            return false;
        }
    }

    public void a(q qVar) {
        r rVar;
        int i;
        if (qVar != null) {
            r rVar2 = (r) this.f1610a.get(Integer.valueOf(qVar.a()));
            if (rVar2 == null) {
                r rVar3 = new r();
                rVar3.e = qVar.f1650a;
                rVar3.f = qVar.b;
                rVar = rVar3;
            } else {
                rVar = rVar2;
            }
            if (qVar.d) {
                rVar.d = true;
            }
            if (qVar.c) {
                if (a(qVar.e * 1000)) {
                    rVar.f1651a++;
                }
                s sVar = (s) this.b.get(Integer.valueOf(qVar.f1650a));
                if (sVar != null) {
                    i = sVar.i;
                } else {
                    i = 7;
                }
                if (a(qVar.e * 1000, (long) i)) {
                    rVar.b++;
                }
                rVar.c++;
            }
            this.f1610a.put(Integer.valueOf(rVar.a()), rVar);
        }
    }

    public void a(s sVar) {
        this.b.put(Integer.valueOf(sVar.a()), sVar);
    }

    public void a(i iVar) {
        if (iVar != null && iVar.i == 17) {
            n nVar = (n) iVar;
            s sVar = (s) this.b.get(Integer.valueOf(nVar.k()));
            if (sVar != null) {
                nVar.p = sVar.d;
                nVar.h = sVar.b;
                nVar.g = sVar.f1652a;
            }
        }
    }
}
