package com.tencent.assistant.manager;

import com.tencent.assistant.download.DownloadInfo;
import java.io.File;
import java.io.FilenameFilter;

/* compiled from: ProGuard */
class bl implements FilenameFilter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f1506a;
    final /* synthetic */ be b;

    bl(be beVar, DownloadInfo downloadInfo) {
        this.b = beVar;
        this.f1506a = downloadInfo;
    }

    public boolean accept(File file, String str) {
        if (!str.startsWith(this.f1506a.packageName) || !str.endsWith(".apk")) {
            return false;
        }
        return true;
    }
}
