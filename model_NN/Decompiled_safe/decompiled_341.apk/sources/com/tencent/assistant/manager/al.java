package com.tencent.assistant.manager;

import com.qq.AppService.AstApp;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.ao;
import com.tencent.assistant.utils.e;

/* compiled from: ProGuard */
class al implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LocalApkInfo f1483a;
    final /* synthetic */ ao b;
    final /* synthetic */ ak c;

    al(ak akVar, LocalApkInfo localApkInfo, ao aoVar) {
        this.c = akVar;
        this.f1483a = localApkInfo;
        this.b = aoVar;
    }

    public void run() {
        String str = this.f1483a.mPackageName;
        if (!e.b(AstApp.i(), str)) {
            e.a(AstApp.i(), str);
        }
        this.b.b(str);
    }
}
