package com.tencent.assistant.manager.smartcard;

import com.tencent.assistant.model.a.b;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.p;
import com.tencent.assistant.model.a.r;
import com.tencent.assistant.model.a.s;
import java.util.List;

/* compiled from: ProGuard */
public class aa extends y {
    public boolean a(i iVar, List<Long> list) {
        if (iVar == null || iVar.i != 3) {
            return false;
        }
        return a((b) iVar, (r) this.f1610a.get(Integer.valueOf(iVar.i)), (s) this.b.get(Integer.valueOf(iVar.i)), (List) this.c.get(Integer.valueOf(iVar.i)), list);
    }

    private boolean a(b bVar, r rVar, s sVar, List<p> list, List<Long> list2) {
        if (sVar == null) {
            return false;
        }
        if (rVar == null) {
            rVar = new r();
            rVar.f = bVar.j;
            rVar.e = bVar.i;
            this.f1610a.put(Integer.valueOf(rVar.e), rVar);
        }
        bVar.a(bVar.i, list);
        if (bVar.c == null || bVar.c.size() < sVar.g) {
            a(bVar.s, bVar.j + "||" + bVar.i + "|" + 3, bVar.i);
            return false;
        } else if (rVar.b >= sVar.b) {
            a(bVar.s, bVar.j + "||" + bVar.i + "|" + 1, bVar.i);
            return false;
        } else if (rVar.f1651a < sVar.f1652a) {
            return true;
        } else {
            a(bVar.s, bVar.j + "||" + bVar.i + "|" + 2, bVar.i);
            return false;
        }
    }

    public void a(i iVar) {
        if (iVar != null && iVar.i == 3) {
            b bVar = (b) iVar;
            s sVar = (s) this.b.get(Integer.valueOf(bVar.i));
            if (sVar != null) {
                bVar.p = sVar.d;
                bVar.f1636a = a(bVar, sVar.g, sVar.j);
            }
        }
    }

    private int a(b bVar, int i, int i2) {
        if (!(bVar == null || bVar.c == null)) {
            int size = bVar.c.size();
            if (size >= i && size < i2) {
                return i;
            }
            if (size >= i2) {
                return i2;
            }
        }
        return 0;
    }
}
