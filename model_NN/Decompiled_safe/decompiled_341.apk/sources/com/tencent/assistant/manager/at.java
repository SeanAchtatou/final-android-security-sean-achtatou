package com.tencent.assistant.manager;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class at {

    /* renamed from: a  reason: collision with root package name */
    protected ReferenceQueue<au> f1490a = new ReferenceQueue<>();
    protected ConcurrentLinkedQueue<WeakReference<au>> b = new ConcurrentLinkedQueue<>();

    protected at() {
    }

    /* access modifiers changed from: protected */
    public void a(au auVar) {
        if (auVar != null) {
            while (true) {
                Reference<? extends au> poll = this.f1490a.poll();
                if (poll == null) {
                    break;
                }
                this.b.remove(poll);
            }
            Iterator<WeakReference<au>> it = this.b.iterator();
            while (it.hasNext()) {
                if (((au) it.next().get()) == auVar) {
                    return;
                }
            }
            this.b.add(new WeakReference(auVar, this.f1490a));
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        Iterator<WeakReference<au>> it = this.b.iterator();
        while (it.hasNext()) {
            au auVar = (au) it.next().get();
            if (auVar != null) {
                try {
                    auVar.a();
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e2) {
                    e2.printStackTrace();
                }
            }
        }
    }
}
