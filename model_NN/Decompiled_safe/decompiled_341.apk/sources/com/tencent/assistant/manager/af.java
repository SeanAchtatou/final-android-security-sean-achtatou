package com.tencent.assistant.manager;

import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager;
import com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean;

/* compiled from: ProGuard */
class af implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f1477a;
    final /* synthetic */ ae b;

    af(ae aeVar, DownloadInfo downloadInfo) {
        this.b = aeVar;
        this.f1477a = downloadInfo;
    }

    public void run() {
        InstallUninstallTaskBean installUninstallTaskBean = new InstallUninstallTaskBean();
        if (this.f1477a != null) {
            installUninstallTaskBean.fileSize = this.f1477a.fileSize;
        }
        this.b.f1476a.k.a(InstallUninstallDialogManager.DIALOG_DEALWITH_TYPE.DOWNLOAD_SPACE_NOT_ENOUGH, installUninstallTaskBean);
    }
}
