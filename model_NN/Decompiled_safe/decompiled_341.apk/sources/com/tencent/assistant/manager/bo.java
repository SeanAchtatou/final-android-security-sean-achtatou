package com.tencent.assistant.manager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.activity.InstalledAppManagerActivity;
import com.tencent.assistant.activity.StartPopWindowActivity;
import com.tencent.assistant.b.a;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.s;
import com.tencent.assistant.module.ds;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.AdviceApp;
import com.tencent.assistant.protocol.jce.AdviceAppCollection;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.protocol.jce.CallSteWardCfg;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.bh;
import com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean;
import com.tencent.assistant.utils.installuninstall.ac;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class bo implements UIEventListener, v, s {
    public static boolean i = true;
    private static bo o;
    private Object A = new Object();
    /* access modifiers changed from: private */
    public TXImageView B;
    private TextView C;
    /* access modifiers changed from: private */
    public TextView D;
    private ImageView E;
    private int F = STConst.ST_PAGE_DOWNLOAD;

    /* renamed from: a  reason: collision with root package name */
    WindowManager f1509a;
    ViewGroup b;
    LayoutInflater c;
    PackageManager d;
    WindowManager.LayoutParams e;
    ArrayList<InstallUninstallTaskBean> f;
    boolean g = false;
    boolean h = false;
    private ds j = new ds();
    private ds k = new ds();
    private cc l = new cc(this);
    private bz m = new bz(this);
    private ds n = new ds();
    /* access modifiers changed from: private */
    public cd p;
    private cd q;
    private cd r;
    private String s;
    private String t;
    private Map<String, by> u = new HashMap();
    private List<String> v = new ArrayList();
    private List<String> w = new ArrayList();
    private by x = null;
    private ce y = null;
    private int z = 2000;

    private bo() {
        this.j.register(this);
        this.k.register(this.l);
        this.n.register(this.m);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
        AstApp.i().k().addUIEventListener(1002, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        u.a().a(this);
        d();
        this.f1509a = (WindowManager) AstApp.i().getSystemService("window");
        this.c = (LayoutInflater) AstApp.i().getSystemService("layout_inflater");
        this.d = AstApp.i().getPackageManager();
    }

    private void d() {
        AdviceAppCollection adviceAppCollection;
        byte[] G = m.a().G();
        if (!(G == null || (adviceAppCollection = (AdviceAppCollection) bh.b(G, AdviceAppCollection.class)) == null)) {
            this.p = new cd(adviceAppCollection);
            this.q = new cf(adviceAppCollection);
            this.r = new cd(adviceAppCollection);
            b(adviceAppCollection.e);
            XLog.d("zxh", "installedNotTips = " + adviceAppCollection.e);
        }
        byte[] H = m.a().H();
        this.v.clear();
        this.w.clear();
        if (H != null) {
            CallSteWardCfg callSteWardCfg = (CallSteWardCfg) bh.b(H, CallSteWardCfg.class);
            if (callSteWardCfg != null) {
                if (callSteWardCfg.b != null) {
                    this.v.addAll(callSteWardCfg.b);
                    for (String str : this.v) {
                        XLog.d("jimluo", "outer config: " + str);
                    }
                }
                if (callSteWardCfg.f2026a != null) {
                    this.w.addAll(callSteWardCfg.f2026a);
                    for (String str2 : this.w) {
                        XLog.d("jimluo", "normal config: " + str2);
                    }
                }
            }
        } else {
            this.v.add(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        }
        for (String str3 : this.v) {
            XLog.d("jimluo", "call outer config: " + str3);
        }
        for (String str4 : this.w) {
            XLog.d("jimluo", "call normal config: " + str4);
        }
    }

    public static synchronized bo a() {
        bo boVar;
        synchronized (bo.class) {
            if (o == null) {
                o = new bo();
            }
            boVar = o;
        }
        return boVar;
    }

    public void a(DownloadInfo downloadInfo) {
        if (this.p != null && this.p.a(downloadInfo.appId, downloadInfo.packageName, downloadInfo.categoryId)) {
            AppConst.TwoBtnDialogInfo a2 = a().a(0, downloadInfo.name, this.p);
            a2.pageId = STConst.ST_PAGE_DOWN_RECOMMEND;
            this.z = STConst.ST_PAGE_DOWN_RECOMMEND;
            a(a2, this.p);
        }
    }

    public void a(String str) {
        XLog.d("RecommendDownloadManager", "<install> 触发安装推荐");
        e();
        this.l.a(str);
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        simpleAppModel.c = str;
        this.k.a(simpleAppModel);
        XLog.d("RecommendDownloadManager", "<install> 服务器拉取安装包 ,packageName = " + str);
    }

    private void e() {
        m.a().b("recommend_pkg_install_begin_time", Long.valueOf(System.currentTimeMillis()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    /* access modifiers changed from: private */
    public boolean f() {
        boolean z2;
        boolean z3;
        long currentTimeMillis = System.currentTimeMillis();
        long a2 = m.a().a("recommend_pkg_install_begin_time", 0L);
        if (a2 != 0 && Math.abs(currentTimeMillis - a2) > 10000) {
            XLog.e("RecommendDownloadManager", "<install> <不会>弹出小黄条，原因：从接收到安装时间到服务器返回超过10秒");
            return false;
        }
        if (this.x == null || Math.abs(currentTimeMillis - this.x.e) > 10000) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (z2) {
            XLog.e("RecommendDownloadManager", "<install> <不会>弹出小黄条，原因：已命中下载完打开逻辑");
            return false;
        }
        if (this.y == null || Math.abs(currentTimeMillis - this.y.f1526a) > 10000) {
            z3 = false;
        } else {
            z3 = true;
        }
        if (z3) {
            XLog.e("RecommendDownloadManager", "<install> <不会>弹出小黄条，原因：已命中下载完《打开》的小黄条");
            return false;
        }
        XLog.d("RecommendDownloadManager", "canShowRecommendTips = true");
        return true;
    }

    private void b(int i2) {
        m.a().b("recommend_pkg_install_tips_switch", Integer.valueOf(i2));
    }

    public boolean b() {
        return m.a().a("recommend_pkg_install_tips_switch", 1) == 0;
    }

    public void b(String str) {
        AstApp.i();
        if ((AstApp.m() instanceof InstalledAppManagerActivity) && this.q != null && this.q.a(0, str, -1)) {
            AppConst.TwoBtnDialogInfo a2 = a().a(1, this.s, this.q);
            a2.pageId = STConst.ST_PAGE_UNINSTALL_RECOMMEND;
            this.z = STConst.ST_PAGE_UNINSTALL_RECOMMEND;
            a(a2, this.q);
        }
    }

    public cd c() {
        return this.r;
    }

    /* access modifiers changed from: private */
    @SuppressLint({"RtlHardcoded"})
    public WindowManager.LayoutParams g() {
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.height = -2;
        layoutParams.width = -1;
        layoutParams.gravity = 51;
        layoutParams.flags = 40;
        layoutParams.type = STConst.ST_PAGE_NECESSARY;
        if (AstApp.i().l()) {
            layoutParams.y = AstApp.i().getResources().getDimensionPixelSize(R.dimen.app_detail_float_bar_content_height);
        } else {
            layoutParams.y = 0;
        }
        return layoutParams;
    }

    /* access modifiers changed from: private */
    public void h() {
        synchronized (this.A) {
            if (this.b != null) {
                this.b.setVisibility(8);
                this.b = null;
            }
        }
    }

    @SuppressLint({"InflateParams"})
    public void a(String str, cd cdVar) {
        int F2 = m.a().F();
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        if (F2 <= 0 || currentTimeMillis - ((long) F2) >= 86400) {
            synchronized (this.A) {
                if (this.b == null) {
                    this.b = (ViewGroup) this.c.inflate((int) R.layout.float_silent_install_succ, (ViewGroup) null);
                }
            }
            this.B = (TXImageView) this.b.findViewById(R.id.image);
            this.C = (TextView) this.b.findViewById(R.id.txt);
            this.D = (TextView) this.b.findViewById(R.id.btn_open);
            this.D.setText((int) R.string.download);
            this.E = (ImageView) this.b.findViewById(R.id.btn_close);
            String str2 = cdVar.h;
            if (str2 != null) {
                if (!TextUtils.isEmpty(str)) {
                    str2 = str2.replace("${name}", str);
                } else {
                    str2 = str2.replace("${name}", Constants.STR_EMPTY);
                }
            }
            this.C.setSingleLine(false);
            this.C.setText(str2);
            this.E.setOnClickListener(new bp(this));
            this.m.a(cdVar);
            SimpleAppModel simpleAppModel = new SimpleAppModel();
            simpleAppModel.c = cdVar.g;
            simpleAppModel.f1634a = cdVar.e;
            simpleAppModel.ac = cdVar.f;
            this.n.a(simpleAppModel);
            XLog.d("RecommendDownloadManager", "<install> 服务器拉取推荐下载的应用信息。。。");
            return;
        }
        XLog.e("RecommendDownloadManager", "<install> 判断<不会>弹小黄条,所有类型弹框间隔必须间隔24小时");
    }

    public void a(AppConst.DialogInfo dialogInfo, cd cdVar) {
        Intent intent;
        Bundle extras;
        AstApp.i();
        BaseActivity m2 = AstApp.m();
        if (!(m2 == null || (intent = m2.getIntent()) == null || (extras = intent.getExtras()) == null)) {
            String string = extras.getString(a.D);
            if (extras.getBoolean(a.t) && TextUtils.isEmpty(string)) {
                return;
            }
        }
        int F2 = m.a().F();
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        if (F2 <= 0 || currentTimeMillis - ((long) F2) >= 86400) {
            long j2 = 40;
            if (AstApp.m() instanceof StartPopWindowActivity) {
                j2 = 900;
            }
            ba.a().postDelayed(new bq(this, dialogInfo, cdVar), j2);
            cdVar.c();
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3, String str, int i4, byte[] bArr) {
        STInfoV2 sTInfoV2 = new STInfoV2(i2, str, i2, STConst.ST_DEFAULT_SLOT, i4);
        sTInfoV2.appId = this.p.e;
        sTInfoV2.recommendId = bArr;
        k.a(sTInfoV2);
    }

    public void a(long j2, String str) {
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        simpleAppModel.f1634a = j2;
        simpleAppModel.ac = str;
        this.j.a(simpleAppModel);
    }

    public void onGetAppInfoSuccess(int i2, int i3, AppSimpleDetail appSimpleDetail) {
        AdviceApp b2;
        if (appSimpleDetail != null) {
            SimpleAppModel a2 = u.a(appSimpleDetail);
            DownloadInfo a3 = DownloadProxy.a().a(a2);
            if (a3 != null && a3.needReCreateInfo(a2)) {
                DownloadProxy.a().b(a3.downloadTicket);
                a3 = null;
            }
            if (a3 == null) {
                StatInfo statInfo = new StatInfo(a2.b, this.z, 0, null, 0);
                statInfo.scene = this.z;
                a3 = DownloadInfo.createDownloadInfo(a2, statInfo);
                a3.downloadState = SimpleDownloadInfo.DownloadState.PAUSED;
                a3.autoInstall = false;
                DownloadProxy.a().d(a3);
                AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, a3));
            } else {
                a3.autoInstall = false;
            }
            if (this.p.e == appSimpleDetail.f2003a && (b2 = this.p.b()) != null) {
                if (a3.statInfo == null) {
                    a3.statInfo = new StatInfo();
                }
                a3.statInfo.recommendId = b2.i;
            }
            TemporaryThreadManager.get().start(new br(this, a3));
            return;
        }
        Toast.makeText(AstApp.i(), c((int) R.string.rec_create_fail), 0).show();
    }

    public void onGetAppInfoFail(int i2, int i3) {
        Toast.makeText(AstApp.i(), c((int) R.string.rec_create_fail), 0).show();
        XLog.e("RecommendDownloadManager", "get appinfo fail." + i2 + ",errorCode:" + i3);
    }

    public void a(int i2) {
        this.F = i2;
    }

    private AppConst.TwoBtnDialogInfo a(int i2, String str, cd cdVar) {
        bs bsVar = new bs(this, cdVar);
        bsVar.titleRes = i2 == 0 ? c((int) R.string.rec_install_title) : c((int) R.string.rec_uninstall_title);
        String str2 = cdVar.h;
        if (str2 != null) {
            if (str != null) {
                str2 = str2.replace("${name}", str);
            } else {
                str2 = str2.replace("${name}", Constants.STR_EMPTY);
            }
        }
        bsVar.contentRes = str2;
        bsVar.lBtnTxtRes = c((int) R.string.rec_select_cancel);
        bsVar.rBtnTxtRes = cdVar.i;
        bsVar.blockCaller = true;
        return bsVar;
    }

    private String c(int i2) {
        return AstApp.i().getBaseContext().getString(i2);
    }

    public void handleUIEvent(Message message) {
        DownloadInfo d2;
        XLog.d("jimluo", "Recommend.event:" + message.what + ",obj:" + message.obj);
        switch (message.what) {
            case 1002:
                if ((message.obj instanceof String) && (d2 = DownloadProxy.a().d((String) message.obj)) != null) {
                    b(d2);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_UNINSTALL:
                if (message.obj instanceof String) {
                    String str = (String) message.obj;
                    if (str.equals(this.t)) {
                        b(str);
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD:
                if (message.obj instanceof DownloadInfo) {
                    DownloadInfo downloadInfo = (DownloadInfo) message.obj;
                    LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(downloadInfo.packageName, downloadInfo.versionCode, downloadInfo.grayVersionCode);
                    if (downloadInfo.uiType == SimpleDownloadInfo.UIType.NORMAL && localApkInfo == null) {
                        a(downloadInfo);
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC:
                if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean)) {
                    InstallUninstallTaskBean installUninstallTaskBean = (InstallUninstallTaskBean) message.obj;
                    if (!b(installUninstallTaskBean) || !d(installUninstallTaskBean)) {
                        if (!c(installUninstallTaskBean) || !e(installUninstallTaskBean)) {
                            a(installUninstallTaskBean);
                            return;
                        } else if (DownloadProxy.a().g() > 0 || !ac.a().c()) {
                            XLog.d("jimluo", "downloading size > 0 || installing task size > 0");
                            return;
                        } else {
                            f(installUninstallTaskBean);
                            XLog.d("jimluo", "match normal call");
                            return;
                        }
                    } else if (DownloadProxy.a().g() > 0 || !ac.a().c()) {
                        XLog.d("jimluo", "downloading size > 0 || installing task size > 0");
                        return;
                    } else {
                        f(installUninstallTaskBean);
                        XLog.d("jimluo", "match outer call");
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void a(HashMap<String, Object> hashMap) {
        d();
    }

    public void c(String str) {
        this.s = str;
    }

    public void d(String str) {
        this.t = str;
    }

    private void b(DownloadInfo downloadInfo) {
        if (downloadInfo != null && !TextUtils.isEmpty(downloadInfo.packageName)) {
            by byVar = this.u.get(downloadInfo.packageName);
            if (byVar == null) {
                byVar = new by(null);
                byVar.d = System.currentTimeMillis();
            }
            byVar.f1519a = downloadInfo.packageName;
            byVar.b = downloadInfo.versionCode;
            byVar.c = downloadInfo.grayVersionCode;
            byVar.f = downloadInfo.fromOutCall;
            byVar.g = downloadInfo.uiType;
            this.u.put(downloadInfo.packageName, byVar);
        }
    }

    private boolean b(InstallUninstallTaskBean installUninstallTaskBean) {
        DownloadInfo c2;
        if (installUninstallTaskBean == null || (c2 = DownloadProxy.a().c(installUninstallTaskBean.downloadTicket)) == null || !c2.fromOutCall) {
            return false;
        }
        return true;
    }

    private boolean c(InstallUninstallTaskBean installUninstallTaskBean) {
        DownloadInfo c2;
        if (installUninstallTaskBean == null || (c2 = DownloadProxy.a().c(installUninstallTaskBean.downloadTicket)) == null || c2.uiType != SimpleDownloadInfo.UIType.NORMAL || c2.fromOutCall) {
            return false;
        }
        return true;
    }

    private boolean d(InstallUninstallTaskBean installUninstallTaskBean) {
        if (installUninstallTaskBean != null) {
            return this.v.contains(installUninstallTaskBean.packageName);
        }
        return false;
    }

    private boolean e(InstallUninstallTaskBean installUninstallTaskBean) {
        if (installUninstallTaskBean != null) {
            return this.w.contains(installUninstallTaskBean.packageName);
        }
        return false;
    }

    private void f(InstallUninstallTaskBean installUninstallTaskBean) {
        if (installUninstallTaskBean != null) {
            com.tencent.assistant.download.a.a().a(installUninstallTaskBean.packageName, installUninstallTaskBean.applinkActionUrl);
            HashMap hashMap = new HashMap();
            hashMap.put("B1", Global.getQUA());
            hashMap.put("B2", Global.getPhoneGuidAndGen());
            hashMap.put("B3", installUninstallTaskBean.packageName);
            com.tencent.beacon.event.a.a("callActiveAfterInstall", true, -1, -1, hashMap, true);
            this.x = this.u.remove(installUninstallTaskBean.packageName);
            if (this.x == null) {
                this.x = new by(null);
                this.x.f1519a = installUninstallTaskBean.packageName;
                this.x.b = installUninstallTaskBean.versionCode;
            }
            this.x.e = System.currentTimeMillis();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    /* access modifiers changed from: package-private */
    public void a(InstallUninstallTaskBean installUninstallTaskBean) {
        boolean z2;
        boolean z3;
        if (System.currentTimeMillis() - m.a().a("succ_float_cancel", 0L) >= 180000) {
            DownloadInfo d2 = DownloadProxy.a().d(installUninstallTaskBean.downloadTicket);
            if (d2 == null || d2.uiType == SimpleDownloadInfo.UIType.NORMAL) {
                if (this.b == null) {
                    try {
                        this.b = (ViewGroup) this.c.inflate((int) R.layout.float_silent_install_succ, (ViewGroup) null);
                        z2 = true;
                    } catch (Exception e2) {
                        return;
                    }
                } else {
                    z2 = false;
                }
                if (this.f == null) {
                    this.f = new ArrayList<>();
                    if (this.b == null) {
                        this.b = (ViewGroup) this.c.inflate((int) R.layout.float_silent_install_succ, (ViewGroup) null);
                    }
                    this.e = new WindowManager.LayoutParams();
                    this.e.height = AstApp.i().getResources().getDimensionPixelSize(R.dimen.app_detail_cfg_icon_size);
                    this.e.width = -1;
                    this.e.gravity = 51;
                    z3 = true;
                } else {
                    z3 = z2;
                }
                if (!this.f.contains(installUninstallTaskBean)) {
                    this.f.add(installUninstallTaskBean);
                }
                if (!this.h) {
                    ImageView imageView = (ImageView) this.b.findViewById(R.id.image);
                    TextView textView = (TextView) this.b.findViewById(R.id.txt);
                    TextView textView2 = (TextView) this.b.findViewById(R.id.btn_open);
                    ImageView imageView2 = (ImageView) this.b.findViewById(R.id.btn_close);
                    if (installUninstallTaskBean.appName.length() < 6) {
                        textView.setText(installUninstallTaskBean.appName + "已安装完成");
                    } else {
                        textView.setText(installUninstallTaskBean.appName.substring(0, 5) + "...已安装完成");
                    }
                    try {
                        imageView.setImageDrawable(this.d.getApplicationInfo(installUninstallTaskBean.packageName, 0).loadIcon(this.d));
                    } catch (Throwable th) {
                        cq.a().b();
                    }
                    imageView2.setOnClickListener(new bt(this));
                    textView2.setOnClickListener(new bu(this, installUninstallTaskBean));
                    if (AstApp.i().l()) {
                        this.e.y = AstApp.i().getResources().getDimensionPixelSize(R.dimen.app_detail_float_bar_content_height);
                    } else {
                        this.e.y = 0;
                    }
                    this.e.flags = 40;
                    this.e.type = STConst.ST_PAGE_NECESSARY;
                    ce ceVar = new ce(null);
                    ceVar.b = installUninstallTaskBean.packageName;
                    ceVar.f1526a = System.currentTimeMillis();
                    this.y = ceVar;
                    if (z3) {
                        try {
                            this.f1509a.addView(this.b, this.e);
                            this.b.requestLayout();
                            this.f1509a.updateViewLayout(this.b, this.e);
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                    } else {
                        this.b.setVisibility(0);
                        try {
                            this.f1509a.updateViewLayout(this.b, this.e);
                        } catch (Exception e4) {
                            e4.printStackTrace();
                        }
                    }
                    this.h = true;
                    this.g = false;
                    if (this.b != null) {
                        this.b.postDelayed(new bw(this, installUninstallTaskBean), 3000);
                    }
                }
            }
        } else if (this.f != null) {
            this.f.clear();
            if (this.b != null) {
                this.b.setVisibility(8);
            }
        }
    }

    public static final void a(String str, long j2) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getQUA());
        hashMap.put("B2", Global.getPhoneGuidAndGen());
        hashMap.put("B3", Constants.STR_EMPTY + j2);
        com.tencent.beacon.event.a.a(str, true, -1, -1, hashMap, true);
    }
}
