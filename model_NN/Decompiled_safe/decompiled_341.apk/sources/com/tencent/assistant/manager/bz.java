package com.tencent.assistant.manager;

import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.s;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.model.StatInfo;

/* compiled from: ProGuard */
class bz implements s {

    /* renamed from: a  reason: collision with root package name */
    cd f1520a;
    final /* synthetic */ bo b;

    bz(bo boVar) {
        this.b = boVar;
    }

    public void a(cd cdVar) {
        this.f1520a = cdVar;
    }

    public void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail) {
        if (appSimpleDetail == null || this.f1520a == null || this.b.D == null || this.b.B == null || this.b.b == null) {
            XLog.e("RecommendDownloadManager", "<install> simpleDetail = " + appSimpleDetail + ", matcher = " + this.f1520a);
            return;
        }
        SimpleAppModel a2 = u.a(appSimpleDetail);
        DownloadInfo a3 = DownloadProxy.a().a(a2);
        if (a3 != null && a3.needReCreateInfo(a2)) {
            DownloadProxy.a().b(a3.downloadTicket);
            a3 = null;
        }
        if (a3 == null) {
            StatInfo statInfo = new StatInfo(a2.b, STConst.ST_PAGE_INSTALL_RECOMMEND, 0, null, 0);
            statInfo.scene = STConst.ST_PAGE_INSTALL_RECOMMEND;
            a3 = DownloadInfo.createDownloadInfo(a2, statInfo);
            a3.downloadState = SimpleDownloadInfo.DownloadState.PAUSED;
        }
        DownloadInfo downloadInfo = a3;
        downloadInfo.autoInstall = true;
        this.b.B.updateImageView(downloadInfo.iconUrl, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        if (!this.b.f()) {
            XLog.e("RecommendDownloadManager", "<install> 判断<不会>弹小黄条");
            return;
        }
        try {
            if (this.b.b.isShown()) {
                XLog.d("RecommendDownloadManager", "<install> 已经有小黄条正在显示  !");
                return;
            }
            XLog.d("RecommendDownloadManager", "<install> 弹出小黄条");
            this.b.b.setVisibility(0);
            this.b.f1509a.addView(this.b.b, this.b.g());
            this.f1520a.c();
            this.b.a(STConst.ST_PAGE_INSTALL_RECOMMEND, AstApp.m().f(), STConst.ST_DEFAULT_SLOT, 100, null);
            bo.a("rec_pop_tips_exp", this.f1520a.e);
            this.b.b.postDelayed(new ca(this), 5000);
            this.b.D.setOnClickListener(new cb(this, downloadInfo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onGetAppInfoFail(int i, int i2) {
        XLog.e("RecommendDownloadManager", "<install> 服务器拉取推荐下载的应用信息<失败>");
    }
}
