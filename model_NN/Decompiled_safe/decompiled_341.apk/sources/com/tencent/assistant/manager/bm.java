package com.tencent.assistant.manager;

import com.tencent.assistant.download.SimpleDownloadInfo;

/* compiled from: ProGuard */
/* synthetic */ class bm {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f1507a = new int[SimpleDownloadInfo.DownloadState.values().length];

    static {
        try {
            f1507a[SimpleDownloadInfo.DownloadState.INIT.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f1507a[SimpleDownloadInfo.DownloadState.ILLEGAL.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f1507a[SimpleDownloadInfo.DownloadState.DOWNLOADING.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f1507a[SimpleDownloadInfo.DownloadState.PAUSED.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f1507a[SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f1507a[SimpleDownloadInfo.DownloadState.FAIL.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f1507a[SimpleDownloadInfo.DownloadState.QUEUING.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f1507a[SimpleDownloadInfo.DownloadState.DELETED.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f1507a[SimpleDownloadInfo.DownloadState.SUCC.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f1507a[SimpleDownloadInfo.DownloadState.COMPLETE.ordinal()] = 10;
        } catch (NoSuchFieldError e10) {
        }
        try {
            f1507a[SimpleDownloadInfo.DownloadState.INSTALLED.ordinal()] = 11;
        } catch (NoSuchFieldError e11) {
        }
    }
}
