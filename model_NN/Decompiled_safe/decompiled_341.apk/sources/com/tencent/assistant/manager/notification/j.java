package com.tencent.assistant.manager.notification;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.widget.RemoteViews;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.notification.a.a.c;
import com.tencent.assistant.utils.r;

/* compiled from: ProGuard */
public class j {

    /* renamed from: a  reason: collision with root package name */
    private static j f1569a = null;
    private int b = 3;
    private c c;

    public static synchronized j a() {
        j jVar;
        synchronized (j.class) {
            if (f1569a == null) {
                f1569a = new j();
            }
            jVar = f1569a;
        }
        return jVar;
    }

    private j() {
    }

    /* access modifiers changed from: private */
    public void a(m mVar) {
        n a2;
        if (mVar.a() > 0) {
            if (mVar.a() > this.b && (a2 = mVar.a(0)) != null) {
                v.a().a(a2.a());
            }
            n b2 = mVar.b(mVar.a() - 1);
            if (b2 != null) {
                a(b2);
            }
        }
    }

    private void a(n nVar) {
        v.a().a(112);
        String b2 = nVar.b();
        String string = AstApp.i().getResources().getString(R.string.install_suc_notification_triker_title, b2);
        Intent intent = new Intent(AstApp.i(), NotificationService.class);
        intent.putExtra("notification_id", 124);
        intent.putExtra("notification_data", nVar.c());
        PendingIntent service = PendingIntent.getService(AstApp.i(), nVar.a(), intent, 268435456);
        Intent intent2 = new Intent(AstApp.i(), NotificationService.class);
        intent2.setAction("android.intent.action.DELETE");
        intent2.putExtra("notification_id", 124);
        intent2.putExtra("notification_data", nVar.c());
        PendingIntent service2 = PendingIntent.getService(AstApp.i(), nVar.a(), intent2, 268435456);
        this.c = new c(nVar.d(), 1);
        this.c.a(new k(this, nVar, string, service, service2));
        this.c.b();
    }

    /* access modifiers changed from: private */
    public RemoteViews a(n nVar, Bitmap bitmap) {
        if (nVar == null) {
            return null;
        }
        RemoteViews remoteViews = new RemoteViews(AstApp.i().getPackageName(), (int) R.layout.notification_card_installapp);
        r rVar = new r(AstApp.i(), true);
        Integer c2 = rVar.c();
        if (c2 != null) {
            remoteViews.setTextColor(R.id.title, c2.intValue());
        }
        Integer a2 = rVar.a();
        if (a2 != null) {
            remoteViews.setTextColor(R.id.content, a2.intValue());
        }
        if (bitmap == null || bitmap.isRecycled()) {
            remoteViews.setImageViewResource(R.id.big_icon, R.drawable.logo72);
        } else {
            remoteViews.setImageViewBitmap(R.id.big_icon, bitmap);
        }
        remoteViews.setFloat(R.id.title, "setTextSize", rVar.d());
        remoteViews.setFloat(R.id.content, "setTextSize", rVar.b());
        remoteViews.setTextViewText(R.id.title, nVar.b());
        remoteViews.setTextViewText(R.id.content, AstApp.i().getResources().getString(R.string.install_suc_notification_content));
        remoteViews.setTextViewText(R.id.rightBtn, AstApp.i().getResources().getString(R.string.install_suc_notification_btn_installed));
        if (r.d() < 20) {
            return remoteViews;
        }
        remoteViews.setInt(R.id.root, "setBackgroundResource", R.color.notification_bg_50);
        return remoteViews;
    }
}
