package com.tencent.assistant.manager;

import android.graphics.Bitmap;
import com.tencent.assistant.model.q;
import com.tencent.assistant.thumbnailCache.o;

/* compiled from: ProGuard */
class cp implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f1535a;
    final /* synthetic */ cm b;

    cp(cm cmVar, o oVar) {
        this.b = cmVar;
        this.f1535a = oVar;
    }

    public void run() {
        q a2 = this.b.b.a(this.f1535a.c());
        if (a2 != null) {
            try {
                Bitmap bitmap = this.f1535a.f;
                if (bitmap != null && !bitmap.isRecycled()) {
                    this.b.a(bitmap, a2);
                    this.b.b.a(a2);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (this.b.f1532a.contains(this.f1535a.c())) {
                this.b.f1532a.remove(this.f1535a.c());
            }
        }
    }
}
