package com.tencent.assistant.manager.notification.a;

import android.text.TextUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.PushInfo;
import com.tencent.assistant.utils.cv;

/* compiled from: ProGuard */
public class n extends d {
    public n(int i, PushInfo pushInfo, byte[] bArr) {
        super(i, pushInfo, bArr);
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        if (super.c() && !TextUtils.isEmpty(this.c.c)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        a((int) R.layout.notification_card_1);
        if (this.i == null) {
            return false;
        }
        i();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        this.j = b(R.layout.notification_card1_right6);
        if (this.m != null) {
            this.j.setTextColor(R.id.timeText, this.m.intValue());
        }
        this.j.setFloat(R.id.timeText, "setTextSize", this.o);
        this.j.setTextViewText(R.id.timeText, cv.d(Long.valueOf(System.currentTimeMillis())));
        this.i.removeAllViews(R.id.rightContainer);
        this.i.addView(R.id.rightContainer, this.j);
        this.i.setViewVisibility(R.id.rightContainer, 0);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        return true;
    }
}
