package com.tencent.assistant.manager.spaceclean;

import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.os.RemoteException;
import android.os.StatFs;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.backgroundscan.a;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.login.PluginLoginIn;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.notification.v;
import com.tencent.assistant.model.spaceclean.SubRubbishInfo;
import com.tencent.assistant.module.callback.ak;
import com.tencent.assistant.module.callback.g;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.c;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.cv;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean;
import com.tencent.connect.common.Constants;
import com.tencent.tmsecurelite.commom.ServiceManager;
import com.tencent.tmsecurelite.commom.b;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;
import com.tencent.tmsecurelite.optimize.f;
import java.io.File;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class SpaceScanManager implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f1613a = {"jpg", "jpeg", "bmp", "png"};
    private static final String[] b = {"mp4", "mkv", "flv", "avi", "rmvb", "rm", "3gp"};
    private static final String[] c = {"mp3", "flac", "ape"};
    private static final String[] d = {"doc", "docx", "ppt", "pptx", "xls", "xlsx", "wps", "dps", "et", "rtf", "pdf"};
    private static final String[] e = {"zip", "rar"};
    private static SpaceScanManager f = null;
    private ReferenceQueue<g> g = new ReferenceQueue<>();
    private ConcurrentLinkedQueue<WeakReference<g>> h = new ConcurrentLinkedQueue<>();
    private ReferenceQueue<ak> i = new ReferenceQueue<>();
    private ConcurrentLinkedQueue<WeakReference<ak>> j = new ConcurrentLinkedQueue<>();
    private boolean k = false;
    /* access modifiers changed from: private */
    public boolean l = false;
    /* access modifiers changed from: private */
    public volatile boolean m = false;
    private volatile boolean n = false;
    private volatile boolean o = false;
    /* access modifiers changed from: private */
    public g p = new g(this, null);
    private Set<String> q = new HashSet(Arrays.asList(f1613a));
    private Set<String> r = new HashSet(Arrays.asList(b));
    private Set<String> s = new HashSet(Arrays.asList(c));
    private Set<String> t = new HashSet(Arrays.asList(d));
    private Set<String> u = new HashSet(Arrays.asList(e));
    private ArrayList<SubRubbishInfo> v = new ArrayList<>();
    private long w = 0;
    private ApkResCallback x = new a(this);
    /* access modifiers changed from: private */
    public ISystemOptimize y = null;
    private ServiceConnection z = new b(this);

    /* compiled from: ProGuard */
    public enum ManagerAvaliableState {
        AVALIABLE,
        NOT_INSTALLED,
        VERSION_LOW
    }

    private SpaceScanManager() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
    }

    public static SpaceScanManager a() {
        if (f == null) {
            synchronized (SpaceScanManager.class) {
                if (f == null) {
                    f = new SpaceScanManager();
                }
            }
        }
        return f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    /* access modifiers changed from: private */
    public void b(List<LocalApkInfo> list) {
        JSONArray jSONArray;
        long a2 = m.a().a("key_space_clean_last_scan_useless_app_time", 0L);
        if (a2 == 0) {
            XLog.d("miles", "lastRunTime == 0. set current time as lastRunTime.");
            m.a().b("key_space_clean_last_scan_useless_app_time", Long.valueOf(System.currentTimeMillis()));
        } else if (System.currentTimeMillis() - a2 <= TesDownloadConfig.TES_CONFIG_CHECK_PERIOD) {
            XLog.d("miles", "SpaceScanManager >> checkUsedlessAndNotify not after one day, just return.");
        } else if (list == null || list.size() == 0) {
            XLog.d("miles", "apks list is null or apks.size() == 0");
        } else {
            XLog.d("miles", "SpaceScanManager >> check useless app begain.");
            List<LocalApkInfo> a3 = a(list);
            if (a3 == null || a3.size() <= 0) {
                m.a().b("key_use_less_app_pkgs", Constants.STR_EMPTY);
            } else {
                try {
                    XLog.d("miles", "usedLessList.size() = " + a3.size());
                    JSONArray jSONArray2 = new JSONArray();
                    String a4 = m.a().a("key_use_less_app_pkgs", Constants.STR_EMPTY);
                    if (a4 == null || a4.length() <= 0) {
                        jSONArray = null;
                    } else {
                        jSONArray = new JSONArray(a4);
                    }
                    for (LocalApkInfo localApkInfo : a3) {
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put("pkgname", localApkInfo.mPackageName);
                        jSONArray2.put(jSONObject);
                    }
                    ArrayList arrayList = new ArrayList(a3);
                    JSONArray jSONArray3 = new JSONArray();
                    if (jSONArray != null) {
                        XLog.d("miles", "oldPkgsUsedLess.length() = " + jSONArray.length());
                        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                            JSONObject jSONObject2 = (JSONObject) jSONArray.get(i2);
                            String string = jSONObject2.getString("pkgname");
                            Iterator<LocalApkInfo> it = a3.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    break;
                                }
                                LocalApkInfo next = it.next();
                                if (next.mPackageName.equals(string)) {
                                    a3.remove(next);
                                    jSONArray3.put(jSONObject2);
                                    break;
                                }
                            }
                        }
                    }
                    if (a3.size() > 0) {
                        v.a().a(arrayList);
                        a.b().f();
                        if (jSONArray2 != null && jSONArray2.length() > 0) {
                            m.a().b("key_use_less_app_pkgs", jSONArray2.toString());
                        }
                    } else if (jSONArray3 != null && jSONArray3.length() > 0) {
                        m.a().b("key_use_less_app_pkgs", jSONArray3.toString());
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            XLog.d("miles", "SpaceScanManager >> check useless app finish.");
        }
    }

    public List<LocalApkInfo> a(List<LocalApkInfo> list) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        String c2 = e.c();
        for (LocalApkInfo next : list) {
            if (((next.flags & 1) == 0 || (next.flags & 128) != 0) && !next.mPackageName.startsWith("com.tencent")) {
                if ((c2 == null || !c2.equals(next.mPackageName)) && next.mFakeLastLaunchTime > 0 && cv.e(next.mFakeLastLaunchTime)) {
                    arrayList.add(next);
                }
            }
        }
        return arrayList;
    }

    public void b() {
        if (g()) {
            float q2 = (float) q();
            float r2 = (float) r();
            if (r2 != 0.0f && q2 != 0.0f) {
                float f2 = q2 / r2;
                if (f2 > 0.0f) {
                    XLog.d("miles", "availableMem = " + bt.a((long) q2) + "; totalMem = " + bt.a((long) r2) + "; avapercent = " + f2);
                    if (f2 <= 0.1f) {
                        f();
                    }
                }
            }
        }
    }

    private long q() {
        try {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
        } catch (Throwable th) {
            th.printStackTrace();
            return 0;
        }
    }

    private long r() {
        try {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        } catch (Throwable th) {
            th.printStackTrace();
            return 0;
        }
    }

    public void c() {
        if (e()) {
            XLog.d("miles", "SpaceScanManager >> bindTmsLiteService. service already binded. return.");
            s();
            return;
        }
        try {
            if (!AstApp.i().bindService(ServiceManager.getIntent(0), this.z, 1)) {
                XLog.d("miles", "SpaceScanManager >> bindTmsLiteService failed. mobile manager version is low.");
                t();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            t();
        }
    }

    public void d() {
        try {
            AstApp.i().unbindService(this.z);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void a(g gVar) {
        boolean z2;
        if (gVar != null) {
            while (true) {
                WeakReference weakReference = (WeakReference) this.g.poll();
                if (weakReference == null) {
                    break;
                }
                this.h.remove(weakReference);
            }
            Iterator<WeakReference<g>> it = this.h.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((g) it.next().get()) == gVar) {
                        z2 = false;
                        break;
                    }
                } else {
                    z2 = true;
                    break;
                }
            }
            if (z2) {
                this.h.add(new WeakReference(gVar, this.g));
            }
        }
    }

    public void b(g gVar) {
        if (gVar != null) {
            Iterator<WeakReference<g>> it = this.h.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                g gVar2 = (g) next.get();
                if (gVar2 != null && gVar2 == gVar) {
                    this.h.remove(next);
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void s() {
        Iterator<WeakReference<g>> it = this.h.iterator();
        while (it.hasNext()) {
            g gVar = (g) it.next().get();
            if (gVar != null) {
                gVar.a();
            }
        }
    }

    /* access modifiers changed from: private */
    public void t() {
        Iterator<WeakReference<g>> it = this.h.iterator();
        while (it.hasNext()) {
            g gVar = (g) it.next().get();
            if (gVar != null) {
                gVar.b();
            }
        }
    }

    /* access modifiers changed from: private */
    public void u() {
        Iterator<WeakReference<g>> it = this.h.iterator();
        while (it.hasNext()) {
            g gVar = (g) it.next().get();
            if (gVar != null) {
                gVar.c();
            }
        }
    }

    public boolean e() {
        return this.l && this.y != null;
    }

    public void a(ak akVar) {
        boolean z2;
        if (akVar != null) {
            while (true) {
                WeakReference weakReference = (WeakReference) this.i.poll();
                if (weakReference == null) {
                    break;
                }
                this.j.remove(weakReference);
            }
            Iterator<WeakReference<ak>> it = this.j.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((ak) it.next().get()) == akVar) {
                        z2 = false;
                        break;
                    }
                } else {
                    z2 = true;
                    break;
                }
            }
            if (z2) {
                this.j.add(new WeakReference(akVar, this.i));
            }
        }
    }

    public void b(ak akVar) {
        if (akVar != null) {
            Iterator<WeakReference<ak>> it = this.j.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                ak akVar2 = (ak) next.get();
                if (akVar2 != null && akVar2 == akVar) {
                    this.j.remove(next);
                    return;
                }
            }
        }
    }

    private void a(long j2, SubRubbishInfo subRubbishInfo) {
        XLog.d("miles", "SpaceScanManager >> notifyRubbishFound. name:" + subRubbishInfo.b + ", size:" + bt.c(subRubbishInfo.c) + ", path:" + subRubbishInfo.f.get(0));
        Iterator<WeakReference<ak>> it = this.j.iterator();
        while (it.hasNext()) {
            ak akVar = (ak) it.next().get();
            if (akVar != null) {
                akVar.a(j2, subRubbishInfo);
            }
        }
    }

    private void b(ArrayList<SubRubbishInfo> arrayList) {
        Iterator<WeakReference<ak>> it = this.j.iterator();
        while (it.hasNext()) {
            ak akVar = (ak) it.next().get();
            if (akVar != null) {
                akVar.a(arrayList);
            }
        }
    }

    private void a(boolean z2) {
        Iterator<WeakReference<ak>> it = this.j.iterator();
        while (it.hasNext()) {
            ak akVar = (ak) it.next().get();
            if (akVar != null) {
                akVar.a(z2);
            }
        }
    }

    public synchronized void f() {
        v.a().a(0, 0);
        v();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    private void v() {
        m.a().b("key_space_clean_last_push_time", Long.valueOf(System.currentTimeMillis()));
        if (this.k) {
            this.k = false;
            m.a().b("key_space_clean_last_push_clicked", (Object) false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public boolean g() {
        long currentTimeMillis = System.currentTimeMillis();
        long a2 = m.a().a("key_space_clean_last_push_time", 0L);
        this.k = m.a().a("key_space_clean_last_push_clicked", false);
        if (this.k && currentTimeMillis - a2 >= TesDownloadConfig.TES_CONFIG_CHECK_PERIOD) {
            return true;
        }
        if (this.k || currentTimeMillis - a2 < 604800000) {
            return false;
        }
        return true;
    }

    public void a(f fVar) {
        TemporaryThreadManager.get().start(new c(this, fVar));
    }

    public void a(b bVar, ArrayList<String> arrayList) {
        XLog.d("miles", "SpaceScanManager >> startClearRubbish.");
        if (this.y != null) {
            try {
                this.y.cleanRubbishAsync(bVar, arrayList);
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void a(ak akVar, ArrayList<String> arrayList) {
        if (arrayList == null || arrayList.size() == 0) {
            akVar.a(true);
            return;
        }
        XLog.d("miles", "SpaceScanManager >> cleanRubbishAsync pathList.size=" + arrayList.size());
        TemporaryThreadManager.get().start(new d(this, arrayList, akVar));
    }

    public void h() {
        TemporaryThreadManager.get().start(new e(this));
    }

    public boolean i() {
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        if (localApkInfo == null) {
            localApkInfo = e.b(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        }
        if (localApkInfo == null || localApkInfo.mVersionCode < 1007) {
            return false;
        }
        return true;
    }

    public boolean j() {
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        if (localApkInfo == null) {
            localApkInfo = e.b(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        }
        if (localApkInfo == null || localApkInfo.mVersionCode < 1018) {
            return false;
        }
        return true;
    }

    public boolean k() {
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        if (localApkInfo == null) {
            localApkInfo = e.b(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        }
        if (localApkInfo != null) {
            return true;
        }
        return false;
    }

    public ManagerAvaliableState l() {
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        if (localApkInfo == null) {
            localApkInfo = e.b(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        }
        if (localApkInfo == null) {
            return ManagerAvaliableState.NOT_INSTALLED;
        }
        if (localApkInfo.mVersionCode < 1007) {
            return ManagerAvaliableState.VERSION_LOW;
        }
        return ManagerAvaliableState.AVALIABLE;
    }

    public boolean m() {
        return this.m;
    }

    public void handleUIEvent(Message message) {
        XLog.d("miles", "SpaceScanMangaer.. handleUIEvent. msg.what=" + message.what);
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_INSTALL:
            case 1013:
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC:
                String str = Constants.STR_EMPTY;
                if (message.obj instanceof String) {
                    str = (String) message.obj;
                } else if (message.obj instanceof InstallUninstallTaskBean) {
                    str = ((InstallUninstallTaskBean) message.obj).packageName;
                }
                if (!TextUtils.isEmpty(str) && str.equals(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME) && !a().e()) {
                    a().c();
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_UNINSTALL:
                if ((message.obj instanceof String) && ((String) message.obj).equals(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME)) {
                    this.l = false;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void n() {
        File[] listFiles;
        if (this.n) {
            XLog.d("miles", "SpaceScanManager >> startSpaceScan. isSpaceScanning = true, return");
        } else if (this.o && this.w != 0 && System.currentTimeMillis() - this.w < 300000) {
            b(this.v);
        } else if (!"mounted".equals(Environment.getExternalStorageState())) {
            b((ArrayList<SubRubbishInfo>) new ArrayList());
            XLog.d("miles", "SpaceScanManager >> storage state is " + Environment.getExternalStorageState() + ", notifyScanFinished.");
        } else {
            this.n = true;
            this.o = false;
            this.v.clear();
            long currentTimeMillis = System.currentTimeMillis();
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            LinkedList linkedList = new LinkedList();
            linkedList.add(externalStorageDirectory);
            BigFileFilter bigFileFilter = new BigFileFilter();
            long j2 = 0;
            while (!linkedList.isEmpty()) {
                File file = (File) linkedList.remove(0);
                if (!(!file.exists() || (listFiles = file.listFiles(bigFileFilter)) == null || listFiles.length == 0)) {
                    long j3 = j2;
                    for (File file2 : listFiles) {
                        if (file2.isDirectory()) {
                            linkedList.add(file2);
                        } else {
                            SubRubbishInfo a2 = a(file2);
                            if (a2 != null) {
                                j3 += a2.c;
                                a(j3, a2);
                                this.v.add(a2);
                            }
                        }
                    }
                    j2 = j3;
                }
            }
            b(this.v);
            XLog.d("miles", "SpaceScanManager >> startSpaceScan finished.");
            this.n = false;
            this.o = true;
            this.w = System.currentTimeMillis();
            XLog.d("miles", "SpaceScanManager >> startSpaceScan cost " + (System.currentTimeMillis() - currentTimeMillis));
        }
    }

    public void a(ArrayList<String> arrayList) {
        this.o = false;
        try {
            Iterator<String> it = arrayList.iterator();
            while (it.hasNext()) {
                File file = new File(it.next());
                if (file.exists()) {
                    file.delete();
                }
            }
        } catch (Throwable th) {
            a(false);
        }
        a(true);
    }

    private SubRubbishInfo a(File file) {
        SubRubbishInfo.RubbishType rubbishType;
        if (file == null || !file.exists()) {
            return null;
        }
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        SubRubbishInfo.RubbishType rubbishType2 = SubRubbishInfo.RubbishType.UNKNOWN;
        if (lastIndexOf != -1 && lastIndexOf + 1 < name.length() - 1) {
            String lowerCase = name.substring(lastIndexOf + 1).toLowerCase();
            if (this.q.contains(lowerCase)) {
                if (file.length() >= 10485760) {
                    rubbishType2 = SubRubbishInfo.RubbishType.BIG_FILE;
                }
            } else if (this.r.contains(lowerCase)) {
                rubbishType2 = SubRubbishInfo.RubbishType.VIDEO_FILE;
            } else if (this.s.contains(lowerCase)) {
                rubbishType2 = SubRubbishInfo.RubbishType.MUSIC_FILE;
            } else if (this.t.contains(lowerCase)) {
                rubbishType2 = SubRubbishInfo.RubbishType.DOC_FILE;
            } else if (this.u.contains(lowerCase)) {
                rubbishType2 = SubRubbishInfo.RubbishType.ZIP_FILE;
            } else if ("apk".contains(lowerCase)) {
                rubbishType2 = SubRubbishInfo.RubbishType.APK_FILE;
            } else if (file.length() >= 10485760) {
                rubbishType2 = SubRubbishInfo.RubbishType.BIG_FILE;
            }
            rubbishType = rubbishType2;
        } else if (file.length() >= 10485760) {
            rubbishType = SubRubbishInfo.RubbishType.BIG_FILE;
        } else {
            rubbishType = rubbishType2;
        }
        if (rubbishType != SubRubbishInfo.RubbishType.UNKNOWN) {
            return new SubRubbishInfo(rubbishType, name, file.length(), false, file.getAbsolutePath());
        }
        return null;
    }

    public boolean o() {
        return this.n;
    }

    public void p() {
        PluginInfo a2 = d.b().a("com.assistant.accelerate");
        if (a2 != null) {
            try {
                Class<?> loadClass = c.a(AstApp.i().getApplicationContext(), a2).loadClass("com.assistant.accelerate.PluginAccelerateEntry");
                Object newInstance = loadClass.newInstance();
                ba.a().post(new f(this, loadClass.getDeclaredMethod("execMemoryClean", Bundle.class), newInstance));
            } catch (Exception e2) {
                e2.printStackTrace();
                AstApp.i().j().sendMessage(PluginLoginIn.getEventDispatcher().obtainMessage(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL));
            }
        } else {
            AstApp.i().j().sendMessage(PluginLoginIn.getEventDispatcher().obtainMessage(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL));
        }
    }
}
