package com.tencent.assistant.manager.notification;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class x extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f1583a;

    x(v vVar) {
        this.f1583a = vVar;
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        if (localApkInfo != null && i == 2 && this.f1583a.f != null && !this.f1583a.f.b() && this.f1583a.f.a(localApkInfo.mPackageName)) {
            v.a().b(false);
        }
    }
}
