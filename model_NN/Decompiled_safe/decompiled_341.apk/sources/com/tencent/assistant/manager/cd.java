package com.tencent.assistant.manager;

import android.text.TextUtils;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.AdviceApp;
import com.tencent.assistant.protocol.jce.AdviceAppCollection;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class cd {
    static Map<Long, Integer> k = new ConcurrentHashMap(3);
    static Map<Long, Integer> l = new ConcurrentHashMap(1);
    static Map<Long, Integer> m = new ConcurrentHashMap(1);

    /* renamed from: a  reason: collision with root package name */
    public HashMap<Long, ArrayList<AdviceApp>> f1525a = null;
    public HashMap<Long, ArrayList<AdviceApp>> b = null;
    public ArrayList<AdviceApp> c = null;
    public ArrayList<AdviceApp> d = null;
    public long e;
    public String f;
    public String g;
    public String h;
    public String i;
    public int j;

    public cd(AdviceAppCollection adviceAppCollection) {
        if (!(adviceAppCollection == null || adviceAppCollection.f1973a == null)) {
            this.f1525a = new HashMap<>();
            this.f1525a.putAll(adviceAppCollection.f1973a);
        }
        if (!(adviceAppCollection == null || adviceAppCollection.b == null)) {
            this.b = new HashMap<>();
            this.b.putAll(adviceAppCollection.b);
        }
        if (!(adviceAppCollection == null || adviceAppCollection.c == null)) {
            this.c = new ArrayList<>();
            this.c.addAll(adviceAppCollection.c);
        }
        if (adviceAppCollection != null && adviceAppCollection.d != null) {
            this.d = new ArrayList<>();
            this.d.addAll(adviceAppCollection.d);
        }
    }

    /* access modifiers changed from: protected */
    public int a(long j2) {
        Integer num = k.get(Long.valueOf(j2));
        if (num == null) {
            num = Integer.valueOf(m.a().d(j2));
            k.put(Long.valueOf(j2), num);
        }
        return num.intValue();
    }

    /* access modifiers changed from: protected */
    public int b(long j2) {
        if (Calendar.getInstance().get(2) + 1 == c(j2)) {
            Integer num = l.get(Long.valueOf(j2));
            if (num == null) {
                num = Integer.valueOf(m.a().e(j2));
                l.put(Long.valueOf(j2), num);
            }
            XLog.i("jesonma", j2 + "当月已经弹出次数：" + num);
            return num.intValue();
        }
        int i2 = Calendar.getInstance().get(2) + 1;
        m.a().a(Long.valueOf(j2), 0);
        l.put(Long.valueOf(j2), 0);
        m.put(Long.valueOf(j2), Integer.valueOf(i2));
        m.a().b(Long.valueOf(j2), i2);
        XLog.i("jesonma", j2 + "当月已经弹出次数：" + 0);
        return 0;
    }

    /* access modifiers changed from: protected */
    public int c(long j2) {
        Integer num = m.get(Long.valueOf(j2));
        if (num == null) {
            num = Integer.valueOf(m.a().f(j2));
            if (num.intValue() == 0) {
                num = Integer.valueOf(Calendar.getInstance().get(2) + 1);
                m.put(Long.valueOf(j2), num);
                m.a().b(Long.valueOf(j2), num.intValue());
            }
        }
        XLog.i("jesonma", "当前的月份为：" + num);
        return num.intValue();
    }

    public boolean a(long j2, String str, long j3) {
        return b(j2, str, j3) && bo.i;
    }

    private boolean b(long j2, String str, long j3) {
        HashMap<Long, ArrayList<AdviceApp>> hashMap = this.f1525a;
        HashMap<Long, ArrayList<AdviceApp>> hashMap2 = this.b;
        if (hashMap != null && hashMap.get(Long.valueOf(j2)) != null && hashMap.get(Long.valueOf(j2)).size() > 0 && a(hashMap.get(Long.valueOf(j2)), j2, str)) {
            return true;
        }
        if (hashMap2 == null || hashMap2.get(Long.valueOf(j3)) == null || hashMap2.get(Long.valueOf(j3)).size() <= 0 || !a(hashMap2.get(Long.valueOf(j3)), j2, str)) {
            return false;
        }
        return true;
    }

    public boolean a() {
        ArrayList<AdviceApp> arrayList = this.d;
        if (arrayList != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= arrayList.size()) {
                    break;
                }
                AdviceApp adviceApp = arrayList.get(i3);
                if (adviceApp != null) {
                    long a2 = adviceApp.a();
                    String b2 = adviceApp.b();
                    String c2 = adviceApp.c();
                    int d2 = adviceApp.d();
                    String e2 = adviceApp.e();
                    String f2 = adviceApp.f();
                    int g2 = adviceApp.g();
                    boolean h2 = adviceApp.h();
                    if ((a2 != 0 || !TextUtils.isEmpty(b2)) && !TextUtils.isEmpty(e2) && ((!h2 || m.a().n() != AppConst.ROOT_STATUS.ROOTED) && ApkResourceManager.getInstance().getLocalApkInfo(b2) == null && (System.currentTimeMillis() / 1000) - ((long) a(a2)) > ((long) d2) && g2 > b(a2))) {
                        this.e = a2;
                        this.f = c2;
                        this.h = e2;
                        this.i = f2;
                        this.g = b2;
                        return true;
                    }
                }
                i2 = i3 + 1;
            }
        }
        return false;
    }

    public AdviceApp b() {
        AdviceApp a2 = a(this.f1525a, this.e);
        if (a2 != null) {
            return a2;
        }
        AdviceApp a3 = a(this.b, this.e);
        if (a3 == null) {
            return null;
        }
        return a3;
    }

    /* access modifiers changed from: protected */
    public AdviceApp a(HashMap<Long, ArrayList<AdviceApp>> hashMap, long j2) {
        if (hashMap == null) {
            return null;
        }
        for (ArrayList next : hashMap.values()) {
            if (next != null) {
                Iterator it = next.iterator();
                while (it.hasNext()) {
                    AdviceApp adviceApp = (AdviceApp) it.next();
                    if (adviceApp.f1972a == j2) {
                        return adviceApp;
                    }
                }
                continue;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean a(ArrayList<AdviceApp> arrayList, long j2, String str) {
        if (arrayList != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= arrayList.size()) {
                    break;
                }
                AdviceApp adviceApp = arrayList.get(i3);
                if (adviceApp != null) {
                    long a2 = adviceApp.a();
                    String b2 = adviceApp.b();
                    String c2 = adviceApp.c();
                    int d2 = adviceApp.d();
                    String e2 = adviceApp.e();
                    String f2 = adviceApp.f();
                    int g2 = adviceApp.g();
                    boolean h2 = adviceApp.h();
                    if ((a2 != 0 || !TextUtils.isEmpty(b2)) && !TextUtils.isEmpty(e2) && ((!h2 || m.a().n() != AppConst.ROOT_STATUS.ROOTED) && ApkResourceManager.getInstance().getLocalApkInfo(b2) == null && j2 != a2 && ((str == null || !str.equals(b2)) && (System.currentTimeMillis() / 1000) - ((long) a(a2)) > ((long) d2) && g2 > b(a2)))) {
                        this.e = a2;
                        this.f = c2;
                        this.h = e2;
                        this.i = f2;
                        this.g = b2;
                        return true;
                    }
                }
                i2 = i3 + 1;
            }
        }
        return false;
    }

    public void c() {
        int currentTimeMillis = (int) (System.currentTimeMillis() / 1000);
        m.a().a(this.e, currentTimeMillis);
        k.put(Long.valueOf(this.e), Integer.valueOf(currentTimeMillis));
        Integer num = l.get(Long.valueOf(this.e));
        if (num == null) {
            num = 0;
        }
        m.a().a(Long.valueOf(this.e), num.intValue() + 1);
        l.put(Long.valueOf(this.e), Integer.valueOf(num.intValue() + 1));
        m.a().d(currentTimeMillis);
    }
}
