package com.tencent.assistant;

import android.content.DialogInterface;
import android.view.View;

/* compiled from: ProGuard */
public interface AppConst {
    public static final int FROM_ACCOUNT = 8;
    public static final int FROM_ALPHA = 4;
    public static final int FROM_APPBAR = 6;
    public static final int FROM_APPDETAIL_SEND_TOPIC = 11;
    public static final int FROM_APPDETAIL_SEND_TOPIC_SHOW_INFO = 12;
    public static final int FROM_APP_BACKUP = 10;
    public static final int FROM_COLLECTION = 2;
    public static final int FROM_CONNECTOR = 7;
    public static final int FROM_DEFAULT = 0;
    public static final int FROM_FREEWIFI = 17;
    public static final int FROM_GUESS_FAVOR = 9;
    public static final int FROM_GUIDE = 16;
    public static final int FROM_PLUGIN = 14;
    public static final int FROM_PRIVACY_PROTECTION = 15;
    public static final int FROM_QQ = 5;
    public static final int FROM_RANK_FRIENDS = 13;
    public static final int FROM_RECOVER_OLD_APPS = 18;
    public static final int FROM_SHARE = 1;
    public static final int FROM_TAKE_NO = 3;
    public static final String KEY_ACCESSTOKEN = "accessToken";
    public static final String KEY_ERRMSG = "ERRMSG";
    public static final String KEY_ERROR_MSG = "error_msg";
    public static final String KEY_FROM_TYPE = "from";
    public static final String KEY_LOGIN_ANIM = "login_anim";
    public static final String KEY_LOGIN_TYPE = "login_type";
    public static final String KEY_META_DATA_GRAY_CODE = "com.tencent.android.qqdownloader.GRAY_CODE";
    public static final String KEY_OPENID = "openId";
    public static final String KEY_OPEN_APPID = "openAppId";
    public static final String KEY_QUICKLOGIN_BUFFER = "quicklogin_buff";
    public static final String KEY_QUICKLOGIN_BUFFER_STR = "quicklogin_buff_str";
    public static final String KEY_QUICKLOGIN_UIN = "quicklogin_uin";
    public static final String KEY_RET = "quicklogin_ret";
    public static final String KING_ROOT_PKGNAME = "com.kingroot.RushRoot";
    public static final int LOGIN_TYPE_FLOATING_DEFAULT = 2;
    public static final int LOGIN_TYPE_FLOATING_NOWX = 6;
    public static final int LOGIN_TYPE_Q_NO_WX = 4;
    public static final int LOGIN_TYPE_Q_QUICKLOGIN_DIRECT = 1;
    public static final int LOGIN_TYPE_Q_QUICKLOGIN_SELECT = 2;
    public static final int LOGIN_TYPE_Q_TAKE_NO = 5;
    public static final String MOBILE_QQ_LOGIN_ACTIVITY = "com.tencent.mobileqq.activity.LoginActivity";
    public static final String MOBILE_QQ_PACKAGENAME = "com.tencent.mobileqq";
    public static final int SHARE_QQ = 1;
    public static final int SHARE_QZ = 2;
    public static final int SHARE_TIMELINE = 4;
    public static final int SHARE_WX = 3;
    public static final String TENCENT_MOBILE_MANAGER_PKGNAME = "com.tencent.qqpimsecure";
    public static final String WX_PACKAGE_NAME = "com.tencent.mm";

    /* compiled from: ProGuard */
    public enum AppState {
        UPDATE,
        DOWNLOADING,
        PAUSED,
        DOWNLOADED,
        INSTALLED,
        FAIL,
        ILLEGAL,
        QUEUING,
        DOWNLOAD,
        SDKUNSUPPORT,
        INSTALLING,
        UNINSTALLING
    }

    /* compiled from: ProGuard */
    public abstract class DialogInfo {
        public boolean blockCaller;
        public String contentRes;
        public int pageId = 2000;
        public String titleRes;
    }

    /* compiled from: ProGuard */
    public enum IdentityType {
        NONE,
        MOBILEQ,
        WX,
        WXCODE
    }

    /* compiled from: ProGuard */
    public abstract class ListDialogInfo extends DialogInfo {
        public boolean hasTitle = true;
        public CharSequence[] items;

        public abstract void onClick(DialogInterface dialogInterface, int i);
    }

    /* compiled from: ProGuard */
    public class LoadingDialogInfo extends DialogInfo {
        public String loadingText;
    }

    /* compiled from: ProGuard */
    public enum LoginEgnineType {
        ENGINE_MOBILE_QQ,
        ENGINE_WX
    }

    /* compiled from: ProGuard */
    public abstract class OneBtnDialogInfo extends DialogInfo {
        public String btnTxtRes;
        public boolean hasTitle = true;

        public abstract void onBtnClick();

        public abstract void onCancell();
    }

    /* compiled from: ProGuard */
    public enum ROOT_STATUS {
        UNKNOWN,
        ROOTED,
        UNROOTED
    }

    /* compiled from: ProGuard */
    public abstract class TwoBtnDialogInfo extends DialogInfo {
        public View extraMsgView = null;
        public boolean hasTitle = true;
        public String lBtnTxtRes;
        public int rBtnBackgroundResId;
        public int rBtnTextColorResId;
        public String rBtnTxtRes;

        public abstract void onCancell();

        public abstract void onLeftBtnClick();

        public abstract void onRightBtnClick();
    }

    /* compiled from: ProGuard */
    public enum WISE_DOWNLOAD_SWITCH_TYPE {
        UPDATE,
        NEW_DOWNLOAD
    }
}
