package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.utils.XLog;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class SwitchPhoneTable implements IBaseTable {
    public static final String SQL_CREATE = "CREATE TABLE if not exists switch_phone_table (type  INTEGER UNIQUE ON CONFLICT REPLACE PRIMARY KEY,plain_count INTEGER,real_count INTEGER,result INTEGER,extra_data INTEGER,total_size INTEGER);";
    public static final String TABLE_NAME = "switch_phone_table";
    public static final int TABLE_VERSION = 1;

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return TABLE_NAME;
    }

    public String createTableSQL() {
        return SQL_CREATE;
    }

    public String[] getAlterSQL(int i, int i2) {
        return new String[]{SQL_CREATE};
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v4, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v7, resolved type: com.tencent.assistant.db.helper.SQLiteDatabaseWrapper} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004b A[SYNTHETIC, Splitter:B:12:0x004b] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0050 A[Catch:{ Exception -> 0x0054 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x007c A[SYNTHETIC, Splitter:B:25:0x007c] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0081 A[Catch:{ Exception -> 0x0085 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x008e A[SYNTHETIC, Splitter:B:34:0x008e] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0093 A[Catch:{ Exception -> 0x0097 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<android.os.Bundle> getAllItems() {
        /*
            r7 = this;
            r1 = 0
            java.lang.String r0 = "BussinessEngine-SwitchPhoneTable"
            java.lang.String r2 = "getAllItems---start"
            com.tencent.assistant.utils.XLog.d(r0, r2)
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            com.tencent.assistant.db.helper.SqliteHelper r0 = r7.getHelper()     // Catch:{ Exception -> 0x0059, all -> 0x008a }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r2 = r0.getReadableDatabaseWrapper()     // Catch:{ Exception -> 0x0059, all -> 0x008a }
            java.lang.String r0 = "select * from switch_phone_table"
            r4 = 0
            android.database.Cursor r1 = r2.rawQuery(r0, r4)     // Catch:{ Exception -> 0x009e }
            java.lang.String r0 = "BussinessEngine-SwitchPhoneTable"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009e }
            r4.<init>()     // Catch:{ Exception -> 0x009e }
            java.lang.String r5 = "getAllItems---cursor = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x009e }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ Exception -> 0x009e }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x009e }
            com.tencent.assistant.utils.XLog.d(r0, r4)     // Catch:{ Exception -> 0x009e }
            if (r1 == 0) goto L_0x0049
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x009e }
            if (r0 == 0) goto L_0x0049
        L_0x003c:
            android.os.Bundle r0 = r7.a(r1)     // Catch:{ Exception -> 0x009e }
            r3.add(r0)     // Catch:{ Exception -> 0x009e }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x009e }
            if (r0 != 0) goto L_0x003c
        L_0x0049:
            if (r1 == 0) goto L_0x004e
            r1.close()     // Catch:{ Exception -> 0x0054 }
        L_0x004e:
            if (r2 == 0) goto L_0x0053
            r2.close()     // Catch:{ Exception -> 0x0054 }
        L_0x0053:
            return r3
        L_0x0054:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0053
        L_0x0059:
            r0 = move-exception
            r2 = r1
        L_0x005b:
            java.lang.String r4 = "BussinessEngine-SwitchPhoneTable"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x009c }
            r5.<init>()     // Catch:{ all -> 0x009c }
            java.lang.String r6 = "getAllItems---e = "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x009c }
            java.lang.String r6 = r0.getMessage()     // Catch:{ all -> 0x009c }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x009c }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x009c }
            com.tencent.assistant.utils.XLog.d(r4, r5)     // Catch:{ all -> 0x009c }
            r0.printStackTrace()     // Catch:{ all -> 0x009c }
            if (r1 == 0) goto L_0x007f
            r1.close()     // Catch:{ Exception -> 0x0085 }
        L_0x007f:
            if (r2 == 0) goto L_0x0053
            r2.close()     // Catch:{ Exception -> 0x0085 }
            goto L_0x0053
        L_0x0085:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0053
        L_0x008a:
            r0 = move-exception
            r2 = r1
        L_0x008c:
            if (r1 == 0) goto L_0x0091
            r1.close()     // Catch:{ Exception -> 0x0097 }
        L_0x0091:
            if (r2 == 0) goto L_0x0096
            r2.close()     // Catch:{ Exception -> 0x0097 }
        L_0x0096:
            throw r0
        L_0x0097:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0096
        L_0x009c:
            r0 = move-exception
            goto L_0x008c
        L_0x009e:
            r0 = move-exception
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.SwitchPhoneTable.getAllItems():java.util.ArrayList");
    }

    private Bundle a(Cursor cursor) {
        XLog.d("BussinessEngine-SwitchPhoneTable", "cursor2Bundle--start");
        Bundle bundle = new Bundle();
        int i = cursor.getInt(cursor.getColumnIndex(SocialConstants.PARAM_TYPE));
        int i2 = cursor.getInt(cursor.getColumnIndex("plain_count"));
        int i3 = cursor.getInt(cursor.getColumnIndex("real_count"));
        int i4 = cursor.getInt(cursor.getColumnIndex("result"));
        String string = cursor.getString(cursor.getColumnIndex("extra_data"));
        XLog.d("BussinessEngine-SwitchPhoneTable", "cursor2Bundle--type = " + i + "plainCount = " + i2 + " realCoutnt = " + i3 + " extra_data = " + string);
        bundle.putInt(SocialConstants.PARAM_TYPE, i);
        bundle.putInt("plainCount", i2);
        bundle.putInt("realCount", i3);
        bundle.putInt("result", i4);
        bundle.putString("extra_data", string);
        bundle.putLong("totalSize", (long) cursor.getInt(cursor.getColumnIndex("total_size")));
        return bundle;
    }

    public int addItem(Bundle bundle) {
        if (bundle == null) {
            return -1;
        }
        int i = bundle.getInt(SocialConstants.PARAM_TYPE);
        int i2 = bundle.getInt("plainCount");
        int i3 = bundle.getInt("realCount");
        int i4 = bundle.getInt("result");
        String string = bundle.getString("extra_data");
        ContentValues contentValues = new ContentValues();
        contentValues.put(SocialConstants.PARAM_TYPE, Integer.valueOf(i));
        contentValues.put("plain_count", Integer.valueOf(i2));
        contentValues.put("real_count", Integer.valueOf(i3));
        contentValues.put("result", Integer.valueOf(i4));
        contentValues.put("extra_data", string);
        return (int) getHelper().getWritableDatabaseWrapper().insert(TABLE_NAME, null, contentValues);
    }

    public int deleteAll() {
        return getHelper().getWritableDatabaseWrapper().delete(TABLE_NAME, null, null);
    }

    public void addBatch(ArrayList<Bundle> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
            try {
                writableDatabaseWrapper.beginTransaction();
                Iterator<Bundle> it = arrayList.iterator();
                while (it.hasNext()) {
                    Bundle next = it.next();
                    int i = next.getInt(SocialConstants.PARAM_TYPE);
                    int i2 = next.getInt("plainCount");
                    int i3 = next.getInt("realCount");
                    int i4 = next.getInt("result");
                    long j = next.getLong("totalSize");
                    String string = next.getString("extra_data");
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(SocialConstants.PARAM_TYPE, Integer.valueOf(i));
                    contentValues.put("plain_count", Integer.valueOf(i2));
                    contentValues.put("real_count", Integer.valueOf(i3));
                    contentValues.put("result", Integer.valueOf(i4));
                    contentValues.put("extra_data", string);
                    contentValues.put("total_size", Long.valueOf(j));
                    XLog.d("BussinessEngine", "addBatch---i = " + writableDatabaseWrapper.insert(TABLE_NAME, null, contentValues));
                }
                writableDatabaseWrapper.setTransactionSuccessful();
            } finally {
                writableDatabaseWrapper.endTransaction();
            }
        }
    }

    public void updateItem(int i, int i2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("result", Integer.valueOf(i2));
        XLog.d("BussinessEngine-SwitchPhoneTable", "result = " + getHelper().getWritableDatabaseWrapper().update(TABLE_NAME, contentValues, "type=" + i, null));
    }
}
