package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.model.r;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class ae implements IBaseTable {
    public synchronized boolean a(r rVar) {
        boolean z = false;
        synchronized (this) {
            if (rVar != null) {
                SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
                ContentValues contentValues = new ContentValues();
                contentValues.put("banner_id", Integer.valueOf(rVar.f1671a));
                contentValues.put("refresh_counter", Integer.valueOf(rVar.k));
                contentValues.put("max_ignore_limit", Integer.valueOf(rVar.j));
                contentValues.put("record_time", Long.valueOf(rVar.l));
                contentValues.put("reversion", Long.valueOf(rVar.m));
                if (((int) (writableDatabaseWrapper.insert("top_banner_show_record_table", null, contentValues) + ((long) 0))) > 0) {
                    z = true;
                }
            }
        }
        return z;
    }

    public synchronized r a(int i) {
        r rVar;
        SQLiteDatabaseWrapper readableDatabaseWrapper = getHelper().getReadableDatabaseWrapper();
        Cursor cursor = null;
        rVar = new r();
        try {
            Cursor rawQuery = readableDatabaseWrapper.rawQuery("select * from top_banner_show_record_table where banner_id=?", new String[]{String.valueOf(i)});
            if (rawQuery != null && rawQuery.moveToFirst()) {
                int columnIndex = rawQuery.getColumnIndex("banner_id");
                int columnIndex2 = rawQuery.getColumnIndex("refresh_counter");
                int columnIndex3 = rawQuery.getColumnIndex("max_ignore_limit");
                int columnIndex4 = rawQuery.getColumnIndex("record_time");
                int columnIndex5 = rawQuery.getColumnIndex("reversion");
                rVar.f1671a = rawQuery.getInt(columnIndex);
                rVar.k = rawQuery.getInt(columnIndex2);
                rVar.j = rawQuery.getInt(columnIndex3);
                rVar.l = (long) rawQuery.getInt(columnIndex4);
                rVar.m = (long) rawQuery.getInt(columnIndex5);
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
        } catch (Exception e) {
            XLog.d("TopBannerShowRecordTable", e.getMessage());
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return rVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0084 A[SYNTHETIC, Splitter:B:30:0x0084] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.tencent.assistant.model.r b(int r11) {
        /*
            r10 = this;
            r8 = 0
            monitor-enter(r10)
            com.tencent.assistant.db.helper.SqliteHelper r0 = r10.getHelper()     // Catch:{ all -> 0x007d }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()     // Catch:{ all -> 0x007d }
            com.tencent.assistant.model.r r9 = new com.tencent.assistant.model.r     // Catch:{ all -> 0x007d }
            r9.<init>()     // Catch:{ all -> 0x007d }
            java.lang.String r1 = "top_banner_show_record_table"
            r2 = 0
            java.lang.String r3 = "banner_id = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0072, all -> 0x0080 }
            r5 = 0
            java.lang.String r6 = java.lang.String.valueOf(r11)     // Catch:{ Exception -> 0x0072, all -> 0x0080 }
            r4[r5] = r6     // Catch:{ Exception -> 0x0072, all -> 0x0080 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0072, all -> 0x0080 }
            if (r1 == 0) goto L_0x006b
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x008a }
            if (r0 == 0) goto L_0x006b
            java.lang.String r0 = "banner_id"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x008a }
            java.lang.String r2 = "refresh_counter"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x008a }
            java.lang.String r3 = "max_ignore_limit"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x008a }
            java.lang.String r4 = "record_time"
            int r4 = r1.getColumnIndex(r4)     // Catch:{ Exception -> 0x008a }
            java.lang.String r5 = "reversion"
            int r5 = r1.getColumnIndex(r5)     // Catch:{ Exception -> 0x008a }
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x008a }
            r9.f1671a = r0     // Catch:{ Exception -> 0x008a }
            int r0 = r1.getInt(r2)     // Catch:{ Exception -> 0x008a }
            r9.k = r0     // Catch:{ Exception -> 0x008a }
            int r0 = r1.getInt(r3)     // Catch:{ Exception -> 0x008a }
            r9.j = r0     // Catch:{ Exception -> 0x008a }
            int r0 = r1.getInt(r4)     // Catch:{ Exception -> 0x008a }
            long r2 = (long) r0     // Catch:{ Exception -> 0x008a }
            r9.l = r2     // Catch:{ Exception -> 0x008a }
            int r0 = r1.getInt(r5)     // Catch:{ Exception -> 0x008a }
            long r2 = (long) r0     // Catch:{ Exception -> 0x008a }
            r9.m = r2     // Catch:{ Exception -> 0x008a }
        L_0x006b:
            if (r1 == 0) goto L_0x0070
            r1.close()     // Catch:{ all -> 0x007d }
        L_0x0070:
            monitor-exit(r10)
            return r9
        L_0x0072:
            r0 = move-exception
            r1 = r8
        L_0x0074:
            r0.printStackTrace()     // Catch:{ all -> 0x0088 }
            if (r1 == 0) goto L_0x0070
            r1.close()     // Catch:{ all -> 0x007d }
            goto L_0x0070
        L_0x007d:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x0080:
            r0 = move-exception
            r1 = r8
        L_0x0082:
            if (r1 == 0) goto L_0x0087
            r1.close()     // Catch:{ all -> 0x007d }
        L_0x0087:
            throw r0     // Catch:{ all -> 0x007d }
        L_0x0088:
            r0 = move-exception
            goto L_0x0082
        L_0x008a:
            r0 = move-exception
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.ae.b(int):com.tencent.assistant.model.r");
    }

    public synchronized boolean b(r rVar) {
        boolean z = true;
        boolean z2 = false;
        synchronized (this) {
            if (rVar != null) {
                SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
                ContentValues contentValues = new ContentValues();
                contentValues.put("banner_id", Integer.valueOf(rVar.f1671a));
                contentValues.put("refresh_counter", Integer.valueOf(rVar.k));
                contentValues.put("max_ignore_limit", Integer.valueOf(rVar.j));
                contentValues.put("record_time", Long.valueOf(rVar.l));
                contentValues.put("reversion", Long.valueOf(rVar.m));
                if (writableDatabaseWrapper.update("top_banner_show_record_table", contentValues, "banner_id = ?", new String[]{String.valueOf(rVar.f1671a)}) + 0 <= 0) {
                    z = false;
                }
                z2 = z;
            }
        }
        return z2;
    }

    public synchronized int c(r rVar) {
        int i = 0;
        synchronized (this) {
            if (rVar != null) {
                i = 0 + getHelper().getWritableDatabaseWrapper().delete("top_banner_show_record_table", "banner_id = ?", new String[]{Integer.toString(rVar.f1671a)});
            }
        }
        return i;
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "top_banner_show_record_table";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists top_banner_show_record_table( _id INTEGER PRIMARY KEY AUTOINCREMENT, banner_id INTEGER, refresh_counter INTEGER, max_ignore_limit INTEGER, record_time INTEGER, reversion INTEGER); ";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i2 != 15) {
            return null;
        }
        return new String[]{"CREATE TABLE if not exists top_banner_show_record_table( _id INTEGER PRIMARY KEY AUTOINCREMENT, banner_id INTEGER, refresh_counter INTEGER, max_ignore_limit INTEGER, record_time INTEGER, reversion INTEGER); "};
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }
}
