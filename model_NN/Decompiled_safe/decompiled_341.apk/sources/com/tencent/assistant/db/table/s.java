package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.protocol.jce.PopUpInfo;

/* compiled from: ProGuard */
public class s implements IBaseTable {
    public synchronized long a(PopUpInfo popUpInfo) {
        long j;
        int i = 0;
        synchronized (this) {
            SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
            if (writableDatabaseWrapper != null) {
                try {
                    writableDatabaseWrapper.beginTransaction();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("_id", Long.valueOf(popUpInfo.f2258a));
                    contentValues.put("showTime", Long.valueOf(popUpInfo.d));
                    contentValues.put("cycle", Integer.valueOf(popUpInfo.f));
                    int update = writableDatabaseWrapper.update("pop_up_info_table", contentValues, "_id = ? ", new String[]{String.valueOf(popUpInfo.f2258a)});
                    if (update <= 0) {
                        i = (int) (writableDatabaseWrapper.insert("pop_up_info_table", null, contentValues) + ((long) 0));
                    } else {
                        i = 0 + update;
                    }
                    writableDatabaseWrapper.setTransactionSuccessful();
                    writableDatabaseWrapper.endTransaction();
                } catch (Exception e) {
                    e.printStackTrace();
                    writableDatabaseWrapper.endTransaction();
                } catch (Throwable th) {
                    writableDatabaseWrapper.endTransaction();
                    throw th;
                }
            }
            j = (long) i;
        }
        return j;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x0086 A[SYNTHETIC, Splitter:B:36:0x0086] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.ArrayList<com.tencent.assistant.protocol.jce.PopUpInfo> a() {
        /*
            r12 = this;
            r8 = 0
            monitor-enter(r12)
            com.tencent.assistant.db.helper.SqliteHelper r0 = r12.getHelper()     // Catch:{ all -> 0x007f }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()     // Catch:{ all -> 0x007f }
            java.util.ArrayList r9 = new java.util.ArrayList     // Catch:{ all -> 0x007f }
            r9.<init>()     // Catch:{ all -> 0x007f }
            java.lang.String r1 = "pop_up_info_table"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            java.lang.String r7 = "showTime desc"
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x008c, all -> 0x0082 }
            if (r1 == 0) goto L_0x006a
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0075 }
            if (r0 == 0) goto L_0x006a
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0075 }
        L_0x0028:
            com.tencent.assistant.protocol.jce.PopUpInfo r0 = new com.tencent.assistant.protocol.jce.PopUpInfo     // Catch:{ Exception -> 0x0075 }
            r0.<init>()     // Catch:{ Exception -> 0x0075 }
            java.lang.String r4 = "_id"
            int r4 = r1.getColumnIndex(r4)     // Catch:{ Exception -> 0x0075 }
            long r4 = r1.getLong(r4)     // Catch:{ Exception -> 0x0075 }
            r0.f2258a = r4     // Catch:{ Exception -> 0x0075 }
            java.lang.String r4 = "showTime"
            int r4 = r1.getColumnIndex(r4)     // Catch:{ Exception -> 0x0075 }
            long r4 = r1.getLong(r4)     // Catch:{ Exception -> 0x0075 }
            r0.d = r4     // Catch:{ Exception -> 0x0075 }
            java.lang.String r4 = "cycle"
            int r4 = r1.getColumnIndex(r4)     // Catch:{ Exception -> 0x0075 }
            int r4 = r1.getInt(r4)     // Catch:{ Exception -> 0x0075 }
            r0.f = r4     // Catch:{ Exception -> 0x0075 }
            long r4 = r0.d     // Catch:{ Exception -> 0x0075 }
            int r6 = r0.f     // Catch:{ Exception -> 0x0075 }
            long r6 = (long) r6     // Catch:{ Exception -> 0x0075 }
            r10 = 86400000(0x5265c00, double:4.2687272E-316)
            long r6 = r6 * r10
            long r4 = r4 + r6
            int r4 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r4 >= 0) goto L_0x0071
            long r4 = r0.f2258a     // Catch:{ Exception -> 0x0075 }
            r12.a(r4)     // Catch:{ Exception -> 0x0075 }
        L_0x0064:
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0075 }
            if (r0 != 0) goto L_0x0028
        L_0x006a:
            if (r1 == 0) goto L_0x006f
            r1.close()     // Catch:{ all -> 0x007f }
        L_0x006f:
            monitor-exit(r12)
            return r9
        L_0x0071:
            r9.add(r0)     // Catch:{ Exception -> 0x0075 }
            goto L_0x0064
        L_0x0075:
            r0 = move-exception
        L_0x0076:
            r0.printStackTrace()     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x006f
            r1.close()     // Catch:{ all -> 0x007f }
            goto L_0x006f
        L_0x007f:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        L_0x0082:
            r0 = move-exception
            r1 = r8
        L_0x0084:
            if (r1 == 0) goto L_0x0089
            r1.close()     // Catch:{ all -> 0x007f }
        L_0x0089:
            throw r0     // Catch:{ all -> 0x007f }
        L_0x008a:
            r0 = move-exception
            goto L_0x0084
        L_0x008c:
            r0 = move-exception
            r1 = r8
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.s.a():java.util.ArrayList");
    }

    public synchronized int a(long j) {
        return getHelper().getWritableDatabaseWrapper().delete("pop_up_info_table", "_id = ?", new String[]{Long.toString(j)});
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "pop_up_info_table";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists pop_up_info_table(_id INTEGER PRIMARY KEY AUTOINCREMENT,showTime INTEGER,cycle INTEGER);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i2 != 4) {
            return null;
        }
        return new String[]{"CREATE TABLE if not exists pop_up_info_table(_id INTEGER PRIMARY KEY AUTOINCREMENT,showTime INTEGER,cycle INTEGER);"};
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }
}
