package com.tencent.assistant.db;

import java.io.File;
import java.io.FilenameFilter;

/* compiled from: ProGuard */
final class b implements FilenameFilter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1229a;

    b(String str) {
        this.f1229a = str;
    }

    public boolean accept(File file, String str) {
        return str.endsWith("." + this.f1229a);
    }
}
