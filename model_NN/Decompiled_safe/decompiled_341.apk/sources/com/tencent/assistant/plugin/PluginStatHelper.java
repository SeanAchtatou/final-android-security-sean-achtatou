package com.tencent.assistant.plugin;

import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.business.i;
import com.tencent.assistantv2.st.business.s;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.Map;

/* compiled from: ProGuard */
public class PluginStatHelper {
    public static void logUserAction(int i, int i2, String str, int i3, byte b, Map<String, String> map) {
        k.a(new STInfoV2(i, str, i2, STConst.ST_DEFAULT_SLOT, i3), map);
    }

    public static void logUserAction(int i, int i2, String str, int i3, byte b, Map<String, String> map, byte[] bArr) {
        STInfoV2 sTInfoV2 = new STInfoV2(i, str, i2, STConst.ST_DEFAULT_SLOT, i3);
        sTInfoV2.recommendId = bArr;
        k.a(sTInfoV2, map);
    }

    public static void logUserActionImmediatly(int i, int i2, String str, int i3, long j, String str2, byte[] bArr) {
        STInfoV2 sTInfoV2 = new STInfoV2(i, str, i2, STConst.ST_DEFAULT_SLOT, i3);
        sTInfoV2.extraData = str2;
        sTInfoV2.pushId = j;
        sTInfoV2.recommendId = bArr;
        sTInfoV2.isImmediately = true;
        k.a(sTInfoV2);
    }

    public static void logConnectionTypeAction(byte[] bArr) {
        i.a().a(bArr);
    }

    public static void logPhotoBackupTypeAction(byte[] bArr) {
        s.a().a(bArr);
    }
}
