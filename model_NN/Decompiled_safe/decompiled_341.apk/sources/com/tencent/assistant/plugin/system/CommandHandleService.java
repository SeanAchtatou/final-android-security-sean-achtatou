package com.tencent.assistant.plugin.system;

import android.content.Intent;
import android.os.IBinder;
import com.tencent.assistant.plugin.PluginInfo;

/* compiled from: ProGuard */
public class CommandHandleService extends BaseAppService {
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int a() {
        return 0;
    }

    public String a(PluginInfo pluginInfo) {
        if (pluginInfo != null) {
            return pluginInfo.getExtendServiceImpl(PluginInfo.META_DATA_COMMANDHANDLE_SERVICE);
        }
        return null;
    }
}
