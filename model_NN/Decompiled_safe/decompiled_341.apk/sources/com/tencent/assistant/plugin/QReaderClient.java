package com.tencent.assistant.plugin;

import android.os.Bundle;
import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.event.EventController;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.login.d;
import com.tencent.assistant.plugin.QLoginCallback;
import com.tencent.assistant.plugin.a.a;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.dg;
import com.tencent.assistantv2.b.ap;
import com.tencent.assistantv2.model.b;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class QReaderClient implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private static QReaderClient f1938a;

    private QReaderClient() {
        EventController k = AstApp.i().k();
        k.addUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS, this);
        k.addUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL, this);
        k.addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        k.addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
        k.addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGOUT, this);
    }

    public static synchronized QReaderClient getInstance() {
        QReaderClient qReaderClient;
        synchronized (QReaderClient.class) {
            if (f1938a == null) {
                f1938a = new QReaderClient();
            }
            qReaderClient = f1938a;
        }
        return qReaderClient;
    }

    public void addQLoginCallback(QLoginCallback qLoginCallback) {
        XLog.i("QReaderClient", "add login callback:" + qLoginCallback);
        dg.b("P_L_S_R", qLoginCallback);
    }

    public void removeQLingCallback(QLoginCallback qLoginCallback) {
        XLog.i("QReaderClient", "remove login callback:" + qLoginCallback);
        dg.c("P_L_S_R", qLoginCallback);
    }

    public SimpleLoginInfo getUserLoginInfo() {
        return a.a();
    }

    public void triggerLogin(QLoginCallback qLoginCallback, Bundle bundle) {
        Bundle bundle2 = new Bundle();
        bundle2.putInt(AppConst.KEY_LOGIN_TYPE, 6);
        bundle2.putInt(AppConst.KEY_FROM_TYPE, 14);
        if (bundle != null) {
            bundle2.putAll(bundle);
        }
        if (qLoginCallback != null) {
            addQLoginCallback(qLoginCallback);
        }
        d.a().a(AppConst.IdentityType.MOBILEQ, bundle2);
    }

    public void notifyReaderProgress(Bundle bundle) {
        if (bundle != null) {
            new ap().a(bundle.getString("bookid"), bundle.getInt("chap"));
            AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_QREADER_READER_PROGRESS, bundle));
        }
    }

    public void notifyReaderDownloadStatus(Bundle bundle) {
        if (bundle != null) {
            b bVar = new b();
            bVar.c = bundle.getString("bookid");
            bVar.g = bundle.getInt("status");
            bVar.d = bundle.getString("coverUrl");
            bVar.f3315a = bundle.getString("bookName");
            bVar.e = bundle.getString("author");
            bVar.f = bundle.getInt("chap");
            com.tencent.assistantv2.mediadownload.b.a().a(bVar);
            AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_QREADER_DOWNLOAD_STATUS, bundle));
        }
    }

    public int downloadQQReader() {
        com.tencent.assistant.link.b.a(AstApp.i(), "tmast://appdetails?selflink=1&pname=com.qq.reader");
        return 0;
    }

    public void handleUIEvent(Message message) {
        QLoginCallback.LoginState loginState;
        QLoginCallback.LoginState loginState2 = QLoginCallback.LoginState.STATE_LOGIN_SUCC;
        SimpleLoginInfo userLoginInfo = getUserLoginInfo();
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS:
                loginState = QLoginCallback.LoginState.STATE_LOGIN_SUCC;
                break;
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL:
                loginState = QLoginCallback.LoginState.STATE_LOGIN_FAILED;
                break;
            case EventDispatcherEnum.UI_EVENT_GET_WXLOGIN_SUCCESS:
            case EventDispatcherEnum.UI_EVENT_GET_WXLOGIN_FAIL:
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
            case EventDispatcherEnum.UI_EVENT_LOGIN_WAIT:
            default:
                loginState = loginState2;
                break;
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL:
                loginState = QLoginCallback.LoginState.STATE_LOGIN_FAILED;
                break;
            case EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL:
                loginState = QLoginCallback.LoginState.STATE_LOGIN_CANCELLED;
                break;
            case EventDispatcherEnum.UI_EVENT_LOGOUT:
                loginState = QLoginCallback.LoginState.STATE_LOGOUT;
                break;
        }
        List<WeakReference<Object>> b = dg.b("P_L_S_R");
        XLog.i("QReaderClient", "login callback state:" + loginState + ",state callback list:" + b);
        if (b != null) {
            Iterator<WeakReference<Object>> it = b.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                if (next == null || next.get() == null || !(next.get() instanceof QLoginCallback)) {
                    XLog.i("QReaderClient", "login callback state:" + loginState + ",weakReference:" + (next != null ? next.get() : null));
                } else {
                    QLoginCallback qLoginCallback = (QLoginCallback) next.get();
                    XLog.i("QReaderClient", "login callback state:" + loginState + ",callback:" + qLoginCallback + ",loginInfo:" + userLoginInfo);
                    qLoginCallback.onLoginStateChanged(loginState, userLoginInfo);
                }
            }
        }
    }
}
