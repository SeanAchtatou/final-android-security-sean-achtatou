package com.tencent.assistant.plugin.system;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.text.TextUtils;
import com.tencent.assistant.plugin.IPluginService;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.c;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.utils.XLog;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public abstract class BaseAppService extends Service {

    /* renamed from: a  reason: collision with root package name */
    private Map<String, Service> f1953a = new ConcurrentHashMap();

    public abstract int a();

    public abstract String a(PluginInfo pluginInfo);

    public void onCreate() {
        super.onCreate();
        XLog.d("icerao", "service:" + this + " onCreate");
        a(d.b().a(a()));
    }

    private void a(List<PluginInfo> list) {
        if (list != null && list.size() > 0) {
            synchronized (this.f1953a) {
                for (PluginInfo next : list) {
                    String a2 = a(next);
                    if (!TextUtils.isEmpty(a2)) {
                        XLog.d("icerao", "service:" + a2);
                        if (!this.f1953a.containsKey(a2)) {
                            Service service = (Service) c.a(getBaseContext(), next, a2);
                            XLog.d("icerao", "serviceImpl:" + service + " not in map.init.");
                            if (service == null) {
                                continue;
                            } else {
                                if (service instanceof IPluginService) {
                                    ((IPluginService) service).attachBaseService(this);
                                }
                                try {
                                    service.onCreate();
                                    this.f1953a.put(next.getPackageName(), service);
                                } catch (Throwable th) {
                                    th.printStackTrace();
                                }
                            }
                        } else {
                            continue;
                        }
                    }
                }
            }
        }
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        a(d.b().a(1));
        if (this.f1953a != null) {
            for (Map.Entry next : this.f1953a.entrySet()) {
                if (next.getValue() != null) {
                    try {
                        ((Service) next.getValue()).onStart(intent, i);
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                }
            }
        }
    }

    public IBinder onBind(Intent intent) {
        if (this.f1953a != null) {
            for (Map.Entry next : this.f1953a.entrySet()) {
                if (next.getValue() != null) {
                    try {
                        return ((Service) next.getValue()).onBind(intent);
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                }
            }
        }
        return null;
    }

    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.f1953a != null) {
            for (Map.Entry next : this.f1953a.entrySet()) {
                if (next.getValue() != null) {
                    try {
                        ((Service) next.getValue()).onDestroy();
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                }
            }
            this.f1953a.clear();
        }
    }
}
