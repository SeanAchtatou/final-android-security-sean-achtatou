package com.tencent.assistant.plugin;

/* compiled from: ProGuard */
public interface QLoginCallback {

    /* compiled from: ProGuard */
    public enum LoginState {
        STATE_LOGIN_CANCELLED,
        STATE_LOGIN_FAILED,
        STATE_LOGIN_SUCC,
        STATE_LOGOUT
    }

    void onLoginStateChanged(LoginState loginState, SimpleLoginInfo simpleLoginInfo);
}
