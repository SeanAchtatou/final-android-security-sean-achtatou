package com.tencent.assistant.plugin;

import android.content.Context;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.plugin.mgr.c;
import com.tencent.assistant.plugin.mgr.d;
import java.lang.reflect.Method;
import java.util.List;

/* compiled from: ProGuard */
public class PluginApplication {

    /* renamed from: a  reason: collision with root package name */
    private static PluginApplication f1929a;

    private PluginApplication() {
    }

    public static synchronized PluginApplication getInstance() {
        PluginApplication pluginApplication;
        synchronized (PluginApplication.class) {
            if (f1929a == null) {
                f1929a = new PluginApplication();
            }
            pluginApplication = f1929a;
        }
        return pluginApplication;
    }

    public void initConnectApplication() {
        List<PluginInfo> a2 = d.b().a(1);
        if (a2 != null && a2.size() > 0) {
            for (PluginInfo next : a2) {
                if (next != null && next.getPackageName().equals("p.com.tencent.android.qqdownloader")) {
                    init(next.getPackageName(), next.getVersion());
                    return;
                }
            }
        }
    }

    public boolean init(String str, int i) {
        PluginInfo a2 = d.b().a(str, i);
        if (a2 == null) {
            return false;
        }
        PluginLoaderInfo a3 = c.a(AstApp.i(), a2);
        String launchApplication = a2.getLaunchApplication();
        if (TextUtils.isEmpty(launchApplication)) {
            return false;
        }
        try {
            Class<?> loadClass = a3.loadClass(launchApplication);
            Object newInstance = loadClass.newInstance();
            loadClass.getDeclaredMethod("attachApplicationContext", Context.class).invoke(newInstance, a3.getContext());
            return true;
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    public void exitConnectPlugin() {
        List<PluginInfo> a2 = d.b().a(1);
        if (a2 != null && a2.size() > 0) {
            for (PluginInfo next : a2) {
                if (next != null && next.getPackageName().equals("p.com.tencent.android.qqdownloader")) {
                    a(next.getPackageName(), next.getVersion());
                    return;
                }
            }
        }
    }

    private boolean a(String str, int i) {
        PluginInfo a2 = d.b().a(str, i);
        if (a2 == null) {
            return false;
        }
        PluginLoaderInfo a3 = c.a(AstApp.i(), a2);
        String launchApplication = a2.getLaunchApplication();
        if (TextUtils.isEmpty(launchApplication)) {
            return false;
        }
        try {
            Class<?> loadClass = a3.loadClass(launchApplication);
            Object newInstance = loadClass.newInstance();
            Method declaredMethod = loadClass.getDeclaredMethod("exitProcess", new Class[0]);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(newInstance, new Object[0]);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /* compiled from: ProGuard */
    public class ConnectInfo {
        public String pcName;
        public int state;

        public ConnectInfo(int i, String str) {
            this.state = i;
            this.pcName = str;
        }
    }

    public ConnectInfo getConnectInfo() {
        List<PluginInfo> a2 = d.b().a(1);
        if (a2 != null && a2.size() > 0) {
            for (PluginInfo next : a2) {
                if (next != null && next.getPackageName().equals("p.com.tencent.android.qqdownloader")) {
                    return b(next.getPackageName(), next.getVersion());
                }
            }
        }
        return null;
    }

    private ConnectInfo b(String str, int i) {
        PluginInfo a2 = d.b().a(str, i);
        if (a2 == null) {
            return null;
        }
        PluginLoaderInfo a3 = c.a(AstApp.i(), a2);
        String launchApplication = a2.getLaunchApplication();
        if (TextUtils.isEmpty(launchApplication)) {
            return null;
        }
        try {
            Class<?> loadClass = a3.loadClass(launchApplication);
            Object newInstance = loadClass.newInstance();
            Method declaredMethod = loadClass.getDeclaredMethod("getConnectState", new Class[0]);
            declaredMethod.setAccessible(true);
            Method declaredMethod2 = loadClass.getDeclaredMethod("getPcName", new Class[0]);
            declaredMethod2.setAccessible(true);
            return new ConnectInfo(((Integer) declaredMethod.invoke(newInstance, new Object[0])).intValue(), (String) declaredMethod2.invoke(newInstance, new Object[0]));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
