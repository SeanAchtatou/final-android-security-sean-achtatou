package com.tencent.assistant.plugin;

import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.plugin.mgr.e;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class PluginInfo {
    public static final String META_DATA_COMMANDHANDLE_SERVICE = "command_handle_service";
    public static final String META_DATA_DEEP_ACC_SERVICE = "deep_acc_service";
    public static final String META_DATA_DOCK_SEC_SERVICE = "dock_sec_service";
    public static final String META_DATA_FREE_WIFI_RECEIVER = "freewifi_receiver";
    public static final String META_DATA_FREE_WIFI_SERVICE = "freewifi_service";
    public static final String META_DATA_QREADER_RECEIVER = "qreader_receiver";
    public static final String META_DATA_SWITCH_PHONE_RESTORE_SERVICE = "switch_phone_restore_service";
    public static final String META_DATA_SWITCH_PHONE_SERVICE = "switch_phone_service";

    /* renamed from: a  reason: collision with root package name */
    private String f1933a = "PluginInfo";
    private int b;
    private String c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;
    private String i;
    private ApplicationInfo j;
    private List<PluginEntry> k = new ArrayList();
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;
    private String q;
    private String r;
    private String s;
    private String t;
    private String u;
    private Map<String, String> v;
    private Map<String, String> w;

    public String getDockReceiverImpl() {
        return this.t;
    }

    public void setDockReceiverImpl(String str) {
        this.t = str;
    }

    public String getAccelerationServiceImpl() {
        return this.u;
    }

    public void setAccelerationServiceImpl(String str) {
        this.u = str;
    }

    public String getHeartBeatReceiverImpl() {
        return this.q;
    }

    public void setHeartBeatReceiverImpl(String str) {
        this.q = str;
    }

    public String getAuthorReceiverImpl() {
        return this.p;
    }

    public void setAuthorReceiverImpl(String str) {
        this.p = str;
    }

    public String getSmsSentReceiverImpl() {
        return this.o;
    }

    public void setSmsSentReceiverImpl(String str) {
        this.o = str;
    }

    public String getLaunchApplication() {
        return this.i;
    }

    public void setLaunchApplication(String str) {
        this.i = str;
    }

    public int getVersion() {
        return this.b;
    }

    public void setVersion(int i2) {
        this.b = i2;
    }

    public String getPackageName() {
        return this.c;
    }

    public void setPackageName(String str) {
        this.c = str;
    }

    public int getPkgid() {
        return this.d;
    }

    public void setPkgid(int i2) {
        this.d = i2;
    }

    public int getMinBaoVersion() {
        return this.e;
    }

    public void setMinBaoVersion(int i2) {
        this.e = i2;
    }

    public int getMinApiLevel() {
        return this.f;
    }

    public void setMinApiLevel(int i2) {
        this.f = i2;
    }

    public int getMinFLevel() {
        return this.g;
    }

    public void setMinFLevel(int i2) {
        this.g = i2;
    }

    public int getInProcess() {
        return this.h;
    }

    public void setInProcess(int i2) {
        this.h = i2;
    }

    public String getSmsReceiverImpl() {
        return this.l;
    }

    public void setSmsReceiverImpl(String str) {
        this.l = str;
    }

    public void addEntry(String str, String str2) {
        this.k.add(new PluginEntry(this, str, a(str2), str2));
    }

    public List<PluginEntry> getPluginEntryList() {
        return this.k;
    }

    public String getPluginUniqueKey() {
        return e.d(this.c, this.b);
    }

    public String getPluginApkPath() {
        return e.a(this.c, this.b);
    }

    public synchronized ApplicationInfo getApplicationInfo() {
        PackageInfo packageArchiveInfo;
        if (this.j == null && (packageArchiveInfo = AstApp.i().getPackageManager().getPackageArchiveInfo(getPluginApkPath(), 1)) != null) {
            this.j = packageArchiveInfo.applicationInfo;
        }
        return this.j;
    }

    private Drawable a(String str) {
        PackageManager packageManager = AstApp.i().getPackageManager();
        try {
            PackageInfo a2 = com.tencent.assistant.utils.e.a(packageManager, getPluginApkPath(), 1);
            if (!(a2 == null || a2.applicationInfo == null)) {
                String pluginApkPath = getPluginApkPath();
                a2.applicationInfo.sourceDir = pluginApkPath;
                a2.applicationInfo.publicSourceDir = pluginApkPath;
                ActivityInfo[] activityInfoArr = a2.activities;
                if (activityInfoArr != null && activityInfoArr.length > 0) {
                    for (ActivityInfo activityInfo : activityInfoArr) {
                        if (activityInfo.name != null && activityInfo.name.equals(str)) {
                            return packageManager.getDrawable(a2.packageName, activityInfo.getIconResource(), a2.applicationInfo);
                        }
                    }
                }
            }
        } catch (Throwable th) {
            XLog.e(this.f1933a, th.getMessage());
        }
        return null;
    }

    public String getMainReceiverImpl() {
        return this.m;
    }

    public void setMainReceiverImpl(String str) {
        this.m = str;
    }

    public String getApkRecieverImpl() {
        return this.n;
    }

    public void setApkRecieverImpl(String str) {
        this.n = str;
    }

    public String getAppServiceImpl() {
        return this.r;
    }

    public void setAppServiceImpl(String str) {
        this.r = str;
    }

    public String getIpcServiceImpl() {
        return this.s;
    }

    public void setIpcServiceImpl(String str) {
        this.s = str;
    }

    public String getPluginLibPath() {
        return e.a(this.c);
    }

    public PluginEntry getPluginEntryByStartActivity(String str) {
        if (this.k != null && this.k.size() > 0) {
            for (PluginEntry next : this.k) {
                if (next.getStartActivity() != null && next.getStartActivity().equals(str)) {
                    return next;
                }
            }
        }
        return null;
    }

    /* compiled from: ProGuard */
    public class PluginEntry {

        /* renamed from: a  reason: collision with root package name */
        private String f1934a;
        private Drawable b;
        private String c;
        private PluginInfo d;

        public PluginEntry(PluginInfo pluginInfo, String str, Drawable drawable, String str2) {
            this.d = pluginInfo;
            this.f1934a = str;
            this.b = drawable;
            this.c = str2;
        }

        public String getName() {
            return this.f1934a;
        }

        public void setName(String str) {
            this.f1934a = str;
        }

        public Drawable getIcon() {
            return this.b;
        }

        public void setIcon(Drawable drawable) {
            this.b = drawable;
        }

        public String getStartActivity() {
            return this.c;
        }

        public void setStartActivity(String str) {
            this.c = str;
        }

        public PluginInfo getHostPlugInfo() {
            return this.d;
        }

        public String toString() {
            return "PluginEntry{name='" + this.f1934a + '\'' + ", icon=" + this.b + ", startActivity='" + this.c + '\'' + '}';
        }
    }

    public String getExtendServiceImpl() {
        return e.a(this.v, ":", "|");
    }

    public String getExtendServiceImpl(String str) {
        if (this.v != null) {
            return this.v.get(str);
        }
        return null;
    }

    public String getExtendReceiverImpl(String str) {
        if (this.w != null) {
            return this.w.get(str);
        }
        return null;
    }

    public void setExtendServiceImpl(String str) {
        this.v = e.a(str, ":", "|");
    }

    public synchronized void addExtendServiceImpl(String str, String str2) {
        if (this.v == null) {
            this.v = new HashMap();
        }
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            this.v.put(str, str2);
        }
    }

    public String getExtendReceiverImpl() {
        return e.a(this.w, ":", "|");
    }

    public void setExtendReceiverImpl(String str) {
        this.w = e.a(str, ":", "|");
    }

    public synchronized void addExtendReceiverImpl(String str, String str2) {
        if (this.w == null) {
            this.w = new HashMap();
        }
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            this.w.put(str, str2);
        }
    }

    public String toString() {
        return "PluginInfo{version=" + this.b + ", packageName='" + this.c + '\'' + ", pkgid=" + this.d + ", minBaoVersion=" + this.e + ", minApiLevel=" + this.f + ", minFLevel=" + this.g + ", inProcess=" + this.h + ", launchApplication='" + this.i + '\'' + ", applicationInfo=" + this.j + ", entryList=" + this.k + ", smsReceiverImpl='" + this.l + '\'' + ", mainReceiverImpl='" + this.m + '\'' + ", apkRecieverImpl='" + this.n + '\'' + ", smsSentReceiverImpl='" + this.o + '\'' + ", authorReceiverImpl='" + this.p + '\'' + ", heartBeatReceiverImpl='" + this.q + '\'' + ", AppServiceImpl='" + this.r + '\'' + ", ipcServiceImpl='" + this.s + '\'' + ", dockReceiverImpl='" + this.t + '\'' + ", accelerationServiceImpl='" + this.u + '\'' + ", extendServiceMap=" + this.v + ", extendReceiverMap=" + this.w + '}';
    }
}
