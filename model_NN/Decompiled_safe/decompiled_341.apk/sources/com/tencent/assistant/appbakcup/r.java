package com.tencent.assistant.appbakcup;

import android.os.Message;
import android.util.Log;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.Global;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.module.aw;
import com.tencent.assistant.protocol.jce.BackupDevice;
import com.tencent.assistant.protocol.jce.GetBackupAppsRequest;
import com.tencent.assistant.protocol.jce.GetBackupAppsResponse;
import com.tencent.assistant.utils.t;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class r extends aw implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private int f843a;
    private ArrayList<BackupDevice> b;
    private int c;
    private Object d = new Object();

    public int a() {
        int f;
        Log.d("AppBackupActivity", "requestDeviceList  ");
        g();
        synchronized (this.d) {
            if (this.f843a != -1) {
                cancel(this.f843a);
            }
            f = f();
            this.f843a = f;
        }
        return f;
    }

    private int f() {
        Log.d("AppBackupActivity", "requestDeviceListInner retryCount = " + this.c);
        GetBackupAppsRequest getBackupAppsRequest = new GetBackupAppsRequest();
        getBackupAppsRequest.a(0);
        int send = send(getBackupAppsRequest);
        Log.d("AppBackupActivity", "requestDeviceListInner retryCount = " + this.c + " seq = " + send);
        return send;
    }

    public ArrayList<BackupDevice> b() {
        return this.b;
    }

    public boolean c() {
        if (this.b == null || this.b.isEmpty()) {
            return false;
        }
        return true;
    }

    public boolean d() {
        return e() != null;
    }

    public BackupDevice e() {
        if (!c()) {
            return null;
        }
        Iterator<BackupDevice> it = this.b.iterator();
        while (it.hasNext()) {
            BackupDevice next = it.next();
            if (next.b.equals(t.u()) && next.a().equals(Global.getPhoneGuid())) {
                return next;
            }
        }
        return null;
    }

    private void g() {
        synchronized (this.d) {
            this.c = 0;
        }
    }

    private void h() {
        synchronized (this.d) {
            this.c++;
        }
    }

    private boolean i() {
        boolean z;
        synchronized (this.d) {
            if (this.c < 2) {
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public void handleUIEvent(Message message) {
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        this.b = ((GetBackupAppsResponse) jceStruct2).a();
        synchronized (this.d) {
            Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_SUCCESS);
            obtainMessage.arg1 = this.f843a;
            AstApp.i().j().sendMessage(obtainMessage);
            Log.d("AppBackupActivity", "onRequestSuccessed -- sendMessage --mSeq = " + this.f843a);
            this.f843a = -1;
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z = false;
        Log.d("AppBackupActivity", "onRequestFailed = errorCode = " + i2);
        if (!i() || i2 == -801 || i2 == -800 || i2 == -826) {
            z = true;
        } else {
            f();
            h();
        }
        if (z) {
            if (this.b != null) {
                this.b.clear();
            }
            synchronized (this.d) {
                Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_FAIL);
                obtainMessage.arg1 = this.f843a;
                obtainMessage.arg2 = i2;
                AstApp.i().j().sendMessage(obtainMessage);
                Log.d("AppBackupActivity", "onRequestFailed -- sendMessage --mSeq = " + this.f843a);
                this.f843a = -1;
            }
        }
    }
}
