package com.tencent.assistant.sdk.a;

import android.os.IBinder;
import android.os.Parcel;

/* compiled from: ProGuard */
class f implements d {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f2459a;

    f(IBinder iBinder) {
        this.f2459a = iBinder;
    }

    public IBinder asBinder() {
        return this.f2459a;
    }

    public void a(byte[] bArr) {
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.tencent.assistant.sdk.remote.SDKActionCallback");
            obtain.writeByteArray(bArr);
            this.f2459a.transact(1, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }
}
