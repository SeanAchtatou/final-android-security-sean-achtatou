package com.tencent.assistant.securescan;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.AppStateButton;
import com.tencent.assistant.component.appdetail.HorizonImageListView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.bo;
import com.tencent.assistant.manager.f;
import com.tencent.assistant.manager.g;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.b;
import com.tencent.assistant.module.u;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.installuninstall.p;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.connect.common.Constants;
import com.tencent.securemodule.impl.AppInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class e extends BaseExpandableListAdapter {
    private static int b = 5;

    /* renamed from: a  reason: collision with root package name */
    public Handler f2503a = new f(this);
    private int c = 2000;
    private long d = -100;
    private b e = new b();
    /* access modifiers changed from: private */
    public Context f;
    private LayoutInflater g;
    private List<AppInfo> h = new ArrayList();
    private List<SimpleAppModel> i = new ArrayList();
    private List<String> j = new ArrayList();
    private List<String> k = new ArrayList();
    private Map<String, Integer> l = new HashMap();
    private Map<String, Integer> m = new HashMap();
    private int n = 0;

    public e(Context context, List<AppInfo> list, List<SimpleAppModel> list2, List<SimpleAppModel> list3, View view) {
        this.f = context;
        if (list != null && list.size() > 0) {
            this.h.addAll(list);
        }
        if (list2 != null && list2.size() > 0) {
            this.i.addAll(list2);
        }
        if (list3 != null && list3.size() > 0) {
            this.i.addAll(list3);
        }
        this.g = LayoutInflater.from(context);
    }

    public void a(List<AppInfo> list, List<SimpleAppModel> list2, List<SimpleAppModel> list3) {
        if (list != null && list.size() > 0) {
            this.h.clear();
            this.h.addAll(list);
        }
        this.i.clear();
        if (list2 != null && list2.size() > 0) {
            this.i.addAll(list2);
        }
        if (list3 != null && list3.size() > 0) {
            this.i.addAll(list3);
        }
        a(this.i);
    }

    private void a(List<SimpleAppModel> list) {
        if (list != null) {
            if (this.l == null) {
                this.l = new HashMap();
            }
            if (this.m == null) {
                this.m = new HashMap();
            }
            for (SimpleAppModel next : list) {
                switch (a(next)) {
                    case 100:
                        this.l.put(next.c, 10);
                        break;
                    case TXTabBarLayout.TABITEM_TIPS_TEXT_ID:
                        this.m.put(next.c, 20);
                        break;
                }
            }
        }
    }

    private int a(SimpleAppModel simpleAppModel) {
        AppInfo appInfo;
        AppInfo appInfo2 = new AppInfo();
        if (simpleAppModel == null) {
            return 0;
        }
        Iterator<AppInfo> it = this.h.iterator();
        while (true) {
            if (!it.hasNext()) {
                appInfo = appInfo2;
                break;
            }
            appInfo = it.next();
            if (appInfo != null && appInfo.pkgName.equals(simpleAppModel.c)) {
                break;
            }
        }
        if ((appInfo.isOfficial == 1 || appInfo.isOfficial == 0) && ((simpleAppModel.B >> 2) & 3) == 1) {
            return 100;
        }
        return TXTabBarLayout.TABITEM_TIPS_TEXT_ID;
    }

    public View getChildView(int i2, int i3, boolean z, View view, ViewGroup viewGroup) {
        m mVar;
        if (view == null || view.getTag() == null) {
            view = this.g.inflate((int) R.layout.scan_risk_app_item, (ViewGroup) null);
            m mVar2 = new m();
            mVar2.f2511a = (LinearLayout) view.findViewById(R.id.container_layout);
            mVar2.b = (TXImageView) view.findViewById(R.id.soft_icon_img);
            mVar2.c = (TextView) view.findViewById(R.id.soft_name_txt);
            mVar2.g = (TextView) view.findViewById(R.id.description_txt);
            mVar2.d = (RelativeLayout) view.findViewById(R.id.uninstall_layout);
            mVar2.e = (Button) view.findViewById(R.id.uninstall_btn);
            mVar2.f = (AppStateButton) view.findViewById(R.id.state_app_btn);
            mVar2.h = (ImageView) view.findViewById(R.id.last_line);
            view.setTag(mVar2);
            mVar = mVar2;
        } else {
            mVar = (m) view.getTag();
        }
        a(mVar, mVar.h, i3, this.i.size());
        SimpleAppModel simpleAppModel = this.i.get(i3);
        switch (a(simpleAppModel)) {
            case 100:
                if (simpleAppModel.d != null) {
                    mVar.c.setText(simpleAppModel.d);
                } else {
                    String r = r(simpleAppModel.c);
                    if (r != null) {
                        mVar.c.setText(r);
                        Log.i("RiskAppInfoAdapter", "应用名为null");
                    } else {
                        mVar.c.setText("应用名未知");
                    }
                }
                mVar.c.setText(simpleAppModel.d);
                mVar.g.setText(b(simpleAppModel));
                mVar.g.setTextColor(this.f.getResources().getColor(R.color.secure_scan_result));
                if (!TextUtils.isEmpty(simpleAppModel.e)) {
                    mVar.b.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                } else {
                    mVar.b.updateImageView(simpleAppModel.c, R.drawable.pic_defaule, TXImageView.TXImageViewType.INSTALL_APK_ICON);
                }
                switch (this.l.get(simpleAppModel.c).intValue()) {
                    case 10:
                        mVar.f.setClickable(false);
                        mVar.f.setVisibility(8);
                        mVar.e.setVisibility(0);
                        mVar.e.setText(this.f.getResources().getString(R.string.secure_scan_exchange_offical));
                        mVar.e.setBackgroundResource(R.drawable.btn_green_selector);
                        mVar.e.setTextColor(this.f.getResources().getColor(R.color.white));
                        mVar.d.setClickable(true);
                        mVar.d.setTag(R.id.simpleAppModel, simpleAppModel);
                        mVar.d.setOnClickListener(new i(this, simpleAppModel, i2, i3));
                        break;
                    case 11:
                        mVar.f.setClickable(false);
                        mVar.f.setVisibility(8);
                        mVar.e.setVisibility(0);
                        mVar.e.setBackgroundResource(R.drawable.btn_green_selector);
                        mVar.e.setTextColor(this.f.getResources().getColor(R.color.white));
                        mVar.e.setText(this.f.getResources().getString(R.string.uninstalling));
                        mVar.d.setClickable(false);
                        break;
                    case 12:
                        mVar.e.setVisibility(8);
                        mVar.d.setClickable(false);
                        mVar.f.setSimpleAppModel(simpleAppModel);
                        mVar.f.setClickable(true);
                        mVar.f.setVisibility(0);
                        mVar.f.setOnClickListener(new g(this, simpleAppModel, i3));
                        break;
                    case 13:
                        mVar.g.setText(this.f.getResources().getString(R.string.secure_scan_exchanged_offical));
                        mVar.g.setTextColor(this.f.getResources().getColor(R.color.secure_scan_no_risk_tip));
                        mVar.e.setVisibility(8);
                        mVar.d.setClickable(false);
                        mVar.f.setSimpleAppModel(simpleAppModel);
                        mVar.f.setClickable(true);
                        mVar.f.setVisibility(0);
                        mVar.f.setOnClickListener(new h(this, simpleAppModel, i3));
                        break;
                }
            case TXTabBarLayout.TABITEM_TIPS_TEXT_ID:
                if (simpleAppModel.d != null) {
                    mVar.c.setText(simpleAppModel.d);
                } else {
                    String r2 = r(simpleAppModel.c);
                    if (r2 != null) {
                        mVar.c.setText(r2);
                        Log.i("RiskAppInfoAdapter", "应用名为null");
                    } else {
                        mVar.c.setText("应用名未知");
                    }
                }
                mVar.c.setText(simpleAppModel.d);
                mVar.g.setText(b(simpleAppModel));
                mVar.g.setTextColor(this.f.getResources().getColor(R.color.secure_scan_result));
                mVar.b.updateImageView(simpleAppModel.c, R.drawable.pic_defaule, TXImageView.TXImageViewType.INSTALL_APK_ICON);
                mVar.f.setClickable(false);
                mVar.f.setVisibility(8);
                mVar.e.setVisibility(0);
                switch (this.m.get(simpleAppModel.c).intValue()) {
                    case 20:
                        mVar.e.setText(this.f.getResources().getString(R.string.uninstall));
                        mVar.e.setBackgroundResource(R.drawable.common_small_download_btn_selector);
                        mVar.e.setTextColor(this.f.getResources().getColor(R.color.state_normal));
                        mVar.d.setClickable(true);
                        mVar.d.setTag(R.id.simpleAppModel, simpleAppModel);
                        mVar.d.setOnClickListener(new k(this, simpleAppModel, i2, i3));
                        break;
                    case 21:
                        mVar.e.setText(this.f.getResources().getString(R.string.uninstalling));
                        mVar.e.setBackgroundResource(R.drawable.state_bg_disable);
                        mVar.e.setTextColor(this.f.getResources().getColor(R.color.appadmin_disabled_text));
                        mVar.d.setClickable(false);
                        break;
                }
        }
        if (z) {
            view.setPadding(0, 0, 0, df.b(5.0f));
        } else {
            view.setPadding(0, 0, 0, 0);
        }
        return view;
    }

    public int getChildrenCount(int i2) {
        if (this.i == null) {
            return 0;
        }
        return this.i.size();
    }

    public int getGroupCount() {
        return 1;
    }

    public long getGroupId(int i2) {
        return (long) i2;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int i2, int i3) {
        return false;
    }

    public void a(String str) {
        if (this.l == null) {
            this.l = new HashMap();
        }
        this.l.put(str, 10);
        notifyDataSetChanged();
    }

    public void b(String str) {
        if (this.m == null) {
            this.m = new HashMap();
        }
        this.m.put(str, 20);
        notifyDataSetChanged();
    }

    public void c(String str) {
        if (this.m == null) {
            this.m = new HashMap();
        }
        this.m.put(str, 21);
        notifyDataSetChanged();
    }

    public void d(String str) {
        if (this.l == null) {
            this.l = new HashMap();
        }
        this.l.put(str, 11);
        notifyDataSetChanged();
    }

    public void e(String str) {
        if (this.l == null) {
            this.l = new HashMap();
        }
        this.l.put(str, 12);
        notifyDataSetChanged();
    }

    public void f(String str) {
        if (this.l == null) {
            this.l = new HashMap();
        }
        this.l.put(str, 13);
        notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public String a(int i2, int i3, int i4) {
        if (i4 == 100) {
            return "03_" + ct.a(i3 + 1);
        }
        return "04_" + ct.a(i3 + 1);
    }

    /* access modifiers changed from: private */
    public String a(int i2) {
        return HorizonImageListView.TMA_ST_HORIZON_IMAGE_TAG + ct.a(i2) + "|" + (i2 % b);
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel, int i2, View view) {
        if (simpleAppModel != null) {
            DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel);
            StatInfo buildDownloadSTInfo = STInfoBuilder.buildDownloadSTInfo(this.f, simpleAppModel);
            buildDownloadSTInfo.slotId = a(i2);
            buildDownloadSTInfo.recommendId = simpleAppModel.y;
            if (a2 != null && a2.needReCreateInfo(simpleAppModel)) {
                DownloadProxy.a().b(a2.downloadTicket);
                ((StartScanActivity) this.f).a((Boolean) false);
                a2 = null;
            }
            if (a2 == null) {
                a2 = DownloadInfo.createDownloadInfo(simpleAppModel, buildDownloadSTInfo);
                if ((view instanceof g) && simpleAppModel != null) {
                    f.a().a(simpleAppModel.q(), (g) view);
                }
            } else {
                a2.updateDownloadInfoStatInfo(buildDownloadSTInfo);
            }
            switch (l.f2510a[u.d(simpleAppModel).ordinal()]) {
                case 1:
                case 2:
                    a.a().a(a2);
                    return;
                case 3:
                case 4:
                    a.a().b(a2.downloadTicket);
                    return;
                case 5:
                    a.a().b(a2);
                    return;
                case 6:
                    a.a().d(a2);
                    return;
                case 7:
                    a.a().c(a2);
                    this.n--;
                    g(a2.packageName);
                    return;
                case 8:
                case 9:
                    a.a().a(a2);
                    return;
                case 10:
                    Toast.makeText(this.f, (int) R.string.unsupported, 0).show();
                    return;
                case 11:
                    Toast.makeText(this.f, (int) R.string.tips_slicent_install, 0).show();
                    return;
                case 12:
                    Toast.makeText(this.f, (int) R.string.tips_slicent_uninstall, 0).show();
                    return;
                default:
                    return;
            }
        }
    }

    private String b(SimpleAppModel simpleAppModel) {
        AppInfo appInfo;
        if (simpleAppModel != null) {
            AppInfo appInfo2 = new AppInfo();
            Iterator<AppInfo> it = this.h.iterator();
            while (true) {
                if (!it.hasNext()) {
                    appInfo = appInfo2;
                    break;
                }
                appInfo = it.next();
                if (simpleAppModel.c.equals(appInfo.pkgName)) {
                    break;
                }
            }
            switch (appInfo.safeType) {
                case 2:
                    if ((appInfo.isOfficial == 1 || appInfo.isOfficial == 0) && ((simpleAppModel.B >> 2) & 3) == 1) {
                        return "有安全隐患，换正版保障安全";
                    }
                    return "恶意软件，建议卸载";
                case 3:
                    if ((appInfo.isOfficial == 1 || appInfo.isOfficial == 0) && ((simpleAppModel.B >> 2) & 3) == 1) {
                        return "存在盗号风险，换正版保障安全";
                    }
                    return "有盗号风险，建议卸载";
                case 4:
                    if ((appInfo.isOfficial == 1 || appInfo.isOfficial == 0) && ((simpleAppModel.B >> 2) & 3) == 1) {
                        return "存在支付风险，换正版保障安全";
                    }
                    return "有支付风险，建议卸载";
                case 5:
                    if ((appInfo.isOfficial == 1 || appInfo.isOfficial == 0) && ((simpleAppModel.B >> 2) & 3) == 1) {
                        return "有安全隐患，换正版保障安全";
                    }
                    return "有安全隐患，建议卸载";
            }
        }
        return Constants.STR_EMPTY;
    }

    /* access modifiers changed from: private */
    public String c(SimpleAppModel simpleAppModel) {
        return this.f.getResources().getString(R.string.secure_scan_dialog_content) + simpleAppModel.d + "(" + bt.a(simpleAppModel.k) + ")" + this.f.getResources().getString(R.string.secure_scan_offical);
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel, int i2) {
        if (simpleAppModel != null) {
            switch (i2) {
                case 100:
                    if (simpleAppModel.c != null && this.l.containsKey(simpleAppModel.c) && this.l.get(simpleAppModel.c).intValue() == 11) {
                        return;
                    }
                case TXTabBarLayout.TABITEM_TIPS_TEXT_ID:
                    if (simpleAppModel.c != null && this.m.containsKey(simpleAppModel.c) && this.m.get(simpleAppModel.c).intValue() == 21) {
                        return;
                    }
            }
            if (!this.j.contains(simpleAppModel.c) && i2 == 100) {
                this.j.add(simpleAppModel.c);
            }
            if (!this.k.contains(simpleAppModel.c) && i2 == 100) {
                this.k.add(simpleAppModel.c);
            }
            bo.a().c(simpleAppModel.d);
            bo.a().d(simpleAppModel.c);
            p.a().a(this.f, String.valueOf(simpleAppModel.f1634a), simpleAppModel.c, (long) simpleAppModel.g, simpleAppModel.d);
        }
    }

    public void g(String str) {
        int i2;
        if (str != null) {
            int i3 = 0;
            while (true) {
                i2 = i3;
                if (i2 < this.i.size() && !this.i.get(i2).c.equals(str)) {
                    i3 = i2 + 1;
                }
            }
            if (i2 != this.i.size()) {
                Iterator<SimpleAppModel> it = this.i.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (it.next().c.equals(str)) {
                            it.remove();
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (this.i.size() < 1) {
                    ((StartScanActivity) this.f).j();
                    notifyDataSetChanged();
                    return;
                }
                ((StartScanActivity) this.f).b(b());
                notifyDataSetChanged();
            }
        }
    }

    public Object getChild(int i2, int i3) {
        return this.i.get(i3);
    }

    public long getChildId(int i2, int i3) {
        return (long) i3;
    }

    public Object getGroup(int i2) {
        return this.i;
    }

    public View getGroupView(int i2, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            return new View(this.f);
        }
        return view;
    }

    private void a(m mVar, ImageView imageView, int i2, int i3) {
        if (i3 == 1) {
            imageView.setVisibility(8);
        } else if (i2 == 0) {
            imageView.setVisibility(0);
        } else if (i2 == i3 - 1) {
            imageView.setVisibility(8);
        } else {
            imageView.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public int a() {
        if (this.i != null) {
            return this.i.size();
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void h(String str) {
        for (SimpleAppModel simpleAppModel : this.i) {
            if (simpleAppModel.c.equals(str)) {
                this.n++;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void i(String str) {
        for (SimpleAppModel simpleAppModel : this.i) {
            if (simpleAppModel.c.equals(str)) {
                this.n--;
            }
        }
    }

    /* access modifiers changed from: protected */
    public int b() {
        if (this.i != null) {
            return this.i.size() - this.n;
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public void a(LocalApkInfo localApkInfo) {
        SimpleAppModel simpleAppModel;
        int i2;
        int i3 = -1;
        int i4 = 0;
        SimpleAppModel simpleAppModel2 = null;
        while (i4 < this.i.size()) {
            if (this.i.get(i4).c.equals(localApkInfo.mPackageName)) {
                simpleAppModel = this.i.get(i4);
                i2 = i4;
            } else {
                simpleAppModel = simpleAppModel2;
                i2 = i3;
            }
            i4++;
            i3 = i2;
            simpleAppModel2 = simpleAppModel;
        }
        a(simpleAppModel2, i3, (View) null);
    }

    /* access modifiers changed from: protected */
    public Boolean j(String str) {
        return Boolean.valueOf(this.l.containsKey(str));
    }

    /* access modifiers changed from: protected */
    public int k(String str) {
        return this.l.get(str).intValue();
    }

    /* access modifiers changed from: protected */
    public Boolean l(String str) {
        return Boolean.valueOf(this.j.contains(str));
    }

    /* access modifiers changed from: protected */
    public void m(String str) {
        Iterator<String> it = this.j.iterator();
        while (it.hasNext()) {
            if (it.next().equals(str)) {
                it.remove();
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public Boolean n(String str) {
        return Boolean.valueOf(this.k.contains(str));
    }

    /* access modifiers changed from: protected */
    public void o(String str) {
        Iterator<String> it = this.k.iterator();
        while (it.hasNext()) {
            if (it.next().equals(str)) {
                it.remove();
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public Boolean p(String str) {
        SimpleAppModel simpleAppModel;
        SimpleAppModel simpleAppModel2 = new SimpleAppModel();
        Iterator<SimpleAppModel> it = this.i.iterator();
        while (true) {
            if (!it.hasNext()) {
                simpleAppModel = simpleAppModel2;
                break;
            }
            simpleAppModel = it.next();
            if (simpleAppModel.c.equals(str)) {
                break;
            }
        }
        if (AppConst.AppState.INSTALLED == u.d(simpleAppModel)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void q(String str) {
        SimpleAppModel simpleAppModel;
        Iterator<SimpleAppModel> it = this.i.iterator();
        while (true) {
            if (!it.hasNext()) {
                simpleAppModel = null;
                break;
            }
            simpleAppModel = it.next();
            if (simpleAppModel.c.equals(str)) {
                break;
            }
        }
        DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel);
        if (a2 != null) {
            DownloadProxy.a().b(a2.downloadTicket);
            ((StartScanActivity) this.f).a((Boolean) false);
        }
    }

    public String r(String str) {
        PackageManager packageManager = this.f.getPackageManager();
        try {
            return packageManager.getApplicationInfo(str, 0).loadLabel(packageManager).toString();
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
