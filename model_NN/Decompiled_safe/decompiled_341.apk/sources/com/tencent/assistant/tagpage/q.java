package com.tencent.assistant.tagpage;

import android.media.MediaPlayer;
import android.view.animation.Animation;

/* compiled from: ProGuard */
class q implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MediaPlayer f2561a;
    final /* synthetic */ TagPageCardAdapter b;

    q(TagPageCardAdapter tagPageCardAdapter, MediaPlayer mediaPlayer) {
        this.b = tagPageCardAdapter;
        this.f2561a = mediaPlayer;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f2561a.start();
        this.b.l.i.setVisibility(8);
    }
}
