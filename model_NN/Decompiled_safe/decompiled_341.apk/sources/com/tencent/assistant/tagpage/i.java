package com.tencent.assistant.tagpage;

import android.view.View;
import android.widget.AbsListView;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AbsListView f2553a;
    final /* synthetic */ TagPageActivity b;

    i(TagPageActivity tagPageActivity, AbsListView absListView) {
        this.b = tagPageActivity;
        this.f2553a = absListView;
    }

    public void run() {
        XLog.i("TagPageActivity", "view.getFirstVisiblePosition() = " + this.f2553a.getFirstVisiblePosition() + ", view.getLastVisiblePosition() = " + this.f2553a.getLastVisiblePosition());
        int childCount = this.f2553a.getChildCount();
        int[] iArr = new int[2];
        int c = df.c() / 2;
        for (int i = 0; i < childCount; i++) {
            View childAt = this.f2553a.getChildAt(i);
            if (childAt != null) {
                childAt.getLocationOnScreen(iArr);
                XLog.i("TagPageActivity", "y[" + i + "] = " + iArr[1]);
                if (iArr[1] < c && iArr[1] + childAt.getHeight() > c) {
                    if (childAt.getTag() instanceof r) {
                        r rVar = (r) childAt.getTag();
                        if (TagPageActivity.n != (this.f2553a.getFirstVisiblePosition() + i) - 1) {
                            TagPageActivity.n = (i + this.f2553a.getFirstVisiblePosition()) - 1;
                            if (TagPageActivity.n != TagPageCardAdapter.f2542a) {
                                this.b.B.a(rVar, TagPageActivity.n);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                }
            }
        }
    }
}
