package com.tencent.assistant.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.qq.AppService.AstApp;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.assistant.manager.notification.NotificationService;
import com.tencent.assistant.manager.notification.y;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class DownloadingService extends Service {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2526a = false;
    private static String b = "DownloadingService";

    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent != null) {
            if (!intent.getBooleanExtra("born", true) || f2526a) {
                stopForeground(true);
                stopSelf();
            } else {
                String stringExtra = intent.getStringExtra("title");
                String stringExtra2 = intent.getStringExtra("content");
                String stringExtra3 = intent.getStringExtra("ticker");
                int intExtra = intent.getIntExtra("notification_id", a());
                Intent intent2 = new Intent(this, NotificationService.class);
                intent2.putExtra("notification_id", intExtra);
                Notification a2 = y.a(AstApp.i(), stringExtra, stringExtra2, stringExtra3, PendingIntent.getService(this, a(), intent2, 268435456), false, true);
                if (a2 != null) {
                    try {
                        startForeground(a(), a2);
                    } catch (Throwable th) {
                    }
                }
            }
        }
        return 2;
    }

    public int a() {
        return TXTabBarLayout.TABITEM_TIPS_TEXT_ID;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Context context) {
        f2526a = true;
        Intent intent = new Intent(context, DownloadingService.class);
        intent.putExtra("born", false);
        try {
            context.startService(intent);
        } catch (Exception e) {
            XLog.e(b, e.getMessage());
        }
    }

    public static void b() {
        f2526a = false;
    }
}
