package com.tencent.assistant.floatingwindow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
public class FloatWindowResultView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    public static int f1279a;
    public static int b;
    private View c = findViewById(R.id.rl_result_layout);
    private TextView d = ((TextView) findViewById(R.id.tv_mem_free_tips));
    private TextView e;

    public FloatWindowResultView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate((int) R.layout.float_window_result_layout, this);
        f1279a = (int) context.getResources().getDimension(R.dimen.floating_window_result_view_width);
        b = (int) context.getResources().getDimension(R.dimen.floating_window_result_view_layout_height);
        this.c.startAnimation(a());
    }

    public void a(String str) {
        if (this.d != null) {
            this.d.setText(str);
        }
    }

    public void b(String str) {
        if (str != null) {
            this.e = (TextView) findViewById(R.id.tv_speedup_tips);
            this.e.setVisibility(0);
            this.e.setText(str);
        }
    }

    public void a(Animation.AnimationListener animationListener) {
        this.c.startAnimation(b(animationListener));
    }

    private Animation a() {
        AnimationSet animationSet = new AnimationSet(false);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) ((-df.a(AstApp.i(), 120.0f)) - b), 0.0f);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        translateAnimation.setDuration(1000);
        animationSet.addAnimation(translateAnimation);
        TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0f, 0.0f, 0.0f, -20.0f);
        translateAnimation2.setInterpolator(new CycleInterpolator(2.0f));
        translateAnimation2.setDuration(3000);
        translateAnimation2.setFillAfter(true);
        animationSet.addAnimation(translateAnimation2);
        return animationSet;
    }

    private Animation b(Animation.AnimationListener animationListener) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(800);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        alphaAnimation.setAnimationListener(animationListener);
        return alphaAnimation;
    }
}
