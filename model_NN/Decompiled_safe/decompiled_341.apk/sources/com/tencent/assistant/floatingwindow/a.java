package com.tencent.assistant.floatingwindow;

import android.view.animation.Animation;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.br;

/* compiled from: ProGuard */
class a implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FloatWindowBigView f1287a;

    a(FloatWindowBigView floatWindowBigView) {
        this.f1287a = floatWindowBigView;
    }

    public void onAnimationStart(Animation animation) {
        int unused = this.f1287a.n = (int) (br.d() * 100.0f);
        this.f1287a.k.setValue((double) this.f1287a.n);
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f1287a.l.setText((int) R.string.floating_window_press_to_accelerate_tips);
        this.f1287a.c();
    }
}
