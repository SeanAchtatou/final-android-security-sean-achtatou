package com.tencent.assistant.utils;

import android.content.pm.PackageManager;
import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.aa;
import com.tencent.assistant.localres.callback.b;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.login.PluginLoginIn;
import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.module.callback.g;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.c;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.securemodule.impl.SecureModuleService;
import com.tencent.securemodule.service.ISecureModuleService;
import com.tencent.securemodule.service.ProductInfo;
import com.tencent.tmsecurelite.optimize.f;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class bk {

    /* renamed from: a  reason: collision with root package name */
    public f f2651a = new bn(this);
    /* access modifiers changed from: private */
    public String b = "ManagerEngine";
    /* access modifiers changed from: private */
    public long c = 0;
    private bq d;
    private aa e = null;
    private ISecureModuleService f;
    /* access modifiers changed from: private */
    public ArrayList<String> g = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public g j = new bm(this);
    private b k = new bp(this);

    static /* synthetic */ long a(bk bkVar, long j2) {
        long j3 = bkVar.c + j2;
        bkVar.c = j3;
        return j3;
    }

    public void a() {
        PluginInfo a2 = d.b().a("com.assistant.accelerate");
        if (a2 != null) {
            try {
                Class<?> loadClass = c.a(AstApp.i().getApplicationContext(), a2).loadClass("com.assistant.accelerate.PluginAccelerateEntry");
                Object newInstance = loadClass.newInstance();
                ba.a().post(new bl(this, loadClass.getDeclaredMethod("execAccelerate", Boolean.TYPE), newInstance));
            } catch (Exception e2) {
                e2.printStackTrace();
                AstApp.i().j().sendMessage(PluginLoginIn.getEventDispatcher().obtainMessage(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL));
            }
        } else {
            AstApp.i().j().sendMessage(PluginLoginIn.getEventDispatcher().obtainMessage(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL));
        }
    }

    public void b() {
        this.g.clear();
        if (!SpaceScanManager.a().e()) {
            SpaceScanManager.a().a(this.j);
            SpaceScanManager.a().c();
            return;
        }
        this.i = true;
        XLog.i(this.b, ">>111start space scan>>");
        SpaceScanManager.a().a(this.f2651a);
    }

    public boolean c() {
        return this.i;
    }

    public void d() {
        this.h = true;
        b();
    }

    public void e() {
        if (this.e == null) {
            this.e = ApkResourceManager.getInstance().getLocalApkLoader();
            this.e.a(this.k);
        }
        this.e.c();
        this.e.a();
    }

    /* access modifiers changed from: private */
    public void a(List<LocalApkInfo> list) {
        if (list != null && !list.isEmpty()) {
            int i2 = 0;
            for (int i3 = 0; i3 < list.size(); i3++) {
                LocalApkInfo localApkInfo = list.get(i3);
                if (localApkInfo != null && localApkInfo.mIsSelect) {
                    i2++;
                }
            }
            XLog.i(this.b, "size=" + list.size() + " select.size=" + i2);
            Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_MGR_APK_DEL_SUCCESS);
            obtainMessage.obj = Integer.valueOf(i2);
            AstApp.i().j().dispatchMessage(obtainMessage);
        }
    }

    public void f() {
        String str;
        if (this.f == null) {
            this.f = SecureModuleService.getInstance(AstApp.i().getApplicationContext());
            this.d = new bq(this, null);
        }
        String channelId = Global.getChannelId();
        int parseInt = Integer.parseInt(Global.getBuildNo());
        try {
            str = AstApp.i().getPackageManager().getPackageInfo(AstApp.i().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            str = null;
        }
        XLog.i(this.b, "channelId:" + channelId + " versionName:" + str + " buildNo:" + parseInt);
        if (this.f.register(new ProductInfo(41, str, parseInt, 0, channelId, null)) == 0) {
            this.f.registerCloudScanListener(AstApp.i().getApplicationContext(), this.d);
            this.f.setNotificationUIEnable(false);
            this.f.cloudScan();
            XLog.i(this.b, "start safe Scan");
        }
    }

    public void g() {
        if (this.e != null) {
            this.e.c();
            this.e.b(this.k);
        }
        try {
            if (this.f != null) {
                this.f.unregisterCloudScanListener(AstApp.i().getApplicationContext(), this.d);
            }
        } catch (Exception e2) {
        }
        SpaceScanManager.a().h();
        SpaceScanManager.a().b(this.j);
    }
}
