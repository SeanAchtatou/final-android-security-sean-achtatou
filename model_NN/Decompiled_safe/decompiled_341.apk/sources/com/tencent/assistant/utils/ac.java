package com.tencent.assistant.utils;

import android.app.Dialog;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.ShareQzView;

/* compiled from: ProGuard */
final class ac implements ShareQzView.ICallback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f2625a;
    final /* synthetic */ Dialog b;

    ac(AppConst.TwoBtnDialogInfo twoBtnDialogInfo, Dialog dialog) {
        this.f2625a = twoBtnDialogInfo;
        this.b = dialog;
    }

    public void onSubmitClick() {
        if (this.f2625a != null) {
            this.f2625a.onRightBtnClick();
            this.b.dismiss();
        }
    }

    public void onCancelClick() {
        if (this.f2625a != null) {
            this.f2625a.onLeftBtnClick();
            this.b.dismiss();
        }
    }
}
