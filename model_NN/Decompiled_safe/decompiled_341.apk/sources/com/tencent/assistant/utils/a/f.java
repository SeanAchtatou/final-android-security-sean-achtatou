package com.tencent.assistant.utils.a;

import java.io.DataInputStream;
import java.io.DataOutputStream;

/* compiled from: ProGuard */
public class f {

    /* renamed from: a  reason: collision with root package name */
    short f2618a;
    short b;
    short c;
    short d;
    short e;
    short f;
    int g;
    int h;
    int i;
    short j;
    short k;
    short l;
    short m;
    short n;
    short o;
    int p;
    int q;
    boolean r;
    byte[] s;
    byte[] t;
    byte[] u;
    byte[] v;

    public void a(DataOutputStream dataOutputStream) {
        dataOutputStream.writeInt(1347092738);
        dataOutputStream.writeShort(e.a(this.f2618a));
        dataOutputStream.writeShort(e.a(this.b));
        dataOutputStream.writeShort(e.a(this.c));
        dataOutputStream.writeShort(e.a(this.d));
        dataOutputStream.writeShort(e.a(this.e));
        dataOutputStream.writeShort(e.a(this.f));
        dataOutputStream.writeInt(e.a(this.g));
        dataOutputStream.writeInt(e.a(this.h));
        dataOutputStream.writeInt(e.a(this.i));
        dataOutputStream.writeShort(e.a(this.j));
        dataOutputStream.writeShort(e.a(this.l));
        dataOutputStream.writeShort(e.a(this.m));
        dataOutputStream.writeShort(e.a(this.n));
        dataOutputStream.writeShort(e.a(this.o));
        dataOutputStream.writeInt(e.a(this.p));
        dataOutputStream.writeInt(e.a(this.q));
        if (this.j > 0) {
            dataOutputStream.write(this.s);
        }
        if (this.l > 0) {
            dataOutputStream.write(this.u);
        }
        if (this.m > 0) {
            dataOutputStream.write(this.v);
        }
    }

    public void a(DataInputStream dataInputStream) {
        this.f2618a = e.a(dataInputStream.readShort());
        this.b = e.a(dataInputStream.readShort());
        this.c = e.a(dataInputStream.readShort());
        this.d = e.a(dataInputStream.readShort());
        this.e = e.a(dataInputStream.readShort());
        this.f = e.a(dataInputStream.readShort());
        this.g = e.a(dataInputStream.readInt());
        this.h = e.a(dataInputStream.readInt());
        this.i = e.a(dataInputStream.readInt());
        this.j = e.a(dataInputStream.readShort());
        this.l = e.a(dataInputStream.readShort());
        this.m = e.a(dataInputStream.readShort());
        this.n = e.a(dataInputStream.readShort());
        this.o = e.a(dataInputStream.readShort());
        this.p = e.a(dataInputStream.readInt());
        this.q = e.a(dataInputStream.readInt());
        this.r = false;
        this.s = new byte[this.j];
        this.u = new byte[this.l];
        this.v = new byte[this.m];
        dataInputStream.read(this.s, 0, this.j);
        dataInputStream.read(this.u, 0, this.l);
        dataInputStream.read(this.v, 0, this.m);
    }

    public void b(DataInputStream dataInputStream) {
        this.f2618a = dataInputStream.readShort();
        this.b = dataInputStream.readShort();
        this.c = dataInputStream.readShort();
        this.d = dataInputStream.readShort();
        this.e = dataInputStream.readShort();
        this.f = dataInputStream.readShort();
        this.g = dataInputStream.readInt();
        this.h = dataInputStream.readInt();
        this.i = dataInputStream.readInt();
        this.j = dataInputStream.readShort();
        this.k = dataInputStream.readShort();
        this.l = dataInputStream.readShort();
        this.m = dataInputStream.readShort();
        this.n = dataInputStream.readShort();
        this.o = dataInputStream.readShort();
        this.p = dataInputStream.readInt();
        this.q = dataInputStream.readInt();
        this.r = dataInputStream.readBoolean();
        this.s = new byte[this.j];
        this.t = new byte[this.k];
        this.u = new byte[this.l];
        this.v = new byte[this.m];
        dataInputStream.read(this.s, 0, this.j);
        dataInputStream.read(this.t, 0, this.k);
        dataInputStream.read(this.u, 0, this.l);
        dataInputStream.read(this.v, 0, this.m);
    }

    public boolean a() {
        if ((this.c & 8) != 0) {
            return true;
        }
        return false;
    }
}
