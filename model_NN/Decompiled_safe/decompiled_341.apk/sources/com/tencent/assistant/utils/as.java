package com.tencent.assistant.utils;

import android.app.Dialog;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;

/* compiled from: ProGuard */
final class as implements Runnable {
    as() {
    }

    public void run() {
        BaseActivity m = AstApp.m();
        if (m != null && AstApp.i().l()) {
            if (FunctionUtils.f2612a == null || !FunctionUtils.f2612a.isShowing()) {
                at atVar = new at(this);
                atVar.titleRes = m.getString(R.string.down_url_error_dialog_title);
                atVar.contentRes = m.getString(R.string.down_url_error_dialog_message);
                atVar.btnTxtRes = m.getString(R.string.ver_low_tips_ok);
                Dialog unused = FunctionUtils.f2612a = v.a(atVar);
            }
        }
    }
}
