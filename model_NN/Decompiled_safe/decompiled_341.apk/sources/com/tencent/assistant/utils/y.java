package com.tencent.assistant.utils;

import android.content.DialogInterface;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class y implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.OneBtnDialogInfo f2728a;

    y(AppConst.OneBtnDialogInfo oneBtnDialogInfo) {
        this.f2728a = oneBtnDialogInfo;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f2728a.onCancell();
    }
}
