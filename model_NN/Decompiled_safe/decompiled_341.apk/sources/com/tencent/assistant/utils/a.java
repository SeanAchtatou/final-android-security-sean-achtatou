package com.tencent.assistant.utils;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class a {
    public static boolean a(ImageView imageView) {
        return false;
    }

    public static void a(ViewGroup viewGroup, View view, View view2) {
        Animation loadAnimation = AnimationUtils.loadAnimation(view.getContext(), R.anim.app_detail_share_dismiss_anim);
        viewGroup.addView(view2);
        view2.setVisibility(4);
        view2.startAnimation(loadAnimation);
    }
}
