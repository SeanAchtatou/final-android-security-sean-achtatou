package com.tencent.assistant.utils;

import android.os.Build;
import android.os.Looper;
import android.os.Process;
import android.os.SystemProperties;
import com.tencent.jni.YYBNDK;
import java.io.File;

/* compiled from: ProGuard */
public final class ci {

    /* renamed from: a  reason: collision with root package name */
    public static final String[] f2668a = {"/system/bin/su", "/system/xbin/su", "/system/sbin/su", "/vendor/bin/su", "/sbin/su"};

    public static int a(boolean z) {
        int i;
        int i2 = 0;
        while (true) {
            if (i2 >= f2668a.length) {
                i = -1;
                break;
            } else if (a(f2668a[i2])) {
                i = i2 + 1;
                break;
            } else {
                i2++;
            }
        }
        if (i == -1) {
            i = 0;
        }
        if (!z) {
            return i;
        }
        if (a()) {
            i |= Process.PROC_COMBINE;
        }
        if (Looper.getMainLooper().getThread().getId() != Thread.currentThread().getId() && b()) {
            return i | Process.PROC_PARENS;
        }
        return i;
    }

    public static boolean a() {
        if (!SystemProperties.getBoolean("ro.secure", true)) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x001e A[SYNTHETIC, Splitter:B:13:0x001e] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0026 A[SYNTHETIC, Splitter:B:19:0x0026] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean b() {
        /*
            r0 = 0
            r2 = 0
            android.net.LocalSocket r1 = new android.net.LocalSocket     // Catch:{ Throwable -> 0x0022, all -> 0x001a }
            r1.<init>()     // Catch:{ Throwable -> 0x0022, all -> 0x001a }
            android.net.LocalSocketAddress r2 = new android.net.LocalSocketAddress     // Catch:{ Throwable -> 0x0038, all -> 0x0036 }
            java.lang.String r3 = "aurora_root"
            android.net.LocalSocketAddress$Namespace r4 = android.net.LocalSocketAddress.Namespace.ABSTRACT     // Catch:{ Throwable -> 0x0038, all -> 0x0036 }
            r2.<init>(r3, r4)     // Catch:{ Throwable -> 0x0038, all -> 0x0036 }
            r1.connect(r2)     // Catch:{ Throwable -> 0x0038, all -> 0x0036 }
            r0 = 1
            if (r1 == 0) goto L_0x0019
            r1.close()     // Catch:{ IOException -> 0x0034 }
        L_0x0019:
            return r0
        L_0x001a:
            r0 = move-exception
            r1 = r2
        L_0x001c:
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ IOException -> 0x002f }
        L_0x0021:
            throw r0
        L_0x0022:
            r1 = move-exception
            r1 = r2
        L_0x0024:
            if (r1 == 0) goto L_0x0019
            r1.close()     // Catch:{ IOException -> 0x002a }
            goto L_0x0019
        L_0x002a:
            r1 = move-exception
        L_0x002b:
            r1.printStackTrace()
            goto L_0x0019
        L_0x002f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0021
        L_0x0034:
            r1 = move-exception
            goto L_0x002b
        L_0x0036:
            r0 = move-exception
            goto L_0x001c
        L_0x0038:
            r2 = move-exception
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.ci.b():boolean");
    }

    public static boolean a(String str) {
        if (str == null) {
            return false;
        }
        try {
            YYBNDK yybndk = new YYBNDK();
            String realLinkFile = yybndk.getRealLinkFile(str);
            if (realLinkFile != null) {
                str = realLinkFile;
            }
            File file = new File(str);
            if (!file.exists() || !file.canRead()) {
                return false;
            }
            int filePermission = yybndk.getFilePermission(str);
            if ((filePermission >= 4000 || file.length() == 113032 || file.length() == 113036 || file.length() == 30880 || file.length() == 311872 || Build.VERSION.SDK_INT >= 17) && filePermission % 2 != 0 && yybndk.getUid(str) == 0) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }
}
