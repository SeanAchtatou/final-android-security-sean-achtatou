package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.f;
import com.tencent.assistant.protocol.jce.GetAuthorAppsRequest;
import com.tencent.assistant.protocol.jce.GetAuthorAppsResponse;

/* compiled from: ProGuard */
public class cq extends BaseEngine<f> {

    /* renamed from: a  reason: collision with root package name */
    public int f1758a = 4;
    /* access modifiers changed from: private */
    public byte[] b = new byte[0];

    public int a(String str) {
        GetAuthorAppsRequest getAuthorAppsRequest = new GetAuthorAppsRequest();
        getAuthorAppsRequest.b = this.b;
        getAuthorAppsRequest.c = this.f1758a;
        getAuthorAppsRequest.e = str;
        return send(getAuthorAppsRequest);
    }

    public int a(String str, byte[] bArr) {
        GetAuthorAppsRequest getAuthorAppsRequest = new GetAuthorAppsRequest();
        getAuthorAppsRequest.b = bArr;
        getAuthorAppsRequest.c = this.f1758a;
        getAuthorAppsRequest.e = str;
        return send(getAuthorAppsRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z;
        if (jceStruct2 != null) {
            GetAuthorAppsRequest getAuthorAppsRequest = (GetAuthorAppsRequest) jceStruct;
            GetAuthorAppsResponse getAuthorAppsResponse = (GetAuthorAppsResponse) jceStruct2;
            this.b = getAuthorAppsResponse.b();
            boolean z2 = getAuthorAppsRequest.b == null || getAuthorAppsRequest.b.length == 0;
            if (getAuthorAppsResponse.d == 1) {
                z = true;
            } else {
                z = false;
            }
            notifyDataChangedInMainThread(new cr(this, i, z2, u.b(getAuthorAppsResponse.a()), z));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        GetAuthorAppsRequest getAuthorAppsRequest = (GetAuthorAppsRequest) jceStruct;
        notifyDataChangedInMainThread(new cs(this, i, i2, getAuthorAppsRequest.b == null || getAuthorAppsRequest.b.length == 0));
    }
}
