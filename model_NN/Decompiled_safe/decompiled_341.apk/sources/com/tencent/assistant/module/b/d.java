package com.tencent.assistant.module.b;

import com.tencent.assistant.db.a.a;
import com.tencent.assistant.protocol.jce.UploadControlRequest;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f1712a;

    d(b bVar) {
        this.f1712a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.b.b.a(com.tencent.assistant.module.b.b, boolean):boolean
     arg types: [com.tencent.assistant.module.b.b, int]
     candidates:
      com.tencent.assistant.module.b.b.a(com.tencent.assistant.module.b.b, int):int
      com.tencent.assistant.module.b.b.a(com.tencent.assistant.module.b.b, com.qq.taf.jce.JceStruct):int
      com.tencent.assistant.module.b.b.a(com.tencent.assistant.db.a.a, boolean):void
      com.tencent.assistant.module.b.b.a(com.tencent.assistant.module.b.b, com.tencent.assistant.db.a.a):void
      com.tencent.assistant.module.b.b.a(com.tencent.assistant.module.b.b, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.b.b.a(com.tencent.assistant.module.b.b, com.tencent.assistant.db.a.a, boolean):void
     arg types: [com.tencent.assistant.module.b.b, com.tencent.assistant.db.a.a, int]
     candidates:
      com.tencent.assistant.module.b.b.a(java.lang.String, java.lang.String, java.util.List<java.lang.String>):boolean
      com.tencent.assistant.module.b.b.a(com.tencent.assistant.module.b.b, com.tencent.assistant.db.a.a, boolean):void */
    public void run() {
        if (this.f1712a.c) {
            boolean unused = this.f1712a.d = false;
            return;
        }
        UploadControlRequest uploadControlRequest = new UploadControlRequest();
        a unused2 = this.f1712a.f = this.f1712a.f();
        if (this.f1712a.f == null) {
            boolean unused3 = this.f1712a.d = false;
        } else if (this.f1712a.c(this.f1712a.f)) {
            this.f1712a.a(this.f1712a.f, true);
            uploadControlRequest.c = this.f1712a.f.c;
            uploadControlRequest.b = this.f1712a.f.b;
            uploadControlRequest.i = this.f1712a.f.m;
            uploadControlRequest.n = this.f1712a.f.n != null ? this.f1712a.f.n : Constants.STR_EMPTY;
            uploadControlRequest.l = this.f1712a.f.k != null ? this.f1712a.f.k : Constants.STR_EMPTY;
            uploadControlRequest.j = this.f1712a.f.g != null ? this.f1712a.f.g : Constants.STR_EMPTY;
            uploadControlRequest.d = this.f1712a.f.i;
            uploadControlRequest.h = this.f1712a.f.f;
            uploadControlRequest.f = this.f1712a.f.d != null ? this.f1712a.f.d : Constants.STR_EMPTY;
            uploadControlRequest.m = this.f1712a.f.l != null ? this.f1712a.f.l : Constants.STR_EMPTY;
            uploadControlRequest.k = this.f1712a.f.h != null ? this.f1712a.f.h : Constants.STR_EMPTY;
            uploadControlRequest.e = this.f1712a.f.j;
            uploadControlRequest.f2412a = this.f1712a.f.f1227a;
            uploadControlRequest.g = this.f1712a.f.e;
            uploadControlRequest.o = b.a(this.f1712a.f.b);
            int unused4 = this.f1712a.e = this.f1712a.send(uploadControlRequest);
        } else {
            if (this.f1712a.f != null && !FileUtil.isFileExists(this.f1712a.f.g) && !FileUtil.isFileExists(this.f1712a.f.h)) {
                this.f1712a.b(this.f1712a.f);
            }
            boolean unused5 = this.f1712a.d = false;
        }
    }
}
