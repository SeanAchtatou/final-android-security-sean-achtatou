package com.tencent.assistant.module;

import com.tencent.assistant.manager.as;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.GetRecommendTabPageResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
class at implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ as f1704a;

    at(as asVar) {
        this.f1704a = asVar;
    }

    public void run() {
        boolean z = true;
        GetRecommendTabPageResponse b = as.w().b(this.f1704a.c);
        if (b == null || b.e != this.f1704a.f1703a.b || b.a() == null || b.a().size() <= 0) {
            int unused = this.f1704a.h = this.f1704a.f1703a.a(this.f1704a.h, this.f1704a.c);
            return;
        }
        as asVar = this.f1704a;
        long j = b.e;
        ArrayList<SimpleAppModel> b2 = u.b(b.a());
        if (b.f != 1) {
            z = false;
        }
        asVar.a(j, b2, z, b.c());
    }
}
