package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.am;
import com.tencent.assistant.protocol.jce.SyncVerifyRstRequest;
import com.tencent.assistant.protocol.jce.SyncVerifyRstResponse;
import com.tencent.assistant.protocol.jce.VerifyInfo;

/* compiled from: ProGuard */
public class fy extends BaseEngine<am> {
    public int a(VerifyInfo verifyInfo) {
        if (verifyInfo == null) {
            return -1;
        }
        SyncVerifyRstRequest syncVerifyRstRequest = new SyncVerifyRstRequest();
        syncVerifyRstRequest.f2387a = verifyInfo;
        return send(syncVerifyRstRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChanged(new fz(this, i, ((SyncVerifyRstResponse) jceStruct2).f2388a));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChanged(new ga(this, i, i2));
    }
}
