package com.tencent.assistant.module.timer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.SystemClock;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.backgroundscan.BackgroundScanTimerJob;
import com.tencent.assistant.module.update.aa;
import com.tencent.assistant.utils.XLog;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/* compiled from: ProGuard */
public abstract class BaseTimePointJob implements TimePointJob {
    /* access modifiers changed from: protected */
    public abstract void d();

    public final int b() {
        return getClass().getSimpleName().hashCode();
    }

    private int g() {
        return b();
    }

    public boolean c_() {
        return true;
    }

    public final void d_() {
        d();
        SystemClock.sleep(100);
        h();
    }

    public void e() {
        h();
    }

    public void f() {
        Intent intent = new Intent("com.tencent.android.qqdownloader.action.SCHEDULE_JOB");
        intent.putExtra("com.tencent.android.qqdownloader.key.SCHEDULE_JOB", getClass().getName());
        ((AlarmManager) AstApp.i().getSystemService("alarm")).cancel(PendingIntent.getBroadcast(AstApp.i(), g(), intent, 268435456));
    }

    private long a(long j) {
        int i;
        boolean z = false;
        int[] b_ = b_();
        if (b_ == null) {
            return -1;
        }
        int i2 = b_[0];
        int length = b_.length;
        int i3 = 0;
        while (true) {
            if (i3 >= length) {
                z = true;
                i = i2;
                break;
            }
            int i4 = b_[i3];
            if (i4 > 0 && aa.a(j) < i4) {
                i = i4;
                break;
            }
            i3++;
        }
        Calendar c = aa.c(i);
        if (z) {
            c.set(6, c.get(6) + 1);
        }
        return c.getTimeInMillis();
    }

    private void h() {
        long a2 = a(System.currentTimeMillis());
        if (a2 > 0) {
            if (Global.isDev() && getClass().getSimpleName().equals(BackgroundScanTimerJob.class.getSimpleName())) {
                XLog.d("BackgroundScan", "<timer> Background scan will be triggered at : " + new SimpleDateFormat("yyyy年M月d日 HH:mm", Locale.getDefault()).format(new Date(a2)));
            }
            Intent intent = new Intent("com.tencent.android.qqdownloader.action.SCHEDULE_JOB");
            intent.putExtra("com.tencent.android.qqdownloader.key.SCHEDULE_JOB", getClass().getName());
            ((AlarmManager) AstApp.i().getSystemService("alarm")).set(0, a2, PendingIntent.getBroadcast(AstApp.i(), g(), intent, 268435456));
        }
    }
}
