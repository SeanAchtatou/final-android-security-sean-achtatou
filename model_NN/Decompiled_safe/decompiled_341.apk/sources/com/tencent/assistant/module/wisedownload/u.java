package com.tencent.assistant.module.wisedownload;

import android.text.TextUtils;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.protocol.jce.AutoDownloadCfg;
import com.tencent.assistant.protocol.jce.AutoDownloadItemCfg;
import com.tencent.assistant.protocol.jce.DownloadSubscriptionInfo;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class u {
    public static List<DownloadInfo> a(boolean z, boolean z2) {
        ArrayList arrayList = new ArrayList();
        ArrayList<DownloadInfo> d = DownloadProxy.a().d();
        if (d == null || d.isEmpty()) {
            return null;
        }
        Iterator<DownloadInfo> it = d.iterator();
        while (it.hasNext()) {
            DownloadInfo next = it.next();
            if (next.uiType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD) {
                arrayList.add(next);
            }
        }
        if (z) {
            if (arrayList.isEmpty()) {
                return null;
            }
            d.clear();
            d.addAll(arrayList);
            arrayList.clear();
            Iterator<DownloadInfo> it2 = d.iterator();
            while (it2.hasNext()) {
                DownloadInfo next2 = it2.next();
                if (next2.downloadState == SimpleDownloadInfo.DownloadState.SUCC) {
                    arrayList.add(next2);
                }
            }
        }
        if (z2) {
            if (arrayList.isEmpty()) {
                return null;
            }
            d.clear();
            d.addAll(arrayList);
            arrayList.clear();
            Iterator<DownloadInfo> it3 = d.iterator();
            while (it3.hasNext()) {
                DownloadInfo next3 = it3.next();
                String filePath = next3.getFilePath();
                if (!TextUtils.isEmpty(filePath) && new File(filePath).exists()) {
                    arrayList.add(next3);
                }
            }
        }
        return arrayList;
    }

    public static boolean a(DownloadInfo downloadInfo) {
        DownloadSubscriptionInfo a2;
        if (downloadInfo == null || downloadInfo.uiType != SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD || (a2 = com.tencent.assistant.module.update.u.a().a(downloadInfo.uiType, downloadInfo.packageName, downloadInfo.versionCode)) == null || a2.e == 0) {
            return false;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>):void
      com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void */
    public static void a() {
        List<DownloadInfo> a2 = a(true, true);
        if (a2 != null && a2.size() > 0) {
            Collections.sort(a2, new v());
        }
        AutoDownloadItemCfg d = d();
        int i = d != null ? d.d : 3;
        if (a2 != null && a2.size() > i) {
            while (true) {
                int i2 = i;
                if (i2 < a2.size()) {
                    DownloadInfo downloadInfo = a2.get(i2);
                    if (downloadInfo != null) {
                        DownloadProxy.a().b(downloadInfo.downloadTicket, true);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>):void
      com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void */
    public static void b() {
        List<DownloadInfo> a2 = a(false);
        if (a2 != null && !a2.isEmpty()) {
            for (DownloadInfo next : a2) {
                if (next != null) {
                    DownloadProxy.a().b(next.downloadTicket, true);
                }
            }
        }
    }

    public static List<DownloadInfo> a(boolean z) {
        ArrayList arrayList = new ArrayList();
        List<DownloadInfo> a2 = a(true, true);
        if (a2 == null || a2.isEmpty()) {
            return null;
        }
        long c = c();
        for (DownloadInfo next : a2) {
            if (next != null) {
                if (z) {
                    if (next.downloadEndTime < c) {
                    }
                } else if (next.downloadEndTime >= c) {
                }
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public static long c() {
        AutoDownloadItemCfg d = d();
        int i = d != null ? d.c : 3;
        long currentTimeMillis = System.currentTimeMillis();
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(currentTimeMillis);
        instance.set(6, instance.get(6) - i);
        return instance.getTimeInMillis();
    }

    public static AutoDownloadItemCfg d() {
        AutoDownloadCfg i = as.w().i();
        if (i == null || i.n == null) {
            return null;
        }
        return i.n.get(4);
    }
}
