package com.tencent.assistant.module;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.model.HotwordsModel;
import com.tencent.assistant.module.callback.ab;
import com.tencent.assistant.protocol.jce.AdvancedHotWord;
import com.tencent.assistant.protocol.jce.ExplicitHotWord;
import com.tencent.assistant.protocol.jce.GetExplicitHotWordsRequest;
import com.tencent.assistant.protocol.jce.GetExplicitHotWordsResponse;
import com.tencent.assistant.protocol.jce.SearchAdvancedHotWordsRequest;
import com.tencent.assistant.protocol.jce.SearchAdvancedHotWordsResponse;
import com.tencent.assistant.protocol.scu.RequestResponePair;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.model.b.a;
import com.tencent.assistantv2.model.b.b;
import com.tencent.assistantv2.model.b.c;
import com.tencent.assistantv2.model.b.e;
import com.tencent.assistantv2.model.b.f;
import com.tencent.assistantv2.model.b.g;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class aa extends BaseEngine<ab> {
    private static aa j = null;

    /* renamed from: a  reason: collision with root package name */
    private final String f1685a = "AppSearchHotWordsEngine";
    private List<HotwordsModel> b = new ArrayList();
    private int c = 0;
    private String d = Constants.STR_EMPTY;
    private int e = 0;
    /* access modifiers changed from: private */
    public long f = -1;
    /* access modifiers changed from: private */
    public long g = -1;
    private long h = 0;
    private ae i = new ae(this, null);
    private b k = new b();
    private g l = new g();
    private GetExplicitHotWordsResponse m = null;
    private SearchAdvancedHotWordsResponse n = null;
    private ExplicitHotWord o = null;
    private ApkResCallback.Stub p = new ab(this);

    /* access modifiers changed from: private */
    public boolean a(long j2, long j3) {
        if (j2 != -1 && this.g != -1 && j2 != this.g) {
            return true;
        }
        if (j3 == -1 || this.f == -1 || j3 == this.f) {
            return false;
        }
        return true;
    }

    private aa() {
        dy.a().a(this.i);
        ApkResourceManager.getInstance().registerApkResCallback(this.p);
    }

    public static synchronized aa a() {
        aa aaVar;
        synchronized (aa.class) {
            if (j == null) {
                j = new aa();
            }
            aaVar = j;
        }
        return aaVar;
    }

    public void b() {
        if (this.e > 0) {
            cancel(this.e);
        }
        TemporaryThreadManager.get().start(new ac(this));
    }

    /* access modifiers changed from: private */
    public void e() {
        GetExplicitHotWordsResponse f2 = as.w().f();
        SearchAdvancedHotWordsResponse e2 = as.w().e();
        long a2 = m.a().a((byte) 14);
        long a3 = m.a().a((byte) JceStruct.SIMPLE_LIST);
        XLog.i("AppSearchHotWordsEngine", "******************* loadCache, version=" + this.g + ":" + this.f + " localVersion=" + a2 + ":" + a3 + " validTime=" + this.h);
        if (!b(f2, e2)) {
            b(a2, a3);
            return;
        }
        XLog.i("AppSearchHotWordsEngine", "load cache, response is valid");
        this.g = f2.b;
        this.f = e2.i;
        this.h = e2.g;
        this.d = e2.h;
        if (a(a2, a3)) {
            XLog.i("AppSearchHotWordsEngine", "load cache, version is invalid");
            b(a2, a2);
        } else if (System.currentTimeMillis() + m.a().A() > this.h) {
            b(a2, a3);
        } else {
            a(f2, e2);
            a(e2);
            a(this.k, this.l, 0);
            this.m = f2;
            this.n = e2;
        }
    }

    /* access modifiers changed from: private */
    public int b(long j2, long j3) {
        XLog.i("AppSearchHotWordsEngine", "********** gethotwords sendRequest");
        ArrayList arrayList = new ArrayList();
        GetExplicitHotWordsRequest getExplicitHotWordsRequest = new GetExplicitHotWordsRequest();
        getExplicitHotWordsRequest.b = j2;
        arrayList.add(getExplicitHotWordsRequest);
        SearchAdvancedHotWordsRequest searchAdvancedHotWordsRequest = new SearchAdvancedHotWordsRequest();
        searchAdvancedHotWordsRequest.b = j3;
        arrayList.add(searchAdvancedHotWordsRequest);
        this.e = send(arrayList);
        return this.e;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.i("AppSearchHotWordsEngine", "onRequestSuccessed single success");
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.i("AppSearchHotWordsEngine", "onRequestFailed single ");
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, List<RequestResponePair> list) {
        SearchAdvancedHotWordsResponse searchAdvancedHotWordsResponse;
        GetExplicitHotWordsResponse getExplicitHotWordsResponse;
        XLog.i("AppSearchHotWordsEngine", "onRequestSuccessed List ");
        if (list != null && this.e == i2) {
            SearchAdvancedHotWordsResponse searchAdvancedHotWordsResponse2 = null;
            GetExplicitHotWordsResponse getExplicitHotWordsResponse2 = null;
            for (RequestResponePair next : list) {
                if (next != null) {
                    if (next.request instanceof GetExplicitHotWordsRequest) {
                        GetExplicitHotWordsRequest getExplicitHotWordsRequest = (GetExplicitHotWordsRequest) next.request;
                        getExplicitHotWordsResponse = (GetExplicitHotWordsResponse) next.response;
                        searchAdvancedHotWordsResponse = searchAdvancedHotWordsResponse2;
                    } else if (next.request instanceof SearchAdvancedHotWordsRequest) {
                        SearchAdvancedHotWordsRequest searchAdvancedHotWordsRequest = (SearchAdvancedHotWordsRequest) next.request;
                        searchAdvancedHotWordsResponse = (SearchAdvancedHotWordsResponse) next.response;
                        getExplicitHotWordsResponse = getExplicitHotWordsResponse2;
                    } else {
                        XLog.i("AppSearchHotWordsEngine", "unkown response");
                        searchAdvancedHotWordsResponse = searchAdvancedHotWordsResponse2;
                        getExplicitHotWordsResponse = getExplicitHotWordsResponse2;
                    }
                    searchAdvancedHotWordsResponse2 = searchAdvancedHotWordsResponse;
                    getExplicitHotWordsResponse2 = getExplicitHotWordsResponse;
                }
            }
            if (getExplicitHotWordsResponse2 != null) {
                this.g = getExplicitHotWordsResponse2.b;
            }
            if (searchAdvancedHotWordsResponse2 != null) {
                this.f = searchAdvancedHotWordsResponse2.i;
                this.h = searchAdvancedHotWordsResponse2.g;
                this.d = searchAdvancedHotWordsResponse2.h;
            }
            if (this.h < System.currentTimeMillis() + m.a().A()) {
                XLog.i("AppSearchHotWordsEngine", "validTime < currentTime");
                this.k.c();
                this.l.c();
                a((b) null, (g) null, 0);
                return;
            }
            a(getExplicitHotWordsResponse2, searchAdvancedHotWordsResponse2);
            a(searchAdvancedHotWordsResponse2);
            XLog.i("AppSearchHotWordsEngine", "explicitDatas size:" + this.k.b());
            XLog.i("AppSearchHotWordsEngine", "searchDatas size:" + this.l.b());
            a(this.k, this.l, 0);
            as.w().a(getExplicitHotWordsResponse2);
            as.w().a(searchAdvancedHotWordsResponse2);
            this.m = getExplicitHotWordsResponse2;
            this.n = searchAdvancedHotWordsResponse2;
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, List<RequestResponePair> list) {
        XLog.i("AppSearchHotWordsEngine", "onRequestFailed List ");
        this.d = Constants.STR_EMPTY;
        a((b) null, (g) null, i3);
    }

    private boolean a(String str) {
        LocalApkInfo installedApkInfo;
        if (!TextUtils.isEmpty(str) && (installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(str)) != null && str.endsWith(installedApkInfo.mPackageName)) {
            return true;
        }
        return false;
    }

    private boolean b(ExplicitHotWord explicitHotWord) {
        if (explicitHotWord == null || explicitHotWord.e >= explicitHotWord.f) {
            return true;
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (explicitHotWord.e > currentTimeMillis || explicitHotWord.f < currentTimeMillis) {
            return true;
        }
        return false;
    }

    private boolean a(ExplicitHotWord explicitHotWord, boolean z) {
        if (explicitHotWord == null || TextUtils.isEmpty(explicitHotWord.f2067a) || a(explicitHotWord.c)) {
            return false;
        }
        if (!z || !b(explicitHotWord)) {
            return true;
        }
        return false;
    }

    private b a(List<ExplicitHotWord> list, boolean z) {
        b bVar = new b();
        if (list != null) {
            for (ExplicitHotWord next : list) {
                if (a(next, z)) {
                    if (TextUtils.isEmpty(next.b)) {
                        next.b = next.f2067a;
                    }
                    if (next.h >= 1 && next.h <= 7) {
                        bVar.a(next.h, next);
                    }
                }
            }
        }
        return bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.aa.a(java.util.List<com.tencent.assistant.protocol.jce.ExplicitHotWord>, boolean):com.tencent.assistantv2.model.b.b
     arg types: [java.util.List<com.tencent.assistant.protocol.jce.ExplicitHotWord>, int]
     candidates:
      com.tencent.assistant.module.aa.a(java.util.List<com.tencent.assistant.protocol.jce.AdvancedHotWord>, java.util.List<com.tencent.assistant.protocol.jce.AdvancedHotWord>):com.tencent.assistantv2.model.b.g
      com.tencent.assistant.module.aa.a(com.tencent.assistant.protocol.jce.GetExplicitHotWordsResponse, com.tencent.assistant.protocol.jce.SearchAdvancedHotWordsResponse):void
      com.tencent.assistant.module.aa.a(long, long):boolean
      com.tencent.assistant.module.aa.a(com.tencent.assistant.protocol.jce.ExplicitHotWord, boolean):boolean
      com.tencent.assistant.module.aa.a(java.util.List<com.tencent.assistant.protocol.jce.ExplicitHotWord>, boolean):com.tencent.assistantv2.model.b.b */
    private b a(List<ExplicitHotWord> list) {
        return a(list, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.aa.a(java.util.List<com.tencent.assistant.protocol.jce.ExplicitHotWord>, boolean):com.tencent.assistantv2.model.b.b
     arg types: [java.util.ArrayList, int]
     candidates:
      com.tencent.assistant.module.aa.a(java.util.List<com.tencent.assistant.protocol.jce.AdvancedHotWord>, java.util.List<com.tencent.assistant.protocol.jce.AdvancedHotWord>):com.tencent.assistantv2.model.b.g
      com.tencent.assistant.module.aa.a(com.tencent.assistant.protocol.jce.GetExplicitHotWordsResponse, com.tencent.assistant.protocol.jce.SearchAdvancedHotWordsResponse):void
      com.tencent.assistant.module.aa.a(long, long):boolean
      com.tencent.assistant.module.aa.a(com.tencent.assistant.protocol.jce.ExplicitHotWord, boolean):boolean
      com.tencent.assistant.module.aa.a(java.util.List<com.tencent.assistant.protocol.jce.ExplicitHotWord>, boolean):com.tencent.assistantv2.model.b.b */
    private b b(List<AdvancedHotWord> list) {
        int[] b2;
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            for (AdvancedHotWord next : list) {
                if (!(next == null || (b2 = c.b(next.h)) == null)) {
                    for (int i2 : b2) {
                        ExplicitHotWord explicitHotWord = new ExplicitHotWord();
                        explicitHotWord.b = next.f1971a;
                        explicitHotWord.f2067a = next.f1971a;
                        explicitHotWord.d = next.h;
                        explicitHotWord.h = i2;
                        explicitHotWord.e = -1;
                        explicitHotWord.f = -1;
                        if (!(next.d == null || next.d.f == null)) {
                            explicitHotWord.c = next.d.f.f;
                            if (TextUtils.isEmpty(explicitHotWord.f2067a)) {
                                explicitHotWord.b = next.d.f.b;
                                explicitHotWord.f2067a = next.d.f.b;
                            }
                        }
                        arrayList.add(explicitHotWord);
                    }
                }
            }
        }
        return a((List<ExplicitHotWord>) arrayList, false);
    }

    private boolean b(AdvancedHotWord advancedHotWord) {
        if (advancedHotWord == null) {
            return false;
        }
        if (advancedHotWord.d == null || advancedHotWord.d.f == null || !a(advancedHotWord.d.f.f)) {
            return true;
        }
        return false;
    }

    private g a(List<AdvancedHotWord> list, List<AdvancedHotWord> list2) {
        int i2;
        int i3;
        g gVar = new g();
        if (list != null) {
            int i4 = 0;
            for (AdvancedHotWord next : list) {
                if (b(next)) {
                    if (next.h >= 1 && next.h <= 8) {
                        gVar.a(next.h, next);
                    }
                    gVar.a(0, next);
                    i3 = i4;
                } else if (list2 != null) {
                    i3 = i4;
                    while (true) {
                        if (i3 < 0 || i3 >= list2.size()) {
                            break;
                        }
                        int i5 = i3 + 1;
                        AdvancedHotWord advancedHotWord = list2.get(i3);
                        if (b(advancedHotWord) && advancedHotWord.h >= 1 && advancedHotWord.h <= 8) {
                            gVar.a(advancedHotWord.h, advancedHotWord);
                            gVar.a(0, advancedHotWord);
                            i3 = i5;
                            break;
                        }
                        i3 = i5;
                    }
                } else {
                    i3 = i4;
                }
                i4 = i3;
            }
            i2 = i4;
        } else {
            i2 = 0;
        }
        if (list2 != null) {
            while (true) {
                int i6 = i2;
                if (i6 >= list2.size()) {
                    break;
                }
                AdvancedHotWord advancedHotWord2 = list2.get(i6);
                if (b(advancedHotWord2) && advancedHotWord2.h >= 1 && advancedHotWord2.h <= 8) {
                    gVar.a(advancedHotWord2.h, advancedHotWord2);
                    gVar.a(0, advancedHotWord2);
                }
                i2 = i6 + 1;
            }
        }
        return gVar;
    }

    private void a(GetExplicitHotWordsResponse getExplicitHotWordsResponse, SearchAdvancedHotWordsResponse searchAdvancedHotWordsResponse) {
        a aVar;
        if (getExplicitHotWordsResponse != null || searchAdvancedHotWordsResponse != null) {
            if (this.k == null) {
                this.k = new b();
            }
            this.k.c();
            if (getExplicitHotWordsResponse != null) {
                this.k = a(getExplicitHotWordsResponse.d);
            }
            b bVar = new b();
            if (searchAdvancedHotWordsResponse != null) {
                bVar.a((e) b(searchAdvancedHotWordsResponse.c));
                bVar.a((e) b(searchAdvancedHotWordsResponse.f));
            }
            if (bVar != null && bVar.b() > 0) {
                if (this.k == null || this.k.b() <= 0) {
                    this.k = bVar;
                    XLog.i("AppSearchHotWordsEngine", "buildExplicitHotwordsPool:explicitDatas is null , use tmpData");
                    return;
                }
                XLog.i("AppSearchHotWordsEngine", "buildExplicitHotwordsPool:explicitDatas not null , check every key");
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 <= 8) {
                        if ((this.k.a(Integer.valueOf(i3)) == null || ((a) this.k.a(Integer.valueOf(i3))).b() <= 0) && (aVar = (a) bVar.a(Integer.valueOf(i3))) != null) {
                            this.k.a(Integer.valueOf(i3), aVar);
                        }
                        i2 = i3 + 1;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    private void a(SearchAdvancedHotWordsResponse searchAdvancedHotWordsResponse) {
        if (searchAdvancedHotWordsResponse != null) {
            if (this.l == null) {
                this.l = new g();
            }
            this.l.c();
            this.l.a((e) a(searchAdvancedHotWordsResponse.c, searchAdvancedHotWordsResponse.f));
            if (this.l == null) {
                this.l = new g();
            }
            XLog.i("AppSearchHotWordsEngine", "buildSearchHotwordsPool:");
            for (int i2 = 0; i2 <= 8; i2++) {
                if (i2 == 3) {
                    this.l.a(i2, AstApp.i().getResources().getString(R.string.search_hot_word_video_title));
                } else if (i2 == 4) {
                    this.l.a(i2, AstApp.i().getResources().getString(R.string.search_hot_word_ebook_title));
                } else if (i2 == 5) {
                    this.l.a(i2, AstApp.i().getResources().getString(R.string.search_hot_word_theme_title));
                } else {
                    this.l.a(i2, AstApp.i().getResources().getString(R.string.search_hot_word_all_title));
                }
                this.l.a(i2, searchAdvancedHotWordsResponse.k, searchAdvancedHotWordsResponse.j);
            }
        }
    }

    private void a(b bVar, g gVar, int i2) {
        XLog.i("AppSearchHotWordsEngine", "notifyUIRefreshData:" + i2);
        notifyDataChangedInMainThread(new ad(this, bVar, i2, gVar));
    }

    public void a(LocalApkInfo localApkInfo) {
        int i2;
        if (localApkInfo != null) {
            if (this.k != null) {
                i2 = this.k.a(localApkInfo.mPackageName) + 0;
            } else {
                i2 = 0;
            }
            if (this.l != null) {
                i2 += this.l.a(localApkInfo.mPackageName);
            }
        } else {
            i2 = 0;
        }
        if (i2 > 0) {
            a(this.k, this.l, 0);
        }
    }

    public String c() {
        return this.d;
    }

    private boolean b(GetExplicitHotWordsResponse getExplicitHotWordsResponse, SearchAdvancedHotWordsResponse searchAdvancedHotWordsResponse) {
        boolean z;
        boolean z2;
        if (getExplicitHotWordsResponse == null || getExplicitHotWordsResponse.d == null || getExplicitHotWordsResponse.d.size() <= 0) {
            z = false;
        } else {
            z = true;
        }
        if (searchAdvancedHotWordsResponse != null) {
            if (searchAdvancedHotWordsResponse.c == null || searchAdvancedHotWordsResponse.c.size() <= 0) {
                z2 = false;
            } else {
                z2 = true;
            }
            if (searchAdvancedHotWordsResponse.f != null && searchAdvancedHotWordsResponse.f.size() > 0) {
                z2 = true;
            }
        } else {
            z2 = false;
        }
        if (!z || !z2) {
            return false;
        }
        return true;
    }

    public a a(int i2) {
        XLog.i("AppSearchHotWordsEngine", "getExplicitHotwordsModel:" + i2);
        if (this.k != null) {
            return (a) this.k.a(Integer.valueOf(i2));
        }
        return null;
    }

    public f b(int i2) {
        XLog.i("AppSearchHotWordsEngine", "getSearchHotwordsModel:" + i2);
        if (this.l != null) {
            return (f) this.l.a(Integer.valueOf(i2));
        }
        return null;
    }

    public boolean a(AdvancedHotWord advancedHotWord) {
        if (this.n == null || advancedHotWord == null) {
            return false;
        }
        ArrayList<AdvancedHotWord> arrayList = this.n.c;
        if (arrayList != null) {
            for (AdvancedHotWord equals : arrayList) {
                if (advancedHotWord.equals(equals)) {
                    return true;
                }
            }
        }
        return false;
    }

    public ExplicitHotWord d() {
        XLog.d("AppSearchHotWordsEngine", "getCurExplicit:" + this.o);
        return this.o;
    }

    public void a(ExplicitHotWord explicitHotWord) {
        this.o = explicitHotWord;
    }
}
