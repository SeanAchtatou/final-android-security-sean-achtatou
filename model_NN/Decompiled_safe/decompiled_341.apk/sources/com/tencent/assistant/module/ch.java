package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistantv2.model.a.e;
import java.util.List;

/* compiled from: ProGuard */
class ch implements CallbackHelper.Caller<e> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1749a;
    final /* synthetic */ boolean b;
    final /* synthetic */ List c;
    final /* synthetic */ ce d;

    ch(ce ceVar, int i, boolean z, List list) {
        this.d = ceVar;
        this.f1749a = i;
        this.b = z;
        this.c = list;
    }

    /* renamed from: a */
    public void call(e eVar) {
        eVar.a(this.f1749a, 0, this.d.i, this.d.n.f1755a, this.b, null, this.c, this.d.l);
    }
}
