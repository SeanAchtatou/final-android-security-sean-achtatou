package com.tencent.assistant.module.update;

import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.Global;
import com.tencent.assistant.db.table.c;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.l;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.manager.notification.v;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.aw;
import com.tencent.assistant.module.fw;
import com.tencent.assistant.module.u;
import com.tencent.assistant.module.update.AppUpdateConst;
import com.tencent.assistant.protocol.jce.AppInfoForIgnore;
import com.tencent.assistant.protocol.jce.AppInfoForUpdate;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.assistant.protocol.jce.ClientStatus;
import com.tencent.assistant.protocol.jce.GetAppUpdateRequest;
import com.tencent.assistant.protocol.jce.GetAppUpdateResponse;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.assistant.usagestats.a;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.cv;
import com.tencent.assistant.utils.g;
import com.tencent.assistant.utils.h;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.st.b;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: ProGuard */
public class k extends aw {

    /* renamed from: a  reason: collision with root package name */
    public static long f1888a = 0;
    private static k b = null;
    private static boolean c = false;
    /* access modifiers changed from: private */
    public b d = new b();
    private Set<s> e = Collections.synchronizedSet(new HashSet());
    private HashSet<s> f = new HashSet<>();
    /* access modifiers changed from: private */
    public c g = new c(AstApp.i());
    /* access modifiers changed from: private */
    public int h = -1;
    private int i = -1;
    private int j = 0;

    private k() {
        g();
        c = true;
    }

    public static boolean a() {
        return c;
    }

    public static synchronized k b() {
        k kVar;
        synchronized (k.class) {
            if (b == null) {
                b = new k();
            }
            kVar = b;
        }
        return kVar;
    }

    /* access modifiers changed from: private */
    public void g() {
        a(as.w().h());
        j();
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED));
    }

    public void a(String str, int i2) {
        this.d.d(str);
        d(str);
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED));
        TemporaryThreadManager.get().start(new l(this));
    }

    public void b(String str, int i2) {
        Iterator<s> it = this.f.iterator();
        while (it.hasNext()) {
            s next = it.next();
            if (next.f1896a.equalsIgnoreCase(str)) {
                a(next);
                i();
                it.remove();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        String str;
        GetAppUpdateRequest getAppUpdateRequest;
        boolean z;
        String str2;
        boolean z2;
        if (this.h != i2 && this.i != i2) {
            return;
        }
        if (this.i == i2) {
            Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_BACKUP_APPLIST_SUCCESS);
            obtainMessage.arg1 = this.i;
            obtainMessage.arg2 = this.j;
            AstApp.i().j().sendMessage(obtainMessage);
            this.i = -1;
            return;
        }
        this.h = -1;
        m.a().b("app_update_response_succ_time", Long.valueOf(System.currentTimeMillis()));
        if (jceStruct == null || !(jceStruct instanceof GetAppUpdateRequest)) {
            str = null;
            getAppUpdateRequest = null;
            z = false;
        } else {
            getAppUpdateRequest = (GetAppUpdateRequest) jceStruct;
            byte a2 = getAppUpdateRequest.a();
            if (getAppUpdateRequest.j != null) {
                str2 = String.valueOf(getAppUpdateRequest.j.f2033a);
            } else {
                str2 = null;
            }
            XLog.d("AppUpdateEngine", "   reqFlag = " + ((int) a2));
            if (a2 == 0 || 2 == a2 || 4 == a2) {
                if (getAppUpdateRequest.j == null || getAppUpdateRequest.j.f2033a != AppUpdateConst.RequestLaunchType.TYPE_INTIME_UPDATE_PUSH.ordinal()) {
                    z2 = true;
                } else {
                    XLog.d("AppUpdateEngine", "TYPE_INTIME_UPDATE_PUSH, do not need update time");
                    z2 = false;
                }
                if (4 != a2) {
                    m.a().b("app_upload_all_succ_time", Long.valueOf(System.currentTimeMillis()));
                }
                str = str2;
                z = z2;
            } else {
                str = str2;
                z = false;
            }
        }
        if (z) {
            m.a().b("app_update_refresh_suc_time", Long.valueOf(System.currentTimeMillis()));
        }
        XLog.d("AppUpdateEngine", "协议成功回调  isNeedUpdateTime = " + z);
        if (jceStruct2 != null && (jceStruct2 instanceof GetAppUpdateResponse)) {
            GetAppUpdateResponse getAppUpdateResponse = (GetAppUpdateResponse) jceStruct2;
            ArrayList<AppUpdateInfo> a3 = a(getAppUpdateResponse.a());
            if (!(getAppUpdateRequest == null || getAppUpdateRequest.j == null || getAppUpdateRequest.j.f2033a != AppUpdateConst.RequestLaunchType.TYPE_STARTUP.ordinal())) {
                f();
            }
            byte[] bArr = (a3 == null || a3.size() <= 0) ? null : a3.get(0).u;
            com.tencent.assistantv2.st.page.c.a(getAppUpdateResponse.d, bArr);
            if (AstApp.i().l() || !m.a().q() || getAppUpdateResponse.d == null) {
                int i3 = 7;
                if (AstApp.i().l()) {
                    i3 = 2;
                } else if (!m.a().q()) {
                    i3 = 1;
                }
                com.tencent.assistantv2.st.page.c.a(getAppUpdateResponse.d, bArr, i3, str);
            } else {
                v.a().a(112, getAppUpdateResponse.d, bArr, false, str);
            }
            c(a3);
            j();
            be.a().a(a3.size() - this.e.size());
        }
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(1016));
        fw.b().a(d());
        fw.b().a();
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        if (this.h != i2 && this.i != i2) {
            return;
        }
        if (this.i == i2) {
            Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_BACKUP_APPLIST_FAIL);
            obtainMessage.arg1 = this.i;
            obtainMessage.arg2 = i3;
            AstApp.i().j().sendMessage(obtainMessage);
            this.i = -1;
            return;
        }
        this.h = -1;
        XLog.d("AppUpdateEngine", "协议失败回调 errorCode=" + i3);
        if (jceStruct != null && (jceStruct instanceof GetAppUpdateRequest) && jceStruct2 != null && (jceStruct2 instanceof GetAppUpdateResponse)) {
            GetAppUpdateRequest getAppUpdateRequest = (GetAppUpdateRequest) jceStruct;
            ClientStatus clientStatus = getAppUpdateRequest.j;
            byte b2 = ((GetAppUpdateResponse) jceStruct2).e;
            XLog.d("AppUpdateEngine", "协议失败回调  clientStatus=" + clientStatus + ", retryFlagFromServer=" + ((int) b2));
            boolean z = false;
            if (clientStatus != null && clientStatus.f2033a == AppUpdateConst.RequestLaunchType.TYPE_STARTUP.ordinal() && b2 < 0) {
                if (m.a().an() > 0 && m.a().ao() < m.a().an()) {
                    z = true;
                }
                XLog.d("AppUpdateEngine", "协议失败回调，重试逻辑  failRetry=" + z + ", MaxRetryTimes=" + m.a().an() + ", currentRetryTimes=" + m.a().ao());
            }
            if (z) {
                m.a().i(m.a().ao() + 1);
                b(b2);
                a(AppUpdateConst.RequestLaunchType.TYPE_STARTUP, getAppUpdateRequest.b(), (Map<String, String>) null);
            }
        }
        TemporaryThreadManager.get().start(new m(this));
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_CHECKUPDATE_FAIL));
    }

    private ArrayList<AppUpdateInfo> a(Map<Integer, ArrayList<AppUpdateInfo>> map) {
        List list;
        ArrayList<AppUpdateInfo> arrayList = new ArrayList<>();
        Hashtable hashtable = new Hashtable();
        for (int i2 = 1; i2 <= 3; i2++) {
            ArrayList arrayList2 = map.get(Integer.valueOf(i2));
            if (i2 == 1 && (list = map.get(0)) != null && !list.isEmpty()) {
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList();
                }
                arrayList2.addAll(0, list);
            }
            ArrayList<AppUpdateInfo> b2 = b(arrayList2);
            if (b2 != null && !b2.isEmpty()) {
                arrayList.addAll(b2);
                hashtable.put(Integer.valueOf(i2), b2);
            }
        }
        as.w().a(hashtable);
        this.d.a(hashtable);
        return arrayList;
    }

    private ArrayList<AppUpdateInfo> b(List<AppUpdateInfo> list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        ArrayList<AppUpdateInfo> arrayList = new ArrayList<>();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                return arrayList;
            }
            AppUpdateInfo appUpdateInfo = list.get(i3);
            if (!TextUtils.isEmpty(appUpdateInfo.f2006a) && g.a(appUpdateInfo)) {
                arrayList.add(appUpdateInfo);
            }
            i2 = i3 + 1;
        }
    }

    private void c(List<AppUpdateInfo> list) {
        if (list != null && !list.isEmpty()) {
            HashMap hashMap = new HashMap();
            for (AppUpdateInfo next : list) {
                if (next.o > 0) {
                    hashMap.put(next.f2006a, Long.valueOf(next.o));
                }
            }
            ApkResourceManager.getInstance().updateAppId(hashMap);
        }
    }

    public void a(AppUpdateConst.RequestLaunchType requestLaunchType, LbsData lbsData, Map<String, String> map) {
        TemporaryThreadManager.get().start(new n(this, requestLaunchType, lbsData, map));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    /* access modifiers changed from: private */
    public int a(LbsData lbsData, int i2) {
        byte b2;
        b.a().a(a.a());
        boolean c2 = cv.c(m.a().a("app_upload_all_succ_time", 0L));
        ArrayList<AppInfoForUpdate> a2 = a(c2);
        if (!c2 && a2 != null && a2.size() >= 80) {
            return a(lbsData, i2, a2);
        }
        if (c2) {
            b2 = 1;
        } else {
            b2 = com.tencent.assistant.appbakcup.m.a() ? (byte) 2 : 0;
        }
        return a(lbsData, i2, a2, b2);
    }

    private int a(LbsData lbsData, int i2, ArrayList<AppInfoForUpdate> arrayList) {
        a.a().a(lbsData, i2, arrayList);
        return -1;
    }

    public void a(LbsData lbsData, int i2, boolean z) {
        byte b2;
        if (z) {
            b2 = 4;
            m.a().b("app_upload_all_succ_time", Long.valueOf(System.currentTimeMillis()));
        } else {
            b2 = com.tencent.assistant.appbakcup.m.a() ? (byte) 2 : 0;
        }
        this.h = a(lbsData, i2, a(z), b2);
    }

    private int a(LbsData lbsData, int i2, ArrayList<AppInfoForUpdate> arrayList, byte b2) {
        GetAppUpdateRequest getAppUpdateRequest = new GetAppUpdateRequest();
        ArrayList<AppInfoForIgnore> h2 = h();
        ArrayList<AutoDownloadInfo> b3 = u.b();
        getAppUpdateRequest.a(b2);
        if (b2 == 2) {
            getAppUpdateRequest.a(t.u());
        }
        if (lbsData != null) {
            getAppUpdateRequest.a(lbsData);
        }
        getAppUpdateRequest.a(arrayList);
        getAppUpdateRequest.b(h2);
        getAppUpdateRequest.c(b3);
        if (lbsData != null) {
            getAppUpdateRequest.a(lbsData);
        }
        ClientStatus clientStatus = new ClientStatus();
        clientStatus.f2033a = i2;
        clientStatus.c = m.a().V();
        getAppUpdateRequest.j = clientStatus;
        return send(getAppUpdateRequest);
    }

    /* access modifiers changed from: private */
    public int a(ArrayList<AppInfoForUpdate> arrayList, LbsData lbsData, byte b2, int i2) {
        b.a().a(a.a());
        GetAppUpdateRequest getAppUpdateRequest = new GetAppUpdateRequest();
        getAppUpdateRequest.a(b2);
        getAppUpdateRequest.a(arrayList);
        getAppUpdateRequest.b(null);
        getAppUpdateRequest.c(null);
        if (lbsData != null) {
            getAppUpdateRequest.a(lbsData);
        }
        ClientStatus clientStatus = new ClientStatus();
        clientStatus.f2033a = i2;
        clientStatus.c = m.a().V();
        getAppUpdateRequest.j = clientStatus;
        XLog.d("AppUpdateEngine", "发起协议 inc flag=" + ((int) getAppUpdateRequest.b) + " action=" + i2 + " lastBackgroundTime=" + clientStatus.c);
        return send(getAppUpdateRequest);
    }

    public int c() {
        b.a().a(a.a());
        GetAppUpdateRequest getAppUpdateRequest = new GetAppUpdateRequest();
        ArrayList<AppInfoForUpdate> a2 = a(false);
        if (a2 != null) {
            this.j = a2.size();
        } else {
            this.j = 0;
        }
        getAppUpdateRequest.a((byte) 3);
        getAppUpdateRequest.a(a2);
        getAppUpdateRequest.b(null);
        getAppUpdateRequest.c(null);
        String u = t.u();
        XLog.d("BACKUP_TAG", "deviceName = " + u);
        if (TextUtils.isEmpty(u)) {
            u = "未知设备";
        }
        getAppUpdateRequest.a(u);
        this.i = send(getAppUpdateRequest);
        return this.i;
    }

    public static ArrayList<AppInfoForUpdate> a(boolean z) {
        boolean z2 = true;
        List<LocalApkInfo> localApkInfos = ApkResourceManager.getInstance().getLocalApkInfos();
        l.d(localApkInfos);
        if (z) {
            localApkInfos = d(localApkInfos);
        }
        if (localApkInfos == null || localApkInfos.isEmpty()) {
            z2 = false;
        }
        if (z2) {
            return e(localApkInfos);
        }
        return null;
    }

    private ArrayList<AppInfoForIgnore> h() {
        ArrayList<AppInfoForIgnore> arrayList = new ArrayList<>();
        for (s next : this.e) {
            AppInfoForIgnore appInfoForIgnore = new AppInfoForIgnore();
            appInfoForIgnore.a(next.f1896a);
            appInfoForIgnore.a(next.c);
            appInfoForIgnore.b(next.b);
            arrayList.add(appInfoForIgnore);
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    private static List<LocalApkInfo> d(List<LocalApkInfo> list) {
        long a2 = m.a().a("app_upload_all_succ_time", 0L);
        ArrayList arrayList = new ArrayList();
        int size = list.size();
        cv.d();
        for (int i2 = 0; i2 < size; i2++) {
            LocalApkInfo localApkInfo = list.get(i2);
            if (localApkInfo != null && cv.c(localApkInfo.mInstallDate) && localApkInfo.mInstallDate >= a2) {
                arrayList.add(localApkInfo);
            }
        }
        return arrayList;
    }

    private static ArrayList<AppInfoForUpdate> e(List<LocalApkInfo> list) {
        ArrayList<AppInfoForUpdate> arrayList = new ArrayList<>();
        for (LocalApkInfo b2 : list) {
            AppInfoForUpdate b3 = b(b2);
            if (b3 != null) {
                arrayList.add(b3);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public static AppInfoForUpdate b(LocalApkInfo localApkInfo) {
        if (localApkInfo == null) {
            return null;
        }
        AppInfoForUpdate appInfoForUpdate = new AppInfoForUpdate();
        if (localApkInfo.mAppid > 0) {
            appInfoForUpdate.e = localApkInfo.mAppid;
        } else {
            appInfoForUpdate.f2001a = localApkInfo.mPackageName;
        }
        appInfoForUpdate.c = localApkInfo.mVersionCode;
        appInfoForUpdate.g = localApkInfo.mVersionName;
        appInfoForUpdate.b = localApkInfo.signature == null ? Constants.STR_EMPTY : localApkInfo.signature;
        appInfoForUpdate.d = localApkInfo.manifestMd5;
        appInfoForUpdate.f = localApkInfo.getAppType();
        appInfoForUpdate.j = localApkInfo.mAppName;
        appInfoForUpdate.i = (long) localApkInfo.launchCount;
        appInfoForUpdate.h = localApkInfo.mLastLaunchTime;
        return appInfoForUpdate;
    }

    public List<AppUpdateInfo> d() {
        return this.d.a();
    }

    public AppUpdateInfo a(String str) {
        return this.d.a(str);
    }

    public boolean b(String str) {
        return a(str) != null;
    }

    public ArrayList<Integer> c(String str) {
        return this.d.b(str);
    }

    public synchronized Set<s> e() {
        return this.e;
    }

    public synchronized boolean a(SimpleAppModel simpleAppModel) {
        boolean z = false;
        synchronized (this) {
            if (simpleAppModel != null) {
                if (this.d.c(simpleAppModel.c)) {
                    a(new s(simpleAppModel.c, simpleAppModel.f, simpleAppModel.g, false));
                    z = true;
                }
            }
        }
        return z;
    }

    private void a(s sVar) {
        if (sVar != null && !this.e.contains(sVar)) {
            this.e.add(sVar);
            AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(1019));
            b(sVar);
        }
    }

    private synchronized void d(String str) {
        if (!TextUtils.isEmpty(str)) {
            ArrayList arrayList = new ArrayList(this.e);
            ArrayList arrayList2 = new ArrayList(1);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                s sVar = (s) it.next();
                if (sVar.f1896a.equalsIgnoreCase(str)) {
                    arrayList2.add(sVar);
                }
            }
            if (!arrayList2.isEmpty()) {
                this.e.removeAll(arrayList2);
                AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE));
                c((s) arrayList2.get(0));
                this.f.add(arrayList2.get(0));
                arrayList2.clear();
            }
        }
    }

    public synchronized void b(SimpleAppModel simpleAppModel) {
        if (simpleAppModel != null) {
            s sVar = new s(simpleAppModel.c, simpleAppModel.f, simpleAppModel.g, false);
            if (this.e.contains(sVar)) {
                this.e.remove(sVar);
                AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE));
                c(sVar);
            }
        }
    }

    private void i() {
        ArrayList arrayList = new ArrayList();
        for (AppUpdateInfo next : this.d.a()) {
            s sVar = new s(next.f2006a, next.n, next.d, h.b(next.q));
            for (s next2 : this.e) {
                if (next2.f1896a.equals(sVar.f1896a) && (next2.c == sVar.c || !sVar.d)) {
                    arrayList.add(next2);
                }
            }
        }
        this.e.clear();
        this.e.addAll(arrayList);
    }

    private void j() {
        this.e.clear();
        this.e.addAll(this.g.a());
        i();
    }

    private void b(s sVar) {
        TemporaryThreadManager.get().start(new o(this, sVar));
    }

    private void c(s sVar) {
        TemporaryThreadManager.get().start(new p(this, sVar));
    }

    public List<AppUpdateInfo> a(int i2) {
        if (this.d != null) {
            return this.d.a(i2);
        }
        return null;
    }

    public void a(List<AppUpdateInfo> list) {
        if (list != null && !list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (AppUpdateInfo next : list) {
                if (a(next)) {
                    arrayList.add(next);
                }
            }
            if (!arrayList.isEmpty()) {
                this.d.a(3, arrayList);
                c(arrayList);
                j();
                AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED));
            }
        }
    }

    private boolean a(AppUpdateInfo appUpdateInfo) {
        LocalApkInfo installedApkInfo;
        if (appUpdateInfo == null || TextUtils.isEmpty(appUpdateInfo.f2006a) || (installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(appUpdateInfo.f2006a)) == null) {
            return false;
        }
        AppUpdateInfo a2 = this.d.a(appUpdateInfo.f2006a);
        if (a2 != null) {
            if (a2.d >= appUpdateInfo.d) {
                return false;
            }
            appUpdateInfo.E = installedApkInfo.manifestMd5;
            appUpdateInfo.C = installedApkInfo.mVersionCode;
            return true;
        } else if (installedApkInfo.mVersionCode >= appUpdateInfo.d) {
            return false;
        } else {
            appUpdateInfo.E = installedApkInfo.manifestMd5;
            appUpdateInfo.C = installedApkInfo.mVersionCode;
            return true;
        }
    }

    public void a(AppUpdateConst.RequestLaunchType requestLaunchType, boolean z) {
        TemporaryThreadManager.get().start(new q(this, z, requestLaunchType));
    }

    public void f() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getQUA());
        hashMap.put("B2", Global.getPhoneGuid());
        hashMap.put("B3", String.valueOf(f1888a));
        hashMap.put("B4", String.valueOf(m.a().an()));
        hashMap.put("B5", String.valueOf(m.a().ao()));
        com.tencent.beacon.event.a.a("StatAppUpdateResponseSuccess", true, 0, 0, hashMap, true);
    }

    public void b(int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getQUA());
        hashMap.put("B2", Global.getPhoneGuid());
        hashMap.put("B3", String.valueOf(f1888a));
        hashMap.put("B4", String.valueOf(m.a().an()));
        hashMap.put("B5", String.valueOf(i2));
        hashMap.put("B6", String.valueOf(m.a().ao()));
        hashMap.put("B7", String.valueOf(System.currentTimeMillis()));
        com.tencent.beacon.event.a.a("StatAppUpdateResponseFailRetry", true, 0, 0, hashMap, true);
    }
}
