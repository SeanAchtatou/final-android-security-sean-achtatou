package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.b;
import com.tencent.assistant.module.callback.x;
import com.tencent.assistant.protocol.jce.PostFeedbackResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class et extends BaseEngine<x> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f1806a = -1;
    /* access modifiers changed from: private */
    public Object b = new Object();

    public void a(String str) {
        TemporaryThreadManager.get().start(new eu(this, str));
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        PostFeedbackResponse postFeedbackResponse = (PostFeedbackResponse) jceStruct2;
        notifyDataChangedInMainThread(new ev(this, postFeedbackResponse));
        XLog.d("Config", "******* post feedback level =" + postFeedbackResponse.b);
        if (postFeedbackResponse.b == 1) {
            b.a().a("DEV");
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new ew(this, i2));
    }
}
