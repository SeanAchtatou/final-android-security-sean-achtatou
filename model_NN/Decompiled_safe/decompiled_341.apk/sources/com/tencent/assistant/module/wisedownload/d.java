package com.tencent.assistant.module.wisedownload;

import com.tencent.assistant.module.wisedownload.condition.a;
import com.tencent.assistant.module.wisedownload.condition.f;

/* compiled from: ProGuard */
public class d extends b {
    public d() {
        a();
    }

    public void a() {
        this.d = new f(this);
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.b = new a(this);
    }

    public boolean e() {
        return true;
    }

    public boolean b() {
        return true;
    }

    public void a(o oVar) {
        int i;
        int i2;
        int i3;
        boolean z;
        boolean z2;
        int i4 = 0;
        if (oVar != null) {
            if (this.d == null || !(this.d instanceof f)) {
                i = 0;
                i2 = 0;
                i3 = 0;
                z = false;
                z2 = false;
            } else {
                f fVar = (f) this.d;
                z2 = fVar.h();
                z = fVar.i();
                i3 = fVar.l();
                i2 = fVar.m();
                i = fVar.j();
                i4 = fVar.k();
            }
            oVar.a(z2, z, i3, i2, i, i4);
        }
    }
}
