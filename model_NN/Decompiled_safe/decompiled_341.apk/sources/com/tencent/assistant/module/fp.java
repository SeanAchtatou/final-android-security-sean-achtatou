package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.ah;
import com.tencent.assistant.protocol.jce.GetSmartCardsResponse;

/* compiled from: ProGuard */
class fp implements CallbackHelper.Caller<ah> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1827a;
    final /* synthetic */ GetSmartCardsResponse b;
    final /* synthetic */ fn c;

    fp(fn fnVar, int i, GetSmartCardsResponse getSmartCardsResponse) {
        this.c = fnVar;
        this.f1827a = i;
        this.b = getSmartCardsResponse;
    }

    /* renamed from: a */
    public void call(ah ahVar) {
        ahVar.a(this.f1827a, 0, this.b.b, this.b.f2168a, this.b);
    }
}
