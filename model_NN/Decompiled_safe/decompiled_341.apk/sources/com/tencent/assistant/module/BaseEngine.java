package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.ActionCallback;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
public abstract class BaseEngine<T extends ActionCallback> extends aw {
    protected CallbackHelper<T> mCallbacks = new CallbackHelper<>();

    public void register(T t) {
        this.mCallbacks.register(t);
    }

    public void unregister(T t) {
        this.mCallbacks.unregister(t);
    }

    public void unregisterAll() {
        this.mCallbacks.unregisterAll();
    }

    /* access modifiers changed from: protected */
    public void notifyDataChangedInMainThread(CallbackHelper.Caller<T> caller) {
        runOnUiThread(new au(this, caller));
    }

    /* access modifiers changed from: protected */
    public void delayNotifyDataChangedInMainThread(CallbackHelper.Caller<T> caller, long j) {
        ba.a().postDelayed(new av(this, caller), j);
    }

    /* access modifiers changed from: protected */
    public void notifyDataChanged(CallbackHelper.Caller<T> caller) {
        this.mCallbacks.broadcast(caller);
    }

    /* access modifiers changed from: protected */
    public void runOnUiThread(Runnable runnable) {
        ba.a().post(runnable);
    }
}
