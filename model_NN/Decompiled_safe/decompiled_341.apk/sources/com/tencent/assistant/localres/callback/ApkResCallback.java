package com.tencent.assistant.localres.callback;

import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.List;

/* compiled from: ProGuard */
public interface ApkResCallback {
    void onDeleteApkSuccess(LocalApkInfo localApkInfo);

    void onGetPkgSizeFinish(LocalApkInfo localApkInfo);

    boolean onInstallApkChangedNeedReplacedEvent();

    void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i);

    void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i, boolean z);

    void onInstalledAppTimeUpdate(List<LocalApkInfo> list);

    void onLoadInstalledApkExtraSuccess(List<LocalApkInfo> list);

    void onLoadInstalledApkFail(int i, String str);

    void onLoadInstalledApkSuccess(List<LocalApkInfo> list);

    void onLoadNotInstalledApkFail(int i, String str);

    void onLoadNotInstalledApkSuccess(List<LocalApkInfo> list);

    void onNotInstalledApkDataChanged(LocalApkInfo localApkInfo, int i);

    /* compiled from: ProGuard */
    public class Stub implements ApkResCallback {
        public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        }

        public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i, boolean z) {
        }

        public void onNotInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        }

        public void onLoadInstalledApkSuccess(List<LocalApkInfo> list) {
        }

        public void onLoadInstalledApkFail(int i, String str) {
        }

        public void onLoadNotInstalledApkSuccess(List<LocalApkInfo> list) {
        }

        public void onLoadNotInstalledApkFail(int i, String str) {
        }

        public void onLoadInstalledApkExtraSuccess(List<LocalApkInfo> list) {
        }

        public void onInstalledAppTimeUpdate(List<LocalApkInfo> list) {
        }

        public void onGetPkgSizeFinish(LocalApkInfo localApkInfo) {
        }

        public void onDeleteApkSuccess(LocalApkInfo localApkInfo) {
        }

        public boolean onInstallApkChangedNeedReplacedEvent() {
            return false;
        }
    }
}
