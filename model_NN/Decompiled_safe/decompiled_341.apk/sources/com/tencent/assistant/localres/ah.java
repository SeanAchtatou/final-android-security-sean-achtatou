package com.tencent.assistant.localres;

import com.tencent.assistant.localres.callback.b;
import com.tencent.assistant.module.callback.CallbackHelper;
import java.util.List;

/* compiled from: ProGuard */
class ah implements CallbackHelper.Caller<b> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f1420a;
    final /* synthetic */ boolean b;
    final /* synthetic */ boolean c;
    final /* synthetic */ int d;
    final /* synthetic */ aa e;

    ah(aa aaVar, List list, boolean z, boolean z2, int i) {
        this.e = aaVar;
        this.f1420a = list;
        this.b = z;
        this.c = z2;
        this.d = i;
    }

    /* renamed from: a */
    public void call(b bVar) {
        bVar.a(this.f1420a, this.b, this.c, this.d);
    }
}
