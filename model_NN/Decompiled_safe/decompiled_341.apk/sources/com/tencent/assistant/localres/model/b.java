package com.tencent.assistant.localres.model;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
final class b implements Parcelable.Creator<LocalApkInfo> {
    b() {
    }

    /* renamed from: a */
    public LocalApkInfo createFromParcel(Parcel parcel) {
        boolean z;
        boolean z2 = true;
        LocalApkInfo localApkInfo = new LocalApkInfo();
        localApkInfo.mPackageName = parcel.readString();
        localApkInfo.mVersionName = parcel.readString();
        localApkInfo.mAppName = parcel.readString();
        localApkInfo.mSortKey = parcel.readString();
        localApkInfo.mAppIconRes = parcel.readInt();
        localApkInfo.mVersionCode = parcel.readInt();
        localApkInfo.launchCount = parcel.readInt();
        localApkInfo.flags = parcel.readInt();
        localApkInfo.occupySize = parcel.readLong();
        localApkInfo.mInstallDate = parcel.readLong();
        localApkInfo.signature = parcel.readString();
        localApkInfo.manifestMd5 = parcel.readString();
        localApkInfo.mLocalFilePath = parcel.readString();
        localApkInfo.mLastLaunchTime = parcel.readLong();
        localApkInfo.mFakeLastLaunchTime = parcel.readLong();
        localApkInfo.mDataUsage = parcel.readLong();
        localApkInfo.mBatteryUsage = parcel.readLong();
        localApkInfo.mInstalleLocation = parcel.readByte();
        localApkInfo.mUid = parcel.readInt();
        if (parcel.readByte() != 0) {
            z = true;
        } else {
            z = false;
        }
        localApkInfo.mIsSelect = z;
        if (parcel.readByte() == 0) {
            z2 = false;
        }
        localApkInfo.mIsSmartSlect = z2;
        return localApkInfo;
    }

    /* renamed from: a */
    public LocalApkInfo[] newArray(int i) {
        return null;
    }
}
