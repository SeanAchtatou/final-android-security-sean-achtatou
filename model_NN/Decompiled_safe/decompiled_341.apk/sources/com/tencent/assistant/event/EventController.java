package com.tencent.assistant.event;

import android.os.Message;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.event.listener.a;
import com.tencent.assistant.event.listener.b;
import com.tencent.assistant.event.listener.c;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class EventController implements a {
    private static EventController i = null;

    /* renamed from: a  reason: collision with root package name */
    protected ReferenceQueue<UIEventListener> f1266a;
    protected ReferenceQueue<a> b;
    protected ReferenceQueue<b> c;
    protected ReferenceQueue<c> d;
    private ConcurrentHashMap<Integer, List<WeakReference<UIEventListener>>> e;
    private ConcurrentHashMap<Integer, List<WeakReference<a>>> f;
    private ConcurrentHashMap<Integer, List<WeakReference<b>>> g;
    private ConcurrentHashMap<Integer, List<WeakReference<c>>> h;

    public static synchronized EventController getInstance() {
        EventController eventController;
        synchronized (EventController.class) {
            if (i == null) {
                i = new EventController();
            }
            eventController = i;
        }
        return eventController;
    }

    private EventController() {
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.e = new ConcurrentHashMap<>();
        this.f = new ConcurrentHashMap<>();
        this.g = new ConcurrentHashMap<>();
        this.h = new ConcurrentHashMap<>();
        this.f1266a = new ReferenceQueue<>();
        this.b = new ReferenceQueue<>();
        this.c = new ReferenceQueue<>();
        this.d = new ReferenceQueue<>();
    }

    public void addCacheEventListener(int i2, a aVar) {
        List<WeakReference> list;
        if (aVar != null) {
            synchronized (this.f) {
                List list2 = this.f.get(Integer.valueOf(i2));
                if (list2 != null) {
                    while (true) {
                        Reference<? extends a> poll = this.b.poll();
                        if (poll == null) {
                            break;
                        }
                        list2.remove(poll);
                    }
                }
                if (list2 == null) {
                    ArrayList arrayList = new ArrayList();
                    this.f.put(Integer.valueOf(i2), arrayList);
                    list = arrayList;
                } else {
                    list = list2;
                }
                for (WeakReference weakReference : list) {
                    if (((a) weakReference.get()) == aVar) {
                        return;
                    }
                }
                list.add(new WeakReference(aVar, this.b));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void removeCacheEventListener(int r6, com.tencent.assistant.event.listener.a r7) {
        /*
            r5 = this;
            if (r7 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            java.util.concurrent.ConcurrentHashMap<java.lang.Integer, java.util.List<java.lang.ref.WeakReference<com.tencent.assistant.event.listener.a>>> r2 = r5.f
            monitor-enter(r2)
            java.util.concurrent.ConcurrentHashMap<java.lang.Integer, java.util.List<java.lang.ref.WeakReference<com.tencent.assistant.event.listener.a>>> r0 = r5.f     // Catch:{ all -> 0x003e }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x003e }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x003e }
            java.util.List r0 = (java.util.List) r0     // Catch:{ all -> 0x003e }
            if (r0 == 0) goto L_0x0041
            java.util.Iterator r3 = r0.iterator()     // Catch:{ all -> 0x003e }
        L_0x0018:
            boolean r1 = r3.hasNext()     // Catch:{ all -> 0x003e }
            if (r1 == 0) goto L_0x0041
            java.lang.Object r1 = r3.next()     // Catch:{ all -> 0x003e }
            java.lang.ref.WeakReference r1 = (java.lang.ref.WeakReference) r1     // Catch:{ all -> 0x003e }
            java.lang.Object r4 = r1.get()     // Catch:{ all -> 0x003e }
            if (r4 != r7) goto L_0x0018
            r0.remove(r1)     // Catch:{ all -> 0x003e }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x003e }
            if (r0 == 0) goto L_0x003c
            java.util.concurrent.ConcurrentHashMap<java.lang.Integer, java.util.List<java.lang.ref.WeakReference<com.tencent.assistant.event.listener.a>>> r0 = r5.f     // Catch:{ all -> 0x003e }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x003e }
            r0.remove(r1)     // Catch:{ all -> 0x003e }
        L_0x003c:
            monitor-exit(r2)     // Catch:{ all -> 0x003e }
            goto L_0x0002
        L_0x003e:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x003e }
            throw r0
        L_0x0041:
            monitor-exit(r2)     // Catch:{ all -> 0x003e }
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.event.EventController.removeCacheEventListener(int, com.tencent.assistant.event.listener.a):void");
    }

    public void addUIEventListener(int i2, UIEventListener uIEventListener) {
        List<WeakReference> list;
        if (uIEventListener != null) {
            synchronized (this.e) {
                List list2 = this.e.get(Integer.valueOf(i2));
                if (list2 != null) {
                    while (true) {
                        Reference<? extends UIEventListener> poll = this.f1266a.poll();
                        if (poll == null) {
                            break;
                        }
                        list2.remove(poll);
                    }
                }
                if (list2 == null) {
                    ArrayList arrayList = new ArrayList();
                    this.e.put(Integer.valueOf(i2), arrayList);
                    list = arrayList;
                } else {
                    list = list2;
                }
                for (WeakReference weakReference : list) {
                    if (((UIEventListener) weakReference.get()) == uIEventListener) {
                        return;
                    }
                }
                list.add(new WeakReference(uIEventListener, this.f1266a));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void removeUIEventListener(int r6, com.tencent.assistant.event.listener.UIEventListener r7) {
        /*
            r5 = this;
            if (r7 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            java.util.concurrent.ConcurrentHashMap<java.lang.Integer, java.util.List<java.lang.ref.WeakReference<com.tencent.assistant.event.listener.UIEventListener>>> r2 = r5.e
            monitor-enter(r2)
            java.util.concurrent.ConcurrentHashMap<java.lang.Integer, java.util.List<java.lang.ref.WeakReference<com.tencent.assistant.event.listener.UIEventListener>>> r0 = r5.e     // Catch:{ all -> 0x003e }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x003e }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x003e }
            java.util.List r0 = (java.util.List) r0     // Catch:{ all -> 0x003e }
            if (r0 == 0) goto L_0x0041
            java.util.Iterator r3 = r0.iterator()     // Catch:{ all -> 0x003e }
        L_0x0018:
            boolean r1 = r3.hasNext()     // Catch:{ all -> 0x003e }
            if (r1 == 0) goto L_0x0041
            java.lang.Object r1 = r3.next()     // Catch:{ all -> 0x003e }
            java.lang.ref.WeakReference r1 = (java.lang.ref.WeakReference) r1     // Catch:{ all -> 0x003e }
            java.lang.Object r4 = r1.get()     // Catch:{ all -> 0x003e }
            if (r4 != r7) goto L_0x0018
            r0.remove(r1)     // Catch:{ all -> 0x003e }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x003e }
            if (r0 == 0) goto L_0x003c
            java.util.concurrent.ConcurrentHashMap<java.lang.Integer, java.util.List<java.lang.ref.WeakReference<com.tencent.assistant.event.listener.UIEventListener>>> r0 = r5.e     // Catch:{ all -> 0x003e }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x003e }
            r0.remove(r1)     // Catch:{ all -> 0x003e }
        L_0x003c:
            monitor-exit(r2)     // Catch:{ all -> 0x003e }
            goto L_0x0002
        L_0x003e:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x003e }
            throw r0
        L_0x0041:
            monitor-exit(r2)     // Catch:{ all -> 0x003e }
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.event.EventController.removeUIEventListener(int, com.tencent.assistant.event.listener.UIEventListener):void");
    }

    public void addConnectionEventListener(int i2, b bVar) {
        List<WeakReference> list;
        if (bVar != null) {
            synchronized (this.g) {
                List list2 = this.g.get(Integer.valueOf(i2));
                if (list2 != null) {
                    while (true) {
                        Reference<? extends b> poll = this.c.poll();
                        if (poll == null) {
                            break;
                        }
                        list2.remove(poll);
                    }
                }
                if (list2 == null) {
                    ArrayList arrayList = new ArrayList();
                    this.g.put(Integer.valueOf(i2), arrayList);
                    list = arrayList;
                } else {
                    list = list2;
                }
                for (WeakReference weakReference : list) {
                    if (((b) weakReference.get()) == bVar) {
                        return;
                    }
                }
                list.add(new WeakReference(bVar, this.c));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void removeConnectionEventListener(int r6, com.tencent.assistant.event.listener.b r7) {
        /*
            r5 = this;
            java.util.concurrent.ConcurrentHashMap<java.lang.Integer, java.util.List<java.lang.ref.WeakReference<com.tencent.assistant.event.listener.b>>> r2 = r5.g
            monitor-enter(r2)
            java.util.concurrent.ConcurrentHashMap<java.lang.Integer, java.util.List<java.lang.ref.WeakReference<com.tencent.assistant.event.listener.b>>> r0 = r5.g     // Catch:{ all -> 0x003d }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x003d }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x003d }
            java.util.List r0 = (java.util.List) r0     // Catch:{ all -> 0x003d }
            if (r0 == 0) goto L_0x003b
            java.util.Iterator r3 = r0.iterator()     // Catch:{ all -> 0x003d }
        L_0x0015:
            boolean r1 = r3.hasNext()     // Catch:{ all -> 0x003d }
            if (r1 == 0) goto L_0x003b
            java.lang.Object r1 = r3.next()     // Catch:{ all -> 0x003d }
            java.lang.ref.WeakReference r1 = (java.lang.ref.WeakReference) r1     // Catch:{ all -> 0x003d }
            java.lang.Object r4 = r1.get()     // Catch:{ all -> 0x003d }
            if (r4 != r7) goto L_0x0015
            r0.remove(r1)     // Catch:{ all -> 0x003d }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x003d }
            if (r0 == 0) goto L_0x0039
            java.util.concurrent.ConcurrentHashMap<java.lang.Integer, java.util.List<java.lang.ref.WeakReference<com.tencent.assistant.event.listener.b>>> r0 = r5.g     // Catch:{ all -> 0x003d }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x003d }
            r0.remove(r1)     // Catch:{ all -> 0x003d }
        L_0x0039:
            monitor-exit(r2)     // Catch:{ all -> 0x003d }
        L_0x003a:
            return
        L_0x003b:
            monitor-exit(r2)     // Catch:{ all -> 0x003d }
            goto L_0x003a
        L_0x003d:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x003d }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.event.EventController.removeConnectionEventListener(int, com.tencent.assistant.event.listener.b):void");
    }

    public void addPluginEventListener(int i2, c cVar) {
        List<WeakReference> list;
        if (cVar != null) {
            synchronized (this.h) {
                List list2 = this.h.get(Integer.valueOf(i2));
                if (list2 != null) {
                    while (true) {
                        Reference<? extends c> poll = this.d.poll();
                        if (poll == null) {
                            break;
                        }
                        list2.remove(poll);
                    }
                }
                if (list2 == null) {
                    ArrayList arrayList = new ArrayList();
                    this.h.put(Integer.valueOf(i2), arrayList);
                    list = arrayList;
                } else {
                    list = list2;
                }
                for (WeakReference weakReference : list) {
                    if (((c) weakReference.get()) == cVar) {
                        return;
                    }
                }
                list.add(new WeakReference(cVar, this.d));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void removePluginEventListener(int r6, com.tencent.assistant.event.listener.c r7) {
        /*
            r5 = this;
            if (r7 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            java.util.concurrent.ConcurrentHashMap<java.lang.Integer, java.util.List<java.lang.ref.WeakReference<com.tencent.assistant.event.listener.c>>> r2 = r5.h
            monitor-enter(r2)
            java.util.concurrent.ConcurrentHashMap<java.lang.Integer, java.util.List<java.lang.ref.WeakReference<com.tencent.assistant.event.listener.c>>> r0 = r5.h     // Catch:{ all -> 0x003e }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x003e }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x003e }
            java.util.List r0 = (java.util.List) r0     // Catch:{ all -> 0x003e }
            if (r0 == 0) goto L_0x0041
            java.util.Iterator r3 = r0.iterator()     // Catch:{ all -> 0x003e }
        L_0x0018:
            boolean r1 = r3.hasNext()     // Catch:{ all -> 0x003e }
            if (r1 == 0) goto L_0x0041
            java.lang.Object r1 = r3.next()     // Catch:{ all -> 0x003e }
            java.lang.ref.WeakReference r1 = (java.lang.ref.WeakReference) r1     // Catch:{ all -> 0x003e }
            java.lang.Object r4 = r1.get()     // Catch:{ all -> 0x003e }
            if (r4 != r7) goto L_0x0018
            r0.remove(r1)     // Catch:{ all -> 0x003e }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x003e }
            if (r0 == 0) goto L_0x003c
            java.util.concurrent.ConcurrentHashMap<java.lang.Integer, java.util.List<java.lang.ref.WeakReference<com.tencent.assistant.event.listener.c>>> r0 = r5.h     // Catch:{ all -> 0x003e }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x003e }
            r0.remove(r1)     // Catch:{ all -> 0x003e }
        L_0x003c:
            monitor-exit(r2)     // Catch:{ all -> 0x003e }
            goto L_0x0002
        L_0x003e:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x003e }
            throw r0
        L_0x0041:
            monitor-exit(r2)     // Catch:{ all -> 0x003e }
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.event.EventController.removePluginEventListener(int, com.tencent.assistant.event.listener.c):void");
    }

    private void a(Message message) {
        ArrayList<WeakReference> arrayList;
        List list = this.e.get(Integer.valueOf(message.what));
        if (list != null && (arrayList = new ArrayList<>(list)) != null) {
            for (WeakReference weakReference : arrayList) {
                UIEventListener uIEventListener = (UIEventListener) weakReference.get();
                if (uIEventListener != null) {
                    uIEventListener.handleUIEvent(message);
                }
            }
        }
    }

    private void b(Message message) {
        ArrayList<WeakReference> arrayList;
        List list = this.f.get(Integer.valueOf(message.what));
        if (list != null && (arrayList = new ArrayList<>(list)) != null) {
            for (WeakReference weakReference : arrayList) {
                a aVar = (a) weakReference.get();
                if (aVar != null) {
                    aVar.a(message);
                }
            }
        }
    }

    private void c(Message message) {
        ArrayList<WeakReference> arrayList;
        List list = this.h.get(Integer.valueOf(message.what));
        if (list != null && (arrayList = new ArrayList<>(list)) != null) {
            for (WeakReference weakReference : arrayList) {
                c cVar = (c) weakReference.get();
                if (cVar != null) {
                    cVar.a(message);
                }
            }
        }
    }

    private void d(Message message) {
        ArrayList<WeakReference> arrayList;
        List list = this.g.get(Integer.valueOf(message.what));
        if (list != null && (arrayList = new ArrayList<>(list)) != null) {
            for (WeakReference weakReference : arrayList) {
                b bVar = (b) weakReference.get();
                if (bVar != null) {
                    bVar.a(message);
                }
            }
        }
    }

    public void handleEvent(Message message) {
        if (message.what > 1000 && message.what < 2000) {
            a(message);
        } else if (message.what > 3000 && message.what < 4000) {
            b(message);
        } else if (message.what > 5000 && message.what < 5100) {
            d(message);
        } else if (message.what > 10000 && message.what < 12000) {
            c(message);
        }
    }
}
