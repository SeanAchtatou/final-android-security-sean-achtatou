package com.tencent.assistant.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class el implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WebViewFooter f1047a;

    el(WebViewFooter webViewFooter) {
        this.f1047a = webViewFooter;
    }

    public void onClick(View view) {
        if (this.f1047a.listener != null) {
            switch (view.getId()) {
                case R.id.webview_back_img /*2131166551*/:
                    this.f1047a.listener.onBack();
                    return;
                case R.id.webview_fresh_img /*2131166552*/:
                    this.f1047a.listener.onFresh();
                    return;
                case R.id.webview_forward_img /*2131166553*/:
                    this.f1047a.listener.onForward();
                    return;
                default:
                    return;
            }
        }
    }
}
