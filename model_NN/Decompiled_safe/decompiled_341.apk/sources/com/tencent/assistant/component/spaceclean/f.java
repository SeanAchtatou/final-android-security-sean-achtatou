package com.tencent.assistant.component.spaceclean;

import android.view.View;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RubbishItemView f1172a;

    f(RubbishItemView rubbishItemView) {
        this.f1172a = rubbishItemView;
    }

    public void onClick(View view) {
        if (this.f1172a.l.g) {
            this.f1172a.l.g = false;
            this.f1172a.h.setVisibility(8);
            this.f1172a.g.setImageResource(R.drawable.icon_open);
            return;
        }
        this.f1172a.l.g = true;
        this.f1172a.h.setVisibility(0);
        if (this.f1172a.n) {
            this.f1172a.h.findViewById(R.id.rubbish_from).setVisibility(0);
        }
        this.f1172a.g.setImageResource(R.drawable.icon_close);
    }
}
