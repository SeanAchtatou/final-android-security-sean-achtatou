package com.tencent.assistant.component.listview;

import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
public class ManagerGeneralController {

    /* renamed from: a  reason: collision with root package name */
    ManagerAdapterInterface f1079a;
    BussinessInterface b;
    AnimationExpandableListView c;

    /* compiled from: ProGuard */
    public interface BussinessCallback {
        void onFinished(ResultInfo resultInfo);
    }

    /* compiled from: ProGuard */
    public interface BussinessListener {
        void onHandlerFinished();

        void onOneItemFinished(Object obj);

        void onOneItemStart(Object obj);
    }

    /* compiled from: ProGuard */
    public interface ManagerAdapterInterface {
        void addNextIndex();

        Object getNextItem();

        PositionInfo getNextPosition();

        Object getSelectItem();

        PositionInfo getSelectPosition();

        void notifyDataChange(Object obj);

        void resetIndex();
    }

    /* compiled from: ProGuard */
    public class ResultInfo {
        public Object data;
        public int result;
    }

    public ManagerGeneralController(ManagerAdapterInterface managerAdapterInterface, BussinessInterface bussinessInterface, AnimationExpandableListView animationExpandableListView) {
        this.f1079a = managerAdapterInterface;
        this.b = bussinessInterface;
        this.c = animationExpandableListView;
    }

    public void deleteAllVisibleItem(BussinessListener bussinessListener) {
        this.c.deleteAllVisibleItem(-1, new k(this, bussinessListener));
    }

    public void deleteAllSelectItem(BussinessListener bussinessListener) {
        this.f1079a.resetIndex();
        a(bussinessListener);
    }

    /* access modifiers changed from: private */
    public void a(BussinessListener bussinessListener) {
        Object nextItem = this.f1079a.getNextItem();
        PositionInfo nextPosition = this.f1079a.getNextPosition();
        if (nextItem == null || nextPosition == null) {
            XLog.d("ManagerGeneralController", "deleteAllSelectItemInner---finish");
            if (bussinessListener != null) {
                bussinessListener.onHandlerFinished();
            }
            ba.a().post(new l(this));
            return;
        }
        XLog.d("ManagerGeneralController", "deleteAllSelectItemInner---position = " + nextPosition.childPosition + "group.pos = " + nextPosition.groupPosition);
        bussinessListener.onOneItemStart(nextItem);
        ba.a().post(new m(this, nextPosition, bussinessListener, nextItem));
    }

    /* compiled from: ProGuard */
    public abstract class BussinessInterface {
        public abstract void doAction(Object obj, BussinessCallback bussinessCallback);

        public void handleBussiness(Object obj, BussinessCallback bussinessCallback) {
            doAction(obj, bussinessCallback);
        }
    }

    /* compiled from: ProGuard */
    public class PositionInfo {
        public int childPosition;
        public int groupPosition;

        public PositionInfo(int i, int i2) {
            this.groupPosition = i;
            this.childPosition = i2;
        }
    }
}
