package com.tencent.assistant.component.slidingdrawer;

import android.os.Handler;
import android.os.Message;
import android.view.View;

/* compiled from: ProGuard */
class a extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SlidingDrawerFrameLayout f1110a;

    a(SlidingDrawerFrameLayout slidingDrawerFrameLayout) {
        this.f1110a = slidingDrawerFrameLayout;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout.a(com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout, boolean):boolean
     arg types: [com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout, int]
     candidates:
      com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout.a(com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout, int):int
      com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout.a(com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout, android.widget.LinearLayout):android.widget.LinearLayout
      com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout.a(com.tencent.assistant.component.slidingdrawer.SlidingDrawerFrameLayout, boolean):boolean */
    public void handleMessage(Message message) {
        ((View) message.obj).setVisibility(0);
        if (this.f1110a.j != null) {
            this.f1110a.j.setBackgroundDrawable(null);
        }
        boolean unused = this.f1110a.p = false;
        this.f1110a.a(true);
    }
}
