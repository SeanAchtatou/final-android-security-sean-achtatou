package com.tencent.assistant.component.treasurebox;

import android.view.View;
import android.view.animation.Animation;

/* compiled from: ProGuard */
class d implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    View f1183a;
    int b;
    int c;
    int d;
    int e;
    boolean f;
    final /* synthetic */ AppTreasureBoxCellToucher g;

    public d(AppTreasureBoxCellToucher appTreasureBoxCellToucher, View view, int i, int i2, int i3, int i4, boolean z) {
        this.g = appTreasureBoxCellToucher;
        this.f1183a = view;
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = i4;
        this.f = z;
    }

    public void onAnimationEnd(Animation animation) {
        if (this.f1183a != null) {
            this.f1183a.clearAnimation();
            if (this.f) {
                this.f1183a.layout(this.b, this.c, this.d, this.e);
            } else {
                this.f1183a.setVisibility(8);
            }
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
