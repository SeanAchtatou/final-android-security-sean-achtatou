package com.tencent.assistant.component.invalidater;

import com.tencent.assistant.utils.ba;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/* compiled from: ProGuard */
public abstract class CommonViewInvalidater implements IViewInvalidater {
    private String TAG = "CommonViewInvalidater";
    public volatile boolean canHandleMsg = true;
    /* access modifiers changed from: private */
    public List<ViewInvalidateMessage> mq = Collections.synchronizedList(new LinkedList());

    /* access modifiers changed from: protected */
    public abstract boolean canHandleMessage();

    public synchronized void sendEmptyMessage(int i) {
        ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(i, null, null);
        if (canHandleMessage()) {
            handleMessage(viewInvalidateMessage);
            handleQueueMsg();
        } else {
            this.mq.add(viewInvalidateMessage);
        }
    }

    public synchronized void dispatchEmptyMessage(int i) {
        ba.a().post(new a(this, i));
    }

    public synchronized void sendMessage(ViewInvalidateMessage viewInvalidateMessage) {
        if (viewInvalidateMessage != null) {
            if (canHandleMessage()) {
                if (viewInvalidateMessage.target != null) {
                    viewInvalidateMessage.target.handleMessage(viewInvalidateMessage);
                } else {
                    handleMessage(viewInvalidateMessage);
                }
                handleQueueMsg();
            } else {
                this.mq.add(viewInvalidateMessage);
            }
        }
    }

    public synchronized void dispatchMessage(ViewInvalidateMessage viewInvalidateMessage) {
        if (viewInvalidateMessage != null) {
            if (canHandleMessage()) {
                ba.a().post(new b(this, viewInvalidateMessage));
            } else {
                this.mq.add(viewInvalidateMessage);
            }
        }
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
    }

    public void handleQueueMsg() {
        if (this.mq.size() != 0 && canHandleMessage()) {
            ViewInvalidateMessage remove = this.mq.remove(0);
            do {
                if (remove.target != null) {
                    remove.target.handleMessage(remove);
                } else {
                    handleMessage(remove);
                }
                if (this.mq.size() > 0) {
                    remove = this.mq.remove(0);
                } else {
                    remove = null;
                }
                if (remove == null) {
                    return;
                }
            } while (canHandleMessage());
        }
    }
}
