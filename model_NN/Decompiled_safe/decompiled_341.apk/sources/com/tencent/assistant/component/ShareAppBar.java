package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.login.d;
import com.tencent.assistant.model.ShareAppModel;
import com.tencent.assistant.model.ShareBaseModel;
import com.tencent.assistant.utils.ap;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/* compiled from: ProGuard */
public class ShareAppBar extends LinearLayout {
    private static final int TIMELINE_SUPPORTED_VERSION = 553779201;
    private View.OnClickListener clickListener = new dk(this);
    private OnShareClickListener mOnShareClickListener;
    protected ShareAppModel mShareAppModel;
    protected ShareBaseModel mShareBaseModel;
    private IWXAPI mWXAPI;
    private ShareAppCallback shareAppCallback = null;
    private TextView tv_share_qq;
    private TextView tv_share_qz;
    private TextView tv_share_timeline;
    private TextView tv_share_wx;

    /* compiled from: ProGuard */
    public interface OnShareClickListener {
        void shareToQQ();

        void shareToQZ();

        void shareToTimeLine();

        void shareToWX();
    }

    /* compiled from: ProGuard */
    public interface ShareAppCallback {
        void noSupportShareApp();
    }

    public ShareAppBar(Context context) {
        super(context);
        initViews(context);
    }

    public ShareAppBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initViews(context);
    }

    public void setShareAppCallback(ShareAppCallback shareAppCallback2) {
        this.shareAppCallback = shareAppCallback2;
    }

    public void setOnShareClickListener(OnShareClickListener onShareClickListener) {
        this.mOnShareClickListener = onShareClickListener;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.tencent.assistant.component.ShareAppBar, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void initViews(Context context) {
        setOrientation(0);
        LayoutInflater.from(context).inflate((int) R.layout.share_bar_common_layout, (ViewGroup) this, true);
        this.tv_share_qq = (TextView) findViewById(R.id.tv_share_qq);
        this.tv_share_qz = (TextView) findViewById(R.id.tv_share_qz);
        this.tv_share_wx = (TextView) findViewById(R.id.tv_share_wx);
        this.tv_share_timeline = (TextView) findViewById(R.id.tv_share_timeline);
        this.tv_share_qq.setOnClickListener(this.clickListener);
        this.tv_share_qz.setOnClickListener(this.clickListener);
        this.tv_share_wx.setOnClickListener(this.clickListener);
        this.tv_share_timeline.setOnClickListener(this.clickListener);
    }

    public void refreshState() {
        boolean z;
        boolean z2;
        boolean z3 = true;
        updateViewState(this.tv_share_qz, !isWxLogin());
        TextView textView = this.tv_share_qq;
        if (!d.a().t() || !d.a().u() || !ap.a()) {
            z = false;
        } else {
            z = true;
        }
        updateViewState(textView, z);
        if (this.mWXAPI == null) {
            this.mWXAPI = WXAPIFactory.createWXAPI(AstApp.i().getApplicationContext(), "wx3909f6add1206543", false);
        }
        TextView textView2 = this.tv_share_wx;
        if (this.mWXAPI.getWXAppSupportAPI() > 553779201) {
            z2 = true;
        } else {
            z2 = false;
        }
        updateViewState(textView2, z2);
        TextView textView3 = this.tv_share_timeline;
        if (this.mWXAPI.getWXAppSupportAPI() <= 553779201) {
            z3 = false;
        }
        updateViewState(textView3, z3);
        if (this.tv_share_qq != null && this.tv_share_qz != null && this.tv_share_wx != null && this.tv_share_timeline != null && 8 == this.tv_share_qq.getVisibility() && 8 == this.tv_share_qz.getVisibility() && 8 == this.tv_share_wx.getVisibility() && 8 == this.tv_share_timeline.getVisibility() && this.shareAppCallback != null) {
            this.shareAppCallback.noSupportShareApp();
        }
    }

    private boolean isWxLogin() {
        return d.a().l();
    }

    private void updateViewState(TextView textView, boolean z) {
        if (textView != null) {
            textView.setVisibility(z ? 0 : 8);
        }
    }

    public void shareToQQ() {
        if (this.mOnShareClickListener != null) {
            this.mOnShareClickListener.shareToQQ();
        }
    }

    public void shareToQZ() {
        if (this.mOnShareClickListener != null) {
            this.mOnShareClickListener.shareToQZ();
        }
    }

    public void shareToWX() {
        if (this.mOnShareClickListener != null) {
            this.mOnShareClickListener.shareToWX();
        }
    }

    public void shareToTimeLine() {
        if (this.mOnShareClickListener != null) {
            this.mOnShareClickListener.shareToTimeLine();
        }
    }
}
