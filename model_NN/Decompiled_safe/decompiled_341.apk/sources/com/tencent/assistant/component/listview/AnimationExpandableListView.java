package com.tencent.assistant.component.listview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.widget.AbsListView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class AnimationExpandableListView extends TXExpandableListView {
    public static final boolean supportScroll = false;

    /* renamed from: a  reason: collision with root package name */
    volatile boolean f1074a = false;
    volatile int b = 0;
    ItemAnimatorLisenter c = null;

    /* compiled from: ProGuard */
    public interface ItemAnimatorLisenter {
        void onAnimatorEnd();
    }

    public AnimationExpandableListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.component.listview.AnimationExpandableListView.a(android.view.View, int, com.tencent.assistant.component.listview.AnimationExpandableListView$ItemAnimatorLisenter, boolean):void
     arg types: [android.view.View, int, com.tencent.assistant.component.listview.a, int]
     candidates:
      com.tencent.assistant.component.listview.AnimationExpandableListView.a(com.tencent.assistant.component.listview.AnimationExpandableListView, android.view.View, int, com.tencent.assistant.component.listview.AnimationExpandableListView$ItemAnimatorLisenter):void
      com.tencent.assistant.component.listview.AnimationExpandableListView.a(android.view.View, int, com.tencent.assistant.component.listview.AnimationExpandableListView$ItemAnimatorLisenter, boolean):void */
    public void deleteAllVisibleItem(int i, ItemAnimatorLisenter itemAnimatorLisenter) {
        ExpandableListView contentView = getContentView();
        int firstVisiblePosition = contentView.getFirstVisiblePosition();
        int lastVisiblePosition = contentView.getLastVisiblePosition();
        if (i == -1) {
            i = firstVisiblePosition;
        }
        if (i < firstVisiblePosition || i > lastVisiblePosition) {
            itemAnimatorLisenter.onAnimatorEnd();
            return;
        }
        View childAt = contentView.getChildAt(i - firstVisiblePosition);
        ExpandableListView.getPackedPositionType(contentView.getExpandableListPosition(i));
        a(childAt, 0, (ItemAnimatorLisenter) new a(this, i, itemAnimatorLisenter), false);
    }

    public void allChildViewVisible() {
        ExpandableListView contentView = getContentView();
        int childCount = contentView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = contentView.getChildAt(i);
            childAt.setAnimation(null);
            childAt.setVisibility(0);
        }
    }

    public void delete(int i, int i2, boolean z, ItemAnimatorLisenter itemAnimatorLisenter) {
        ExpandableListAdapter rawAdapter = getRawAdapter();
        if (i >= 0 && i < rawAdapter.getGroupCount() && i2 >= 0 && i2 < rawAdapter.getChildrenCount(i)) {
            ExpandableListView contentView = getContentView();
            long packedPositionForChild = ExpandableListView.getPackedPositionForChild(i, i2);
            int firstVisiblePosition = contentView.getFirstVisiblePosition();
            int lastVisiblePosition = contentView.getLastVisiblePosition();
            int flatListPosition = contentView.getFlatListPosition(packedPositionForChild);
            if (flatListPosition >= firstVisiblePosition && flatListPosition <= lastVisiblePosition) {
                a(contentView.getChildAt(flatListPosition - firstVisiblePosition), flatListPosition, itemAnimatorLisenter, z);
            } else if (itemAnimatorLisenter != null) {
                itemAnimatorLisenter.onAnimatorEnd();
            }
        }
    }

    private void a(View view, int i, ItemAnimatorLisenter itemAnimatorLisenter, boolean z) {
        Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.item_delete_anim);
        loadAnimation.setDuration(200);
        loadAnimation.setFillEnabled(true);
        loadAnimation.setFillAfter(true);
        loadAnimation.setFillBefore(false);
        loadAnimation.setAnimationListener(new b(this, view, z, i, itemAnimatorLisenter));
        view.startAnimation(loadAnimation);
    }

    /* access modifiers changed from: private */
    public void a(View view, int i, ItemAnimatorLisenter itemAnimatorLisenter) {
        HeightAnimation heightAnimation = new HeightAnimation(view);
        heightAnimation.setDuration(100);
        heightAnimation.setAnimationListener(new c(this, view, itemAnimatorLisenter));
        startAnimation(heightAnimation);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.component.listview.AnimationExpandableListView.a(android.view.View, int, com.tencent.assistant.component.listview.AnimationExpandableListView$ItemAnimatorLisenter, boolean):void
     arg types: [android.view.View, int, com.tencent.assistant.component.listview.AnimationExpandableListView$ItemAnimatorLisenter, int]
     candidates:
      com.tencent.assistant.component.listview.AnimationExpandableListView.a(com.tencent.assistant.component.listview.AnimationExpandableListView, android.view.View, int, com.tencent.assistant.component.listview.AnimationExpandableListView$ItemAnimatorLisenter):void
      com.tencent.assistant.component.listview.AnimationExpandableListView.a(android.view.View, int, com.tencent.assistant.component.listview.AnimationExpandableListView$ItemAnimatorLisenter, boolean):void */
    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
        if (i == 0 && this.f1074a) {
            ExpandableListView contentView = getContentView();
            int firstVisiblePosition = contentView.getFirstVisiblePosition();
            int lastVisiblePosition = contentView.getLastVisiblePosition();
            View childAt = contentView.getChildAt(this.b - firstVisiblePosition);
            XLog.d("AnimationExpand", "positin = " + this.b + " firstVisiblePosition = " + firstVisiblePosition + " lastVisiblePosition = " + lastVisiblePosition);
            a(childAt, this.b, this.c, true);
            this.f1074a = false;
        }
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        super.onScroll(absListView, i, i2, i3);
    }

    /* compiled from: ProGuard */
    public class HeightAnimation extends Animation {

        /* renamed from: a  reason: collision with root package name */
        ViewGroup.LayoutParams f1075a = this.d.getLayoutParams();
        int b = this.d.getHeight();
        private View d;

        public HeightAnimation(View view) {
            this.d = view;
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float f, Transformation transformation) {
            super.applyTransformation(f, transformation);
            XLog.d("AnimationExpand", "interpolatedTime --" + f);
            this.f1075a.height = (int) (((float) this.b) * (1.0f - f));
            this.d.setLayoutParams(this.f1075a);
        }
    }
}
