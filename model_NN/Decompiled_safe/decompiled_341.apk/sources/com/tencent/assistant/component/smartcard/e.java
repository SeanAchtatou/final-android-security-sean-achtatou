package com.tencent.assistant.component.smartcard;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.a.m;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class e extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f1144a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ NormalSmartCardPlayerSHowItem c;

    e(NormalSmartCardPlayerSHowItem normalSmartCardPlayerSHowItem, m mVar, STInfoV2 sTInfoV2) {
        this.c = normalSmartCardPlayerSHowItem;
        this.f1144a = mVar;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.c.f1115a, AppDetailActivityV5.class);
        intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.f1144a.d);
        this.c.f1115a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.updateStatusToDetail(this.f1144a.d);
        }
        return this.b;
    }
}
