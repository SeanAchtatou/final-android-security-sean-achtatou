package com.tencent.assistant.component.listener;

import android.content.Context;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.protocol.jce.UserTaskCfg;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public abstract class OnUserTaskClickListener implements View.OnClickListener {
    protected int b = 0;

    public abstract void OnUserTaskClick(View view);

    public void onClick(View view) {
        doClick(view);
    }

    public void doClick(View view) {
        this.b = view.getId();
        switch (this.b) {
            case R.id.header_layout /*2131165456*/:
                a(view);
                break;
            case R.id.close_zone /*2131165993*/:
                a();
                break;
        }
        OnUserTaskClick(view);
    }

    /* access modifiers changed from: protected */
    public void a() {
        UserTaskCfg o = as.w().o();
        o.f2418a = 0;
        o.b = 1;
        as.w().b(o);
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
        Context context = view.getContext();
        if (context instanceof BaseActivity) {
            BaseActivity baseActivity = (BaseActivity) context;
            int f = baseActivity.f();
            int p = baseActivity.p();
            String str = Constants.STR_EMPTY;
            if (view.getTag(R.id.tma_st_slot_tag) instanceof String) {
                str = (String) view.getTag(R.id.tma_st_slot_tag);
            }
            k.a(new STInfoV2(f, str, p, STConst.ST_DEFAULT_SLOT, 200));
        }
    }
}
