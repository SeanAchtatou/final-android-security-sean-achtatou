package com.tencent.assistant.component.spaceclean;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.spaceclean.SubRubbishInfo;
import com.tencent.assistant.model.spaceclean.a;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ProGuard */
public class RubbishItemView extends RelativeLayout {
    public static boolean isDeleting = false;
    public static boolean isFirstSelect = true;

    /* renamed from: a  reason: collision with root package name */
    private View f1166a;
    private View b;
    private RelativeLayout c;
    private TXImageView d;
    private TextView e;
    private TextView f;
    /* access modifiers changed from: private */
    public ImageView g;
    /* access modifiers changed from: private */
    public LinearLayout h;
    /* access modifiers changed from: private */
    public Context i;
    private LayoutInflater j;
    private Handler k;
    /* access modifiers changed from: private */
    public a l;
    /* access modifiers changed from: private */
    public STInfoV2 m;
    /* access modifiers changed from: private */
    public boolean n;
    private int o;
    private Map<String, Bitmap> p;
    private View.OnClickListener q;

    public RubbishItemView(Context context) {
        this(context, null, null, null, false, -1.0f);
    }

    public RubbishItemView(Context context, a aVar, STInfoV2 sTInfoV2) {
        this(context, null, aVar, sTInfoV2, false, -1.0f);
    }

    public RubbishItemView(Context context, a aVar, STInfoV2 sTInfoV2, boolean z, float f2) {
        this(context, null, aVar, sTInfoV2, z, f2);
    }

    public RubbishItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, null, null, false, -1.0f);
    }

    public RubbishItemView(Context context, AttributeSet attributeSet, a aVar, STInfoV2 sTInfoV2, boolean z, float f2) {
        super(context, attributeSet);
        this.n = false;
        this.p = new HashMap();
        this.q = new f(this);
        this.i = context;
        this.j = (LayoutInflater) this.i.getSystemService("layout_inflater");
        this.l = aVar;
        this.m = sTInfoV2;
        this.n = z;
        this.o = Environment.getExternalStorageDirectory().getAbsolutePath().length();
        a();
        refreshData(aVar, sTInfoV2, f2 == -1.0f ? this.i.getResources().getDimension(R.dimen.common_rubbish_detail_item_height) : f2);
    }

    public void setHandler(Handler handler) {
        this.k = handler;
    }

    public void setShowFrom(boolean z) {
        this.n = z;
    }

    private void a() {
        this.f1166a = this.j.inflate((int) R.layout.space_clean_rubbish_item, this);
        this.b = this.f1166a.findViewById(R.id.top_margin);
        this.c = (RelativeLayout) this.f1166a.findViewById(R.id.rl_rubbish_overview);
        this.d = (TXImageView) this.f1166a.findViewById(R.id.rubbish_icon_img);
        this.e = (TextView) this.f1166a.findViewById(R.id.rubbish_name);
        this.f = (TextView) this.f1166a.findViewById(R.id.rubbish_size);
        this.g = (ImageView) this.f1166a.findViewById(R.id.iv_expand);
        this.h = (LinearLayout) this.f1166a.findViewById(R.id.ll_rubbish_detail);
        this.g.setOnClickListener(this.q);
        this.c.setOnClickListener(this.q);
    }

    public void setTopMarginVisibility(int i2) {
        if (this.b != null) {
            this.b.setVisibility(i2);
        }
    }

    public void refreshData(a aVar, STInfoV2 sTInfoV2, float f2) {
        g gVar;
        if (aVar != null) {
            int i2 = R.drawable.pic_defaule;
            this.l = aVar;
            this.m = sTInfoV2;
            if (aVar.f1675a == 1) {
                try {
                    this.e.setText(this.i.getPackageManager().getApplicationInfo(aVar.e, 0).loadLabel(this.i.getPackageManager()));
                } catch (Exception e2) {
                    this.e.setText(this.i.getResources().getString(R.string.unknown));
                    e2.printStackTrace();
                }
            } else {
                i2 = a(aVar.b);
                this.e.setText(aVar.b);
            }
            this.d.updateImageView(aVar.e, i2, TXImageView.TXImageViewType.INSTALL_APK_ICON);
            this.f.setText(this.i.getString(R.string.rubbish_clear_selected_size, bt.c(aVar.d), bt.c(aVar.c)));
            int a2 = df.a(this.i, 0.7f);
            Iterator<SubRubbishInfo> it = aVar.f.iterator();
            int i3 = 0;
            while (it.hasNext()) {
                SubRubbishInfo next = it.next();
                int i4 = i3 + 1;
                View childAt = this.h.getChildAt(i3);
                int i5 = i4 + 1;
                View childAt2 = this.h.getChildAt(i4);
                if (childAt == null || childAt2 == null || childAt2.getTag() == null) {
                    View inflate = this.j.inflate((int) R.layout.space_clean_sub_rubbish_item, (ViewGroup) null);
                    g gVar2 = new g(this, null);
                    gVar2.f1173a = (ImageView) inflate.findViewById(R.id.iv_thumbnail);
                    gVar2.b = (TextView) inflate.findViewById(R.id.rubbish_name);
                    gVar2.c = (TextView) inflate.findViewById(R.id.rubbish_from);
                    gVar2.d = (TextView) inflate.findViewById(R.id.rubbish_size);
                    gVar2.e = (TextView) inflate.findViewById(R.id.iv_check);
                    inflate.setTag(gVar2);
                    View view = new View(this.i);
                    view.setBackgroundColor(this.i.getResources().getColor(R.color.appadmin_list_item_divide_line));
                    this.h.addView(view, new LinearLayout.LayoutParams(-1, a2));
                    this.h.addView(inflate, new LinearLayout.LayoutParams(-1, (int) f2));
                    gVar = gVar2;
                    childAt2 = inflate;
                } else {
                    gVar = (g) childAt2.getTag();
                    childAt.setVisibility(0);
                    childAt2.setVisibility(0);
                }
                gVar.f1173a.setTag(next.f.get(0));
                a(gVar, next, childAt2);
                i3 = i5;
            }
            int childCount = this.h.getChildCount();
            if (i3 < childCount) {
                while (i3 < childCount) {
                    View childAt3 = this.h.getChildAt(i3);
                    if (childAt3 != null) {
                        childAt3.setVisibility(8);
                    }
                    i3++;
                }
            }
            if (this.l.g) {
                this.h.setVisibility(0);
                this.g.setImageResource(R.drawable.icon_close);
                return;
            }
            this.h.setVisibility(8);
            this.g.setImageResource(R.drawable.icon_open);
        }
    }

    private void a(g gVar, SubRubbishInfo subRubbishInfo, View view) {
        if (subRubbishInfo.f1673a == SubRubbishInfo.RubbishType.VIDEO_FILE) {
            if (subRubbishInfo.f == null || this.p.get(subRubbishInfo.f.get(0)) == null) {
                gVar.f1173a.setImageDrawable(this.i.getResources().getDrawable(R.drawable.common_videopic));
                TemporaryThreadManager.get().start(new a(this, subRubbishInfo, gVar));
            } else {
                gVar.f1173a.setImageBitmap(this.p.get(subRubbishInfo.f.get(0)));
            }
            gVar.f1173a.setVisibility(0);
        } else {
            gVar.f1173a.setVisibility(8);
        }
        gVar.b.setText(subRubbishInfo.b);
        if (this.n) {
            gVar.c.setVisibility(0);
            String str = subRubbishInfo.f.get(0);
            String substring = str.substring(this.o, str.lastIndexOf("/"));
            if (TextUtils.isEmpty(substring)) {
                substring = "/sdcard";
            } else {
                String[] split = substring.split("/");
                if (split.length > 2) {
                    substring = File.separator + split[1] + File.separator + split[2];
                }
            }
            gVar.c.setText(this.i.getResources().getString(R.string.space_clean_rubbish_from, substring));
        } else {
            gVar.c.setVisibility(8);
        }
        gVar.d.setText(bt.c(subRubbishInfo.c));
        gVar.e.setSelected(subRubbishInfo.d);
        gVar.e.setOnClickListener(new c(this, subRubbishInfo, gVar));
        view.setOnClickListener(new d(this, subRubbishInfo, gVar));
    }

    /* access modifiers changed from: private */
    public Bitmap a(String str, int i2, int i3, int i4) {
        if (str == null) {
            return null;
        }
        if (this.p.get(str) != null) {
            return this.p.get(str);
        }
        Bitmap extractThumbnail = ThumbnailUtils.extractThumbnail(ThumbnailUtils.createVideoThumbnail(str, i4), i2, i3, 2);
        this.p.put(str, extractThumbnail);
        return extractThumbnail;
    }

    private int a(String str) {
        if ("安装包".equals(str)) {
            return R.drawable.rubbish_icon_apk;
        }
        if ("垃圾文件".equals(str)) {
            return R.drawable.rubbish_icon_file;
        }
        if ("视频".equals(str)) {
            return R.drawable.common_icon_video;
        }
        if ("音乐".equals(str)) {
            return R.drawable.common_icon_music;
        }
        if ("文档".equals(str)) {
            return R.drawable.common_icon_doc;
        }
        if ("压缩文件".equals(str)) {
            return R.drawable.common_icon_zip;
        }
        if ("其他文件".equals(str)) {
            return R.drawable.common_icon_film;
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public void a(TextView textView, SubRubbishInfo subRubbishInfo) {
        e eVar = new e(this, textView, subRubbishInfo);
        eVar.blockCaller = true;
        eVar.titleRes = this.i.getString(R.string.rubbish_clear_tips_dialog_title);
        eVar.lBtnTxtRes = this.i.getString(R.string.cancel);
        eVar.rBtnTxtRes = this.i.getString(R.string.app_admin_uninstall_dialog_tips_confirm);
        eVar.contentRes = this.i.getString(R.string.rubbish_clear_tips_dialog_content);
        v.a(eVar);
    }

    /* access modifiers changed from: private */
    public void b(TextView textView, SubRubbishInfo subRubbishInfo) {
        textView.setSelected(!textView.isSelected());
        subRubbishInfo.d = textView.isSelected();
        if (subRubbishInfo.d) {
            this.l.c += subRubbishInfo.c;
        } else {
            this.l.c -= subRubbishInfo.c;
        }
        this.f.setText(this.i.getString(R.string.rubbish_clear_selected_size, bt.c(this.l.d), bt.c(this.l.c)));
        if (this.k != null) {
            Message obtain = Message.obtain(this.k, 26);
            obtain.obj = this.l;
            obtain.sendToTarget();
        }
    }
}
