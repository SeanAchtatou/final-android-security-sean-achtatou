package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.component.FlowLayout;
import com.tencent.assistantv2.component.al;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.List;

/* compiled from: ProGuard */
public class CommentHeaderTagView extends RelativeLayout {
    private int COMMENT_TAG_INFO_KEY = EventDispatcherEnum.UI_EVENT_APP_DETAIL_DOWNLOAD_CLICK;
    private TextView averageCount;
    /* access modifiers changed from: private */
    public boolean isSelect = false;
    /* access modifiers changed from: private */
    public al mClickListener;
    /* access modifiers changed from: private */
    public Context mContext;
    private LayoutInflater mInflater;
    private ImageView moreImage;
    private RelativeLayout taglayout;
    /* access modifiers changed from: private */
    public FlowLayout taglistlayout;
    private TextView totalUsersCount;

    public CommentHeaderTagView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mContext = context;
        initView();
    }

    public CommentHeaderTagView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mContext = context;
        initView();
    }

    public CommentHeaderTagView(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }

    private void initView() {
        this.mInflater = LayoutInflater.from(this.mContext);
        View inflate = this.mInflater.inflate((int) R.layout.comment_detail_header_taglist_layout, this);
        this.averageCount = (TextView) inflate.findViewById(R.id.comment_average_numbers);
        this.totalUsersCount = (TextView) inflate.findViewById(R.id.comment_average_text_totol_custom);
        this.taglayout = (RelativeLayout) inflate.findViewById(R.id.comment_taglayout);
        this.taglistlayout = (FlowLayout) inflate.findViewById(R.id.comment_taglist);
        this.moreImage = (ImageView) inflate.findViewById(R.id.expand_more_img);
    }

    public void setAverageScore(String str) {
        this.averageCount.setVisibility(0);
        this.averageCount.setText(str);
    }

    public void setTotalUser(String str) {
        this.totalUsersCount.setVisibility(0);
        this.totalUsersCount.setText(str);
    }

    public void setTagListVisible(int i) {
        this.taglayout.setVisibility(i);
    }

    public void setOnFlowLayoutListener(al alVar) {
        this.mClickListener = alVar;
        this.taglistlayout.a(alVar);
    }

    public void clickCommentTag(CommentTagInfo commentTagInfo) {
        if (this.mClickListener != null) {
            this.mClickListener.a(null, commentTagInfo);
        }
    }

    public void setMoreImageVisible(int i) {
        this.moreImage.setVisibility(i);
    }

    public void setCommentTagListData(CommentTagInfo commentTagInfo, List<CommentTagInfo> list) {
        String str;
        if (list != null && list.size() > 0) {
            ImageView imageView = (ImageView) findViewById(R.id.expand_more_img);
            this.taglistlayout.removeAllViews();
            RelativeLayout relativeLayout = new RelativeLayout(this.mContext);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            TextView textView = new TextView(this.mContext);
            textView.setId(R.id.commenttag_txview_id);
            textView.setBackgroundResource(R.drawable.comment_tag_score_normal_background);
            textView.setTextColor(Color.parseColor("#6e6e6e"));
            textView.setTextSize(0, (float) this.mContext.getResources().getDimensionPixelSize(R.dimen.app_detail_comment_tag_text_size));
            textView.setText(this.mContext.getResources().getString(R.string.comment_detail_tag_all));
            textView.setOnClickListener(new ad(this));
            relativeLayout.addView(textView, layoutParams);
            if (commentTagInfo == null || TextUtils.isEmpty(commentTagInfo.f2043a)) {
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams2.addRule(7, textView.getId());
                layoutParams2.addRule(8, textView.getId());
                ImageView imageView2 = new ImageView(this.mContext);
                imageView2.setImageResource(R.drawable.pinglun_icon_on);
                imageView2.setId(R.id.commenttag_selectimg_id);
                relativeLayout.addView(imageView2, layoutParams2);
            }
            ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(-2, -2);
            marginLayoutParams.leftMargin = 0;
            marginLayoutParams.rightMargin = 16;
            marginLayoutParams.topMargin = 0;
            marginLayoutParams.bottomMargin = 16;
            this.taglistlayout.addView(relativeLayout, marginLayoutParams);
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < list.size()) {
                    CommentTagInfo commentTagInfo2 = list.get(i2);
                    String a2 = commentTagInfo2.a();
                    byte b = commentTagInfo2.b();
                    RelativeLayout relativeLayout2 = new RelativeLayout(this.mContext);
                    RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
                    TextView textView2 = new TextView(this.mContext);
                    textView2.setId(R.id.commenttag_txview_id);
                    if (b == 2) {
                        textView2.setBackgroundResource(R.drawable.comment_tag_color_good_background);
                        textView2.setTextColor(Color.parseColor("#bb9359"));
                        str = "11_";
                    } else if (b == 3) {
                        textView2.setBackgroundResource(R.drawable.comment_tag_color_bad_background);
                        textView2.setTextColor(Color.parseColor("#8891b7"));
                        str = "11_";
                    } else {
                        textView2.setBackgroundResource(R.drawable.comment_tag_score_normal_background);
                        textView2.setTextColor(Color.parseColor("#6e6e6e"));
                        str = "10_";
                    }
                    String str2 = a2 + " (" + ct.a(commentTagInfo2.e) + ")";
                    SpannableString spannableString = new SpannableString(str2);
                    spannableString.setSpan(new AbsoluteSizeSpan(13, true), 0, a2.length(), 33);
                    spannableString.setSpan(new AbsoluteSizeSpan(11, true), a2.length(), str2.length(), 33);
                    textView2.setTextSize(0, (float) this.mContext.getResources().getDimensionPixelSize(R.dimen.app_detail_comment_tag_text_size));
                    textView2.setText(spannableString);
                    textView2.setTag(R.id.tma_st_slot_tag, ct.a(i2 + 1));
                    textView2.setOnClickListener(new ae(this, str, commentTagInfo2));
                    relativeLayout2.addView(textView2, layoutParams3);
                    if (!(commentTagInfo == null || commentTagInfo.f2043a == null || !commentTagInfo.f2043a.equals(commentTagInfo2.f2043a))) {
                        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
                        layoutParams4.addRule(7, textView2.getId());
                        layoutParams4.addRule(8, textView2.getId());
                        ImageView imageView3 = new ImageView(this.mContext);
                        imageView3.setImageResource(R.drawable.pinglun_icon_on);
                        imageView3.setId(R.id.commenttag_selectimg_id);
                        relativeLayout2.addView(imageView3, layoutParams4);
                    }
                    ViewGroup.MarginLayoutParams marginLayoutParams2 = new ViewGroup.MarginLayoutParams(-2, -2);
                    marginLayoutParams2.leftMargin = 0;
                    marginLayoutParams2.rightMargin = 16;
                    marginLayoutParams2.topMargin = 0;
                    marginLayoutParams2.bottomMargin = 16;
                    this.taglistlayout.addView(relativeLayout2, marginLayoutParams2);
                    k.a(new STInfoV2(STConst.ST_PAGE_APP_DETAIL_COMMENT, str + ct.a(i2 + 1), 0, STConst.ST_DEFAULT_SLOT, 100));
                    i = i2 + 1;
                } else {
                    imageView.setOnClickListener(new af(this));
                    return;
                }
            }
        }
    }

    public void delLineViews(int i) {
        this.taglistlayout.a(i);
    }
}
