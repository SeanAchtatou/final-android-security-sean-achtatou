package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.tencent.android.qqdownloader.b;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.plugin.PluginContext;
import com.tencent.assistant.thumbnailCache.o;
import com.tencent.assistant.thumbnailCache.p;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.bf;
import java.lang.ref.WeakReference;
import java.util.HashMap;

/* compiled from: ProGuard */
public class TXImageView extends ImageView implements p {
    private static final String KEY_TYPE = "TYPE";
    private static final String KEY_URL = "URL";
    private static final String TAG = "TXImageView";
    private static Handler imageHandler = null;
    protected int defaultResId = -1;
    /* access modifiers changed from: private */
    public ViewInvalidateMessageHandler invalidateHandler = new j(this);
    /* access modifiers changed from: private */
    public IViewInvalidater invalidater;
    /* access modifiers changed from: private */
    public Bitmap mBitmap;
    private Handler mHandler = null;
    protected int mImageShape = 0;
    protected TXImageViewType mImageType = TXImageViewType.NETWORK_IMAGE_ICON;
    protected volatile String mImageUrlString = null;
    /* access modifiers changed from: private */
    public boolean mIsLoadFinish = false;
    private WeakReference<ITXImageViewListener> mListener = null;
    /* access modifiers changed from: private */
    public TXImageView self;

    /* compiled from: ProGuard */
    public interface ITXImageViewListener {
        void onTXImageViewLoadImageFinish(TXImageView tXImageView, Bitmap bitmap);
    }

    public TXImageView(Context context) {
        super(context);
        createHandler();
        this.self = this;
    }

    public TXImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        createHandler();
        this.self = this;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b.m);
        if (obtainStyledAttributes != null) {
            this.mImageType = TXImageViewType.a(obtainStyledAttributes.getInt(1, 6));
            this.mImageUrlString = obtainStyledAttributes.getString(0);
            this.mImageShape = obtainStyledAttributes.getInt(2, 0);
            obtainStyledAttributes.recycle();
        }
        if (this.mImageUrlString != null) {
            sendImageRequest();
        }
    }

    public void updateImageView(String str, int i, TXImageViewType tXImageViewType) {
        if (this.mImageShape == 1) {
            tXImageViewType = TXImageViewType.ROUND_IMAGE;
        }
        this.defaultResId = i;
        this.mImageType = tXImageViewType;
        if (TextUtils.isEmpty(str)) {
            this.mIsLoadFinish = false;
            this.mImageUrlString = null;
            if (i >= 0) {
                setImageResource(i);
            } else {
                setImageBitmap(null);
            }
        } else if (this.mImageUrlString == null || !str.equals(this.mImageUrlString) || this.mImageType != tXImageViewType || !this.mIsLoadFinish) {
            this.mIsLoadFinish = false;
            this.mImageUrlString = str;
            this.mBitmap = null;
            if (this.mImageType != TXImageViewType.UNKNOWN_IMAGE_TYPE) {
                if (i >= 0) {
                    try {
                        setImageResource(i);
                    } catch (Throwable th) {
                    }
                } else {
                    setImageBitmap(null);
                }
                sendImageRequest();
            } else if (i >= 0) {
                setImageResource(i);
            } else {
                setImageBitmap(null);
            }
        }
    }

    public void setImageDrawableAndResetImageUrlString(Drawable drawable) {
        try {
            super.setImageDrawable(drawable);
        } catch (Throwable th) {
            cq.a().b();
        }
        this.mImageUrlString = null;
    }

    public void setImageUrlString(String str) {
        this.mImageUrlString = str;
    }

    /* access modifiers changed from: protected */
    public void sendImageRequest() {
        if (this.invalidater != null) {
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1);
            HashMap hashMap = new HashMap();
            hashMap.put(KEY_URL, new String(this.mImageUrlString));
            viewInvalidateMessage.params = hashMap;
            viewInvalidateMessage.target = this.invalidateHandler;
            this.invalidater.sendMessage(viewInvalidateMessage);
            return;
        }
        sendImageRequestToInvalidater();
    }

    /* access modifiers changed from: private */
    public void sendImageRequestToInvalidater() {
        Handler imageHandler2 = getImageHandler();
        if (imageHandler2 != null) {
            imageHandler2.post(new g(this));
        }
    }

    private void createHandler() {
        this.mHandler = new i(this);
    }

    /* compiled from: ProGuard */
    public enum TXImageViewType {
        INSTALL_APK_ICON,
        UNINSTALL_APK_ICON,
        LOCAL_IMAGE,
        LOCAL_LARGER_IMAGE_THUMBNAIL,
        LOCAL_AUDIO_COVER,
        LOCAL_VIDEO_THUMBNAIL,
        NETWORK_IMAGE_ICON,
        NETWORK_IMAGE_MIDDLE,
        UNKNOWN_IMAGE_TYPE,
        ROUND_IMAGE;

        static TXImageViewType a(int i) {
            switch (i) {
                case 0:
                    return INSTALL_APK_ICON;
                case 1:
                    return UNINSTALL_APK_ICON;
                case 2:
                    return LOCAL_IMAGE;
                case 3:
                    return LOCAL_LARGER_IMAGE_THUMBNAIL;
                case 4:
                    return LOCAL_AUDIO_COVER;
                case 5:
                    return LOCAL_VIDEO_THUMBNAIL;
                case 6:
                    return NETWORK_IMAGE_ICON;
                case 7:
                    return NETWORK_IMAGE_MIDDLE;
                case 8:
                    return ROUND_IMAGE;
                default:
                    return UNKNOWN_IMAGE_TYPE;
            }
        }

        public int getThumbnailRequestType() {
            switch (k.f1207a[ordinal()]) {
                case 1:
                    return 3;
                case 2:
                    return 5;
                case 3:
                    return 4;
                case 4:
                    return 6;
                case 5:
                    return 7;
                case 6:
                default:
                    return 1;
                case 7:
                    return 2;
                case 8:
                    return 8;
                case 9:
                    return 10;
            }
        }
    }

    public void thumbnailRequestStarted(o oVar) {
    }

    public void thumbnailRequestCompleted(o oVar) {
        if (oVar != null && oVar.f != null && !oVar.f.isRecycled()) {
            this.mHandler.obtainMessage(0, oVar).sendToTarget();
        }
    }

    public void thumbnailRequestCancelled(o oVar) {
        this.mIsLoadFinish = false;
    }

    public void thumbnailRequestFailed(o oVar) {
        this.mIsLoadFinish = false;
    }

    public void setListener(ITXImageViewListener iTXImageViewListener) {
        if (iTXImageViewListener != null) {
            this.mListener = new WeakReference<>(iTXImageViewListener);
        }
    }

    /* access modifiers changed from: private */
    public void onImageLoadFinishCallListener(Bitmap bitmap) {
        ITXImageViewListener iTXImageViewListener;
        if (bitmap != null && this.mListener != null && (iTXImageViewListener = this.mListener.get()) != null) {
            iTXImageViewListener.onTXImageViewLoadImageFinish(this, bitmap);
        }
    }

    public void setInvalidater(IViewInvalidater iViewInvalidater) {
        this.invalidater = iViewInvalidater;
    }

    public void setImageResource(int i) {
        if (i < 0) {
            return;
        }
        if (getContext() instanceof PluginContext) {
            try {
                super.setImageResource(i);
            } catch (Throwable th) {
                cq.a().b();
            }
        } else if (this.mImageShape == 1 || this.mImageType == TXImageViewType.ROUND_IMAGE) {
            try {
                super.setImageBitmap(bf.a(i));
            } catch (Throwable th2) {
                cq.a().b();
            }
        } else {
            try {
                super.setImageResource(i);
            } catch (Throwable th3) {
                cq.a().b();
            }
        }
    }

    public void setImageResourceWithDrawable(Drawable drawable) {
        try {
            setImageDrawable(drawable);
        } catch (Throwable th) {
            setImageDrawable(null);
            cq.a().b();
        }
    }

    public static synchronized Handler getImageHandler() {
        Handler handler;
        synchronized (TXImageView.class) {
            if (imageHandler == null) {
                imageHandler = ba.a("TXImageViewHandler");
            }
            handler = imageHandler;
        }
        return handler;
    }
}
