package com.tencent.assistant.component.txscrollview;

import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
class q implements TXScrollViewBase.ISmoothScrollRunnableListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TXRefreshScrollViewBase f1213a;

    q(TXRefreshScrollViewBase tXRefreshScrollViewBase) {
        this.f1213a = tXRefreshScrollViewBase;
    }

    public void onSmoothScrollFinished() {
        if (this.f1213a.mRefreshListViewListener != null) {
            this.f1213a.mRefreshListViewListener.onTXRefreshListViewRefresh(this.f1213a.mCurScrollState);
        }
    }
}
