package com.tencent.assistant.component.smartcard;

import android.view.View;

/* compiled from: ProGuard */
class n implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NormalSmartcardBaseItem f1153a;

    n(NormalSmartcardBaseItem normalSmartcardBaseItem) {
        this.f1153a = normalSmartcardBaseItem;
    }

    public void onClick(View view) {
        if (this.f1153a.smartcardListener != null) {
            this.f1153a.smartcardListener.onActionClose(this.f1153a.smartcardModel.i, this.f1153a.smartcardModel.j);
        }
        this.f1153a.a("04_001", 200, this.f1153a.smartcardModel.r, -1);
    }
}
