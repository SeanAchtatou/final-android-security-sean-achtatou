package com.tencent.assistant.component.appdetail;

import android.content.Context;
import android.graphics.Rect;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.appdetail.process.AppdetailActionUIListener;
import com.tencent.assistant.component.appdetail.process.a;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.module.u;
import com.tencent.assistantv2.component.appdetail.ExchangeColorTextView;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class AppdetailDownloadBar extends RelativeLayout {
    public static final int STATE_APPBAR = 3;
    public static final int STATE_COMMENT = 2;
    public static final int STATE_DETAIL = 1;
    /* access modifiers changed from: private */
    public ViewGroup A;
    /* access modifiers changed from: private */
    public LinearLayout B;
    /* access modifiers changed from: private */
    public LinearLayout C;
    /* access modifiers changed from: private */
    public ImageView D;
    /* access modifiers changed from: private */
    public TextView E;
    /* access modifiers changed from: private */
    public ImageView F;
    /* access modifiers changed from: private */
    public TextView G;
    /* access modifiers changed from: private */
    public InnerScrollView H;
    private boolean I = false;
    /* access modifiers changed from: private */
    public boolean J = true;
    private OnTMAParamClickListener K = new e(this);
    private AppdetailActionUIListener L = new h(this);

    /* renamed from: a  reason: collision with root package name */
    ExchangeColorTextView f896a;
    AppdetailScrollView b;
    String c = "15_001";
    String d = "07_001";
    boolean e = false;
    boolean f;
    int g = -1;
    View h;
    /* access modifiers changed from: private */
    public Context i;
    /* access modifiers changed from: private */
    public ProgressBar j;
    private ProgressBar k;
    private View l;
    /* access modifiers changed from: private */
    public ImageView m;
    public int mstate = 0;
    /* access modifiers changed from: private */
    public TextView n;
    /* access modifiers changed from: private */
    public Button o;
    private Button p;
    private Button q;
    /* access modifiers changed from: private */
    public Button r;
    /* access modifiers changed from: private */
    public Button s;
    public int showType = 0;
    private View t;
    private View u;
    private View v;
    private TextView w;
    private TextView x;
    private TextView y;
    /* access modifiers changed from: private */
    public a z;

    public AppdetailDownloadBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public AppdetailDownloadBar(Context context) {
        super(context);
        a(context);
    }

    public int getNeedScrollToBottomOnResume() {
        return this.g;
    }

    public void setNeedScrollToBottomOnResume(int i2) {
        this.g = i2;
    }

    private void a(Context context) {
        this.i = context;
        LayoutInflater.from(context).inflate((int) R.layout.appdetail_download_bar, this);
        this.j = (ProgressBar) findViewById(R.id.appdetail_progress_bar);
        this.k = (ProgressBar) findViewById(R.id.appdetail_progress_bar_1);
        this.k.setId(R.id.appdetail_progress_bar);
        this.k.setOnClickListener(this.K);
        this.j.setOnClickListener(this.K);
        this.o = (Button) findViewById(R.id.appdetail_progress_btn);
        this.o.setOnClickListener(this.K);
        this.l = findViewById(R.id.appdetail_floating_layout);
        this.m = (ImageView) findViewById(R.id.appdetail_floating_icon);
        this.n = (TextView) findViewById(R.id.appdetail_floating_txt);
        this.t = findViewById(R.id.app_updatesizeinfo);
        this.w = (TextView) findViewById(R.id.app_size_tips);
        this.x = (TextView) findViewById(R.id.app_size_sumsize);
        this.y = (TextView) findViewById(R.id.app_size_slimsize);
        this.A = (ViewGroup) findViewById(R.id.two_btn_auth);
        this.B = (LinearLayout) findViewById(R.id.wx_auth);
        this.C = (LinearLayout) findViewById(R.id.qq_auth);
        this.D = (ImageView) findViewById(R.id.wx_auth_icon);
        this.E = (TextView) findViewById(R.id.wx_auth_txt);
        this.F = (ImageView) findViewById(R.id.qq_auth_icon);
        this.G = (TextView) findViewById(R.id.qq_auth_txt);
        this.B.setOnClickListener(this.K);
        this.C.setOnClickListener(this.K);
        this.p = (Button) findViewById(R.id.btn_pause_download);
        this.q = (Button) findViewById(R.id.btn_delete_download);
        this.p.setOnClickListener(this.K);
        this.q.setOnClickListener(this.K);
        this.f896a = (ExchangeColorTextView) findViewById(R.id.appdetail_floating_txt_b);
        this.r = (Button) findViewById(R.id.appdetail_progress_btn_for_cmd);
        this.u = findViewById(R.id.appdetail_progress_btn_for_cmd_area);
        this.s = (Button) findViewById(R.id.appdetail_progress_btn_for_appbar);
        this.v = findViewById(R.id.appdetail_progress_btn_for_appbar_area);
    }

    public void setSimpleAppModel(a aVar) {
        this.z = aVar;
    }

    public void setHasCommented(boolean z2, AppConst.AppState appState, long j2, String str, int i2, int i3, boolean z3) {
        this.I = true;
        this.e = z2;
        if (this.mstate == 2 && this.r != null) {
            this.u.setVisibility(0);
            this.r.setOnClickListener(this.K);
        }
        if (z2) {
            if (this.z != null) {
                this.z.a(z2, appState, j2, str, i2, i3, z3);
            }
            if (this.r != null && z2) {
                this.r.setText((int) R.string.comment_txt_tips_already_cmd);
                this.r.setTextColor(this.i.getResources().getColor(R.color.comment_over_txt_tips));
                this.r.setBackgroundResource(R.drawable.common_btn_big_disabled);
                this.r.setOnClickListener(null);
            }
        }
    }

    public void setCommentButtonClickable(boolean z2) {
        if (this.r != null) {
            this.r.setClickable(z2);
        }
    }

    public void setClickBtnForCmdText(boolean z2) {
        if (this.r != null && this.e && z2) {
            this.r.setText((int) R.string.comment_txt_tips_already_cmd);
            this.r.setTextColor(this.i.getResources().getColor(R.color.comment_over_txt_tips));
            this.r.setBackgroundResource(R.drawable.common_btn_big_disabled);
            this.r.setOnClickListener(null);
        } else if (this.r != null) {
            this.r.setText((int) R.string.comment_detail_write_text);
        }
    }

    public void setProgressAndDownloading(int i2, int i3) {
        if (this.j != null && this.k != null) {
            this.j.setProgress(i2);
            this.j.setSecondaryProgress(i3);
            this.k.setProgress(i2);
            this.k.setSecondaryProgress(i3);
            if (i2 > 0 && i2 < 100) {
                this.f896a.a(((float) i2) / 100.0f);
            } else if (i2 == 0) {
                this.f896a.a(((float) i2) / 100.0f);
            }
        }
    }

    public void setMyProgressDrawable(int i2) {
        Rect bounds = this.j.getProgressDrawable().getBounds();
        this.j.setProgressDrawable(getContext().getResources().getDrawable(i2));
        this.j.getProgressDrawable().setBounds(bounds);
    }

    public void setMyScrolView(InnerScrollView innerScrollView, AppdetailScrollView appdetailScrollView, View view, boolean z2) {
        this.H = innerScrollView;
        this.b = appdetailScrollView;
        this.h = view;
        this.f = z2;
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.z != null && this.showType != 3) {
            AppConst.AppState d2 = u.d(this.z.a());
            if (this.H == null) {
                return;
            }
            if ((d2 == AppConst.AppState.DOWNLOAD || d2 == AppConst.AppState.UPDATE || d2 == AppConst.AppState.PAUSED) && this.mstate == 1) {
                int measuredHeight = this.h.getMeasuredHeight() - this.H.getHeight();
                if (this.h.findViewById(R.id.my_tag_area).getVisibility() == 0) {
                    measuredHeight = this.h.findViewById(R.id.my_tag_area).getTop();
                } else if (this.h.findViewById(R.id.recommend_view).getVisibility() == 0) {
                    measuredHeight = this.h.findViewById(R.id.recommend_view).getTop();
                }
                if (measuredHeight < 0) {
                    measuredHeight = 0;
                }
                this.H.startScrollToBottom(measuredHeight);
                this.b.smoothScrollHeader(true);
            }
        }
    }

    public void scrolViewToBottom(boolean z2) {
        if (this.H != null) {
            this.H.postDelayed(new f(this), 1000);
        }
    }

    public void updateDownloadState2Bar(String str, String str2) {
        if (this.t != null) {
            this.t.setVisibility(8);
        }
        if (this.k != null) {
            this.k.setVisibility(8);
        }
        this.l.setVisibility(0);
        this.f896a.setVisibility(8);
        this.n.setText(Constants.STR_EMPTY);
        if (!TextUtils.isEmpty(str)) {
            this.n.append(new SpannableString(str));
        }
        if (!TextUtils.isEmpty(str2)) {
            SpannableString spannableString = new SpannableString(" (" + str2 + ")");
            spannableString.setSpan(new AbsoluteSizeSpan(16, true), 0, spannableString.length(), 17);
            this.n.append(spannableString);
        }
    }

    public void updateBarText(String str, int i2) {
        if (this.t != null) {
            this.t.setVisibility(8);
        }
        if (this.k != null) {
            this.k.setVisibility(8);
        }
        if (str.startsWith(getResources().getString(R.string.update)) && str.endsWith("B")) {
            this.l.setVisibility(0);
            this.f896a.setVisibility(8);
            this.n.setText(Constants.STR_EMPTY);
            this.n.append(new SpannableString(str.subSequence(0, 2)));
            SpannableString spannableString = new SpannableString(" (" + str.substring(3) + ")");
            spannableString.setSpan(new AbsoluteSizeSpan(16, true), 0, spannableString.length(), 17);
            this.n.append(spannableString);
        } else if (!TextUtils.isEmpty(str) && this.n != null) {
            this.n.setText(str);
            int progress = this.j.getProgress();
            if ((progress <= 0 || progress >= 100) && !str.endsWith("%") && !str.equals("继续") && !str.equals("等待中") && !str.equals("暂停")) {
                this.l.setVisibility(0);
                this.f896a.setVisibility(8);
            } else {
                this.l.setVisibility(8);
                this.o.setVisibility(8);
                this.f896a.setVisibility(0);
                if (str.equals("继续")) {
                    this.k.setVisibility(0);
                    this.j.setVisibility(8);
                    this.f896a.a(true);
                    this.f896a.postDelayed(new g(this, progress), 200);
                } else {
                    this.k.setVisibility(8);
                    this.j.setVisibility(0);
                    this.f896a.a(false);
                }
                this.f896a.a(str);
                this.f896a.a(((float) progress) / 100.0f);
            }
            if (i2 > 0) {
                this.m.setVisibility(0);
                this.m.setImageResource(i2);
                return;
            }
            this.m.setVisibility(8);
        }
    }

    public void updateSlimBarText(String str, String str2, String str3) {
        if (!TextUtils.isEmpty(str) && this.l != null && !TextUtils.isEmpty(str2)) {
            this.l.setVisibility(8);
            this.f896a.setVisibility(8);
            this.t.setVisibility(0);
            this.w.setText(str);
            this.x.setText("(" + str2);
            this.y.setText(str3 + ")");
        }
    }

    public void onCreate() {
        if (this.z != null) {
            this.z.f();
        }
    }

    public void onResume() {
        if (this.z != null) {
            this.z.g();
        }
    }

    public void onPause() {
        if (this.z != null) {
            this.z.h();
        }
    }

    public void onDestroy() {
        if (this.z != null) {
            this.z.i();
        }
    }

    public void setState(int i2) {
        if (this.z != null) {
            this.z.a(i2);
            this.mstate = i2;
        }
        if (i2 == 3) {
            this.v.setVisibility(0);
            this.s.setOnClickListener(this.K);
            return;
        }
        this.v.setVisibility(8);
        if (i2 != 2) {
            this.u.setVisibility(8);
        } else if (this.I) {
            this.u.setVisibility(0);
            if (!this.e) {
                this.r.setOnClickListener(this.K);
            }
        }
    }

    public AppdetailActionUIListener getUiListener() {
        return this.L;
    }
}
