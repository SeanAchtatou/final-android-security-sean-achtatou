package com.tencent.assistant.component.slidingdrawer;

import android.view.View;
import android.view.ViewGroup;

/* compiled from: ProGuard */
public abstract class BaseSlidingDrawerAdapter {

    /* renamed from: a  reason: collision with root package name */
    protected SlidingDrawerFrameLayout f1107a;

    public abstract Object getChild(int i);

    public abstract View getChildView(int i, ViewGroup viewGroup);

    public abstract Object getGroup(int i);

    public abstract int getGroupCount();

    public abstract View getGroupView(int i, ViewGroup viewGroup);

    public abstract void onSetFrameLayout();

    public SlidingDrawerFrameLayout getFrameLayout() {
        return this.f1107a;
    }

    public void setFrameLayout(SlidingDrawerFrameLayout slidingDrawerFrameLayout) {
        this.f1107a = slidingDrawerFrameLayout;
        onSetFrameLayout();
    }
}
