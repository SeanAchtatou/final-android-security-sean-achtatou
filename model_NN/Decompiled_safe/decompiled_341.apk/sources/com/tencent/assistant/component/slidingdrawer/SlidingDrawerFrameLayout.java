package com.tencent.assistant.component.slidingdrawer;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.SwitchButton;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class SlidingDrawerFrameLayout extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f1109a;
    private LayoutInflater b;
    /* access modifiers changed from: private */
    public CustomScrollView c;
    private LinearLayout d;
    private List<d> e = new ArrayList();
    private BaseSlidingDrawerAdapter f;
    private c g = new c(this, null);
    /* access modifiers changed from: private */
    public View.OnClickListener h;
    private OnSlidingDrawerListener i;
    /* access modifiers changed from: private */
    public LinearLayout j;
    /* access modifiers changed from: private */
    public int k = -1;
    /* access modifiers changed from: private */
    public int l = -1;
    private int m = -1;
    private int n = -1;
    private boolean o = false;
    /* access modifiers changed from: private */
    public boolean p = false;
    /* access modifiers changed from: private */
    public View q;
    /* access modifiers changed from: private */
    public Handler r = new a(this);

    /* compiled from: ProGuard */
    public interface ILoadingViewListener {
        void onLoadingViewEnd();

        void onLoadingViewStart();
    }

    /* compiled from: ProGuard */
    public interface OnSlidingDrawerListener {
        void onCloseGroup(int i, View view);

        void onExpandGroup(int i, View view);
    }

    public SlidingDrawerFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public SlidingDrawerFrameLayout(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
        this.f1109a = context;
        this.b = LayoutInflater.from(context);
        View inflate = this.b.inflate((int) R.layout.sliding_drawer_frame_layout, this);
        this.c = (CustomScrollView) inflate.findViewById(R.id.sliding_drawer_scrollview);
        this.d = (LinearLayout) inflate.findViewById(R.id.sliding_drawer_linear_layout);
        this.q = this;
    }

    private void a() {
        if (this.d != null && this.e != null && this.e.size() != 0) {
            this.d.removeAllViews();
            for (int i2 = 0; i2 < this.e.size(); i2++) {
                d dVar = this.e.get(i2);
                if (dVar.f1113a != null) {
                    this.d.addView(dVar.f1113a);
                    dVar.f1113a.setTag(new Integer(i2));
                    dVar.f1113a.setOnClickListener(this.g);
                    LinearLayout linearLayout = new LinearLayout(this.f1109a);
                    this.d.addView(linearLayout);
                    ((LinearLayout.LayoutParams) linearLayout.getLayoutParams()).setMargins(1, 0, 1, 0);
                }
            }
        }
    }

    public void expandGroup(int i2) {
        View childView;
        int i3 = 0;
        if (i2 <= this.e.size() && this.e.get(i2).f1113a != null) {
            int i4 = this.k;
            if (this.j != null) {
                closeGroup(i4, false);
            }
            if (i4 != i2 && (childView = this.f.getChildView(i2, this)) != null) {
                if (childView instanceof ISlidingDrawerView) {
                    ((ISlidingDrawerView) childView).onExpandGroup();
                }
                childView.setVisibility(4);
                LinearLayout linearLayout = (LinearLayout) this.d.getChildAt((i2 * 2) + 1);
                if (linearLayout.getChildCount() <= 0) {
                    linearLayout.addView(childView);
                }
                linearLayout.setBackgroundResource(R.drawable.helper_cardbg_midpress);
                this.l = 0;
                while (true) {
                    int i5 = i3;
                    if (i5 >= this.e.size() || i5 >= i2) {
                        this.l -= 3;
                        this.m = -1;
                    } else {
                        this.l = this.e.get(i5).f1113a.getHeight() + this.l;
                        i3 = i5 + 1;
                    }
                }
                this.l -= 3;
                this.m = -1;
                if (this.m <= 0) {
                    if (childView instanceof ISlidingDrawerView) {
                        this.m = ((ISlidingDrawerView) childView).getDrawerViewHeight();
                    }
                    int height = this.n - this.e.get(i2).f1113a.getHeight();
                    if (this.m <= 0 || this.m > height) {
                        this.m = height;
                    }
                }
                linearLayout.setTag(Integer.valueOf(this.m));
                linearLayout.startAnimation(new e(this, linearLayout, 50));
                this.p = true;
                this.j = linearLayout;
                this.k = i2;
                if (this.i != null) {
                    this.i.onExpandGroup(i2, this.j);
                }
            }
        }
    }

    public void closeGroup(int i2, boolean z) {
        View childAt;
        if (i2 <= this.e.size() && this.e.get(i2).f1113a != null) {
            this.l = 0;
            if (this.j != null) {
                if (this.i != null) {
                    this.i.onCloseGroup(this.k, this.j);
                }
                if (this.j != null && this.j.getChildCount() > 0 && (childAt = this.j.getChildAt(0)) != null && (childAt instanceof ISlidingDrawerView)) {
                    ((ISlidingDrawerView) childAt).onCloseGroup();
                }
                if (z) {
                    b bVar = new b(this, this.j, SwitchButton.SWITCH_ANIMATION_DURATION);
                    this.p = true;
                    this.j.startAnimation(bVar);
                    return;
                }
                this.j.getLayoutParams().height = 0;
                this.j.setVisibility(8);
                this.j = null;
                this.k = -1;
                this.c.scrollTo(0, 0);
                a(false);
            }
        }
    }

    public int setAdapter(BaseSlidingDrawerAdapter baseSlidingDrawerAdapter) {
        if (baseSlidingDrawerAdapter == null) {
            return 0;
        }
        this.f = baseSlidingDrawerAdapter;
        baseSlidingDrawerAdapter.setFrameLayout(this);
        this.e.clear();
        int groupCount = baseSlidingDrawerAdapter.getGroupCount();
        for (int i2 = 0; i2 < groupCount; i2++) {
            View groupView = baseSlidingDrawerAdapter.getGroupView(i2, this.d);
            if (groupView != null) {
                d dVar = new d(this, null);
                dVar.f1113a = groupView;
                dVar.b = baseSlidingDrawerAdapter.getChildView(i2, this.d);
                this.e.add(dVar);
            }
        }
        a();
        return this.e.size();
    }

    public void setScrollEnalbe(boolean z) {
        this.c.requestDisallowInterceptTouchEvent(z);
    }

    public void setGroupClickListener(View.OnClickListener onClickListener) {
        if (onClickListener != null) {
            this.h = onClickListener;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (-1 == this.n) {
            this.n = i5 - i3;
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.o) {
            return false;
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public boolean getScreenLocker() {
        return this.o;
    }

    public void setScreenLocker(boolean z) {
        this.o = z;
    }

    public void setSlidingDrawerListener(OnSlidingDrawerListener onSlidingDrawerListener) {
        this.i = onSlidingDrawerListener;
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (this.c != null) {
            this.c.setScrollFlag(z);
        }
    }

    public int getExpandPosition() {
        return this.k;
    }

    public void onResume() {
        if (this.c != null) {
            this.c.scrollTo(0, this.l);
        }
    }
}
