package com.tencent.assistant.component;

import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class aj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f888a;
    final /* synthetic */ DownloadProgressButton b;

    aj(DownloadProgressButton downloadProgressButton, int i) {
        this.b = downloadProgressButton;
        this.f888a = i;
    }

    public void run() {
        if (this.b.getDisplayCount() == this.f888a) {
            this.b.downCountText.setText(String.valueOf(this.f888a));
            this.b.downCountText.setVisibility(0);
            this.b.downImage.setImageResource(R.drawable.icon_xiazai_empty);
        }
    }
}
