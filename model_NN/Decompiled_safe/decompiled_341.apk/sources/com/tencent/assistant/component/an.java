package com.tencent.assistant.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.module.callback.l;
import com.tencent.assistant.protocol.jce.AppCategory;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
class an implements l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameCategoryListView f892a;

    an(GameCategoryListView gameCategoryListView) {
        this.f892a = gameCategoryListView;
    }

    public void a(int i, int i2, List<ColorCardItem> list, List<AppCategory> list2, List<AppCategory> list3) {
        if (this.f892a.viewPagerlistener != null) {
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1, null, this.f892a.pageMessageHandler);
            viewInvalidateMessage.arg1 = i2;
            viewInvalidateMessage.arg2 = i;
            HashMap hashMap = new HashMap();
            if (this.f892a.mCategoryId == -2) {
                hashMap.put(0, list);
                hashMap.put(1, list2);
                if (list3 != null) {
                    hashMap.put(2, list3);
                }
            }
            viewInvalidateMessage.params = hashMap;
            this.f892a.viewPagerlistener.sendMessage(viewInvalidateMessage);
        }
    }
}
