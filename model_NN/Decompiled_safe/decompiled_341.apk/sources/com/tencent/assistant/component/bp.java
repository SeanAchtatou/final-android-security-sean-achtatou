package com.tencent.assistant.component;

import android.view.View;

/* compiled from: ProGuard */
class bp implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PicView f971a;

    bp(PicView picView) {
        this.f971a = picView;
    }

    public void onClick(View view) {
        if (this.f971a.animationListener != null) {
            this.f971a.animationListener.onAnimationViewClick();
        }
    }
}
