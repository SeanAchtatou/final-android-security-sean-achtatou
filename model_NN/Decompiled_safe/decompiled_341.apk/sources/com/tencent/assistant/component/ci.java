package com.tencent.assistant.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class ci extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PreInstallAppListView f991a;

    ci(PreInstallAppListView preInstallAppListView) {
        this.f991a = preInstallAppListView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.PreInstalledAppListAdapter.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.tencent.assistant.adapter.PreInstalledAppListAdapter.a(int, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistant.adapter.PreInstalledAppListAdapter.a(android.widget.TextView, long):void
      com.tencent.assistant.adapter.PreInstalledAppListAdapter.a(com.tencent.assistant.adapter.cm, com.tencent.assistant.localres.model.LocalApkInfo):void
      com.tencent.assistant.adapter.PreInstalledAppListAdapter.a(com.tencent.assistant.adapter.PreInstalledAppListAdapter, boolean):boolean
      com.tencent.assistant.adapter.PreInstalledAppListAdapter.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, int):void
      com.tencent.assistant.adapter.PreInstalledAppListAdapter.a(boolean, boolean):void */
    public void onTMAClick(View view) {
        if (this.f991a.mFooterView.getFooterViewEnable() && this.f991a.mUninstallAppList.size() > 0) {
            this.f991a.mUninstallFailSet.clear();
            this.f991a.startUninstallApp((LocalApkInfo) this.f991a.mUninstallAppList.get(0));
            boolean unused = this.f991a.mUninstalling = true;
            boolean unused2 = this.f991a.mIsUserStartUninstall = true;
            this.f991a.mAdapter.a(this.f991a.mUninstalling, true);
            this.f991a.updateFootView();
            this.f991a.reportBatchUninstall(this.f991a.mUninstallAppList.size());
        }
    }
}
