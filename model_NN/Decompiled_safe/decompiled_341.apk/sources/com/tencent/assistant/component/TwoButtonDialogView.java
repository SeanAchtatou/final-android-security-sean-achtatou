package com.tencent.assistant.component;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.cq;

/* compiled from: ProGuard */
public class TwoButtonDialogView extends RelativeLayout {
    private boolean inflateSuccess;
    private RelativeLayout mExtraLayout;
    private LayoutInflater mInflater;
    private View mInfoLayout;
    private TextView mInfoText;
    private Button mNagitiveBtn;
    private Button mPositiveBtn;
    private TextView mTitleText;
    private View mTtitleLayout;

    public TwoButtonDialogView(Context context) {
        this(context, null);
    }

    public TwoButtonDialogView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.inflateSuccess = true;
        this.mInflater = LayoutInflater.from(context);
        initView();
    }

    public boolean isInflateSucc() {
        return this.inflateSuccess;
    }

    private void initView() {
        this.inflateSuccess = true;
        try {
            this.mInflater.inflate((int) R.layout.dialog_2button, this);
            this.mTitleText = (TextView) findViewById(R.id.title);
            this.mTtitleLayout = findViewById(R.id.titleLayout);
            this.mInfoText = (TextView) findViewById(R.id.msg);
            this.mInfoLayout = findViewById(R.id.msgLayout);
            this.mNagitiveBtn = (Button) findViewById(R.id.nagitive_btn);
            this.mPositiveBtn = (Button) findViewById(R.id.positive_btn);
            this.mExtraLayout = (RelativeLayout) findViewById(R.id.extraLayout);
        } catch (Throwable th) {
            this.inflateSuccess = false;
            cq.a().b();
        }
    }

    public void setTitleAndMsg(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            this.mTitleText.setText(str);
        }
        if (!TextUtils.isEmpty(str2)) {
            this.mInfoText.setText(str2);
        }
    }

    public void setHasTitle(boolean z) {
        if (!z) {
            this.mTtitleLayout.setVisibility(8);
        } else {
            this.mTtitleLayout.setVisibility(0);
        }
    }

    public void setButton(String str, String str2, View.OnClickListener onClickListener, View.OnClickListener onClickListener2) {
        if (!TextUtils.isEmpty(str)) {
            this.mNagitiveBtn.setText(str);
        }
        if (!TextUtils.isEmpty(str2)) {
            this.mPositiveBtn.setText(str2);
        }
        if (onClickListener != null) {
            this.mNagitiveBtn.setOnClickListener(onClickListener);
        }
        if (onClickListener2 != null) {
            this.mPositiveBtn.setOnClickListener(onClickListener2);
        }
    }

    public void setButtonStyle(boolean z, int i, int i2) {
        if (z) {
            this.mNagitiveBtn.setTextColor(getResources().getColor(i));
            this.mNagitiveBtn.setBackgroundDrawable(getResources().getDrawable(i2));
            return;
        }
        this.mPositiveBtn.setTextColor(getResources().getColor(i));
        this.mPositiveBtn.setBackgroundDrawable(getResources().getDrawable(i2));
    }

    public void addExtraMsgView(View view) {
        if (view != null) {
            ViewParent parent = view.getParent();
            if (parent != null && (parent instanceof ViewGroup)) {
                ((ViewGroup) view.getParent()).removeView(view);
            }
            this.mExtraLayout.setVisibility(0);
            this.mExtraLayout.addView(view);
        }
    }
}
