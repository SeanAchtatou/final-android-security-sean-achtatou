package com.tencent.assistant.component;

import android.os.Handler;
import android.view.ViewTreeObserver;

/* compiled from: ProGuard */
class cc implements ViewTreeObserver.OnGlobalLayoutListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopViewDialog f985a;

    cc(PopViewDialog popViewDialog) {
        this.f985a = popViewDialog;
    }

    public void onGlobalLayout() {
        this.f985a.scrollView.scrollTo(0, 40);
        if (this.f985a.scrollView.getRootView().getHeight() - this.f985a.scrollView.getHeight() > 100) {
            new Handler().postDelayed(new cd(this), 300);
        }
    }
}
