package com.tencent.assistant.component;

import android.graphics.Rect;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/* compiled from: ProGuard */
class au extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HorizontalListView f950a;

    au(HorizontalListView horizontalListView) {
        this.f950a = horizontalListView;
    }

    public boolean onDown(MotionEvent motionEvent) {
        return this.f950a.onDown(motionEvent);
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return this.f950a.onFling(motionEvent, motionEvent2, f, f2);
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        synchronized (this.f950a) {
            this.f950a.mNextX += (int) f;
            this.f950a.requestLayout();
        }
        return true;
    }

    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f950a.getChildCount()) {
                return true;
            }
            View childAt = this.f950a.getChildAt(i2);
            if (a(motionEvent, childAt)) {
                if (this.f950a.mOnItemClicked != null) {
                    this.f950a.mOnItemClicked.onItemClick(this.f950a, childAt, this.f950a.mLeftViewIndex + 1 + i2, this.f950a.mAdapter.getItemId(this.f950a.mLeftViewIndex + 1 + i2));
                }
                if (this.f950a.mOnItemSelected == null) {
                    return true;
                }
                this.f950a.mOnItemSelected.onItemSelected(this.f950a, childAt, this.f950a.mLeftViewIndex + 1 + i2, this.f950a.mAdapter.getItemId(this.f950a.mLeftViewIndex + 1 + i2));
                return true;
            }
            i = i2 + 1;
        }
    }

    public void onLongPress(MotionEvent motionEvent) {
        int childCount = this.f950a.getChildCount();
        int i = 0;
        while (i < childCount) {
            View childAt = this.f950a.getChildAt(i);
            if (!a(motionEvent, childAt)) {
                i++;
            } else if (this.f950a.mOnItemLongClicked != null) {
                this.f950a.mOnItemLongClicked.onItemLongClick(this.f950a, childAt, this.f950a.mLeftViewIndex + 1 + i, this.f950a.mAdapter.getItemId(i + this.f950a.mLeftViewIndex + 1));
                return;
            } else {
                return;
            }
        }
    }

    private boolean a(MotionEvent motionEvent, View view) {
        Rect rect = new Rect();
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        int i = iArr[0];
        int i2 = iArr[1];
        rect.set(i, i2, view.getWidth() + i, view.getHeight() + i2);
        return rect.contains((int) motionEvent.getRawX(), (int) motionEvent.getRawY());
    }
}
