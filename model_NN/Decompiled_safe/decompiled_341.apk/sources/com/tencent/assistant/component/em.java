package com.tencent.assistant.component;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class em extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f1048a;
    final /* synthetic */ WifiTipsView b;

    em(WifiTipsView wifiTipsView, Context context) {
        this.b = wifiTipsView;
        this.f1048a = context;
    }

    public void onTMAClick(View view) {
        this.b.closeWifiTips();
    }

    public STInfoV2 getStInfo() {
        return STInfoBuilder.buildSTInfo(this.f1048a, 200);
    }
}
