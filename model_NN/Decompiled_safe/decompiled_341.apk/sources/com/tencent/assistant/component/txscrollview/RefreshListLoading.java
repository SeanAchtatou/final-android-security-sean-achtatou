package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
public class RefreshListLoading extends TXLoadingLayoutBase {

    /* renamed from: a  reason: collision with root package name */
    private View f1187a;
    private ProgressBar b;
    private ImageView c;
    private ImageView d;
    private TextView e;
    private TextView f;
    private RelativeLayout g;
    private RotateAnimation h;
    private RotateAnimation i;
    private CharSequence j;
    private CharSequence k;
    private CharSequence l;
    private CharSequence m;
    private CharSequence n;
    private CharSequence o;
    private String p;
    private boolean q = false;

    public RefreshListLoading(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, TXScrollViewBase.ScrollMode.PULL_FROM_END);
        a();
    }

    public RefreshListLoading(Context context, TXScrollViewBase.ScrollDirection scrollDirection, TXScrollViewBase.ScrollMode scrollMode) {
        super(context, scrollDirection, scrollMode);
        a(context, this.mScrollMode);
        a();
    }

    public void setRefreshTimeKey(String str) {
        this.p = str;
    }

    private void a() {
        this.h = new RotateAnimation(0.0f, -180.0f, 1, 0.5f, 1, 0.5f);
        this.h.setInterpolator(new LinearInterpolator());
        this.h.setDuration(100);
        this.h.setFillAfter(true);
        this.i = new RotateAnimation(-180.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        this.i.setInterpolator(new LinearInterpolator());
        this.i.setDuration(100);
        this.i.setFillAfter(true);
    }

    public void reset() {
        if (this.mScrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_START) {
            this.c.setVisibility(0);
            this.f1187a.setVisibility(0);
        } else {
            this.c.setVisibility(8);
            this.f1187a.setVisibility(8);
        }
        this.c.clearAnimation();
        this.e.setVisibility(0);
        this.e.setText(this.j);
        this.b.setVisibility(8);
        this.d.setVisibility(8);
    }

    public void pullToRefresh() {
        this.c.setVisibility(0);
        this.c.clearAnimation();
        this.e.setVisibility(0);
        this.e.setText(this.j);
        this.f.setVisibility(8);
        this.b.setVisibility(8);
        this.d.setVisibility(8);
    }

    public void releaseToRefresh() {
        this.c.clearAnimation();
        this.c.startAnimation(this.h);
        this.e.setVisibility(0);
        this.e.setText(this.k);
        this.f.setVisibility(8);
    }

    public void refreshing() {
        this.c.clearAnimation();
        this.c.setVisibility(8);
        this.d.setVisibility(8);
        this.e.setText(this.l);
        this.f.setVisibility(8);
        this.f1187a.setVisibility(0);
        this.b.setVisibility(0);
    }

    public void loadFinish(String str) {
        this.c.setVisibility(8);
        this.d.setVisibility(0);
        try {
            this.d.setImageResource(R.drawable.loaded_all);
        } catch (Throwable th) {
            cq.a().b();
        }
        this.e.setText(str);
        this.f.setVisibility(8);
        this.b.setVisibility(8);
        if (!TextUtils.isEmpty(str)) {
            this.f1187a.setVisibility(0);
            this.e.setVisibility(0);
            return;
        }
        this.f1187a.setVisibility(8);
        this.e.setVisibility(8);
    }

    public void setWidth(int i2) {
        getLayoutParams().width = i2;
        requestLayout();
    }

    public void setHeight(int i2) {
        getLayoutParams().height = i2;
        requestLayout();
    }

    public void onPull(int i2) {
    }

    public void hideAllSubViews() {
        if (this.c.getVisibility() == 0) {
            this.c.setVisibility(4);
        }
        if (this.d.getVisibility() == 0) {
            this.d.setVisibility(4);
        }
        if (this.e.getVisibility() == 0) {
            this.e.setVisibility(4);
        }
        if (this.f.getVisibility() == 0) {
            this.f.setVisibility(4);
        }
        if (this.b.getVisibility() == 0) {
            this.b.setVisibility(4);
        }
    }

    public void showAllSubViews() {
        if (this.c.getVisibility() == 4) {
            this.c.setVisibility(0);
        }
        if (this.d.getVisibility() == 4) {
            this.d.setVisibility(0);
        }
        if (this.e.getVisibility() == 4) {
            this.e.setVisibility(0);
        }
        if (this.f.getVisibility() == 4) {
        }
        if (this.b.getVisibility() == 4) {
            this.b.setVisibility(0);
        }
    }

    public int getContentSize() {
        return this.g.getHeight();
    }

    public int getTriggerSize() {
        return getResources().getDimensionPixelSize(R.dimen.tx_pull_to_refresh_trigger_size);
    }

    private void a(Context context, TXScrollViewBase.ScrollMode scrollMode) {
        if (scrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_START) {
            this.j = context.getString(R.string.refresh_list_loading_pull_from_start);
            this.l = context.getString(R.string.refresh_list_loading_refreshing_from_start);
        } else {
            this.j = context.getString(R.string.refresh_list_loading_pullToRefresh);
            this.l = context.getString(R.string.refresh_list_loading_refreshing_from_end);
        }
        this.k = context.getString(R.string.refresh_list_loading_releaseToRefresh);
        this.o = context.getString(R.string.refresh_loading_fail);
        this.m = context.getString(R.string.refresh_list_loading_loadfinish);
        this.n = context.getString(R.string.refresh_suc);
        LayoutInflater.from(context).inflate((int) R.layout.refresh_list_loading, this);
        this.g = (RelativeLayout) findViewById(R.id.refresh_list_loading_content);
        this.f1187a = findViewById(R.id.left_ly);
        this.b = (ProgressBar) findViewById(R.id.refresh_list_loading_progress);
        this.c = (ImageView) findViewById(R.id.refresh_image);
        this.d = (ImageView) findViewById(R.id.result_img);
        this.e = (TextView) findViewById(R.id.refresh_text);
        this.f = (TextView) findViewById(R.id.small_txt);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.g.getLayoutParams();
        if (scrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_START) {
            layoutParams.height = df.a(getContext(), 60.0f);
            layoutParams.gravity = 80;
            this.c.setVisibility(0);
        } else {
            layoutParams.height = df.a(getContext(), 63.0f);
            layoutParams.gravity = 48;
            this.c.setVisibility(8);
        }
        reset();
    }

    public void refreshSuc() {
        this.c.setVisibility(8);
        this.b.setVisibility(8);
        if (this.q) {
            this.d.setImageResource(R.drawable.right_white);
        } else {
            this.d.setImageResource(R.drawable.right);
        }
        this.d.setVisibility(0);
        this.e.setVisibility(0);
        this.e.setText(this.n);
        this.f.setVisibility(8);
    }

    public void refreshFail(String str) {
        this.c.setVisibility(8);
        this.b.setVisibility(8);
        if (this.q) {
            this.d.setImageResource(R.drawable.shibai_white);
        } else {
            this.d.setImageResource(R.drawable.shibai);
        }
        this.d.setVisibility(0);
        this.e.setVisibility(0);
        this.e.setText(str);
        this.f.setVisibility(8);
    }

    public void loadSuc() {
        reset();
    }

    public void loadFail() {
        this.c.setVisibility(8);
        this.b.setVisibility(8);
        this.d.setImageResource(R.drawable.shibai);
        this.d.setVisibility(0);
        this.e.setVisibility(0);
        this.e.setText(this.o);
        this.f.setVisibility(8);
    }

    public void refreshLoadingResource() {
        this.q = true;
        this.c.setImageResource(R.drawable.refresh_arrow_white);
        Drawable drawable = getContext().getResources().getDrawable(R.anim.rotate_loading_view_withtopbanner);
        drawable.setBounds(0, df.a(getContext(), 16.0f), 0, df.a(getContext(), 16.0f));
        this.b.setIndeterminateDrawable(drawable);
        this.e.setTextColor(getContext().getResources().getColor(R.color.white));
        this.f.setTextColor(getContext().getResources().getColor(R.color.white));
    }

    public void resetLoadingResource() {
        this.q = false;
        this.c.setImageResource(R.drawable.refresh_arrow);
        Drawable drawable = getContext().getResources().getDrawable(R.anim.rotate_loading_view);
        drawable.setBounds(0, df.a(getContext(), 16.0f), 0, df.a(getContext(), 16.0f));
        this.b.setIndeterminateDrawable(drawable);
        this.e.setTextColor(getContext().getResources().getColor(R.color.pull_refresh_txt_color));
        this.f.setTextColor(getContext().getResources().getColor(R.color.pull_refresh_txt_small_color));
    }
}
