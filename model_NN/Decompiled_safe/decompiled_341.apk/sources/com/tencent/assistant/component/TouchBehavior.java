package com.tencent.assistant.component;

import android.view.MotionEvent;
import com.tencent.assistant.component.TouchAnalizer;

/* compiled from: ProGuard */
public abstract class TouchBehavior {
    public static final int RET_CONTINUE = 0;
    public static final int RET_CONTINUE_FORCE_MORE = 5;
    public static final int RET_FAILED = 1;
    public static final int RET_MATCHED = 2;
    public static final int RET_MATCHED_AND_CALLED_FALSE = 4;
    public static final int RET_MATCHED_AND_CALLED_TRUE = 3;
    protected TouchBehaviorEventJudger judger;
    protected TouchAnalizer manager;
    private boolean paused = false;
    protected TouchAnalizer.BehaviorType type;

    /* compiled from: ProGuard */
    public interface TouchBehaviorEventJudger {
        int judgeEvent(TouchAnalizer.BehaviorType behaviorType, float f, float f2, int i);
    }

    public abstract int analizeTouchEvent(MotionEvent motionEvent);

    public TouchBehavior(TouchAnalizer touchAnalizer) {
        this.manager = touchAnalizer;
    }

    public void pause() {
        this.paused = true;
    }

    public void setJudger(TouchBehaviorEventJudger touchBehaviorEventJudger) {
        this.judger = touchBehaviorEventJudger;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.paused) {
            if (motionEvent.getAction() != 0) {
                return false;
            }
            this.paused = false;
        }
        int analizeTouchEvent = analizeTouchEvent(motionEvent);
        if (analizeTouchEvent == 0) {
            return false;
        }
        if (analizeTouchEvent == 1) {
            this.paused = true;
            return false;
        } else if (analizeTouchEvent == 2) {
            return this.manager.onBehavior(this.type, motionEvent.getX(), motionEvent.getY());
        } else {
            if (analizeTouchEvent == 3) {
                return true;
            }
            if (analizeTouchEvent == 4) {
                return false;
            }
            if (analizeTouchEvent == 5) {
                return true;
            }
            this.paused = true;
            return false;
        }
    }
}
