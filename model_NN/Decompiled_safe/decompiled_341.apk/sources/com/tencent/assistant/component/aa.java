package com.tencent.assistant.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.CommentReplyActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.component.RankNormalListView;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class aa extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentDetail f879a;
    final /* synthetic */ CommentDetailView b;

    aa(CommentDetailView commentDetailView, CommentDetail commentDetail) {
        this.b = commentDetailView;
        this.f879a = commentDetail;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.b.mContext, CommentReplyActivity.class);
        intent.putExtra("comment", this.f879a);
        intent.putExtra("simpleAppModel", this.b.simpleAppModel);
        this.b.mContext.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (!(this.b.getContext() instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 i = ((AppDetailActivityV5) this.b.getContext()).i();
        i.slotId = a.a(RankNormalListView.ST_HIDE_INSTALLED_APPS, 1);
        i.actionId = 200;
        return i;
    }
}
