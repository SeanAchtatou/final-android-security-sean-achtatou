package com.tencent.assistant.component;

import android.view.animation.Animation;

/* renamed from: com.tencent.assistant.component.do  reason: invalid class name */
/* compiled from: ProGuard */
class Cdo extends dq {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SwitchButton f1024a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    Cdo(SwitchButton switchButton) {
        super(null);
        this.f1024a = switchButton;
    }

    public void onAnimationEnd(Animation animation) {
        this.f1024a.setOnToOffView();
    }
}
