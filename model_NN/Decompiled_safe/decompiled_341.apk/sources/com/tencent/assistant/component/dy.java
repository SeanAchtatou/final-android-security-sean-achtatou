package com.tencent.assistant.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.UpdateIgnoreListActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.module.u;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class dy extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateListView f1033a;

    dy(UpdateListView updateListView) {
        this.f1033a = updateListView;
    }

    public void onTMAClick(View view) {
        if (u.b(this.f1033a.mUpdateListAdapter.b) != 0) {
            this.f1033a.mContext.startActivity(new Intent(this.f1033a.mContext, UpdateIgnoreListActivity.class));
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildUpdateStatInfo = this.f1033a.buildUpdateStatInfo();
        if (buildUpdateStatInfo != null) {
            buildUpdateStatInfo.slotId = a.a("06", "001");
        }
        return buildUpdateStatInfo;
    }
}
