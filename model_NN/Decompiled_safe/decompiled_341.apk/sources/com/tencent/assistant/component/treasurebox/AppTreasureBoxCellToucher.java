package com.tencent.assistant.component.treasurebox;

import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;

/* compiled from: ProGuard */
public class AppTreasureBoxCellToucher implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    float f1176a = 0.0f;
    float b = 0.0f;
    boolean c = false;
    AppTreasureBoxCell1 d;
    private LinearLayout e = null;
    private TranslateAnimation f = null;
    private int g = Integer.MIN_VALUE;
    private int h = Integer.MIN_VALUE;
    private int i = Integer.MIN_VALUE;
    private int j = Integer.MIN_VALUE;
    private long k;
    private long l;

    public AppTreasureBoxCellToucher(AppTreasureBoxCell1 appTreasureBoxCell1) {
        this.d = appTreasureBoxCell1;
        if (this.d != null) {
            this.e = this.d.getAboveAreaView();
        }
    }

    private void a() {
        this.d.onTouchCellOpen();
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (this.d != null) {
                    this.d.endDemoAnimation();
                }
                this.f1176a = motionEvent.getRawX();
                this.b = motionEvent.getRawY();
                this.c = false;
                this.k = System.currentTimeMillis();
                if (this.g == Integer.MIN_VALUE) {
                    this.g = this.e.getLeft();
                    this.h = this.e.getTop();
                    this.i = this.e.getRight();
                    this.j = this.e.getBottom();
                    break;
                }
                break;
            case 1:
                this.l = System.currentTimeMillis();
                if (!this.c) {
                    a(0, (this.g - this.e.getLeft()) + this.e.getWidth(), 0, 0, false);
                    a();
                } else if (((motionEvent.getRawX() - this.f1176a) * 1000.0f) / ((float) (this.l - this.k)) > 700.0f) {
                    a(0, (this.g - this.e.getLeft()) + this.e.getWidth(), 0, 0, false);
                    a();
                } else if (!a(this.f1176a, motionEvent.getRawX())) {
                    a(0, this.g - this.e.getLeft(), 0, 0, true);
                } else {
                    a(0, (this.g - this.e.getLeft()) + this.e.getWidth(), 0, 0, false);
                    a();
                }
                this.c = false;
                break;
            case 2:
                if (!this.c && a(this.f1176a, this.b, motionEvent.getRawX(), motionEvent.getRawY())) {
                    this.c = true;
                }
                if (this.c) {
                    a((int) (motionEvent.getRawX() - this.f1176a), 0);
                    break;
                }
                break;
            case 3:
                this.c = false;
                break;
        }
        return true;
    }

    private boolean a(float f2, float f3, float f4, float f5) {
        if (Math.sqrt(Math.pow((double) (f4 - f2), 2.0d) + Math.pow((double) (f3 - f5), 2.0d)) > 15.0d) {
            return true;
        }
        return false;
    }

    private boolean a(float f2, float f3) {
        if (this.e == null || Float.compare(Math.abs(f3 - f2), 0.7f * ((float) this.e.getWidth())) <= 0) {
            return false;
        }
        return true;
    }

    private void a(int i2, int i3) {
        if (this.e != null && i2 > 0) {
            a(this.g + i2, this.h + i3, this.i + i2, this.j + i3);
        }
    }

    private void a(int i2, int i3, int i4, int i5) {
        if (this.e != null) {
            this.e.layout(i2, i3, i4, i5);
        }
    }

    private void a(int i2, int i3, int i4, int i5, boolean z) {
        if (this.e != null) {
            this.e.clearAnimation();
            this.f = new TranslateAnimation((float) i2, (float) i3, (float) i4, (float) i5);
            this.f.setInterpolator(AnimationUtils.loadInterpolator(this.e.getContext(), 17432583));
            this.f.setDuration(300);
            this.f.setRepeatCount(0);
            this.f.setFillAfter(true);
            if (z) {
                this.f.setAnimationListener(new d(this, this.e, this.g, this.h, this.i, this.j, z));
            } else {
                this.f.setAnimationListener(new d(this, this.e, this.e.getWidth() + this.g, this.h, this.e.getWidth() + this.i, this.j, z));
            }
            this.e.startAnimation(this.f);
        }
    }
}
