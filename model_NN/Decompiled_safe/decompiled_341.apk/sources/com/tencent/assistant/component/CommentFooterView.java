package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
public class CommentFooterView extends TXLoadingLayoutBase {
    public static final int TYPE_ERROR = 4;
    public static final int TYPE_FINISHED = 3;
    public static final int TYPE_HAS_MORE = 1;
    public static final int TYPE_LOADING = 2;
    private CharSequence error;
    private CharSequence hasMore;
    private CharSequence isLoading;
    private LinearLayout layout;
    private CharSequence loadFinished;
    private ProgressBar loadingBar;
    private int mType = 0;
    private TextView seeMore;

    public CommentFooterView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CommentFooterView(Context context, TXScrollViewBase.ScrollDirection scrollDirection, TXScrollViewBase.ScrollMode scrollMode) {
        super(context, scrollDirection, scrollMode);
        initLayout(context);
    }

    private void initLayout(Context context) {
        this.isLoading = context.getString(R.string.refresh_list_loading_refreshing_from_end);
        this.loadFinished = context.getString(R.string.is_finished);
        this.hasMore = context.getString(R.string.comment_detail_more_text);
        this.error = context.getString(R.string.is_next_page_error);
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.comment_detail_footer_layout, this);
        this.seeMore = (TextView) inflate.findViewById(R.id.comment_detail_footer_text);
        this.layout = (LinearLayout) inflate.findViewById(R.id.layout_id);
        this.loadingBar = (ProgressBar) inflate.findViewById(R.id.foot_loading_bar);
        reset();
    }

    public TextView getSeeMore() {
        return this.seeMore;
    }

    public void setSeeMore(TextView textView) {
        this.seeMore = textView;
    }

    public void reset() {
        this.seeMore.setText(this.isLoading);
    }

    public void pullToRefresh() {
        this.seeMore.setText(this.isLoading);
    }

    public void releaseToRefresh() {
        this.seeMore.setText(this.isLoading);
    }

    public void refreshing() {
        this.seeMore.setText(this.isLoading);
    }

    public void loadFinish(String str) {
        this.seeMore.setText(this.loadFinished);
    }

    public void onPull(int i) {
    }

    public void hideAllSubViews() {
    }

    public void showAllSubViews() {
    }

    public void setWidth(int i) {
        getLayoutParams().width = i;
        requestLayout();
    }

    public void setHeight(int i) {
        getLayoutParams().height = i;
        requestLayout();
    }

    public int getContentSize() {
        return this.layout.getHeight();
    }

    public int getTriggerSize() {
        return getResources().getDimensionPixelSize(R.dimen.tx_pull_to_refresh_trigger_size);
    }

    public void freshState(int i) {
        this.mType = i;
        switch (i) {
            case 1:
                setClickable(true);
                setEnabled(true);
                this.loadingBar.setVisibility(8);
                this.seeMore.setText(this.hasMore);
                this.seeMore.setVisibility(8);
                break;
            case 2:
                setClickable(false);
                setEnabled(false);
                this.loadingBar.setVisibility(0);
                this.seeMore.setText(this.isLoading);
                this.seeMore.setVisibility(0);
                break;
            case 3:
                setClickable(false);
                setEnabled(false);
                this.loadingBar.setVisibility(8);
                this.seeMore.setVisibility(8);
                break;
            case 4:
                setClickable(true);
                setEnabled(true);
                this.loadingBar.setVisibility(8);
                this.seeMore.setText(this.error);
                this.seeMore.setVisibility(0);
                break;
        }
        requestLayout();
    }

    public int getState() {
        return this.mType;
    }

    public void refreshSuc() {
    }

    public void refreshFail(String str) {
    }

    public void loadSuc() {
    }

    public void loadFail() {
    }
}
