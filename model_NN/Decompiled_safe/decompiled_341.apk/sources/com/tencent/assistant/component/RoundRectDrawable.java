package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.text.TextUtils;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
public class RoundRectDrawable extends ShapeDrawable {
    private static float[] radii = {radius, radius, radius, radius, radius, radius, radius, radius};
    private static float radius = df.a(2.0f);
    private Context context;

    public RoundRectDrawable(Context context2) {
        super(new RoundRectShape(radii, null, null));
        this.context = context2;
    }

    public void setRadius(float f) {
        float a2 = df.a(f);
        setShape(new RoundRectShape(new float[]{a2, a2, a2, a2, a2, a2, a2, a2}, null, null));
    }

    public void setColor(int i) {
        getPaint().setColor(this.context.getResources().getColor(i));
    }

    public void setColorDerect(int i) {
        getPaint().setColor(i);
    }

    public void setColor(String str) {
        if (!TextUtils.isEmpty(str)) {
            getPaint().setColor(Color.parseColor(str));
        }
    }
}
