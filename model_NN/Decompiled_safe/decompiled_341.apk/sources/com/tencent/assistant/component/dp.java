package com.tencent.assistant.component;

/* compiled from: ProGuard */
class dp implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SwitchButton f1025a;

    dp(SwitchButton switchButton) {
        this.f1025a = switchButton;
    }

    public void run() {
        if (this.f1025a.isSwitchListenerOn) {
            this.f1025a.onSwitchButtonClickListener.onSwitchButtonClick(this.f1025a.mView, this.f1025a.isSwitchOn);
            this.f1025a.onSwitchButtonClickListener.userActionReport(this.f1025a.mView);
        }
    }
}
