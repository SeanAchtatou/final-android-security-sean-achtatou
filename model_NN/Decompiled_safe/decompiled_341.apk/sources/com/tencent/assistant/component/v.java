package com.tencent.assistant.component;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.login.a.a;
import com.tencent.assistant.module.callback.k;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentReply;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.utils.ct;
import com.tencent.connect.common.Constants;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
class v implements k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentDetailView f1218a;

    v(CommentDetailView commentDetailView) {
        this.f1218a = commentDetailView;
    }

    public void a(int i, int i2, boolean z, CommentTagInfo commentTagInfo, List<CommentDetail> list, List<CommentDetail> list2, List<CommentTagInfo> list3, boolean z2, byte[] bArr, CommentDetail commentDetail) {
        if (this.f1218a.isLoadingFirstPageData && z) {
            boolean unused = this.f1218a.isLoadingFirstPageData = false;
        }
        if (this.f1218a.pageChangeListener == null) {
            this.f1218a.onCommentListResponseHandle(i2, z, commentTagInfo, list, list2, list3, z2, commentDetail);
            return;
        }
        ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1, null, this.f1218a.pageMessageHandler);
        viewInvalidateMessage.arg1 = i2;
        HashMap hashMap = new HashMap();
        hashMap.put("isFirstPage", Boolean.valueOf(z));
        hashMap.put("data", list);
        hashMap.put("selectedData", list2);
        hashMap.put("hasNext", Boolean.valueOf(z2));
        hashMap.put("curVerComment", commentDetail);
        hashMap.put("taglist", list3);
        hashMap.put("reqTagInfo", commentTagInfo);
        viewInvalidateMessage.params = hashMap;
        this.f1218a.pageChangeListener.sendMessage(viewInvalidateMessage);
    }

    public void a(int i, int i2, CommentDetail commentDetail, long j) {
        if (this.f1218a.pageChangeListener == null) {
            this.f1218a.onWriteCommentFinishHandle(i2, commentDetail, j);
            return;
        }
        ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(2, null, this.f1218a.pageMessageHandler);
        viewInvalidateMessage.arg1 = i2;
        HashMap hashMap = new HashMap();
        hashMap.put("comment", commentDetail);
        hashMap.put("oldCommentId", Long.valueOf(j));
        viewInvalidateMessage.params = hashMap;
        this.f1218a.pageChangeListener.sendMessage(viewInvalidateMessage);
    }

    public void a(int i, int i2, long j, String str, String str2, long j2) {
        if (a.b(i2) && i2 == 0) {
            CommentReply commentReply = new CommentReply();
            commentReply.d = str2;
            commentReply.c = str;
            commentReply.b = j2;
            commentReply.e = true;
            if (this.f1218a.selectComments != null) {
                for (int i3 = 0; i3 < this.f1218a.selectComments.size(); i3++) {
                    CommentDetail commentDetail = (CommentDetail) this.f1218a.selectComments.get(i3);
                    if (commentDetail.h == j) {
                        ac acVar = (ac) this.f1218a.selectCommentList.getChildAt(i3).getTag();
                        commentDetail.k.add(commentReply);
                        if (acVar.g) {
                            acVar.f.removeViewAt(acVar.f.getChildCount() - 1);
                            acVar.f.getChildAt(acVar.f.getChildCount() - 1).findViewById(R.id.divider).setVisibility(0);
                            View access$1100 = this.f1218a.getReplyView(true, i3, str2, str, j2);
                            access$1100.findViewById(R.id.divider).setVisibility(8);
                            acVar.f.addView(access$1100);
                            return;
                        }
                        this.f1218a.selectCommentList.removeViewAt(i3);
                        this.f1218a.selectCommentList.addView(this.f1218a.fillValues(commentDetail, i3), i3);
                        return;
                    }
                }
            }
            if (this.f1218a.comments != null) {
                for (int i4 = 0; i4 < this.f1218a.comments.size(); i4++) {
                    CommentDetail commentDetail2 = (CommentDetail) this.f1218a.comments.get(i4);
                    if (commentDetail2.h == j) {
                        ac acVar2 = (ac) this.f1218a.commentListView.getChildAt(i4).getTag();
                        commentDetail2.k.add(commentReply);
                        if (acVar2.g) {
                            acVar2.f.removeViewAt(acVar2.f.getChildCount() - 1);
                            acVar2.f.getChildAt(acVar2.f.getChildCount() - 1).findViewById(R.id.divider).setVisibility(0);
                            View access$11002 = this.f1218a.getReplyView(commentReply.e, i4, str2, str, j2);
                            access$11002.findViewById(R.id.divider).setVisibility(8);
                            acVar2.f.addView(access$11002);
                            return;
                        }
                        this.f1218a.commentListView.removeViewAt(i4);
                        this.f1218a.commentListView.addView(this.f1218a.fillValues(commentDetail2, i4), i4);
                        return;
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v36, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v3, resolved type: com.tencent.assistant.protocol.jce.CommentDetail} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v59, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v3, resolved type: com.tencent.assistant.protocol.jce.CommentDetail} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00c8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r16, int r17, long r18, java.lang.String r20, int r21, long r22) {
        /*
            r15 = this;
            boolean r2 = com.tencent.assistant.login.a.a.b(r17)
            if (r2 != 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            r10 = 0
            if (r17 != 0) goto L_0x0197
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            android.content.Context r2 = r2.mContext
            com.tencent.assistant.component.CommentDetailView r3 = r15.f1218a
            android.content.Context r3 = r3.mContext
            r4 = 2131362042(0x7f0a00fa, float:1.8343853E38)
            java.lang.String r3 = r3.getString(r4)
            r4 = 0
            android.widget.Toast r2 = android.widget.Toast.makeText(r2, r3, r4)
            r2.show()
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            java.util.ArrayList r2 = r2.selectComments
            if (r2 == 0) goto L_0x01b8
            r2 = 0
            r11 = r2
        L_0x002f:
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            java.util.ArrayList r2 = r2.selectComments
            int r2 = r2.size()
            if (r11 >= r2) goto L_0x01b8
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            java.util.ArrayList r2 = r2.selectComments
            java.lang.Object r2 = r2.get(r11)
            com.tencent.assistant.protocol.jce.CommentDetail r2 = (com.tencent.assistant.protocol.jce.CommentDetail) r2
            long r2 = r2.h
            int r2 = (r2 > r18 ? 1 : (r2 == r18 ? 0 : -1))
            if (r2 != 0) goto L_0x018d
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            java.util.ArrayList r2 = r2.selectComments
            java.lang.Object r2 = r2.get(r11)
            r10 = r2
            com.tencent.assistant.protocol.jce.CommentDetail r10 = (com.tencent.assistant.protocol.jce.CommentDetail) r10
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            android.widget.LinearLayout r2 = r2.selectCommentList
            android.view.View r12 = r2.getChildAt(r11)
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            com.tencent.assistant.component.CommentDetailView r3 = r15.f1218a
            android.widget.LinearLayout r3 = r3.selectCommentList
            android.view.View r3 = r3.getChildAt(r11)
            java.lang.Object r3 = r3.getTag()
            com.tencent.assistant.component.ac r3 = (com.tencent.assistant.component.ac) r3
            r8 = 0
            r9 = 1
            r4 = r20
            r5 = r21
            r6 = r22
            r2.updateComment(r3, r4, r5, r6, r8, r9)
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            android.widget.LinearLayout r2 = r2.selectCommentList
            r2.removeViewAt(r11)
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            java.util.ArrayList r2 = r2.selectComments
            r2.remove(r11)
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            java.util.ArrayList r2 = r2.selectComments
            int r2 = r2.size()
            if (r2 != 0) goto L_0x00a6
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            r3 = 8
            r2.setSelectVisibility(r3)
        L_0x00a6:
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            r2.showListView()
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            android.widget.LinearLayout r2 = r2.commentListView
            r3 = 0
            r2.addView(r12, r3)
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            com.tencent.assistant.component.CommentFooterView r2 = r2.mFooterView
            r3 = 0
            r2.setVisibility(r3)
            r3 = r10
        L_0x00c0:
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            java.util.ArrayList r2 = r2.comments
            if (r2 == 0) goto L_0x01b6
            r2 = 0
            r12 = r2
        L_0x00ca:
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            java.util.ArrayList r2 = r2.comments
            int r2 = r2.size()
            if (r12 >= r2) goto L_0x01b6
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            android.widget.LinearLayout r2 = r2.commentListView
            r4 = 0
            android.view.View r13 = r2.getChildAt(r4)
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            java.util.ArrayList r2 = r2.comments
            r4 = 0
            java.lang.Object r2 = r2.get(r4)
            r10 = r2
            com.tencent.assistant.protocol.jce.CommentDetail r10 = (com.tencent.assistant.protocol.jce.CommentDetail) r10
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            java.util.ArrayList r2 = r2.comments
            java.lang.Object r2 = r2.get(r12)
            com.tencent.assistant.protocol.jce.CommentDetail r2 = (com.tencent.assistant.protocol.jce.CommentDetail) r2
            long r4 = r2.h
            int r2 = (r4 > r18 ? 1 : (r4 == r18 ? 0 : -1))
            if (r2 != 0) goto L_0x0192
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            java.util.ArrayList r2 = r2.comments
            java.lang.Object r2 = r2.get(r12)
            r11 = r2
            com.tencent.assistant.protocol.jce.CommentDetail r11 = (com.tencent.assistant.protocol.jce.CommentDetail) r11
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            android.widget.LinearLayout r2 = r2.commentListView
            android.view.View r14 = r2.getChildAt(r12)
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            java.lang.Object r3 = r14.getTag()
            com.tencent.assistant.component.ac r3 = (com.tencent.assistant.component.ac) r3
            com.tencent.assistant.component.CommentDetailView r4 = r15.f1218a
            android.content.Context r4 = r4.mContext
            r5 = 2131362020(0x7f0a00e4, float:1.8343809E38)
            java.lang.String r8 = r4.getString(r5)
            r9 = 1
            r4 = r20
            r5 = r21
            r6 = r22
            r2.updateComment(r3, r4, r5, r6, r8, r9)
            if (r12 == 0) goto L_0x0160
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            android.widget.LinearLayout r2 = r2.commentListView
            r2.removeViewAt(r12)
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            android.widget.LinearLayout r2 = r2.commentListView
            r3 = 0
            r2.addView(r14, r3)
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            java.lang.Object r3 = r13.getTag()
            com.tencent.assistant.component.ac r3 = (com.tencent.assistant.component.ac) r3
            java.lang.String r4 = r10.g
            int r5 = r10.b
            long r6 = r10.m
            java.lang.String r8 = r10.c
            r9 = 0
            r2.updateComment(r3, r4, r5, r6, r8, r9)
        L_0x0160:
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            com.tencent.assistant.component.CommentDetailView$CommentSucceedListener r2 = r2.mListener
            if (r2 == 0) goto L_0x0006
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            com.tencent.assistant.component.CommentDetailView$CommentSucceedListener r3 = r2.mListener
            if (r17 != 0) goto L_0x01b4
            r2 = 1
        L_0x0171:
            r0 = r18
            r3.onCommentSucceed(r2, r0)
            if (r17 != 0) goto L_0x0006
            if (r11 == 0) goto L_0x0006
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            com.tencent.assistant.component.CommentDetailView$CommentSucceedListener r2 = r2.mListener
            long r3 = r11.h
            int r7 = r11.d
            r5 = r20
            r6 = r21
            r2.onGetOldComment(r3, r5, r6, r7)
            goto L_0x0006
        L_0x018d:
            int r2 = r11 + 1
            r11 = r2
            goto L_0x002f
        L_0x0192:
            int r2 = r12 + 1
            r12 = r2
            goto L_0x00ca
        L_0x0197:
            com.tencent.assistant.component.CommentDetailView r2 = r15.f1218a
            android.content.Context r2 = r2.mContext
            com.tencent.assistant.component.CommentDetailView r3 = r15.f1218a
            android.content.Context r3 = r3.mContext
            r4 = 2131362039(0x7f0a00f7, float:1.8343847E38)
            java.lang.String r3 = r3.getString(r4)
            r4 = 0
            android.widget.Toast r2 = android.widget.Toast.makeText(r2, r3, r4)
            r2.show()
            r11 = r10
            goto L_0x0160
        L_0x01b4:
            r2 = 0
            goto L_0x0171
        L_0x01b6:
            r11 = r3
            goto L_0x0160
        L_0x01b8:
            r3 = r10
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.component.v.a(int, int, long, java.lang.String, int, long):void");
    }

    public void a(int i, int i2, long j, int i3, long j2) {
        long j3;
        if (i2 == -800 && this.f1218a.comments != null) {
            int i4 = 0;
            while (true) {
                int i5 = i4;
                if (i5 < this.f1218a.comments.size()) {
                    View childAt = this.f1218a.commentListView.getChildAt(i5);
                    if (!(childAt == null || childAt.getTag() == null)) {
                        ac acVar = (ac) childAt.getTag();
                        CommentDetail commentDetail = (CommentDetail) this.f1218a.comments.get(i5);
                        if (!(acVar == null || commentDetail == null || commentDetail.h != j)) {
                            Drawable drawable = this.f1218a.getResources().getDrawable(R.drawable.pinglun_icon_zan);
                            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                            Drawable drawable2 = this.f1218a.getResources().getDrawable(R.drawable.pinglun_icon_yizan);
                            drawable2.setBounds(0, 0, drawable2.getMinimumWidth(), drawable2.getMinimumHeight());
                            long parseInt = (long) Integer.parseInt(acVar.h.getTag(R.id.comment_praise_count) + Constants.STR_EMPTY);
                            if (((Boolean) acVar.h.getTag()).booleanValue()) {
                                acVar.h.setTag(false);
                                acVar.h.setCompoundDrawables(drawable, null, null, null);
                                j3 = parseInt - 1;
                                acVar.h.setText(ct.a(j3) + Constants.STR_EMPTY);
                                acVar.h.setTextColor(Color.parseColor("#6e6e6e"));
                            } else {
                                acVar.h.setTag(true);
                                acVar.h.setCompoundDrawables(drawable2, null, null, null);
                                j3 = 1 + parseInt;
                                acVar.h.setText(ct.a(j3) + Constants.STR_EMPTY);
                                acVar.h.setTextColor(Color.parseColor("#b68a46"));
                            }
                            acVar.h.setTag(R.id.comment_praise_count, Long.valueOf(j3));
                            return;
                        }
                    }
                    i4 = i5 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
