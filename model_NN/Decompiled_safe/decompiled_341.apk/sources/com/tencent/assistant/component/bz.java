package com.tencent.assistant.component;

import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class bz implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopViewDialog f981a;

    bz(PopViewDialog popViewDialog) {
        this.f981a = popViewDialog;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        String obj = editable.toString();
        this.f981a.overMoreText.setText(String.format(this.f981a.mContext.getString(R.string.comment_txt_tips), Integer.valueOf(obj.length())));
        if (obj.length() > 100) {
            this.f981a.overMoreText.setVisibility(0);
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.f981a.overMoreText.getText().toString());
            ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(-65536);
            ForegroundColorSpan foregroundColorSpan2 = new ForegroundColorSpan(this.f981a.mContext.getResources().getColor(R.color.comment_over_txt_tips));
            spannableStringBuilder.setSpan(foregroundColorSpan, 0, this.f981a.overMoreText.getText().toString().indexOf("/"), 33);
            spannableStringBuilder.setSpan(foregroundColorSpan2, this.f981a.overMoreText.getText().toString().indexOf("/") + 1, this.f981a.overMoreText.getText().toString().length(), 18);
            this.f981a.overMoreText.setText(spannableStringBuilder);
        } else if (obj.length() > 0) {
            this.f981a.overMoreText.setVisibility(0);
            this.f981a.overMoreText.setTextColor(this.f981a.mContext.getResources().getColor(R.color.comment_over_txt_tips));
            String str = null;
            CharSequence text = this.f981a.errorTips.getText();
            if (text != null) {
                str = text.toString();
            }
            String string = this.f981a.mContext.getString(R.string.comment_expendcount_error);
            if (this.f981a.errorTips.getVisibility() == 0 && !TextUtils.isEmpty(str) && str.equals(string)) {
                this.f981a.errorTips.setVisibility(8);
            }
        } else {
            this.f981a.overMoreText.setVisibility(0);
        }
    }
}
