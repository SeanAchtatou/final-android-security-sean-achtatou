package com.tencent.assistant.model;

import java.io.Serializable;

/* compiled from: ProGuard */
public class PluginStartEntry implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private int f1630a;
    private String b;
    private String c;
    private int d;
    private String e;
    private String f;

    public PluginStartEntry(int i, String str, String str2, int i2, String str3, String str4) {
        this.f1630a = i;
        this.b = str;
        this.c = str2;
        this.d = i2;
        this.e = str3;
        this.f = str4;
    }

    public String a() {
        return this.c;
    }

    public int b() {
        return this.d;
    }

    public void a(int i) {
        this.d = i;
    }

    public String c() {
        return this.e;
    }

    public int d() {
        return this.f1630a;
    }
}
