package com.tencent.assistant.model;

import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import com.tencent.assistant.protocol.jce.ListRecommend;
import com.tencent.assistant.protocol.jce.ListRecommendIIT;
import com.tencent.assistant.protocol.jce.ListRecommendTop;
import com.tencent.assistant.protocol.jce.RecommendIT;
import com.tencent.assistant.utils.ct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ProGuard */
public class g {
    private static final Spanned c = ct.f2674a;

    /* renamed from: a  reason: collision with root package name */
    private ListRecommend f1664a;
    private Map<String, Spanned> b;

    public g(ListRecommend listRecommend) {
        this.f1664a = listRecommend;
        if (this.f1664a != null) {
            ListRecommendIIT b2 = listRecommend.b();
            ArrayList<RecommendIT> c2 = listRecommend.c();
            ListRecommendTop d = listRecommend.d();
            if (this.b == null) {
                this.b = new HashMap(3);
            }
            if (b2 != null && !TextUtils.isEmpty(b2.b)) {
                this.b.put(b2.b, Html.fromHtml(b2.b));
            }
            if (c2 != null) {
                Iterator<RecommendIT> it = c2.iterator();
                while (it.hasNext()) {
                    RecommendIT next = it.next();
                    if (next != null && !TextUtils.isEmpty(next.b)) {
                        try {
                            this.b.put(next.b, Html.fromHtml(next.b));
                        } catch (Exception e) {
                        }
                    }
                }
            }
            if (d != null) {
                if (!TextUtils.isEmpty(d.b)) {
                    try {
                        this.b.put(d.b, Html.fromHtml(d.b));
                    } catch (Exception e2) {
                    }
                }
                if (!TextUtils.isEmpty(d.d)) {
                    try {
                        this.b.put(d.d, Html.fromHtml(d.d));
                    } catch (Exception e3) {
                    }
                }
            }
        }
    }

    public int a() {
        if (this.f1664a != null) {
            return this.f1664a.a();
        }
        return 0;
    }

    public ListRecommend b() {
        return this.f1664a;
    }

    public Spanned a(String str) {
        if (TextUtils.isEmpty(str)) {
            return c;
        }
        Spanned spanned = null;
        if (this.b != null) {
            spanned = this.b.get(str);
        }
        if (spanned != null) {
            return spanned;
        }
        try {
            return Html.fromHtml(str);
        } catch (Exception e) {
            e.printStackTrace();
            return spanned;
        }
    }
}
