package com.tencent.assistant.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.component.listener.OnUserTaskClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.utils.ct;

/* compiled from: ProGuard */
class ck extends OnUserTaskClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadActivity f452a;

    ck(DownloadActivity downloadActivity) {
        this.f452a = downloadActivity;
    }

    public void OnUserTaskClick(View view) {
        String scheme;
        Uri parse = Uri.parse(this.f452a.Z.g.f1970a);
        Intent intent = new Intent("android.intent.action.VIEW", parse);
        if (b.a(this.f452a.n, intent) && (scheme = intent.getScheme()) != null) {
            if (scheme.equals("tmast") || scheme.equals("http")) {
                Bundle bundle = new Bundle();
                int a2 = ct.a(parse.getQueryParameter("scene"), 0);
                if (a2 != 0) {
                    bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, a2);
                } else {
                    bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f452a.f());
                }
                b.b(this.f452a.n, this.f452a.Z.g.f1970a, bundle);
            }
        }
    }
}
