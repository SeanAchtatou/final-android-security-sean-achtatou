package com.tencent.assistant.activity;

import android.util.Log;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.f.a;
import java.util.ArrayList;

/* compiled from: ProGuard */
class es implements a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PhotoBackupNewActivity f561a;

    es(PhotoBackupNewActivity photoBackupNewActivity) {
        this.f561a = photoBackupNewActivity;
    }

    public void a() {
        Log.d("PhotoBackupNewActivity", "usbConnected");
        this.f561a.x();
    }

    public void b() {
        Log.d("PhotoBackupNewActivity", "wifiConnected");
        this.f561a.y();
    }

    public void c() {
        Log.d("PhotoBackupNewActivity", "wifiDisable");
        this.f561a.c(2);
    }

    public void a(int i) {
        Log.d("PhotoBackupNewActivity", "requestFailed： " + i);
        if (!com.tencent.connector.ipc.a.a().d()) {
            this.f561a.d(4);
            Toast.makeText(AstApp.i(), (int) R.string.photo_backup_get_failed, 1).show();
            this.f561a.finish();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.PhotoBackupNewActivity.a(com.tencent.assistant.activity.PhotoBackupNewActivity, boolean):boolean
     arg types: [com.tencent.assistant.activity.PhotoBackupNewActivity, int]
     candidates:
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.activity.PhotoBackupNewActivity.a(com.tencent.assistant.activity.PhotoBackupNewActivity, boolean):boolean */
    public void a(Object obj) {
        Log.d("PhotoBackupNewActivity", "requestSuccess newtag:");
        if (com.tencent.connector.ipc.a.a().d()) {
            if (this.f561a.Q) {
                boolean unused = this.f561a.Q = false;
            } else {
                return;
            }
        }
        this.f561a.d(4);
        ArrayList arrayList = (ArrayList) obj;
        if (arrayList == null || arrayList.size() == 0) {
            this.f561a.c(2);
            return;
        }
        this.f561a.c(1);
        this.f561a.y.updateList(arrayList);
    }

    public void b(int i) {
        Log.d("PhotoBackupNewActivity", "invitePcFailed:" + i);
        if (!com.tencent.connector.ipc.a.a().d()) {
            this.f561a.j();
            this.f561a.D();
        }
    }

    public void b(Object obj) {
        Log.d("PhotoBackupNewActivity", "invitePcSuccess:");
    }
}
