package com.tencent.assistant.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.ImageView;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.link.b;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.cm;
import com.tencent.assistant.model.q;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.f;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.bg;
import com.tencent.assistantv2.activity.GuideActivity;
import com.tencent.assistantv2.st.business.StartUpCostTimeSTManager;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class SplashActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f384a = false;
    private final int b = EventDispatcherEnum.CACHE_EVENT_START;
    /* access modifiers changed from: private */
    public boolean c = false;
    private q d = null;
    /* access modifiers changed from: private */
    public boolean e = false;
    private SplashScreenReceiver f = null;
    private ImageView g = null;
    private Runnable h = new gk(this);

    public int a() {
        return STConst.ST_PAGE_SPLASH;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z = true;
        AstApp.i().a();
        try {
            k.a(StartUpCostTimeSTManager.TIMETYPE.START, System.currentTimeMillis(), 0);
            XLog.d("qt4atest", "TIMETYPE.START");
        } catch (Throwable th) {
        }
        super.onCreate(bundle);
        Bundle a2 = bg.a(getIntent());
        if (a2 != null) {
            String string = a2.getString("appCustom");
            if (!TextUtils.isEmpty(string)) {
                b.a(this, string);
                finish();
                return;
            }
        }
        k.a(StartUpCostTimeSTManager.TIMETYPE.B5_TIME, System.currentTimeMillis(), 0);
        TemporaryThreadManager.get().start(new gg(this));
        AstApp.i().e();
        f.a((byte) 1);
        if (m.a().a("key_has_loaded_5_2", false)) {
            z = false;
        }
        f384a = z;
        if (z) {
            TemporaryThreadManager.get().start(new gh(this));
            startActivity(new Intent(this, GuideActivity.class));
            finish();
            return;
        }
        b();
    }

    /* compiled from: ProGuard */
    public class SplashScreenReceiver extends BroadcastReceiver {
        public SplashScreenReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            SplashActivity.this.c();
        }
    }

    public void a(int i) {
        k.a(new STInfoV2(a(), STConst.ST_DEFAULT_SLOT, i, STConst.ST_DEFAULT_SLOT, 100));
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b() {
        /*
            r5 = this;
            r3 = 1
            com.tencent.assistantv2.st.business.StartUpCostTimeSTManager$TIMETYPE r0 = com.tencent.assistantv2.st.business.StartUpCostTimeSTManager.TIMETYPE.B11_TIME
            long r1 = java.lang.System.currentTimeMillis()
            com.tencent.assistantv2.st.k.a(r0, r1, r3)
            com.tencent.assistant.manager.cm r2 = new com.tencent.assistant.manager.cm
            r2.<init>()
            com.tencent.assistant.model.q r0 = r2.a()
            r5.d = r0
            com.tencent.assistant.model.q r0 = r5.d
            if (r0 == 0) goto L_0x00a6
            com.tencent.assistant.model.q r0 = r5.d
            int r0 = r0.f()
            if (r0 > 0) goto L_0x00ae
            r0 = 3000(0xbb8, float:4.204E-42)
            r1 = r0
        L_0x0024:
            android.view.LayoutInflater r0 = r5.getLayoutInflater()     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r3 = 2130903379(0x7f030153, float:1.7413574E38)
            r4 = 0
            android.view.View r3 = r0.inflate(r3, r4)     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r0 = 2131166431(0x7f0704df, float:1.7947107E38)
            android.view.View r0 = r3.findViewById(r0)     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            android.widget.ImageView r0 = (android.widget.ImageView) r0     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r5.g = r0     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            android.graphics.Bitmap r0 = r2.b()     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            if (r0 == 0) goto L_0x00a6
            boolean r2 = r0.isRecycled()     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            if (r2 != 0) goto L_0x00a6
            android.widget.ImageView r2 = r5.g     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r2.setImageBitmap(r0)     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            android.view.Window r0 = r5.getWindow()     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r2 = 1024(0x400, float:1.435E-42)
            r4 = 1024(0x400, float:1.435E-42)
            r0.setFlags(r2, r4)     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r5.setContentView(r3)     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r0 = 1
            r5.c = r0     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            com.tencent.assistant.activity.SplashActivity$SplashScreenReceiver r0 = new com.tencent.assistant.activity.SplashActivity$SplashScreenReceiver     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r0.<init>()     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r5.f = r0     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            android.content.IntentFilter r0 = new android.content.IntentFilter     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r0.<init>()     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            java.lang.String r2 = "android.intent.action.SCREEN_OFF"
            r0.addAction(r2)     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            com.qq.AppService.AstApp r2 = com.qq.AppService.AstApp.i()     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            com.tencent.assistant.activity.SplashActivity$SplashScreenReceiver r3 = r5.f     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r2.registerReceiver(r3, r0)     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            android.widget.ImageView r0 = r5.g     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            com.tencent.assistant.activity.gi r2 = new com.tencent.assistant.activity.gi     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r2.<init>(r5)     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r0.setOnClickListener(r2)     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            android.content.Intent r0 = r5.getIntent()     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            java.lang.String r2 = "preActivityTagName"
            r3 = 2000(0x7d0, float:2.803E-42)
            int r0 = r0.getIntExtra(r2, r3)     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r5.a(r0)     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            android.os.Handler r0 = com.tencent.assistant.utils.ba.a()     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            java.lang.Runnable r2 = r5.h     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            long r3 = (long) r1     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r0.postDelayed(r2, r3)     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            com.tencent.assistant.utils.TemporaryThreadManager r0 = com.tencent.assistant.utils.TemporaryThreadManager.get()     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            com.tencent.assistant.activity.gj r1 = new com.tencent.assistant.activity.gj     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r1.<init>(r5)     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
            r0.start(r1)     // Catch:{ Throwable -> 0x00b5, all -> 0x00b3 }
        L_0x00a6:
            boolean r0 = r5.c
            if (r0 != 0) goto L_0x00ad
            r5.c()
        L_0x00ad:
            return
        L_0x00ae:
            int r0 = r0 * 1000
            r1 = r0
            goto L_0x0024
        L_0x00b3:
            r0 = move-exception
            throw r0
        L_0x00b5:
            r0 = move-exception
            goto L_0x00a6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.activity.SplashActivity.b():void");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.e = true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.e) {
            c();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        d();
        super.onDestroy();
        if (this.f != null) {
            try {
                AstApp.i().unregisterReceiver(this.f);
            } catch (Exception e2) {
            }
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || !this.c) {
            return false;
        }
        c();
        return true;
    }

    /* access modifiers changed from: private */
    public void c() {
        int i = 2000;
        if (this.c) {
            ba.a().removeCallbacks(this.h);
            if (this.d.g() > 0) {
                new cm().a(this.d);
            }
            this.c = false;
            i = a();
        }
        Intent intent = new Intent();
        intent.setClassName(AstApp.i().getPackageName(), "com.tencent.assistantv2.activity.MainActivity");
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, Integer.valueOf(i));
        intent.addFlags(67108864);
        try {
            startActivity(intent);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        if (!AstApp.r()) {
            TemporaryThreadManager.get().start(new gl(this));
        }
        finish();
    }

    private void d() {
        try {
            if (this.g != null && this.g.getDrawable() != null) {
                Drawable drawable = this.g.getDrawable();
                drawable.setCallback(null);
                if (drawable instanceof BitmapDrawable) {
                    Bitmap bitmap = ((BitmapDrawable) this.g.getDrawable()).getBitmap();
                    this.g.setImageDrawable(null);
                    this.g.setBackgroundDrawable(null);
                    this.g.setImageBitmap(null);
                    if (bitmap != null && !bitmap.isRecycled()) {
                        bitmap.recycle();
                    }
                }
            }
        } catch (Throwable th) {
        }
    }
}
