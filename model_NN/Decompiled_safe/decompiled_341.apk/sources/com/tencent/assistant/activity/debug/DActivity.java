package com.tencent.assistant.activity.debug;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.notification.NotificationService;
import com.tencent.assistant.manager.notification.r;
import com.tencent.assistant.manager.notification.y;
import com.tencent.assistant.plugin.PluginIPCClient;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.plugin.mgr.e;
import com.tencent.assistant.protocol.jce.ActionLink;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.assistant.protocol.jce.PushIconInfo;
import com.tencent.assistant.protocol.jce.PushInfo;
import com.tencent.assistant.protocol.jce.PushInfoExtend;
import com.tencent.assistant.utils.cv;
import com.tencent.assistantv2.model.h;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class DActivity extends Activity implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    PluginIPCClient f471a = null;
    private ListView b;
    /* access modifiers changed from: private */
    public ImageView c;
    /* access modifiers changed from: private */
    public EditText d;
    /* access modifiers changed from: private */
    public EditText e;
    private ViewGroup f;
    private View.OnClickListener g = new af(this);

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.d_layout);
        this.f = (ViewGroup) findViewById(R.id.btn_group);
        this.b = (ListView) findViewById(R.id.plugin_list);
        this.c = (ImageView) findViewById(R.id.pluginStartIcon);
        this.d = (EditText) findViewById(R.id.input);
        this.e = (EditText) findViewById(R.id.down_input);
        e.a(true);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, this);
        a("webview", new a(this));
        a("安装插件", new m(this));
        a("清理所有插件(重启生效)", new ac(this));
        a("触发登录", new ag(this));
        a("获取登录", new ah(this));
        a("图片备份", new ai(this));
        a("拉取Push", new aj(this));
        a("快捷方式", new ak(this));
        a("下载调试", this.g);
        a("跳入插件", new al(this));
        a("进入更新", new b(this));
        a("个人中心", new c(this));
        a("拉取更新push", new d(this));
        a("测试Push样式", new e(this));
        a("触发登录", new g(this));
        a("评论测试主人态", new h(this));
        a("Main跳转", new i(this));
        a("q立方主题", new j(this));
        a("重新刷新插件列表", new k(this));
        a("拉取个人中心数据", new l(this));
        a("外部直接下载", new n(this));
        a("外部下载", new o(this));
        a("dump JsBrige", new p(this));
        a("引导功能:" + (m.a().a("test_guide", false) ? "开" : "关"), new q(this));
        a("App信息上报（永久root）", new r(this));
        a("App信息上报（临时root）", new t(this));
        a("下载推荐", new v(this));
        a("临时root", new x(this));
        a("大文件清理", new z(this));
        a("全量更新请求", new ab(this));
        a("强制全量请求", new ad(this));
        a();
    }

    /* access modifiers changed from: private */
    public PushInfo a(int i) {
        PushInfo pushInfo = new PushInfo();
        pushInfo.f2265a = 0;
        pushInfo.b = "这是个测试的push这是个测试的这是个测试的这是个测试的这是个测试的这是个测试的这是个测试的这是个测试的";
        pushInfo.c = "QQ空间这是个测试的这是个测试的这是个测试的这是个测试的这是个测试的这是个测试的这是个测试的这是个测试的";
        ActionUrl actionUrl = new ActionUrl();
        actionUrl.f1970a = "tmast://update";
        actionUrl.b = 0;
        pushInfo.d = actionUrl;
        ArrayList<PushIconInfo> arrayList = new ArrayList<>();
        PushIconInfo pushIconInfo = new PushIconInfo();
        pushIconInfo.f2264a = 2;
        pushIconInfo.b = "com.tencent.qqmusic";
        pushIconInfo.c = 73;
        arrayList.add(pushIconInfo);
        PushIconInfo pushIconInfo2 = new PushIconInfo();
        pushIconInfo2.f2264a = 2;
        pushIconInfo2.b = "com.tencent.qqmusic";
        pushIconInfo2.c = 73;
        arrayList.add(pushIconInfo2);
        PushIconInfo pushIconInfo3 = new PushIconInfo();
        pushIconInfo3.f2264a = 2;
        pushIconInfo3.b = "com.tencent.qqmusic";
        pushIconInfo3.c = 73;
        arrayList.add(pushIconInfo3);
        pushInfo.e = arrayList;
        PushIconInfo pushIconInfo4 = new PushIconInfo();
        pushIconInfo4.f2264a = 2;
        pushIconInfo4.b = "com.tencent.android.qqdownloader";
        pushIconInfo4.c = 0;
        pushInfo.f = pushIconInfo4;
        pushInfo.g = (byte) i;
        HashMap hashMap = new HashMap();
        hashMap.put("extra_key_1", "立省");
        hashMap.put("extra_key_2", "<font color=\"#5AC65C\">0.00K</font>");
        hashMap.put("extra_key_3", "下载");
        hashMap.put("extra_key_4", "5");
        pushInfo.h = hashMap;
        pushInfo.i = actionUrl;
        pushInfo.j = 10;
        pushInfo.k = 5;
        PushInfoExtend pushInfoExtend = new PushInfoExtend();
        HashMap hashMap2 = new HashMap();
        hashMap2.put((byte) 0, new ActionLink(new ActionUrl("tmast://update", 0), "000"));
        hashMap2.put((byte) 1, new ActionLink(new ActionUrl("tmast://update", 0), "111"));
        hashMap2.put((byte) 2, new ActionLink(new ActionUrl("tmast://update", 0), "222"));
        hashMap2.put((byte) 3, new ActionLink(new ActionUrl("tmast://update", 0), "333"));
        hashMap2.put((byte) 4, new ActionLink(new ActionUrl("tmast://update", 0), "444"));
        pushInfoExtend.a(hashMap2);
        pushInfoExtend.b = "com.tencent.mobileqq";
        pushInfoExtend.f2266a = "asfasfsa";
        pushInfo.l = pushInfoExtend;
        return pushInfo;
    }

    private void a(String str, View.OnClickListener onClickListener) {
        Button button = new Button(this);
        button.setText(str);
        button.setOnClickListener(onClickListener);
        this.f.addView(button);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.f471a != null) {
            this.f471a.close(this);
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        List<PluginInfo> a2 = d.b().a(-1);
        ArrayList arrayList = new ArrayList();
        for (PluginInfo next : a2) {
            List<PluginInfo.PluginEntry> pluginEntryList = next.getPluginEntryList();
            if (pluginEntryList != null) {
                for (PluginInfo.PluginEntry next2 : pluginEntryList) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("name", next2.getName());
                    hashMap.put("packagename", next.getPackageName());
                    hashMap.put("version", Integer.valueOf(next.getVersion()));
                    hashMap.put("sa", next2.getStartActivity());
                    hashMap.put("launchApplication", next.getLaunchApplication());
                    hashMap.put("inprocess", Integer.valueOf(next.getInProcess()));
                    hashMap.put("icon", next2.getIcon());
                    arrayList.add(hashMap);
                }
            }
        }
        this.b.setAdapter((ListAdapter) new SimpleAdapter(this, arrayList, R.layout.plugin_list_item, new String[]{"name", "packagename", "version", "sa", "launchApplication"}, new int[]{R.id.name, R.id.pn, R.id.v, R.id.sa, R.id.app}));
        this.b.setOnItemClickListener(new ae(this, arrayList));
    }

    public Notification a(Bitmap bitmap) {
        return y.a(AstApp.i(), R.drawable.logo32, b(bitmap), "stitletitletitletitletitletitletitletitletitletitle", System.currentTimeMillis(), b(), null, true, false);
    }

    private PendingIntent b() {
        Intent intent = new Intent(AstApp.i(), NotificationService.class);
        intent.putExtra("notification_id", 121);
        ActionUrl actionUrl = new ActionUrl();
        actionUrl.f1970a = "http://m.baidu.com";
        intent.putExtra("notification_action", actionUrl);
        return PendingIntent.getService(AstApp.i(), 121, intent, 268435456);
    }

    private RemoteViews b(Bitmap bitmap) {
        RemoteViews remoteViews = new RemoteViews(AstApp.i().getPackageName(), (int) R.layout.notification_card_subscription_download);
        r rVar = new r(AstApp.i(), true);
        rVar.c();
        Integer valueOf = Integer.valueOf(AstApp.i().getResources().getColor(R.color.blue_color));
        if (valueOf != null) {
            remoteViews.setTextColor(R.id.title, valueOf.intValue());
        }
        Integer a2 = rVar.a();
        if (a2 != null) {
            remoteViews.setTextColor(R.id.content, a2.intValue());
        }
        if (bitmap == null || bitmap.isRecycled()) {
            remoteViews.setImageViewResource(R.id.big_icon, R.drawable.logo72);
        } else {
            remoteViews.setImageViewBitmap(R.id.big_icon, bitmap);
        }
        remoteViews.setFloat(R.id.title, "setTextSize", rVar.d());
        remoteViews.setFloat(R.id.content, "setTextSize", rVar.b());
        remoteViews.setTextViewText(R.id.title, "stitletitletitletitletitletitletitletitletitletitle");
        remoteViews.setTextViewText(R.id.content, "contentcontentcontentcontentcontentcontentcontentcontent");
        RemoteViews remoteViews2 = new RemoteViews(AstApp.i().getPackageName(), (int) R.layout.notification_card1_right6);
        if (a2 != null) {
            remoteViews2.setTextColor(R.id.timeText, a2.intValue());
        }
        remoteViews2.setFloat(R.id.timeText, "setTextSize", rVar.b());
        remoteViews2.setTextViewText(R.id.timeText, cv.d(Long.valueOf(System.currentTimeMillis())));
        remoteViews.removeAllViews(R.id.rightContainer);
        if (com.tencent.assistant.utils.r.d() >= 20) {
            remoteViews.setInt(R.id.root, "setBackgroundResource", R.color.notification_bg_50);
        }
        remoteViews.setInt(R.id.root, "setBackgroundResource", R.color.guide_green);
        return remoteViews;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING /*1148*/:
                Toast.makeText(this, "视频下载信息：" + ((h) message.obj), 0).show();
                return;
            default:
                return;
        }
    }
}
