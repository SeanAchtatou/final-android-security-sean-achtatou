package com.tencent.assistant.activity;

import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.module.callback.y;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class dx extends y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HelperFeedbackActivity f539a;

    dx(HelperFeedbackActivity helperFeedbackActivity) {
        this.f539a = helperFeedbackActivity;
    }

    public void a(int i, int i2) {
        String unused = this.f539a.z = (String) null;
        if (i == 0 && i2 == 0) {
            this.f539a.u.getInputView().setText(Constants.STR_EMPTY);
            this.f539a.u.getInputView().setSelection(0);
            Toast.makeText(this.f539a, (int) R.string.feedback_send_success, 0).show();
            this.f539a.finish();
            return;
        }
        Toast.makeText(this.f539a, (int) R.string.feedback_send_failed, 0).show();
    }
}
