package com.tencent.assistant.activity;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.mediadownload.c;
import com.tencent.assistantv2.model.d;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class cn extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f455a;
    final /* synthetic */ DownloadActivity b;

    cn(DownloadActivity downloadActivity, String str) {
        this.b = downloadActivity;
        this.f455a = str;
    }

    public void onLeftBtnClick() {
        k.a(new STInfoV2(STConst.ST_PAGE_DOWNLOAD_URL_UNSAFE, a.a("03", 1), this.b.f(), STConst.ST_DEFAULT_SLOT, 200));
    }

    public void onRightBtnClick() {
        d a2 = d.a(this.b.O.c(), this.b.O.b(), this.f455a);
        a2.l = a.a(STInfoBuilder.buildSTInfo(this.b, 900));
        c.c().a(a2);
        k.a(new STInfoV2(STConst.ST_PAGE_DOWNLOAD_URL_UNSAFE, a.a("03", 0), this.b.f(), STConst.ST_DEFAULT_SLOT, 200));
    }

    public void onCancell() {
    }
}
