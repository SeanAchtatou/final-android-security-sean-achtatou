package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class t extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkMgrActivity f655a;

    t(ApkMgrActivity apkMgrActivity) {
        this.f655a = apkMgrActivity;
    }

    public void onTMAClick(View view) {
        if (this.f655a.x.getFooterViewEnable()) {
            if (this.f655a.A.e()) {
                boolean unused = this.f655a.E = true;
                if (SpaceScanManager.a().e()) {
                    SpaceScanManager.a().a(this.f655a.V);
                    XLog.i("ApkMgrActivity", "startScanRubbish in ApkMgrActivity");
                } else {
                    this.f655a.D();
                }
            }
            this.f655a.z();
            boolean unused2 = this.f655a.C = true;
            this.f655a.x.updateContent(this.f655a.getString(R.string.apkmgr_is_deleting));
            this.f655a.x.setFooterViewEnable(false);
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f655a, 200);
        buildSTInfo.slotId = "05_001";
        return buildSTInfo;
    }
}
