package com.tencent.assistant.activity.debug;

import android.view.View;
import android.widget.Toast;
import com.tencent.assistant.plugin.QReaderClient;
import com.tencent.assistant.plugin.SimpleLoginInfo;
import com.tencent.assistant.plugin.mgr.f;

/* compiled from: ProGuard */
class at implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ QReaderPluginDebugActivity f494a;

    at(QReaderPluginDebugActivity qReaderPluginDebugActivity) {
        this.f494a = qReaderPluginDebugActivity;
    }

    public void onClick(View view) {
        SimpleLoginInfo userLoginInfo = QReaderClient.getInstance().getUserLoginInfo();
        if (userLoginInfo != null) {
            f.a().a(this.f494a, userLoginInfo.uin, 3);
        } else {
            Toast.makeText(this.f494a, "没有登录qq无法充值", 0).show();
        }
    }
}
