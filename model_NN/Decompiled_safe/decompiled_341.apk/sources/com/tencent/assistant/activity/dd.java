package com.tencent.assistant.activity;

import android.app.Activity;
import android.os.Bundle;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.RankNormalListView;

/* compiled from: ProGuard */
public class dd extends cy {
    public dd() {
    }

    public dd(Activity activity) {
        super(activity);
    }

    public void d(Bundle bundle) {
        super.d(bundle);
        e(true);
        f(false);
        super.b(6);
    }

    public void C() {
    }

    public int D() {
        return 0;
    }

    public int E() {
        return 2;
    }

    public int J() {
        return STConst.ST_PAGE_GAME_RANKING_NET;
    }

    public String B() {
        return "04";
    }

    public String K() {
        return RankNormalListView.ST_HIDE_INSTALLED_APPS;
    }
}
