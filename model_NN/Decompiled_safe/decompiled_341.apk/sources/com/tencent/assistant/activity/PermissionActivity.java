package com.tencent.assistant.activity;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class PermissionActivity extends BaseActivity {
    private LinearLayout n;
    private Context t;
    private LayoutInflater u;
    private ArrayList<String> v = new ArrayList<>();
    private PackageManager w;
    private SecondNavigationTitleViewV5 x;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_permission_layout);
        i();
        j();
    }

    public int f() {
        return STConst.ST_PAGE_APP_DETAIL_MORE_PERMISSION;
    }

    private void i() {
        ArrayList<String> stringArrayListExtra = getIntent().getStringArrayListExtra("com.tencent.assistant.PERMISSION_LIST");
        if (stringArrayListExtra != null) {
            this.v.addAll(stringArrayListExtra);
        }
        this.w = getPackageManager();
    }

    private void j() {
        this.t = this;
        this.x = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.x.b(getResources().getString(R.string.app_permissions));
        this.x.a(this);
        this.x.d();
        this.u = LayoutInflater.from(this.t);
        this.n = (LinearLayout) findViewById(R.id.permission_detail_layout);
        a(this.v);
    }

    private void a(ArrayList<String> arrayList) {
        PermissionInfo permissionInfo;
        int i = 0;
        if (arrayList == null || arrayList.size() == 0) {
            View inflate = this.u.inflate((int) R.layout.permission_item, (ViewGroup) null);
            ((TextView) inflate.findViewById(R.id.permission_title)).setText(this.t.getString(R.string.no_any_premission));
            this.n.addView(inflate);
            return;
        }
        while (true) {
            int i2 = i;
            if (i2 < arrayList.size()) {
                try {
                    permissionInfo = this.w.getPermissionInfo(arrayList.get(i2), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                    permissionInfo = null;
                }
                if (permissionInfo != null) {
                    String str = (String) permissionInfo.loadLabel(this.w);
                    String str2 = (String) permissionInfo.loadDescription(this.w);
                    if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
                        View inflate2 = this.u.inflate((int) R.layout.permission_item, (ViewGroup) null);
                        ((TextView) inflate2.findViewById(R.id.permission_title)).setText(str.replace("（", "(").replace("）", ")"));
                        ((TextView) inflate2.findViewById(R.id.permission_detail)).setText(str2.replace("（", "(").replace("）", ")"));
                        this.n.addView(inflate2);
                    }
                }
                i = i2 + 1;
            } else {
                this.n.addView((LinearLayout) this.u.inflate((int) R.layout.nothing_layout, (ViewGroup) null));
                return;
            }
        }
    }
}
