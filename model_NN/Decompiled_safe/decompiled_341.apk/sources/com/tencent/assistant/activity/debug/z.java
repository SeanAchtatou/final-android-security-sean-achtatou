package com.tencent.assistant.activity.debug;

import android.view.View;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class z implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f522a;

    z(DActivity dActivity) {
        this.f522a = dActivity;
    }

    public void onClick(View view) {
        TemporaryThreadManager.get().start(new aa(this));
    }
}
