package com.tencent.assistant.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.AppAdapter;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.module.callback.b;
import com.tencent.assistant.module.k;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class TencentAppListActivity extends BaseActivity implements ITXRefreshListViewListener {
    private ApkResCallback A = new gz(this);
    private b B = new hb(this);
    private View.OnClickListener C = new hc(this);
    /* access modifiers changed from: private */
    public k n;
    /* access modifiers changed from: private */
    public TXGetMoreListView t;
    private SecondNavigationTitleViewV5 u;
    /* access modifiers changed from: private */
    public AppAdapter v;
    private long w = 0;
    private String x = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public LoadingView y;
    private NormalErrorRecommendPage z;

    public int f() {
        return STConst.ST_PAGE_SOFTWARE_CATEGORY_TENCENT;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.author_applist_layout);
        j();
        v();
    }

    private void j() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.w = extras.getLong("com.tencent.assistant.CATATORY_ID");
            this.x = extras.getString("activityTitleName");
        }
    }

    private void v() {
        this.y = (LoadingView) findViewById(R.id.loading_view);
        this.y.setVisibility(0);
        this.z = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.z.setButtonClickListener(this.C);
        this.u = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.u.b(this.x);
        this.u.a(this);
        this.u.i();
        this.n = new k(-10, 20);
        this.t = (TXGetMoreListView) findViewById(R.id.author_applist);
        this.t.setVisibility(8);
        this.v = new AppAdapter(this, this.t, this.n.a(), true);
        this.v.a(f(), this.w, "03_");
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new AbsListView.LayoutParams(-1, df.a(this, 4.0f)));
        imageView.setBackgroundColor(getResources().getColor(17170445));
        this.t.addHeaderView(imageView);
        this.t.setAdapter(this.v);
        this.t.setDivider(null);
        this.t.setRefreshListViewListener(this);
        i();
    }

    public void i() {
        if (this.v.getCount() > 0) {
            w();
            this.v.notifyDataSetChanged();
        } else {
            this.n.c();
        }
        this.t.onRefreshComplete(this.n.h(), true);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.n.unregister(this.B);
        super.onPause();
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.A);
        this.u.m();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.n.register(this.B);
        this.v.notifyDataSetChanged();
        super.onResume();
        ApkResourceManager.getInstance().registerApkResCallback(this.A);
        this.u.l();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.n = null;
        this.t = null;
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void w() {
        this.t.setVisibility(0);
        this.y.setVisibility(8);
        this.z.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void b(int i) {
        this.t.setVisibility(8);
        this.y.setVisibility(8);
        this.z.setVisibility(0);
        this.z.setErrorType(i);
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        this.n.f();
    }
}
