package com.tencent.assistant.activity.debug;

import android.content.Context;
import android.view.View;
import android.widget.Toast;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.utils.FileUtil;
import java.io.File;

/* compiled from: ProGuard */
class m implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f509a;

    m(DActivity dActivity) {
        this.f509a = dActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.plugin.mgr.d.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean
     arg types: [com.tencent.assistant.activity.debug.DActivity, java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.assistant.plugin.mgr.d.a(java.lang.String, android.content.pm.ApplicationInfo, android.content.pm.ActivityInfo[], java.lang.String):java.lang.String
      com.tencent.assistant.plugin.mgr.d.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean */
    public void onClick(View view) {
        File[] listFiles = new File(FileUtil.getCommonPath(FileUtil.PLUGIN_DIR_PATH)).listFiles();
        if (listFiles != null && listFiles.length > 0) {
            for (File file : listFiles) {
                if (file.isFile() && (file.getAbsolutePath().endsWith(".plg") || file.getAbsolutePath().endsWith(".apk"))) {
                    try {
                        d.b().a((Context) this.f509a, file.getAbsolutePath(), (String) null, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(this.f509a, e.getMessage(), 1).show();
                    }
                }
            }
        }
        this.f509a.a();
    }
}
