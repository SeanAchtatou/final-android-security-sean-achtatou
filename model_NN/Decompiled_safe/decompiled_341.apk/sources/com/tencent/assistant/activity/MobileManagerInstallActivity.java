package com.tencent.assistant.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.g;
import com.tencent.assistant.module.callback.s;
import com.tencent.assistant.module.ds;
import com.tencent.assistant.module.u;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean;
import com.tencent.assistant.utils.m;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.component.AppStateButtonV5;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class MobileManagerInstallActivity extends BaseActivity implements UIEventListener, NetworkMonitor.ConnectivityChangeListener, s {
    private final int A = 10;
    private final int B = -10;
    private final int C = 20;
    private final int D = 21;
    private final String E = "000116083137393932323936";
    /* access modifiers changed from: private */
    public int F = -1;
    /* access modifiers changed from: private */
    public SimpleAppModel G;
    /* access modifiers changed from: private */
    public ds H = new ds();
    /* access modifiers changed from: private */
    public Handler I = new ej(this);
    private g J = new em(this);
    private SecondNavigationTitleViewV5 n;
    private RelativeLayout t;
    private ImageView u;
    /* access modifiers changed from: private */
    public AppStateButtonV5 v;
    /* access modifiers changed from: private */
    public Button w;
    private LoadingView x;
    private final int y = -1;
    private final int z = -2;

    public int f() {
        return STConst.ST_PAGE_GARBAGE_UNINSTALL_STEWARD;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_mobile_manager_install);
        w();
        i();
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        AstApp.i().k().addUIEventListener(1013, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
        SpaceScanManager.a().a(this.J);
        cq.a().a((NetworkMonitor.ConnectivityChangeListener) this);
        j();
    }

    private void i() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = extras.getString("toast_message");
            if (!TextUtils.isEmpty(string)) {
                Toast.makeText(this, string, 0).show();
            }
        }
    }

    public void handleUIEvent(Message message) {
        XLog.d("miles", "MobileMnagerInstallActivity.  handleUIEvent. msg.what=" + message.what);
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_INSTALL /*1011*/:
            case 1013:
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC /*1026*/:
                String str = Constants.STR_EMPTY;
                String str2 = Constants.STR_EMPTY;
                if (message.obj instanceof String) {
                    str = (String) message.obj;
                    str2 = (String) message.obj;
                } else if (message.obj instanceof InstallUninstallTaskBean) {
                    InstallUninstallTaskBean installUninstallTaskBean = (InstallUninstallTaskBean) message.obj;
                    String str3 = installUninstallTaskBean.downloadTicket;
                    str2 = installUninstallTaskBean.packageName;
                    str = str3;
                }
                if ((!TextUtils.isEmpty(str) && str.equals(this.G.q())) || (!TextUtils.isEmpty(str2) && str2.equals(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME))) {
                    SpaceScanManager.a().c();
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_UNINSTALL /*1012*/:
                if ((message.obj instanceof String) && ((String) message.obj).equals(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME)) {
                    this.I.sendEmptyMessage(-1);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.n != null) {
            this.n.l();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.n != null) {
            this.n.m();
        }
    }

    private void j() {
        if (this.G == null) {
            this.G = new SimpleAppModel();
            this.G.c = AppConst.TENCENT_MOBILE_MANAGER_PKGNAME;
            this.G.ac = "000116083137393932323936";
        }
        this.v.a(this.G);
        TemporaryThreadManager.get().start(new ei(this));
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        int i = 0;
        if (this.t != null) {
            this.t.setVisibility(z2 ? 8 : 0);
        }
        if (this.x != null) {
            LoadingView loadingView = this.x;
            if (!z2) {
                i = 8;
            }
            loadingView.setVisibility(i);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.assistant.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.u.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.w, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    /* access modifiers changed from: private */
    public void v() {
        DownloadInfo downloadInfo = null;
        DownloadInfo a2 = DownloadProxy.a().a(this.G);
        StatInfo a3 = a.a(STInfoBuilder.buildSTInfo(this, this.G, STConst.ST_DEFAULT_SLOT, 200, null));
        if (a2 == null || !a2.needReCreateInfo(this.G)) {
            downloadInfo = a2;
        } else {
            DownloadProxy.a().b(a2.downloadTicket);
        }
        if (downloadInfo == null) {
            downloadInfo = DownloadInfo.createDownloadInfo(this.G, a3, this.v);
        } else {
            downloadInfo.updateDownloadInfoStatInfo(a3);
        }
        switch (en.f556a[u.a(downloadInfo, false, false).ordinal()]) {
            case 1:
            case 2:
                com.tencent.assistant.download.a.a().a(downloadInfo);
                return;
            case 3:
            case 4:
                com.tencent.assistant.download.a.a().b(downloadInfo.downloadTicket);
                return;
            case 5:
                com.tencent.assistant.download.a.a().b(downloadInfo);
                return;
            case 6:
                if (!downloadInfo.isDownloadFileExist()) {
                    AppConst.TwoBtnDialogInfo b = b(downloadInfo);
                    if (b != null) {
                        v.a(b);
                        return;
                    }
                    return;
                }
                com.tencent.assistant.download.a.a().d(downloadInfo);
                return;
            case 7:
                com.tencent.assistant.download.a.a().c(downloadInfo);
                return;
            case 8:
            case 9:
                com.tencent.assistant.download.a.a().a(downloadInfo);
                return;
            case 10:
                Toast.makeText(this, (int) R.string.tips_slicent_install, 0).show();
                return;
            case 11:
                Toast.makeText(this, (int) R.string.tips_slicent_uninstall, 0).show();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public CharSequence a(DownloadInfo downloadInfo) {
        String format;
        boolean z2 = ApkResourceManager.getInstance().getInstalledApkInfo(downloadInfo.packageName) != null;
        if (z2 && downloadInfo.isSslUpdate()) {
            format = String.format(getResources().getString(R.string.mobile_rubbish_clear_btn_update), bt.a(downloadInfo.sllFileSize));
        } else if (!z2 || downloadInfo.isUpdate != 1) {
            format = String.format(getResources().getString(R.string.mobile_rubbish_clear_btn_download), bt.a(downloadInfo.fileSize));
        } else {
            format = String.format(getResources().getString(R.string.mobile_rubbish_clear_btn_update), bt.a(downloadInfo.fileSize));
        }
        SpannableString spannableString = new SpannableString(format);
        spannableString.setSpan(new AbsoluteSizeSpan(14, true), 4, format.length(), 33);
        return spannableString;
    }

    private AppConst.TwoBtnDialogInfo b(DownloadInfo downloadInfo) {
        el elVar = new el(this, downloadInfo);
        elVar.hasTitle = true;
        elVar.titleRes = getResources().getString(R.string.down_page_dialog_title);
        elVar.contentRes = getResources().getString(R.string.down_page_dialog_content);
        elVar.lBtnTxtRes = getResources().getString(R.string.down_page_dialog_left_del);
        elVar.rBtnTxtRes = getResources().getString(R.string.down_page_dialog_right_down);
        return elVar;
    }

    private void w() {
        this.n = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.n.a(this);
        this.n.b(getString(R.string.rubbish_clear_title));
        this.n.d();
        this.t = (RelativeLayout) findViewById(R.id.layout_main);
        this.u = (ImageView) findViewById(R.id.image_banner);
        try {
            Bitmap a2 = m.a(R.drawable.rubbish_clean_banner);
            if (a2 != null && !a2.isRecycled()) {
                this.u.setImageBitmap(a2);
            }
        } catch (Throwable th) {
            cq.a().b();
        }
        this.v = (AppStateButtonV5) findViewById(R.id.btn_download);
        this.w = (Button) findViewById(R.id.btn_no_net);
        this.v.a((int) getResources().getDimension(R.dimen.app_detail_float_bar_btn_height));
        this.v.b(16);
        this.x = (LoadingView) findViewById(R.id.loading);
        b(true);
    }

    public void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail) {
        if (appSimpleDetail == null) {
            this.I.sendEmptyMessage(-10);
            return;
        }
        this.G = u.a(appSimpleDetail);
        this.I.sendEmptyMessage(10);
    }

    public void onGetAppInfoFail(int i, int i2) {
        this.I.sendEmptyMessage(-10);
    }

    public void onConnected(APN apn) {
        if (!SpaceScanManager.a().e()) {
            SpaceScanManager.a().c();
        }
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        AstApp.i().k().removeUIEventListener(1013, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
        SpaceScanManager.a().b(this.J);
        cq.a().b(this);
        super.onDestroy();
    }
}
