package com.tencent.assistant.activity;

import android.content.DialogInterface;
import android.view.KeyEvent;

/* compiled from: ProGuard */
class aw implements DialogInterface.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppTreasureBoxActivity f411a;

    aw(AppTreasureBoxActivity appTreasureBoxActivity) {
        this.f411a = appTreasureBoxActivity;
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        if (keyEvent.getAction() != 1) {
            return true;
        }
        this.f411a.onCloseBtnClick();
        return true;
    }
}
