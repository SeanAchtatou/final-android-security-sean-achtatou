package com.tencent.assistant.activity;

import android.os.Handler;
import android.os.Message;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.module.u;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class ej extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MobileManagerInstallActivity f552a;

    ej(MobileManagerInstallActivity mobileManagerInstallActivity) {
        this.f552a = mobileManagerInstallActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.MobileManagerInstallActivity.a(com.tencent.assistant.activity.MobileManagerInstallActivity, boolean):void
     arg types: [com.tencent.assistant.activity.MobileManagerInstallActivity, int]
     candidates:
      com.tencent.assistant.activity.MobileManagerInstallActivity.a(com.tencent.assistant.activity.MobileManagerInstallActivity, int):int
      com.tencent.assistant.activity.MobileManagerInstallActivity.a(com.tencent.assistant.activity.MobileManagerInstallActivity, com.tencent.assistant.download.DownloadInfo):java.lang.CharSequence
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.activity.MobileManagerInstallActivity.a(com.tencent.assistant.activity.MobileManagerInstallActivity, boolean):void */
    public void handleMessage(Message message) {
        DownloadInfo downloadInfo = null;
        super.handleMessage(message);
        int unused = this.f552a.F = message.what;
        switch (this.f552a.F) {
            case -10:
                this.f552a.v.setVisibility(8);
                this.f552a.w.setVisibility(0);
                XLog.e("miles", "MobileManagerInstallActivity >> get Tencent Mobile Manager[com.tencent.qqpimsecure] from server fail...");
                this.f552a.b(false);
                return;
            case -2:
            case -1:
                this.f552a.H.a(this.f552a.G);
                this.f552a.H.register(this.f552a);
                if (this.f552a.F == -1) {
                    XLog.w("miles", "MobileManagerInstallActivity >> check Tencent Mobile Manager[com.tencent.qqpimsecure] from local : not installed...");
                    return;
                } else {
                    XLog.w("miles", "MobileManagerInstallActivity >> check Tencent Mobile Manager[com.tencent.qqpimsecure] from local : lower version...");
                    return;
                }
            case 10:
                this.f552a.b(false);
                this.f552a.v.setVisibility(0);
                this.f552a.w.setVisibility(8);
                this.f552a.v.a(this.f552a.G);
                DownloadInfo a2 = DownloadProxy.a().a(this.f552a.G);
                StatInfo a3 = a.a(STInfoBuilder.buildSTInfo(this.f552a, this.f552a.G, STConst.ST_DEFAULT_SLOT, 200, null));
                if (a2 == null || !a2.needReCreateInfo(this.f552a.G)) {
                    downloadInfo = a2;
                } else {
                    DownloadProxy.a().b(a2.downloadTicket);
                }
                if (downloadInfo == null) {
                    downloadInfo = DownloadInfo.createDownloadInfo(this.f552a.G, a3);
                } else {
                    downloadInfo.updateDownloadInfoStatInfo(a3);
                }
                AppConst.AppState d = u.d(this.f552a.G);
                if (d == AppConst.AppState.DOWNLOADED) {
                    this.f552a.v.a(this.f552a.getResources().getString(R.string.appbutton_install));
                } else if (d == AppConst.AppState.DOWNLOAD || d == AppConst.AppState.UPDATE || d == AppConst.AppState.ILLEGAL) {
                    this.f552a.v.a(this.f552a.a(downloadInfo));
                    if (d == AppConst.AppState.ILLEGAL) {
                        this.f552a.v.a(AppConst.AppState.DOWNLOAD);
                    }
                }
                this.f552a.v.setOnClickListener(new ek(this));
                return;
            case 20:
            case 21:
                XLog.d("miles", "MobileManagerInstallActivity >> MOBILE_MANAGER_CHECK_SUCESS or MOBILE_MANAGER_CHECK_SUCESS_AFTER_INSTALL");
                this.f552a.setResult(-1);
                this.f552a.finish();
                return;
            default:
                return;
        }
    }
}
