package com.tencent.assistant.activity;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class gd extends ClickableSpan implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceCleanActivity f600a;
    private final View.OnClickListener b;
    private int c;

    public gd(SpaceCleanActivity spaceCleanActivity, View.OnClickListener onClickListener) {
        this.f600a = spaceCleanActivity;
        this.b = onClickListener;
        this.c = spaceCleanActivity.getResources().getColor(R.color.rubbish_clear_clear_success_guide);
    }

    public void onClick(View view) {
        this.b.onClick(view);
    }

    public void updateDrawState(TextPaint textPaint) {
        textPaint.setColor(this.c);
        textPaint.setUnderlineText(false);
    }
}
