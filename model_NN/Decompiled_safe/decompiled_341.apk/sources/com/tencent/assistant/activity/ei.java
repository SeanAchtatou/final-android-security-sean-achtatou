package com.tencent.assistant.activity;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.utils.e;

/* compiled from: ProGuard */
class ei implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MobileManagerInstallActivity f551a;

    ei(MobileManagerInstallActivity mobileManagerInstallActivity) {
        this.f551a = mobileManagerInstallActivity;
    }

    public void run() {
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        boolean z = false;
        if (localApkInfo == null) {
            z = e.a(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        }
        if (localApkInfo == null && !z) {
            this.f551a.I.sendEmptyMessage(-1);
        } else if (!SpaceScanManager.a().e()) {
            SpaceScanManager.a().c();
        } else {
            this.f551a.I.sendEmptyMessage(20);
        }
    }
}
