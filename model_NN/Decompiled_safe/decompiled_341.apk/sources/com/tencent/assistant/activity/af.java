package com.tencent.assistant.activity;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class af extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppBackupActivity f394a;

    af(AppBackupActivity appBackupActivity) {
        this.f394a = appBackupActivity;
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        this.f394a.runOnUiThread(new ag(this));
    }
}
