package com.tencent.assistant.activity.debug;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.m;

/* compiled from: ProGuard */
public class ServerAdressSettingActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Spinner f473a = null;
    /* access modifiers changed from: private */
    public Spinner b = null;
    /* access modifiers changed from: private */
    public int c = 0;
    /* access modifiers changed from: private */
    public TextView d = null;
    /* access modifiers changed from: private */
    public int e = 0;
    /* access modifiers changed from: private */
    public int f = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.server_adress_setting);
        this.d = (TextView) findViewById(R.id.server_name);
        this.d.setText("当前：" + Global.getServerAddressName());
        this.c = m.a().d();
        a();
        b();
    }

    public boolean a(int i, int i2) {
        if (i == 0) {
            if (i2 >= this.e || i2 < 0) {
                return false;
            }
            return true;
        } else if (1 != i) {
            return false;
        } else {
            if (i2 < this.e || i2 >= this.e + this.f) {
                return false;
            }
            return true;
        }
    }

    public void a() {
        int i;
        this.f473a = (Spinner) findViewById(R.id.spinner1);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.server_adress_setting_item);
        if (Global.DIVIDING_POS < Global.SERVER_ENVIRONMENT_NAME.length) {
            for (int i2 = 0; i2 <= Global.DIVIDING_POS; i2++) {
                arrayAdapter.add(Global.SERVER_ENVIRONMENT_NAME[i2]);
            }
            arrayAdapter.add("未设置");
            this.e = arrayAdapter.getCount() > 0 ? arrayAdapter.getCount() - 1 : 0;
            this.f473a.setAdapter((SpinnerAdapter) arrayAdapter);
            this.f473a.setOnItemSelectedListener(new av(this));
            boolean a2 = a(0, this.c);
            Spinner spinner = this.f473a;
            if (a2) {
                i = this.c;
            } else {
                i = this.e;
            }
            spinner.setSelection(i);
        }
    }

    public void b() {
        int i;
        this.b = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.server_adress_setting_item);
        if (Global.DIVIDING_POS < Global.SERVER_ENVIRONMENT_NAME.length) {
            int i2 = Global.DIVIDING_POS;
            while (true) {
                i2++;
                if (i2 >= Global.SERVER_ENVIRONMENT_NAME.length) {
                    break;
                }
                arrayAdapter.add(Global.SERVER_ENVIRONMENT_NAME[i2]);
            }
            arrayAdapter.add("未设置");
            this.f = arrayAdapter.getCount() > 0 ? arrayAdapter.getCount() - 1 : 0;
            this.b.setAdapter((SpinnerAdapter) arrayAdapter);
            this.b.setOnItemSelectedListener(new aw(this));
            boolean a2 = a(1, this.c);
            Spinner spinner = this.b;
            if (a2) {
                i = this.c - this.e;
            } else {
                i = this.f;
            }
            spinner.setSelection(i);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_SERVER_ENVIRONMENT_CHANGE));
    }
}
