package com.tencent.assistant.activity;

import android.os.IBinder;
import android.os.Message;
import com.tencent.assistant.utils.XLog;
import com.tencent.tmsecurelite.optimize.f;

/* compiled from: ProGuard */
class gc extends f {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceCleanActivity f599a;

    gc(SpaceCleanActivity spaceCleanActivity) {
        this.f599a = spaceCleanActivity;
    }

    public IBinder asBinder() {
        return null;
    }

    public void c() {
        XLog.d("miles", "SpaceCleanActivity onScanStarted.");
        long unused = this.f599a.R = System.currentTimeMillis();
    }

    public void a(int i) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.SpaceCleanActivity.e(com.tencent.assistant.activity.SpaceCleanActivity, boolean):boolean
     arg types: [com.tencent.assistant.activity.SpaceCleanActivity, int]
     candidates:
      com.tencent.assistant.activity.SpaceCleanActivity.e(com.tencent.assistant.activity.SpaceCleanActivity, long):long
      com.tencent.assistant.activity.SpaceCleanActivity.e(com.tencent.assistant.activity.SpaceCleanActivity, boolean):boolean */
    public void b() {
        XLog.d("miles", "SpaceCleanActivity onScanFinished.");
        if (this.f599a.W.hasMessages(25)) {
            this.f599a.W.removeMessages(25);
        }
        Message obtain = Message.obtain(this.f599a.W, 25);
        obtain.obj = Long.valueOf(this.f599a.H);
        obtain.sendToTarget();
        XLog.d("miles", "-----------扫描结束----------");
        if (this.f599a.H == 0) {
            boolean unused = this.f599a.x = true;
        }
        long unused2 = this.f599a.S = System.currentTimeMillis();
        this.f599a.D();
    }

    public void a() {
        XLog.d("miles", "SpaceCleanActivity onScanCanceled");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0093, code lost:
        if (android.text.TextUtils.isEmpty(r5.trim()) != false) goto L_0x0095;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r8, com.tencent.tmsecurelite.commom.DataEntity r9) {
        /*
            r7 = this;
            java.lang.String r0 = "rubbish.suggest"
            boolean r1 = r9.getBoolean(r0)     // Catch:{ Exception -> 0x007d }
            java.lang.String r0 = "app.pkg"
            java.lang.String r2 = r9.getString(r0)     // Catch:{ Exception -> 0x007d }
            java.lang.String r0 = "rubbish.size"
            long r3 = r9.getLong(r0)     // Catch:{ Exception -> 0x007d }
            java.lang.String r0 = "rubbish.desc"
            java.lang.String r5 = r9.getString(r0)     // Catch:{ Exception -> 0x007d }
            java.lang.String r0 = "rubbish.path.array"
            org.json.JSONArray r6 = r9.getJSONArray(r0)     // Catch:{ Exception -> 0x007d }
            switch(r8) {
                case 1: goto L_0x0077;
                case 2: goto L_0x0089;
                case 3: goto L_0x00b5;
                case 4: goto L_0x00be;
                default: goto L_0x0021;
            }     // Catch:{ Exception -> 0x007d }
        L_0x0021:
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f599a     // Catch:{ Exception -> 0x007d }
            com.tencent.assistant.activity.SpaceCleanActivity.h(r0, r3)     // Catch:{ Exception -> 0x007d }
            if (r1 == 0) goto L_0x002d
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f599a     // Catch:{ Exception -> 0x007d }
            com.tencent.assistant.activity.SpaceCleanActivity.i(r0, r3)     // Catch:{ Exception -> 0x007d }
        L_0x002d:
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f599a     // Catch:{ Exception -> 0x007d }
            com.tencent.assistant.activity.SpaceCleanActivity.w(r0)     // Catch:{ Exception -> 0x007d }
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f599a     // Catch:{ Exception -> 0x007d }
            int r0 = r0.G     // Catch:{ Exception -> 0x007d }
            int r0 = r0 % 4
            if (r0 != 0) goto L_0x0076
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f599a     // Catch:{ Exception -> 0x007d }
            r1 = 0
            int unused = r0.G = r1     // Catch:{ Exception -> 0x007d }
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f599a     // Catch:{ Exception -> 0x007d }
            android.os.Handler r0 = r0.W     // Catch:{ Exception -> 0x007d }
            r1 = 24
            boolean r0 = r0.hasMessages(r1)     // Catch:{ Exception -> 0x007d }
            if (r0 == 0) goto L_0x005b
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f599a     // Catch:{ Exception -> 0x007d }
            android.os.Handler r0 = r0.W     // Catch:{ Exception -> 0x007d }
            r1 = 24
            r0.removeMessages(r1)     // Catch:{ Exception -> 0x007d }
        L_0x005b:
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f599a     // Catch:{ Exception -> 0x007d }
            android.os.Handler r0 = r0.W     // Catch:{ Exception -> 0x007d }
            r1 = 24
            android.os.Message r0 = android.os.Message.obtain(r0, r1)     // Catch:{ Exception -> 0x007d }
            com.tencent.assistant.activity.SpaceCleanActivity r1 = r7.f599a     // Catch:{ Exception -> 0x007d }
            long r1 = r1.H     // Catch:{ Exception -> 0x007d }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ Exception -> 0x007d }
            r0.obj = r1     // Catch:{ Exception -> 0x007d }
            r0.sendToTarget()     // Catch:{ Exception -> 0x007d }
        L_0x0076:
            return
        L_0x0077:
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f599a     // Catch:{ Exception -> 0x007d }
            r0.a(r1, r2, r3, r5, r6)     // Catch:{ Exception -> 0x007d }
            goto L_0x0021
        L_0x007d:
            r0 = move-exception
            java.lang.String r1 = "miles"
            java.lang.String r2 = "SpaceCleanActivty. onRubbishFound Exception."
            com.tencent.assistant.utils.XLog.d(r1, r2)
            r0.printStackTrace()
            goto L_0x0076
        L_0x0089:
            if (r5 == 0) goto L_0x0095
            java.lang.String r0 = r5.trim()     // Catch:{ Exception -> 0x007d }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x007d }
            if (r0 == 0) goto L_0x0097
        L_0x0095:
            java.lang.String r5 = "未安装"
        L_0x0097:
            if (r1 != 0) goto L_0x00ac
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007d }
            r0.<init>()     // Catch:{ Exception -> 0x007d }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x007d }
            java.lang.String r2 = "(建议保留)"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x007d }
            java.lang.String r5 = r0.toString()     // Catch:{ Exception -> 0x007d }
        L_0x00ac:
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f599a     // Catch:{ Exception -> 0x007d }
            java.lang.String r2 = "安装包"
            r0.b(r1, r2, r3, r5, r6)     // Catch:{ Exception -> 0x007d }
            goto L_0x0021
        L_0x00b5:
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f599a     // Catch:{ Exception -> 0x007d }
            java.lang.String r2 = "垃圾文件"
            r0.b(r1, r2, r3, r5, r6)     // Catch:{ Exception -> 0x007d }
            goto L_0x0021
        L_0x00be:
            if (r5 == 0) goto L_0x00d1
            java.lang.String r0 = "破损安装包"
            boolean r0 = r5.contains(r0)     // Catch:{ Exception -> 0x007d }
            if (r0 == 0) goto L_0x00d1
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f599a     // Catch:{ Exception -> 0x007d }
            java.lang.String r2 = "安装包"
            r0.b(r1, r2, r3, r5, r6)     // Catch:{ Exception -> 0x007d }
            goto L_0x0021
        L_0x00d1:
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f599a     // Catch:{ Exception -> 0x007d }
            java.lang.String r2 = "垃圾文件"
            r0.b(r1, r2, r3, r5, r6)     // Catch:{ Exception -> 0x007d }
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.activity.gc.a(int, com.tencent.tmsecurelite.commom.DataEntity):void");
    }
}
