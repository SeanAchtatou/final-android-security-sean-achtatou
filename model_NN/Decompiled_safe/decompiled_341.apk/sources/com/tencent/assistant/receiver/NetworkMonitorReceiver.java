package com.tencent.assistant.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import com.tencent.assistant.manager.cq;

/* compiled from: ProGuard */
public class NetworkMonitorReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            cq.a().a((NetworkInfo) intent.getParcelableExtra("networkInfo"));
        }
    }
}
