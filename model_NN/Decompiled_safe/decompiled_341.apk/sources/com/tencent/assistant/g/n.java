package com.tencent.assistant.g;

import android.os.Message;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.login.a.a;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class n implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f1320a;

    n(e eVar) {
        this.f1320a = eVar;
    }

    public void a(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_SHARE_SUCCESS);
        obtainMessage.obj = 2;
        AstApp.i().j().sendMessage(obtainMessage);
        XLog.i("xjp", "*** share succeeded ***");
    }

    public void a(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.i("Jie", "share fail:errcode=" + i2);
        a.c(i2);
        Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_SHARE_FAIL);
        obtainMessage.obj = 2;
        AstApp.i().j().sendMessage(obtainMessage);
    }
}
