package com.tencent.assistant.g;

import com.tencent.assistant.activity.ShareBaseActivity;
import com.tencent.assistant.component.ShareAppContentView;
import com.tencent.assistant.model.ShareAppModel;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.v;

/* compiled from: ProGuard */
class q implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1323a;
    final /* synthetic */ ShareBaseActivity b;
    final /* synthetic */ p c;

    q(p pVar, int i, ShareBaseActivity shareBaseActivity) {
        this.c = pVar;
        this.f1323a = i;
        this.b = shareBaseActivity;
    }

    public void run() {
        if (this.c.b(this.f1323a)) {
            XLog.i("YYBShareOganizer", "show YYB dialog once when resumed");
            ShareAppModel b2 = this.c.c.b();
            if (b2 == null || b2.e == 0) {
                b2 = this.c.b(this.b);
            }
            try {
                ShareAppContentView shareAppContentView = new ShareAppContentView(this.b);
                shareAppContentView.setShareContentText(this.c.a(this.b));
                ShareBaseActivity shareBaseActivity = this.b;
                if (b2 == null) {
                    b2 = this.c.b(this.b);
                }
                v.a(shareBaseActivity, b2, shareAppContentView, (int) STConst.ST_PAGE_YYB_SHARE_DIALOG_ONCE, 0);
                o.e();
            } catch (Throwable th) {
            }
        } else {
            XLog.i("YYBShareOganizer", "sign YYB need show when next in");
        }
    }
}
