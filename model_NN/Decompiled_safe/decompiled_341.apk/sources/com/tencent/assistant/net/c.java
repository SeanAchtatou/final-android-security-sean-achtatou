package com.tencent.assistant.net;

import android.content.Context;
import android.net.Proxy;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.qq.AppService.AstApp;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f1926a = true;
    private static b b = new b();
    private static boolean c = false;

    public static boolean a() {
        if (b.f1925a == APN.UN_DETECT) {
            j();
        }
        return f1926a;
    }

    public static boolean b() {
        return !TextUtils.isEmpty(Proxy.getDefaultHost());
    }

    public static int c() {
        if (d()) {
            return 3;
        }
        if (g()) {
            return 5;
        }
        if (f()) {
            return 2;
        }
        if (e()) {
            return 1;
        }
        return 4;
    }

    public static boolean d() {
        return i() == APN.WIFI;
    }

    public static boolean e() {
        APN i = i();
        return i == APN.CMNET || i == APN.CMWAP || i == APN.UNINET || i == APN.UNIWAP;
    }

    public static boolean f() {
        APN i = i();
        return i == APN.CTWAP || i == APN.CTNET || i == APN.WAP3G || i == APN.NET3G;
    }

    public static boolean g() {
        APN i = i();
        return i == APN.WAP4G || i == APN.NET4G;
    }

    public static b h() {
        if (b.f1925a == APN.UN_DETECT) {
            j();
        }
        return b;
    }

    public static APN i() {
        return h().f1925a;
    }

    public static void j() {
        b = b(AstApp.i());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001b, code lost:
        if (r0.isAvailable() == false) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.tencent.assistant.net.b b(android.content.Context r4) {
        /*
            r3 = 1
            com.tencent.assistant.net.b r1 = new com.tencent.assistant.net.b
            r1.<init>()
            r2 = 0
            java.lang.String r0 = "connectivity"
            java.lang.Object r0 = r4.getSystemService(r0)     // Catch:{ Throwable -> 0x0026 }
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ Throwable -> 0x0026 }
            if (r0 == 0) goto L_0x0064
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()     // Catch:{ Throwable -> 0x0026 }
        L_0x0015:
            if (r0 == 0) goto L_0x001d
            boolean r2 = r0.isAvailable()     // Catch:{ Throwable -> 0x0062 }
            if (r2 != 0) goto L_0x0028
        L_0x001d:
            r2 = 0
            com.tencent.assistant.net.c.f1926a = r2     // Catch:{ Throwable -> 0x0062 }
            com.tencent.assistant.net.APN r2 = com.tencent.assistant.net.APN.NO_NETWORK     // Catch:{ Throwable -> 0x0062 }
            r1.f1925a = r2     // Catch:{ Throwable -> 0x0062 }
            r0 = r1
        L_0x0025:
            return r0
        L_0x0026:
            r0 = move-exception
            r0 = r2
        L_0x0028:
            com.tencent.assistant.net.c.f1926a = r3
            if (r0 == 0) goto L_0x005d
            int r0 = r0.getType()
            if (r0 != r3) goto L_0x005d
            com.tencent.assistant.net.APN r0 = com.tencent.assistant.net.APN.WIFI
            r1.f1925a = r0
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()
            java.lang.String r2 = "wifi"
            java.lang.Object r0 = r0.getSystemService(r2)
            android.net.wifi.WifiManager r0 = (android.net.wifi.WifiManager) r0
            if (r0 == 0) goto L_0x0056
            android.net.wifi.WifiInfo r0 = r0.getConnectionInfo()     // Catch:{ Throwable -> 0x0058 }
            if (r0 == 0) goto L_0x0056
            java.lang.String r2 = r0.getBSSID()     // Catch:{ Throwable -> 0x0058 }
            r1.e = r2     // Catch:{ Throwable -> 0x0058 }
            java.lang.String r0 = r0.getSSID()     // Catch:{ Throwable -> 0x0058 }
            r1.f = r0     // Catch:{ Throwable -> 0x0058 }
        L_0x0056:
            r0 = r1
            goto L_0x0025
        L_0x0058:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0056
        L_0x005d:
            com.tencent.assistant.net.b r0 = c(r4)
            goto L_0x0025
        L_0x0062:
            r2 = move-exception
            goto L_0x0028
        L_0x0064:
            r0 = r2
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.net.c.b(android.content.Context):com.tencent.assistant.net.b");
    }

    private static b c(Context context) {
        b bVar = new b();
        boolean b2 = b();
        bVar.d = b2;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String networkOperator = telephonyManager.getNetworkOperator();
        bVar.b = networkOperator;
        int networkType = telephonyManager.getNetworkType();
        bVar.c = networkType;
        switch (a(networkOperator)) {
            case 0:
                switch (networkType) {
                    case 1:
                    case 2:
                        if (b2) {
                            bVar.f1925a = APN.CMWAP;
                        } else {
                            bVar.f1925a = APN.CMNET;
                        }
                        return bVar;
                    case 13:
                        if (b2) {
                            bVar.f1925a = APN.WAP4G;
                        } else {
                            bVar.f1925a = APN.NET4G;
                        }
                        return bVar;
                    default:
                        if (b2) {
                            bVar.f1925a = APN.UNKNOW_WAP;
                        } else {
                            bVar.f1925a = APN.UNKNOWN;
                        }
                        return bVar;
                }
            case 1:
                switch (networkType) {
                    case 1:
                    case 2:
                        if (b2) {
                            bVar.f1925a = APN.UNIWAP;
                        } else {
                            bVar.f1925a = APN.UNINET;
                        }
                        return bVar;
                    case 3:
                    case 8:
                    case 10:
                    case 15:
                        if (b2) {
                            bVar.f1925a = APN.WAP3G;
                        } else {
                            bVar.f1925a = APN.NET3G;
                        }
                        return bVar;
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 9:
                    case 11:
                    case 12:
                    case 14:
                    default:
                        if (b2) {
                            bVar.f1925a = APN.UNKNOW_WAP;
                        } else {
                            bVar.f1925a = APN.UNKNOWN;
                        }
                        return bVar;
                    case 13:
                        if (b2) {
                            bVar.f1925a = APN.WAP4G;
                        } else {
                            bVar.f1925a = APN.NET4G;
                        }
                        return bVar;
                }
            case 2:
                switch (networkType) {
                    case 13:
                        if (b2) {
                            bVar.f1925a = APN.WAP4G;
                        } else {
                            bVar.f1925a = APN.NET4G;
                        }
                        return bVar;
                    default:
                        if (b2) {
                            bVar.f1925a = APN.CTWAP;
                        } else {
                            bVar.f1925a = APN.CTNET;
                        }
                        return bVar;
                }
            default:
                if (b2) {
                    bVar.f1925a = APN.UNKNOW_WAP;
                } else {
                    bVar.f1925a = APN.UNKNOWN;
                }
                return bVar;
        }
    }

    public static int a(String str) {
        if (!TextUtils.isEmpty(str)) {
            if (str.equals("46000") || str.equals("46002") || str.equals("46007")) {
                return 0;
            }
            if (str.equals("46001")) {
                return 1;
            }
            if (str.equals("46003")) {
                return 2;
            }
        }
        return -1;
    }

    public static void a(boolean z) {
        c = z;
    }

    public static boolean k() {
        return c;
    }

    public static boolean a(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "airplane_mode_on", 0) == 1;
    }
}
