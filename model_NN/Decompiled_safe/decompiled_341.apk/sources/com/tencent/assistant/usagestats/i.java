package com.tencent.assistant.usagestats;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/* compiled from: ProGuard */
final class i implements Set<K> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f2598a;

    i(f fVar) {
        this.f2598a = fVar;
    }

    public boolean add(K k) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection<? extends K> collection) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        this.f2598a.c();
    }

    public boolean contains(Object obj) {
        return this.f2598a.a(obj) >= 0;
    }

    public boolean containsAll(Collection<?> collection) {
        return f.a(this.f2598a.b(), collection);
    }

    public boolean isEmpty() {
        return this.f2598a.a() == 0;
    }

    public Iterator<K> iterator() {
        return new g(this.f2598a, 0);
    }

    public boolean remove(Object obj) {
        int a2 = this.f2598a.a(obj);
        if (a2 < 0) {
            return false;
        }
        this.f2598a.a(a2);
        return true;
    }

    public boolean removeAll(Collection<?> collection) {
        return f.b(this.f2598a.b(), collection);
    }

    public boolean retainAll(Collection<?> collection) {
        return f.c(this.f2598a.b(), collection);
    }

    public int size() {
        return this.f2598a.a();
    }

    public Object[] toArray() {
        return this.f2598a.b(0);
    }

    public <T> T[] toArray(T[] tArr) {
        return this.f2598a.a(tArr, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.usagestats.f.a(java.util.Set, java.lang.Object):boolean
     arg types: [com.tencent.assistant.usagestats.i, java.lang.Object]
     candidates:
      com.tencent.assistant.usagestats.f.a(java.util.Map, java.util.Collection<?>):boolean
      com.tencent.assistant.usagestats.f.a(int, int):java.lang.Object
      com.tencent.assistant.usagestats.f.a(int, java.lang.Object):V
      com.tencent.assistant.usagestats.f.a(java.lang.Object, java.lang.Object):void
      com.tencent.assistant.usagestats.f.a(java.lang.Object[], int):T[]
      com.tencent.assistant.usagestats.f.a(java.util.Set, java.lang.Object):boolean */
    public boolean equals(Object obj) {
        return f.a((Set) this, obj);
    }

    public int hashCode() {
        int i = 0;
        for (int a2 = this.f2598a.a() - 1; a2 >= 0; a2--) {
            Object a3 = this.f2598a.a(a2, 0);
            i += a3 == null ? 0 : a3.hashCode();
        }
        return i;
    }
}
