package com.tencent.assistant.usagestats;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class PkgUsageStats implements Parcelable {
    public static final Parcelable.Creator<PkgUsageStats> CREATOR = new l();

    /* renamed from: a  reason: collision with root package name */
    public String f2587a;
    public int b;
    public long c;
    public Map<String, Long> d;

    public String toString() {
        return "PkgUsageStats{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.f2587a + "}";
    }

    public PkgUsageStats(String str, int i, long j, Map<String, Long> map) {
        this.f2587a = str;
        this.b = i;
        this.c = j;
        this.d = new HashMap(map);
    }

    public PkgUsageStats(Parcel parcel) {
        this.f2587a = parcel.readString();
        this.b = parcel.readInt();
        this.c = parcel.readLong();
        int readInt = parcel.readInt();
        this.d = new HashMap(readInt);
        for (int i = 0; i < readInt; i++) {
            this.d.put(parcel.readString(), Long.valueOf(parcel.readLong()));
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f2587a);
        parcel.writeInt(this.b);
        parcel.writeLong(this.c);
        parcel.writeInt(this.d.size());
        for (Map.Entry next : this.d.entrySet()) {
            parcel.writeString((String) next.getKey());
            parcel.writeLong(((Long) next.getValue()).longValue());
        }
    }
}
