package com.tencent.assistant.link.a;

import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.link.sdk.a;
import com.tencent.assistant.utils.TemporaryThreadManager;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
class b implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f1396a;

    b(a aVar) {
        this.f1396a = aVar;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_INSTALL:
                if (message.obj instanceof String) {
                    String str = (String) message.obj;
                    if (!TextUtils.isEmpty(str) && this.f1396a.b.containsKey(str)) {
                        this.f1396a.b.put(str, Integer.valueOf(a.b));
                        TemporaryThreadManager.get().start(new c(this, str));
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_GOFRONT:
                if (this.f1396a.b.size() > 0) {
                    for (Map.Entry entry : new HashMap(this.f1396a.b).entrySet()) {
                        if (entry != null) {
                            String str2 = (String) entry.getKey();
                            Integer num = (Integer) entry.getValue();
                            if (num != null && num.intValue() == a.b) {
                                AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.APP_LINK_EVENT_INSTALLED_AND_ACTION_COMPLETE, str2));
                            }
                        }
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }
}
