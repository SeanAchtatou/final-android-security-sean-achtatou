package com.tencent.assistant.protocol;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.protocol.jce.PkgRsp;
import com.tencent.assistant.protocol.jce.Response;

/* compiled from: ProGuard */
public class b implements ProtocolDecoder {

    /* renamed from: a  reason: collision with root package name */
    private static final b f1964a = new b();

    private b() {
    }

    public Response decodeResponse(byte[] bArr) {
        return l.a(bArr);
    }

    public JceStruct decodeResponseBody(JceStruct jceStruct, byte[] bArr) {
        return l.a(jceStruct, bArr, null);
    }

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            bVar = f1964a;
        }
        return bVar;
    }

    public PkgRsp decodeResponseV2(byte[] bArr) {
        return l.b(bArr);
    }
}
