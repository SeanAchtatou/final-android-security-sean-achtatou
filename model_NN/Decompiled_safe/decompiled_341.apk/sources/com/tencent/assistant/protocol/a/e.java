package com.tencent.assistant.protocol.a;

import android.content.Context;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.protocol.a.a.a;
import com.tencent.assistant.protocol.a.a.b;
import com.tencent.assistant.protocol.a.a.c;
import com.tencent.assistant.protocol.i;
import com.tencent.assistant.protocol.jce.RspHead;
import com.tencent.assistant.protocol.jce.StatCSChannelData;
import com.tencent.connect.common.Constants;
import org.apache.http.impl.client.DefaultHttpClient;

/* compiled from: ProGuard */
public class e {
    private static e e;

    /* renamed from: a  reason: collision with root package name */
    private int f1963a = 0;
    private c b = null;
    private a c = null;
    private d d = null;
    private long f = 0;

    private e() {
        r();
        t();
        s();
    }

    private void r() {
        if (this.f1963a == 0) {
            try {
                this.b = (c) Class.forName("com.tencent.assistant.protocol.a.b.b").newInstance();
            } catch (ClassNotFoundException e2) {
                e2.printStackTrace();
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
            } catch (InstantiationException e4) {
                e4.printStackTrace();
            }
        }
    }

    private void s() {
        if (this.f1963a == 0) {
            try {
                this.c = (a) Class.forName("com.tencent.assistant.protocol.a.b.a").newInstance();
            } catch (ClassNotFoundException e2) {
                e2.printStackTrace();
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
            } catch (InstantiationException e4) {
                e4.printStackTrace();
            }
        }
    }

    private void t() {
        if (this.f1963a == 0) {
            try {
                this.d = (d) Class.forName("com.tencent.assistant.protocol.a.b.d").newInstance();
            } catch (ClassNotFoundException e2) {
                e2.printStackTrace();
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
            } catch (InstantiationException e4) {
                e4.printStackTrace();
            }
        }
    }

    public static synchronized e a() {
        e eVar;
        synchronized (e.class) {
            if (e == null) {
                e = new e();
            }
            eVar = e;
        }
        return eVar;
    }

    public void a(RspHead rspHead) {
        if (this.b != null) {
            this.b.a(rspHead);
            this.f = rspHead.k - (System.currentTimeMillis() / 1000);
            i.a("Environment", "onUpdateRspHeadData svrTimestampDiff...." + this.f);
        }
    }

    public DefaultHttpClient b() {
        DefaultHttpClient defaultHttpClient = null;
        if (this.b != null) {
            defaultHttpClient = this.b.a();
        }
        if (defaultHttpClient == null) {
            return new DefaultHttpClient();
        }
        return defaultHttpClient;
    }

    public b c() {
        if (this.b != null) {
            return this.b.b();
        }
        return null;
    }

    public c d() {
        if (this.b != null) {
            return this.b.c();
        }
        return null;
    }

    public String e() {
        if (this.b != null) {
            return this.b.e();
        }
        return Constants.STR_EMPTY;
    }

    public void a(boolean z, long j) {
        if (this.b != null) {
            this.b.a(z, j);
        }
    }

    public String f() {
        if (this.b == null) {
            return Constants.STR_EMPTY;
        }
        this.b.d();
        return Constants.STR_EMPTY;
    }

    public void a(a aVar) {
        if (this.b != null) {
            this.b.a(aVar);
        }
    }

    public void a(StatCSChannelData statCSChannelData) {
        if (this.b != null) {
            this.b.a(statCSChannelData);
        }
    }

    public Context g() {
        if (this.c != null) {
            return this.c.a();
        }
        return null;
    }

    public int h() {
        if (this.c != null) {
            return this.c.b();
        }
        return 0;
    }

    public boolean i() {
        if (this.c != null) {
            return this.c.c();
        }
        return true;
    }

    public void j() {
        if (this.c != null) {
            this.c.d();
        }
    }

    public boolean a(JceStruct jceStruct) {
        if (jceStruct == null || this.d == null) {
            return false;
        }
        return this.d.a(jceStruct);
    }

    public int b(JceStruct jceStruct) {
        if (jceStruct == null || this.d == null) {
            return 0;
        }
        return this.d.b(jceStruct);
    }

    public JceStruct c(JceStruct jceStruct) {
        if (jceStruct == null || this.d == null) {
            return null;
        }
        return this.d.c(jceStruct);
    }

    public b k() {
        if (this.d != null) {
            return this.d.a();
        }
        return null;
    }

    public long l() {
        return this.f;
    }

    public long m() {
        if (this.c != null) {
            return this.c.e() * 1000;
        }
        return 3600000;
    }

    public boolean n() {
        if (this.c != null) {
            return this.c.f();
        }
        return false;
    }

    public long o() {
        if (this.c != null) {
            return (long) (this.c.g() * 1000);
        }
        return 60000;
    }

    public int p() {
        if (this.b != null) {
            return this.b.f();
        }
        return -1;
    }

    public int q() {
        switch (p()) {
            case 1:
                return 15000;
            case 2:
                return 30000;
            case 3:
            default:
                return 45000;
        }
    }
}
