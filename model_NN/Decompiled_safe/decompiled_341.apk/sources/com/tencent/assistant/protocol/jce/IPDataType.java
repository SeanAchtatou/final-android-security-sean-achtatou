package com.tencent.assistant.protocol.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class IPDataType implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final IPDataType f2217a = new IPDataType(0, 1, "IPDATA_TYPE_API");
    public static final IPDataType b = new IPDataType(1, 2, "IPDATA_TYPE_IMAGE_DOWNLOAD");
    public static final IPDataType c = new IPDataType(2, 3, "IPDATA_TYPE_DEFAULT");
    static final /* synthetic */ boolean d = (!IPDataType.class.desiredAssertionStatus());
    private static IPDataType[] e = new IPDataType[3];
    private int f;
    private String g = new String();

    public String toString() {
        return this.g;
    }

    private IPDataType(int i, int i2, String str) {
        this.g = str;
        this.f = i2;
        e[i] = this;
    }
}
