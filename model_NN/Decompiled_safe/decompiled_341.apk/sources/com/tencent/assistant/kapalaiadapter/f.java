package com.tencent.assistant.kapalaiadapter;

import com.tencent.assistant.kapalaiadapter.IMoblieModelConfig;

/* compiled from: ProGuard */
/* synthetic */ class f {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f1391a = new int[IMoblieModelConfig.SAMSUNG.values().length];
    static final /* synthetic */ int[] b = new int[IMoblieModelConfig.HTC.values().length];
    static final /* synthetic */ int[] c = new int[IMoblieModelConfig.HUAWEI.values().length];
    static final /* synthetic */ int[] d = new int[IMoblieModelConfig.ZTE.values().length];
    static final /* synthetic */ int[] e = new int[IMoblieModelConfig.XIAOMI.values().length];
    static final /* synthetic */ int[] f = new int[IMoblieModelConfig.ALPS.values().length];
    static final /* synthetic */ int[] g = new int[IMoblieModelConfig.COOLPAD.values().length];
    static final /* synthetic */ int[] h = new int[IMoblieModelConfig.LENOVO.values().length];
    static final /* synthetic */ int[] i = new int[IMoblieModelConfig.BBK.values().length];
    static final /* synthetic */ int[] j = new int[IMoblieModelConfig.SONY.values().length];
    static final /* synthetic */ int[] k = new int[IMoblieModelConfig.OPPO.values().length];
    static final /* synthetic */ int[] l = new int[IMoblieModelConfig.GIONEE.values().length];
    static final /* synthetic */ int[] m = new int[IMoblieModelConfig.YULONG.values().length];

    static {
        try {
            m[IMoblieModelConfig.YULONG._8020.ordinal()] = 1;
        } catch (NoSuchFieldError e2) {
        }
        try {
            m[IMoblieModelConfig.YULONG._8022.ordinal()] = 2;
        } catch (NoSuchFieldError e3) {
        }
        try {
            m[IMoblieModelConfig.YULONG._8060.ordinal()] = 3;
        } catch (NoSuchFieldError e4) {
        }
        try {
            m[IMoblieModelConfig.YULONG._5860A.ordinal()] = 4;
        } catch (NoSuchFieldError e5) {
        }
        try {
            m[IMoblieModelConfig.YULONG._COOLPAD_5950.ordinal()] = 5;
        } catch (NoSuchFieldError e6) {
        }
        try {
            l[IMoblieModelConfig.GIONEE._V182.ordinal()] = 1;
        } catch (NoSuchFieldError e7) {
        }
        try {
            l[IMoblieModelConfig.GIONEE._GN137.ordinal()] = 2;
        } catch (NoSuchFieldError e8) {
        }
        try {
            k[IMoblieModelConfig.OPPO._R821T.ordinal()] = 1;
        } catch (NoSuchFieldError e9) {
        }
        try {
            k[IMoblieModelConfig.OPPO._R819T.ordinal()] = 2;
        } catch (NoSuchFieldError e10) {
        }
        try {
            k[IMoblieModelConfig.OPPO._R831T.ordinal()] = 3;
        } catch (NoSuchFieldError e11) {
        }
        try {
            k[IMoblieModelConfig.OPPO._R827T.ordinal()] = 4;
        } catch (NoSuchFieldError e12) {
        }
        try {
            k[IMoblieModelConfig.OPPO._R829T.ordinal()] = 5;
        } catch (NoSuchFieldError e13) {
        }
        try {
            k[IMoblieModelConfig.OPPO._R815T.ordinal()] = 6;
        } catch (NoSuchFieldError e14) {
        }
        try {
            k[IMoblieModelConfig.OPPO._R823T.ordinal()] = 7;
        } catch (NoSuchFieldError e15) {
        }
        try {
            k[IMoblieModelConfig.OPPO._U707T.ordinal()] = 8;
        } catch (NoSuchFieldError e16) {
        }
        try {
            k[IMoblieModelConfig.OPPO._R833T.ordinal()] = 9;
        } catch (NoSuchFieldError e17) {
        }
        try {
            k[IMoblieModelConfig.OPPO._R813T.ordinal()] = 10;
        } catch (NoSuchFieldError e18) {
        }
        try {
            j[IMoblieModelConfig.SONY._S39H.ordinal()] = 1;
        } catch (NoSuchFieldError e19) {
        }
        try {
            j[IMoblieModelConfig.SONY._MT15I.ordinal()] = 2;
        } catch (NoSuchFieldError e20) {
        }
        try {
            j[IMoblieModelConfig.SONY._LT26I.ordinal()] = 3;
        } catch (NoSuchFieldError e21) {
        }
        try {
            j[IMoblieModelConfig.SONY._LT26II.ordinal()] = 4;
        } catch (NoSuchFieldError e22) {
        }
        try {
            i[IMoblieModelConfig.BBK._VIVO_Y11.ordinal()] = 1;
        } catch (NoSuchFieldError e23) {
        }
        try {
            i[IMoblieModelConfig.BBK._VIVO_Y17T.ordinal()] = 2;
        } catch (NoSuchFieldError e24) {
        }
        try {
            i[IMoblieModelConfig.BBK._VIVO_Y11I_T.ordinal()] = 3;
        } catch (NoSuchFieldError e25) {
        }
        try {
            i[IMoblieModelConfig.BBK._VIVO_Y15T.ordinal()] = 4;
        } catch (NoSuchFieldError e26) {
        }
        try {
            i[IMoblieModelConfig.BBK._VIVO_S7T.ordinal()] = 5;
        } catch (NoSuchFieldError e27) {
        }
        try {
            i[IMoblieModelConfig.BBK._VIVO_X1ST.ordinal()] = 6;
        } catch (NoSuchFieldError e28) {
        }
        try {
            i[IMoblieModelConfig.BBK._VIVO_Y13.ordinal()] = 7;
        } catch (NoSuchFieldError e29) {
        }
        try {
            i[IMoblieModelConfig.BBK._VIVO_S7.ordinal()] = 8;
        } catch (NoSuchFieldError e30) {
        }
        try {
            i[IMoblieModelConfig.BBK._VIVO_Y19T.ordinal()] = 9;
        } catch (NoSuchFieldError e31) {
        }
        try {
            i[IMoblieModelConfig.BBK._VIVO_S11T.ordinal()] = 10;
        } catch (NoSuchFieldError e32) {
        }
        try {
            i[IMoblieModelConfig.BBK._VIVO_Y3T.ordinal()] = 11;
        } catch (NoSuchFieldError e33) {
        }
        try {
            i[IMoblieModelConfig.BBK._VIVO_Y20T.ordinal()] = 12;
        } catch (NoSuchFieldError e34) {
        }
        try {
            i[IMoblieModelConfig.BBK._VIVO_S9.ordinal()] = 13;
        } catch (NoSuchFieldError e35) {
        }
        try {
            h[IMoblieModelConfig.LENOVO._LENOVO_A390T.ordinal()] = 1;
        } catch (NoSuchFieldError e36) {
        }
        try {
            h[IMoblieModelConfig.LENOVO._LENOVO_A630T.ordinal()] = 2;
        } catch (NoSuchFieldError e37) {
        }
        try {
            h[IMoblieModelConfig.LENOVO._LENOVO_A308T.ordinal()] = 3;
        } catch (NoSuchFieldError e38) {
        }
        try {
            h[IMoblieModelConfig.LENOVO._LENOVO_A820T.ordinal()] = 4;
        } catch (NoSuchFieldError e39) {
        }
        try {
            h[IMoblieModelConfig.LENOVO._LENOVO_A850.ordinal()] = 5;
        } catch (NoSuchFieldError e40) {
        }
        try {
            h[IMoblieModelConfig.LENOVO._LENOVO_A670T.ordinal()] = 6;
        } catch (NoSuchFieldError e41) {
        }
        try {
            h[IMoblieModelConfig.LENOVO._LENOVO_A278T.ordinal()] = 7;
        } catch (NoSuchFieldError e42) {
        }
        try {
            h[IMoblieModelConfig.LENOVO._LENOVO_S720.ordinal()] = 8;
        } catch (NoSuchFieldError e43) {
        }
        try {
            h[IMoblieModelConfig.LENOVO._LENOVO_A789.ordinal()] = 9;
        } catch (NoSuchFieldError e44) {
        }
        try {
            h[IMoblieModelConfig.LENOVO._S890.ordinal()] = 10;
        } catch (NoSuchFieldError e45) {
        }
        try {
            h[IMoblieModelConfig.LENOVO._LENOVO_S890.ordinal()] = 11;
        } catch (NoSuchFieldError e46) {
        }
        try {
            h[IMoblieModelConfig.LENOVO._LENOVO_A820.ordinal()] = 12;
        } catch (NoSuchFieldError e47) {
        }
        try {
            g[IMoblieModelConfig.COOLPAD._COOLPAD_8297.ordinal()] = 1;
        } catch (NoSuchFieldError e48) {
        }
        try {
            g[IMoblieModelConfig.COOLPAD._COOLPAD_8079.ordinal()] = 2;
        } catch (NoSuchFieldError e49) {
        }
        try {
            g[IMoblieModelConfig.COOLPAD._COOLPAD_7296.ordinal()] = 3;
        } catch (NoSuchFieldError e50) {
        }
        try {
            g[IMoblieModelConfig.COOLPAD._COOLPAD_7295C.ordinal()] = 4;
        } catch (NoSuchFieldError e51) {
        }
        try {
            g[IMoblieModelConfig.COOLPAD._COOLPAD_7295A.ordinal()] = 5;
        } catch (NoSuchFieldError e52) {
        }
        try {
            g[IMoblieModelConfig.COOLPAD._COOLPAD_8076D.ordinal()] = 6;
        } catch (NoSuchFieldError e53) {
        }
        try {
            g[IMoblieModelConfig.COOLPAD._COOLPAD7295.ordinal()] = 7;
        } catch (NoSuchFieldError e54) {
        }
        try {
            g[IMoblieModelConfig.COOLPAD._8720.ordinal()] = 8;
        } catch (NoSuchFieldError e55) {
        }
        try {
            g[IMoblieModelConfig.COOLPAD._COOLPAD_7270.ordinal()] = 9;
        } catch (NoSuchFieldError e56) {
        }
        try {
            f[IMoblieModelConfig.ALPS._R811.ordinal()] = 1;
        } catch (NoSuchFieldError e57) {
        }
        try {
            f[IMoblieModelConfig.ALPS._R801.ordinal()] = 2;
        } catch (NoSuchFieldError e58) {
        }
        try {
            f[IMoblieModelConfig.ALPS._U701.ordinal()] = 3;
        } catch (NoSuchFieldError e59) {
        }
        try {
            e[IMoblieModelConfig.XIAOMI._MI_1S.ordinal()] = 1;
        } catch (NoSuchFieldError e60) {
        }
        try {
            e[IMoblieModelConfig.XIAOMI._2013022.ordinal()] = 2;
        } catch (NoSuchFieldError e61) {
        }
        try {
            e[IMoblieModelConfig.XIAOMI._HM_1SC.ordinal()] = 3;
        } catch (NoSuchFieldError e62) {
        }
        try {
            e[IMoblieModelConfig.XIAOMI._HM_NOTE_1TD.ordinal()] = 4;
        } catch (NoSuchFieldError e63) {
        }
        try {
            d[IMoblieModelConfig.ZTE._ZTE_V889D.ordinal()] = 1;
        } catch (NoSuchFieldError e64) {
        }
        try {
            c[IMoblieModelConfig.HUAWEI._HUAWEI_Y511_T00.ordinal()] = 1;
        } catch (NoSuchFieldError e65) {
        }
        try {
            c[IMoblieModelConfig.HUAWEI._H30_T00.ordinal()] = 2;
        } catch (NoSuchFieldError e66) {
        }
        try {
            c[IMoblieModelConfig.HUAWEI._HUAWEI_G520_0000.ordinal()] = 3;
        } catch (NoSuchFieldError e67) {
        }
        try {
            c[IMoblieModelConfig.HUAWEI._HUAWEI_G610_U00.ordinal()] = 4;
        } catch (NoSuchFieldError e68) {
        }
        try {
            c[IMoblieModelConfig.HUAWEI._HUAWEI_G610_T11.ordinal()] = 5;
        } catch (NoSuchFieldError e69) {
        }
        try {
            c[IMoblieModelConfig.HUAWEI._H30_U10.ordinal()] = 6;
        } catch (NoSuchFieldError e70) {
        }
        try {
            c[IMoblieModelConfig.HUAWEI._HUAWEI_P6_C00.ordinal()] = 7;
        } catch (NoSuchFieldError e71) {
        }
        try {
            c[IMoblieModelConfig.HUAWEI._H30_T10.ordinal()] = 8;
        } catch (NoSuchFieldError e72) {
        }
        try {
            c[IMoblieModelConfig.HUAWEI._HUAWEI_A199.ordinal()] = 9;
        } catch (NoSuchFieldError e73) {
        }
        try {
            c[IMoblieModelConfig.HUAWEI._HUAWEI_Y321_C00.ordinal()] = 10;
        } catch (NoSuchFieldError e74) {
        }
        try {
            c[IMoblieModelConfig.HUAWEI._HUAWEI_C8813D.ordinal()] = 11;
        } catch (NoSuchFieldError e75) {
        }
        try {
            c[IMoblieModelConfig.HUAWEI._HUAWEI_U8825D.ordinal()] = 12;
        } catch (NoSuchFieldError e76) {
        }
        try {
            c[IMoblieModelConfig.HUAWEI._HUAWEI_Y325_T00.ordinal()] = 13;
        } catch (NoSuchFieldError e77) {
        }
        try {
            c[IMoblieModelConfig.HUAWEI._HUAWEI_G610_C00.ordinal()] = 14;
        } catch (NoSuchFieldError e78) {
        }
        try {
            c[IMoblieModelConfig.HUAWEI._HUAWEI_Y320_T00.ordinal()] = 15;
        } catch (NoSuchFieldError e79) {
        }
        try {
            c[IMoblieModelConfig.HUAWEI._HUAWEI_G750_T00.ordinal()] = 16;
        } catch (NoSuchFieldError e80) {
        }
        try {
            b[IMoblieModelConfig.HTC._HTC_T328T.ordinal()] = 1;
        } catch (NoSuchFieldError e81) {
        }
        try {
            b[IMoblieModelConfig.HTC._HTC_T528T.ordinal()] = 2;
        } catch (NoSuchFieldError e82) {
        }
        try {
            b[IMoblieModelConfig.HTC._HTC_T528W.ordinal()] = 3;
        } catch (NoSuchFieldError e83) {
        }
        try {
            b[IMoblieModelConfig.HTC._HTC_INCREDIBLE_S.ordinal()] = 4;
        } catch (NoSuchFieldError e84) {
        }
        try {
            b[IMoblieModelConfig.HTC._HTC_EVO_3D_X515m.ordinal()] = 5;
        } catch (NoSuchFieldError e85) {
        }
        try {
            b[IMoblieModelConfig.HTC._HTC_T328W.ordinal()] = 6;
        } catch (NoSuchFieldError e86) {
        }
        try {
            b[IMoblieModelConfig.HTC._HTC_D816W.ordinal()] = 7;
        } catch (NoSuchFieldError e87) {
        }
        try {
            b[IMoblieModelConfig.HTC._HTC_T328D.ordinal()] = 8;
        } catch (NoSuchFieldError e88) {
        }
        try {
            b[IMoblieModelConfig.HTC._HTC_T528D.ordinal()] = 9;
        } catch (NoSuchFieldError e89) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._GT_I8552.ordinal()] = 1;
        } catch (NoSuchFieldError e90) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._GT_S7572.ordinal()] = 2;
        } catch (NoSuchFieldError e91) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._GT_I8262D.ordinal()] = 3;
        } catch (NoSuchFieldError e92) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._GT_N7102.ordinal()] = 4;
        } catch (NoSuchFieldError e93) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._SM_N9009.ordinal()] = 5;
        } catch (NoSuchFieldError e94) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._GT_I9152.ordinal()] = 6;
        } catch (NoSuchFieldError e95) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._GT_S7562I.ordinal()] = 7;
        } catch (NoSuchFieldError e96) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._SCH_I959.ordinal()] = 8;
        } catch (NoSuchFieldError e97) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._GT_I8558.ordinal()] = 9;
        } catch (NoSuchFieldError e98) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._SM_N9002.ordinal()] = 10;
        } catch (NoSuchFieldError e99) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._SCH_I869.ordinal()] = 11;
        } catch (NoSuchFieldError e100) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._SCH_N719.ordinal()] = 12;
        } catch (NoSuchFieldError e101) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._GT_I9502.ordinal()] = 13;
        } catch (NoSuchFieldError e102) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._GT_I9082.ordinal()] = 14;
        } catch (NoSuchFieldError e103) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._SCH_I829.ordinal()] = 15;
        } catch (NoSuchFieldError e104) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._GT_I9158.ordinal()] = 16;
        } catch (NoSuchFieldError e105) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._SCH_I879.ordinal()] = 17;
        } catch (NoSuchFieldError e106) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._SM_G3812.ordinal()] = 18;
        } catch (NoSuchFieldError e107) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._SM_G7108.ordinal()] = 19;
        } catch (NoSuchFieldError e108) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._SM_G3502U.ordinal()] = 20;
        } catch (NoSuchFieldError e109) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._SM_G3502.ordinal()] = 21;
        } catch (NoSuchFieldError e110) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._GT_S6352.ordinal()] = 22;
        } catch (NoSuchFieldError e111) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._SCH_W2013.ordinal()] = 23;
        } catch (NoSuchFieldError e112) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._SM_G3819D.ordinal()] = 24;
        } catch (NoSuchFieldError e113) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._SCH_I939.ordinal()] = 25;
        } catch (NoSuchFieldError e114) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._SM_G7106.ordinal()] = 26;
        } catch (NoSuchFieldError e115) {
        }
        try {
            f1391a[IMoblieModelConfig.SAMSUNG._GT_I9082I.ordinal()] = 27;
        } catch (NoSuchFieldError e116) {
        }
    }
}
