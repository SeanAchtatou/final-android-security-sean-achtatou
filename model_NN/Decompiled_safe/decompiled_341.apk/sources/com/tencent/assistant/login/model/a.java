package com.tencent.assistant.login.model;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.protocol.jce.Ticket;
import com.tencent.assistant.utils.bh;

/* compiled from: ProGuard */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    protected AppConst.IdentityType f1461a;
    protected byte[] b;
    protected Ticket c;

    /* access modifiers changed from: protected */
    public abstract JceStruct a();

    /* access modifiers changed from: protected */
    public abstract byte[] getKey();

    public a(AppConst.IdentityType identityType) {
        this.f1461a = identityType;
    }

    public AppConst.IdentityType getType() {
        return this.f1461a;
    }

    public Ticket getTicket() {
        if (this.c == null) {
            this.c = new Ticket();
            this.c.f2396a = (byte) this.f1461a.ordinal();
            JceStruct a2 = a();
            if (a2 != null) {
                this.c.b = bh.a(a2);
            } else {
                this.c.b = new byte[0];
            }
        }
        return this.c;
    }

    public byte[] encryptBody(byte[] bArr) {
        if (this.b == null) {
            this.b = getKey();
        }
        return this.b != null ? bh.a(bArr, this.b) : bArr;
    }

    public byte[] decryptBody(byte[] bArr) {
        if (this.b == null) {
            this.b = getKey();
        }
        return this.b != null ? bh.b(bArr, this.b) : bArr;
    }
}
