package com.tencent.assistant.login;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.m;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class LoginRichDialog extends BaseActivity implements View.OnClickListener, UIEventListener {
    private static final String n = LoginRichDialog.class.getSimpleName();
    private final int A = 350;
    private ImageView t;
    private RelativeLayout u = null;
    private RelativeLayout v = null;
    private Context w = null;
    private final String x = "03_001";
    private final String y = "03_003";
    private final String z = "03_005";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.w = this;
        try {
            setContentView((int) R.layout.login_rich_dialog_layout);
        } catch (Throwable th) {
        }
        getWindow().setLayout((int) (((float) getWindow().getWindowManager().getDefaultDisplay().getWidth()) * 0.8667f), -2);
        i();
        j();
        XLog.i(n, "*** onCreate ***");
    }

    private void i() {
        this.t = (ImageView) findViewById(R.id.iv_login_dialog_tips_bg);
        try {
            Bitmap a2 = m.a(R.drawable.login_dialog_tips_bg);
            if (a2 != null && !a2.isRecycled()) {
                this.t.setImageBitmap(a2);
            }
            this.u = (RelativeLayout) findViewById(R.id.qq_account_login);
            this.v = (RelativeLayout) findViewById(R.id.wx_account_login);
            this.u.setOnClickListener(this);
            this.v.setOnClickListener(this);
            if (!w()) {
                this.v.setVisibility(8);
            }
        } catch (Throwable th) {
            cq.a().b();
            finish();
        }
    }

    public void onClick(View view) {
        if (R.id.qq_account_login == view.getId()) {
            XLog.i(n, "QQ登录");
            Bundle bundle = new Bundle();
            bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
            bundle.putInt(AppConst.KEY_FROM_TYPE, 8);
            d.a().a(AppConst.IdentityType.MOBILEQ, bundle);
            if (!isFinishing()) {
                finish();
            }
            a("03_001", Constants.STR_EMPTY, 200);
        } else if (R.id.wx_account_login == view.getId()) {
            XLog.i(n, "微信登录");
            Bundle bundle2 = new Bundle();
            bundle2.putInt(AppConst.KEY_FROM_TYPE, 8);
            d.a().a(AppConst.IdentityType.WX, bundle2);
            if (!isFinishing()) {
                finish();
            }
            a("03_003", Constants.STR_EMPTY, 200);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (isFinishing()) {
            return true;
        }
        finish();
        a("03_005", Constants.STR_EMPTY, 200);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        XLog.i(n, "*** onResume ***");
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        XLog.i(n, "*** onDestroy ***");
        v();
        super.onDestroy();
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (4 != i || keyEvent.getRepeatCount() != 0) {
            return true;
        }
        finish();
        a("03_005", Constants.STR_EMPTY, 200);
        return true;
    }

    public void onConfigurationChanged(Configuration configuration) {
        XLog.i(n, "*** onConfigurationChanged ***");
        super.onConfigurationChanged(configuration);
    }

    public void overridePendingTransition(int i, int i2) {
    }

    public boolean onMenuOpened(int i, Menu menu) {
        return true;
    }

    public int f() {
        if (!d.a().v()) {
            return STConst.ST_PAGE_LOGIN_RICH_DIALOG_QQ;
        }
        return STConst.ST_PAGE_LOGIN_RICH_DIALOG_QQ_WX;
    }

    private void a(String str, String str2, int i) {
        XLog.i(n, "[logReport] ---> actionId = " + i + ", slotId = " + str + ", extraData = " + str2);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, i);
        buildSTInfo.slotId = str;
        buildSTInfo.extraData = str2;
        k.a(buildSTInfo);
    }

    private void j() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
    }

    private void v() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL:
            case EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL:
                if (!isFinishing()) {
                    finish();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private boolean w() {
        return e.b("com.tencent.mm", 350) && d.a().v();
    }
}
