package com.tencent.assistant.thumbnailCache;

import android.graphics.Bitmap;
import android.text.TextUtils;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.au;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.net.APN;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class k implements NetworkMonitor.ConnectivityChangeListener, au {
    private static k h = null;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public f f2577a = new f();
    /* access modifiers changed from: private */
    public ArrayList<o> b = new ArrayList<>();
    private Map<String, o> c = new LinkedHashMap();
    /* access modifiers changed from: private */
    public Map<String, o> d = new LinkedHashMap();
    /* access modifiers changed from: private */
    public Map<String, o> e = new LinkedHashMap();
    /* access modifiers changed from: private */
    public boolean f = true;
    /* access modifiers changed from: private */
    public long g;
    private p i = new l(this);

    static /* synthetic */ long a(k kVar, long j) {
        long j2 = kVar.g + j;
        kVar.g = j2;
        return j2;
    }

    public static k b() {
        if (h == null) {
            h = new k();
        }
        return h;
    }

    private k() {
        m mVar = new m(this);
        mVar.setDaemon(true);
        mVar.start();
        cq.a().a((NetworkMonitor.ConnectivityChangeListener) this);
        cq.a().a((au) this);
        this.g = System.currentTimeMillis() + 60000;
    }

    private void b(String str, int i2, p pVar) {
        o oVar = new o(str, i2);
        oVar.a(this.i);
        oVar.a(pVar);
        a(oVar);
    }

    /* access modifiers changed from: private */
    public void a(o oVar) {
        if (oVar != null) {
            synchronized (this.b) {
                o oVar2 = this.c.get(oVar.c());
                if (oVar2 != null) {
                    oVar2.a(oVar);
                } else {
                    this.b.add(oVar);
                    this.c.put(oVar.c(), oVar);
                    this.b.notify();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(o oVar) {
        if (oVar != null) {
            oVar.b(this.i);
            o oVar2 = this.e.get(oVar.c());
            if (oVar2 != null) {
                oVar2.a(oVar);
            } else {
                this.e.put(oVar.c(), oVar);
            }
        }
    }

    /* access modifiers changed from: private */
    public o d() {
        o oVar;
        synchronized (this.b) {
            while (true) {
                if (this.b.size() == 0) {
                    oVar = null;
                    break;
                }
                oVar = this.b.get(0);
                this.b.remove(0);
                this.c.remove(oVar.c());
                if (1 != oVar.a()) {
                    break;
                }
            }
        }
        return oVar;
    }

    public Bitmap a(String str, int i2, p pVar) {
        Bitmap bitmap = null;
        if (!TextUtils.isEmpty(str)) {
            int c2 = o.c(i2);
            switch (c2) {
                case 9:
                    break;
                default:
                    bitmap = this.f2577a.a(str, c2);
                    if (bitmap == null) {
                        b(str, c2, pVar);
                        break;
                    }
                    break;
            }
        }
        return bitmap;
    }

    public void c() {
        this.f2577a.b();
    }

    public void onConnected(APN apn) {
        this.f2577a.a(apn);
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        this.f2577a.a(apn2);
    }

    public void a() {
        this.f2577a.d();
    }
}
