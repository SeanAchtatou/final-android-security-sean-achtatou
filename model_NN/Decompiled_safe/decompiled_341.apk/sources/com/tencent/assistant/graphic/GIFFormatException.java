package com.tencent.assistant.graphic;

/* compiled from: ProGuard */
public class GIFFormatException extends Exception {
    public GIFFormatException() {
        super("The image is not a GIF.");
    }
}
