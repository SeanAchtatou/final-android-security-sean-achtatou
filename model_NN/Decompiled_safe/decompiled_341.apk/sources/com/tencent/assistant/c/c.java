package com.tencent.assistant.c;

import com.tencent.assistant.db.table.p;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.protocol.jce.NpcCfg;
import com.tencent.assistant.protocol.jce.NpcListCfg;
import com.tencent.assistant.st.h;
import java.util.List;

/* compiled from: ProGuard */
public class c {
    private static c c = null;

    /* renamed from: a  reason: collision with root package name */
    private p f867a = new p();
    private boolean b = false;

    private c() {
    }

    public static synchronized c a() {
        c cVar;
        synchronized (c.class) {
            if (c == null) {
                c = new c();
            }
            cVar = c;
        }
        return cVar;
    }

    public NpcCfg b() {
        NpcCfg npcCfg;
        NpcListCfg p = as.w().p();
        if (p != null) {
            npcCfg = a(p.f2245a);
        } else {
            npcCfg = null;
        }
        this.b = npcCfg != null;
        return npcCfg;
    }

    public boolean a(NpcCfg npcCfg) {
        boolean b2 = b(npcCfg);
        this.b = b2;
        return b2;
    }

    public boolean c() {
        return this.b;
    }

    private boolean b(NpcCfg npcCfg) {
        if (npcCfg != null && c(npcCfg) && !d(npcCfg)) {
            return true;
        }
        return false;
    }

    public void a(NpcCfg npcCfg, boolean z) {
        int i = 0;
        if (!z) {
            i = this.f867a.a(npcCfg.f2244a) + 1;
        }
        this.f867a.a(npcCfg.f2244a, i);
    }

    private NpcCfg a(List<NpcCfg> list) {
        NpcCfg npcCfg = null;
        if (!(list == null || list.size() == 0)) {
            for (NpcCfg next : list) {
                if (next == null || !c(next) || ((npcCfg != null && npcCfg.f2244a >= next.f2244a) || d(next))) {
                    next = npcCfg;
                }
                npcCfg = next;
            }
        }
        return npcCfg;
    }

    private boolean c(NpcCfg npcCfg) {
        if (npcCfg != null) {
            long a2 = h.a();
            if (npcCfg.c < a2 && npcCfg.d > a2) {
                return true;
            }
            this.f867a.b(npcCfg.f2244a);
        }
        return false;
    }

    private boolean d(NpcCfg npcCfg) {
        if (npcCfg == null || this.f867a.a(npcCfg.f2244a) >= npcCfg.h) {
            return true;
        }
        return false;
    }
}
