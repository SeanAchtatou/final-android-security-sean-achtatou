package com.tencent.connector.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.connector.ConnectionActivity;

/* compiled from: ProGuard */
public class ContentConnectionTip extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f3523a;
    /* access modifiers changed from: private */
    public ConnectionActivity b;
    private Button c;
    private View.OnClickListener d;

    public ContentConnectionTip(Context context) {
        super(context);
        this.f3523a = context;
    }

    public ContentConnectionTip(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3523a = context;
    }

    public ContentConnectionTip(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f3523a = context;
    }

    public void setHostActivity(ConnectionActivity connectionActivity) {
        this.b = connectionActivity;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.c = (Button) findViewById(R.id.again);
        this.d = new g(this);
        if (this.c != null) {
            this.c.setOnClickListener(this.d);
        }
    }
}
