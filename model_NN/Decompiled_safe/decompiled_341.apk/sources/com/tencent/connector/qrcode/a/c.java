package com.tencent.connector.qrcode.a;

import android.hardware.Camera;
import java.util.Comparator;

/* compiled from: ProGuard */
final class c implements Comparator<Camera.Size> {
    c() {
    }

    /* renamed from: a */
    public int compare(Camera.Size size, Camera.Size size2) {
        int i = size.height * size.width;
        int i2 = size2.height * size2.width;
        if (i2 < i) {
            return -1;
        }
        if (i2 > i) {
            return 1;
        }
        return 0;
    }
}
