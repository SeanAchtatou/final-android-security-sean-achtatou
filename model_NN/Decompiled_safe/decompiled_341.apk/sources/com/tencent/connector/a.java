package com.tencent.connector;

import android.graphics.Rect;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import com.tencent.connector.component.ViewfinderView;

/* compiled from: ProGuard */
class a implements ViewfinderView.LaserAnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CaptureActivity f3512a;

    a(CaptureActivity captureActivity) {
        this.f3512a = captureActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.connector.CaptureActivity.a(com.tencent.connector.CaptureActivity, boolean):boolean
     arg types: [com.tencent.connector.CaptureActivity, int]
     candidates:
      com.tencent.connector.CaptureActivity.a(com.tencent.connector.CaptureActivity, android.media.MediaPlayer):android.media.MediaPlayer
      com.tencent.connector.CaptureActivity.a(com.tencent.connector.CaptureActivity, android.view.animation.Animation):android.view.animation.Animation
      com.tencent.connector.CaptureActivity.a(com.google.zxing.h, android.graphics.Bitmap):void
      com.tencent.connector.CaptureActivity.a(com.tencent.connector.CaptureActivity, boolean):boolean */
    public void onStartAnimation(Rect rect, int i) {
        if (!this.f3512a.n) {
            boolean unused = this.f3512a.n = true;
            if (rect != null) {
                this.f3512a.l.setLayoutParams(new LinearLayout.LayoutParams(rect.width() - (i * 2), -2));
                Animation unused2 = this.f3512a.m = new TranslateAnimation(0.0f, 0.0f, (float) (rect.bottom - this.f3512a.l.getHeight()), (float) rect.top);
                this.f3512a.m.setDuration(3000);
                this.f3512a.m.setRepeatMode(1);
                this.f3512a.m.setInterpolator(new AccelerateDecelerateInterpolator());
                this.f3512a.m.setRepeatCount(-1);
                this.f3512a.l.setAnimation(this.f3512a.m);
                this.f3512a.m.startNow();
            }
        }
    }
}
