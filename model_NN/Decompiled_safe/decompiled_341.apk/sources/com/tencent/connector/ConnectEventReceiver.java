package com.tencent.connector;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.qq.AppService.AppService;
import com.qq.AppService.AstApp;
import com.qq.AppService.t;
import com.qq.l.p;
import com.qq.l.q;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.link.b;
import com.tencent.assistant.model.OnlinePCListItemModel;
import com.tencent.assistant.st.a;
import java.util.List;

/* compiled from: ProGuard */
public class ConnectEventReceiver extends BroadcastReceiver {
    private static boolean b = false;

    /* renamed from: a  reason: collision with root package name */
    private EventDispatcher f3508a;

    public void onReceive(Context context, Intent intent) {
        String action;
        if (context != null && intent != null && (action = intent.getAction()) != null) {
            if (this.f3508a == null) {
                this.f3508a = AstApp.i().j();
            }
            if (action.equals("disconnect")) {
                if (!b) {
                    this.f3508a.sendMessage(this.f3508a.obtainMessage(5001));
                    b = true;
                }
                AppService.C();
                AppService.A();
                AppService.E();
                AppService.G();
                q.m().o();
            } else if (action.equals("connect")) {
                b = false;
                AppService.o();
                this.f3508a.sendMessageDelayed(this.f3508a.obtainMessage(5002), 400);
                a.a().b((byte) 0);
                if (AppService.t()) {
                    if (TextUtils.isEmpty(AppService.g)) {
                        p.m().a(-1);
                    } else if (!AppService.g.contains("127.0.0.1")) {
                        p.m().a(0);
                    } else {
                        p.m().a(1);
                        q.m().n();
                    }
                    p.m().c(p.m().p().b(), 1, -1);
                    p.m().n();
                    p.m().o();
                    p.m().q();
                }
                t.a();
            } else if (action.equals("refresh")) {
                this.f3508a.sendMessage(this.f3508a.obtainMessage(5003));
            } else if (action.equals("refreshOnNetChange")) {
                this.f3508a.sendMessage(this.f3508a.obtainMessage(EventDispatcherEnum.CONNECTION_EVENT_WIFI_CHANGE));
            } else if (action.equals("destory")) {
                this.f3508a.sendMessage(this.f3508a.obtainMessage(EventDispatcherEnum.CONNECTION_EVENT_SERVICE_DESTROY));
            } else if (!action.equals("notification_connection") && !action.equals("notification_no_connection")) {
                if (action.equals("usb")) {
                    if (!intent.getBooleanExtra("value", false)) {
                        AppService.e();
                    } else if (com.tencent.connector.ipc.a.b(context)) {
                        AppService.n();
                    } else if (b(context)) {
                        a(context);
                    }
                } else if (action.equals("login")) {
                    Intent intent2 = new Intent(context, ConnectionActivity.class);
                    intent2.putExtras(intent);
                    intent2.setFlags(805306368);
                    context.startActivity(intent2);
                } else if (action.equals("pc_ping")) {
                    String stringExtra = intent.getStringExtra("name");
                    String stringExtra2 = intent.getStringExtra("ip");
                    String stringExtra3 = intent.getStringExtra("pc_guid");
                    intent.getBooleanExtra("author", false);
                    OnlinePCListItemModel onlinePCListItemModel = new OnlinePCListItemModel();
                    onlinePCListItemModel.f1629a = stringExtra3;
                    onlinePCListItemModel.b = stringExtra;
                    onlinePCListItemModel.d = stringExtra2;
                    onlinePCListItemModel.c = 69907;
                    this.f3508a.sendMessage(this.f3508a.obtainMessage(EventDispatcherEnum.CONNECTION_EVENT_PC_PING, onlinePCListItemModel));
                } else if (action.equals("cancelLogin")) {
                    this.f3508a.sendMessage(this.f3508a.obtainMessage(EventDispatcherEnum.CONNECTION_EVENT_CANCEL_LOGIN));
                } else if (action.equals("wcsLogin")) {
                    Intent intent3 = new Intent(context, ConnectionActivity.class);
                    intent3.putExtras(intent);
                    intent3.setFlags(805306368);
                    context.startActivity(intent3);
                } else if (action.equals("low_tip")) {
                    Intent intent4 = new Intent(context, ConnectionActivity.class);
                    intent4.putExtras(intent);
                    intent4.setFlags(805306368);
                    context.startActivity(intent4);
                }
            }
        }
    }

    private void a(Context context) {
        b.a(context, Uri.parse("tmast://usbdebugmode"));
    }

    private boolean b(Context context) {
        List<ActivityManager.RunningTaskInfo> runningTasks;
        if (context != null) {
            String packageName = context.getPackageName();
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            if (activityManager != null && (runningTasks = activityManager.getRunningTasks(1)) != null && !runningTasks.isEmpty() && runningTasks.get(0).topActivity.getPackageName().equals(packageName)) {
                return true;
            }
        }
        return false;
    }
}
