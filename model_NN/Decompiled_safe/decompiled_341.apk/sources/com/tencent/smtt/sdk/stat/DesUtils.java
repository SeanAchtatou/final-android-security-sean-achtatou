package com.tencent.smtt.sdk.stat;

import java.lang.reflect.Array;

public class DesUtils {
    private static final int[] E = {32, 1, 2, 3, 4, 5, 4, 5, 6, 7, 8, 9, 8, 9, 10, 11, 12, 13, 12, 13, 14, 15, 16, 17, 16, 17, 18, 19, 20, 21, 20, 21, 22, 23, 24, 25, 24, 25, 26, 27, 28, 29, 28, 29, 30, 31, 32, 1};
    public static final int FLAG_DECRYPT = 0;
    public static final int FLAG_ENCRYPT = 1;
    private static final int[] IP = {58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4, 62, 54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16, 8, 57, 49, 41, 33, 25, 17, 9, 1, 59, 51, 43, 35, 27, 19, 11, 3, 61, 53, 45, 37, 29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7};
    private static final int[] IP_1 = {40, 8, 48, 16, 56, 24, 64, 32, 39, 7, 47, 15, 55, 23, 63, 31, 38, 6, 46, 14, 54, 22, 62, 30, 37, 5, 45, 13, 53, 21, 61, 29, 36, 4, 44, 12, 52, 20, 60, 28, 35, 3, 43, 11, 51, 19, 59, 27, 34, 2, 42, 10, 50, 18, 58, 26, 33, 1, 41, 9, 49, 17, 57, 25};
    public static final byte[] KEY = {-25, -101, -115, 1, 47, 7, -27, -59, 18, Byte.MIN_VALUE, 123, 79, -44, 37, 46, 115};
    private static final int[] LeftMove = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};
    public static final byte[] MAC_KEY = {37, -110, 60, Byte.MAX_VALUE, 42, -27, -17, -110};
    public static final byte[] MTT_KEY = {-122, -8, -23, -84, -125, 113, 84, 99};
    private static final int[] P = {16, 7, 20, 21, 29, 12, 28, 17, 1, 15, 23, 26, 5, 18, 31, 10, 2, 8, 24, 14, 32, 27, 3, 9, 19, 13, 30, 6, 22, 11, 4, 25};
    private static final int[] PC_1 = {57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36, 63, 55, 47, 39, 31, 23, 15, 7, 62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20, 12, 4};
    private static final int[] PC_2 = {14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4, 26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40, 51, 45, 33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32};
    public static final byte[] QQMARKET_KEY = "AL!#$AC9Ahg@KLJ1".getBytes();
    public static final byte[] REPORT_KEY_TEA = {98, -24, 57, -84, -115, 117, 55, 121};
    private static final int[][][] S_Box = {new int[][]{new int[]{14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7}, new int[]{0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8}, new int[]{4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0}, new int[]{15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13}}, new int[][]{new int[]{15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10}, new int[]{3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5}, new int[]{0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15}, new int[]{13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9}}, new int[][]{new int[]{10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8}, new int[]{13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1}, new int[]{13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7}, new int[]{1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12}}, new int[][]{new int[]{7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15}, new int[]{13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9}, new int[]{10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4}, new int[]{3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14}}, new int[][]{new int[]{2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9}, new int[]{14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6}, new int[]{4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14}, new int[]{11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3}}, new int[][]{new int[]{12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11}, new int[]{10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8}, new int[]{9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6}, new int[]{4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13}}, new int[][]{new int[]{4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1}, new int[]{13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6}, new int[]{1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2}, new int[]{6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12}}, new int[][]{new int[]{13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7}, new int[]{1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2}, new int[]{7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8}, new int[]{2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11}}};

    private static byte[] UnitDes(byte[] des_key, byte[] des_data, int flag) {
        if (des_key.length == 8 && des_data.length == 8 && (flag == 1 || flag == 0)) {
            int[] iArr = new int[64];
            int[] iArr2 = new int[64];
            byte[] bArr = new byte[8];
            int[][] KeyArray = (int[][]) Array.newInstance(Integer.TYPE, 16, 48);
            int[] keydata = ReadDataToBirnaryIntArray(des_key);
            int[] encryptdata = ReadDataToBirnaryIntArray(des_data);
            KeyInitialize(keydata, KeyArray);
            return Encrypt(encryptdata, flag, KeyArray);
        }
        throw new RuntimeException("Data Format Error !");
    }

    private static void KeyInitialize(int[] key, int[][] keyarray) {
        int[] K0 = new int[56];
        for (int i = 0; i < 56; i++) {
            K0[i] = key[PC_1[i] - 1];
        }
        for (int i2 = 0; i2 < 16; i2++) {
            LeftBitMove(K0, LeftMove[i2]);
            for (int j = 0; j < 48; j++) {
                keyarray[i2][j] = K0[PC_2[j] - 1];
            }
        }
    }

    private static byte[] Encrypt(int[] timeData, int flag, int[][] keyarray) {
        byte[] encrypt = new byte[8];
        int flags = flag;
        int[] M = new int[64];
        int[] MIP_1 = new int[64];
        for (int i = 0; i < 64; i++) {
            M[i] = timeData[IP[i] - 1];
        }
        if (flags == 1) {
            for (int i2 = 0; i2 < 16; i2++) {
                LoopF(M, i2, flags, keyarray);
            }
        } else if (flags == 0) {
            for (int i3 = 15; i3 > -1; i3--) {
                LoopF(M, i3, flags, keyarray);
            }
        }
        for (int i4 = 0; i4 < 64; i4++) {
            MIP_1[i4] = M[IP_1[i4] - 1];
        }
        GetEncryptResultOfByteArray(MIP_1, encrypt);
        return encrypt;
    }

    /* JADX WARN: Type inference failed for: r7v0, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r4v6, types: [byte] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int[] ReadDataToBirnaryIntArray(byte[] r7) {
        /*
            r6 = 8
            int[] r0 = new int[r6]
            r2 = 0
        L_0x0005:
            if (r2 >= r6) goto L_0x001e
            byte r4 = r7[r2]
            r0[r2] = r4
            r4 = r0[r2]
            if (r4 >= 0) goto L_0x001b
            r4 = r0[r2]
            int r4 = r4 + 256
            r0[r2] = r4
            r4 = r0[r2]
            int r4 = r4 % 256
            r0[r2] = r4
        L_0x001b:
            int r2 = r2 + 1
            goto L_0x0005
        L_0x001e:
            r4 = 64
            int[] r1 = new int[r4]
            r2 = 0
        L_0x0023:
            if (r2 >= r6) goto L_0x003f
            r3 = 0
        L_0x0026:
            if (r3 >= r6) goto L_0x003c
            int r4 = r2 * 8
            int r4 = r4 + 7
            int r4 = r4 - r3
            r5 = r0[r2]
            int r5 = r5 % 2
            r1[r4] = r5
            r4 = r0[r2]
            int r4 = r4 / 2
            r0[r2] = r4
            int r3 = r3 + 1
            goto L_0x0026
        L_0x003c:
            int r2 = r2 + 1
            goto L_0x0023
        L_0x003f:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.smtt.sdk.stat.DesUtils.ReadDataToBirnaryIntArray(byte[]):int[]");
    }

    private static void LeftBitMove(int[] k, int offset) {
        int[] c0 = new int[28];
        int[] d0 = new int[28];
        int[] c1 = new int[28];
        int[] d1 = new int[28];
        for (int i = 0; i < 28; i++) {
            c0[i] = k[i];
            d0[i] = k[i + 28];
        }
        if (offset == 1) {
            for (int i2 = 0; i2 < 27; i2++) {
                c1[i2] = c0[i2 + 1];
                d1[i2] = d0[i2 + 1];
            }
            c1[27] = c0[0];
            d1[27] = d0[0];
        } else if (offset == 2) {
            for (int i3 = 0; i3 < 26; i3++) {
                c1[i3] = c0[i3 + 2];
                d1[i3] = d0[i3 + 2];
            }
            c1[26] = c0[0];
            d1[26] = d0[0];
            c1[27] = c0[1];
            d1[27] = d0[1];
        }
        for (int i4 = 0; i4 < 28; i4++) {
            k[i4] = c1[i4];
            k[i4 + 28] = d1[i4];
        }
    }

    private static void LoopF(int[] M, int times, int flag, int[][] keyarray) {
        int[] L0 = new int[32];
        int[] R0 = new int[32];
        int[] L1 = new int[32];
        int[] R1 = new int[32];
        int[] RE = new int[48];
        int[][] S = (int[][]) Array.newInstance(Integer.TYPE, 8, 6);
        int[] sBoxData = new int[8];
        int[] sValue = new int[32];
        int[] RP = new int[32];
        for (int i = 0; i < 32; i++) {
            L0[i] = M[i];
            R0[i] = M[i + 32];
        }
        for (int i2 = 0; i2 < 48; i2++) {
            RE[i2] = R0[E[i2] - 1];
            RE[i2] = RE[i2] + keyarray[times][i2];
            if (RE[i2] == 2) {
                RE[i2] = 0;
            }
        }
        for (int i3 = 0; i3 < 8; i3++) {
            for (int j = 0; j < 6; j++) {
                S[i3][j] = RE[(i3 * 6) + j];
            }
            sBoxData[i3] = S_Box[i3][(S[i3][0] << 1) + S[i3][5]][(S[i3][1] << 3) + (S[i3][2] << 2) + (S[i3][3] << 1) + S[i3][4]];
            for (int j2 = 0; j2 < 4; j2++) {
                sValue[((i3 * 4) + 3) - j2] = sBoxData[i3] % 2;
                sBoxData[i3] = sBoxData[i3] / 2;
            }
        }
        for (int i4 = 0; i4 < 32; i4++) {
            RP[i4] = sValue[P[i4] - 1];
            L1[i4] = R0[i4];
            R1[i4] = L0[i4] + RP[i4];
            if (R1[i4] == 2) {
                R1[i4] = 0;
            }
            if ((flag == 0 && times == 0) || (flag == 1 && times == 15)) {
                M[i4] = R1[i4];
                M[i4 + 32] = L1[i4];
            } else {
                M[i4] = L1[i4];
                M[i4 + 32] = R1[i4];
            }
        }
    }

    private static void GetEncryptResultOfByteArray(int[] data, byte[] value) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                value[i] = (byte) (value[i] + (data[(i << 3) + j] << (7 - j)));
            }
        }
    }

    private static byte[] ByteDataFormat(byte[] data) {
        int len = data.length;
        int padlen = 8 - (len % 8);
        int newlen = len + padlen;
        byte[] newdata = new byte[newlen];
        System.arraycopy(data, 0, newdata, 0, len);
        for (int i = len; i < newlen; i++) {
            newdata[i] = (byte) padlen;
        }
        return newdata;
    }

    private static byte[] KeyDataFormat(byte[] key) {
        byte[] fixkey = new byte[8];
        for (int i = 0; i < fixkey.length; i++) {
            fixkey[i] = 0;
        }
        if (key.length > 8) {
            System.arraycopy(key, 0, fixkey, 0, fixkey.length);
        } else {
            System.arraycopy(key, 0, fixkey, 0, key.length);
        }
        return fixkey;
    }

    public static byte[] DesEncrypt(byte[] des_key, byte[] des_data, int flag) {
        if (des_data == null || des_key == null) {
            return des_data;
        }
        try {
            byte[] format_key = KeyDataFormat(des_key);
            byte[] format_data = ByteDataFormat(des_data);
            int datalen = format_data.length;
            int unitcount = datalen / 8;
            byte[] result_data = new byte[datalen];
            for (int i = 0; i < unitcount; i++) {
                byte[] tmpkey = new byte[8];
                byte[] tmpdata = new byte[8];
                System.arraycopy(format_key, 0, tmpkey, 0, 8);
                System.arraycopy(format_data, i * 8, tmpdata, 0, 8);
                System.arraycopy(UnitDes(tmpkey, tmpdata, flag), 0, result_data, i * 8, 8);
            }
            if (flag == 0) {
                byte[] tempResData = new byte[des_data.length];
                System.arraycopy(result_data, 0, tempResData, 0, tempResData.length);
                byte b = tempResData[tempResData.length - 1];
                if (b > 0 && b <= 8) {
                    boolean bDesCheckResult = true;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= b) {
                            break;
                        } else if (b != tempResData[(tempResData.length - 1) - i2]) {
                            bDesCheckResult = false;
                            break;
                        } else {
                            i2++;
                        }
                    }
                    if (bDesCheckResult) {
                        result_data = new byte[(tempResData.length - b)];
                        System.arraycopy(tempResData, 0, result_data, 0, result_data.length);
                    }
                }
            }
            return result_data;
        } catch (Exception e) {
            return des_data;
        }
    }
}
