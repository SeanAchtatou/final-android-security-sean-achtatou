package com.tencent.smtt.sdk;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import com.tencent.connect.common.Constants;
import com.tencent.smtt.sdk.TesDownloadConfig;
import com.tencent.smtt.utils.PostEncryption;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;
import org.json.JSONObject;

public class TesDownloader {
    private static final String DEFAULT_ENCODE_NAME = "utf-8";
    private static final String DEFAULT_POST_ADDR = "http://cfg.imtt.qq.com/tbs";
    private static final int DEFAULT_TIME_OUT = 20000;
    private static final String LOGTAG = "SecuritySwitchPuller";
    private static final int MSG_CHECKCONFIG_AND_DOWNLOAD_TESAPK = 102;
    private static final int MSG_SEND_CHECKCONFIG_REQUEST = 100;
    private static final int MSG_START_DOWNLOAD_TESAPK = 101;
    private static final int PROTOCOL_VERSION = 1;
    private static final String TES_METADATA = "com.tencent.mtt.TES_CODE";
    private static final String THREAD_NAME = "SecuritySwitchPuller";
    private static Context sAppContext;
    private static String sDefalutUserAgent;
    private static Handler sTesDownloaderHandler = null;

    public static boolean needDownload(Context context) {
        sAppContext = context.getApplicationContext();
        TesDownloadConfig tesDownloadConfig = TesDownloadConfig.getInstance(sAppContext);
        if (sTesDownloaderHandler == null) {
            initTesDownloaderThread();
        }
        String oldAppVersionName = tesDownloadConfig.mAppVersionName;
        int oldAppVersionCode = tesDownloadConfig.mAppVersionCode;
        String oldAppVersionMetadata = tesDownloadConfig.mAppMetadata;
        String packageName = sAppContext.getPackageName();
        PackageManager packageManager = sAppContext.getPackageManager();
        String appVersionName = null;
        int appVersionCode = 0;
        String appMetadata = null;
        try {
            PackageInfo appPackageInfo = packageManager.getPackageInfo(packageName, 0);
            appVersionName = appPackageInfo.versionName;
            appVersionCode = appPackageInfo.versionCode;
            Bundle data = packageManager.getApplicationInfo(packageName, 128).metaData;
            if (data != null) {
                appMetadata = data.getString(TES_METADATA);
            } else {
                appMetadata = Constants.STR_EMPTY;
            }
        } catch (Throwable e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            Log.e("SecuritySwitchPuller", "needDownload - exceptions:" + errors.toString());
        }
        tesDownloadConfig.mSyncMap.put(TesDownloadConfig.TesConfigKey.KEY_APP_VERSIONNAME, appVersionName);
        tesDownloadConfig.mSyncMap.put(TesDownloadConfig.TesConfigKey.KEY_APP_VERSIONCODE, Integer.valueOf(appVersionCode));
        tesDownloadConfig.mSyncMap.put(TesDownloadConfig.TesConfigKey.KEY_APP_METADATA, appMetadata);
        long timeNow = System.currentTimeMillis();
        long delta = (timeNow - tesDownloadConfig.mLastCheckTime) - TesDownloadConfig.TES_CONFIG_CHECK_PERIOD;
        tesDownloadConfig.mLastCheckTime = timeNow;
        tesDownloadConfig.mSyncMap.put(TesDownloadConfig.TesConfigKey.KEY_LAST_CHECK, Long.valueOf(timeNow));
        tesDownloadConfig.commit();
        Log.e("SecuritySwitchPuller", "delta:" + delta);
        if (delta > 0) {
            checkConfig(true);
        } else if (!(appVersionName == null || appVersionCode == 0 || appMetadata == null || (appVersionName.equals(oldAppVersionName) && appVersionCode == oldAppVersionCode && appMetadata.equals(oldAppVersionMetadata)))) {
            checkConfig(true);
        }
        return tesDownloadConfig.mNeedDownload;
    }

    private static void initTesDownloaderThread() {
        QbSdkLog.e("SecuritySwitchPuller", "init SecuritySwitchPuller thread & handler...");
        HandlerThread handlerThread = new HandlerThread("SecuritySwitchPuller");
        handlerThread.start();
        sTesDownloaderHandler = new Handler(handlerThread.getLooper()) {
            /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
            public void handleMessage(Message msg) {
                try {
                    switch (msg.what) {
                        case 100:
                            TesDownloader.sendRequest(((Boolean) msg.obj).booleanValue());
                            return;
                        case 101:
                        default:
                            return;
                        case TesDownloader.MSG_CHECKCONFIG_AND_DOWNLOAD_TESAPK /*102*/:
                            TesDownloader.sendRequest(false);
                            return;
                    }
                } catch (Throwable e) {
                    StringWriter errors = new StringWriter();
                    e.printStackTrace(new PrintWriter(errors));
                    QbSdkLog.e("SecuritySwitchPuller", "handleMessage - exceptions:" + errors.toString());
                }
                StringWriter errors2 = new StringWriter();
                e.printStackTrace(new PrintWriter(errors2));
                QbSdkLog.e("SecuritySwitchPuller", "handleMessage - exceptions:" + errors2.toString());
            }
        };
    }

    private static void checkConfig(boolean isUpdate) {
        sTesDownloaderHandler.removeMessages(100);
        Message.obtain(sTesDownloaderHandler, 100, Boolean.valueOf(isUpdate)).sendToTarget();
    }

    private static JSONObject postJsonData(boolean isUpdate) {
        int i = 1;
        TesDownloadConfig tesDownloadConfig = TesDownloadConfig.getInstance(sAppContext);
        String ua = getDefaultUserAgent(sAppContext);
        String imsi = null;
        String imei = null;
        try {
            TelephonyManager telManager = (TelephonyManager) sAppContext.getSystemService("phone");
            imsi = telManager.getSubscriberId();
            imei = telManager.getDeviceId();
        } catch (Exception e) {
        }
        JSONObject jsonData = new JSONObject();
        try {
            jsonData.put("PROTOCOLVERSION", 1);
            jsonData.put("FUNCTION", 2);
            jsonData.put("APPN", sAppContext.getPackageName());
            jsonData.put("APPVN", notNullString(tesDownloadConfig.mAppVersionName));
            jsonData.put("APPVC", tesDownloadConfig.mAppVersionCode);
            jsonData.put("APPMETA", notNullString(tesDownloadConfig.mAppMetadata));
            jsonData.put("TESSDKV", 0);
            jsonData.put("TESV", 0);
            jsonData.put("CPU", notNullString(System.getProperty("os.arch")));
            jsonData.put("UA", ua);
            jsonData.put("IMSI", notNullString(imsi));
            jsonData.put("IMEI", notNullString(imei));
            jsonData.put("STATUS", 0);
            boolean securityStatus = SecuritySwitch.isSecurityApplyed();
            int securitySdkVersion = SecuritySwitch.getSecuritySdkVersion();
            int securityJsVersion = SecuritySwitch.getSecurityJsVersion();
            if (!securityStatus) {
                i = 0;
            }
            jsonData.put("SECURITY_STATUS", i);
            jsonData.put("SECURITY_VSDK", securitySdkVersion);
            jsonData.put("SECURITY_VJS", securityJsVersion);
        } catch (Exception e2) {
        }
        return jsonData;
    }

    /* access modifiers changed from: private */
    public static void sendRequest(boolean isUpdate) {
        InputStream nis;
        Log.e("SecuritySwitchPuller", "sendRequest...");
        TesDownloadConfig tesDownloadConfig = TesDownloadConfig.getInstance(sAppContext);
        JSONObject jsonData = postJsonData(isUpdate);
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://cfg.imtt.qq.com/tbs?mk=" + PostEncryption.getInstance().initRSAKey()).openConnection();
            httpURLConnection.setRequestMethod(Constants.HTTP_POST);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setConnectTimeout(DEFAULT_TIME_OUT);
            if (Build.VERSION.SDK_INT > 13) {
                httpURLConnection.setRequestProperty("Connection", "close");
            } else {
                httpURLConnection.setRequestProperty("http.keepAlive", "false");
            }
            byte[] postData = null;
            try {
                String lastUrl = tesDownloadConfig.mLastUrl;
                int lastError = tesDownloadConfig.mLastError;
                if (!TextUtils.isEmpty(lastUrl) && lastError != 0) {
                    jsonData.put("LASTURL", lastUrl);
                    jsonData.put("LASTERR", lastError);
                }
                postData = PostEncryption.getInstance().DESEncrypt(jsonData.toString().getBytes("utf-8"));
            } catch (Exception e) {
            }
            if (postData != null) {
                httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                httpURLConnection.setRequestProperty("Content-Length", String.valueOf(postData.length));
                try {
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    outputStream.write(postData);
                    outputStream.flush();
                    if (httpURLConnection.getResponseCode() == 200) {
                        tesDownloadConfig.mSyncMap.put(TesDownloadConfig.TesConfigKey.KEY_LASTERR, 0);
                        tesDownloadConfig.commit();
                        InputStream is = httpURLConnection.getInputStream();
                        String contentEncoding = httpURLConnection.getContentEncoding();
                        if (contentEncoding == null || !contentEncoding.equalsIgnoreCase("gzip")) {
                            if (contentEncoding != null) {
                                if (contentEncoding.equalsIgnoreCase("deflate")) {
                                    nis = new InflaterInputStream(is, new Inflater(true));
                                }
                            }
                            nis = is;
                        } else {
                            nis = new GZIPInputStream(is);
                        }
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        byte[] rspData = new byte[128];
                        while (true) {
                            int len = nis.read(rspData);
                            if (len != -1) {
                                bos.write(rspData, 0, len);
                            } else {
                                String str = new String(PostEncryption.getInstance().DESDecrypt(bos.toByteArray()), "utf-8");
                                bos.close();
                                readResponse(str);
                                return;
                            }
                        }
                    }
                } catch (Throwable e2) {
                    StringWriter errors = new StringWriter();
                    e2.printStackTrace(new PrintWriter(errors));
                    Log.e("SecuritySwitchPuller", "sendRequest - exceptions:" + errors.toString());
                }
            }
        } catch (Exception e3) {
        }
    }

    private static void readResponse(String response) throws Exception {
        boolean z;
        boolean z2 = true;
        Log.e("SecuritySwitchPuller", "readResponse...");
        if (!TextUtils.isEmpty(response)) {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("RET") == 0) {
                int x5disabled = jsonObject.getInt("USEX5");
                int responseCode = jsonObject.getInt("RESPONSECODE");
                String downloadUrl = jsonObject.getString("DOWNLOADURL");
                int tesApkServerVersion = jsonObject.getInt("TESAPKSERVERVERSION");
                int needSecurity = jsonObject.getInt("USE_SECURITY");
                TesDownloadConfig tesDownloadConfig = TesDownloadConfig.getInstance(sAppContext);
                Map<String, Object> map = tesDownloadConfig.mSyncMap;
                if (x5disabled == 1) {
                    z = false;
                } else {
                    z = true;
                }
                map.put(TesDownloadConfig.TesConfigKey.KEY_X5_DISABLED, Boolean.valueOf(z));
                tesDownloadConfig.mSyncMap.put(TesDownloadConfig.TesConfigKey.KEY_TESV, Integer.valueOf(tesApkServerVersion));
                if (0 == 0 || tesApkServerVersion > 0) {
                    tesDownloadConfig.mSyncMap.put(TesDownloadConfig.TesConfigKey.KEY_TESDOWNLOADURL, downloadUrl);
                    tesDownloadConfig.mSyncMap.put(TesDownloadConfig.TesConfigKey.KEY_NEEDDOWNLOAD, true);
                    tesDownloadConfig.mSyncMap.put(TesDownloadConfig.TesConfigKey.KEY_RESPONSECODE, Integer.valueOf(responseCode));
                } else {
                    tesDownloadConfig.mSyncMap.put(TesDownloadConfig.TesConfigKey.KEY_NEEDDOWNLOAD, false);
                }
                Map<String, Object> map2 = tesDownloadConfig.mSyncMap;
                if (needSecurity != 1) {
                    z2 = false;
                }
                map2.put(TesDownloadConfig.TesConfigKey.KEY_SECURITY_SWITCH, Boolean.valueOf(z2));
                tesDownloadConfig.commit();
            }
        }
    }

    static String getDefaultUserAgent(Context context) {
        String versionfinal;
        String modelTmp;
        if (!TextUtils.isEmpty(sDefalutUserAgent)) {
            return sDefalutUserAgent;
        }
        Locale locale = Locale.getDefault();
        StringBuffer buffer = new StringBuffer();
        String version = Build.VERSION.RELEASE;
        try {
            versionfinal = new String(version.getBytes("UTF-8"), "ISO8859-1");
        } catch (Exception e) {
            versionfinal = version;
        }
        if (versionfinal.length() > 0) {
            buffer.append(versionfinal);
        } else {
            buffer.append("1.0");
        }
        buffer.append("; ");
        String language = locale.getLanguage();
        if (language != null) {
            buffer.append(language.toLowerCase());
            String country = locale.getCountry();
            if (country != null) {
                buffer.append("-");
                buffer.append(country.toLowerCase());
            }
        } else {
            buffer.append("en");
        }
        if ("REL".equals(Build.VERSION.CODENAME)) {
            String model = Build.MODEL;
            try {
                modelTmp = new String(model.getBytes("UTF-8"), "ISO8859-1");
            } catch (Exception e2) {
                modelTmp = model;
            }
            if (modelTmp.length() > 0) {
                buffer.append("; ");
                buffer.append(modelTmp);
            }
        }
        String id = Build.ID.replaceAll("[一-龥]", Constants.STR_EMPTY);
        if (id.length() > 0) {
            buffer.append(" Build/");
            buffer.append(id);
        }
        String format = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/533.1 (KHTML, like Gecko)Version/4.0 Mobile Safari/533.1", buffer);
        sDefalutUserAgent = format;
        return format;
    }

    private static String notNullString(String src) {
        return src == null ? Constants.STR_EMPTY : src;
    }
}
