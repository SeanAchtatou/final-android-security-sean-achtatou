package com.tencent.smtt.sdk;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Build;
import android.os.Message;
import android.view.KeyEvent;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class SystemWebViewClient extends WebViewClient {
    private static final String LOGTAG = "SystemWebViewClient";
    private WebViewClient mClient;
    private WebView mWebView;

    SystemWebViewClient(WebView webView, WebViewClient client) {
        this.mWebView = webView;
        this.mClient = client;
    }

    public void onLoadResource(WebView view, String url) {
        this.mWebView.setSysWebView(view);
        this.mClient.onLoadResource(this.mWebView, url);
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        this.mWebView.setSysWebView(view);
        return this.mClient.shouldOverrideUrlLoading(this.mWebView, url);
    }

    @TargetApi(11)
    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        if (Build.VERSION.SDK_INT < 11) {
            QbSdkLog.w(LOGTAG, "[shouldInterceptRequest] -- Your sdk_version is:" + Build.VERSION.SDK_INT);
            QbSdkLog.e(LOGTAG, "Sorry, your sdk_version is too low to call this method!");
            return null;
        }
        com.tencent.smtt.export.external.interfaces.WebResourceResponse res = this.mClient.shouldInterceptRequest(this.mWebView, url);
        if (res != null) {
            return new WebResourceResponse(res.getMimeType(), res.getEncoding(), res.getData());
        }
        return null;
    }

    public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {
        this.mWebView.setSysWebView(view);
        return this.mClient.shouldOverrideKeyEvent(this.mWebView, event);
    }

    public void onFormResubmission(WebView view, Message dontResend, Message resend) {
        this.mWebView.setSysWebView(view);
        this.mClient.onFormResubmission(this.mWebView, dontResend, resend);
    }

    public void onPageFinished(WebView view, String url) {
        this.mWebView.setSysWebView(view);
        this.mWebView.mPv++;
        this.mWebView.compareQBTimestampForReboot();
        this.mClient.onPageFinished(this.mWebView, url);
    }

    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        this.mWebView.setSysWebView(view);
        this.mClient.onPageStarted(this.mWebView, url, favicon);
    }

    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        this.mWebView.setSysWebView(view);
        this.mClient.onReceivedError(this.mWebView, errorCode, description, failingUrl);
    }

    public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
        this.mWebView.setSysWebView(view);
        this.mClient.onReceivedHttpAuthRequest(this.mWebView, new HttpAuthHandlerImpl(handler), host, realm);
    }

    public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
        this.mWebView.setSysWebView(view);
        this.mClient.doUpdateVisitedHistory(this.mWebView, url, isReload);
    }

    @TargetApi(12)
    public void onReceivedLoginRequest(WebView view, String realm, String account, String args) {
        if (Build.VERSION.SDK_INT >= 12) {
            this.mWebView.setSysWebView(view);
            this.mClient.onReceivedLoginRequest(this.mWebView, realm, account, args);
        }
    }

    @TargetApi(8)
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        if (Build.VERSION.SDK_INT >= 8) {
            this.mWebView.setSysWebView(view);
            this.mClient.onReceivedSslError(this.mWebView, new SslErrorHandlerImpl(handler), new SslErrorImpl(error));
        }
    }

    public void onScaleChanged(WebView view, float oldScale, float newScale) {
        this.mWebView.setSysWebView(view);
        this.mClient.onScaleChanged(this.mWebView, oldScale, newScale);
    }

    public void onTooManyRedirects(WebView view, Message cancelMsg, Message continueMsg) {
        this.mWebView.setSysWebView(view);
        this.mClient.onTooManyRedirects(this.mWebView, cancelMsg, continueMsg);
    }

    public void onUnhandledKeyEvent(WebView view, KeyEvent event) {
        this.mWebView.setSysWebView(view);
        this.mClient.onUnhandledKeyEvent(this.mWebView, event);
    }

    private static class HttpAuthHandlerImpl implements com.tencent.smtt.export.external.interfaces.HttpAuthHandler {
        private HttpAuthHandler mHandler;

        HttpAuthHandlerImpl(HttpAuthHandler handler) {
            this.mHandler = handler;
        }

        public void proceed(String username, String password) {
            this.mHandler.proceed(username, password);
        }

        public void cancel() {
            this.mHandler.cancel();
        }

        public boolean useHttpAuthUsernamePassword() {
            return this.mHandler.useHttpAuthUsernamePassword();
        }
    }

    private static class SslErrorHandlerImpl implements com.tencent.smtt.export.external.interfaces.SslErrorHandler {
        SslErrorHandler mSslErrorHandler;

        SslErrorHandlerImpl(SslErrorHandler sslErrorHandler) {
            this.mSslErrorHandler = sslErrorHandler;
        }

        public void proceed() {
            this.mSslErrorHandler.proceed();
        }

        public void cancel() {
            this.mSslErrorHandler.cancel();
        }
    }

    private static class SslErrorImpl implements com.tencent.smtt.export.external.interfaces.SslError {
        SslError mSslError;

        SslErrorImpl(SslError sslError) {
            this.mSslError = sslError;
        }

        public SslCertificate getCertificate() {
            return this.mSslError.getCertificate();
        }

        public boolean addError(int error) {
            return this.mSslError.addError(error);
        }

        public boolean hasError(int error) {
            return this.mSslError.hasError(error);
        }

        public int getPrimaryError() {
            return this.mSslError.getPrimaryError();
        }
    }
}
