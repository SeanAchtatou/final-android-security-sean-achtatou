package com.tencent.smtt.secure;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class UrlDownload {
    private String url;

    UrlDownload(String url2) {
        this.url = url2;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0053 A[SYNTHETIC, Splitter:B:25:0x0053] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] getBinData() {
        /*
            r9 = this;
            r2 = 0
            r3 = 0
            java.net.URL r7 = new java.net.URL     // Catch:{ Exception -> 0x005e, all -> 0x0050 }
            java.lang.String r8 = r9.url     // Catch:{ Exception -> 0x005e, all -> 0x0050 }
            r7.<init>(r8)     // Catch:{ Exception -> 0x005e, all -> 0x0050 }
            java.net.URLConnection r6 = r7.openConnection()     // Catch:{ Exception -> 0x005e, all -> 0x0050 }
            java.net.HttpURLConnection r6 = (java.net.HttpURLConnection) r6     // Catch:{ Exception -> 0x005e, all -> 0x0050 }
            r7 = 300(0x12c, float:4.2E-43)
            r6.setConnectTimeout(r7)     // Catch:{ Exception -> 0x005e, all -> 0x0050 }
            r7 = 300(0x12c, float:4.2E-43)
            r6.setReadTimeout(r7)     // Catch:{ Exception -> 0x005e, all -> 0x0050 }
            java.io.BufferedInputStream r4 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x005e, all -> 0x0050 }
            java.io.InputStream r7 = r6.getInputStream()     // Catch:{ Exception -> 0x005e, all -> 0x0050 }
            r4.<init>(r7)     // Catch:{ Exception -> 0x005e, all -> 0x0050 }
            r7 = 4096(0x1000, float:5.74E-42)
            byte[] r1 = new byte[r7]     // Catch:{ Exception -> 0x0037, all -> 0x005b }
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0037, all -> 0x005b }
            r0.<init>()     // Catch:{ Exception -> 0x0037, all -> 0x005b }
        L_0x002b:
            int r5 = r4.read(r1)     // Catch:{ Exception -> 0x0037, all -> 0x005b }
            r7 = -1
            if (r5 == r7) goto L_0x003f
            r7 = 0
            r0.write(r1, r7, r5)     // Catch:{ Exception -> 0x0037, all -> 0x005b }
            goto L_0x002b
        L_0x0037:
            r7 = move-exception
            r3 = r4
        L_0x0039:
            if (r3 == 0) goto L_0x003e
            r3.close()     // Catch:{ Exception -> 0x0057 }
        L_0x003e:
            return r2
        L_0x003f:
            r6.disconnect()     // Catch:{ Exception -> 0x0037, all -> 0x005b }
            byte[] r2 = r0.toByteArray()     // Catch:{ Exception -> 0x0037, all -> 0x005b }
            if (r4 == 0) goto L_0x004b
            r4.close()     // Catch:{ Exception -> 0x004d }
        L_0x004b:
            r3 = r4
            goto L_0x003e
        L_0x004d:
            r7 = move-exception
            r3 = r4
            goto L_0x003e
        L_0x0050:
            r7 = move-exception
        L_0x0051:
            if (r3 == 0) goto L_0x0056
            r3.close()     // Catch:{ Exception -> 0x0059 }
        L_0x0056:
            throw r7
        L_0x0057:
            r7 = move-exception
            goto L_0x003e
        L_0x0059:
            r8 = move-exception
            goto L_0x0056
        L_0x005b:
            r7 = move-exception
            r3 = r4
            goto L_0x0051
        L_0x005e:
            r7 = move-exception
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.smtt.secure.UrlDownload.getBinData():byte[]");
    }

    /* access modifiers changed from: package-private */
    public ArrayList<String> getListString() {
        ArrayList<String> arSites = new ArrayList<>();
        try {
            HttpURLConnection urlConnection = (HttpURLConnection) new URL(this.url).openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while (true) {
                String line = br.readLine();
                if (line != null) {
                    arSites.add(line);
                } else {
                    br.close();
                    urlConnection.disconnect();
                    return arSites;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
