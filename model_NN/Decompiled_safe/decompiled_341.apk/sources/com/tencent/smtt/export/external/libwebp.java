package com.tencent.smtt.export.external;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.tencent.connect.common.Constants;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class libwebp {
    private static final int BITMAP_ALPHA_8 = 1;
    private static final int BITMAP_ARGB_4444 = 3;
    private static final int BITMAP_ARGB_8888 = 4;
    private static final int BITMAP_RGB_565 = 2;
    private static final String LOGTAG = "[image]";
    private static boolean isMultiCore = false;
    private static libwebp mInstance = null;
    private static boolean mIsLoadLibSuccess = false;
    private int mBitmapType = 4;

    public native int[] nativeDecode(byte[] bArr, boolean z, int[] iArr, int[] iArr2);

    public native int[] nativeDecodeInto(byte[] bArr, boolean z, int[] iArr, int[] iArr2);

    public native int[] nativeDecode_16bit(byte[] bArr, boolean z, int i);

    public native int nativeGetInfo(byte[] bArr, int[] iArr, int[] iArr2);

    public native int[] nativeIDecode(byte[] bArr, boolean z, int[] iArr, int[] iArr2);

    public static libwebp getInstance(Context ctx) {
        if (mInstance == null) {
            loadWepLibraryIfNeed(ctx);
            mInstance = new libwebp();
        }
        return mInstance;
    }

    public static void loadWepLibraryIfNeed(Context context, String soPath) {
        if (!mIsLoadLibSuccess) {
            try {
                System.load(soPath + File.separator + "libwebp_base.so");
                mIsLoadLibSuccess = true;
            } catch (UnsatisfiedLinkError e) {
                Log.e(LOGTAG, "Load WebP Library Error...: libwebp.java - loadWepLibraryIfNeed()");
            }
        }
    }

    public static void loadWepLibraryIfNeed(Context ctx) {
        if (!mIsLoadLibSuccess) {
            try {
                LibraryLoader.loadLibrary(ctx, "webp_base");
                mIsLoadLibSuccess = true;
            } catch (UnsatisfiedLinkError e) {
                Log.e(LOGTAG, "Load WebP Library Error...: libwebp.java - loadWepLibraryIfNeed()");
            }
        }
    }

    private boolean isMultiCore() {
        return getCPUinfo().contains("processor");
    }

    private String getCPUinfo() {
        String result = Constants.STR_EMPTY;
        try {
            InputStream in = new ProcessBuilder("/system/bin/cat", "/proc/cpuinfo").start().getInputStream();
            byte[] re = new byte[1024];
            while (in.read(re) != -1) {
                result = result + new String(re);
            }
            in.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public int getInfo(byte[] data, int[] resultW, int[] resultH) {
        if (!mIsLoadLibSuccess) {
            return 0;
        }
        return nativeGetInfo(data, resultW, resultH);
    }

    public int[] decodeBase(byte[] data, int[] wBmp, int[] hBmp) {
        if (mIsLoadLibSuccess) {
            return nativeDecode(data, isMultiCore, wBmp, hBmp);
        }
        Log.e(LOGTAG, "Load WebP Library Error...");
        return null;
    }

    public int[] decodeBase_16bit(byte[] data, Bitmap.Config config) {
        if (!mIsLoadLibSuccess) {
            Log.e(LOGTAG, "Load WebP Library Error...");
            return null;
        }
        switch (AnonymousClass1.$SwitchMap$android$graphics$Bitmap$Config[config.ordinal()]) {
            case 1:
                this.mBitmapType = 3;
                break;
            case 2:
                this.mBitmapType = 2;
                break;
            default:
                this.mBitmapType = 2;
                break;
        }
        return nativeDecode_16bit(data, isMultiCore, this.mBitmapType);
    }

    /* renamed from: com.tencent.smtt.export.external.libwebp$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$android$graphics$Bitmap$Config = new int[Bitmap.Config.values().length];

        static {
            try {
                $SwitchMap$android$graphics$Bitmap$Config[Bitmap.Config.ARGB_4444.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$android$graphics$Bitmap$Config[Bitmap.Config.RGB_565.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    public int[] decodeInto(byte[] data, int[] wBmp, int[] hBmp) {
        if (mIsLoadLibSuccess) {
            return nativeDecodeInto(data, isMultiCore, wBmp, hBmp);
        }
        Log.e(LOGTAG, "Load WebP Library Error...");
        return null;
    }

    public int[] incDecode(byte[] data, int[] wBmp, int[] hBmp) {
        if (mIsLoadLibSuccess) {
            return nativeIDecode(data, isMultiCore, wBmp, hBmp);
        }
        Log.e(LOGTAG, "Load WebP Library Error...");
        return null;
    }
}
