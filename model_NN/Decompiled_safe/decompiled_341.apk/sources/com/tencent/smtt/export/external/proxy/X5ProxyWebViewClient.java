package com.tencent.smtt.export.external.proxy;

import com.tencent.smtt.export.external.WebViewWizardBase;
import com.tencent.smtt.export.external.interfaces.IX5WebViewClient;

public abstract class X5ProxyWebViewClient extends ProxyWebViewClient {
    public X5ProxyWebViewClient(WebViewWizardBase wizard) {
        this.mWebViewClient = (IX5WebViewClient) wizard.newInstance(wizard.isDynamicMode(), "com.tencent.smtt.webkit.WebViewClient");
    }
}
