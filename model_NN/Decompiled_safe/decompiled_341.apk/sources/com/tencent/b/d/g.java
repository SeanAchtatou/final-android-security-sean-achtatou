package com.tencent.b.d;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import java.util.Locale;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONObject;

class g {

    /* renamed from: a  reason: collision with root package name */
    String f3378a;
    String b;
    DisplayMetrics c;
    int d;
    String e;
    String f;
    String g;
    String h;
    String i;
    String j;
    String k;
    int l;
    String m;
    Context n;
    private String o;
    private String p;
    private String q;
    private String r;

    private g(Context context) {
        this.b = String.valueOf(2.11f);
        this.d = Build.VERSION.SDK_INT;
        this.e = Build.MODEL;
        this.f = Build.MANUFACTURER;
        this.g = Locale.getDefault().getLanguage();
        this.l = 0;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.r = null;
        this.n = context;
        this.c = i.a(context);
        this.f3378a = i.c(context);
        this.i = i.b(context);
        this.j = TimeZone.getDefault().getID();
        this.k = i.f(context);
        this.m = context.getPackageName();
        this.r = i.a();
    }

    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject) {
        jSONObject.put("sr", this.c.widthPixels + "*" + this.c.heightPixels);
        k.a(jSONObject, "av", this.f3378a);
        k.a(jSONObject, "ch", this.h);
        k.a(jSONObject, "mf", this.f);
        k.a(jSONObject, "sv", this.b);
        k.a(jSONObject, "ov", Integer.toString(this.d));
        jSONObject.put("os", 1);
        k.a(jSONObject, "op", this.i);
        k.a(jSONObject, "lg", this.g);
        k.a(jSONObject, "md", this.e);
        k.a(jSONObject, "tz", this.j);
        if (this.l != 0) {
            jSONObject.put("jb", this.l);
        }
        k.a(jSONObject, "sd", this.k);
        k.a(jSONObject, "apn", this.m);
        if (k.b(this.n) && k.a(this.n)) {
            JSONObject jSONObject2 = new JSONObject();
            k.a(jSONObject2, "bs", k.g(this.n));
            k.a(jSONObject2, "ss", k.h(this.n));
            if (jSONObject2.length() > 0) {
                k.a(jSONObject, "wf", jSONObject2.toString());
            }
        }
        JSONArray a2 = k.a(this.n, 10);
        if (a2 != null && a2.length() > 0) {
            k.a(jSONObject, "wflist", a2.toString());
        }
        k.a(jSONObject, "sen", this.o);
        k.a(jSONObject, "cpu", this.p);
        k.a(jSONObject, "ram", this.q);
        k.a(jSONObject, "rom", this.r);
    }
}
