package com.tencent.b.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.tencent.b.d.k;

public class d extends f {
    public d(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void a(a aVar) {
        synchronized (this) {
            k.a("write CheckEntity to sharedPreferences:" + aVar.toString());
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.f3373a).edit();
            edit.putString(f(), aVar.toString());
            edit.commit();
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        synchronized (this) {
            k.a("write mid to sharedPreferences");
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.f3373a).edit();
            edit.putString(i(), str);
            edit.commit();
        }
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        return true;
    }

    /* access modifiers changed from: protected */
    public String b() {
        String string;
        synchronized (this) {
            k.a("read mid from sharedPreferences");
            string = PreferenceManager.getDefaultSharedPreferences(this.f3373a).getString(i(), null);
        }
        return string;
    }

    /* access modifiers changed from: protected */
    public a c() {
        a aVar;
        synchronized (this) {
            aVar = new a(PreferenceManager.getDefaultSharedPreferences(this.f3373a).getString(f(), null));
            k.a("read CheckEntity from sharedPreferences:" + aVar.toString());
        }
        return aVar;
    }
}
