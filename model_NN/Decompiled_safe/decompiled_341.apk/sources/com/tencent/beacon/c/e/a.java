package com.tencent.beacon.c.e;

import com.tencent.beacon.e.c;
import com.tencent.beacon.e.d;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public final class a extends c {
    private static ArrayList<d> e;
    private static Map<String, String> f;

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<d> f3418a = null;
    public int b = 0;
    public String c = Constants.STR_EMPTY;
    public Map<String, String> d = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.d.a(java.util.Collection, int):void
     arg types: [java.util.ArrayList<com.tencent.beacon.c.e.d>, int]
     candidates:
      com.tencent.beacon.e.d.a(byte, int):void
      com.tencent.beacon.e.d.a(int, int):void
      com.tencent.beacon.e.d.a(long, int):void
      com.tencent.beacon.e.d.a(com.tencent.beacon.e.c, int):void
      com.tencent.beacon.e.d.a(java.lang.Object, int):void
      com.tencent.beacon.e.d.a(java.lang.String, int):void
      com.tencent.beacon.e.d.a(java.util.Map, int):void
      com.tencent.beacon.e.d.a(short, int):void
      com.tencent.beacon.e.d.a(boolean, int):void
      com.tencent.beacon.e.d.a(byte[], int):void
      com.tencent.beacon.e.d.a(java.util.Collection, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.d.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.beacon.e.d.a(byte, int):void
      com.tencent.beacon.e.d.a(int, int):void
      com.tencent.beacon.e.d.a(long, int):void
      com.tencent.beacon.e.d.a(com.tencent.beacon.e.c, int):void
      com.tencent.beacon.e.d.a(java.lang.Object, int):void
      com.tencent.beacon.e.d.a(java.lang.String, int):void
      com.tencent.beacon.e.d.a(java.util.Collection, int):void
      com.tencent.beacon.e.d.a(short, int):void
      com.tencent.beacon.e.d.a(boolean, int):void
      com.tencent.beacon.e.d.a(byte[], int):void
      com.tencent.beacon.e.d.a(java.util.Map, int):void */
    public final void a(d dVar) {
        dVar.a((Collection) this.f3418a, 0);
        dVar.a(this.b, 1);
        dVar.a(this.c, 2);
        if (this.d != null) {
            dVar.a((Map) this.d, 3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList<com.tencent.beacon.c.e.d>, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object */
    public final void a(com.tencent.beacon.e.a aVar) {
        if (e == null) {
            e = new ArrayList<>();
            e.add(new d());
        }
        this.f3418a = (ArrayList) aVar.a((Object) e, 0, true);
        this.b = aVar.a(this.b, 1, true);
        this.c = aVar.b(2, true);
        if (f == null) {
            f = new HashMap();
            f.put(Constants.STR_EMPTY, Constants.STR_EMPTY);
        }
        this.d = (Map) aVar.a((Object) f, 3, false);
    }
}
