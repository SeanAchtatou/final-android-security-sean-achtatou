package com.tencent.d.a;

import android.os.Process;
import android.util.Log;
import java.util.concurrent.LinkedBlockingQueue;

/* compiled from: ProGuard */
class i extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f3576a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    i(h hVar, String str) {
        super(str);
        this.f3576a = hVar;
    }

    public void run() {
        int i = 0;
        if (f.g() && f.t.compareAndSet(false, true)) {
            try {
                f.o();
                try {
                    f.o = f.h.getPackageName();
                } catch (Exception e) {
                    f.o = "unknow";
                }
                f.j = new LinkedBlockingQueue<>(15000);
                f.n = Process.myPid();
                Log.d("TMLog", "TMLog init start ");
                f.m();
                f.y.setName("logWriteThread");
                f.y.start();
                f.w.removeCallbacks(f.x);
            } catch (Exception e2) {
                f.t.set(false);
                e2.printStackTrace();
                int i2 = f.v.get();
                Log.d("TMLog", "TMLog init post retry " + i2 + " times, interval " + f.u[i2]);
                f.w.removeCallbacks(f.x);
                f.w.postDelayed(f.x, (long) (f.u[i2] * 60000));
                int i3 = i2 + 1;
                if (i3 < f.u.length) {
                    i = i3;
                }
                f.v.set(i);
            }
        }
    }
}
