package com.tencent.cloud.smartcard.a;

import android.content.Context;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.smartcard.NormalSmartcardBaseItem;
import com.tencent.assistant.component.smartcard.SmartcardListener;
import com.tencent.assistant.manager.smartcard.c;
import com.tencent.assistant.manager.smartcard.y;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.protocol.jce.SmartCardTag;
import com.tencent.cloud.smartcard.view.NormalSmartCardLabelItem;

/* compiled from: ProGuard */
public class a extends c {
    public Class<? extends JceStruct> a() {
        return SmartCardTag.class;
    }

    public com.tencent.assistant.model.a.a b() {
        return new com.tencent.cloud.smartcard.b.a();
    }

    /* access modifiers changed from: protected */
    public y c() {
        return new com.tencent.cloud.smartcard.c.a();
    }

    public NormalSmartcardBaseItem a(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        return new NormalSmartCardLabelItem(context, iVar, smartcardListener, iViewInvalidater);
    }
}
