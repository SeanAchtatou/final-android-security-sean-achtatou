package com.google.zxing.b.a.a.a;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;

final class c extends h {
    c(a aVar) {
        super(aVar);
    }

    public String a() {
        if (this.f128a.b < 48) {
            throw NotFoundException.a();
        }
        StringBuffer stringBuffer = new StringBuffer();
        b(stringBuffer, 8);
        int a2 = this.b.a(48, 2);
        stringBuffer.append("(392");
        stringBuffer.append(a2);
        stringBuffer.append(')');
        stringBuffer.append(this.b.a(50, (String) null).a());
        return stringBuffer.toString();
    }
}
