package com.google.zxing.b.a.a;

import com.google.zxing.NotFoundException;
import com.google.zxing.b.a.a;
import com.google.zxing.b.a.a.a.j;
import com.google.zxing.b.a.b;
import com.google.zxing.b.a.f;
import com.google.zxing.h;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.SwitchButton;
import com.tencent.assistant.component.TotalTabLayout;
import com.tencent.securemodule.service.Constants;
import java.util.Hashtable;
import java.util.Vector;

public final class c extends a {
    private static final int[] g = {7, 5, 4, 3, 1};
    private static final int[] h = {4, 20, 52, 104, 204};
    private static final int[] i = {0, 348, 1388, 2948, 3988};
    private static final int[][] j = {new int[]{1, 8, 4, 1}, new int[]{3, 6, 4, 1}, new int[]{3, 4, 6, 1}, new int[]{3, 2, 8, 1}, new int[]{2, 6, 5, 1}, new int[]{2, 2, 9, 1}};
    private static final int[][] k = {new int[]{1, 3, 9, 27, 81, 32, 96, 77}, new int[]{20, 60, 180, 118, 143, 7, 21, 63}, new int[]{189, 145, 13, 39, 117, 140, 209, 205}, new int[]{193, 157, 49, 147, 19, 57, 171, 91}, new int[]{62, 186, TotalTabLayout.START_ID_INDEX, 197, 169, 85, 44, 132}, new int[]{185, 133, 188, 142, 4, 12, 36, 108}, new int[]{113, 128, 173, 97, 80, 29, 87, 50}, new int[]{SwitchButton.SWITCH_ANIMATION_DURATION, 28, 84, 41, 123, 158, 52, 156}, new int[]{46, 138, 203, 187, 139, 206, 196, 166}, new int[]{76, 17, 51, 153, 37, 111, 122, 155}, new int[]{43, 129, 176, 106, 107, NormalErrorRecommendPage.ERROR_TYPE_SEARCH_RESULT_EMPTY, 119, 146}, new int[]{16, 48, 144, 10, 30, 90, 59, 177}, new int[]{109, 116, 137, 200, 178, 112, 125, 164}, new int[]{70, 210, 208, Constants.PLATFROM_ANDROID_PAD, 184, 130, 179, 115}, new int[]{134, 191, 151, 31, 93, 68, 204, 190}, new int[]{148, 22, 66, 198, 172, 94, 71, 2}, new int[]{6, 18, 54, 162, 64, 192, 154, 40}, new int[]{120, 149, 25, 75, 14, 42, 126, 167}, new int[]{79, 26, 78, 23, 69, 207, 199, 175}, new int[]{103, 98, 83, 38, 114, 131, 182, 124}, new int[]{161, 61, 183, 127, 170, 88, 53, 159}, new int[]{55, 165, 73, 8, 24, 72, 5, 15}, new int[]{45, 135, 194, 160, 58, 174, 100, 89}};
    private static final int[][] l = {new int[]{0, 0}, new int[]{0, 1, 1}, new int[]{0, 2, 1, 3}, new int[]{0, 4, 1, 3, 2}, new int[]{0, 4, 1, 3, 3, 5}, new int[]{0, 4, 1, 3, 4, 5, 5}, new int[]{0, 0, 1, 1, 2, 2, 3, 3}, new int[]{0, 0, 1, 1, 2, 2, 3, 4, 4}, new int[]{0, 0, 1, 1, 2, 2, 3, 4, 5, 5}, new int[]{0, 0, 1, 1, 2, 3, 3, 4, 4, 5, 5}};
    private static final int m = l[l.length - 1].length;
    private final Vector n = new Vector(11);
    private final int[] o = new int[2];
    private final int[] p = new int[m];

    private static int a(com.google.zxing.common.a aVar, int i2) {
        boolean a2 = aVar.a(i2);
        while (i2 < aVar.b && aVar.a(i2) == a2) {
            i2++;
        }
        boolean z = !a2;
        while (i2 < aVar.b && aVar.a(i2) == z) {
            i2++;
        }
        return i2;
    }

    private com.google.zxing.b.a.c a(com.google.zxing.common.a aVar, int i2, boolean z) {
        int i3;
        int i4;
        int i5;
        if (z) {
            int i6 = this.o[0] - 1;
            while (i6 >= 0 && !aVar.a(i6)) {
                i6--;
            }
            i3 = i6 + 1;
            i5 = this.o[0] - i3;
            i4 = this.o[1];
        } else {
            i3 = this.o[0];
            int i7 = this.o[1];
            while (true) {
                i7++;
                if (!aVar.a(i7) || i7 >= aVar.b) {
                    i4 = i7;
                    i5 = i7 - this.o[1];
                }
            }
            i4 = i7;
            i5 = i7 - this.o[1];
        }
        int[] iArr = this.f127a;
        for (int length = iArr.length - 1; length > 0; length--) {
            iArr[length] = iArr[length - 1];
        }
        iArr[0] = i5;
        try {
            return new com.google.zxing.b.a.c(a(iArr, j), new int[]{i3, i4}, i3, i4, i2);
        } catch (NotFoundException e) {
            return null;
        }
    }

    private static h a(Vector vector) {
        String a2 = j.a(a.a(vector)).a();
        com.google.zxing.j[] c = ((b) vector.elementAt(0)).d().c();
        com.google.zxing.j[] c2 = ((b) vector.lastElement()).d().c();
        return new h(a2, null, new com.google.zxing.j[]{c[0], c[1], c2[0], c2[1]}, com.google.zxing.a.o);
    }

    private void a(int i2) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6 = false;
        boolean z7 = true;
        int a2 = a(this.e);
        int a3 = a(this.f);
        int i3 = (a2 + a3) - i2;
        boolean z8 = (a2 & 1) == 1;
        boolean z9 = (a3 & 1) == 0;
        if (a2 > 13) {
            z = true;
            z2 = false;
        } else if (a2 < 4) {
            z = false;
            z2 = true;
        } else {
            z = false;
            z2 = false;
        }
        if (a3 > 13) {
            z3 = false;
            z6 = true;
        } else {
            z3 = a3 < 4;
        }
        if (i3 == 1) {
            if (z8) {
                if (z9) {
                    throw NotFoundException.a();
                }
                z5 = z2;
                z7 = z3;
                z4 = true;
            } else if (!z9) {
                throw NotFoundException.a();
            } else {
                z6 = true;
                z7 = z3;
                z4 = z;
                z5 = z2;
            }
        } else if (i3 == -1) {
            if (z8) {
                if (z9) {
                    throw NotFoundException.a();
                }
                boolean z10 = z3;
                z4 = z;
                z5 = true;
                z7 = z10;
            } else if (!z9) {
                throw NotFoundException.a();
            } else {
                z4 = z;
                z5 = z2;
            }
        } else if (i3 != 0) {
            throw NotFoundException.a();
        } else if (z8) {
            if (!z9) {
                throw NotFoundException.a();
            } else if (a2 < a3) {
                z6 = true;
                boolean z11 = z3;
                z4 = z;
                z5 = true;
                z7 = z11;
            } else {
                z4 = true;
                z5 = z2;
            }
        } else if (z9) {
            throw NotFoundException.a();
        } else {
            z7 = z3;
            z4 = z;
            z5 = z2;
        }
        if (z5) {
            if (z4) {
                throw NotFoundException.a();
            }
            a(this.e, this.c);
        }
        if (z4) {
            b(this.e, this.c);
        }
        if (z7) {
            if (z6) {
                throw NotFoundException.a();
            }
            a(this.f, this.c);
        }
        if (z6) {
            b(this.f, this.d);
        }
    }

    private static boolean a(com.google.zxing.b.a.c cVar, boolean z, boolean z2) {
        return cVar.a() != 0 || !z || !z2;
    }

    private boolean a(Vector vector, com.google.zxing.b.a.c cVar) {
        boolean z;
        int size = vector.size() + 1;
        if (size > this.p.length) {
            throw NotFoundException.a();
        }
        for (int i2 = 0; i2 < vector.size(); i2++) {
            this.p[i2] = ((b) vector.elementAt(i2)).d().a();
        }
        this.p[size - 1] = cVar.a();
        for (int[] iArr : l) {
            if (iArr.length >= size) {
                int i3 = 0;
                while (true) {
                    if (i3 >= size) {
                        z = true;
                        break;
                    } else if (this.p[i3] != iArr[i3]) {
                        z = false;
                        break;
                    } else {
                        i3++;
                    }
                }
                if (z) {
                    return size == iArr.length;
                }
            }
        }
        throw NotFoundException.a();
    }

    private void b(com.google.zxing.common.a aVar, Vector vector, int i2) {
        int[] iArr = this.f127a;
        iArr[0] = 0;
        iArr[1] = 0;
        iArr[2] = 0;
        iArr[3] = 0;
        int a2 = aVar.a();
        if (i2 < 0) {
            i2 = vector.isEmpty() ? 0 : ((b) vector.lastElement()).d().b()[1];
        }
        boolean z = vector.size() % 2 != 0;
        boolean z2 = false;
        int i3 = i2;
        while (i3 < a2) {
            z2 = !aVar.a(i3);
            if (!z2) {
                break;
            }
            i3++;
        }
        int i4 = i3;
        int i5 = 0;
        boolean z3 = z2;
        int i6 = i4;
        for (int i7 = i3; i7 < a2; i7++) {
            if (aVar.a(i7) ^ z3) {
                iArr[i5] = iArr[i5] + 1;
            } else {
                if (i5 == 3) {
                    if (z) {
                        c(iArr);
                    }
                    if (b(iArr)) {
                        this.o[0] = i6;
                        this.o[1] = i7;
                        return;
                    }
                    if (z) {
                        c(iArr);
                    }
                    i6 += iArr[0] + iArr[1];
                    iArr[0] = iArr[2];
                    iArr[1] = iArr[3];
                    iArr[2] = 0;
                    iArr[3] = 0;
                    i5--;
                } else {
                    i5++;
                }
                iArr[i5] = 1;
                z3 = !z3;
            }
        }
        throw NotFoundException.a();
    }

    private boolean b() {
        b bVar = (b) this.n.elementAt(0);
        b b = bVar.b();
        int i2 = 2;
        int b2 = bVar.c().b();
        for (int i3 = 1; i3 < this.n.size(); i3++) {
            b bVar2 = (b) this.n.elementAt(i3);
            b2 += bVar2.b().b();
            i2++;
            if (bVar2.c() != null) {
                b2 += bVar2.c().b();
                i2++;
            }
        }
        return (b2 % 211) + ((i2 + -4) * 211) == b.a();
    }

    private static void c(int[] iArr) {
        int length = iArr.length;
        for (int i2 = 0; i2 < length / 2; i2++) {
            int i3 = iArr[i2];
            iArr[i2] = iArr[(length - i2) - 1];
            iArr[(length - i2) - 1] = i3;
        }
    }

    /* access modifiers changed from: package-private */
    public b a(com.google.zxing.common.a aVar, Vector vector, int i2) {
        com.google.zxing.b.a.c a2;
        b bVar;
        boolean z = vector.size() % 2 == 0;
        int i3 = -1;
        boolean z2 = true;
        do {
            b(aVar, vector, i3);
            a2 = a(aVar, i2, z);
            if (a2 == null) {
                i3 = a(aVar, this.o[0]);
                continue;
            } else {
                z2 = false;
                continue;
            }
        } while (z2);
        boolean a3 = a(vector, a2);
        b a4 = a(aVar, a2, z, true);
        try {
            bVar = a(aVar, a2, z, false);
        } catch (NotFoundException e) {
            if (a3) {
                bVar = null;
            } else {
                throw e;
            }
        }
        return new b(a4, bVar, a2, a3);
    }

    /* access modifiers changed from: package-private */
    public b a(com.google.zxing.common.a aVar, com.google.zxing.b.a.c cVar, boolean z, boolean z2) {
        int[] iArr = this.b;
        iArr[0] = 0;
        iArr[1] = 0;
        iArr[2] = 0;
        iArr[3] = 0;
        iArr[4] = 0;
        iArr[5] = 0;
        iArr[6] = 0;
        iArr[7] = 0;
        if (z2) {
            b(aVar, cVar.b()[0], iArr);
        } else {
            a(aVar, cVar.b()[1] + 1, iArr);
            int i2 = 0;
            for (int length = iArr.length - 1; i2 < length; length--) {
                int i3 = iArr[i2];
                iArr[i2] = iArr[length];
                iArr[length] = i3;
                i2++;
            }
        }
        float a2 = ((float) a(iArr)) / ((float) 17);
        int[] iArr2 = this.e;
        int[] iArr3 = this.f;
        float[] fArr = this.c;
        float[] fArr2 = this.d;
        for (int i4 = 0; i4 < iArr.length; i4++) {
            float f = (1.0f * ((float) iArr[i4])) / a2;
            int i5 = (int) (0.5f + f);
            if (i5 < 1) {
                i5 = 1;
            } else if (i5 > 8) {
                i5 = 8;
            }
            int i6 = i4 >> 1;
            if ((i4 & 1) == 0) {
                iArr2[i6] = i5;
                fArr[i6] = f - ((float) i5);
            } else {
                iArr3[i6] = i5;
                fArr2[i6] = f - ((float) i5);
            }
        }
        a(17);
        int a3 = ((z2 ? 0 : 1) + ((cVar.a() * 4) + (z ? 0 : 2))) - 1;
        int i7 = 0;
        int length2 = iArr2.length - 1;
        int i8 = 0;
        while (length2 >= 0) {
            if (a(cVar, z, z2)) {
                i8 += k[a3][length2 * 2] * iArr2[length2];
            }
            length2--;
            i7 = iArr2[length2] + i7;
        }
        int i9 = 0;
        int i10 = 0;
        for (int length3 = iArr3.length - 1; length3 >= 0; length3--) {
            if (a(cVar, z, z2)) {
                i9 += k[a3][(length3 * 2) + 1] * iArr3[length3];
            }
            i10 += iArr3[length3];
        }
        int i11 = i8 + i9;
        if ((i7 & 1) != 0 || i7 > 13 || i7 < 4) {
            throw NotFoundException.a();
        }
        int i12 = (13 - i7) / 2;
        int i13 = g[i12];
        return new b(i[i12] + (f.a(iArr2, i13, true) * h[i12]) + f.a(iArr3, 9 - i13, false), i11);
    }

    public h a(int i2, com.google.zxing.common.a aVar, Hashtable hashtable) {
        a();
        a(i2, aVar);
        return a(this.n);
    }

    /* access modifiers changed from: package-private */
    public Vector a(int i2, com.google.zxing.common.a aVar) {
        while (true) {
            b a2 = a(aVar, this.n, i2);
            this.n.addElement(a2);
            if (a2.a()) {
                if (b()) {
                    return this.n;
                }
                if (a2.e()) {
                    throw NotFoundException.a();
                }
            }
        }
    }

    public void a() {
        this.n.setSize(0);
    }
}
