package com.google.zxing.b.a.a.a;

final class l {

    /* renamed from: a  reason: collision with root package name */
    private final o f129a;
    private final boolean b;

    l() {
        this.b = true;
        this.f129a = null;
    }

    l(o oVar, boolean z) {
        this.b = z;
        this.f129a = oVar;
    }

    l(boolean z) {
        this.b = z;
        this.f129a = null;
    }

    /* access modifiers changed from: package-private */
    public o a() {
        return this.f129a;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.b;
    }
}
