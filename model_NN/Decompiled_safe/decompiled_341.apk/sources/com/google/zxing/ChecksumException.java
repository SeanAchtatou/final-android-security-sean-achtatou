package com.google.zxing;

public final class ChecksumException extends ReaderException {

    /* renamed from: a  reason: collision with root package name */
    private static final ChecksumException f111a = new ChecksumException();

    private ChecksumException() {
    }

    public static ChecksumException a() {
        return f111a;
    }
}
