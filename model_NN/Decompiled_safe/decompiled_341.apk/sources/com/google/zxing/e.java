package com.google.zxing;

public abstract class e {

    /* renamed from: a  reason: collision with root package name */
    private final int f193a;
    private final int b;

    protected e(int i, int i2) {
        this.f193a = i;
        this.b = i2;
    }

    public abstract byte[] a();

    public abstract byte[] a(int i, byte[] bArr);

    public final int b() {
        return this.f193a;
    }

    public final int c() {
        return this.b;
    }

    public boolean d() {
        return false;
    }

    public e e() {
        throw new RuntimeException("This luminance source does not support rotation.");
    }
}
