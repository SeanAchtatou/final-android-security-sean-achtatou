package com.google.zxing.d.b;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private final d f192a;
    private final d b;
    private final d c;

    public h(d[] dVarArr) {
        this.f192a = dVarArr[0];
        this.b = dVarArr[1];
        this.c = dVarArr[2];
    }

    public d a() {
        return this.f192a;
    }

    public d b() {
        return this.b;
    }

    public d c() {
        return this.c;
    }
}
