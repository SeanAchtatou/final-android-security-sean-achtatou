package com.google.zxing.d.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.k;
import java.util.Vector;

final class b {

    /* renamed from: a  reason: collision with root package name */
    private final com.google.zxing.common.b f186a;
    private final Vector b = new Vector(5);
    private final int c;
    private final int d;
    private final int e;
    private final int f;
    private final float g;
    private final int[] h;
    private final k i;

    b(com.google.zxing.common.b bVar, int i2, int i3, int i4, int i5, float f2, k kVar) {
        this.f186a = bVar;
        this.c = i2;
        this.d = i3;
        this.e = i4;
        this.f = i5;
        this.g = f2;
        this.h = new int[3];
        this.i = kVar;
    }

    private float a(int i2, int i3, int i4, int i5) {
        com.google.zxing.common.b bVar = this.f186a;
        int c2 = bVar.c();
        int[] iArr = this.h;
        iArr[0] = 0;
        iArr[1] = 0;
        iArr[2] = 0;
        int i6 = i2;
        while (i6 >= 0 && bVar.a(i3, i6) && iArr[1] <= i4) {
            iArr[1] = iArr[1] + 1;
            i6--;
        }
        if (i6 < 0 || iArr[1] > i4) {
            return Float.NaN;
        }
        while (i6 >= 0 && !bVar.a(i3, i6) && iArr[0] <= i4) {
            iArr[0] = iArr[0] + 1;
            i6--;
        }
        if (iArr[0] > i4) {
            return Float.NaN;
        }
        int i7 = i2 + 1;
        while (i7 < c2 && bVar.a(i3, i7) && iArr[1] <= i4) {
            iArr[1] = iArr[1] + 1;
            i7++;
        }
        if (i7 == c2 || iArr[1] > i4) {
            return Float.NaN;
        }
        while (i7 < c2 && !bVar.a(i3, i7) && iArr[2] <= i4) {
            iArr[2] = iArr[2] + 1;
            i7++;
        }
        if (iArr[2] > i4 || Math.abs(((iArr[0] + iArr[1]) + iArr[2]) - i5) * 5 >= i5 * 2 || !a(iArr)) {
            return Float.NaN;
        }
        return a(iArr, i7);
    }

    private static float a(int[] iArr, int i2) {
        return ((float) (i2 - iArr[2])) - (((float) iArr[1]) / 2.0f);
    }

    private a a(int[] iArr, int i2, int i3) {
        int i4 = iArr[0] + iArr[1] + iArr[2];
        float a2 = a(iArr, i3);
        float a3 = a(i2, (int) a2, iArr[1] * 2, i4);
        if (!Float.isNaN(a3)) {
            float f2 = ((float) ((iArr[0] + iArr[1]) + iArr[2])) / 3.0f;
            int size = this.b.size();
            for (int i5 = 0; i5 < size; i5++) {
                if (((a) this.b.elementAt(i5)).a(f2, a3, a2)) {
                    return new a(a2, a3, f2);
                }
            }
            a aVar = new a(a2, a3, f2);
            this.b.addElement(aVar);
            if (this.i != null) {
                this.i.a(aVar);
            }
        }
        return null;
    }

    private boolean a(int[] iArr) {
        float f2 = this.g;
        float f3 = f2 / 2.0f;
        for (int i2 = 0; i2 < 3; i2++) {
            if (Math.abs(f2 - ((float) iArr[i2])) >= f3) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public a a() {
        a a2;
        a a3;
        int i2 = this.c;
        int i3 = this.f;
        int i4 = i2 + this.e;
        int i5 = this.d + (i3 >> 1);
        int[] iArr = new int[3];
        for (int i6 = 0; i6 < i3; i6++) {
            int i7 = i5 + ((i6 & 1) == 0 ? (i6 + 1) >> 1 : -((i6 + 1) >> 1));
            iArr[0] = 0;
            iArr[1] = 0;
            iArr[2] = 0;
            int i8 = i2;
            while (i8 < i4 && !this.f186a.a(i8, i7)) {
                i8++;
            }
            int i9 = 0;
            for (int i10 = i8; i10 < i4; i10++) {
                if (!this.f186a.a(i10, i7)) {
                    if (i9 == 1) {
                        i9++;
                    }
                    iArr[i9] = iArr[i9] + 1;
                } else if (i9 == 1) {
                    iArr[i9] = iArr[i9] + 1;
                } else if (i9 != 2) {
                    i9++;
                    iArr[i9] = iArr[i9] + 1;
                } else if (a(iArr) && (a3 = a(iArr, i7, i10)) != null) {
                    return a3;
                } else {
                    iArr[0] = iArr[2];
                    iArr[1] = 1;
                    iArr[2] = 0;
                    i9 = 1;
                }
            }
            if (a(iArr) && (a2 = a(iArr, i7, i4)) != null) {
                return a2;
            }
        }
        if (!this.b.isEmpty()) {
            return (a) this.b.elementAt(0);
        }
        throw NotFoundException.a();
    }
}
