package com.google.zxing.a.a;

import com.google.zxing.FormatException;
import com.tencent.assistant.component.TotalTabLayout;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private static final e[] f120a = h();
    private final int b;
    private final int c;
    private final int d;
    private final int e;
    private final int f;
    private final h g;
    private final int h;

    private e(int i, int i2, int i3, int i4, int i5, h hVar) {
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = i4;
        this.f = i5;
        this.g = hVar;
        int a2 = hVar.a();
        g[] b2 = hVar.b();
        int i6 = 0;
        for (g gVar : b2) {
            i6 += (gVar.b() + a2) * gVar.a();
        }
        this.h = i6;
    }

    public static e a(int i, int i2) {
        if ((i & 1) == 0 && (i2 & 1) == 0) {
            for (e eVar : f120a) {
                if (eVar.c == i && eVar.d == i2) {
                    return eVar;
                }
            }
            throw FormatException.a();
        }
        throw FormatException.a();
    }

    private static e[] h() {
        return new e[]{new e(1, 10, 10, 8, 8, new h(5, new g(1, 3, null), (f) null)), new e(2, 12, 12, 10, 10, new h(7, new g(1, 5, null), (f) null)), new e(3, 14, 14, 12, 12, new h(10, new g(1, 8, null), (f) null)), new e(4, 16, 16, 14, 14, new h(12, new g(1, 12, null), (f) null)), new e(5, 18, 18, 16, 16, new h(14, new g(1, 18, null), (f) null)), new e(6, 20, 20, 18, 18, new h(18, new g(1, 22, null), (f) null)), new e(7, 22, 22, 20, 20, new h(20, new g(1, 30, null), (f) null)), new e(8, 24, 24, 22, 22, new h(24, new g(1, 36, null), (f) null)), new e(9, 26, 26, 24, 24, new h(28, new g(1, 44, null), (f) null)), new e(10, 32, 32, 14, 14, new h(36, new g(1, 62, null), (f) null)), new e(11, 36, 36, 16, 16, new h(42, new g(1, 86, null), (f) null)), new e(12, 40, 40, 18, 18, new h(48, new g(1, 114, null), (f) null)), new e(13, 44, 44, 20, 20, new h(56, new g(1, 144, null), (f) null)), new e(14, 48, 48, 22, 22, new h(68, new g(1, 174, null), (f) null)), new e(15, 52, 52, 24, 24, new h(42, new g(2, 102, null), (f) null)), new e(16, 64, 64, 14, 14, new h(56, new g(2, 140, null), (f) null)), new e(17, 72, 72, 16, 16, new h(36, new g(4, 92, null), (f) null)), new e(18, 80, 80, 18, 18, new h(48, new g(4, 114, null), (f) null)), new e(19, 88, 88, 20, 20, new h(56, new g(4, 144, null), (f) null)), new e(20, 96, 96, 22, 22, new h(68, new g(4, 174, null), (f) null)), new e(21, 104, 104, 24, 24, new h(56, new g(6, TotalTabLayout.START_ID_INDEX, null), (f) null)), new e(22, 120, 120, 18, 18, new h(68, new g(6, 175, null), (f) null)), new e(23, 132, 132, 20, 20, new h(62, new g(8, 163, null), (f) null)), new e(24, 144, 144, 22, 22, new h(62, new g(8, 156, null), new g(2, 155, null), null)), new e(25, 8, 18, 6, 16, new h(7, new g(1, 5, null), (f) null)), new e(26, 8, 32, 6, 14, new h(11, new g(1, 10, null), (f) null)), new e(27, 12, 26, 10, 24, new h(14, new g(1, 16, null), (f) null)), new e(28, 12, 36, 10, 16, new h(18, new g(1, 22, null), (f) null)), new e(29, 16, 36, 10, 16, new h(24, new g(1, 32, null), (f) null)), new e(30, 16, 48, 14, 22, new h(28, new g(1, 49, null), (f) null))};
    }

    public int a() {
        return this.b;
    }

    public int b() {
        return this.c;
    }

    public int c() {
        return this.d;
    }

    public int d() {
        return this.e;
    }

    public int e() {
        return this.f;
    }

    public int f() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public h g() {
        return this.g;
    }

    public String toString() {
        return String.valueOf(this.b);
    }
}
