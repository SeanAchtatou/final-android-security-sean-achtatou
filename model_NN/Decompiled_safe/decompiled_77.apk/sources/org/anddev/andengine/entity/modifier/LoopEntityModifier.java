package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.LoopModifier;

public class LoopEntityModifier extends LoopModifier<IEntity> implements IEntityModifier {

    public interface ILoopEntityModifierListener extends LoopModifier.ILoopModifierListener<IEntity> {
    }

    public LoopEntityModifier(IEntityModifier pEntityModifier) {
        super(pEntityModifier);
    }

    public LoopEntityModifier(IEntityModifier.IEntityModifierListener pEntityModifierListener, int pLoopCount, ILoopEntityModifierListener pLoopModifierListener, IEntityModifier pEntityModifier) {
        super(pEntityModifierListener, pLoopCount, pLoopModifierListener, pEntityModifier);
    }

    public LoopEntityModifier(IEntityModifier.IEntityModifierListener pEntityModifierListener, int pLoopCount, IEntityModifier pEntityModifier) {
        super(pEntityModifierListener, pLoopCount, pEntityModifier);
    }

    public LoopEntityModifier(int pLoopCount, IEntityModifier pEntityModifier) {
        super(pLoopCount, pEntityModifier);
    }

    protected LoopEntityModifier(LoopEntityModifier pLoopEntityModifier) {
        super((LoopModifier) pLoopEntityModifier);
    }

    public LoopEntityModifier clone() {
        return new LoopEntityModifier(this);
    }
}
