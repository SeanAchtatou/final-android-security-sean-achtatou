package org.anddev.andengine.entity.scene.background.modifier;

import org.anddev.andengine.entity.scene.background.IBackground;
import org.anddev.andengine.entity.scene.background.modifier.IBackgroundModifier;
import org.anddev.andengine.util.modifier.LoopModifier;

public class LoopBackgroundModifier extends LoopModifier<IBackground> implements IBackgroundModifier {

    public interface ILoopBackgroundModifierListener extends LoopModifier.ILoopModifierListener<IBackground> {
    }

    public LoopBackgroundModifier(IBackgroundModifier pBackgroundModifier) {
        super(pBackgroundModifier);
    }

    public LoopBackgroundModifier(IBackgroundModifier.IBackgroundModifierListener pBackgroundModifierListener, int pLoopCount, ILoopBackgroundModifierListener pLoopModifierListener, IBackgroundModifier pBackgroundModifier) {
        super(pBackgroundModifierListener, pLoopCount, pLoopModifierListener, pBackgroundModifier);
    }

    public LoopBackgroundModifier(IBackgroundModifier.IBackgroundModifierListener pBackgroundModifierListener, int pLoopCount, IBackgroundModifier pBackgroundModifier) {
        super(pBackgroundModifierListener, pLoopCount, pBackgroundModifier);
    }

    public LoopBackgroundModifier(int pLoopCount, IBackgroundModifier pBackgroundModifier) {
        super(pLoopCount, pBackgroundModifier);
    }

    protected LoopBackgroundModifier(LoopBackgroundModifier pLoopBackgroundModifier) {
        super((LoopModifier) pLoopBackgroundModifier);
    }

    public LoopBackgroundModifier clone() {
        return new LoopBackgroundModifier(this);
    }
}
