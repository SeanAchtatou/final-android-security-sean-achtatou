package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.BaseDurationModifier;

public abstract class DurationShapeModifier extends BaseDurationModifier<IEntity> implements IEntityModifier {
    public DurationShapeModifier() {
    }

    public DurationShapeModifier(float pDuration) {
        super(pDuration);
    }

    public DurationShapeModifier(float pDuration, IEntityModifier.IEntityModifierListener pEntityModifierListener) {
        super(pDuration, pEntityModifierListener);
    }

    protected DurationShapeModifier(DurationShapeModifier pDurationShapeModifier) {
        super(pDurationShapeModifier);
    }
}
