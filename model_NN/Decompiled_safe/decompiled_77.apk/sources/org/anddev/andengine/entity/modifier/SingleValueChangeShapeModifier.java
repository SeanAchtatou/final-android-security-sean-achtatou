package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.BaseSingleValueChangeModifier;

public abstract class SingleValueChangeShapeModifier extends BaseSingleValueChangeModifier<IEntity> implements IEntityModifier {
    public SingleValueChangeShapeModifier(float pDuration, float pValueChange) {
        super(pDuration, pValueChange);
    }

    public SingleValueChangeShapeModifier(float pDuration, float pValueChange, IEntityModifier.IEntityModifierListener pEntityModifierListener) {
        super(pDuration, pValueChange, pEntityModifierListener);
    }

    protected SingleValueChangeShapeModifier(SingleValueChangeShapeModifier pSingleValueChangeShapeModifier) {
        super(pSingleValueChangeShapeModifier);
    }
}
