package org.anddev.andengine.entity.scene.background;

import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureManager;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.source.ITextureSource;

public class RepeatingSpriteBackground extends SpriteBackground {
    private final float mScale;
    private Texture mTexture;

    public RepeatingSpriteBackground(float pCameraWidth, float pCameraHeight, TextureManager pTextureManager, ITextureSource pTextureSource) throws IllegalArgumentException {
        this(pCameraWidth, pCameraHeight, pTextureManager, pTextureSource, 1.0f);
    }

    public RepeatingSpriteBackground(float pCameraWidth, float pCameraHeight, TextureManager pTextureManager, ITextureSource pTextureSource, float pScale) throws IllegalArgumentException {
        super(null);
        this.mScale = pScale;
        this.mEntity = loadSprite(pCameraWidth, pCameraHeight, pTextureManager, pTextureSource);
    }

    public Texture getTexture() {
        return this.mTexture;
    }

    private Sprite loadSprite(float pCameraWidth, float pCameraHeight, TextureManager pTextureManager, ITextureSource pTextureSource) throws IllegalArgumentException {
        this.mTexture = new Texture(pTextureSource.getWidth(), pTextureSource.getHeight(), TextureOptions.REPEATING_PREMULTIPLYALPHA);
        TextureRegion textureRegion = TextureRegionFactory.createFromSource(this.mTexture, pTextureSource, 0, 0);
        int width = Math.round(pCameraWidth / this.mScale);
        int height = Math.round(pCameraHeight / this.mScale);
        textureRegion.setWidth(width);
        textureRegion.setHeight(height);
        pTextureManager.loadTexture(this.mTexture);
        Sprite sprite = new Sprite(0.0f, 0.0f, (float) width, (float) height, textureRegion);
        sprite.setScaleCenter(0.0f, 0.0f);
        sprite.setScale(this.mScale);
        return sprite;
    }
}
