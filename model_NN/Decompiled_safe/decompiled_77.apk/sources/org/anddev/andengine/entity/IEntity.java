package org.anddev.andengine.entity;

import java.util.Comparator;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.opengl.IDrawable;
import org.anddev.andengine.util.IMatcher;
import org.anddev.andengine.util.Transformation;

public interface IEntity extends IDrawable, IUpdateHandler {

    public interface IEntityMatcher extends IMatcher<IEntity> {
    }

    void attachChild(IEntity iEntity);

    void clearEntityModifiers();

    void clearUpdateHandlers();

    float[] convertLocalToSceneCoordinates(float f, float f2);

    float[] convertSceneToLocalCoordinates(float f, float f2);

    IEntity detachChild(IEntityMatcher iEntityMatcher);

    boolean detachChild(IEntity iEntity);

    void detachChildren();

    boolean detachChildren(IEntityMatcher iEntityMatcher);

    boolean detachSelf();

    IEntity findChild(IEntityMatcher iEntityMatcher);

    float getAlpha();

    float getBlue();

    IEntity getChild(int i);

    int getChildCount();

    IEntity getFirstChild();

    float getGreen();

    float getInitialX();

    float getInitialY();

    IEntity getLastChild();

    Transformation getLocalToSceneTransformation();

    IEntity getParent();

    float getRed();

    float getRotation();

    float getRotationCenterX();

    float getRotationCenterY();

    float getScaleCenterX();

    float getScaleCenterY();

    float getScaleX();

    float getScaleY();

    float[] getSceneCenterCoordinates();

    Transformation getSceneToLocalTransformation();

    Object getUserData();

    float getX();

    float getY();

    int getZIndex();

    boolean isIgnoreUpdate();

    boolean isScaled();

    boolean isVisible();

    void onAttached();

    void onDetached();

    void registerEntityModifier(IEntityModifier iEntityModifier);

    void registerUpdateHandler(IUpdateHandler iUpdateHandler);

    void setAlpha(float f);

    void setColor(float f, float f2, float f3);

    void setColor(float f, float f2, float f3, float f4);

    void setIgnoreUpdate(boolean z);

    void setInitialPosition();

    void setParent(IEntity iEntity);

    void setPosition(float f, float f2);

    void setPosition(IEntity iEntity);

    void setRotation(float f);

    void setRotationCenter(float f, float f2);

    void setRotationCenterX(float f);

    void setRotationCenterY(float f);

    void setScale(float f);

    void setScale(float f, float f2);

    void setScaleCenter(float f, float f2);

    void setScaleCenterX(float f);

    void setScaleCenterY(float f);

    void setScaleX(float f);

    void setScaleY(float f);

    void setUserData(Object obj);

    void setVisible(boolean z);

    void setZIndex(int i);

    void sortChildren();

    void sortChildren(Comparator<IEntity> comparator);

    boolean unregisterEntityModifier(IEntityModifier iEntityModifier);

    boolean unregisterEntityModifiers(IEntityModifier.IEntityModifierMatcher iEntityModifierMatcher);

    boolean unregisterUpdateHandler(IUpdateHandler iUpdateHandler);

    boolean unregisterUpdateHandlers(IUpdateHandler.IUpdateHandlerMatcher iUpdateHandlerMatcher);
}
