package org.anddev.andengine.engine.camera.hud;

import org.anddev.andengine.entity.scene.CameraScene;

public class HUD extends CameraScene {
    public HUD() {
        this(1);
        setBackgroundEnabled(false);
    }

    public HUD(int pLayerCount) {
        super(pLayerCount);
        setBackgroundEnabled(false);
    }
}
