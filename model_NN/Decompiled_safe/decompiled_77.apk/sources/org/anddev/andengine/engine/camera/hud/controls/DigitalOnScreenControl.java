package org.anddev.andengine.engine.camera.hud.controls;

import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class DigitalOnScreenControl extends BaseOnScreenControl {
    public DigitalOnScreenControl(float pX, float pY, Camera pCamera, TextureRegion pControlBaseTextureRegion, TextureRegion pControlKnobTextureRegion, float pTimeBetweenUpdates, BaseOnScreenControl.IOnScreenControlListener pOnScreenControlListener) {
        super(pX, pY, pCamera, pControlBaseTextureRegion, pControlKnobTextureRegion, pTimeBetweenUpdates, pOnScreenControlListener);
    }

    /* access modifiers changed from: protected */
    public void onUpdateControlKnob(float pRelativeX, float pRelativeY) {
        if (pRelativeX == 0.0f && pRelativeY == 0.0f) {
            super.onUpdateControlKnob(0.0f, 0.0f);
        }
        if (Math.abs(pRelativeX) > Math.abs(pRelativeY)) {
            if (pRelativeX > 0.0f) {
                super.onUpdateControlKnob(0.5f, 0.0f);
            } else if (pRelativeX < 0.0f) {
                super.onUpdateControlKnob(-0.5f, 0.0f);
            } else if (pRelativeX == 0.0f) {
                super.onUpdateControlKnob(0.0f, 0.0f);
            }
        } else if (pRelativeY > 0.0f) {
            super.onUpdateControlKnob(0.0f, 0.5f);
        } else if (pRelativeY < 0.0f) {
            super.onUpdateControlKnob(0.0f, -0.5f);
        } else if (pRelativeY == 0.0f) {
            super.onUpdateControlKnob(0.0f, 0.0f);
        }
    }
}
