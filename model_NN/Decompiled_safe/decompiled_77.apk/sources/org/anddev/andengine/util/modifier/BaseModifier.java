package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.modifier.IModifier;

public abstract class BaseModifier<T> implements IModifier<T> {
    protected boolean mFinished;
    /* access modifiers changed from: protected */
    public IModifier.IModifierListener<T> mModifierListener;
    private boolean mRemoveWhenFinished;

    public abstract IModifier<T> clone();

    public BaseModifier() {
        this((IModifier.IModifierListener) null);
    }

    public BaseModifier(IModifier.IModifierListener iModifierListener) {
        this.mRemoveWhenFinished = true;
        this.mModifierListener = iModifierListener;
    }

    protected BaseModifier(BaseModifier baseModifier) {
        this(baseModifier.mModifierListener);
    }

    public boolean isFinished() {
        return this.mFinished;
    }

    public final boolean isRemoveWhenFinished() {
        return this.mRemoveWhenFinished;
    }

    public final void setRemoveWhenFinished(boolean pRemoveWhenFinished) {
        this.mRemoveWhenFinished = pRemoveWhenFinished;
    }

    public IModifier.IModifierListener<T> getModifierListener() {
        return this.mModifierListener;
    }

    public void setModifierListener(IModifier.IModifierListener<T> pModifierListener) {
        this.mModifierListener = pModifierListener;
    }
}
