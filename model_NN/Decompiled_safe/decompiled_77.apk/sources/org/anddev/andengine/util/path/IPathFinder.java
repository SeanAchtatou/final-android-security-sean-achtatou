package org.anddev.andengine.util.path;

public interface IPathFinder<T> {
    Path findPath(T t, int i, int i2, int i3, int i4, int i5);
}
