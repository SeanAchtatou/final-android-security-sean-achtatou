package org.anddev.andengine.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.Window;
import android.view.WindowManager;
import java.util.concurrent.Callable;
import org.anddev.andengine.ui.activity.BaseActivity;
import org.anddev.andengine.util.progress.IProgressListener;
import org.anddev.andengine.util.progress.ProgressCallable;

public class ActivityUtils {
    public static void requestFullscreen(Activity pActivity) {
        Window window = pActivity.getWindow();
        window.addFlags(1024);
        window.clearFlags(2048);
        window.requestFeature(1);
    }

    public static void setScreenBrightness(Activity pActivity, float pScreenBrightness) {
        Window window = pActivity.getWindow();
        WindowManager.LayoutParams windowLayoutParams = window.getAttributes();
        windowLayoutParams.screenBrightness = pScreenBrightness;
        window.setAttributes(windowLayoutParams);
    }

    public static void keepScreenOn(Activity pActivity) {
        pActivity.getWindow().addFlags(128);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, int, int, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback<java.lang.Exception>, boolean):void
     arg types: [android.content.Context, int, int, java.util.concurrent.Callable<T>, org.anddev.andengine.util.Callback<T>, ?[OBJECT, ARRAY], int]
     candidates:
      org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback<java.lang.Exception>, boolean):void
      org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, int, int, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback<java.lang.Exception>, boolean):void */
    public static <T> void doAsync(Context pContext, int pTitleResID, int pMessageResID, Callable<T> pCallable, Callback<T> pCallback) {
        doAsync(pContext, pTitleResID, pMessageResID, (Callable) pCallable, (Callback) pCallback, (Callback<Exception>) null, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback<java.lang.Exception>, boolean):void
     arg types: [android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.util.concurrent.Callable<T>, org.anddev.andengine.util.Callback<T>, ?[OBJECT, ARRAY], int]
     candidates:
      org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, int, int, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback<java.lang.Exception>, boolean):void
      org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback<java.lang.Exception>, boolean):void */
    public static <T> void doAsync(Context pContext, CharSequence pTitle, CharSequence pMessage, Callable<T> pCallable, Callback<T> pCallback) {
        doAsync(pContext, pTitle, pMessage, (Callable) pCallable, (Callback) pCallback, (Callback<Exception>) null, false);
    }

    public static <T> void doAsync(Context pContext, int pTitleResID, int pMessageResID, Callable callable, Callback callback, boolean pCancelable) {
        doAsync(pContext, pTitleResID, pMessageResID, callable, callback, (Callback<Exception>) null, pCancelable);
    }

    public static <T> void doAsync(Context pContext, CharSequence pTitle, CharSequence pMessage, Callable callable, Callback callback, boolean pCancelable) {
        doAsync(pContext, pTitle, pMessage, callable, callback, (Callback<Exception>) null, pCancelable);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, int, int, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback<java.lang.Exception>, boolean):void
     arg types: [android.content.Context, int, int, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback<java.lang.Exception>, int]
     candidates:
      org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback<java.lang.Exception>, boolean):void
      org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, int, int, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback<java.lang.Exception>, boolean):void */
    public static <T> void doAsync(Context pContext, int pTitleResID, int pMessageResID, Callable callable, Callback callback, Callback<Exception> pExceptionCallback) {
        doAsync(pContext, pTitleResID, pMessageResID, callable, callback, pExceptionCallback, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback<java.lang.Exception>, boolean):void
     arg types: [android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback<java.lang.Exception>, int]
     candidates:
      org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, int, int, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback<java.lang.Exception>, boolean):void
      org.anddev.andengine.util.ActivityUtils.doAsync(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.util.concurrent.Callable, org.anddev.andengine.util.Callback, org.anddev.andengine.util.Callback<java.lang.Exception>, boolean):void */
    public static <T> void doAsync(Context pContext, CharSequence pTitle, CharSequence pMessage, Callable callable, Callback callback, Callback<Exception> pExceptionCallback) {
        doAsync(pContext, pTitle, pMessage, callable, callback, pExceptionCallback, false);
    }

    public static <T> void doAsync(Context pContext, int pTitleResID, int pMessageResID, Callable<T> pCallable, Callback<T> pCallback, Callback<Exception> pExceptionCallback, boolean pCancelable) {
        doAsync(pContext, pContext.getString(pTitleResID), pContext.getString(pMessageResID), pCallable, pCallback, pExceptionCallback, pCancelable);
    }

    public static <T> void doAsync(Context pContext, CharSequence pTitle, CharSequence pMessage, Callable<T> pCallable, Callback<T> pCallback, Callback<Exception> pExceptionCallback, boolean pCancelable) {
        final Context context = pContext;
        final CharSequence charSequence = pTitle;
        final CharSequence charSequence2 = pMessage;
        final boolean z = pCancelable;
        final Callable<T> callable = pCallable;
        final Callback<T> callback = pCallback;
        final Callback<Exception> callback2 = pExceptionCallback;
        new AsyncTask<Void, Void, T>() {
            private Exception mException = null;
            private ProgressDialog mPD;

            public void onPreExecute() {
                this.mPD = ProgressDialog.show(context, charSequence, charSequence2, true, z);
                if (z) {
                    ProgressDialog progressDialog = this.mPD;
                    final Callback callback = callback2;
                    progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface pDialogInterface) {
                            callback.onCallback(new BaseActivity.CancelledException());
                            pDialogInterface.dismiss();
                        }
                    });
                }
                super.onPreExecute();
            }

            public T doInBackground(Void... params) {
                try {
                    return callable.call();
                } catch (Exception e) {
                    this.mException = e;
                    return null;
                }
            }

            public void onPostExecute(T result) {
                try {
                    this.mPD.dismiss();
                } catch (Exception e) {
                    Debug.e("Error", e);
                }
                if (isCancelled()) {
                    this.mException = new BaseActivity.CancelledException();
                }
                if (this.mException == null) {
                    callback.onCallback(result);
                } else if (callback2 == null) {
                    Debug.e("Error", this.mException);
                } else {
                    callback2.onCallback(this.mException);
                }
                super.onPostExecute(result);
            }
        }.execute((Object[]) null);
    }

    public static <T> void doProgressAsync(Context pContext, int pTitleResID, ProgressCallable<T> pCallable, Callback<T> pCallback) {
        doProgressAsync(pContext, pTitleResID, pCallable, pCallback, null);
    }

    public static <T> void doProgressAsync(Context pContext, int pTitleResID, ProgressCallable<T> pCallable, Callback<T> pCallback, Callback<Exception> pExceptionCallback) {
        final Context context = pContext;
        final int i = pTitleResID;
        final ProgressCallable<T> progressCallable = pCallable;
        final Callback<T> callback = pCallback;
        final Callback<Exception> callback2 = pExceptionCallback;
        new AsyncTask<Void, Integer, T>() {
            private Exception mException = null;
            private ProgressDialog mPD;

            public void onPreExecute() {
                this.mPD = new ProgressDialog(context);
                this.mPD.setTitle(i);
                this.mPD.setIcon(17301582);
                this.mPD.setIndeterminate(false);
                this.mPD.setProgressStyle(1);
                this.mPD.show();
                super.onPreExecute();
            }

            public T doInBackground(Void... params) {
                try {
                    return progressCallable.call(new IProgressListener() {
                        public void onProgressChanged(int pProgress) {
                            AnonymousClass2.this.onProgressUpdate(Integer.valueOf(pProgress));
                        }
                    });
                } catch (Exception e) {
                    this.mException = e;
                    return null;
                }
            }

            public void onProgressUpdate(Integer... values) {
                this.mPD.setProgress(values[0].intValue());
            }

            public void onPostExecute(T result) {
                try {
                    this.mPD.dismiss();
                } catch (Exception e) {
                    Debug.e("Error", e);
                }
                if (isCancelled()) {
                    this.mException = new BaseActivity.CancelledException();
                }
                if (this.mException == null) {
                    callback.onCallback(result);
                } else if (callback2 == null) {
                    Debug.e("Error", this.mException);
                } else {
                    callback2.onCallback(this.mException);
                }
                super.onPostExecute(result);
            }
        }.execute((Object[]) null);
    }

    public static <T> void doAsync(Context pContext, int pTitleResID, int pMessageResID, AsyncCallable asyncCallable, final Callback callback, Callback<Exception> pExceptionCallback) {
        final ProgressDialog pd = ProgressDialog.show(pContext, pContext.getString(pTitleResID), pContext.getString(pMessageResID));
        asyncCallable.call(new Callback<T>() {
            public void onCallback(T result) {
                try {
                    pd.dismiss();
                } catch (Exception e) {
                    Debug.e("Error", e);
                }
                callback.onCallback(result);
            }
        }, pExceptionCallback);
    }
}
