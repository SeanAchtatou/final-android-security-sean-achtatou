package org.anddev.andengine.util;

import org.anddev.andengine.util.pool.GenericPool;

public class TransformationPool {
    private static final GenericPool<Transformation> POOL = new GenericPool<Transformation>() {
        /* access modifiers changed from: protected */
        public Transformation onAllocatePoolItem() {
            return new Transformation();
        }
    };

    public static Transformation obtain() {
        return POOL.obtainPoolItem();
    }

    public static void recycle(Transformation pTransformation) {
        pTransformation.setToIdentity();
        POOL.recyclePoolItem(pTransformation);
    }
}
