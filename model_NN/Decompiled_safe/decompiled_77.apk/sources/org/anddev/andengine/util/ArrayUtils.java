package org.anddev.andengine.util;

public class ArrayUtils {
    public static <T> T random(T[] pArray) {
        return pArray[MathUtils.random(0, pArray.length - 1)];
    }
}
