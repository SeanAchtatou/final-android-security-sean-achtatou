package org.anddev.andengine.opengl.texture.source.decorator;

import android.graphics.AvoidXfermode;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import org.anddev.andengine.opengl.texture.source.decorator.BaseTextureSourceDecorator;
import org.anddev.andengine.opengl.texture.source.decorator.shape.ITextureSourceDecoratorShape;

public class ColorKeyTextureSourceDecorator extends BaseShapeTextureSourceDecorator {
    private static final int TOLERANCE_DEFAULT = 0;
    private final int mColorKeyColor;
    private final int mTolerance;

    public ColorKeyTextureSourceDecorator(ITextureSource pTextureSource, ITextureSourceDecoratorShape pTextureSourceDecoratorShape, int pColorKeyColor) {
        this(pTextureSource, pTextureSourceDecoratorShape, pColorKeyColor, 0, null);
    }

    public ColorKeyTextureSourceDecorator(ITextureSource pTextureSource, ITextureSourceDecoratorShape pTextureSourceDecoratorShape, int pColorKeyColor, BaseTextureSourceDecorator.TextureSourceDecoratorOptions pTextureSourceDecoratorOptions) {
        this(pTextureSource, pTextureSourceDecoratorShape, pColorKeyColor, 0, pTextureSourceDecoratorOptions);
    }

    public ColorKeyTextureSourceDecorator(ITextureSource pTextureSource, ITextureSourceDecoratorShape pTextureSourceDecoratorShape, int pColorKeyColor, int pTolerance) {
        this(pTextureSource, pTextureSourceDecoratorShape, pColorKeyColor, pTolerance, null);
    }

    public ColorKeyTextureSourceDecorator(ITextureSource pTextureSource, ITextureSourceDecoratorShape pTextureSourceDecoratorShape, int pColorKeyColor, int pTolerance, BaseTextureSourceDecorator.TextureSourceDecoratorOptions pTextureSourceDecoratorOptions) {
        super(pTextureSource, pTextureSourceDecoratorShape, pTextureSourceDecoratorOptions);
        this.mColorKeyColor = pColorKeyColor;
        this.mTolerance = pTolerance;
        this.mPaint.setXfermode(new AvoidXfermode(pColorKeyColor, pTolerance, AvoidXfermode.Mode.TARGET));
        this.mPaint.setColor(0);
    }

    public ColorKeyTextureSourceDecorator clone() {
        return new ColorKeyTextureSourceDecorator(this.mTextureSource, this.mTextureSourceDecoratorShape, this.mColorKeyColor, this.mTolerance, this.mTextureSourceDecoratorOptions);
    }
}
