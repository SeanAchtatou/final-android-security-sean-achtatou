package org.anddev.andengine.opengl.texture.region.buffer;

import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class TiledTextureRegionBuffer extends BaseTextureRegionBuffer {
    public TiledTextureRegionBuffer(TiledTextureRegion pTextureRegion, int pDrawType) {
        super(pTextureRegion, pDrawType);
    }

    public TiledTextureRegion getTextureRegion() {
        return (TiledTextureRegion) super.getTextureRegion();
    }

    /* access modifiers changed from: protected */
    public float getX1() {
        TiledTextureRegion textureRegion = getTextureRegion();
        return textureRegion.getTexturePositionOfCurrentTileX() / ((float) textureRegion.getTexture().getWidth());
    }

    /* access modifiers changed from: protected */
    public float getX2() {
        TiledTextureRegion textureRegion = getTextureRegion();
        return (textureRegion.getTexturePositionOfCurrentTileX() + ((float) textureRegion.getTileWidth())) / ((float) textureRegion.getTexture().getWidth());
    }

    /* access modifiers changed from: protected */
    public float getY1() {
        TiledTextureRegion textureRegion = getTextureRegion();
        return textureRegion.getTexturePositionOfCurrentTileY() / ((float) textureRegion.getTexture().getHeight());
    }

    /* access modifiers changed from: protected */
    public float getY2() {
        TiledTextureRegion textureRegion = getTextureRegion();
        return (textureRegion.getTexturePositionOfCurrentTileY() + ((float) textureRegion.getTileHeight())) / ((float) textureRegion.getTexture().getHeight());
    }
}
