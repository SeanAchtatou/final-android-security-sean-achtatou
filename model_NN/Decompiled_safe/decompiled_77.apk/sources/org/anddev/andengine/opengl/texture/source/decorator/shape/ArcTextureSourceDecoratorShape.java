package org.anddev.andengine.opengl.texture.source.decorator.shape;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import org.anddev.andengine.opengl.texture.source.decorator.BaseTextureSourceDecorator;

public class ArcTextureSourceDecoratorShape implements ITextureSourceDecoratorShape {
    private static final float STARTANGLE_DEFAULT = 0.0f;
    private static final float SWEEPANGLE_DEFAULT = 360.0f;
    private static final boolean USECENTER_DEFAULT = true;
    private static ArcTextureSourceDecoratorShape sDefaultInstance;
    private final RectF mRectF;
    private final float mStartAngle;
    private final float mSweepAngle;
    private final boolean mUseCenter;

    public ArcTextureSourceDecoratorShape() {
        this(STARTANGLE_DEFAULT, SWEEPANGLE_DEFAULT, USECENTER_DEFAULT);
    }

    public ArcTextureSourceDecoratorShape(float pStartAngle, float pSweepAngle, boolean pUseCenter) {
        this.mRectF = new RectF();
        this.mStartAngle = pStartAngle;
        this.mSweepAngle = pSweepAngle;
        this.mUseCenter = pUseCenter;
    }

    @Deprecated
    public static ArcTextureSourceDecoratorShape getDefaultInstance() {
        if (sDefaultInstance == null) {
            sDefaultInstance = new ArcTextureSourceDecoratorShape();
        }
        return sDefaultInstance;
    }

    public void onDecorateBitmap(Canvas pCanvas, Paint pPaint, BaseTextureSourceDecorator.TextureSourceDecoratorOptions pDecoratorOptions) {
        this.mRectF.set(pDecoratorOptions.getInsetLeft(), pDecoratorOptions.getInsetTop(), ((float) (pCanvas.getWidth() - 1)) - pDecoratorOptions.getInsetRight(), ((float) (pCanvas.getHeight() - 1)) - pDecoratorOptions.getInsetBottom());
        pCanvas.drawArc(this.mRectF, this.mStartAngle, this.mSweepAngle, this.mUseCenter, pPaint);
    }
}
