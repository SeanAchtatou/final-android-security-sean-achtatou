package org.anddev.andengine.opengl.texture.source.decorator.shape;

import android.graphics.Canvas;
import android.graphics.Paint;
import org.anddev.andengine.opengl.texture.source.decorator.BaseTextureSourceDecorator;

public class RectangleTextureSourceDecoratorShape implements ITextureSourceDecoratorShape {
    private static RectangleTextureSourceDecoratorShape sDefaultInstance;

    public static RectangleTextureSourceDecoratorShape getDefaultInstance() {
        if (sDefaultInstance == null) {
            sDefaultInstance = new RectangleTextureSourceDecoratorShape();
        }
        return sDefaultInstance;
    }

    public void onDecorateBitmap(Canvas pCanvas, Paint pPaint, BaseTextureSourceDecorator.TextureSourceDecoratorOptions pDecoratorOptions) {
        pCanvas.drawRect(pDecoratorOptions.getInsetLeft(), pDecoratorOptions.getInsetTop(), ((float) (pCanvas.getWidth() - 1)) - pDecoratorOptions.getInsetRight(), ((float) (pCanvas.getHeight() - 1)) - pDecoratorOptions.getInsetBottom(), pPaint);
    }
}
