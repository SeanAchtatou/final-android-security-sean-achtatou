package com.polartouch.blockpro;

import android.content.SharedPreferences;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.WindowManager;
import com.badlogic.gdx.math.Vector2;

public class Const {
    public static final int CAMERA_HEIGHT = 800;
    public static final int CAMERA_WIDTH = 480;
    public static final boolean allowSleep = false;
    public static BlockPro blockpro = null;
    public static final int boxDensity = 15;
    public static final float boxFriction = 1.7f;
    public static final boolean continuousPhysics = false;
    private static final boolean debug = false;
    public static int extraDropTime = 0;
    public static final int fps = 60;
    public static final int glowshape = 3;
    public static final Vector2 gravity = new Vector2(0.0f, 2700.0f);
    public static final Vector2 gravityUp = new Vector2(0.0f, -2700.0f);
    static final int gravity_int = 2700;
    public static final int highTowerNrOfLives = 3;
    public static final float highTowerScrollSpeed = 2.0f;
    public static final int highTowerblocksPerLevel = 10;
    private static Const inst = new Const();
    public static boolean justCommingFromMenu = false;
    public static boolean justStartingUp = true;
    public static final int maxBoxSpeed = 4;
    public static final int maxStickBoxSpeed = 5;
    public static final int[] nrOfLevelsArray = {15, 10};
    public static final int positionIterations = 15;
    public static int selectedArcadeBlockType = 0;
    public static int selectedWorld = 0;
    public static int selectedlevel = 1;
    public static final int sixshape = 2;
    public static final int square = 1;
    public static final int triglow = 0;
    private static Vibrator v = null;
    public static final int velocityIterations = 8;
    private static boolean wantVibration;
    private static boolean wantVibrationSet = false;

    public static int getArcadeScore(BlockPro bp) {
        return PreferenceManager.getDefaultSharedPreferences(bp.getBaseContext()).getInt("arcadeScore", 0);
    }

    public static int getHighTowerScore(BlockPro bp) {
        return PreferenceManager.getDefaultSharedPreferences(bp.getBaseContext()).getInt("hightowerScore", 0);
    }

    public static Const getInstance() {
        return inst;
    }

    public static int getScreenRotation() {
        Display disp = ((WindowManager) blockpro.getSystemService("window")).getDefaultDisplay();
        try {
            return ((Integer) disp.getClass().getMethod("getRotation", new Class[0]).invoke(disp, new Object[0])).intValue();
        } catch (NoSuchMethodException e) {
            int orientation = disp.getOrientation();
            if (orientation == 0 && (orientation = blockpro.getResources().getConfiguration().orientation) == 0) {
                if (disp.getWidth() == disp.getHeight()) {
                    orientation = 3;
                } else if (disp.getWidth() < disp.getHeight()) {
                    orientation = 1;
                } else {
                    orientation = 2;
                }
            }
            if (orientation == 1) {
                return 0;
            }
            return 1;
        } catch (Exception e2) {
            return 0;
        }
    }

    public static int getSensitivity(BlockPro bp) {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(bp.getBaseContext()).getString("sensitivity", "2"));
    }

    public static boolean getUseAnimation(BlockPro bp) {
        return Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(bp.getBaseContext()).getBoolean("animation", true)).booleanValue();
    }

    public static boolean getUseParticles(BlockPro bp) {
        return Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(bp.getBaseContext()).getBoolean("particles", true)).booleanValue();
    }

    /* JADX INFO: Multiple debug info for r0v2 float: [D('margin' float), D('bottom' float)] */
    /* JADX INFO: Multiple debug info for r6v2 com.badlogic.gdx.math.Vector2[]: [D('type' int), D('vertices' com.badlogic.gdx.math.Vector2[])] */
    /* JADX INFO: Multiple debug info for r6v4 com.badlogic.gdx.math.Vector2[]: [D('type' int), D('vertices' com.badlogic.gdx.math.Vector2[])] */
    /* JADX INFO: Multiple debug info for r6v6 com.badlogic.gdx.math.Vector2[]: [D('type' int), D('vertices' com.badlogic.gdx.math.Vector2[])] */
    /* JADX INFO: Multiple debug info for r6v8 com.badlogic.gdx.math.Vector2[]: [D('type' int), D('vertices' com.badlogic.gdx.math.Vector2[])] */
    /* JADX INFO: Multiple debug info for r6v9 com.badlogic.gdx.math.Vector2[]: [D('type' int), D('vertices' com.badlogic.gdx.math.Vector2[])] */
    public static Vector2[] getVertices(int type, float halfWidth, float halfHeight) {
        float margin2 = 0.0f / (2.0f * halfWidth);
        float top = -halfHeight;
        float bottom = halfHeight;
        float left = -halfWidth;
        float right = halfWidth;
        Vector2[] vertices = null;
        switch (type) {
            case 0:
                return new Vector2[]{new Vector2(left + margin2, bottom - margin2), new Vector2(0.0f, top + margin2), new Vector2(right - margin2, bottom - margin2)};
            case 1:
                return new Vector2[]{new Vector2(left, top), new Vector2(right, top), new Vector2(right, bottom), new Vector2(left, bottom)};
            case 2:
                return new Vector2[]{new Vector2(-0.46f * halfWidth, -0.94f * halfHeight), new Vector2(0.46f * halfWidth, -0.94f * halfHeight), new Vector2(0.94f * halfWidth, 0.0f * halfHeight), new Vector2(0.46f * halfWidth, 0.94f * halfHeight), new Vector2(-0.46f * halfWidth, 0.94f * halfHeight), new Vector2(halfWidth * -0.94f, halfHeight * 0.0f)};
            case 3:
                return new Vector2[]{new Vector2(0.04f + left, 0.04f + top), new Vector2(right - 0.04f, top + 0.04f), new Vector2(right - 0.04f, bottom - 0.04f), new Vector2(left + 0.04f, bottom - 0.04f)};
            default:
                return vertices;
        }
    }

    public static int getWorld1Level(BlockPro bp) {
        return PreferenceManager.getDefaultSharedPreferences(bp.getBaseContext()).getInt("world1level", 0);
    }

    public static int getWorld2Level(BlockPro bp) {
        return PreferenceManager.getDefaultSharedPreferences(bp.getBaseContext()).getInt("world2level", 0);
    }

    public static void log(String s) {
    }

    public static void setArcadeScore(BlockPro bp, int score) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(bp.getBaseContext()).edit();
        editor.putInt("arcadeScore", score);
        editor.commit();
    }

    public static void setHighTowerScore(BlockPro bp, int score) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(bp.getBaseContext()).edit();
        editor.putInt("hightowerScore", score);
        editor.commit();
    }

    public static void setVibration(BlockPro bp, boolean value) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(bp.getBaseContext()).edit();
        editor.putBoolean("vibrate", value);
        editor.commit();
        wantVibration = value;
    }

    public static void setWorld1Level(BlockPro bp, int level) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(bp.getBaseContext()).edit();
        editor.putInt("world1level", level);
        editor.commit();
    }

    public static void setWorld2Level(BlockPro bp, int level) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(bp.getBaseContext()).edit();
        editor.putInt("world2level", level);
        editor.commit();
    }

    public static void updateVibration(BlockPro bp) {
        wantVibration = PreferenceManager.getDefaultSharedPreferences(bp.getBaseContext()).getBoolean("vibrate", true);
    }

    public static void vibrate(long ms, BlockPro bp) {
        if (!wantVibrationSet) {
            blockpro = bp;
            v = (Vibrator) bp.getSystemService("vibrator");
            updateVibration(blockpro);
        }
        if (wantVibration) {
            v.vibrate(ms);
        }
    }

    private Const() {
    }
}
