package com.polartouch.blockpro.boxes;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.Const;
import com.polartouch.blockpro.game.BottomBlock;
import java.util.Random;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.entity.particle.ParticleSystem;
import org.anddev.andengine.entity.particle.emitter.PointParticleEmitter;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.extension.physics.box2d.util.Vector2Pool;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class StickyBox extends Box {
    private static final float PTMR = 32.0f;
    public static float highestStableBlock = 600.0f;
    protected final int MAXSPEED = 5;
    private int blinkingtimer;
    private BlockPro blockpro;
    public boolean isBlinking = false;
    public boolean isNotStarted = true;
    private boolean isStable;
    private PointParticleEmitter pe;
    private ParticleSystem ps;
    private int stableTimer;
    public int statValue = 0;
    private boolean usesParticle = false;
    private float vx;
    private float xPOffset;
    private float yPOffset;

    public StickyBox(int type, Engine e, FixedStepPhysicsWorld mPhysicsWorld, TextureRegion tr, TextureRegion trShadow, float x) {
        super(type, e, mPhysicsWorld, tr, trShadow, x);
    }

    private void blink() {
        this.blinkingtimer++;
        if (this.blinkingtimer > 30) {
            this.faceShadow.setAlpha(1.0f);
            this.isBlinking = false;
        }
    }

    public StickyBox build(BottomBlock bb, BlockPro blockpro2) {
        this.blockpro = blockpro2;
        this.boxFriction = 0.6f;
        super.build();
        this.stableTimer = 0;
        Vector2 v = Vector2Pool.obtain(this.vx, 0.0f);
        this.body.setLinearVelocity(v);
        Vector2Pool.recycle(v);
        return this;
    }

    public StickyBox impulse(float vx2) {
        this.vx = vx2;
        return this;
    }

    public boolean isStable() {
        return this.isStable;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* access modifiers changed from: protected */
    public void onFaceUpdate(float pSecondsElapsed) {
        if (this.body != null) {
            updateShadowPosition();
            if (this.isDead) {
                return;
            }
            if (this.isStable) {
                updateStucknes();
            } else if (this.isBlinking) {
                blink();
            } else {
                if (this.usesParticle) {
                    this.pe.setCenter(this.face.getX() + this.xPOffset, this.face.getY() + this.yPOffset);
                }
                Vector2 v = Vector2Pool.obtain(this.body.getLinearVelocity());
                v.x = Math.min(5.0f, Math.max(-5.0f, v.x));
                v.y = Math.min(5.0f, Math.max(-5.0f, v.y));
                Vector2 pos = Vector2Pool.obtain(this.body.getPosition());
                if (pos.x * 32.0f < (-this.face.getWidth()) && v.x < 0.0f) {
                    pos.x = 15.0f - pos.x;
                    this.body.setTransform(pos, this.body.getAngularVelocity());
                } else if (pos.x * 32.0f > 480.0f + this.face.getWidth() && v.x > 0.0f) {
                    pos.x = 15.0f - pos.x;
                    this.body.setTransform(pos, this.body.getAngularVelocity());
                }
                if (this.isNotStarted) {
                    Vector2Pool.recycle(pos);
                    Vector2Pool.recycle(v);
                    return;
                }
                pos.x *= 32.0f;
                pos.y *= 32.0f;
                if (v.y * ((float) this.direction) < 5.0f) {
                    this.body.applyForce(this.direction == 1 ? Const.gravity : Const.gravityUp, this.body.getWorldCenter());
                }
                updateCameraPos();
                this.body.setLinearVelocity(v);
                if (Math.abs(v.y) >= 0.01f || Math.abs(v.x) >= 1.0f || this.face.getY() <= ((float) (this.y + 10)) || ((double) Math.abs(this.body.getAngularVelocity())) >= 0.01d) {
                    this.stableTimer = 0;
                    this.isDead = !this.isStable && pos.y > 850.0f && this.direction == 1;
                    this.blockpro.updateCamera();
                } else {
                    int i = this.stableTimer;
                    this.stableTimer = i + 1;
                    if (i > 4) {
                        setToStable();
                    }
                }
                Vector2Pool.recycle(pos);
                Vector2Pool.recycle(v);
            }
        }
    }

    public void resetHighestBlock() {
        highestStableBlock = 600.0f;
    }

    public StickyBox rotate(float rotation) {
        super.rotate(rotation);
        return this;
    }

    /* access modifiers changed from: protected */
    public void setColor() {
        Random r = new Random();
        float f2 = r.nextFloat();
        this.face.setColor(1.0f - (r.nextFloat() * f2), 0.0f, f2);
    }

    public void setParticleEffect(PointParticleEmitter particleEmitter, ParticleSystem ps2, float pWidth, float pHeight) {
        this.pe = particleEmitter;
        this.ps = ps2;
        this.xPOffset = (((float) this.tr.getWidth()) - pWidth) / 2.0f;
        this.yPOffset = (((float) this.tr.getHeight()) - pHeight) / 2.0f;
        this.usesParticle = true;
        ps2.setParticlesSpawnEnabled(true);
    }

    private void setToStable() {
        this.isStable = true;
        this.body.setType(BodyDef.BodyType.KinematicBody);
        this.body.setAngularVelocity(0.0f);
        if (highestStableBlock > this.face.getY()) {
            highestStableBlock = this.face.getY();
        }
    }

    /* access modifiers changed from: protected */
    public void setY() {
        this.y = Math.round(highestStableBlock - 350.0f);
    }

    public void stopParticleEffect() {
        this.usesParticle = false;
        if (this.ps != null) {
            this.ps.setParticlesSpawnEnabled(false);
        }
    }

    public StickyBox tilt(float tilt) {
        super.tilt(tilt);
        return this;
    }

    private void updateCameraPos() {
        if ((this.blockpro.getCamera().getCenterY() - 400.0f) + 500.0f <= this.face.getY()) {
            this.blockpro.getCamera().setCenter(0.0f, (this.face.getY() - 500.0f) + 400.0f);
        }
    }

    private void updateShadowPosition() {
        float y = this.face.getY();
        this.faceShadow.setPosition(this.face.getX() - this.xOffset, y - this.yOffset);
        this.faceShadow.setRotation(this.face.getRotation());
    }

    private void updateStucknes() {
        Vector2 v = Vector2Pool.obtain(this.body.getLinearVelocity().x, 0.0f);
        this.body.setLinearVelocity(v);
        updateShadowPosition();
        Vector2Pool.recycle(v);
    }
}
