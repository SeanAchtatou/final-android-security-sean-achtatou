package com.polartouch.blockpro.boxes;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.polartouch.blockpro.Const;
import com.polartouch.blockpro.SmoothSprite;
import java.util.Random;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.entity.particle.ParticleSystem;
import org.anddev.andengine.entity.particle.emitter.PointParticleEmitter;
import org.anddev.andengine.entity.particle.initializer.AlphaInitializer;
import org.anddev.andengine.entity.particle.initializer.ColorInitializer;
import org.anddev.andengine.entity.particle.initializer.RotationInitializer;
import org.anddev.andengine.entity.particle.initializer.VelocityInitializer;
import org.anddev.andengine.entity.particle.modifier.AlphaModifier;
import org.anddev.andengine.entity.particle.modifier.ColorModifier;
import org.anddev.andengine.entity.particle.modifier.ExpireModifier;
import org.anddev.andengine.entity.particle.modifier.ScaleModifier;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.extension.physics.box2d.util.Vector2Pool;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class Box {
    protected static final int MAXSPEED = 4;
    protected float angimpulse;
    public Body body;
    protected float boxFriction;
    protected int direction = 1;
    private final Engine engine;
    private final int extraTime = (Const.extraDropTime * 15);
    public SmoothSprite face;
    public SmoothSprite faceShadow;
    public boolean isDead = false;
    private final FixedStepPhysicsWorld mPhysicsWorld;
    private PointParticleEmitter particleEmitter;
    private ParticleSystem particleSystem;
    private PhysicsConnector pc;
    public int releaseTime = (this.extraTime + 15);
    protected float rotation;
    protected float scale;
    protected TextureRegion tr;
    private final TextureRegion trShadow;
    private final int type;
    private boolean usesParticle = false;
    protected float xOffset;
    private float xOffsetParticle;
    protected float xvalue;
    protected float xvalue2;
    protected int y;
    protected float yOffset;
    private float yOffsetParticle;

    public Box(int type2, Engine e, FixedStepPhysicsWorld mPhysicsWorld2, TextureRegion tr2, TextureRegion trShadow2, float x) {
        this.engine = e;
        this.mPhysicsWorld = mPhysicsWorld2;
        this.tr = tr2;
        this.trShadow = trShadow2;
        this.xOffset = (float) ((trShadow2.getWidth() - tr2.getWidth()) / 2);
        this.yOffset = (float) ((trShadow2.getHeight() - tr2.getHeight()) / 2);
        this.type = type2;
        this.scale = 1.0f;
        this.xvalue2 = x;
        this.xvalue = (x / 100.0f) * ((float) (Const.CAMERA_WIDTH - tr2.getWidth()));
        this.boxFriction = 1.0f;
    }

    public void activate() {
        this.body.setActive(true);
        this.engine.getScene().getChild(4).attachChild(this.face);
        this.engine.getScene().getChild(3).attachChild(this.faceShadow);
        this.mPhysicsWorld.registerPhysicsConnector(this.pc);
    }

    public Box addBubbleParticle(Scene scene, TextureRegion particleTR) {
        this.xOffsetParticle = (float) (particleTR.getWidth() / 2);
        this.yOffsetParticle = (float) (particleTR.getHeight() / 2);
        this.usesParticle = true;
        this.particleEmitter = new PointParticleEmitter(this.xvalue, (float) this.y);
        this.particleSystem = new ParticleSystem(this.particleEmitter, 2.0f, 4.0f, 10, particleTR);
        this.particleSystem.addParticleInitializer(new ColorInitializer(1.0f, 1.0f, 1.0f));
        this.particleSystem.addParticleInitializer(new AlphaInitializer(0.0f));
        this.particleSystem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 1);
        this.particleSystem.addParticleInitializer(new VelocityInitializer(-8.0f, 8.0f, -20.0f, -40.0f));
        this.particleSystem.addParticleInitializer(new RotationInitializer(0.0f));
        this.particleSystem.addParticleModifier(new ScaleModifier(0.4f, 1.0f, 0.0f, 3.0f));
        this.particleSystem.addParticleModifier(new ColorModifier(1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 3.0f));
        this.particleSystem.addParticleModifier(new AlphaModifier(0.0f, 0.2f, 0.0f, 0.2f));
        this.particleSystem.addParticleModifier(new AlphaModifier(0.2f, 0.0f, 1.0f, 3.0f));
        this.particleSystem.addParticleModifier(new ExpireModifier(3.0f));
        this.particleSystem.setParticlesSpawnEnabled(false);
        scene.getChild(2).attachChild(this.particleSystem);
        return this;
    }

    public Box addNormalParticle(Scene scene, TextureRegion particleTR) {
        this.xOffsetParticle = (float) (particleTR.getWidth() / 2);
        this.yOffsetParticle = (float) (particleTR.getHeight() / 2);
        this.usesParticle = true;
        this.particleEmitter = new PointParticleEmitter(this.xvalue, (float) this.y);
        this.particleSystem = new ParticleSystem(this.particleEmitter, 4.0f, 4.0f, 10, particleTR);
        this.particleSystem.addParticleInitializer(new ColorInitializer(1.0f, 1.0f, 1.0f));
        this.particleSystem.addParticleInitializer(new AlphaInitializer(0.0f));
        this.particleSystem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 1);
        this.particleSystem.addParticleInitializer(new VelocityInitializer(0.0f, 0.0f, -5.0f, -5.0f));
        this.particleSystem.addParticleInitializer(new RotationInitializer(0.0f));
        this.particleSystem.addParticleModifier(new ScaleModifier(0.4f, 0.4f, 0.0f, 1.5f));
        this.particleSystem.addParticleModifier(new ColorModifier(1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.5f));
        this.particleSystem.addParticleModifier(new AlphaModifier(0.0f, 0.1f, 0.0f, 0.2f));
        this.particleSystem.addParticleModifier(new AlphaModifier(0.1f, 0.0f, 1.0f, 1.5f));
        this.particleSystem.addParticleModifier(new ExpireModifier(1.5f));
        this.particleSystem.setParticlesSpawnEnabled(false);
        scene.getChild(2).attachChild(this.particleSystem);
        return this;
    }

    public Box build() {
        FixtureDef objectFixtureDef = PhysicsFactory.createFixtureDef(this.scale * this.scale * 15.0f, 0.0f, this.scale * this.scale * 1.7f * this.boxFriction);
        setY();
        createFace(this.xvalue, (float) this.y);
        this.face.setScale(this.scale);
        this.faceShadow.setScale(this.scale);
        this.faceShadow.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 1);
        Vector2[] vertices = Const.getVertices(this.type, (this.face.getWidthScaled() * 0.5f) / 32.0f, (this.face.getHeightScaled() * 0.5f) / 32.0f);
        setColor();
        this.body = PhysicsFactory.createPolygonBody(this.mPhysicsWorld, this.face, vertices, BodyDef.BodyType.DynamicBody, objectFixtureDef);
        this.body.setTransform(this.body.getPosition(), -1.57f * this.rotation);
        this.body.applyAngularImpulse(this.angimpulse);
        this.body.setActive(false);
        this.pc = new PhysicsConnector(this.face, this.body, true, true);
        return this;
    }

    private void createFace(float x, float y2) {
        this.face = new SmoothSprite(x, y2, this.tr) {
            public void onManagedUpdate(float pSecondsElapsed) {
                super.onManagedUpdate(pSecondsElapsed);
                Box.this.onFaceUpdate(pSecondsElapsed);
            }
        };
        this.faceShadow = new SmoothSprite(x - this.xOffset, y2 - this.yOffset, this.trShadow);
    }

    public Box direction(int direction2) {
        this.direction = direction2;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* access modifiers changed from: protected */
    public void onFaceUpdate(float pSecondsElapsed) {
        boolean z;
        if (this.body != null) {
            Vector2 v = Vector2Pool.obtain(this.body.getLinearVelocity());
            v.y = Math.min(4.0f, Math.max(-4.0f, v.y));
            if (v.y * ((float) this.direction) < 4.0f) {
                this.body.applyForce(this.direction == 1 ? Const.gravity : Const.gravityUp, this.body.getWorldCenter());
            }
            float xCenter = this.body.getWorldCenter().x * 32.0f;
            float y2 = this.face.getY();
            this.faceShadow.setPosition(this.face.getX() - this.xOffset, y2 - this.yOffset);
            this.faceShadow.setRotation(this.face.getRotation());
            if ((y2 <= 800.0f || this.direction != 1) && (y2 >= 0.0f || this.direction != -1)) {
                z = false;
            } else {
                z = true;
            }
            this.isDead = z;
            if (this.usesParticle && (v.y < -1.0f || v.y > 1.0f)) {
                this.particleSystem.setParticlesSpawnEnabled(true);
                this.particleEmitter.setCenter(xCenter - this.xOffsetParticle, y2 - this.yOffsetParticle);
            } else if (this.particleEmitter != null) {
                this.particleSystem.setParticlesSpawnEnabled(false);
            }
            Vector2Pool.recycle(v);
        }
    }

    public Box rotate(float angimpulse2) {
        this.angimpulse = angimpulse2;
        return this;
    }

    /* access modifiers changed from: protected */
    public void setColor() {
        Random r = new Random();
        this.face.setColor(r.nextFloat() / 3.0f, r.nextFloat() * 0.7f, 1.0f);
    }

    /* access modifiers changed from: protected */
    public void setY() {
        if (this.direction == -1) {
            this.y = 870;
        } else {
            this.y = -70;
        }
    }

    public void stop() {
        if (this.particleSystem != null) {
            this.particleSystem.setParticlesSpawnEnabled(false);
            this.particleSystem = null;
            this.particleEmitter = null;
        }
    }

    public Box tilt(float rotation2) {
        this.rotation = rotation2;
        return this;
    }

    public Box time(int releaseTime2) {
        this.releaseTime = this.extraTime + releaseTime2;
        return this;
    }

    public String toString() {
        return "Box";
    }

    public Box up() {
        this.direction = -1;
        setY();
        return this;
    }
}
