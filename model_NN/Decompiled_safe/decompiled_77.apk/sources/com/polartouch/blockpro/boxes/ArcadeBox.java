package com.polartouch.blockpro.boxes;

import com.badlogic.gdx.math.Vector2;
import com.polartouch.blockpro.Const;
import java.util.Random;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.extension.physics.box2d.util.Vector2Pool;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class ArcadeBox extends Box {
    private static final int MARGIN = 50;
    private static final float PTMR = 32.0f;
    private static Random r = new Random();
    public int statBounces = 0;

    public ArcadeBox(int type, Engine e, FixedStepPhysicsWorld mPhysicsWorld, TextureRegion tr, TextureRegion trShadow, float x) {
        super(type, e, mPhysicsWorld, tr, trShadow, x);
    }

    public ArcadeBox build() {
        super.build();
        this.body.setLinearVelocity(new Vector2(((float) r.nextInt(6)) + ((float) ((Math.round(this.xvalue2) - 20) / -10)), (float) (this.direction * 15)));
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* access modifiers changed from: protected */
    public void onFaceUpdate(float pSecondsElapsed) {
        Vector2 force;
        if (this.body != null) {
            Vector2 v = Vector2Pool.obtain(this.body.getLinearVelocity());
            v.x = Math.min(4.0f, Math.max(-4.0f, v.x));
            v.y = Math.min(4.0f, Math.max(-4.0f, v.y));
            Vector2 pos = Vector2Pool.obtain(this.body.getPosition());
            if (pos.x * 32.0f < (-this.face.getWidth()) && v.x < 0.0f) {
                pos.x = 15.0f - pos.x;
                this.body.setTransform(pos, this.body.getAngularVelocity());
            } else if (pos.x * 32.0f > 480.0f + this.face.getWidth() && v.x > 0.0f) {
                pos.x = 15.0f - pos.x;
                this.body.setTransform(pos, this.body.getAngularVelocity());
            }
            pos.x *= 32.0f;
            pos.y *= 32.0f;
            float y = this.face.getY();
            float x = this.face.getX();
            if (v.y * ((float) this.direction) < 4.0f) {
                if (this.direction == 1) {
                    force = Const.gravity;
                } else {
                    force = Const.gravityUp;
                }
                this.body.applyForce(force, this.body.getWorldCenter());
            }
            this.faceShadow.setPosition(x - this.xOffset, y - this.yOffset);
            this.faceShadow.setRotation(this.face.getRotation());
            this.body.setLinearVelocity(v);
            Vector2Pool.recycle(v);
            if ((pos.y > 750.0f && this.direction == 1) || (pos.y < 50.0f && this.direction == -1)) {
                reveseGravity();
            }
            this.isDead = (pos.y > 800.0f && this.direction == 1) || (pos.y < 0.0f && this.direction == -1);
            Vector2Pool.recycle(pos);
        }
    }

    public void reveseGravity() {
        this.statBounces++;
        this.direction *= -1;
    }

    /* access modifiers changed from: protected */
    public void setColor() {
        Random r2 = new Random();
        float f2 = r2.nextFloat();
        this.face.setColor(1.0f - (r2.nextFloat() * f2), 0.0f, f2);
    }

    public ArcadeBox up() {
        super.up();
        return this;
    }
}
