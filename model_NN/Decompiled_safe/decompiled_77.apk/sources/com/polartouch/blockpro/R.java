package com.polartouch.blockpro;

public final class R {

    public static final class anim {
        public static final int fade = 2130968576;
        public static final int hold = 2130968577;
    }

    public static final class array {
        public static final int rotation_options = 2131099650;
        public static final int rotation_values = 2131099651;
        public static final int sensitivity_options = 2131099648;
        public static final int sensitivity_values = 2131099649;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int solid_blue = 2131165185;
        public static final int solid_green = 2131165186;
        public static final int solid_red = 2131165184;
        public static final int solid_yellow = 2131165187;
    }

    public static final class drawable {
        public static final int blue = 2130837506;
        public static final int green = 2130837507;
        public static final int icon = 2130837504;
        public static final int red = 2130837505;
        public static final int screen_background_black = 2130837509;
        public static final int translucent_background = 2130837510;
        public static final int transparent_background = 2130837511;
        public static final int yellow = 2130837508;
    }

    public static final class id {
        public static final int about_feedback_button = 2131427330;
        public static final int aboutscrollview = 2131427328;
        public static final int back_title = 2131427340;
        public static final int exit_title = 2131427341;
        public static final int lncontainerpopup = 2131427336;
        public static final int lnparent = 2131427338;
        public static final int lnparentpopup = 2131427335;
        public static final int popuplayout = 2131427333;
        public static final int settings_title = 2131427339;
        public static final int textView1 = 2131427329;
        public static final int textView2 = 2131427331;
        public static final int textView3 = 2131427332;
        public static final int txtpopup = 2131427337;
        public static final int wv1 = 2131427334;
    }

    public static final class layout {
        public static final int about_view = 2130903040;
        public static final int arcade_help_view = 2130903041;
        public static final int changelog_view = 2130903042;
        public static final int help = 2130903043;
        public static final int help_popup = 2130903044;
        public static final int help_view = 2130903045;
        public static final int hightower_help_view = 2130903046;
        public static final int level_help_view = 2130903047;
        public static final int main = 2130903048;
    }

    public static final class menu {
        public static final int menu = 2131361792;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int changelog = 2131230721;
        public static final int notes = 2131230722;
    }

    public static final class style {
        public static final int Theme = 2131296256;
        public static final int Theme_Translucent = 2131296258;
        public static final int Theme_Transparent = 2131296257;
    }

    public static final class xml {
        public static final int settings = 2131034112;
    }
}
