package com.polartouch.blockpro;

import android.graphics.Typeface;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.font.FontFactory;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;

public class FontLoader {
    private final BlockPro blockpro;
    private Texture[] loadedTextures;
    private int loadedTexturesCount;
    public Font mFont;
    public Texture mFontTexture;
    public Font mMenuFont;
    public Font mMenuFont2;
    public Font mMenuFont3;
    public Texture mMenuFontTexture;
    public Texture mMenuFontTexture2;
    public Texture mMenuFontTexture3;

    public FontLoader(BlockPro bp) {
        this.blockpro = bp;
        load();
    }

    public boolean isLoadedToMemory() {
        for (int i = 0; i < this.loadedTexturesCount; i++) {
            if (!this.loadedTextures[i].isLoadedToHardware()) {
                return false;
            }
        }
        return true;
    }

    public void load() {
        FontFactory.setAssetBasePath("fonts/");
        this.mFontTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFont = FontFactory.createFromAsset(this.mFontTexture, this.blockpro, "gl.ttf", 35.0f, true, -1);
        this.mMenuFontTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mMenuFont = new Font(this.mMenuFontTexture, Typeface.create(Typeface.DEFAULT, 1), 40.0f, true, -1);
        this.mMenuFontTexture2 = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mMenuFont2 = new Font(this.mMenuFontTexture2, Typeface.create(Typeface.DEFAULT, 1), 60.0f, true, -1);
        this.mMenuFontTexture3 = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mMenuFont3 = new Font(this.mMenuFontTexture3, Typeface.create(Typeface.DEFAULT, 1), 90.0f, true, -1);
        loadTextures(this.mFontTexture, this.mMenuFontTexture, this.mMenuFontTexture2, this.mMenuFontTexture3);
        this.blockpro.getEngine().getFontManager().loadFonts(this.mFont, this.mMenuFont, this.mMenuFont2, this.mMenuFont3);
    }

    private void loadTextures(Texture... params) {
        this.loadedTextures = params;
        this.loadedTexturesCount = params.length;
        Const.log("loaded fonttextures " + this.loadedTexturesCount);
        for (int i = 0; i < this.loadedTexturesCount; i++) {
            this.blockpro.getEngine().getTextureManager().loadTexture(params[i]);
        }
    }

    public void unloadTextures() {
        Const.log("unloaded fonttextures " + this.loadedTexturesCount);
        for (int i = 0; i < this.loadedTexturesCount; i++) {
            this.blockpro.getEngine().getTextureManager().unloadTexture(this.loadedTextures[i]);
        }
        this.blockpro.getEngine().getFontManager().clear();
    }
}
