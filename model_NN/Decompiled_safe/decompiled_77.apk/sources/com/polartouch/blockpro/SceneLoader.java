package com.polartouch.blockpro;

import com.flurry.android.FlurryAgent;
import com.polartouch.blockpro.game.arcade.ArcadeScene;
import com.polartouch.blockpro.game.hightower.HighTowerScene;
import com.polartouch.blockpro.game.level.LevelScene;
import com.polartouch.blockpro.menu.MainScene;
import org.anddev.andengine.engine.Engine;

public class SceneLoader {
    private static SceneLoader inst = new SceneLoader();
    /* access modifiers changed from: private */
    public BlockPro blockpro;
    /* access modifiers changed from: private */
    public CustomScene currentScene;
    private Engine engine;
    /* access modifiers changed from: private */
    public CustomScene newScene;

    public static SceneLoader getInstance() {
        return inst;
    }

    public CustomScene getCurrentScene() {
        return this.currentScene;
    }

    public void loadArcadeScene() {
        FlurryAgent.logEvent("LoadArcadeScene");
        showLoadingScene();
        IAsyncCallback callback = new IAsyncCallback() {
            public void onComplete() {
                if (SceneLoader.this.currentScene != null) {
                    SceneLoader.this.currentScene.unload();
                }
                SceneLoader.this.newScene.startWhenLoaded();
                SceneLoader.this.saveSceneData();
            }

            public void workToDo() {
                SceneLoader.this.newScene = new ArcadeScene(6, SceneLoader.this.blockpro);
                SceneLoader.this.newScene.onloadScene();
            }
        };
        new AsyncTaskLoader().execute(callback);
    }

    public void loadHighTowerScene() {
        showLoadingScene();
        FlurryAgent.logEvent("LoadHighTowerScene");
        IAsyncCallback callback = new IAsyncCallback() {
            public void onComplete() {
                if (SceneLoader.this.currentScene != null) {
                    SceneLoader.this.currentScene.unload();
                }
                SceneLoader.this.newScene.startWhenLoaded();
                SceneLoader.this.saveSceneData();
            }

            public void workToDo() {
                SceneLoader.this.newScene = new HighTowerScene(6, SceneLoader.this.blockpro);
                SceneLoader.this.newScene.onloadScene();
            }
        };
        new AsyncTaskLoader().execute(callback);
    }

    public void loadLevelScene() {
        showLoadingScene();
        Stat.getInstance().startLevel(Const.selectedlevel, Const.selectedWorld);
        IAsyncCallback callback = new IAsyncCallback() {
            public void onComplete() {
                if (SceneLoader.this.currentScene != null) {
                    SceneLoader.this.currentScene.unload();
                }
                SceneLoader.this.newScene.startWhenLoaded();
                SceneLoader.this.saveSceneData();
            }

            public void workToDo() {
                SceneLoader.this.newScene = new LevelScene(6, SceneLoader.this.blockpro);
                SceneLoader.this.newScene.onloadScene();
            }
        };
        new AsyncTaskLoader().execute(callback);
    }

    public void loadMenuScene(Boolean firstLoad) {
        this.newScene = new MainScene(5, this.blockpro);
        this.newScene.onloadScene();
        this.engine.setScene(this.newScene);
        if (this.currentScene != null && !firstLoad.booleanValue()) {
            this.currentScene.unload();
        }
        saveSceneData();
    }

    public void onPause() {
        if (this.currentScene != null) {
            this.currentScene.onPause();
        }
    }

    public void onResume() {
        if (this.currentScene != null) {
            this.currentScene.onResume();
        }
    }

    /* access modifiers changed from: private */
    public void saveSceneData() {
        this.currentScene = this.newScene;
        this.newScene = null;
    }

    private void showLoadingScene() {
        LoadingScene loadingScene = new LoadingScene(1, this.blockpro);
        loadingScene.onloadScene();
        this.engine.setScene(loadingScene);
    }

    public void start(Engine e, BlockPro bp) {
        this.engine = e;
        this.blockpro = bp;
    }
}
