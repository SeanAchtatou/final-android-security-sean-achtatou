package com.polartouch.blockpro.game.level;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;
import com.badlogic.gdx.math.Vector2;
import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.Const;
import com.polartouch.blockpro.FontLoader;
import com.polartouch.blockpro.R;
import com.polartouch.blockpro.Stat;
import com.polartouch.blockpro.backgrounds.SkyBackground;
import com.polartouch.blockpro.backgrounds.WaterBackground;
import com.polartouch.blockpro.boxes.Box;
import com.polartouch.blockpro.game.GameMenues;
import com.polartouch.blockpro.game.GameScene;
import com.polartouch.blockpro.game.GameTextureLoader;
import com.polartouch.blockpro.game.SteadyBar;
import java.util.ArrayList;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.sensor.accelerometer.AccelerometerData;

public class LevelScene extends GameScene implements Scene.IOnSceneTouchListener {
    private boolean blocksIsStable;
    private final FontLoader fl;
    /* access modifiers changed from: private */
    public int levelToLoad = 1;
    private int levelToLoadText;
    private LevelLoader levelloader;
    private boolean okToWin;
    private int startDelay;
    private int statBodies;
    private int statBodiesText;
    private boolean statDead;
    public boolean statHasWon;
    private int statMovement;
    private int statRotation;
    private ChangeableText textBlock;
    private ChangeableText textDifficulty;
    private ChangeableText textLevel;
    private int winDelay;

    public LevelScene(int mLayerCount, BlockPro bp) {
        super(mLayerCount);
        this.gtl = GameTextureLoader.getInstance();
        this.engine = bp.getEngine();
        this.blockpro = bp;
        setOnSceneTouchListener(this);
        this.levelToLoad = Const.selectedlevel;
        this.fl = bp.fontloader;
    }

    private void createAndAddStaticSprites() {
        if (this.background == null) {
            switch (Const.selectedWorld) {
                case 0:
                    this.background = new SkyBackground(this.blockpro, this);
                    break;
                case 1:
                    this.background = new WaterBackground(this.blockpro, this);
                    break;
            }
        }
        getChild(5).attachChild(new Sprite(0.0f, 0.0f, this.gtl.trHud));
    }

    private void createAndAddTexts() {
        if (this.textLevel == null) {
            this.textLevel = new ChangeableText(20.0f, 20.0f, this.fl.mFont, "Level: XX/XX", "Level: 10/10".length());
            this.textLevel.setText("23456789");
            this.textLevel.setText("Level: " + this.levelToLoad + "/" + this.levelloader.nrOfLevels);
            this.textBlock = new ChangeableText(20.0f, 70.0f, this.fl.mFont, "", "Blocks: XX/XX".length());
            this.textBlock.setText("0123456789");
            this.textBlock.setText("Blocks: " + this.statBodies + "/" + this.levelloader.statMaxBodies);
            this.textDifficulty = new ChangeableText(240.0f, 20.0f, this.fl.mFont, "Diff: " + (Const.extraDropTime == 1 ? "Normal" : "Hard"), 12);
        }
        getChild(5).attachChild(this.textLevel);
        getChild(5).attachChild(this.textBlock);
        getChild(5).attachChild(this.textDifficulty);
    }

    private void createUpdateHandler() {
        this.updateHandler = new TimerHandler(0.1f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                if (!LevelScene.this.pausedGame) {
                    LevelScene.this.startedTimer = true;
                    LevelScene.this.tryToAddFace();
                    LevelScene.this.updateStats();
                    LevelScene.this.updateWinState();
                    if (LevelScene.this.hasWon()) {
                        LevelScene.this.storeResult();
                        LevelScene.this.stopHandlers();
                        LevelScene.this.setChildScene(LevelScene.this.gamemenues.completeMenuScene, false, true, true);
                        Stat.getInstance().completeLevel(LevelScene.this.levelToLoad, Const.selectedWorld);
                    } else if (LevelScene.this.hasLost()) {
                        Const.vibrate(50, LevelScene.this.blockpro);
                        LevelScene.this.stopHandlers();
                        LevelScene.this.setChildScene(LevelScene.this.gamemenues.deadMenuScene, false, true, true);
                        Stat.getInstance().failLevel(LevelScene.this.levelToLoad, Const.selectedWorld);
                    }
                    LevelScene.this.updateText();
                }
            }
        });
    }

    public void goToNextLevel() {
        if (this.levelloader.nrOfLevels > this.levelToLoad) {
            this.levelToLoad++;
            Stat.getInstance().nextLevel(this.levelToLoad, Const.selectedWorld);
            resetScene();
            return;
        }
        Toast.makeText(this.blockpro, "THIS WAS THE LAST LEVEL ;)", 0).show();
    }

    /* access modifiers changed from: protected */
    public boolean hasLost() {
        return this.statDead;
    }

    /* access modifiers changed from: protected */
    public boolean hasWon() {
        return this.statHasWon;
    }

    public void onAccelerometerChanged(AccelerometerData data) {
        if (this.levelloader != null && this.bottomblock != null && !this.pausedGame) {
            this.bottomblock.update(data);
        }
    }

    public void onloadScene() {
        this.gtl.start(this.blockpro);
        this.gtl.loadTextures();
        createUpdateHandler();
        onloadSceneObjects();
        this.engine.enableAccelerometerSensor(this.blockpro, this);
    }

    /* access modifiers changed from: protected */
    public void onloadSceneObjects() {
        this.bodies = new ArrayList();
        this.mPhysicsWorld = new FixedStepPhysicsWorld(60, new Vector2(0.0f, 0.0f), false, 8, 15);
        this.mPhysicsWorld.setContinuousPhysics(false);
        this.levelloader = new LevelLoader(this, this.mPhysicsWorld, this.gtl, this.blockpro);
        resetStats();
        this.levelloader.loadWaitingBodiesOnLevel(this.levelToLoad);
        createAndAddStaticSprites();
        createAndAddTexts();
        this.bottomblock = this.levelloader.loadBottomBlock(this.levelToLoad);
        this.steadybar = new SteadyBar(this, this.blockpro);
    }

    public void onPause() {
        this.gamemenues.startMenu();
        this.engine.disableAccelerometerSensor(this.blockpro);
    }

    public void onResume() {
        this.engine.enableAccelerometerSensor(this.blockpro, this);
    }

    public boolean onSceneTouchEvent(Scene tmpscene, TouchEvent sceneTouchEvent) {
        if (sceneTouchEvent.getAction() == 1) {
            Log.i("touch!", "touch_inside_lvl");
        }
        return true;
    }

    public void resetScene() {
        if (!this.statDead || hasWon()) {
            Stat.getInstance().resetLevel(this.levelToLoad, Const.selectedWorld);
        }
        super.resetScene();
    }

    private void resetStats() {
        this.statBodies = 0;
        this.levelloader.statMaxBodies = 0;
        this.statMovement = 0;
        this.statRotation = 0;
        this.statDead = false;
        this.okToWin = false;
        this.blocksIsStable = false;
        this.statDead = false;
        this.statHasWon = false;
        this.winDelay = 0;
        this.startedTimer = false;
        this.levelToLoadText = 91239;
        this.statBodiesText = 12381;
        this.startDelay = 0;
    }

    public void setWon() {
        this.statHasWon = true;
    }

    public void showRules() {
        this.blockpro.runOnUiThread(new Runnable() {
            public void run() {
                new AlertDialog.Builder(LevelScene.this.blockpro).setTitle("Story mode rules").setIcon(17301569).setView(LayoutInflater.from(LevelScene.this.blockpro).inflate((int) R.layout.level_help_view, (ViewGroup) null)).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).show();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void startGame() {
        super.startGame();
        Const.log("justcomming from menu=" + Const.justCommingFromMenu);
        if (Const.justCommingFromMenu) {
            this.gamemenues = new GameMenues(this.blockpro, this.gtl, this);
            showStartMenu();
            return;
        }
        startPhysics();
    }

    /* access modifiers changed from: private */
    public void storeResult() {
        int storedhighscore = 0;
        switch (Const.selectedWorld) {
            case 0:
                storedhighscore = Const.getWorld1Level(this.blockpro);
                break;
            case 1:
                storedhighscore = Const.getWorld2Level(this.blockpro);
                break;
        }
        if (this.levelToLoad + 1 > storedhighscore) {
            switch (Const.selectedWorld) {
                case 0:
                    Const.setWorld1Level(this.blockpro, this.levelToLoad);
                    return;
                case 1:
                    Const.setWorld2Level(this.blockpro, this.levelToLoad);
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void tryToAddFace() {
        if (this.startDelay < 20) {
            this.startDelay++;
        } else if (!this.levelloader.waitingBodies.isEmpty()) {
            Box box = this.levelloader.waitingBodies.get(0);
            int i = box.releaseTime - 1;
            box.releaseTime = i;
            if (i <= 0) {
                Box cb = this.levelloader.waitingBodies.remove(0);
                this.bodies.add(cb);
                cb.activate();
                this.statBodies++;
            }
        } else if (this.winDelay > 10) {
            this.okToWin = true;
        } else {
            this.winDelay++;
        }
    }

    public void unload() {
        unloadChildrenAndObjects();
        this.gtl.unloadTextures();
        this.background.unload();
        this.background = null;
        this.bottomblock.unload();
        this.bottomblock = null;
        if (this.steadybar != null) {
            this.steadybar.unload();
            this.steadybar = null;
        }
        this.bodies = null;
        this.levelloader.waitingBodies = null;
    }

    /* access modifiers changed from: private */
    public void updateStats() {
        if (this.okToWin) {
            float maxmovement = 0.0f;
            float maxrotation = 0.0f;
            for (int n = 0; n < this.bodies.size(); n++) {
                Box box = (Box) this.bodies.get(n);
                maxmovement = Math.max(maxmovement, Math.abs(box.body.getLinearVelocity().y));
                maxrotation = Math.max(maxrotation, box.body.getAngularVelocity());
            }
            this.statMovement = Math.round(10.0f * maxmovement);
            this.statRotation = Math.round(200.0f * maxrotation);
            this.blocksIsStable = this.statMovement < 20 && this.statRotation < 70;
        }
        for (int n2 = 0; n2 < this.bodies.size(); n2++) {
            if (((Box) this.bodies.get(n2)).isDead) {
                this.statDead = true;
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateText() {
        if (this.levelToLoadText != this.levelToLoad) {
            this.textLevel.setText("Level: " + this.levelToLoad + "/" + this.levelloader.nrOfLevels);
            this.levelToLoadText = this.levelToLoad;
        }
        if (this.statBodies != this.statBodiesText) {
            this.textBlock.setText("Blocks: " + this.statBodies + "/" + this.levelloader.statMaxBodies);
            this.statBodiesText = this.statBodies;
        }
    }

    /* access modifiers changed from: private */
    public void updateWinState() {
        if (this.blocksIsStable) {
            this.steadybar.start();
        }
    }
}
