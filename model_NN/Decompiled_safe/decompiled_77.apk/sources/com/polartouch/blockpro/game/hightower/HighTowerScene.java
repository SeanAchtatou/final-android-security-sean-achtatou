package com.polartouch.blockpro.game.hightower;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;
import com.badlogic.gdx.math.Vector2;
import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.Const;
import com.polartouch.blockpro.FontLoader;
import com.polartouch.blockpro.R;
import com.polartouch.blockpro.backgrounds.WaterBackground;
import com.polartouch.blockpro.boxes.Box;
import com.polartouch.blockpro.boxes.StickyBox;
import com.polartouch.blockpro.game.GameMenues;
import com.polartouch.blockpro.game.GameScene;
import com.polartouch.blockpro.game.GameTextureLoader;
import java.util.ArrayList;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.entity.particle.ParticleSystem;
import org.anddev.andengine.entity.particle.emitter.PointParticleEmitter;
import org.anddev.andengine.entity.particle.initializer.AlphaInitializer;
import org.anddev.andengine.entity.particle.initializer.ColorInitializer;
import org.anddev.andengine.entity.particle.initializer.RotationInitializer;
import org.anddev.andengine.entity.particle.initializer.VelocityInitializer;
import org.anddev.andengine.entity.particle.modifier.AlphaModifier;
import org.anddev.andengine.entity.particle.modifier.ColorModifier;
import org.anddev.andengine.entity.particle.modifier.ExpireModifier;
import org.anddev.andengine.entity.particle.modifier.ScaleModifier;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.vertex.TextVertexBuffer;
import org.anddev.andengine.sensor.accelerometer.AccelerometerData;
import org.anddev.andengine.util.constants.TimeConstants;

public class HighTowerScene extends GameScene implements Scene.IOnSceneTouchListener {
    public static int STATUS_BLINK = 2;
    public static int STATUS_BLINK_WAIT = 3;
    public static int STATUS_FALLDOWN = 4;
    public static int STATUS_SCROLLUP = 1;
    public static int STATUS_WAIT = 0;
    public static int status;
    /* access modifiers changed from: private */
    public StickyBox boxToFollow;
    private int extraLifes;
    private final FontLoader fl;
    private HighTowerLoader htl;
    private Sprite hudSprite;
    final String[] levelDesc = {"", "Lets tilt them a bit", "Can you handle rotation?", "Some more tilt..", "Now both rotation and tilt!", "Tilt and even more rotation!", "Some wind from the side!", "You are getting high now", "Spooky!", "Final level! How long can you last?"};
    private PointParticleEmitter particleEmitter;
    private ParticleSystem particleSystem;
    private TimerHandler scrollUpdateHandler;
    /* access modifiers changed from: private */
    public boolean scrollupStarted;
    private int statBodies;
    private int statBodiesText;
    private boolean statDead;
    private int statHighScore;
    /* access modifiers changed from: private */
    public int statLevel;
    private int statLevelText;
    /* access modifiers changed from: private */
    public int statLife;
    private int statLifeText;
    private int statScore;
    private int statScoreText;
    private ChangeableText textBlock;
    private ChangeableText textHighScore;
    private ChangeableText textLevel;
    private ChangeableText textLife;
    private ChangeableText textScore;
    private boolean usesParticle = false;
    private int waittimer;

    public HighTowerScene(int mLayerCount, BlockPro bp) {
        super(mLayerCount);
        this.gtl = GameTextureLoader.getInstance();
        this.engine = bp.getEngine();
        this.blockpro = bp;
        setOnSceneTouchListener(this);
        this.fl = bp.fontloader;
    }

    private void createAndAddStaticSprites() {
        if (this.background == null) {
            this.background = new WaterBackground(this.blockpro, this);
        }
        this.hudSprite = new Sprite(0.0f, 0.0f, this.gtl.trHud);
        this.hudSprite.setHeight(180.0f);
        getChild(5).attachChild(this.hudSprite);
    }

    private void createAndAddTexts() {
        if (this.textLife == null) {
            this.textLife = new ChangeableText(360.0f, 20.0f, this.fl.mFont, "", 7);
            this.textLife.setText("0123456789");
            this.textLife.setText(String.valueOf(this.statLife) + "/" + 3);
            this.textBlock = new ChangeableText(130.0f, 70.0f, this.fl.mFont, "", 3);
            this.textBlock.setText("0123456789");
            this.textBlock.setText("0");
            this.textLevel = new ChangeableText(130.0f, 120.0f, this.fl.mFont, "", 2);
            this.textLevel.setText("0123456789");
            this.textLevel.setText("1");
            this.textScore = new ChangeableText(360.0f, 70.0f, this.fl.mFont, "", 7);
            this.textScore.setText("0123456789");
            this.textScore.setText("0");
            this.textHighScore = new ChangeableText(360.0f, 120.0f, this.fl.mFont, "", 7);
            this.textHighScore.setText("0123456789");
            this.textHighScore.setText(new StringBuilder().append(this.statHighScore).toString());
        }
        ChangeableText textTitle = new ChangeableText(20.0f, 20.0f, this.fl.mFont, "High Tower", 10);
        ChangeableText textLifeText = new ChangeableText(210.0f, 20.0f, this.fl.mFont, "Life:", 5);
        ChangeableText textBlockText = new ChangeableText(20.0f, 70.0f, this.fl.mFont, "Blocks:", 7);
        ChangeableText textLevelText = new ChangeableText(20.0f, 120.0f, this.fl.mFont, "Level:", 6);
        ChangeableText textScoreText = new ChangeableText(210.0f, 70.0f, this.fl.mFont, "Score:", 6);
        ChangeableText textHighScoreText = new ChangeableText(210.0f, 120.0f, this.fl.mFont, "Highscore:", 10);
        this.hudSprite.attachChild(textTitle);
        this.hudSprite.attachChild(this.textLife);
        this.hudSprite.attachChild(textLifeText);
        this.hudSprite.attachChild(this.textBlock);
        this.hudSprite.attachChild(textBlockText);
        this.hudSprite.attachChild(this.textLevel);
        this.hudSprite.attachChild(textLevelText);
        this.hudSprite.attachChild(this.textScore);
        this.hudSprite.attachChild(textScoreText);
        this.hudSprite.attachChild(this.textHighScore);
        this.hudSprite.attachChild(textHighScoreText);
    }

    private void createParticleEffect(Scene scene, TextureRegion particleTR) {
        this.particleEmitter = new PointParticleEmitter(-100.0f, -100.0f);
        this.particleSystem = new ParticleSystem(this.particleEmitter, 2.0f, 4.0f, 10, particleTR);
        this.particleSystem.addParticleInitializer(new ColorInitializer(1.0f, 1.0f, 1.0f));
        this.particleSystem.addParticleInitializer(new AlphaInitializer(0.0f));
        this.particleSystem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 1);
        this.particleSystem.addParticleInitializer(new VelocityInitializer(-8.0f, 8.0f, -20.0f, -40.0f));
        this.particleSystem.addParticleInitializer(new RotationInitializer(0.0f));
        this.particleSystem.addParticleModifier(new ScaleModifier(0.4f, 1.0f, 0.0f, 3.0f));
        this.particleSystem.addParticleModifier(new ColorModifier(1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 3.0f));
        this.particleSystem.addParticleModifier(new AlphaModifier(0.0f, 0.2f, 0.0f, 0.2f));
        this.particleSystem.addParticleModifier(new AlphaModifier(0.2f, 0.0f, 1.0f, 3.0f));
        this.particleSystem.addParticleModifier(new ExpireModifier(3.0f));
        this.particleSystem.setParticlesSpawnEnabled(false);
        scene.getChild(2).attachChild(this.particleSystem);
    }

    private void createScrollUpdateHandler() {
        this.scrollUpdateHandler = new TimerHandler(0.0133f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                float cameraY = HighTowerScene.this.blockpro.getCamera().getCenterY() - 400.0f;
                if (!HighTowerScene.this.scrollupStarted) {
                    return;
                }
                if (HighTowerScene.this.boxToFollow.face.getY() < 200.0f + cameraY) {
                    HighTowerScene.this.blockpro.getCamera().setCenter(0.0f, (cameraY - (2.0f * ((float) (((HighTowerScene.this.statLevel - 1) / 5) + 1)))) + 400.0f);
                    HighTowerScene.this.onCameraUpdatePrivate();
                    return;
                }
                HighTowerScene.this.scrollupStarted = false;
                HighTowerScene.status = HighTowerScene.STATUS_BLINK;
                HighTowerScene.this.boxToFollow.isBlinking = true;
            }
        });
    }

    private void createUpdateHandler() {
        this.updateHandler = new TimerHandler(0.1f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                if (!HighTowerScene.this.pausedGame) {
                    HighTowerScene.this.startedTimer = true;
                    HighTowerScene.this.updateStats();
                    HighTowerScene.this.gameLoop();
                    HighTowerScene.this.updateStableStatus();
                    if (HighTowerScene.this.hasLost()) {
                        HighTowerScene.this.stopHandlers();
                        HighTowerScene.this.setChildScene(HighTowerScene.this.gamemenues.finishedScene, false, true, true);
                    }
                    HighTowerScene.this.updateText();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void gameLoop() {
        if (status == STATUS_WAIT) {
            int i = this.waittimer;
            this.waittimer = i + 1;
            if (i > 10) {
                this.waittimer = 0;
                status = STATUS_SCROLLUP;
                this.scrollupStarted = false;
                this.statBodies++;
                this.statLevel = Math.min((int) (Math.floor((double) (this.statBodies / 10)) + 1.0d), 10);
                if (this.statBodies % 10 == 0 && this.statLevel >= 2 && this.statBodies <= 91) {
                    showDescToast();
                    if (this.statLife < 3) {
                        this.extraLifes++;
                        showAddLifeToast();
                    }
                }
                StickyBox box = getBox(this.statLevel);
                this.bodies.add(box);
                box.activate();
                this.boxToFollow = box;
            }
        } else if (status == STATUS_SCROLLUP) {
            if (!this.scrollupStarted) {
                this.scrollupStarted = true;
                registerUpdateHandler(this.scrollUpdateHandler);
            }
        } else if (status == STATUS_BLINK) {
            unregisterUpdateHandler(this.scrollUpdateHandler);
            status = STATUS_BLINK_WAIT;
        } else if (status == STATUS_BLINK_WAIT) {
            if (!this.boxToFollow.isBlinking) {
                if (this.usesParticle) {
                    this.boxToFollow.setParticleEffect(this.particleEmitter, this.particleSystem, (float) this.gtl.bubble.getWidth(), (float) this.gtl.bubble.getHeight());
                }
                status = STATUS_FALLDOWN;
            }
        } else if (status == STATUS_FALLDOWN) {
            this.boxToFollow.isNotStarted = false;
            if (this.boxToFollow.isStable() || this.boxToFollow.isDead) {
                status = STATUS_WAIT;
                if (this.usesParticle) {
                    this.boxToFollow.stopParticleEffect();
                }
                if (this.boxToFollow.isDead) {
                    this.statBodies--;
                    showLoseLifeToast();
                    Const.vibrate(70, this.blockpro);
                    updateScore(-this.boxToFollow.statValue);
                    return;
                }
                updateScore(Math.round(((((float) this.boxToFollow.statValue) * (this.boxToFollow.face.getY() - 800.0f)) / (StickyBox.highestStableBlock - 800.0f)) * ((((-this.boxToFollow.face.getY()) + 800.0f) / 800.0f) + 1.0f)));
            }
        }
    }

    private StickyBox getBox(int statLevel2) {
        float til = 0.0f;
        float rot = 0.0f;
        float vx = 0.0f;
        switch (statLevel2) {
            case 1:
                break;
            case 2:
                til = 0.4f;
                break;
            case 3:
                rot = 7.0f;
                break;
            case 4:
                til = 0.7f;
                break;
            case 5:
                til = 1.0f;
                rot = 40.0f;
                break;
            case TextVertexBuffer.VERTICES_PER_CHARACTER /*6*/:
                til = 1.0f;
                rot = 80.0f;
                break;
            case TimeConstants.DAYSPERWEEK /*7*/:
                vx = 1.0f;
                rot = 50.0f;
                break;
            case 8:
                vx = 2.0f;
                til = 1.0f;
                rot = 100.0f;
                break;
            case 9:
                vx = -6.0f;
                rot = 100.0f;
                til = 1.0f;
                break;
            default:
                vx = 10.0f;
                rot = 150.0f;
                til = 1.0f;
                break;
        }
        StickyBox sb = this.htl.getABox(til, rot, vx);
        if (this.statBodies == 1) {
            sb.resetHighestBlock();
        }
        return sb.build(this.bottomblock, this.blockpro);
    }

    public void goToNextLevel() {
    }

    /* access modifiers changed from: protected */
    public boolean hasLost() {
        return this.statDead;
    }

    /* access modifiers changed from: protected */
    public boolean hasWon() {
        return false;
    }

    public void onAccelerometerChanged(AccelerometerData data) {
        if (this.htl != null && this.bottomblock != null && !this.pausedGame) {
            this.bottomblock.update(data);
        }
    }

    public void onCameraUpdate() {
        if (status == STATUS_FALLDOWN) {
            onCameraUpdatePrivate();
        }
    }

    /* access modifiers changed from: private */
    public void onCameraUpdatePrivate() {
        float y = this.blockpro.getCamera().getMinY();
        this.hudSprite.setPosition(0.0f, y);
        this.background.updateSpritePositions(y);
    }

    public void onloadScene() {
        this.gtl.start(this.blockpro);
        this.gtl.loadTextures();
        this.gamemenues = new GameMenues(this.blockpro, this.gtl, this);
        createUpdateHandler();
        onloadSceneObjects();
        this.engine.enableAccelerometerSensor(this.blockpro, this);
    }

    /* access modifiers changed from: protected */
    public void onloadSceneObjects() {
        this.blockpro.activateUpdateCamera();
        this.bodies = new ArrayList();
        this.mPhysicsWorld = new FixedStepPhysicsWorld(60, new Vector2(0.0f, 0.0f), false, 8, 15);
        this.mPhysicsWorld.setContinuousPhysics(false);
        this.htl = new HighTowerLoader(this, this.mPhysicsWorld, this.gtl, this.blockpro);
        resetStats();
        createAndAddStaticSprites();
        createAndAddTexts();
        this.bottomblock = this.htl.getBottomBlock();
        createScrollUpdateHandler();
        this.usesParticle = Const.getUseParticles(this.blockpro);
        if (this.usesParticle) {
            createParticleEffect(this, this.gtl.bubble);
        }
        onCameraUpdatePrivate();
    }

    public void onPause() {
        this.gamemenues.startMenu();
        this.engine.disableAccelerometerSensor(this.blockpro);
    }

    public void onResume() {
        this.engine.enableAccelerometerSensor(this.blockpro, this);
    }

    public boolean onSceneTouchEvent(Scene tmpscene, TouchEvent sceneTouchEvent) {
        if (sceneTouchEvent.getAction() == 1) {
            Log.i("touch!", "touch_inside_lvl");
        }
        return true;
    }

    private void resetStats() {
        this.statBodies = 0;
        this.statDead = false;
        this.startedTimer = false;
        this.statBodiesText = 12381;
        this.statScore = 0;
        this.extraLifes = 0;
        this.statHighScore = Const.getHighTowerScore(this.blockpro);
        this.statLife = 3;
        this.scrollupStarted = false;
        status = STATUS_WAIT;
        this.statLevel = 1;
    }

    public void setWon() {
    }

    private void showAddLifeToast() {
        this.blockpro.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(HighTowerScene.this.blockpro, "+1 Life!", 0).show();
            }
        });
    }

    private void showDescToast() {
        this.blockpro.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(HighTowerScene.this.blockpro, "Level " + HighTowerScene.this.statLevel + " - " + HighTowerScene.this.levelDesc[HighTowerScene.this.statLevel - 1], 1).show();
            }
        });
    }

    private void showLoseLifeToast() {
        this.blockpro.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(HighTowerScene.this.blockpro, "Block lost, " + HighTowerScene.this.statLife + " lives left!", 1).show();
            }
        });
    }

    public void showRules() {
        this.blockpro.runOnUiThread(new Runnable() {
            public void run() {
                new AlertDialog.Builder(HighTowerScene.this.blockpro).setTitle("High Tower rules").setIcon(17301569).setView(LayoutInflater.from(HighTowerScene.this.blockpro).inflate((int) R.layout.hightower_help_view, (ViewGroup) null)).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).show();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void startGame() {
        super.startGame();
        onCameraUpdatePrivate();
        showStartMenu();
    }

    public void unload() {
        this.blockpro.deactivateUpdateCamera();
        unloadChildrenAndObjects();
        unregisterUpdateHandler(this.scrollUpdateHandler);
        this.gtl.unloadTextures();
        this.background.unload();
        this.background = null;
        this.bottomblock.unload();
        this.bottomblock = null;
        this.bodies = null;
    }

    /* access modifiers changed from: protected */
    public void unloadRestOfChildrenAndObjects() {
        super.unloadRestOfChildrenAndObjects();
        if (this.boxToFollow != null) {
            this.boxToFollow.stopParticleEffect();
            this.boxToFollow = null;
        }
    }

    public void updateOnBBlockMove(Vector2 vTemp) {
        for (int i = 0; i < this.bodies.size(); i++) {
            if (((StickyBox) this.bodies.get(i)).isStable()) {
                ((Box) this.bodies.get(i)).body.setLinearVelocity(vTemp);
            }
        }
    }

    private void updateScore(int score) {
        this.statScore += score;
        if (this.statScore != this.statScoreText) {
            this.textScore.setText(new StringBuilder().append(this.statScore).toString());
            this.statScoreText = this.statScore;
        }
        if (this.statScore > this.statHighScore) {
            this.statHighScore = this.statScore;
            this.textHighScore.setText(new StringBuilder().append(this.statHighScore).toString());
            Const.setHighTowerScore(this.blockpro, this.statScore);
        }
    }

    /* access modifiers changed from: private */
    public void updateStableStatus() {
        if (this.bodies != null) {
            this.bodies.size();
        }
    }

    /* access modifiers changed from: private */
    public void updateStats() {
        int nrDead = 0;
        for (int n = 0; n < this.bodies.size(); n++) {
            if (((Box) this.bodies.get(n)).isDead) {
                nrDead++;
            }
        }
        this.statLife = (3 - nrDead) + this.extraLifes;
        if (this.statLife <= 0) {
            this.statDead = true;
        }
    }

    /* access modifiers changed from: private */
    public void updateText() {
        if (this.statBodies != this.statBodiesText) {
            this.textBlock.setText(new StringBuilder().append(this.statBodies).toString());
            this.statBodiesText = this.statBodies;
        }
        if (this.statLevel != this.statLevelText) {
            this.textLevel.setText(new StringBuilder().append(this.statLevel).toString());
            this.statLevelText = this.statLevel;
        }
        if (this.statLife != this.statLifeText) {
            this.textLife.setText(String.valueOf(this.statLife) + "/" + 3);
            this.statLifeText = this.statLife;
        }
    }
}
