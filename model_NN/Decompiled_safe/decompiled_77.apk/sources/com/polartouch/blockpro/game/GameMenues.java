package com.polartouch.blockpro.game;

import com.flurry.android.FlurryAgent;
import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.Const;
import com.polartouch.blockpro.FontLoader;
import org.anddev.andengine.entity.scene.menu.MenuScene;
import org.anddev.andengine.entity.scene.menu.item.IMenuItem;
import org.anddev.andengine.entity.scene.menu.item.TextMenuItem;
import org.anddev.andengine.entity.shape.Shape;

public class GameMenues implements MenuScene.IOnMenuItemClickListener {
    private static final int NEXTLEVEL = 3;
    private static final int NONE = 0;
    private static final int QUIT = 2;
    private static final int RESTART = 1;
    private static final int RESUME = 4;
    private static final int RULES = 5;
    private static final int START = 6;
    private static final float margin = 80.0f;
    private final BlockPro blockpro;
    public MenuScene completeMenuScene = createCompleteMenuScene();
    public MenuScene deadMenuScene = createDeadMenuScene();
    public MenuScene finishedScene = createFinishedMenuScene();
    private final FontLoader fl;
    private final GameScene gs;
    public MenuScene menuScene = createMenuScene();
    public MenuScene startScene = createStartScene();

    public GameMenues(BlockPro bp, GameTextureLoader ltl, GameScene gs2) {
        this.blockpro = bp;
        this.gs = gs2;
        this.fl = bp.fontloader;
    }

    /* access modifiers changed from: protected */
    public MenuScene createCompleteMenuScene() {
        MenuScene ms = new MenuScene(this.blockpro.getCamera());
        ms.addMenuItem(createTitleMenuItem("LEVEL COMPLETED!"));
        ms.addMenuItem(createNextLevelMenuItem());
        ms.addMenuItem(createResetMenuItem());
        ms.addMenuItem(createQuitMenuItem());
        ms.buildAnimations();
        ms.setBackgroundEnabled(false);
        ms.setOnMenuItemClickListener(this);
        return ms;
    }

    /* access modifiers changed from: protected */
    public MenuScene createDeadMenuScene() {
        MenuScene ms = new MenuScene(this.blockpro.getCamera());
        ms.addMenuItem(createTitleMenuItem("FAILED"));
        ms.addMenuItem(createResetMenuItem());
        ms.addMenuItem(createQuitMenuItem());
        ms.buildAnimations();
        ms.setBackgroundEnabled(false);
        ms.setOnMenuItemClickListener(this);
        return ms;
    }

    /* access modifiers changed from: protected */
    public MenuScene createFinishedMenuScene() {
        MenuScene ms = new MenuScene(this.blockpro.getCamera());
        ms.addMenuItem(createTitleMenuItem("FINISHED"));
        ms.addMenuItem(createResetMenuItem());
        ms.addMenuItem(createQuitMenuItem());
        ms.buildAnimations();
        ms.setBackgroundEnabled(false);
        ms.setOnMenuItemClickListener(this);
        return ms;
    }

    /* access modifiers changed from: protected */
    public MenuScene createMenuScene() {
        MenuScene ms = new MenuScene(this.blockpro.getCamera());
        ms.addMenuItem(createTitleMenuItem("PAUSED"));
        ms.addMenuItem(createResumeMenuItem());
        ms.addMenuItem(createRulesMenuItem());
        ms.addMenuItem(createResetMenuItem());
        ms.addMenuItem(createQuitMenuItem());
        ms.buildAnimations();
        ms.setBackgroundEnabled(false);
        ms.setOnMenuItemClickListener(this);
        return ms;
    }

    private TextMenuItem createNextLevelMenuItem() {
        TextMenuItem nextMenuItem = new TextMenuItem(3, this.fl.mMenuFont2, "NEXT LEVEL");
        nextMenuItem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        nextMenuItem.setHeight(margin);
        return nextMenuItem;
    }

    private TextMenuItem createQuitMenuItem() {
        TextMenuItem quitMenuItem = new TextMenuItem(2, this.fl.mMenuFont2, "QUIT");
        quitMenuItem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        quitMenuItem.setHeight(margin);
        return quitMenuItem;
    }

    private TextMenuItem createResetMenuItem() {
        TextMenuItem resetMenuItem = new TextMenuItem(1, this.fl.mMenuFont2, "RESTART");
        resetMenuItem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        resetMenuItem.setHeight(margin);
        return resetMenuItem;
    }

    private TextMenuItem createResumeMenuItem() {
        TextMenuItem resumeMenuItem = new TextMenuItem(4, this.fl.mMenuFont2, "RESUME");
        resumeMenuItem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        resumeMenuItem.setHeight(margin);
        return resumeMenuItem;
    }

    private TextMenuItem createRulesMenuItem() {
        TextMenuItem quitMenuItem = new TextMenuItem(5, this.fl.mMenuFont2, "RULES");
        quitMenuItem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        quitMenuItem.setHeight(margin);
        return quitMenuItem;
    }

    private TextMenuItem createStartMenuItem() {
        TextMenuItem menuItem = new TextMenuItem(6, this.fl.mMenuFont2, "START GAME");
        menuItem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        menuItem.setHeight(margin);
        return menuItem;
    }

    /* access modifiers changed from: protected */
    public MenuScene createStartScene() {
        MenuScene ms = new MenuScene(this.blockpro.getCamera());
        ms.addMenuItem(createTitleMenuItem("welcome"));
        ms.addMenuItem(createStartMenuItem());
        ms.addMenuItem(createRulesMenuItem());
        ms.addMenuItem(createQuitMenuItem());
        ms.buildAnimations();
        ms.setBackgroundEnabled(false);
        ms.setOnMenuItemClickListener(this);
        return ms;
    }

    private TextMenuItem createTitleMenuItem(String text) {
        TextMenuItem titleMenuItem = new TextMenuItem(0, this.fl.mMenuFont, "-- " + text + " --");
        titleMenuItem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        titleMenuItem.setHeight(56.0f);
        return titleMenuItem;
    }

    public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem, float pMenuItemLocalX, float pMenuItemLocalY) {
        pMenuItem.setColor(0.5f, 0.0f, 0.0f);
        Const.vibrate(30, this.blockpro);
        switch (pMenuItem.getID()) {
            case 1:
                this.menuScene.back();
                this.finishedScene.back();
                this.deadMenuScene.back();
                this.completeMenuScene.back();
                this.gs.resetScene();
                break;
            case 2:
                this.gs.showExitWarningDialog(this.blockpro);
                break;
            case 3:
                this.completeMenuScene.back();
                this.gs.goToNextLevel();
                break;
            case 4:
                stopMenu();
                break;
            case 5:
                this.gs.showRules();
                FlurryAgent.onEvent("Show Rules");
                break;
            case 6:
                this.startScene.back();
                this.gs.startPhysics();
                break;
        }
        pMenuItem.setColor(1.0f, 1.0f, 1.0f);
        return true;
    }

    public void startMenu() {
        this.gs.stopHandlers();
        this.gs.setChildScene(this.menuScene, false, true, true);
    }

    public void stopMenu() {
        this.menuScene.back();
        this.gs.startHandlers();
    }
}
