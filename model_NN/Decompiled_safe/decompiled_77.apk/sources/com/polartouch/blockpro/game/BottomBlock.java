package com.polartouch.blockpro.game;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.Const;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;
import org.anddev.andengine.extension.physics.box2d.util.Vector2Pool;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.sensor.accelerometer.AccelerometerData;

public class BottomBlock {
    static final int maxXValue = 6;
    private static int rotation;
    private final Texture bottomBlockTexture = new Texture(512, 64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
    private Body boxBody;
    private Sprite boxFace;
    private TextureRegion boxTR;
    private final BlockPro bp;
    private final GameScene gamescene;
    private int height;
    private float oldV = 0.0f;
    private float pBlue = 1.0f;
    private float pGreen = 1.0f;
    private float pRed = 1.0f;
    private final PhysicsWorld pw;
    public float scale;
    private final float sensativity;

    public BottomBlock(GameScene gamescene2, PhysicsWorld pw2, BlockPro bp2) {
        this.boxTR = TextureRegionFactory.createFromAsset(this.bottomBlockTexture, bp2, "bottomblock1_267_47.png", 0, 0);
        bp2.getEngine().getTextureManager().loadTextures(this.bottomBlockTexture);
        this.bp = bp2;
        this.gamescene = gamescene2;
        this.pw = pw2;
        height(15);
        this.scale = 1.0f;
        this.sensativity = (((float) Const.getSensitivity(bp2)) * 1.4f) + 1.0f;
        rotation = Const.getScreenRotation();
    }

    public BottomBlock build() {
        this.boxFace = new Sprite(((float) (Const.CAMERA_WIDTH - this.boxTR.getWidth())) * 0.5f, (float) (Const.CAMERA_HEIGHT - this.height), this.boxTR);
        this.boxFace.setScale(this.scale / 2.0f);
        this.boxFace.setColor(this.pRed, this.pGreen, this.pBlue);
        this.boxBody = PhysicsFactory.createBoxBody(this.pw, this.boxFace, BodyDef.BodyType.KinematicBody, PhysicsFactory.createFixtureDef(10.0f, 0.0f, 10.0f));
        this.gamescene.getChild(3).attachChild(this.boxFace);
        this.pw.registerPhysicsConnector(new PhysicsConnector(this.boxFace, this.boxBody, true, true));
        return this;
    }

    private float getSpeedFromData(AccelerometerData data) {
        switch (rotation) {
            case 1:
                return data.getY() * this.sensativity;
            case 2:
                return data.getX() * this.sensativity;
            case 3:
                return (-data.getY()) * this.sensativity;
            default:
                return (-data.getX()) * this.sensativity;
        }
    }

    public float getX() {
        return this.boxFace.getX();
    }

    public BottomBlock height(int heightPercent) {
        this.height = Math.round((float) ((heightPercent * Const.CAMERA_HEIGHT) / 100));
        return this;
    }

    public BottomBlock image(TextureRegion tr) {
        this.boxTR = tr;
        return this;
    }

    public Boolean isLoadedToHardware() {
        return Boolean.valueOf(this.bottomBlockTexture.isLoadedToHardware());
    }

    public void reset() {
        build();
    }

    public BottomBlock scale(float scale2) {
        this.scale = scale2;
        return this;
    }

    public BottomBlock setColor(float pRed2, float pGreen2, float pBlue2) {
        this.pRed = pRed2;
        this.pGreen = pGreen2;
        this.pBlue = pBlue2;
        return this;
    }

    public void unload() {
        this.bp.getEngine().getTextureManager().unloadTextures(this.bottomBlockTexture);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void update(AccelerometerData data) {
        if (this.boxBody != null && this.gamescene.startedTimer) {
            Vector2 vTemp = Vector2Pool.obtain(0.0f, 0.0f);
            vTemp.x = getSpeedFromData(data);
            float currX = this.boxBody.getWorldCenter().x * 32.0f;
            float halfWidth = this.boxFace.getWidthScaled() * 0.5f;
            if (currX - halfWidth < 0.0f && vTemp.x < 0.0f) {
                vTemp.x = 0.0f;
            } else if (currX + halfWidth > 480.0f && vTemp.x > 0.0f) {
                vTemp.x = 0.0f;
            }
            vTemp.x = Math.max(-6.0f, Math.min(6.0f, vTemp.x));
            if (vTemp.x != this.oldV) {
                this.boxBody.setLinearVelocity(vTemp);
                this.oldV = vTemp.x;
                this.gamescene.updateOnBBlockMove(vTemp);
            }
            Vector2Pool.recycle(vTemp);
        }
    }
}
