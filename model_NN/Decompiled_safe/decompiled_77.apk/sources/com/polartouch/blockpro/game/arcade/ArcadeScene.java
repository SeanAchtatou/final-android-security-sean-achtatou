package com.polartouch.blockpro.game.arcade;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.badlogic.gdx.math.Vector2;
import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.FontLoader;
import com.polartouch.blockpro.R;
import com.polartouch.blockpro.backgrounds.ArcadeBackground;
import com.polartouch.blockpro.boxes.ArcadeBox;
import com.polartouch.blockpro.game.GameMenues;
import com.polartouch.blockpro.game.GameScene;
import com.polartouch.blockpro.game.GameTextureLoader;
import java.util.ArrayList;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.sensor.accelerometer.AccelerometerData;

public class ArcadeScene extends GameScene implements Scene.IOnSceneTouchListener {
    private ArcadeLoader arcadeloader;
    private boolean blocksIsStable;
    private final FontLoader fl;
    /* access modifiers changed from: private */
    public int maxBounces;
    private final int maxBouncesPerBlock = 3;
    private int statBodies;
    /* access modifiers changed from: private */
    public int statBounces;
    private boolean statDead;
    private int statMovement;
    private int statRotation;
    private int statScore;
    private ChangeableText textBlock;
    private ChangeableText textGravities;
    private ChangeableText textLevel;
    private ChangeableText textScore;

    public ArcadeScene(int mLayerCount, BlockPro bp) {
        super(mLayerCount);
        this.gtl = GameTextureLoader.getInstance();
        this.engine = bp.getEngine();
        this.blockpro = bp;
        setOnSceneTouchListener(this);
        this.fl = bp.fontloader;
    }

    private void createAndAddStaticSprites() {
        if (this.background == null) {
            this.background = new ArcadeBackground(this.blockpro, this);
        }
        getChild(5).attachChild(new Sprite(0.0f, 0.0f, this.gtl.trHud));
    }

    private void createAndAddTexts() {
        this.textLevel = new ChangeableText(20.0f, 20.0f, this.fl.mFont, "Arcade Mode", "Arcade Mode".length());
        this.textBlock = new ChangeableText(20.0f, 70.0f, this.fl.mFont, "", "Blocks: XX/XX".length());
        this.textBlock.setText("0123456789");
        this.textBlock.setText("Blocks: 0");
        this.textScore = new ChangeableText(240.0f, 20.0f, this.fl.mFont, "", "Score: XXXX".length());
        this.textScore.setText("0123456789");
        this.textScore.setText("Score: 0");
        this.textGravities = new ChangeableText(240.0f, 70.0f, this.fl.mFont, "", "Gravities: X/X".length());
        this.textGravities.setText("0123456789");
        this.textGravities.setText("Gravities: 0/0");
        getChild(5).attachChild(this.textLevel);
        getChild(5).attachChild(this.textBlock);
        getChild(5).attachChild(this.textScore);
        getChild(5).attachChild(this.textGravities);
    }

    private void createUpdateHandler() {
        this.updateHandler = new TimerHandler(0.05f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                ArcadeScene.this.tryToAddFace();
                ArcadeScene.this.updateStats();
                if (ArcadeScene.this.statBounces >= ArcadeScene.this.maxBounces) {
                    ArcadeScene.this.setChildScene(ArcadeScene.this.gamemenues.finishedScene, false, true, true);
                }
                ArcadeScene.this.updateText();
                ArcadeScene.this.startedTimer = true;
            }
        });
    }

    public void goToNextLevel() {
    }

    /* access modifiers changed from: protected */
    public boolean hasLost() {
        return this.statDead;
    }

    /* access modifiers changed from: protected */
    public boolean hasWon() {
        return false;
    }

    public void onAccelerometerChanged(AccelerometerData data) {
        if (this.arcadeloader != null && this.bottomblock != null) {
            this.bottomblock.update(data);
        }
    }

    public void onloadScene() {
        this.gtl.start(this.blockpro);
        this.gtl.loadTextures();
        createUpdateHandler();
        this.gamemenues = new GameMenues(this.blockpro, this.gtl, this);
        onloadSceneObjects();
        this.engine.enableAccelerometerSensor(this.blockpro, this);
    }

    /* access modifiers changed from: protected */
    public void onloadSceneObjects() {
        this.bodies = new ArrayList();
        this.mPhysicsWorld = new FixedStepPhysicsWorld(60, new Vector2(0.0f, 0.0f), false, 8, 15);
        this.arcadeloader = new ArcadeLoader(this, this.mPhysicsWorld, this.gtl, this.blockpro);
        resetStats();
        createAndAddStaticSprites();
        createAndAddTexts();
        this.bottomblock = this.arcadeloader.getBottomBlock();
    }

    public void onPause() {
        this.gamemenues.startMenu();
        this.engine.disableAccelerometerSensor(this.blockpro);
    }

    public void onResume() {
        this.engine.enableAccelerometerSensor(this.blockpro, this);
    }

    public boolean onSceneTouchEvent(Scene tmpscene, TouchEvent sceneTouchEvent) {
        if (sceneTouchEvent.getAction() == 1) {
            Log.i("touch!", "touch_inside_lvl");
        }
        return true;
    }

    private void resetStats() {
        this.statBodies = 0;
        this.statMovement = 0;
        this.statRotation = 0;
        this.statDead = false;
        this.blocksIsStable = true;
        this.statDead = false;
        this.statBounces = 0;
        this.statScore = 0;
        this.startedTimer = false;
    }

    public void showRules() {
        this.blockpro.runOnUiThread(new Runnable() {
            public void run() {
                new AlertDialog.Builder(ArcadeScene.this.blockpro).setTitle("Arcade Mode rules").setIcon(17301569).setView(LayoutInflater.from(ArcadeScene.this.blockpro).inflate((int) R.layout.arcade_help_view, (ViewGroup) null)).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).show();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void startGame() {
        super.startGame();
        showStartMenu();
    }

    /* access modifiers changed from: private */
    public void tryToAddFace() {
        if (this.blocksIsStable) {
            this.statScore += 100;
            if (this.bodies.size() <= 1) {
                ArcadeBox box = this.arcadeloader.getABox();
                this.bodies.add(box);
                box.activate();
                ArcadeBox box2 = this.arcadeloader.getAUpBox();
                this.bodies.add(box2);
                box2.activate();
                this.statBodies += 2;
                return;
            }
            ArcadeBox box3 = this.arcadeloader.getARandomBox();
            this.bodies.add(box3);
            box3.activate();
            this.statBodies++;
            this.statScore += 100;
        }
    }

    public void unload() {
        unloadChildrenAndObjects();
        this.gtl.unloadTextures();
        this.background.unload();
        this.background = null;
        this.bottomblock.unload();
        this.bottomblock = null;
        this.bodies = null;
    }

    /* access modifiers changed from: private */
    public void updateStats() {
        boolean z;
        boolean z2;
        float maxmovement = 0.0f;
        float maxrotation = 0.0f;
        Boolean started = true;
        int totalBounces = 0;
        int numberOfStableBlocks = 0;
        for (int n = 0; n < this.bodies.size(); n++) {
            ArcadeBox box = (ArcadeBox) this.bodies.get(n);
            maxmovement = Math.max(maxmovement, box.body.getLinearVelocity().len());
            maxrotation = Math.max(maxrotation, box.body.getAngularVelocity());
            if (box.body.getWorldCenter().y * 32.0f < 20.0f) {
                started = false;
            }
            totalBounces += box.statBounces;
            if (box.body.getLinearVelocity().len() * 10.0f < 10.0f && box.body.getAngularVelocity() * 200.0f < 10.0f && started.booleanValue()) {
                numberOfStableBlocks++;
            }
        }
        this.statBounces = totalBounces;
        this.maxBounces = this.bodies.size() * 3;
        this.statMovement = Math.round(maxmovement * 10.0f);
        this.statRotation = Math.round(maxrotation * 200.0f);
        if (this.statMovement >= 10 || this.statRotation >= 10 || !started.booleanValue()) {
            z = false;
        } else {
            z = true;
        }
        this.blocksIsStable = z;
        if (numberOfStableBlocks + 1 >= this.bodies.size()) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.blocksIsStable = z2;
    }

    /* access modifiers changed from: private */
    public void updateText() {
        this.textBlock.setText("Blocks: " + this.statBodies);
        this.textScore.setText("Score: " + this.statScore);
        this.textGravities.setText("Bounces: " + this.statBounces + "/" + this.maxBounces);
    }
}
