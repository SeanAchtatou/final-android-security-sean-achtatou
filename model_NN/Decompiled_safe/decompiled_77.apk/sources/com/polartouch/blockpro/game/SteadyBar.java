package com.polartouch.blockpro.game;

import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.Const;
import com.polartouch.blockpro.game.level.LevelScene;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.ui.activity.BaseGameActivity;

public class SteadyBar {
    private final BaseGameActivity blockpro;
    private final Sprite innerFace;
    private final LevelScene levelscene;
    private final Sprite outerFace;
    private int progress;
    /* access modifiers changed from: private */
    public boolean running;
    private boolean startedOnce;
    public TextureRegion steadybarInner;
    public TextureRegion steadybarOuter;
    private final Texture steadybars = new Texture(512, 128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
    private TimerHandler updateHandler;
    private final int width;

    public SteadyBar(LevelScene levelscene2, BlockPro blockpro2) {
        this.blockpro = blockpro2;
        this.levelscene = levelscene2;
        this.steadybarInner = TextureRegionFactory.createFromAsset(this.steadybars, blockpro2, "steadybar_inner_387_49.png", 0, 0);
        this.steadybarOuter = TextureRegionFactory.createFromAsset(this.steadybars, blockpro2, "steadybar_outer_387_49.png", 0, 60);
        this.innerFace = new Sprite(1000.0f, 1000.0f, this.steadybarInner);
        this.outerFace = new Sprite(1000.0f, 1000.0f, this.steadybarOuter);
        this.width = (int) this.innerFace.getWidthScaled();
        resetLevel();
        Const.log("loaded steadybar");
        blockpro2.getEngine().getTextureManager().loadTextures(this.steadybars);
        createUpdateHandler();
    }

    private void createUpdateHandler() {
        this.updateHandler = new TimerHandler(0.05f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                if (SteadyBar.this.running) {
                    SteadyBar.this.decreaseLevel();
                }
            }
        });
    }

    public void decreaseLevel() {
        if (this.progress > 1) {
            int i = this.progress - 3;
            this.progress = i;
            setProgress(i);
            return;
        }
        this.levelscene.setWon();
    }

    public void hide() {
        this.innerFace.setAlpha(0.0f);
        this.outerFace.setAlpha(0.0f);
        this.innerFace.setPosition(1000.0f, 1000.0f);
        this.outerFace.setPosition(1000.0f, 1000.0f);
    }

    public void pause() {
        this.running = false;
    }

    public void resetLevel() {
        this.levelscene.getChild(5).attachChild(this.innerFace);
        this.levelscene.getChild(5).attachChild(this.outerFace);
        this.progress = this.width;
        this.running = false;
        hide();
    }

    public void resume() {
        if (this.startedOnce) {
            this.running = true;
        }
    }

    public void setProgress(int progress2) {
        this.innerFace.setWidth(((float) ((this.width * progress2) / this.width)) * 1.0f);
        this.innerFace.setColor(1.0f, 1.0f, 0.7f + ((0.3f * ((float) progress2)) / ((float) this.width)));
    }

    public void show() {
        int x = (Const.CAMERA_WIDTH - this.width) / 2;
        this.innerFace.setPosition((float) x, 200.0f);
        this.outerFace.setPosition((float) x, 200.0f);
        this.innerFace.setAlpha(1.0f);
        this.outerFace.setAlpha(0.7f);
    }

    public void start() {
        this.startedOnce = true;
        if (!this.running) {
            show();
            this.blockpro.getEngine().registerUpdateHandler(this.updateHandler);
            this.running = true;
        }
    }

    public void unload() {
        pause();
        Const.log("unloaded steadybar");
        this.blockpro.getEngine().unregisterUpdateHandler(this.updateHandler);
        this.blockpro.getEngine().getTextureManager().unloadTextures(this.steadybars);
    }
}
