package com.polartouch.blockpro.game;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.badlogic.gdx.math.Vector2;
import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.Const;
import com.polartouch.blockpro.CustomScene;
import com.polartouch.blockpro.SceneLoader;
import com.polartouch.blockpro.backgrounds.Background;
import com.polartouch.blockpro.boxes.Box;
import java.util.List;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.sensor.accelerometer.IAccelerometerListener;

public abstract class GameScene extends CustomScene implements IAccelerometerListener {
    protected Background background;
    /* access modifiers changed from: protected */
    public BlockPro blockpro;
    public List<Box> bodies;
    public BottomBlock bottomblock;
    protected Engine engine;
    /* access modifiers changed from: protected */
    public GameMenues gamemenues;
    protected GameTextureLoader gtl;
    protected FixedStepPhysicsWorld mPhysicsWorld;
    /* access modifiers changed from: protected */
    public boolean pausedGame = true;
    private TimerHandler startHandler;
    /* access modifiers changed from: private */
    public boolean started;
    public boolean startedTimer;
    protected SteadyBar steadybar;
    protected TimerHandler updateHandler;

    public abstract void goToNextLevel();

    /* access modifiers changed from: protected */
    public abstract boolean hasLost();

    /* access modifiers changed from: protected */
    public abstract boolean hasWon();

    public abstract void onPause();

    public abstract void onResume();

    public abstract void onloadScene();

    /* access modifiers changed from: protected */
    public abstract void onloadSceneObjects();

    public abstract void unload();

    public GameScene(int pLayerCount) {
        super(pLayerCount);
        createStartHandler();
    }

    /* access modifiers changed from: private */
    public Boolean allIsLoaded() {
        if (this.background != null && !this.background.isLoadedToMemory()) {
            return false;
        }
        if (!this.gtl.isLoadedToMemory()) {
            return false;
        }
        if (!this.bottomblock.isLoadedToHardware().booleanValue()) {
            return false;
        }
        return true;
    }

    public void createStartHandler() {
        this.startHandler = new TimerHandler(0.3f, true, new ITimerCallback() {
            int i = 0;

            public void onTimePassed(TimerHandler pTimerHandler) {
                this.i++;
                Const.log("loading..." + this.i + " " + GameScene.this.allIsLoaded() + " " + (!GameScene.this.started));
                if (GameScene.this.allIsLoaded().booleanValue() && !GameScene.this.started) {
                    GameScene.this.started = true;
                    GameScene.this.engine.setScene(this);
                    GameScene.this.startGame();
                }
            }
        });
    }

    public void pressBackButton() {
        if (!hasChildScene()) {
            this.gamemenues.startMenu();
        }
        showExitWarningDialogInGame(this.blockpro);
    }

    public void pressMenuButton() {
        if (!hasWon() && !hasLost()) {
            if (hasChildScene()) {
                this.gamemenues.stopMenu();
            } else {
                this.gamemenues.startMenu();
            }
        }
    }

    public void resetScene() {
        this.blockpro.runOnUpdateThread(new Runnable() {
            public void run() {
                GameScene.this.unloadHandlers();
                GameScene.this.unloadRestOfChildrenAndObjects();
                GameScene.this.onloadSceneObjects();
                GameScene.this.startWhenLoaded();
            }
        });
    }

    public void showExitWarningDialog(BlockPro blockpro2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(blockpro2);
        builder.setMessage("Are you sure you want to exit?").setCancelable(true).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                SceneLoader.getInstance().loadMenuScene(false);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    public void showExitWarningDialogInGame(BlockPro bp) {
        showExitWarningDialog(bp);
    }

    public void showRules() {
    }

    public void showStartMenu() {
        setChildScene(this.gamemenues.startScene, false, true, true);
    }

    /* access modifiers changed from: protected */
    public void startGame() {
        this.engine.unregisterUpdateHandler(this.startHandler);
    }

    public void startHandlers() {
        this.engine.enableAccelerometerSensor(this.blockpro, this);
        registerUpdateHandler(this.updateHandler);
        if (this.background != null) {
            this.background.start();
        }
        if (this.steadybar != null) {
            this.steadybar.resume();
        }
        this.pausedGame = false;
    }

    public void startPhysics() {
        Const.justCommingFromMenu = false;
        registerUpdateHandler(this.mPhysicsWorld);
        startHandlers();
    }

    public void startStartHandler() {
        this.started = false;
        this.engine.registerUpdateHandler(this.startHandler);
    }

    public Scene startWhenLoaded() {
        startStartHandler();
        return this;
    }

    public void stopHandlers() {
        this.pausedGame = true;
        this.engine.disableAccelerometerSensor(this.blockpro);
        unregisterUpdateHandler(this.updateHandler);
        if (this.steadybar != null) {
            this.steadybar.pause();
        }
        if (this.background != null) {
            this.background.stop();
        }
    }

    public void stopPhysics() {
        unregisterUpdateHandler(this.mPhysicsWorld);
        stopHandlers();
    }

    public String toString() {
        return "GameScene.java";
    }

    /* access modifiers changed from: protected */
    public void unloadChildrenAndObjects() {
        getChild(0).detachChildren();
        getChild(1).detachChildren();
        getChild(2).detachChildren();
        unloadRestOfChildrenAndObjects();
    }

    /* access modifiers changed from: private */
    public void unloadHandlers() {
        stopPhysics();
        this.engine.unregisterUpdateHandler(this.mPhysicsWorld);
        this.engine.unregisterUpdateHandler(this.updateHandler);
        this.mPhysicsWorld.clearPhysicsConnectors();
    }

    /* access modifiers changed from: protected */
    public void unloadRestOfChildrenAndObjects() {
        getChild(3).detachChildren();
        getChild(4).detachChildren();
        getChild(5).detachChildren();
        if (this.bottomblock != null) {
            this.bottomblock.unload();
        }
        if (this.steadybar != null) {
            this.steadybar.unload();
        }
        for (int i = 0; i < this.bodies.size(); i++) {
            this.bodies.get(i).stop();
        }
        this.bodies = null;
    }

    public void updateOnBBlockMove(Vector2 vTemp) {
    }
}
