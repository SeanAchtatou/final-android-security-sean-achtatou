package com.polartouch.blockpro.game.level;

import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.Const;
import com.polartouch.blockpro.boxes.Box;
import com.polartouch.blockpro.game.BottomBlock;
import com.polartouch.blockpro.game.GameScene;
import com.polartouch.blockpro.game.GameTextureLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.opengl.vertex.TextVertexBuffer;
import org.anddev.andengine.util.constants.TimeConstants;

public class LevelLoader {
    public static Random random = new Random();
    private BlockPro blockpro;
    private Engine engine;
    private GameScene gs;
    private int levelToLoad;
    private GameTextureLoader ltl;
    private FixedStepPhysicsWorld mPhysicsWorld;
    public int nrOfLevels = 0;
    public int statMaxBodies;
    public List<Box> waitingBodies;

    public LevelLoader(GameScene ls, FixedStepPhysicsWorld pw, GameTextureLoader ltl2, BlockPro blockpro2) {
        this.gs = ls;
        this.engine = blockpro2.getEngine();
        this.mPhysicsWorld = pw;
        this.ltl = ltl2;
        this.blockpro = blockpro2;
        this.waitingBodies = new ArrayList();
    }

    public BottomBlock loadBottomBlock(int levelToLoad2) {
        this.levelToLoad = levelToLoad2;
        switch (Const.selectedWorld) {
            case 0:
                return loadBottomBlockWorld1();
            case 1:
                return loadBottomBlockWorld2();
            default:
                return null;
        }
    }

    public void loadWaitingBodiesOnLevel(int levelToLoad2) {
        this.levelToLoad = levelToLoad2;
        switch (Const.selectedWorld) {
            case 0:
                loadWaitingBodiesOnWorld1();
                break;
            case 1:
                loadWaitingBodiesOnWorld2();
                break;
        }
        this.nrOfLevels = Const.nrOfLevelsArray[Const.selectedWorld];
        this.statMaxBodies = this.waitingBodies.size();
    }

    public BottomBlock loadBottomBlockWorld1() {
        BottomBlock bBlock = new BottomBlock(this.gs, this.mPhysicsWorld, this.blockpro);
        switch (this.levelToLoad) {
            case 1:
            case 2:
            case 3:
            case 4:
                bBlock.scale(2.0f);
                break;
            case 5:
            case TextVertexBuffer.VERTICES_PER_CHARACTER /*6*/:
                bBlock.height(50).scale(2.0f);
                break;
            case TimeConstants.DAYSPERWEEK /*7*/:
            case 8:
            case 9:
                bBlock.scale(2.0f);
                break;
            case Const.highTowerblocksPerLevel:
            case 11:
            case TimeConstants.MONTHSPERYEAR /*12*/:
                bBlock.scale(1.3f);
                break;
            case 13:
                bBlock.scale(2.5f).height(10);
                break;
            case 14:
            case 15:
                bBlock.scale(1.0f);
                break;
        }
        bBlock.setColor(0.0f, 0.0f, 0.0f);
        bBlock.build();
        return bBlock;
    }

    private BottomBlock loadBottomBlockWorld2() {
        BottomBlock bBlock = new BottomBlock(this.gs, this.mPhysicsWorld, this.blockpro);
        switch (this.levelToLoad) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case TextVertexBuffer.VERTICES_PER_CHARACTER /*6*/:
            case TimeConstants.DAYSPERWEEK /*7*/:
                bBlock.scale(2.0f);
                break;
            case 8:
                bBlock.scale(1.7f);
                break;
            case 9:
                bBlock.scale(1.7f).height(80);
                break;
            case Const.highTowerblocksPerLevel:
                bBlock.scale(2.0f);
                break;
        }
        bBlock.setColor(1.0f, 1.0f, 1.0f);
        bBlock.build();
        return bBlock;
    }

    private void loadWaitingBodiesOnWorld1() {
        switch (this.levelToLoad) {
            case 1:
                add(square(50));
                add(square(30).time(40));
                add(square(60).time(40));
                return;
            case 2:
                add(square(50));
                add(rect(30));
                add(square(80).time(25));
                add(square(50));
                return;
            case 3:
                add(square(10));
                add(square(20));
                add(square(60).tilt(0.5f).time(20));
                add(rect(10).time(30));
                return;
            case 4:
                add(square(50));
                add(square(40));
                add(rect(50));
                add(rect(60).time(20));
                add(square(50).time(20));
                add(square(20).time(30));
                add(square(50).time(30));
                return;
            case 5:
                add(square(40).up().time(40));
                add(rect(60).up());
                add(square(40));
                add(square(40).up().time(30));
                add(square(20).up());
                add(square(90).time(60));
                add(square(60).up());
                return;
            case TextVertexBuffer.VERTICES_PER_CHARACTER /*6*/:
                add(square(30).up().time(40));
                add(square(30).up());
                add(square(60));
                add(square(40).time(30));
                add(rect(40));
                add(rect(90).up().time(60));
                add(square(10).up().time(40));
                add(square(70).time(50));
                return;
            case TimeConstants.DAYSPERWEEK /*7*/:
                add(tri(50));
                add(tri(30).time(20));
                add(tri(60).time(20));
                add(tri(60).rotate(1.0f).time(30));
                return;
            case 8:
                square(30);
                square(60);
                add(tri(40));
                add(tri(50).time(20));
                add(rect_long(40));
                add(tri(40));
                add(tri(50));
                add(square(60).rotate(40.0f));
                return;
            case 9:
                add(tri(40));
                add(tri(50));
                add(big(40).tilt(2.0f));
                add(tri(40));
                add(tri(40).tilt(1.0f));
                return;
            case Const.highTowerblocksPerLevel:
                add(rect(10));
                add(square(70).time(20));
                add(square(60).tilt(0.5f).time(20));
                add(rect(10).time(30).tilt(0.5f));
                return;
            case 11:
                add(square(40).tilt(0.2f).rotate(100.0f));
                add(square(60).tilt(0.9f).time(20).rotate(100.0f));
                add(square(60));
                add(square(20).time(25));
                add(square(50));
                add(square(20).time(30).rotate(100.0f));
                add(square(30).time(20).rotate(-10.0f));
                add(square(50).time(30).rotate(-100.0f));
                return;
            case TimeConstants.MONTHSPERYEAR /*12*/:
                add(square(20));
                add(square(40).time(30));
                add(square(60).time(20));
                add(square(30).time(30));
                add(square(40).time(30));
                add(square(40).time(30));
                add(square(20).time(30).tilt(0.4f));
                add(square(70).time(50).tilt(-0.4f));
                return;
            case 13:
                add(square(10).time(20));
                add(square(60).tilt(0.2f).rotate(100.0f));
                add(square(40).rotate(-10.0f));
                add(square(50).rotate(-200.0f));
                add(square(10).rotate(10.0f).tilt(-0.2f));
                add(square(30).rotate(-30.0f).tilt(0.3f));
                add(square(10).tilt(0.5f).time(30));
                add(square(70));
                add(square(40).tilt(0.2f));
                add(square(10).tilt(-0.4f));
                add(square(50));
                return;
            case 14:
                add(square(40).tilt(0.2f).time(20));
                add(square(30).tilt(0.2f).time(20));
                add(rect(20).tilt(0.3f).time(20));
                add(rect(70).tilt(-0.4f).time(20));
                add(rect_long(30).rotate(10.0f).time(30));
                add(rect(20).rotate(-20.0f).time(30));
                add(rect(40).rotate(20.0f));
                return;
            case 15:
                add(rect_long(40).tilt(0.7f));
                add(rect(20).tilt(1.0f));
                add(rect(60));
                add(rect(40));
                add(rect(20));
                add(rect(30).time(30));
                add(rect(80).tilt(1.0f).time(30));
                add(rect_long(60));
                add(big(40).time(40).rotate(700.0f));
                return;
            default:
                return;
        }
    }

    private void loadWaitingBodiesOnWorld2() {
        switch (this.levelToLoad) {
            case 1:
                add(big(10));
                add(big(50));
                add(big(10));
                add(big(90).time(25));
                return;
            case 2:
                add(rect(10).tilt(1.0f));
                add(rect(30).tilt(1.0f));
                add(rect(50).tilt(1.0f).time(20));
                add(rect(60).tilt(1.0f));
                add(rect(20).tilt(1.0f));
                return;
            case 3:
                add(rect(50).tilt(1.0f));
                add(rect(50).tilt(1.0f).time(30));
                add(rect_long(50).time(30));
                add(rect(20).tilt(1.0f).time(30));
                add(rect(50).tilt(1.0f).time(30));
                add(rect(80).tilt(1.0f).time(30));
                return;
            case 4:
                add(rect(50).tilt(1.0f));
                add(rect(50));
                add(rect(50).tilt(1.0f));
                add(rect(50));
                add(rect(50).tilt(1.0f));
                add(rect(50));
                add(rect_long(50).time(20));
                add(rect(20).tilt(1.0f));
                add(rect_long(50).time(20));
                add(rect(50).tilt(1.0f));
                return;
            case 5:
                add(six(50));
                add(six(30).time(40));
                add(big(60).tilt(1.0f).time(40));
                add(big(70).time(30));
                add(big(40).rotate(20.0f).time(30));
                add(six(70).time(40));
                return;
            case TextVertexBuffer.VERTICES_PER_CHARACTER /*6*/:
                add(six(50));
                add(six(30).time(40));
                add(six(60).tilt(1.0f));
                add(six(70));
                add(six(40).rotate(20.0f));
                add(six(70).time(40));
                return;
            case TimeConstants.DAYSPERWEEK /*7*/:
                add(rect(50).tilt(1.0f));
                add(rect_long(50).time(30));
                add(rect(50).tilt(1.0f));
                add(rect_long(50).time(30));
                add(rect(10).time(30));
                add(rect(50).time(30));
                add(rect(90));
                return;
            case 8:
                add(big(50));
                add(tri(30).time(20));
                add(six(60).tilt(1.0f));
                add(rect_long(70));
                add(square(40).rotate(20.0f));
                add(tri(70).time(40));
                add(tri(30).time(20));
                add(square(20).time(30).rotate(70.0f));
                add(big(70).time(40));
                return;
            case 9:
                add(rect(60).up());
                add(rect(20).up());
                add(rect(20).up().time(30));
                add(rect(90).up().time(30));
                add(rect(20).up());
                add(rect(20).up().tilt(1.0f).time(30));
                add(rect(50).up().tilt(1.0f));
                add(rect_long(20).up().time(40));
                add(rect(20).rotate(30.0f).up().time(31));
                add(rect(50).rotate(-50.0f).up());
                return;
            case Const.highTowerblocksPerLevel:
                add(rect_long(20).tilt(1.0f));
                add(rect_long(40).tilt(1.0f).time(30));
                add(rect_long(20).tilt(0.0f).time(30));
                add(rect_long(30).tilt(1.0f).time(30));
                add(rect_long(20).tilt(1.0f).time(30));
                add(rect_long(50).tilt(1.0f).time(30));
                add(rect_long(50).tilt(0.0f).time(30));
                return;
            default:
                return;
        }
    }

    private Box square(int pos) {
        return new Box(3, this.engine, this.mPhysicsWorld, this.ltl.squareBlock, this.ltl.squareSBlock, (float) pos);
    }

    private Box rect(int pos) {
        return new Box(3, this.engine, this.mPhysicsWorld, this.ltl.rectBlock, this.ltl.rectSBlock, (float) pos);
    }

    private Box rect_long(int pos) {
        return new Box(3, this.engine, this.mPhysicsWorld, this.ltl.longRectBlock, this.ltl.longSRectBlock, (float) pos);
    }

    private Box big(int pos) {
        return new Box(3, this.engine, this.mPhysicsWorld, this.ltl.bigBlock, this.ltl.bigSBlock, (float) pos);
    }

    private Box six(int pos) {
        return new Box(2, this.engine, this.mPhysicsWorld, this.ltl.sixBlock, this.ltl.sixSBlock, (float) pos);
    }

    private Box tri(int pos) {
        return new Box(0, this.engine, this.mPhysicsWorld, this.ltl.triBlock, this.ltl.triSBlock, (float) pos);
    }

    private void add(Box box) {
        switch (Const.selectedWorld) {
            case 0:
                this.waitingBodies.add(addNormalParticle(box).build());
                return;
            case 1:
                this.waitingBodies.add(addBubbleParticle(box).build());
                return;
            default:
                return;
        }
    }

    private Box addNormalParticle(Box box) {
        if (!Const.getUseParticles(this.blockpro)) {
            return box;
        }
        return box.addNormalParticle(this.gs, this.ltl.particle1);
    }

    private Box addBubbleParticle(Box box) {
        if (!Const.getUseParticles(this.blockpro)) {
            return box;
        }
        return box.addBubbleParticle(this.gs, this.ltl.bubble);
    }
}
