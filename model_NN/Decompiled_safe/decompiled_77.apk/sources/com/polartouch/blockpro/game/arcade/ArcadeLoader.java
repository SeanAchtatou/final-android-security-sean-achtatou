package com.polartouch.blockpro.game.arcade;

import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.Const;
import com.polartouch.blockpro.boxes.ArcadeBox;
import com.polartouch.blockpro.game.BottomBlock;
import com.polartouch.blockpro.game.GameScene;
import com.polartouch.blockpro.game.GameTextureLoader;
import java.util.Random;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class ArcadeLoader {
    private static Random random = new Random();
    private BlockPro bp;
    private Engine engine;
    private GameScene gs;
    private FixedStepPhysicsWorld mPhysicsWorld;
    private TextureRegion selectedSTR;
    private TextureRegion selectedTR;

    public ArcadeLoader(GameScene ls, FixedStepPhysicsWorld pw, GameTextureLoader ltl, BlockPro bp2) {
        this.gs = ls;
        this.engine = bp2.getEngine();
        this.mPhysicsWorld = pw;
        this.bp = bp2;
        switch (Const.selectedArcadeBlockType) {
            case 0:
                this.selectedTR = ltl.squareBlock;
                this.selectedSTR = ltl.squareSBlock;
                return;
            case 1:
                this.selectedTR = ltl.rectBlock;
                this.selectedSTR = ltl.rectSBlock;
                return;
            case 2:
                this.selectedTR = ltl.longRectBlock;
                this.selectedSTR = ltl.longSRectBlock;
                return;
            default:
                return;
        }
    }

    public ArcadeBox getABox() {
        return new ArcadeBox(3, this.engine, this.mPhysicsWorld, this.selectedTR, this.selectedSTR, (float) (random.nextInt(60) + 20)).build();
    }

    public ArcadeBox getAUpBox() {
        return new ArcadeBox(3, this.engine, this.mPhysicsWorld, this.selectedTR, this.selectedSTR, (float) (random.nextInt(60) + 20)).up().build();
    }

    public ArcadeBox getARandomBox() {
        if (random.nextBoolean()) {
            return getABox();
        }
        return getAUpBox();
    }

    public BottomBlock getBottomBlock() {
        BottomBlock bBlock = new BottomBlock(this.gs, this.mPhysicsWorld, this.bp);
        bBlock.height(50).scale(1.6f).setColor(1.0f, 1.0f, 1.0f).build();
        return bBlock;
    }
}
