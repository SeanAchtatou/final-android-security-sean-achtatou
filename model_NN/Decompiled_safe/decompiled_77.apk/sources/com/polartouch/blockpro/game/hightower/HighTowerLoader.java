package com.polartouch.blockpro.game.hightower;

import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.boxes.StickyBox;
import com.polartouch.blockpro.game.BottomBlock;
import com.polartouch.blockpro.game.GameScene;
import com.polartouch.blockpro.game.GameTextureLoader;
import java.util.Random;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class HighTowerLoader {
    private static Random random = new Random();
    private BlockPro bp;
    private Engine engine;
    private GameScene gs;
    private GameTextureLoader ltl;
    private FixedStepPhysicsWorld mPhysicsWorld;

    public HighTowerLoader(GameScene ls, FixedStepPhysicsWorld pw, GameTextureLoader ltl2, BlockPro bp2) {
        this.gs = ls;
        this.engine = bp2.getEngine();
        this.mPhysicsWorld = pw;
        this.ltl = ltl2;
        this.bp = bp2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public StickyBox getABox(float til, float rot, float vx) {
        int value;
        TextureRegion tr;
        TextureRegion trShadow;
        float tilt = 0.0f;
        int shape = 3;
        int r = random.nextInt(100);
        if (r < 35) {
            value = 10;
            tr = this.ltl.squareBlock;
            trShadow = this.ltl.squareSBlock;
            if (r < 10) {
                tilt = 0.0f + 1.0f;
            }
        } else if (r < 70) {
            value = 15;
            tr = this.ltl.rectBlock;
            trShadow = this.ltl.rectSBlock;
        } else if (r < 80) {
            shape = 2;
            value = 20;
            tr = this.ltl.sixBlock;
            trShadow = this.ltl.sixSBlock;
        } else if (r < 90) {
            value = 15;
            tr = this.ltl.bigBlock;
            trShadow = this.ltl.bigSBlock;
        } else {
            value = 20;
            tr = this.ltl.longRectBlock;
            trShadow = this.ltl.longSRectBlock;
        }
        float tilt2 = tilt + (2.0f * til * (random.nextFloat() - 0.5f));
        float rotation = 0.0f + (2.0f * rot * (random.nextFloat() - 0.5f));
        int pos = random.nextInt(60) + 20;
        if (pos > 50 && vx != 0.0f) {
            vx *= -1.0f;
            pos = 90;
        } else if (vx != 0.0f) {
            pos = 10;
        }
        StickyBox abox = new StickyBox(shape, this.engine, this.mPhysicsWorld, tr, trShadow, (float) pos);
        abox.statValue = Math.round(10.0f + (Math.abs(tilt2) * 5.0f) + Math.min(10.0f, Math.abs(rot / 50.0f)) + Math.min(30.0f, Math.abs(vx) * 6.0f)) + value;
        return abox.tilt(tilt2).rotate(rotation).impulse(vx);
    }

    public BottomBlock getBottomBlock() {
        BottomBlock bBlock = new BottomBlock(this.gs, this.mPhysicsWorld, this.bp);
        bBlock.scale(1.6f).setColor(1.0f, 1.0f, 1.0f).build();
        return bBlock;
    }
}
