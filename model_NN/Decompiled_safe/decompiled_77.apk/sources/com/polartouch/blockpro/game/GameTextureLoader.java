package com.polartouch.blockpro.game;

import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.Const;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;

public class GameTextureLoader {
    private static GameTextureLoader inst = new GameTextureLoader();
    public TextureRegion bigBlock;
    public TextureRegion bigSBlock;
    BlockPro blockpro;
    public TextureRegion bubble;
    Engine engine;
    private Texture[] loadedTextures;
    private int loadedTexturesCount;
    public TextureRegion longRectBlock;
    public TextureRegion longSRectBlock;
    private Texture mBlocksTexture;
    public Texture mHudTexture;
    private Texture mParticleTexture;
    private Texture mShadowBlocksTexture;
    public TextureRegion particle1;
    public TextureRegion rectBlock;
    public TextureRegion rectSBlock;
    public TextureRegion sixBlock;
    public TextureRegion sixSBlock;
    public TextureRegion squareBlock;
    public TextureRegion squareSBlock;
    public TextureRegion trHud;
    public TextureRegion triBlock;
    public TextureRegion triSBlock;

    public static GameTextureLoader getInstance() {
        return inst;
    }

    public boolean isLoadedToMemory() {
        for (int i = 0; i < this.loadedTexturesCount; i++) {
            if (!this.loadedTextures[i].isLoadedToHardware()) {
                return false;
            }
        }
        return true;
    }

    public void loadTextures() {
        this.mBlocksTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.squareBlock = TextureRegionFactory.createFromAsset(this.mBlocksTexture, this.blockpro, "blocks/square_61_61.png", 0, 0);
        this.rectBlock = TextureRegionFactory.createFromAsset(this.mBlocksTexture, this.blockpro, "blocks/rect_92_61.png", 63, 0);
        this.longRectBlock = TextureRegionFactory.createFromAsset(this.mBlocksTexture, this.blockpro, "blocks/long_rect_162_54.png", 0, 63);
        this.sixBlock = TextureRegionFactory.createFromAsset(this.mBlocksTexture, this.blockpro, "blocks/six_100_87.png", 0, 120);
        this.bigBlock = TextureRegionFactory.createFromAsset(this.mBlocksTexture, this.blockpro, "blocks/big_95.png", 102, 120);
        this.triBlock = TextureRegionFactory.createFromAsset(this.mBlocksTexture, this.blockpro, "blocks/tri_102_89.png", 0, 210);
        this.mShadowBlocksTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.squareSBlock = TextureRegionFactory.createFromAsset(this.mShadowBlocksTexture, this.blockpro, "blocks/square_93_93.png", 0, 0);
        this.rectSBlock = TextureRegionFactory.createFromAsset(this.mShadowBlocksTexture, this.blockpro, "blocks/rect_118_91.png", 95, 0);
        this.longSRectBlock = TextureRegionFactory.createFromAsset(this.mShadowBlocksTexture, this.blockpro, "blocks/long_rect_192_84.png", 0, 100);
        this.sixSBlock = TextureRegionFactory.createFromAsset(this.mShadowBlocksTexture, this.blockpro, "blocks/six_shadow_126_116.png", 0, 186);
        this.bigSBlock = TextureRegionFactory.createFromAsset(this.mShadowBlocksTexture, this.blockpro, "blocks/big_shadow_127.png", 130, 186);
        this.triSBlock = TextureRegionFactory.createFromAsset(this.mShadowBlocksTexture, this.blockpro, "blocks/tri_shadow_128_116.png", 0, 320);
        this.mParticleTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.particle1 = TextureRegionFactory.createFromAsset(this.mParticleTexture, this.blockpro, "particle1.png", 0, 0);
        this.bubble = TextureRegionFactory.createFromAsset(this.mParticleTexture, this.blockpro, "bubble_43.png", 100, 0);
        this.mHudTexture = new Texture(512, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.trHud = TextureRegionFactory.createFromAsset(this.mHudTexture, this.blockpro, "hud_1_480_131.png", 0, 0);
        loadTextures(this.mBlocksTexture, this.mShadowBlocksTexture, this.mHudTexture, this.mParticleTexture);
    }

    private void loadTextures(Texture... params) {
        this.loadedTextures = params;
        this.loadedTexturesCount = params.length;
        Const.log("loaded GTL " + this.loadedTexturesCount);
        for (int i = 0; i < this.loadedTexturesCount; i++) {
            this.blockpro.getEngine().getTextureManager().loadTexture(params[i]);
        }
    }

    public void start(BlockPro blockpro2) {
        this.blockpro = blockpro2;
        this.engine = blockpro2.getEngine();
        TextureRegionFactory.setAssetBasePath("gfx/");
    }

    public void unloadTextures() {
        Const.log("unloaded GTL " + this.loadedTexturesCount);
        for (int i = 0; i < this.loadedTexturesCount; i++) {
            this.blockpro.getEngine().getTextureManager().unloadTexture(this.loadedTextures[i]);
        }
    }
}
