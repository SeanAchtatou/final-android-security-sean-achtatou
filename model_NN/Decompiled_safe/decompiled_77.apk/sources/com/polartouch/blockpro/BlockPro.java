package com.polartouch.blockpro;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;
import com.flurry.android.FlurryAgent;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.BoundCamera;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.ui.activity.BaseGameActivity;

public class BlockPro extends BaseGameActivity {
    private BoundCamera camera;
    public FontLoader fontloader;
    TextureRegion mBoxMedium;
    int mFaceCount = 0;
    private boolean okToPushMenu = true;
    private SceneLoader sl;
    private boolean updateCamera;
    private PowerManager.WakeLock wl;

    public void activateUpdateCamera() {
        this.camera.setBounds(0.0f, 480.0f, -8000.0f, 800.0f);
        this.camera.setCenter(240.0f, 400.0f);
        this.updateCamera = true;
    }

    private void closeIfBetaExpired() {
        if (!Calendar.getInstance().before(new GregorianCalendar(2011, 9, 15))) {
            Toast.makeText(this, "This beta version is way too old, please make sure you always have the newest version!", 1).show();
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse("market://search?q=pub:Polartouch.se"));
            startActivity(intent);
            finish();
        }
    }

    public void deactivateUpdateCamera() {
        this.updateCamera = false;
        this.camera.setCenter(240.0f, 400.0f);
        this.camera.setBounds(0.0f, 480.0f, 0.0f, 800.0f);
    }

    public BoundCamera getCamera() {
        return this.camera;
    }

    public boolean getUpdateCamera() {
        return this.updateCamera;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.wl = ((PowerManager) getSystemService("power")).newWakeLock(26, "DoNotDimScreen");
        closeIfBetaExpired();
        this.sl = SceneLoader.getInstance();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Scene s = getEngine().getScene();
        if (keyCode == 4 && s != null) {
            ((CustomScene) s).pressBackButton();
            return true;
        } else if (keyCode != 82 || !this.okToPushMenu) {
            return true;
        } else {
            ((CustomScene) s).pressMenuButton();
            this.okToPushMenu = false;
            return true;
        }
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 4 && keyCode == 82) {
            this.okToPushMenu = true;
        }
        return true;
    }

    public void onLoadComplete() {
        Const.log("onLoadComplete");
    }

    public Engine onLoadEngine() {
        Const.blockpro = this;
        this.camera = new BoundCamera(0.0f, 0.0f, 480.0f, 800.0f, 0.0f, 480.0f, 0.0f, 800.0f) {
            public void onUpdate(float pSecondsElapsed) {
                super.onUpdate(pSecondsElapsed);
                BlockPro.this.updateCamera();
            }
        };
        EngineOptions eo = new EngineOptions(true, EngineOptions.ScreenOrientation.PORTRAIT, new FillResolutionPolicy(), this.camera);
        eo.getRenderOptions().disableExtensionVertexBufferObjects();
        eo.getRenderOptions().disableExtensionVertexBufferObjects();
        return new Engine(eo);
    }

    public void onLoadResources() {
        Const.log("onLoadResources");
        TextureRegionFactory.setAssetBasePath("gfx/");
    }

    public Scene onLoadScene() {
        Log.d("Log", "onLoadScene");
        this.fontloader = new FontLoader(this);
        this.sl.start(getEngine(), this);
        this.sl.loadMenuScene(true);
        return this.sl.getCurrentScene();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Log.d("onPause", "onPause");
        this.wl.release();
        this.sl.onPause();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.wl.acquire();
        this.sl.onResume();
    }

    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "154I1H616HAG1SRWKTDS");
        FlurryAgent.onPageView();
    }

    public void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    public void updateCamera() {
        if (this.updateCamera) {
            this.sl.getCurrentScene().onCameraUpdate();
        }
    }
}
