package com.polartouch.blockpro;

import com.flurry.android.FlurryAgent;
import java.util.HashMap;

public class Stat {
    private static Stat inst = new Stat();

    public static Stat getInstance() {
        return inst;
    }

    private Stat() {
    }

    public void completeLevel(int level, int world) {
        HashMap<String, String> parameters = new HashMap<>();
        String diff = Const.extraDropTime == 1 ? "Normal" : "Hard";
        parameters.put("Status", "win");
        FlurryAgent.onEvent("_World " + world + " Level " + level + " " + diff, parameters);
    }

    public void failLevel(int level, int world) {
        HashMap<String, String> parameters = new HashMap<>();
        String diff = Const.extraDropTime == 1 ? "Normal" : "Hard";
        parameters.put("Status", "fail");
        FlurryAgent.onEvent("_World " + world + " Level " + level + " " + diff, parameters);
    }

    public void nextLevel(int level, int world) {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("Level", String.valueOf(level));
        parameters.put("World", String.valueOf(world));
        parameters.put("Diff", Const.extraDropTime == 1 ? "Normal" : "Hard");
        FlurryAgent.onEvent("Story Mode - NextLevel", parameters);
    }

    public void resetLevel(int level, int world) {
        HashMap<String, String> parameters = new HashMap<>();
        String diff = Const.extraDropTime == 1 ? "Normal" : "Hard";
        parameters.put("Status", "restart");
        FlurryAgent.onEvent("_World " + world + " Level " + level + " " + diff, parameters);
    }

    public void startLevel(int level, int world) {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("Level", String.valueOf(level));
        parameters.put("World", String.valueOf(world));
        parameters.put("Diff", Const.extraDropTime == 1 ? "Normal" : "Hard");
        FlurryAgent.onEvent("Story Mode", parameters);
    }
}
