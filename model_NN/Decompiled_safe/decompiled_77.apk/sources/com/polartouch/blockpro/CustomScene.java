package com.polartouch.blockpro;

import org.anddev.andengine.entity.scene.Scene;

public abstract class CustomScene extends Scene {
    public abstract void onPause();

    public abstract void onResume();

    public abstract void onloadScene();

    public abstract void pressBackButton();

    public abstract void pressMenuButton();

    public abstract Scene startWhenLoaded();

    public abstract String toString();

    public abstract void unload();

    public CustomScene(int pLayerCount) {
        super(pLayerCount);
    }

    public void onCameraUpdate() {
    }

    public void updateFollowCamera(float ms) {
    }
}
