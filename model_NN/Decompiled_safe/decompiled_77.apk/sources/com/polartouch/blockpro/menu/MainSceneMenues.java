package com.polartouch.blockpro.menu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.Const;
import com.polartouch.blockpro.R;
import com.polartouch.blockpro.SceneLoader;

public class MainSceneMenues {
    /* access modifiers changed from: private */
    public final BlockPro blockpro;

    public MainSceneMenues(BlockPro blockpro2) {
        this.blockpro = blockpro2;
    }

    public void checkAndOpenChangelog() {
        try {
            int versionCode = this.blockpro.getPackageManager().getPackageInfo(this.blockpro.getPackageName(), 0).versionCode;
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this.blockpro.getBaseContext());
            if (settings.getInt("changelogversion", 0) < versionCode) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("changelogversion", versionCode);
                editor.commit();
                displayChangelog();
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.w("Unable to get version code. Will not show changelog", e);
        }
    }

    /* access modifiers changed from: private */
    public void displayChangelog() {
        new AlertDialog.Builder(this.blockpro).setTitle("Welcome!").setIcon(17301569).setView(LayoutInflater.from(this.blockpro).inflate((int) R.layout.changelog_view, (ViewGroup) null)).setNegativeButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    public void pushAbout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.blockpro);
        builder.setTitle("About");
        builder.setItems(new CharSequence[]{"About the game", "Changelog", "More games!"}, new DialogInterface.OnClickListener() {
            private void moreGames() {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("market://search?q=pub:Polartouch.se"));
                MainSceneMenues.this.blockpro.startActivity(intent);
            }

            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        MainSceneMenues.this.showAbout();
                        return;
                    case 1:
                        MainSceneMenues.this.displayChangelog();
                        return;
                    case 2:
                        moreGames();
                        return;
                    default:
                        return;
                }
            }
        });
        builder.create().show();
    }

    public void pushPlayButton() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.blockpro);
        builder.setTitle("Choose Mode:");
        builder.setItems(new CharSequence[]{"Story Mode", "Arcade Mode", "High Tower (new!)"}, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Const.justCommingFromMenu = true;
                switch (item) {
                    case 0:
                        MainSceneMenues.this.selectWorldDialog();
                        return;
                    case 1:
                        MainSceneMenues.this.selectArcadeDialog();
                        return;
                    case 2:
                        MainSceneMenues.this.startHighTower();
                        return;
                    default:
                        return;
                }
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void selectArcadeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.blockpro);
        builder.setTitle("Select block type:");
        builder.setItems(new CharSequence[]{"Squares", "Rectangles", "Long rectangles"}, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Const.selectedArcadeBlockType = item;
                SceneLoader.getInstance().loadArcadeScene();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                MainSceneMenues.this.pushPlayButton();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void selectDiffDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.blockpro);
        builder.setTitle("Select difficulty:");
        builder.setItems(new CharSequence[]{"Normal", "Hard"}, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Const.extraDropTime = 1 - item;
                MainSceneMenues.this.selectLevel();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                MainSceneMenues.this.selectWorldDialog();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void selectLevel() {
        int levelscompleted = 0;
        switch (Const.selectedWorld) {
            case 0:
                levelscompleted = Const.getWorld1Level(this.blockpro);
                break;
            case 1:
                levelscompleted = Const.getWorld2Level(this.blockpro);
                break;
        }
        int levelscompleted2 = levelscompleted + 1;
        if (levelscompleted2 > Const.nrOfLevelsArray[Const.selectedWorld]) {
            levelscompleted2 = Const.nrOfLevelsArray[Const.selectedWorld];
        }
        CharSequence[] items = new String[levelscompleted2];
        for (int i = 1; i <= levelscompleted2; i++) {
            items[i - 1] = "level " + i;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this.blockpro);
        builder.setTitle("Select Level:");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Const.selectedlevel = item + 1;
                SceneLoader.getInstance().loadLevelScene();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                MainSceneMenues.this.selectDiffDialog();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void selectWorldDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.blockpro);
        builder.setTitle("Select world:");
        builder.setItems(new CharSequence[]{"1. Sky", "2. Underwater (new)"}, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Const.selectedWorld = item;
                MainSceneMenues.this.selectDiffDialog();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                MainSceneMenues.this.pushPlayButton();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void sendEmail() {
        Intent emailIntent = new Intent("android.intent.action.SEND");
        emailIntent.setType("plain/text");
        emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{"info@polartouch.se"});
        emailIntent.putExtra("android.intent.extra.SUBJECT", "Block Pro");
        emailIntent.putExtra("android.intent.extra.TEXT", "");
        this.blockpro.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    /* access modifiers changed from: private */
    public void showAbout() {
        View view = LayoutInflater.from(this.blockpro).inflate((int) R.layout.about_view, (ViewGroup) null);
        AlertDialog ad = new AlertDialog.Builder(this.blockpro).create();
        ad.setTitle("About");
        ad.setIcon(17301569);
        ad.setView(view);
        ad.setButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        ad.show();
        ((Button) ad.findViewById(R.id.about_feedback_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainSceneMenues.this.sendEmail();
            }
        });
    }

    public void showHelp() {
        new AlertDialog.Builder(this.blockpro).setTitle("Help").setIcon(17301569).setView(LayoutInflater.from(this.blockpro).inflate((int) R.layout.help_view, (ViewGroup) null)).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    public void showOptions() {
        this.blockpro.startActivity(new Intent(this.blockpro, Options.class));
    }

    /* access modifiers changed from: private */
    public void startHighTower() {
        SceneLoader.getInstance().loadHighTowerScene();
    }
}
