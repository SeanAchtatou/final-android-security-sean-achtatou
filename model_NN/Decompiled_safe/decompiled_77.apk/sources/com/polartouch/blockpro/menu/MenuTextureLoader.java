package com.polartouch.blockpro.menu;

import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.Const;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class MenuTextureLoader {
    private static MenuTextureLoader inst = new MenuTextureLoader();
    private BlockPro blockpro;
    public Texture buttonsTexture;
    private Texture[] loadedTextures;
    private int loadedTexturesCount;
    public Texture mBgTexture;
    public Texture mBgTexture2;
    public Texture mBorderTexture;
    public TextureRegion menu_bg1;
    public TextureRegion menu_bg2;
    public TextureRegion menu_bg3;
    public TextureRegion menu_block1;
    public TextureRegion menu_block2;
    public TextureRegion menu_logo;
    public TiledTextureRegion trAbout;
    public TiledTextureRegion trHelp;
    public TiledTextureRegion trOptions;
    public TiledTextureRegion trPlay;
    public TiledTextureRegion trQuit;

    public static MenuTextureLoader getInstance() {
        return inst;
    }

    public void start(BlockPro blockpro2) {
        this.blockpro = blockpro2;
        TextureRegionFactory.setAssetBasePath("gfx/menu/");
    }

    public void loadTextures() {
        this.mBgTexture = new Texture(1024, 1024, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.menu_bg3 = TextureRegionFactory.createFromAsset(this.mBgTexture, this.blockpro, "bg3_480_800.png", 0, 0);
        this.menu_bg2 = TextureRegionFactory.createFromAsset(this.mBgTexture, this.blockpro, "bg2_480_677.png", 500, 0);
        this.menu_block1 = TextureRegionFactory.createFromAsset(this.mBgTexture, this.blockpro, "block1_63_64.png", 0, 802);
        this.menu_block2 = TextureRegionFactory.createFromAsset(this.mBgTexture, this.blockpro, "block2_118_58.png", 70, 802);
        this.menu_logo = TextureRegionFactory.createFromAsset(this.mBgTexture, this.blockpro, "blockprologo_459_144.png", 200, 802);
        this.mBgTexture2 = new Texture(512, 1024, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.menu_bg1 = TextureRegionFactory.createFromAsset(this.mBgTexture2, this.blockpro, "bg1_480_800.png", 0, 0);
        this.buttonsTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.trPlay = TextureRegionFactory.createTiledFromAsset(this.buttonsTexture, this.blockpro, "play_275_156.png", 0, 0, 1, 2);
        this.trOptions = TextureRegionFactory.createTiledFromAsset(this.buttonsTexture, this.blockpro, "options_245_84.png", 50, 158, 1, 2);
        this.trHelp = TextureRegionFactory.createTiledFromAsset(this.buttonsTexture, this.blockpro, "help_147_80.png", 0, 242, 1, 2);
        this.trAbout = TextureRegionFactory.createTiledFromAsset(this.buttonsTexture, this.blockpro, "about_191_84.png", 0, 328, 1, 2);
        this.trQuit = TextureRegionFactory.createTiledFromAsset(this.buttonsTexture, this.blockpro, "quit_137_86.png", 0, 417, 1, 2);
        loadTextures(this.buttonsTexture, this.mBgTexture, this.mBgTexture2);
    }

    private void loadTextures(Texture... params) {
        this.loadedTextures = params;
        this.loadedTexturesCount = params.length;
        Const.log("loadMenuTextures " + this.loadedTexturesCount);
        for (int i = 0; i < this.loadedTexturesCount; i++) {
            this.blockpro.getEngine().getTextureManager().loadTexture(params[i]);
        }
    }

    public boolean isLoadedToMemory() {
        for (int i = 0; i < this.loadedTexturesCount; i++) {
            if (!this.loadedTextures[i].isLoadedToHardware()) {
                return false;
            }
        }
        return true;
    }

    public void unloadTextures() {
        Const.log("unloadMenuTextures " + this.loadedTexturesCount);
        for (int i = 0; i < this.loadedTexturesCount; i++) {
            this.blockpro.getEngine().getTextureManager().unloadTexture(this.loadedTextures[i]);
        }
    }
}
