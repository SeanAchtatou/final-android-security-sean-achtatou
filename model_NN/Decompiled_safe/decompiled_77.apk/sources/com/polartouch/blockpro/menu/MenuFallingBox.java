package com.polartouch.blockpro.menu;

import com.polartouch.blockpro.SmoothSprite;
import java.util.Random;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class MenuFallingBox extends SmoothSprite {
    private static final float margin = 50.0f;
    private static final int speed = 50;
    private float rotation = 0.0f;

    public MenuFallingBox(int x, int y, TextureRegion path) {
        super((float) x, (float) y, path);
        Random generator = new Random();
        if (generator.nextInt(100) < speed) {
            this.rotation = 10.0f;
        }
        setPosition((float) Math.round(margin + (generator.nextFloat() * 380.0f)), (float) y);
    }

    public void onManagedUpdate(float pSecondsElapsed) {
        setRotation(getRotation() + (this.rotation * pSecondsElapsed));
        float x = getX();
        float y = getY() + (margin * pSecondsElapsed);
        setPosition(x, y);
        if (y > 850.0f) {
            setPosition(x, -50.0f);
        }
    }
}
