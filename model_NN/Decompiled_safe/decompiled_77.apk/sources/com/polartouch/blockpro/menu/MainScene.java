package com.polartouch.blockpro.menu;

import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.Const;
import com.polartouch.blockpro.CustomScene;
import com.polartouch.blockpro.SmoothSprite;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.input.touch.TouchEvent;

public class MainScene extends CustomScene implements Scene.IOnSceneTouchListener {
    private final BlockPro blockpro;
    private MenuBackgroundSprite menu_sprite1a;
    private MenuBackgroundSprite menu_sprite1b;
    private final MainSceneMenues msm;
    private final CustomScene scene = this;
    private AnimatedSprite selectedButton;
    private AnimatedSprite spAbout;
    private AnimatedSprite spHelp;
    private AnimatedSprite spOptions;
    private AnimatedSprite spPlay;
    private AnimatedSprite spQuit;
    private final MenuTextureLoader tr = MenuTextureLoader.getInstance();

    public MainScene(int mLayerCount, BlockPro bp) {
        super(mLayerCount);
        this.blockpro = bp;
        this.msm = new MainSceneMenues(bp);
    }

    public void onCameraUpdate() {
    }

    public void onloadScene() {
        this.tr.start(this.blockpro);
        this.tr.loadTextures();
        onloadSceneObjects();
        this.msm.checkAndOpenChangelog();
    }

    public void onloadSceneObjects() {
        this.menu_sprite1a = new MenuBackgroundSprite(0, 0, this.tr.menu_bg1);
        this.menu_sprite1b = new MenuBackgroundSprite(0, Const.CAMERA_HEIGHT, this.tr.menu_bg1);
        SmoothSprite menu_sprite2 = new SmoothSprite(0.0f, 66.0f, this.tr.menu_bg2);
        SmoothSprite menu_sprite3 = new SmoothSprite(0.0f, 0.0f, this.tr.menu_bg3);
        menu_sprite2.setAlpha(0.6f);
        menu_sprite3.setAlpha(0.6f);
        getChild(0).attachChild(this.menu_sprite1a);
        getChild(0).attachChild(this.menu_sprite1b);
        getChild(1).attachChild(menu_sprite2);
        getChild(3).attachChild(menu_sprite3);
        for (int i = 0; i < 4; i++) {
            getChild(2).attachChild(new MenuFallingBox(0, -30 - (i * 200), this.tr.menu_block1));
        }
        getChild(4).attachChild(new SmoothSprite((float) ((Const.CAMERA_WIDTH - this.tr.menu_logo.getWidth()) / 2), 40.0f, this.tr.menu_logo));
        this.spPlay = new AnimatedSprite(102.0f, 235.0f, this.tr.trPlay);
        this.spOptions = new AnimatedSprite(117.0f, 370.0f, this.tr.trOptions);
        this.spHelp = new AnimatedSprite(180.0f, 460.0f, this.tr.trHelp);
        this.spAbout = new AnimatedSprite(145.0f, 550.0f, this.tr.trAbout);
        this.spQuit = new AnimatedSprite(172.0f, 640.0f, this.tr.trQuit);
        this.spHelp.setAlpha(0.2f);
        this.spAbout.setAlpha(0.2f);
        getChild(4).attachChild(this.spPlay);
        getChild(4).attachChild(this.spOptions);
        getChild(4).attachChild(this.spHelp);
        getChild(4).attachChild(this.spAbout);
        getChild(4).attachChild(this.spQuit);
        this.scene.setTouchAreaBindingEnabled(true);
        this.scene.setOnSceneTouchListener(this);
    }

    public void onPause() {
    }

    public void onResume() {
        Const.log("Resuming Menu - isAllloaded =" + this.tr.isLoadedToMemory());
    }

    public boolean onSceneTouchEvent(Scene tmpscene, TouchEvent touchEvent) {
        if (touchEvent.getAction() == 0) {
            this.selectedButton = spriteSelected(touchEvent);
            if (this.selectedButton != null) {
                this.selectedButton.setCurrentTileIndex(1);
            }
        }
        if (this.selectedButton != null) {
            if (touchEvent.getAction() == 2) {
                if (this.selectedButton.contains(touchEvent.getX(), touchEvent.getY())) {
                    this.selectedButton.setCurrentTileIndex(1);
                } else {
                    this.selectedButton.setCurrentTileIndex(0);
                }
            }
            if (touchEvent.getAction() == 1 && spriteSelected(touchEvent) == this.selectedButton) {
                this.selectedButton.setCurrentTileIndex(0);
                pushButton(this.selectedButton);
            }
        }
        return true;
    }

    public void pressBackButton() {
        quit();
    }

    public void pressMenuButton() {
    }

    private void pushButton(AnimatedSprite button) {
        if (button == this.spPlay) {
            this.msm.pushPlayButton();
        }
        if (button == this.spOptions) {
            this.msm.showOptions();
        }
        if (button == this.spHelp) {
            this.msm.showHelp();
        }
        if (button == this.spAbout) {
            this.msm.pushAbout();
        }
        if (button == this.spQuit) {
            quit();
        }
    }

    private void quit() {
        this.blockpro.finish();
    }

    private AnimatedSprite spriteSelected(TouchEvent tEvent) {
        if (this.spPlay.contains(tEvent.getX(), tEvent.getY())) {
            return this.spPlay;
        }
        if (this.spOptions.contains(tEvent.getX(), tEvent.getY())) {
            return this.spOptions;
        }
        if (this.spHelp.contains(tEvent.getX(), tEvent.getY())) {
            return this.spHelp;
        }
        if (this.spAbout.contains(tEvent.getX(), tEvent.getY())) {
            return this.spAbout;
        }
        if (this.spQuit.contains(tEvent.getX(), tEvent.getY())) {
            return this.spQuit;
        }
        return null;
    }

    public Scene startWhenLoaded() {
        return this;
    }

    public String toString() {
        return "MenuScene";
    }

    public void unload() {
        getChild(0).detachChildren();
        getChild(1).detachChildren();
        getChild(2).detachChildren();
        getChild(3).detachChildren();
        getChild(4).detachChildren();
        reset();
        this.tr.unloadTextures();
    }
}
