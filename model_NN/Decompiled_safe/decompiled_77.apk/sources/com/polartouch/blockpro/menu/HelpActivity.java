package com.polartouch.blockpro.menu;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import com.polartouch.blockpro.R;

public class HelpActivity extends Activity {
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(1);
        setContentView((int) R.layout.help);
        WebView wv = (WebView) findViewById(R.id.wv1);
        wv.loadDataWithBaseURL("file:///www/nothing/noting", "", "text/html", "utf-8", "");
        wv.loadUrl("file:///android_asset/help.html");
    }
}
