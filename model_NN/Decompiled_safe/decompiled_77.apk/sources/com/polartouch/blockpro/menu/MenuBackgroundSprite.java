package com.polartouch.blockpro.menu;

import com.polartouch.blockpro.SmoothSprite;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class MenuBackgroundSprite extends SmoothSprite {
    private static final int speed = -50;

    public MenuBackgroundSprite(int x, int y, TextureRegion path) {
        super((float) x, (float) y, path);
    }

    public void onManagedUpdate(float pSecondsElapsed) {
        float x = getX();
        float y = getY() + (-50.0f * pSecondsElapsed);
        setPosition(x, y);
        if (y <= -800.0f) {
            setPosition(x, 1600.0f + y);
        }
    }
}
