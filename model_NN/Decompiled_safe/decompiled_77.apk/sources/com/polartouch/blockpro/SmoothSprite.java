package com.polartouch.blockpro;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.util.GLHelper;

public class SmoothSprite extends Sprite {
    public SmoothSprite(float x, float y, TextureRegion path) {
        super(x, y, path);
    }

    /* access modifiers changed from: protected */
    public void onInitDraw(GL10 pGL) {
        super.onInitDraw(pGL);
        GLHelper.enableTextures(pGL);
        GLHelper.enableTexCoordArray(pGL);
        GLHelper.enableDither(pGL);
    }
}
