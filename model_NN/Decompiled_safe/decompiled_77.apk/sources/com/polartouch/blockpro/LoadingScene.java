package com.polartouch.blockpro;

import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.util.HorizontalAlign;

public class LoadingScene extends CustomScene {
    private FontLoader fl;

    public LoadingScene(int mLayerCount, BlockPro bp) {
        super(mLayerCount);
        this.fl = bp.fontloader;
    }

    public void onloadScene() {
        onloadSceneObjects();
    }

    /* access modifiers changed from: protected */
    public void onloadSceneObjects() {
        setBackground(new ColorBackground(0.0f, 0.0f, 0.0f));
        attachChild(new Text(180.0f, 400.0f, this.fl.mFont, "Loading ...", HorizontalAlign.CENTER));
    }

    public String toString() {
        return null;
    }

    public void onPause() {
    }

    public void onResume() {
    }

    public void pressBackButton() {
    }

    public void pressMenuButton() {
    }

    public Scene startWhenLoaded() {
        return null;
    }

    public void unload() {
    }
}
