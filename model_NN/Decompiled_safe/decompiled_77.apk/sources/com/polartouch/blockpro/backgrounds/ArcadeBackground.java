package com.polartouch.blockpro.backgrounds;

import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.game.GameScene;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;

public class ArcadeBackground extends Background {
    private Sprite backgroundSprite = new Sprite(0.0f, 0.0f, this.backgroundTR);
    private TextureRegion backgroundTR;

    public ArcadeBackground(BlockPro blockpro, GameScene gs) {
        super(blockpro, gs);
        this.staticBackground = true;
        this.mBgTexture = new Texture(512, 1024, TextureOptions.NEAREST);
        this.backgroundTR = TextureRegionFactory.createFromAsset(this.mBgTexture, blockpro, "bg/arcade1.jpg", 0, 0);
        gs.getChild(0).attachChild(this.backgroundSprite);
        loadTextures(this.mBgTexture);
    }
}
