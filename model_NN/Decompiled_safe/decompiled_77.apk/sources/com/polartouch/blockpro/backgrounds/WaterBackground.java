package com.polartouch.blockpro.backgrounds;

import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.Const;
import com.polartouch.blockpro.SmoothSprite;
import com.polartouch.blockpro.game.GameScene;
import com.polartouch.blockpro.game.GameTextureLoader;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.entity.particle.ParticleSystem;
import org.anddev.andengine.entity.particle.emitter.PointParticleEmitter;
import org.anddev.andengine.entity.particle.initializer.AlphaInitializer;
import org.anddev.andengine.entity.particle.initializer.ColorInitializer;
import org.anddev.andengine.entity.particle.initializer.RotationInitializer;
import org.anddev.andengine.entity.particle.initializer.VelocityInitializer;
import org.anddev.andengine.entity.particle.modifier.AlphaModifier;
import org.anddev.andengine.entity.particle.modifier.ColorModifier;
import org.anddev.andengine.entity.particle.modifier.ExpireModifier;
import org.anddev.andengine.entity.particle.modifier.ScaleModifier;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;

public class WaterBackground extends Background {
    private SmoothSprite bg;
    private SmoothSprite bg2;
    private SmoothSprite bg3;
    private SmoothSprite bg4;
    private SmoothSprite bglight;
    final int len = 2;
    private Texture mBgTexture3;
    private final float maxalpha = 0.4f;
    private float timer;
    private TextureRegion waterbg;
    private TextureRegion waterlight;
    private TextureRegion waterlines1;
    private TextureRegion waterlines2;
    private TextureRegion waterlines3;

    public WaterBackground(BlockPro blockpro, GameScene gs) {
        super(blockpro, gs);
        this.mBgTexture = new Texture(1024, 1024, TextureOptions.NEAREST);
        this.waterbg = TextureRegionFactory.createFromAsset(this.mBgTexture, blockpro, "bg/waterbg.png", 0, 0);
        this.waterlines1 = TextureRegionFactory.createFromAsset(this.mBgTexture, blockpro, "bg/waterlines.jpg", 482, 0);
        this.mBgTexture3 = new Texture(256, 512, TextureOptions.NEAREST);
        this.waterlight = TextureRegionFactory.createFromAsset(this.mBgTexture3, blockpro, "bg/waterlight_244_354.png", 0, 0);
        this.bg = new SmoothSprite(0.0f, 0.0f, this.waterbg);
        this.bg2 = new SmoothSprite(0.0f, 0.0f, this.waterlines1);
        this.bglight = new SmoothSprite(118.0f, 0.0f, this.waterlight);
        this.bg2.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 1);
        this.bglight.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 1);
        this.bg2.setAlpha(0.4f);
        this.bglight.setAlpha(0.9f);
        this.timer = 0.0f;
        gs.getChild(0).attachChild(this.bg);
        gs.getChild(1).attachChild(this.bg2);
        gs.getChild(2).attachChild(this.bglight);
        if (Const.getUseParticles(blockpro)) {
            addParticles();
        }
        if (this.prefsAnimating) {
            this.mBgTexture2 = new Texture(1024, 1024, TextureOptions.NEAREST);
            this.waterlines2 = TextureRegionFactory.createFromAsset(this.mBgTexture2, blockpro, "bg/waterlines2.jpg", 0, 0);
            this.waterlines3 = TextureRegionFactory.createFromAsset(this.mBgTexture2, blockpro, "bg/waterlines3.jpg", 500, 0);
            this.bg3 = new SmoothSprite(0.0f, 0.0f, this.waterlines2);
            this.bg4 = new SmoothSprite(0.0f, 0.0f, this.waterlines3);
            this.bg3.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 1);
            this.bg4.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 1);
            this.bg3.setAlpha(0.0f);
            this.bg4.setAlpha(0.0f);
            gs.getChild(1).attachChild(this.bg3);
            gs.getChild(1).attachChild(this.bg4);
            loadTextures(this.mBgTexture, this.mBgTexture2, this.mBgTexture3);
            storeSprites(this.bg, this.bg2, this.bg3, this.bg4, this.bglight);
        } else {
            storeSprites(this.bg, this.bg2, this.bglight);
            loadTextures(this.mBgTexture, this.mBgTexture3);
        }
        createUpdateHandler();
    }

    public void update() {
        if (this.prefsAnimating) {
            int period = (int) Math.floor((double) (this.timer / 2.0f));
            float rest = this.timer - ((float) (period * 2));
            switch (period) {
                case 0:
                    setAlphas(this.bg2, this.bg3, this.bg4, rest);
                    break;
                case 1:
                    setAlphas(this.bg3, this.bg4, this.bg2, rest);
                    break;
                case 2:
                    setAlphas(this.bg4, this.bg2, this.bg3, rest);
                    break;
            }
            this.timer += 0.1f;
            if (this.timer >= 6.0f) {
                this.timer = 0.0f;
            }
        }
    }

    private void setAlphas(SmoothSprite a, SmoothSprite b, SmoothSprite c, float rest) {
        a.setAlpha(((2.0f - rest) * 0.4f) / 2.0f);
        b.setAlpha((0.4f * rest) / 2.0f);
        c.setAlpha(0.0f);
    }

    private void createUpdateHandler() {
        this.updateHandler = new TimerHandler(0.1f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                WaterBackground.this.update();
            }
        });
    }

    public void addParticles() {
        PointParticleEmitter particleEmitter1 = new PointParticleEmitter(96.0f, 840.0f);
        PointParticleEmitter particleEmitter2 = new PointParticleEmitter(192.0f, 840.0f);
        PointParticleEmitter particleEmitter3 = new PointParticleEmitter(288.0f, 840.0f);
        PointParticleEmitter particleEmitter4 = new PointParticleEmitter(384.0f, 840.0f);
        createParticleSystem(particleEmitter1);
        createParticleSystem(particleEmitter2);
        createParticleSystem(particleEmitter3);
        createParticleSystem(particleEmitter4);
    }

    private void createParticleSystem(PointParticleEmitter ppe) {
        ParticleSystem particleSystem = new ParticleSystem(ppe, 0.1f, 0.4f, 10, GameTextureLoader.getInstance().bubble);
        particleSystem.addParticleInitializer(new ColorInitializer(1.0f, 1.0f, 1.0f));
        particleSystem.addParticleInitializer(new AlphaInitializer(0.3f));
        particleSystem.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 1);
        particleSystem.addParticleInitializer(new VelocityInitializer(-5.0f, 5.0f, -30.0f, -70.0f));
        particleSystem.addParticleInitializer(new RotationInitializer(0.0f));
        particleSystem.addParticleModifier(new ScaleModifier(0.4f, 1.0f, 0.0f, 30.0f));
        particleSystem.addParticleModifier(new ColorModifier(1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 30.0f));
        particleSystem.addParticleModifier(new AlphaModifier(0.3f, 0.0f, 20.0f, 30.0f));
        particleSystem.addParticleModifier(new ExpireModifier(30.0f));
        this.gs.getChild(2).attachChild(particleSystem);
    }
}
