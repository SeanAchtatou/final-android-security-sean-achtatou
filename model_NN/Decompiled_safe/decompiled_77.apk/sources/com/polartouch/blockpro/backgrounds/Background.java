package com.polartouch.blockpro.backgrounds;

import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.Const;
import com.polartouch.blockpro.game.GameScene;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.Texture;

public class Background {
    protected float baseX;
    protected float baseY;
    protected BlockPro blockpro;
    protected GameScene gs;
    protected Texture[] loadedTextures;
    protected int loadedTexturesCount;
    protected Texture mBgTexture;
    protected Texture mBgTexture2;
    protected boolean prefsAnimating;
    protected boolean staticBackground = false;
    private Sprite[] storedSprites;
    private int storedSpritesCount;
    protected TimerHandler updateHandler;

    public Background(BlockPro bp, GameScene gs2) {
        this.gs = gs2;
        this.blockpro = bp;
        this.baseY = 0.0f;
        this.baseX = 0.0f;
        this.prefsAnimating = Const.getUseAnimation(this.blockpro);
    }

    public boolean isLoadedToMemory() {
        for (int i = 0; i < this.loadedTexturesCount; i++) {
            if (!this.loadedTextures[i].isLoadedToHardware()) {
                return false;
            }
        }
        return true;
    }

    public boolean isLoadedToMemory(Texture... params) {
        for (Texture isLoadedToHardware : params) {
            if (!isLoadedToHardware.isLoadedToHardware()) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void loadTextures(Texture... params) {
        this.loadedTextures = params;
        this.loadedTexturesCount = params.length;
        if (!isLoadedToMemory(params)) {
            Const.log("loaded background " + this.loadedTexturesCount);
            for (int i = 0; i < this.loadedTexturesCount; i++) {
                this.blockpro.getEngine().getTextureManager().loadTexture(params[i]);
            }
        }
    }

    public void start() {
        if (!this.staticBackground) {
            this.gs.registerUpdateHandler(this.updateHandler);
        }
    }

    public void stop() {
        if (!this.staticBackground) {
            this.gs.unregisterUpdateHandler(this.updateHandler);
        }
    }

    /* access modifiers changed from: protected */
    public void storeSprites(Sprite... params) {
        this.storedSprites = params;
        this.storedSpritesCount = params.length;
    }

    public void unload() {
        Const.log("unloaded background " + this.loadedTexturesCount);
        this.blockpro.getEngine().getTextureManager().unloadTextures(this.mBgTexture);
        this.loadedTexturesCount = 0;
    }

    public void updateSpritePositions(float y) {
        for (int i = 0; i < this.storedSpritesCount; i++) {
            this.storedSprites[i].setPosition(this.storedSprites[i].getX(), y);
        }
    }
}
