package com.polartouch.blockpro.backgrounds;

import com.polartouch.blockpro.BlockPro;
import com.polartouch.blockpro.SmoothSprite;
import com.polartouch.blockpro.game.GameScene;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;

public class SkyBackground extends Background {
    private final SmoothSprite backgroundSprite = new SmoothSprite(0.0f, 0.0f, this.skyBg);
    private SmoothSprite backgroundSprite2;
    private SmoothSprite backgroundSprite3;
    private SmoothSprite backgroundSprite4;
    private final TextureRegion skyBg;
    private final TextureRegion skyBg2;

    public SkyBackground(BlockPro blockpro, GameScene gs) {
        super(blockpro, gs);
        this.mBgTexture = new Texture(1024, 1024, TextureOptions.NEAREST_PREMULTIPLYALPHA);
        this.skyBg = TextureRegionFactory.createFromAsset(this.mBgTexture, blockpro, "bg/sky1.jpg", 0, 0);
        this.skyBg2 = TextureRegionFactory.createFromAsset(this.mBgTexture, blockpro, "bg/sky2.png", 482, 0);
        this.backgroundSprite.setBlendFunction(6406, 771);
        gs.getChild(0).attachChild(this.backgroundSprite);
        if (this.prefsAnimating) {
            this.backgroundSprite2 = new SmoothSprite(0.0f, 798.0f, this.skyBg);
            this.backgroundSprite3 = new SmoothSprite(0.0f, -200.0f, this.skyBg2);
            this.backgroundSprite4 = new SmoothSprite(0.0f, 598.0f, this.skyBg2);
            this.backgroundSprite2.setBlendFunction(6406, 771);
            this.backgroundSprite3.setBlendFunction(6406, 771);
            this.backgroundSprite4.setBlendFunction(6406, 771);
            gs.getChild(0).attachChild(this.backgroundSprite2);
            gs.getChild(1).attachChild(this.backgroundSprite3);
            gs.getChild(1).attachChild(this.backgroundSprite4);
        }
        loadTextures(this.mBgTexture);
        createUpdateHandler();
    }

    private void createUpdateHandler() {
        this.updateHandler = new TimerHandler(0.05f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                SkyBackground.this.update();
            }
        });
    }

    private void moveSprites(SmoothSprite spriteA, SmoothSprite spriteB, float diff, float screenHeight, float speed) {
        SmoothSprite topSprite;
        SmoothSprite bottomSprite;
        Boolean rightOrder = Boolean.valueOf(spriteA.getY() < spriteB.getY());
        if (rightOrder.booleanValue()) {
            topSprite = spriteA;
        } else {
            topSprite = spriteB;
        }
        if (!rightOrder.booleanValue()) {
            bottomSprite = spriteA;
        } else {
            bottomSprite = spriteB;
        }
        float topY = topSprite.getY() - speed;
        float x = topSprite.getX();
        bottomSprite.setPosition(x, topY + diff);
        topSprite.setPosition(x, topY);
        if (topY <= (-diff)) {
            topSprite.setPosition(x, (2.0f * screenHeight) + topY);
        }
    }

    public void update() {
        if (this.prefsAnimating) {
            moveSprites(this.backgroundSprite, this.backgroundSprite2, this.baseX + 798.0f, this.baseY + 800.0f, 0.5f);
            moveSprites(this.backgroundSprite3, this.backgroundSprite4, this.baseX + 798.0f, this.baseY + 800.0f, 1.2f);
        }
    }
}
