package com.flurry.android;

import android.content.Context;

final class e implements Runnable {
    private /* synthetic */ Context a;
    private /* synthetic */ boolean b;
    private /* synthetic */ FlurryAgent c;

    e(FlurryAgent flurryAgent, Context context, boolean z) {
        this.c = flurryAgent;
        this.a = context;
        this.b = z;
    }

    public final void run() {
        if (!this.c.q) {
            FlurryAgent.a(this.c, this.a);
        }
        FlurryAgent.a(this.c, this.a, this.b);
    }
}
