package com.flurry.android;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

final class q implements View.OnClickListener {
    static String a = "FlurryAgent";
    static String b = "";
    private static volatile String c = "market://";
    private static volatile String d = "market://details?id=";
    private static volatile String e = "https://market.android.com/details?id=";
    private static String f = "com.flurry.android.ACTION_CATALOG";
    private static Random g = new Random(System.currentTimeMillis());
    private static int h = 5000;
    private static volatile int z = 0;
    private String i;
    private String j;
    private String k;
    private String l;
    private long m;
    private long n;
    private long o;
    private w p;
    private boolean q = true;
    private Map r = new HashMap();
    private Handler s;
    private boolean t;
    private transient Map u = new HashMap();
    private af v;
    private List w = new ArrayList();
    private Map x = new HashMap();
    /* access modifiers changed from: private */
    public AppCircleCallback y;

    static /* synthetic */ void a(q qVar, Context context, String str) {
        if (str.startsWith(d)) {
            String substring = str.substring(d.length());
            if (qVar.q) {
                try {
                    ah.a(a, "Launching Android Market for app " + substring);
                    context.startActivity(new Intent("android.intent.action.VIEW").setData(Uri.parse(str)));
                } catch (Exception e2) {
                    ah.c(a, "Cannot launch Marketplace url " + str, e2);
                }
            } else {
                ah.a(a, "Launching Android Market website for app " + substring);
                context.startActivity(new Intent("android.intent.action.VIEW").setData(Uri.parse(e + substring)));
            }
        } else {
            ah.d(a, "Unexpected android market url scheme: " + str);
        }
    }

    static {
        g.nextInt();
    }

    public q(Context context, a aVar) {
        this.i = aVar.f;
        this.j = aVar.g;
        this.k = aVar.a;
        this.l = aVar.b;
        this.m = aVar.c;
        this.n = aVar.d;
        this.o = aVar.e;
        this.s = aVar.h;
        this.v = new af(this.s, h);
        context.getResources().getDisplayMetrics();
        this.p = new w(context, this, aVar);
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(d + context.getPackageName()));
        this.q = packageManager.queryIntentActivities(intent, 65536).size() > 0;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.p.d();
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        this.p.e();
    }

    /* access modifiers changed from: package-private */
    public final void a(Map map, Map map2, Map map3, Map map4, Map map5, Map map6) {
        this.p.a(map, map2, map3, map4, map5, map6);
    }

    /* access modifiers changed from: package-private */
    public final long c() {
        return this.p.c();
    }

    /* access modifiers changed from: package-private */
    public final Set d() {
        return this.p.a();
    }

    /* access modifiers changed from: package-private */
    public final AdImage a(long j2) {
        return this.p.b(j2);
    }

    /* access modifiers changed from: package-private */
    public final List e() {
        return this.w;
    }

    /* access modifiers changed from: package-private */
    public final ag b(long j2) {
        return (ag) this.u.get(Long.valueOf(j2));
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        this.u.clear();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Context context, String str) {
        try {
            List a2 = a(Arrays.asList(str), (Long) null);
            if (a2 == null || a2.isEmpty()) {
                Intent intent = new Intent(f);
                intent.addCategory("android.intent.category.DEFAULT");
                context.startActivity(intent);
            } else {
                ag agVar = new ag(str, (byte) 2, SystemClock.elapsedRealtime() - this.o);
                agVar.b = (s) a2.get(0);
                a(agVar);
                b(context, agVar, this.i + a(agVar, Long.valueOf(System.currentTimeMillis())));
            }
        } catch (Exception e2) {
            ah.d(a, "Failed to launch promotional canvas for hook: " + str, e2);
        }
        return;
    }

    /* access modifiers changed from: package-private */
    public final void a(AppCircleCallback appCircleCallback) {
        this.y = appCircleCallback;
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z2) {
        this.t = z2;
    }

    /* access modifiers changed from: package-private */
    public final boolean g() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public final String h() {
        return this.i;
    }

    static void a(String str) {
        f = str;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Context context, ag agVar, String str) {
        this.s.post(new aj(this, str, context, agVar));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0052, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0053, code lost:
        com.flurry.android.ah.c(com.flurry.android.q.a, "Unknown host: " + r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0071, code lost:
        if (r7.y != null) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0073, code lost:
        e("Unknown host: " + r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00ab, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00ac, code lost:
        r6 = r1;
        r1 = r0;
        r0 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0052 A[ExcHandler: UnknownHostException (r0v3 'e' java.net.UnknownHostException A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String d(java.lang.String r8) {
        /*
            r7 = this;
            r5 = 0
            java.lang.String r0 = com.flurry.android.q.c     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            boolean r0 = r8.startsWith(r0)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            if (r0 != 0) goto L_0x0050
            org.apache.http.client.methods.HttpGet r0 = new org.apache.http.client.methods.HttpGet     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            r0.<init>(r8)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            org.apache.http.impl.client.DefaultHttpClient r1 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            r1.<init>()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            org.apache.http.HttpResponse r0 = r1.execute(r0)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            org.apache.http.StatusLine r1 = r0.getStatusLine()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            int r1 = r1.getStatusCode()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x0038
            org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.String r0 = org.apache.http.util.EntityUtils.toString(r0)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.String r1 = com.flurry.android.q.c     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x00ab }
            boolean r1 = r0.startsWith(r1)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x00ab }
            if (r1 != 0) goto L_0x0037
            java.lang.String r0 = r7.d(r0)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x00ab }
        L_0x0037:
            return r0
        L_0x0038:
            java.lang.String r0 = com.flurry.android.q.a     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            r2.<init>()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.String r3 = "Cannot process with responseCode "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.String r1 = r1.toString()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            com.flurry.android.ah.c(r0, r1)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
        L_0x0050:
            r0 = r8
            goto L_0x0037
        L_0x0052:
            r0 = move-exception
            java.lang.String r1 = com.flurry.android.q.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Unknown host: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r0.getMessage()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.flurry.android.ah.c(r1, r2)
            com.flurry.android.AppCircleCallback r1 = r7.y
            if (r1 == 0) goto L_0x008d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown host: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            r7.e(r0)
        L_0x008d:
            r0 = r5
            goto L_0x0037
        L_0x008f:
            r0 = move-exception
            r1 = r8
        L_0x0091:
            java.lang.String r2 = com.flurry.android.q.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Failed on url: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            com.flurry.android.ah.c(r2, r1, r0)
            r0 = r5
            goto L_0x0037
        L_0x00ab:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0091
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.q.d(java.lang.String):java.lang.String");
    }

    /* access modifiers changed from: private */
    public void e(String str) {
        a(new ac(this, str));
    }

    /* access modifiers changed from: package-private */
    public final synchronized Offer b(String str) {
        Offer offer;
        List a2 = a(Arrays.asList(str), (Long) null);
        if (a2 == null || a2.isEmpty()) {
            offer = null;
        } else {
            offer = a(str, (s) a2.get(0));
            ah.a(a, "Impression for offer with ID " + offer.getId());
        }
        return offer;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Context context, long j2) {
        OfferInSdk offerInSdk = (OfferInSdk) this.x.get(Long.valueOf(j2));
        if (offerInSdk == null) {
            ah.b(a, "Cannot find offer " + j2);
        } else {
            ag agVar = offerInSdk.b;
            agVar.a(new j((byte) 4, i()));
            ah.a(a, "Offer " + offerInSdk.a + " accepted");
            a(context, agVar, offerInSdk.f);
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized List c(String str) {
        List list;
        if (!this.p.b()) {
            list = Collections.emptyList();
        } else {
            s[] a2 = this.p.a(str);
            ArrayList arrayList = new ArrayList();
            if (a2 != null && a2.length > 0) {
                for (s a3 : a2) {
                    arrayList.add(a(str, a3));
                }
            }
            ah.a(a, "Impressions for " + arrayList.size() + " offers.");
            list = arrayList;
        }
        return list;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            this.x.remove((Long) it.next());
        }
    }

    private Offer a(String str, s sVar) {
        ag agVar = new ag(str, (byte) 3, i());
        a(agVar);
        agVar.a(new j((byte) 2, i()));
        agVar.b = sVar;
        ak a2 = this.p.a(sVar.a);
        String str2 = a2 == null ? "" : a2.a;
        int i2 = a2 == null ? 0 : a2.c;
        int i3 = z + 1;
        z = i3;
        OfferInSdk offerInSdk = new OfferInSdk((long) i3, FlurryAgent.c() + a(agVar, Long.valueOf(agVar.a())), sVar.h, sVar.d, str2, i2);
        offerInSdk.b = agVar;
        this.x.put(Long.valueOf(offerInSdk.a), offerInSdk);
        return new Offer(offerInSdk.a, offerInSdk.f, offerInSdk.g, offerInSdk.c, offerInSdk.d, offerInSdk.e);
    }

    /* access modifiers changed from: package-private */
    public final synchronized List a(Context context, List list, Long l2, int i2, boolean z2) {
        List emptyList;
        if (!this.p.b() || list == null) {
            emptyList = Collections.emptyList();
        } else {
            List a2 = a(list, l2);
            int min = Math.min(list.size(), a2.size());
            ArrayList arrayList = new ArrayList();
            for (int i3 = 0; i3 < min; i3++) {
                String str = (String) list.get(i3);
                f b2 = this.p.b(str);
                if (b2 != null) {
                    ag agVar = new ag((String) list.get(i3), (byte) 1, i());
                    a(agVar);
                    if (i3 < a2.size()) {
                        agVar.b = (s) a2.get(i3);
                        agVar.a(new j((byte) 2, i()));
                        v vVar = new v(context, this, agVar, b2, i2, z2);
                        vVar.a(a(agVar, (Long) null));
                        arrayList.add(vVar);
                    }
                } else {
                    ah.d(a, "Cannot find hook: " + str);
                }
            }
            emptyList = arrayList;
        }
        return emptyList;
    }

    /* access modifiers changed from: package-private */
    public final synchronized View a(Context context, String str, int i2) {
        ad adVar;
        adVar = new ad(this, context, str, i2);
        this.v.a(adVar);
        return adVar;
    }

    private void a(ag agVar) {
        if (this.w.size() < 32767) {
            this.w.add(agVar);
            this.u.put(Long.valueOf(agVar.a()), agVar);
        }
    }

    private List a(List list, Long l2) {
        if (!this.p.b() && list != null && !list.isEmpty()) {
            return Collections.emptyList();
        }
        s[] a2 = this.p.a((String) list.get(0));
        if (a2 == null || a2.length <= 0) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(Arrays.asList(a2));
        Collections.shuffle(arrayList);
        if (l2 != null) {
            Iterator it = arrayList.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((s) it.next()).a == l2.longValue()) {
                        it.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        return arrayList.subList(0, Math.min(arrayList.size(), list.size()));
    }

    /* access modifiers changed from: package-private */
    public final long i() {
        return SystemClock.elapsedRealtime() - this.o;
    }

    public final synchronized void onClick(View view) {
        v vVar = (v) view;
        ag a2 = vVar.a();
        a2.a(new j((byte) 4, i()));
        if (this.t) {
            b(view.getContext(), a2, vVar.b(this.i));
        } else {
            a(view.getContext(), a2, vVar.b(this.j));
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str, String str2) {
        this.r.put(str, str2);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void j() {
        this.r.clear();
    }

    private static void b(Context context, ag agVar, String str) {
        Intent intent = new Intent(f);
        intent.addCategory("android.intent.category.DEFAULT");
        intent.putExtra("u", str);
        if (agVar != null) {
            intent.putExtra("o", agVar.a());
        }
        context.startActivity(intent);
    }

    private String a(ag agVar, Long l2) {
        s sVar = agVar.b;
        StringBuilder append = new StringBuilder().append("?apik=").append(this.k).append("&cid=").append(sVar.e).append("&adid=").append(sVar.a).append("&pid=").append(this.l).append("&iid=").append(this.m).append("&sid=").append(this.n).append("&its=").append(agVar.a()).append("&hid=").append(i.a(agVar.a)).append("&ac=").append(a(sVar.g));
        if (this.r != null && !this.r.isEmpty()) {
            for (Map.Entry entry : this.r.entrySet()) {
                append.append("&").append("c_" + i.a((String) entry.getKey())).append("=").append(i.a((String) entry.getValue()));
            }
        }
        append.append("&ats=");
        if (l2 != null) {
            append.append(l2);
        }
        return append.toString();
    }

    private static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < bArr.length; i2++) {
            int i3 = (bArr[i2] >> 4) & 15;
            if (i3 < 10) {
                sb.append((char) (i3 + 48));
            } else {
                sb.append((char) ((i3 + 65) - 10));
            }
            byte b2 = bArr[i2] & 15;
            if (b2 < 10) {
                sb.append((char) (b2 + 48));
            } else {
                sb.append((char) ((b2 + 65) - 10));
            }
        }
        return sb.toString();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[adLogs=").append(this.w).append("]");
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public final AdImage k() {
        return this.p.a((short) 1);
    }

    private static void a(Runnable runnable) {
        new Handler().post(runnable);
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2) {
        if (this.y != null) {
            a(new ab(this, i2));
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean l() {
        return this.p.b();
    }
}
