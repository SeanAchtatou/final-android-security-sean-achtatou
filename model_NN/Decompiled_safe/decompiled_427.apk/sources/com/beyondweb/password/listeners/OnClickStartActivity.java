package com.beyondweb.password.listeners;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

public class OnClickStartActivity implements View.OnClickListener {
    private Class<?> destination;
    private final Activity from;

    public OnClickStartActivity(Activity from2, Class<?> destination2) {
        this.from = from2;
        this.destination = destination2;
    }

    public void onClick(View v) {
        this.from.startActivity(new Intent(v.getContext(), this.destination));
    }
}
