package com.beyondweb.password;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.beyondweb.password.demo.R;
import com.beyondweb.password.util.Passwords;

public class Interlude extends Activity {
    /* access modifiers changed from: private */
    public int currentLevel;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        findLevel();
        prepareInterlude();
        setNextLevelButton();
        MediaPlayer.create(this, (int) R.raw.levelwin).start();
    }

    private void findLevel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.currentLevel = extras.getInt("currentLevel");
        } else {
            this.currentLevel = 0;
        }
    }

    private void prepareInterlude() {
        setContentView((int) R.layout.level_win);
        ((TextView) findViewById(R.id.WinMessage)).setText("The password of level " + this.currentLevel + " is:");
        ((TextView) findViewById(R.id.Password)).setText(new Passwords(this).getLevel(this.currentLevel).getPassword());
    }

    private void setNextLevelButton() {
        ((Button) findViewById(R.id.NextLevel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Game.class);
                intent.putExtra("currentLevel", Interlude.this.currentLevel + 1);
                Interlude.this.startActivity(intent);
                Interlude.this.finish();
            }
        });
    }
}
