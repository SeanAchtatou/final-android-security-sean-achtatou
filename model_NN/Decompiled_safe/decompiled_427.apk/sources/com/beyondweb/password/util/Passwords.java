package com.beyondweb.password.util;

import android.content.Context;
import com.beyondweb.password.Level;
import com.beyondweb.password.demo.R;
import java.util.ArrayList;
import java.util.List;

public class Passwords {
    private List<Level> passwords;

    public Passwords(Context c) {
        boolean free = c.getResources().getBoolean(R.bool.free);
        ArrayList<Level> passwords2 = new ArrayList<>();
        passwords2.add(new Level("password", R.drawable.picture_level0, R.drawable.hint_level0, "Level 0 - Read instructions first!"));
        passwords2.add(new Level("two", R.drawable.picture_level1, R.drawable.hint_level1, "Level 1 - Easy just to begin"));
        passwords2.add(new Level("pi", R.drawable.picture_level2, R.drawable.hint_level2, "Level 2 - Be irrational!"));
        passwords2.add(new Level("euler", R.drawable.picture_level3, R.drawable.hint_level3, "Level 3 - Another constant?"));
        passwords2.add(new Level("linux", R.drawable.picture_level4, R.drawable.hint_level4, "Level 4 - Universal = Open = Free?"));
        passwords2.add(new Level("guitar", R.drawable.picture_level5, R.drawable.hint_level5, "Level 5 - BEADGE"));
        passwords2.add(new Level("limit", R.drawable.picture_level6, R.drawable.hint_level6, "Level 6 - For all... What!?"));
        passwords2.add(new Level("diamond", R.drawable.picture_level7, R.drawable.hint_level7, "Level 7 - I'm very hard!"));
        passwords2.add(new Level("fermat", R.drawable.picture_level8, R.drawable.hint_level8, "Level 8 - x^n + y^n = z^n"));
        passwords2.add(new Level("nickel", R.drawable.picture_level9, R.drawable.hint_level9, "Level 9 - Knights who say NI!"));
        passwords2.add(new Level("google", R.drawable.picture_level10, R.drawable.hint_level10, "Level 10 - I'm feeling lucky!"));
        passwords2.add(new Level("jesus", R.drawable.picture_level11, R.drawable.hint_level11, "Level 11 - Birthday?"));
        passwords2.add(new Level("crash", R.drawable.picture_level12, R.drawable.hint_level12, "Level 12 - ????"));
        if (!free) {
            passwords2.add(new Level("helloworld", R.drawable.picture_level13, R.drawable.hint_level13, "Level 13 - F*ck your brain!"));
            passwords2.add(new Level("ramanujan", R.drawable.picture_level14, R.drawable.hint_level14, "Level 14 - f(x, y) = x^3 + y^3"));
            passwords2.add(new Level("restaurant", R.drawable.picture_level15, R.drawable.hint_level15, "Level 15 - At the end of the universe"));
            passwords2.add(new Level("winter", R.drawable.picture_level16, R.drawable.hint_level16, "Level 16 - Vivaldi"));
            passwords2.add(new Level("inferno", R.drawable.picture_level17, R.drawable.hint_level17, "Level 17 - I am hot!"));
            passwords2.add(new Level("asu", R.drawable.picture_level18, R.drawable.hint_level18, "Level 18 - I want you!"));
            passwords2.add(new Level("yellow", R.drawable.picture_level19, R.drawable.hint_level19, "Level 19 - 98 : -16 : 93"));
            passwords2.add(new Level("lazarus", R.drawable.picture_level20, R.drawable.hint_level20, "Level 20 - Just four days"));
            passwords2.add(new Level("heap", R.drawable.picture_level21, R.drawable.hint_level21, "Level 21 - I am your father!"));
            passwords2.add(new Level("nirvana", R.drawable.picture_level22, R.drawable.hint_level22, "Level 22 - You rock!"));
            passwords2.add(new Level("derwent", R.drawable.picture_level23, R.drawable.hint_level23, "Level 23 - Born in 1955."));
            passwords2.add(new Level("markov", R.drawable.picture_level24, R.drawable.hint_level24, "Level 24 - A chain?"));
            passwords2.add(new Level("war", R.drawable.picture_level25, R.drawable.hint_level25, "Level 25 - Janken...!"));
            passwords2.add(new Level("life", R.drawable.picture_level26, R.drawable.hint_level26, "Level 26 - 42"));
            passwords2.add(new Level("seppuku", R.drawable.picture_level27, R.drawable.hint_level27, "Level 27 - Honor"));
            passwords2.add(new Level("higgs", R.drawable.picture_level28, R.drawable.hint_level28, "Level 28 - Where are you?"));
            passwords2.add(new Level("", R.drawable.picture_level29, R.drawable.hint_level29, "Level 29 - void"));
            passwords2.add(new Level("zeppelin", R.drawable.picture_level30, R.drawable.hint_level30, "Level 30 - 1968"));
            passwords2.add(new Level("structure", R.drawable.picture_level31, R.drawable.hint_level31, "Level 31 - Is this a tree?"));
            passwords2.add(new Level("qwerty", R.drawable.picture_level32, R.drawable.hint_level32, "Level 32 - How cute! Press me... Please?"));
            passwords2.add(new Level("hide", R.drawable.picture_level33, R.drawable.hint_level33, "Level 33 - On guitaaar... ?"));
            passwords2.add(new Level("knuth", R.drawable.picture_level34, R.drawable.hint_level34, "Level 34 - Reward"));
            passwords2.add(new Level("pegasus", R.drawable.picture_level35, R.drawable.hint_level35, "Level 35 - Look up to the skies and see"));
            passwords2.add(new Level("straight", R.drawable.picture_level36, R.drawable.hint_level36, "Level 36 - 23456"));
            passwords2.add(new Level("java", R.drawable.picture_level37, R.drawable.hint_level37, "Level 37 - My language"));
            passwords2.add(new Level("prince", R.drawable.picture_level38, R.drawable.hint_level38, "Level 37 - A Hat"));
        }
        this.passwords = passwords2;
    }

    public Level getLevel(int level) {
        if (level < this.passwords.size()) {
            return this.passwords.get(level);
        }
        return this.passwords.get(0);
    }

    public String getPasswordOfLevel(int level) {
        if (level < this.passwords.size()) {
            return this.passwords.get(level).getPassword();
        }
        return "";
    }

    public int size() {
        return this.passwords.size();
    }

    public List<Level> getPasswords() {
        return this.passwords;
    }
}
