package com.beyondweb.password;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import com.beyondweb.password.demo.R;
import com.beyondweb.password.util.Passwords;
import java.util.ArrayList;
import java.util.List;

public class ChooseLevel extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.choose_level);
        final Spinner levels = (Spinner) findViewById(R.id.Levels);
        List<Level> passwords = new ArrayList<>();
        passwords.addAll(new Passwords(this).getPasswords());
        passwords.remove(0);
        levels.setAdapter((SpinnerAdapter) new ArrayAdapter<>(this, 17367057, passwords));
        ((Button) findViewById(R.id.GoToLevel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int prevLevel = levels.getSelectedItemPosition();
                EditText input = (EditText) ChooseLevel.this.findViewById(R.id.PasswordInput);
                if (input == null || !input.getText().toString().equals(new Passwords(ChooseLevel.this).getPasswordOfLevel(prevLevel))) {
                    Toast.makeText(v.getContext(), "Wrong password...", 0).show();
                    return;
                }
                Intent intent = new Intent(v.getContext(), Game.class);
                intent.putExtra("currentLevel", prevLevel + 1);
                ChooseLevel.this.startActivity(intent);
            }
        });
    }
}
