package com.beyondweb.password;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;
import com.beyondweb.password.demo.R;
import com.beyondweb.password.util.Passwords;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class Game extends Activity {
    private static final String MY_AD_UNIT_ID = "a14dc875176affc";
    /* access modifiers changed from: private */
    public int currentLevel;
    /* access modifiers changed from: private */
    public int tries = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        findLevel();
        prepareLevel();
        setSendButton();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Return to home");
        menu.add("Find more hints on our forum");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if ("Return to home".equals(item.getTitle())) {
            startActivity(new Intent(this, Home.class));
        } else {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://password.beyondwebstudio.com/forum")));
        }
        return super.onOptionsItemSelected(item);
    }

    private void findLevel() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.currentLevel = extras.getInt("currentLevel");
        } else {
            this.currentLevel = 0;
        }
    }

    private void prepareLevel() {
        Passwords passwords = new Passwords(this);
        setContentView((int) R.layout.question);
        ((ImageView) findViewById(R.id.Hint)).setImageResource(passwords.getLevel(this.currentLevel).getHint());
        ((ImageView) findViewById(R.id.Frame)).setImageResource(passwords.getLevel(this.currentLevel).getImage());
        if (getResources().getBoolean(R.bool.free)) {
            AdView adView = new AdView(this, AdSize.BANNER, MY_AD_UNIT_ID);
            adView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            adView.setGravity(53);
            ((FrameLayout) findViewById(R.id.question)).addView(adView);
            adView.loadAd(new AdRequest());
        }
    }

    private void setSendButton() {
        ((Button) findViewById(R.id.SendAnswer)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent;
                EditText passwordInput = (EditText) Game.this.findViewById(R.id.PasswordInput);
                if (passwordInput == null || !passwordInput.getText().toString().equals(new Passwords(Game.this).getPasswordOfLevel(Game.this.currentLevel))) {
                    if (Game.this.tries < 5) {
                        Toast.makeText(v.getContext(), "Wrong answer...", 0).show();
                    } else {
                        Toast.makeText(v.getContext(), "Get more hints on our forum ( press menu )", 0).show();
                    }
                    Game game = Game.this;
                    game.tries = game.tries + 1;
                    return;
                }
                if (Game.this.currentLevel == new Passwords(Game.this).size() - 1) {
                    intent = new Intent(v.getContext(), GameEnd.class);
                } else {
                    intent = new Intent(v.getContext(), Interlude.class);
                    intent.putExtra("currentLevel", Game.this.currentLevel);
                    Game.this.saveState();
                }
                Game.this.startActivity(intent);
                Game.this.finish();
            }
        });
    }

    /* access modifiers changed from: private */
    public void saveState() {
        SharedPreferences settings = getSharedPreferences("passwordPrefs", 0);
        SharedPreferences.Editor editor = settings.edit();
        if (settings.getInt("latestLevel", 0) < this.currentLevel) {
            editor.putInt("latestLevel", this.currentLevel);
            editor.commit();
        }
    }
}
