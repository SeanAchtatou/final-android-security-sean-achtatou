package com.beyondweb.password;

public class Level {
    private final int hint;
    private final int image;
    private final String levelDescription;
    private final String password;

    public Level(String password2, int image2, int hint2, String levelDescription2) {
        this.password = password2;
        this.image = image2;
        this.levelDescription = levelDescription2;
        this.hint = hint2;
    }

    public String getLevelDescription() {
        return this.levelDescription;
    }

    public String getPassword() {
        return this.password;
    }

    public int getImage() {
        return this.image;
    }

    public int getHint() {
        return this.hint;
    }

    public String toString() {
        return this.levelDescription;
    }
}
