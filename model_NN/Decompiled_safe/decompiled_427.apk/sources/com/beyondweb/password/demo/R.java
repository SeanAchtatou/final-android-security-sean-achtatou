package com.beyondweb.password.demo;

public final class R {

    public static final class attr {
    }

    public static final class bool {
        public static final int free = 2131034112;
    }

    public static final class drawable {
        public static final int hint_level0 = 2130837504;
        public static final int hint_level1 = 2130837505;
        public static final int hint_level10 = 2130837506;
        public static final int hint_level11 = 2130837507;
        public static final int hint_level12 = 2130837508;
        public static final int hint_level13 = 2130837509;
        public static final int hint_level14 = 2130837510;
        public static final int hint_level15 = 2130837511;
        public static final int hint_level16 = 2130837512;
        public static final int hint_level17 = 2130837513;
        public static final int hint_level18 = 2130837514;
        public static final int hint_level19 = 2130837515;
        public static final int hint_level2 = 2130837516;
        public static final int hint_level20 = 2130837517;
        public static final int hint_level21 = 2130837518;
        public static final int hint_level22 = 2130837519;
        public static final int hint_level23 = 2130837520;
        public static final int hint_level24 = 2130837521;
        public static final int hint_level25 = 2130837522;
        public static final int hint_level26 = 2130837523;
        public static final int hint_level27 = 2130837524;
        public static final int hint_level28 = 2130837525;
        public static final int hint_level29 = 2130837526;
        public static final int hint_level3 = 2130837527;
        public static final int hint_level30 = 2130837528;
        public static final int hint_level31 = 2130837529;
        public static final int hint_level32 = 2130837530;
        public static final int hint_level33 = 2130837531;
        public static final int hint_level34 = 2130837532;
        public static final int hint_level35 = 2130837533;
        public static final int hint_level36 = 2130837534;
        public static final int hint_level37 = 2130837535;
        public static final int hint_level38 = 2130837536;
        public static final int hint_level4 = 2130837537;
        public static final int hint_level5 = 2130837538;
        public static final int hint_level6 = 2130837539;
        public static final int hint_level7 = 2130837540;
        public static final int hint_level8 = 2130837541;
        public static final int hint_level9 = 2130837542;
        public static final int icon = 2130837543;
        public static final int imagem_home = 2130837544;
        public static final int instructions = 2130837545;
        public static final int logo = 2130837546;
        public static final int picture_level0 = 2130837547;
        public static final int picture_level1 = 2130837548;
        public static final int picture_level10 = 2130837549;
        public static final int picture_level11 = 2130837550;
        public static final int picture_level12 = 2130837551;
        public static final int picture_level13 = 2130837552;
        public static final int picture_level14 = 2130837553;
        public static final int picture_level15 = 2130837554;
        public static final int picture_level16 = 2130837555;
        public static final int picture_level17 = 2130837556;
        public static final int picture_level18 = 2130837557;
        public static final int picture_level19 = 2130837558;
        public static final int picture_level2 = 2130837559;
        public static final int picture_level20 = 2130837560;
        public static final int picture_level21 = 2130837561;
        public static final int picture_level22 = 2130837562;
        public static final int picture_level23 = 2130837563;
        public static final int picture_level24 = 2130837564;
        public static final int picture_level25 = 2130837565;
        public static final int picture_level26 = 2130837566;
        public static final int picture_level27 = 2130837567;
        public static final int picture_level28 = 2130837568;
        public static final int picture_level29 = 2130837569;
        public static final int picture_level3 = 2130837570;
        public static final int picture_level30 = 2130837571;
        public static final int picture_level31 = 2130837572;
        public static final int picture_level32 = 2130837573;
        public static final int picture_level33 = 2130837574;
        public static final int picture_level34 = 2130837575;
        public static final int picture_level35 = 2130837576;
        public static final int picture_level36 = 2130837577;
        public static final int picture_level37 = 2130837578;
        public static final int picture_level38 = 2130837579;
        public static final int picture_level4 = 2130837580;
        public static final int picture_level5 = 2130837581;
        public static final int picture_level6 = 2130837582;
        public static final int picture_level7 = 2130837583;
        public static final int picture_level8 = 2130837584;
        public static final int picture_level9 = 2130837585;
    }

    public static final class id {
        public static final int Buy = 2131165188;
        public static final int ChooseLevel = 2131165196;
        public static final int Continue = 2131165195;
        public static final int Forum = 2131165198;
        public static final int Frame = 2131165193;
        public static final int GoToHome = 2131165189;
        public static final int GoToLevel = 2131165187;
        public static final int Hint = 2131165200;
        public static final int Instructions = 2131165197;
        public static final int Levels = 2131165185;
        public static final int Logo = 2131165184;
        public static final int NewGame = 2131165194;
        public static final int NextLevel = 2131165192;
        public static final int Password = 2131165191;
        public static final int PasswordInput = 2131165186;
        public static final int SendAnswer = 2131165201;
        public static final int WinMessage = 2131165190;
        public static final int question = 2131165199;
    }

    public static final class layout {
        public static final int choose_level = 2130903040;
        public static final int game_end = 2130903041;
        public static final int instructions = 2130903042;
        public static final int level_win = 2130903043;
        public static final int main = 2130903044;
        public static final int question = 2130903045;
    }

    public static final class raw {
        public static final int levelwin = 2130968576;
    }

    public static final class string {
        public static final int all_level_cleared = 2131099653;
        public static final int app_name = 2131099648;
        public static final int choose_level = 2131099659;
        public static final int congratulations = 2131099652;
        public static final int continue_game = 2131099658;
        public static final int enter = 2131099649;
        public static final int go_to_home = 2131099655;
        public static final int go_to_level = 2131099651;
        public static final int instructions = 2131099660;
        public static final int new_game = 2131099657;
        public static final int next_level = 2131099656;
        public static final int password_previous_level = 2131099650;
        public static final int thank_you = 2131099654;
    }
}
