package com.beyondweb.password;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.beyondweb.password.demo.R;
import com.beyondweb.password.listeners.OnClickStartActivity;
import com.beyondweb.password.util.Passwords;

public class Home extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        setupButton(R.id.NewGame, Game.class);
        setupButton(R.id.Instructions, Instructions.class);
        setupButton(R.id.ChooseLevel, ChooseLevel.class);
        ((Button) findViewById(R.id.Continue)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent;
                int latestLevel = Home.this.getSharedPreferences("passwordPrefs", 0).getInt("latestLevel", -1);
                if (latestLevel == new Passwords(Home.this).size() - 1) {
                    intent = new Intent(v.getContext(), GameEnd.class);
                } else {
                    intent = new Intent(v.getContext(), Game.class);
                    intent.putExtra("currentLevel", latestLevel + 1);
                }
                Home.this.startActivity(intent);
            }
        });
        findViewById(R.id.Forum).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Home.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://password.beyondwebstudio.com/forum")));
            }
        });
    }

    private void setupButton(int id, Class<?> cls) {
        ((Button) findViewById(id)).setOnClickListener(new OnClickStartActivity(this, cls));
    }
}
