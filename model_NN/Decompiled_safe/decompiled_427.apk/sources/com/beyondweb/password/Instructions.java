package com.beyondweb.password;

import android.app.Activity;
import android.os.Bundle;
import com.beyondweb.password.demo.R;

public class Instructions extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.instructions);
    }
}
