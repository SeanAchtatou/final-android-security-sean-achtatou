package com.beyondweb.password;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.beyondweb.password.demo.R;

public class GameEnd extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.game_end);
        ((Button) findViewById(R.id.GoToHome)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GameEnd.this.startActivity(new Intent(v.getContext(), Home.class));
            }
        });
        ((Button) findViewById(R.id.Buy)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GameEnd.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.beyondweb.password")));
            }
        });
    }
}
