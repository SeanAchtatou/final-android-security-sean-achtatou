package dialog;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.TitleView;

public final class DialogScreenPuzzleInfo extends DialogScreenBase {
    private static final int BUTTON_ID_OK = 0;
    private final View.OnClickListener m_hOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case 0:
                    break;
                default:
                    throw new RuntimeException("Invalid view id");
            }
            DialogScreenPuzzleInfo.this.m_hDialogManager.dismiss();
        }
    };
    private final TextView m_tvInfo;

    public DialogScreenPuzzleInfo(Activity hActivity, DialogManager hDialogManager) {
        super(hActivity, hDialogManager);
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        TitleView hTitle = new TitleView(hActivity, hMetrics);
        hTitle.setTitle(ResString.dialog_screen_puzzle_info_title);
        addView(hTitle);
        RelativeLayout vgContent = new RelativeLayout(hActivity);
        vgContent.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        addView(vgContent);
        ScrollView svScrollView = new ScrollView(hActivity);
        RelativeLayout.LayoutParams hScrollViewParams = new RelativeLayout.LayoutParams(-1, -2);
        hScrollViewParams.addRule(13);
        svScrollView.setLayoutParams(hScrollViewParams);
        vgContent.addView(svScrollView);
        this.m_tvInfo = new TextView(hActivity);
        this.m_tvInfo.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.m_tvInfo.setGravity(17);
        this.m_tvInfo.setTextColor(-1);
        this.m_tvInfo.setTextSize(1, 18.0f);
        int iDip20 = ResDimens.getDip(hMetrics, 20);
        this.m_tvInfo.setPadding(iDip20, iDip20, iDip20, iDip20);
        this.m_tvInfo.setAutoLinkMask(15);
        svScrollView.addView(this.m_tvInfo);
        ButtonContainer vgButtonContainer = new ButtonContainer(hActivity);
        vgButtonContainer.createButton(0, "btn_img_ok", this.m_hOnClick);
        addView(vgButtonContainer);
    }

    public void onShow(String sPuzzleInfo) {
        this.m_tvInfo.setText(sPuzzleInfo);
    }

    public void cleanUp() {
        super.cleanUp();
        this.m_tvInfo.setText((CharSequence) null);
    }
}
