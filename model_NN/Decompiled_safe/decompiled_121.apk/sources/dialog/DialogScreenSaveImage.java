package dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cfg.Option;
import helper.Global;
import helper.ImageSaver;
import res.ResDimens;
import res.ResString;
import screen.ScreenPlayBase;
import view.ButtonContainer;
import view.CustomButton;
import view.TitleView;

public final class DialogScreenSaveImage extends DialogScreenBase {
    private static final int BUTTON_ID_OK = 1;
    private static final int BUTTON_ID_VIEW_IMAGE = 0;
    /* access modifiers changed from: private */
    public final CustomButton m_btnViewImage;
    /* access modifiers changed from: private */
    public final Activity m_hActivity;
    private final View.OnClickListener m_hOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case 0:
                    if (DialogScreenSaveImage.this.m_hViewImageIntent != null) {
                        DialogScreenSaveImage.this.m_hActivity.startActivity(DialogScreenSaveImage.this.m_hViewImageIntent);
                        return;
                    }
                    return;
                case 1:
                    DialogScreenSaveImage.this.m_hDialogManager.dismiss();
                    if (DialogScreenSaveImage.this.m_hResultData != null && DialogScreenSaveImage.this.m_hOnDismiss != null) {
                        DialogScreenSaveImage.this.m_hHandler.post(new Runnable() {
                            public void run() {
                                DialogScreenSaveImage.this.m_hDialogManager.showPuzzleSolved(DialogScreenSaveImage.this.m_hResultData, DialogScreenSaveImage.this.m_hOnDismiss);
                            }
                        });
                        return;
                    }
                    return;
                default:
                    throw new RuntimeException("Invalid view id");
            }
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnDismissListener m_hOnDismiss;
    /* access modifiers changed from: private */
    public ScreenPlayBase.ResultData m_hResultData;
    /* access modifiers changed from: private */
    public MediaScannerConnection m_hScanner;
    /* access modifiers changed from: private */
    public final MediaScannerConnection.MediaScannerConnectionClient m_hScannerConnectionClient = new MediaScannerConnection.MediaScannerConnectionClient() {
        public void onMediaScannerConnected() {
            DialogScreenSaveImage.this.m_hScanner.scanFile(DialogScreenSaveImage.this.m_sImageFilePath, "image/jpeg");
        }

        public void onScanCompleted(String path, Uri uri) {
            if (uri != null) {
                try {
                    DialogScreenSaveImage.this.m_hViewImageIntent = new Intent("android.intent.action.VIEW");
                    DialogScreenSaveImage.this.m_hViewImageIntent.setDataAndType(uri, "image/jpeg");
                } catch (Throwable th) {
                    DialogScreenSaveImage.this.m_hScanner.disconnect();
                    throw th;
                }
            }
            DialogScreenSaveImage.this.m_hScanner.disconnect();
            DialogScreenSaveImage.this.m_hHandler.post(new Runnable() {
                public void run() {
                    DialogScreenSaveImage.this.m_pbProgress.setVisibility(8);
                    DialogScreenSaveImage.this.m_tvImageSaveState.setText(ResString.dialog_screen_save_image_image_saved);
                    DialogScreenSaveImage.this.m_tvImageFilePath.setText(DialogScreenSaveImage.this.m_sImageFilePath);
                    DialogScreenSaveImage.this.m_btnViewImage.setEnabled(true);
                }
            });
        }
    };
    /* access modifiers changed from: private */
    public Intent m_hViewImageIntent;
    /* access modifiers changed from: private */
    public final View m_pbProgress;
    /* access modifiers changed from: private */
    public String m_sImageFilePath;
    /* access modifiers changed from: private */
    public final TextView m_tvImageFilePath;
    /* access modifiers changed from: private */
    public final TextView m_tvImageSaveState;

    public DialogScreenSaveImage(Activity hActivity, DialogManager hDialogManager) {
        super(hActivity, hDialogManager);
        this.m_hActivity = hActivity;
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        TitleView hTitle = new TitleView(hActivity, hMetrics);
        hTitle.setTitle(ResString.dialog_screen_save_image_title);
        addView(hTitle);
        RelativeLayout vgContentContainer = new RelativeLayout(hActivity);
        vgContentContainer.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        addView(vgContentContainer);
        this.m_pbProgress = new ProgressBar(hActivity, null, 16842874);
        RelativeLayout.LayoutParams hProgressBarParams = new RelativeLayout.LayoutParams(-2, -2);
        hProgressBarParams.addRule(13);
        this.m_pbProgress.setLayoutParams(hProgressBarParams);
        vgContentContainer.addView(this.m_pbProgress);
        LinearLayout vgTextContainer = new LinearLayout(hActivity);
        vgTextContainer.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        vgTextContainer.setOrientation(1);
        vgTextContainer.setGravity(17);
        vgContentContainer.addView(vgTextContainer);
        this.m_tvImageSaveState = new TextView(hActivity);
        this.m_tvImageSaveState.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.m_tvImageSaveState.setGravity(17);
        this.m_tvImageSaveState.setTextColor(-1);
        this.m_tvImageSaveState.setTextSize(1, 20.0f);
        this.m_tvImageSaveState.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
        this.m_tvImageSaveState.setTypeface(Typeface.DEFAULT, 1);
        int iDip20 = ResDimens.getDip(hMetrics, 20);
        this.m_tvImageSaveState.setPadding(iDip20, iDip20, iDip20, iDip20);
        vgTextContainer.addView(this.m_tvImageSaveState);
        this.m_tvImageFilePath = new TextView(hActivity);
        this.m_tvImageFilePath.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.m_tvImageFilePath.setGravity(17);
        this.m_tvImageFilePath.setTextColor(-1);
        this.m_tvImageFilePath.setTextSize(1, 20.0f);
        this.m_tvImageFilePath.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
        this.m_tvImageFilePath.setPadding(iDip20, 0, iDip20, 0);
        vgTextContainer.addView(this.m_tvImageFilePath);
        ButtonContainer vgButtonContainer = new ButtonContainer(hActivity);
        this.m_btnViewImage = vgButtonContainer.createButton(0, "btn_img_view_image", this.m_hOnClick);
        vgButtonContainer.createButton(1, "btn_img_ok", this.m_hOnClick);
        addView(vgButtonContainer);
    }

    public void onShow(final String sPuzzleId, final int iImageIndex, ScreenPlayBase.ResultData hResultData, DialogInterface.OnDismissListener hOnDismiss) {
        this.m_hResultData = hResultData;
        this.m_hOnDismiss = hOnDismiss;
        this.m_tvImageFilePath.setText((CharSequence) null);
        if (!Global.isSDCardAvailable()) {
            this.m_pbProgress.setVisibility(8);
            this.m_tvImageSaveState.setText(ResString.sd_card_unmounted);
            this.m_tvImageFilePath.setVisibility(8);
            this.m_btnViewImage.setEnabled(false);
            return;
        }
        this.m_pbProgress.setVisibility(0);
        this.m_tvImageSaveState.setText((CharSequence) null);
        this.m_tvImageFilePath.setVisibility(0);
        this.m_btnViewImage.setEnabled(false);
        new Thread(new Runnable() {
            public void run() {
                DialogScreenSaveImage.this.m_sImageFilePath = ImageSaver.save(DialogScreenSaveImage.this.m_hActivity, sPuzzleId, iImageIndex);
                if (TextUtils.isEmpty(DialogScreenSaveImage.this.m_sImageFilePath)) {
                    DialogScreenSaveImage.this.m_hHandler.post(new Runnable() {
                        public void run() {
                            DialogScreenSaveImage.this.m_pbProgress.setVisibility(8);
                            DialogScreenSaveImage.this.m_tvImageSaveState.setText(ResString.dialog_screen_save_image_unable_to_save);
                        }
                    });
                    return;
                }
                DialogScreenSaveImage.this.m_hScanner = new MediaScannerConnection(DialogScreenSaveImage.this.m_hActivity, DialogScreenSaveImage.this.m_hScannerConnectionClient);
                DialogScreenSaveImage.this.m_hScanner.connect();
            }
        }).start();
    }

    public boolean unlockScreenOnDismiss() {
        if (this.m_hResultData == null || this.m_hOnDismiss == null) {
            return true;
        }
        return false;
    }

    public void cleanUp() {
        super.cleanUp();
        this.m_tvImageSaveState.setText((CharSequence) null);
        this.m_tvImageFilePath.setText((CharSequence) null);
    }
}
