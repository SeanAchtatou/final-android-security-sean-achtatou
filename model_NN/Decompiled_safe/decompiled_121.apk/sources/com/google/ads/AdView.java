package com.google.ads;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cfg.Option;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;

public class AdView extends RelativeLayout implements Ad {
    private d a;

    public AdView(Activity activity, AdSize adSize, String adUnitId) {
        super(activity.getApplicationContext());
        if (a(activity, adSize, (AttributeSet) null)) {
            a(activity, adSize, adUnitId);
        }
    }

    public AdView(Context context, AttributeSet attrs) {
        super(context, attrs);
        a(context, attrs);
    }

    public AdView(Context context, AttributeSet attrs, int i) {
        this(context, attrs);
    }

    private void a(Activity activity, AdSize adSize, String str) {
        this.a = new d(activity, this, adSize, str, false);
        setGravity(17);
        setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        addView(this.a.i(), (int) TypedValue.applyDimension(1, (float) adSize.getWidth(), activity.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(1, (float) adSize.getHeight(), activity.getResources().getDisplayMetrics()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [java.lang.String, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00d5  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0152  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.content.Context r10, android.util.AttributeSet r11) {
        /*
            r9 = this;
            r8 = 0
            if (r11 != 0) goto L_0x0004
        L_0x0003:
            return
        L_0x0004:
            java.lang.String r1 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r2 = "adSize"
            java.lang.String r1 = r11.getAttributeValue(r1, r2)
            if (r1 != 0) goto L_0x0016
            java.lang.String r1 = "AdView missing required XML attribute \"adSize\"."
            com.google.ads.AdSize r2 = com.google.ads.AdSize.BANNER
            r9.a(r10, r1, r2, r11)
            goto L_0x0003
        L_0x0016:
            java.lang.String r2 = "BANNER"
            boolean r2 = r2.equals(r1)
            if (r2 == 0) goto L_0x0031
            com.google.ads.AdSize r1 = com.google.ads.AdSize.BANNER
            r5 = r1
        L_0x0021:
            java.lang.String r1 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r2 = "adUnitId"
            java.lang.String r1 = r11.getAttributeValue(r1, r2)
            if (r1 != 0) goto L_0x0074
            java.lang.String r1 = "AdView missing required XML attribute \"adUnitId\"."
            r9.a(r10, r1, r5, r11)
            goto L_0x0003
        L_0x0031:
            java.lang.String r2 = "IAB_MRECT"
            boolean r2 = r2.equals(r1)
            if (r2 == 0) goto L_0x003d
            com.google.ads.AdSize r1 = com.google.ads.AdSize.IAB_MRECT
            r5 = r1
            goto L_0x0021
        L_0x003d:
            java.lang.String r2 = "IAB_BANNER"
            boolean r2 = r2.equals(r1)
            if (r2 == 0) goto L_0x0049
            com.google.ads.AdSize r1 = com.google.ads.AdSize.IAB_BANNER
            r5 = r1
            goto L_0x0021
        L_0x0049:
            java.lang.String r2 = "IAB_LEADERBOARD"
            boolean r2 = r2.equals(r1)
            if (r2 == 0) goto L_0x0055
            com.google.ads.AdSize r1 = com.google.ads.AdSize.IAB_LEADERBOARD
            r5 = r1
            goto L_0x0021
        L_0x0055:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Invalid \"adSize\" value in XML layout: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = "."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.google.ads.AdSize r2 = com.google.ads.AdSize.BANNER
            r9.a(r10, r1, r2, r11)
            goto L_0x0003
        L_0x0074:
            boolean r2 = r9.isInEditMode()
            if (r2 == 0) goto L_0x0084
            java.lang.String r3 = "Ads by Google"
            r4 = -1
            r1 = r9
            r2 = r10
            r6 = r11
            r1.a(r2, r3, r4, r5, r6)
            goto L_0x0003
        L_0x0084:
            java.lang.String r2 = "@string/"
            boolean r2 = r1.startsWith(r2)
            if (r2 == 0) goto L_0x014a
            java.lang.String r2 = "@string/"
            int r2 = r2.length()
            java.lang.String r2 = r1.substring(r2)
            java.lang.String r3 = r10.getPackageName()
            android.util.TypedValue r4 = new android.util.TypedValue
            r4.<init>()
            android.content.res.Resources r6 = r9.getResources()     // Catch:{ NotFoundException -> 0x010f }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ NotFoundException -> 0x010f }
            r7.<init>()     // Catch:{ NotFoundException -> 0x010f }
            java.lang.StringBuilder r3 = r7.append(r3)     // Catch:{ NotFoundException -> 0x010f }
            java.lang.String r7 = ":string/"
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ NotFoundException -> 0x010f }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ NotFoundException -> 0x010f }
            java.lang.String r2 = r2.toString()     // Catch:{ NotFoundException -> 0x010f }
            r3 = 1
            r6.getValue(r2, r4, r3)     // Catch:{ NotFoundException -> 0x010f }
            java.lang.CharSequence r2 = r4.string
            if (r2 == 0) goto L_0x012e
            java.lang.CharSequence r1 = r4.string
            java.lang.String r1 = r1.toString()
            r2 = r1
        L_0x00c9:
            java.lang.String r1 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r3 = "loadAdOnCreate"
            boolean r3 = r11.getAttributeBooleanValue(r1, r3, r8)
            boolean r1 = r10 instanceof android.app.Activity
            if (r1 == 0) goto L_0x0152
            r0 = r10
            android.app.Activity r0 = (android.app.Activity) r0
            r1 = r0
            boolean r4 = r9.a(r10, r5, r11)
            if (r4 == 0) goto L_0x0003
            r9.a(r1, r5, r2)
            if (r3 == 0) goto L_0x0003
            com.google.ads.AdRequest r1 = new com.google.ads.AdRequest
            r1.<init>()
            java.lang.String r2 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r3 = "keywords"
            java.lang.String r2 = r11.getAttributeValue(r2, r3)
            if (r2 == 0) goto L_0x014d
            java.lang.String r3 = ","
            java.lang.String[] r2 = r2.split(r3)
            int r3 = r2.length
            r4 = r8
        L_0x00fb:
            if (r4 >= r3) goto L_0x014d
            r5 = r2[r4]
            java.lang.String r5 = r5.trim()
            int r6 = r5.length()
            if (r6 == 0) goto L_0x010c
            r1.addKeyword(r5)
        L_0x010c:
            int r4 = r4 + 1
            goto L_0x00fb
        L_0x010f:
            r2 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not find resource for \"adUnitId\": \""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = "\"."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r9.a(r10, r1, r5, r11)
            goto L_0x0003
        L_0x012e:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "\"adUnitId\" was not a string: \""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r3 = "\"."
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r9.a(r10, r2, r5, r11)
        L_0x014a:
            r2 = r1
            goto L_0x00c9
        L_0x014d:
            r9.loadAd(r1)
            goto L_0x0003
        L_0x0152:
            java.lang.String r1 = "AdView was initialized with a Context that wasn't an Activity."
            com.google.ads.util.a.b(r1)
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdView.a(android.content.Context, android.util.AttributeSet):void");
    }

    private void a(Context context, String str, int i, AdSize adSize, AttributeSet attributeSet) {
        if (getChildCount() == 0) {
            TextView textView = attributeSet == null ? new TextView(context) : new TextView(context, attributeSet);
            textView.setGravity(17);
            textView.setText(str);
            textView.setTextColor(i);
            textView.setBackgroundColor(Option.HINT_COLOR_DEFAULT);
            LinearLayout linearLayout = attributeSet == null ? new LinearLayout(context) : new LinearLayout(context, attributeSet);
            linearLayout.setGravity(17);
            LinearLayout linearLayout2 = attributeSet == null ? new LinearLayout(context) : new LinearLayout(context, attributeSet);
            linearLayout2.setGravity(17);
            linearLayout2.setBackgroundColor(i);
            int applyDimension = (int) TypedValue.applyDimension(1, (float) adSize.getWidth(), context.getResources().getDisplayMetrics());
            int applyDimension2 = (int) TypedValue.applyDimension(1, (float) adSize.getHeight(), context.getResources().getDisplayMetrics());
            linearLayout.addView(textView, applyDimension - 2, applyDimension2 - 2);
            linearLayout2.addView(linearLayout);
            addView(linearLayout2, applyDimension, applyDimension2);
        }
    }

    private void a(Context context, String str, AdSize adSize, AttributeSet attributeSet) {
        a.b(str);
        a(context, str, -65536, adSize, attributeSet);
        if (isInEditMode()) {
            return;
        }
        if (context instanceof Activity) {
            a((Activity) context, adSize, "");
        } else {
            a.b("AdView was initialized with a Context that wasn't an Activity.");
        }
    }

    private boolean a(Context context, AdSize adSize, AttributeSet attributeSet) {
        if (AdUtil.b(context)) {
            return true;
        }
        a(context, "You must have INTERNET and ACCESS_NETWORK_STATE permissions in AndroidManifest.xml.", adSize, attributeSet);
        return false;
    }

    public void destroy() {
        this.a.b();
    }

    public boolean isReady() {
        if (this.a == null) {
            return false;
        }
        return this.a.o();
    }

    public boolean isRefreshing() {
        return this.a.p();
    }

    public void loadAd(AdRequest adRequest) {
        if (isRefreshing()) {
            this.a.c();
        }
        this.a.a(adRequest);
    }

    public void setAdListener(AdListener adListener) {
        this.a.a(adListener);
    }

    public void stopLoading() {
        this.a.z();
    }
}
