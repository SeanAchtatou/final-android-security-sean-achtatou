package com.hivong.puzzlebox.seaside;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int background_head = 2130837505;
        public static final int btn_bg_normal = 2130837506;
        public static final int btn_bg_pressed = 2130837507;
        public static final int btn_img_add_my_puzzle_image = 2130837508;
        public static final int btn_img_arrow_down = 2130837509;
        public static final int btn_img_arrow_left = 2130837510;
        public static final int btn_img_arrow_right = 2130837511;
        public static final int btn_img_arrow_up = 2130837512;
        public static final int btn_img_back = 2130837513;
        public static final int btn_img_cancel = 2130837514;
        public static final int btn_img_delete = 2130837515;
        public static final int btn_img_favorite_no = 2130837516;
        public static final int btn_img_favorite_yes = 2130837517;
        public static final int btn_img_gallery = 2130837518;
        public static final int btn_img_group = 2130837519;
        public static final int btn_img_hint = 2130837520;
        public static final int btn_img_hint_disable = 2130837521;
        public static final int btn_img_history = 2130837522;
        public static final int btn_img_info = 2130837523;
        public static final int btn_img_market_search = 2130837524;
        public static final int btn_img_more = 2130837525;
        public static final int btn_img_more_cancel = 2130837526;
        public static final int btn_img_my_puzzle_add = 2130837527;
        public static final int btn_img_my_puzzle_list = 2130837528;
        public static final int btn_img_next = 2130837529;
        public static final int btn_img_ok = 2130837530;
        public static final int btn_img_refresh = 2130837531;
        public static final int btn_img_save = 2130837532;
        public static final int btn_img_shuffle = 2130837533;
        public static final int btn_img_vibration_off = 2130837534;
        public static final int btn_img_vibration_on = 2130837535;
        public static final int btn_img_view_image = 2130837536;
        public static final int icon = 2130837537;
    }

    public static final class integer {
        public static final int grid_size_max = 2131034114;
        public static final int grid_size_min = 2131034113;
        public static final int market_id = 2131034112;
    }

    public static final class raw {
        public static final int image_0 = 2130903040;
        public static final int image_1 = 2130903041;
        public static final int image_10 = 2130903042;
        public static final int image_11 = 2130903043;
        public static final int image_12 = 2130903044;
        public static final int image_13 = 2130903045;
        public static final int image_14 = 2130903046;
        public static final int image_15 = 2130903047;
        public static final int image_16 = 2130903048;
        public static final int image_17 = 2130903049;
        public static final int image_18 = 2130903050;
        public static final int image_19 = 2130903051;
        public static final int image_2 = 2130903052;
        public static final int image_20 = 2130903053;
        public static final int image_21 = 2130903054;
        public static final int image_22 = 2130903055;
        public static final int image_23 = 2130903056;
        public static final int image_24 = 2130903057;
        public static final int image_25 = 2130903058;
        public static final int image_26 = 2130903059;
        public static final int image_27 = 2130903060;
        public static final int image_28 = 2130903061;
        public static final int image_29 = 2130903062;
        public static final int image_3 = 2130903063;
        public static final int image_30 = 2130903064;
        public static final int image_31 = 2130903065;
        public static final int image_32 = 2130903066;
        public static final int image_33 = 2130903067;
        public static final int image_34 = 2130903068;
        public static final int image_35 = 2130903069;
        public static final int image_36 = 2130903070;
        public static final int image_37 = 2130903071;
        public static final int image_38 = 2130903072;
        public static final int image_39 = 2130903073;
        public static final int image_4 = 2130903074;
        public static final int image_40 = 2130903075;
        public static final int image_41 = 2130903076;
        public static final int image_42 = 2130903077;
        public static final int image_43 = 2130903078;
        public static final int image_44 = 2130903079;
        public static final int image_45 = 2130903080;
        public static final int image_46 = 2130903081;
        public static final int image_47 = 2130903082;
        public static final int image_48 = 2130903083;
        public static final int image_49 = 2130903084;
        public static final int image_5 = 2130903085;
        public static final int image_50 = 2130903086;
        public static final int image_51 = 2130903087;
        public static final int image_52 = 2130903088;
        public static final int image_53 = 2130903089;
        public static final int image_54 = 2130903090;
        public static final int image_55 = 2130903091;
        public static final int image_56 = 2130903092;
        public static final int image_57 = 2130903093;
        public static final int image_58 = 2130903094;
        public static final int image_59 = 2130903095;
        public static final int image_6 = 2130903096;
        public static final int image_60 = 2130903097;
        public static final int image_61 = 2130903098;
        public static final int image_62 = 2130903099;
        public static final int image_63 = 2130903100;
        public static final int image_64 = 2130903101;
        public static final int image_65 = 2130903102;
        public static final int image_66 = 2130903103;
        public static final int image_67 = 2130903104;
        public static final int image_68 = 2130903105;
        public static final int image_69 = 2130903106;
        public static final int image_7 = 2130903107;
        public static final int image_70 = 2130903108;
        public static final int image_71 = 2130903109;
        public static final int image_72 = 2130903110;
        public static final int image_73 = 2130903111;
        public static final int image_74 = 2130903112;
        public static final int image_75 = 2130903113;
        public static final int image_76 = 2130903114;
        public static final int image_77 = 2130903115;
        public static final int image_78 = 2130903116;
        public static final int image_79 = 2130903117;
        public static final int image_8 = 2130903118;
        public static final int image_80 = 2130903119;
        public static final int image_81 = 2130903120;
        public static final int image_82 = 2130903121;
        public static final int image_83 = 2130903122;
        public static final int image_84 = 2130903123;
        public static final int image_85 = 2130903124;
        public static final int image_86 = 2130903125;
        public static final int image_87 = 2130903126;
        public static final int image_88 = 2130903127;
        public static final int image_89 = 2130903128;
        public static final int image_9 = 2130903129;
        public static final int image_90 = 2130903130;
        public static final int image_91 = 2130903131;
        public static final int image_92 = 2130903132;
        public static final int image_93 = 2130903133;
        public static final int image_94 = 2130903134;
    }

    public static final class string {
        public static final int ad_whirl_key = 2130968579;
        public static final int app_name = 2130968576;
        public static final int dev_name = 2130968577;
        public static final int image_info_0 = 2130968580;
        public static final int image_info_1 = 2130968581;
        public static final int info = 2130968578;
    }
}
