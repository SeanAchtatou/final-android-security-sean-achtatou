package game;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import view.PuzzlePieceContainer;

public final class PuzzlePiece {
    private static final int ALPHA_GROUPING = 75;
    private static final int UNGROUPE = -1;
    private final Bitmap m_hBitmap;
    private Bitmap m_hBitmapGrouped;
    private final Paint m_hPaintGroup = new Paint();
    private PuzzlePieceContainer m_hPuzzlePieceContainer;
    private final Rect m_hRect = new Rect();
    private int m_iColumnInGrid;
    private int m_iColumnInGroupBitmap;
    private final int m_iFinalColumn;
    private final int m_iFinalPosition;
    private final int m_iFinalRow;
    private int m_iGroupHeight;
    private int m_iGroupId = -1;
    private int m_iGroupSize;
    private int m_iGroupWidth;
    private int m_iRowInGrid;
    private int m_iRowInGroupBitmap;

    public PuzzlePiece(int iFinalPosition, int iFinalColumn, int iFinalRow, Bitmap hBitmap) {
        this.m_iFinalPosition = iFinalPosition;
        this.m_iFinalColumn = iFinalColumn;
        this.m_iFinalRow = iFinalRow;
        this.m_hBitmap = hBitmap;
        this.m_hRect.left = 0;
        this.m_hRect.top = 0;
        this.m_hRect.right = hBitmap.getWidth();
        this.m_hRect.bottom = hBitmap.getHeight();
        this.m_hPaintGroup.setColor(-16776961);
        this.m_hPaintGroup.setAlpha(ALPHA_GROUPING);
        this.m_hPaintGroup.setStyle(Paint.Style.STROKE);
        this.m_hPaintGroup.setStyle(Paint.Style.FILL);
    }

    public int getFinalPosition() {
        return this.m_iFinalPosition;
    }

    public int getFinalColumn() {
        return this.m_iFinalColumn;
    }

    public int getFinalRow() {
        return this.m_iFinalRow;
    }

    public Bitmap getBitmap() {
        return this.m_hBitmapGrouped != null ? this.m_hBitmapGrouped : this.m_hBitmap;
    }

    public boolean hasGroup() {
        return this.m_iGroupId != -1;
    }

    public void ungroup() {
        this.m_iGroupId = -1;
        this.m_hBitmapGrouped = null;
        this.m_iColumnInGroupBitmap = 0;
        this.m_iRowInGroupBitmap = 0;
    }

    public void setGroupId(int iGroupId) {
        if (iGroupId == -1) {
            throw new RuntimeException("Invalid group id");
        }
        this.m_iGroupId = iGroupId;
        this.m_hBitmapGrouped = Bitmap.createBitmap(this.m_hRect.right, this.m_hRect.bottom, Bitmap.Config.RGB_565);
        Canvas hCanvas = new Canvas(this.m_hBitmapGrouped);
        hCanvas.drawBitmap(this.m_hBitmap, 0.0f, 0.0f, (Paint) null);
        hCanvas.drawRect(this.m_hRect, this.m_hPaintGroup);
    }

    public int getGroupId() {
        return this.m_iGroupId;
    }

    public void setGroupDimension(int iGroupWidth, int iGroupHeight) {
        this.m_iGroupWidth = iGroupWidth;
        this.m_iGroupHeight = iGroupHeight;
        this.m_iGroupSize = iGroupWidth * iGroupHeight;
    }

    public int getGroupWidth() {
        return this.m_iGroupWidth;
    }

    public int getGroupHeight() {
        return this.m_iGroupHeight;
    }

    public int getGroupSize() {
        return this.m_iGroupSize;
    }

    public boolean hasSameGroupDimension(PuzzlePiece hPuzzlePiece) {
        if (this.m_iGroupWidth != hPuzzlePiece.getGroupWidth()) {
            return false;
        }
        if (this.m_iGroupHeight != hPuzzlePiece.getGroupHeight()) {
            return false;
        }
        return true;
    }

    public void setColumnInGrid(int iColumn) {
        this.m_iColumnInGrid = iColumn;
    }

    public int getColumnInGrid() {
        return this.m_iColumnInGrid;
    }

    public void setColumnInGroupBitmap(int iColumn) {
        this.m_iColumnInGroupBitmap = iColumn;
    }

    public int getColumnInGroupBitmap() {
        return this.m_iColumnInGroupBitmap;
    }

    public void setRowInGrid(int iRow) {
        this.m_iRowInGrid = iRow;
    }

    public int getRowInGrid() {
        return this.m_iRowInGrid;
    }

    public void setRowInGroupBitmap(int iRow) {
        this.m_iRowInGroupBitmap = iRow;
    }

    public int getRowInGroupBitmap() {
        return this.m_iRowInGroupBitmap;
    }

    public void setContainer(PuzzlePieceContainer hPuzzlePieceContainer) {
        this.m_hPuzzlePieceContainer = hPuzzlePieceContainer;
    }

    public PuzzlePieceContainer getContainer() {
        return this.m_hPuzzlePieceContainer;
    }
}
