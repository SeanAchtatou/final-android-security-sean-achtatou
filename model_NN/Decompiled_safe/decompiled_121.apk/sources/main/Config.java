package main;

import helper.DevConfig;
import java.util.Random;

public final class Config extends DevConfig {
    public String getAdWhirlKey() {
        if (new Random().nextInt(99) < 75) {
            return "4adc0b6843db42e0a79dc947037e64de";
        }
        return "234f6abcbd8c44a3a775014d6d3bb6b9";
    }
}
