package helper;

import android.os.SystemClock;
import java.util.HashMap;

public final class TimeMeasure {
    private static HashMap<Long, Long> arrTimer = new HashMap<>();

    public static synchronized long start() {
        long j;
        synchronized (TimeMeasure.class) {
            if (Constants.debug()) {
                long lId = SystemClock.uptimeMillis();
                arrTimer.put(Long.valueOf(lId), Long.valueOf(lId));
                j = lId;
            } else {
                j = 0;
            }
        }
        return j;
    }

    public static synchronized void stop(long lId, String sTag, String sText) {
        Long lStartTime;
        synchronized (TimeMeasure.class) {
            if (Constants.debug() && (lStartTime = arrTimer.remove(Long.valueOf(lId))) != null) {
                L.v(sTag, "TIME:" + (SystemClock.uptimeMillis() - lStartTime.longValue()) + " - " + sText);
            }
        }
    }
}
