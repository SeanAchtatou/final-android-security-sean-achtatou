package helper;

import android.content.Context;

public final class Constants {
    public static final String EMAIL_CRASH = "crashreport@simboly.com";
    public static final boolean ENABLE_CRASH_HANDLER = true;
    public static final String LIB_VERSION = "8";
    private static boolean s_bDebug;
    private static boolean s_bInit = false;

    public static void init(Context hContext) {
        boolean z;
        if ((hContext.getApplicationInfo().flags & 2) == 0) {
            z = false;
        } else {
            z = true;
        }
        s_bDebug = z;
        s_bInit = true;
    }

    private static void checkInit() {
        if (!s_bInit) {
            throw new RuntimeException("Call init first");
        }
    }

    public static boolean debug() {
        checkInit();
        return s_bDebug;
    }
}
