package data;

import android.graphics.Bitmap;
import helper.BitmapHelper;
import helper.Constants;
import helper.L;
import java.io.File;
import java.io.Serializable;
import java.util.UUID;

public final class MyPuzzleImage implements Serializable {
    private static final transient String IMAGE_EXT = ".img";
    private static final transient String THUMBNAIL_EXT = ".tbn";
    private static final long serialVersionUID = -1849015583151580493L;
    private transient Bitmap m_hThumbnail;
    private final int m_iGridSize;
    private int m_iMoves = 0;
    private long m_lTimeInMs = 0;
    private final String m_sId = UUID.randomUUID().toString();
    private String m_sMovesName;
    private final String m_sMyPuzzleId;
    private String m_sTimeInMsName;

    public static MyPuzzleImage createMyPuzzleImage(String sMyPuzzleId, Bitmap hImage, int iGridSize) {
        try {
            return new MyPuzzleImage(sMyPuzzleId, hImage, iGridSize);
        } catch (Exception e) {
            Exception e2 = e;
            if (!Constants.debug()) {
                return null;
            }
            e2.printStackTrace();
            return null;
        }
    }

    private MyPuzzleImage(String sMyPuzzleId, Bitmap hImage, int iGridSize) {
        this.m_sMyPuzzleId = sMyPuzzleId;
        this.m_iGridSize = iGridSize;
        if (!BitmapHelper.saveBitmapAsByteArray(getImageFile(), hImage)) {
            throw new RuntimeException();
        }
    }

    public String getId() {
        return this.m_sId;
    }

    public int getGridSize() {
        return this.m_iGridSize;
    }

    public void setMoves(String sMovesName, int iMoves) {
        this.m_sMovesName = sMovesName;
        this.m_iMoves = iMoves;
    }

    public String getMovesName() {
        return this.m_sMovesName;
    }

    public int getMoves() {
        return this.m_iMoves;
    }

    public void setTimeInMs(String sTimeName, long lTime) {
        this.m_sTimeInMsName = sTimeName;
        this.m_lTimeInMs = lTime;
    }

    public String getTimeInMsName() {
        return this.m_sTimeInMsName;
    }

    public long getTimeInMs() {
        return this.m_lTimeInMs;
    }

    public Bitmap getThumbnail(int iThumbnailSize) {
        if (this.m_hThumbnail != null) {
            return this.m_hThumbnail;
        }
        File hThumbnailFile = getThumbnailFile();
        this.m_hThumbnail = BitmapHelper.loadBitmapFromByteArrayFile(hThumbnailFile);
        if (this.m_hThumbnail != null) {
            if (this.m_hThumbnail.getHeight() == iThumbnailSize || this.m_hThumbnail.getWidth() == iThumbnailSize) {
                return this.m_hThumbnail;
            }
            hThumbnailFile.delete();
            L.v(getClass().getSimpleName(), "delete thumbnail, because wrong size");
        }
        createThumbnail(BitmapHelper.loadBitmapFromByteArrayFile(getImageFile()), iThumbnailSize);
        return this.m_hThumbnail;
    }

    public void clearThumbnail() {
        this.m_hThumbnail = null;
    }

    public Bitmap getImage() {
        return BitmapHelper.loadBitmapFromByteArrayFile(getImageFile());
    }

    public void createThumbnail(Bitmap hImage, int iThumbnailSize) {
        this.m_hThumbnail = BitmapHelper.scaleBitmap(hImage, iThumbnailSize);
        if (this.m_hThumbnail == null) {
            throw new RuntimeException("Unable to load image");
        } else if (!BitmapHelper.saveBitmapAsByteArray(getThumbnailFile(), this.m_hThumbnail) && Constants.debug()) {
            throw new RuntimeException("Unable to save thumbnail");
        }
    }

    public void deleteFromSD() {
        getImageFile().delete();
        getThumbnailFile().delete();
    }

    public boolean isImageOnSD() {
        if (getImageFile().exists()) {
            return true;
        }
        return false;
    }

    public static synchronized String getImageFilename(String sMyPuzzleId) {
        String str;
        synchronized (MyPuzzleImage.class) {
            str = String.valueOf(sMyPuzzleId) + IMAGE_EXT;
        }
        return str;
    }

    public static synchronized String getThumbnailFilename(String sMyPuzzleId) {
        String str;
        synchronized (MyPuzzleImage.class) {
            str = String.valueOf(sMyPuzzleId) + THUMBNAIL_EXT;
        }
        return str;
    }

    private File getImageFile() {
        return new File(MyPuzzle.getMyPuzzleDir(this.m_sMyPuzzleId), getImageFilename(this.m_sId));
    }

    private File getThumbnailFile() {
        return new File(MyPuzzle.getMyPuzzleDir(this.m_sMyPuzzleId), getThumbnailFilename(this.m_sId));
    }
}
