package defpackage;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import com.google.ads.AdActivity;
import com.google.ads.AdView;
import com.google.ads.a;
import com.google.ads.b;
import com.google.ads.c;
import com.google.ads.e;
import com.google.ads.f;
import com.google.ads.util.AdUtil;
import com.google.ads.util.d;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: h  reason: default package */
public final class h {
    private static final Object a = new Object();
    private WeakReference b;
    private a c;
    private b d = null;
    private k e = null;
    private e f = null;
    private f g;
    private f h = new f();
    private String i;
    private g j;
    private s k;
    private Handler l = new Handler();
    private long m;
    private boolean n = false;
    private boolean o = false;
    private SharedPreferences p;
    private long q = 0;
    private ac r;
    private boolean s = false;
    private LinkedList t;
    private LinkedList u;
    private int v;

    public h(Activity activity, a aVar, f fVar, String str) {
        this.b = new WeakReference(activity);
        this.c = aVar;
        this.g = fVar;
        this.i = str;
        synchronized (a) {
            this.p = activity.getApplicationContext().getSharedPreferences("GoogleAdMobAdsPrefs", 0);
            this.m = 60000;
        }
        this.r = new ac(this);
        this.t = new LinkedList();
        this.u = new LinkedList();
        a();
        AdUtil.g(activity.getApplicationContext());
    }

    private synchronized boolean v() {
        return this.e != null;
    }

    private synchronized void w() {
        Activity activity = (Activity) this.b.get();
        if (activity == null) {
            d.e("activity was null while trying to ping tracking URLs.");
        } else {
            Iterator it = this.t.iterator();
            while (it.hasNext()) {
                new Thread(new u((String) it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    private synchronized void x() {
        Activity activity = (Activity) this.b.get();
        if (activity == null) {
            d.e("activity was null while trying to ping click tracking URLs.");
        } else {
            Iterator it = this.u.iterator();
            while (it.hasNext()) {
                new Thread(new u((String) it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    public final synchronized void a() {
        Activity d2 = d();
        if (d2 == null) {
            d.a("activity was null while trying to create an AdWebView.");
        } else {
            this.j = new g(d2.getApplicationContext(), this.g);
            this.j.setVisibility(8);
            if (this.c instanceof AdView) {
                this.k = new s(this, l.b, true, false);
            } else {
                this.k = new s(this, l.b, true, true);
            }
            this.j.setWebViewClient(this.k);
        }
    }

    public final synchronized void a(float f2) {
        this.q = (long) (1000.0f * f2);
    }

    public final synchronized void a(int i2) {
        this.v = i2;
    }

    public final void a(long j2) {
        synchronized (a) {
            SharedPreferences.Editor edit = this.p.edit();
            edit.putLong("Timeout" + this.i, j2);
            edit.commit();
            if (this.s) {
                this.m = j2;
            }
        }
    }

    public final synchronized void a(c cVar) {
        this.e = null;
        if (this.c instanceof com.google.ads.d) {
            if (cVar == c.NO_FILL) {
                this.h.n();
            } else if (cVar == c.NETWORK_ERROR) {
                this.h.l();
            }
        }
        d.c("onFailedToReceiveAd(" + cVar + ")");
    }

    public final synchronized void a(e eVar) {
        if (v()) {
            d.e("loadAd called while the ad is already loading, so aborting.");
        } else if (AdActivity.c()) {
            d.e("loadAd called while an interstitial or landing page is displayed, so aborting");
        } else {
            Activity d2 = d();
            if (d2 == null) {
                d.e("activity is null while trying to load an ad.");
            } else if (AdUtil.c(d2.getApplicationContext()) && AdUtil.b(d2.getApplicationContext())) {
                this.n = false;
                this.t.clear();
                this.f = eVar;
                this.e = new k(this);
                this.e.a(eVar);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Runnable runnable) {
        this.l.post(runnable);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        d.a("Adding a tracking URL: " + str);
        this.t.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(LinkedList linkedList) {
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            d.a("Adding a click tracking URL: " + ((String) it.next()));
        }
        this.u = linkedList;
    }

    public final synchronized void b() {
        if (this.o) {
            d.a("Disabling refreshing.");
            this.l.removeCallbacks(this.r);
            this.o = false;
        } else {
            d.a("Refreshing is already disabled.");
        }
    }

    public final synchronized void c() {
        if (!(this.c instanceof AdView)) {
            d.a("Tried to enable refreshing on something other than an AdView.");
        } else if (!this.o) {
            d.a("Enabling refreshing every " + this.q + " milliseconds.");
            this.l.postDelayed(this.r, this.q);
            this.o = true;
        } else {
            d.a("Refreshing is already enabled.");
        }
    }

    public final Activity d() {
        return (Activity) this.b.get();
    }

    public final a e() {
        return this.c;
    }

    public final synchronized k f() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final String g() {
        return this.i;
    }

    public final synchronized g h() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public final synchronized s i() {
        return this.k;
    }

    public final f j() {
        return this.g;
    }

    public final f k() {
        return this.h;
    }

    public final synchronized int l() {
        return this.v;
    }

    public final long m() {
        return this.m;
    }

    public final synchronized boolean n() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void o() {
        this.e = null;
        this.n = true;
        this.j.setVisibility(0);
        this.h.c();
        if (this.c instanceof AdView) {
            w();
        }
        d.c("onReceiveAd()");
    }

    public final synchronized void p() {
        this.h.o();
        d.c("onDismissScreen()");
    }

    public final synchronized void q() {
        d.c("onPresentScreen()");
    }

    public final synchronized void r() {
        d.c("onLeaveApplication()");
    }

    public final void s() {
        this.h.b();
        x();
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean t() {
        return !this.u.isEmpty();
    }

    public final synchronized void u() {
        if (this.f == null) {
            d.a("Tried to refresh before calling loadAd().");
        } else if (this.c instanceof AdView) {
            if (!((AdView) this.c).isShown() || !AdUtil.d()) {
                d.a("Not refreshing because the ad is not visible.");
            } else {
                d.c("Refreshing ad.");
                a(this.f);
            }
            this.l.postDelayed(this.r, this.q);
        } else {
            d.a("Tried to refresh an ad that wasn't an AdView.");
        }
    }
}
