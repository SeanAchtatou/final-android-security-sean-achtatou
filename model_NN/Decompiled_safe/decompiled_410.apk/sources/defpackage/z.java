package defpackage;

import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.google.ads.util.d;
import java.util.HashMap;

/* renamed from: z  reason: default package */
public final class z implements t {
    public final void a(h hVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("a");
        if (str == null) {
            d.a("Could not get the action parameter for open GMSG.");
        } else if (str.equals("webapp")) {
            AdActivity.a(hVar, new i("webapp", hashMap));
        } else {
            AdActivity.a(hVar, new i("intent", hashMap));
        }
    }
}
