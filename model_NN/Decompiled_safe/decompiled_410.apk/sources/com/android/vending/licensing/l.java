package com.android.vending.licensing;

import android.content.Context;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public final class l implements q {
    private long a;
    private long b;
    private long c;
    private long d;
    private long e = 0;
    private t f;
    private d g;

    public l(Context context, g gVar) {
        this.g = new d(context.getSharedPreferences("com.android.vending.licensing.ServerManagedPolicy", 0), gVar);
        this.f = t.valueOf(this.g.b("lastResponse", t.RETRY.toString()));
        this.a = Long.parseLong(this.g.b("validityTimestamp", "0"));
        this.b = Long.parseLong(this.g.b("retryUntil", "0"));
        this.c = Long.parseLong(this.g.b("maxRetries", "0"));
        this.d = Long.parseLong(this.g.b("retryCount", "0"));
    }

    private void a(long j) {
        this.d = j;
        this.g.a("retryCount", Long.toString(j));
    }

    private void a(String str) {
        Long valueOf;
        String l;
        try {
            valueOf = Long.valueOf(Long.parseLong(str));
            l = str;
        } catch (NumberFormatException e2) {
            valueOf = Long.valueOf(System.currentTimeMillis() + 60000);
            l = Long.toString(valueOf.longValue());
        }
        this.a = valueOf.longValue();
        this.g.a("validityTimestamp", l);
    }

    private void b(String str) {
        String str2;
        Long l;
        try {
            l = Long.valueOf(Long.parseLong(str));
            str2 = str;
        } catch (NumberFormatException e2) {
            str2 = "0";
            l = null;
        }
        this.b = l.longValue();
        this.g.a("retryUntil", str2);
    }

    private void c(String str) {
        String str2;
        Long l;
        try {
            l = Long.valueOf(Long.parseLong(str));
            str2 = str;
        } catch (NumberFormatException e2) {
            str2 = "0";
            l = null;
        }
        this.c = l.longValue();
        this.g.a("maxRetries", str2);
    }

    private static Map d(String str) {
        HashMap hashMap = new HashMap();
        try {
            for (NameValuePair nameValuePair : URLEncodedUtils.parse(new URI("?" + str), "UTF-8")) {
                hashMap.put(nameValuePair.getName(), nameValuePair.getValue());
            }
        } catch (URISyntaxException e2) {
        }
        return hashMap;
    }

    public final void a(t tVar, u uVar) {
        if (tVar != t.RETRY) {
            a(0);
        } else {
            a(this.d + 1);
        }
        if (tVar == t.LICENSED) {
            Map d2 = d(uVar.g);
            this.f = tVar;
            a((String) d2.get("VT"));
            b((String) d2.get("GT"));
            c((String) d2.get("GR"));
        } else if (tVar == t.NOT_LICENSED) {
            a("0");
            b("0");
            c("0");
        }
        this.e = System.currentTimeMillis();
        this.f = tVar;
        this.g.a("lastResponse", tVar.toString());
        this.g.a();
    }

    public final boolean a() {
        long currentTimeMillis = System.currentTimeMillis();
        if (this.f == t.LICENSED) {
            if (currentTimeMillis <= this.a) {
                return true;
            }
        } else if (this.f == t.RETRY && currentTimeMillis < this.e + 60000) {
            return currentTimeMillis <= this.b || this.d <= this.c;
        }
        return false;
    }
}
