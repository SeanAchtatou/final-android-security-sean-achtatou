package com.android.vending.licensing;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.android.vending.licensing.a.a;
import com.android.vending.licensing.a.b;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public final class s implements ServiceConnection {
    private static final SecureRandom a = new SecureRandom();
    private ILicensingService b;
    /* access modifiers changed from: private */
    public PublicKey c;
    private final Context d;
    private final q e;
    /* access modifiers changed from: private */
    public Handler f;
    private final String g;
    private final String h;
    /* access modifiers changed from: private */
    public final Set i = new HashSet();
    private final Queue j = new LinkedList();

    public s(Context context, q qVar, String str) {
        this.d = context;
        this.e = qVar;
        this.c = a(str);
        this.g = this.d.getPackageName();
        this.h = a(context, this.g);
        HandlerThread handlerThread = new HandlerThread("background thread");
        handlerThread.start();
        this.f = new Handler(handlerThread.getLooper());
    }

    private static String a(Context context, String str) {
        try {
            return String.valueOf(context.getPackageManager().getPackageInfo(str, 0).versionCode);
        } catch (PackageManager.NameNotFoundException e2) {
            return "";
        }
    }

    private static PublicKey a(String str) {
        try {
            return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(b.a(str)));
        } catch (NoSuchAlgorithmException e2) {
            throw new RuntimeException(e2);
        } catch (a e3) {
            throw new IllegalArgumentException(e3);
        } catch (InvalidKeySpecException e4) {
            throw new IllegalArgumentException(e4);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(a aVar) {
        this.i.remove(aVar);
        if (this.i.isEmpty()) {
            c();
        }
    }

    private void b() {
        while (true) {
            a aVar = (a) this.j.poll();
            if (aVar != null) {
                try {
                    this.b.a((long) aVar.b(), aVar.c(), new f(this, aVar));
                    this.i.add(aVar);
                } catch (RemoteException e2) {
                    b(aVar);
                }
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void b(a aVar) {
        this.e.a(t.RETRY, null);
        if (this.e.a()) {
            aVar.a().a();
        } else {
            aVar.a().a(v.CONNECTION_ERROR);
        }
    }

    private void c() {
        if (this.b != null) {
            try {
                this.d.unbindService(this);
            } catch (IllegalArgumentException e2) {
            }
            this.b = null;
        }
    }

    public final synchronized void a() {
        c();
        this.f.getLooper().quit();
    }

    public final synchronized void a(p pVar) {
        if (this.e.a()) {
            pVar.a();
        } else {
            a aVar = new a(this.e, new m(), pVar, a.nextInt(), this.g, this.h);
            if (this.b == null) {
                try {
                    if (this.d.bindService(new Intent(ILicensingService.class.getName()), this, 1)) {
                        this.j.offer(aVar);
                    } else {
                        b(aVar);
                    }
                } catch (SecurityException e2) {
                    pVar.a(v.MISSING_PERMISSION);
                }
            } else {
                this.j.offer(aVar);
                b();
            }
        }
        return;
    }

    public final synchronized void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        ILicensingService iVar;
        if (iBinder == null) {
            iVar = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.android.vending.licensing.ILicensingService");
            iVar = (queryLocalInterface == null || !(queryLocalInterface instanceof ILicensingService)) ? new i(iBinder) : (ILicensingService) queryLocalInterface;
        }
        this.b = iVar;
        b();
    }

    public final synchronized void onServiceDisconnected(ComponentName componentName) {
        this.b = null;
    }
}
