package com.android.vending.licensing;

import com.android.vending.licensing.a.a;
import com.android.vending.licensing.a.b;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public final class o implements g {
    private static final byte[] a;
    private Cipher b;
    private Cipher c;

    static {
        byte[] bArr = new byte[16];
        bArr[0] = 16;
        bArr[1] = 74;
        bArr[2] = 71;
        bArr[3] = -80;
        bArr[4] = 32;
        bArr[5] = 101;
        bArr[6] = -47;
        bArr[7] = 72;
        bArr[8] = 117;
        bArr[9] = -14;
        bArr[11] = -29;
        bArr[12] = 70;
        bArr[13] = 65;
        bArr[14] = -12;
        bArr[15] = 74;
        a = bArr;
    }

    public o(byte[] bArr, String str, String str2) {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(SecretKeyFactory.getInstance("PBEWITHSHAAND256BITAES-CBC-BC").generateSecret(new PBEKeySpec((String.valueOf(str) + str2).toCharArray(), bArr, 1024, 256)).getEncoded(), "AES");
            this.b = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.b.init(1, secretKeySpec, new IvParameterSpec(a));
            this.c = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.c.init(2, secretKeySpec, new IvParameterSpec(a));
        } catch (GeneralSecurityException e) {
            throw new RuntimeException("Invalid environment", e);
        }
    }

    public final String a(String str) {
        if (str == null) {
            return null;
        }
        try {
            return b.a(this.b.doFinal(("com.android.vending.licensing.AESObfuscator-1|" + str).getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Invalid environment", e);
        } catch (GeneralSecurityException e2) {
            throw new RuntimeException("Invalid environment", e2);
        }
    }

    public final String b(String str) {
        if (str == null) {
            return null;
        }
        try {
            String str2 = new String(this.c.doFinal(b.a(str)), "UTF-8");
            if (str2.indexOf("com.android.vending.licensing.AESObfuscator-1|") == 0) {
                return str2.substring("com.android.vending.licensing.AESObfuscator-1|".length(), str2.length());
            }
            throw new n("Header not found (invalid data or key):" + str);
        } catch (a e) {
            throw new n(String.valueOf(e.getMessage()) + ":" + str);
        } catch (IllegalBlockSizeException e2) {
            throw new n(String.valueOf(e2.getMessage()) + ":" + str);
        } catch (BadPaddingException e3) {
            throw new n(String.valueOf(e3.getMessage()) + ":" + str);
        } catch (UnsupportedEncodingException e4) {
            throw new RuntimeException("Invalid environment", e4);
        }
    }
}
