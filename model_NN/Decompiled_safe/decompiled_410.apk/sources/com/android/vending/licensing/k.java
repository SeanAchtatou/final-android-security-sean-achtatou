package com.android.vending.licensing;

final class k implements Runnable {
    private /* synthetic */ f a;
    private final /* synthetic */ int b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;

    k(f fVar, int i, String str, String str2) {
        this.a = fVar;
        this.b = i;
        this.c = str;
        this.d = str2;
    }

    public final void run() {
        if (this.a.a.i.contains(this.a.b)) {
            this.a.a.f.removeCallbacks(this.a.c);
            this.a.b.a(this.a.a.c, this.b, this.c, this.d);
            this.a.a.a(this.a.b);
        }
    }
}
