package com.android.vending.licensing;

import android.text.TextUtils;
import com.android.vending.licensing.a.b;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Iterator;
import java.util.regex.Pattern;
import java.util.zip.CRC32;

final class a {
    private final q a;
    private final p b;
    private final int c;
    private final String d;
    private final String e;
    private final h f;

    a(q qVar, h hVar, p pVar, int i, String str, String str2) {
        this.a = qVar;
        this.f = hVar;
        this.b = pVar;
        this.c = i;
        this.d = str;
        this.e = str2;
    }

    private void a(t tVar, u uVar) {
        this.a.a(tVar, uVar);
        if (this.a.a()) {
            this.b.a();
        } else {
            this.b.b();
        }
    }

    private void a(v vVar) {
        this.b.a(vVar);
    }

    private void d() {
        this.b.b();
    }

    public final p a() {
        return this.b;
    }

    public final void a(PublicKey publicKey, int i, String str, String str2) {
        u uVar;
        long j;
        CRC32 crc32 = new CRC32();
        crc32.reset();
        crc32.update(i);
        long value = crc32.getValue();
        if (value == 3523407757L || value == 1007455905 || value == 2768625435L) {
            try {
                Signature instance = Signature.getInstance("SHA1withRSA");
                instance.initVerify(publicKey);
                instance.update(str.getBytes());
                if (!instance.verify(b.a(str2))) {
                    d();
                    return;
                }
                try {
                    TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(':');
                    simpleStringSplitter.setString(str);
                    Iterator it = simpleStringSplitter.iterator();
                    if (!it.hasNext()) {
                        throw new IllegalArgumentException("Blank response.");
                    }
                    String str3 = (String) it.next();
                    String str4 = it.hasNext() ? (String) it.next() : "";
                    String[] split = TextUtils.split(str3, Pattern.quote("|"));
                    if (split.length < 6) {
                        throw new IllegalArgumentException("Wrong number of fields.");
                    }
                    uVar = new u();
                    uVar.g = str4;
                    uVar.a = Integer.parseInt(split[0]);
                    uVar.b = Integer.parseInt(split[1]);
                    uVar.c = split[2];
                    uVar.d = split[3];
                    uVar.e = split[4];
                    uVar.f = Long.parseLong(split[5]);
                    if (uVar.a != i) {
                        d();
                        return;
                    } else if (uVar.b != this.c) {
                        d();
                        return;
                    } else if (!uVar.c.equals(this.d)) {
                        d();
                        return;
                    } else if (!uVar.d.equals(this.e)) {
                        d();
                        return;
                    } else if (TextUtils.isEmpty(uVar.e)) {
                        d();
                        return;
                    } else {
                        crc32.reset();
                        crc32.update(10);
                        j = crc32.getValue();
                    }
                } catch (IllegalArgumentException e2) {
                    d();
                    return;
                }
            } catch (NoSuchAlgorithmException e3) {
                throw new RuntimeException(e3);
            } catch (InvalidKeyException e4) {
                a(v.INVALID_PUBLIC_KEY);
                return;
            } catch (SignatureException e5) {
                throw new RuntimeException(e5);
            } catch (com.android.vending.licensing.a.a e6) {
                d();
                return;
            }
        } else {
            uVar = null;
            j = 0;
        }
        if (value == 3523407757L || value == 1007455905) {
            if (j == 852952723) {
                a(this.f.a(), uVar);
            } else {
                a(t.NOT_LICENSED, uVar);
            }
        } else if (value == 2768625435L) {
            a(t.NOT_LICENSED, uVar);
        } else if (value == 2768625435L) {
            a(t.RETRY, uVar);
        } else if (value == 3580832660L) {
            a(t.RETRY, uVar);
        } else if (value == 2724731650L) {
            a(t.RETRY, uVar);
        } else if (value == 1007455905) {
            a(v.INVALID_PACKAGE_NAME);
        } else if (value == 1259060791) {
            a(v.NON_MATCHING_UID);
        } else if (value == 1259060791) {
            a(v.NOT_MARKET_MANAGED);
        } else {
            d();
        }
    }

    public final int b() {
        return this.c;
    }

    public final String c() {
        return this.d;
    }
}
