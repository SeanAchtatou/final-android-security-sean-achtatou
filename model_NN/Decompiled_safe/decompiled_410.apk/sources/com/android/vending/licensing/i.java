package com.android.vending.licensing;

import android.os.IBinder;
import android.os.Parcel;

final class i implements ILicensingService {
    private IBinder a;

    i(IBinder iBinder) {
        this.a = iBinder;
    }

    public final void a(long j, String str, e eVar) {
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.android.vending.licensing.ILicensingService");
            obtain.writeLong(j);
            obtain.writeString(str);
            obtain.writeStrongBinder(eVar != null ? eVar.asBinder() : null);
            this.a.transact(1, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }

    public final IBinder asBinder() {
        return this.a;
    }
}
