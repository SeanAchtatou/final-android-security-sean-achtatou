package com.android.vending.licensing;

import android.content.SharedPreferences;

public final class d {
    private final SharedPreferences a;
    private final g b;
    private SharedPreferences.Editor c = null;

    public d(SharedPreferences sharedPreferences, g gVar) {
        this.a = sharedPreferences;
        this.b = gVar;
    }

    public final void a() {
        if (this.c != null) {
            this.c.commit();
            this.c = null;
        }
    }

    public final void a(String str, String str2) {
        if (this.c == null) {
            this.c = this.a.edit();
        }
        this.c.putString(str, this.b.a(str2));
    }

    public final String b(String str, String str2) {
        String string = this.a.getString(str, null);
        if (string != null) {
            try {
                return this.b.b(string);
            } catch (n e) {
            }
        }
        return str2;
    }
}
