package com.android.vending.licensing;

public enum v {
    INVALID_PACKAGE_NAME,
    NON_MATCHING_UID,
    NOT_MARKET_MANAGED,
    CHECK_IN_PROGRESS,
    INVALID_PUBLIC_KEY,
    MISSING_PERMISSION,
    CONNECTION_ERROR
}
