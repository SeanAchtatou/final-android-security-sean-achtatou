package com.kajirin.android.candylib;

import android.content.Context;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import com.kajirin.android.jppoker_f.R;

public class Setting extends PreferenceActivity {
    private static int b;
    /* access modifiers changed from: private */
    public static String[] c;
    private int a;
    private Preference.OnPreferenceChangeListener d = new o(this);

    public static int a(String str) {
        if (str.equals("game01")) {
            return 1;
        }
        if (str.equals("game02")) {
            return 2;
        }
        if (str.equals("game03")) {
            return 3;
        }
        if (str.equals("game04")) {
            return 4;
        }
        return str.equals("game05") ? 5 : 1;
    }

    public static String a(Context context, String str, String str2) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(str, str2);
    }

    public static boolean a(Context context, String str) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(str, true);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        c = new String[]{" ", (String) getText(R.string.game01), (String) getText(R.string.game02), (String) getText(R.string.game03), (String) getText(R.string.game04), (String) getText(R.string.game05)};
        setVolumeControlStream(3);
        this.a = a(a(this, "game_preference", "game01"));
        b = JpPokerLib.t;
        addPreferencesFromResource(R.xml.preferences);
        getPreferenceManager().findPreference("game_preference").setSummary(c[this.a]);
        ((ListPreference) findPreference("game_preference")).setOnPreferenceChangeListener(this.d);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (a(this, "toggle_preference")) {
            JpPokerLib.f = 1;
        } else {
            JpPokerLib.f = 0;
        }
        if (a(this, "toggle_preference2")) {
            JpPokerLib.t = 1;
        } else {
            JpPokerLib.t = 0;
        }
        if (b != JpPokerLib.t) {
            if (JpPokerLib.t == 0) {
                if (JpPokerLib.s != 0) {
                    JpPokerLib.r = 1;
                }
                JpPokerLib.s = 0;
            } else if (JpPokerLib.s != JpPokerLib.u) {
                JpPokerLib.s = JpPokerLib.u;
                JpPokerLib.r = 1;
            }
        }
        JpPokerLib.l = a(a(this, "game_preference", "game01"));
        if (this.a != JpPokerLib.l) {
            JpPokerLib.q = 1;
        }
    }
}
