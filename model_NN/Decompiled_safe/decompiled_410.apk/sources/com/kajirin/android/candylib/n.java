package com.kajirin.android.candylib;

import android.graphics.drawable.Drawable;
import android.text.Html;

final class n implements Html.ImageGetter {
    private /* synthetic */ Help a;

    n(Help help) {
        this.a = help;
    }

    public final Drawable getDrawable(String str) {
        Drawable drawable = this.a.getResources().getDrawable(this.a.getResources().getIdentifier(str, "drawable", this.a.getPackageName()));
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        return drawable;
    }
}
