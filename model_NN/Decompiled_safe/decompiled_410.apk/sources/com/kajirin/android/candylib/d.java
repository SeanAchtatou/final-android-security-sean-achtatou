package com.kajirin.android.candylib;

import android.app.Activity;
import android.content.DialogInterface;

final class d implements DialogInterface.OnClickListener {
    private /* synthetic */ JpPokerLib a;
    private final /* synthetic */ Activity b;

    d(JpPokerLib jpPokerLib, Activity activity) {
        this.a = jpPokerLib;
        this.b = activity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.b.setResult(-1);
        try {
            this.a.finish();
        } catch (Exception e) {
        }
    }
}
