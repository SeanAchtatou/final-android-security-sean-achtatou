package com.kajirin.android.candylib;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.kajirin.android.jppoker_f.R;

public final class b extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    private int A;
    private int B;
    private int C;
    private int D;
    private float E;
    private int F;
    private int G;
    private int H;
    private int I;
    private int J;
    private int K;
    private int L;
    private float M;
    private int N;
    private int O;
    private int P;
    private int Q;
    private int R;
    private int S;
    private int T;
    private int U;
    private long V;
    private long W;
    private int X;
    private int Y;
    private int Z;
    private final int a;
    private String aA;
    private String[] aB;
    private String[] aC;
    private int[][] aD;
    private int aa;
    private int ab;
    private int ac;
    private int ad;
    private int ae;
    private int af;
    private int[] ag;
    private int[] ah;
    private Rect[] ai;
    private Rect[] aj;
    private int[] ak;
    private int al;
    private int[] am;
    private int[] an;
    private int ao;
    private int ap;
    private int[] aq;
    private Bitmap ar;
    private Bitmap as;
    private Bitmap at;
    private Bitmap au;
    private Bitmap av;
    private Canvas aw;
    private Canvas ax;
    private int ay;
    private String az;
    private Handler b;
    private Runnable c;
    private float d;
    private float e;
    private int f;
    private SurfaceHolder g;
    private Thread h;
    private int i;
    private Bitmap[] j;
    private int[] k;
    private int[] l;
    private int[] m;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;

    public b(Context context) {
        super(context);
        this.a = 5000;
        this.b = new Handler();
        this.d = 1.0f;
        this.e = 1.0f;
        this.f = 0;
        this.i = 0;
        this.j = new Bitmap[9];
        this.k = new int[9];
        this.l = new int[9];
        this.m = new int[9];
        this.p = 0;
        this.q = 0;
        this.r = 0;
        this.s = 0;
        this.t = 0;
        this.u = 0;
        this.v = 0;
        this.w = 0;
        this.x = 0;
        this.y = 0;
        this.z = -999;
        this.A = 0;
        this.B = 0;
        this.C = 0;
        this.D = 0;
        this.K = 0;
        this.L = 0;
        this.ag = new int[10];
        this.ah = new int[5];
        this.ai = new Rect[10];
        this.aj = new Rect[5];
        this.ak = new int[52];
        this.am = new int[5];
        this.an = new int[5];
        this.aq = new int[11];
        this.ar = null;
        this.as = null;
        this.at = null;
        this.ay = 0;
        this.az = " ";
        this.aA = " ";
        this.aB = new String[]{"6-9 (800) Machine", "6-9 (800) Machine", "6-9 Machine", "5-8 (800) Machine", "5-8 Machine", "5-6 Machine"};
        this.aC = new String[]{"No Pair", "One Pair (Jacks OR Better)", "Two Pair", "Three Card", "Straight", "Flash", "Fullhouse", "Four Card", "Straight Flash", "Royal Straight Flash", "Jack Pot !"};
        this.aD = new int[][]{new int[]{1, 2, 3, 4, 6, 9, 25, 50, 800}, new int[]{1, 2, 3, 4, 6, 9, 25, 50, 800}, new int[]{1, 2, 3, 4, 6, 9, 25, 50, 250}, new int[]{1, 2, 3, 4, 5, 8, 25, 50, 800}, new int[]{1, 2, 3, 4, 5, 8, 25, 50, 250}, new int[]{1, 1, 2, 3, 5, 6, 25, 50, 250}};
        this.f = 0;
        this.p = 200;
        this.q = 200;
        this.e = context.getResources().getDisplayMetrics().density;
        this.d = this.e;
        this.n = 0;
        this.o = 0;
        this.g = getHolder();
        this.g.addCallback(this);
        this.g.setFixedSize(getWidth(), getHeight());
    }

    private int a(int i2, int i3) {
        boolean z2;
        int i4 = 0;
        while (true) {
            if (i4 < 5) {
                if (this.ag[i4] == 1 && a(i2, i3, this.aj[i4]) == 1) {
                    a(0);
                    c(i4);
                    z2 = true;
                    break;
                }
                i4++;
            } else {
                z2 = false;
                break;
            }
        }
        if (!z2) {
            int i5 = 0;
            while (true) {
                if (i5 >= 10) {
                    break;
                } else if (this.ag[i5] == 1 && a(i2, i3, this.ai[i5]) == 1) {
                    a(0);
                    if (i5 < 5) {
                        c(i5);
                    } else if (this.Y == 6 && i5 == 5) {
                        this.Y = 8;
                        a(4, this.ai[i5].left, this.ai[i5].top, 1);
                        this.V = System.currentTimeMillis() + 50;
                    } else if (this.Y == 6 && i5 == 9) {
                        this.Y = 9;
                        a(5, this.ai[i5].left, this.ai[i5].top, 1);
                        this.V = System.currentTimeMillis() + 50;
                    } else {
                        if (this.Y == 6 && (i5 == 6 || i5 == 7 || i5 == 8)) {
                            this.ag[5] = 0;
                            this.ag[9] = 0;
                            a(6, 15, 304, 0);
                            a(6, 15, 339, 0);
                            int i6 = JpPokerLib.d + this.ad;
                            JpPokerLib.d = i6;
                            if (i6 > 999999999) {
                                JpPokerLib.d = 999999999;
                            }
                            this.ad = 0;
                            i();
                            this.V = System.currentTimeMillis() + 50;
                            Rect rect = new Rect(173, 35, 246, 119);
                            this.aw.drawBitmap(this.as, rect, rect, (Paint) null);
                            Rect rect2 = new Rect(140, 3, 340, 36);
                            this.aw.drawBitmap(this.as, rect2, rect2, (Paint) null);
                            Paint paint = new Paint();
                            paint.setTextSize(18.0f);
                            paint.setColor(-65536);
                            this.aw.drawText(this.aB[JpPokerLib.k], 140.0f, 22.0f, paint);
                            this.Y = 0;
                            this.Z = 0;
                        }
                        if (this.Y == 0 && this.Z == 1) {
                            if (i5 == 6) {
                                a(1, 177, 304, 1);
                                if (JpPokerLib.d > 0) {
                                    if (this.ac < this.ae) {
                                        int i7 = this.ae - this.ac;
                                        if (i7 > JpPokerLib.d) {
                                            i7 = JpPokerLib.d;
                                        }
                                        JpPokerLib.d -= i7;
                                        this.ac = i7 + this.ac;
                                        i();
                                        j();
                                        if (this.ac == this.ae && this.ae > 5) {
                                            c(2, 173, 15);
                                        }
                                    }
                                    k();
                                } else {
                                    d(1);
                                    a(1, 177, 304, 0);
                                    i();
                                }
                                this.aa = 0;
                            }
                            if (i5 == 7) {
                                a(0, 258, 304, 1);
                                if (JpPokerLib.d > 0) {
                                    if (this.ac < this.ae) {
                                        JpPokerLib.d--;
                                        this.ac++;
                                        i();
                                        j();
                                        if (this.ac == this.ae && this.ae > 5) {
                                            c(2, 173, 15);
                                        }
                                    }
                                    if (this.ac == this.ae) {
                                        k();
                                    }
                                } else {
                                    d(1);
                                    a(0, 258, 304, 0);
                                    i();
                                }
                                this.ab = 0;
                            }
                            if (i5 == 8) {
                                k();
                            }
                        }
                        if (this.Y == 3 && i5 == 8) {
                            a(2, this.ai[i5].left, this.ai[i5].top, 1);
                            this.Y = 4;
                        }
                    }
                    z2 = true;
                } else {
                    i5++;
                }
            }
        }
        if (JpPokerLib.s == 1) {
            return 0;
        }
        if (!z2) {
            if (this.x > i2) {
                return 1;
            }
            if (this.y < i2) {
                return 3;
            }
        }
        return 2;
    }

    private static int a(int i2, int i3, Rect rect) {
        return (i3 <= rect.top || i3 > rect.bottom || i2 <= rect.left || i2 > rect.right) ? 0 : 1;
    }

    private static int a(int[] iArr, int i2) {
        int[] iArr2 = new int[5];
        int[] iArr3 = new int[5];
        int[] iArr4 = new int[4];
        int[] iArr5 = new int[13];
        for (int i3 = 0; i3 < 5; i3++) {
            iArr3[i3] = iArr[i3] % 13;
            iArr2[i3] = iArr[i3] / 13;
        }
        a(iArr3);
        a(iArr2);
        for (int i4 = 0; i4 < 4; i4++) {
            iArr4[i4] = 0;
        }
        for (int i5 = 0; i5 < 5; i5++) {
            for (int i6 = 0; i6 < 4; i6++) {
                if (i6 == iArr2[i5]) {
                    iArr4[i6] = iArr4[i6] + 1;
                }
            }
        }
        for (int i7 = 0; i7 < 13; i7++) {
            iArr5[i7] = 0;
        }
        for (int i8 = 0; i8 < 5; i8++) {
            for (int i9 = 0; i9 < 13; i9++) {
                if (i9 == iArr3[i8]) {
                    iArr5[i9] = iArr5[i9] + 1;
                }
            }
        }
        if (i2 < 11) {
            if (iArr3[0] + 1 == iArr3[1] && iArr3[0] + 2 == iArr3[2] && iArr3[0] + 3 == iArr3[3]) {
                int i10 = 0;
                while (i10 < 5) {
                    if (iArr3[0] == iArr[i10] / 13 || iArr3[1] == iArr[i10] / 13 || iArr3[2] == iArr[i10] / 13 || iArr3[3] == iArr[i10] / 13) {
                        i10++;
                    } else {
                        iArr[i10] = 99;
                        return 1;
                    }
                }
            } else if (iArr3[1] + 1 == iArr3[2] && iArr3[1] + 2 == iArr3[3] && iArr3[1] + 3 == iArr3[4]) {
                int i11 = 0;
                while (i11 < 5) {
                    if (iArr3[1] == iArr[i11] / 13 || iArr3[2] == iArr[i11] / 13 || iArr3[3] == iArr[i11] / 13 || iArr3[4] == iArr[i11] / 13) {
                        i11++;
                    } else {
                        iArr[i11] = 99;
                        return 1;
                    }
                }
            }
            int i12 = 0;
            for (int i13 = 0; i13 < 5; i13++) {
                if (iArr[i13] == 0 || iArr[i13] == 1 || iArr[i13] == 12 || iArr[i13] == 23 || iArr[i13] == 24) {
                    i12++;
                }
            }
            if (i12 > 1) {
                for (int i14 = 0; i14 < 5; i14++) {
                    if (!(iArr[i14] == 0 || iArr[i14] == 1 || iArr[i14] == 12 || iArr[i14] == 23 || iArr[i14] == 24)) {
                        iArr[i14] = 99;
                    }
                }
                return 1;
            }
            for (int i15 = 0; i15 < 4; i15++) {
                if (iArr4[i15] == 4) {
                    for (int i16 = 0; i16 < 5; i16++) {
                        if (iArr[i16] / 13 != i15) {
                            iArr[i16] = 99;
                        }
                    }
                    return 1;
                }
            }
            for (int i17 = 0; i17 < 5; i17++) {
                if (iArr[i17] % 13 < 9) {
                    iArr[i17] = 99;
                }
            }
            return 1;
        } else if (i2 < 15) {
            for (int i18 = 0; i18 < 5; i18++) {
                if (iArr[i18] % 13 != i2 - 2) {
                    iArr[i18] = 99;
                }
            }
            return 1;
        } else if (i2 < 1500) {
            for (int i19 = 0; i19 < 5; i19++) {
                if (!(iArr[i19] % 13 == (i2 / 100) - 2 || iArr[i19] % 13 == (i2 % 100) - 2)) {
                    iArr[i19] = 99;
                }
            }
            return 1;
        } else if (i2 < 2100) {
            for (int i20 = 0; i20 < 5; i20++) {
                if (iArr[i20] % 13 != (i2 % 100) - 2) {
                    iArr[i20] = 99;
                }
            }
            return 1;
        } else if (i2 < 2150) {
            return 0;
        } else {
            if (i2 == 2150) {
                return 0;
            }
            if (i2 < 3500) {
                return 0;
            }
            if (i2 < 3600) {
                return 0;
            }
            if (i2 < 3700) {
                return 0;
            }
            return i2 == 3700 ? 0 : 0;
        }
    }

    private int a(int[] iArr, int[] iArr2) {
        int[] iArr3 = new int[5];
        int[] iArr4 = new int[5];
        int[] iArr5 = new int[4];
        int[] iArr6 = new int[13];
        for (int i2 = 0; i2 < 5; i2++) {
            iArr4[i2] = iArr[i2] % 13;
            iArr3[i2] = iArr[i2] / 13;
        }
        a(iArr4);
        a(iArr3);
        for (int i3 = 0; i3 < 4; i3++) {
            iArr5[i3] = 0;
        }
        for (int i4 = 0; i4 < 5; i4++) {
            for (int i5 = 0; i5 < 4; i5++) {
                if (i5 == iArr3[i4]) {
                    iArr5[i5] = iArr5[i5] + 1;
                }
            }
        }
        for (int i6 = 0; i6 < 13; i6++) {
            iArr6[i6] = 0;
        }
        for (int i7 = 0; i7 < 5; i7++) {
            for (int i8 = 0; i8 < 13; i8++) {
                if (i8 == iArr4[i7]) {
                    iArr6[i8] = iArr6[i8] + 1;
                }
            }
        }
        if (this.ae == this.ac && iArr[0] == 0 && iArr[1] == 1 && iArr[2] == 12 && iArr[3] == 23 && iArr[4] == 24) {
            return 3800;
        }
        if ((iArr[0] == 8 && iArr[1] == 9 && iArr[2] == 10 && iArr[3] == 11 && iArr[4] == 12) || ((iArr[0] == 21 && iArr[1] == 22 && iArr[2] == 23 && iArr[3] == 24 && iArr[4] == 25) || ((iArr[0] == 34 && iArr[1] == 35 && iArr[2] == 36 && iArr[3] == 37 && iArr[4] == 38) || (iArr[0] == 47 && iArr[1] == 48 && iArr[2] == 49 && iArr[3] == 50 && iArr[4] == 51)))) {
            return 3700;
        }
        if (iArr5[0] == 5 || iArr5[1] == 5 || iArr5[2] == 5 || iArr5[3] == 5) {
            if (iArr4[0] + 1 == iArr4[1] && iArr4[0] + 2 == iArr4[2] && iArr4[0] + 3 == iArr4[3] && iArr4[0] + 4 == iArr4[4]) {
                return iArr4[4] + 3602;
            }
            if (iArr4[0] + 1 == iArr4[1] && iArr4[0] + 2 == iArr4[2] && iArr4[0] + 3 == iArr4[3] && iArr4[0] == (iArr4[4] + 1) % 13) {
                return 3605;
            }
        }
        for (int i9 = 0; i9 < 13; i9++) {
            if (iArr6[i9] == 4) {
                for (int i10 = 0; i10 < 5; i10++) {
                    if (iArr[i10] % 13 != i9) {
                        iArr2[0] = iArr[i10];
                    }
                }
                return i9 + 3502;
            }
        }
        for (int i11 = 0; i11 < 13; i11++) {
            if (iArr6[i11] == 3) {
                for (int i12 = 0; i12 < 13; i12++) {
                    if (iArr6[i12] == 2) {
                        return (i11 * 100) + 2200 + i12 + 2;
                    }
                }
                continue;
            }
        }
        if (iArr5[0] == 5 || iArr5[1] == 5 || iArr5[2] == 5 || iArr5[3] == 5) {
            for (int i13 = 0; i13 < 5; i13++) {
                iArr2[i13] = iArr[i13];
            }
            return 2150;
        } else if (iArr4[0] + 1 == iArr4[1] && iArr4[0] + 2 == iArr4[2] && iArr4[0] + 3 == iArr4[3] && iArr4[0] + 4 == iArr4[4]) {
            return iArr4[4] + 2102;
        } else {
            if (iArr4[0] + 1 == iArr4[1] && iArr4[0] + 2 == iArr4[2] && iArr4[0] + 3 == iArr4[3] && iArr4[0] == (iArr4[4] + 1) % 13) {
                return 2105;
            }
            for (int i14 = 0; i14 < 13; i14++) {
                if (iArr6[i14] == 3) {
                    int i15 = 0;
                    for (int i16 = 0; i16 < 5; i16++) {
                        if (iArr[i16] % 13 != i14) {
                            iArr2[i15] = iArr[i16];
                            i15++;
                        }
                    }
                    return i14 + 2002;
                }
            }
            for (int i17 = 0; i17 < 13; i17++) {
                if (iArr6[12 - i17] == 2) {
                    int i18 = 0;
                    while (i18 < 13) {
                        if (iArr6[i18] != 2 || 12 - i17 == i18) {
                            i18++;
                        } else {
                            for (int i19 = 0; i19 < 5; i19++) {
                                if (!(iArr[i19] % 13 == 12 - i17 || iArr[i19] % 13 == i18)) {
                                    iArr2[0] = iArr[i19];
                                }
                            }
                            return ((12 - i17) * 100) + 200 + i18 + 2;
                        }
                    }
                    continue;
                }
            }
            for (int i20 = 0; i20 < 13; i20++) {
                if (iArr6[i20] == 2) {
                    int i21 = 0;
                    for (int i22 = 0; i22 < 5; i22++) {
                        if (iArr[i22] % 13 != i20) {
                            iArr2[i21] = iArr[i22];
                            i21++;
                        }
                    }
                    return i20 + 2;
                }
            }
            for (int i23 = 0; i23 < 5; i23++) {
                iArr2[i23] = iArr[i23];
            }
            return 0;
        }
    }

    private void a() {
        this.X = 0;
        JpPokerLib.j = 0;
        this.Z = 0;
        this.Y = 0;
        this.S = 0;
        this.T = 0;
        for (int i2 = 0; i2 < 10; i2++) {
            this.ag[i2] = 0;
        }
        this.ag[6] = 1;
        this.ag[7] = 1;
        this.ag[8] = 1;
        this.ai[0] = new Rect(15, 269, 80, 293);
        this.ai[1] = new Rect(96, 269, 161, 293);
        this.ai[2] = new Rect(177, 269, 242, 293);
        this.ai[3] = new Rect(258, 269, 323, 293);
        this.ai[4] = new Rect(339, 269, 404, 293);
        this.ai[5] = new Rect(15, 304, 80, 328);
        this.ai[6] = new Rect(177, 304, 242, 328);
        this.ai[7] = new Rect(258, 304, 323, 328);
        this.ai[8] = new Rect(339, 304, 404, 328);
        this.ai[9] = new Rect(15, 339, 80, 363);
        this.aj[0] = new Rect(11, 145, 84, 249);
        this.aj[1] = new Rect(92, 145, 165, 249);
        this.aj[2] = new Rect(173, 145, 246, 249);
        this.aj[3] = new Rect(254, 145, 327, 249);
        this.aj[4] = new Rect(335, 145, 408, 249);
        for (int i3 = 0; i3 < 5; i3++) {
            this.ah[i3] = 0;
        }
        this.ao = 0;
        this.af = 0;
        this.Z = 0;
        this.Y = 0;
        this.aa = 0;
        this.ab = 0;
        this.ac = 0;
        this.ad = 0;
        this.ae = 0;
        d();
        e();
        f();
        g();
        this.U = 0;
        JpPokerLib.i.incrementProgressBy(10);
        if (JpPokerLib.e != JpPokerLib.b(JpPokerLib.d)) {
            JpPokerLib.d = 0;
            JpPokerLib.e = JpPokerLib.b(0);
            JpPokerLib.x = 500000;
            JpPokerLib.y = JpPokerLib.b(500000);
        }
        if (JpPokerLib.k <= 0 || JpPokerLib.k > 5) {
            JpPokerLib.k = 1;
        }
        if (JpPokerLib.x < 500000) {
            JpPokerLib.x = 500000;
            JpPokerLib.y = JpPokerLib.b(500000);
        }
        if (JpPokerLib.c != JpPokerLib.b(JpPokerLib.b)) {
            JpPokerLib.b = 0;
            JpPokerLib.c = JpPokerLib.b(0);
        }
        if (JpPokerLib.y != JpPokerLib.b(JpPokerLib.x)) {
            JpPokerLib.x = 500000;
            JpPokerLib.y = JpPokerLib.b(500000);
        }
        if (JpPokerLib.w != JpPokerLib.b(JpPokerLib.v)) {
            JpPokerLib.x = 500000;
            JpPokerLib.y = JpPokerLib.b(500000);
            JpPokerLib.d = 0;
            JpPokerLib.e = JpPokerLib.b(0);
            JpPokerLib.v = 0;
            JpPokerLib.w = JpPokerLib.b(0);
        }
        if (JpPokerLib.x > 999999999) {
            JpPokerLib.x = 999999999;
            JpPokerLib.y = JpPokerLib.b(999999999);
        }
        if (JpPokerLib.k <= 0 || JpPokerLib.k > 5) {
            JpPokerLib.k = 3;
        }
        if (JpPokerLib.d <= 0) {
            d(0);
        }
        if (JpPokerLib.x > 999999999) {
            JpPokerLib.x = 999999999;
            JpPokerLib.y = JpPokerLib.b(999999999);
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        Resources resources = getResources();
        this.as = BitmapFactory.decodeResource(resources, R.drawable.a00, options);
        JpPokerLib.i.incrementProgressBy(4);
        this.av = BitmapFactory.decodeResource(resources, R.drawable.b00, options);
        JpPokerLib.i.incrementProgressBy(4);
        this.au = BitmapFactory.decodeResource(resources, R.drawable.c00, options);
        JpPokerLib.i.incrementProgressBy(4);
        JpPokerLib.i.incrementProgressBy(4);
        JpPokerLib.i.incrementProgressBy(4);
        Rect rect = new Rect(0, 0, 419, 380);
        this.aw.drawBitmap(this.as, rect, rect, (Paint) null);
        this.ax.drawBitmap(this.as, rect, rect, (Paint) null);
        i();
        j();
        a(1, 177, 304, 0);
        a(0, 258, 304, 0);
        a(2, 339, 304, 0);
        if (this.ar != null) {
            Canvas lockCanvas = this.g.lockCanvas();
            if (JpPokerLib.s == 1) {
                lockCanvas.rotate(90.0f, 0.0f, (float) this.q);
            }
            if (this.d == 1.0f) {
                lockCanvas.drawBitmap(this.ar, new Rect(0, 0, 419, 380), JpPokerLib.s == 0 ? new Rect(this.r, this.s, this.r + 419, this.s + 380) : new Rect(this.r - this.q, this.s, (this.r - this.q) + 419, this.s + 380), (Paint) null);
            } else {
                Bitmap createScaledBitmap = Bitmap.createScaledBitmap(this.ar, (((int) (100.0f * this.d)) * 419) / 100, (((int) (100.0f * this.d)) * 380) / 100, true);
                if (JpPokerLib.s == 0) {
                    lockCanvas.drawBitmap(createScaledBitmap, (float) this.r, (float) this.s, (Paint) null);
                } else {
                    lockCanvas.drawBitmap(createScaledBitmap, (float) (this.r - this.q), (float) this.s, (Paint) null);
                }
            }
            this.g.unlockCanvasAndPost(lockCanvas);
        }
        this.V = System.currentTimeMillis() + 83;
        if (JpPokerLib.d < 10000) {
            this.ae = 10;
        } else if (JpPokerLib.d < 500000) {
            this.ae = 50;
        } else if (JpPokerLib.d < 1000000) {
            this.ae = 200;
        } else if (JpPokerLib.d < 50000000) {
            this.ae = 600;
        } else {
            this.ae = 1200;
        }
        i();
        JpPokerLib.g = false;
        JpPokerLib.i.incrementProgressBy(10);
    }

    private static void a(int i2) {
        if (JpPokerLib.f != 1) {
            return;
        }
        if (i2 == 0) {
            JpPokerLib.a(0);
        } else if (i2 == 1) {
            JpPokerLib.a(1);
        } else if (i2 == 2) {
            JpPokerLib.a(2);
        } else if (i2 == 3) {
            JpPokerLib.a(3);
        }
    }

    private void a(int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        if (JpPokerLib.s != 0) {
            if (this.d == 1.0f) {
                int i9 = (this.q - i3) - this.s;
                i5 = i4 - this.r;
                i6 = i9;
            } else {
                i5 = ((i4 - this.r) * 100) / ((int) (this.d * 100.0f));
                i6 = (((this.q - i3) - this.s) * 100) / ((int) (this.d * 100.0f));
            }
            int i10 = this.q - i3;
            i7 = i4 - this.q;
            i8 = i10;
        } else if (this.d == 1.0f) {
            i8 = i4;
            i7 = i3;
            int i11 = i4 - this.s;
            i5 = i3 - this.r;
            i6 = i11;
        } else {
            int i12 = ((i3 - this.r) * 100) / ((int) (this.d * 100.0f));
            i8 = i4;
            i7 = i3;
            i5 = i12;
            i6 = ((i4 - this.s) * 100) / ((int) (this.d * 100.0f));
        }
        if (i2 == 0) {
            if (this.n == 0) {
                JpPokerLib.m = i5;
                JpPokerLib.n = i6;
                JpPokerLib.o = i2;
            }
            this.n = 1;
            for (int i13 = 0; i13 < 9; i13++) {
                this.k[i13] = 0;
            }
            this.k[4] = 3;
            this.l[4] = i7;
            this.m[4] = i8;
        } else if (i2 == 2) {
            if (this.n == 1) {
                this.o = 1;
            } else if (this.o == 0) {
                JpPokerLib.m = i5;
                JpPokerLib.n = i6;
                JpPokerLib.o = 0;
                this.n = 1;
                for (int i14 = 0; i14 < 9; i14++) {
                    this.k[i14] = 0;
                }
                this.k[4] = 3;
                this.l[4] = i7;
                this.m[4] = i8;
            }
            if (this.o == 1) {
                JpPokerLib.m = i5;
                JpPokerLib.n = i6;
                JpPokerLib.o = i2;
            }
        }
        if (i2 == 1) {
            if (this.n == 1) {
                JpPokerLib.m = i5;
                JpPokerLib.n = i6;
                JpPokerLib.o = i2;
                this.k[4] = 0;
                this.k[5] = 3;
                this.l[5] = i7;
                this.m[5] = i8;
            }
            this.n = 0;
            this.o = 0;
        }
    }

    private void a(int i2, int i3, int i4, int i5) {
        int i6 = (i2 / 5) + (i5 * 3);
        int i7 = i2 % 5;
        this.aw.drawBitmap(this.av, new Rect((i7 * 65) + 0, (i6 * 24) + 104, (i7 * 65) + 65, (i6 * 24) + 128), new Rect(i3 + 0, i4 + 0, i3 + 65, i4 + 24), (Paint) null);
        this.X = 1;
    }

    private void a(Canvas canvas) {
        for (int i2 = 4; i2 < 6; i2++) {
            if (this.k[i2] > 0) {
                int[] iArr = this.k;
                iArr[i2] = iArr[i2] - 1;
                canvas.drawBitmap(this.j[i2], (float) (this.l[i2] - 35), (float) (this.m[i2] - 35), (Paint) null);
            }
        }
    }

    private void a(String str) {
        this.ay = 20;
        this.az = str;
        this.aA = " ";
    }

    private static void a(int[] iArr) {
        int i2 = 0;
        do {
            if (iArr[i2] > iArr[i2 + 1]) {
                int i3 = iArr[i2];
                iArr[i2] = iArr[i2 + 1];
                iArr[i2 + 1] = i3;
                i2 = 0;
            } else {
                i2++;
            }
        } while (i2 < 4);
    }

    private static int b(int i2, int i3) {
        int i4 = (i3 - i2) + 1;
        return i4 > 0 ? a.a(i4) + i2 : i2;
    }

    /* JADX WARN: Type inference failed for: r11v0, types: [com.kajirin.android.candylib.b] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void b() {
        /*
            r11 = this;
            r10 = 304(0x130, float:4.26E-43)
            r9 = 6
            r6 = 145(0x91, float:2.03E-43)
            r8 = 1
            r7 = 0
            android.graphics.Paint r0 = new android.graphics.Paint
            r0.<init>()
            int r1 = com.kajirin.android.candylib.JpPokerLib.q
            if (r1 != r8) goto L_0x0088
            r11.S = r7
            r11.T = r7
            r0 = r7
        L_0x0015:
            r1 = 10
            if (r0 < r1) goto L_0x0051
            int[] r0 = r11.ag
            r0[r9] = r8
            int[] r0 = r11.ag
            r1 = 7
            r0[r1] = r8
            int[] r0 = r11.ag
            r1 = 8
            r0[r1] = r8
            r0 = r7
        L_0x0029:
            r1 = 5
            if (r0 < r1) goto L_0x0058
            r11.ao = r7
            r11.af = r7
            r11.aa = r7
            r11.ab = r7
            r11.ac = r7
            r11.ad = r7
            r11.ae = r7
            r11.U = r7
            int r0 = com.kajirin.android.candylib.JpPokerLib.d
            r1 = 10000(0x2710, float:1.4013E-41)
            if (r0 >= r1) goto L_0x005f
            r0 = 10
            r11.ae = r0
        L_0x0046:
            r11.Z = r7
            r11.Y = r7
            int r0 = com.kajirin.android.candylib.JpPokerLib.l
            com.kajirin.android.candylib.JpPokerLib.k = r0
            com.kajirin.android.candylib.JpPokerLib.q = r7
        L_0x0050:
            return
        L_0x0051:
            int[] r1 = r11.ag
            r1[r0] = r7
            int r0 = r0 + 1
            goto L_0x0015
        L_0x0058:
            int[] r1 = r11.ah
            r1[r0] = r7
            int r0 = r0 + 1
            goto L_0x0029
        L_0x005f:
            int r0 = com.kajirin.android.candylib.JpPokerLib.d
            r1 = 500000(0x7a120, float:7.00649E-40)
            if (r0 >= r1) goto L_0x006b
            r0 = 50
            r11.ae = r0
            goto L_0x0046
        L_0x006b:
            int r0 = com.kajirin.android.candylib.JpPokerLib.d
            r1 = 1000000(0xf4240, float:1.401298E-39)
            if (r0 >= r1) goto L_0x0077
            r0 = 200(0xc8, float:2.8E-43)
            r11.ae = r0
            goto L_0x0046
        L_0x0077:
            int r0 = com.kajirin.android.candylib.JpPokerLib.d
            r1 = 50000000(0x2faf080, float:3.6872239E-37)
            if (r0 >= r1) goto L_0x0083
            r0 = 600(0x258, float:8.41E-43)
            r11.ae = r0
            goto L_0x0046
        L_0x0083:
            r0 = 1200(0x4b0, float:1.682E-42)
            r11.ae = r0
            goto L_0x0046
        L_0x0088:
            int r1 = com.kajirin.android.candylib.JpPokerLib.r
            if (r1 != r8) goto L_0x00ba
            int r1 = com.kajirin.android.candylib.JpPokerLib.s
            if (r1 != 0) goto L_0x026d
            int r1 = r11.C
            r11.p = r1
            int r1 = r11.D
            r11.q = r1
            float r1 = r11.E
            r11.d = r1
            int r1 = r11.F
            r11.r = r1
            int r1 = r11.G
            r11.s = r1
            int r1 = r11.H
            r11.t = r1
            int r1 = r11.I
            r11.u = r1
            int r1 = r11.J
            r11.v = r1
        L_0x00b0:
            long r1 = java.lang.System.currentTimeMillis()
            r11.V = r1
            r11.X = r8
            com.kajirin.android.candylib.JpPokerLib.r = r7
        L_0x00ba:
            int r1 = com.kajirin.android.candylib.JpPokerLib.o
            if (r1 != 0) goto L_0x00df
            int r1 = com.kajirin.android.candylib.JpPokerLib.m
            int r2 = com.kajirin.android.candylib.JpPokerLib.n
            int r1 = r11.a(r1, r2)
            int r2 = com.kajirin.android.candylib.JpPokerLib.p
            r3 = 2
            if (r2 != r3) goto L_0x00d5
            if (r1 != r8) goto L_0x028f
            int r1 = r11.u
            r11.r = r1
            r1 = 40
            r11.w = r1
        L_0x00d5:
            long r1 = java.lang.System.currentTimeMillis()
            r11.V = r1
            r1 = -999(0xfffffffffffffc19, float:NaN)
            com.kajirin.android.candylib.JpPokerLib.o = r1
        L_0x00df:
            int r1 = com.kajirin.android.candylib.JpPokerLib.o
            r2 = 2
            if (r1 != r2) goto L_0x00ee
            long r1 = java.lang.System.currentTimeMillis()
            r11.V = r1
            r1 = -999(0xfffffffffffffc19, float:NaN)
            com.kajirin.android.candylib.JpPokerLib.o = r1
        L_0x00ee:
            int r1 = com.kajirin.android.candylib.JpPokerLib.o
            if (r1 != r8) goto L_0x0110
            int r1 = r11.Y
            if (r1 != 0) goto L_0x02a7
            r1 = 177(0xb1, float:2.48E-43)
            r11.a(r8, r1, r10, r7)
            r1 = 258(0x102, float:3.62E-43)
            r11.a(r7, r1, r10, r7)
        L_0x0100:
            r1 = 2
            r2 = 339(0x153, float:4.75E-43)
            r11.a(r1, r2, r10, r7)
        L_0x0106:
            long r1 = java.lang.System.currentTimeMillis()
            r11.V = r1
            r1 = -999(0xfffffffffffffc19, float:NaN)
            com.kajirin.android.candylib.JpPokerLib.o = r1
        L_0x0110:
            long r1 = java.lang.System.currentTimeMillis()
            long r3 = r11.V
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x0df4
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 166(0xa6, double:8.2E-322)
            long r1 = r1 + r3
            r11.V = r1
            int r1 = r11.w
            if (r1 <= 0) goto L_0x0134
            int r1 = r11.w
            int r1 = r1 - r8
            r11.w = r1
            int r1 = r11.w
            if (r1 != 0) goto L_0x0134
            int r1 = r11.t
            r11.r = r1
        L_0x0134:
            int r1 = r11.U
            int r1 = r1 + 1
            r11.U = r1
            int r1 = r11.U
            r2 = 70
            if (r1 <= r2) goto L_0x0142
            r11.U = r7
        L_0x0142:
            int r1 = com.kajirin.android.candylib.JpPokerLib.p
            if (r1 != r8) goto L_0x02dc
            int r1 = r11.u
            r11.r = r1
            r1 = 2
            r11.w = r1
        L_0x014d:
            int r1 = com.kajirin.android.candylib.JpPokerLib.j
            if (r1 != r8) goto L_0x0153
            com.kajirin.android.candylib.JpPokerLib.j = r7
        L_0x0153:
            int r1 = r11.Y
            switch(r1) {
                case 0: goto L_0x02ea;
                case 1: goto L_0x03d6;
                case 2: goto L_0x0433;
                case 3: goto L_0x0510;
                case 4: goto L_0x0541;
                case 5: goto L_0x08a0;
                case 6: goto L_0x08e5;
                case 7: goto L_0x095a;
                case 8: goto L_0x09d6;
                case 9: goto L_0x0a2f;
                case 10: goto L_0x0a9b;
                case 11: goto L_0x0bdc;
                case 12: goto L_0x0c00;
                case 13: goto L_0x0158;
                case 14: goto L_0x0158;
                case 15: goto L_0x0158;
                case 16: goto L_0x0158;
                case 17: goto L_0x0158;
                case 18: goto L_0x0158;
                case 19: goto L_0x0158;
                case 20: goto L_0x0d6a;
                default: goto L_0x0158;
            }
        L_0x0158:
            r11.X = r8
        L_0x015a:
            long r1 = java.lang.System.currentTimeMillis()
            long r3 = r11.W
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x018f
            int r1 = r11.Y
            r2 = 3
            if (r1 == r2) goto L_0x018f
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 5000(0x1388, double:2.4703E-320)
            long r1 = r1 + r3
            r11.W = r1
            int r1 = com.kajirin.android.candylib.JpPokerLib.x
            int r1 = r1 + 1
            com.kajirin.android.candylib.JpPokerLib.x = r1
            r2 = 999999999(0x3b9ac9ff, float:0.004723787)
            if (r1 <= r2) goto L_0x0182
            r1 = 999999999(0x3b9ac9ff, float:0.004723787)
            com.kajirin.android.candylib.JpPokerLib.x = r1
        L_0x0182:
            int r1 = com.kajirin.android.candylib.JpPokerLib.x
            r2 = 360(0x168, float:5.04E-43)
            r3 = 59
            r11.d(r1, r2, r3)
            r1 = 3
            a(r1)
        L_0x018f:
            int r1 = r11.X
            if (r1 != r8) goto L_0x0050
            int r1 = r11.Y
            r2 = 4
            if (r1 != r2) goto L_0x01a3
            int r1 = r11.Z
            r2 = 8
            if (r1 != r2) goto L_0x01a3
            int r1 = r11.ao
            r11.b(r1)
        L_0x01a3:
            int r1 = r11.ay
            if (r1 <= 0) goto L_0x021d
            r0.setAntiAlias(r8)
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.FILL
            r0.setStyle(r1)
            int r1 = r11.ay
            if (r1 <= r8) goto L_0x0218
            r1 = 1098907648(0x41800000, float:16.0)
            r0.setTextSize(r1)
            r1 = -7829368(0xffffffffff888888, float:NaN)
            r0.setColor(r1)
            android.graphics.Canvas r1 = r11.aw
            android.graphics.Rect r2 = new android.graphics.Rect
            r3 = 21
            r4 = 176(0xb0, float:2.47E-43)
            r5 = 291(0x123, float:4.08E-43)
            r6 = 231(0xe7, float:3.24E-43)
            r2.<init>(r3, r4, r5, r6)
            r1.drawRect(r2, r0)
            r1 = -65536(0xffffffffffff0000, float:NaN)
            r0.setColor(r1)
            android.graphics.Canvas r1 = r11.aw
            android.graphics.Rect r2 = new android.graphics.Rect
            r3 = 20
            r4 = 175(0xaf, float:2.45E-43)
            r5 = 290(0x122, float:4.06E-43)
            r6 = 230(0xe6, float:3.22E-43)
            r2.<init>(r3, r4, r5, r6)
            r1.drawRect(r2, r0)
            r1 = -1
            r0.setColor(r1)
            android.graphics.Canvas r1 = r11.aw
            android.graphics.Rect r2 = new android.graphics.Rect
            r3 = 21
            r4 = 176(0xb0, float:2.47E-43)
            r5 = 288(0x120, float:4.04E-43)
            r6 = 228(0xe4, float:3.2E-43)
            r2.<init>(r3, r4, r5, r6)
            r1.drawRect(r2, r0)
            r1 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0.setColor(r1)
            android.graphics.Canvas r1 = r11.aw
            java.lang.String r2 = r11.az
            r3 = 1108082688(0x420c0000, float:35.0)
            r4 = 1128792064(0x43480000, float:200.0)
            r1.drawText(r2, r3, r4, r0)
            android.graphics.Canvas r1 = r11.aw
            java.lang.String r2 = r11.aA
            r3 = 1108082688(0x420c0000, float:35.0)
            r4 = 1130102784(0x435c0000, float:220.0)
            r1.drawText(r2, r3, r4, r0)
        L_0x0218:
            int r0 = r11.ay
            int r0 = r0 - r8
            r11.ay = r0
        L_0x021d:
            android.view.SurfaceHolder r0 = r11.g
            android.graphics.Canvas r0 = r0.lockCanvas()
            r1 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0.drawColor(r1)
            int r1 = com.kajirin.android.candylib.JpPokerLib.s
            if (r1 != r8) goto L_0x0235
            r1 = 1119092736(0x42b40000, float:90.0)
            r2 = 0
            int r3 = r11.q
            float r3 = (float) r3
            r0.rotate(r1, r2, r3)
        L_0x0235:
            float r1 = r11.d
            r2 = 1065353216(0x3f800000, float:1.0)
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 != 0) goto L_0x0e17
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 419(0x1a3, float:5.87E-43)
            r3 = 380(0x17c, float:5.32E-43)
            r1.<init>(r7, r7, r2, r3)
            int r2 = com.kajirin.android.candylib.JpPokerLib.s
            if (r2 != 0) goto L_0x0dfe
            android.graphics.Rect r2 = new android.graphics.Rect
            int r3 = r11.r
            int r4 = r11.s
            int r5 = r11.r
            int r5 = r5 + 419
            int r6 = r11.s
            int r6 = r6 + 380
            r2.<init>(r3, r4, r5, r6)
        L_0x025b:
            android.graphics.Bitmap r3 = r11.ar
            r4 = 0
            r0.drawBitmap(r3, r1, r2, r4)
        L_0x0261:
            r11.a(r0)
            android.view.SurfaceHolder r1 = r11.g
            r1.unlockCanvasAndPost(r0)
            r11.X = r7
            goto L_0x0050
        L_0x026d:
            int r1 = r11.K
            r11.p = r1
            int r1 = r11.L
            r11.q = r1
            float r1 = r11.M
            r11.d = r1
            int r1 = r11.N
            r11.r = r1
            int r1 = r11.O
            r11.s = r1
            int r1 = r11.P
            r11.t = r1
            int r1 = r11.Q
            r11.u = r1
            int r1 = r11.R
            r11.v = r1
            goto L_0x00b0
        L_0x028f:
            r2 = 2
            if (r1 != r2) goto L_0x029a
            int r1 = r11.t
            r11.r = r1
            r11.w = r7
            goto L_0x00d5
        L_0x029a:
            r2 = 3
            if (r1 != r2) goto L_0x00d5
            int r1 = r11.v
            r11.r = r1
            r1 = 40
            r11.w = r1
            goto L_0x00d5
        L_0x02a7:
            int r1 = r11.Y
            if (r1 == r8) goto L_0x02b0
            int r1 = r11.Y
            r2 = 2
            if (r1 != r2) goto L_0x02d0
        L_0x02b0:
            int[] r1 = r11.ag
            r1[r9] = r7
            int[] r1 = r11.ag
            r2 = 7
            r1[r2] = r7
            int[] r1 = r11.ag
            r2 = 8
            r1[r2] = r7
            r1 = 177(0xb1, float:2.48E-43)
            r11.a(r9, r1, r10, r7)
            r1 = 258(0x102, float:3.62E-43)
            r11.a(r9, r1, r10, r7)
            r1 = 339(0x153, float:4.75E-43)
            r11.a(r9, r1, r10, r7)
            goto L_0x0106
        L_0x02d0:
            int r1 = r11.Y
            r2 = 3
            if (r1 == r2) goto L_0x0100
            int r1 = r11.Y
            r2 = 4
            if (r1 != r2) goto L_0x0106
            goto L_0x0100
        L_0x02dc:
            int r1 = com.kajirin.android.candylib.JpPokerLib.p
            r2 = 3
            if (r1 != r2) goto L_0x014d
            int r1 = r11.v
            r11.r = r1
            r1 = 2
            r11.w = r1
            goto L_0x014d
        L_0x02ea:
            int r1 = r11.aa
            if (r1 == 0) goto L_0x0300
            int r1 = r11.aa
            int r1 = r1 + 1
            r11.aa = r1
            int r1 = r11.aa
            r2 = 2
            if (r1 <= r2) goto L_0x0300
            r1 = 177(0xb1, float:2.48E-43)
            r11.a(r8, r1, r10, r7)
            r11.aa = r7
        L_0x0300:
            int r1 = r11.ab
            if (r1 == 0) goto L_0x0316
            int r1 = r11.ab
            int r1 = r1 + 1
            r11.ab = r1
            int r1 = r11.ab
            r2 = 2
            if (r1 <= r2) goto L_0x0316
            r1 = 258(0x102, float:3.62E-43)
            r11.a(r7, r1, r10, r7)
            r11.ab = r7
        L_0x0316:
            int r1 = r11.Z
            if (r1 != 0) goto L_0x036d
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 140(0x8c, float:1.96E-43)
            r3 = 3
            r4 = 340(0x154, float:4.76E-43)
            r5 = 36
            r1.<init>(r2, r3, r4, r5)
            android.graphics.Canvas r2 = r11.aw
            android.graphics.Bitmap r3 = r11.as
            r4 = 0
            r2.drawBitmap(r3, r1, r1, r4)
            r1 = 1099956224(0x41900000, float:18.0)
            r0.setTextSize(r1)
            r1 = -65536(0xffffffffffff0000, float:NaN)
            r0.setColor(r1)
            android.graphics.Canvas r1 = r11.aw
            java.lang.String[] r2 = r11.aB
            int r3 = com.kajirin.android.candylib.JpPokerLib.k
            r2 = r2[r3]
            r3 = 1124859904(0x430c0000, float:140.0)
            r4 = 1102053376(0x41b00000, float:22.0)
            r1.drawText(r2, r3, r4, r0)
            r11.j()
            int r1 = com.kajirin.android.candylib.JpPokerLib.d
            r2 = 10000(0x2710, float:1.4013E-41)
            if (r1 >= r2) goto L_0x0395
            r1 = 10
            r11.ae = r1
        L_0x0354:
            r11.i()
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 5
            r3 = 115(0x73, float:1.61E-43)
            r4 = 412(0x19c, float:5.77E-43)
            r5 = 294(0x126, float:4.12E-43)
            r1.<init>(r2, r3, r4, r5)
            android.graphics.Canvas r2 = r11.aw
            android.graphics.Bitmap r3 = r11.as
            r4 = 0
            r2.drawBitmap(r3, r1, r1, r4)
            r11.Z = r8
        L_0x036d:
            int r1 = r11.S
            r2 = 5
            if (r1 != r2) goto L_0x0158
            int r1 = com.kajirin.android.candylib.JpPokerLib.d
            int r2 = r11.ae
            if (r1 < r2) goto L_0x03be
            int r1 = r11.ae
            r11.ac = r1
            int r1 = com.kajirin.android.candylib.JpPokerLib.d
            int r2 = r11.ae
            int r1 = r1 - r2
            com.kajirin.android.candylib.JpPokerLib.d = r1
            r11.i()
            r11.j()
            r1 = 2
            r2 = 173(0xad, float:2.42E-43)
            r3 = 15
            r11.c(r1, r2, r3)
            r11.Y = r8
            goto L_0x0158
        L_0x0395:
            int r1 = com.kajirin.android.candylib.JpPokerLib.d
            r2 = 500000(0x7a120, float:7.00649E-40)
            if (r1 >= r2) goto L_0x03a1
            r1 = 50
            r11.ae = r1
            goto L_0x0354
        L_0x03a1:
            int r1 = com.kajirin.android.candylib.JpPokerLib.d
            r2 = 1000000(0xf4240, float:1.401298E-39)
            if (r1 >= r2) goto L_0x03ad
            r1 = 200(0xc8, float:2.8E-43)
            r11.ae = r1
            goto L_0x0354
        L_0x03ad:
            int r1 = com.kajirin.android.candylib.JpPokerLib.d
            r2 = 50000000(0x2faf080, float:3.6872239E-37)
            if (r1 >= r2) goto L_0x03b9
            r1 = 600(0x258, float:8.41E-43)
            r11.ae = r1
            goto L_0x0354
        L_0x03b9:
            r1 = 1200(0x4b0, float:1.682E-42)
            r11.ae = r1
            goto L_0x0354
        L_0x03be:
            int r1 = com.kajirin.android.candylib.JpPokerLib.d
            if (r1 != 0) goto L_0x03c6
            r11.S = r7
            goto L_0x0158
        L_0x03c6:
            int r1 = com.kajirin.android.candylib.JpPokerLib.d
            r11.ac = r1
            com.kajirin.android.candylib.JpPokerLib.d = r7
            r11.i()
            r11.j()
            r11.Y = r8
            goto L_0x0158
        L_0x03d6:
            r11.aa = r7
            r11.ab = r7
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 5
            r3 = 115(0x73, float:1.61E-43)
            r4 = 412(0x19c, float:5.77E-43)
            r5 = 294(0x126, float:4.12E-43)
            r1.<init>(r2, r3, r4, r5)
            android.graphics.Canvas r2 = r11.aw
            android.graphics.Bitmap r3 = r11.as
            r4 = 0
            r2.drawBitmap(r3, r1, r1, r4)
            r11.e()
            r11.f()
            r11.g()
            r11.h()
            int r1 = com.kajirin.android.candylib.JpPokerLib.b
            int r1 = r1 + 1
            com.kajirin.android.candylib.JpPokerLib.b = r1
            r2 = 999999999(0x3b9ac9ff, float:0.004723787)
            if (r1 <= r2) goto L_0x0407
            com.kajirin.android.candylib.JpPokerLib.b = r7
        L_0x0407:
            r11.Z = r7
            r1 = 11
            r11.c(r8, r1, r6)
            r1 = 92
            r11.c(r8, r1, r6)
            r1 = 173(0xad, float:2.42E-43)
            r11.c(r8, r1, r6)
            r1 = 254(0xfe, float:3.56E-43)
            r11.c(r8, r1, r6)
            r1 = 335(0x14f, float:4.7E-43)
            r11.c(r8, r1, r6)
            r11.i()
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 + r3
            r11.V = r1
            r1 = 2
            r11.Y = r1
            goto L_0x0158
        L_0x0433:
            int r1 = r11.Z
            switch(r1) {
                case 0: goto L_0x044e;
                case 1: goto L_0x046d;
                case 2: goto L_0x0478;
                case 3: goto L_0x0483;
                case 4: goto L_0x048e;
                case 5: goto L_0x0499;
                default: goto L_0x0438;
            }
        L_0x0438:
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 83
            long r1 = r1 + r3
            r11.V = r1
            int r1 = r11.Y
            r2 = 2
            if (r1 != r2) goto L_0x050c
            int r1 = r11.Z
            int r1 = r1 + 1
            r11.Z = r1
            goto L_0x0158
        L_0x044e:
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 5
            r3 = 115(0x73, float:1.61E-43)
            r4 = 412(0x19c, float:5.77E-43)
            r5 = 294(0x126, float:4.12E-43)
            r1.<init>(r2, r3, r4, r5)
            android.graphics.Canvas r2 = r11.aw
            android.graphics.Bitmap r3 = r11.as
            r4 = 0
            r2.drawBitmap(r3, r1, r1, r4)
            a(r8)
            r1 = 55
            r2 = 11
            r11.b(r1, r2, r6)
            goto L_0x0438
        L_0x046d:
            a(r8)
            r1 = 55
            r2 = 92
            r11.b(r1, r2, r6)
            goto L_0x0438
        L_0x0478:
            a(r8)
            r1 = 55
            r2 = 173(0xad, float:2.42E-43)
            r11.b(r1, r2, r6)
            goto L_0x0438
        L_0x0483:
            a(r8)
            r1 = 55
            r2 = 254(0xfe, float:3.56E-43)
            r11.b(r1, r2, r6)
            goto L_0x0438
        L_0x048e:
            a(r8)
            r1 = 55
            r2 = 335(0x14f, float:4.7E-43)
            r11.b(r1, r2, r6)
            goto L_0x0438
        L_0x0499:
            int[] r1 = r11.ag
            r1[r9] = r7
            int[] r1 = r11.ag
            r2 = 7
            r1[r2] = r7
            int[] r1 = r11.ag
            r2 = 8
            r1[r2] = r8
            r1 = 177(0xb1, float:2.48E-43)
            r11.a(r9, r1, r10, r7)
            r1 = 258(0x102, float:3.62E-43)
            r11.a(r9, r1, r10, r7)
            r1 = 2
            r2 = 339(0x153, float:4.75E-43)
            r11.a(r1, r2, r10, r7)
            int[] r1 = r11.am
            r1 = r1[r7]
            r2 = 11
            r11.b(r1, r2, r6)
            int[] r1 = r11.am
            r1 = r1[r8]
            r2 = 92
            r11.b(r1, r2, r6)
            int[] r1 = r11.am
            r2 = 2
            r1 = r1[r2]
            r2 = 173(0xad, float:2.42E-43)
            r11.b(r1, r2, r6)
            int[] r1 = r11.am
            r2 = 3
            r1 = r1[r2]
            r2 = 254(0xfe, float:3.56E-43)
            r11.b(r1, r2, r6)
            int[] r1 = r11.am
            r2 = 4
            r1 = r1[r2]
            r2 = 335(0x14f, float:4.7E-43)
            r11.b(r1, r2, r6)
            r1 = r7
        L_0x04e9:
            r2 = 5
            if (r1 < r2) goto L_0x04f1
            r1 = 3
            r11.Y = r1
            goto L_0x0438
        L_0x04f1:
            int[] r2 = r11.ah
            r2[r1] = r7
            int[] r2 = r11.ag
            r2[r1] = r8
            r2 = 3
            android.graphics.Rect[] r3 = r11.ai
            r3 = r3[r1]
            int r3 = r3.left
            android.graphics.Rect[] r4 = r11.ai
            r4 = r4[r1]
            int r4 = r4.top
            r11.a(r2, r3, r4, r7)
            int r1 = r1 + 1
            goto L_0x04e9
        L_0x050c:
            r11.Z = r7
            goto L_0x0158
        L_0x0510:
            int r1 = r11.S
            r2 = 5
            if (r1 != r2) goto L_0x0158
            int[] r1 = r11.am
            a(r1)
            int[] r1 = r11.am
            int[] r2 = r11.an
            int r1 = r11.a(r1, r2)
            int[] r2 = r11.an
            b(r2)
            int[] r2 = r11.am
            a(r2, r1)
            int[] r1 = r11.am
            a(r1)
            r1 = r7
        L_0x0532:
            r2 = 5
            if (r1 < r2) goto L_0x053a
            r1 = 4
            r11.Y = r1
            goto L_0x0158
        L_0x053a:
            int[] r2 = r11.ah
            r2[r1] = r8
            int r1 = r1 + 1
            goto L_0x0532
        L_0x0541:
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 83
            long r1 = r1 + r3
            r11.V = r1
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 5
            r3 = 115(0x73, float:1.61E-43)
            r4 = 412(0x19c, float:5.77E-43)
            r5 = 144(0x90, float:2.02E-43)
            r1.<init>(r2, r3, r4, r5)
            android.graphics.Canvas r2 = r11.aw
            android.graphics.Bitmap r3 = r11.as
            r4 = 0
            r2.drawBitmap(r3, r1, r1, r4)
            int r1 = r11.Z
            switch(r1) {
                case 0: goto L_0x0570;
                case 1: goto L_0x05fe;
                case 2: goto L_0x061d;
                case 3: goto L_0x063c;
                case 4: goto L_0x065c;
                case 5: goto L_0x067c;
                case 6: goto L_0x069c;
                case 7: goto L_0x06da;
                case 8: goto L_0x086f;
                default: goto L_0x0563;
            }
        L_0x0563:
            int r1 = r11.Y
            r2 = 4
            if (r1 != r2) goto L_0x089c
            int r1 = r11.Z
            int r1 = r1 + 1
            r11.Z = r1
            goto L_0x0158
        L_0x0570:
            r1 = r7
        L_0x0571:
            r2 = 5
            if (r1 < r2) goto L_0x05b9
            int[] r1 = r11.am
            r1 = r1[r7]
            r2 = 99
            if (r1 != r2) goto L_0x05c8
            r1 = 11
            r11.c(r8, r1, r6)
        L_0x0581:
            int[] r1 = r11.am
            r1 = r1[r8]
            r2 = 99
            if (r1 != r2) goto L_0x05d2
            r1 = 92
            r11.c(r8, r1, r6)
        L_0x058e:
            int[] r1 = r11.am
            r2 = 2
            r1 = r1[r2]
            r2 = 99
            if (r1 != r2) goto L_0x05dc
            r1 = 173(0xad, float:2.42E-43)
            r11.c(r8, r1, r6)
        L_0x059c:
            int[] r1 = r11.am
            r2 = 3
            r1 = r1[r2]
            r2 = 99
            if (r1 != r2) goto L_0x05e7
            r1 = 254(0xfe, float:3.56E-43)
            r11.c(r8, r1, r6)
        L_0x05aa:
            int[] r1 = r11.am
            r2 = 4
            r1 = r1[r2]
            r2 = 99
            if (r1 != r2) goto L_0x05f2
            r1 = 335(0x14f, float:4.7E-43)
            r11.c(r8, r1, r6)
            goto L_0x0563
        L_0x05b9:
            int[] r2 = r11.ah
            r2 = r2[r1]
            if (r2 != 0) goto L_0x05c5
            int[] r2 = r11.am
            r3 = 99
            r2[r1] = r3
        L_0x05c5:
            int r1 = r1 + 1
            goto L_0x0571
        L_0x05c8:
            int[] r1 = r11.am
            r1 = r1[r7]
            r2 = 11
            r11.b(r1, r2, r6)
            goto L_0x0581
        L_0x05d2:
            int[] r1 = r11.am
            r1 = r1[r8]
            r2 = 92
            r11.b(r1, r2, r6)
            goto L_0x058e
        L_0x05dc:
            int[] r1 = r11.am
            r2 = 2
            r1 = r1[r2]
            r2 = 173(0xad, float:2.42E-43)
            r11.b(r1, r2, r6)
            goto L_0x059c
        L_0x05e7:
            int[] r1 = r11.am
            r2 = 3
            r1 = r1[r2]
            r2 = 254(0xfe, float:3.56E-43)
            r11.b(r1, r2, r6)
            goto L_0x05aa
        L_0x05f2:
            int[] r1 = r11.am
            r2 = 4
            r1 = r1[r2]
            r2 = 335(0x14f, float:4.7E-43)
            r11.b(r1, r2, r6)
            goto L_0x0563
        L_0x05fe:
            int[] r1 = r11.am
            r1 = r1[r7]
            r2 = 99
            if (r1 != r2) goto L_0x0612
            a(r8)
            r1 = 55
            r2 = 11
            r11.b(r1, r2, r6)
            goto L_0x0563
        L_0x0612:
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 50
            long r1 = r1 + r3
            r11.V = r1
            goto L_0x0563
        L_0x061d:
            int[] r1 = r11.am
            r1 = r1[r8]
            r2 = 99
            if (r1 != r2) goto L_0x0631
            a(r8)
            r1 = 55
            r2 = 92
            r11.b(r1, r2, r6)
            goto L_0x0563
        L_0x0631:
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 50
            long r1 = r1 + r3
            r11.V = r1
            goto L_0x0563
        L_0x063c:
            int[] r1 = r11.am
            r2 = 2
            r1 = r1[r2]
            r2 = 99
            if (r1 != r2) goto L_0x0651
            a(r8)
            r1 = 55
            r2 = 173(0xad, float:2.42E-43)
            r11.b(r1, r2, r6)
            goto L_0x0563
        L_0x0651:
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 50
            long r1 = r1 + r3
            r11.V = r1
            goto L_0x0563
        L_0x065c:
            int[] r1 = r11.am
            r2 = 3
            r1 = r1[r2]
            r2 = 99
            if (r1 != r2) goto L_0x0671
            a(r8)
            r1 = 55
            r2 = 254(0xfe, float:3.56E-43)
            r11.b(r1, r2, r6)
            goto L_0x0563
        L_0x0671:
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 50
            long r1 = r1 + r3
            r11.V = r1
            goto L_0x0563
        L_0x067c:
            int[] r1 = r11.am
            r2 = 4
            r1 = r1[r2]
            r2 = 99
            if (r1 != r2) goto L_0x0691
            a(r8)
            r1 = 55
            r2 = 335(0x14f, float:4.7E-43)
            r11.b(r1, r2, r6)
            goto L_0x0563
        L_0x0691:
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 50
            long r1 = r1 + r3
            r11.V = r1
            goto L_0x0563
        L_0x069c:
            r11.h()
            int[] r1 = r11.am
            r1 = r1[r7]
            r2 = 11
            r11.b(r1, r2, r6)
            int[] r1 = r11.am
            r1 = r1[r8]
            r2 = 92
            r11.b(r1, r2, r6)
            int[] r1 = r11.am
            r2 = 2
            r1 = r1[r2]
            r2 = 173(0xad, float:2.42E-43)
            r11.b(r1, r2, r6)
            int[] r1 = r11.am
            r2 = 3
            r1 = r1[r2]
            r2 = 254(0xfe, float:3.56E-43)
            r11.b(r1, r2, r6)
            int[] r1 = r11.am
            r2 = 4
            r1 = r1[r2]
            r2 = 335(0x14f, float:4.7E-43)
            r11.b(r1, r2, r6)
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 166(0xa6, double:8.2E-322)
            long r1 = r1 + r3
            r11.V = r1
            goto L_0x0563
        L_0x06da:
            r1 = r7
        L_0x06db:
            r2 = 5
            if (r1 < r2) goto L_0x070d
            int[] r1 = r11.ag
            r2 = 8
            r1[r2] = r7
            r1 = 339(0x153, float:4.75E-43)
            r11.a(r9, r1, r10, r7)
            int[] r1 = r11.am
            a(r1)
            int[] r1 = r11.am
            int[] r2 = r11.an
            int r1 = r11.a(r1, r2)
            r11.ao = r1
            int r1 = r11.ao
            r2 = 11
            if (r1 >= r2) goto L_0x0727
            int[] r1 = r11.aq
            r2 = r1[r7]
            int r2 = r2 + 1
            r1[r7] = r2
            r11.ac = r7
        L_0x0708:
            r11.i()
            goto L_0x0563
        L_0x070d:
            int[] r2 = r11.ah
            r2[r1] = r7
            int[] r2 = r11.ag
            r2[r1] = r7
            android.graphics.Rect[] r2 = r11.ai
            r2 = r2[r1]
            int r2 = r2.left
            android.graphics.Rect[] r3 = r11.ai
            r3 = r3[r1]
            int r3 = r3.top
            r11.a(r9, r2, r3, r7)
            int r1 = r1 + 1
            goto L_0x06db
        L_0x0727:
            r2 = 15
            if (r1 >= r2) goto L_0x0746
            int[] r1 = r11.aq
            r2 = r1[r8]
            int r2 = r2 + 1
            r1[r8] = r2
            int[][] r1 = r11.aD
            int r2 = com.kajirin.android.candylib.JpPokerLib.k
            r1 = r1[r2]
            r1 = r1[r7]
            int r2 = r11.ac
            int r1 = r1 * r2
            int r2 = r11.ac
            int r1 = r1 + r2
            r11.ad = r1
            r11.ac = r7
            goto L_0x0708
        L_0x0746:
            r2 = 1500(0x5dc, float:2.102E-42)
            if (r1 >= r2) goto L_0x0766
            int[] r1 = r11.aq
            r2 = 2
            r3 = r1[r2]
            int r3 = r3 + 1
            r1[r2] = r3
            int[][] r1 = r11.aD
            int r2 = com.kajirin.android.candylib.JpPokerLib.k
            r1 = r1[r2]
            r1 = r1[r8]
            int r2 = r11.ac
            int r1 = r1 * r2
            int r2 = r11.ac
            int r1 = r1 + r2
            r11.ad = r1
            r11.ac = r7
            goto L_0x0708
        L_0x0766:
            r2 = 2100(0x834, float:2.943E-42)
            if (r1 >= r2) goto L_0x0787
            int[] r1 = r11.aq
            r2 = 3
            r3 = r1[r2]
            int r3 = r3 + 1
            r1[r2] = r3
            int[][] r1 = r11.aD
            int r2 = com.kajirin.android.candylib.JpPokerLib.k
            r1 = r1[r2]
            r2 = 2
            r1 = r1[r2]
            int r2 = r11.ac
            int r1 = r1 * r2
            int r2 = r11.ac
            int r1 = r1 + r2
            r11.ad = r1
            r11.ac = r7
            goto L_0x0708
        L_0x0787:
            r2 = 2150(0x866, float:3.013E-42)
            if (r1 >= r2) goto L_0x07a9
            int[] r1 = r11.aq
            r2 = 4
            r3 = r1[r2]
            int r3 = r3 + 1
            r1[r2] = r3
            int[][] r1 = r11.aD
            int r2 = com.kajirin.android.candylib.JpPokerLib.k
            r1 = r1[r2]
            r2 = 3
            r1 = r1[r2]
            int r2 = r11.ac
            int r1 = r1 * r2
            int r2 = r11.ac
            int r1 = r1 + r2
            r11.ad = r1
            r11.ac = r7
            goto L_0x0708
        L_0x07a9:
            r2 = 2150(0x866, float:3.013E-42)
            if (r1 != r2) goto L_0x07cb
            int[] r1 = r11.aq
            r2 = 5
            r3 = r1[r2]
            int r3 = r3 + 1
            r1[r2] = r3
            int[][] r1 = r11.aD
            int r2 = com.kajirin.android.candylib.JpPokerLib.k
            r1 = r1[r2]
            r2 = 4
            r1 = r1[r2]
            int r2 = r11.ac
            int r1 = r1 * r2
            int r2 = r11.ac
            int r1 = r1 + r2
            r11.ad = r1
            r11.ac = r7
            goto L_0x0708
        L_0x07cb:
            r2 = 3500(0xdac, float:4.905E-42)
            if (r1 >= r2) goto L_0x07ec
            int[] r1 = r11.aq
            r2 = r1[r9]
            int r2 = r2 + 1
            r1[r9] = r2
            int[][] r1 = r11.aD
            int r2 = com.kajirin.android.candylib.JpPokerLib.k
            r1 = r1[r2]
            r2 = 5
            r1 = r1[r2]
            int r2 = r11.ac
            int r1 = r1 * r2
            int r2 = r11.ac
            int r1 = r1 + r2
            r11.ad = r1
            r11.ac = r7
            goto L_0x0708
        L_0x07ec:
            r2 = 3600(0xe10, float:5.045E-42)
            if (r1 >= r2) goto L_0x080d
            int[] r1 = r11.aq
            r2 = 7
            r3 = r1[r2]
            int r3 = r3 + 1
            r1[r2] = r3
            int[][] r1 = r11.aD
            int r2 = com.kajirin.android.candylib.JpPokerLib.k
            r1 = r1[r2]
            r1 = r1[r9]
            int r2 = r11.ac
            int r1 = r1 * r2
            int r2 = r11.ac
            int r1 = r1 + r2
            r11.ad = r1
            r11.ac = r7
            goto L_0x0708
        L_0x080d:
            r2 = 3700(0xe74, float:5.185E-42)
            if (r1 >= r2) goto L_0x0830
            int[] r1 = r11.aq
            r2 = 8
            r3 = r1[r2]
            int r3 = r3 + 1
            r1[r2] = r3
            int[][] r1 = r11.aD
            int r2 = com.kajirin.android.candylib.JpPokerLib.k
            r1 = r1[r2]
            r2 = 7
            r1 = r1[r2]
            int r2 = r11.ac
            int r1 = r1 * r2
            int r2 = r11.ac
            int r1 = r1 + r2
            r11.ad = r1
            r11.ac = r7
            goto L_0x0708
        L_0x0830:
            r2 = 3700(0xe74, float:5.185E-42)
            if (r1 != r2) goto L_0x0854
            int[] r1 = r11.aq
            r2 = 9
            r3 = r1[r2]
            int r3 = r3 + 1
            r1[r2] = r3
            int[][] r1 = r11.aD
            int r2 = com.kajirin.android.candylib.JpPokerLib.k
            r1 = r1[r2]
            r2 = 8
            r1 = r1[r2]
            int r2 = r11.ac
            int r1 = r1 * r2
            int r2 = r11.ac
            int r1 = r1 + r2
            r11.ad = r1
            r11.ac = r7
            goto L_0x0708
        L_0x0854:
            r2 = 3800(0xed8, float:5.325E-42)
            if (r1 != r2) goto L_0x0708
            int[] r1 = r11.aq
            r2 = 10
            r3 = r1[r2]
            int r3 = r3 + 1
            r1[r2] = r3
            int r1 = com.kajirin.android.candylib.JpPokerLib.x
            int r2 = r11.ac
            int r1 = r1 + r2
            r11.ad = r1
            r11.ac = r7
            com.kajirin.android.candylib.JpPokerLib.x = r7
            goto L_0x0708
        L_0x086f:
            int r1 = r11.ao
            r11.b(r1)
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 + r3
            r11.V = r1
            c()
            int r1 = r11.ao
            r2 = 11
            if (r1 >= r2) goto L_0x088b
            r1 = 7
            r11.Y = r1
            goto L_0x0563
        L_0x088b:
            int r1 = r11.ao
            r2 = 3800(0xed8, float:5.325E-42)
            if (r1 != r2) goto L_0x0897
            r1 = 20
            r11.Y = r1
            goto L_0x0563
        L_0x0897:
            r1 = 5
            r11.Y = r1
            goto L_0x0563
        L_0x089c:
            r11.Z = r7
            goto L_0x0158
        L_0x08a0:
            int[] r1 = r11.ag
            r2 = 5
            r1[r2] = r8
            int[] r1 = r11.ag
            r1[r9] = r8
            int[] r1 = r11.ag
            r2 = 7
            r1[r2] = r8
            int[] r1 = r11.ag
            r2 = 8
            r1[r2] = r8
            int[] r1 = r11.ag
            r2 = 9
            r1[r2] = r8
            r1 = 4
            r2 = 15
            r11.a(r1, r2, r10, r7)
            r1 = 177(0xb1, float:2.48E-43)
            r11.a(r8, r1, r10, r7)
            r1 = 258(0x102, float:3.62E-43)
            r11.a(r7, r1, r10, r7)
            r1 = 2
            r2 = 339(0x153, float:4.75E-43)
            r11.a(r1, r2, r10, r7)
            r1 = 5
            r2 = 15
            r3 = 339(0x153, float:4.75E-43)
            r11.a(r1, r2, r3, r7)
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 8333(0x208d, double:4.117E-320)
            long r1 = r1 + r3
            r11.V = r1
            r11.Y = r9
            goto L_0x0158
        L_0x08e5:
            int[] r1 = r11.ag
            r2 = 5
            r1[r2] = r7
            int[] r1 = r11.ag
            r2 = 9
            r1[r2] = r7
            r1 = 15
            r11.a(r9, r1, r10, r7)
            r1 = 15
            r2 = 339(0x153, float:4.75E-43)
            r11.a(r9, r1, r2, r7)
            int r1 = com.kajirin.android.candylib.JpPokerLib.d
            int r2 = r11.ad
            int r1 = r1 + r2
            com.kajirin.android.candylib.JpPokerLib.d = r1
            r2 = 999999999(0x3b9ac9ff, float:0.004723787)
            if (r1 <= r2) goto L_0x090d
            r1 = 999999999(0x3b9ac9ff, float:0.004723787)
            com.kajirin.android.candylib.JpPokerLib.d = r1
        L_0x090d:
            r11.ad = r7
            r11.i()
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 173(0xad, float:2.42E-43)
            r3 = 35
            r4 = 246(0xf6, float:3.45E-43)
            r5 = 119(0x77, float:1.67E-43)
            r1.<init>(r2, r3, r4, r5)
            android.graphics.Canvas r2 = r11.aw
            android.graphics.Bitmap r3 = r11.as
            r4 = 0
            r2.drawBitmap(r3, r1, r1, r4)
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 140(0x8c, float:1.96E-43)
            r3 = 3
            r4 = 340(0x154, float:4.76E-43)
            r5 = 36
            r1.<init>(r2, r3, r4, r5)
            android.graphics.Canvas r2 = r11.aw
            android.graphics.Bitmap r3 = r11.as
            r4 = 0
            r2.drawBitmap(r3, r1, r1, r4)
            r1 = 1099956224(0x41900000, float:18.0)
            r0.setTextSize(r1)
            r1 = -65536(0xffffffffffff0000, float:NaN)
            r0.setColor(r1)
            android.graphics.Canvas r1 = r11.aw
            java.lang.String[] r2 = r11.aB
            int r3 = com.kajirin.android.candylib.JpPokerLib.k
            r2 = r2[r3]
            r3 = 1124859904(0x430c0000, float:140.0)
            r4 = 1102053376(0x41b00000, float:22.0)
            r1.drawText(r2, r3, r4, r0)
            r11.Y = r7
            r11.Z = r7
            goto L_0x0158
        L_0x095a:
            int[] r1 = r11.ag
            r1[r9] = r8
            int[] r1 = r11.ag
            r2 = 7
            r1[r2] = r8
            int[] r1 = r11.ag
            r2 = 8
            r1[r2] = r8
            r1 = 177(0xb1, float:2.48E-43)
            r11.a(r8, r1, r10, r7)
            r1 = 258(0x102, float:3.62E-43)
            r11.a(r7, r1, r10, r7)
            r1 = 2
            r2 = 339(0x153, float:4.75E-43)
            r11.a(r1, r2, r10, r7)
            r1 = 15
            r11.a(r9, r1, r10, r7)
            r1 = 15
            r2 = 339(0x153, float:4.75E-43)
            r11.a(r9, r1, r2, r7)
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 83
            long r1 = r1 + r3
            r11.V = r1
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 173(0xad, float:2.42E-43)
            r3 = 35
            r4 = 246(0xf6, float:3.45E-43)
            r5 = 119(0x77, float:1.67E-43)
            r1.<init>(r2, r3, r4, r5)
            android.graphics.Canvas r2 = r11.aw
            android.graphics.Bitmap r3 = r11.as
            r4 = 0
            r2.drawBitmap(r3, r1, r1, r4)
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 140(0x8c, float:1.96E-43)
            r3 = 3
            r4 = 340(0x154, float:4.76E-43)
            r5 = 36
            r1.<init>(r2, r3, r4, r5)
            android.graphics.Canvas r2 = r11.aw
            android.graphics.Bitmap r3 = r11.as
            r4 = 0
            r2.drawBitmap(r3, r1, r1, r4)
            r1 = 1099956224(0x41900000, float:18.0)
            r0.setTextSize(r1)
            r1 = -65536(0xffffffffffff0000, float:NaN)
            r0.setColor(r1)
            android.graphics.Canvas r1 = r11.aw
            java.lang.String[] r2 = r11.aB
            int r3 = com.kajirin.android.candylib.JpPokerLib.k
            r2 = r2[r3]
            r3 = 1124859904(0x430c0000, float:140.0)
            r4 = 1102053376(0x41b00000, float:22.0)
            r1.drawText(r2, r3, r4, r0)
            r11.Y = r7
            r11.Z = r7
            goto L_0x0158
        L_0x09d6:
            int[] r1 = r11.ag
            r1[r9] = r7
            int[] r1 = r11.ag
            r2 = 7
            r1[r2] = r7
            int[] r1 = r11.ag
            r2 = 8
            r1[r2] = r7
            int[] r1 = r11.ag
            r2 = 9
            r1[r2] = r7
            r1 = 177(0xb1, float:2.48E-43)
            r11.a(r9, r1, r10, r7)
            r1 = 258(0x102, float:3.62E-43)
            r11.a(r9, r1, r10, r7)
            r1 = 339(0x153, float:4.75E-43)
            r11.a(r9, r1, r10, r7)
            r1 = 15
            r2 = 339(0x153, float:4.75E-43)
            r11.a(r9, r1, r2, r7)
            int r1 = r11.ad
            r11.ac = r1
            r11.ad = r7
            r11.i()
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 83
            long r1 = r1 + r3
            r11.V = r1
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 5
            r3 = 115(0x73, float:1.61E-43)
            r4 = 412(0x19c, float:5.77E-43)
            r5 = 144(0x90, float:2.02E-43)
            r1.<init>(r2, r3, r4, r5)
            android.graphics.Canvas r2 = r11.aw
            android.graphics.Bitmap r3 = r11.as
            r4 = 0
            r2.drawBitmap(r3, r1, r1, r4)
            r11.Z = r7
            r1 = 10
            r11.Y = r1
            goto L_0x0158
        L_0x0a2f:
            int[] r1 = r11.ag
            r2 = 5
            r1[r2] = r7
            int[] r1 = r11.ag
            r1[r9] = r7
            int[] r1 = r11.ag
            r2 = 7
            r1[r2] = r7
            int[] r1 = r11.ag
            r2 = 8
            r1[r2] = r7
            r1 = 15
            r11.a(r9, r1, r10, r7)
            r1 = 177(0xb1, float:2.48E-43)
            r11.a(r9, r1, r10, r7)
            r1 = 258(0x102, float:3.62E-43)
            r11.a(r9, r1, r10, r7)
            r1 = 339(0x153, float:4.75E-43)
            r11.a(r9, r1, r10, r7)
            int r1 = r11.ad
            int r1 = r1 / 2
            r11.ac = r1
            int r1 = com.kajirin.android.candylib.JpPokerLib.d
            int r2 = r11.ad
            int r3 = r11.ac
            int r2 = r2 - r3
            int r1 = r1 + r2
            com.kajirin.android.candylib.JpPokerLib.d = r1
            r2 = 999999999(0x3b9ac9ff, float:0.004723787)
            if (r1 <= r2) goto L_0x0a71
            r1 = 999999999(0x3b9ac9ff, float:0.004723787)
            com.kajirin.android.candylib.JpPokerLib.d = r1
        L_0x0a71:
            r11.ad = r7
            r11.i()
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 83
            long r1 = r1 + r3
            r11.V = r1
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 5
            r3 = 115(0x73, float:1.61E-43)
            r4 = 412(0x19c, float:5.77E-43)
            r5 = 144(0x90, float:2.02E-43)
            r1.<init>(r2, r3, r4, r5)
            android.graphics.Canvas r2 = r11.aw
            android.graphics.Bitmap r3 = r11.as
            r4 = 0
            r2.drawBitmap(r3, r1, r1, r4)
            r11.Z = r7
            r1 = 10
            r11.Y = r1
            goto L_0x0158
        L_0x0a9b:
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 83
            long r1 = r1 + r3
            r11.V = r1
            int r1 = r11.Z
            switch(r1) {
                case 0: goto L_0x0ab7;
                case 1: goto L_0x0b1c;
                case 2: goto L_0x0b27;
                case 3: goto L_0x0b33;
                case 4: goto L_0x0b3f;
                case 5: goto L_0x0b4b;
                case 6: goto L_0x0b57;
                case 7: goto L_0x0b6e;
                default: goto L_0x0aa9;
            }
        L_0x0aa9:
            int r1 = r11.Y
            r2 = 10
            if (r1 != r2) goto L_0x0bcd
            int r1 = r11.Z
            int r1 = r1 + 1
            r11.Z = r1
            goto L_0x0158
        L_0x0ab7:
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 173(0xad, float:2.42E-43)
            r3 = 35
            r4 = 246(0xf6, float:3.45E-43)
            r5 = 119(0x77, float:1.67E-43)
            r1.<init>(r2, r3, r4, r5)
            android.graphics.Canvas r2 = r11.aw
            android.graphics.Bitmap r3 = r11.as
            r4 = 0
            r2.drawBitmap(r3, r1, r1, r4)
            r1 = 11
            r11.c(r8, r1, r6)
            r1 = 92
            r11.c(r8, r1, r6)
            r1 = 173(0xad, float:2.42E-43)
            r11.c(r8, r1, r6)
            r1 = 254(0xfe, float:3.56E-43)
            r11.c(r8, r1, r6)
            r1 = 335(0x14f, float:4.7E-43)
            r11.c(r8, r1, r6)
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 140(0x8c, float:1.96E-43)
            r3 = 3
            r4 = 340(0x154, float:4.76E-43)
            r5 = 36
            r1.<init>(r2, r3, r4, r5)
            android.graphics.Canvas r2 = r11.aw
            android.graphics.Bitmap r3 = r11.as
            r4 = 0
            r2.drawBitmap(r3, r1, r1, r4)
            r1 = 1099956224(0x41900000, float:18.0)
            r0.setTextSize(r1)
            r1 = -65536(0xffffffffffff0000, float:NaN)
            r0.setColor(r1)
            android.graphics.Canvas r1 = r11.aw
            java.lang.String[] r2 = r11.aB
            int r3 = com.kajirin.android.candylib.JpPokerLib.k
            r2 = r2[r3]
            r3 = 1124859904(0x430c0000, float:140.0)
            r4 = 1102053376(0x41b00000, float:22.0)
            r1.drawText(r2, r3, r4, r0)
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 + r3
            r11.V = r1
            goto L_0x0aa9
        L_0x0b1c:
            a(r8)
            r1 = 55
            r2 = 11
            r11.b(r1, r2, r6)
            goto L_0x0aa9
        L_0x0b27:
            a(r8)
            r1 = 55
            r2 = 92
            r11.b(r1, r2, r6)
            goto L_0x0aa9
        L_0x0b33:
            a(r8)
            r1 = 55
            r2 = 173(0xad, float:2.42E-43)
            r11.b(r1, r2, r6)
            goto L_0x0aa9
        L_0x0b3f:
            a(r8)
            r1 = 55
            r2 = 254(0xfe, float:3.56E-43)
            r11.b(r1, r2, r6)
            goto L_0x0aa9
        L_0x0b4b:
            a(r8)
            r1 = 55
            r2 = 335(0x14f, float:4.7E-43)
            r11.b(r1, r2, r6)
            goto L_0x0aa9
        L_0x0b57:
            a(r8)
            r1 = 55
            r2 = 173(0xad, float:2.42E-43)
            r3 = 15
            r11.b(r1, r2, r3)
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 + r3
            r11.V = r1
            goto L_0x0aa9
        L_0x0b6e:
            r11.e()
            r11.f()
            r11.g()
            r11.h()
            int[] r1 = r11.ak
            int r2 = r11.al
            int r3 = r2 + 1
            r11.al = r3
            r1 = r1[r2]
            r11.ap = r1
            int r1 = r11.ap
            r2 = 173(0xad, float:2.42E-43)
            r3 = 15
            r11.b(r1, r2, r3)
            int r1 = r11.ap
            int r1 = r1 % 13
            r2 = 12
            if (r1 != r2) goto L_0x0ba8
            r1 = r7
        L_0x0b98:
            r2 = 5
            if (r1 < r2) goto L_0x0ba1
            r1 = 12
            r11.Y = r1
            goto L_0x0aa9
        L_0x0ba1:
            int[] r2 = r11.ah
            r2[r1] = r7
            int r1 = r1 + 1
            goto L_0x0b98
        L_0x0ba8:
            r1 = r7
        L_0x0ba9:
            r2 = 5
            if (r1 < r2) goto L_0x0bb2
            r1 = 11
            r11.Y = r1
            goto L_0x0aa9
        L_0x0bb2:
            int[] r2 = r11.ah
            r2[r1] = r7
            int[] r2 = r11.ag
            r2[r1] = r8
            r2 = 3
            android.graphics.Rect[] r3 = r11.ai
            r3 = r3[r1]
            int r3 = r3.left
            android.graphics.Rect[] r4 = r11.ai
            r4 = r4[r1]
            int r4 = r4.top
            r11.a(r2, r3, r4, r7)
            int r1 = r1 + 1
            goto L_0x0ba9
        L_0x0bcd:
            int r1 = r11.Y
            r2 = 12
            if (r1 != r2) goto L_0x0bd8
            r1 = 7
            r11.Z = r1
            goto L_0x0158
        L_0x0bd8:
            r11.Z = r7
            goto L_0x0158
        L_0x0bdc:
            int r1 = r11.S
            r2 = 5
            if (r1 != r2) goto L_0x0158
            r1 = 4
            int r1 = b(r7, r1)
            int[] r2 = r11.ah
            r2[r1] = r8
            r2 = 3
            android.graphics.Rect[] r3 = r11.ai
            r3 = r3[r1]
            int r3 = r3.left
            android.graphics.Rect[] r4 = r11.ai
            r1 = r4[r1]
            int r1 = r1.top
            r11.a(r2, r3, r1, r8)
            r1 = 12
            r11.Y = r1
            goto L_0x0158
        L_0x0c00:
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 83
            long r1 = r1 + r3
            r11.V = r1
            int r1 = r11.Z
            switch(r1) {
                case 0: goto L_0x0c1c;
                case 1: goto L_0x0c26;
                case 2: goto L_0x0c40;
                case 3: goto L_0x0c5a;
                case 4: goto L_0x0c76;
                case 5: goto L_0x0c93;
                case 6: goto L_0x0cb1;
                case 7: goto L_0x0cc9;
                case 8: goto L_0x0d2e;
                default: goto L_0x0c0e;
            }
        L_0x0c0e:
            int r1 = r11.Y
            r2 = 12
            if (r1 != r2) goto L_0x0d66
            int r1 = r11.Z
            int r1 = r1 + 1
            r11.Z = r1
            goto L_0x0158
        L_0x0c1c:
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 + r3
            r11.V = r1
            goto L_0x0c0e
        L_0x0c26:
            int[] r1 = r11.ah
            r1 = r1[r7]
            if (r1 != 0) goto L_0x0c36
            int[] r1 = r11.am
            r1 = r1[r7]
            r2 = 11
            r11.b(r1, r2, r6)
            goto L_0x0c0e
        L_0x0c36:
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 50
            long r1 = r1 + r3
            r11.V = r1
            goto L_0x0c0e
        L_0x0c40:
            int[] r1 = r11.ah
            r1 = r1[r8]
            if (r1 != 0) goto L_0x0c50
            int[] r1 = r11.am
            r1 = r1[r8]
            r2 = 92
            r11.b(r1, r2, r6)
            goto L_0x0c0e
        L_0x0c50:
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 50
            long r1 = r1 + r3
            r11.V = r1
            goto L_0x0c0e
        L_0x0c5a:
            int[] r1 = r11.ah
            r2 = 2
            r1 = r1[r2]
            if (r1 != 0) goto L_0x0c6c
            int[] r1 = r11.am
            r2 = 2
            r1 = r1[r2]
            r2 = 173(0xad, float:2.42E-43)
            r11.b(r1, r2, r6)
            goto L_0x0c0e
        L_0x0c6c:
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 50
            long r1 = r1 + r3
            r11.V = r1
            goto L_0x0c0e
        L_0x0c76:
            int[] r1 = r11.ah
            r2 = 3
            r1 = r1[r2]
            if (r1 != 0) goto L_0x0c88
            int[] r1 = r11.am
            r2 = 3
            r1 = r1[r2]
            r2 = 254(0xfe, float:3.56E-43)
            r11.b(r1, r2, r6)
            goto L_0x0c0e
        L_0x0c88:
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 50
            long r1 = r1 + r3
            r11.V = r1
            goto L_0x0c0e
        L_0x0c93:
            int[] r1 = r11.ah
            r2 = 4
            r1 = r1[r2]
            if (r1 != 0) goto L_0x0ca6
            int[] r1 = r11.am
            r2 = 4
            r1 = r1[r2]
            r2 = 335(0x14f, float:4.7E-43)
            r11.b(r1, r2, r6)
            goto L_0x0c0e
        L_0x0ca6:
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 50
            long r1 = r1 + r3
            r11.V = r1
            goto L_0x0c0e
        L_0x0cb1:
            r1 = r7
        L_0x0cb2:
            r2 = 5
            if (r1 >= r2) goto L_0x0c0e
            int[] r2 = r11.ah
            r2 = r2[r1]
            if (r2 != r8) goto L_0x0cc6
            int[] r2 = r11.am
            r2 = r2[r1]
            int r3 = r1 * 81
            int r3 = r3 + 11
            r11.b(r2, r3, r6)
        L_0x0cc6:
            int r1 = r1 + 1
            goto L_0x0cb2
        L_0x0cc9:
            int r1 = r11.ap
            int r1 = r1 % 13
            r2 = r7
            r3 = r7
        L_0x0ccf:
            r4 = 5
            if (r3 < r4) goto L_0x0d0a
            r3 = 1103101952(0x41c00000, float:24.0)
            r0.setTextSize(r3)
            if (r1 >= r2) goto L_0x0d19
            int r1 = r11.ac
            int r1 = r1 * 2
            r11.ad = r1
            r11.ac = r7
            r1 = -16776961(0xffffffffff0000ff, float:-1.7014636E38)
            r0.setColor(r1)
            android.graphics.Canvas r1 = r11.aw
            java.lang.String r2 = "WIN"
            r3 = 1117782016(0x42a00000, float:80.0)
            r4 = 1124859904(0x430c0000, float:140.0)
            r1.drawText(r2, r3, r4, r0)
        L_0x0cf2:
            r1 = 1094713344(0x41400000, float:12.0)
            r0.setTextSize(r1)
            r1 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0.setColor(r1)
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 + r3
            r11.V = r1
            c()
            goto L_0x0c0e
        L_0x0d0a:
            int[] r4 = r11.ah
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0d16
            int[] r2 = r11.am
            r2 = r2[r3]
            int r2 = r2 % 13
        L_0x0d16:
            int r3 = r3 + 1
            goto L_0x0ccf
        L_0x0d19:
            r11.ad = r7
            r11.ac = r7
            r1 = -65536(0xffffffffffff0000, float:NaN)
            r0.setColor(r1)
            android.graphics.Canvas r1 = r11.aw
            java.lang.String r2 = "LOSE"
            r3 = 1117782016(0x42a00000, float:80.0)
            r4 = 1124859904(0x430c0000, float:140.0)
            r1.drawText(r2, r3, r4, r0)
            goto L_0x0cf2
        L_0x0d2e:
            r11.i()
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 + r3
            r11.V = r1
            r1 = r7
        L_0x0d3b:
            r2 = 5
            if (r1 < r2) goto L_0x0d47
            int r1 = r11.ad
            if (r1 != 0) goto L_0x0d61
            r1 = 7
            r11.Y = r1
            goto L_0x0c0e
        L_0x0d47:
            int[] r2 = r11.ah
            r2[r1] = r7
            int[] r2 = r11.ag
            r2[r1] = r7
            android.graphics.Rect[] r2 = r11.ai
            r2 = r2[r1]
            int r2 = r2.left
            android.graphics.Rect[] r3 = r11.ai
            r3 = r3[r1]
            int r3 = r3.top
            r11.a(r9, r2, r3, r7)
            int r1 = r1 + 1
            goto L_0x0d3b
        L_0x0d61:
            r1 = 5
            r11.Y = r1
            goto L_0x0c0e
        L_0x0d66:
            r11.Z = r7
            goto L_0x0158
        L_0x0d6a:
            r1 = 2
            a(r1)
            int[] r1 = r11.ag
            r1[r9] = r8
            int[] r1 = r11.ag
            r2 = 7
            r1[r2] = r8
            int[] r1 = r11.ag
            r2 = 8
            r1[r2] = r8
            r1 = 177(0xb1, float:2.48E-43)
            r11.a(r8, r1, r10, r7)
            r1 = 258(0x102, float:3.62E-43)
            r11.a(r7, r1, r10, r7)
            r1 = 2
            r2 = 339(0x153, float:4.75E-43)
            r11.a(r1, r2, r10, r7)
            int r1 = com.kajirin.android.candylib.JpPokerLib.d
            int r2 = r11.ad
            int r1 = r1 + r2
            com.kajirin.android.candylib.JpPokerLib.d = r1
            r2 = 999999999(0x3b9ac9ff, float:0.004723787)
            if (r1 <= r2) goto L_0x0d9e
            r1 = 999999999(0x3b9ac9ff, float:0.004723787)
            com.kajirin.android.candylib.JpPokerLib.d = r1
        L_0x0d9e:
            r11.ad = r7
            r11.i()
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 5000(0x1388, double:2.4703E-320)
            long r1 = r1 + r3
            r11.V = r1
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 173(0xad, float:2.42E-43)
            r3 = 35
            r4 = 246(0xf6, float:3.45E-43)
            r5 = 119(0x77, float:1.67E-43)
            r1.<init>(r2, r3, r4, r5)
            android.graphics.Canvas r2 = r11.aw
            android.graphics.Bitmap r3 = r11.as
            r4 = 0
            r2.drawBitmap(r3, r1, r1, r4)
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 140(0x8c, float:1.96E-43)
            r3 = 3
            r4 = 340(0x154, float:4.76E-43)
            r5 = 36
            r1.<init>(r2, r3, r4, r5)
            android.graphics.Canvas r2 = r11.aw
            android.graphics.Bitmap r3 = r11.as
            r4 = 0
            r2.drawBitmap(r3, r1, r1, r4)
            r1 = 1099956224(0x41900000, float:18.0)
            r0.setTextSize(r1)
            r1 = -65536(0xffffffffffff0000, float:NaN)
            r0.setColor(r1)
            android.graphics.Canvas r1 = r11.aw
            java.lang.String[] r2 = r11.aB
            int r3 = com.kajirin.android.candylib.JpPokerLib.k
            r2 = r2[r3]
            r3 = 1124859904(0x430c0000, float:140.0)
            r4 = 1102053376(0x41b00000, float:22.0)
            r1.drawText(r2, r3, r4, r0)
            r11.Y = r7
            r11.Z = r7
            goto L_0x0158
        L_0x0df4:
            r1 = 10
            java.lang.Thread.sleep(r1)     // Catch:{ Exception -> 0x0dfb }
            goto L_0x015a
        L_0x0dfb:
            r1 = move-exception
            goto L_0x015a
        L_0x0dfe:
            android.graphics.Rect r2 = new android.graphics.Rect
            int r3 = r11.r
            int r4 = r11.q
            int r3 = r3 - r4
            int r4 = r11.s
            int r5 = r11.r
            int r6 = r11.q
            int r5 = r5 - r6
            int r5 = r5 + 419
            int r6 = r11.s
            int r6 = r6 + 380
            r2.<init>(r3, r4, r5, r6)
            goto L_0x025b
        L_0x0e17:
            r1 = 1120403456(0x42c80000, float:100.0)
            float r2 = r11.d
            float r1 = r1 * r2
            int r1 = (int) r1
            int r1 = r1 * 380
            int r1 = r1 / 100
            r2 = 1120403456(0x42c80000, float:100.0)
            float r3 = r11.d
            float r2 = r2 * r3
            int r2 = (int) r2
            int r2 = r2 * 419
            int r2 = r2 / 100
            android.graphics.Bitmap r3 = r11.ar
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createScaledBitmap(r3, r2, r1, r8)
            int r2 = com.kajirin.android.candylib.JpPokerLib.s
            if (r2 != 0) goto L_0x0e41
            int r2 = r11.r
            float r2 = (float) r2
            int r3 = r11.s
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            goto L_0x0261
        L_0x0e41:
            int r2 = r11.r
            int r3 = r11.q
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r11.s
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            goto L_0x0261
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kajirin.android.candylib.b.b():void");
    }

    private void b(int i2) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize(24.0f);
        if (i2 < 11) {
            paint.setColor(-65536);
            this.aw.drawText("LOSE", 30.0f, 140.0f, paint);
        } else {
            paint.setColor(-16776961);
            this.aw.drawText("WIN", 30.0f, 140.0f, paint);
        }
        paint.setTextSize(18.0f);
        paint.setColor(-256);
        if (i2 > 10) {
            if (i2 < 15) {
                this.aw.drawText(this.aC[1], 100.0f, 140.0f, paint);
            } else if (i2 < 1500) {
                this.aw.drawText(this.aC[2], 100.0f, 140.0f, paint);
            } else if (i2 < 2100) {
                this.aw.drawText(this.aC[3], 100.0f, 140.0f, paint);
            } else if (i2 < 2150) {
                this.aw.drawText(this.aC[4], 100.0f, 140.0f, paint);
            } else if (i2 == 2150) {
                this.aw.drawText(this.aC[5], 100.0f, 140.0f, paint);
            } else if (i2 < 3500) {
                this.aw.drawText(this.aC[6], 100.0f, 140.0f, paint);
            } else if (i2 < 3600) {
                this.aw.drawText(this.aC[7], 100.0f, 140.0f, paint);
            } else if (i2 < 3700) {
                this.aw.drawText(this.aC[8], 100.0f, 140.0f, paint);
            } else if (i2 == 3700) {
                this.aw.drawText(this.aC[9], 100.0f, 140.0f, paint);
            } else if (i2 == 3800) {
                this.aw.drawText(this.aC[10], 100.0f, 140.0f, paint);
            }
        }
        paint.setTextSize(12.0f);
        paint.setColor(-16777216);
        this.X = 1;
    }

    private void b(int i2, int i3, int i4) {
        int i5;
        int i6;
        if (i2 < 52) {
            i5 = ((i2 / 13) * 2) + ((i2 % 13) / 7);
            i6 = (i2 % 13) % 7;
        } else {
            i5 = ((i2 - 52) * 2) + 1;
            i6 = 6;
        }
        this.aw.drawBitmap(this.au, new Rect((i6 * 72) + 0, (i5 * 103) + 0, (i6 * 72) + 73, (i5 * 103) + 104), new Rect(i3 + 0, i4 + 0, i3 + 73, i4 + 104), (Paint) null);
        this.X = 1;
    }

    private static void b(int[] iArr) {
        int i2 = 0;
        do {
            if (iArr[i2] % 13 <= iArr[i2 + 1] % 13 || iArr[i2 + 1] == 99) {
                i2++;
            } else {
                int i3 = iArr[i2];
                iArr[i2] = iArr[i2 + 1];
                iArr[i2 + 1] = i3;
                i2 = 0;
            }
        } while (i2 < 4);
    }

    private static void c() {
        do {
        } while (System.currentTimeMillis() + 1000 > System.currentTimeMillis());
    }

    private void c(int i2) {
        if (this.Y == 3 || this.Y == 11) {
            this.ah[i2] = (this.ah[i2] + 1) % 2;
            if (this.ah[i2] == 1) {
                a(3, this.ai[i2].left, this.ai[i2].top, 1);
            } else {
                a(3, this.ai[i2].left, this.ai[i2].top, 0);
            }
        }
        if (this.Y == 11) {
            this.Y = 12;
        }
    }

    private void c(int i2, int i3) {
        int[] iArr = new int[6];
        int[] iArr2 = new int[6];
        int i4 = i2;
        for (int i5 = 0; i5 < 6; i5++) {
            iArr[5 - i5] = i4 % 10;
            i4 /= 10;
        }
        boolean z2 = false;
        for (int i6 = 0; i6 < 6; i6++) {
            if (iArr[i6] != 0) {
                iArr2[i6] = 1;
                z2 = true;
            } else if (!z2) {
                iArr2[i6] = 0;
            } else {
                iArr2[i6] = 1;
            }
        }
        for (int i7 = 0; i7 < 6; i7++) {
            this.aw.drawBitmap(this.av, iArr2[i7] == 1 ? new Rect((iArr[i7] * 5) + 260, 224, (iArr[i7] * 5) + 265, 232) : new Rect(310, 224, 315, 232), new Rect((i7 * 5) + 8, i3, (i7 * 5) + 13, i3 + 8), (Paint) null);
        }
        this.X = 1;
    }

    private void c(int i2, int i3, int i4) {
        Rect rect;
        Rect rect2;
        if (i2 == 2) {
            Rect rect3 = new Rect((i2 * 73) + 0, 20, (i2 * 73) + 73, 104);
            rect = rect3;
            rect2 = new Rect(i3 + 0, i4 + 0 + 20, i3 + 73, i4 + 104);
        } else {
            Rect rect4 = new Rect((i2 * 73) + 0, 0, (i2 * 73) + 73, 104);
            rect = rect4;
            rect2 = new Rect(i3 + 0, i4 + 0, i3 + 73, i4 + 104);
        }
        this.aw.drawBitmap(this.av, rect, rect2, (Paint) null);
        this.X = 1;
    }

    private void d() {
        for (int i2 = 0; i2 < 52; i2++) {
            this.ak[i2] = i2;
        }
        for (int i3 = 0; i3 < 11; i3++) {
            this.aq[i3] = 0;
        }
    }

    private void d(int i2) {
        JpPokerLib.d = 100;
        if (i2 == 1) {
            a("メダルが無いので融資します。");
        }
    }

    private void d(int i2, int i3, int i4) {
        int[] iArr = new int[9];
        int[] iArr2 = new int[9];
        int i5 = i2;
        for (int i6 = 0; i6 < 9; i6++) {
            iArr[8 - i6] = i5 % 10;
            i5 /= 10;
        }
        boolean z2 = false;
        for (int i7 = 0; i7 < 8; i7++) {
            if (iArr[i7] != 0) {
                iArr2[i7] = 1;
                z2 = true;
            } else if (!z2) {
                iArr2[i7] = 0;
            } else {
                iArr2[i7] = 1;
            }
        }
        iArr2[8] = 1;
        for (int i8 = 0; i8 < 9; i8++) {
            this.aw.drawBitmap(this.av, iArr2[i8] == 1 ? new Rect((iArr[i8] * 5) + 260, 224, (iArr[i8] * 5) + 265, 232) : new Rect(310, 224, 315, 232), new Rect((i8 * 5) + i3, i4, i3 + 5 + (i8 * 5), i4 + 8), (Paint) null);
        }
        this.X = 1;
    }

    private void e() {
        this.al = 0;
        for (int i2 = 0; i2 < 5; i2++) {
            this.am[i2] = 99;
        }
        for (int i3 = 0; i3 < 5; i3++) {
            this.an[i3] = 99;
        }
    }

    private void f() {
        int b2 = b(0, 100) + 321;
        for (int i2 = 0; i2 < b2; i2++) {
            int b3 = b(0, 51);
            int b4 = b(0, 51);
            int i3 = this.ak[b3];
            this.ak[b3] = this.ak[b4];
            this.ak[b4] = i3;
        }
    }

    private void g() {
        int[] iArr = new int[52];
        for (int i2 = 0; i2 < 52; i2++) {
            iArr[i2] = this.ak[i2];
        }
        int b2 = b(10, 40);
        int i3 = 0;
        int i4 = b2;
        while (i4 < 52) {
            this.ak[i3] = iArr[i4];
            i4++;
            i3++;
        }
        int i5 = 0;
        while (i5 < b2) {
            this.ak[i3] = iArr[i5];
            i5++;
            i3++;
        }
    }

    private void h() {
        for (int i2 = 0; i2 < 5; i2++) {
            if (this.am[i2] == 99) {
                int[] iArr = this.am;
                int[] iArr2 = this.ak;
                int i3 = this.al;
                this.al = i3 + 1;
                iArr[i2] = iArr2[i3];
            }
        }
    }

    private void i() {
        int[] iArr = new int[9];
        int[] iArr2 = new int[9];
        int i2 = JpPokerLib.d;
        for (int i3 = 0; i3 < 9; i3++) {
            iArr[8 - i3] = i2 % 10;
            i2 /= 10;
        }
        boolean z2 = false;
        for (int i4 = 0; i4 < 8; i4++) {
            if (iArr[i4] != 0) {
                iArr2[i4] = 1;
                z2 = true;
            } else if (!z2) {
                iArr2[i4] = 0;
            } else {
                iArr2[i4] = 1;
            }
        }
        iArr2[8] = 1;
        for (int i5 = 0; i5 < 9; i5++) {
            this.aw.drawBitmap(this.av, iArr2[i5] == 1 ? new Rect((iArr[i5] * 13) + 195, 152, (iArr[i5] * 13) + 208, 170) : new Rect(325, 152, 338, 170), new Rect((i5 * 13) + 285, 343, (i5 * 13) + 298, 361), (Paint) null);
        }
        this.X = 1;
        d(JpPokerLib.d, 360, 19);
        d(this.ac, 360, 29);
        d(this.ad, 360, 39);
        d(this.ae, 360, 49);
        d(JpPokerLib.x, 360, 59);
        d(JpPokerLib.b, 360, 69);
    }

    private void j() {
        if (this.T == 1) {
            d(this.aq[10], 121, 9);
            d(this.aq[9], 121, 19);
            d(this.aq[8], 121, 29);
            d(this.aq[7], 121, 39);
            d(this.aq[6], 121, 49);
            d(this.aq[5], 121, 59);
            d(this.aq[4], 121, 69);
            d(this.aq[3], 121, 79);
            d(this.aq[2], 121, 89);
            d(this.aq[1], 121, 99);
            d(this.aq[0], 121, 109);
        }
        c(this.aD[JpPokerLib.k][8] * this.ac, 19);
        c(this.aD[JpPokerLib.k][7] * this.ac, 29);
        c(this.aD[JpPokerLib.k][6] * this.ac, 39);
        c(this.aD[JpPokerLib.k][5] * this.ac, 49);
        c(this.aD[JpPokerLib.k][4] * this.ac, 59);
        c(this.aD[JpPokerLib.k][3] * this.ac, 69);
        c(this.aD[JpPokerLib.k][2] * this.ac, 79);
        c(this.aD[JpPokerLib.k][1] * this.ac, 89);
        c(this.aD[JpPokerLib.k][0] * this.ac, 99);
    }

    private void k() {
        a(2, 339, 304, 1);
        if (this.ac != 0) {
            this.af = this.ac;
            i();
            if (this.ac == this.ae && this.ae > 5) {
                c(2, 173, 15);
            }
            this.Y = 1;
        } else if (this.af == 0) {
            a("ＢＥＴしてください。");
            a(2, 339, 304, 0);
        } else if (JpPokerLib.d > 0) {
            if (this.af > JpPokerLib.d) {
                this.ac = JpPokerLib.d;
                this.af = JpPokerLib.d;
                JpPokerLib.d = 0;
                i();
                j();
                this.Y = 1;
            } else {
                this.ac = this.af;
                JpPokerLib.d -= this.af;
                i();
                j();
                this.Y = 1;
            }
            if (this.ac == this.ae && this.ae > 5) {
                c(2, 173, 15);
            }
        } else {
            d(1);
            a(2, 339, 304, 0);
            i();
        }
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        return super.onKeyDown(i2, keyEvent);
    }

    public final boolean onKeyUp(int i2, KeyEvent keyEvent) {
        return super.onKeyUp(i2, keyEvent);
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        this.A = (int) motionEvent.getX();
        this.B = (int) motionEvent.getY();
        this.z = motionEvent.getAction();
        return true;
    }

    public final void run() {
        while (this.h != null && !JpPokerLib.h) {
            if (this.f == 0) {
                this.i = 1;
                this.z = -999;
                this.S = 0;
                this.T = 0;
                this.n = 0;
                this.o = 0;
                if (this.ar == null) {
                    this.ar = Bitmap.createBitmap(419, 380, Bitmap.Config.RGB_565);
                    this.aw = new Canvas(this.ar);
                    this.aw.setBitmap(this.ar);
                    this.aw.drawColor(-1);
                }
                JpPokerLib.i.incrementProgressBy(10);
                if (this.at == null) {
                    this.at = Bitmap.createBitmap(419, 380, Bitmap.Config.RGB_565);
                    this.ax = new Canvas(this.at);
                    this.ax.setBitmap(this.at);
                    this.ax.drawColor(-1);
                }
                JpPokerLib.i.incrementProgressBy(10);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inScaled = false;
                Resources resources = getResources();
                this.j[0] = BitmapFactory.decodeResource(resources, R.drawable.up, options);
                JpPokerLib.i.incrementProgressBy(3);
                this.j[1] = BitmapFactory.decodeResource(resources, R.drawable.down, options);
                JpPokerLib.i.incrementProgressBy(3);
                this.j[2] = BitmapFactory.decodeResource(resources, R.drawable.left, options);
                JpPokerLib.i.incrementProgressBy(3);
                this.j[3] = BitmapFactory.decodeResource(resources, R.drawable.right, options);
                JpPokerLib.i.incrementProgressBy(3);
                this.j[4] = BitmapFactory.decodeResource(resources, R.drawable.dot, options);
                JpPokerLib.i.incrementProgressBy(3);
                this.j[5] = BitmapFactory.decodeResource(resources, R.drawable.maru, options);
                JpPokerLib.i.incrementProgressBy(3);
                this.j[6] = BitmapFactory.decodeResource(resources, R.drawable.batsu, options);
                JpPokerLib.i.incrementProgressBy(3);
                this.j[7] = BitmapFactory.decodeResource(resources, R.drawable.sankaku, options);
                JpPokerLib.i.incrementProgressBy(3);
                this.j[8] = BitmapFactory.decodeResource(resources, R.drawable.sikaku, options);
                JpPokerLib.i.incrementProgressBy(3);
                for (int i2 = 0; i2 < 9; i2++) {
                    this.k[i2] = 0;
                    this.l[i2] = 0;
                    this.m[i2] = 0;
                }
                this.n = 0;
                this.o = 0;
                System.gc();
                a();
                this.f = 1;
                this.c = new p(this);
                this.b.postDelayed(this.c, 5000);
                this.i = 0;
            } else {
                a(this.z, this.A, this.B);
                getContext();
                if (!JpPokerLib.g) {
                    this.i = 1;
                    b();
                    this.i = 0;
                }
            }
        }
    }

    public final void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        this.C = i3;
        this.D = i4;
        this.K = i4;
        this.L = i3;
        this.p = this.C;
        this.q = this.D;
        this.E = this.e;
        float f2 = ((float) this.q) / 384.0f;
        float f3 = 1257.0f / ((float) this.p);
        if (f2 < f3) {
            this.E = f2;
        } else {
            this.E = f3;
        }
        this.F = (this.p - ((((int) (this.E * 100.0f)) * 419) / 100)) / 2;
        this.G = (this.q - ((((int) (this.E * 100.0f)) * 380) / 100)) / 2;
        if (this.F >= 0) {
            this.H = this.F;
            this.I = this.F;
            this.J = this.F;
        } else {
            this.H = this.F;
            this.I = 0;
            this.J = this.p - ((((int) (this.E * 100.0f)) * 419) / 100);
        }
        this.p = this.K;
        this.q = this.L;
        this.M = ((float) this.p) / 424.0f;
        float f4 = ((float) this.q) / 384.0f;
        if (this.M > f4) {
            this.M = f4;
        }
        if (this.M > 3.0f) {
            this.M = 3.0f;
        } else if (this.M < 0.4f) {
            this.M = 0.4f;
        }
        this.N = (this.p - ((((int) (this.M * 100.0f)) * 419) / 100)) / 2;
        this.O = (this.q - ((((int) (this.M * 100.0f)) * 380) / 100)) / 2;
        if (this.N >= 0) {
            this.P = this.N;
            this.Q = this.N;
            this.R = this.N;
        } else {
            this.P = this.N;
            this.Q = 0;
            this.R = this.p - ((((int) (this.M * 100.0f)) * 419) / 100);
        }
        if (JpPokerLib.s == 0) {
            this.p = this.C;
            this.q = this.D;
            this.d = this.E;
            this.r = this.F;
            this.s = this.G;
            this.t = this.H;
            this.u = this.I;
            this.v = this.J;
        } else {
            this.p = this.K;
            this.q = this.L;
            this.d = this.M;
            this.r = this.N;
            this.s = this.O;
            this.t = this.P;
            this.u = this.Q;
            this.v = this.R;
        }
        this.w = 0;
        this.x = 139;
        this.y = 419 - this.x;
        this.n = 0;
        this.o = 0;
        setFocusable(true);
    }

    public final void surfaceCreated(SurfaceHolder surfaceHolder) {
        JpPokerLib.h = false;
        this.h = new Thread(this);
        this.h.start();
        setFocusable(true);
    }

    public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        JpPokerLib.g = true;
        JpPokerLib.h = true;
        try {
            this.h.join();
        } catch (Exception e2) {
        }
        if (this.ad > 0) {
            JpPokerLib.d += this.ad;
            this.ad = 0;
        }
        if (this.Y == 0 && this.ac > 0) {
            JpPokerLib.d += this.ac;
            this.ac = 0;
        }
        if (JpPokerLib.d > 999999999) {
            JpPokerLib.d = 999999999;
        }
        if (JpPokerLib.x > 999999999) {
            JpPokerLib.x = 999999999;
        }
        if (JpPokerLib.b > 999999999) {
            JpPokerLib.b -= 999999999;
        }
        JpPokerLib.y = JpPokerLib.b(JpPokerLib.x);
        JpPokerLib.c = JpPokerLib.b(JpPokerLib.b);
        if (JpPokerLib.k <= 0 || JpPokerLib.k > 5) {
            JpPokerLib.k = 1;
        }
        this.h = null;
    }
}
