package com.kajirin.android.candylib;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import com.android.vending.licensing.l;
import com.android.vending.licensing.o;
import com.android.vending.licensing.p;
import com.android.vending.licensing.s;
import com.google.ads.AdView;
import com.google.ads.f;
import com.kajirin.android.jppoker_f.R;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;
import jp.co.nobot.libAdMaker.e;
import jp.co.nobot.libAdMaker.libAdMaker;

public class JpPokerLib extends Activity implements SensorEventListener, e {
    private static final String D = System.getProperty("line.separator");
    private static int E = 0;
    private static int F = -1;
    private static int G = 0;
    private static MediaPlayer[] H = new MediaPlayer[4];
    public static ProgressDialog a;
    public static int b;
    public static int c;
    public static int d;
    public static int e;
    public static int f = 1;
    public static boolean g;
    public static boolean h = false;
    public static ProgressDialog i;
    public static int j = 0;
    public static int k = 1;
    public static int l = 1;
    public static int m = 0;
    public static int n = 0;
    public static int o = -999;
    public static int p = 0;
    public static int q = 0;
    public static int r = 0;
    public static int s = 1;
    public static int t = 0;
    public static int u = 0;
    public static int v;
    public static int w;
    public static int x;
    public static int y;
    private Handler A = new Handler();
    private Runnable B;
    private libAdMaker C = null;
    private SensorManager I;
    private Sensor J;
    private float[] K = {0.0f, 0.0f, 0.0f};
    private float[] L = {0.0f, 0.0f, 0.0f};
    private final int M = 3000;
    private Handler N = new Handler();
    private Runnable O;
    private p P;
    private s Q;
    /* access modifiers changed from: private */
    public Activity R;
    private String S;
    private long T;
    private final int z = 1000;

    public static void a(int i2) {
        if (f == 1 && H[i2] != null) {
            MediaPlayer mediaPlayer = H[i2];
            mediaPlayer.setLooping(false);
            mediaPlayer.seekTo(0);
            mediaPlayer.start();
        }
    }

    /* access modifiers changed from: private */
    public void a(Activity activity, String str, String str2, String str3) {
        g = true;
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setIcon((int) R.drawable.icon);
        builder.setTitle(str);
        builder.setMessage(str2);
        builder.setPositiveButton(str3, new d(this, activity));
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    static /* synthetic */ void a(JpPokerLib jpPokerLib) {
        SharedPreferences.Editor edit = jpPokerLib.getSharedPreferences(b(new byte[]{67, 99, 107, 95, 117, 99, 97, 124, 122, 69, 122, 118, 97}), 0).edit();
        jpPokerLib.S = Settings.Secure.getString(jpPokerLib.getContentResolver(), "android_id");
        jpPokerLib.T = System.currentTimeMillis();
        edit.putString(b(new byte[]{72, 104, 120, 102, 115, 116, 91, 119}), jpPokerLib.S);
        edit.putLong(b(new byte[]{88, 100, 99, 106, 67, 101, 115, 126, 100}), jpPokerLib.T);
        edit.commit();
    }

    public static int b(int i2) {
        String num = Integer.toString(i2);
        int length = num.length();
        int i3 = i2 < 200 ? 72 : i2 < 600 ? 23 : i2 < 1000 ? 235 : 542;
        for (int i4 = 0; i4 < length; i4++) {
            i3 = ((i3 + (num.charAt(i4) * num.charAt(i4))) % 100000) + (num.charAt(i4) * 15 * length);
        }
        return i3;
    }

    /* access modifiers changed from: private */
    public static String b(byte[] bArr) {
        byte[] bArr2 = new byte[bArr.length];
        byte b2 = 12;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            bArr2[i2] = (byte) (bArr[i2] ^ b2);
            b2 = (byte) (b2 + 1);
            if (b2 > 120) {
                b2 = 12;
            }
        }
        try {
            return new String(bArr2, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            return new String(bArr2);
        }
    }

    private void b() {
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setBackgroundColor(-16777216);
        linearLayout.setOrientation(1);
        linearLayout.setGravity(51);
        setContentView(linearLayout);
        this.C = new libAdMaker(this);
        this.C.setHorizontalScrollBarEnabled(false);
        this.C.a(this);
        this.C.a = b(new byte[]{58, 63, 54});
        this.C.b = b(new byte[]{56, 61, 61, 61});
        this.C.a(b(new byte[]{100, 121, 122, Byte.MAX_VALUE, 42, 62, 61, 122, 121, 116, 113, 114, 107, 55, 123, Byte.MAX_VALUE, 49, 112, Byte.MAX_VALUE, 116, 69, 83, 12, 74, 74, 67, 73, 8, 73, 89, 90, 88, 3, 84, 91, 76, 1, 8, 64, 4, 7, 3, 76, 66, 84, 23, 82, 79, 81, 81}));
        this.C.setBackgroundColor(-16777216);
        this.C.d();
        linearLayout.addView(this.C, -2, (int) ((getApplicationContext().getResources().getDisplayMetrics().density * 50.0f) + 0.5f));
        linearLayout.addView(new b(this), -2, -2);
    }

    private int c() {
        String packageName = getApplicationContext().getPackageName();
        String str = (String) getText(R.string.app_name);
        if (packageName.equals(b(new byte[]{111, 98, 99, 33, 123, 112, 120, 122, 102, 124, 120, 57, 121, 119, 126, 105, 115, 116, 122, 49, 74, 81, 82, 76, 79, 64, 84, 120, 78}))) {
            if (str.equals(b(new byte[]{-17, -113, -74, -20, -109, -78, -15, -112, -105, -10, -108, -72, -5, -102, -121, -8, -97, -98, -3, -100, -88, -62, -95, -104, -57, -90, -69, -60, -85, -107, -55, -87, -121, -50, -83, -109, 111, 119, 96, 118, 113}))) {
                return 111111;
            }
            if (str.equals(b(new byte[]{70, 108, 109, 100, 64, 126, 102, 67, 123, 126, 115, 101, 71, 95, 72, 94, 89}))) {
                return 111111;
            }
        }
        if (packageName.equals(b(new byte[]{111, 98, 99, 33, 123, 112, 120, 122, 102, 124, 120, 57, 121, 119, 126, 105, 115, 116, 122, 49, 74, 81, 82, 76, 79, 64, 84}))) {
            if (str.equals(b(new byte[]{-17, -113, -74, -20, -109, -78, -15, -112, -105, -10, -108, -72, -5, -102, -121, -8, -97, -98, -3, -100, -88, -62, -95, -104, -57, -90, -69, -60, -85, -107, -55, -87, -121, -50, -83, -109}))) {
                return 222222;
            }
            if (str.equals(b(new byte[]{70, 108, 109, 100, 64, 126, 102, 67, 123, 126, 115, 101}))) {
                return 222222;
            }
        }
        return 0;
    }

    private static void c(int i2) {
        if (H[i2] != null && H[i2].isPlaying()) {
            H[i2].stop();
            try {
                H[i2].prepare();
            } catch (IOException | IllegalStateException e2) {
            }
        }
    }

    static /* synthetic */ void c(JpPokerLib jpPokerLib) {
        String string = Settings.Secure.getString(jpPokerLib.getContentResolver(), "android_id");
        jpPokerLib.P = new m(jpPokerLib);
        jpPokerLib.Q = new s(jpPokerLib, new l(jpPokerLib, new o(new byte[]{-23, -108, -57, 29, -35, -21, -96, -47, 52, -31, 55, 54, -15, -106, -100, -20, 31, -89, -13, 74}, jpPokerLib.getPackageName(), string)), "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgLVla0HjIXI94f7yXtEeCFkb89OZfd2AUxSYixVcuJsj7deM7zzR+kdKTT5SyYFI2c8nC3kVHLfjcCYn/gqHxbP7iULYdScoT11nf8ASDC2owKxTrdWUV8w0lSQV77JpX0s2UTka2X3odTd2yryezq1viNRaOHswUMd12uApCtAb7fXr/us+SuT++xEJPw2g/6UuiD+RPUCDRnX3OIZ//+M89d5KyGVt9qi+ptLVlCMy23BVgwRzmyYCkVjdE5NdMr4tdC/aRuGhTF7xLmLmDJA43l4S/l51jZpgwksqsTXbuifAI9haeTu19vtpnGeFizRYb+h/NCDHoctaNk8RnQIDAQAB");
        jpPokerLib.Q.a(jpPokerLib.P);
    }

    public final void a() {
        String language = Locale.getDefault().getLanguage();
        if (language.equals(Locale.JAPAN.toString()) || language.equals(Locale.JAPANESE.toString())) {
            a(this, b(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), b(new byte[]{-17, -114, -101, -20, -109, -69, -15, -112, -88, -14, -97, -97, -5, -104, -67, -8, -99, -78, -3, -99, -124, -62, -95, -112, -57, -89, -103, -60, -85, -107, -55, -88, -95, -50, -83, -84, -45, -78, -70, -43, -70, -112, -47, -127, -94, -38, -69, -82, -33, -65, -78, -36, -63, -25, -95, -62, -64, -90, -60, -52, -83, -10, -49, -93, -22, -52, -83, -50, -36, -78, -45, -47, -73, -41, -36, -76, -39, -25, -71, -38, -59, -66, -34, -35}), b(new byte[]{-21, -72, -116, -21, -86, -105}));
        } else {
            a(this, b(new byte[]{73, Byte.MAX_VALUE, 124, 96, 98}), b(new byte[]{88, 101, 107, 47, 118, 99, 119, 118, 52, 99, 115, 101, 107, 112, 117, 117, 60, 112, 107, 108, 84, 1, 74, 66, 82, 64, 6, 70, 70, 9, 99, 69, 88, 72, 92, 65, 85, 69, 18, 80, 91, 91, 88, 82, 91, 77, 83, 84, 82, 19}), b(new byte[]{73, 117, 103, 123}));
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            switch (keyEvent.getKeyCode()) {
                case 4:
                    g = true;
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle((String) getText(R.string.app_name));
                    builder.setIcon((int) R.drawable.icon);
                    builder.setMessage((String) getText(R.string.OnePersonPlay28));
                    builder.setPositiveButton(b(new byte[]{67, 70}), new l(this, this));
                    builder.setNegativeButton(b(new byte[]{79, 76, 64, 76, 85, 93}), new c(this, this));
                    builder.setCancelable(false);
                    builder.create();
                    builder.show();
                    return true;
            }
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    public void onAccuracyChanged(Sensor sensor, int i2) {
    }

    public void onCreate(Bundle bundle) {
        boolean z2;
        NetworkInfo activeNetworkInfo;
        String language;
        int i2;
        int i3;
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFormat(-3);
        getWindow().addFlags(1024);
        E = 0;
        F = -1;
        g = true;
        ProgressDialog progressDialog = new ProgressDialog(this);
        i = progressDialog;
        progressDialog.setTitle(getText(R.string.app_name));
        i.setMessage(getText(R.string.OnePersonPlay4));
        i.setIndeterminate(false);
        i.setProgressStyle(0);
        i.setIcon((int) R.drawable.icon);
        i.setMax(100);
        i.setCancelable(false);
        i.show();
        SharedPreferences sharedPreferences = getSharedPreferences(b(new byte[]{67, 99, 107, 95, 117, 99, 97, 124, 122, 69, 122, 118, 97}), 0);
        if (Setting.a(this, b(new byte[]{120, 98, 105, 104, 124, 116, 77, 99, 102, 112, 112, 114, 106, 124, 116, 120, 121}))) {
            f = 1;
        } else {
            f = 0;
        }
        if (Setting.a(this, b(new byte[]{120, 98, 105, 104, 124, 116, 77, 99, 102, 112, 112, 114, 106, 124, 116, 120, 121, 47}))) {
            t = 1;
        } else {
            t = 0;
        }
        int a2 = Setting.a(Setting.a(this, b(new byte[]{107, 108, 99, 106, 79, 97, 96, 118, 114, 112, 100, 114, 118, 122, Byte.MAX_VALUE}), b(new byte[]{107, 108, 99, 106, 32, 32})));
        k = a2;
        l = a2;
        b = sharedPreferences.getInt(b(new byte[]{75, 108, 99, 106}), 0);
        c = sharedPreferences.getInt(b(new byte[]{95, 120, 99, 72, 113, 124, 119}), 0);
        d = sharedPreferences.getInt(b(new byte[]{79, 98, 103, 97}), 0);
        e = sharedPreferences.getInt(b(new byte[]{95, 120, 99, 76, Byte.MAX_VALUE, 120, 124}), 0);
        E = sharedPreferences.getInt(b(new byte[]{79, 101, 107, 108, 123, 67}), 0);
        v = sharedPreferences.getInt(b(new byte[]{72, 104, 108, 123}), 0);
        w = sharedPreferences.getInt(b(new byte[]{95, 73, 107, 109, 100}), 0);
        x = sharedPreferences.getInt(b(new byte[]{70, 108, 109, 100, 96, 126, 102}), 0);
        y = sharedPreferences.getInt(b(new byte[]{95, 71, 111, 108, 123, 97, 125, 103}), 0);
        sharedPreferences.getString(b(new byte[]{72, 104, 120, 102, 115, 116, 91, 119}), b(new byte[]{98, 98, 96, 106}));
        sharedPreferences.getLong(b(new byte[]{88, 100, 99, 106, 67, 101, 115, 126, 100}), 10);
        if (b == 0) {
            c = b(b);
        }
        if (d == 0) {
            e = b(d);
        }
        if (v == 0) {
            w = b(v);
        }
        if (x == 0) {
            x = 500000;
            y = b(500000);
        }
        if (t == 0) {
            s = 0;
            u = 0;
        } else {
            s = 1;
            u = 1;
        }
        i.incrementProgressBy(10);
        try {
            H[0] = MediaPlayer.create(this, (int) R.raw.s27000);
            i.incrementProgressBy(9);
            H[1] = MediaPlayer.create(this, (int) R.raw.s27001);
            i.incrementProgressBy(9);
            H[2] = MediaPlayer.create(this, (int) R.raw.s27002);
            i.incrementProgressBy(9);
            H[3] = MediaPlayer.create(this, (int) R.raw.s27003);
        } catch (Exception e2) {
        }
        setVolumeControlStream(3);
        if (c() == 222222) {
            this.S = b(new byte[]{61, 63, 61, 59, 37});
            this.R = this;
            this.O = new h(this);
            SharedPreferences sharedPreferences2 = getSharedPreferences(b(new byte[]{67, 99, 107, 95, 117, 99, 97, 124, 122, 69, 122, 118, 97}), 0);
            this.S = sharedPreferences2.getString(b(new byte[]{72, 104, 120, 102, 115, 116, 91, 119}), b(new byte[]{98, 98, 96, 106}));
            this.T = sharedPreferences2.getLong(b(new byte[]{88, 100, 99, 106, 67, 101, 115, 126, 100}), 10);
            if (!this.S.equals(Settings.Secure.getString(getContentResolver(), "android_id"))) {
                this.N.postDelayed(this.O, 3000);
                this.S = Settings.Secure.getString(getContentResolver(), "android_id");
            } else if (this.T + 2592000000L < System.currentTimeMillis()) {
                this.N.postDelayed(this.O, 3000);
                this.S = Settings.Secure.getString(getContentResolver(), "android_id");
            } else {
                try {
                    if (this.Q != null) {
                        this.Q.a();
                        this.Q = null;
                    }
                } catch (Exception e3) {
                }
                try {
                    if (this.N != null) {
                        this.N.removeCallbacks(this.O);
                        this.N = null;
                    }
                } catch (Exception e4) {
                }
            }
        }
        if (c() == 111111) {
            try {
                String[] strArr = getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 4096).requestedPermissions;
                int length = strArr.length;
                int i4 = 0;
                while (true) {
                    if (i4 >= length) {
                        i2 = 0;
                        break;
                    } else if (strArr[i4].equals("android.permission.INTERNET")) {
                        i2 = 0 + 1;
                        break;
                    } else {
                        i4++;
                    }
                }
                int i5 = 0;
                while (true) {
                    if (i5 >= length) {
                        i3 = i2;
                        break;
                    } else if (strArr[i5].equals("android.permission.ACCESS_NETWORK_STATE")) {
                        i3 = i2 + 1;
                        break;
                    } else {
                        i5++;
                    }
                }
                if (i3 == 2) {
                    z2 = true;
                    activeNetworkInfo = ((ConnectivityManager) getApplicationContext().getSystemService("connectivity")).getActiveNetworkInfo();
                    if ((activeNetworkInfo == null && activeNetworkInfo.isConnected()) || !z2) {
                        language = Locale.getDefault().getLanguage();
                        if (!language.equals(Locale.JAPAN.toString()) || language.equals(Locale.JAPANESE.toString())) {
                            a(this, b(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), b(new byte[]{-17, -114, -101, -20, -109, -69, -15, -112, -88, -14, -97, -97, -5, -104, -67, -8, -99, -78, -3, -99, -124, -62, -95, -112, -57, -89, -103, -60, -85, -107, -55, -88, -95, -50, -83, -84, -45, -78, -70, -43, -70, -112, -47, -127, -94, -38, -69, -82, -33, -65, -78, -36, -63, -25, -95, -62, -64, -90, -60, -52, -83, -10, -49, -93, -22, -52, -83, -50, -36, -78, -45, -47, -73, -41, -36, -76, -39, -25, -71, -38, -59, -66, -34, -35}), b(new byte[]{-21, -72, -116, -21, -86, -105}));
                        } else {
                            a(this, b(new byte[]{73, Byte.MAX_VALUE, 124, 96, 98}), b(new byte[]{88, 101, 107, 47, 118, 99, 119, 118, 52, 99, 115, 101, 107, 112, 117, 117, 60, 112, 107, 108, 84, 1, 74, 66, 82, 64, 6, 70, 70, 9, 99, 69, 88, 72, 92, 65, 85, 69, 18, 80, 91, 91, 88, 82, 91, 77, 83, 84, 82, 19}), b(new byte[]{73, 117, 103, 123}));
                            return;
                        }
                    } else {
                        String language2 = Locale.getDefault().getLanguage();
                        if (language2.equals(Locale.JAPAN.toString()) || language2.equals(Locale.JAPANESE.toString())) {
                            b();
                            return;
                        } else if (a.a(101) + 0 < 95) {
                            b();
                            return;
                        } else {
                            LinearLayout linearLayout = new LinearLayout(this);
                            linearLayout.setBackgroundColor(-16777216);
                            linearLayout.setOrientation(1);
                            linearLayout.setGravity(51);
                            setContentView(linearLayout);
                            AdView adView = new AdView(this, f.a, "a14dfcf247a2879");
                            adView.setBackgroundColor(0);
                            linearLayout.addView(adView, -2, (int) ((getApplicationContext().getResources().getDisplayMetrics().density * 50.0f) + 0.5f));
                            adView.a(new com.google.ads.e());
                            linearLayout.addView(new b(this), -2, -2);
                            return;
                        }
                    }
                }
            } catch (PackageManager.NameNotFoundException e5) {
            }
            z2 = false;
            activeNetworkInfo = ((ConnectivityManager) getApplicationContext().getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null && activeNetworkInfo.isConnected()) {
            }
            language = Locale.getDefault().getLanguage();
            if (!language.equals(Locale.JAPAN.toString())) {
            }
            a(this, b(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), b(new byte[]{-17, -114, -101, -20, -109, -69, -15, -112, -88, -14, -97, -97, -5, -104, -67, -8, -99, -78, -3, -99, -124, -62, -95, -112, -57, -89, -103, -60, -85, -107, -55, -88, -95, -50, -83, -84, -45, -78, -70, -43, -70, -112, -47, -127, -94, -38, -69, -82, -33, -65, -78, -36, -63, -25, -95, -62, -64, -90, -60, -52, -83, -10, -49, -93, -22, -52, -83, -50, -36, -78, -45, -47, -73, -41, -36, -76, -39, -25, -71, -38, -59, -66, -34, -35}), b(new byte[]{-21, -72, -116, -21, -86, -105}));
        } else if (c() != 222222) {
            String language3 = Locale.getDefault().getLanguage();
            if (language3.equals(Locale.JAPAN.toString()) || language3.equals(Locale.JAPANESE.toString())) {
                a(this, b(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), b(new byte[]{-17, -116, -109, -20, -111, -65, -10, -88, -126, -10, -105, -71, -5, -101, -78, -8, -97, -76, -3, -100, -100, -62, -94, -95}), b(new byte[]{-21, -72, -116, -21, -86, -105}));
            } else {
                a(this, b(new byte[]{73, Byte.MAX_VALUE, 124, 96, 98}), b(new byte[]{67, 121, 102, 106, 98, 49, 119, 97, 102, 122, 100, 57}), b(new byte[]{73, 117, 103, 123}));
            }
        } else if (this.S.equals(Settings.Secure.getString(getContentResolver(), "android_id"))) {
            setContentView(new b(this));
            G = 1;
        } else {
            String language4 = Locale.getDefault().getLanguage();
            if (language4.equals(Locale.JAPAN.toString()) || language4.equals(Locale.JAPANESE.toString())) {
                a(this, b(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), b(new byte[]{69, 99, 103, 123, 121, 112, 126, 122, 110, 112, 54, 82, 106, 107, 117, 105, 50}), b(new byte[]{-21, -72, -116, -21, -86, -105}));
            } else {
                a(this, b(new byte[]{73, Byte.MAX_VALUE, 124, 96, 98}), b(new byte[]{69, 99, 103, 123, 121, 112, 126, 122, 110, 112, 54, 82, 106, 107, 117, 105, 50}), b(new byte[]{73, 117, 103, 123}));
            }
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        g = true;
        String language = Locale.getDefault().getLanguage();
        return (language.equals(Locale.JAPAN.toString()) || language.equals(Locale.JAPANESE.toString())) ? new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle(b(new byte[]{-17, -114, -89, -20, -110, -75, -15, -111, -81, -10, -107, -92, -5, -101, -93, -8, -98, -75, -3, -100, -119, -62, -95, -97})).setMessage(b(new byte[]{-17, -116, -99, -20, -111, -65, -15, -111, -74, -10, -107, Byte.MIN_VALUE, -5, -102, -80, -8, -98, -84, -3, -100, -100, -62, -96, -108, -57, -90, -127, -60, -85, -102, -55, -86, -125, -50, -82, -82, -45, -78, -101, -48, -74, -111, -43, -75, -125, -38, -71, -120, -33, -65, -121, -36, -63, -44, -95, -63, -56, -90, -57, -31, -85, -56, -50, -88, -51, -13, -83, -50, -53, -78, -48, -64, -73, -43, -44, -76, -38, -5, -71, -40, -17, -66, -35, -42, -125, -30, -49, Byte.MIN_VALUE, -25, -39, -123, -28, -31, -118, -23, -11, -113, -18, -46, -116, -14, -64, -111, -16, -9, -106, -11, -1, -101, -115, -122, -19, -115, -103, -7, -95, -81, -15, -112, -77, -12, -103, -114, -7, -102, -70, -2, -97, -112, -61, -96, -126, -64, -91, -80, -59, -90, -84, -54, -86, -87})).setPositiveButton(b(new byte[]{-17, -113, -84, -20, -109, -122, -15, -112, -66, -3, -91, -85, -3, -100, -65}), new e(this)).setNegativeButton(b(new byte[]{-21, -72, -116, -21, -86, -105}), new f(this)).setCancelable(false).create() : new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle(b(new byte[]{64, 100, 109, 106, 126, 98, 119, 119, 52, 80, 100, 101, 119, 107})).setMessage(b(new byte[]{88, 101, 103, 124, 48, 112, 98, 99, 120, 124, 117, 118, 108, 112, 117, 117, 60, 116, 109, 63, 78, 78, 86, 3, 72, 76, 69, 66, 70, 90, 79, 79, 2, 13, 126, 67, 85, 80, 65, 86, 20, 69, 67, 69, 91, 81, 91, 72, 89, 29, 87, 75, 96, 39, 48, 44, 41, 101, 7, 41, 44, 59, 37, 34, 40, 109, 3, 46, 34, 58, 55, 39, 122})).setPositiveButton(b(new byte[]{78, 120, 119, 47, 113, 97, 98}), new g(this)).setNegativeButton(b(new byte[]{73, 117, 103, 123}), new i(this)).setCancelable(false).create();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 0, (int) R.string.menu_item0).setIcon(17301577);
        menu.add(0, 1, 0, (int) R.string.menu_item1).setIcon(17301568);
        menu.add(0, 2, 0, (int) R.string.menu_item2).setIcon((int) R.drawable.favicon);
        menu.add(0, 3, 0, (int) R.string.menu_item3).setIcon(17301569);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            if (this.C != null) {
                this.C.e();
                this.C = null;
            }
        } catch (Exception e2) {
        }
        if (d > 999999999) {
            d = 999999999;
        }
        if (x > 999999999) {
            x = 999999999;
        }
        if (b > 999999999) {
            b -= 999999999;
        }
        y = b(x);
        e = b(d);
        w = b(v);
        c = b(b);
        SharedPreferences.Editor edit = getSharedPreferences(b(new byte[]{67, 99, 107, 95, 117, 99, 97, 124, 122, 69, 122, 118, 97}), 0).edit();
        c = b(b);
        e = b(d);
        w = b(v);
        y = b(x);
        edit.putInt(b(new byte[]{75, 108, 99, 106}), b);
        edit.putInt(b(new byte[]{95, 120, 99, 72, 113, 124, 119}), c);
        edit.putInt(b(new byte[]{79, 98, 103, 97}), d);
        edit.putInt(b(new byte[]{95, 120, 99, 76, Byte.MAX_VALUE, 120, 124}), e);
        edit.putInt(b(new byte[]{79, 101, 107, 108, 123, 67}), E);
        edit.putInt(b(new byte[]{72, 104, 108, 123}), v);
        edit.putInt(b(new byte[]{95, 73, 107, 109, 100}), w);
        edit.putInt(b(new byte[]{70, 108, 109, 100, 96, 126, 102}), x);
        edit.putInt(b(new byte[]{95, 71, 111, 108, 123, 97, 125, 103}), y);
        edit.commit();
        for (int i2 = 0; i2 < 4; i2++) {
            if (H[i2] != null) {
                c(i2);
                H[i2].release();
            }
        }
        if (this.J != null) {
            this.I.unregisterListener(this);
            this.J = null;
        }
        try {
            if (this.Q != null) {
                this.Q.a();
                this.Q = null;
            }
        } catch (Exception e3) {
        }
        try {
            if (this.N != null) {
                this.N.removeCallbacks(this.O);
                this.N = null;
            }
        } catch (Exception e4) {
        }
        System.exit(0);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        String str;
        switch (menuItem.getItemId()) {
            case 0:
                Intent intent = new Intent(this, Setting.class);
                intent.putExtra(b(new byte[]{120, 104, 118, 123}), b(new byte[]{88, 72, 93, 91, 48, 85, 83, 71, 85}));
                startActivityForResult(intent, 0);
                return true;
            case 1:
                startActivityForResult(new Intent(this, Help.class), 1);
                return true;
            case 2:
                startActivity(new Intent(b(new byte[]{109, 99, 106, 125, Byte.MAX_VALUE, 120, 118, 61, 125, 123, 98, 114, 118, 109, 52, 122, Byte.MAX_VALUE, 105, 119, 112, 78, 15, 116, 106, 97, 114}), Uri.parse(b(new byte[]{100, 121, 122, Byte.MAX_VALUE, 42, 62, 61, 120, 117, Byte.MAX_VALUE, Byte.MAX_VALUE, 101, 113, 119, 52, 120, 115, 112}))));
                return true;
            case 3:
                try {
                    str = getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0).versionName;
                } catch (PackageManager.NameNotFoundException e2) {
                    str = " ";
                }
                g = true;
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle((String) getText(R.string.app_name));
                builder.setIcon((int) R.drawable.icon);
                builder.setMessage(String.valueOf(b(new byte[]{90, 104, 124, 124, 121, 126, 124, 41, 52})) + str + D + D + b(new byte[]{79, 98, 126, 118, 98, 120, 117, 123, 96, 53, 36, 39, 41, 40, 54, 59, 95, 124, 112, 123, 89, 105, 77, 86, 87, 64}));
                builder.setPositiveButton(b(new byte[]{67, 70}), new k(this));
                builder.setCancelable(false);
                builder.create();
                builder.show();
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        try {
            if (this.C != null) {
                this.C.f();
            }
        } catch (Exception e2) {
        }
        g = true;
        c(0);
        c(1);
        c(2);
        if (this.J != null) {
            this.I.unregisterListener(this);
            this.J = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        ProgressDialog progressDialog = new ProgressDialog(this);
        a = progressDialog;
        progressDialog.setTitle(getText(R.string.app_name));
        a.setIndeterminate(false);
        a.setProgressStyle(0);
        a.setCancelable(false);
        a.show();
        this.B = new j(this);
        this.A.postDelayed(this.B, 1000);
        try {
            if (this.C != null) {
                this.C.d();
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        g = false;
        this.I = (SensorManager) getSystemService("sensor");
        List<Sensor> sensorList = this.I.getSensorList(1);
        if (sensorList.size() > 0) {
            this.J = sensorList.get(0);
        }
        if (this.J != null) {
            this.I.registerListener(this, this.J, 2);
        }
        try {
            if (this.C != null) {
                this.C.d();
            }
        } catch (Exception e2) {
        }
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        float[] fArr = sensorEvent.values;
        this.K[0] = (fArr[0] * 0.1f) + (this.K[0] * 0.9f);
        this.K[1] = (fArr[1] * 0.1f) + (this.K[1] * 0.9f);
        this.K[2] = (fArr[2] * 0.1f) + (this.K[2] * 0.9f);
        this.L[0] = fArr[0] - this.K[0];
        this.L[1] = fArr[1] - this.K[1];
        this.L[2] = fArr[2] - this.K[2];
        u = 0;
        if (s == 0) {
            if (fArr[0] < -8.0f) {
                u = 1;
            } else if (fArr[0] > 8.0f) {
                u = 1;
            }
        } else if (fArr[0] < -2.0f) {
            u = 1;
        } else if (fArr[0] > 2.0f) {
            u = 1;
        }
        if (t == 0) {
            if (s != 0) {
                r = 1;
            }
            s = 0;
        } else if (fArr[2] < 8.0f && s != u) {
            s = u;
            r = 1;
        }
        if (s == 0) {
            if (fArr[0] < -2.0f) {
                if (p != 1) {
                    r = 1;
                }
            } else if (fArr[0] > 2.0f) {
                if (p != 3) {
                    r = 1;
                }
            } else if (p != 2) {
                r = 1;
            }
        }
        p = 2;
        if (s == 0) {
            if (fArr[0] < -2.0f) {
                p = 1;
            } else if (fArr[0] > 2.0f) {
                p = 3;
            }
        }
        if ((Math.abs(this.L[0]) + Math.abs(this.L[1])) + Math.abs(this.L[2]) > 15.0f) {
            j = 1;
        }
    }
}
