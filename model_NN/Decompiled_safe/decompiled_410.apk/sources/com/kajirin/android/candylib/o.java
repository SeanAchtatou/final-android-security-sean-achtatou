package com.kajirin.android.candylib;

import android.preference.Preference;

final class o implements Preference.OnPreferenceChangeListener {
    private /* synthetic */ Setting a;

    o(Setting setting) {
        this.a = setting;
    }

    public final boolean onPreferenceChange(Preference preference, Object obj) {
        JpPokerLib.l = Setting.a((String) obj);
        this.a.getPreferenceManager().findPreference("game_preference").setSummary(Setting.c[JpPokerLib.l]);
        return true;
    }
}
