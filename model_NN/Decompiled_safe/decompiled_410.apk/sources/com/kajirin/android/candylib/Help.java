package com.kajirin.android.candylib;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;
import com.kajirin.android.jppoker_f.R;

public class Help extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setVolumeControlStream(3);
        setContentView((int) R.layout.help);
        ((TextView) findViewById(R.id.DiscriptionTextView)).setText(Html.fromHtml((String) getText(R.string.help), new n(this), null));
    }
}
