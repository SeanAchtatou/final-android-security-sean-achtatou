package com.kajirin.android.candylib;

import android.app.Activity;
import com.android.vending.licensing.p;
import com.android.vending.licensing.v;
import java.util.Locale;

final class m implements p {
    private /* synthetic */ JpPokerLib a;

    /* synthetic */ m(JpPokerLib jpPokerLib) {
        this(jpPokerLib, (byte) 0);
    }

    private m(JpPokerLib jpPokerLib, byte b) {
        this.a = jpPokerLib;
    }

    public final void a() {
        if (!this.a.isFinishing()) {
            JpPokerLib.a(this.a);
        }
    }

    public final void a(v vVar) {
        if (!this.a.isFinishing()) {
            JpPokerLib.g = true;
            if (vVar == v.INVALID_PACKAGE_NAME) {
                String language = Locale.getDefault().getLanguage();
                if (language.equals(Locale.JAPAN.toString()) || language.equals(Locale.JAPANESE.toString())) {
                    this.a.a(this.a.R, JpPokerLib.b(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), JpPokerLib.b(new byte[]{-17, -114, -97, -20, -109, -110, -15, -111, -91, -10, -107, -85, -5, -101, -94, -8, -99, -111, -3, -99, -124, -62, -95, -112, -57, -89, -97, -60, -85, -95, -55, -88, -112, -50, -83, -124, -45, -80, -89, -48, -74, -71, -43, -74, -98, -38, -69, -65, -33, -68, Byte.MIN_VALUE, -36, -63, -38, -95, -63, -41, -90, -58, -59}), JpPokerLib.b(new byte[]{-21, -72, -116, -21, -86, -105}));
                } else {
                    this.a.a(this.a.R, JpPokerLib.b(new byte[]{73, Byte.MAX_VALUE, 124, 96, 98}), JpPokerLib.b(new byte[]{92, 108, 109, 100, 113, 118, 119, 51, 125, 102, 54, 121, 119, 109, 58, 114, 114, 110, 106, 126, 76, 77, 71, 71, 10}), JpPokerLib.b(new byte[]{73, 117, 103, 123}));
                }
            } else if (vVar == v.NON_MATCHING_UID) {
                String language2 = Locale.getDefault().getLanguage();
                if (language2.equals(Locale.JAPAN.toString()) || language2.equals(Locale.JAPANESE.toString())) {
                    this.a.a(this.a.R, JpPokerLib.b(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), JpPokerLib.b(new byte[]{-21, -126, -80, -22, -116, -71, -15, -110, -70, -10, -108, -75, -5, -102, -115, -8, -97, -73, -3, -98, -121, -62, -93, -116, -57, -92, -116, -60, -87, -83, -55, -88, -67, -50, -83, -84, -45, -77, -125, -48, -73, -119, -43, -75, Byte.MIN_VALUE, -38, -69, -107, -44, -101, -65, -39, -15, -61, -95, -62, -56, -90, -57, -46, -85, -53, -58, -88, -51, -13, -83, -50, -57, -78, -45, -52, -73, -43, -44}), JpPokerLib.b(new byte[]{-21, -72, -116, -21, -86, -105}));
                } else {
                    this.a.a(this.a.R, JpPokerLib.b(new byte[]{73, Byte.MAX_VALUE, 124, 96, 98}), JpPokerLib.b(new byte[]{94, 104, Byte.MAX_VALUE, 122, 117, 98, 102, 118, 112, 53, 112, 120, 106, 57, 123, 59, 108, 124, 125, 116, 65, 70, 71, 3, 80, 77, 71, 83, 8, 64, 89, 11, 66, 66, 90, 15, 68, 89, 87, 19, 87, 64, 68, 69, 93, 87, 78, 27, 93, 77, 78, 17}), JpPokerLib.b(new byte[]{73, 117, 103, 123}));
                }
            } else if (vVar == v.NOT_MARKET_MANAGED) {
                String language3 = Locale.getDefault().getLanguage();
                if (language3.equals(Locale.JAPAN.toString()) || language3.equals(Locale.JAPANESE.toString())) {
                    this.a.a(this.a.R, JpPokerLib.b(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), JpPokerLib.b(new byte[]{-17, -114, -112, -20, -109, -83, -15, -111, -91, -10, -107, -108, -5, -102, -110, -8, -99, -70, -3, -97, -95, -62, -93, -80, -57, -92, -120, -60, -85, -72, -55, -88, -81, -50, -84, -98, -45, -78, -114, -48, -74, -115, -43, -74, -76, -36, -78, -67, -33, -68, -75, -36, -62, -53, -95, -62, -6, -90, -57, -36, -85, -53, -39, -88, -52, -49}), JpPokerLib.b(new byte[]{-21, -72, -116, -21, -86, -105}));
                    return;
                }
                JpPokerLib jpPokerLib = this.a;
                Activity b = this.a.R;
                String a2 = JpPokerLib.b(new byte[]{73, Byte.MAX_VALUE, 124, 96, 98});
                byte[] bArr = new byte[39];
                bArr[0] = 65;
                bArr[1] = 108;
                bArr[2] = 124;
                bArr[3] = 100;
                bArr[4] = 117;
                bArr[5] = 101;
                bArr[6] = 50;
                bArr[7] = 119;
                bArr[8] = 123;
                bArr[9] = 112;
                bArr[10] = 101;
                bArr[11] = 55;
                bArr[12] = 118;
                bArr[13] = 118;
                bArr[14] = 110;
                bArr[15] = 59;
                bArr[16] = 119;
                bArr[17] = 115;
                bArr[18] = 113;
                bArr[19] = 104;
                bArr[21] = 64;
                bArr[22] = 64;
                bArr[23] = 76;
                bArr[24] = 81;
                bArr[25] = 81;
                bArr[26] = 6;
                bArr[27] = 83;
                bArr[28] = 64;
                bArr[29] = 76;
                bArr[30] = 10;
                bArr[31] = 91;
                bArr[32] = 77;
                bArr[33] = 78;
                bArr[34] = 69;
                bArr[35] = 78;
                bArr[36] = 87;
                bArr[37] = 84;
                bArr[38] = 28;
                jpPokerLib.a(b, a2, JpPokerLib.b(bArr), JpPokerLib.b(new byte[]{73, 117, 103, 123}));
            } else if (vVar == v.CHECK_IN_PROGRESS) {
                String language4 = Locale.getDefault().getLanguage();
                if (language4.equals(Locale.JAPAN.toString()) || language4.equals(Locale.JAPANESE.toString())) {
                    this.a.a(this.a.R, JpPokerLib.b(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), JpPokerLib.b(new byte[]{-23, -124, -125, -20, -111, -65, -15, -112, -107, -10, -108, -80, -5, -102, -103, -8, -98, -78, -10, -71, -95, -57, -109, -95, -57, -92, -86, -60, -87, -80, -55, -86, -117, -50, -81, -124, -39, -79, Byte.MIN_VALUE, -37, -107, -71, -46, -113, -107, -38, -69, -100, -33, -68, -89, -36, -64, -61}), JpPokerLib.b(new byte[]{-21, -72, -116, -21, -86, -105}));
                } else {
                    this.a.a(this.a.R, JpPokerLib.b(new byte[]{73, Byte.MAX_VALUE, 124, 96, 98}), JpPokerLib.b(new byte[]{77, 45, 126, 125, 117, 103, 123, 124, 97, 102, 54, 116, 112, 124, 121, 112, 60, 111, 123, 110, 85, 68, 81, 87, 4, 76, 85, 7, 73, 69, 88, 78, 77, 73, 87, 15, 89, 95, 18, 67, 70, 90, 81, 69, 93, 74, 73, 21}), JpPokerLib.b(new byte[]{73, 117, 103, 123}));
                }
            } else if (vVar == v.INVALID_PUBLIC_KEY) {
                String language5 = Locale.getDefault().getLanguage();
                if (language5.equals(Locale.JAPAN.toString()) || language5.equals(Locale.JAPANESE.toString())) {
                    this.a.a(this.a.R, JpPokerLib.b(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), JpPokerLib.b(new byte[]{-22, -127, -119, -22, -66, -117, -15, -110, -127, -10, -108, -101, -5, -104, -123, -2, -103, -79, -9, -119, -85, -56, -81, -106, -57, -92, -86, -64, -84, -120, -49, -95, -107, -50, -81, -120, -45, -80, -85, -48, -76, -73}), JpPokerLib.b(new byte[]{-21, -72, -116, -21, -86, -105}));
                } else {
                    this.a.a(this.a.R, JpPokerLib.b(new byte[]{73, Byte.MAX_VALUE, 124, 96, 98}), JpPokerLib.b(new byte[]{95, 120, 126, Byte.MAX_VALUE, 124, 120, 119, 119, 52, 101, 99, 117, 116, 112, 121, 59, 119, 120, 103, 63, 73, 82, 2, 74, 74, 83, 71, 75, 65, 77, 4}), JpPokerLib.b(new byte[]{73, 117, 103, 123}));
                }
            } else if (vVar == v.MISSING_PERMISSION) {
                String language6 = Locale.getDefault().getLanguage();
                if (language6.equals(Locale.JAPAN.toString()) || language6.equals(Locale.JAPANESE.toString())) {
                    JpPokerLib jpPokerLib2 = this.a;
                    Activity b2 = this.a.R;
                    String a3 = JpPokerLib.b(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88});
                    byte[] bArr2 = new byte[90];
                    bArr2[0] = -17;
                    bArr2[1] = -113;
                    bArr2[2] = -84;
                    bArr2[3] = -20;
                    bArr2[4] = -109;
                    bArr2[5] = -122;
                    bArr2[6] = -15;
                    bArr2[7] = -112;
                    bArr2[8] = -66;
                    bArr2[9] = -10;
                    bArr2[10] = -105;
                    bArr2[11] = -72;
                    bArr2[12] = -5;
                    bArr2[13] = -103;
                    bArr2[14] = -106;
                    bArr2[15] = 120;
                    bArr2[16] = 115;
                    bArr2[17] = 112;
                    bArr2[18] = 48;
                    bArr2[19] = 126;
                    bArr2[20] = 78;
                    bArr2[21] = 69;
                    bArr2[22] = 80;
                    bArr2[23] = 76;
                    bArr2[24] = 77;
                    bArr2[25] = 65;
                    bArr2[26] = 8;
                    bArr2[27] = 81;
                    bArr2[28] = 77;
                    bArr2[29] = 71;
                    bArr2[30] = 78;
                    bArr2[31] = 66;
                    bArr2[32] = 66;
                    bArr2[33] = 74;
                    bArr2[35] = 108;
                    bArr2[36] = 120;
                    bArr2[37] = 116;
                    bArr2[38] = 113;
                    bArr2[39] = 120;
                    bArr2[40] = 107;
                    bArr2[41] = 121;
                    bArr2[42] = Byte.MAX_VALUE;
                    bArr2[43] = 116;
                    bArr2[44] = 125;
                    bArr2[45] = 119;
                    bArr2[46] = 105;
                    bArr2[47] = 126;
                    bArr2[48] = -33;
                    bArr2[49] = -67;
                    bArr2[50] = -77;
                    bArr2[51] = -36;
                    bArr2[52] = -63;
                    bArr2[53] = -17;
                    bArr2[54] = -95;
                    bArr2[55] = -63;
                    bArr2[56] = -26;
                    bArr2[57] = -90;
                    bArr2[58] = -60;
                    bArr2[59] = -24;
                    bArr2[60] = -85;
                    bArr2[61] = -53;
                    bArr2[62] = -15;
                    bArr2[63] = -88;
                    bArr2[64] = -50;
                    bArr2[65] = -12;
                    bArr2[66] = -90;
                    bArr2[67] = -25;
                    bArr2[68] = -31;
                    bArr2[69] = -76;
                    bArr2[70] = -35;
                    bArr2[71] = -4;
                    bArr2[72] = -73;
                    bArr2[73] = -44;
                    bArr2[74] = -38;
                    bArr2[75] = -78;
                    bArr2[76] = -25;
                    bArr2[77] = -36;
                    bArr2[78] = -78;
                    bArr2[79] = -3;
                    bArr2[80] = -35;
                    bArr2[81] = -66;
                    bArr2[82] = -33;
                    bArr2[83] = -8;
                    bArr2[84] = -125;
                    bArr2[85] = -32;
                    bArr2[86] = -5;
                    bArr2[87] = Byte.MIN_VALUE;
                    bArr2[88] = -28;
                    bArr2[89] = -25;
                    jpPokerLib2.a(b2, a3, JpPokerLib.b(bArr2), JpPokerLib.b(new byte[]{-21, -72, -116, -21, -86, -105}));
                    return;
                }
                this.a.a(this.a.R, JpPokerLib.b(new byte[]{73, Byte.MAX_VALUE, 124, 96, 98}), JpPokerLib.b(new byte[]{77, 125, 126, 47, 125, 100, 97, 103, 52, 103, 115, 102, 109, 124, 105, 111, 60, 126, 113, 114, 14, 64, 76, 71, 86, 74, 79, 67, 6, 95, 79, 69, 72, 68, 64, 72, 30, 114, 122, 118, 119, 126, 105, 123, 113, 122, Byte.MAX_VALUE, 117, 111, 120, 30, 79, 37, 51, 47, 42, 55, 54, 47, 40, 38, 103}), JpPokerLib.b(new byte[]{73, 117, 103, 123}));
            } else if (vVar == v.CONNECTION_ERROR) {
                String language7 = Locale.getDefault().getLanguage();
                if (language7.equals(Locale.JAPAN.toString()) || language7.equals(Locale.JAPANESE.toString())) {
                    this.a.a(this.a.R, JpPokerLib.b(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), JpPokerLib.b(new byte[]{-17, -114, -89, -20, -110, -75, -15, -111, -81, -10, -107, -92, -5, -101, -93, -4, -66, -89, -10, -75, -83, -62, -93, -115, -61, -89, -100, -60, -88, -88, -61, -85, -74, -55, -111, -114, -45, -80, -100, -42, -117, -80, -34, -111, -71, -38, -69, -73, -33, -68, -68, -36, -62, -53, -95, -62, -6, -90, -57, -34, -85, -55, -56, -88, -49, -64, -83, -52, -45, -78, -47, -37, -73, -42, -7, -76, -37, -27, -71, -39, -13, -66, -33, -15, -121, -61, -40, -117, -50, -24, -123, -27, -6, -118, -21, -4, -113, -20, -56, -116, -15, -2, -111, -14, -44, -106, -9, -30, -101, -115, -119, -19, -113, -110}), JpPokerLib.b(new byte[]{-21, -72, -116, -21, -86, -105}));
                } else {
                    this.a.a(this.a.R, JpPokerLib.b(new byte[]{73, Byte.MAX_VALUE, 124, 96, 98}), JpPokerLib.b(new byte[]{79, 101, 107, 108, 123, 49, 116, 124, 102, 53, 122, 126, 123, 124, 116, 104, 121, 61, 115, 106, 83, 85, 2, 65, 65, 5, 69, 72, 69, 68, 95, 69, 69, 78, 79, 91, 89, 94, 92, 29, 20, 101, 90, 82, 89, 74, 95, 27, 95, 85, 91, 92, 43, 97, 59, 44, 49, 55, 102, 41, 45, 61, 61, 36, 62, 38, 96}), JpPokerLib.b(new byte[]{73, 117, 103, 123}));
                }
            } else {
                String language8 = Locale.getDefault().getLanguage();
                if (language8.equals(Locale.JAPAN.toString()) || language8.equals(Locale.JAPANESE.toString())) {
                    this.a.a(this.a.R, JpPokerLib.b(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), JpPokerLib.b(new byte[]{-17, -116, -109, -20, -111, -65, -10, -88, -126, -10, -105, -71, -5, -101, -78, -8, -97, -76, -3, -100, -100, -62, -94, -95}), JpPokerLib.b(new byte[]{-21, -72, -116, -21, -86, -105}));
                } else {
                    this.a.a(this.a.R, JpPokerLib.b(new byte[]{73, Byte.MAX_VALUE, 124, 96, 98}), JpPokerLib.b(new byte[]{67, 121, 102, 106, 98, 49, 119, 97, 102, 122, 100, 57}), JpPokerLib.b(new byte[]{73, 117, 103, 123}));
                }
            }
        }
    }

    public final void b() {
        if (!this.a.isFinishing()) {
            JpPokerLib.g = true;
            this.a.showDialog(0);
        }
    }
}
