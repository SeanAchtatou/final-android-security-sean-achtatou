package com.google.ads;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.ads.util.AdUtil;
import com.google.ads.util.d;

public class AdView extends RelativeLayout implements a {
    private h a;

    public AdView(Activity activity, f fVar, String str) {
        super(activity.getApplicationContext());
        if (a(activity, fVar, (AttributeSet) null)) {
            a(activity, fVar, str);
        }
    }

    public AdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    public AdView(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet);
    }

    private void a(Activity activity, f fVar, String str) {
        this.a = new h(activity, this, fVar, str);
        setGravity(17);
        setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        addView(this.a.h(), (int) TypedValue.applyDimension(1, (float) fVar.a(), activity.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(1, (float) fVar.b(), activity.getResources().getDisplayMetrics()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [java.lang.String, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00db  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0158  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.content.Context r12, android.util.AttributeSet r13) {
        /*
            r11 = this;
            r9 = 0
            java.lang.String r3 = "@string/"
            java.lang.String r10 = "\"."
            java.lang.String r8 = "http://schemas.android.com/apk/lib/com.google.ads"
            if (r13 != 0) goto L_0x000a
        L_0x0009:
            return
        L_0x000a:
            java.lang.String r1 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r1 = "adSize"
            java.lang.String r1 = r13.getAttributeValue(r8, r1)
            if (r1 != 0) goto L_0x001c
            java.lang.String r1 = "AdView missing required XML attribute \"adSize\"."
            com.google.ads.f r2 = com.google.ads.f.a
            r11.a(r12, r1, r2, r13)
            goto L_0x0009
        L_0x001c:
            java.lang.String r2 = "BANNER"
            boolean r2 = r2.equals(r1)
            if (r2 == 0) goto L_0x0037
            com.google.ads.f r1 = com.google.ads.f.a
            r5 = r1
        L_0x0027:
            java.lang.String r1 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r1 = "adUnitId"
            java.lang.String r1 = r13.getAttributeValue(r8, r1)
            if (r1 != 0) goto L_0x007a
            java.lang.String r1 = "AdView missing required XML attribute \"adUnitId\"."
            r11.a(r12, r1, r5, r13)
            goto L_0x0009
        L_0x0037:
            java.lang.String r2 = "IAB_MRECT"
            boolean r2 = r2.equals(r1)
            if (r2 == 0) goto L_0x0043
            com.google.ads.f r1 = com.google.ads.f.b
            r5 = r1
            goto L_0x0027
        L_0x0043:
            java.lang.String r2 = "IAB_BANNER"
            boolean r2 = r2.equals(r1)
            if (r2 == 0) goto L_0x004f
            com.google.ads.f r1 = com.google.ads.f.c
            r5 = r1
            goto L_0x0027
        L_0x004f:
            java.lang.String r2 = "IAB_LEADERBOARD"
            boolean r2 = r2.equals(r1)
            if (r2 == 0) goto L_0x005b
            com.google.ads.f r1 = com.google.ads.f.d
            r5 = r1
            goto L_0x0027
        L_0x005b:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Invalid \"adSize\" value in XML layout: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = "."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.google.ads.f r2 = com.google.ads.f.a
            r11.a(r12, r1, r2, r13)
            goto L_0x0009
        L_0x007a:
            boolean r2 = r11.isInEditMode()
            if (r2 == 0) goto L_0x008a
            java.lang.String r3 = "Ads by Google"
            r4 = -1
            r1 = r11
            r2 = r12
            r6 = r13
            r1.a(r2, r3, r4, r5, r6)
            goto L_0x0009
        L_0x008a:
            java.lang.String r2 = "@string/"
            boolean r2 = r1.startsWith(r3)
            if (r2 == 0) goto L_0x0150
            java.lang.String r2 = "@string/"
            int r2 = r3.length()
            java.lang.String r2 = r1.substring(r2)
            java.lang.String r3 = r12.getPackageName()
            android.util.TypedValue r4 = new android.util.TypedValue
            r4.<init>()
            android.content.res.Resources r6 = r11.getResources()     // Catch:{ NotFoundException -> 0x0115 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ NotFoundException -> 0x0115 }
            r7.<init>()     // Catch:{ NotFoundException -> 0x0115 }
            java.lang.StringBuilder r3 = r7.append(r3)     // Catch:{ NotFoundException -> 0x0115 }
            java.lang.String r7 = ":string/"
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ NotFoundException -> 0x0115 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ NotFoundException -> 0x0115 }
            java.lang.String r2 = r2.toString()     // Catch:{ NotFoundException -> 0x0115 }
            r3 = 1
            r6.getValue(r2, r4, r3)     // Catch:{ NotFoundException -> 0x0115 }
            java.lang.CharSequence r2 = r4.string
            if (r2 == 0) goto L_0x0134
            java.lang.CharSequence r1 = r4.string
            java.lang.String r1 = r1.toString()
            r2 = r1
        L_0x00cf:
            java.lang.String r1 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r1 = "loadAdOnCreate"
            boolean r3 = r13.getAttributeBooleanValue(r8, r1, r9)
            boolean r1 = r12 instanceof android.app.Activity
            if (r1 == 0) goto L_0x0158
            r0 = r12
            android.app.Activity r0 = (android.app.Activity) r0
            r1 = r0
            boolean r4 = r11.a(r12, r5, r13)
            if (r4 == 0) goto L_0x0009
            r11.a(r1, r5, r2)
            if (r3 == 0) goto L_0x0009
            com.google.ads.e r1 = new com.google.ads.e
            r1.<init>()
            java.lang.String r2 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r2 = "keywords"
            java.lang.String r2 = r13.getAttributeValue(r8, r2)
            if (r2 == 0) goto L_0x0153
            java.lang.String r3 = ","
            java.lang.String[] r2 = r2.split(r3)
            int r3 = r2.length
            r4 = r9
        L_0x0101:
            if (r4 >= r3) goto L_0x0153
            r5 = r2[r4]
            java.lang.String r5 = r5.trim()
            int r6 = r5.length()
            if (r6 == 0) goto L_0x0112
            r1.a(r5)
        L_0x0112:
            int r4 = r4 + 1
            goto L_0x0101
        L_0x0115:
            r2 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not find resource for \"adUnitId\": \""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = "\"."
            java.lang.StringBuilder r1 = r1.append(r10)
            java.lang.String r1 = r1.toString()
            r11.a(r12, r1, r5, r13)
            goto L_0x0009
        L_0x0134:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "\"adUnitId\" was not a string: \""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r3 = "\"."
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.String r2 = r2.toString()
            r11.a(r12, r2, r5, r13)
        L_0x0150:
            r2 = r1
            goto L_0x00cf
        L_0x0153:
            r11.a(r1)
            goto L_0x0009
        L_0x0158:
            java.lang.String r1 = "AdView was initialized with a Context that wasn't an Activity."
            com.google.ads.util.d.b(r1)
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdView.a(android.content.Context, android.util.AttributeSet):void");
    }

    private void a(Context context, String str, int i, f fVar, AttributeSet attributeSet) {
        if (getChildCount() == 0) {
            TextView textView = attributeSet == null ? new TextView(context) : new TextView(context, attributeSet);
            textView.setGravity(17);
            textView.setText(str);
            textView.setTextColor(i);
            textView.setBackgroundColor(-16777216);
            LinearLayout linearLayout = attributeSet == null ? new LinearLayout(context) : new LinearLayout(context, attributeSet);
            linearLayout.setGravity(17);
            LinearLayout linearLayout2 = attributeSet == null ? new LinearLayout(context) : new LinearLayout(context, attributeSet);
            linearLayout2.setGravity(17);
            linearLayout2.setBackgroundColor(i);
            int applyDimension = (int) TypedValue.applyDimension(1, (float) fVar.a(), context.getResources().getDisplayMetrics());
            int applyDimension2 = (int) TypedValue.applyDimension(1, (float) fVar.b(), context.getResources().getDisplayMetrics());
            linearLayout.addView(textView, applyDimension - 2, applyDimension2 - 2);
            linearLayout2.addView(linearLayout);
            addView(linearLayout2, applyDimension, applyDimension2);
        }
    }

    private void a(Context context, String str, f fVar, AttributeSet attributeSet) {
        d.b(str);
        a(context, str, -65536, fVar, attributeSet);
        if (isInEditMode()) {
            return;
        }
        if (context instanceof Activity) {
            a((Activity) context, fVar, "");
        } else {
            d.b("AdView was initialized with a Context that wasn't an Activity.");
        }
    }

    private boolean a(Context context, f fVar, AttributeSet attributeSet) {
        if (AdUtil.b(context)) {
            return true;
        }
        a(context, "You must have INTERNET and ACCESS_NETWORK_STATE permissions in AndroidManifest.xml.", fVar, attributeSet);
        return false;
    }

    public final void a(e eVar) {
        if (this.a.n()) {
            this.a.b();
        }
        this.a.a(eVar);
    }
}
