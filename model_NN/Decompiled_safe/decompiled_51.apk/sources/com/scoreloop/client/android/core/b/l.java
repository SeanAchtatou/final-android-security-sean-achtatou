package com.scoreloop.client.android.core.b;

public enum l {
    CREATED(201),
    FREED(1),
    UNKNOWN(0),
    VERIFIED(200);
    
    private final int e;

    private l(int i) {
        this.e = i;
    }
}
