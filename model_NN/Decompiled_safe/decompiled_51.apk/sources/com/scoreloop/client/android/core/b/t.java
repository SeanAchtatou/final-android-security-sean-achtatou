package com.scoreloop.client.android.core.b;

import java.util.List;
import org.json.JSONObject;

public final class t {
    private final String a;
    private String b;

    private t(String str) {
        this.a = str;
    }

    private t(String str, String str2) {
        this(str);
        this.b = str2;
    }

    public t(JSONObject jSONObject) {
        this.a = jSONObject.getString("id");
        this.b = jSONObject.getString("name");
    }

    public static t a() {
        return a("701bb990-80d8-11de-8a39-0800200c9a66");
    }

    private static t a(String str) {
        if (str == null) {
            throw new IllegalArgumentException();
        }
        List<t> g = k.a().f().g();
        if (g != null) {
            for (t tVar : g) {
                if (str.equalsIgnoreCase(tVar.a)) {
                    return tVar;
                }
            }
        }
        return new t(str, "nameless");
    }

    public static t b() {
        List g = k.a().f().g();
        return (g == null || g.size() <= 0) ? a("428a66d4-e6ca-4ff0-b7ea-f482ba4541a1") : (t) g.get(0);
    }

    public final String c() {
        return this.a;
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof t)) {
            return super.equals(obj);
        }
        t tVar = (t) obj;
        if (this.a != null && tVar.a != null) {
            return this.a.equalsIgnoreCase(tVar.a);
        }
        throw new IllegalStateException();
    }

    public final int hashCode() {
        if (this.a != null) {
            return this.a.hashCode();
        }
        throw new IllegalStateException();
    }

    public final String toString() {
        return this.b;
    }
}
