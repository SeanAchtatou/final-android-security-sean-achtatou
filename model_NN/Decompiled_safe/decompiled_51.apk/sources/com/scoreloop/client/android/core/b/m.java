package com.scoreloop.client.android.core.b;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import org.json.JSONObject;

public final class m {
    private Context a;
    private String b;
    private l c = l.UNKNOWN;

    public final String a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final void a(Context context) {
        this.a = context;
    }

    public final void a(l lVar) {
        this.c = lVar;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final String b() {
        if (this.a != null) {
            return ((TelephonyManager) this.a.getSystemService("phone")).getDeviceId();
        }
        throw new IllegalStateException("please use setAndroidContext before calling this method");
    }

    public final JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("id", this.b);
        jSONObject.put("uuid", b());
        jSONObject.put("name", (Object) null);
        jSONObject.put("system_name", "Android");
        jSONObject.put("system_version", Build.VERSION.RELEASE);
        jSONObject.put("model", Build.MODEL);
        return jSONObject;
    }
}
