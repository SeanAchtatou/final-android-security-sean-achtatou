package com.scoreloop.client.android.core.b;

import android.content.Context;
import com.scoreloop.client.android.core.a;
import com.scoreloop.client.android.core.b;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.BitSet;
import java.util.Properties;

public final class j implements a {
    private final BitSet a;
    private final b b;
    private final com.scoreloop.client.android.core.c.a c;
    private final k d;

    public j(Context context, String str, String str2) {
        this(context, str, str2, "https://api.scoreloop.com/bayeux");
    }

    private j(Context context, String str, String str2, String str3) {
        this.a = new BitSet();
        this.b = null;
        q qVar = new q(str, str2);
        qVar.b("1.0");
        try {
            this.c = new com.scoreloop.client.android.core.c.a(new URL(str3));
            this.c.a(qVar);
            this.d = new k(this, this.c);
            this.d.a(qVar);
            k.a(this.d);
            this.d.c().a(context);
            Properties a2 = a(context);
            if (a2 != null) {
                a.a(a2.getProperty("currency.code"));
                a.b(a2.getProperty("currency.symbol"));
                a.c(a2.getProperty("currency.name.singular"));
                a.d(a2.getProperty("currency.name.plural"));
            }
        } catch (MalformedURLException e) {
            throw new IllegalStateException(e);
        }
    }

    private static Properties a(Context context) {
        try {
            InputStream open = context.getAssets().open("scoreloop.properties");
            try {
                Properties properties = new Properties();
                properties.load(open);
                return properties;
            } catch (IOException e) {
                return null;
            }
        } catch (IOException e2) {
            return null;
        }
    }

    public final o a() {
        if (!this.d.d().f()) {
            return new o(0, 1);
        }
        int intValue = this.d.d().c().intValue();
        q d2 = this.d.d();
        return new o(intValue, (!d2.f() ? 1 : Integer.valueOf(d2.b().intValue() - d2.c().intValue())).intValue());
    }

    public final void a(o oVar) {
        this.d.d().b(Integer.valueOf(oVar.d()));
        this.d.d().a(Integer.valueOf(oVar.e()));
    }
}
