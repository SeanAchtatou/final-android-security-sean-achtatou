package com.scoreloop.client.android.core.c;

import org.json.JSONObject;

public class m {
    private static int a = 0;
    private static /* synthetic */ boolean k = (!m.class.desiredAssertionStatus());
    private final p b;
    private String c;
    private JSONObject d;
    private Exception e;
    private JSONObject f;
    private final int g;
    private n h = n.GET;
    private o i;
    private l j = l.IDLE;

    public m(p pVar) {
        this.b = pVar;
        int i2 = a;
        a = i2 + 1;
        this.g = i2;
    }

    public String a() {
        return this.c;
    }

    public final void a(o oVar) {
        if (k || this.j == l.EXECUTING) {
            this.j = l.COMPLETED;
            this.i = oVar;
            this.e = null;
            return;
        }
        throw new AssertionError();
    }

    public final void a(Exception exc) {
        if (k || this.j == l.EXECUTING) {
            this.j = l.FAILED;
            this.i = null;
            this.e = exc;
            return;
        }
        throw new AssertionError();
    }

    public final void a(JSONObject jSONObject) {
        this.f = jSONObject;
    }

    public JSONObject b() {
        return this.d;
    }

    public n c() {
        return this.h;
    }

    public final p d() {
        if (this.b != null) {
            return this.b;
        }
        throw new IllegalStateException();
    }

    public final Exception e() {
        return this.e;
    }

    public final JSONObject f() {
        return this.f;
    }

    public final int g() {
        return this.g;
    }

    public final o h() {
        return this.i;
    }

    public final synchronized l i() {
        return this.j;
    }

    public final boolean j() {
        l i2 = i();
        return i2 == l.COMPLETED || i2 == l.CANCELLED || i2 == l.FAILED;
    }

    public final void k() {
        if (k || this.j == l.EXECUTING || this.j == l.ENQUEUED) {
            this.j = l.CANCELLED;
            this.i = null;
            this.e = null;
            return;
        }
        throw new AssertionError();
    }

    public final void l() {
        if (k || this.j == l.IDLE) {
            this.j = l.ENQUEUED;
            return;
        }
        throw new AssertionError();
    }

    public final void m() {
        if (k || this.j == l.IDLE || this.j == l.ENQUEUED) {
            this.j = l.EXECUTING;
            return;
        }
        throw new AssertionError();
    }
}
