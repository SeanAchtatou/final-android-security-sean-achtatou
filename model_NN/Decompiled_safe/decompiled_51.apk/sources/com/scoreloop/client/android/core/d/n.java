package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.b.q;
import com.scoreloop.client.android.core.c.m;
import com.scoreloop.client.android.core.c.p;
import org.json.JSONException;
import org.json.JSONObject;

final class n extends m {
    private final q a;
    private final com.scoreloop.client.android.core.b.n b;
    private final k c;

    public n(p pVar, q qVar, k kVar, com.scoreloop.client.android.core.b.n nVar) {
        super(pVar);
        this.a = qVar;
        this.c = kVar;
        this.b = nVar;
    }

    public final String a() {
        return String.format("/service/games/%s/scores", this.a.a());
    }

    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            this.b.a(this.c.c().a());
            this.b.a(this.c.f());
            jSONObject.put("score", this.b.f());
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException("Invalid score data", e);
        }
    }

    public final com.scoreloop.client.android.core.c.n c() {
        return com.scoreloop.client.android.core.c.n.POST;
    }
}
