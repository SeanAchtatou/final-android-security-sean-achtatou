package com.scoreloop.client.android.core.c;

public enum n {
    DELETE("delete"),
    GET("get"),
    POST("post"),
    PUT("put");
    
    private final String e;

    private n(String str) {
        this.e = str;
    }

    public static n[] a() {
        return (n[]) f.clone();
    }
}
