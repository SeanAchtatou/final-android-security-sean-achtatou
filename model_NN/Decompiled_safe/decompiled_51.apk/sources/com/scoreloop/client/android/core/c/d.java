package com.scoreloop.client.android.core.c;

import android.os.Handler;
import android.os.Message;

final class d extends Handler {
    private /* synthetic */ a a;

    /* synthetic */ d(a aVar) {
        this(aVar, (byte) 0);
    }

    private d(a aVar, byte b) {
        this.a = aVar;
    }

    public final void handleMessage(Message message) {
        m a2 = this.a.e;
        m unused = this.a.e = null;
        if (a2.i() != l.CANCELLED) {
            switch (message.what) {
                case 1:
                    o oVar = (o) message.obj;
                    Integer a3 = oVar.a();
                    if (a3 != null && a3.intValue() == a2.g()) {
                        a2.a(oVar);
                        break;
                    } else {
                        a2.a(new Exception("Invalid response ID, expected:" + a2.g() + ", but was:" + a3));
                        break;
                    }
                case 2:
                    a2.a((Exception) message.obj);
                    break;
                case 3:
                    a2.a((Exception) message.obj);
                    break;
                default:
                    throw new IllegalStateException("Unknown message type");
            }
            a2.d().a(a2);
        }
        a.c(this.a);
        if (this.a.e == null) {
            a.d(this.a);
        }
    }
}
