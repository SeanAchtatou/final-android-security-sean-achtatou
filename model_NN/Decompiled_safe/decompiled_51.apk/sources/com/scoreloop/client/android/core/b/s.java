package com.scoreloop.client.android.core.b;

import org.json.JSONObject;

public final class s {
    private Integer a;
    private Integer b;

    public final Integer a() {
        return this.a;
    }

    public final void a(JSONObject jSONObject) {
        this.a = Integer.valueOf(jSONObject.getInt("rank"));
        this.b = Integer.valueOf(jSONObject.getInt("total"));
        if (this.a.intValue() == 0) {
            this.a = null;
        }
    }
}
