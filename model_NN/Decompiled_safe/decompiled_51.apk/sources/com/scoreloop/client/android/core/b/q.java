package com.scoreloop.client.android.core.b;

public final class q {
    private String a;
    private String b;
    private Integer c;
    private Integer d;
    private final String e;
    private String f;

    q(String str, String str2) {
        this.b = str;
        this.e = str2;
    }

    public final String a() {
        return this.b;
    }

    public final void a(Integer num) {
        this.c = num;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final Integer b() {
        return this.c;
    }

    public final void b(Integer num) {
        this.d = num;
    }

    public final void b(String str) {
        this.f = str;
    }

    public final Integer c() {
        return this.d;
    }

    public final String d() {
        return this.e;
    }

    public final String e() {
        return this.f;
    }

    public final boolean f() {
        return (this.d == null || this.c == null) ? false : true;
    }
}
