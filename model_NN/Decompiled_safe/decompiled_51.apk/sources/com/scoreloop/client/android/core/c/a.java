package com.scoreloop.client.android.core.c;

import com.scoreloop.client.android.core.a.c;
import com.scoreloop.client.android.core.b.q;
import java.net.URL;
import java.nio.channels.IllegalSelectorException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.LinkedList;
import org.json.JSONException;
import org.json.JSONObject;

public class a {
    private static /* synthetic */ boolean g = (!a.class.desiredAssertionStatus());
    private final URL a;
    private final h b = new h(this.c);
    private final d c = new d(this);
    private j d;
    /* access modifiers changed from: private */
    public m e;
    private final LinkedList f = new LinkedList();

    public a(URL url) {
        this.a = url;
        this.b.setPriority(1);
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA1");
            instance.reset();
            instance.update("https://api.scoreloop.com/bayeux/android/v2".getBytes());
            byte[] digest = instance.digest();
            instance.reset();
            instance.update("https://www.scoreloop.com/android/updates".getBytes());
            byte[] digest2 = instance.digest();
            byte[] bArr = new byte[16];
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = (byte) ((digest[(i + 6) % digest.length] ^ digest2[(i + 3) % digest2.length]) ^ 62);
            }
            this.d = new j(this.a, this.b, bArr);
            for (int i2 = 0; i2 < digest.length; i2++) {
                digest[i2] = (byte) (digest[i2] ^ 26);
            }
            this.d.b(c.a(digest));
            for (int i3 = 0; i3 < digest2.length; i3++) {
                digest2[i3] = (byte) (digest2[i3] ^ 53);
            }
            this.d.a(c.a(digest2));
            this.b.a(this.d);
            this.b.start();
        } catch (NoSuchAlgorithmException e2) {
            throw new IllegalStateException();
        }
    }

    static /* synthetic */ void c(a aVar) {
        "currentRequest: " + (aVar.e != null ? aVar.e.toString() : "null");
        Iterator it = aVar.f.iterator();
        while (it.hasNext()) {
            m mVar = (m) it.next();
            mVar.toString() + ", " + mVar.i().toString();
        }
    }

    private void c(m mVar) {
        "startProcessingRequest: " + mVar.toString();
        if (!g && mVar == null) {
            throw new AssertionError();
        } else if (!g && mVar.j()) {
            throw new AssertionError();
        } else if (g || this.e == null) {
            this.e = mVar;
            this.e.d();
            this.e.m();
            this.b.a(this.e);
        } else {
            throw new AssertionError();
        }
    }

    static /* synthetic */ void d(a aVar) {
        m mVar;
        if (g || aVar.e == null) {
            do {
                mVar = (m) aVar.f.poll();
                if (mVar != null && !mVar.j()) {
                    aVar.c(mVar);
                    return;
                }
            } while (mVar != null);
            return;
        }
        throw new AssertionError();
    }

    public final void a(q qVar) {
        this.b.a(qVar);
    }

    public final void a(m mVar) {
        "addRequest: " + mVar.toString();
        if (mVar.i() == l.ENQUEUED || mVar.i() == l.EXECUTING) {
            throw new IllegalStateException("Request already enqueued or executing");
        } else if (mVar.a() == null) {
            throw new IllegalStateException("Request channel is not set");
        } else if (mVar.c() == null) {
            throw new IllegalStateException("Request method is not set");
        } else {
            if (mVar.f() == null) {
                mVar.a(new JSONObject());
            }
            try {
                mVar.f().put("method", mVar.c().toString());
                if (this.e != null || !this.f.isEmpty()) {
                    mVar.l();
                    this.f.add(mVar);
                    return;
                }
                c(mVar);
            } catch (JSONException e2) {
                throw new IllegalSelectorException();
            }
        }
    }

    public final void b(m mVar) {
        "cancelRequest: " + mVar.toString();
        if (this.e != mVar) {
            mVar.k();
            mVar.d().a(mVar);
        } else if (this.e != null) {
            "doCancelCurrentRequest canceling request: " + this.e.toString();
            this.e.k();
            this.e.d().a(this.e);
            this.b.b();
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        this.b.a();
        super.finalize();
    }
}
