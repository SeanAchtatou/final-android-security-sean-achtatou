package com.scoreloop.client.android.core.b;

public enum u {
    FEMALE("f"),
    MALE("m"),
    UNKNOWN("?");
    
    private String d;

    private u(String str) {
        this.d = str;
    }

    public final String a() {
        return this.d;
    }
}
