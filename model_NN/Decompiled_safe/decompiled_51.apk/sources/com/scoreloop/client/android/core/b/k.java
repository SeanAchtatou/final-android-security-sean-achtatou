package com.scoreloop.client.android.core.b;

import com.scoreloop.client.android.core.a;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public final class k {
    private static k a;
    private final m b = new m();
    private q c;
    private final a d;
    private final com.scoreloop.client.android.core.c.a e;
    private String f;
    private d g;
    private List h = new ArrayList();
    private g i = g.INITIAL;
    private final h j = new h();

    public k(a aVar, com.scoreloop.client.android.core.c.a aVar2) {
        this.e = aVar2;
        this.d = aVar;
    }

    public static k a() {
        return a;
    }

    static void a(k kVar) {
        a = kVar;
    }

    public final void a(d dVar) {
        this.g = dVar;
    }

    public final void a(g gVar) {
        this.i = gVar;
    }

    public final void a(q qVar) {
        this.c = qVar;
    }

    public final void a(List list) {
        this.h = list;
    }

    public final void a(JSONObject jSONObject) {
        this.f = jSONObject.getString("direct_pay_url");
    }

    public final d b() {
        return this.g;
    }

    public final m c() {
        return this.b;
    }

    public final q d() {
        return this.c;
    }

    public final com.scoreloop.client.android.core.c.a e() {
        return this.e;
    }

    public final h f() {
        return this.j;
    }

    public final boolean g() {
        return this.i == g.AUTHENTICATED;
    }

    public final g h() {
        return this.i;
    }

    public final List i() {
        return this.j.g();
    }
}
