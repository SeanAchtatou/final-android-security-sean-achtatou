package com.scoreloop.client.android.core.c;

import org.json.JSONArray;
import org.json.JSONObject;

public final class o {
    private final String a;
    private final Object b;
    private final int c;
    private final Integer d;
    private final boolean e;

    public o(Object obj, String str, int i, Integer num) {
        this.b = obj;
        this.a = str;
        this.c = i;
        this.d = num;
        this.e = obj instanceof JSONArray;
    }

    public final Integer a() {
        return this.d;
    }

    public final JSONArray b() {
        return (JSONArray) this.b;
    }

    public final JSONObject c() {
        return (JSONObject) this.b;
    }

    public final int d() {
        return this.c;
    }
}
