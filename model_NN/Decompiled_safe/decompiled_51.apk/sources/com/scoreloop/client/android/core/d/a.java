package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.b.g;
import com.scoreloop.client.android.core.b.h;
import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.b.q;
import com.scoreloop.client.android.core.c.m;
import com.scoreloop.client.android.core.c.o;
import org.json.JSONObject;

public abstract class a {
    private static /* synthetic */ boolean f = (!a.class.desiredAssertionStatus());
    o a;
    private final k b;
    /* access modifiers changed from: private */
    public m c;
    private final k d;
    /* access modifiers changed from: private */
    public l e;

    a(k kVar, o oVar) {
        if (oVar == null) {
            throw new IllegalArgumentException("observer parameter cannot be null");
        }
        if (kVar == null) {
            this.d = k.a();
        } else {
            this.d = kVar;
        }
        if (f || this.d != null) {
            this.a = oVar;
            this.b = new k(this);
            return;
        }
        throw new AssertionError();
    }

    static Integer a(JSONObject jSONObject) {
        JSONObject jSONObject2;
        if (!jSONObject.has("error") || (jSONObject2 = jSONObject.getJSONObject("error")) == null) {
            return null;
        }
        return Integer.valueOf(jSONObject2.getInt("code"));
    }

    static /* synthetic */ void a(a aVar) {
        " observer = " + aVar.a.toString();
        if (aVar.a != null) {
            aVar.a.a(aVar);
            return;
        }
        throw new IllegalStateException("could not invoke DidReceiveResponse() because of the lack of the observer in the RequestController's instance !");
    }

    /* access modifiers changed from: private */
    public void b(Exception exc) {
        if (this.a != null) {
            this.a.a(exc);
            return;
        }
        throw new IllegalStateException("could not invoke ControllerDidFail() because of the lack of the observer in the RequestController's instance !");
    }

    /* access modifiers changed from: package-private */
    public final q a() {
        return this.d.d();
    }

    /* access modifiers changed from: package-private */
    public final void a(m mVar) {
        g h;
        if (!(!f() || (h = this.d.h()) == g.AUTHENTICATED || h == g.AUTHENTICATING)) {
            if (this.e == null) {
                this.e = new l(this.d, new ab(this));
            }
            this.e.h();
        }
        this.c = mVar;
        this.d.e().a(mVar);
    }

    /* access modifiers changed from: package-private */
    public final void a(Exception exc) {
        m mVar = this.c;
        if (mVar != null) {
            mVar.a(exc);
            b(exc);
        }
    }

    /* access modifiers changed from: package-private */
    public abstract boolean a(m mVar, o oVar);

    /* access modifiers changed from: package-private */
    public final o b() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public final k c() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final k d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final h e() {
        return this.d.f();
    }

    /* access modifiers changed from: package-private */
    public abstract boolean f();

    /* access modifiers changed from: package-private */
    public void g() {
        if (this.c != null) {
            if (!this.c.j()) {
                this.d.e().b(this.c);
            }
            this.c = null;
        }
    }
}
