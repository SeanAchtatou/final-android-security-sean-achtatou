package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.c.m;
import com.scoreloop.client.android.core.c.p;

final class k implements p {
    private /* synthetic */ a a;

    /* synthetic */ k(a aVar) {
        this(aVar, (byte) 0);
    }

    private k(a aVar, byte b) {
        this.a = aVar;
    }

    public final void a(m mVar) {
        switch (d.a[mVar.i().ordinal()]) {
            case 1:
                "RequestCallback.onRequestCompleted: request completed: " + mVar.toString();
                try {
                    if (this.a.a(mVar, mVar.h())) {
                        a.a(this.a);
                        return;
                    }
                    return;
                } catch (Exception e) {
                    this.a.b(e);
                    return;
                }
            case 2:
                "RequestCallback.onRequestCompleted: request failed: " + mVar.toString();
                this.a.b(mVar.e());
                return;
            case 3:
                "RequestCallback.onRequestCompleted: request cancelled: " + mVar.toString();
                this.a.b(new q());
                return;
            default:
                throw new IllegalStateException("onRequestCompleted called for not completed request");
        }
    }
}
