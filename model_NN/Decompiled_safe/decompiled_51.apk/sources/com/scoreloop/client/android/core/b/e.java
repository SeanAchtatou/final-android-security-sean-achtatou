package com.scoreloop.client.android.core.b;

import org.json.JSONObject;

final class e {
    private final String a;
    private final int b;
    private final int c;

    public e(JSONObject jSONObject) {
        this.a = jSONObject.getString("decoration");
        this.b = jSONObject.getInt("threshold");
        this.c = jSONObject.getInt("value");
    }
}
