package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.b.h;
import com.scoreloop.client.android.core.b.n;
import com.scoreloop.client.android.core.b.q;
import com.scoreloop.client.android.core.b.t;
import com.scoreloop.client.android.core.c.p;
import org.json.JSONException;
import org.json.JSONObject;

final class m extends com.scoreloop.client.android.core.c.m {
    private final q a;
    private final Integer b;
    private final n c = null;
    private final t d;
    private final h e;

    public m(p pVar, q qVar, t tVar, h hVar, Integer num) {
        super(pVar);
        this.a = qVar;
        this.d = tVar;
        this.e = hVar;
        this.b = num;
    }

    public final String a() {
        return String.format("/service/games/%s/scores/rankings", this.a.a());
    }

    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.d != null) {
                jSONObject.putOpt("search_list_id", this.d.c());
            }
            if (this.c != null) {
                jSONObject.put("score", this.c.f());
            } else {
                jSONObject.put("user_id", this.e.e());
                jSONObject.put("mode", this.b);
            }
            return jSONObject;
        } catch (JSONException e2) {
            throw new IllegalStateException("Invalid challenge data", e2);
        }
    }

    public final com.scoreloop.client.android.core.c.n c() {
        return com.scoreloop.client.android.core.c.n.GET;
    }
}
