package com.scoreloop.client.android.core.c;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class j {
    private String a = null;
    private Boolean b = false;
    /* access modifiers changed from: private */
    public f c;
    private final b d;
    private String e;
    private URI f;

    j(URL url, b bVar, byte[] bArr) {
        if (bVar == null) {
            throw new IllegalArgumentException("Observer can't be null");
        }
        try {
            this.f = url.toURI();
            this.d = bVar;
            if (bArr != null) {
                this.c = new k(this.f, bArr);
            } else {
                this.c = new f(this.f);
            }
        } catch (URISyntaxException e2) {
            throw new IllegalArgumentException("Malformed URL");
        }
    }

    private void a(c cVar, JSONObject jSONObject) {
        int i = 0;
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(jSONObject);
        JSONArray a2 = this.c.a(cVar.b(), jSONArray);
        int length = a2.length();
        int i2 = 0;
        while (i2 < length) {
            try {
                JSONObject jSONObject2 = a2.getJSONObject(i2);
                String string = jSONObject2.getString("channel");
                Object opt = jSONObject2.opt("data");
                Integer valueOf = jSONObject2.has("id") ? Integer.valueOf(jSONObject2.optInt("id")) : null;
                JSONObject optJSONObject = jSONObject2.optJSONObject("ext");
                if (optJSONObject != null) {
                    i = optJSONObject.optInt("status");
                    if (optJSONObject.has("token")) {
                        this.e = optJSONObject.getString("token");
                    }
                }
                if ("/meta/handshake".equalsIgnoreCase(string)) {
                    try {
                        this.a = jSONObject2.getString("clientId");
                        this.b = true;
                    } catch (JSONException e2) {
                        throw new g(e2);
                    }
                } else if (!"/meta/connect".equalsIgnoreCase(string) && !"/meta/disconnect".equalsIgnoreCase(string)) {
                    this.d.a(valueOf, opt, string, i);
                }
                i2++;
            } catch (JSONException e3) {
                throw new g();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(c cVar) {
        JSONObject jSONObject;
        Integer valueOf = Integer.valueOf(cVar.a);
        JSONObject jSONObject2 = cVar.c;
        String str = cVar.b;
        JSONObject jSONObject3 = cVar.d;
        if (str == null) {
            throw new IllegalArgumentException("Request's channel can't be null !");
        }
        if (this.a == null) {
            JSONObject jSONObject4 = new JSONObject();
            try {
                jSONObject4.put("channel", "/meta/handshake");
                jSONObject4.put("version", "1");
                JSONArray jSONArray = new JSONArray();
                jSONArray.put("request-response");
                jSONObject4.put("supportedConnectionTypes", jSONArray);
                JSONObject jSONObject5 = new JSONObject();
                JSONObject jSONObject6 = new JSONObject();
                jSONObject6.put("version", "2");
                jSONObject5.put("api", jSONObject6);
                jSONObject4.put("ext", jSONObject5);
                a(cVar, this.d.a(jSONObject4));
            } catch (JSONException e2) {
                throw new IllegalStateException();
            }
        }
        JSONObject jSONObject7 = new JSONObject();
        try {
            jSONObject7.put("clientId", this.a);
            jSONObject7.put("channel", str);
            jSONObject7.put("data", jSONObject2);
            jSONObject7.put("id", valueOf);
            if (this.e != null) {
                jSONObject = jSONObject3 == null ? new JSONObject() : jSONObject3;
                jSONObject.put("token", this.e);
            } else {
                jSONObject = jSONObject3;
            }
            jSONObject7.putOpt("ext", jSONObject);
            a(cVar, jSONObject7);
        } catch (JSONException e3) {
            throw new i(e3);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.c.a(str);
    }

    /* access modifiers changed from: package-private */
    public final void b(String str) {
        this.c.b(str);
    }
}
