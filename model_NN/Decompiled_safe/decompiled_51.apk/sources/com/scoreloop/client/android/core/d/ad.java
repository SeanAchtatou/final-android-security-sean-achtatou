package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.b.d;
import com.scoreloop.client.android.core.b.h;
import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.b.n;
import com.scoreloop.client.android.core.b.q;
import com.scoreloop.client.android.core.c.m;
import com.scoreloop.client.android.core.c.p;
import org.json.JSONException;
import org.json.JSONObject;

class ad extends m {
    protected d a;
    protected q b;
    private k c;

    public ad(p pVar, k kVar, q qVar, d dVar) {
        super(pVar);
        this.c = kVar;
        this.b = qVar;
        this.a = dVar;
    }

    public String a() {
        return String.format("/service/games/%s/challenges", this.b.a());
    }

    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            h f = this.c.f();
            if (this.a.a() != null && this.a.a().equals(f)) {
                n b2 = this.a.b();
                if (b2 != null) {
                    b2.a(f.c());
                }
            } else if (this.a.c() != null) {
                this.a.c().equals(f);
            }
            jSONObject.put("challenge", this.a.i());
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException("Invalid challenge data", e);
        }
    }

    public com.scoreloop.client.android.core.c.n c() {
        return com.scoreloop.client.android.core.c.n.POST;
    }
}
