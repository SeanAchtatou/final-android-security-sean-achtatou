package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.b.h;
import com.scoreloop.client.android.core.b.q;
import com.scoreloop.client.android.core.c.n;
import com.scoreloop.client.android.core.c.p;
import org.json.JSONException;
import org.json.JSONObject;

final class y extends f {
    private String b;
    private String c;

    public y(p pVar, q qVar, h hVar) {
        super(pVar, qVar, hVar);
        this.b = hVar.f();
        this.c = hVar.d();
    }

    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            this.a.c(this.b);
            this.a.b(this.c);
            jSONObject.put("user", this.a.i());
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException("Invalid user data", e);
        }
    }

    public final n c() {
        return n.PUT;
    }
}
