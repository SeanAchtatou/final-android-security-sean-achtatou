package com.scoreloop.client.android.core.c;

import com.scoreloop.client.android.core.a.c;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;

class f {
    private final HttpClient a = new DefaultHttpClient();
    private String b;
    private final URI c;
    private String d;

    f(URI uri) {
        this.c = uri;
        HttpParams params = this.a.getParams();
        HttpConnectionParams.setSoTimeout(params, 90000);
        HttpConnectionParams.setConnectionTimeout(params, 10000);
    }

    private JSONArray a(HttpPost httpPost, Object obj) {
        try {
            String a2 = a(httpPost, obj.toString());
            try {
                JSONArray jSONArray = new JSONArray(a2);
                c(jSONArray.toString(4));
                return jSONArray;
            } catch (JSONException e) {
                d(a2);
                throw new g("Server response body failed to parse", e);
            }
        } catch (HttpResponseException e2) {
            throw new g("Failure indicated by HTTP status code", e2);
        } catch (ClientProtocolException e3) {
            throw new g("HTTP protocol error", e3);
        } catch (InterruptedIOException e4) {
            throw new e(e4);
        } catch (IOException e5) {
            throw new i("I/O error occured", e5);
        }
    }

    private static void c(String str) {
        String[] split = str.split("\\},\\n");
        for (int i = 0; i < split.length - 1; i++) {
            split[i] + "},\n";
        }
    }

    private static void d(String str) {
        String[] split = str.split("\\},\\n");
        for (int i = 0; i < split.length - 1; i++) {
            split[i] + "},\n";
        }
    }

    /* access modifiers changed from: package-private */
    public String a(HttpPost httpPost, String str) {
        if (this.d == null || this.b == null) {
            throw new IllegalStateException("Try setting password and username first");
        }
        httpPost.setHeader("Content-Type", b());
        httpPost.setHeader("Accept", b());
        httpPost.setHeader("X-Scoreloop-SDK-Version", "1");
        httpPost.setHeader("Authorization", "Basic " + c.a((this.d + ":" + this.b).getBytes()));
        try {
            httpPost.setEntity(new StringEntity(str, "UTF8"));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.a.execute(httpPost).getEntity().getContent()));
            StringBuffer stringBuffer = new StringBuffer();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append(10);
                } else {
                    bufferedReader.close();
                    return stringBuffer.toString();
                }
            }
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException();
        }
    }

    /* access modifiers changed from: package-private */
    public final HttpPost a() {
        return new HttpPost(this.c);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.core.c.f.a(org.apache.http.client.methods.HttpPost, java.lang.Object):org.json.JSONArray
     arg types: [org.apache.http.client.methods.HttpPost, org.json.JSONArray]
     candidates:
      com.scoreloop.client.android.core.c.f.a(org.apache.http.client.methods.HttpPost, java.lang.String):java.lang.String
      com.scoreloop.client.android.core.c.f.a(org.apache.http.client.methods.HttpPost, org.json.JSONArray):org.json.JSONArray
      com.scoreloop.client.android.core.c.f.a(org.apache.http.client.methods.HttpPost, byte[]):byte[]
      com.scoreloop.client.android.core.c.f.a(org.apache.http.client.methods.HttpPost, java.lang.Object):org.json.JSONArray */
    /* access modifiers changed from: package-private */
    public final JSONArray a(HttpPost httpPost, JSONArray jSONArray) {
        String jSONArray2;
        try {
            jSONArray2 = jSONArray.toString(4);
        } catch (JSONException e) {
            jSONArray2 = jSONArray.toString();
        }
        c(jSONArray2);
        return a(httpPost, (Object) jSONArray);
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.b = str;
    }

    /* access modifiers changed from: package-private */
    public final byte[] a(HttpPost httpPost, byte[] bArr) {
        if (this.d == null || this.b == null) {
            throw new IllegalStateException("Try setting password and username first");
        }
        httpPost.setHeader("Content-Type", b());
        httpPost.setHeader("Accept", b());
        httpPost.setHeader("X-Scoreloop-SDK-Version", "1");
        httpPost.setHeader("Authorization", "Basic " + c.a((this.d + ":" + this.b).getBytes()));
        httpPost.setEntity(new ByteArrayEntity(bArr));
        HttpEntity entity = this.a.execute(httpPost).getEntity();
        byte[] bArr2 = new byte[((int) entity.getContentLength())];
        BufferedInputStream bufferedInputStream = new BufferedInputStream(entity.getContent());
        int length = bArr2.length;
        int i = 0;
        while (length > 0) {
            int read = bufferedInputStream.read(bArr2, i, length);
            if (read == -1) {
                throw new IllegalStateException("Premature EOF");
            }
            length -= read;
            i += read;
        }
        bufferedInputStream.close();
        return bArr2;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return "application/json";
    }

    /* access modifiers changed from: package-private */
    public final void b(String str) {
        this.d = str;
    }
}
