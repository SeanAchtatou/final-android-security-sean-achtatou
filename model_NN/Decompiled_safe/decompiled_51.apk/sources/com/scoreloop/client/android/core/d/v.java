package com.scoreloop.client.android.core.d;

import android.os.Build;
import com.scoreloop.client.android.core.c.m;
import com.scoreloop.client.android.core.c.n;
import com.scoreloop.client.android.core.c.p;
import java.nio.channels.IllegalSelectorException;
import org.json.JSONException;
import org.json.JSONObject;

final class v extends m {
    private final com.scoreloop.client.android.core.b.m a;
    private final h b;

    public v(p pVar, com.scoreloop.client.android.core.b.m mVar, h hVar) {
        super(pVar);
        this.a = mVar;
        this.b = hVar;
    }

    public final String a() {
        return "/service/device";
    }

    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            switch (ac.a[this.b.ordinal()]) {
                case 1:
                    jSONObject.put("uuid", this.a.b());
                    jSONObject.put("system", Build.MODEL);
                    break;
                case 2:
                    jSONObject.put("device", this.a.c());
                    break;
                case 3:
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("uuid", this.a.b());
                    jSONObject2.put("id", this.a.a());
                    jSONObject2.put("system", Build.MODEL);
                    jSONObject2.put("state", "freed");
                    jSONObject.put("device", jSONObject2);
                    break;
                case 4:
                    jSONObject.put("device", this.a.c());
                default:
                    throw new IllegalSelectorException();
            }
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException("Invalid device data", e);
        }
    }

    public final n c() {
        switch (ac.a[this.b.ordinal()]) {
            case 1:
                return n.GET;
            case 2:
                return n.POST;
            case 3:
            case 4:
                return n.PUT;
            default:
                throw new IllegalSelectorException();
        }
    }

    public final h n() {
        return this.b;
    }
}
