package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.b.h;
import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.b.n;
import com.scoreloop.client.android.core.b.o;
import com.scoreloop.client.android.core.b.t;
import com.scoreloop.client.android.core.c.m;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

public final class r extends a {
    private o b;
    private Integer c;
    private o d;
    private s e;
    private int f;
    private List g;
    /* access modifiers changed from: private */
    public n h;
    private t i;

    private r(k kVar, o oVar) {
        super(kVar, oVar);
        this.g = new ArrayList();
        this.i = t.b();
        this.c = 0;
        this.d = new o(0, 25);
        this.b = new o(0, 0);
    }

    public r(o oVar) {
        this(k.a(), oVar);
    }

    private static List a(JSONArray jSONArray, int i2) {
        ArrayList arrayList = new ArrayList();
        for (int i3 = 0; i3 < i2; i3++) {
            arrayList.add(new n(jSONArray.getJSONObject(i3).getJSONObject("score")));
        }
        return arrayList;
    }

    static /* synthetic */ void a(r rVar, Integer num) {
        if (num != null) {
            int a = rVar.d.a() / 2;
            rVar.d = new o(num.intValue() - 1 < a ? 0 : (num.intValue() - 1) - a, rVar.d.a());
        }
        rVar.n();
    }

    private void n() {
        o oVar = this.b;
        oVar.a(this.d.b());
        oVar.c();
        aa aaVar = new aa(c(), a(), this.i, d().f(), this.c, Integer.valueOf(this.d.a() + 1), Integer.valueOf(oVar.b()));
        g();
        a(aaVar);
    }

    public final void a(h hVar) {
        if (this.e == null) {
            this.e = new s(d(), new t(this));
        }
        s sVar = this.e;
        sVar.a(this.i);
        sVar.a(hVar, this.c.intValue());
        this.h = null;
    }

    public final void a(o oVar) {
        this.d = oVar;
    }

    public final void a(Integer num) {
        this.c = num;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(m mVar, com.scoreloop.client.android.core.c.o oVar) {
        if (oVar.d() != 200) {
            throw new Exception("Request failed, returned status: " + oVar.d());
        }
        JSONArray jSONArray = oVar.c().getJSONArray("scores");
        o oVar2 = this.d;
        this.f = jSONArray.length();
        int min = Math.min(this.f, this.d.a());
        this.g.clear();
        this.g = a(jSONArray, min);
        if (this.h != null && !this.g.contains(this.h)) {
            this.g.add((this.h.c().intValue() - this.d.b()) - 1, this.h);
            this.g.remove(this.h.c().intValue() >= this.d.b() + this.g.size() ? 0 : this.g.size() - 1);
        }
        int b2 = this.d.b() + 1;
        for (n b3 : this.g) {
            b3.b(Integer.valueOf(b2));
            b2++;
        }
        this.b = new o(oVar2.b(), this.g.size());
        return true;
    }

    /* access modifiers changed from: package-private */
    public final boolean f() {
        return true;
    }

    public final o h() {
        return this.b;
    }

    public final o i() {
        return this.d;
    }

    public final List j() {
        return this.g;
    }

    public final boolean k() {
        return this.f > this.d.a();
    }

    public final boolean l() {
        return this.b.b() > 0 && this.d.a() > 0;
    }

    public final void m() {
        this.h = null;
        n();
    }
}
