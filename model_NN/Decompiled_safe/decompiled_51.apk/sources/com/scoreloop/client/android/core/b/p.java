package com.scoreloop.client.android.core.b;

import org.json.JSONException;
import org.json.JSONObject;

public class p extends c {
    public final String a() {
        return "com.facebook.v1";
    }

    /* access modifiers changed from: protected */
    public final void c(h hVar, JSONObject jSONObject) {
        if (hVar == null || jSONObject == null) {
            throw new IllegalArgumentException();
        } else if (hVar.j() != null) {
            JSONObject jSONObject2 = new JSONObject();
            try {
                try {
                    jSONObject2.put("uid", Long.valueOf(hVar.j().getLong("uid")));
                    try {
                        jSONObject.put("facebook", jSONObject2);
                    } catch (JSONException e) {
                        throw new IllegalStateException(e);
                    }
                } catch (JSONException e2) {
                    throw new IllegalStateException(e2);
                }
            } catch (JSONException e3) {
                throw new IllegalStateException(e3);
            }
        }
    }
}
