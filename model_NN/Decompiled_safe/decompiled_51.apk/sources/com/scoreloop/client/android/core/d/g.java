package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.b.d;
import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.b.q;
import com.scoreloop.client.android.core.c.n;
import com.scoreloop.client.android.core.c.p;

final class g extends ad {
    public g(p pVar, k kVar, q qVar, d dVar) {
        super(pVar, kVar, qVar, dVar);
    }

    public final String a() {
        return String.format("/service/games/%s/challenges/%s", this.b.a(), this.a.d());
    }

    public final n c() {
        return n.PUT;
    }
}
