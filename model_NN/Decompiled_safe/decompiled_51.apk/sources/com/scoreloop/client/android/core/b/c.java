package com.scoreloop.client.android.core.b;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class c {
    private static List b;
    private static List c;
    private JSONObject a;

    static {
        ArrayList arrayList = new ArrayList();
        c = arrayList;
        arrayList.add(p.class);
        c.add(b.class);
        c.add(r.class);
        ArrayList arrayList2 = new ArrayList();
        for (Class newInstance : c) {
            try {
                arrayList2.add(newInstance.newInstance());
            } catch (IllegalAccessException e) {
                throw new IllegalStateException(e);
            } catch (InstantiationException e2) {
                throw new IllegalStateException(e2);
            }
        }
        b = arrayList2;
    }

    public static void a(h hVar, JSONObject jSONObject) {
        for (c c2 : b) {
            c2.c(hVar, jSONObject);
        }
    }

    private String b() {
        return a().split("\\.")[1];
    }

    public static void b(h hVar, JSONObject jSONObject) {
        boolean z;
        for (c cVar : b) {
            if (hVar == null || jSONObject == null) {
                throw new IllegalArgumentException();
            }
            String b2 = cVar.b();
            if (!jSONObject.has(b2)) {
                z = false;
            } else {
                Object opt = jSONObject.opt(b2);
                z = opt == null ? false : JSONObject.NULL.equals(opt) ? false : !(opt instanceof String) || !"".equalsIgnoreCase((String) opt);
            }
            if (z) {
                try {
                    hVar.a(jSONObject.getJSONObject(cVar.b()), cVar.b());
                } catch (JSONException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }

    public abstract String a();

    /* access modifiers changed from: protected */
    public void c(h hVar, JSONObject jSONObject) {
        if (hVar.d(b())) {
            this.a = hVar.e(b());
        }
        if (this.a != null) {
            try {
                jSONObject.put(b(), this.a);
            } catch (JSONException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
