package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.b.h;
import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.b.t;
import com.scoreloop.client.android.core.c.m;
import com.scoreloop.client.android.core.c.o;
import org.json.JSONArray;

public final class s extends a {
    private com.scoreloop.client.android.core.b.s b;
    private t c;

    public s(k kVar, o oVar) {
        super(kVar, oVar);
    }

    public final void a(h hVar, int i) {
        if (hVar == null) {
            throw new IllegalArgumentException("user paramter cannot be null");
        }
        m mVar = new m(c(), a(), this.c, hVar, Integer.valueOf(i));
        g();
        a(mVar);
    }

    public final void a(t tVar) {
        this.c = tVar;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(m mVar, o oVar) {
        if (oVar.d() != 200) {
            throw new Exception("Request failed");
        }
        JSONArray b2 = oVar.b();
        this.b = new com.scoreloop.client.android.core.b.s();
        this.b.a(b2.getJSONObject(0).getJSONObject("ranking"));
        return true;
    }

    /* access modifiers changed from: package-private */
    public final boolean f() {
        return true;
    }

    public final com.scoreloop.client.android.core.b.s h() {
        return this.b;
    }
}
