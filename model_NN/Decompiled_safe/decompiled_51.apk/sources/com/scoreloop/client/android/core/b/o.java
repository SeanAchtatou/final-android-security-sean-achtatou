package com.scoreloop.client.android.core.b;

public final class o {
    private int a;
    private int b;

    public o(int i, int i2) {
        if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("length & location must be >= 0");
        }
        this.b = i;
        this.a = i2;
    }

    public final int a() {
        return this.a;
    }

    public final void a(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("location must be >= 0");
        }
        this.b = i;
    }

    public final int b() {
        return this.b;
    }

    public final void c() {
        this.a = 0;
    }

    /* access modifiers changed from: package-private */
    public final int d() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final int e() {
        return this.b + this.a;
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof o)) {
            return super.equals(obj);
        }
        o oVar = (o) obj;
        return this.b == oVar.b && e() == oVar.e();
    }

    public final int hashCode() {
        return new Integer(this.b).hashCode() ^ new Integer(e()).hashCode();
    }

    public final String toString() {
        return " [" + this.b + ", " + e() + "] ";
    }
}
