package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.b.g;
import com.scoreloop.client.android.core.b.k;

final class e implements o {
    private static /* synthetic */ boolean a = (!l.class.desiredAssertionStatus());
    private /* synthetic */ l b;

    /* synthetic */ e(l lVar) {
        this(lVar, (byte) 0);
    }

    private e(l lVar, byte b2) {
        this.b = lVar;
    }

    public final void a(a aVar) {
        k d = this.b.d();
        if (!a && d.h() != g.AUTHENTICATING) {
            throw new AssertionError();
        } else if (d.c().a() != null) {
            d.f().a(d.c().a());
            ae unused = this.b.b = null;
        } else {
            throw new IllegalStateException();
        }
    }

    public final void a(Exception exc) {
        k d = this.b.d();
        if (a || d.h() == g.AUTHENTICATING) {
            d.a(g.FAILED);
            this.b.a(exc);
            ae unused = this.b.b = null;
            return;
        }
        throw new AssertionError();
    }
}
