package com.scoreloop.client.android.core.b;

import org.json.JSONObject;

public final class i {
    private Double a;
    private Integer b;
    private Integer c;
    private /* synthetic */ h d;

    public i(h hVar) {
        this.d = hVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(JSONObject jSONObject) {
        if (jSONObject.has("winning_probability")) {
            this.a = Double.valueOf(jSONObject.optDouble("winning_probability"));
        }
        if (jSONObject.has("challenges_lost")) {
            this.c = Integer.valueOf(jSONObject.optInt("challenges_lost"));
        }
        if (jSONObject.has("challenges_won")) {
            this.b = Integer.valueOf(jSONObject.optInt("challenges_won"));
        }
    }
}
