package com.scoreloop.client.android.core.b;

public enum g {
    AUTHENTICATED,
    AUTHENTICATING,
    DEVICE_KNOWN,
    DEVICE_UNKNOWN,
    DEVICE_VERIFIED,
    FAILED,
    INITIAL,
    READY
}
