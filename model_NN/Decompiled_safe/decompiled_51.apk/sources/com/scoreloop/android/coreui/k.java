package com.scoreloop.android.coreui;

import android.content.Context;
import com.scoreloop.client.android.core.b.j;
import com.scoreloop.client.android.core.b.n;
import com.scoreloop.client.android.core.b.o;
import com.scoreloop.client.android.core.d.p;

public abstract class k {
    static j a;

    public static void a() {
        if (a != null) {
            a.a(new o(0, 2));
            return;
        }
        throw new IllegalStateException("client object is null. has ScoreloopManager.init() been called?");
    }

    public static void a(int i, int i2, com.scoreloop.client.android.core.d.o oVar) {
        n nVar = new n(Double.valueOf((double) i), com.scoreloop.client.android.core.b.k.a().f());
        nVar.a(Integer.valueOf(i2));
        new p(oVar).a(nVar);
    }

    public static void a(Context context, String str, String str2) {
        if (a == null) {
            a = new j(context, str, str2);
        }
    }
}
