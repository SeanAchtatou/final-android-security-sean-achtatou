package com.scoreloop.android.coreui;

import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.ProgressBar;
import com.google.ads.R;

abstract class j extends ActivityGroup {
    j() {
    }

    private Dialog a(int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(i).setCancelable(true);
        AlertDialog create = builder.create();
        create.getWindow().requestFeature(1);
        create.setCanceledOnTouchOutside(true);
        return create;
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        ((ProgressBar) findViewById(R.id.progress_indicator)).setVisibility(z ? 0 : 4);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 1:
                return a((int) R.string.sl_error_message_not_on_highscore_list);
            case 2:
            case 4:
            case 5:
            case 6:
            case 7:
            case 9:
            default:
                return null;
            case 3:
                return a((int) R.string.sl_error_message_network);
            case 8:
                return a((int) R.string.sl_error_message_email_already_taken);
            case 10:
                return a((int) R.string.sl_error_message_invalid_email_format);
            case 11:
                return a((int) R.string.sl_error_message_name_already_taken);
        }
    }
}
