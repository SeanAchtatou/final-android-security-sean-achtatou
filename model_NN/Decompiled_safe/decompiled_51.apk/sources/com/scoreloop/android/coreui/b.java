package com.scoreloop.android.coreui;

import android.view.View;
import com.scoreloop.client.android.core.b.h;
import com.scoreloop.client.android.core.b.k;

final class b implements View.OnClickListener {
    private /* synthetic */ ProfileActivity a;

    b(ProfileActivity profileActivity) {
        this.a = profileActivity;
    }

    public final void onClick(View view) {
        this.a.a();
        String trim = this.a.b.getText().toString().trim();
        String trim2 = this.a.a.getText().toString().trim();
        this.a.b.setText(trim);
        this.a.a.setText(trim2);
        h f = k.a().f();
        f.c(trim);
        f.b(trim2);
        this.a.b(true);
        this.a.a(true);
        this.a.g.h();
    }
}
