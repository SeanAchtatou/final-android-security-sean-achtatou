package com.scoreloop.android.coreui;

import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.d.a;
import com.scoreloop.client.android.core.d.j;

final class e implements j {
    private /* synthetic */ HighscoresActivity a;

    /* synthetic */ e(HighscoresActivity highscoresActivity) {
        this(highscoresActivity, (byte) 0);
    }

    private e(HighscoresActivity highscoresActivity, byte b) {
        this.a = highscoresActivity;
    }

    public final void a() {
        HighscoresActivity.b(this.a, 8);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, boolean):void
     arg types: [com.scoreloop.android.coreui.HighscoresActivity, int]
     candidates:
      com.scoreloop.android.coreui.HighscoresActivity.a(android.view.View, boolean):void
      com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, int):void
      com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, com.scoreloop.android.coreui.i):void
      com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, boolean):void */
    public final void a(a aVar) {
        this.a.h.setText(k.a().f().f());
        this.a.a(false);
        this.a.b(false);
        this.a.b();
    }

    public final void a(Exception exc) {
        HighscoresActivity.b(this.a, 3);
    }

    public final void b() {
        HighscoresActivity.b(this.a, 10);
    }

    public final void c() {
        HighscoresActivity.b(this.a, 11);
    }
}
