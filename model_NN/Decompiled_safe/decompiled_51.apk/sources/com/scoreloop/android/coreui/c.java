package com.scoreloop.android.coreui;

import com.scoreloop.client.android.core.d.a;
import com.scoreloop.client.android.core.d.j;

final class c implements j {
    private /* synthetic */ ProfileActivity a;

    /* synthetic */ c(ProfileActivity profileActivity) {
        this(profileActivity, (byte) 0);
    }

    private c(ProfileActivity profileActivity, byte b) {
        this.a = profileActivity;
    }

    public final void a() {
        ProfileActivity.a(this.a, 8, true);
    }

    public final void a(a aVar) {
        this.a.a(false);
        this.a.b(false);
        this.a.b();
    }

    public final void a(Exception exc) {
        ProfileActivity.a(this.a, 3, false);
    }

    public final void b() {
        ProfileActivity.a(this.a, 10, true);
    }

    public final void c() {
        ProfileActivity.a(this.a, 11, true);
    }
}
