package com.scoreloop.android.coreui;

import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import com.google.ads.R;

final class g implements AdapterView.OnItemSelectedListener {
    private /* synthetic */ HighscoresActivity a;

    g(HighscoresActivity highscoresActivity) {
        this.a = highscoresActivity;
    }

    public final void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        this.a.f.a(Integer.valueOf(i));
        ((TextView) this.a.d.findViewById(R.id.rank)).setText("");
        this.a.c(false);
    }

    public final void onNothingSelected(AdapterView adapterView) {
    }
}
