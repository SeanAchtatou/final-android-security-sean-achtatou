package com.google.ads;

public class AdSize {
    public static final AdSize a = new AdSize(320, 50, "320x50_mb");
    public static final AdSize b = new AdSize(300, 250, "300x250_as");
    public static final AdSize c = new AdSize(468, 60, "468x60_as");
    public static final AdSize d = new AdSize(728, 90, "728x90_as");
    private int e;
    private int f;
    private String g;

    private AdSize(int i, int i2, String str) {
        this.e = i;
        this.f = i2;
        this.g = str;
    }

    public final int a() {
        return this.e;
    }

    public final int b() {
        return this.f;
    }

    public String toString() {
        return this.g;
    }
}
