package com.a.a;

import android.os.Environment;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;

public abstract class c {
    private static String a;
    private static String b;
    /* access modifiers changed from: private */
    public static int c;
    /* access modifiers changed from: private */
    public static String d;
    private static String e;
    private static String f;
    private static String g;

    public static String a(int i) {
        return String.valueOf(g) + Integer.toString(i) + ".p";
    }

    public static void a() {
        a = new String("");
        b = new String("");
        d = new String("");
        f = new String("");
        e = new String("");
        g = new String("");
        g = String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + File.separator + ".droidga" + File.separator;
        Log.v("GameAd", "createPath(): mGameAdPath=" + g);
        File file = new File(g);
        if (!file.exists() && !file.mkdirs()) {
            Log.v("GameAd", "dir.mkdirs() ....error!");
        }
        k();
        b = "market://search?q=pub:\"Droid Studio\"";
        if (!h(1002) && !h(1002)) {
            f = String.valueOf(f) + Integer.toString(1002) + ",";
        }
        new a().start();
        Log.v("GameAd", "init(): lastShowGameId=" + c + ",clickId=" + e);
    }

    public static String b() {
        return b;
    }

    private static String b(String str) {
        return str.replace("\r\n", "");
    }

    private static int c(String str) {
        try {
            return Integer.parseInt(str);
        } catch (Exception e2) {
            return -1;
        }
    }

    public static void c() {
        int i;
        if (!(c == -1 || (i = c) == -1 || i == 0 || g(i))) {
            e = String.valueOf(e) + Integer.toString(i) + ",";
        }
        l();
    }

    public static String d() {
        return d;
    }

    /* access modifiers changed from: private */
    public static String d(int i) {
        String str = String.valueOf(Integer.toString(i)) + "#";
        int length = str.length() + a.indexOf(str);
        int indexOf = a.indexOf("\n", length);
        if (indexOf <= 0) {
            indexOf = a.length();
        }
        String substring = a.substring(length, indexOf);
        Log.v("GameAd", "getGameUrlById(),gameId=" + i + ",url=" + substring);
        return substring;
    }

    public static int e() {
        return c;
    }

    private static boolean e(int i) {
        try {
            return new File(a(i)).isFile();
        } catch (Exception e2) {
            return false;
        }
    }

    public static boolean f() {
        if (c == -1) {
            return false;
        }
        if (c == 0) {
            return false;
        }
        if (d.equals("")) {
            Log.v("GameAd", "checkShowGameAd():getShowGameUrl() = false");
            return false;
        } else if (!e(c)) {
            return false;
        } else {
            if (g(c)) {
                Log.v("GameAd", "checkShowGameAd():mLastShowGameId =" + c);
                return false;
            }
            Log.v("GameAd", "checkShowGameAd() OK!!, mLastShowGameId =" + c + "clickId=" + e);
            return true;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00b8 A[SYNTHETIC, Splitter:B:42:0x00b8] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00c6 A[SYNTHETIC, Splitter:B:49:0x00c6] */
    /* JADX WARNING: Removed duplicated region for block: B:65:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:30:0x00a2=Splitter:B:30:0x00a2, B:39:0x00b3=Splitter:B:39:0x00b3} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean f(int r9) {
        /*
            r8 = -1
            r7 = 0
            if (r9 != r8) goto L_0x0006
            r0 = r7
        L_0x0005:
            return r0
        L_0x0006:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = java.lang.Integer.toString(r9)
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.<init>(r1)
            java.lang.String r1 = ".jpg"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "http://www.gamecenter.me/wbc_game_ad/droidstudio/"
            r1.<init>(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.a.a.b r1 = new com.a.a.b
            r1.<init>()
            java.io.InputStream r0 = r1.a(r0)
            if (r0 != 0) goto L_0x0039
            r0 = r7
            goto L_0x0005
        L_0x0039:
            r1 = 0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = com.a.a.c.g
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r2.<init>(r3)
            java.lang.String r3 = "_ad.p"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r3 = 1
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ FileNotFoundException -> 0x00db, Exception -> 0x00b2 }
            r4.<init>()     // Catch:{ FileNotFoundException -> 0x00db, Exception -> 0x00b2 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00db, Exception -> 0x00b2 }
            r4.<init>(r2)     // Catch:{ FileNotFoundException -> 0x00db, Exception -> 0x00b2 }
            r1 = 8192(0x2000, float:1.14794E-41)
            byte[] r1 = new byte[r1]     // Catch:{ FileNotFoundException -> 0x00a0, Exception -> 0x00d8, all -> 0x00d5 }
        L_0x005e:
            int r5 = r0.read(r1)     // Catch:{ FileNotFoundException -> 0x00a0, Exception -> 0x00d8, all -> 0x00d5 }
            if (r5 != r8) goto L_0x009b
            r4.flush()     // Catch:{ FileNotFoundException -> 0x00a0, Exception -> 0x00d8, all -> 0x00d5 }
            java.lang.String r0 = "GameAd"
            java.lang.String r1 = "downloadImgFile()....ok"
            android.util.Log.v(r0, r1)     // Catch:{ FileNotFoundException -> 0x00a0, Exception -> 0x00d8, all -> 0x00d5 }
            r4.close()     // Catch:{ IOException -> 0x00cf }
            r0 = r3
        L_0x0072:
            if (r0 == 0) goto L_0x0005
            java.lang.String r1 = a(r9)
            java.io.File r3 = new java.io.File
            r3.<init>(r1)
            boolean r4 = r3.isFile()
            if (r4 == 0) goto L_0x0086
            r3.delete()
        L_0x0086:
            java.io.File r3 = new java.io.File
            r3.<init>(r2)
            boolean r2 = r3.isFile()
            if (r2 == 0) goto L_0x0005
            java.io.File r2 = new java.io.File
            r2.<init>(r1)
            r3.renameTo(r2)
            goto L_0x0005
        L_0x009b:
            r6 = 0
            r4.write(r1, r6, r5)     // Catch:{ FileNotFoundException -> 0x00a0, Exception -> 0x00d8, all -> 0x00d5 }
            goto L_0x005e
        L_0x00a0:
            r0 = move-exception
            r1 = r4
        L_0x00a2:
            r0.printStackTrace()     // Catch:{ all -> 0x00c3 }
            if (r1 == 0) goto L_0x00dd
            r1.close()     // Catch:{ IOException -> 0x00ac }
            r0 = r7
            goto L_0x0072
        L_0x00ac:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r7
            goto L_0x0072
        L_0x00b2:
            r0 = move-exception
        L_0x00b3:
            r0.printStackTrace()     // Catch:{ all -> 0x00c3 }
            if (r1 == 0) goto L_0x00dd
            r1.close()     // Catch:{ IOException -> 0x00bd }
            r0 = r7
            goto L_0x0072
        L_0x00bd:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r7
            goto L_0x0072
        L_0x00c3:
            r0 = move-exception
        L_0x00c4:
            if (r1 == 0) goto L_0x00c9
            r1.close()     // Catch:{ IOException -> 0x00ca }
        L_0x00c9:
            throw r0
        L_0x00ca:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00c9
        L_0x00cf:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r3
            goto L_0x0072
        L_0x00d5:
            r0 = move-exception
            r1 = r4
            goto L_0x00c4
        L_0x00d8:
            r0 = move-exception
            r1 = r4
            goto L_0x00b3
        L_0x00db:
            r0 = move-exception
            goto L_0x00a2
        L_0x00dd:
            r0 = r7
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.c.f(int):boolean");
    }

    /* access modifiers changed from: private */
    public static boolean g(int i) {
        if (i == -1) {
            return false;
        }
        return e.indexOf(new StringBuilder(String.valueOf(Integer.toString(i))).append(",").toString()) >= 0;
    }

    private static boolean h(int i) {
        if (i == -1 || i == 0) {
            return false;
        }
        return f.indexOf(new StringBuilder(String.valueOf(Integer.toString(i))).append(",").toString()) >= 0;
    }

    static /* synthetic */ void i() {
        int i;
        int indexOf = a.indexOf("\n");
        if (indexOf > 0) {
            String substring = a.substring(0, indexOf);
            Log.v("GameAd", "getNextGameId():line=" + substring);
            int i2 = 0;
            while (true) {
                int indexOf2 = substring.indexOf(",", i2);
                if (indexOf2 > 0) {
                    String substring2 = substring.substring(i2, indexOf2);
                    Log.v("GameAd", "getNextGameId(): gameId=" + substring2);
                    if (!substring2.equals("")) {
                        int c2 = c(substring2);
                        if (!h(c2) && !g(c2)) {
                            i = c2;
                            break;
                        }
                        i2 = indexOf2 + 1;
                    } else {
                        i = -1;
                        break;
                    }
                } else {
                    i = -1;
                    break;
                }
            }
        } else {
            i = -1;
        }
        Log.v("GameAd", "work():gameId=" + i);
        if (i == -1) {
            c = -1;
            e = "";
            l();
            return;
        }
        String d2 = d(i);
        if (!d2.equals("")) {
            c = i;
            d = d2;
            if (e(i)) {
                l();
            } else if (f(i)) {
                l();
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean j() {
        InputStream a2 = new b().a("http://www.gamecenter.me/wbc_game_ad/droidstudio/game_list.txt");
        if (a2 == null) {
            Log.v("GameAd", "work(): in = null");
            return false;
        }
        try {
            byte[] bArr = new byte[512];
            while (true) {
                int read = a2.read(bArr);
                if (read == -1) {
                    a = b(a);
                    Log.v("GameAd", "work():GameAdXml=" + a);
                    b = d(0);
                    return true;
                }
                a = String.valueOf(a) + new String(bArr).substring(0, read);
            }
        } catch (Exception e2) {
            Log.v("GameAd", "work(): exception:" + e2.getMessage());
            return false;
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void k() {
        /*
            r0 = -1
            com.a.a.c.c = r0
            java.lang.String r0 = ""
            com.a.a.c.d = r0
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            java.lang.String r2 = com.a.a.c.g     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            java.lang.String r2 = "gad2"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            java.io.FileReader r2 = new java.io.FileReader     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            r0 = 0
        L_0x002c:
            java.lang.String r2 = r1.readLine()     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            if (r2 != 0) goto L_0x0058
        L_0x0032:
            r1.close()     // Catch:{ Exception -> 0x0075, all -> 0x008e }
        L_0x0035:
            java.lang.String r0 = "GameAd"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "showId="
            r1.<init>(r2)
            int r2 = com.a.a.c.c
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ",clickId="
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = com.a.a.c.e
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.v(r0, r1)
            return
        L_0x0058:
            java.lang.String r2 = b(r2)     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            if (r0 != 0) goto L_0x0067
            int r2 = c(r2)     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            com.a.a.c.c = r2     // Catch:{ Exception -> 0x0075, all -> 0x008e }
        L_0x0064:
            int r0 = r0 + 1
            goto L_0x002c
        L_0x0067:
            r3 = 1
            if (r0 != r3) goto L_0x0077
            java.lang.String r3 = "\n"
            java.lang.String r4 = ""
            java.lang.String r2 = r2.replace(r3, r4)     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            com.a.a.c.e = r2     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            goto L_0x0064
        L_0x0075:
            r0 = move-exception
            goto L_0x0035
        L_0x0077:
            java.lang.String r0 = "\n"
            java.lang.String r3 = ""
            java.lang.String r0 = r2.replace(r0, r3)     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            com.a.a.c.f = r0     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            java.lang.String r2 = "market"
            int r0 = r0.indexOf(r2)     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            if (r0 < 0) goto L_0x0032
            java.lang.String r0 = ""
            com.a.a.c.f = r0     // Catch:{ Exception -> 0x0075, all -> 0x008e }
            goto L_0x0032
        L_0x008e:
            r0 = move-exception
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.c.k():void");
    }

    private static void l() {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(String.valueOf(g) + "gad2")));
            bufferedWriter.write(String.valueOf(Integer.toString(c)) + "\n");
            bufferedWriter.write(String.valueOf(e.replace("\n", "")) + "\n");
            bufferedWriter.write(String.valueOf(f.replace("\n", "")) + "\n");
            bufferedWriter.close();
        } catch (Exception e2) {
        }
    }
}
