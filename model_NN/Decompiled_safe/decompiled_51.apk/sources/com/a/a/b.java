package com.a.a;

import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public final class b {
    private HttpClient a;
    private HttpParams b;

    public final InputStream a(String str) {
        try {
            HttpGet httpGet = new HttpGet(str);
            if (this.a == null) {
                this.b = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(this.b, 20000);
                HttpConnectionParams.setSoTimeout(this.b, 20000);
                HttpConnectionParams.setSocketBufferSize(this.b, 8192);
                HttpClientParams.setRedirecting(this.b, true);
                HttpProtocolParams.setUserAgent(this.b, "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16");
                this.a = new DefaultHttpClient(this.b);
            }
            HttpResponse execute = this.a.execute(httpGet);
            if (execute.getStatusLine().getStatusCode() == 200) {
                return execute.getEntity().getContent();
            }
            Log.v("DownSongList", "doGet(): error");
            Log.v("DownSongList", "doGet(): null");
            return null;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }
}
