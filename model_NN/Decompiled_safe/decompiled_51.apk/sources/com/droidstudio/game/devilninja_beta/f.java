package com.droidstudio.game.devilninja_beta;

import android.widget.CompoundButton;
import com.droidstudio.game.devilninja_beta.a.g;

final class f implements CompoundButton.OnCheckedChangeListener {
    private /* synthetic */ Prefs a;

    f(Prefs prefs) {
        this.a = prefs;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        g.a(1);
        if (z) {
            this.a.a.edit().putInt("isFast", 1).commit();
        } else {
            this.a.a.edit().putInt("isFast", 0).commit();
        }
    }
}
