package com.droidstudio.game.devilninja_beta.a;

import android.graphics.Rect;

public class c {
    protected int a;
    protected int b;
    protected int c;
    protected int d;
    private int e = 0;
    private int f = 0;
    private int g = 0;
    private int h = 0;

    private int a() {
        return this.a + this.e;
    }

    private int b() {
        return f() - this.f;
    }

    private int c() {
        return this.b + this.g;
    }

    private int d() {
        return h() - this.h;
    }

    public final void a(int i, int i2, int i3, int i4) {
        this.e = i;
        this.g = i2;
        this.f = i3;
        this.h = i4;
    }

    public final boolean a(c cVar) {
        return cVar.a() < b() && cVar.b() > a() && cVar.c() < d() && cVar.d() > c();
    }

    public final void b(int i) {
        this.c = i;
    }

    public final boolean b(int i, int i2, int i3, int i4) {
        return i < b() && i3 > a() && i2 < d() && i4 > c();
    }

    public final void c(int i) {
        this.d = i;
    }

    public void d(int i) {
        this.b = i;
    }

    public final int e() {
        return this.a;
    }

    public final void e(int i) {
        this.a += i;
    }

    public final int f() {
        return this.a + this.c;
    }

    public final void f(int i) {
        this.b += i;
    }

    public final int g() {
        return this.b;
    }

    public final int h() {
        return this.b + this.d;
    }

    public final int i() {
        return this.c;
    }

    public final Rect j() {
        return new Rect(this.a, this.b, f(), h());
    }

    public final Rect k() {
        Rect j = j();
        int width = (j.left + ((j.width() - 80) / 2)) - 6;
        int height = (((j.height() - 80) / 2) + j.top) - 8;
        return new Rect(width, height, width + 80, height + 80);
    }
}
