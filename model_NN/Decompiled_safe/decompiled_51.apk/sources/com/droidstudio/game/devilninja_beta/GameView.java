package com.droidstudio.game.devilninja_beta;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.droidstudio.game.devilninja_beta.a.e;
import com.droidstudio.game.devilninja_beta.a.h;
import com.droidstudio.game.devilninja_beta.a.j;
import com.google.ads.R;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {
    /* access modifiers changed from: private */
    public Bitmap A;
    /* access modifiers changed from: private */
    public Bitmap B;
    /* access modifiers changed from: private */
    public Bitmap C;
    /* access modifiers changed from: private */
    public Bitmap D;
    /* access modifiers changed from: private */
    public Bitmap E;
    /* access modifiers changed from: private */
    public Bitmap F;
    /* access modifiers changed from: private */
    public Bitmap G;
    /* access modifiers changed from: private */
    public Bitmap H;
    /* access modifiers changed from: private */
    public Bitmap I;
    /* access modifiers changed from: private */
    public Bitmap J;
    /* access modifiers changed from: private */
    public Bitmap K;
    /* access modifiers changed from: private */
    public Bitmap L;
    /* access modifiers changed from: private */
    public Bitmap M;
    /* access modifiers changed from: private */
    public Bitmap N;
    /* access modifiers changed from: private */
    public j O;
    /* access modifiers changed from: private */
    public Paint P;
    /* access modifiers changed from: private */
    public Paint Q;
    /* access modifiers changed from: private */
    public Typeface R;
    /* access modifiers changed from: private */
    public e S;
    private boolean T = false;
    private SharedPreferences a;
    /* access modifiers changed from: private */
    public MainActivity b;
    private c c;
    private Context d;
    private Handler e;
    /* access modifiers changed from: private */
    public Bitmap f;
    /* access modifiers changed from: private */
    public Bitmap g;
    /* access modifiers changed from: private */
    public Bitmap h;
    /* access modifiers changed from: private */
    public Bitmap i;
    /* access modifiers changed from: private */
    public Bitmap j;
    /* access modifiers changed from: private */
    public Bitmap k;
    /* access modifiers changed from: private */
    public Bitmap l;
    /* access modifiers changed from: private */
    public Bitmap m;
    /* access modifiers changed from: private */
    public Bitmap n;
    /* access modifiers changed from: private */
    public Bitmap o;
    /* access modifiers changed from: private */
    public Bitmap p;
    /* access modifiers changed from: private */
    public Bitmap q;
    /* access modifiers changed from: private */
    public Bitmap r;
    /* access modifiers changed from: private */
    public Bitmap s;
    /* access modifiers changed from: private */
    public Bitmap t;
    /* access modifiers changed from: private */
    public Bitmap u;
    /* access modifiers changed from: private */
    public Bitmap v;
    /* access modifiers changed from: private */
    public Bitmap w;
    /* access modifiers changed from: private */
    public Bitmap x;
    /* access modifiers changed from: private */
    public Bitmap y;
    /* access modifiers changed from: private */
    public Bitmap z;

    public GameView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = context;
        getHolder().addCallback(this);
        this.e = new d(this);
        System.gc();
        this.f = a((int) R.drawable.bg1, 1138, 320);
        this.g = a((int) R.drawable.bg2, 700, 320);
        this.h = a((int) R.drawable.road_left, 90, 185);
        this.i = a((int) R.drawable.road_mid, 250, 185);
        this.j = a((int) R.drawable.road_right, 90, 185);
        this.y = a((int) R.drawable.road_element, 342, 140);
        this.k = a((int) R.drawable.role_run, 660, 60);
        this.o = a((int) R.drawable.enemy_run, 675, 75);
        this.p = a((int) R.drawable.enemy_drop, 225, 75);
        this.q = a((int) R.drawable.enemy_dead, 325, 75);
        this.r = a((int) R.drawable.dragon_fly, 680, 70);
        this.s = a((int) R.drawable.enemy_dead, 325, 75);
        this.l = a((int) R.drawable.role_jump, 480, 80);
        this.m = a((int) R.drawable.role_rotate, 500, 100);
        this.n = a((int) R.drawable.role_falldown, 210, 70);
        this.z = a((int) R.drawable.score_frame, h.f, h.e);
        this.A = a((int) R.drawable.numbers_small, h.i, h.h);
        this.C = a((int) R.drawable.pause_tips, 400, 200);
        this.B = a((int) R.drawable.blood, h.m, h.l);
        this.v = a((int) R.drawable.btn_jump, 120, 60);
        this.w = a((int) R.drawable.btn_fire, 120, 60);
        this.t = a((int) R.drawable.btn_pause, 30, 30);
        this.u = a((int) R.drawable.btn_speed, 60, 30);
        this.x = a((int) R.drawable.btn_img_power, 390, 130);
        this.D = a((int) R.drawable.roadblock, 150, 60);
        this.E = a((int) R.drawable.role_hurt, 360, 120);
        this.F = a((int) R.drawable.pause_info, 168, 63);
        this.G = a((int) R.drawable.food, 150, 30);
        this.H = a((int) R.drawable.broadsword, 400, 80);
        this.I = a((int) R.drawable.dart, 19, 25);
        this.J = a((int) R.drawable.weapon_counter, h.a, h.b);
        this.K = a((int) R.drawable.dart_power, 40, 40);
        this.L = a((int) R.drawable.power_line2, 1920, 180);
        this.M = a((int) R.drawable.power_fire, 320, 80);
        this.N = a((int) R.drawable.light, 110, 55);
        this.R = Typeface.createFromAsset(getContext().getAssets(), "fonts/b.ttf");
        this.P = new Paint(1);
        this.P.setColor(-256);
        this.P.setStyle(Paint.Style.FILL);
        this.P.setTextSize(16.0f);
        this.P.setTextAlign(Paint.Align.RIGHT);
        this.P.setTypeface(this.R);
        this.Q = new Paint(1);
        this.Q.setColor(-256);
        this.Q.setStyle(Paint.Style.FILL);
        this.Q.setTextSize(18.0f);
        this.Q.setTextAlign(Paint.Align.LEFT);
        this.a = this.d.getSharedPreferences("com.ningo.game.action.stick", 0);
        setFocusable(true);
    }

    private Bitmap a(int i2, int i3, int i4) {
        Bitmap decodeStream = BitmapFactory.decodeStream(this.d.getResources().openRawResource(i2));
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeStream, i3, i4, true);
        a(decodeStream);
        return createScaledBitmap;
    }

    private void a(int i2) {
        this.a.edit().putInt("isFast", i2).commit();
    }

    private static void a(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            bitmap.recycle();
        }
    }

    static /* synthetic */ void a(GameView gameView, int i2, int i3) {
        gameView.a(i3);
        Log.v("GameView", "doGameOver(), score=" + i2 + ", speed=" + i3);
        if (i2 > gameView.c() && gameView.a.getInt("highScore", 0) < i2) {
            gameView.a.edit().putInt("highScore", i2).commit();
        }
        Intent intent = new Intent();
        intent.setClass(gameView.b, GameoverActivity.class);
        intent.putExtra("curScore", i2);
        intent.putExtra("highScore", gameView.c());
        intent.putExtra("killEnemys", gameView.S.k());
        gameView.b.startActivity(intent);
    }

    private int c() {
        return this.a.getInt("highScore", 0);
    }

    public final int a(int i2, int i3) {
        return this.S.b(i2, i3);
    }

    public final void a() {
        this.S.f = 2;
    }

    public final void a(MainActivity mainActivity) {
        this.b = mainActivity;
    }

    public final void b() {
        this.S.b();
    }

    public final void b(int i2, int i3) {
        this.S.a(i2, i3);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        if (this.S == null) {
            this.O = new j(i3 + 1, i4);
            this.S = new e(this.O, this.a.getInt("isFast", 0));
        }
        this.c = new c(this, surfaceHolder, getContext(), this.e);
        this.c.a(true);
        this.c.start();
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        if (!(this.c == null || this.S == null)) {
            a(this.S.j());
        }
        boolean z2 = true;
        this.c.a(false);
        while (z2) {
            try {
                this.c.join();
                z2 = false;
            } catch (InterruptedException e2) {
                Log.d("", "Surface destroy failure:", e2);
            }
        }
        if (!this.b.a()) {
            Log.v("GameView", "freeImageRes()...ok");
            a(this.f);
            a(this.g);
            a(this.h);
            a(this.i);
            a(this.j);
            a(this.D);
            a(this.k);
            a(this.l);
            a(this.m);
            a(this.n);
            a(this.o);
            a(this.p);
            a(this.q);
            a(this.s);
            a(this.r);
            a(this.t);
            a(this.u);
            a(this.v);
            a(this.w);
            a(this.x);
            a(this.y);
            a(this.z);
            a(this.F);
            a(this.A);
            a(this.B);
            a(this.C);
            a(this.E);
            a(this.G);
            a(this.H);
            a(this.I);
            a(this.J);
            a(this.K);
            a(this.L);
            a(this.M);
            a(this.N);
        }
    }
}
