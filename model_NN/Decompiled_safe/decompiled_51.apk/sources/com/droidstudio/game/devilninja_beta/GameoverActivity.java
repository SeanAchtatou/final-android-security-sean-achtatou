package com.droidstudio.game.devilninja_beta;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.droidstudio.game.devilninja_beta.a.g;
import com.google.ads.R;
import com.scoreloop.android.coreui.k;

public class GameoverActivity extends Activity implements View.OnClickListener {
    private ProgressDialog a = null;
    private int b;
    private int c;
    private int d;
    /* access modifiers changed from: private */
    public int e;

    static /* synthetic */ void a(GameoverActivity gameoverActivity) {
        if (gameoverActivity.a != null && gameoverActivity.a.isShowing()) {
            gameoverActivity.a.dismiss();
            gameoverActivity.a = null;
        }
    }

    public final void a() {
        if (this.a != null && this.a.isShowing()) {
            this.a.dismiss();
        }
        this.a = ProgressDialog.show(this, "", "Submit Score to Server...", true);
        this.a.setCancelable(true);
        k.a(this.c, 0, new j(this));
        k.a(this.d, 1, new j(this));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit /*2131099667*/:
                this.e = 0;
                a();
                break;
            case R.id.btn_tryagain /*2131099668*/:
                startActivity(new Intent(this, TipsActivity.class));
                finish();
                break;
            case R.id.btn_quit /*2131099669*/:
                finish();
                break;
            default:
                return;
        }
        g.a(1);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.gameover);
        ((Button) findViewById(R.id.btn_submit)).setOnClickListener(this);
        ((Button) findViewById(R.id.btn_tryagain)).setOnClickListener(this);
        ((Button) findViewById(R.id.btn_quit)).setOnClickListener(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.b = extras.getInt("curScore", 0);
            this.c = extras.getInt("highScore", 0);
            this.d = extras.getInt("killEnemys", 0);
        }
        Typeface createFromAsset = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/b.ttf");
        TextView textView = (TextView) findViewById(R.id.tv_cur_score);
        textView.setTypeface(createFromAsset);
        textView.setText(String.valueOf(Integer.toString(this.b)) + "m");
        TextView textView2 = (TextView) findViewById(R.id.tv_high_score);
        textView2.setTypeface(createFromAsset);
        textView2.setText(String.valueOf(Integer.toString(this.c)) + "m");
        TextView textView3 = (TextView) findViewById(R.id.tv_kill_enemys);
        textView3.setTypeface(createFromAsset);
        textView3.setText(Integer.toString(this.d));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
