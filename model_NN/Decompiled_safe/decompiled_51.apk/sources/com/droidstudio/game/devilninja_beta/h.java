package com.droidstudio.game.devilninja_beta;

import android.widget.CompoundButton;
import com.droidstudio.game.devilninja_beta.a.g;

final class h implements CompoundButton.OnCheckedChangeListener {
    private /* synthetic */ Prefs a;

    h(Prefs prefs) {
        this.a = prefs;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        g.a(1);
        if (z) {
            this.a.a.edit().putBoolean("isSound", true).commit();
        } else {
            this.a.a.edit().putBoolean("isSound", false).commit();
        }
    }
}
