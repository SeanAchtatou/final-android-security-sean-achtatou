package com.droidstudio.game.devilninja_beta;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import com.droidstudio.game.devilninja_beta.a.a;
import com.droidstudio.game.devilninja_beta.a.f;
import com.droidstudio.game.devilninja_beta.a.h;
import com.droidstudio.game.devilninja_beta.a.l;
import com.droidstudio.game.devilninja_beta.a.m;
import java.util.Iterator;
import java.util.LinkedList;

final class c extends Thread {
    private SurfaceHolder a;
    private Context b;
    private Handler c;
    private boolean d = true;
    private long e;
    private /* synthetic */ GameView f;

    public c(GameView gameView, SurfaceHolder surfaceHolder, Context context, Handler handler) {
        this.f = gameView;
        this.a = surfaceHolder;
        this.b = context;
        this.c = handler;
    }

    private static int a(int i) {
        switch (i) {
            case 0:
                return 10;
            case 1:
                return 20;
            case 2:
                return 40;
            case 3:
                return 70;
            case 4:
            default:
                return 225;
        }
    }

    private void a(Canvas canvas) {
        Bitmap j;
        Iterator it = this.f.S.d.iterator();
        while (it.hasNext()) {
            f fVar = (f) it.next();
            if (fVar.a() <= this.f.O.c && fVar.b() >= -20) {
                int h = fVar.h();
                if (h != 0) {
                    int i = (h - 1) * 57;
                    canvas.drawBitmap(this.f.y, new Rect(i, 0, i + 57, 140), new Rect(fVar.i(), fVar.j(), fVar.k(), fVar.l()), (Paint) null);
                }
                switch (fVar.g()) {
                    case 1:
                        j = this.f.h;
                        break;
                    case 2:
                        j = this.f.i;
                        break;
                    case 3:
                        j = this.f.j;
                        break;
                    default:
                        j = this.f.i;
                        Log.v("GameView", "drawRoad()----default");
                        break;
                }
                canvas.drawBitmap(j, (float) fVar.a(), (float) fVar.c(), (Paint) null);
                int m = fVar.m();
                if (m != 0) {
                    int i2 = (m - 1) * 50;
                    canvas.drawBitmap(this.f.D, new Rect(i2, 0, i2 + 50, 60), new Rect(fVar.n(), fVar.p(), fVar.o(), fVar.q()), (Paint) null);
                }
            }
        }
    }

    private void a(Canvas canvas, int i) {
        int[] iArr = {i % 10, (i / 10) % 10, (i / 100) % 10, (i / 1000) % 10, (i / 10000) % 10, (i / 100000) % 10};
        int j = this.f.S.d().j();
        int k = this.f.S.d().k();
        Rect rect = new Rect();
        Rect rect2 = new Rect();
        boolean z = false;
        for (int length = iArr.length - 1; length >= 0; length--) {
            int i2 = iArr[length];
            if (i2 != 0 || z) {
                int i3 = h.j * i2;
                rect.set(i3, 0, h.j + i3, h.k);
                int i4 = j + 5 + ((5 - length) * 15);
                rect2.set(i4, k + 5, i4 + 15, k + 24);
                canvas.drawBitmap(this.f.A, rect, rect2, (Paint) null);
                z = true;
            }
        }
    }

    private static void a(Canvas canvas, Bitmap bitmap, int i, int i2) {
        Paint paint = new Paint();
        paint.setColor(-16777216);
        for (int i3 = 0; i3 < 5; i3++) {
            paint.setAlpha(a(i3));
            canvas.drawBitmap(bitmap, (float) (i - ((3 - i3) * 8)), (float) i2, paint);
        }
    }

    private static void a(Canvas canvas, Bitmap bitmap, Rect rect, Rect rect2) {
        Paint paint = new Paint();
        paint.setColor(-16777216);
        Rect rect3 = new Rect();
        for (int i = 0; i < 5; i++) {
            paint.setAlpha(a(i));
            int i2 = rect2.left - ((3 - i) * 8);
            rect3.set(i2, rect2.top, rect2.width() + i2, rect2.bottom);
            canvas.drawBitmap(bitmap, rect, rect3, paint);
        }
    }

    private void b(Canvas canvas) {
        int i;
        int i2;
        Bitmap bitmap;
        Iterator it = this.f.S.e.iterator();
        while (it.hasNext()) {
            a aVar = (a) it.next();
            int b2 = aVar.b();
            if (b2 != 0 && aVar.e() <= this.f.O.c) {
                int c2 = aVar.c() - 1;
                if (b2 == 1 || b2 == 4) {
                    i = c2 * 75;
                    bitmap = this.f.o;
                    i2 = 75;
                } else if (b2 == 2) {
                    i = c2 * 75;
                    bitmap = this.f.p;
                    i2 = 75;
                } else {
                    i = c2 * 65;
                    i2 = 65;
                    bitmap = this.f.q;
                }
                canvas.drawBitmap(bitmap, new Rect(i, 0, i2 + i, 75), new Rect(aVar.e(), aVar.g(), aVar.f(), aVar.h()), (Paint) null);
            }
        }
    }

    private void c(Canvas canvas) {
        Bitmap bitmap;
        int i;
        int i2;
        Iterator it = this.f.S.c().iterator();
        while (it.hasNext()) {
            l lVar = (l) it.next();
            int i3 = lVar.e;
            if (i3 != 0 && lVar.e() <= this.f.O.c && lVar.f() >= -30) {
                int i4 = lVar.f - 1;
                if (i3 == 1 || i3 == 3) {
                    i2 = 85;
                    i = 70;
                    bitmap = this.f.r;
                } else {
                    i2 = 65;
                    i = 75;
                    bitmap = this.f.s;
                }
                int i5 = i;
                Bitmap bitmap2 = bitmap;
                int i6 = i2;
                int i7 = i4 * i6;
                canvas.drawBitmap(bitmap2, new Rect(i7, 0, i6 + i7, i5), new Rect(lVar.e(), lVar.g(), lVar.f(), lVar.h()), (Paint) null);
            }
        }
    }

    private void d(Canvas canvas) {
        this.f.B.getWidth();
        for (int i = 0; i < this.f.S.g(); i++) {
            canvas.drawBitmap(this.f.B, (float) (((this.f.O.c - h.g) - (h.m * (i + 1))) - (i * 6)), (float) h.n, (Paint) null);
        }
    }

    private void e(Canvas canvas) {
        m h = this.f.S.h();
        if (h.e() == 1) {
            int f2 = h.f();
            Log.v("GameView", "drawWeapon(): shape=" + f2);
            if (f2 != 0) {
                Log.v("GameView", "drawWeapon()....");
                int i = (f2 - 1) * 100;
                Rect rect = new Rect(i, 0, i + 100, 80);
                int h2 = h.h();
                int i2 = h.i();
                canvas.drawBitmap(this.f.H, rect, new Rect(h2, i2, h2 + 100, i2 + 80), (Paint) null);
            }
        }
        LinkedList d2 = h.d();
        if (!d2.isEmpty()) {
            Iterator it = d2.iterator();
            while (it.hasNext()) {
                Point point = (Point) it.next();
                a(canvas, this.f.I, point.x, point.y);
            }
        }
        Iterator it2 = h.b().iterator();
        while (it2.hasNext()) {
            Point point2 = (Point) it2.next();
            a(canvas, this.f.K, point2.x, point2.y);
        }
    }

    public final void a(boolean z) {
        this.d = z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:126:0x0792 A[SYNTHETIC, Splitter:B:126:0x0792] */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x0797 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x0000 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x05a5 A[Catch:{ Exception -> 0x05f2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x05b8 A[Catch:{ Exception -> 0x05f2 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r13 = this;
        L_0x0000:
            boolean r0 = r13.d
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            r0 = 0
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x05f2 }
            r13.e = r1     // Catch:{ Exception -> 0x05f2 }
            com.droidstudio.game.devilninja_beta.GameView r1 = r13.f     // Catch:{ Exception -> 0x079f, all -> 0x078c }
            com.droidstudio.game.devilninja_beta.a.e r1 = r1.S     // Catch:{ Exception -> 0x079f, all -> 0x078c }
            int r1 = r1.f     // Catch:{ Exception -> 0x079f, all -> 0x078c }
            r2 = 1
            if (r1 != r2) goto L_0x0020
            com.droidstudio.game.devilninja_beta.GameView r1 = r13.f     // Catch:{ Exception -> 0x079f, all -> 0x078c }
            com.droidstudio.game.devilninja_beta.a.e r1 = r1.S     // Catch:{ Exception -> 0x079f, all -> 0x078c }
            r1.a()     // Catch:{ Exception -> 0x079f, all -> 0x078c }
        L_0x0020:
            android.view.SurfaceHolder r1 = r13.a     // Catch:{ Exception -> 0x079f, all -> 0x078c }
            r2 = 0
            android.graphics.Canvas r0 = r1.lockCanvas(r2)     // Catch:{ Exception -> 0x079f, all -> 0x078c }
            android.view.SurfaceHolder r1 = r13.a     // Catch:{ Exception -> 0x0647, all -> 0x0798 }
            monitor-enter(r1)     // Catch:{ Exception -> 0x0647, all -> 0x0798 }
            com.droidstudio.game.devilninja_beta.GameView r2 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r2 = r2.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.h r2 = r2.d()     // Catch:{ all -> 0x0644 }
            float r3 = r2.H()     // Catch:{ all -> 0x0644 }
            boolean r2 = r2.I()     // Catch:{ all -> 0x0644 }
            if (r2 == 0) goto L_0x006f
            r0.save()     // Catch:{ all -> 0x0644 }
            r4 = 0
            r5 = 0
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.j r6 = r6.O     // Catch:{ all -> 0x0644 }
            int r6 = r6.c     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r7 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.j r7 = r7.O     // Catch:{ all -> 0x0644 }
            int r7 = r7.d     // Catch:{ all -> 0x0644 }
            r0.clipRect(r4, r5, r6, r7)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.j r4 = r4.O     // Catch:{ all -> 0x0644 }
            int r4 = r4.c     // Catch:{ all -> 0x0644 }
            int r4 = r4 / 2
            float r4 = (float) r4     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r5 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.j r5 = r5.O     // Catch:{ all -> 0x0644 }
            int r5 = r5.d     // Catch:{ all -> 0x0644 }
            int r5 = r5 / 2
            float r5 = (float) r5     // Catch:{ all -> 0x0644 }
            r0.rotate(r3, r4, r5)     // Catch:{ all -> 0x0644 }
        L_0x006f:
            com.droidstudio.game.devilninja_beta.GameView r3 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r3 = r3.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.k r3 = r3.a     // Catch:{ all -> 0x0644 }
            float r3 = r3.a()     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r4 = r4.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.k r4 = r4.a     // Catch:{ all -> 0x0644 }
            float r4 = r4.b()     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r5 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r5 = r5.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.k r5 = r5.a     // Catch:{ all -> 0x0644 }
            int r5 = r5.c()     // Catch:{ all -> 0x0644 }
            android.content.Context r6 = r13.b     // Catch:{ all -> 0x0644 }
            r6.getResources()     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r6 = r6.f     // Catch:{ all -> 0x0644 }
            int r4 = (r4 > r3 ? 1 : (r4 == r3 ? 0 : -1))
            if (r4 < 0) goto L_0x05fc
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r4 = r4.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.k r4 = r4.a     // Catch:{ all -> 0x0644 }
            float r4 = r4.d()     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r7 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            int r8 = (int) r3     // Catch:{ all -> 0x0644 }
            r9 = 0
            int r3 = (int) r3     // Catch:{ all -> 0x0644 }
            int r10 = (int) r4     // Catch:{ all -> 0x0644 }
            int r3 = r3 + r10
            r7.<init>(r8, r9, r3, r5)     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r8 = 0
            r9 = 0
            int r4 = (int) r4     // Catch:{ all -> 0x0644 }
            r3.<init>(r8, r9, r4, r5)     // Catch:{ all -> 0x0644 }
            r4 = 0
            r0.drawBitmap(r6, r7, r3, r4)     // Catch:{ all -> 0x0644 }
        L_0x00c4:
            com.droidstudio.game.devilninja_beta.GameView r3 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r3 = r3.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.n r3 = r3.l()     // Catch:{ all -> 0x0644 }
            int r4 = r3.b()     // Catch:{ all -> 0x0644 }
            if (r4 == 0) goto L_0x00f4
            int r4 = r3.b()     // Catch:{ all -> 0x0644 }
            r5 = 1
            int r4 = r4 - r5
            int r4 = r4 * 55
            android.graphics.Rect r5 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r6 = 0
            int r7 = r4 + 55
            r8 = 55
            r5.<init>(r4, r6, r7, r8)     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r3 = r3.j()     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r4 = r4.N     // Catch:{ all -> 0x0644 }
            r6 = 0
            r0.drawBitmap(r4, r5, r3, r6)     // Catch:{ all -> 0x0644 }
        L_0x00f4:
            com.droidstudio.game.devilninja_beta.GameView r3 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r3 = r3.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.k r3 = r3.b     // Catch:{ all -> 0x0644 }
            float r3 = r3.a()     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r4 = r4.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.k r4 = r4.b     // Catch:{ all -> 0x0644 }
            float r4 = r4.b()     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r5 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r5 = r5.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.k r5 = r5.b     // Catch:{ all -> 0x0644 }
            int r5 = r5.c()     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r6 = r6.g     // Catch:{ all -> 0x0644 }
            int r4 = (r4 > r3 ? 1 : (r4 == r3 ? 0 : -1))
            if (r4 < 0) goto L_0x065b
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r4 = r4.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.k r4 = r4.b     // Catch:{ all -> 0x0644 }
            float r4 = r4.d()     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r7 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            int r8 = (int) r3     // Catch:{ all -> 0x0644 }
            r9 = 0
            int r3 = (int) r3     // Catch:{ all -> 0x0644 }
            int r10 = (int) r4     // Catch:{ all -> 0x0644 }
            int r3 = r3 + r10
            r7.<init>(r8, r9, r3, r5)     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r8 = 0
            r9 = 0
            int r4 = (int) r4     // Catch:{ all -> 0x0644 }
            r3.<init>(r8, r9, r4, r5)     // Catch:{ all -> 0x0644 }
            r4 = 0
            r0.drawBitmap(r6, r7, r3, r4)     // Catch:{ all -> 0x0644 }
        L_0x0144:
            r13.a(r0)     // Catch:{ all -> 0x0644 }
            r3 = 0
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r4 = r4.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.i r4 = r4.c     // Catch:{ all -> 0x0644 }
            int r5 = r4.b()     // Catch:{ all -> 0x0644 }
            int r6 = r4.l()     // Catch:{ all -> 0x0644 }
            if (r6 != 0) goto L_0x015e
            int r6 = r4.d()     // Catch:{ all -> 0x0644 }
        L_0x015e:
            r7 = 9
            if (r6 < r7) goto L_0x06a3
            r7 = 11
            if (r6 > r7) goto L_0x06a3
            com.droidstudio.game.devilninja_beta.GameView r5 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r5 = r5.k     // Catch:{ all -> 0x0644 }
            r7 = 1
            int r6 = r6 - r7
            int r6 = r6 * 60
            android.graphics.Rect r7 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r8 = 0
            int r9 = r6 + 60
            r10 = 60
            r7.<init>(r6, r8, r9, r10)     // Catch:{ all -> 0x0644 }
            r6 = r7
            r12 = r3
            r3 = r5
            r5 = r12
        L_0x017e:
            android.graphics.Rect r4 = r4.j()     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r7 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r7 = r7.S     // Catch:{ all -> 0x0644 }
            int r7 = r7.j()     // Catch:{ all -> 0x0644 }
            r8 = 1
            if (r7 != r8) goto L_0x0190
            r5 = 1
        L_0x0190:
            if (r5 != 0) goto L_0x0712
            r5 = 0
            r0.drawBitmap(r3, r6, r4, r5)     // Catch:{ all -> 0x0644 }
        L_0x0196:
            r13.b(r0)     // Catch:{ all -> 0x0644 }
            r13.c(r0)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r3 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r3 = r3.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.d r3 = r3.e()     // Catch:{ all -> 0x0644 }
            int r4 = r3.a     // Catch:{ all -> 0x0644 }
            if (r4 == 0) goto L_0x01d9
            int r4 = r3.a     // Catch:{ all -> 0x0644 }
            r5 = 1
            int r4 = r4 - r5
            int r4 = r4 * 30
            android.graphics.Rect r5 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r6 = 0
            int r7 = r4 + 30
            r8 = 30
            r5.<init>(r4, r6, r7, r8)     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r4 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            int r6 = r3.b()     // Catch:{ all -> 0x0644 }
            int r7 = r3.c()     // Catch:{ all -> 0x0644 }
            int r8 = r3.d()     // Catch:{ all -> 0x0644 }
            int r3 = r3.e()     // Catch:{ all -> 0x0644 }
            r4.<init>(r6, r7, r8, r3)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r3 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r3 = r3.G     // Catch:{ all -> 0x0644 }
            r6 = 0
            r0.drawBitmap(r3, r5, r4, r6)     // Catch:{ all -> 0x0644 }
        L_0x01d9:
            r13.e(r0)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r3 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r3 = r3.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.b r3 = r3.i()     // Catch:{ all -> 0x0644 }
            boolean r4 = r3.b()     // Catch:{ all -> 0x0644 }
            if (r4 == 0) goto L_0x020c
            int r4 = r3.d()     // Catch:{ all -> 0x0644 }
            r5 = 1
            int r4 = r4 - r5
            int r4 = r4 * 480
            android.graphics.Rect r5 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r6 = 0
            int r7 = r4 + 480
            r8 = 180(0xb4, float:2.52E-43)
            r5.<init>(r4, r6, r7, r8)     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r3 = r3.j()     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r4 = r4.L     // Catch:{ all -> 0x0644 }
            r6 = 0
            r0.drawBitmap(r4, r5, r3, r6)     // Catch:{ all -> 0x0644 }
        L_0x020c:
            com.droidstudio.game.devilninja_beta.GameView r3 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r3 = r3.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.h r3 = r3.d()     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r4 = r4.t     // Catch:{ all -> 0x0644 }
            r5 = 3
            int r5 = r3.a(r5)     // Catch:{ all -> 0x0644 }
            float r5 = (float) r5     // Catch:{ all -> 0x0644 }
            r6 = 3
            int r6 = r3.b(r6)     // Catch:{ all -> 0x0644 }
            float r6 = (float) r6     // Catch:{ all -> 0x0644 }
            r7 = 0
            r0.drawBitmap(r4, r5, r6, r7)     // Catch:{ all -> 0x0644 }
            r4 = 5
            int r4 = r3.g(r4)     // Catch:{ all -> 0x0644 }
            r5 = 1
            int r4 = r4 - r5
            int r4 = r4 * 30
            android.graphics.Rect r5 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r6 = 0
            int r7 = r4 + 30
            r8 = 30
            r5.<init>(r4, r6, r7, r8)     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r4 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r6 = 5
            int r6 = r3.a(r6)     // Catch:{ all -> 0x0644 }
            r7 = 5
            int r7 = r3.b(r7)     // Catch:{ all -> 0x0644 }
            r8 = 5
            int r8 = r3.c(r8)     // Catch:{ all -> 0x0644 }
            r9 = 5
            int r9 = r3.d(r9)     // Catch:{ all -> 0x0644 }
            r4.<init>(r6, r7, r8, r9)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r6 = r6.u     // Catch:{ all -> 0x0644 }
            r7 = 0
            r0.drawBitmap(r6, r5, r4, r7)     // Catch:{ all -> 0x0644 }
            r4 = 1
            int r4 = r3.g(r4)     // Catch:{ all -> 0x0644 }
            r5 = 1
            int r4 = r4 - r5
            int r4 = r4 * 60
            android.graphics.Rect r5 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r6 = 0
            int r7 = r4 + 60
            r8 = 60
            r5.<init>(r4, r6, r7, r8)     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r4 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r6 = 1
            int r6 = r3.a(r6)     // Catch:{ all -> 0x0644 }
            r7 = 1
            int r7 = r3.b(r7)     // Catch:{ all -> 0x0644 }
            r8 = 1
            int r8 = r3.c(r8)     // Catch:{ all -> 0x0644 }
            r9 = 1
            int r9 = r3.d(r9)     // Catch:{ all -> 0x0644 }
            r4.<init>(r6, r7, r8, r9)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r6 = r6.v     // Catch:{ all -> 0x0644 }
            r7 = 0
            r0.drawBitmap(r6, r5, r4, r7)     // Catch:{ all -> 0x0644 }
            r6 = 2
            int r6 = r3.g(r6)     // Catch:{ all -> 0x0644 }
            r7 = 1
            int r6 = r6 - r7
            int r6 = r6 * 60
            r7 = 0
            int r8 = r6 + 60
            r9 = 60
            r5.set(r6, r7, r8, r9)     // Catch:{ all -> 0x0644 }
            r6 = 2
            int r6 = r3.a(r6)     // Catch:{ all -> 0x0644 }
            r7 = 2
            int r7 = r3.b(r7)     // Catch:{ all -> 0x0644 }
            r8 = 2
            int r8 = r3.c(r8)     // Catch:{ all -> 0x0644 }
            r9 = 2
            int r9 = r3.d(r9)     // Catch:{ all -> 0x0644 }
            r4.set(r6, r7, r8, r9)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r6 = r6.w     // Catch:{ all -> 0x0644 }
            r7 = 0
            r0.drawBitmap(r6, r5, r4, r7)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r6 = r6.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.h r6 = r6.d()     // Catch:{ all -> 0x0644 }
            int r6 = r6.j()     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r7 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r7 = r7.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.h r7 = r7.d()     // Catch:{ all -> 0x0644 }
            int r7 = r7.k()     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r8 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r8 = r8.z     // Catch:{ all -> 0x0644 }
            float r6 = (float) r6     // Catch:{ all -> 0x0644 }
            float r7 = (float) r7     // Catch:{ all -> 0x0644 }
            r9 = 0
            r0.drawBitmap(r8, r6, r7, r9)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r6 = r6.S     // Catch:{ all -> 0x0644 }
            int r6 = r6.f()     // Catch:{ all -> 0x0644 }
            r13.a(r0, r6)     // Catch:{ all -> 0x0644 }
            r13.d(r0)     // Catch:{ all -> 0x0644 }
            if (r3 == 0) goto L_0x0324
            boolean r6 = r3.c()     // Catch:{ all -> 0x0644 }
            if (r6 == 0) goto L_0x0324
            r6 = 3
            int r6 = r3.a(r6)     // Catch:{ all -> 0x0644 }
            r7 = 4
            int r6 = r6 - r7
            r7 = 3
            int r7 = r3.b(r7)     // Catch:{ all -> 0x0644 }
            r8 = 4
            int r7 = r7 - r8
            com.droidstudio.game.devilninja_beta.GameView r8 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r8 = r8.F     // Catch:{ all -> 0x0644 }
            float r6 = (float) r6     // Catch:{ all -> 0x0644 }
            float r7 = (float) r7     // Catch:{ all -> 0x0644 }
            r9 = 0
            r0.drawBitmap(r8, r6, r7, r9)     // Catch:{ all -> 0x0644 }
        L_0x0324:
            int r6 = r3.l()     // Catch:{ all -> 0x0644 }
            if (r6 != 0) goto L_0x0717
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r6 = r6.P     // Catch:{ all -> 0x0644 }
            r7 = 1098907648(0x41800000, float:16.0)
            r6.setTextSize(r7)     // Catch:{ all -> 0x0644 }
        L_0x0335:
            boolean r6 = r3.q()     // Catch:{ all -> 0x0644 }
            if (r6 == 0) goto L_0x07a5
            int r4 = r3.t()     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r5 = new android.graphics.Paint     // Catch:{ all -> 0x0644 }
            r5.<init>()     // Catch:{ all -> 0x0644 }
            r6 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r5.setColor(r6)     // Catch:{ all -> 0x0644 }
            r5.setAlpha(r4)     // Catch:{ all -> 0x0644 }
            int r4 = r3.u()     // Catch:{ all -> 0x0644 }
            r6 = 1
            int r4 = r4 - r6
            int r4 = r4 * 30
            android.graphics.Rect r6 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r7 = 0
            int r8 = r4 + 30
            r9 = 30
            r6.<init>(r4, r7, r8, r9)     // Catch:{ all -> 0x0644 }
            android.graphics.Point r4 = r3.s()     // Catch:{ all -> 0x0644 }
            int r4 = r4.x     // Catch:{ all -> 0x0644 }
            android.graphics.Point r7 = r3.s()     // Catch:{ all -> 0x0644 }
            int r7 = r7.y     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r8 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            int r9 = r4 + 30
            int r10 = r7 + 30
            r8.<init>(r4, r7, r9, r10)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r4 = r4.G     // Catch:{ all -> 0x0644 }
            r0.drawBitmap(r4, r6, r8, r5)     // Catch:{ all -> 0x0644 }
            r4 = r6
            r5 = r8
        L_0x037e:
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r6 = r6.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.m r6 = r6.h()     // Catch:{ all -> 0x0644 }
            int r7 = com.droidstudio.game.devilninja_beta.a.h.c     // Catch:{ all -> 0x0644 }
            int r8 = com.droidstudio.game.devilninja_beta.a.h.d     // Catch:{ all -> 0x0644 }
            int r9 = r6.g()     // Catch:{ all -> 0x0644 }
            r10 = 1
            if (r9 != r10) goto L_0x0412
            r9 = 0
            int r10 = r7 * 2
            r4.set(r7, r9, r10, r8)     // Catch:{ all -> 0x0644 }
            android.graphics.Point r9 = r3.F()     // Catch:{ all -> 0x0644 }
            int r9 = r9.x     // Catch:{ all -> 0x0644 }
            android.graphics.Point r10 = r3.F()     // Catch:{ all -> 0x0644 }
            int r10 = r10.y     // Catch:{ all -> 0x0644 }
            int r7 = r7 + r9
            int r8 = r8 + r10
            r5.set(r9, r10, r7, r8)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r7 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r7 = r7.J     // Catch:{ all -> 0x0644 }
            r8 = 0
            r0.drawBitmap(r7, r4, r5, r8)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r4 = r4.Q     // Catch:{ all -> 0x0644 }
            android.graphics.Typeface r4 = r4.getTypeface()     // Catch:{ all -> 0x0644 }
            java.lang.String r5 = "x"
            com.droidstudio.game.devilninja_beta.GameView r7 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r7 = r7.Q     // Catch:{ all -> 0x0644 }
            r8 = 1099956224(0x41900000, float:18.0)
            r7.setTextSize(r8)     // Catch:{ all -> 0x0644 }
            int r7 = r9 + 29
            float r7 = (float) r7     // Catch:{ all -> 0x0644 }
            int r8 = r10 + 19
            float r8 = (float) r8     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r11 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r11 = r11.Q     // Catch:{ all -> 0x0644 }
            r0.drawText(r5, r7, r8, r11)     // Catch:{ all -> 0x0644 }
            int r5 = r6.a     // Catch:{ all -> 0x0644 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r6 = r6.Q     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r7 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Typeface r7 = r7.R     // Catch:{ all -> 0x0644 }
            r6.setTypeface(r7)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r6 = r6.Q     // Catch:{ all -> 0x0644 }
            r7 = 1101004800(0x41a00000, float:20.0)
            r6.setTextSize(r7)     // Catch:{ all -> 0x0644 }
            int r6 = r9 + 42
            float r6 = (float) r6     // Catch:{ all -> 0x0644 }
            int r7 = r10 + 19
            float r7 = (float) r7     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r8 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r8 = r8.Q     // Catch:{ all -> 0x0644 }
            r0.drawText(r5, r6, r7, r8)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r5 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r5 = r5.Q     // Catch:{ all -> 0x0644 }
            r5.setTypeface(r4)     // Catch:{ all -> 0x0644 }
        L_0x0412:
            int r4 = r3.z()     // Catch:{ all -> 0x0644 }
            boolean r5 = r3.A()     // Catch:{ all -> 0x0644 }
            if (r5 == 0) goto L_0x044b
            if (r4 == 0) goto L_0x044b
            r5 = 1
            int r4 = r4 - r5
            int r4 = r4 * 130
            android.graphics.Rect r5 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r6 = 0
            int r7 = r4 + 130
            r8 = 130(0x82, float:1.82E-43)
            r5.<init>(r4, r6, r7, r8)     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r4 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            int r6 = r3.B()     // Catch:{ all -> 0x0644 }
            int r7 = r3.C()     // Catch:{ all -> 0x0644 }
            int r8 = r3.D()     // Catch:{ all -> 0x0644 }
            int r9 = r3.E()     // Catch:{ all -> 0x0644 }
            r4.<init>(r6, r7, r8, r9)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r6 = r6.x     // Catch:{ all -> 0x0644 }
            r7 = 0
            r0.drawBitmap(r6, r5, r4, r7)     // Catch:{ all -> 0x0644 }
        L_0x044b:
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r4 = r4.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.i r4 = r4.c     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r5 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r5 = r5.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.h r5 = r5.d()     // Catch:{ all -> 0x0644 }
            int r5 = r5.v()     // Catch:{ all -> 0x0644 }
            if (r5 == 0) goto L_0x0486
            int r6 = r4.b()     // Catch:{ all -> 0x0644 }
            r7 = 5
            if (r6 == r7) goto L_0x0486
            r6 = 1
            int r5 = r5 - r6
            int r5 = r5 * 80
            android.graphics.Rect r6 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r7 = 0
            int r8 = r5 + 80
            r9 = 80
            r6.<init>(r5, r7, r8, r9)     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r4 = r4.k()     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r5 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r5 = r5.M     // Catch:{ all -> 0x0644 }
            r7 = 0
            r0.drawBitmap(r5, r6, r4, r7)     // Catch:{ all -> 0x0644 }
        L_0x0486:
            int r3 = r3.f()     // Catch:{ all -> 0x0644 }
            if (r3 == 0) goto L_0x04bb
            r4 = 1
            int r3 = r3 - r4
            int r3 = r3 * 180
            android.graphics.Rect r4 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r5 = 0
            int r6 = r3 + 180
            r7 = 120(0x78, float:1.68E-43)
            r4.<init>(r3, r5, r6, r7)     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r5 = 0
            r6 = 0
            com.droidstudio.game.devilninja_beta.GameView r7 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.j r7 = r7.O     // Catch:{ all -> 0x0644 }
            int r7 = r7.c     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r8 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.j r8 = r8.O     // Catch:{ all -> 0x0644 }
            int r8 = r8.d     // Catch:{ all -> 0x0644 }
            r3.<init>(r5, r6, r7, r8)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r5 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r5 = r5.E     // Catch:{ all -> 0x0644 }
            r6 = 0
            r0.drawBitmap(r5, r4, r3, r6)     // Catch:{ all -> 0x0644 }
        L_0x04bb:
            com.droidstudio.game.devilninja_beta.GameView r3 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r3 = r3.S     // Catch:{ all -> 0x0644 }
            int r3 = r3.f     // Catch:{ all -> 0x0644 }
            r4 = 2
            if (r3 != r4) goto L_0x0587
            android.graphics.Paint r3 = new android.graphics.Paint     // Catch:{ all -> 0x0644 }
            r3.<init>()     // Catch:{ all -> 0x0644 }
            r4 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r3.setColor(r4)     // Catch:{ all -> 0x0644 }
            r4 = 170(0xaa, float:2.38E-43)
            r3.setAlpha(r4)     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r4 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r5 = 0
            r6 = 0
            com.droidstudio.game.devilninja_beta.GameView r7 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.j r7 = r7.O     // Catch:{ all -> 0x0644 }
            int r7 = r7.c     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r8 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.j r8 = r8.O     // Catch:{ all -> 0x0644 }
            int r8 = r8.d     // Catch:{ all -> 0x0644 }
            r4.<init>(r5, r6, r7, r8)     // Catch:{ all -> 0x0644 }
            r0.drawRect(r4, r3)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r3 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.j r3 = r3.O     // Catch:{ all -> 0x0644 }
            int r3 = r3.c     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r4 = r4.C     // Catch:{ all -> 0x0644 }
            int r4 = r4.getWidth()     // Catch:{ all -> 0x0644 }
            int r3 = r3 - r4
            int r3 = r3 / 2
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r4 = r4.C     // Catch:{ all -> 0x0644 }
            float r5 = (float) r3     // Catch:{ all -> 0x0644 }
            r6 = 1101004800(0x41a00000, float:20.0)
            r7 = 0
            r0.drawBitmap(r4, r5, r6, r7)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r4 = r4.Q     // Catch:{ all -> 0x0644 }
            android.graphics.Typeface r4 = r4.getTypeface()     // Catch:{ all -> 0x0644 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r6 = r6.S     // Catch:{ all -> 0x0644 }
            int r6 = r6.f()     // Catch:{ all -> 0x0644 }
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ all -> 0x0644 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ all -> 0x0644 }
            r5.<init>(r6)     // Catch:{ all -> 0x0644 }
            java.lang.String r6 = "M"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0644 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r6 = r6.Q     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r7 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Typeface r7 = r7.R     // Catch:{ all -> 0x0644 }
            r6.setTypeface(r7)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r6 = r6.Q     // Catch:{ all -> 0x0644 }
            r7 = 1103101952(0x41c00000, float:24.0)
            r6.setTextSize(r7)     // Catch:{ all -> 0x0644 }
            int r3 = r3 + 205
            float r6 = (float) r3     // Catch:{ all -> 0x0644 }
            r7 = 1122369536(0x42e60000, float:115.0)
            com.droidstudio.game.devilninja_beta.GameView r8 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r8 = r8.Q     // Catch:{ all -> 0x0644 }
            r0.drawText(r5, r6, r7, r8)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r5 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r5 = r5.S     // Catch:{ all -> 0x0644 }
            int r5 = r5.k()     // Catch:{ all -> 0x0644 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ all -> 0x0644 }
            float r3 = (float) r3     // Catch:{ all -> 0x0644 }
            r6 = 1125515264(0x43160000, float:150.0)
            com.droidstudio.game.devilninja_beta.GameView r7 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r7 = r7.Q     // Catch:{ all -> 0x0644 }
            r0.drawText(r5, r3, r6, r7)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r3 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r3 = r3.Q     // Catch:{ all -> 0x0644 }
            r3.setTypeface(r4)     // Catch:{ all -> 0x0644 }
        L_0x0587:
            if (r2 == 0) goto L_0x058c
            r0.restore()     // Catch:{ all -> 0x0644 }
        L_0x058c:
            monitor-exit(r1)     // Catch:{ all -> 0x0644 }
            if (r0 == 0) goto L_0x0594
            android.view.SurfaceHolder r1 = r13.a     // Catch:{ Exception -> 0x05f2 }
            r1.unlockCanvasAndPost(r0)     // Catch:{ Exception -> 0x05f2 }
        L_0x0594:
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x05f2 }
            long r2 = r13.e     // Catch:{ Exception -> 0x05f2 }
            long r0 = r0 - r2
            r13.e = r0     // Catch:{ Exception -> 0x05f2 }
            long r0 = r13.e     // Catch:{ Exception -> 0x05f2 }
            r2 = 35
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x05ad
            r0 = 35
            long r2 = r13.e     // Catch:{ Exception -> 0x05f2 }
            long r0 = r0 - r2
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x05f2 }
        L_0x05ad:
            com.droidstudio.game.devilninja_beta.GameView r0 = r13.f     // Catch:{ Exception -> 0x05f2 }
            com.droidstudio.game.devilninja_beta.a.e r0 = r0.S     // Catch:{ Exception -> 0x05f2 }
            int r0 = r0.f     // Catch:{ Exception -> 0x05f2 }
            r1 = 3
            if (r0 != r1) goto L_0x0000
            r0 = 300(0x12c, double:1.48E-321)
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x05f2 }
            android.os.Message r0 = new android.os.Message     // Catch:{ Exception -> 0x05f2 }
            r0.<init>()     // Catch:{ Exception -> 0x05f2 }
            android.os.Bundle r1 = new android.os.Bundle     // Catch:{ Exception -> 0x05f2 }
            r1.<init>()     // Catch:{ Exception -> 0x05f2 }
            java.lang.String r2 = "GAME_OVER"
            com.droidstudio.game.devilninja_beta.GameView r3 = r13.f     // Catch:{ Exception -> 0x05f2 }
            com.droidstudio.game.devilninja_beta.a.e r3 = r3.S     // Catch:{ Exception -> 0x05f2 }
            int r3 = r3.f()     // Catch:{ Exception -> 0x05f2 }
            r1.putInt(r2, r3)     // Catch:{ Exception -> 0x05f2 }
            java.lang.String r2 = "GAME_PARAM_SPEED"
            com.droidstudio.game.devilninja_beta.GameView r3 = r13.f     // Catch:{ Exception -> 0x05f2 }
            com.droidstudio.game.devilninja_beta.a.e r3 = r3.S     // Catch:{ Exception -> 0x05f2 }
            int r3 = r3.j()     // Catch:{ Exception -> 0x05f2 }
            r1.putInt(r2, r3)     // Catch:{ Exception -> 0x05f2 }
            r0.setData(r1)     // Catch:{ Exception -> 0x05f2 }
            android.os.Handler r1 = r13.c     // Catch:{ Exception -> 0x05f2 }
            r1.sendMessage(r0)     // Catch:{ Exception -> 0x05f2 }
            r0 = 0
            r13.d = r0     // Catch:{ Exception -> 0x05f2 }
            goto L_0x0000
        L_0x05f2:
            r0 = move-exception
            java.lang.String r1 = ""
            java.lang.String r2 = "Error at 'run' method"
            android.util.Log.d(r1, r2, r0)
            goto L_0x0000
        L_0x05fc:
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r4 = r4.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.k r4 = r4.a     // Catch:{ all -> 0x0644 }
            float r4 = r4.d()     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r7 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            int r8 = (int) r3     // Catch:{ all -> 0x0644 }
            r9 = 0
            int r3 = (int) r3     // Catch:{ all -> 0x0644 }
            int r10 = (int) r4     // Catch:{ all -> 0x0644 }
            int r3 = r3 + r10
            r7.<init>(r8, r9, r3, r5)     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r8 = 0
            r9 = 0
            int r10 = (int) r4     // Catch:{ all -> 0x0644 }
            r3.<init>(r8, r9, r10, r5)     // Catch:{ all -> 0x0644 }
            r8 = 0
            r0.drawBitmap(r6, r7, r3, r8)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r3 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r3 = r3.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.k r3 = r3.a     // Catch:{ all -> 0x0644 }
            float r3 = r3.e()     // Catch:{ all -> 0x0644 }
            int r3 = (int) r3     // Catch:{ all -> 0x0644 }
            float r3 = (float) r3     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r7 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r8 = 0
            r9 = 0
            int r10 = (int) r3     // Catch:{ all -> 0x0644 }
            r7.<init>(r8, r9, r10, r5)     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r8 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            int r9 = (int) r4     // Catch:{ all -> 0x0644 }
            r10 = 0
            int r4 = (int) r4     // Catch:{ all -> 0x0644 }
            int r3 = (int) r3     // Catch:{ all -> 0x0644 }
            int r3 = r3 + r4
            r8.<init>(r9, r10, r3, r5)     // Catch:{ all -> 0x0644 }
            r3 = 0
            r0.drawBitmap(r6, r7, r8, r3)     // Catch:{ all -> 0x0644 }
            goto L_0x00c4
        L_0x0644:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0644 }
            throw r2     // Catch:{ Exception -> 0x0647, all -> 0x0798 }
        L_0x0647:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x064b:
            java.lang.String r2 = ""
            java.lang.String r3 = "Error at 'run' method"
            android.util.Log.d(r2, r3, r0)     // Catch:{ all -> 0x079d }
            if (r1 == 0) goto L_0x0594
            android.view.SurfaceHolder r0 = r13.a     // Catch:{ Exception -> 0x05f2 }
            r0.unlockCanvasAndPost(r1)     // Catch:{ Exception -> 0x05f2 }
            goto L_0x0594
        L_0x065b:
            com.droidstudio.game.devilninja_beta.GameView r4 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r4 = r4.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.k r4 = r4.b     // Catch:{ all -> 0x0644 }
            float r4 = r4.d()     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r7 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            int r8 = (int) r3     // Catch:{ all -> 0x0644 }
            r9 = 0
            int r3 = (int) r3     // Catch:{ all -> 0x0644 }
            int r10 = (int) r4     // Catch:{ all -> 0x0644 }
            int r3 = r3 + r10
            r7.<init>(r8, r9, r3, r5)     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r8 = 0
            r9 = 0
            int r10 = (int) r4     // Catch:{ all -> 0x0644 }
            r3.<init>(r8, r9, r10, r5)     // Catch:{ all -> 0x0644 }
            r8 = 0
            r0.drawBitmap(r6, r7, r3, r8)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r3 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r3 = r3.S     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.k r3 = r3.b     // Catch:{ all -> 0x0644 }
            float r3 = r3.e()     // Catch:{ all -> 0x0644 }
            int r3 = (int) r3     // Catch:{ all -> 0x0644 }
            float r3 = (float) r3     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r7 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r8 = 0
            r9 = 0
            int r10 = (int) r3     // Catch:{ all -> 0x0644 }
            r7.<init>(r8, r9, r10, r5)     // Catch:{ all -> 0x0644 }
            android.graphics.Rect r8 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            int r9 = (int) r4     // Catch:{ all -> 0x0644 }
            r10 = 0
            int r4 = (int) r4     // Catch:{ all -> 0x0644 }
            int r3 = (int) r3     // Catch:{ all -> 0x0644 }
            int r3 = r3 + r4
            r8.<init>(r9, r10, r3, r5)     // Catch:{ all -> 0x0644 }
            r3 = 0
            r0.drawBitmap(r6, r7, r8, r3)     // Catch:{ all -> 0x0644 }
            goto L_0x0144
        L_0x06a3:
            r7 = 1
            if (r5 != r7) goto L_0x06c0
            com.droidstudio.game.devilninja_beta.GameView r5 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r5 = r5.k     // Catch:{ all -> 0x0644 }
            r7 = 1
            int r6 = r6 - r7
            int r6 = r6 * 60
            android.graphics.Rect r7 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r8 = 0
            int r9 = r6 + 60
            r10 = 60
            r7.<init>(r6, r8, r9, r10)     // Catch:{ all -> 0x0644 }
            r6 = r7
            r12 = r3
            r3 = r5
            r5 = r12
            goto L_0x017e
        L_0x06c0:
            r7 = 5
            if (r5 != r7) goto L_0x06db
            com.droidstudio.game.devilninja_beta.GameView r3 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r3 = r3.m     // Catch:{ all -> 0x0644 }
            r5 = 1
            r7 = 1
            int r6 = r6 - r7
            int r6 = r6 * 100
            android.graphics.Rect r7 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r8 = 0
            int r9 = r6 + 100
            r10 = 100
            r7.<init>(r6, r8, r9, r10)     // Catch:{ all -> 0x0644 }
            r6 = r7
            goto L_0x017e
        L_0x06db:
            r7 = 4
            if (r5 != r7) goto L_0x06f8
            com.droidstudio.game.devilninja_beta.GameView r5 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r5 = r5.n     // Catch:{ all -> 0x0644 }
            r7 = 1
            int r6 = r6 - r7
            int r6 = r6 * 70
            android.graphics.Rect r7 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r8 = 0
            int r9 = r6 + 70
            r10 = 70
            r7.<init>(r6, r8, r9, r10)     // Catch:{ all -> 0x0644 }
            r6 = r7
            r12 = r3
            r3 = r5
            r5 = r12
            goto L_0x017e
        L_0x06f8:
            com.droidstudio.game.devilninja_beta.GameView r5 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Bitmap r5 = r5.l     // Catch:{ all -> 0x0644 }
            r7 = 1
            int r6 = r6 - r7
            int r6 = r6 * 80
            android.graphics.Rect r7 = new android.graphics.Rect     // Catch:{ all -> 0x0644 }
            r8 = 0
            int r9 = r6 + 80
            r10 = 80
            r7.<init>(r6, r8, r9, r10)     // Catch:{ all -> 0x0644 }
            r6 = r7
            r12 = r3
            r3 = r5
            r5 = r12
            goto L_0x017e
        L_0x0712:
            a(r0, r3, r6, r4)     // Catch:{ all -> 0x0644 }
            goto L_0x0196
        L_0x0717:
            com.droidstudio.game.devilninja_beta.GameView r7 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r7 = r7.P     // Catch:{ all -> 0x0644 }
            float r7 = r7.getTextSize()     // Catch:{ all -> 0x0644 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r9 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.e r9 = r9.S     // Catch:{ all -> 0x0644 }
            int r9 = r9.f()     // Catch:{ all -> 0x0644 }
            java.lang.String r9 = java.lang.Integer.toString(r9)     // Catch:{ all -> 0x0644 }
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ all -> 0x0644 }
            r8.<init>(r9)     // Catch:{ all -> 0x0644 }
            java.lang.String r9 = "m"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0644 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0644 }
            switch(r6) {
                case 1: goto L_0x0780;
                case 2: goto L_0x0784;
                case 3: goto L_0x0788;
                default: goto L_0x0745;
            }     // Catch:{ all -> 0x0644 }
        L_0x0745:
            r6 = r7
        L_0x0746:
            com.droidstudio.game.devilninja_beta.GameView r7 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r7 = r7.P     // Catch:{ all -> 0x0644 }
            r7.setTextSize(r6)     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r6 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r6 = r6.P     // Catch:{ all -> 0x0644 }
            android.graphics.Paint$FontMetrics r6 = r6.getFontMetrics()     // Catch:{ all -> 0x0644 }
            float r6 = r6.descent     // Catch:{ all -> 0x0644 }
            double r6 = (double) r6     // Catch:{ all -> 0x0644 }
            double r6 = java.lang.Math.ceil(r6)     // Catch:{ all -> 0x0644 }
            r9 = 4613937818241073152(0x4008000000000000, double:3.0)
            double r6 = r6 * r9
            r9 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r6 = r6 + r9
            int r6 = (int) r6     // Catch:{ all -> 0x0644 }
            int r6 = r6 + 30
            com.droidstudio.game.devilninja_beta.GameView r7 = r13.f     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.a.j r7 = r7.O     // Catch:{ all -> 0x0644 }
            int r7 = r7.c     // Catch:{ all -> 0x0644 }
            r9 = 5
            int r7 = r7 - r9
            float r7 = (float) r7     // Catch:{ all -> 0x0644 }
            float r6 = (float) r6     // Catch:{ all -> 0x0644 }
            com.droidstudio.game.devilninja_beta.GameView r9 = r13.f     // Catch:{ all -> 0x0644 }
            android.graphics.Paint r9 = r9.P     // Catch:{ all -> 0x0644 }
            r0.drawText(r8, r7, r6, r9)     // Catch:{ all -> 0x0644 }
            goto L_0x0335
        L_0x0780:
            r6 = 1065353216(0x3f800000, float:1.0)
            float r6 = r6 + r7
            goto L_0x0746
        L_0x0784:
            r6 = 1073741824(0x40000000, float:2.0)
            float r6 = r6 + r7
            goto L_0x0746
        L_0x0788:
            r6 = 1077936128(0x40400000, float:3.0)
            float r6 = r6 + r7
            goto L_0x0746
        L_0x078c:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x0790:
            if (r1 == 0) goto L_0x0797
            android.view.SurfaceHolder r2 = r13.a     // Catch:{ Exception -> 0x05f2 }
            r2.unlockCanvasAndPost(r1)     // Catch:{ Exception -> 0x05f2 }
        L_0x0797:
            throw r0     // Catch:{ Exception -> 0x05f2 }
        L_0x0798:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x0790
        L_0x079d:
            r0 = move-exception
            goto L_0x0790
        L_0x079f:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x064b
        L_0x07a5:
            r12 = r5
            r5 = r4
            r4 = r12
            goto L_0x037e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.droidstudio.game.devilninja_beta.c.run():void");
    }
}
