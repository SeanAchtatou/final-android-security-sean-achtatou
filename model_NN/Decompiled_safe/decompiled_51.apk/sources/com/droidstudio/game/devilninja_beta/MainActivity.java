package com.droidstudio.game.devilninja_beta;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.media.AsyncPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.droidstudio.game.devilninja_beta.a.g;
import com.google.ads.AdView;
import com.google.ads.R;

public class MainActivity extends Activity {
    private GestureDetector a = null;
    /* access modifiers changed from: private */
    public GameView b;
    private AsyncPlayer c = null;
    private SharedPreferences d;
    private AdView e;
    private boolean f;

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (z) {
            this.e.setVisibility(0);
        } else {
            this.e.setVisibility(8);
        }
    }

    private void c() {
        this.b.a();
        a(true);
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.c != null) {
            this.c.stop();
            this.c = null;
        }
    }

    public final boolean a() {
        return this.f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.media.AsyncPlayer.play(android.content.Context, android.net.Uri, boolean, int):void}
     arg types: [android.content.Context, android.net.Uri, int, int]
     candidates:
      ClspMth{android.media.AsyncPlayer.play(android.content.Context, android.net.Uri, boolean, android.media.AudioAttributes):void throws java.lang.IllegalArgumentException}
      ClspMth{android.media.AsyncPlayer.play(android.content.Context, android.net.Uri, boolean, int):void} */
    public final void b() {
        if (g.a()) {
            if (this.c == null) {
                this.c = new AsyncPlayer("aPlayer");
                if (this.c == null) {
                    return;
                }
            }
            this.c.play(getBaseContext(), Uri.parse("android.resource://" + getPackageName() + "/" + ((int) R.raw.m_game)), true, 3);
        }
    }

    public void finish() {
        this.f = false;
        d();
        Log.v("MainActivity", "finish()...now");
        super.finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        this.b = (GameView) findViewById(R.id.gameview);
        this.b.a(this);
        this.a = new GestureDetector(this, new n(this));
        this.e = (AdView) findViewById(R.id.ads);
        a(false);
        this.d = getBaseContext().getSharedPreferences("com.ningo.game.action.stick", 0);
        if (this.d.getBoolean("isSound", true)) {
            b();
        }
        this.f = true;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        c();
        AlertDialog create = new AlertDialog.Builder(this).setMessage("Do you want to exit?").setTitle("Exit").setIcon((int) R.drawable.icon).setPositiveButton("Yes", new k(this)).setNeutralButton("Cancel", new l(this)).create();
        create.setOnDismissListener(new m(this));
        create.show();
        return false;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        d();
        c();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        b();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 1) {
            this.b.b();
        }
        super.onTouchEvent(motionEvent);
        return this.a.onTouchEvent(motionEvent);
    }
}
