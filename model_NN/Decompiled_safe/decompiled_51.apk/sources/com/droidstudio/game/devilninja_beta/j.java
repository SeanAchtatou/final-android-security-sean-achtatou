package com.droidstudio.game.devilninja_beta;

import android.app.AlertDialog;
import com.google.ads.R;
import com.scoreloop.client.android.core.d.a;
import com.scoreloop.client.android.core.d.o;

final class j implements o {
    final /* synthetic */ GameoverActivity a;

    /* synthetic */ j(GameoverActivity gameoverActivity) {
        this(gameoverActivity, (byte) 0);
    }

    private j(GameoverActivity gameoverActivity, byte b) {
        this.a = gameoverActivity;
    }

    public final void a(a aVar) {
        if (this.a.e == 0) {
            this.a.e = 1;
        } else {
            this.a.e = 2;
        }
        GameoverActivity.a(this.a);
    }

    public final void a(Exception exc) {
        GameoverActivity.a(this.a);
        new AlertDialog.Builder(this.a).setTitle("Upload Failure!").setMessage("Network error, submit again?").setIcon((int) R.drawable.icon).setPositiveButton("Yes", new a(this)).setNeutralButton("Cancel", new b(this)).create().show();
    }
}
