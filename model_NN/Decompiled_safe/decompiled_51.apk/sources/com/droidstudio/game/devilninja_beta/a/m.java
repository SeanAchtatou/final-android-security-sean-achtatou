package com.droidstudio.game.devilninja_beta.a;

import android.graphics.Point;
import java.util.Iterator;
import java.util.LinkedList;

public final class m {
    public int a;
    private LinkedList b = new LinkedList();
    private int c = 0;
    private int d = 0;
    private int e = 0;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private LinkedList k = new LinkedList();

    public final void a() {
        Iterator it = this.k.iterator();
        while (it.hasNext()) {
            ((Point) it.next()).x += 5;
        }
    }

    public final boolean a(int i2, int i3) {
        int i4 = this.a - 1;
        this.a = i4;
        if (i4 < 0) {
            this.a = 0;
            return false;
        }
        this.b.add(new Point(i2, i3));
        return true;
    }

    public final LinkedList b() {
        return this.k;
    }

    public final boolean b(int i2, int i3) {
        if (this.c != 0) {
            return false;
        }
        this.d = 1;
        this.e = 1;
        this.f = 0;
        this.g = i2;
        this.h = i3;
        this.i = 20;
        this.j = 80;
        return true;
    }

    public final void c() {
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            ((Point) it.next()).x += 5;
        }
    }

    public final boolean c(int i2, int i3) {
        this.k.add(new Point(i2, i3));
        return true;
    }

    public final LinkedList d() {
        return this.b;
    }

    public final void d(int i2, int i3) {
        this.c = i2;
        if (this.c == 0) {
            this.e = 0;
            this.a = 0;
            return;
        }
        this.a += i3;
    }

    public final int e() {
        return this.d;
    }

    public final int f() {
        if (this.d != 0) {
            this.f++;
            if (this.f >= 2) {
                this.f = 0;
                if (this.e == 1) {
                    this.e = 2;
                    this.i = 60;
                } else if (this.e == 2) {
                    this.e = 3;
                    this.i = 80;
                } else if (this.e == 3) {
                    this.e = 4;
                    this.i = 100;
                } else {
                    this.e = 0;
                    this.d = 0;
                }
            }
        }
        return this.e;
    }

    public final int g() {
        return this.c;
    }

    public final int h() {
        return this.g;
    }

    public final int i() {
        return this.h;
    }

    public final int j() {
        return this.g + this.i;
    }

    public final int k() {
        return this.h + this.j;
    }
}
