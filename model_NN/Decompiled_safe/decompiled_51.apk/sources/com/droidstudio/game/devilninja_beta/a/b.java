package com.droidstudio.game.devilninja_beta.a;

public final class b extends c {
    private j e;
    private boolean f = false;
    private int g = 1;
    private int h = 4;
    private int i = 0;

    public b(j jVar) {
        this.a = -480;
        this.b = 30;
        this.c = 480;
        this.d = 180;
        this.e = jVar;
    }

    public final void a() {
        this.f = true;
        this.a = -480;
        this.g = 1;
    }

    public final boolean b() {
        return this.f;
    }

    public final void c() {
        if (this.f) {
            e(8);
            if (this.a >= this.e.c) {
                this.f = false;
            }
        }
    }

    public final int d() {
        this.i++;
        if (this.i >= this.h) {
            this.i = 0;
            switch (this.g) {
                case 1:
                    this.g = 2;
                    break;
                case 2:
                    this.g = 3;
                    break;
                case 3:
                    this.g = 4;
                    break;
                case 4:
                    this.g = 1;
                    break;
            }
        }
        return this.g;
    }
}
