package com.droidstudio.game.devilninja_beta.a;

public final class l extends c {
    public int e;
    public int f;
    public int g;
    public int h;
    private boolean i;

    public l(int i2, int i3, boolean z, boolean z2) {
        if (this.e == 0) {
            this.a = i2;
            this.b = i3;
            this.c = 85;
            this.d = 70;
            if (!z2) {
                this.e = 1;
                this.f = 1;
            } else {
                this.e = 3;
                this.f = 5;
            }
            this.g = 0;
            this.h = 3;
            a(15, 0, 15, 5);
            this.i = z;
        }
    }

    public final boolean a() {
        return this.i;
    }

    public final void b() {
        this.e = 2;
        this.f = 1;
        this.g = 0;
        this.h = 3;
        this.a += 10;
        this.c = 65;
        this.d = 75;
    }
}
