package com.droidstudio.game.devilninja_beta.a;

public final class k {
    private float a;
    private float b;
    private float c;
    private float d;
    private float e;
    private float f;
    private int g;
    private int h;
    private int i;
    private int j;
    private float k = 0.0f;
    private float l = 0.0f;
    private float m = 0.0f;
    private float n = 0.0f;

    public k(int i2, int i3, int i4) {
        this.i = i2;
        this.j = 320;
        this.g = i4;
        this.h = i3;
        this.a = 0.0f;
        this.b = this.a + ((float) this.h);
        this.c = 0.0f;
        this.d = this.c + ((float) this.g);
        this.e = 0.0f;
        this.f = 0.0f;
    }

    public final float a() {
        return this.a;
    }

    public final void a(float f2) {
        this.k = f2;
    }

    public final float b() {
        return this.b;
    }

    public final int c() {
        return this.g;
    }

    public final float d() {
        return this.b > this.a ? (float) this.h : ((float) this.i) - this.a;
    }

    public final float e() {
        if (this.b > this.a) {
            return 0.0f;
        }
        return this.b;
    }

    public final void f() {
        float f2 = this.a + this.k;
        this.f += this.k;
        if (f2 > ((float) this.i)) {
            this.a = f2 - ((float) this.i);
            this.b = this.a + ((float) this.h);
        } else if (f2 <= ((float) this.i)) {
            this.a = f2;
            float f3 = this.a + ((float) this.h);
            if (f3 > ((float) this.i)) {
                this.b = f3 - ((float) this.i);
            } else {
                this.b = f3;
            }
        }
    }
}
