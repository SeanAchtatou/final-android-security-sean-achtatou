package com.droidstudio.game.devilninja_beta;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import com.droidstudio.game.devilninja_beta.a.g;
import com.google.ads.R;

public class TipsActivity extends Activity implements View.OnClickListener {
    public void onClick(View view) {
        if (view.getId() == R.id.btn_play) {
            g.a(1);
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.tips);
        if (((WindowManager) getSystemService("window")).getDefaultDisplay().getHeight() > 480) {
            ((RelativeLayout) findViewById(R.id.bg_tips)).setBackgroundResource(R.drawable.bg_tips);
        }
        findViewById(R.id.btn_play).setOnClickListener(this);
    }
}
