package com.droidstudio.game.devilninja_beta.a;

public final class f {
    private int a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f = 0;
    private int g;
    private int h;
    private int i = 57;
    private int j = 140;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;

    public f(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        this.a = i2;
        this.b = i3;
        this.c = i4;
        this.d = i5;
        this.e = i6;
        this.g = i7;
        this.h = -(this.j - 4);
        this.k = i8;
        this.n = 50;
        this.o = 60;
        if (this.k != 0) {
            this.l = i9;
            this.m = -(this.o - 2);
            return;
        }
        this.l = 0;
        this.m = 0;
    }

    public final int a() {
        return this.a;
    }

    public final void a(int i2) {
        this.a += i2;
    }

    public final int b() {
        return this.a + this.c;
    }

    public final int c() {
        return this.b;
    }

    public final int d() {
        return this.b + 25;
    }

    public final int e() {
        return this.b + 55;
    }

    public final int f() {
        return this.b + 30;
    }

    public final int g() {
        return this.e;
    }

    public final int h() {
        return this.f;
    }

    public final int i() {
        return this.a + this.g;
    }

    public final int j() {
        return this.b + this.h;
    }

    public final int k() {
        return i() + this.i;
    }

    public final int l() {
        return j() + this.j;
    }

    public final int m() {
        return this.k;
    }

    public final int n() {
        return this.a + this.l;
    }

    public final int o() {
        return n() + this.n;
    }

    public final int p() {
        return this.b + 30 + this.m;
    }

    public final int q() {
        return p() + this.o;
    }
}
