package com.droidstudio.game.devilninja_beta.a;

import android.util.Log;
import java.util.LinkedList;
import java.util.Random;

public final class e {
    private int A;
    private m B;
    private b C;
    private int D;
    private int E;
    private int F;
    private int G;
    private long H;
    private long I;
    private int J;
    private n K;
    private long L = 0;
    public k a;
    public k b;
    public i c;
    public LinkedList d;
    public LinkedList e;
    public int f;
    private j g;
    private h h;
    private Random i = new Random();
    private LinkedList j;
    private int k;
    private int l = 0;
    private int m;
    private int n;
    private int o;
    private int p;
    private boolean q;
    private int r = 0;
    private int s;
    private int t;
    private int u = 0;
    private int v = 0;
    private int w = 0;
    private int x = 0;
    private int y;
    private d z;

    public e(j jVar, int i2) {
        this.g = jVar;
        this.a = new k(1138, jVar.c, jVar.d);
        this.a.a(0.3f);
        this.b = new k(700, jVar.c, jVar.d);
        this.b.a(4.0f);
        this.d = new LinkedList();
        this.e = new LinkedList();
        this.m = 40;
        this.D = 0;
        this.k = 8;
        this.J = i2;
        this.h = new h(this.g);
        if (this.J == 1) {
            this.h.e(5);
        }
        this.c = new i();
        this.c.n();
        this.f = 1;
        this.E = 3;
        this.F = 20;
        this.z = new d();
        this.B = new m();
        this.C = new b(this.g);
        this.j = new LinkedList();
        this.s = 45;
        this.t = 0;
        this.I = System.currentTimeMillis();
        this.H = System.currentTimeMillis();
        n();
        m();
        this.K = new n();
    }

    private void a(int i2) {
        if (i2 != this.J) {
            if (i2 == 2) {
                this.k = 0;
                this.a.a(0.0f);
                this.b.a(0.0f);
            } else if (i2 == 1) {
                this.k = 9;
                this.b.a(5.0f);
            } else {
                this.k = 8;
                this.b.a(4.0f);
            }
            if (i2 != 2) {
                this.J = i2;
            }
        }
    }

    private void a(int i2, int i3, boolean z2, boolean z3) {
        this.j.add(new l(i2, i3, z2, z3));
        if (z3) {
            g.a(17);
        }
    }

    private void a(a aVar) {
        aVar.a(3);
        g.a(8);
        this.x++;
    }

    private void a(l lVar) {
        lVar.b();
        g.a(8);
        this.x++;
    }

    private void a(boolean z2) {
        boolean z3;
        char c2;
        if (this.c.b() != 5) {
            if (z2) {
                z3 = this.B.c(this.c.f(), this.c.g());
                c2 = 1;
            } else if (this.B.g() == 0) {
                if (this.B.e() != 1) {
                    z3 = this.B.b(this.c.f(), this.c.g());
                    c2 = 2;
                } else {
                    return;
                }
            } else if (System.currentTimeMillis() - this.L >= 100) {
                this.L = System.currentTimeMillis();
                z3 = this.B.a(this.c.f(), this.c.a());
                if (this.B.a == 0) {
                    this.B.d(0, 0);
                }
                c2 = 3;
            } else {
                z3 = false;
                c2 = 0;
            }
            if (z3) {
                this.c.c();
                if (c2 == 1) {
                    g.a(7);
                } else if (c2 == 2) {
                    g.a(5);
                } else {
                    g.a(6);
                }
            }
        }
    }

    private int b(int i2) {
        return this.i.nextInt(i2);
    }

    private void m() {
        int i2 = this.D / 10;
        if (i2 <= 900) {
            this.m = 35;
            this.s = 35;
            this.y = 7;
            this.n = 50;
            this.q = false;
            this.t = 0;
            this.p = 0;
            this.o = 3;
        } else if (i2 > 900 && i2 <= 2000) {
            this.m = 25;
            this.s = 25;
            this.y = 6;
            this.n = 80;
            this.q = false;
            this.t = 1;
            this.p = 0;
            this.o = 3;
        } else if (i2 > 2000 && i2 <= 5000) {
            this.m = 15;
            this.s = 25;
            this.y = 5;
            this.n = 120;
            this.q = true;
            this.t = 2;
            this.p = 0;
            this.o = 3;
        } else if (i2 > 5000) {
            this.m = 13;
            this.s = 15;
            this.y = 4;
            this.n = 180;
            this.t = 2;
            this.p = 1;
            this.o = 2;
        }
    }

    private void n() {
        if (this.J == 0 || this.J != 1) {
            this.k = 8;
            this.b.a(4.0f);
            return;
        }
        this.k = 9;
        this.b.a(5.0f);
    }

    private int o() {
        g.a(12);
        int i2 = this.E - 1;
        this.E = i2;
        return i2;
    }

    private int p() {
        if (this.c.b() == 5) {
            return 2;
        }
        return this.o;
    }

    /* JADX WARNING: Removed duplicated region for block: B:592:0x0bfb  */
    /* JADX WARNING: Removed duplicated region for block: B:604:0x0c32  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r14 = this;
            com.droidstudio.game.devilninja_beta.a.k r1 = r14.a
            r1.f()
            com.droidstudio.game.devilninja_beta.a.k r1 = r14.b
            r1.f()
            java.util.LinkedList r1 = r14.d
            java.util.Iterator r2 = r1.iterator()
        L_0x0010:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x041e
            int r1 = r14.D
            int r2 = r14.k
            int r1 = r1 + r2
            r14.D = r1
            com.droidstudio.game.devilninja_beta.a.m r1 = r14.B
            r1.c()
            com.droidstudio.game.devilninja_beta.a.m r1 = r14.B
            int r1 = r1.e()
            r2 = 1
            if (r1 != r2) goto L_0x005b
            com.droidstudio.game.devilninja_beta.a.m r1 = r14.B
            int r2 = r1.h()
            com.droidstudio.game.devilninja_beta.a.m r1 = r14.B
            int r3 = r1.i()
            com.droidstudio.game.devilninja_beta.a.m r1 = r14.B
            int r4 = r1.j()
            com.droidstudio.game.devilninja_beta.a.m r1 = r14.B
            int r5 = r1.k()
            java.util.LinkedList r1 = r14.e
            java.util.Iterator r6 = r1.iterator()
        L_0x0049:
            boolean r1 = r6.hasNext()
            if (r1 != 0) goto L_0x042c
            java.util.LinkedList r1 = r14.j
            java.util.Iterator r6 = r1.iterator()
        L_0x0055:
            boolean r1 = r6.hasNext()
            if (r1 != 0) goto L_0x044a
        L_0x005b:
            com.droidstudio.game.devilninja_beta.a.m r1 = r14.B
            java.util.LinkedList r3 = r1.d()
            boolean r1 = r3.isEmpty()
            if (r1 != 0) goto L_0x007f
            java.util.LinkedList r1 = r14.e
            java.util.Iterator r4 = r1.iterator()
        L_0x006d:
            boolean r1 = r4.hasNext()
            if (r1 != 0) goto L_0x0464
            java.util.LinkedList r1 = r14.j
            java.util.Iterator r4 = r1.iterator()
        L_0x0079:
            boolean r1 = r4.hasNext()
            if (r1 != 0) goto L_0x04b6
        L_0x007f:
            com.droidstudio.game.devilninja_beta.a.m r1 = r14.B
            r1.a()
            com.droidstudio.game.devilninja_beta.a.m r1 = r14.B
            java.util.LinkedList r1 = r1.b()
            java.util.Iterator r2 = r1.iterator()
        L_0x008e:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x0500
            com.droidstudio.game.devilninja_beta.a.b r1 = r14.C
            boolean r1 = r1.b()
            if (r1 == 0) goto L_0x00b9
            com.droidstudio.game.devilninja_beta.a.b r1 = r14.C
            r1.c()
            java.util.LinkedList r1 = r14.e
            java.util.Iterator r2 = r1.iterator()
        L_0x00a7:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x055c
            java.util.LinkedList r1 = r14.j
            java.util.Iterator r2 = r1.iterator()
        L_0x00b3:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x0586
        L_0x00b9:
            java.util.LinkedList r1 = r14.e
            java.util.Iterator r2 = r1.iterator()
        L_0x00bf:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x05ac
            java.util.LinkedList r1 = r14.e
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x00d9
            java.util.LinkedList r1 = r14.e
            java.util.Iterator r3 = r1.iterator()
        L_0x00d3:
            boolean r1 = r3.hasNext()
            if (r1 != 0) goto L_0x05e9
        L_0x00d9:
            java.util.LinkedList r1 = r14.e
            java.util.Iterator r2 = r1.iterator()
        L_0x00df:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x0646
            java.util.LinkedList r1 = r14.j
            java.util.Iterator r2 = r1.iterator()
        L_0x00eb:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x0651
            java.util.LinkedList r1 = r14.j
            java.util.Iterator r2 = r1.iterator()
        L_0x00f7:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x06b8
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            r1.m()
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            int r1 = r1.b()
            r2 = 5
            if (r1 == r2) goto L_0x0123
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            int r1 = r1.g()
            com.droidstudio.game.devilninja_beta.a.j r2 = r14.g
            int r2 = r2.d
            if (r1 <= r2) goto L_0x075c
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            int r1 = r1.b()
            r2 = 4
            if (r1 != r2) goto L_0x075c
            r1 = 3
            r14.f = r1
        L_0x0123:
            com.droidstudio.game.devilninja_beta.a.d r1 = r14.z
            int r2 = r1.b
            r3 = 1
            int r2 = r2 - r3
            r1.b = r2
            com.droidstudio.game.devilninja_beta.a.d r1 = r14.z
            int r1 = r1.a
            if (r1 == 0) goto L_0x0192
            com.droidstudio.game.devilninja_beta.a.d r1 = r14.z
            int r2 = r14.k
            int r2 = -r2
            r1.a(r2)
            com.droidstudio.game.devilninja_beta.a.d r1 = r14.z
            int r1 = r1.d()
            if (r1 < 0) goto L_0x018d
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            com.droidstudio.game.devilninja_beta.a.d r2 = r14.z
            int r2 = r2.b()
            com.droidstudio.game.devilninja_beta.a.d r3 = r14.z
            int r3 = r3.c()
            com.droidstudio.game.devilninja_beta.a.d r4 = r14.z
            int r4 = r4.d()
            com.droidstudio.game.devilninja_beta.a.d r5 = r14.z
            int r5 = r5.e()
            boolean r1 = r1.b(r2, r3, r4, r5)
            if (r1 == 0) goto L_0x0192
            com.droidstudio.game.devilninja_beta.a.d r1 = r14.z
            int r1 = r1.a
            com.droidstudio.game.devilninja_beta.a.d r2 = r14.z
            int r2 = r2.b()
            com.droidstudio.game.devilninja_beta.a.d r3 = r14.z
            int r3 = r3.c()
            r4 = 9
            com.droidstudio.game.devilninja_beta.a.g.a(r4)
            r4 = 1
            if (r1 != r4) goto L_0x095a
            int r4 = r14.E
            int r4 = r4 + 1
            r14.E = r4
        L_0x017f:
            r4 = 1
            if (r1 == r4) goto L_0x0188
            r4 = 2
            if (r1 == r4) goto L_0x0188
            r4 = 3
            if (r1 != r4) goto L_0x018d
        L_0x0188:
            com.droidstudio.game.devilninja_beta.a.h r4 = r14.h
            r4.a(r1, r2, r3)
        L_0x018d:
            com.droidstudio.game.devilninja_beta.a.d r1 = r14.z
            r1.a()
        L_0x0192:
            java.util.LinkedList r1 = r14.d
            java.util.Iterator r2 = r1.iterator()
        L_0x0198:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x09be
            java.util.LinkedList r1 = r14.e
            java.util.Iterator r2 = r1.iterator()
        L_0x01a4:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x09cf
            java.util.LinkedList r1 = r14.j
            java.util.Iterator r2 = r1.iterator()
        L_0x01b0:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x09f2
            com.droidstudio.game.devilninja_beta.a.m r1 = r14.B
            java.util.LinkedList r1 = r1.d()
            java.util.Iterator r2 = r1.iterator()
        L_0x01c0:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x0a07
            com.droidstudio.game.devilninja_beta.a.m r1 = r14.B
            java.util.LinkedList r1 = r1.b()
            java.util.Iterator r2 = r1.iterator()
        L_0x01d0:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x0a1d
            com.droidstudio.game.devilninja_beta.a.h r1 = r14.h
            r1.a()
            com.droidstudio.game.devilninja_beta.a.h r1 = r14.h
            r1.b()
            int r1 = r14.D
            int r1 = r1 / 10
            r2 = 0
            r3 = 0
        L_0x01e6:
            r4 = 100
            if (r3 < r4) goto L_0x0a33
        L_0x01ea:
            if (r2 == 0) goto L_0x01f1
            r2 = 13
            com.droidstudio.game.devilninja_beta.a.g.a(r2)
        L_0x01f1:
            com.droidstudio.game.devilninja_beta.a.h r2 = r14.h
            boolean r2 = r2.q()
            if (r2 == 0) goto L_0x01fe
            com.droidstudio.game.devilninja_beta.a.h r2 = r14.h
            r2.r()
        L_0x01fe:
            com.droidstudio.game.devilninja_beta.a.h r2 = r14.h
            r2.y()
            com.droidstudio.game.devilninja_beta.a.h r2 = r14.h
            boolean r2 = r2.h()
            if (r2 != 0) goto L_0x0229
            com.droidstudio.game.devilninja_beta.a.h r2 = r14.h
            boolean r2 = r2.n()
            if (r2 != 0) goto L_0x0a5f
            r2 = 0
            com.droidstudio.game.devilninja_beta.a.h r3 = r14.h
            int r3 = r3.i()
            if (r3 != 0) goto L_0x0a4f
            r3 = 100
            if (r1 < r3) goto L_0x0a4f
            r1 = 1
        L_0x0221:
            if (r1 == 0) goto L_0x0229
            com.droidstudio.game.devilninja_beta.a.h r1 = r14.h
            r2 = 1
            r1.a(r2)
        L_0x0229:
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            int r1 = r1.b()
            r2 = 5
            if (r1 != r2) goto L_0x02b0
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            int r1 = r1.b()
            r2 = 5
            if (r1 != r2) goto L_0x0298
            int r1 = r14.G
            int r1 = r1 + 1
            r14.G = r1
            int r1 = r14.G
            r2 = 150(0x96, float:2.1E-43)
            if (r1 < r2) goto L_0x0298
            com.droidstudio.game.devilninja_beta.a.m r1 = r14.B
            com.droidstudio.game.devilninja_beta.a.i r2 = r14.c
            int r2 = r2.f()
            com.droidstudio.game.devilninja_beta.a.i r3 = r14.c
            int r3 = r3.g()
            r4 = 40
            int r3 = r3 - r4
            r1.c(r2, r3)
            com.droidstudio.game.devilninja_beta.a.m r1 = r14.B
            com.droidstudio.game.devilninja_beta.a.i r2 = r14.c
            int r2 = r2.f()
            com.droidstudio.game.devilninja_beta.a.i r3 = r14.c
            int r3 = r3.g()
            int r3 = r3 + 30
            r1.c(r2, r3)
            com.droidstudio.game.devilninja_beta.a.m r1 = r14.B
            com.droidstudio.game.devilninja_beta.a.i r2 = r14.c
            int r2 = r2.f()
            com.droidstudio.game.devilninja_beta.a.i r3 = r14.c
            int r3 = r3.g()
            int r3 = r3 + 100
            r1.c(r2, r3)
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            r2 = 3
            r1.a(r2)
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            r2 = 80
            r1.b(r2)
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            r2 = 80
            r1.c(r2)
            r14.n()
        L_0x0298:
            java.util.LinkedList r1 = r14.j
            java.util.Iterator r2 = r1.iterator()
        L_0x029e:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x0a83
            java.util.LinkedList r1 = r14.e
            java.util.Iterator r2 = r1.iterator()
        L_0x02aa:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x0a9f
        L_0x02b0:
            com.droidstudio.game.devilninja_beta.a.n r1 = r14.K
            r1.a()
            java.util.LinkedList r1 = r14.d
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x0ab9
            java.util.LinkedList r1 = r14.d
            java.lang.Object r1 = r1.getLast()
            com.droidstudio.game.devilninja_beta.a.f r1 = (com.droidstudio.game.devilninja_beta.a.f) r1
            int r1 = r1.b()
            com.droidstudio.game.devilninja_beta.a.j r2 = r14.g
            int r2 = r2.c
            if (r1 < r2) goto L_0x0ab9
            r1 = 0
        L_0x02d0:
            if (r1 == 0) goto L_0x02fc
            java.util.LinkedList r1 = r14.d
            boolean r1 = r1.isEmpty()
            if (r1 == 0) goto L_0x0ad7
            r1 = 0
            java.util.Random r2 = r14.i
            r3 = 3
            int r2 = r2.nextInt(r3)
            int r10 = r2 + 6
            r2 = 0
            r11 = r2
            r2 = r1
        L_0x02e7:
            if (r11 < r10) goto L_0x0abc
            com.droidstudio.game.devilninja_beta.a.f r1 = new com.droidstudio.game.devilninja_beta.a.f
            r3 = 195(0xc3, float:2.73E-43)
            r4 = 90
            r5 = -1
            r6 = 3
            r7 = 0
            r8 = 0
            r9 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            java.util.LinkedList r2 = r14.d
            r2.add(r1)
        L_0x02fc:
            int r1 = r14.l
            int r1 = r1 + 1
            r14.l = r1
            int r1 = r14.l
            int r2 = r14.m
            if (r1 >= r2) goto L_0x0bd1
            r1 = 0
        L_0x0309:
            if (r1 == 0) goto L_0x035e
            java.util.Random r1 = r14.i
            r2 = 6
            int r1 = r1.nextInt(r2)
            switch(r1) {
                case 0: goto L_0x0c3d;
                case 1: goto L_0x0c3d;
                case 2: goto L_0x0c41;
                case 3: goto L_0x0c41;
                default: goto L_0x0315;
            }
        L_0x0315:
            int r1 = r14.p
            if (r1 != 0) goto L_0x0c45
            r1 = 0
            r2 = r1
        L_0x031b:
            if (r2 == 0) goto L_0x0c49
            com.droidstudio.game.devilninja_beta.a.j r1 = r14.g
            int r1 = r1.c
            r3 = 30
            int r1 = r1 - r3
            java.util.Random r3 = r14.i
            r4 = 80
            int r3 = r3.nextInt(r4)
            int r1 = r1 - r3
            r3 = r1
        L_0x032e:
            r4 = -1
            java.util.LinkedList r1 = r14.d
            java.util.Iterator r5 = r1.iterator()
        L_0x0335:
            boolean r1 = r5.hasNext()
            if (r1 != 0) goto L_0x0c52
            r1 = r4
        L_0x033c:
            r4 = -1
            if (r1 == r4) goto L_0x035e
            r4 = 75
            int r1 = r1 - r4
            r4 = 0
            java.util.Random r5 = r14.i
            r6 = 3
            int r5 = r5.nextInt(r6)
            if (r5 != 0) goto L_0x034d
            r4 = 1
        L_0x034d:
            com.droidstudio.game.devilninja_beta.a.a r5 = new com.droidstudio.game.devilninja_beta.a.a
            r5.<init>(r3, r1, r4, r2)
            java.util.LinkedList r1 = r14.e
            r1.add(r5)
            if (r2 == 0) goto L_0x035e
            r1 = 17
            com.droidstudio.game.devilninja_beta.a.g.a(r1)
        L_0x035e:
            int r1 = r14.r
            int r1 = r1 + 1
            r14.r = r1
            int r1 = r14.r
            int r2 = r14.s
            if (r1 >= r2) goto L_0x0c8d
            r1 = 0
        L_0x036b:
            if (r1 == 0) goto L_0x03a8
            int r1 = r14.t
            if (r1 != 0) goto L_0x0cc8
            r1 = 3
        L_0x0372:
            java.util.Random r2 = r14.i
            int r1 = r2.nextInt(r1)
            switch(r1) {
                case 0: goto L_0x0cd4;
                case 1: goto L_0x0cd4;
                case 2: goto L_0x0cd4;
                case 3: goto L_0x0cd7;
                case 4: goto L_0x0cd7;
                case 5: goto L_0x0cd7;
                case 6: goto L_0x0cda;
                case 7: goto L_0x0cda;
                case 8: goto L_0x0cda;
                default: goto L_0x037b;
            }
        L_0x037b:
            r1 = 3
        L_0x037c:
            r2 = 1
            if (r1 != r2) goto L_0x0cdd
            com.droidstudio.game.devilninja_beta.a.j r2 = r14.g
            int r2 = r2.c
            int r3 = r14.n
            int r2 = r2 - r3
            java.util.Random r3 = r14.i
            int r4 = r14.n
            int r4 = r4 + 10
            int r3 = r3.nextInt(r4)
            int r2 = r2 + r3
            r3 = -75
            r13 = r3
            r3 = r2
            r2 = r13
        L_0x0396:
            r4 = 0
            java.util.Random r5 = r14.i
            r6 = 2
            int r5 = r5.nextInt(r6)
            if (r5 != 0) goto L_0x03a1
            r4 = 1
        L_0x03a1:
            r5 = 3
            if (r1 != r5) goto L_0x0d16
            r1 = 1
        L_0x03a5:
            r14.a(r3, r2, r4, r1)
        L_0x03a8:
            com.droidstudio.game.devilninja_beta.a.d r1 = r14.z
            int r1 = r1.b
            if (r1 > 0) goto L_0x03c8
            r1 = 12
            int r1 = r14.b(r1)
            switch(r1) {
                case 0: goto L_0x0d19;
                case 1: goto L_0x0d19;
                case 2: goto L_0x0d19;
                case 3: goto L_0x0d1c;
                case 4: goto L_0x0d1f;
                case 5: goto L_0x0d22;
                case 6: goto L_0x0d2b;
                case 7: goto L_0x0d2e;
                case 8: goto L_0x0d47;
                default: goto L_0x03b7;
            }
        L_0x03b7:
            r1 = 0
        L_0x03b8:
            com.droidstudio.game.devilninja_beta.a.d r2 = r14.z
            int r2 = r2.f()
            if (r2 != 0) goto L_0x0d60
            r1 = 3
        L_0x03c1:
            if (r1 != 0) goto L_0x0d84
            com.droidstudio.game.devilninja_beta.a.d r1 = r14.z
            r1.a()
        L_0x03c8:
            int r1 = r14.D
            int r1 = r1 / 10
            r2 = 0
            int r3 = r14.u
            int r3 = r3 + 1100
            if (r1 < r3) goto L_0x041a
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r1 < r3) goto L_0x0dac
            r3 = 2000(0x7d0, float:2.803E-42)
            if (r1 >= r3) goto L_0x0dac
            r1 = 2
        L_0x03dc:
            int r2 = r14.w
            if (r2 >= r1) goto L_0x041a
            int r2 = r14.v
            int r3 = r2 + 1
            r14.v = r3
            r3 = 5
            if (r2 < r3) goto L_0x041a
            r2 = 0
            r14.v = r2
            java.util.Random r2 = r14.i
            r3 = 200(0xc8, float:2.8E-43)
            int r2 = r2.nextInt(r3)
            int r2 = r2 + 250
            java.util.Random r3 = r14.i
            r4 = 100
            int r3 = r3.nextInt(r4)
            int r3 = r3 + 20
            r4 = 1
            r5 = 1
            r14.a(r2, r3, r4, r5)
            int r2 = r14.w
            int r2 = r2 + 1
            r14.w = r2
            int r2 = r14.w
            if (r2 < r1) goto L_0x041a
            int r1 = r14.u
            if (r1 != 0) goto L_0x0de0
            r1 = 1100(0x44c, float:1.541E-42)
            r14.u = r1
        L_0x0417:
            r1 = 0
            r14.w = r1
        L_0x041a:
            r14.m()
            return
        L_0x041e:
            java.lang.Object r1 = r2.next()
            com.droidstudio.game.devilninja_beta.a.f r1 = (com.droidstudio.game.devilninja_beta.a.f) r1
            int r3 = r14.k
            int r3 = -r3
            r1.a(r3)
            goto L_0x0010
        L_0x042c:
            java.lang.Object r1 = r6.next()
            com.droidstudio.game.devilninja_beta.a.a r1 = (com.droidstudio.game.devilninja_beta.a.a) r1
            int r7 = r1.b()
            if (r7 == 0) goto L_0x0049
            int r7 = r1.b()
            r8 = 3
            if (r7 == r8) goto L_0x0049
            boolean r7 = r1.b(r2, r3, r4, r5)
            if (r7 == 0) goto L_0x0049
            r14.a(r1)
            goto L_0x0049
        L_0x044a:
            java.lang.Object r1 = r6.next()
            com.droidstudio.game.devilninja_beta.a.l r1 = (com.droidstudio.game.devilninja_beta.a.l) r1
            int r7 = r1.e
            r8 = 2
            if (r7 == r8) goto L_0x0055
            int r7 = r1.e
            if (r7 == 0) goto L_0x0055
            boolean r7 = r1.b(r2, r3, r4, r5)
            if (r7 == 0) goto L_0x0055
            r14.a(r1)
            goto L_0x0055
        L_0x0464:
            java.lang.Object r1 = r4.next()
            com.droidstudio.game.devilninja_beta.a.a r1 = (com.droidstudio.game.devilninja_beta.a.a) r1
            int r2 = r1.b()
            r5 = 3
            if (r2 == r5) goto L_0x006d
            int r2 = r1.b()
            if (r2 == 0) goto L_0x006d
            java.util.Iterator r5 = r3.iterator()
        L_0x047b:
            boolean r2 = r5.hasNext()
            if (r2 == 0) goto L_0x006d
            java.lang.Object r2 = r5.next()
            android.graphics.Point r2 = (android.graphics.Point) r2
            int r6 = r1.e()
            int r7 = r2.x
            if (r6 >= r7) goto L_0x047b
            int r6 = r2.x
            int r7 = r1.f()
            if (r6 >= r7) goto L_0x047b
            int r6 = r1.g()
            int r7 = r2.y
            if (r6 >= r7) goto L_0x047b
            int r2 = r2.y
            int r6 = r1.h()
            if (r2 >= r6) goto L_0x047b
            java.lang.String r2 = "UIModel"
            java.lang.String r6 = "handleWeaponMove()--swoop:Dead!"
            android.util.Log.v(r2, r6)
            r14.a(r1)
            r5.remove()
            goto L_0x006d
        L_0x04b6:
            java.lang.Object r1 = r4.next()
            com.droidstudio.game.devilninja_beta.a.l r1 = (com.droidstudio.game.devilninja_beta.a.l) r1
            int r2 = r1.e
            r5 = 1
            if (r2 != r5) goto L_0x0079
            java.util.Iterator r5 = r3.iterator()
        L_0x04c5:
            boolean r2 = r5.hasNext()
            if (r2 == 0) goto L_0x0079
            java.lang.Object r2 = r5.next()
            android.graphics.Point r2 = (android.graphics.Point) r2
            int r6 = r1.e()
            int r7 = r2.x
            if (r6 >= r7) goto L_0x04c5
            int r6 = r2.x
            int r7 = r1.f()
            if (r6 >= r7) goto L_0x04c5
            int r6 = r1.g()
            int r7 = r2.y
            if (r6 >= r7) goto L_0x04c5
            int r2 = r2.y
            int r6 = r1.h()
            if (r2 >= r6) goto L_0x04c5
            java.lang.String r2 = "UIModel"
            java.lang.String r6 = "handleWeaponMove()--swoop:Dead!"
            android.util.Log.v(r2, r6)
            r14.a(r1)
            r5.remove()
            goto L_0x0079
        L_0x0500:
            java.lang.Object r1 = r2.next()
            android.graphics.Point r1 = (android.graphics.Point) r1
            int r3 = r1.x
            int r4 = r1.y
            int r5 = r1.x
            int r5 = r5 + 40
            int r1 = r1.y
            int r6 = r1 + 40
            java.util.LinkedList r1 = r14.e
            java.util.Iterator r7 = r1.iterator()
        L_0x0518:
            boolean r1 = r7.hasNext()
            if (r1 != 0) goto L_0x053f
            java.util.LinkedList r1 = r14.j
            java.util.Iterator r7 = r1.iterator()
        L_0x0524:
            boolean r1 = r7.hasNext()
            if (r1 == 0) goto L_0x008e
            java.lang.Object r1 = r7.next()
            com.droidstudio.game.devilninja_beta.a.l r1 = (com.droidstudio.game.devilninja_beta.a.l) r1
            int r8 = r1.e
            r9 = 1
            if (r8 != r9) goto L_0x0524
            boolean r8 = r1.b(r3, r4, r5, r6)
            if (r8 == 0) goto L_0x0524
            r14.a(r1)
            goto L_0x0524
        L_0x053f:
            java.lang.Object r1 = r7.next()
            com.droidstudio.game.devilninja_beta.a.a r1 = (com.droidstudio.game.devilninja_beta.a.a) r1
            int r8 = r1.b()
            if (r8 == 0) goto L_0x0518
            int r8 = r1.b()
            r9 = 3
            if (r8 == r9) goto L_0x0518
            boolean r8 = r1.b(r3, r4, r5, r6)
            if (r8 == 0) goto L_0x0518
            r14.a(r1)
            goto L_0x0518
        L_0x055c:
            java.lang.Object r1 = r2.next()
            com.droidstudio.game.devilninja_beta.a.a r1 = (com.droidstudio.game.devilninja_beta.a.a) r1
            int r3 = r1.b()
            if (r3 == 0) goto L_0x00a7
            int r3 = r1.b()
            r4 = 3
            if (r3 == r4) goto L_0x00a7
            int r3 = r1.e()
            com.droidstudio.game.devilninja_beta.a.j r4 = r14.g
            int r4 = r4.c
            if (r3 > r4) goto L_0x00a7
            com.droidstudio.game.devilninja_beta.a.b r3 = r14.C
            boolean r3 = r3.a(r1)
            if (r3 == 0) goto L_0x00a7
            r14.a(r1)
            goto L_0x00a7
        L_0x0586:
            java.lang.Object r1 = r2.next()
            com.droidstudio.game.devilninja_beta.a.l r1 = (com.droidstudio.game.devilninja_beta.a.l) r1
            int r3 = r1.e
            r4 = 2
            if (r3 == r4) goto L_0x00b3
            int r3 = r1.e
            if (r3 == 0) goto L_0x00b3
            int r3 = r1.e()
            com.droidstudio.game.devilninja_beta.a.j r4 = r14.g
            int r4 = r4.c
            if (r3 > r4) goto L_0x00b3
            com.droidstudio.game.devilninja_beta.a.b r3 = r14.C
            boolean r3 = r3.a(r1)
            if (r3 == 0) goto L_0x00b3
            r14.a(r1)
            goto L_0x00b3
        L_0x05ac:
            java.lang.Object r1 = r2.next()
            com.droidstudio.game.devilninja_beta.a.a r1 = (com.droidstudio.game.devilninja_beta.a.a) r1
            int r3 = r1.b()
            if (r3 == 0) goto L_0x00bf
            r4 = 1
            if (r3 != r4) goto L_0x05d1
            boolean r3 = r1.a()
            if (r3 == 0) goto L_0x05cc
            r3 = -2
            int r4 = r14.k
            int r3 = r3 - r4
            r4 = 1
            int r3 = r3 - r4
        L_0x05c7:
            r1.e(r3)
            goto L_0x00bf
        L_0x05cc:
            r3 = -2
            int r4 = r14.k
            int r3 = r3 - r4
            goto L_0x05c7
        L_0x05d1:
            r4 = 2
            if (r3 != r4) goto L_0x05e0
            int r3 = r14.k
            int r3 = -r3
            r1.e(r3)
            r3 = 5
            r1.f(r3)
            goto L_0x00bf
        L_0x05e0:
            r4 = 3
            if (r3 != r4) goto L_0x00bf
            r3 = 2
            r1.e(r3)
            goto L_0x00bf
        L_0x05e9:
            java.lang.Object r1 = r3.next()
            r0 = r1
            com.droidstudio.game.devilninja_beta.a.a r0 = (com.droidstudio.game.devilninja_beta.a.a) r0
            r2 = r0
            int r1 = r2.b()
            r4 = 1
            if (r1 != r4) goto L_0x00d3
            r4 = 0
            java.util.LinkedList r1 = r14.d
            java.util.Iterator r5 = r1.iterator()
        L_0x05ff:
            boolean r1 = r5.hasNext()
            if (r1 != 0) goto L_0x0613
            r1 = r4
        L_0x0606:
            if (r1 != 0) goto L_0x00d3
            r1 = 2
            r2.a(r1)
            r1 = -15
            r2.e(r1)
            goto L_0x00d3
        L_0x0613:
            java.lang.Object r1 = r5.next()
            com.droidstudio.game.devilninja_beta.a.f r1 = (com.droidstudio.game.devilninja_beta.a.f) r1
            int r6 = r2.e()
            int r7 = r2.i()
            int r7 = r7 / 2
            int r6 = r6 + r7
            int r7 = r1.a()
            if (r6 <= r7) goto L_0x05ff
            int r7 = r1.b()
            if (r6 > r7) goto L_0x05ff
            int r6 = r2.h()
            int r7 = r1.d()
            if (r6 < r7) goto L_0x05ff
            int r6 = r2.h()
            int r1 = r1.e()
            if (r6 > r1) goto L_0x05ff
            r1 = 1
            goto L_0x0606
        L_0x0646:
            java.lang.Object r1 = r2.next()
            com.droidstudio.game.devilninja_beta.a.a r1 = (com.droidstudio.game.devilninja_beta.a.a) r1
            r1.d()
            goto L_0x00df
        L_0x0651:
            java.lang.Object r1 = r2.next()
            com.droidstudio.game.devilninja_beta.a.l r1 = (com.droidstudio.game.devilninja_beta.a.l) r1
            int r3 = r1.e
            r4 = 1
            if (r3 != r4) goto L_0x06af
            int r3 = r14.k
            int r3 = -r3
            r4 = 3
            int r3 = r3 - r4
            r1.e(r3)
            int r3 = r1.g()
            java.util.Random r4 = r14.i
            r5 = 25
            int r4 = r4.nextInt(r5)
            int r4 = r4 + 20
            if (r3 > r4) goto L_0x067a
            r3 = 5
            r1.f(r3)
            goto L_0x00eb
        L_0x067a:
            int r3 = r1.h()
            r4 = 150(0x96, float:2.1E-43)
            if (r3 <= r4) goto L_0x0694
            boolean r3 = r14.q
            if (r3 == 0) goto L_0x00eb
            int r3 = r1.h()
            r4 = 170(0xaa, float:2.38E-43)
            if (r3 >= r4) goto L_0x00eb
            r3 = 5
            r1.f(r3)
            goto L_0x00eb
        L_0x0694:
            boolean r3 = r1.a()
            if (r3 != 0) goto L_0x06a9
            java.util.Random r3 = r14.i
            r4 = 2
            int r3 = r3.nextInt(r4)
            if (r3 != 0) goto L_0x00eb
            r3 = 1
            r1.f(r3)
            goto L_0x00eb
        L_0x06a9:
            r3 = 5
            r1.f(r3)
            goto L_0x00eb
        L_0x06af:
            r4 = 2
            if (r3 != r4) goto L_0x00eb
            r3 = 2
            r1.e(r3)
            goto L_0x00eb
        L_0x06b8:
            java.lang.Object r1 = r2.next()
            com.droidstudio.game.devilninja_beta.a.l r1 = (com.droidstudio.game.devilninja_beta.a.l) r1
            int r3 = r1.g
            int r3 = r3 + 1
            r1.g = r3
            int r3 = r1.g
            int r4 = r1.h
            if (r3 < r4) goto L_0x00f7
            r3 = 0
            r1.g = r3
            int r3 = r1.e
            r4 = 1
            if (r3 != r4) goto L_0x06fa
            int r3 = r1.f
            r4 = 1
            if (r3 != r4) goto L_0x06dc
            r3 = 2
            r1.f = r3
            goto L_0x00f7
        L_0x06dc:
            int r3 = r1.f
            r4 = 2
            if (r3 != r4) goto L_0x06e6
            r3 = 3
            r1.f = r3
            goto L_0x00f7
        L_0x06e6:
            int r3 = r1.f
            r4 = 3
            if (r3 != r4) goto L_0x06f0
            r3 = 4
            r1.f = r3
            goto L_0x00f7
        L_0x06f0:
            int r3 = r1.f
            r4 = 4
            if (r3 != r4) goto L_0x00f7
            r3 = 1
            r1.f = r3
            goto L_0x00f7
        L_0x06fa:
            int r3 = r1.e
            r4 = 2
            if (r3 != r4) goto L_0x072a
            int r3 = r1.f
            r4 = 1
            if (r3 != r4) goto L_0x0709
            r3 = 2
            r1.f = r3
            goto L_0x00f7
        L_0x0709:
            int r3 = r1.f
            r4 = 2
            if (r3 != r4) goto L_0x0713
            r3 = 3
            r1.f = r3
            goto L_0x00f7
        L_0x0713:
            int r3 = r1.f
            r4 = 3
            if (r3 != r4) goto L_0x071d
            r3 = 4
            r1.f = r3
            goto L_0x00f7
        L_0x071d:
            int r3 = r1.f
            r4 = 4
            if (r3 != r4) goto L_0x00f7
            r3 = 0
            r1.e = r3
            r3 = 0
            r1.f = r3
            goto L_0x00f7
        L_0x072a:
            int r3 = r1.e
            r4 = 3
            if (r3 != r4) goto L_0x00f7
            int r3 = r1.f
            r4 = 5
            if (r3 != r4) goto L_0x0739
            r3 = 6
            r1.f = r3
            goto L_0x00f7
        L_0x0739:
            int r3 = r1.f
            r4 = 6
            if (r3 != r4) goto L_0x0743
            r3 = 7
            r1.f = r3
            goto L_0x00f7
        L_0x0743:
            int r3 = r1.f
            r4 = 7
            if (r3 != r4) goto L_0x074e
            r3 = 8
            r1.f = r3
            goto L_0x00f7
        L_0x074e:
            int r3 = r1.f
            r4 = 8
            if (r3 != r4) goto L_0x00f7
            r3 = 1
            r1.f = r3
            r3 = 1
            r1.e = r3
            goto L_0x00f7
        L_0x075c:
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            int r1 = r1.b()
            r2 = 2
            if (r1 != r2) goto L_0x0780
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            int r1 = r1.d()
            r2 = 3
            if (r1 != r2) goto L_0x077e
            r1 = 2
        L_0x076f:
            com.droidstudio.game.devilninja_beta.a.i r2 = r14.c
            int r2 = r2.g()
            int r1 = r2 - r1
            com.droidstudio.game.devilninja_beta.a.i r2 = r14.c
            r2.d(r1)
            goto L_0x0123
        L_0x077e:
            r1 = 7
            goto L_0x076f
        L_0x0780:
            r2 = 3
            if (r1 != r2) goto L_0x07ee
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            int r1 = r1.g()
            int r1 = r1 + 5
            com.droidstudio.game.devilninja_beta.a.i r2 = r14.c
            r2.d(r1)
        L_0x0790:
            r2 = 0
            r1 = -1
            java.util.LinkedList r3 = r14.d
            java.util.Iterator r3 = r3.iterator()
        L_0x0798:
            boolean r4 = r3.hasNext()
            if (r4 != 0) goto L_0x080b
        L_0x079e:
            r3 = 1
            if (r1 != r3) goto L_0x0860
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            int r1 = r1.b()
            r3 = 1
            if (r1 == r3) goto L_0x07b5
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            r3 = 1
            r1.a(r3)
            r1 = 15
            com.droidstudio.game.devilninja_beta.a.g.a(r1)
        L_0x07b5:
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            r3 = 60
            int r2 = r2 - r3
            int r2 = r2 + 0
            r1.d(r2)
        L_0x07bf:
            int r1 = r14.F
            r2 = 1
            int r1 = r1 - r2
            r14.F = r1
            if (r1 > 0) goto L_0x0123
            r1 = 0
            r14.F = r1
            r1 = 0
            java.util.LinkedList r2 = r14.j
            java.util.Iterator r2 = r2.iterator()
            r3 = r1
        L_0x07d2:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x0899
            if (r3 == 0) goto L_0x08bd
            com.droidstudio.game.devilninja_beta.a.h r1 = r14.h
            r1.e()
            r1 = 20
            r14.F = r1
            int r1 = r14.o()
            if (r1 != 0) goto L_0x0123
            r1 = 3
            r14.f = r1
            goto L_0x0123
        L_0x07ee:
            r2 = 4
            if (r1 != r2) goto L_0x0790
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            int r1 = r1.d()
            r2 = 4
            if (r1 != r2) goto L_0x0809
            r1 = 2
        L_0x07fb:
            com.droidstudio.game.devilninja_beta.a.i r2 = r14.c
            int r2 = r2.g()
            int r1 = r1 + r2
            com.droidstudio.game.devilninja_beta.a.i r2 = r14.c
            r2.d(r1)
            goto L_0x0123
        L_0x0809:
            r1 = 5
            goto L_0x07fb
        L_0x080b:
            java.lang.Object r1 = r3.next()
            com.droidstudio.game.devilninja_beta.a.f r1 = (com.droidstudio.game.devilninja_beta.a.f) r1
            com.droidstudio.game.devilninja_beta.a.i r4 = r14.c
            int r4 = r4.f()
            int r5 = r1.a()
            r6 = 7
            int r5 = r5 - r6
            if (r4 < r5) goto L_0x085d
            int r5 = r1.b()
            int r5 = r5 + 7
            if (r4 > r5) goto L_0x085d
            com.droidstudio.game.devilninja_beta.a.i r3 = r14.c
            int r3 = r3.h()
            int r4 = r1.d()
            if (r3 < r4) goto L_0x0849
            com.droidstudio.game.devilninja_beta.a.i r3 = r14.c
            int r3 = r3.h()
            int r4 = r1.e()
            if (r3 > r4) goto L_0x0849
            r2 = 1
            int r1 = r1.f()
            r13 = r2
            r2 = r1
            r1 = r13
            goto L_0x079e
        L_0x0849:
            com.droidstudio.game.devilninja_beta.a.i r3 = r14.c
            int r3 = r3.h()
            int r1 = r1.e()
            int r1 = r1 + 20
            if (r3 <= r1) goto L_0x085a
            r1 = 4
            goto L_0x079e
        L_0x085a:
            r1 = 3
            goto L_0x079e
        L_0x085d:
            r1 = 3
            goto L_0x0798
        L_0x0860:
            r2 = 4
            if (r1 != r2) goto L_0x0885
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            int r1 = r1.b()
            r2 = 1
            if (r1 == r2) goto L_0x0875
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            int r1 = r1.b()
            r2 = 3
            if (r1 != r2) goto L_0x07bf
        L_0x0875:
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            r2 = 4
            r1.a(r2)
            r1 = 2
            r14.a(r1)
            r1 = 2
            com.droidstudio.game.devilninja_beta.a.g.a(r1)
            goto L_0x07bf
        L_0x0885:
            r2 = 3
            if (r1 != r2) goto L_0x07bf
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            int r1 = r1.b()
            r2 = 1
            if (r1 != r2) goto L_0x07bf
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            r2 = 3
            r1.a(r2)
            goto L_0x07bf
        L_0x0899:
            java.lang.Object r1 = r2.next()
            com.droidstudio.game.devilninja_beta.a.l r1 = (com.droidstudio.game.devilninja_beta.a.l) r1
            int r4 = r1.e
            r5 = 2
            if (r4 == r5) goto L_0x07d2
            int r4 = r1.e
            if (r4 == 0) goto L_0x07d2
            if (r3 != 0) goto L_0x07d2
            com.droidstudio.game.devilninja_beta.a.i r4 = r14.c
            boolean r1 = r4.a(r1)
            if (r1 == 0) goto L_0x07d2
            java.lang.String r1 = "UIModel"
            java.lang.String r3 = "handleRoleAction():Hit by dragon"
            android.util.Log.v(r1, r3)
            r1 = 1
            r3 = r1
            goto L_0x07d2
        L_0x08bd:
            r1 = 0
            java.util.LinkedList r2 = r14.e
            java.util.Iterator r2 = r2.iterator()
            r3 = r1
        L_0x08c5:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x08e1
            if (r3 == 0) goto L_0x0902
            com.droidstudio.game.devilninja_beta.a.h r1 = r14.h
            r1.e()
            r1 = 20
            r14.F = r1
            int r1 = r14.o()
            if (r1 != 0) goto L_0x0123
            r1 = 3
            r14.f = r1
            goto L_0x0123
        L_0x08e1:
            java.lang.Object r1 = r2.next()
            com.droidstudio.game.devilninja_beta.a.a r1 = (com.droidstudio.game.devilninja_beta.a.a) r1
            int r4 = r1.b()
            r5 = 1
            if (r4 != r5) goto L_0x08c5
            if (r3 != 0) goto L_0x08c5
            com.droidstudio.game.devilninja_beta.a.i r4 = r14.c
            boolean r1 = r4.a(r1)
            if (r1 == 0) goto L_0x08c5
            java.lang.String r1 = "UIModel"
            java.lang.String r3 = "handleRoleAction():Hit by enemy"
            android.util.Log.v(r1, r3)
            r1 = 1
            r3 = r1
            goto L_0x08c5
        L_0x0902:
            r1 = 0
            java.util.LinkedList r2 = r14.d
            java.util.Iterator r2 = r2.iterator()
            r3 = r1
        L_0x090a:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x0926
            if (r3 == 0) goto L_0x0123
            com.droidstudio.game.devilninja_beta.a.h r1 = r14.h
            r1.e()
            r1 = 20
            r14.F = r1
            int r1 = r14.o()
            if (r1 != 0) goto L_0x0123
            r1 = 3
            r14.f = r1
            goto L_0x0123
        L_0x0926:
            java.lang.Object r1 = r2.next()
            com.droidstudio.game.devilninja_beta.a.f r1 = (com.droidstudio.game.devilninja_beta.a.f) r1
            int r4 = r1.m()
            if (r4 == 0) goto L_0x090a
            if (r3 != 0) goto L_0x090a
            com.droidstudio.game.devilninja_beta.a.i r4 = r14.c
            int r5 = r1.n()
            int r6 = r1.p()
            int r6 = r6 + 5
            int r7 = r1.o()
            r8 = 5
            int r7 = r7 - r8
            int r1 = r1.q()
            boolean r1 = r4.b(r5, r6, r7, r1)
            if (r1 == 0) goto L_0x090a
            java.lang.String r1 = "UIModel"
            java.lang.String r3 = "handleRoleAction():Hit by roadblock"
            android.util.Log.v(r1, r3)
            r1 = 1
            r3 = r1
            goto L_0x090a
        L_0x095a:
            r4 = 4
            if (r1 != r4) goto L_0x0976
            com.droidstudio.game.devilninja_beta.a.b r4 = r14.C
            boolean r4 = r4.b()
            if (r4 != 0) goto L_0x096f
            com.droidstudio.game.devilninja_beta.a.b r4 = r14.C
            r4.a()
            r4 = 11
            com.droidstudio.game.devilninja_beta.a.g.a(r4)
        L_0x096f:
            com.droidstudio.game.devilninja_beta.a.h r4 = r14.h
            r4.G()
            goto L_0x017f
        L_0x0976:
            r4 = 2
            if (r1 != r4) goto L_0x0983
            com.droidstudio.game.devilninja_beta.a.m r4 = r14.B
            r5 = 1
            r6 = 30
            r4.d(r5, r6)
            goto L_0x017f
        L_0x0983:
            r4 = 3
            if (r1 != r4) goto L_0x0990
            com.droidstudio.game.devilninja_beta.a.m r4 = r14.B
            r5 = 1
            r6 = 60
            r4.d(r5, r6)
            goto L_0x017f
        L_0x0990:
            r4 = 5
            if (r1 != r4) goto L_0x018d
            com.droidstudio.game.devilninja_beta.a.i r4 = r14.c
            r5 = 5
            r4.a(r5)
            com.droidstudio.game.devilninja_beta.a.i r4 = r14.c
            r5 = 20
            int r5 = r3 - r5
            r4.d(r5)
            com.droidstudio.game.devilninja_beta.a.i r4 = r14.c
            r5 = 100
            r4.b(r5)
            com.droidstudio.game.devilninja_beta.a.i r4 = r14.c
            r5 = 100
            r4.c(r5)
            r4 = 0
            r14.G = r4
            r4 = 12
            r14.k = r4
            r4 = 10
            com.droidstudio.game.devilninja_beta.a.g.a(r4)
            goto L_0x017f
        L_0x09be:
            java.lang.Object r1 = r2.next()
            com.droidstudio.game.devilninja_beta.a.f r1 = (com.droidstudio.game.devilninja_beta.a.f) r1
            int r1 = r1.b()
            if (r1 >= 0) goto L_0x0198
            r2.remove()
            goto L_0x0198
        L_0x09cf:
            java.lang.Object r1 = r2.next()
            com.droidstudio.game.devilninja_beta.a.a r1 = (com.droidstudio.game.devilninja_beta.a.a) r1
            int r3 = r1.b()
            if (r3 == 0) goto L_0x09ed
            int r3 = r1.f()
            r4 = -20
            if (r3 < r4) goto L_0x09ed
            int r1 = r1.g()
            com.droidstudio.game.devilninja_beta.a.j r3 = r14.g
            int r3 = r3.d
            if (r1 <= r3) goto L_0x01a4
        L_0x09ed:
            r2.remove()
            goto L_0x01a4
        L_0x09f2:
            java.lang.Object r1 = r2.next()
            com.droidstudio.game.devilninja_beta.a.l r1 = (com.droidstudio.game.devilninja_beta.a.l) r1
            int r3 = r1.f()
            if (r3 < 0) goto L_0x0a02
            int r1 = r1.e
            if (r1 != 0) goto L_0x01b0
        L_0x0a02:
            r2.remove()
            goto L_0x01b0
        L_0x0a07:
            java.lang.Object r1 = r2.next()
            android.graphics.Point r1 = (android.graphics.Point) r1
            int r1 = r1.x
            com.droidstudio.game.devilninja_beta.a.j r3 = r14.g
            int r3 = r3.c
            r4 = 20
            int r3 = r3 - r4
            if (r1 <= r3) goto L_0x01c0
            r2.remove()
            goto L_0x01c0
        L_0x0a1d:
            java.lang.Object r1 = r2.next()
            android.graphics.Point r1 = (android.graphics.Point) r1
            int r1 = r1.x
            com.droidstudio.game.devilninja_beta.a.j r3 = r14.g
            int r3 = r3.c
            r4 = 20
            int r3 = r3 - r4
            if (r1 <= r3) goto L_0x01d0
            r2.remove()
            goto L_0x01d0
        L_0x0a33:
            int r4 = r3 * 1000
            int r5 = r3 + 1
            int r5 = r5 * 1000
            com.droidstudio.game.devilninja_beta.a.h r6 = r14.h
            int r6 = r6.m()
            if (r6 != r4) goto L_0x0a4b
            if (r1 < r5) goto L_0x01ea
            com.droidstudio.game.devilninja_beta.a.h r2 = r14.h
            r2.h(r5)
            r2 = 1
            goto L_0x01ea
        L_0x0a4b:
            int r3 = r3 + 1
            goto L_0x01e6
        L_0x0a4f:
            com.droidstudio.game.devilninja_beta.a.h r3 = r14.h
            int r3 = r3.i()
            r4 = 1
            if (r3 != r4) goto L_0x0df2
            r3 = 200(0xc8, float:2.8E-43)
            if (r1 < r3) goto L_0x0df2
            r1 = 1
            goto L_0x0221
        L_0x0a5f:
            com.droidstudio.game.devilninja_beta.a.h r1 = r14.h
            r1.p()
            com.droidstudio.game.devilninja_beta.a.i r1 = r14.c
            int r1 = r1.h()
            com.droidstudio.game.devilninja_beta.a.h r2 = r14.h
            int r2 = r2.o()
            if (r1 <= r2) goto L_0x0229
            com.droidstudio.game.devilninja_beta.a.h r1 = r14.h
            r2 = 0
            r1.a(r2)
            com.droidstudio.game.devilninja_beta.a.h r1 = r14.h
            r1.g()
            r1 = 4
            com.droidstudio.game.devilninja_beta.a.g.a(r1)
            goto L_0x0229
        L_0x0a83:
            java.lang.Object r1 = r2.next()
            com.droidstudio.game.devilninja_beta.a.l r1 = (com.droidstudio.game.devilninja_beta.a.l) r1
            int r3 = r1.e
            r4 = 2
            if (r3 == r4) goto L_0x029e
            int r3 = r1.e
            if (r3 == 0) goto L_0x029e
            com.droidstudio.game.devilninja_beta.a.i r3 = r14.c
            boolean r3 = r3.a(r1)
            if (r3 == 0) goto L_0x029e
            r14.a(r1)
            goto L_0x029e
        L_0x0a9f:
            java.lang.Object r1 = r2.next()
            com.droidstudio.game.devilninja_beta.a.a r1 = (com.droidstudio.game.devilninja_beta.a.a) r1
            int r3 = r1.b()
            r4 = 1
            if (r3 != r4) goto L_0x02aa
            com.droidstudio.game.devilninja_beta.a.i r3 = r14.c
            boolean r3 = r3.a(r1)
            if (r3 == 0) goto L_0x02aa
            r14.a(r1)
            goto L_0x02aa
        L_0x0ab9:
            r1 = 1
            goto L_0x02d0
        L_0x0abc:
            com.droidstudio.game.devilninja_beta.a.f r1 = new com.droidstudio.game.devilninja_beta.a.f
            r3 = 195(0xc3, float:2.73E-43)
            r4 = 250(0xfa, float:3.5E-43)
            r5 = -1
            r6 = 2
            r7 = 0
            r8 = 0
            r9 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            java.util.LinkedList r3 = r14.d
            r3.add(r1)
            int r1 = r2 + 250
            int r2 = r11 + 1
            r11 = r2
            r2 = r1
            goto L_0x02e7
        L_0x0ad7:
            java.util.LinkedList r1 = r14.d
            java.lang.Object r1 = r1.getLast()
            com.droidstudio.game.devilninja_beta.a.f r1 = (com.droidstudio.game.devilninja_beta.a.f) r1
            int r2 = r1.b()
            int r1 = r1.c()
            r3 = 195(0xc3, float:2.73E-43)
            if (r1 == 0) goto L_0x0aef
            r4 = 195(0xc3, float:2.73E-43)
            if (r1 != r4) goto L_0x0b4b
        L_0x0aef:
            java.util.Random r1 = r14.i
            r4 = 3
            int r1 = r1.nextInt(r4)
            switch(r1) {
                case 0: goto L_0x0b40;
                case 1: goto L_0x0b44;
                case 2: goto L_0x0b48;
                default: goto L_0x0af9;
            }
        L_0x0af9:
            r1 = r3
        L_0x0afa:
            r3 = r1
        L_0x0afb:
            java.util.Random r1 = r14.i
            r4 = 100
            int r1 = r1.nextInt(r4)
            int r1 = r1 + 40
            int r2 = r2 + r1
            com.droidstudio.game.devilninja_beta.a.f r1 = new com.droidstudio.game.devilninja_beta.a.f
            r4 = 90
            r5 = 185(0xb9, float:2.59E-43)
            r6 = 1
            r7 = 0
            r8 = 0
            r9 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            java.util.LinkedList r4 = r14.d
            r4.add(r1)
            int r1 = r2 + 90
            java.util.Random r2 = r14.i
            int r4 = r14.y
            int r2 = r2.nextInt(r4)
            int r10 = r2 + 1
            r2 = 0
            r4 = 0
            r11 = r4
            r13 = r1
            r1 = r2
            r2 = r13
        L_0x0b2a:
            if (r11 < r10) goto L_0x0b76
            com.droidstudio.game.devilninja_beta.a.f r1 = new com.droidstudio.game.devilninja_beta.a.f
            r4 = 90
            r5 = 185(0xb9, float:2.59E-43)
            r6 = 3
            r7 = 0
            r8 = 0
            r9 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            java.util.LinkedList r2 = r14.d
            r2.add(r1)
            goto L_0x02fc
        L_0x0b40:
            r1 = 180(0xb4, float:2.52E-43)
            r3 = r1
            goto L_0x0afb
        L_0x0b44:
            r1 = 195(0xc3, float:2.73E-43)
            r3 = r1
            goto L_0x0afb
        L_0x0b48:
            r1 = 210(0xd2, float:2.94E-43)
            goto L_0x0afa
        L_0x0b4b:
            r4 = 180(0xb4, float:2.52E-43)
            if (r1 != r4) goto L_0x0b63
            java.util.Random r1 = r14.i
            r4 = 2
            int r1 = r1.nextInt(r4)
            switch(r1) {
                case 0: goto L_0x0b5c;
                case 1: goto L_0x0b60;
                default: goto L_0x0b59;
            }
        L_0x0b59:
            r1 = r3
        L_0x0b5a:
            r3 = r1
            goto L_0x0afb
        L_0x0b5c:
            r1 = 180(0xb4, float:2.52E-43)
            r3 = r1
            goto L_0x0afb
        L_0x0b60:
            r1 = 195(0xc3, float:2.73E-43)
            goto L_0x0b5a
        L_0x0b63:
            java.util.Random r1 = r14.i
            r4 = 2
            int r1 = r1.nextInt(r4)
            switch(r1) {
                case 0: goto L_0x0b6e;
                case 1: goto L_0x0b72;
                default: goto L_0x0b6d;
            }
        L_0x0b6d:
            goto L_0x0afb
        L_0x0b6e:
            r1 = 195(0xc3, float:2.73E-43)
            r3 = r1
            goto L_0x0afb
        L_0x0b72:
            r1 = 210(0xd2, float:2.94E-43)
            r3 = r1
            goto L_0x0afb
        L_0x0b76:
            if (r1 != 0) goto L_0x0bcc
            if (r11 <= 0) goto L_0x0bcc
            r4 = 1
            int r4 = r10 - r4
            if (r11 >= r4) goto L_0x0bcc
            com.droidstudio.game.devilninja_beta.a.i r4 = r14.c
            int r4 = r4.b()
            r5 = 5
            if (r4 != r5) goto L_0x0bba
            r4 = 0
        L_0x0b89:
            if (r4 == 0) goto L_0x0dee
            r1 = 1
            r12 = r1
            r8 = r4
        L_0x0b8e:
            java.util.Random r1 = r14.i
            r4 = 30
            int r1 = r1.nextInt(r4)
            int r7 = r1 + 20
            java.util.Random r1 = r14.i
            r4 = 20
            int r1 = r1.nextInt(r4)
            int r9 = r1 + 5
            com.droidstudio.game.devilninja_beta.a.f r1 = new com.droidstudio.game.devilninja_beta.a.f
            r4 = 250(0xfa, float:3.5E-43)
            r5 = 185(0xb9, float:2.59E-43)
            r6 = 2
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            java.util.LinkedList r4 = r14.d
            r4.add(r1)
            int r1 = r2 + 250
            int r2 = r11 + 1
            r11 = r2
            r2 = r1
            r1 = r12
            goto L_0x0b2a
        L_0x0bba:
            java.util.Random r4 = r14.i
            r5 = 6
            int r4 = r4.nextInt(r5)
            switch(r4) {
                case 0: goto L_0x0bc6;
                case 1: goto L_0x0bc8;
                case 2: goto L_0x0bca;
                default: goto L_0x0bc4;
            }
        L_0x0bc4:
            r4 = 0
            goto L_0x0b89
        L_0x0bc6:
            r4 = 1
            goto L_0x0b89
        L_0x0bc8:
            r4 = 2
            goto L_0x0b89
        L_0x0bca:
            r4 = 3
            goto L_0x0b89
        L_0x0bcc:
            r1 = 0
            r4 = 0
            r12 = r4
            r8 = r1
            goto L_0x0b8e
        L_0x0bd1:
            r2 = 0
            java.util.LinkedList r1 = r14.e
            int r1 = r1.size()
            r3 = 6
            if (r1 >= r3) goto L_0x0deb
            r3 = 0
            java.util.LinkedList r1 = r14.d
            java.util.Iterator r4 = r1.iterator()
        L_0x0be2:
            boolean r1 = r4.hasNext()
            if (r1 != 0) goto L_0x0c00
            r1 = r3
        L_0x0be9:
            if (r1 == 0) goto L_0x0deb
            long r1 = java.lang.System.currentTimeMillis()
            long r3 = r14.H
            long r1 = r1 - r3
            r3 = 3000(0xbb8, double:1.482E-320)
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 < 0) goto L_0x0c22
            r1 = 1
        L_0x0bf9:
            if (r1 != 0) goto L_0x0c32
            r2 = 5
            r14.l = r2
            goto L_0x0309
        L_0x0c00:
            java.lang.Object r1 = r4.next()
            com.droidstudio.game.devilninja_beta.a.f r1 = (com.droidstudio.game.devilninja_beta.a.f) r1
            int r5 = r1.g()
            r6 = 2
            if (r5 == r6) goto L_0x0c14
            int r5 = r1.g()
            r6 = 3
            if (r5 != r6) goto L_0x0be2
        L_0x0c14:
            int r1 = r1.a()
            com.droidstudio.game.devilninja_beta.a.j r5 = r14.g
            int r5 = r5.c
            int r5 = r5 + 1
            if (r1 <= r5) goto L_0x0be2
            r1 = 1
            goto L_0x0be9
        L_0x0c22:
            java.util.Random r1 = r14.i
            int r2 = r14.p()
            int r1 = r1.nextInt(r2)
            if (r1 != 0) goto L_0x0c30
            r1 = 1
            goto L_0x0bf9
        L_0x0c30:
            r1 = 0
            goto L_0x0bf9
        L_0x0c32:
            r2 = 0
            r14.l = r2
            long r2 = java.lang.System.currentTimeMillis()
            r14.H = r2
            goto L_0x0309
        L_0x0c3d:
            r1 = 0
            r2 = r1
            goto L_0x031b
        L_0x0c41:
            r1 = 1
            r2 = r1
            goto L_0x031b
        L_0x0c45:
            r1 = 1
            r2 = r1
            goto L_0x031b
        L_0x0c49:
            com.droidstudio.game.devilninja_beta.a.j r1 = r14.g
            int r1 = r1.c
            int r1 = r1 + 10
            r3 = r1
            goto L_0x032e
        L_0x0c52:
            java.lang.Object r1 = r5.next()
            com.droidstudio.game.devilninja_beta.a.f r1 = (com.droidstudio.game.devilninja_beta.a.f) r1
            if (r2 != 0) goto L_0x0c73
            int r6 = r1.g()
            r7 = 2
            if (r6 != r7) goto L_0x0335
            int r6 = r1.a()
            com.droidstudio.game.devilninja_beta.a.j r7 = r14.g
            int r7 = r7.c
            r8 = 5
            int r7 = r7 - r8
            if (r6 <= r7) goto L_0x0335
            int r1 = r1.f()
            goto L_0x033c
        L_0x0c73:
            int r6 = r1.g()
            r7 = 2
            if (r6 != r7) goto L_0x0335
            int r6 = r1.a()
            com.droidstudio.game.devilninja_beta.a.j r7 = r14.g
            int r7 = r7.c
            r8 = 110(0x6e, float:1.54E-43)
            int r7 = r7 - r8
            if (r6 <= r7) goto L_0x0335
            int r1 = r1.f()
            goto L_0x033c
        L_0x0c8d:
            r1 = 0
            java.util.LinkedList r2 = r14.j
            int r2 = r2.size()
            r3 = 8
            if (r2 >= r3) goto L_0x0ca6
            long r1 = java.lang.System.currentTimeMillis()
            long r3 = r14.I
            long r1 = r1 - r3
            r3 = 2600(0xa28, double:1.2846E-320)
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 < 0) goto L_0x0cad
            r1 = 1
        L_0x0ca6:
            if (r1 != 0) goto L_0x0cbd
            r2 = 5
            r14.r = r2
            goto L_0x036b
        L_0x0cad:
            java.util.Random r1 = r14.i
            int r2 = r14.p()
            int r1 = r1.nextInt(r2)
            if (r1 != 0) goto L_0x0cbb
            r1 = 1
            goto L_0x0ca6
        L_0x0cbb:
            r1 = 0
            goto L_0x0ca6
        L_0x0cbd:
            r2 = 0
            r14.r = r2
            long r2 = java.lang.System.currentTimeMillis()
            r14.I = r2
            goto L_0x036b
        L_0x0cc8:
            int r1 = r14.t
            r2 = 1
            if (r1 != r2) goto L_0x0cd0
            r1 = 6
            goto L_0x0372
        L_0x0cd0:
            r1 = 9
            goto L_0x0372
        L_0x0cd4:
            r1 = 2
            goto L_0x037c
        L_0x0cd7:
            r1 = 1
            goto L_0x037c
        L_0x0cda:
            r1 = 3
            goto L_0x037c
        L_0x0cdd:
            r2 = 2
            if (r1 != r2) goto L_0x0cf5
            com.droidstudio.game.devilninja_beta.a.j r2 = r14.g
            int r2 = r2.c
            int r2 = r2 + 10
            java.util.Random r3 = r14.i
            r4 = 70
            int r3 = r3.nextInt(r4)
            int r3 = r3 + 20
            r13 = r3
            r3 = r2
            r2 = r13
            goto L_0x0396
        L_0x0cf5:
            com.droidstudio.game.devilninja_beta.a.j r2 = r14.g
            int r2 = r2.c
            int r3 = r14.n
            int r2 = r2 - r3
            int r2 = r2 + 10
            java.util.Random r3 = r14.i
            r4 = 10
            int r3 = r3.nextInt(r4)
            int r2 = r2 + r3
            java.util.Random r3 = r14.i
            r4 = 60
            int r3 = r3.nextInt(r4)
            int r3 = r3 + 20
            r13 = r3
            r3 = r2
            r2 = r13
            goto L_0x0396
        L_0x0d16:
            r1 = 0
            goto L_0x03a5
        L_0x0d19:
            r1 = 2
            goto L_0x03b8
        L_0x0d1c:
            r1 = 3
            goto L_0x03b8
        L_0x0d1f:
            r1 = 4
            goto L_0x03b8
        L_0x0d22:
            r1 = 1
            int r2 = r14.E
            r3 = 6
            if (r2 < r3) goto L_0x03b8
            r1 = 4
            goto L_0x03b8
        L_0x0d2b:
            r1 = 5
            goto L_0x03b8
        L_0x0d2e:
            r1 = 6
            int r1 = r14.b(r1)
            if (r1 != 0) goto L_0x0d38
            r1 = 2
            goto L_0x03b8
        L_0x0d38:
            r2 = 1
            if (r1 == r2) goto L_0x0d41
            r2 = 2
            if (r1 == r2) goto L_0x0d41
            r2 = 3
            if (r1 != r2) goto L_0x0d44
        L_0x0d41:
            r1 = 4
            goto L_0x03b8
        L_0x0d44:
            r1 = 5
            goto L_0x03b8
        L_0x0d47:
            r1 = 4
            int r1 = r14.b(r1)
            if (r1 != 0) goto L_0x0d51
            r1 = 4
            goto L_0x03b8
        L_0x0d51:
            r2 = 1
            if (r1 != r2) goto L_0x0d57
            r1 = 1
            goto L_0x03b8
        L_0x0d57:
            r2 = 2
            if (r1 != r2) goto L_0x0d5d
            r1 = 3
            goto L_0x03b8
        L_0x0d5d:
            r1 = 5
            goto L_0x03b8
        L_0x0d60:
            com.droidstudio.game.devilninja_beta.a.d r2 = r14.z
            int r2 = r2.f()
            r3 = 1
            if (r2 != r3) goto L_0x0d6c
            r1 = 4
            goto L_0x03c1
        L_0x0d6c:
            com.droidstudio.game.devilninja_beta.a.d r2 = r14.z
            int r2 = r2.f()
            r3 = 2
            if (r2 != r3) goto L_0x0d78
            r1 = 2
            goto L_0x03c1
        L_0x0d78:
            com.droidstudio.game.devilninja_beta.a.d r2 = r14.z
            int r2 = r2.f()
            r3 = 4
            if (r2 != r3) goto L_0x03c1
            r1 = 5
            goto L_0x03c1
        L_0x0d84:
            java.util.Random r2 = r14.i
            r3 = 100
            int r2 = r2.nextInt(r3)
            int r2 = r2 + 430
            r3 = 5
            if (r1 != r3) goto L_0x0da0
            r3 = 110(0x6e, float:1.54E-43)
        L_0x0d93:
            com.droidstudio.game.devilninja_beta.a.d r4 = r14.z
            r4.a(r1, r2, r3)
            int r1 = r14.A
            int r1 = r1 + 1
            r14.A = r1
            goto L_0x03c8
        L_0x0da0:
            r3 = 130(0x82, float:1.82E-43)
            java.util.Random r4 = r14.i
            r5 = 30
            int r4 = r4.nextInt(r5)
            int r3 = r3 - r4
            goto L_0x0d93
        L_0x0dac:
            r3 = 2000(0x7d0, float:2.803E-42)
            if (r1 <= r3) goto L_0x0db7
            r3 = 4000(0xfa0, float:5.605E-42)
            if (r1 >= r3) goto L_0x0db7
            r1 = 3
            goto L_0x03dc
        L_0x0db7:
            r3 = 4000(0xfa0, float:5.605E-42)
            if (r1 <= r3) goto L_0x0dc2
            r3 = 6000(0x1770, float:8.408E-42)
            if (r1 >= r3) goto L_0x0dc2
            r1 = 4
            goto L_0x03dc
        L_0x0dc2:
            r3 = 6000(0x1770, float:8.408E-42)
            if (r1 <= r3) goto L_0x0dcd
            r3 = 9000(0x2328, float:1.2612E-41)
            if (r1 >= r3) goto L_0x0dcd
            r1 = 5
            goto L_0x03dc
        L_0x0dcd:
            r3 = 9000(0x2328, float:1.2612E-41)
            if (r1 <= r3) goto L_0x0dd8
            r3 = 12000(0x2ee0, float:1.6816E-41)
            if (r1 >= r3) goto L_0x0dd8
            r1 = 6
            goto L_0x03dc
        L_0x0dd8:
            r3 = 12000(0x2ee0, float:1.6816E-41)
            if (r1 <= r3) goto L_0x0de8
            r1 = 8
            goto L_0x03dc
        L_0x0de0:
            int r1 = r14.u
            int r1 = r1 + 1000
            r14.u = r1
            goto L_0x0417
        L_0x0de8:
            r1 = r2
            goto L_0x03dc
        L_0x0deb:
            r1 = r2
            goto L_0x0bf9
        L_0x0dee:
            r12 = r1
            r8 = r4
            goto L_0x0b8e
        L_0x0df2:
            r1 = r2
            goto L_0x0221
        */
        throw new UnsupportedOperationException("Method not decompiled: com.droidstudio.game.devilninja_beta.a.e.a():void");
    }

    public final void a(int i2, int i3) {
        if (i2 >= this.h.a(2) - 60 && i3 >= this.h.b(2) - 30 && i3 <= this.h.d(2) + 60) {
            Log.v("UIModel", "handleLongPress(Fire)");
            this.h.w();
        }
    }

    public final int b(int i2, int i3) {
        if (this.f == 2) {
            this.f = 1;
            return 4;
        } else if (i2 <= this.h.c(3) + 10 && i3 <= this.h.d(3) + 60) {
            this.f = 2;
            this.h.e(3);
            Log.v("UIModel", "handlePressButton(Pause)");
            g.a(3);
            return 3;
        } else if (i2 >= this.h.a(5) - 20 && i2 <= this.h.c(5) + 60 && i3 >= this.h.b(5) && i3 <= this.h.d(5) + 60) {
            if (this.h.d() == 1) {
                this.h.e(5);
                a(1);
            } else {
                this.h.f(5);
                a(0);
            }
            Log.v("UIModel", "handlePressButton(Pause)");
            g.a(16);
            return 5;
        } else if (i2 <= this.h.c(1) + 100 && i3 >= this.h.b(1) - 100 && i3 <= this.h.d(1) + 100) {
            this.h.e(1);
            int b2 = this.c.b();
            if ((b2 == 1 || b2 == 2 || b2 == 3) && this.c.o() < 2) {
                this.c.a(2);
                g.a(14);
            }
            Log.v("UIModel", "handlePressButton(Jump)");
            return 1;
        } else if (i2 < this.h.a(2) - 130 || i3 < this.h.b(2) - 130 || i3 > this.h.d(2) + 100) {
            return 0;
        } else {
            this.h.e(2);
            Log.v("UIModel", "handlePressButton(Fire)");
            a(false);
            return 2;
        }
    }

    public final void b() {
        this.h.f(3);
        this.h.f(1);
        this.h.f(2);
        if (this.h.A()) {
            this.h.x();
            a(true);
        }
    }

    public final LinkedList c() {
        return this.j;
    }

    public final h d() {
        return this.h;
    }

    public final d e() {
        return this.z;
    }

    public final int f() {
        return this.D / 10;
    }

    public final int g() {
        return this.E;
    }

    public final m h() {
        return this.B;
    }

    public final b i() {
        return this.C;
    }

    public final int j() {
        return this.J;
    }

    public final int k() {
        return this.x;
    }

    public final n l() {
        return this.K;
    }
}
