package com.droidstudio.game.devilninja_beta;

import android.content.DialogInterface;
import android.content.Intent;

final class k implements DialogInterface.OnClickListener {
    private /* synthetic */ MainActivity a;

    k(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        MainActivity mainActivity = this.a;
        mainActivity.startActivity(new Intent(mainActivity, MenuActivity.class));
        this.a.finish();
    }
}
