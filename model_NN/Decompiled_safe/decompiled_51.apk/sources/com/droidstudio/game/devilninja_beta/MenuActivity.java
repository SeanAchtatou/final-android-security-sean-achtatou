package com.droidstudio.game.devilninja_beta;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import com.a.a.c;
import com.droidstudio.game.devilninja_beta.a.g;
import com.google.ads.R;
import com.scoreloop.android.coreui.HighscoresActivity;
import com.scoreloop.android.coreui.k;

public class MenuActivity extends Activity implements View.OnClickListener {
    private SharedPreferences a;
    private ImageView b = null;
    private g c;

    private boolean a() {
        return this.a.getBoolean("isSound", true);
    }

    public void finish() {
        Log.v("MenuActivity", "finish()...now");
        super.finish();
    }

    public void onClick(View view) {
        g.a(1);
        Intent intent = null;
        switch (view.getId()) {
            case R.id.btn_more /*2131099654*/:
                intent = new Intent("android.intent.action.VIEW", Uri.parse(c.b()));
                break;
            case R.id.btn_play /*2131099672*/:
                intent = new Intent(this, TipsActivity.class);
                break;
            case R.id.btn_options /*2131099673*/:
                intent = new Intent(this, Prefs.class);
                break;
            case R.id.btn_highsocre /*2131099674*/:
                intent = new Intent(this, HighscoresActivity.class);
                break;
            case R.id.btn_exit /*2131099675*/:
                startActivity(new Intent(this, AdSplash.class));
                finish();
                return;
        }
        if (intent != null) {
            startActivity(intent);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(1024, 1024);
        ((WindowManager) getSystemService("window")).getDefaultDisplay().getHeight();
        setContentView((int) R.layout.menu_activity);
        k.a(this, "a92ace02-0aa5-41af-93d5-b1d45ea669f7", "nIiMeTc7gV5Wm/0ADUbIQ/EZvegIo6u6BUWkx8Zv2KqCCXp0qtfRqg==");
        k.a();
        ((Button) findViewById(R.id.btn_play)).setOnClickListener(this);
        ((Button) findViewById(R.id.btn_highsocre)).setOnClickListener(this);
        ((Button) findViewById(R.id.btn_more)).setOnClickListener(this);
        ((Button) findViewById(R.id.btn_options)).setOnClickListener(this);
        ((Button) findViewById(R.id.btn_exit)).setOnClickListener(this);
        this.a = getBaseContext().getSharedPreferences("com.ningo.game.action.stick", 0);
        boolean a2 = a();
        this.c = new g(this);
        this.c.a(a2);
        this.c.a();
        g.a(getBaseContext());
        c.a();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        g.a(this.c);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        boolean a2 = a();
        g.a(a2);
        boolean z = this.a.getBoolean("isVibrate", true);
        g.b(z);
        if (z) {
            Log.v("MenuActivity", "SoundManager.vibrate=true");
        } else {
            Log.v("MenuActivity", "SoundManager.vibrate=flase");
        }
        if (this.c != null) {
            this.c.a(a2);
            if (a2) {
                g.b(this.c);
            } else {
                this.c.b();
            }
        }
    }
}
