package com.droidstudio.game.devilninja_beta.a;

import android.content.Context;
import android.media.SoundPool;
import android.os.Vibrator;
import android.util.Log;
import com.google.ads.R;
import java.util.HashMap;
import java.util.Random;

public abstract class g {
    private static SoundPool a;
    private static HashMap b;
    private static Vibrator c;
    private static Random d;
    private static boolean e;
    private static boolean f;

    public static void a(int i) {
        if (i != 0) {
            if (e) {
                float f2 = i == 14 ? 2.0f : 1.0f;
                try {
                    a.play(((Integer) b.get(Integer.valueOf(i))).intValue(), f2, f2, 1, 0, 1.0f);
                } catch (Exception e2) {
                    Log.d("PlaySounds", e2.toString());
                }
            }
            if ((i == 7 || i == 1 || i == 2 || i == 12 || i == 9) && c != null && f) {
                c.vibrate(25);
            }
        }
    }

    public static void a(Context context) {
        d = new Random();
        a = new SoundPool(30, 3, 100);
        HashMap hashMap = new HashMap();
        b = hashMap;
        hashMap.put(2, Integer.valueOf(a.load(context, R.raw.ro_dead, 1)));
        b.put(1, Integer.valueOf(a.load(context, R.raw.click, 1)));
        b.put(4, Integer.valueOf(a.load(context, R.raw.scene_change, 1)));
        b.put(3, Integer.valueOf(a.load(context, R.raw.pause, 1)));
        b.put(5, Integer.valueOf(a.load(context, R.raw.fire_dao, 1)));
        b.put(6, Integer.valueOf(a.load(context, R.raw.fire_feibiao, 1)));
        b.put(7, Integer.valueOf(a.load(context, R.raw.fire_power, 1)));
        b.put(8, Integer.valueOf(a.load(context, R.raw.enemy_dead, 1)));
        b.put(9, Integer.valueOf(a.load(context, R.raw.food, 1)));
        b.put(10, Integer.valueOf(a.load(context, R.raw.power, 1)));
        b.put(11, Integer.valueOf(a.load(context, R.raw.power_line, 1)));
        b.put(12, Integer.valueOf(a.load(context, R.raw.ro_hurt, 1)));
        b.put(13, Integer.valueOf(a.load(context, R.raw.meterwarn, 1)));
        b.put(14, Integer.valueOf(a.load(context, R.raw.jump, 1)));
        b.put(15, Integer.valueOf(a.load(context, R.raw.landon, 1)));
        b.put(16, Integer.valueOf(a.load(context, R.raw.speed, 1)));
        b.put(17, Integer.valueOf(a.load(context, R.raw.dragon_create, 1)));
        if (c == null) {
            c = (Vibrator) context.getSystemService("vibrator");
        }
        e = true;
        f = true;
    }

    public static void a(boolean z) {
        e = z;
    }

    public static boolean a() {
        return e;
    }

    public static void b(boolean z) {
        f = z;
    }
}
