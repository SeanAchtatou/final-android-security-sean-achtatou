package com.droidstudio.game.devilninja_beta.a;

import android.graphics.Point;

public final class h {
    private static int R = 5;
    public static int a = 50;
    public static int b = 25;
    public static int c = 25;
    public static int d = 25;
    public static int e = 32;
    public static int f = 127;
    public static int g = 10;
    public static int h = 22;
    public static int i = 220;
    public static int j = 22;
    public static int k = 22;
    public static int l = 20;
    public static int m = 23;
    public static int n = 40;
    private int A;
    private int B;
    private int C;
    private int D;
    private int E;
    private int F;
    private Point G;
    private Point H;
    private int I;
    private int J;
    private boolean K;
    private int L = 4;
    private int M;
    private int N;
    private int O;
    private int P;
    private int Q;
    private int S;
    private int T;
    private int U = 3;
    private boolean V = false;
    private int W = 0;
    private int X;
    private int Y;
    private final int Z = 8;
    private final int aa = 3;
    private int ab;
    private Point ac;
    private int ad;
    private int ae;
    private int af;
    private int ag;
    private int ah;
    private int ai;
    private int aj;
    private boolean ak = false;
    private int al;
    private int am;
    private boolean an;
    private float ao;
    private int ap;
    private int aq;
    private int ar;
    private int as;
    private j o;
    private long p;
    private boolean q = false;
    private int r;
    private int s;
    private int t = 0;
    private int u;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;

    public h(j jVar) {
        this.o = jVar;
        this.p = 0;
        this.u = this.o.a + 40;
        this.v = this.o.d - 70;
        this.w = this.o.c - 100;
        this.x = this.o.d - 70;
        this.y = this.o.a + 10;
        this.z = this.o.b + 5;
        this.A = this.o.a + 80;
        this.B = this.o.b + 5;
        this.C = 1;
        this.D = 1;
        this.E = 1;
        this.F = 1;
        this.N = 0;
        this.M = 0;
        this.S = (this.o.c - f) - g;
        this.T = R;
        this.ab = 0;
        this.ac = new Point();
        this.G = new Point(160, 8);
        this.H = new Point((this.o.c - g) - m, n);
        this.ah = 0;
        this.ai = 3;
        this.aj = 0;
        this.I = 0;
        this.J = 0;
        this.K = true;
    }

    public final boolean A() {
        return this.ak;
    }

    public final int B() {
        return this.w - 35;
    }

    public final int C() {
        return this.x - 35;
    }

    public final int D() {
        return (this.w - 35) + 130;
    }

    public final int E() {
        return (this.x - 35) + 130;
    }

    public final Point F() {
        return this.G;
    }

    public final void G() {
        this.an = true;
        this.aq = 0;
        this.ap = 2;
        this.ao = 0.8f;
        this.ar = 3;
        this.as = 90;
    }

    public final float H() {
        if (!this.an) {
            this.ao = 0.0f;
            return this.ao;
        }
        this.aq++;
        if (this.aq >= this.ar) {
            if (this.ap != 1) {
                this.as--;
                if (this.as > 0) {
                    if (this.ao == 0.8f) {
                        this.ao = -0.8f;
                    } else if (this.ao == -0.8f) {
                        this.ao = 0.8f;
                    }
                }
            } else if (this.ao == 2.5f) {
                this.ao = -2.4f;
            } else if (this.ao == -2.4f) {
                this.ao = 2.3f;
            } else if (this.ao == 2.3f) {
                this.ao = -2.2f;
            } else if (this.ao == -2.2f) {
                this.ao = 2.0f;
            } else if (this.ao == 2.0f) {
                this.ao = -1.8f;
            } else if (this.ao == -1.8f) {
                this.ao = 1.5f;
            } else if (this.ao == 1.5f) {
                this.ao = -1.2f;
            } else if (this.ao == -1.2f) {
                this.ao = 1.0f;
            } else if (this.ao == 1.0f) {
                this.ao = -0.5f;
            } else if (this.ao != -0.5f) {
                if (this.ao == 0.0f) {
                    this.an = false;
                }
            }
            this.ao = 0.0f;
            this.an = false;
        }
        return this.ao;
    }

    public final boolean I() {
        return this.an;
    }

    public final int a(int i2) {
        if (i2 != 1) {
            if (i2 == 2) {
                return this.w;
            }
            if (i2 == 3) {
                return this.y;
            }
            if (i2 == 5) {
                return this.A;
            }
        }
        return this.u;
    }

    public final void a() {
        this.p++;
    }

    public final void a(int i2, int i3, int i4) {
        int i5;
        int i6;
        this.ab = i2;
        this.ac.x = i3;
        this.ac.y = i4;
        if (i2 == 1) {
            int i7 = this.H.x;
            i5 = i7;
            i6 = this.H.y;
        } else {
            int i8 = this.G.x;
            i5 = i8;
            i6 = this.G.y;
        }
        this.ad = (i3 - i5) / 8;
        this.ae = (i4 - i6) / 8;
        this.af = 0;
        this.ag = 0;
    }

    public final void a(boolean z2) {
        this.V = z2;
        if (z2) {
            this.W = this.o.d;
            this.X = 1;
        }
    }

    public final int b(int i2) {
        if (i2 != 1) {
            if (i2 == 2) {
                return this.x;
            }
            if (i2 == 3) {
                return this.z;
            }
            if (i2 == 5) {
                return this.B;
            }
        }
        return this.v;
    }

    public final void b() {
        if (this.p >= 50 && this.p <= 300) {
            if (this.I > 3) {
                this.K = false;
            } else if (this.F == 2) {
                this.I = 4;
                this.K = false;
            } else {
                this.J++;
                if (this.J >= this.U) {
                    this.J = 0;
                    this.K = !this.K;
                    if (this.K) {
                        this.U = 25;
                        this.I++;
                        return;
                    }
                    this.U = 5;
                }
            }
        }
    }

    public final int c(int i2) {
        if (i2 != 1) {
            if (i2 == 2) {
                return this.w + 60;
            }
            if (i2 == 3) {
                return this.y + 30;
            }
            if (i2 == 5) {
                return this.A + 30;
            }
        }
        return this.u + 60;
    }

    public final boolean c() {
        return this.K;
    }

    public final int d() {
        return this.F;
    }

    public final int d(int i2) {
        if (i2 != 1) {
            if (i2 == 2) {
                return this.x + 60;
            }
            if (i2 == 3) {
                return this.z + 30;
            }
            if (i2 == 5) {
                return this.B + 30;
            }
        }
        return this.v + 60;
    }

    public final void e() {
        this.N = 0;
        this.M = 1;
    }

    public final void e(int i2) {
        if (i2 == 1) {
            this.C = 2;
            this.D = 1;
        } else if (i2 == 2) {
            this.D = 2;
            this.C = 1;
        } else if (i2 == 3) {
            this.E = 2;
            this.D = 1;
            this.C = 1;
        } else if (i2 == 5) {
            this.F = 2;
            this.D = 1;
            this.C = 1;
        }
    }

    public final int f() {
        if (this.M == 0) {
            return 0;
        }
        this.N++;
        if (this.N == 4) {
            this.N = 0;
            switch (this.M) {
                case 1:
                    this.M = 2;
                    break;
                case 2:
                    this.M = 0;
                    break;
                default:
                    this.M = 0;
                    break;
            }
        }
        return this.M;
    }

    public final void f(int i2) {
        if (i2 == 1) {
            this.C = 1;
        } else if (i2 == 2) {
            this.D = 1;
        } else if (i2 == 3) {
            this.E = 1;
        } else if (i2 == 5) {
            this.F = 1;
        }
    }

    public final int g(int i2) {
        if (i2 == 1) {
            if (this.C != 1) {
                return 2;
            }
        } else if (i2 == 2) {
            if (this.D != 1) {
                return 2;
            }
        } else if (i2 != 3) {
            if (i2 != 5) {
                return 0;
            }
            if (this.F == 1) {
                return 2;
            }
        }
        return 1;
    }

    public final void g() {
        this.q = true;
        this.r = 1;
        this.s = 0;
        this.t++;
    }

    public final void h(int i2) {
        this.Q = i2;
        this.P = 1;
    }

    public final boolean h() {
        return this.q;
    }

    public final int i() {
        return this.t;
    }

    public final int j() {
        return this.S;
    }

    public final int k() {
        return this.T;
    }

    public final int l() {
        if (this.P == 0) {
            return 0;
        }
        int i2 = this.O;
        this.O = i2 + 1;
        if (i2 == 5) {
            this.O = 0;
            this.P = this.P == 1 ? 2 : this.P == 2 ? 3 : this.P == 3 ? 4 : this.P == 4 ? 5 : this.P == 5 ? 0 : 0;
        }
        return this.P;
    }

    public final int m() {
        return this.Q;
    }

    public final boolean n() {
        return this.V;
    }

    public final int o() {
        return this.W;
    }

    public final void p() {
        if (this.V) {
            this.W -= 3;
            if (this.W <= 0) {
                this.V = false;
                this.X = 0;
                this.Y = 0;
            }
        }
    }

    public final boolean q() {
        return this.ab != 0;
    }

    public final void r() {
        if (q()) {
            this.af++;
            if (this.af >= 3) {
                this.af = 0;
                int i2 = this.ag;
                this.ag = i2 + 1;
                if (i2 == 8) {
                    this.ab = 0;
                    return;
                }
                this.ac.y -= this.ae;
                this.ac.x -= this.ad;
            }
        }
    }

    public final Point s() {
        return this.ac;
    }

    public final int t() {
        return ((8 - this.ag) * 15) + 120;
    }

    public final int u() {
        return this.ab;
    }

    public final int v() {
        if (!this.ak) {
            this.ah = 0;
        } else if (this.ah == 0) {
            this.ah = 1;
            this.aj = 0;
        } else {
            this.aj++;
            if (this.aj >= this.ai) {
                this.aj = 0;
                if (this.ah == 1) {
                    this.ah = 2;
                } else if (this.ah == 2) {
                    this.ah = 3;
                } else if (this.ah == 3) {
                    this.ah = 4;
                } else if (this.ah == 4) {
                    this.ah = 1;
                }
            }
        }
        return this.ah;
    }

    public final void w() {
        this.al = 1;
        this.am = 0;
        this.ak = true;
    }

    public final void x() {
        this.al = 0;
        this.ak = false;
    }

    public final void y() {
        if (this.ak && this.al != 0) {
            this.am++;
            if (this.am >= 3) {
                this.am = 0;
                if (this.al == 1) {
                    this.al = 2;
                } else if (this.al == 2) {
                    this.al = 3;
                } else if (this.al == 3) {
                    this.al = 1;
                }
            }
        }
    }

    public final int z() {
        return this.al;
    }
}
