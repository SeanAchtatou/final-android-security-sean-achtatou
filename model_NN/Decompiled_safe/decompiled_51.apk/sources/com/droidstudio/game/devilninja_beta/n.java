package com.droidstudio.game.devilninja_beta;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class n extends GestureDetector.SimpleOnGestureListener {
    private /* synthetic */ MainActivity a;

    n(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public final boolean onDoubleTap(MotionEvent motionEvent) {
        return true;
    }

    public final boolean onDown(MotionEvent motionEvent) {
        int a2 = this.a.b.a((int) motionEvent.getX(), (int) motionEvent.getY());
        if (a2 == 3) {
            this.a.a(true);
            this.a.d();
        } else if (a2 == 4) {
            this.a.a(false);
            this.a.b();
        }
        return true;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        this.a.b.b();
        return true;
    }

    public final void onLongPress(MotionEvent motionEvent) {
        this.a.b.b((int) motionEvent.getX(), (int) motionEvent.getY());
    }

    public final boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        this.a.b.b();
        return true;
    }

    public final void onShowPress(MotionEvent motionEvent) {
    }

    public final boolean onSingleTapUp(MotionEvent motionEvent) {
        this.a.b.b();
        return true;
    }
}
