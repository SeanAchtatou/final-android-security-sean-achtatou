package com.droidstudio.game.devilninja_beta;

import android.media.MediaPlayer;
import com.google.ads.R;

final class g {
    private MediaPlayer a = null;
    private boolean b;
    private /* synthetic */ MenuActivity c;

    public g(MenuActivity menuActivity) {
        this.c = menuActivity;
    }

    static /* synthetic */ void a(g gVar) {
        if (gVar.a != null && gVar.a.isPlaying()) {
            gVar.a.pause();
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.a != null) {
            if (this.a.isPlaying()) {
                this.a.stop();
            }
            this.a.release();
            this.a = null;
        }
    }

    static /* synthetic */ void b(g gVar) {
        if (gVar.a != null) {
            if (gVar.b && !gVar.a.isPlaying()) {
                gVar.a.start();
            }
        } else if (gVar.b) {
            gVar.a();
        }
    }

    public final void a() {
        if (this.b) {
            b();
            if (this.a == null) {
                this.a = MediaPlayer.create(this.c.getBaseContext(), (int) R.raw.m_menu);
                if (this.a == null) {
                    return;
                }
            }
            this.a.start();
        }
    }

    public final void a(boolean z) {
        this.b = z;
    }
}
