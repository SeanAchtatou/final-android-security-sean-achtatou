package com.droidstudio.game.devilninja_beta.a;

import java.util.ArrayList;

public final class i extends c {
    private int e;
    private int f;
    private int g;
    private int h = 0;
    private int i;
    private int j;
    private int k;
    private int l = 0;
    private ArrayList m = new ArrayList();
    private int n;

    public final int a() {
        return this.b + 20;
    }

    public final void a(int i2) {
        this.g = 0;
        int i3 = this.e;
        if (i2 == 1) {
            this.i = 3;
            this.f = 1;
            this.n = 0;
            if (i3 == 2 || i3 == 3) {
                this.b -= -20;
                this.c = 60;
                this.d = 60;
            }
        } else if (i2 == 2) {
            this.n++;
            this.i = 7;
            this.f = 1;
            if (i3 == 1) {
                this.b -= 20;
            }
            this.c = 80;
            this.d = 80;
        } else if (i2 == 3) {
            this.i = 5;
            this.f = 4;
            this.c = 80;
            this.d = 80;
        } else if (i2 == 5) {
            this.i = 4;
            this.f = 1;
        } else if (i2 == 4) {
            this.i = 4;
            this.f = 1;
            this.c = 70;
            this.d = 70;
        }
        this.e = i2;
        this.h = 0;
    }

    public final int b() {
        return this.e;
    }

    public final void c() {
        this.g = 9;
        this.j = 0;
        this.k = 2;
        if (this.e != 1) {
            this.c = 60;
            this.d = 60;
        }
    }

    public final int d() {
        return this.f;
    }

    public final void d(int i2) {
        int i3 = -10;
        if (i2 >= -10) {
            i3 = i2;
        }
        super.d(i3);
    }

    public final int l() {
        return this.g;
    }

    public final void m() {
        if (this.g != 0) {
            this.j++;
            if (this.j >= this.k) {
                this.j = 0;
                if (this.g == 9) {
                    this.g = 10;
                } else if (this.g == 10) {
                    this.g = 11;
                } else if (this.g == 11) {
                    this.g = 0;
                    if (this.e == 2 || this.e == 3) {
                        this.c = 80;
                        this.d = 80;
                    } else if (this.e == 1) {
                        this.c = 60;
                        this.d = 60;
                    } else if (this.e == 4) {
                        this.c = 70;
                        this.d = 70;
                    }
                }
            }
        }
        this.h++;
        if (this.h >= this.i) {
            this.h = 0;
            if (this.e == 1) {
                this.i = 3;
                if (this.f == 1) {
                    this.f = 2;
                    return;
                } else if (this.f == 2) {
                    this.f = 3;
                    return;
                } else if (this.f == 3) {
                    this.f = 4;
                    return;
                } else if (this.f == 4) {
                    this.f = 5;
                    return;
                } else if (this.f == 5) {
                    this.f = 6;
                    return;
                } else if (this.f == 6) {
                    this.f = 7;
                    return;
                } else if (this.f == 7) {
                    this.f = 8;
                    return;
                } else if (this.f != 8) {
                    return;
                }
            } else if (this.e == 2) {
                if (this.f == 1) {
                    this.f = 2;
                    this.i = 5;
                    return;
                } else if (this.f == 2) {
                    this.f = 3;
                    this.i = 4;
                    return;
                } else if (this.f == 3) {
                    a(3);
                    return;
                } else {
                    return;
                }
            } else if (this.e == 3) {
                if (this.f == 4) {
                    this.f = 5;
                    this.i = 5;
                    return;
                } else if (this.f == 5) {
                    this.f = 6;
                    this.i = 6;
                    return;
                } else {
                    return;
                }
            } else if (this.e == 5) {
                if (this.f == 1) {
                    this.f = 2;
                    return;
                } else if (this.f == 2) {
                    this.f = 3;
                    return;
                } else if (this.f == 3) {
                    this.f = 4;
                    return;
                } else if (this.f == 4) {
                    this.f = 5;
                    return;
                } else if (this.f != 5) {
                    this.f = 1;
                    return;
                }
            } else if (this.e != 4) {
                return;
            } else {
                if (this.f == 1) {
                    this.f = 2;
                    return;
                } else if (this.f == 2) {
                    this.f = 3;
                    return;
                }
            }
            this.f = 1;
        }
    }

    public final void n() {
        this.a = 45;
        this.b = 75;
        this.c = 80;
        this.d = 80;
        a(30, 6, 2, 10);
        this.i = 5;
        this.e = 3;
        this.f = 4;
        this.n = 0;
    }

    public final int o() {
        return this.n;
    }
}
