package com.droidstudio.game.devilninja_beta;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import com.google.ads.R;

public class Prefs extends Activity {
    /* access modifiers changed from: private */
    public SharedPreferences a;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.options);
        this.a = getBaseContext().getSharedPreferences("com.ningo.game.action.stick", 0);
        CheckBox checkBox = (CheckBox) findViewById(R.id.options_vibrate_checkbox);
        checkBox.setChecked(this.a.getBoolean("isVibrate", true));
        checkBox.setOnCheckedChangeListener(new e(this));
        CheckBox checkBox2 = (CheckBox) findViewById(R.id.options_sounds_checkbox);
        checkBox2.setChecked(this.a.getBoolean("isSound", true));
        checkBox2.setOnCheckedChangeListener(new h(this));
        CheckBox checkBox3 = (CheckBox) findViewById(R.id.options_fast_checkbox);
        checkBox3.setChecked(this.a.getInt("isFast", 0) == 1);
        checkBox3.setOnCheckedChangeListener(new f(this));
        ((Button) findViewById(R.id.btn_back)).setOnClickListener(new i(this));
    }
}
