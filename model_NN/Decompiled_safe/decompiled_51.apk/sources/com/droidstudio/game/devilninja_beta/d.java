package com.droidstudio.game.devilninja_beta;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

final class d extends Handler {
    private /* synthetic */ GameView a;

    d(GameView gameView) {
        this.a = gameView;
    }

    public final void handleMessage(Message message) {
        int i = message.getData().getInt("GAME_OVER");
        int i2 = message.getData().getInt("GAME_PARAM_SPEED");
        if (this.a.b != null) {
            GameView.a(this.a, i, i2);
            ((MainActivity) this.a.d).finish();
            return;
        }
        Log.v("View", "mParentActivity is null");
    }
}
