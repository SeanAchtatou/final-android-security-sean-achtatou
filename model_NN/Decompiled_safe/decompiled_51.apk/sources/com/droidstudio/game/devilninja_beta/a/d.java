package com.droidstudio.game.devilninja_beta.a;

public final class d {
    public int a;
    public int b;
    private int c;
    private int d;
    private int e;

    public d() {
        a(0, 0, 0);
    }

    public final void a() {
        this.a = 0;
        this.b = 250;
    }

    public final void a(int i) {
        this.d += i;
    }

    public final void a(int i, int i2, int i3) {
        this.a = i;
        this.b = 250;
        this.d = i2;
        this.e = i3;
        if (i != 0) {
            this.c++;
        }
    }

    public final int b() {
        return this.d;
    }

    public final int c() {
        return this.e;
    }

    public final int d() {
        return this.d + 30;
    }

    public final int e() {
        return this.e + 30;
    }

    public final int f() {
        return this.c;
    }
}
