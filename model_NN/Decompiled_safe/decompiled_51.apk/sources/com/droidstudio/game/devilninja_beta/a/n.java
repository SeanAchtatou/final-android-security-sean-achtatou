package com.droidstudio.game.devilninja_beta.a;

import java.util.Random;

public final class n extends c {
    private int e = 0;
    private int f = 100;
    private int g = 0;
    private int h;
    private boolean i = false;
    private Random j = new Random();

    public n() {
        this.a = 0;
        this.b = 0;
        this.c = 55;
        this.d = 55;
    }

    public final void a() {
        if (!this.i) {
            this.e++;
            if (this.e > this.f && this.j.nextInt(2) == 0) {
                this.g = 0;
                this.i = true;
                this.h = 1;
                this.a = this.j.nextInt(300) + 100;
                this.b = this.j.nextInt(15) + 10;
                return;
            }
            return;
        }
        this.g++;
        if (this.g >= 3) {
            this.g = 0;
            if (this.h == 1) {
                this.h = 0;
            } else if (this.h == 0) {
                this.h = 2;
            } else {
                this.i = false;
                this.h = 0;
                this.e = 0;
            }
        }
    }

    public final int b() {
        return this.h;
    }
}
