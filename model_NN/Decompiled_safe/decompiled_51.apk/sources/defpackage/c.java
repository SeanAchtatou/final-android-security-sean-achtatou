package defpackage;

import android.webkit.WebView;

/* renamed from: c  reason: default package */
final class c implements Runnable {
    private final String a;
    private final String b;
    private final WebView c;
    private /* synthetic */ k d;

    public c(k kVar, WebView webView, String str, String str2) {
        this.d = kVar;
        this.c = webView;
        this.a = str;
        this.b = str2;
    }

    public final void run() {
        this.c.loadDataWithBaseURL(this.a, this.b, "text/html", "utf-8", null);
    }
}
