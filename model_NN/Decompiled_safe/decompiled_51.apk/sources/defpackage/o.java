package defpackage;

import android.webkit.WebView;
import com.google.ads.util.d;
import java.util.HashMap;

/* renamed from: o  reason: default package */
public final class o implements t {
    public final void a(h hVar, HashMap hashMap, WebView webView) {
        if (webView instanceof g) {
            ((g) webView).a();
        } else {
            d.b("Trying to close WebView that isn't an AdWebView");
        }
    }
}
