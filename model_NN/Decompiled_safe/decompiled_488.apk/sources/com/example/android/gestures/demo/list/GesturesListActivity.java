package com.example.android.gestures.demo.list;

import android.app.ListActivity;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.os.Bundle;
import android.provider.Contacts;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import com.THOMSONROGERS.R;
import java.util.ArrayList;

public class GesturesListActivity extends ListActivity implements GestureOverlayView.OnGesturePerformedListener {
    private GestureLibrary mLibrary;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.gestmain);
        setListAdapter(new SimpleCursorAdapter(this, 17367043, managedQuery(Contacts.People.CONTENT_URI, new String[]{"_id", "display_name"}, null, null, "name ASC"), new String[]{"display_name"}, new int[]{16908308}));
        this.mLibrary = GestureLibraries.fromRawResource(this, R.raw.actions);
        if (!this.mLibrary.load()) {
            finish();
        }
        ((GestureOverlayView) findViewById(R.id.gestures)).addOnGesturePerformedListener(this);
    }

    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
        ArrayList<Prediction> predictions = this.mLibrary.recognize(gesture);
        if (predictions.size() > 0 && predictions.get(0).score > 1.0d) {
            String action = predictions.get(0).name;
            if ("action_add".equals(action)) {
                Toast.makeText(this, "Adding a contact", 0).show();
            } else if ("action_delete".equals(action)) {
                Toast.makeText(this, "Removing a contact", 0).show();
            } else if ("action_refresh".equals(action)) {
                Toast.makeText(this, "Reloading contacts", 0).show();
            }
        }
    }
}
