package com.PADWAY;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.THOMSONROGERS.R;

public class Contact extends Activity {
    ImageView callourlawfrim;
    TextView txt0;
    ImageView websiteicon;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.contact);
        this.txt0 = (TextView) findViewById(R.id.textView01);
        this.txt0.setText("If you would rather speak to an attorney directly about your case, please call 18882230448");
        this.callourlawfrim = (ImageView) findViewById(R.id.Imageviewcallourlawfirm);
        this.callourlawfrim.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent dial = new Intent();
                dial.setAction("android.intent.action.DIAL");
                dial.setData(Uri.parse("tel:18882230448"));
                Contact.this.startActivity(dial);
            }
        });
        this.websiteicon = (ImageView) findViewById(R.id.Imageviewwebsiteicon);
        this.websiteicon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent website = new Intent();
                website.setAction("android.intent.action.VIEW");
                website.setData(Uri.parse("http://thomsonrogers.com/"));
                Contact.this.startActivity(website);
            }
        });
    }
}
