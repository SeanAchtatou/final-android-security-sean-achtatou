package com.PADWAY;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.THOMSONROGERS.R;
import com.tritone.DBDriod;
import com.tritone.TabBarExample;
import java.util.ArrayList;

public class OldRecentRecord extends Activity {
    public static final String DATABASE_NAME = "CarInsurancelatest.dbnew12345";
    public static final String USER_TABLE = "newLocationnew";
    public static final String USER_TABLE2 = "Insurance";
    public static final String USER_TABLE_NAME = "EditFormLatest";
    ArrayList<String> date = new ArrayList<>();
    ArrayList<String> date1 = new ArrayList<>();
    DBDriod droid;
    int i;
    ArrayList<String> locationDate = new ArrayList<>();
    ArrayList<String> locationTime = new ArrayList<>();
    ListView lv;
    public SQLiteDatabase myDb;
    String[] testValues;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.recordlistview);
        ((Button) findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OldRecentRecord.this.finish();
            }
        });
        this.droid = new DBDriod(this);
        this.date1 = this.droid.getLocationcount();
        this.locationDate = this.droid.getLocationDate();
        this.locationTime = this.droid.getLocationTime();
        for (int i2 = 0; i2 < this.date1.size(); i2++) {
            System.out.println("old rec: " + this.date1.get(i2) + " : " + this.locationDate.get(i2) + " : " + this.locationTime.get(i2));
        }
        this.lv = (ListView) findViewById(R.id.ListView01);
        this.lv.setAdapter((ListAdapter) new la());
        this.lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                OldRecentRecord.this.i = arg2;
                Intent intent = new Intent(OldRecentRecord.this, TabBarExample.class);
                intent.putExtra("operation", "oldRecords");
                intent.putExtra("date", OldRecentRecord.this.locationDate.get(OldRecentRecord.this.i));
                intent.putExtra("time", OldRecentRecord.this.locationTime.get(OldRecentRecord.this.i));
                System.out.println(" old date: " + OldRecentRecord.this.locationDate.get(OldRecentRecord.this.i) + " : " + OldRecentRecord.this.locationTime.get(OldRecentRecord.this.i));
                intent.putExtra("i", OldRecentRecord.this.i);
                OldRecentRecord.this.startActivity(intent);
            }
        });
    }

    public void getMsgCount(String timedate) {
        String WHERE = " timedate = '" + timedate + "'";
        try {
            openConnection();
            Cursor c = this.myDb.query("newLocationnew", new String[]{"timedate", "Time", "Date", "Address", "city", "country", "state", "ReadSurfaceCondition", "lightCondition", "weatherCondition"}, WHERE, null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i2 = 0; i2 < numRows; i2++) {
                    this.date.add(c.getString(0));
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    public void openConnection() {
        this.myDb = getApplicationContext().openOrCreateDatabase(DATABASE_NAME, 0, null);
    }

    public void closeConnection() {
        this.myDb.close();
    }

    class la extends BaseAdapter {
        la() {
        }

        public int getCount() {
            return OldRecentRecord.this.date1.size();
        }

        public Object getItem(int position) {
            return OldRecentRecord.this.date1;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = OldRecentRecord.this.getLayoutInflater().inflate((int) R.layout.recordlisttext, (ViewGroup) null);
            }
            TextView tv = (TextView) convertView.findViewById(R.id.TextView01);
            Button delete = (Button) convertView.findViewById(R.id.deleteButton);
            ((ImageView) convertView.findViewById(R.id.ImageView01)).setImageResource(R.drawable.tabtwoo);
            delete.setBackgroundResource(R.drawable.delete);
            tv.setPadding(20, 20, 0, 0);
            tv.setTextColor(-16777216);
            tv.setTextSize(13.0f);
            tv.setText("Time:" + OldRecentRecord.this.date1.get(position));
            delete.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    OldRecentRecord.this.droid.deleteEditForm(OldRecentRecord.this.locationDate.get(position), OldRecentRecord.this.locationTime.get(position));
                    OldRecentRecord.this.droid.deleteInsurance(OldRecentRecord.this.locationDate.get(position), OldRecentRecord.this.locationTime.get(position));
                    OldRecentRecord.this.droid.deleteLocation(OldRecentRecord.this.locationDate.get(position), OldRecentRecord.this.locationTime.get(position));
                    OldRecentRecord.this.locationDate.clear();
                    OldRecentRecord.this.locationTime.clear();
                    OldRecentRecord.this.date1.clear();
                    OldRecentRecord.this.date1 = OldRecentRecord.this.droid.getLocationcount();
                    OldRecentRecord.this.locationDate = OldRecentRecord.this.droid.getLocationDate();
                    OldRecentRecord.this.locationTime = OldRecentRecord.this.droid.getLocationTime();
                    OldRecentRecord.this.lv.setAdapter((ListAdapter) new la());
                }
            });
            return convertView;
        }
    }
}
