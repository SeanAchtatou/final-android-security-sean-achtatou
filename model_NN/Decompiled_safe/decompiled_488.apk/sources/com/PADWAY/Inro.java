package com.PADWAY;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.THOMSONROGERS.R;

public class Inro extends Activity {
    TextView txt0;
    TextView txt1;
    TextView txt2;
    TextView txt3;
    TextView txt4;
    TextView txt5;
    TextView txt6;
    ImageView websiteicon;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.inro);
        this.txt0 = (TextView) findViewById(R.id.textview01);
        this.txt0.setTypeface(null, 1);
        this.txt0.setText("\nAbout Us");
        this.txt1 = (TextView) findViewById(R.id.TextView01);
        this.txt1.setText("\n   Welcome to the Toronto Law Firm of Thomson, Rogers.Since opening our doors in 1936, Thomson, Rogers has garnered a reputation for taking on the most complex and challenging litigation matters and delivering results. We are intensely driven to help our clients by creating innovative solutions.s");
        this.txt2 = (TextView) findViewById(R.id.TextView02);
        this.txt2.setText("\n   When Thomson, Rogers takes your case, you will be represented by the best lawyers in Toronto. No one prepares more thoroughly than we do, which gives you a tremendous advantage, in or out of the courtroom.If you have been injured in a car accident in Ontario, speak with one of our personal injury lawyers who specialize in severe catastrophic injuries such as quadriplegia, paraplegia, head injury, and amputation.  Our personal injury lawyers also represent professional/medical malpractice claims and product liability actions.");
        this.txt3 = (TextView) findViewById(R.id.TextView03);
        this.txt3.setText("\n   Our planning and municipal law group is active in all areas of land development, including environmental assessments and expropriations. They regularly appear in proceedings and representations before the Ontario Municipal Board and other administrative tribunals.");
        this.txt4 = (TextView) findViewById(R.id.TextView04);
        this.txt4.setText("\n   Thomson, Rogers provides strong representation in the area of family/matrimonial litigation. Our commercial and employment litigation group is experienced in all manner of contractual disputes including wrongful dismissal, real estate litigation, debtor/creditor litigation, and corporate and commercial disputes. Our education law group advises school boards and independent schools about education law, labour relations and employment.");
        this.txt5 = (TextView) findViewById(R.id.TextView05);
        this.txt5.setText("\n   Capitalize on the experience and perspective of Thomson, Rogers - Seasoned advocates and savvy negotiators with the expertise to help you secure the best result.  If you would rather speak to an attorney directly about your case, please call 18882230448.\n  Website: http://thomsonrogers.com/");
        this.txt6 = (TextView) findViewById(R.id.TextView06);
        this.txt6.setText("");
    }
}
