package com.PADWAY;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.THOMSONROGERS.R;

public class Stastics extends Activity {
    TextView txt1;
    TextView txt10;
    TextView txt11;
    TextView txt12;
    TextView txt2;
    TextView txt3;
    TextView txt4;
    TextView txt5;
    TextView txt6;
    TextView txt7;
    TextView txt8;
    TextView txt9;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.stastics);
        this.txt1 = (TextView) findViewById(R.id.TextView01);
        this.txt1.setTypeface(null, 1);
        this.txt1.setText("IMPORTANT");
        this.txt2 = (TextView) findViewById(R.id.TextView02);
        this.txt2.setTypeface(null, 1);
        this.txt2.setText("\nIt is important that you follow the proper process to be sure your priorities are correct and also so any claims you need to make will stand the best possible chance of being successful.");
        this.txt3 = (TextView) findViewById(R.id.TextView03);
        this.txt3.setText("\nOur quick guide below is an easy cheat sheet for you to use in case you or a loved one are an unfortunate victim of an auto accident.");
        this.txt4 = (TextView) findViewById(R.id.TextView04);
        this.txt4.setText("\n1.Determine if you or your passengers are hurt and need first aid or immediate medical attention. If you do, seek help from emergency medical personnel by calling 911.");
        this.txt5 = (TextView) findViewById(R.id.TextView05);
        this.txt5.setText("\n 2.If you are not in need of immediate attention, safely exit the vehicle and move away from traffic.");
        this.txt6 = (TextView) findViewById(R.id.TextView06);
        this.txt6.setText("\n3.\tTake time to survey the scene of the accident to make yourself aware of important details such as: identify the driver of the other vehicle(s) involved, identify any possible witnesses to the accident - be sure to point them out to the investigating officer, look for skid marks from the tires of any vehicle involved, as well as physical evidence such as broken glass, broken tail lights, car parts, etc - the location of this debris in the road way will often reveal the \"point of impact\" of the vehicles which can become crucial evidence - again, point out any thing you find to the police officer.");
        this.txt7 = (TextView) findViewById(R.id.TextView07);
        this.txt7.setText("\n4.\tTalk to the police officer - make sure that he or she understands how the accident happened. Do not take responsibility for the accident or state anything that may place fault on you even if you think you may be at fault. There may be things of which you are unaware that caused or contributed to the accident.");
        this.txt8 = (TextView) findViewById(R.id.TextView08);
        this.txt8.setText("\n5.\tUse ‘Auto accident kits’ form to Obtain the following information from the other driver(s) : name, address, phone numbers, name of liability insurance company and name and phone number of their insurance agent.");
        this.txt9 = (TextView) findViewById(R.id.TextView09);
        this.txt9.setText("\n6.\tUse ‘Auto accident kits’ form to Take pictures  of the vehicles, the accident scene, the roadway, the driver(s) and passenger(s) of the other vehicles.");
        this.txt10 = (TextView) findViewById(R.id.TextView10);
        this.txt10.setText("\n7.\tUse ‘Auto accident kits’ form to Collect contact details of witnesses  who were not involved in the accident - get their names, addresses and telephone numbers.");
        this.txt11 = (TextView) findViewById(R.id.TextView11);
        this.txt11.setText("\n8.\tObtain a copy of the accident report and case number from the police officer.");
        this.txt12 = (TextView) findViewById(R.id.TextView12);
        this.txt12.setText("\n9.\tSeek medical attention for yourself and all passengers in your vehicle. Some injuries will be unnoticeable due to shock. Significant injuries may go undetected for several days or even longer.");
    }
}
