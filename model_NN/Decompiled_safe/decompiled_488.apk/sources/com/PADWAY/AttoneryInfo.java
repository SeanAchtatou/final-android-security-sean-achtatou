package com.PADWAY;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.THOMSONROGERS.R;

public class AttoneryInfo extends Activity {
    String attr;
    Button b1;
    TextView txt2;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.attoneryinfo);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            this.attr = b.getString("attonery1");
        }
        this.txt2 = (TextView) findViewById(R.id.TextView02);
        this.txt2.setTextSize(14.0f);
        this.txt2.setText(this.attr);
        this.b1 = (Button) findViewById(R.id.attonary);
        this.b1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AttoneryInfo.this.finish();
            }
        });
    }
}
