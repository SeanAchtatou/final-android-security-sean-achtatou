package com.PADWAY;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class Start extends Activity {
    static int i = 1;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        if (i == 1) {
            startActivity(new Intent(this, SplashScreen.class));
        } else if (i > 1) {
            startActivity(new Intent(this, HomeEdit.class));
        }
        i++;
        finish();
    }
}
