package com.PADWAY;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.THOMSONROGERS.R;
import com.tritone.DBDriod;
import com.tritone.TabBarExample;

public class HomeEdit extends Activity {
    Button Emergencyphonecalls;
    Button Personal;
    Button aboutUs;
    Button accidentInfo;
    boolean b1;
    DBDriod droid;
    Button editRecent;
    String flag = "new";
    String hai;
    Button newCollision;
    Button oldRecords;
    String[] str = new String[14];
    TransitionDrawable transition;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.home);
        SharedPreferences.Editor prefsEditor = getSharedPreferences("myPrefs", 1).edit();
        Bundle b = getIntent().getExtras();
        this.droid = new DBDriod(this);
        if (b != null) {
            this.flag = b.getString("operation");
            this.str[0] = b.getString("firstname");
            prefsEditor.putString("firstname", this.str[0]);
            this.str[1] = b.getString("lastname");
            this.str[2] = b.getString("contactNumber");
            this.str[3] = b.getString("email");
            this.str[4] = b.getString("driverLicense");
            this.str[5] = b.getString("state");
            this.str[6] = b.getString("licensePlate");
            this.str[7] = b.getString("make");
            this.str[8] = b.getString("model");
            this.str[9] = b.getString("company");
            this.str[10] = b.getString("policy");
            this.str[11] = b.getString("phoneNumber");
            this.str[12] = b.getString("bodyPhoneNumber");
        }
        this.newCollision = (Button) findViewById(R.id.button03);
        this.newCollision.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(HomeEdit.this, TabBarExample.class);
                System.out.println(HomeEdit.this.flag);
                if (HomeEdit.this.flag.equals("new")) {
                    intent.putExtra("operation", "new");
                } else if (HomeEdit.this.flag.equals("personal")) {
                    intent.putExtra("operation", "personal");
                    intent.putExtra("firstname1", HomeEdit.this.str[0]);
                    intent.putExtra("lastname1", HomeEdit.this.str[1]);
                    intent.putExtra("dlno1", HomeEdit.this.str[4]);
                    intent.putExtra("phno1", HomeEdit.this.str[2]);
                    intent.putExtra("email1", HomeEdit.this.str[3]);
                    intent.putExtra("state1", HomeEdit.this.str[5]);
                    intent.putExtra("LicensePlate1", HomeEdit.this.str[6]);
                    intent.putExtra("InsuranceCompany1", HomeEdit.this.str[9]);
                    intent.putExtra("PolicyNumber1", HomeEdit.this.str[10]);
                    intent.putExtra("VehicleMake1", HomeEdit.this.str[7]);
                    intent.putExtra("PassengerNews1", HomeEdit.this.str[8]);
                }
                HomeEdit.this.startActivity(intent);
            }
        });
        this.oldRecords = (Button) findViewById(R.id.button02);
        this.oldRecords.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeEdit.this.startActivity(new Intent(HomeEdit.this, OldRecentRecord.class));
            }
        });
        this.Emergencyphonecalls = (Button) findViewById(R.id.button01);
        this.Emergencyphonecalls.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeEdit.this.startActivity(new Intent(HomeEdit.this, Emergency.class));
            }
        });
        this.accidentInfo = (Button) findViewById(R.id.button04);
        this.accidentInfo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeEdit.this.startActivity(new Intent(HomeEdit.this, Accident.class));
            }
        });
        this.aboutUs = (Button) findViewById(R.id.button05);
        this.aboutUs.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeEdit.this.startActivity(new Intent(HomeEdit.this, AboutUs.class));
            }
        });
        this.Personal = (Button) findViewById(R.id.button06);
        this.Personal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeEdit.this.startActivity(new Intent(HomeEdit.this, Personal.class));
            }
        });
    }
}
