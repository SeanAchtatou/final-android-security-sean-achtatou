package com.PADWAY;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.THOMSONROGERS.R;

public class Emergency extends Activity {
    Button car_repair;
    Button emergency_service;
    Button taxi_service;
    Button towing;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.emergency);
        ((Button) findViewById(R.id.Buttoncallmyemergency)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction("android.intent.action.DIAL");
                intent.setData(Uri.parse("tel:911"));
                Emergency.this.startActivity(intent);
            }
        });
        ((Button) findViewById(R.id.Buttoncallmylawyer)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction("android.intent.action.DIAL");
                intent.setData(Uri.parse("tel:18882230448"));
                Emergency.this.startActivity(intent);
            }
        });
    }
}
