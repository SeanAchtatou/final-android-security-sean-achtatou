package com.PADWAY;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import com.THOMSONROGERS.R;
import java.io.DataInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class SplashScreen extends Activity {
    protected boolean _active = true;
    protected int _splashTime = 2000;
    String filename = "personal";
    String temp;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.splash);
        try {
            DataInputStream stream = new DataInputStream(openFileInput(this.filename));
            while (stream.available() != 0) {
                this.temp = stream.readLine();
            }
        } catch (FileNotFoundException e) {
            this.temp = "false";
            try {
                FileOutputStream out = openFileOutput(this.filename, 0);
                out.write(this.temp.getBytes());
                out.flush();
                out.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        new Thread() {
            public void run() {
                int waited = 0;
                while (SplashScreen.this._active && waited < SplashScreen.this._splashTime) {
                    try {
                        sleep(100);
                        if (SplashScreen.this._active) {
                            waited += 100;
                        }
                    } catch (InterruptedException e) {
                        SplashScreen.this.finish();
                        if (SplashScreen.this.temp.equals("false")) {
                            SplashScreen.this.startActivity(new Intent("com.droidnova.android.splashscreen.MyApp"));
                        } else {
                            SplashScreen.this.startActivity(new Intent(SplashScreen.this, HomeEdit.class));
                        }
                        stop();
                        return;
                    } catch (Throwable th) {
                        SplashScreen.this.finish();
                        if (SplashScreen.this.temp.equals("false")) {
                            SplashScreen.this.startActivity(new Intent("com.droidnova.android.splashscreen.MyApp"));
                        } else {
                            SplashScreen.this.startActivity(new Intent(SplashScreen.this, HomeEdit.class));
                        }
                        stop();
                        throw th;
                    }
                }
                SplashScreen.this.finish();
                if (SplashScreen.this.temp.equals("false")) {
                    SplashScreen.this.startActivity(new Intent("com.droidnova.android.splashscreen.MyApp"));
                } else {
                    SplashScreen.this.startActivity(new Intent(SplashScreen.this, HomeEdit.class));
                }
                stop();
            }
        }.start();
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != 0) {
            return true;
        }
        this._active = false;
        return true;
    }
}
