package com.PADWAY;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.THOMSONROGERS.R;

public class Accident extends Activity {
    Button b1;
    Button b2;
    Button b3;
    Button b4;
    Button b5;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.accident);
        ((Button) findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Accident.this.startActivity(new Intent(Accident.this, HomeEdit.class));
                Accident.this.finish();
            }
        });
        this.b1 = (Button) findViewById(R.id.carRentalInformation);
        this.b1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Accident.this, AttoneryInfo.class);
                intent.putExtra("attonery1", "Q: What will it cost to retain an attorney?\nAns: Most personal injury attorneys charge nothing up front, waiting for payment until a settlement or court decision is reached on your behalf.In most cases, if no recovery is made for you, there is no charge. Most people do not realize that attorneys spend a significant amount of time and money on your behalf hiring investigators to survey the accident scene, interviewing witnesses, paying court filing fees, and spending a significant amount of time analyzing your case - all before receiving payment.\n\n\nQ: In a personal injury claim, what am I entitled to be compensated for?\nAns: Subject to some exceptions, a person who sustains an injury due to the negligence of another is entitled to be compensated for:\n1. Past and future medical bills;\n2. Aggravation of a pre-existing condition;\n3. Lost wages and/or loss of earning capacity;\n4. Property damage to your vehicle;\n5. 5. Pain, suffering and other \"non-economic damages\"\n6. If you are married, your spouse may be entitled to compensation for their loss of consortium.\n\n\nQ: Should I talk to the other driver’s insurance company? What if they call me?\nAns: Never give an oral statement to the other side’s insurance company. If you are contacted, be polite, but decline to talk. IInsurance companies’ claims adjusters are professional negotiators, with extensive experience.  Claims adjusters are well trained by insurance company lawyers to ask questions in a manner designed to hurt you and help them.  Simply say \"thank you for calling but I am not prepared to discuss this matter with you at this time.\"  Your attorney will determine the best way to proceed.\n\n\nQ: What happens if I am in an accident and the other driver does not have insurance?\nAns: If an injured person has uninsured/underinsured motorist (UM) coverage on their own insurance policy, he/she may be able to make a claim for their UM benefits.  An injured person can make a claim for UM benefits if the driver responsible for the accident did not have insurance, or did not have enough insurance to cover the damages sustained by the injured person in the accident.\n\n\nQ: Should I move the car to the side of the road before the police arrive?\nAns: Yes. You should always protect the scene of the accident to avoid additional collisions. Do not allow your car to obstruct the road if it can be moved. Warn approaching cars by raising the hood of your car and using your car’s hazard warning lights.  In addition, for nighttime accidents, place flares or reflectors on the road if available. ");
                Accident.this.startActivity(intent);
            }
        });
        this.b2 = (Button) findViewById(R.id.towinginfo);
        this.b2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Accident.this, AttoneryInfo.class);
                intent.putExtra("attonery1", "Q: Does my insurance cover towing?\nAns: Towing is an add-on to most insurance policies. Unless you have purchased this extra coverage, you will be responsible for the towing charge. The towing from the impound yard to the auto body repair facility may be provided by your insurance company or the body shop.\n\nQ: Will the towing company bill my insurance directly? \nAns: No. In most cases, you will be required to pay the towing bill and then be reimbursed if you have towing coverage. \n\nQ: Can I choose the tow company and where my vehicle will be towed?\nAns: Yes, but with a few exceptions. The tow company you choose must be on the scene of the accident within 20 minutes of the call. The tow company will almost always tow your vehicle back to their own lot unless you have a prior relationship with them.  They will usually not release your vehicle until the towing bill has been paid.  A few auto clubs have preferred tow companies and locations where vehicles are towed. \n\nQ: Where will my car be towed? \nAns: If your car is cannot be driven after the accident, it will be towed by the towing company that responds to the police call for car removal.  Towing companies are usually called on a rotating basis for car removal so your car will be towed to the impound yard of the responding tow truck.You will be given information at the scene stating where your vehicle was taken. If you are taken by ambulance to the hospital, a police officer will usually follow up and bring the information to the hospital.  If you are unsure where your vehicle was towed, call the police department for that information.");
                Accident.this.startActivity(intent);
            }
        });
        this.b3 = (Button) findViewById(R.id.insuranceInformation);
        this.b3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Accident.this, AttoneryInfo.class);
                intent.putExtra("attonery1", "Q: Can I choose my own repair facility?\nAns: By law, an auto insurance policyholder may choose an auto repair facility. An insurance company cannot force a policyholder to have their vehicle repaired at a specific facility.  Choosing your own repair facility is your right. your own repair facility is your right.\n\nQ: Will I have to pay extra if I choose my own repair facility?\nAns: No. Your insurance company is required to pay for all reasonable and necessary repairs to restore your vehicle no matter who performs the work. However, you should verify that the repair facility of your choice will bill the insurance company directly and will accept the insurance reimbursement as payment in full minus your deductible. \n\nQ: Will new original parts be used to repair my car?\nAns: You should always ask about the parts that will be used for the repair of your vehicle.  You should insist upon parts that are equal to the original equipment manufacturers.  If non-approved parts are used, it could void your manufacturer’s warranty and cause a decrease in value to the vehicle. However, it is important to realize that the body shop is bound by your insurance policy when repairing the vehicle and may be required to use aftermarket parts.\n\nQ: What is the difference between OEM, aftermarket, and like kind quality parts?\nAns: OEM parts are new parts made by the original manufacturer of your vehicle. Aftermarket parts are new parts that are not manufactured by the Original Equipment Manufacturer. In some cases, aftermarket parts may be the only option when original parts are no longer available.  Be sure these parts are warranted. \"Like kind quality\" parts are used parts that may be needed for early model vehicles in cases where OEM and aftermarket parts are no longer manufactured. \n\nQ: Will the work be guaranteed? \nAns: All good repair facilities offer a written warranty on all repairs work and repainting in addition to any warranty offered by the insurer.Be sure to ask about the warranty before any repair work is begun.");
                Accident.this.startActivity(intent);
            }
        });
        this.b4 = (Button) findViewById(R.id.carrentalinfo);
        this.b4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Accident.this, AttoneryInfo.class);
                intent.putExtra("attonery1", "Q: Will my insurance cover a car rental?\nAns: Your auto insurance will not automatically cover a car rental in the event of an accident. You need to purchase extra coverage before an accident. Rental reimbursement coverage is usually inexpensive, averaging only a few dollars per month. Most car repairs take two weeks or longer, so a replacement rental can cost $500 or more. Even if you do not have an accident for many years, the coverage will pay for itself when you need it most. \n\nQ: I need a ride to the rental office. Can someone pick me up?\nAns: Most car rental companies will pick you up or arrange for transportation to the car rental office. Car rental companies realize that you have been inconvenienced due to an accident, so they make every effort to accommodate your situation. Call the car rental company of your choice to inquire about transportation to their office.\n\nQ: I need a replacement now. What documentation do I need for the rental?\nAns: 1.Your insurance company name\n  2.Your claim number (if assigned)\n  3.Your insurance policy number\n 4.our insurance adjusters’ name / number\n 5.Date of Loss\n 6.Insurance card (proof of insurance)\n 7.Repair shop information\n\n\nQ: Will the car rental company bill my insurance?\nAns: If you are able to provide the car rental company with a claim number from your insurance company, they will call your insurance company to verify the claim. After receiving verification of your insurance coverage, car rental companies will usually bill the insurance company directly and you will not be charged if your daily rental total is below the maximum amount allowed by your insurance company. \n\nQ: Am I entitled to a rental car when another driver caused the accident? \nAns:In most cases when clear fault is established, the insurance company of the at-fault driver will pay for the rental car. However, it may take several days for the other driver’s insurance company to agree to pay for the rental. Also, if the other driver was uninsured, you will be stuck with the bill, so it’s always best to carry car rental reimbursement coverage on your own policy to insure coverage and a timely rental.\n\nQ: •\tWhat type of car will my insurance cover?\nAns: Rental reimbursement usually covers a vehicle that costs $30 per day or less. If you would like a larger car that costs more than your maximum daily reimbursable amount, you will be responsible for the overage.");
                Accident.this.startActivity(intent);
            }
        });
        this.b5 = (Button) findViewById(R.id.insuranceinfo);
        this.b5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Accident.this, AttoneryInfo.class);
                intent.putExtra("attonery1", "\nQ: Is minimum required insurance enough?\nAns:No. Minimum limits coverage will not cover any major accident.  In North Carolina, minimum limits coverage is only $30,000.00.  If you are sued, your personal assets may be taken. Uninsured motorist coverage and collision insurance are also not required under most state laws. Without collision insurance, your car will not be covered if you cause an accident. Uninsured motorist coverage will protect you when the other driver is uninsured.  It is recommended that you have Uninsured, Underinsured and Medical Payments coverage to protect yourself from the negligence of others.  If you have questions about your insurance coverage please contact Hull & Chandler, P.A.\n\nQ:How can I save money on my insurance premium?\nAns: Shop around. Rates can vary by several hundred dollars.Maintain a good driving record. Avoid traffic tickets and accidents.Take a driver’s education course. Many insurance companies give discounts to educated drivers.\nDiscounts for safety equipment. Airbags, anti-lock brakes, and anti-theft devices can decrease your rate.Choose higher deductibles. Higher deductibles will decrease your premium.\n\nQ:Will my insurance rates go up after my accident?\nAns:Maybe, Insurance companies use many factors to determine your premium. Traffic violations, age, gender, residential neighbourhood, miles traveled, prior accidents, and other factors may affect your rate. Different insurance companies use different methods to determine premiums. If you are a good driver, have no traffic violations, and haven’t had an accident in years, then a new accident will probably have little effect on your rate");
                Accident.this.startActivity(intent);
            }
        });
    }
}
