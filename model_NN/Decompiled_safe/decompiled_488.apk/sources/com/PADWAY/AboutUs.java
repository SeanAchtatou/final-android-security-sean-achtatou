package com.PADWAY;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import com.THOMSONROGERS.R;

public class AboutUs extends TabActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.aboutus);
        ((Button) findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AboutUs.this.finish();
            }
        });
        TabHost tabHost = (TabHost) findViewById(16908306);
        TabHost.TabSpec firstSpec = tabHost.newTabSpec("tab1");
        TabHost.TabSpec secondSpec = tabHost.newTabSpec("tab2");
        TabHost.TabSpec thirdSpec = tabHost.newTabSpec("tab3");
        firstSpec.setIndicator("", getResources().getDrawable(R.drawable.aboutus)).setContent(new Intent(this, Inro.class));
        secondSpec.setIndicator("", getResources().getDrawable(R.drawable.nowinnofees)).setContent(new Intent(this, NoFee.class));
        thirdSpec.setIndicator("", getResources().getDrawable(R.drawable.contactus)).setContent(new Intent(this, Contact.class));
        tabHost.addTab(firstSpec);
        tabHost.addTab(secondSpec);
        tabHost.addTab(thirdSpec);
    }
}
