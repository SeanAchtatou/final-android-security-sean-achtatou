package com.PADWAY;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import com.THOMSONROGERS.R;
import com.tritone.FirstTab;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Pattern;

public class Personal extends Activity {
    private static final String CLASS_TAG = "Validate";
    public static final int INVALID_TEXT_COLOR = -65536;
    public static final int VALID_TEXT_COLOR = -16777216;
    EditText CarVeredit;
    Button Donebtn;
    EditText InsuCompedit;
    EditText InsuPhnedit;
    EditText InsuPolcyedit;
    EditText VhcleLiceedit;
    EditText VhcleMdledit;
    EditText Vhclemakeedit;
    EditText bdyshpPhnedit;
    Button cancelbtn;
    String filename = "personal";
    EditText fusrnameedit;
    EditText usrContatctedit;
    EditText usrDrvedit;
    EditText usrEmaledit;
    EditText usrlnameedit;
    EditText usrstateedit;
    String warning;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.personal);
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", 1);
        this.fusrnameedit = (EditText) findViewById(R.id.fusrnameedit);
        this.usrlnameedit = (EditText) findViewById(R.id.usrlnameedit);
        this.usrContatctedit = (EditText) findViewById(R.id.usrContatctedit);
        this.usrEmaledit = (EditText) findViewById(R.id.usrEmaledit);
        this.usrDrvedit = (EditText) findViewById(R.id.usrDrvedit);
        this.usrstateedit = (EditText) findViewById(R.id.usrstateedit);
        this.VhcleLiceedit = (EditText) findViewById(R.id.VhcleLiceedit);
        this.Vhclemakeedit = (EditText) findViewById(R.id.Vhclemakeedit);
        this.VhcleMdledit = (EditText) findViewById(R.id.VhcleMdledit);
        this.fusrnameedit.setText(myPrefs.getString("firstname", ""));
        this.usrlnameedit.setText(myPrefs.getString("lastname", ""));
        this.usrContatctedit.setText(myPrefs.getString("contactNumber", ""));
        this.usrEmaledit.setText(myPrefs.getString("email", ""));
        this.usrDrvedit.setText(myPrefs.getString("driverLicense", ""));
        this.usrstateedit.setText(myPrefs.getString("state", ""));
        this.VhcleLiceedit.setText(myPrefs.getString("licensePlate", ""));
        this.Vhclemakeedit.setText(myPrefs.getString("make", ""));
        this.VhcleMdledit.setText(myPrefs.getString("model", ""));
        ((TableLayout) findViewById(R.id.tt)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) Personal.this.getSystemService("input_method");
                imm.hideSoftInputFromWindow(Personal.this.fusrnameedit.getWindowToken(), 0);
                imm.hideSoftInputFromInputMethod(Personal.this.usrlnameedit.getWindowToken(), 0);
                imm.hideSoftInputFromInputMethod(Personal.this.usrContatctedit.getWindowToken(), 0);
                imm.hideSoftInputFromInputMethod(Personal.this.usrEmaledit.getWindowToken(), 0);
                imm.hideSoftInputFromInputMethod(Personal.this.usrDrvedit.getWindowToken(), 0);
                imm.hideSoftInputFromInputMethod(Personal.this.usrstateedit.getWindowToken(), 0);
                imm.hideSoftInputFromInputMethod(Personal.this.VhcleLiceedit.getWindowToken(), 0);
                imm.hideSoftInputFromInputMethod(Personal.this.Vhclemakeedit.getWindowToken(), 0);
                imm.hideSoftInputFromInputMethod(Personal.this.VhcleMdledit.getWindowToken(), 0);
            }
        });
        final SharedPreferences.Editor prefsEditor = myPrefs.edit();
        this.Donebtn = (Button) findViewById(R.id.Donebtn);
        this.Donebtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                prefsEditor.putString("firstname", Personal.this.fusrnameedit.getText().toString());
                prefsEditor.putString("lastname", Personal.this.usrlnameedit.getText().toString());
                prefsEditor.putString("contactNumber", Personal.this.usrContatctedit.getText().toString());
                prefsEditor.putString("email", Personal.this.usrEmaledit.getText().toString());
                prefsEditor.putString("driverLicense", Personal.this.usrDrvedit.getText().toString());
                prefsEditor.putString("state", Personal.this.usrstateedit.getText().toString());
                prefsEditor.putString("licensePlate", Personal.this.VhcleLiceedit.getText().toString());
                prefsEditor.putString("make", Personal.this.Vhclemakeedit.getText().toString());
                prefsEditor.putString("model", Personal.this.VhcleMdledit.getText().toString());
                prefsEditor.commit();
                if (Personal.this.validated()) {
                    Intent intent = new Intent(Personal.this, HomeEdit.class);
                    intent.putExtra("operation", "personal");
                    intent.putExtra("firstname", Personal.this.fusrnameedit.getText().toString());
                    intent.putExtra("lastname", Personal.this.usrlnameedit.getText().toString());
                    intent.putExtra("contactNumber", Personal.this.usrContatctedit.getText().toString());
                    intent.putExtra("email", Personal.this.usrEmaledit.getText().toString());
                    intent.putExtra("driverLicense", Personal.this.usrDrvedit.getText().toString());
                    intent.putExtra("state", Personal.this.usrstateedit.getText().toString());
                    intent.putExtra("licensePlate", Personal.this.VhcleLiceedit.getText().toString());
                    intent.putExtra("make", Personal.this.Vhclemakeedit.getText().toString());
                    intent.putExtra("model", Personal.this.VhcleMdledit.getText().toString());
                    intent.putExtra("firsttab", "firstTab");
                    try {
                        FileOutputStream out = Personal.this.openFileOutput(Personal.this.filename, 0);
                        out.write("true".getBytes());
                        out.flush();
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Personal.this.startActivity(intent);
                    return;
                }
                Personal.this.AlertDialog();
            }
        });
        this.cancelbtn = (Button) findViewById(R.id.Buttoncancel);
        this.cancelbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Personal.this.startActivity(new Intent(Personal.this, HomeEdit.class));
                Personal.this.finish();
            }
        });
    }

    public void AlertDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Required...");
        alertDialog.setMessage("Please enter the " + this.warning + " fields");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.setIcon((int) R.drawable.warning);
        alertDialog.show();
    }

    public static boolean isRegEmail(EditText editText, boolean required) {
        Log.d(CLASS_TAG, "isReg_email");
        return isValid(editText, editText.getResources().getString(R.string.regex_email), required);
    }

    public static boolean isRegPhone(EditText editText, boolean required) {
        Log.d(CLASS_TAG, "isReg_phone");
        return isValid(editText, editText.getResources().getString(R.string.regex_phone), required);
    }

    public static boolean isValid(EditText editText, String regex, boolean required) {
        Log.d(CLASS_TAG, "isValid()");
        boolean validated = true;
        String text = editText.getText().toString().trim();
        boolean hasText = hasText(editText);
        editText.setTextColor(-16777216);
        if (required && !hasText) {
            validated = false;
        }
        if (!validated || !hasText || Pattern.matches(regex, text)) {
            return validated;
        }
        editText.setTextColor(-65536);
        return false;
    }

    public static boolean hasText(EditText editText) {
        Log.d(CLASS_TAG, "hasText()");
        String text = editText.getText().toString().trim();
        if (text.length() != 0) {
            return true;
        }
        editText.setText(text);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean validated() {
        boolean validated = true;
        if (!FirstTab.hasText(this.fusrnameedit)) {
            this.warning = "firstname";
            validated = true;
        }
        if (!FirstTab.hasText(this.usrlnameedit)) {
            this.warning = "lastname";
            validated = true;
        }
        if (!FirstTab.hasText(this.usrContatctedit)) {
            this.warning = "contact Number";
            validated = true;
        }
        if (!FirstTab.hasText(this.usrEmaledit)) {
            this.warning = "Email";
            validated = true;
        }
        if (FirstTab.isRegEmail(this.usrEmaledit, true)) {
            return validated;
        }
        this.warning = "Email should x@x.com form";
        return true;
    }

    public static boolean hasText1(Spinner select) {
        Log.d(CLASS_TAG, "hasText()");
        String text = select.getContext().toString().trim();
        if (text.length() != 0) {
            return true;
        }
        select.setTag(text);
        return false;
    }
}
