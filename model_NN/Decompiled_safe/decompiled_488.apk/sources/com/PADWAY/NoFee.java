package com.PADWAY;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.THOMSONROGERS.R;

public class NoFee extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.nofee);
        TextView tv = (TextView) findViewById(R.id.textView01);
        tv.setTypeface(null, 3);
        tv.setTextSize(13.0f);
        tv.setTextColor(-16776961);
        tv.setText("\nNo win No Fees \n");
        ((TextView) findViewById(R.id.TextView01)).setText("\"‘Unless we win your case there is no legal fee, but you may still be responsible for disbursements.  However, if we win the case we will be entitled to a legal fee which will be outlined in our retainer’\"");
    }
}
