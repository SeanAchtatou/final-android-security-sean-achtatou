package com.android.gesture.builder;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import com.THOMSONROGERS.R;
import com.tritone.DBDriod;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class CreateGestureActivity extends Activity {
    private static final float LENGTH_THRESHOLD = 50.0f;
    public static String filename;
    private static GestureLibrary sStore;
    private static int val = 0;
    String FILENAME = "Audio_File_New_Details";
    Bitmap bmp;
    DBDriod droid;
    /* access modifiers changed from: private */
    public View mDoneButton;
    /* access modifiers changed from: private */
    public Gesture mGesture;
    private File mStoreFile;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.create_gesture);
        try {
            DataInputStream stream = new DataInputStream(openFileInput(this.FILENAME));
            while (stream.available() != 0) {
                stream.readLine();
            }
        } catch (FileNotFoundException e) {
            String temp = new StringBuilder(String.valueOf(val)).toString();
            try {
                FileOutputStream fos = openFileOutput(this.FILENAME, 0);
                fos.write(temp.getBytes());
                fos.close();
                System.out.println("File Created..................");
            } catch (FileNotFoundException e2) {
                e2.printStackTrace();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }
        try {
            String temp2 = new StringBuilder(String.valueOf(val)).toString();
            System.out.println("Value Inserted......" + val);
            FileOutputStream fos2 = openFileOutput(this.FILENAME, 0);
            fos2.write(temp2.getBytes());
            fos2.close();
        } catch (IOException e4) {
            e4.printStackTrace();
        }
        this.mStoreFile = new File(Environment.getExternalStorageDirectory(), "");
        if (sStore == null) {
            sStore = GestureLibraries.fromFile(this.mStoreFile);
        }
        this.mDoneButton = findViewById(R.id.done);
        GestureOverlayView overlay = (GestureOverlayView) findViewById(R.id.gestures_overlay);
        overlay.setBackgroundColor(-1);
        overlay.addOnGestureListener(new GesturesProcessor(this, null));
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (this.mGesture != null) {
            outState.putParcelable("gesture", this.mGesture);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.mGesture = (Gesture) savedInstanceState.getParcelable("gesture");
        if (this.mGesture != null) {
            final GestureOverlayView overlay = (GestureOverlayView) findViewById(R.id.gestures_overlay);
            overlay.post(new Runnable() {
                public void run() {
                    overlay.setGesture(CreateGestureActivity.this.mGesture);
                }
            });
            this.mDoneButton.setEnabled(true);
        }
    }

    public static GestureLibrary getStore() {
        return sStore;
    }

    public void addGesture(View v) {
        if (this.mGesture != null) {
            TextView input = (TextView) findViewById(R.id.gesture_name);
            CharSequence name = input.getText();
            if (name.length() == 0) {
                input.setError(getString(R.string.error_missing_name));
                return;
            }
            SharedPreferences myPrefs = getSharedPreferences("myPrefs", 1);
            GestureLibrary store = getStore();
            store.addGesture(name.toString(), this.mGesture);
            store.save();
            Calendar cal = Calendar.getInstance();
            String string = myPrefs.getString("time", "time");
            String string2 = myPrefs.getString("date", "date");
            String sb = new StringBuilder(String.valueOf(cal.get(10) + cal.get(12) + cal.get(13))).toString();
            System.out.println(new StringBuilder().append(GestureBuilderActivity.val).toString());
            setResult(-1);
            String absolutePath = new File(Environment.getExternalStorageDirectory(), "gesture" + GestureBuilderActivity.val).getAbsolutePath();
            new Intent().putExtra("name", input.toString());
        } else {
            setResult(0);
        }
        finish();
    }

    public void cancelGesture(View v) {
        setResult(0);
        finish();
    }

    private class GesturesProcessor implements GestureOverlayView.OnGestureListener {
        private GesturesProcessor() {
        }

        /* synthetic */ GesturesProcessor(CreateGestureActivity createGestureActivity, GesturesProcessor gesturesProcessor) {
            this();
        }

        public void onGestureStarted(GestureOverlayView overlay, MotionEvent event) {
            System.out.println("OnGestureStarted");
            CreateGestureActivity.this.mDoneButton.setEnabled(false);
            CreateGestureActivity.this.mGesture = null;
        }

        public void onGesture(GestureOverlayView overlay, MotionEvent event) {
            System.out.println("On Gesture method is called");
        }

        public void onGestureEnded(GestureOverlayView overlay, MotionEvent event) {
            onGesture(overlay, event);
            CreateGestureActivity.this.mGesture = overlay.getGesture();
            if (CreateGestureActivity.this.mGesture.getLength() < CreateGestureActivity.LENGTH_THRESHOLD) {
                System.out.println("666666666666666666666666666");
                System.out.println("mGesture12222222222222222222:   " + CreateGestureActivity.this.mGesture.getLength());
            }
            CreateGestureActivity.this.mDoneButton.setEnabled(true);
        }

        public void onGestureCancelled(GestureOverlayView overlay, MotionEvent event) {
        }
    }
}
