package com.android.gesture.builder;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.THOMSONROGERS.R;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class GestureBuilderActivity extends ListActivity {
    private static final int DIALOG_RENAME_GESTURE = 1;
    private static final String GESTURES_INFO_ID = "gestures.info_id";
    private static final int MENU_ID_REMOVE = 2;
    private static final int MENU_ID_RENAME = 1;
    private static final int REQUEST_NEW_GESTURE = 1;
    private static final int STATUS_CANCELLED = 1;
    private static final int STATUS_NOT_LOADED = 3;
    private static final int STATUS_NO_STORAGE = 2;
    private static final int STATUS_SUCCESS = 0;
    /* access modifiers changed from: private */
    public static GestureLibrary sStore;
    public static int val = 0;
    String DATE;
    String FILENAME = "Audio_File_New_Details2";
    String TIME;
    /* access modifiers changed from: private */
    public GesturesAdapter mAdapter;
    private NamedGesture mCurrentRenameGesture;
    /* access modifiers changed from: private */
    public TextView mEmpty;
    private EditText mInput;
    private Dialog mRenameDialog;
    /* access modifiers changed from: private */
    public final Comparator<NamedGesture> mSorter = new Comparator<NamedGesture>() {
        public int compare(NamedGesture object1, NamedGesture object2) {
            return object1.name.compareTo(object2.name);
        }
    };
    /* access modifiers changed from: private */
    public File mStoreFile;
    private GesturesLoadTask mTask;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        Calendar instance = Calendar.getInstance();
        try {
            DataInputStream stream = new DataInputStream(openFileInput(this.FILENAME));
            while (stream.available() != 0) {
                String temp1 = stream.readLine();
                Toast.makeText(this, "data read from file....." + temp1, 0).show();
                val = Integer.parseInt(temp1);
                val++;
            }
            try {
                String temp = new StringBuilder(String.valueOf(val)).toString();
                System.out.println("Value Inserted......" + val);
                FileOutputStream fos = openFileOutput(this.FILENAME, 0);
                fos.write(temp.getBytes());
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e2) {
            String temp2 = new StringBuilder(String.valueOf(val)).toString();
            try {
                FileOutputStream fos2 = openFileOutput(this.FILENAME, 0);
                fos2.write(temp2.getBytes());
                fos2.close();
                System.out.println("File Created..................");
            } catch (FileNotFoundException e3) {
                e3.printStackTrace();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        }
        String file = "gesture" + val;
        System.out.println("file created:............." + file);
        this.mStoreFile = new File(Environment.getExternalStorageDirectory(), file);
        setContentView((int) R.layout.gestures_list);
        System.out.println("1......oncreate");
        this.mAdapter = new GesturesAdapter(this);
        setListAdapter(this.mAdapter);
        if (sStore == null) {
            sStore = GestureLibraries.fromFile(this.mStoreFile);
        }
        this.mEmpty = (TextView) findViewById(16908292);
        loadGestures();
        registerForContextMenu(getListView());
    }

    public static GestureLibrary getStore() {
        return sStore;
    }

    public void reloadGestures(View v) {
        loadGestures();
    }

    public void addGesture(View v) {
        startActivityForResult(new Intent(this, CreateGestureActivity.class), 1);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1) {
            switch (requestCode) {
                case 1:
                    loadGestures();
                    return;
                default:
                    return;
            }
        }
    }

    private void loadGestures() {
        if (!(this.mTask == null || this.mTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.mTask.cancel(true);
        }
        this.mTask = (GesturesLoadTask) new GesturesLoadTask(this, null).execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!(this.mTask == null || this.mTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.mTask.cancel(true);
            this.mTask = null;
        }
        cleanupRenameDialog();
    }

    /* access modifiers changed from: private */
    public void checkForEmpty() {
        if (this.mAdapter.getCount() == 0) {
            this.mEmpty.setText((int) R.string.gestures_empty);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (this.mCurrentRenameGesture != null) {
            outState.putLong(GESTURES_INFO_ID, this.mCurrentRenameGesture.gesture.getID());
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        long id = state.getLong(GESTURES_INFO_ID, -1);
        if (id != -1) {
            for (String name : sStore.getGestureEntries()) {
                Iterator<Gesture> it = sStore.getGestures(name).iterator();
                while (true) {
                    if (it.hasNext()) {
                        Gesture gesture = it.next();
                        if (gesture.getID() == id) {
                            this.mCurrentRenameGesture = new NamedGesture();
                            this.mCurrentRenameGesture.name = name;
                            this.mCurrentRenameGesture.gesture = gesture;
                            return;
                        }
                    }
                }
            }
        }
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(((TextView) ((AdapterView.AdapterContextMenuInfo) menuInfo).targetView).getText());
        System.out.println("5......oncreate");
        menu.add(0, 1, 0, (int) R.string.gestures_rename);
        menu.add(0, 2, 0, (int) R.string.gestures_delete);
    }

    public boolean onContextItemSelected(MenuItem item) {
        NamedGesture gesture = (NamedGesture) ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).targetView.getTag();
        switch (item.getItemId()) {
            case 1:
                renameGesture(gesture);
                return true;
            case 2:
                deleteGesture(gesture);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void renameGesture(NamedGesture gesture) {
        this.mCurrentRenameGesture = gesture;
        showDialog(1);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        if (id == 1) {
            return createRenameDialog();
        }
        return super.onCreateDialog(id);
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        if (id == 1) {
            this.mInput.setText(this.mCurrentRenameGesture.name);
        }
    }

    private Dialog createRenameDialog() {
        View layout = View.inflate(this, R.layout.dialog_rename, null);
        this.mInput = (EditText) layout.findViewById(R.id.name);
        ((TextView) layout.findViewById(R.id.label)).setText((int) R.string.gestures_rename_label);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(0);
        builder.setTitle(getString(R.string.gestures_rename_title));
        builder.setCancelable(true);
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                GestureBuilderActivity.this.cleanupRenameDialog();
            }
        });
        builder.setNegativeButton(getString(R.string.cancel_action), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GestureBuilderActivity.this.cleanupRenameDialog();
            }
        });
        builder.setPositiveButton(getString(R.string.rename_action), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GestureBuilderActivity.this.changeGestureName();
            }
        });
        builder.setView(layout);
        return builder.create();
    }

    /* access modifiers changed from: private */
    public void changeGestureName() {
        if (!TextUtils.isEmpty(this.mInput.getText().toString())) {
            NamedGesture renameGesture = this.mCurrentRenameGesture;
            GesturesAdapter adapter = this.mAdapter;
            int count = adapter.getCount();
            int i = 0;
            while (true) {
                if (i >= count) {
                    break;
                }
                NamedGesture gesture = (NamedGesture) adapter.getItem(i);
                if (gesture.gesture.getID() == renameGesture.gesture.getID()) {
                    sStore.removeGesture(gesture.name, gesture.gesture);
                    gesture.name = this.mInput.getText().toString();
                    sStore.addGesture(gesture.name, gesture.gesture);
                    break;
                }
                i++;
            }
            adapter.notifyDataSetChanged();
        }
        this.mCurrentRenameGesture = null;
    }

    /* access modifiers changed from: private */
    public void cleanupRenameDialog() {
        if (this.mRenameDialog != null) {
            this.mRenameDialog.dismiss();
            this.mRenameDialog = null;
        }
        this.mCurrentRenameGesture = null;
    }

    private void deleteGesture(NamedGesture gesture) {
        sStore.removeGesture(gesture.name, gesture.gesture);
        sStore.save();
        System.out.println("6...... delete");
        GesturesAdapter adapter = this.mAdapter;
        adapter.setNotifyOnChange(false);
        adapter.remove(gesture);
        adapter.sort(this.mSorter);
        checkForEmpty();
        adapter.notifyDataSetChanged();
        Toast.makeText(this, (int) R.string.gestures_delete_success, 0).show();
    }

    private class GesturesLoadTask extends AsyncTask<Void, NamedGesture, Integer> {
        private int mPathColor;
        private int mThumbnailInset;
        private int mThumbnailSize;

        private GesturesLoadTask() {
        }

        /* synthetic */ GesturesLoadTask(GestureBuilderActivity gestureBuilderActivity, GesturesLoadTask gesturesLoadTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            Resources resources = GestureBuilderActivity.this.getResources();
            this.mPathColor = resources.getColor(R.color.gesture_color);
            this.mThumbnailInset = (int) resources.getDimension(R.dimen.gesture_thumbnail_inset);
            this.mThumbnailSize = (int) resources.getDimension(R.dimen.gesture_thumbnail_size);
            GestureBuilderActivity.this.findViewById(R.id.addButton).setEnabled(false);
            GestureBuilderActivity.this.findViewById(R.id.reloadButton).setEnabled(false);
            GestureBuilderActivity.this.mAdapter.setNotifyOnChange(false);
            GestureBuilderActivity.this.mAdapter.clear();
        }

        /* access modifiers changed from: protected */
        public Integer doInBackground(Void... params) {
            if (isCancelled()) {
                return 1;
            }
            if (!"mounted".equals(Environment.getExternalStorageState())) {
                return 2;
            }
            GestureLibrary store = GestureBuilderActivity.sStore;
            if (!store.load()) {
                return 3;
            }
            for (String name : store.getGestureEntries()) {
                if (isCancelled()) {
                    break;
                }
                Iterator<Gesture> it = store.getGestures(name).iterator();
                while (it.hasNext()) {
                    Gesture gesture = it.next();
                    Bitmap bitmap = gesture.toBitmap(this.mThumbnailSize, this.mThumbnailSize, this.mThumbnailInset, this.mPathColor);
                    NamedGesture namedGesture = new NamedGesture();
                    namedGesture.gesture = gesture;
                    namedGesture.name = name;
                    GestureBuilderActivity.this.mAdapter.addBitmap(Long.valueOf(namedGesture.gesture.getID()), bitmap);
                    publishProgress(namedGesture);
                }
            }
            return 0;
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(NamedGesture... values) {
            super.onProgressUpdate((Object[]) values);
            GesturesAdapter adapter = GestureBuilderActivity.this.mAdapter;
            adapter.setNotifyOnChange(false);
            for (NamedGesture gesture : values) {
                adapter.add(gesture);
            }
            adapter.sort(GestureBuilderActivity.this.mSorter);
            adapter.notifyDataSetChanged();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Integer result) {
            super.onPostExecute((Object) result);
            if (result.intValue() == 2) {
                GestureBuilderActivity.this.getListView().setVisibility(0);
                GestureBuilderActivity.this.mEmpty.setVisibility(0);
                GestureBuilderActivity.this.mEmpty.setText(GestureBuilderActivity.this.getString(R.string.gestures_error_loading, new Object[]{GestureBuilderActivity.this.mStoreFile.getAbsolutePath()}));
                return;
            }
            GestureBuilderActivity.this.findViewById(R.id.addButton).setEnabled(true);
            GestureBuilderActivity.this.findViewById(R.id.reloadButton).setEnabled(true);
            GestureBuilderActivity.this.checkForEmpty();
        }
    }

    static class NamedGesture {
        Gesture gesture;
        String name;

        NamedGesture() {
        }
    }

    private class GesturesAdapter extends ArrayAdapter<NamedGesture> {
        private final LayoutInflater mInflater;
        private final Map<Long, Drawable> mThumbnails = Collections.synchronizedMap(new HashMap());

        public GesturesAdapter(Context context) {
            super(context, 0);
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        }

        /* access modifiers changed from: package-private */
        public void addBitmap(Long id, Bitmap bitmap) {
            this.mThumbnails.put(id, new BitmapDrawable(bitmap));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.gestures_item, parent, false);
            }
            NamedGesture gesture = (NamedGesture) getItem(position);
            TextView label = (TextView) convertView;
            label.setTag(gesture);
            label.setText(gesture.name);
            label.setCompoundDrawablesWithIntrinsicBounds(this.mThumbnails.get(Long.valueOf(gesture.gesture.getID())), (Drawable) null, (Drawable) null, (Drawable) null);
            return convertView;
        }
    }
}
