package com.email;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.THOMSONROGERS.R;
import com.tritone.DBDriod;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Email extends Activity {
    String Address;
    String City;
    String Country;
    String DLno;
    String[] Docs;
    String Driver;
    String Email;
    String Firstname;
    String Insucomp;
    String Lastname;
    String License;
    String LicensePlate;
    String Lightcond;
    String Make;
    String Model;
    String Passanger;
    String PhoneNo;
    String PhoneNumber;
    String Policyno;
    String Roadcond;
    String State;
    String Time;
    String Timedate;
    String Vehiclemake;
    String Vehiclemodel;
    String Weather;
    TextView add;
    TextView city;
    TextView country;
    TextView date;
    String date1;
    TextView dlno;
    TextView driver;
    TextView driver1;
    DBDriod droid;
    EditText emailEdit;
    EditText emailSubject;
    EditText emailaddress;
    TextView emailid;
    TextView fname;
    int index;
    TextView injuredName;
    TextView injuredName1;
    TextView injuredPhNo1;
    String injuredPhoneNUmber;
    String injuredname;
    TextView injuredphNo;
    TextView insuranceCompany;
    TextView insurancecomp;
    TextView licenseplat;
    TextView licenseplat1;
    TextView licenseplate;
    TextView lightcond;
    TextView lname;
    TextView make;
    TextView model;
    String operation;
    TextView pcid;
    TextView phno;
    TextView policeName;
    TextView policeName1;
    String policeNumber;
    String policename;
    TextView policephNo;
    TextView policepnNo;
    TextView policyNumber;
    TextView policyno;
    TextView reportNumber;
    TextView reportNumber1;
    String reportnumber;
    TextView roadsurfacecond;
    ImageView send;
    int size = 0;
    TextView state;
    TextView time;
    String time1;
    Uri uri;
    TextView vehiclemake;
    TextView vehiclemake1;
    TextView vehiclemodel;
    TextView vehiclemodel1;
    TextView weathercond;
    TextView witnessName;
    TextView witnessName1;
    String witnessname;
    TextView witnessphNo;
    TextView witnessphNo1;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        String temperory;
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView(R.layout.email);
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", 1);
        this.time1 = myPrefs.getString("time", "time");
        this.date1 = myPrefs.getString("date", "date");
        this.index = myPrefs.getInt("RecordIndexVal", 0);
        this.operation = myPrefs.getString("operation", "");
        this.send = (ImageView) findViewById(R.id.emailSend);
        this.emailSubject = (EditText) findViewById(R.id.emailsubject);
        this.emailSubject.setText("Car Accident Toolkit");
        this.emailaddress = (EditText) findViewById(R.id.emailaddress);
        this.emailaddress.setText("nmhull@lawyercarolina.com");
        this.fname = (TextView) findViewById(R.id.txtfirst);
        this.lname = (TextView) findViewById(R.id.txtlast);
        this.dlno = (TextView) findViewById(R.id.txtdlno);
        this.phno = (TextView) findViewById(R.id.txtphoneno);
        this.emailid = (TextView) findViewById(R.id.txtemailid);
        this.licenseplate = (TextView) findViewById(R.id.licenseplate);
        this.make = (TextView) findViewById(R.id.make);
        this.model = (TextView) findViewById(R.id.model);
        this.date = (TextView) findViewById(R.id.txtdate);
        this.time = (TextView) findViewById(R.id.txttime);
        this.add = (TextView) findViewById(R.id.txtaddress);
        this.city = (TextView) findViewById(R.id.txtcity);
        this.country = (TextView) findViewById(R.id.txtcountry);
        this.state = (TextView) findViewById(R.id.txtstate);
        this.roadsurfacecond = (TextView) findViewById(R.id.txtroadsurf);
        this.lightcond = (TextView) findViewById(R.id.txtlightcond);
        this.weathercond = (TextView) findViewById(R.id.weathercond);
        this.driver = (TextView) findViewById(R.id.driver);
        this.licenseplat = (TextView) findViewById(R.id.licenseplate12);
        this.insurancecomp = (TextView) findViewById(R.id.insuransecomp);
        this.policyno = (TextView) findViewById(R.id.policyno);
        this.vehiclemake = (TextView) findViewById(R.id.vehiclemake);
        this.vehiclemodel = (TextView) findViewById(R.id.vehiclemodel);
        this.witnessName = (TextView) findViewById(R.id.witnessName);
        this.witnessphNo = (TextView) findViewById(R.id.witnessphNo);
        this.injuredName = (TextView) findViewById(R.id.injuredName);
        this.injuredphNo = (TextView) findViewById(R.id.injuredphNo);
        this.policeName = (TextView) findViewById(R.id.policeName);
        this.policephNo = (TextView) findViewById(R.id.policephNo);
        this.reportNumber = (TextView) findViewById(R.id.reportNumber);
        this.driver1 = (TextView) findViewById(R.id.driver1);
        this.licenseplat1 = (TextView) findViewById(R.id.licenseplate1);
        this.insuranceCompany = (TextView) findViewById(R.id.insuransecomp1);
        this.policyNumber = (TextView) findViewById(R.id.policyno1);
        this.vehiclemake1 = (TextView) findViewById(R.id.vehiclemake1);
        this.vehiclemodel1 = (TextView) findViewById(R.id.vehiclemodel1);
        this.witnessName1 = (TextView) findViewById(R.id.witnessName1);
        this.witnessphNo1 = (TextView) findViewById(R.id.witnessphNo1);
        this.injuredName1 = (TextView) findViewById(R.id.injuredName1);
        this.injuredPhNo1 = (TextView) findViewById(R.id.injuredphNo1);
        this.policepnNo = (TextView) findViewById(R.id.policephNo1);
        this.policeName1 = (TextView) findViewById(R.id.policeName1);
        this.reportNumber1 = (TextView) findViewById(R.id.reportNumber1);
        this.droid = new DBDriod(this);
        String[] str = this.droid.getEditForm();
        int length = str.length;
        for (int i = 0; i < length; i++) {
            String str2 = str[i];
            this.Firstname = str[0];
            this.Lastname = str[1];
            this.DLno = str[2];
            this.PhoneNo = str[3];
            this.Email = str[4];
            this.LicensePlate = str[6];
            this.Make = str[7];
            this.Model = str[8];
        }
        String[] str3 = this.droid.getLocationForm();
        int length2 = str3.length;
        for (int i2 = 0; i2 < length2; i2++) {
            String str4 = str3[i2];
            this.Time = str3[1];
            this.Timedate = str3[2];
            this.City = str3[4];
            this.Address = str3[3];
            this.State = str3[6];
            this.Country = str3[5];
            this.Roadcond = str3[7];
            this.Lightcond = str3[8];
            this.Weather = str3[9];
        }
        ArrayList list = this.droid.getSpecificInsurance(this.date1, this.time1);
        if (this.operation.equals("new")) {
            System.out.println("in new BLOCk");
            if (!myPrefs.getBoolean("database", false) || !myPrefs.getBoolean("database1", false) || !myPrefs.getBoolean("database2", false)) {
                System.out.println("nEW bLOCK");
                this.Firstname = "";
                this.Lastname = "";
                this.DLno = "";
                this.PhoneNo = "";
                this.Email = "";
                this.LicensePlate = "";
                this.Make = "";
                this.Model = "";
                this.Time = "";
                this.Timedate = "";
                this.City = "";
                this.Address = "";
                this.State = "";
                this.Country = "";
                this.Roadcond = "";
                this.Lightcond = "";
                this.Weather = "";
                this.Driver = "";
                this.License = "";
                this.Insucomp = "";
                this.Policyno = "";
                this.Passanger = "";
                this.Vehiclemake = "";
                this.Vehiclemodel = "";
                this.witnessname = "";
                this.PhoneNumber = "";
                this.injuredname = "";
                this.injuredPhoneNUmber = "";
                this.policename = "";
                this.policeNumber = "";
                this.reportnumber = "";
            }
        } else if (this.operation.equals("oldrecords")) {
            System.out.println("Olod Block");
            System.out.println("index" + this.index);
            String[] Fr = this.droid.getAllEditForm(this.index);
            int length3 = Fr.length;
            for (int i3 = 0; i3 < length3; i3++) {
                String str5 = Fr[i3];
                this.Firstname = DBDriod.stredit[0];
                this.Lastname = DBDriod.stredit[1];
                this.DLno = DBDriod.stredit[2];
                this.PhoneNo = DBDriod.stredit[3];
                this.Email = DBDriod.stredit[4];
                this.LicensePlate = DBDriod.stredit[5];
                this.Make = DBDriod.stredit[6];
                this.Model = DBDriod.stredit[7];
            }
            String[] allLocationForm = this.droid.getAllLocationForm(this.index);
            this.Time = DBDriod.strloc[1];
            this.Timedate = DBDriod.strloc[2];
            this.City = DBDriod.strloc[3];
            this.Address = DBDriod.strloc[4];
            this.State = DBDriod.strloc[5];
            this.Country = DBDriod.strloc[6];
            this.Roadcond = DBDriod.strloc[7];
            this.Lightcond = DBDriod.strloc[8];
            this.Weather = DBDriod.strloc[9];
            String[] allInsuranceForm = this.droid.getAllInsuranceForm(this.index);
            this.Driver = DBDriod.strins[4];
            this.License = DBDriod.strins[1];
            this.Insucomp = DBDriod.strins[2];
            this.Policyno = DBDriod.strins[3];
            this.Passanger = DBDriod.strins[0];
            this.Vehiclemake = DBDriod.strins[5];
            this.Vehiclemodel = DBDriod.strins[6];
            this.witnessname = DBDriod.strins[7];
            this.PhoneNumber = DBDriod.strins[8];
            this.injuredname = DBDriod.strins[9];
            this.injuredPhoneNUmber = DBDriod.strins[10];
            this.policename = DBDriod.strins[11];
            this.policeNumber = DBDriod.strins[12];
            this.reportnumber = DBDriod.strins[13];
        }
        this.fname.setText(this.Firstname);
        this.lname.setText(this.Lastname);
        this.dlno.setText(this.DLno);
        this.phno.setText(this.PhoneNo);
        this.emailid.setText(this.Email);
        this.licenseplate.setText(this.LicensePlate);
        this.make.setText(this.Make);
        this.model.setText(this.Model);
        this.date.setText(this.Timedate);
        this.time.setText(this.Time);
        this.add.setText(this.Address);
        this.city.setText(this.City);
        this.country.setText(this.Country);
        this.state.setText(this.State);
        this.roadsurfacecond.setText(this.Roadcond);
        this.lightcond.setText(this.Lightcond);
        this.weathercond.setText(this.Weather);
        try {
            this.driver.setText(list.get(0).toString());
            this.licenseplat.setText(list.get(1).toString());
            System.out.println("mail222222222222:::::::" + list.get(1).toString());
            this.insurancecomp.setText(list.get(2).toString());
            this.policyno.setText(list.get(3).toString());
            this.vehiclemake.setText(list.get(4).toString());
            this.vehiclemodel.setText(list.get(5).toString());
            this.witnessName.setText(list.get(6).toString());
            this.witnessphNo.setText(list.get(7).toString());
            this.injuredName.setText(list.get(8).toString());
            this.injuredphNo.setText(list.get(9).toString());
            this.policeName.setText(list.get(10).toString());
            this.policephNo.setText(list.get(11).toString());
            this.reportNumber.setText(list.get(12).toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            this.driver1.setText(list.get(14).toString());
            this.licenseplat1.setText(list.get(15).toString());
            this.insuranceCompany.setText(list.get(16).toString());
            this.policyNumber.setText(list.get(17).toString());
            this.vehiclemake1.setText(list.get(18).toString());
            this.vehiclemodel1.setText(list.get(19).toString());
            this.witnessName1.setText(list.get(20).toString());
            this.witnessphNo1.setText(list.get(21).toString());
            this.injuredName1.setText(list.get(22).toString());
            this.injuredPhNo1.setText(list.get(23).toString());
            this.policepnNo.setText(list.get(24).toString());
            this.policeName1.setText(list.get(25).toString());
            this.reportNumber1.setText(list.get(26).toString());
        } catch (Exception e2) {
            Log.d("Exception occurs", "Exception caught handled");
        }
        ArrayList<String> images = this.droid.getImage(this.time1, this.date1);
        ArrayList<String> audioRecord = this.droid.getmusic(this.date1, this.time1);
        ArrayList<String> textNote = this.droid.getNote(this.date1, this.time1);
        try {
            this.size = images.size() + audioRecord.size() + textNote.size();
            this.Docs = new String[this.size];
            int temp = 0;
            for (int i4 = 0; i4 < images.size(); i4++) {
                this.Docs[i4] = "/sdcard/" + images.get(i4);
                System.out.println("image: " + images.get(i4));
                temp++;
            }
            for (int i5 = 0; i5 < audioRecord.size(); i5++) {
                this.Docs[temp] = "/sdcard/com.hascode.recorder/" + audioRecord.get(i5);
                temp++;
            }
            for (int i6 = 0; i6 < textNote.size(); i6++) {
                if (textNote.get(i6).length() >= 3) {
                    temperory = String.valueOf(textNote.get(i6).substring(0, 3)) + this.date1 + ".txt";
                } else {
                    temperory = String.valueOf(textNote.get(i6)) + this.date1 + ".txt";
                }
                try {
                    File root = Environment.getExternalStorageDirectory();
                    if (root.canWrite()) {
                        BufferedWriter out = new BufferedWriter(new FileWriter(new File(root, temperory)));
                        out.write(textNote.get(i6));
                        out.close();
                    }
                } catch (IOException e3) {
                    System.out.println(e3);
                }
                this.Docs[temp] = "/sdcard/" + temperory;
                temp++;
            }
            if (this.size > 0) {
                zipfiles();
                this.uri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "zipfiles.zip"));
            }
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        this.send.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent email = new Intent("android.intent.action.SEND");
                email.setType("jpeg/image");
                email.putExtra("android.intent.extra.EMAIL", new String[]{"nmhull@lawyercarolina.com"});
                email.putExtra("android.intent.extra.SUBJECT", "Car Accident Report");
                Email.this.send.setImageResource(R.drawable.sendblue);
                email.putExtra("android.intent.extra.TEXT", "firstname:" + Email.this.fname.getText().toString() + "\n lastname:" + ((Object) Email.this.lname.getText()) + "\n" + ((Object) Email.this.phno.getText()) + "\n Email:\t" + ((Object) Email.this.emailid.getText()) + "\n LicensePlate: \t" + ((Object) Email.this.licenseplate.getText()) + "\n Make: \t" + ((Object) Email.this.make.getText()) + "\n Model: \t" + ((Object) Email.this.model.getText()) + "\n Date:\t" + ((Object) Email.this.date.getText()) + "\n Time:\t" + ((Object) Email.this.time.getText()) + "\n Address:\t" + ((Object) Email.this.add.getText()) + "\n City:" + ((Object) Email.this.city.getText()) + "\n Country:" + ((Object) Email.this.country.getText()) + "\n State:\t" + ((Object) Email.this.state.getText()) + "\n RoadSurfaceCondition:\t" + ((Object) Email.this.roadsurfacecond.getText()) + "\n lightCondition:\t" + ((Object) Email.this.lightcond.getText()) + "\n WeatherCondition:\t" + ((Object) Email.this.weathercond.getText()) + "\n Driver:\t" + ((Object) Email.this.driver.getText()) + "\n LicensePlate:\t" + ((Object) Email.this.licenseplat.getText()) + "\n InsuranceCompany:\t" + ((Object) Email.this.insurancecomp.getText()) + "\n PolicyNumber:\t" + ((Object) Email.this.policyno.getText()) + "\n VehicleMake:\t" + ((Object) Email.this.vehiclemake.getText()) + "\n VehicleModel:\t" + ((Object) Email.this.vehiclemodel.getText()) + "\n WitnessName:\t" + ((Object) Email.this.witnessName.getText()) + "\n WitnessPhoneNumber:\t" + ((Object) Email.this.witnessphNo.getText()) + "\n Injured Name:\t" + ((Object) Email.this.injuredName.getText()) + "\n Injured Phone Number:\t" + ((Object) Email.this.injuredphNo.getText()) + "\n PoliceName:\t" + ((Object) Email.this.policeName.getText()) + "\n Police Phone Number:\t" + ((Object) Email.this.policephNo.getText()) + "\n Report Number:\t" + ((Object) Email.this.reportNumber.getText()));
                if (Email.this.size > 0) {
                    email.putExtra("android.intent.extra.STREAM", Email.this.uri);
                }
                Email.this.startActivity(Intent.createChooser(email, "Send mail...."));
            }
        });
    }

    private void zipfiles() {
        try {
            byte[] buffer = new byte[1024];
            ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(new File(Environment.getExternalStorageDirectory(), "zipfiles.zip")));
            for (int i = 0; i < this.Docs.length; i++) {
                System.out.println("Adding " + this.Docs[i]);
                FileInputStream fin = new FileInputStream(this.Docs[i]);
                zout.putNextEntry(new ZipEntry(this.Docs[i]));
                while (true) {
                    int length = fin.read(buffer);
                    if (length <= 0) {
                        break;
                    }
                    zout.write(buffer, 0, length);
                }
                zout.closeEntry();
                fin.close();
            }
            zout.close();
            System.out.println("Zip file has been created!");
        } catch (IOException e) {
            System.out.println("IOException :" + e);
        }
    }
}
