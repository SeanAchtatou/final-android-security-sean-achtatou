package com.camera;

import android.hardware.Camera;

abstract class CameraFinder {
    static CameraFinder INSTANCE = buildFinder();

    /* access modifiers changed from: package-private */
    public abstract Camera open();

    CameraFinder() {
    }

    private static CameraFinder buildFinder() {
        return new ClassicFinder();
    }
}
