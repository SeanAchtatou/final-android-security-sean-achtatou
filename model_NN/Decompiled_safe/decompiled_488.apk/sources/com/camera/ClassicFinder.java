package com.camera;

import android.hardware.Camera;

class ClassicFinder extends CameraFinder {
    ClassicFinder() {
    }

    /* access modifiers changed from: package-private */
    public Camera open() {
        return Camera.open();
    }
}
