package com.camera;

import android.app.Activity;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.THOMSONROGERS.R;
import com.tritone.DBDriod;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class PictureDemo extends Activity {
    /* access modifiers changed from: private */
    public Camera camera = null;
    int counter = 0;
    DBDriod droid;
    /* access modifiers changed from: private */
    public boolean inPreview = false;
    ImageView ivcameraclick;
    int m_rotation = 0;
    Camera.PictureCallback photoCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            new SavePhotoTask().execute(data);
            camera.startPreview();
            PictureDemo.this.inPreview = true;
        }
    };
    private SurfaceView preview = null;
    /* access modifiers changed from: private */
    public SurfaceHolder previewHolder = null;
    SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                PictureDemo.this.camera.setPreviewDisplay(PictureDemo.this.previewHolder);
            } catch (Throwable th) {
                Throwable t = th;
                Log.e("PictureDemo-surfaceCallback", "Exception in setPreviewDisplay()", t);
                Toast.makeText(PictureDemo.this, t.getMessage(), 1).show();
            }
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Camera.Parameters parameters = PictureDemo.this.camera.getParameters();
            Camera.Size size = PictureDemo.this.getBestPreviewSize(width, height, parameters);
            if (size != null) {
                parameters.setPreviewSize(size.width, size.height);
                parameters.setPictureFormat(256);
                PictureDemo.this.camera.setParameters(parameters);
                PictureDemo.this.camera.startPreview();
                PictureDemo.this.inPreview = true;
            }
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(-3);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.maincam);
        this.droid = new DBDriod(this);
        this.preview = (SurfaceView) findViewById(R.id.surface_camera);
        this.ivcameraclick = (ImageView) findViewById(R.id.ImageViewcameraclick);
        this.previewHolder = this.preview.getHolder();
        this.previewHolder.addCallback(this.surfaceCallback);
        this.previewHolder.setType(3);
        this.ivcameraclick.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PictureDemo.this.inPreview) {
                    Camera.Parameters parameters = PictureDemo.this.camera.getParameters();
                    int nCamRotate = 90;
                    if ((PictureDemo.this.m_rotation > 45 && PictureDemo.this.m_rotation < 135) || (PictureDemo.this.m_rotation > 225 && PictureDemo.this.m_rotation < 315)) {
                        nCamRotate = 0;
                    }
                    System.out.println(" take picture");
                    parameters.set("rotation", nCamRotate);
                    PictureDemo.this.camera.setParameters(parameters);
                    PictureDemo.this.camera.takePicture(null, null, PictureDemo.this.photoCallback);
                    PictureDemo.this.inPreview = false;
                }
            }
        });
    }

    public void onResume() {
        super.onResume();
        this.camera = CameraFinder.INSTANCE.open();
    }

    public void onPause() {
        if (this.inPreview) {
            this.camera.stopPreview();
        }
        this.camera.release();
        this.camera = null;
        this.inPreview = false;
        super.onPause();
    }

    /* access modifiers changed from: private */
    public Camera.Size getBestPreviewSize(int width, int height, Camera.Parameters parameters) {
        Camera.Size result = null;
        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    if (size.width * size.height > result.width * result.height) {
                        result = size;
                    }
                }
            }
        }
        return result;
    }

    class SavePhotoTask extends AsyncTask<byte[], String, String> {
        SavePhotoTask() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(byte[]... jpeg) {
            PictureDemo.this.counter++;
            SharedPreferences myPrefs = PictureDemo.this.getSharedPreferences("myPrefs", 1);
            String time = myPrefs.getString("time", "time");
            String date = myPrefs.getString("date", "date");
            Calendar cal = Calendar.getInstance();
            String temp = new StringBuilder(String.valueOf(cal.get(10) + cal.get(12) + cal.get(13))).toString();
            PictureDemo.this.droid.createimage(time, date, "driver" + date + temp + PictureDemo.this.counter + ".jpg");
            File photo = new File(Environment.getExternalStorageDirectory(), "driver" + date + temp + PictureDemo.this.counter + ".jpg");
            if (photo.exists()) {
                photo.delete();
            }
            try {
                FileOutputStream fos = new FileOutputStream(photo.getPath());
                fos.write(jpeg[0]);
                fos.close();
                return null;
            } catch (IOException e) {
                Log.e("PictureDemo", "Exception in photoCallback", e);
                return null;
            }
        }
    }
}
