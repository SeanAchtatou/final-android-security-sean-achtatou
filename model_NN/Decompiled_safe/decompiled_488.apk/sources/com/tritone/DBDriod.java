package com.tritone;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class DBDriod {
    public static final String CREATE_TABLE_DRAW = "CREATE TABLE IF NOT EXISTS Drawing(Time VARCHAR(25),DATE VARCHAR(25),draw BLOB);";
    public static final String CREATE_TABLE_EDITFORM = "CREATE TABLE IF NOT EXISTS EditFormLatestnew1( id INTEGER PRIMARY KEY AUTOINCREMENT,Date VARCHAR(25),Time VARCHAR(25),FirstName VARCHAR(10),LastName VARCHAR(10),DLNO VARCHAR(10),Pno VARCHAR(10),Email VARCHAR(25),injured VARCHAR(9),VehicleMake VARCHAR(10),VehicleModel VARCHAR(10),LicensePlate VARCHAR(10))";
    public static final String CREATE_TABLE_IMAGE = "CREATE TABLE IF NOT EXISTS imagenew123(Time VARCHAR(25),DATE VARCHAR(25),myImage BLOB);";
    public static final String CREATE_TABLE_INSURANCE = "CREATE TABLE IF NOT EXISTS LatestInsurancenew1(Date VARCHAR(25),Time VARCHAR(25),Driver VARCHAR(10),DriverphoneNumber VARCHAR(10),LicensePlate VARCHAR(10),InsuranceCompany VARCHAR(10),PolicyNumber VARCHAR(10),VehicleMake VARCHAR(10),VehicleModel VARCHAR(10),WitnessName VARCHAR(10),PhoneNumber VARCHAR(10),InjuredName VARCHAR(10),PhoneNumber1 VARCHAR(10),PoliceName VARCHAR(10),PhoneNumber2 VARCHAR(10),ReportNumber VARCHAR(10))";
    public static final String CREATE_TABLE_LOCATION = "CREATE TABLE IF NOT EXISTS newLocationnew(id INTEGER PRIMARY KEY AUTOINCREMENT, tempDate VARCHAR(25),tempTime VARCHAR(25),timedate VARCHAR(50),Time VARCHAR(25),DATE VARCHAR(25),Address VARCHAR(50),city VARCHAR(15),country VARCHAR(15),state VARCHAR(15),ReadSurfaceCondition VARCHAR(10),lightCondition VARCHAR(10),weatherCondition VARCHAR(10),witness VARCHAR(10))";
    public static final String CREATE_TABLE_MUSIC = "CREATE TABLE IF NOT EXISTS recordnew(Time VARCHAR(25),DATE VARCHAR(25),music VARCAR(20));";
    public static final String DATABASE_NAME = "CarInsurancelatest.dbnewagain12345";
    public static final int DATABASE_VERSION = 1;
    public static String DlNo = "";
    public static String Driver = "";
    public static String DriverphoneNumber = "";
    public static String Email = "";
    public static String Firstname = "";
    public static String InjuredName = "";
    public static String InsuranceCompany = "";
    public static String Lastname = "";
    public static String LicensePlate = "";
    public static String PhoneNumber = "";
    public static String PhoneNumber1 = "";
    public static String PhoneNumber2 = "";
    public static String Pno = "";
    public static String PoliceName = "";
    public static String PolicyNumber = "";
    public static String ReadSurfaceCondition = "";
    public static String ReportNumber = "";
    private static final String TABLE_NOTES = "notesnew";
    public static final String USER_TABLE = "newLocationnew";
    public static final String USER_TABLE2 = "LatestInsurancenew1";
    public static final String USER_TABLE_NAME = "EditFormLatestnew1";
    public static String VehicleMake = "";
    public static String VehicleModel = "";
    public static String WitnessName = "";
    public static String address = "";
    public static String city = "";
    public static String country = "";
    public static String date = "";
    public static ArrayList<String> dateData = new ArrayList<>();
    public static String[] dateTime = null;
    public static final String draw = "Drawing";
    public static final String image = "imagenew123";
    public static String injured = "";
    public static String licensePlate = "";
    public static String lightCondition = "";
    public static final String recorder = "recordnew";
    public static String state = "";
    public static String[] str = {Firstname, Lastname, DlNo, Pno, Email, injured, licensePlate, vehicleMake, vehicleModel};
    public static String[] str1 = {timedate, time, date, address, city, country, state, ReadSurfaceCondition, lightCondition, weatherCondition, witness};
    public static String[] str2 = {Driver, DriverphoneNumber, LicensePlate, InsuranceCompany, PolicyNumber, VehicleMake, VehicleModel, WitnessName, PhoneNumber, InjuredName, PhoneNumber1, PoliceName, PhoneNumber2, ReportNumber};
    public static String[] str6;
    public static String[] str7;
    public static String[] str8;
    public static String[] stredit;
    public static String[] strins;
    public static String[] strloc;
    public static String time = "";
    public static ArrayList<String> timeData = new ArrayList<>();
    public static String timedate = "";
    public static String vehicleMake = "";
    public static String vehicleModel = "";
    public static String weatherCondition = "";
    public static String witness = "";
    public Context context;
    ArrayList<String> contry = new ArrayList<>();
    ArrayList<String> date1 = new ArrayList<>();
    ArrayList<String> list = new ArrayList<>();
    String music12 = "";
    public SQLiteDatabase myDb;
    String myimage = "";
    ArrayList<String> name = new ArrayList<>();
    ArrayList<String> policyNumber = new ArrayList<>();
    String[][] str3;
    String[][] str4;
    String[][] str5;
    String[] str9;

    public DBDriod(Context context2) {
        this.context = context2;
        openConnection();
        this.myDb.execSQL(CREATE_TABLE_EDITFORM);
        this.myDb.execSQL(CREATE_TABLE_LOCATION);
        this.myDb.execSQL(CREATE_TABLE_INSURANCE);
        this.myDb.execSQL(CREATE_TABLE_IMAGE);
        this.myDb.execSQL(CREATE_TABLE_MUSIC);
        this.myDb.execSQL(CREATE_TABLE_DRAW);
        closeConnection();
    }

    public void openConnection() {
        this.myDb = this.context.openOrCreateDatabase(DATABASE_NAME, 0, null);
    }

    public void closeConnection() {
        this.myDb.close();
    }

    public void createEditForm(String date2, String time2, String Firstname2, String LastName, String DLNO, String Pno2, String Email2, String injured2, String vehiclemake, String vehiclemodel, String licenseplate) {
        Firstname = Firstname2;
        Lastname = LastName;
        DlNo = DLNO;
        Pno = Pno2;
        Email = Email2;
        injured = injured2;
        vehicleMake = vehiclemake;
        vehicleModel = vehiclemodel;
        licensePlate = licenseplate;
        openConnection();
        this.myDb.execSQL("INSERT INTO EditFormLatestnew1 (Date, Time, FirstName, LastName, DLNO, Pno, Email, injured, VehicleMake, VehicleModel, LicensePlate) VALUES('" + date2 + "','" + time2 + "','" + Firstname2 + "','" + LastName + "','" + DLNO + "','" + Pno2 + "','" + Email2 + "','" + injured2 + "','" + vehiclemake + "','" + vehiclemodel + "','" + licenseplate + "');");
        closeConnection();
    }

    public void updateEditFormContent(String date2, String time2, String newdate, String newtime, String Firstname2, String LastName, String DLNO, String Pno2, String Email2, String injured2, String vehiclemake, String vehiclemodel, String licenseplate) {
        openConnection();
        this.myDb.execSQL("UPDATE EditFormLatestnew1 set Date  = '" + newdate + "' , Time  = '" + newtime + "', Firstname = '" + Firstname2 + "',Lastname = '" + LastName + "',DLNO = '" + DLNO + "', Pno = '" + Pno2 + "', Email = '" + Email2 + "', injured = '" + injured2 + "', VehicleMake = '" + vehiclemake + "', VehicleModel = '" + vehiclemodel + "', LicensePlate = '" + licenseplate + "' WHERE Date  = '" + date2 + "' and Time  = '" + time2 + "';");
        closeConnection();
    }

    public String[] getEditFormDateTime() {
        try {
            dateTime = new String[2];
            openConnection();
            Cursor c = this.myDb.query(USER_TABLE_NAME, new String[]{"Date", "Time"}, null, null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    dateTime[0] = c.getString(0);
                    dateTime[1] = c.getString(1);
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return str;
    }

    public String[] getEditForm() {
        try {
            openConnection();
            Cursor c = this.myDb.query(USER_TABLE_NAME, new String[]{"FirstName", "LastName", "DLNO", "Pno", "Email", "injured", "vehicleMake", "vehicleModel", "licensePlate"}, null, null, null, null, null);
            int numRows = c.getCount();
            System.out.println("number of records" + numRows);
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    str[0] = c.getString(0);
                    System.out.println("str[0]:::" + str[0]);
                    str[1] = c.getString(1);
                    System.out.println("str[1]::::" + str[1]);
                    str[2] = c.getString(2);
                    System.out.println("str[2]:::" + str[2]);
                    str[3] = c.getString(3);
                    System.out.println("str[3]:::::" + str[3]);
                    str[4] = c.getString(4);
                    System.out.println("str[4]:::::" + str[4]);
                    str[5] = c.getString(5);
                    System.out.println("str[4]:::::" + str[5]);
                    str[6] = c.getString(6);
                    System.out.println("str[6]::::" + str[6]);
                    str[7] = c.getString(7);
                    System.out.println("str[7]::::::" + str[7]);
                    str[8] = c.getString(8);
                    System.out.println("str[8]:::::::" + str[8]);
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return str;
    }

    public String[] getAllEditForm(int id) {
        try {
            stredit = new String[9];
            openConnection();
            Cursor c = this.myDb.query(USER_TABLE_NAME, new String[]{"FirstName", "LastName", "DLNO", "Pno", "Email", "injured", "vehicleMake", "vehicleModel", "licensePlate"}, null, null, null, null, null);
            int numRows = c.getCount();
            System.out.println("number of records" + numRows + " : " + id);
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    if (id == i) {
                        stredit[0] = c.getString(0);
                        System.out.println("str[4]:::::" + stredit[0]);
                        stredit[1] = c.getString(1);
                        System.out.println("str[4]:::::" + stredit[1]);
                        stredit[2] = c.getString(2);
                        System.out.println("str[4]:::::" + stredit[2]);
                        stredit[3] = c.getString(3);
                        System.out.println("str[4]:::::" + stredit[3]);
                        stredit[4] = c.getString(4);
                        System.out.println("str[4]:::::" + stredit[4]);
                        stredit[5] = c.getString(5);
                        System.out.println("str[4]:::::" + stredit[5]);
                        stredit[6] = c.getString(6);
                        System.out.println("str[4]:::::" + stredit[6]);
                        stredit[7] = c.getString(7);
                        System.out.println("str[4]:::::" + stredit[7]);
                        stredit[8] = c.getString(8);
                        System.out.println("str[4]:::::" + stredit[8]);
                    }
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return str;
    }

    public ArrayList<String> getEditCount() {
        try {
            openConnection();
            Cursor c = this.myDb.query(USER_TABLE_NAME, new String[]{"FirstName", "LastName", "DLNO", "Pno", "Email", "injured", "vehicleMake", "vehicleModel", "licensePlate"}, null, null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    this.name.add(c.getString(0));
                    System.out.println("name:::::::::::" + this.name.get(0));
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return this.name;
    }

    public void deleteEditForm(String dlno) {
        openConnection();
        this.myDb.execSQL("DELETE FROM EditFormLatestnew1 WHERE DLNO = '" + dlno + "' ;");
        closeConnection();
    }

    public void updateEditForm(String Firstname2, String LastName, String DLNO, String Pno2, String Email2) {
        openConnection();
        this.myDb.execSQL("UPDATE EditFormLatestnew1 set Firstname = '" + Firstname2 + "',Lastname = '" + LastName + "', Pno = '" + Pno2 + "', Email = '" + Email2 + "' WHERE DLNO= '" + DLNO + "';");
        closeConnection();
    }

    public void createInsurance(String date2, String time2, String Driver2, String DriverphoneNumber2, String LicensePlate2, String InsuranceCompany2, String PolicyNumber2, String VehicleMake2, String VehicleModel2, String WitnessName2, String phoneNumber, String InjuredName2, String PhoneNumber12, String PoliceName2, String PhoneNumber22, String ReportNumber2) {
        LicensePlate = LicensePlate2;
        openConnection();
        this.myDb.execSQL("INSERT INTO LatestInsurancenew1 (Date,Time,Driver,DriverphoneNumber,LicensePlate,InsuranceCompany,PolicyNumber,VehicleMake,VehicleModel,WitnessName,PhoneNumber,InjuredName,PhoneNumber1,PoliceName,PhoneNumber2,ReportNumber) VALUES('" + date2 + "','" + time2 + "','" + Driver2 + "','" + DriverphoneNumber2 + "','" + LicensePlate2 + "','" + InsuranceCompany2 + "','" + PolicyNumber2 + "','" + VehicleMake2 + "','" + VehicleModel2 + "','" + WitnessName2 + "','" + phoneNumber + "','" + InjuredName2 + "','" + PhoneNumber12 + "','" + PoliceName2 + "','" + PhoneNumber22 + "','" + ReportNumber2 + "');");
        closeConnection();
    }

    public void updateInsurance(String date2, String time2, String newdate, String newtime, String Driver2, String DriverphoneNumber2, String LicensePlate2, String InsuranceCompany2, String PolicyNumber2, String VehicleMake2, String VehicleModel2, String WitnessName2, String phoneNumber, String InjuredName2, String PhoneNumber12, String PoliceName2, String PhoneNumber22, String ReportNumber2) {
        openConnection();
        this.myDb.execSQL("UPDATE LatestInsurancenew1 set Date = '" + newdate + "' , Time = '" + newtime + "', Driver = '" + Driver2 + "',DriverphoneNumber = '" + DriverphoneNumber2 + "', LicensePlate = '" + LicensePlate2 + "', InsuranceCompany = '" + InsuranceCompany2 + "' , PolicyNumber = '" + PolicyNumber2 + "' , VehicleMake = '" + VehicleMake2 + "' , VehicleModel = '" + VehicleModel2 + "' , WitnessName = '" + WitnessName2 + "', PhoneNumber = '" + phoneNumber + "', InjuredName = '" + InjuredName2 + "', PhoneNumber1 = '" + PhoneNumber12 + "', PoliceName = '" + PoliceName2 + "', PhoneNumber2 = '" + PhoneNumber22 + "', ReportNumber = '" + ReportNumber2 + "' WHERE Date = '" + date2 + "' and Time = '" + time2 + "';");
        closeConnection();
    }

    public ArrayList<String> allRecords() {
        try {
            openConnection();
            Cursor c = this.myDb.rawQuery("SELECT Driver,LicensePlate,InsuranceCompany,PolicyNumber,VehicleMake,VehicleModel,WitnessName,PhoneNumber,InjuredName,PhoneNumber1,PoliceName,PhoneNumber2,ReportNumber FROM LatestInsurancenew1" + (" DATE= '" + date + "' AND time='" + time + "'"), null);
            if (c == null || !c.moveToFirst()) {
                return this.list;
            }
            do {
                String firstName = c.getString(c.getColumnIndex("Driver"));
                String licensePlate2 = c.getString(c.getColumnIndex("LicensePlate"));
                String insuranceCompany = c.getString(c.getColumnIndex("InsuranceCompany"));
                String policyNumber2 = c.getString(c.getColumnIndex("PolicyNumber"));
                String vehicleMake2 = c.getString(c.getColumnIndex("VehicleMake"));
                String vehicleModel2 = c.getString(c.getColumnIndex("VehicleModel"));
                String witnessName = c.getString(c.getColumnIndex("WitnessName"));
                String phoneNumber = c.getString(c.getColumnIndex("PhoneNumber"));
                String injuredName = c.getString(c.getColumnIndex("InjuredName"));
                String phoneNumber1 = c.getString(c.getColumnIndex("PhoneNumber1"));
                String policeName = c.getString(c.getColumnIndex("PoliceName"));
                String phoneNumber2 = c.getString(c.getColumnIndex("PhoneNumber2"));
                this.list.add(firstName + ",Age: " + licensePlate2 + insuranceCompany + policyNumber2 + vehicleMake2 + vehicleModel2 + witnessName + phoneNumber + injuredName + phoneNumber1 + policeName + phoneNumber2 + c.getString(c.getColumnIndex("ReportNumber")));
            } while (c.moveToNext());
            return this.list;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    public String[] getInsuranceFormDateTime() {
        try {
            dateTime = new String[2];
            openConnection();
            Cursor c = this.myDb.query(USER_TABLE2, new String[]{"Date", "Time"}, null, null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    dateTime[0] = c.getString(0);
                    dateTime[1] = c.getString(1);
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return str2;
    }

    public String[] getInsuranceForm() {
        try {
            openConnection();
            Cursor c = this.myDb.query(USER_TABLE2, new String[]{"Driver", "DriverphoneNumber", "LicensePlate", "InsuranceCompany", "PolicyNumber", "VehicleMake", "VehicleModel", "WitnessName", "PhoneNumber", "InjuredName", "PhoneNumber1", "PoliceName", "PhoneNumber2", "ReportNumber"}, null, null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    str2[0] = c.getString(0);
                    str2[1] = c.getString(1);
                    str2[2] = c.getString(2);
                    str2[3] = c.getString(3);
                    str2[4] = c.getString(4);
                    str2[5] = c.getString(5);
                    str2[6] = c.getString(6);
                    str2[7] = c.getString(7);
                    str2[8] = c.getString(8);
                    str2[9] = c.getString(9);
                    str2[10] = c.getString(10);
                    str2[11] = c.getString(11);
                    str2[12] = c.getString(12);
                    str2[13] = c.getString(13);
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return str2;
    }

    public ArrayList getSpecificInsurance(String date2, String time2) {
        ArrayList list2 = new ArrayList();
        try {
            openConnection();
            Cursor c = this.myDb.query(USER_TABLE2, new String[]{"Driver", "DriverphoneNumber", "LicensePlate", "InsuranceCompany", "PolicyNumber", "VehicleMake", "VehicleModel", "WitnessName", "PhoneNumber", "InjuredName", "PhoneNumber1", "PoliceName", "PhoneNumber2", "ReportNumber", "Date", "Time"}, " Date= '" + date2 + "' AND Time='" + time2 + "'", null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    list2.add(c.getString(0));
                    System.out.println("insur: " + c.getString(0));
                    list2.add(c.getString(1));
                    System.out.println("insur: " + c.getString(1));
                    list2.add(c.getString(2));
                    System.out.println("insur: " + c.getString(2));
                    list2.add(c.getString(3));
                    System.out.println("insur: " + c.getString(3));
                    list2.add(c.getString(4));
                    System.out.println("insur: " + c.getString(4));
                    list2.add(c.getString(5));
                    System.out.println("insur: " + c.getString(5));
                    list2.add(c.getString(6));
                    System.out.println("insur: " + c.getString(6));
                    list2.add(c.getString(7));
                    System.out.println("insur: " + c.getString(7));
                    list2.add(c.getString(8));
                    System.out.println("insur: " + c.getString(8));
                    list2.add(c.getString(9));
                    System.out.println("insur: " + c.getString(9));
                    list2.add(c.getString(10));
                    System.out.println("insur: " + c.getString(10));
                    list2.add(c.getString(11));
                    System.out.println("insur: " + c.getString(11));
                    list2.add(c.getString(12));
                    System.out.println("insur: " + c.getString(12));
                    list2.add(c.getString(13));
                    System.out.println("insur: " + c.getString(13));
                    System.out.println("insur: " + c.getString(14));
                    System.out.println("insur: " + c.getString(15));
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return list2;
    }

    public int getSpecificInsuranceRecordCount(String date2, String time2) {
        int count = 0;
        try {
            openConnection();
            Cursor c = this.myDb.query(USER_TABLE2, new String[]{"Driver", "DriverphoneNumber", "LicensePlate", "InsuranceCompany", "PolicyNumber", "VehicleMake", "VehicleModel", "WitnessName", "PhoneNumber", "InjuredName", "PhoneNumber1", "PoliceName", "PhoneNumber2", "ReportNumber", "Date", "Time"}, " Date= '" + date2 + "' AND Time='" + time2 + "'", null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    count++;
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return count;
    }

    public String[] getAllInsuranceForm(int id) {
        try {
            strins = new String[14];
            openConnection();
            System.out.println("getInsuranceForm111");
            Cursor c = this.myDb.query(USER_TABLE2, new String[]{"Driver", "DriverphoneNumber", "LicensePlate", "InsuranceCompany", "PolicyNumber", "VehicleMake", "VehicleModel", "WitnessName", "PhoneNumber", "InjuredName", "PhoneNumber1", "PoliceName", "PhoneNumber2", "ReportNumber"}, null, null, null, null, null);
            System.out.println("getInsuranceForm112");
            int numRows = c.getCount();
            System.out.println("getInsuranceForm113");
            c.moveToFirst();
            System.out.println("getInsuranceForm114");
            if (c != null) {
                System.out.println("getInsuranceForm115");
                for (int i = 0; i < numRows; i++) {
                    if (id == i) {
                        strins[0] = c.getString(0);
                        strins[1] = c.getString(1);
                        strins[2] = c.getString(2);
                        strins[3] = c.getString(3);
                        strins[4] = c.getString(4);
                        strins[5] = c.getString(5);
                        strins[6] = c.getString(6);
                        strins[7] = c.getString(7);
                        strins[8] = c.getString(8);
                        strins[9] = c.getString(9);
                        strins[10] = c.getString(10);
                        strins[11] = c.getString(11);
                        strins[12] = c.getString(12);
                        strins[13] = c.getString(13);
                    }
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return str2;
    }

    public void deleteInsuranceForm(String LicensePlate2) {
        openConnection();
        this.myDb.execSQL("DELETE FROM LatestInsurancenew1 WHERE LicensePlate = '" + LicensePlate2 + "' ;");
        closeConnection();
    }

    public void updateInsuranceForm(String Driver2, String DriverphoneNumber2, String LicensePlate2, String InsuranceCompany2, String PolicyNumber2, String PassengerNumber, String VehicleMake2) {
        openConnection();
        this.myDb.execSQL("UPDATE LatestInsurancenew1 set Driver = '" + Driver2 + "', LicensePlate = '" + LicensePlate2 + "', InsuranceCompany = '" + InsuranceCompany2 + "', PassengerNumber = '" + PassengerNumber + "', VehicleMake = '" + VehicleMake2 + "' , PolicyNumber= '" + PolicyNumber2 + "';");
        closeConnection();
    }

    public void createLocation(String tempDate, String tempTime, String timedate2, String Time, String Date, String Address, String city2, String country2, String state2, String ReadSurfaceCondition2, String lightCondition2, String weatherCondition2, String witness2) {
        time = Time;
        date = Date;
        openConnection();
        this.myDb.execSQL("INSERT INTO newLocationnew (tempDate, tempTime, timedate,Time,Date,Address,city,country,state,ReadSurfaceCondition,lightCondition,weatherCondition,witness) VALUES('" + tempDate + "','" + tempTime + "','" + timedate2 + "','" + Time + "','" + Date + "','" + Address + "','" + city2 + "','" + country2 + "','" + state2 + "','" + ReadSurfaceCondition2 + "','" + lightCondition2 + "','" + weatherCondition2 + "','" + witness2 + "');");
        closeConnection();
    }

    public void updateLocation(String tempDate, String tempTime, String newtempDate, String newtempTime, String timedate2, String Time, String Date, String Address, String city2, String country2, String state2, String ReadSurfaceCondition2, String lightCondition2, String weatherCondition2, String witness2) {
        openConnection();
        this.myDb.execSQL("UPDATE newLocationnew set tempDate = '" + newtempDate + "' , tempTime = '" + newtempTime + "', timedate = '" + timedate2 + "', Time = '" + Time + "', Date = '" + Date + "', Address = '" + Address + "', city = '" + city2 + "' , country= '" + country2 + "', state= '" + state2 + "', ReadSurfaceCondition= '" + ReadSurfaceCondition2 + "', lightCondition= '" + lightCondition2 + "', weatherCondition= '" + weatherCondition2 + "', witness= '" + witness2 + "'  WHERE tempDate = '" + tempDate + "' and tempTime = '" + tempTime + "';");
        closeConnection();
    }

    public String[] getLocationFormDateTime() {
        try {
            dateTime = new String[2];
            openConnection();
            Cursor c = this.myDb.query("newLocationnew", new String[]{"tempDate", "tempTime"}, null, null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    dateTime[0] = c.getString(0);
                    dateTime[1] = c.getString(1);
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return str1;
    }

    public String[] getLocationForm() {
        try {
            openConnection();
            Cursor c = this.myDb.query("newLocationnew", new String[]{"timedate", "Time", "Date", "Address", "city", "country", "state", "ReadSurfaceCondition", "lightCondition", "weatherCondition", "witness"}, null, null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    str1[0] = c.getString(0);
                    str1[1] = c.getString(1);
                    str1[2] = c.getString(2);
                    str1[3] = c.getString(3);
                    str1[4] = c.getString(4);
                    str1[5] = c.getString(5);
                    str1[6] = c.getString(6);
                    str1[7] = c.getString(7);
                    str1[8] = c.getString(8);
                    str1[9] = c.getString(9);
                    str1[10] = c.getString(10);
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return str1;
    }

    public String[] getAllLocationForm(int id) {
        try {
            strloc = new String[11];
            openConnection();
            Cursor c = this.myDb.query("newLocationnew", new String[]{"timedate", "Time", "Date", "Address", "city", "country", "state", "ReadSurfaceCondition", "lightCondition", "weatherCondition", "witness"}, null, null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    if (id == i) {
                        strloc[0] = c.getString(0);
                        strloc[1] = c.getString(1);
                        strloc[2] = c.getString(2);
                        strloc[3] = c.getString(3);
                        strloc[4] = c.getString(4);
                        strloc[5] = c.getString(5);
                        strloc[6] = c.getString(6);
                        strloc[7] = c.getString(7);
                        strloc[8] = c.getString(8);
                        strloc[9] = c.getString(9);
                        strloc[10] = c.getString(10);
                    }
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return str1;
    }

    public void deleteLocationForm(String policeId) {
        openConnection();
        this.myDb.execSQL("DELETE FROM newLocationnew WHERE policeId = '" + policeId + "' ;");
        closeConnection();
    }

    public ArrayList<String> getLocationCount() {
        try {
            openConnection();
            Cursor c = this.myDb.query("newLocationnew", new String[]{"timedate", "Time", "Date", "Address", "city", "country", "state", "ReadSurfaceCondition", "lightCondition", "weatherCondition", "witness"}, null, null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    this.contry.add(c.getString(4));
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return this.contry;
    }

    public void deleteInsurance(String date2, String time2) {
        openConnection();
        this.myDb.execSQL("DELETE FROM LatestInsurancenew1 WHERE Date = '" + date2 + "' and Time = '" + time2 + "' ;");
        closeConnection();
    }

    public ArrayList<String> getInsuranceCount() {
        try {
            openConnection();
            Cursor c = this.myDb.query(USER_TABLE2, new String[]{"Driver", "DriverphoneNumber", "LicensePlate", "InsuranceCompany", "PolicyNumber", "PassengerNumber", "VehicleMake", "VehicleModel"}, null, null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    this.policyNumber.add(c.getString(3));
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return this.policyNumber;
    }

    public void deleteLocation(String date2, String time2) {
        openConnection();
        this.myDb.execSQL("DELETE FROM newLocationnew WHERE tempDate = '" + date2 + "' and tempTime = '" + time2 + "' ;");
        closeConnection();
    }

    public void deleteEditForm(String date2, String time2) {
        openConnection();
        this.myDb.execSQL("DELETE FROM EditFormLatestnew1 WHERE Date = '" + date2 + "' and Time = '" + time2 + "' ;");
        closeConnection();
    }

    public ArrayList<String> getLocationcount() {
        try {
            openConnection();
            Cursor c = this.myDb.query("newLocationnew", new String[]{"timedate", "tempDate", "tempTime", "Address", "city", "country", "state", "ReadSurfaceCondition", "lightCondition", "weatherCondition", "witness"}, null, null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    this.date1.add(c.getString(0));
                    dateData.add(c.getString(1));
                    timeData.add(c.getString(2));
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return this.date1;
    }

    public ArrayList<String> getLocationDate() {
        ArrayList<String> date2 = new ArrayList<>();
        try {
            openConnection();
            Cursor c = this.myDb.query("newLocationnew", new String[]{"timedate", "tempDate", "tempTime", "Address", "city", "country", "state", "ReadSurfaceCondition", "lightCondition", "weatherCondition", "witness"}, null, null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    date2.add(c.getString(1));
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return date2;
    }

    public ArrayList<String> getLocationTime() {
        ArrayList<String> time2 = new ArrayList<>();
        try {
            openConnection();
            Cursor c = this.myDb.query("newLocationnew", new String[]{"timedate", "tempDate", "tempTime", "Address", "city", "country", "state", "ReadSurfaceCondition", "lightCondition", "weatherCondition", "witness"}, null, null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    time2.add(c.getString(2));
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return time2;
    }

    public String[][] getEditlist(String date2, String time2) {
        try {
            openConnection();
            Cursor c = this.myDb.query(USER_TABLE_NAME, new String[]{"FirstName", "LastName", "DLNO", "Pno", "Email", "injured", "vehicleMake", "vehicleModel", "licensePlate", "Date", "Time"}, " Date= '" + date2 + "' AND Time='" + time2 + "'", null, null, null, null);
            int numRows = c.getCount();
            this.str3 = (String[][]) Array.newInstance(String.class, numRows, 9);
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    this.str3[i][0] = c.getString(0);
                    System.out.println(" edit: " + c.getString(0));
                    this.str3[i][1] = c.getString(1);
                    System.out.println(" edit: " + c.getString(1));
                    this.str3[i][2] = c.getString(2);
                    System.out.println(" edit: " + c.getString(2));
                    this.str3[i][3] = c.getString(3);
                    System.out.println(" edit: " + c.getString(3));
                    this.str3[i][4] = c.getString(4);
                    System.out.println(" edit: " + c.getString(4));
                    this.str3[i][5] = c.getString(5);
                    System.out.println(" edit: " + c.getString(5));
                    this.str3[i][6] = c.getString(6);
                    System.out.println(" edit: " + c.getString(6));
                    this.str3[i][7] = c.getString(7);
                    System.out.println(" edit: " + c.getString(7));
                    this.str3[i][8] = c.getString(8);
                    System.out.println(" edit: " + c.getString(8));
                    System.out.println(" edit: " + c.getString(9));
                    System.out.println(" edit: " + c.getString(10));
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return this.str3;
    }

    public int getEditlistRecordCount(String date2, String time2) {
        int numRows = 0;
        try {
            openConnection();
            Cursor c = this.myDb.query(USER_TABLE_NAME, new String[]{"FirstName", "LastName", "DLNO", "Pno", "Email", "injured", "vehicleMake", "vehicleModel", "licensePlate", "Date", "Time"}, " Date= '" + date2 + "' AND Time='" + time2 + "'", null, null, null, null);
            numRows = c.getCount();
            c.moveToFirst();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return numRows;
    }

    public String[][] getEditlist() {
        try {
            openConnection();
            String str10 = " Date= '" + date + "' AND Time='" + time + "'";
            Cursor c = this.myDb.query(USER_TABLE_NAME, new String[]{"FirstName", "LastName", "DLNO", "Pno", "Email", "injured", "vehicleMake", "vehicleModel", "licensePlate"}, null, null, null, null, null);
            int numRows = c.getCount();
            this.str3 = (String[][]) Array.newInstance(String.class, numRows, 9);
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    this.str3[i][0] = c.getString(0);
                    this.str3[i][1] = c.getString(1);
                    this.str3[i][2] = c.getString(2);
                    this.str3[i][3] = c.getString(3);
                    this.str3[i][4] = c.getString(4);
                    this.str3[i][5] = c.getString(5);
                    this.str3[i][6] = c.getString(6);
                    this.str3[i][7] = c.getString(7);
                    this.str3[i][8] = c.getString(8);
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return this.str3;
    }

    public String[][] getLocationlist(String date2, String time2) {
        try {
            openConnection();
            Cursor c = this.myDb.query("newLocationnew", new String[]{"timedate", "Time", "Date", "Address", "city", "country", "state", "ReadSurfaceCondition", "lightCondition", "weatherCondition", "witness", "tempDate", "tempTime"}, " tempDate= '" + date2 + "' AND tempTime='" + time2 + "'", null, null, null, null);
            int numRows = c.getCount();
            this.str4 = (String[][]) Array.newInstance(String.class, numRows, 12);
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    this.str4[i][0] = c.getString(0);
                    System.out.println("location: " + c.getString(0));
                    this.str4[i][1] = c.getString(1);
                    System.out.println("location: " + c.getString(1));
                    this.str4[i][2] = c.getString(2);
                    System.out.println("location: " + c.getString(2));
                    this.str4[i][3] = c.getString(3);
                    System.out.println("location: " + c.getString(3));
                    this.str4[i][4] = c.getString(4);
                    System.out.println("location: " + c.getString(4));
                    this.str4[i][5] = c.getString(5);
                    System.out.println("location: " + c.getString(5));
                    this.str4[i][6] = c.getString(6);
                    System.out.println("location: " + c.getString(6));
                    this.str4[i][7] = c.getString(7);
                    System.out.println("location: " + c.getString(7));
                    this.str4[i][8] = c.getString(8);
                    System.out.println("location: " + c.getString(8));
                    this.str4[i][9] = c.getString(9);
                    System.out.println("location: " + c.getString(9));
                    this.str4[i][10] = c.getString(10);
                    System.out.println("location: " + c.getString(10));
                    System.out.println("location: " + c.getString(11));
                    System.out.println("location: " + c.getString(12));
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return this.str4;
    }

    public int getLocationlistRecordCount(String date2, String time2) {
        int numRows = 0;
        try {
            openConnection();
            Cursor c = this.myDb.query("newLocationnew", new String[]{"timedate", "Time", "Date", "Address", "city", "country", "state", "ReadSurfaceCondition", "lightCondition", "weatherCondition", "witness", "tempDate", "tempTime"}, " tempDate= '" + date2 + "' AND tempTime='" + time2 + "'", null, null, null, null);
            numRows = c.getCount();
            c.moveToFirst();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return numRows;
    }

    public String[][] getLocationlist() {
        try {
            openConnection();
            String str10 = " tempDate= '" + date + "' AND tempTime='" + time + "'";
            Cursor c = this.myDb.query("newLocationnew", new String[]{"timedate", "Time", "Date", "Address", "city", "country", "state", "ReadSurfaceCondition", "lightCondition", "weatherCondition", "witness"}, null, null, null, null, null);
            int numRows = c.getCount();
            this.str4 = (String[][]) Array.newInstance(String.class, numRows, 12);
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    this.str4[i][0] = c.getString(0);
                    this.str4[i][1] = c.getString(1);
                    this.str4[i][2] = c.getString(2);
                    this.str4[i][3] = c.getString(3);
                    this.str4[i][4] = c.getString(4);
                    this.str4[i][5] = c.getString(5);
                    this.str4[i][6] = c.getString(6);
                    this.str4[i][7] = c.getString(7);
                    this.str4[i][8] = c.getString(8);
                    this.str4[i][9] = c.getString(9);
                    this.str4[i][10] = c.getString(10);
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return this.str4;
    }

    public String[][] getInsuranceList() {
        try {
            openConnection();
            Cursor c = this.myDb.query(USER_TABLE2, new String[]{"Driver", "DriverphoneNumber", "LicensePlate", "InsuranceCompany", "PolicyNumber", "VehicleMake", "VehicleModel", "WitnessName", "PhoneNumber", "InjuredName", "PhoneNumber1", "PoliceName", "PhoneNumber2", "ReportNumber"}, null, null, null, null, null);
            int numRows = c.getCount();
            this.str5 = (String[][]) Array.newInstance(String.class, numRows, 14);
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    this.str5[i][0] = c.getString(0);
                    this.str5[i][1] = c.getString(1);
                    this.str5[i][2] = c.getString(2);
                    this.str5[i][3] = c.getString(3);
                    this.str5[i][4] = c.getString(4);
                    this.str5[i][5] = c.getString(5);
                    this.str5[i][6] = c.getString(6);
                    this.str5[i][7] = c.getString(7);
                    this.str5[i][8] = c.getString(8);
                    this.str5[i][9] = c.getString(9);
                    this.str5[i][10] = c.getString(10);
                    this.str5[i][11] = c.getString(11);
                    this.str5[i][12] = c.getString(12);
                    this.str5[i][13] = c.getString(13);
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
        } finally {
            closeConnection();
        }
        return this.str5;
    }

    public void createimage(String time2, String date2, String image12) {
        openConnection();
        this.myDb.execSQL("INSERT INTO imagenew123 (time,date ,myImage)VALUES('" + time2 + "','" + date2 + "','" + image12 + "');");
        closeConnection();
    }

    public void createmusic(String date2, String time2, String music122) {
        openConnection();
        this.myDb.execSQL("INSERT INTO recordnew (time,date , music)  VALUES ('" + time2 + "','" + date2 + "','" + music122 + "');");
        closeConnection();
    }

    public void createdraw(String date2, String time2, String draw12) {
        openConnection();
        this.myDb.execSQL("INSERT INTO Drawing (time,date,draw)  VALUES ('" + time2 + "','" + date2 + "','" + draw12 + "');");
    }

    public String[] getdraw(String date2, String time2) {
        String WHERE = " date= '" + date2 + "' AND time='" + time2 + "'";
        try {
            openConnection();
            Cursor c = this.myDb.query(draw, new String[]{"time", "date", "draw"}, WHERE, null, null, null, null);
            int numRows = c.getCount();
            this.str9 = new String[numRows];
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    this.str9[i] = c.getString(2);
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this.str9;
    }

    public ArrayList<String> getmusic(String date2, String time2) {
        ArrayList<String> audioRecord = new ArrayList<>();
        String WHERE = " date = '" + date2 + "' AND time='" + time2 + "'";
        System.out.println("Searchm: " + date2 + " : " + time2);
        audioRecord.clear();
        try {
            openConnection();
            Cursor c = this.myDb.query(recorder, new String[]{"time", "date", "music"}, WHERE, null, null, null, null);
            int numRows = c.getCount();
            System.out.println("musicRecords: " + numRows);
            str7 = new String[numRows];
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    audioRecord.add(c.getString(2));
                    System.out.println("mp3: " + str7[i]);
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return audioRecord;
    }

    public ArrayList<String> getNote(String date2, String time2) {
        ArrayList<String> textNote = new ArrayList<>();
        String WHERE = " date = '" + date2 + "' AND time='" + time2 + "'";
        textNote.clear();
        try {
            openConnection();
            Cursor c = this.myDb.query(TABLE_NOTES, new String[]{"id", "note", "lastedit", "date", "time"}, WHERE, null, null, null, null);
            int numRows = c.getCount();
            str8 = new String[numRows];
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    textNote.add(c.getString(1));
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return textNote;
    }

    public ArrayList<String> getImage(String time2, String date2) {
        ArrayList<String> images = new ArrayList<>();
        String WHERE = " date = '" + date2 + "' AND time='" + time2 + "'";
        images.clear();
        try {
            openConnection();
            Cursor c = this.myDb.query(image, new String[]{"time", "date", "myImage"}, WHERE, null, null, null, null);
            int numRows = c.getCount();
            str6 = new String[numRows];
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    images.add(c.getString(2));
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return images;
    }

    public void deleteImage(String imagetxt) {
        openConnection();
        this.myDb.execSQL("DELETE FROM imagenew123 WHERE myImage = '" + imagetxt + "' ;");
        closeConnection();
    }

    public void deleteAudio(String audio) {
        openConnection();
        this.myDb.execSQL("DELETE FROM recordnew WHERE music = '" + audio + "' ;");
        closeConnection();
    }

    public void deleteNotepad(String text) {
        openConnection();
        this.myDb.execSQL("DELETE FROM notesnew WHERE note = '" + text + "' ;");
        closeConnection();
    }
}
