package com.tritone;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.THOMSONROGERS.R;
import com.mapp.GPS;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SecondTab extends Activity {
    private static final String CLASS_TAG = "Validate";
    static final int DATE_DIALOG_ID = 1;
    static final int TIME_DIALOG_ID = 2;
    String DATE;
    String[] LightReport = {"Select", "Day Light", "Dusk", "Dawn", "Darkness(Lighted Roadway)", "Darkness(Roadway nonLighted)", "Darkness(Unknown Lighted)", "Unknown"};
    String TIME;
    String address;
    String address1;
    EditText addrredit;
    String city;
    EditText cityedit;
    String country;
    EditText countryedit;
    Button curpos;
    String date;
    private DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            SecondTab.this.month = monthOfYear;
            SecondTab.this.day = dayOfMonth;
            SecondTab.this.updateDate();
        }
    };
    EditText dateedit;
    String datetime;
    /* access modifiers changed from: private */
    public int day;
    ImageView done;
    DBDriod droid;
    /* access modifiers changed from: private */
    public int hours;
    ImageView injuryNo;
    String lightString;
    Spinner lightedit;
    /* access modifiers changed from: private */
    public int min;
    /* access modifiers changed from: private */
    public int month;
    String operation;
    Button pinpointeditdit;
    SharedPreferences.Editor prefsEditor;
    String[] roadReport = {"Select", "Dry", "Wet", "water(Standing,Worthy)", "Ice", "Snow", "Slush", "Sand,Mud,Dirt,Gravel", "Feul,Oil", "Unknown"};
    String roadString;
    Spinner roadedit;
    String state1;
    EditText stateedit;
    String tempDate;
    String tempTime;
    String time;
    private TimePickerDialog.OnTimeSetListener timeListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            SecondTab.this.hours = hourOfDay;
            SecondTab.this.min = minute;
            SecondTab.this.updateTime();
        }
    };
    EditText timeedit;
    TextView timetext;
    ImageView unknown;
    String[] weatherReport = {"Select", "Cloudy", "clean", "Rainy", "Snowy", "fog,smog,smoke", "sleet,Hail,Freezy,Rain", "Severe cross winds", "Blowing sand,Dirty sand", "Unknown"};
    String weatherString;
    Spinner weatheredit;
    String witness = "";
    ImageView witnessYes;
    private int year;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.location);
        RelativeLayout ab = (RelativeLayout) findViewById(R.id.ab);
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", 1);
        this.prefsEditor = myPrefs.edit();
        this.prefsEditor.putBoolean("database1", false);
        this.prefsEditor.commit();
        this.operation = myPrefs.getString("operation", "");
        this.tempTime = myPrefs.getString("time", "time");
        this.tempDate = myPrefs.getString("date", "date");
        this.lightedit = (Spinner) findViewById(R.id.lightedit);
        this.weatheredit = (Spinner) findViewById(R.id.weatheredit);
        this.roadedit = (Spinner) findViewById(R.id.roadedit);
        this.lightedit.setAdapter((SpinnerAdapter) new ArrayAdapter<>(this, 17367048, this.LightReport));
        this.roadedit.setAdapter((SpinnerAdapter) new ArrayAdapter<>(this, 17367048, this.roadReport));
        this.weatheredit.setAdapter((SpinnerAdapter) new ArrayAdapter<>(this, 17367048, this.weatherReport));
        this.droid = new DBDriod(this);
        this.dateedit = (EditText) findViewById(R.id.dateedit);
        this.timeedit = (EditText) findViewById(R.id.timeedit);
        this.curpos = (Button) findViewById(R.id.curpos);
        this.addrredit = (EditText) findViewById(R.id.addrredit);
        this.cityedit = (EditText) findViewById(R.id.cityedit);
        this.stateedit = (EditText) findViewById(R.id.stateedit);
        this.countryedit = (EditText) findViewById(R.id.countryedit);
        this.pinpointeditdit = (Button) findViewById(R.id.pinpointeditdit);
        this.unknown = (ImageView) findViewById(R.id.unknown);
        this.witnessYes = (ImageView) findViewById(R.id.witness);
        this.injuryNo = (ImageView) findViewById(R.id.injury);
        Bundle b1 = getIntent().getExtras();
        if (b1 != null) {
            this.date = b1.getString("date1");
            this.time = b1.getString("time1");
            this.address1 = b1.getString("address1");
            this.city = b1.getString("city1");
            this.country = b1.getString("country1");
            this.state1 = b1.getString("state1");
            this.dateedit.setText(this.date);
            this.timeedit.setText(this.time);
            this.addrredit.setText(this.address1);
            this.cityedit.setText(this.city);
            this.countryedit.setText(this.country);
            this.stateedit.setText(this.state1);
            this.lightString = b1.getString("light1");
            for (int i = 0; i < this.roadReport.length; i++) {
                if (this.roadReport[i].equals(this.lightString)) {
                    this.lightedit.setSelection(i);
                }
            }
            this.weatherString = b1.getString("weather1");
            for (int i2 = 0; i2 < this.weatherReport.length; i2++) {
                if (this.weatherReport[i2].equals(this.weatherString)) {
                    this.weatheredit.setSelection(i2);
                }
            }
            this.roadString = b1.getString("road1");
            for (int i3 = 0; i3 < this.LightReport.length; i3++) {
                if (this.LightReport[i3].equals(this.roadString)) {
                    this.roadedit.setSelection(i3);
                }
            }
            if (b1.getString("witness1").equals("witnessYes")) {
                this.witnessYes.setImageResource(R.drawable.yesblue);
                this.injuryNo.setImageResource(R.drawable.no);
                this.unknown.setImageResource(R.drawable.unknown);
            } else if (b1.getString("witness1").equals("unknown")) {
                this.witnessYes.setImageResource(R.drawable.yes);
                this.injuryNo.setImageResource(R.drawable.no);
                this.unknown.setImageResource(R.drawable.unknownblue);
            } else {
                this.injuryNo.setImageResource(R.drawable.noblue);
                this.witnessYes.setImageResource(R.drawable.yes);
                this.unknown.setImageResource(R.drawable.unknown);
            }
        } else {
            this.cityedit.setText(this.city);
            this.countryedit.setText(this.country);
            this.addrredit.setText(this.address);
        }
        this.lightedit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                SecondTab.this.lightString = SecondTab.this.lightedit.getItemAtPosition(arg2).toString();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.weatheredit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                SecondTab.this.weatherString = SecondTab.this.weatheredit.getItemAtPosition(arg2).toString();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.done = (ImageView) findViewById(R.id.done);
        this.roadedit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                SecondTab.this.roadString = SecondTab.this.roadedit.getItemAtPosition(arg2).toString();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.curpos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondTab.this.startActivityForResult(new Intent(SecondTab.this, GPS.class), 2);
            }
        });
        this.injuryNo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondTab.this.injuryNo.setImageResource(R.drawable.noblue);
                SecondTab.this.witnessYes.setImageResource(R.drawable.yes);
                SecondTab.this.unknown.setImageResource(R.drawable.unknown);
                SecondTab.this.witness = "injuryNo";
            }
        });
        this.witnessYes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondTab.this.witnessYes.setImageResource(R.drawable.yesblue);
                SecondTab.this.injuryNo.setImageResource(R.drawable.no);
                SecondTab.this.unknown.setImageResource(R.drawable.unknown);
                SecondTab.this.witness = "witnessYes";
            }
        });
        this.unknown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondTab.this.witnessYes.setImageResource(R.drawable.yes);
                SecondTab.this.injuryNo.setImageResource(R.drawable.no);
                SecondTab.this.unknown.setImageResource(R.drawable.unknownblue);
                SecondTab.this.witness = "unknown";
            }
        });
        ab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) SecondTab.this.getSystemService("input_method");
                imm.hideSoftInputFromWindow(SecondTab.this.timeedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(SecondTab.this.dateedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(SecondTab.this.cityedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(SecondTab.this.countryedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(SecondTab.this.stateedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(SecondTab.this.addrredit.getWindowToken(), 0);
            }
        });
        this.datetime = new SimpleDateFormat("MMM d,yyyy hh:mm aaa").format(Calendar.getInstance().getTime());
        System.out.println(this.datetime);
        this.done.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SecondTab.this.droid.getLocationlistRecordCount(SecondTab.this.tempDate, SecondTab.this.tempTime) == 0) {
                    SecondTab.this.droid.createLocation(SecondTab.this.tempDate, SecondTab.this.tempTime, SecondTab.this.datetime, SecondTab.this.timeedit.getText().toString(), SecondTab.this.dateedit.getText().toString(), SecondTab.this.addrredit.getText().toString(), SecondTab.this.cityedit.getText().toString(), SecondTab.this.countryedit.getText().toString(), SecondTab.this.stateedit.getText().toString(), SecondTab.this.roadString, SecondTab.this.lightString, SecondTab.this.weatherString, SecondTab.this.witness);
                } else {
                    SecondTab.this.droid.updateLocation(SecondTab.this.tempDate, SecondTab.this.tempTime, SecondTab.this.tempDate, SecondTab.this.tempTime, SecondTab.this.datetime, SecondTab.this.timeedit.getText().toString(), SecondTab.this.dateedit.getText().toString(), SecondTab.this.addrredit.getText().toString(), SecondTab.this.cityedit.getText().toString(), SecondTab.this.countryedit.getText().toString(), SecondTab.this.stateedit.getText().toString(), SecondTab.this.roadString, SecondTab.this.lightString, SecondTab.this.weatherString, SecondTab.this.witness);
                }
                SecondTab.this.done.setImageResource(R.drawable.saveblue);
                SecondTab.this.prefsEditor.putBoolean("database1", true);
                SecondTab.this.prefsEditor.commit();
                Toast.makeText(SecondTab.this.getBaseContext(), "saved successfully", 0).show();
            }
        });
        this.dateedit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondTab.this.showDialog(1);
            }
        });
        this.TIME = myPrefs.getString("time", "time");
        this.DATE = myPrefs.getString("date", "date");
        Calendar cal = Calendar.getInstance();
        this.year = cal.get(1);
        this.month = cal.get(2);
        this.day = cal.get(5);
        updateDate();
        this.timeedit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondTab.this.showDialog(2);
            }
        });
        this.hours = cal.get(10);
        this.min = cal.get(12);
        updateTime();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.droid.getLocationlistRecordCount(this.tempDate, this.tempTime) == 0) {
            this.droid.createLocation(this.tempDate, this.tempTime, this.datetime, this.timeedit.getText().toString(), this.dateedit.getText().toString(), this.addrredit.getText().toString(), this.cityedit.getText().toString(), this.countryedit.getText().toString(), this.stateedit.getText().toString(), this.roadString, this.lightString, this.weatherString, this.witness);
        } else {
            this.droid.updateLocation(this.tempDate, this.tempTime, this.tempDate, this.tempTime, this.datetime, this.timeedit.getText().toString(), this.dateedit.getText().toString(), this.addrredit.getText().toString(), this.cityedit.getText().toString(), this.countryedit.getText().toString(), this.stateedit.getText().toString(), this.roadString, this.lightString, this.weatherString, this.witness);
        }
        this.prefsEditor.putBoolean("database1", true);
        this.prefsEditor.commit();
    }

    public void AlertDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Required...");
        alertDialog.setMessage("Please... Fill The Required Fields ");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.setIcon((int) R.drawable.warning);
        alertDialog.show();
    }

    public static boolean hasText(EditText editText) {
        Log.d(CLASS_TAG, "hasText()");
        String text = editText.getText().toString().trim();
        if (text.length() != 0) {
            return true;
        }
        editText.setText(text);
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean validated() {
        boolean validated = true;
        if (!FirstTab.hasText(this.timeedit)) {
            validated = false;
        }
        if (!FirstTab.hasText(this.dateedit)) {
            validated = false;
        }
        if (!FirstTab.hasText(this.addrredit)) {
            validated = false;
        }
        if (!FirstTab.hasText(this.cityedit)) {
            validated = false;
        }
        if (!FirstTab.hasText(this.countryedit)) {
            validated = false;
        }
        if (!FirstTab.hasText(this.stateedit)) {
            validated = false;
        }
        if (!FirstTab.hasText1(this.roadedit)) {
            validated = false;
        }
        if (!FirstTab.hasText1(this.weatheredit)) {
            validated = false;
        }
        if (!FirstTab.hasText1(this.lightedit)) {
            return false;
        }
        return validated;
    }

    public void updateDate() {
        this.dateedit.setText(this.DATE);
    }

    public void updateTime() {
        this.timeedit.setText(this.TIME);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new DatePickerDialog(this, this.dateListener, this.year, this.month, this.day);
            case 2:
                return new TimePickerDialog(this, this.timeListener, this.hours, this.min, false);
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == -1) {
                    String stringExtra = data.getStringExtra("policeId");
                    String time1 = data.getStringExtra("time");
                    String date1 = data.getStringExtra("date");
                    String address12 = data.getStringExtra("address");
                    String city1 = data.getStringExtra("city");
                    String country1 = data.getStringExtra("country");
                    String state12 = data.getStringExtra("state");
                    String ReadSurfaceCondition1 = data.getStringExtra("ReadSurfaceCondition");
                    String lightConditon1 = data.getStringExtra("lightConditon");
                    String weatherCondition1 = data.getStringExtra("weatherCondition");
                    this.timeedit.setText(time1);
                    this.dateedit.setText(date1);
                    this.addrredit.setText(address12);
                    this.cityedit.setText(city1);
                    this.countryedit.setText(country1);
                    this.stateedit.setText(state12);
                    this.roadedit.setTag(ReadSurfaceCondition1);
                    this.lightedit.setTag(lightConditon1);
                    this.weatheredit.setTag(weatherCondition1);
                    break;
                }
                break;
            case 2:
                if (resultCode == -1) {
                    String address2 = data.getStringExtra("address");
                    String address13 = data.getStringExtra("locality");
                    String address22 = data.getStringExtra("wholeaddress");
                    String adminArea = data.getStringExtra("adminArea");
                    System.out.println("adrs: " + address2 + " End.");
                    System.out.println("adrs2: " + address13 + " End.");
                    if (address2 != null && address13 != null && address22 != null) {
                        this.addrredit.setText(address22);
                        this.cityedit.setText(address2);
                        this.countryedit.setText(address13);
                        this.stateedit.setText(adminArea);
                        break;
                    } else {
                        this.addrredit.setText("");
                        this.cityedit.setText("");
                        this.countryedit.setText("");
                        break;
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, "Delete");
        menu.add(0, 2, 0, "Update");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                Toast.makeText(getBaseContext(), "deleted", 0).show();
                this.timeedit.setText("");
                this.dateedit.setText("");
                this.addrredit.setText("");
                this.cityedit.setText("");
                this.countryedit.setText("");
                this.stateedit.setText("");
                this.roadedit.setTag("");
                this.lightedit.setTag("");
                this.weatheredit.setTag("");
                break;
            case 2:
                Toast.makeText(getBaseContext(), "UPdated", 0).show();
                this.timeedit.setText("");
                this.dateedit.setText("");
                this.addrredit.setText("");
                this.cityedit.setText("");
                this.countryedit.setText("");
                this.stateedit.setText("");
                this.roadedit.setTag("");
                this.lightedit.setTag("");
                this.weatheredit.setTag("");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.droid.closeConnection();
    }
}
