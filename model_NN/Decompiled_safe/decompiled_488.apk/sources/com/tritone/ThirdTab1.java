package com.tritone;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.THOMSONROGERS.R;
import java.util.ArrayList;

public class ThirdTab1 extends Activity {
    private static final String CLASS_TAG = "Validate";
    public static final String DATABASE_NAME = "CarInsurance.dbneww";
    public static final String USER_TABLE_NAME = "EditFormLatest";
    EditText DLNOedit;
    EditText DriverPhNoedit;
    EditText Insurancecompanyedit;
    EditText InturerNoedit;
    EditText Inturernmeedit;
    EditText LicencePlateedit;
    EditText PoliceNoedit;
    EditText Policenmeedit;
    EditText PolicyNoedit;
    EditText ReportNoedit;
    EditText VehicleModel;
    EditText WitnessPhNoedit;
    EditText Witnessnmeedit;
    Button add;
    private String[] array_spinner;
    String date1;
    SQLiteDatabase db;
    Button delete;
    Button done;
    String driver;
    DBDriod droid;
    ArrayList<String> list = new ArrayList<>();
    String passengerNo;
    EditText spinner1;
    String[] str = new String[5];
    String time1;
    EditText vehicleMakeedit;
    String warning = "";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.driver1);
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", 1);
        final SharedPreferences.Editor prefsEditor = myPrefs.edit();
        this.time1 = myPrefs.getString("time", "time");
        this.date1 = myPrefs.getString("date", "date");
        System.out.println("Activity Called...");
        prefsEditor.putBoolean("database2", false);
        prefsEditor.commit();
        this.LicencePlateedit = (EditText) findViewById(R.id.LicencePlateedit);
        this.Insurancecompanyedit = (EditText) findViewById(R.id.Insurancecompanyedit);
        this.PolicyNoedit = (EditText) findViewById(R.id.PolicyNoedit);
        this.DriverPhNoedit = (EditText) findViewById(R.id.DriverPhNoedit);
        this.vehicleMakeedit = (EditText) findViewById(R.id.vehicleMakeedit);
        this.spinner1 = (EditText) findViewById(R.id.Drivernmeedit);
        this.VehicleModel = (EditText) findViewById(R.id.Passangeredit12);
        this.Witnessnmeedit = (EditText) findViewById(R.id.Witnessnmeedit);
        this.WitnessPhNoedit = (EditText) findViewById(R.id.WitnessPhNoedit);
        this.Inturernmeedit = (EditText) findViewById(R.id.Inturernmeedit);
        this.InturerNoedit = (EditText) findViewById(R.id.InturerNoedit);
        this.Policenmeedit = (EditText) findViewById(R.id.Policenmeedit);
        this.PoliceNoedit = (EditText) findViewById(R.id.PoliceNoedit);
        this.ReportNoedit = (EditText) findViewById(R.id.ReportNoedit);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            this.driver = b.getString("Driver1");
            System.out.println(String.valueOf(this.driver) + "diverrrrrrr");
            System.out.println("droiujjouiujlk" + this.driver);
            this.spinner1.setText(b.getString("Driver1"));
            System.out.println("Driver33333333:" + b.getString("Driver1"));
            this.DriverPhNoedit.setText(b.getString("DriverphoneNumber1"));
            this.LicencePlateedit.setText(b.getString("LicensePlate1"));
            this.Insurancecompanyedit.setText(b.getString("InsuranceCompany1"));
            this.PolicyNoedit.setText(b.getString("PolicyNumber1"));
            this.passengerNo = b.getString("PassengerNumber1");
            this.vehicleMakeedit.setText(b.getString("VehicleMake1"));
            this.VehicleModel.setText(b.getString("VehicleModel1"));
            this.Witnessnmeedit.setText(b.getString("Witnessnmeedit1"));
            this.WitnessPhNoedit.setText(b.getString("WitnessPhNoedit1"));
            this.Inturernmeedit.setText(b.getString("Inturernmeedit1"));
            this.InturerNoedit.setText(b.getString("InturerNoedit1"));
            this.Policenmeedit.setText(b.getString("Policenmeedit1"));
            this.PoliceNoedit.setText(b.getString("PoliceNoedit1"));
            this.ReportNoedit.setText(b.getString("ReportNoedit1"));
        }
        this.droid = new DBDriod(this);
        this.spinner1 = (EditText) findViewById(R.id.Drivernmeedit);
        ((AbsoluteLayout) findViewById(R.id.drvab)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) ThirdTab1.this.getSystemService("input_method");
                imm.hideSoftInputFromWindow(ThirdTab1.this.DriverPhNoedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab1.this.LicencePlateedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab1.this.Insurancecompanyedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab1.this.PolicyNoedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab1.this.DriverPhNoedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab1.this.vehicleMakeedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab1.this.spinner1.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab1.this.VehicleModel.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab1.this.Witnessnmeedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab1.this.WitnessPhNoedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab1.this.Inturernmeedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab1.this.InturerNoedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab1.this.Policenmeedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab1.this.PoliceNoedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab1.this.ReportNoedit.getWindowToken(), 0);
            }
        });
        this.done = (Button) findViewById(R.id.done);
        this.done.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ThirdTab1.this.droid.createInsurance(ThirdTab1.this.date1, ThirdTab1.this.time1, ThirdTab1.this.spinner1.getText().toString(), ThirdTab1.this.DriverPhNoedit.getText().toString(), ThirdTab1.this.LicencePlateedit.getText().toString(), ThirdTab1.this.Insurancecompanyedit.getText().toString(), ThirdTab1.this.PolicyNoedit.getText().toString(), ThirdTab1.this.vehicleMakeedit.getText().toString(), ThirdTab1.this.VehicleModel.getText().toString(), ThirdTab1.this.Witnessnmeedit.getText().toString(), ThirdTab1.this.WitnessPhNoedit.getText().toString(), ThirdTab1.this.Inturernmeedit.getText().toString(), ThirdTab1.this.InturerNoedit.getText().toString(), ThirdTab1.this.Policenmeedit.getText().toString(), ThirdTab1.this.PoliceNoedit.getText().toString(), ThirdTab1.this.ReportNoedit.getText().toString());
                System.out.println(String.valueOf(ThirdTab1.this.spinner1.getText().toString()) + " " + ThirdTab1.this.LicencePlateedit.getText().toString() + " " + ThirdTab1.this.Insurancecompanyedit.getText().toString() + " " + ThirdTab1.this.PolicyNoedit.getText().toString() + " " + ThirdTab1.this.driver + " " + ThirdTab1.this.vehicleMakeedit.getText().toString() + " " + ThirdTab1.this.VehicleModel.getText().toString() + " " + ThirdTab1.this.Witnessnmeedit.getText().toString() + ThirdTab1.this.WitnessPhNoedit.getText().toString() + ThirdTab1.this.Inturernmeedit.getText().toString() + " " + ThirdTab1.this.InturerNoedit.getText().toString() + " " + ThirdTab1.this.Policenmeedit.getText().toString() + " " + ThirdTab1.this.PoliceNoedit.getText().toString() + " " + ThirdTab1.this.ReportNoedit.getText().toString());
                Toast.makeText(ThirdTab1.this.getBaseContext(), "saved successfully", 1).show();
                ThirdTab1.this.done.setBackgroundResource(R.drawable.saveblue);
                prefsEditor.putBoolean("database2", true);
                prefsEditor.commit();
            }
        });
        this.add = (Button) findViewById(R.id.add);
        this.add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ThirdTab1.this.setResult(-1, new Intent());
                ThirdTab1.this.finish();
            }
        });
    }
}
