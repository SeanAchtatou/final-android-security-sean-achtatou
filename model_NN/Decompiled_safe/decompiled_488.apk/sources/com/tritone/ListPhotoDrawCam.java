package com.tritone;

import android.app.Activity;
import android.content.Intent;
import android.gesture.Gesture;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.THOMSONROGERS.R;
import com.bitsetters.android.notes.NoteEdit;
import com.bitsetters.android.notes.NoteList;
import com.camera.PictureDemo;
import com.hascode.android.RecorderActivity;
import java.util.ArrayList;

public class ListPhotoDrawCam extends Activity {
    Integer arrow = Integer.valueOf((int) R.drawable.arow);
    String date;
    String[] drawing_files;
    DBDriod droid;
    ImageView im1;
    ImageView im2;
    ImageView im3;
    ImageView im4;
    String[] image_files;
    ListView list;
    ArrayList<Integer> listimage = new ArrayList<>();
    private Gesture mGesture;
    String[] music_files;
    String[] notepad_files;
    int posit = 0;
    final int requestCode = 0;
    String selected_list = "";
    int size = 0;
    ArrayList<String> sublist_text;
    boolean temp = false;
    boolean temp1 = true;
    String[] testValues = {"Camera ", "notepad", "Record  "};
    String time;
    int val = 0;
    int val1 = 0;
    View view = null;
    View view2 = null;
    View[] viewtemp = new View[9];

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        this.droid = new DBDriod(this);
        setContentView((int) R.layout.users_list);
        this.listimage.add(Integer.valueOf((int) R.drawable.d3));
        this.listimage.add(Integer.valueOf((int) R.drawable.d2));
        this.listimage.add(Integer.valueOf((int) R.drawable.d1));
        this.list = (ListView) findViewById(R.id.ListView01);
        this.list.setDividerHeight(4);
        this.list.setAdapter((ListAdapter) new adapter(this));
        this.list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                ListPhotoDrawCam.this.view = arg1;
                ListPhotoDrawCam.this.selected_list = ListPhotoDrawCam.this.testValues[position];
                if (ListPhotoDrawCam.this.testValues[position].equals("notepad")) {
                    ListPhotoDrawCam.this.startActivity(new Intent(ListPhotoDrawCam.this, NoteEdit.class));
                } else if (ListPhotoDrawCam.this.testValues[position].equals("Camera ")) {
                    ListPhotoDrawCam.this.startActivity(new Intent(ListPhotoDrawCam.this, PictureDemo.class));
                } else {
                    ListPhotoDrawCam.this.startActivity(new Intent(ListPhotoDrawCam.this, RecorderActivity.class));
                }
            }
        });
    }

    class adapter extends BaseAdapter {
        Activity con;
        LayoutInflater inflater = LayoutInflater.from(this.con);

        public adapter(Activity context) {
            this.con = context;
        }

        public int getCount() {
            return ListPhotoDrawCam.this.testValues.length;
        }

        public Object getItem(int position) {
            return ListPhotoDrawCam.this.testValues;
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            final int pos = position;
            if (convertView == null) {
                this.inflater = this.con.getLayoutInflater();
                convertView = this.inflater.inflate((int) R.layout.fourthtablisttext, (ViewGroup) null);
            }
            ImageView im = (ImageView) convertView.findViewById(R.id.ImageView01);
            ImageView image = (ImageView) convertView.findViewById(R.id.ImageView02);
            im.setImageResource(ListPhotoDrawCam.this.listimage.get(position).intValue());
            if (pos == 0) {
                im.setImageResource(R.drawable.d3);
                image.setImageResource(R.drawable.cameralist);
            }
            if (pos == 1) {
                image.setImageResource(R.drawable.d2);
                image.setImageResource(R.drawable.noteslist);
            }
            if (pos == 2) {
                image.setImageResource(R.drawable.d1);
                image.setImageResource(R.drawable.recordinglist);
            }
            image.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (pos == 0) {
                        System.out.println("Inside Pos 0");
                        Intent intent = new Intent(ListPhotoDrawCam.this, CamRecListClass.class);
                        intent.putExtra("mode", "camera");
                        ListPhotoDrawCam.this.startActivity(intent);
                    }
                    if (pos == 1) {
                        ListPhotoDrawCam.this.startActivity(new Intent(ListPhotoDrawCam.this, NoteList.class));
                    }
                    if (pos == 2) {
                        Intent intent2 = new Intent(ListPhotoDrawCam.this, CamRecListClass.class);
                        intent2.putExtra("mode", "recorder");
                        ListPhotoDrawCam.this.startActivity(intent2);
                    }
                }
            });
            return convertView;
        }
    }
}
