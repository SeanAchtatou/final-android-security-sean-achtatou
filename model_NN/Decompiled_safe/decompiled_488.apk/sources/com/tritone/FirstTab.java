package com.tritone;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;
import com.THOMSONROGERS.R;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class FirstTab extends Activity {
    private static final String CLASS_TAG = "Validate";
    public static final String DATABASE_NAME = "CarInsurance.dbnew";
    public static final int INVALID_TEXT_COLOR = -65536;
    public static final String USER_TABLE_NAME = "EditFormLatest";
    public static final int VALID_TEXT_COLOR = -16777216;
    public static boolean dbstatus = false;
    EditText DLnumber;
    ImageView Done;
    ImageView Driver;
    EditText Email;
    ImageView Injury;
    ImageView No;
    EditText PHnumber;
    ImageView Witness;
    ImageView Yes;
    RelativeLayout ab;
    Button add;
    Button clear;
    public Context context;
    String date1;
    SQLiteDatabase db;
    Button delete;
    Button driverself;
    DBDriod droid;
    EditText feditname;
    String firstname;
    ArrayList<String> firstnameArray = new ArrayList<>();
    String injured;
    EditText leditname;
    EditText licensePlate;
    String operation;
    SharedPreferences.Editor prefsEditor;
    String[] str = new String[10];
    String time1;
    EditText vehicleMake;
    EditText vehiclemodel;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.main1);
        final SharedPreferences myPrefs = getSharedPreferences("myPrefs", 1);
        this.prefsEditor = myPrefs.edit();
        this.prefsEditor.putBoolean("database", false);
        this.prefsEditor.commit();
        this.operation = myPrefs.getString("operation", "");
        this.time1 = myPrefs.getString("time", "time");
        this.date1 = myPrefs.getString("date", "date");
        this.Yes = (ImageView) findViewById(R.id.Yes);
        this.No = (ImageView) findViewById(R.id.No);
        this.injured = "click";
        this.droid = new DBDriod(this);
        this.feditname = (EditText) findViewById(R.id.feditname);
        this.leditname = (EditText) findViewById(R.id.leditname);
        this.DLnumber = (EditText) findViewById(R.id.DLnumber);
        this.PHnumber = (EditText) findViewById(R.id.PHnumber);
        this.Email = (EditText) findViewById(R.id.Email);
        this.vehiclemodel = (EditText) findViewById(R.id.modeledit);
        this.vehicleMake = (EditText) findViewById(R.id.makeedit);
        this.licensePlate = (EditText) findViewById(R.id.licensePlatedit);
        final Bundle b = getIntent().getExtras();
        if (b != null) {
            String s1 = b.getString("firstname1");
            try {
                if (b.getString("injured").equals("yes")) {
                    this.Yes.setImageResource(R.drawable.yesblue);
                } else {
                    this.No.setImageResource(R.drawable.noblue);
                }
            } catch (NullPointerException e) {
            }
            String str1 = b.getString("lookatme");
            if (str1.equals("look2")) {
                this.feditname.setText(s1);
                this.leditname.setText(b.getString("lastname1"));
                this.DLnumber.setText(b.getString("dlno1"));
                this.PHnumber.setText(b.getString("phno1"));
                this.Email.setText(b.getString("email1"));
                this.vehiclemodel.setText(b.getString("vehiclemodel"));
                this.vehicleMake.setText(b.getString("vehiclemake"));
                this.licensePlate.setText(b.getString("licenseplate"));
                try {
                    if (b.getString("injured").equals("yes")) {
                        this.Yes.setImageResource(R.drawable.yesblue);
                    } else {
                        this.No.setImageResource(R.drawable.noblue);
                    }
                } catch (NullPointerException e2) {
                    this.No.setImageResource(R.drawable.noblue);
                }
            }
            if (str1.equals("look")) {
                this.driverself = (Button) findViewById(R.id.driverself);
                this.driverself.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        System.out.println("setting the text");
                        FirstTab.this.feditname.setText(b.getString("firstname1"));
                        FirstTab.this.leditname.setText(b.getString("lastname1"));
                        FirstTab.this.DLnumber.setText(b.getString("dlno1"));
                        FirstTab.this.PHnumber.setText(b.getString("phno1"));
                        System.out.println("strphonenumber::::::" + b.getString("phno1"));
                        FirstTab.this.Email.setText(b.getString("email1"));
                        FirstTab.this.vehiclemodel.setText(b.getString("vehiclemodel"));
                        FirstTab.this.vehicleMake.setText(b.getString("vehiclemake"));
                        FirstTab.this.licensePlate.setText(b.getString("licenseplate"));
                        if (b.getString("injured") == null) {
                            FirstTab.this.No.setImageResource(R.drawable.noblue);
                        } else if (b.getString("injured").equals("yes")) {
                            FirstTab.this.Yes.setImageResource(R.drawable.yesblue);
                        } else {
                            FirstTab.this.No.setImageResource(R.drawable.noblue);
                        }
                        String firsname = myPrefs.getString("firstname", "firstname");
                        String lastname = myPrefs.getString("lastname", "lastname");
                        String drivernumber = myPrefs.getString("driverLicense", "drivernumber");
                        String phonenumber = myPrefs.getString("contactNumber", "contactNumber");
                        String emailid = myPrefs.getString("email", "email");
                        String licenseplate = myPrefs.getString("licensePlate", "licensePlate");
                        String make = myPrefs.getString("make", "make");
                        String model = myPrefs.getString("model", "model");
                        FirstTab.this.feditname.setText(firsname);
                        FirstTab.this.leditname.setText(lastname);
                        FirstTab.this.DLnumber.setText(drivernumber);
                        FirstTab.this.PHnumber.setText(phonenumber);
                        FirstTab.this.Email.setText(emailid);
                        FirstTab.this.vehiclemodel.setText(model);
                        FirstTab.this.vehicleMake.setText(make);
                        FirstTab.this.licensePlate.setText(licenseplate);
                        FirstTab.this.prefsEditor.commit();
                    }
                });
            } else {
                String string = b.getString("firstname1");
                this.feditname.setText(b.getString("firstname1"));
                this.leditname.setText(b.getString("lastname1"));
                this.DLnumber.setText(b.getString("dlno1"));
                this.PHnumber.setText(b.getString("phno1"));
                this.Email.setText(b.getString("email1"));
                this.vehiclemodel.setText(b.getString("vehiclemodel"));
                this.vehicleMake.setText(b.getString("vehiclemake"));
                this.licensePlate.setText(b.getString("licenseplate"));
                try {
                    if (b.getString("injured").equals("yes")) {
                        this.Yes.setImageResource(R.drawable.yesblue);
                    } else {
                        this.No.setImageResource(R.drawable.noblue);
                    }
                } catch (NullPointerException e3) {
                    this.No.setImageResource(R.drawable.noblue);
                }
            }
        }
        this.clear = (Button) findViewById(R.id.clear);
        this.clear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FirstTab.this.feditname.setText(" ");
                FirstTab.this.leditname.setText(" ");
                FirstTab.this.DLnumber.setText(" ");
                FirstTab.this.PHnumber.setText(" ");
                FirstTab.this.Email.setText(" ");
                FirstTab.this.vehiclemodel.setText(" ");
                FirstTab.this.vehicleMake.setText(" ");
                FirstTab.this.licensePlate.setText(" ");
            }
        });
        this.ab = (RelativeLayout) findViewById(R.id.ab);
        this.ab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.out.println("layout is clicked");
                InputMethodManager imm = (InputMethodManager) FirstTab.this.getSystemService("input_method");
                imm.hideSoftInputFromWindow(FirstTab.this.feditname.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(FirstTab.this.leditname.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(FirstTab.this.DLnumber.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(FirstTab.this.PHnumber.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(FirstTab.this.Email.getWindowToken(), 0);
                imm.hideSoftInputFromInputMethod(FirstTab.this.licensePlate.getWindowToken(), 0);
                imm.hideSoftInputFromInputMethod(FirstTab.this.vehicleMake.getWindowToken(), 0);
                imm.hideSoftInputFromInputMethod(FirstTab.this.licensePlate.getWindowToken(), 0);
            }
        });
        ImageView imageView = (ImageView) findViewById(R.id.image1);
        this.Yes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FirstTab.this.Yes.setImageResource(R.drawable.yesblue);
                FirstTab.this.No.setImageResource(R.drawable.no);
                FirstTab.this.injured = "yes";
            }
        });
        this.No.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FirstTab.this.No.setImageResource(R.drawable.noblue);
                FirstTab.this.Yes.setImageResource(R.drawable.yes);
                FirstTab.this.injured = "no";
            }
        });
        this.Done = (ImageView) findViewById(R.id.Done);
        this.Done.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    if (FirstTab.this.droid.getEditlistRecordCount(FirstTab.this.date1, FirstTab.this.time1) == 0) {
                        FirstTab.this.droid.createEditForm(FirstTab.this.date1, FirstTab.this.time1, FirstTab.this.feditname.getText().toString(), FirstTab.this.leditname.getText().toString(), FirstTab.this.DLnumber.getText().toString(), FirstTab.this.PHnumber.getText().toString(), FirstTab.this.Email.getText().toString(), FirstTab.this.injured, FirstTab.this.vehicleMake.getText().toString(), FirstTab.this.vehiclemodel.getText().toString(), FirstTab.this.licensePlate.getText().toString());
                    } else {
                        FirstTab.this.droid.updateEditFormContent(FirstTab.this.date1, FirstTab.this.time1, FirstTab.this.date1, FirstTab.this.time1, FirstTab.this.feditname.getText().toString(), FirstTab.this.leditname.getText().toString(), FirstTab.this.DLnumber.getText().toString(), FirstTab.this.PHnumber.getText().toString(), FirstTab.this.Email.getText().toString(), FirstTab.this.injured, FirstTab.this.vehicleMake.getText().toString(), FirstTab.this.vehiclemodel.getText().toString(), FirstTab.this.licensePlate.getText().toString());
                    }
                    FirstTab.this.droid.getEditForm();
                    FirstTab.this.Done.setImageResource(R.drawable.saveblue);
                    FirstTab.dbstatus = true;
                    Toast.makeText(FirstTab.this.getBaseContext(), "saved successfully", 0).show();
                    FirstTab.this.prefsEditor.putBoolean("database", true);
                    FirstTab.this.prefsEditor.commit();
                } catch (Exception e) {
                    Toast.makeText(FirstTab.this.getBaseContext(), "Driver License Already Exists", 0).show();
                    FirstTab.this.prefsEditor.putBoolean("database", false);
                    FirstTab.this.prefsEditor.commit();
                    FirstTab.dbstatus = false;
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.droid.getEditlistRecordCount(this.date1, this.time1) == 0) {
            this.droid.createEditForm(this.date1, this.time1, this.feditname.getText().toString(), this.leditname.getText().toString(), this.DLnumber.getText().toString(), this.PHnumber.getText().toString(), this.Email.getText().toString(), this.injured, this.vehicleMake.getText().toString(), this.vehiclemodel.getText().toString(), this.licensePlate.getText().toString());
        } else {
            this.droid.updateEditFormContent(this.date1, this.time1, this.date1, this.time1, this.feditname.getText().toString(), this.leditname.getText().toString(), this.DLnumber.getText().toString(), this.PHnumber.getText().toString(), this.Email.getText().toString(), this.injured, this.vehicleMake.getText().toString(), this.vehiclemodel.getText().toString(), this.licensePlate.getText().toString());
        }
        dbstatus = true;
        this.prefsEditor.putBoolean("database", true);
        this.prefsEditor.commit();
    }

    public void AlertDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Required...");
        alertDialog.setMessage("Please... Fill The Required Fields ");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.setIcon((int) R.drawable.warning);
        alertDialog.show();
    }

    public static boolean isRegEmail(EditText editText, boolean required) {
        Log.d(CLASS_TAG, "isReg_email");
        return isValid(editText, editText.getResources().getString(R.string.regex_email), required);
    }

    public static boolean isRegPhone(EditText editText, boolean required) {
        Log.d(CLASS_TAG, "isReg_phone");
        return isValid(editText, editText.getResources().getString(R.string.regex_phone), required);
    }

    public static boolean isValid(EditText editText, String regex, boolean required) {
        Log.d(CLASS_TAG, "isValid()");
        boolean validated = true;
        String text = editText.getText().toString().trim();
        boolean hasText = hasText(editText);
        editText.setTextColor(-16777216);
        if (required && !hasText) {
            validated = false;
        }
        if (!validated || !hasText || Pattern.matches(regex, text)) {
            return validated;
        }
        editText.setTextColor(-65536);
        return false;
    }

    public static boolean hasText(EditText editText) {
        Log.d(CLASS_TAG, "hasText()");
        String text = editText.getText().toString().trim();
        if (text.length() != 0) {
            return true;
        }
        editText.setText(text);
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean validated() {
        boolean validated = true;
        if (!hasText(this.feditname)) {
            validated = false;
        }
        if (!hasText(this.leditname)) {
            validated = false;
        }
        if (!hasText(this.DLnumber)) {
            validated = false;
        }
        if (!hasText(this.PHnumber)) {
            validated = false;
        }
        if (!hasText(this.Email)) {
            validated = false;
        }
        if (!isRegEmail(this.Email, true)) {
            validated = false;
        }
        if (!hasText(this.vehiclemodel)) {
            validated = false;
        }
        if (!hasText(this.vehicleMake)) {
            validated = false;
        }
        if (!hasText(this.licensePlate)) {
            return false;
        }
        return validated;
    }

    public static boolean hasText1(Spinner select) {
        Log.d(CLASS_TAG, "hasText()");
        String text = select.getContext().toString().trim();
        if (text.length() != 0) {
            return true;
        }
        select.setTag(text);
        return false;
    }

    public void openConnection() {
        this.db = getApplicationContext().openOrCreateDatabase("CarInsurance.dbnew", 0, null);
    }

    public void closeConnection() {
        this.db.close();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 2, 0, "Update");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                this.droid.updateEditForm(this.feditname.getText().toString(), this.leditname.getText().toString(), this.DLnumber.getText().toString(), this.PHnumber.getText().toString(), this.Email.getText().toString());
                Toast.makeText(getBaseContext(), "Updated", 0).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == -1) {
                    String firstname2 = data.getStringExtra("firstname");
                    String lastname = data.getStringExtra("lastname");
                    String DLNO = data.getStringExtra("DLNO");
                    String Pno = data.getStringExtra("Pno");
                    String Email1 = data.getStringExtra("Email");
                    this.feditname.setText(firstname2);
                    this.leditname.setText(lastname);
                    this.DLnumber.setText(DLNO);
                    this.PHnumber.setText(Pno);
                    this.Email.setText(Email1);
                    break;
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
