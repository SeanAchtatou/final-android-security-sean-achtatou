package com.tritone;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.THOMSONROGERS.R;
import java.util.ArrayList;

public class LocationList extends Activity {
    public static final String DATABASE_NAME = "CarInsurance.dbnew";
    public static final String USER_TABLE = "newLocation";
    public static final String USER_TABLE_NAME = "EditFormLatest";
    ArrayList<String> country = new ArrayList<>();
    SQLiteDatabase db;
    DBDriod droid;
    ListView list;
    String[] str = {"policeId", "Time", "Date", "Address", "city", "country", "state", "ReadSurfaceCondition", "lightCondition", "weatherCondition"};

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        this.droid = new DBDriod(this);
        this.country = this.droid.getLocationCount();
        setContentView((int) R.layout.users_list);
        System.out.println("LocationList view      ...................................................");
        this.list = (ListView) findViewById(R.id.ListView01);
        this.list.setAdapter((ListAdapter) new adapter(this));
        this.list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                LocationList.this.getLocation(LocationList.this.country.get(arg2).toString());
                Intent returnIntent = new Intent();
                returnIntent.putExtra("policeId", LocationList.this.str[0]);
                System.out.println("str[0]::::::::::::" + LocationList.this.str[0]);
                returnIntent.putExtra("time", LocationList.this.str[1]);
                System.out.println(LocationList.this.str[1]);
                returnIntent.putExtra("date", LocationList.this.str[2]);
                System.out.println(LocationList.this.str[2]);
                returnIntent.putExtra("address", LocationList.this.str[3]);
                returnIntent.putExtra("city", LocationList.this.str[4]);
                returnIntent.putExtra("country", LocationList.this.str[5]);
                returnIntent.putExtra("state", LocationList.this.str[6]);
                returnIntent.putExtra("ReadSurfaceCondition", LocationList.this.str[7]);
                returnIntent.putExtra("lightConditon", LocationList.this.str[8]);
                returnIntent.putExtra("weatherCondition", LocationList.this.str[9]);
                LocationList.this.setResult(-1, returnIntent);
                LocationList.this.finish();
            }
        });
    }

    public void getLocation(String country2) {
        String WHERE = "country ='" + country2 + "'";
        System.out.println(String.valueOf(country2) + ":country");
        try {
            openConnection();
            Cursor c = this.db.query(USER_TABLE, new String[]{"policeId", "Time", "Date", "Address", "city", "country", "state", "ReadSurfaceCondition", "lightCondition", "weatherCondition"}, WHERE, null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    this.str[0] = c.getString(0);
                    this.str[1] = c.getString(1);
                    this.str[2] = c.getString(2);
                    this.str[3] = c.getString(3);
                    this.str[4] = c.getString(4);
                    this.str[5] = c.getString(5);
                    this.str[6] = c.getString(6);
                    this.str[7] = c.getString(7);
                    this.str[8] = c.getString(8);
                    this.str[9] = c.getString(9);
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    public void openConnection() {
        this.db = getApplicationContext().openOrCreateDatabase("CarInsurance.dbnew", 0, null);
    }

    public void closeConnection() {
        this.db.close();
    }

    class adapter extends BaseAdapter {
        Activity c;
        private LayoutInflater inflater;

        adapter(Activity c2) {
            this.c = c2;
            this.inflater = LayoutInflater.from(c2);
            System.out.println("adapter cons");
        }

        public int getCount() {
            System.out.println("getCount()");
            return LocationList.this.country.size();
        }

        public Object getItem(int position) {
            System.out.println("getItem(position)");
            return LocationList.this.country;
        }

        public long getItemId(int position) {
            System.out.println("getItemId(position)");
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                this.inflater = this.c.getLayoutInflater();
                convertView = this.inflater.inflate((int) R.layout.report_list, (ViewGroup) null);
                System.out.println("getView()");
            }
            TextView tv = (TextView) convertView.findViewById(R.id.TextView01);
            tv.setTextSize(20.0f);
            tv.setTextColor(-16777216);
            tv.setText(LocationList.this.country.get(position).toString());
            return convertView;
        }
    }
}
