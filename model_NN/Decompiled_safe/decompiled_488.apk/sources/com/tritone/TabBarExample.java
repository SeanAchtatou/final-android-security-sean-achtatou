package com.tritone;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import com.THOMSONROGERS.R;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class TabBarExample extends TabActivity {
    String Address;
    String City;
    String Country;
    String DLno;
    String[] Docs;
    String Driver;
    String Email;
    String Firstname;
    String Insucomp;
    String Lastname;
    String License;
    String LicensePlate;
    String Lightcond;
    String Make;
    String Model;
    String Passanger;
    String PhoneNo;
    String PhoneNumber;
    String Policyno;
    String Roadcond;
    String State;
    String Time;
    String Timedate;
    String Vehiclemake;
    String Vehiclemodel;
    String Weather;
    AlertDialog.Builder alertbox;
    String cur_status = "";
    String date1;
    private int day;
    DBDriod droid;
    private int hours;
    int i;
    int index;
    String injuredPhoneNUmber;
    String injuredname;
    Intent intent;
    Intent intent1;
    Intent intent2;
    Button mail;
    Button menu;
    private int min;
    private int month;
    String oldDate;
    String oldTime;
    String operation;
    String policeNumber;
    String policename;
    String reportnumber;
    private int sec;
    String[] str = new String[14];
    String[] str1 = new String[15];
    String tab2;
    TabHost tabHost;
    String time1;
    Uri uri;
    String witnessname;
    private int year;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView(R.layout.main);
        this.tabHost = (TabHost) findViewById(16908306);
        this.alertbox = new AlertDialog.Builder(this);
        Bundle b = getIntent().getExtras();
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", 1);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putInt("RecordIndexVal", 0);
        prefsEditor.putString("operation", "");
        prefsEditor.commit();
        if (b != null) {
            this.operation = b.getString("operation");
            this.oldDate = b.getString("date");
            this.oldTime = b.getString("time");
            this.str1[0] = b.getString("firstname1");
            this.str1[1] = b.getString("lastname1");
            this.str1[2] = b.getString("phno1");
            this.str1[3] = b.getString("email1");
            this.str1[4] = b.getString("dlno1");
            this.str1[5] = b.getString("state1");
            this.str1[6] = b.getString("LicensePlate1");
            this.str1[7] = b.getString("VehicleMake1");
            this.str1[8] = b.getString("PassengerNews1");
            this.str1[9] = b.getString("InsuranceCompany1");
            this.str1[10] = b.getString("PolicyNumber1");
            this.str1[11] = b.getString("phoneNumber1");
            this.str1[12] = b.getString("bodyPhoneNumber1");
            Calendar cal = Calendar.getInstance();
            this.year = cal.get(1);
            this.month = cal.get(2);
            this.day = cal.get(5);
            this.hours = cal.get(10);
            this.min = cal.get(12);
            this.sec = cal.get(13);
        }
        if (this.operation.equals("new")) {
            this.intent = new Intent(this, FirstTab.class);
            this.intent.putExtra("lookatme", "look");
            this.intent1 = new Intent(this, SecondTab.class);
            this.intent2 = new Intent(this, ThirdTab.class);
            String date = String.valueOf(this.day) + "-" + (this.month + 1) + "-" + this.year;
            String time = String.valueOf(this.hours) + ":" + this.min + ":" + this.sec;
            this.cur_status = "new";
            System.out.println("new date: " + date + " : " + time);
            prefsEditor.putString("time", time);
            prefsEditor.putString("date", date);
            prefsEditor.putString("operation", "new");
            prefsEditor.commit();
        } else if (this.operation.equals("edit")) {
            DBDriod dBDriod = new DBDriod(this);
            String[] str2 = dBDriod.getEditForm();
            this.intent = new Intent(this, FirstTab.class);
            this.intent.putExtra("lookatme", "look1");
            this.intent.putExtra("firstname1", str2[0]);
            this.cur_status = "other";
            this.intent.putExtra("lastname1", str2[1]);
            this.intent.putExtra("dlno1", str2[2]);
            this.intent.putExtra("phno1", str2[3]);
            this.intent.putExtra("email1", str2[4]);
            this.intent.putExtra("injured", str2[5]);
            this.intent.putExtra("vehiclemake", str2[6]);
            this.intent.putExtra("vehiclemodel", str2[7]);
            this.intent.putExtra("licenseplate", str2[8]);
            this.intent1 = new Intent(this, SecondTab.class);
            String[] str12 = dBDriod.getLocationForm();
            this.intent1.putExtra("time1", str12[1]);
            this.intent1.putExtra("date1", str12[2]);
            prefsEditor.putString("time", str12[1]);
            prefsEditor.putString("date", str12[2]);
            ArrayList list = dBDriod.getSpecificInsurance(myPrefs.getString("date", "date"), myPrefs.getString("time", ""));
            prefsEditor.putString("operation", "recent");
            prefsEditor.commit();
            this.intent1.putExtra("address1", str12[3]);
            this.intent1.putExtra("city1", str12[4]);
            this.intent1.putExtra("country1", str12[5]);
            this.intent1.putExtra("state1", str12[6]);
            this.intent1.putExtra("road1", str12[7]);
            this.intent1.putExtra("light1", str12[8]);
            this.intent1.putExtra("weather1", str12[9]);
            this.intent1.putExtra("witness1", str12[10]);
            this.intent2 = new Intent(this, ThirdTab.class);
            String[] insuranceForm = dBDriod.getInsuranceForm();
            try {
                this.intent2.putExtra("Driver1", list.get(0).toString());
                this.intent2.putExtra("DriverphoneNumber", list.get(1).toString());
                this.intent2.putExtra("LicensePlate1", list.get(2).toString());
                this.intent2.putExtra("InsuranceCompany1", list.get(3).toString());
                this.intent2.putExtra("PolicyNumber1", list.get(4).toString());
                this.intent2.putExtra("VehicleMake1", list.get(5).toString());
                this.intent2.putExtra("VehicleModel1", list.get(6).toString());
                this.intent2.putExtra("Witnessnmeedit1", list.get(7).toString());
                this.intent2.putExtra("WitnessPhNoedit1", list.get(8).toString());
                this.intent2.putExtra("Inturernmeedit1", list.get(9).toString());
                this.intent2.putExtra("InturerNoedit1", list.get(10).toString());
                this.intent2.putExtra("Policenmeedit1", list.get(11).toString());
                this.intent2.putExtra("PoliceNoedit1", list.get(12).toString());
                this.intent2.putExtra("ReportNoedit1", list.get(13).toString());
                this.intent2.putExtra("operation", "moreinfo1");
            } catch (Exception e) {
            }
        } else if (this.operation.equals("moreinfo")) {
            this.intent2 = new Intent(this, ThirdTab1.class);
            this.intent = new Intent(this, FirstTab.class);
            this.intent.putExtra("lookatme", "look");
            this.intent.putExtra("firstname1", this.str1[0]);
            this.intent.putExtra("lastname1", this.str1[1]);
            this.intent.putExtra("dlno1", this.str1[4]);
            this.intent.putExtra("phno1", this.str1[2]);
            this.intent.putExtra("email1", this.str1[3]);
            this.intent.putExtra("injured", "no");
            this.intent.putExtra("vehiclemake", this.str1[6]);
            this.intent.putExtra("vehiclemodel", this.str1[7]);
            this.intent.putExtra("licenseplate", this.str1[8]);
            this.intent1 = new Intent(this, SecondTab.class);
        } else if (this.operation.equals("moreinfo1")) {
            this.intent2 = new Intent(this, ThirdTab1.class);
            String date2 = myPrefs.getString("date", "date");
            String time2 = myPrefs.getString("time", "");
            DBDriod dBDriod2 = new DBDriod(this);
            try {
                ArrayList list2 = dBDriod2.getSpecificInsurance(date2, time2);
                if (list2.size() > 14) {
                    this.intent2.putExtra("Driver1", list2.get(14).toString());
                    this.intent2.putExtra("DriverphoneNumber1", list2.get(15).toString());
                    this.intent2.putExtra("LicensePlate1", list2.get(16).toString());
                    this.intent2.putExtra("InsuranceCompany1", list2.get(17).toString());
                    this.intent2.putExtra("PolicyNumber1", list2.get(18).toString());
                    this.intent2.putExtra("VehicleMake1", list2.get(19).toString());
                    this.intent2.putExtra("VehicleModel1", list2.get(20).toString());
                    this.intent2.putExtra("Witnessnmeedit1", list2.get(21).toString());
                    this.intent2.putExtra("WitnessPhNoedit1", list2.get(22).toString());
                    this.intent2.putExtra("Inturernmeedit1", list2.get(23).toString());
                    this.intent2.putExtra("InturerNoedit1", list2.get(24).toString());
                    this.intent2.putExtra("Policenmeedit1", list2.get(25).toString());
                    this.intent2.putExtra("PoliceNoedit1", list2.get(26).toString());
                    this.intent2.putExtra("ReportNoedit1", list2.get(27).toString());
                }
            } catch (Exception e2) {
            }
            this.intent1 = new Intent(this, SecondTab.class);
            try {
                String[][] st2 = dBDriod2.getLocationlist();
                this.intent1.putExtra("address1", st2[this.i][3]);
                this.intent1.putExtra("city1", st2[this.i][4]);
                this.intent1.putExtra("country1", st2[this.i][5]);
                this.intent1.putExtra("state1", st2[this.i][6]);
                this.intent1.putExtra("road1", st2[this.i][7]);
                this.intent1.putExtra("light1", st2[this.i][8]);
                this.intent1.putExtra("weather1", st2[this.i][9]);
                this.intent1.putExtra("witness1", st2[this.i][10]);
            } catch (Exception e3) {
            }
            this.intent = new Intent(this, FirstTab.class);
            String[] str3 = dBDriod2.getEditForm();
            this.intent.putExtra("lookatme", "look2");
            this.intent.putExtra("firstname1", str3[0]);
            this.intent.putExtra("lastname1", str3[1]);
            this.intent.putExtra("dlno1", str3[2]);
            this.intent.putExtra("phno1", str3[3]);
            this.intent.putExtra("email1", str3[4]);
            this.intent.putExtra("injured", str3[5]);
            this.intent.putExtra("vehiclemake", str3[6]);
            this.intent.putExtra("vehiclemodel", str3[7]);
            this.intent.putExtra("licenseplate", str3[8]);
        } else if (this.operation.equals("personal")) {
            this.intent = new Intent(this, FirstTab.class);
            this.intent.putExtra("lookatme", "look");
            this.intent.putExtra("firstname1", this.str1[0]);
            this.intent.putExtra("lastname1", this.str1[1]);
            this.intent.putExtra("dlno1", this.str1[4]);
            this.intent.putExtra("phno1", this.str1[2]);
            this.intent.putExtra("email1", this.str1[3]);
            this.intent.putExtra("injured", this.str1[5]);
            this.intent.putExtra("vehiclemake", this.str1[6]);
            this.intent.putExtra("vehiclemodel", this.str1[7]);
            this.intent.putExtra("licenseplate", this.str1[8]);
            prefsEditor.putString("time", String.valueOf(this.hours) + ":" + this.min);
            prefsEditor.putString("date", String.valueOf(this.day) + "-" + (this.month + 1) + "-" + this.year);
            prefsEditor.putString("operation", "new");
            prefsEditor.commit();
            this.cur_status = "new";
            this.intent1 = new Intent(this, SecondTab.class);
            this.intent2 = new Intent(this, ThirdTab.class);
        } else if (this.operation.equals("moreinfo2")) {
            this.intent = new Intent(this, FirstTab.class);
            DBDriod dBDriod3 = new DBDriod(this);
            this.intent.putExtra("lookatme", "look1");
            String[][] st1 = dBDriod3.getEditlist();
            this.intent.putExtra("firstname1", st1[this.i][0]);
            this.intent.putExtra("lastname1", st1[this.i][1]);
            this.intent.putExtra("dlno1", st1[this.i][2]);
            this.intent.putExtra("phno1", st1[this.i][3]);
            this.intent.putExtra("email1", st1[this.i][4]);
            this.intent.putExtra("injured", st1[this.i][5]);
            this.intent.putExtra("vehiclemake", st1[this.i][6]);
            this.intent.putExtra("vehiclemodel", st1[this.i][7]);
            this.intent.putExtra("licenseplate", st1[this.i][8]);
            Bundle extras = getIntent().getExtras();
            this.intent1 = new Intent(this, SecondTab.class);
            String[][] st22 = dBDriod3.getLocationlist();
            prefsEditor.putInt("RecordIndexVal", this.i);
            prefsEditor.putString("operation", "oldrecords");
            prefsEditor.commit();
            this.intent1.putExtra("address1", st22[this.i][3]);
            this.intent1.putExtra("city1", st22[this.i][4]);
            this.intent1.putExtra("country1", st22[this.i][5]);
            this.intent1.putExtra("state1", st22[this.i][6]);
            this.intent1.putExtra("road1", st22[this.i][7]);
            this.intent1.putExtra("light1", st22[this.i][8]);
            this.intent1.putExtra("weather1", st22[this.i][9]);
            this.intent1.putExtra("witness1", st22[this.i][10]);
            this.intent2 = new Intent(this, ThirdTab1.class);
            ArrayList list3 = dBDriod3.getSpecificInsurance(myPrefs.getString("date", ""), myPrefs.getString("time", ""));
            this.intent2.putExtra("Driver1", list3.get(0).toString());
            this.intent2.putExtra("DriverphoneNumber", list3.get(1).toString());
            this.intent2.putExtra("LicensePlate1", list3.get(2).toString());
            this.intent2.putExtra("InsuranceCompany1", list3.get(3).toString());
            this.intent2.putExtra("PolicyNumber1", list3.get(4).toString());
            this.intent2.putExtra("VehicleMake1", list3.get(5).toString());
            this.intent2.putExtra("VehicleModel1", list3.get(6).toString());
            this.intent2.putExtra("Witnessnmeedit1", list3.get(7).toString());
            this.intent2.putExtra("WitnessPhNoedit1", list3.get(8).toString());
            this.intent2.putExtra("Inturernmeedit1", list3.get(9).toString());
            this.intent2.putExtra("InturerNoedit1", list3.get(10).toString());
            this.intent2.putExtra("Policenmeedit1", list3.get(11).toString());
            this.intent2.putExtra("PoliceNoedit1", list3.get(12).toString());
            this.intent2.putExtra("ReportNoedit1", list3.get(13).toString());
            if (list3.size() > 14) {
                this.intent2.putExtra("Driver1", list3.get(14).toString());
                this.intent2.putExtra("DriverphoneNumber1", list3.get(15).toString());
                this.intent2.putExtra("LicensePlate1", list3.get(16).toString());
                this.intent2.putExtra("InsuranceCompany1", list3.get(17).toString());
                this.intent2.putExtra("PolicyNumber1", list3.get(18).toString());
                this.intent2.putExtra("VehicleMake1", list3.get(19).toString());
                this.intent2.putExtra("VehicleModel1", list3.get(20).toString());
                this.intent2.putExtra("Witnessnmeedit1", list3.get(21).toString());
                this.intent2.putExtra("WitnessPhNoedit1", list3.get(22).toString());
                this.intent2.putExtra("Inturernmeedit1", list3.get(23).toString());
                this.intent2.putExtra("InturerNoedit1", list3.get(24).toString());
                this.intent2.putExtra("Policenmeedit1", list3.get(25).toString());
                this.intent2.putExtra("PoliceNoedit1", list3.get(26).toString());
                this.intent2.putExtra("ReportNoedit1", list3.get(27).toString());
            }
        }
        if (this.operation.equals("oldRecords")) {
            DBDriod dBDriod4 = new DBDriod(this);
            this.intent = new Intent(this, FirstTab.class);
            this.intent.putExtra("lookatme", "look1");
            Bundle b1 = getIntent().getExtras();
            this.intent1 = new Intent(this, SecondTab.class);
            this.intent2 = new Intent(this, ThirdTab.class);
            this.intent2.putExtra("operation", "moreinfo2");
            System.out.println(" date: " + this.oldDate + " : " + this.oldTime);
            if (b1 != null) {
                this.i = 0;
                String[][] st12 = dBDriod4.getEditlist(this.oldDate, this.oldTime);
                String[][] st23 = dBDriod4.getLocationlist(this.oldDate, this.oldTime);
                ArrayList list4 = dBDriod4.getSpecificInsurance(this.oldDate, this.oldTime);
                this.cur_status = "other";
                this.intent.putExtra("firstname1", st12[this.i][0]);
                this.intent.putExtra("lastname1", st12[this.i][1]);
                this.intent.putExtra("dlno1", st12[this.i][2]);
                this.intent.putExtra("phno1", st12[this.i][3]);
                this.intent.putExtra("email1", st12[this.i][4]);
                this.intent.putExtra("injured", st12[this.i][5]);
                this.intent.putExtra("vehiclemake", st12[this.i][6]);
                this.intent.putExtra("vehiclemodel", st12[this.i][7]);
                this.intent.putExtra("licenseplate", st12[this.i][8]);
                this.intent1.putExtra("time1", st23[this.i][0]);
                this.intent1.putExtra("date1", st23[this.i][1]);
                prefsEditor.putString("time", st23[this.i][1]);
                prefsEditor.putString("date", st23[this.i][2]);
                prefsEditor.putInt("RecordIndexVal", this.i);
                prefsEditor.putString("operation", "oldrecords");
                prefsEditor.commit();
                this.intent1.putExtra("address1", st23[this.i][3]);
                this.intent1.putExtra("city1", st23[this.i][4]);
                this.intent1.putExtra("country1", st23[this.i][5]);
                this.intent1.putExtra("state1", st23[this.i][6]);
                this.intent1.putExtra("road1", st23[this.i][7]);
                this.intent1.putExtra("light1", st23[this.i][8]);
                this.intent1.putExtra("weather1", st23[this.i][9]);
                this.intent1.putExtra("witness1", st23[this.i][10]);
                if (list4.size() > 0) {
                    this.intent2.putExtra("Driver1", list4.get(0).toString());
                    this.intent2.putExtra("DriverphoneNumber", list4.get(1).toString());
                    this.intent2.putExtra("LicensePlate1", list4.get(2).toString());
                    this.intent2.putExtra("InsuranceCompany1", list4.get(3).toString());
                    this.intent2.putExtra("PolicyNumber1", list4.get(4).toString());
                    this.intent2.putExtra("VehicleMake1", list4.get(5).toString());
                    this.intent2.putExtra("VehicleModel1", list4.get(6).toString());
                    this.intent2.putExtra("Witnessnmeedit1", list4.get(7).toString());
                    this.intent2.putExtra("WitnessPhNoedit1", list4.get(8).toString());
                    this.intent2.putExtra("Inturernmeedit1", list4.get(9).toString());
                    this.intent2.putExtra("InturerNoedit1", list4.get(10).toString());
                    this.intent2.putExtra("Policenmeedit1", list4.get(11).toString());
                    this.intent2.putExtra("PoliceNoedit1", list4.get(12).toString());
                    this.intent2.putExtra("ReportNoedit1", list4.get(13).toString());
                }
                if (list4.size() > 14) {
                    this.intent2.putExtra("Driver2", list4.get(14).toString());
                    this.intent2.putExtra("DriverphoneNumber2", list4.get(15).toString());
                    this.intent2.putExtra("LicensePlate2", list4.get(16).toString());
                    this.intent2.putExtra("InsuranceCompany2", list4.get(17).toString());
                    this.intent2.putExtra("PolicyNumber2", list4.get(18).toString());
                    this.intent2.putExtra("VehicleMake2", list4.get(19).toString());
                    this.intent2.putExtra("VehicleModel2", list4.get(20).toString());
                    this.intent2.putExtra("Witnessnmeedit2", list4.get(21).toString());
                    this.intent2.putExtra("WitnessPhNoedit2", list4.get(22).toString());
                    this.intent2.putExtra("Inturernmeedit2", list4.get(23).toString());
                    this.intent2.putExtra("InturerNoedit2", list4.get(24).toString());
                    this.intent2.putExtra("Policenmeedit2", list4.get(25).toString());
                    this.intent2.putExtra("PoliceNoedit2", list4.get(26).toString());
                    this.intent2.putExtra("ReportNoedit2", list4.get(27).toString());
                }
            }
        }
        TabHost.TabSpec firstSpec = this.tabHost.newTabSpec("tab1");
        TabHost.TabSpec thirdSpec = this.tabHost.newTabSpec("tab3");
        TabHost.TabSpec secondSpec = this.tabHost.newTabSpec("tab2");
        TabHost.TabSpec fourthSpec = this.tabHost.newTabSpec("tab4");
        firstSpec.setIndicator("", getResources().getDrawable(R.drawable.tabone)).setContent(this.intent2);
        secondSpec.setIndicator("", getResources().getDrawable(R.drawable.tabtwo)).setContent(this.intent1);
        thirdSpec.setIndicator("", getResources().getDrawable(R.drawable.lady)).setContent(this.intent);
        fourthSpec.setIndicator("", getResources().getDrawable(R.drawable.tabfour)).setContent(new Intent(this, ListPhotoDrawCam.class));
        this.tabHost.addTab(firstSpec);
        this.tabHost.addTab(secondSpec);
        this.tabHost.addTab(thirdSpec);
        this.tabHost.addTab(fourthSpec);
        this.tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            public void onTabChanged(String tabId) {
                if (!tabId.equals("tab2") && !tabId.equals("tab3")) {
                    tabId.equals("tab4");
                }
            }
        });
        this.menu = (Button) findViewById(R.id.back);
        this.menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TabBarExample.this.alertbox.setMessage("\t \t \t \t Law Firm\nAre you sure you want to exit this page");
                TabBarExample.this.alertbox.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
                TabBarExample.this.alertbox.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        TabBarExample.this.finish();
                    }
                });
                TabBarExample.this.alertbox.show();
            }
        });
        this.mail = (Button) findViewById(R.id.mail);
        this.mail.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String temperory;
                SharedPreferences myPrefs = TabBarExample.this.getSharedPreferences("myPrefs", 1);
                TabBarExample.this.time1 = myPrefs.getString("time", "time");
                TabBarExample.this.date1 = myPrefs.getString("date", "date");
                TabBarExample.this.index = myPrefs.getInt("RecordIndexVal", 0);
                TabBarExample.this.operation = myPrefs.getString("operation", "");
                TabBarExample.this.droid = new DBDriod(TabBarExample.this);
                String[] str = TabBarExample.this.droid.getEditForm();
                int length = str.length;
                for (int i = 0; i < length; i++) {
                    String str2 = str[i];
                    TabBarExample.this.Firstname = str[0];
                    TabBarExample.this.Lastname = str[1];
                    TabBarExample.this.DLno = str[2];
                    TabBarExample.this.PhoneNo = str[3];
                    TabBarExample.this.Email = str[4];
                    TabBarExample.this.LicensePlate = str[6];
                    TabBarExample.this.Make = str[7];
                    TabBarExample.this.Model = str[8];
                }
                String[] str3 = TabBarExample.this.droid.getLocationForm();
                int length2 = str3.length;
                for (int i2 = 0; i2 < length2; i2++) {
                    String str4 = str3[i2];
                    TabBarExample.this.Time = str3[1];
                    TabBarExample.this.Timedate = str3[2];
                    TabBarExample.this.City = str3[4];
                    TabBarExample.this.Address = str3[3];
                    TabBarExample.this.State = str3[6];
                    TabBarExample.this.Country = str3[5];
                    TabBarExample.this.Roadcond = str3[7];
                    TabBarExample.this.Lightcond = str3[8];
                    TabBarExample.this.Weather = str3[9];
                }
                ArrayList list = TabBarExample.this.droid.getSpecificInsurance(TabBarExample.this.date1, TabBarExample.this.time1);
                String dr = "";
                String licensePlate = "";
                String insurComp = "";
                String policyNo = "";
                String vehicleMake = "";
                String vehicleModel = "";
                String witnessName = "";
                String witnessPh = "";
                String injuredName = "";
                String injuredPh = "";
                String policeName = "";
                String policePh = "";
                String reportNo = "";
                if (list.size() == 14) {
                    dr = list.get(0).toString();
                    licensePlate = list.get(1).toString();
                    insurComp = list.get(2).toString();
                    policyNo = list.get(3).toString();
                    vehicleMake = list.get(4).toString();
                    vehicleModel = list.get(5).toString();
                    witnessName = list.get(6).toString();
                    witnessPh = list.get(7).toString();
                    injuredName = list.get(8).toString();
                    injuredPh = list.get(9).toString();
                    policeName = list.get(10).toString();
                    policePh = list.get(11).toString();
                    reportNo = list.get(12).toString();
                }
                if (TabBarExample.this.operation.equals("new")) {
                    if (!myPrefs.getBoolean("database", false) || !myPrefs.getBoolean("database1", false) || !myPrefs.getBoolean("database2", false)) {
                        TabBarExample.this.Firstname = "";
                        TabBarExample.this.Lastname = "";
                        TabBarExample.this.DLno = "";
                        TabBarExample.this.PhoneNo = "";
                        TabBarExample.this.Email = "";
                        TabBarExample.this.LicensePlate = "";
                        TabBarExample.this.Make = "";
                        TabBarExample.this.Model = "";
                        TabBarExample.this.Time = "";
                        TabBarExample.this.Timedate = "";
                        TabBarExample.this.City = "";
                        TabBarExample.this.Address = "";
                        TabBarExample.this.State = "";
                        TabBarExample.this.Country = "";
                        TabBarExample.this.Roadcond = "";
                        TabBarExample.this.Lightcond = "";
                        TabBarExample.this.Weather = "";
                        TabBarExample.this.Driver = "";
                        TabBarExample.this.License = "";
                        TabBarExample.this.Insucomp = "";
                        TabBarExample.this.Policyno = "";
                        TabBarExample.this.Passanger = "";
                        TabBarExample.this.Vehiclemake = "";
                        TabBarExample.this.Vehiclemodel = "";
                        TabBarExample.this.witnessname = "";
                        TabBarExample.this.PhoneNumber = "";
                        TabBarExample.this.injuredname = "";
                        TabBarExample.this.injuredPhoneNUmber = "";
                        TabBarExample.this.policename = "";
                        TabBarExample.this.policeNumber = "";
                        TabBarExample.this.reportnumber = "";
                    }
                    String[] editForm = TabBarExample.this.droid.getEditForm();
                    if (DBDriod.str.length == 9) {
                        TabBarExample.this.Firstname = DBDriod.str[0];
                        TabBarExample.this.Lastname = DBDriod.str[1];
                        TabBarExample.this.DLno = DBDriod.str[2];
                        TabBarExample.this.PhoneNo = DBDriod.str[3];
                        TabBarExample.this.Email = DBDriod.str[4];
                        TabBarExample.this.LicensePlate = DBDriod.str[5];
                        TabBarExample.this.Make = DBDriod.str[6];
                        TabBarExample.this.Model = DBDriod.str[7];
                    }
                    String[] locationForm = TabBarExample.this.droid.getLocationForm();
                    if (DBDriod.str1.length == 11) {
                        TabBarExample.this.Time = DBDriod.str1[1];
                        TabBarExample.this.Timedate = DBDriod.str1[2];
                        TabBarExample.this.City = DBDriod.str1[3];
                        TabBarExample.this.Address = DBDriod.str1[4];
                        TabBarExample.this.State = DBDriod.str1[5];
                        TabBarExample.this.Country = DBDriod.str1[6];
                        TabBarExample.this.Roadcond = DBDriod.str1[7];
                        TabBarExample.this.Lightcond = DBDriod.str1[8];
                        TabBarExample.this.Weather = DBDriod.str1[9];
                    }
                    String[] insuranceForm = TabBarExample.this.droid.getInsuranceForm();
                    if (DBDriod.str2.length == 14) {
                        TabBarExample.this.Driver = DBDriod.str2[4];
                        TabBarExample.this.License = DBDriod.str2[1];
                        TabBarExample.this.Insucomp = DBDriod.str2[2];
                        TabBarExample.this.Policyno = DBDriod.str2[3];
                        TabBarExample.this.Passanger = DBDriod.str2[0];
                        TabBarExample.this.Vehiclemake = DBDriod.str2[5];
                        TabBarExample.this.Vehiclemodel = DBDriod.str2[6];
                        TabBarExample.this.witnessname = DBDriod.str2[7];
                        TabBarExample.this.PhoneNumber = DBDriod.str2[8];
                        TabBarExample.this.injuredname = DBDriod.str2[9];
                        TabBarExample.this.injuredPhoneNUmber = DBDriod.str2[10];
                        TabBarExample.this.policename = DBDriod.str2[11];
                        TabBarExample.this.policeNumber = DBDriod.str2[12];
                        TabBarExample.this.reportnumber = DBDriod.str2[13];
                    }
                } else if (TabBarExample.this.operation.equals("oldrecords")) {
                    String[][] st1 = TabBarExample.this.droid.getEditlist(TabBarExample.this.oldDate, TabBarExample.this.oldTime);
                    String[][] st2 = TabBarExample.this.droid.getLocationlist(TabBarExample.this.oldDate, TabBarExample.this.oldTime);
                    ArrayList<String> Insurancelist = TabBarExample.this.droid.getSpecificInsurance(TabBarExample.this.oldDate, TabBarExample.this.oldTime);
                    TabBarExample.this.Firstname = st1[0][0];
                    TabBarExample.this.Lastname = st1[0][1];
                    TabBarExample.this.DLno = st1[0][2];
                    TabBarExample.this.PhoneNo = st1[0][3];
                    TabBarExample.this.Email = st1[0][4];
                    TabBarExample.this.LicensePlate = st1[0][5];
                    TabBarExample.this.Make = st1[0][6];
                    TabBarExample.this.Model = st1[0][7];
                    TabBarExample.this.Timedate = st2[0][0];
                    TabBarExample.this.Time = st2[0][1];
                    TabBarExample.this.Address = st2[0][3];
                    TabBarExample.this.City = st2[0][4];
                    TabBarExample.this.Country = st2[0][5];
                    TabBarExample.this.State = st2[0][6];
                    TabBarExample.this.Roadcond = st2[0][7];
                    TabBarExample.this.Lightcond = st2[0][8];
                    TabBarExample.this.Weather = st2[0][9];
                    TabBarExample.this.Driver = (String) Insurancelist.get(0);
                    TabBarExample.this.License = (String) Insurancelist.get(1);
                    TabBarExample.this.Insucomp = (String) Insurancelist.get(2);
                    TabBarExample.this.Policyno = (String) Insurancelist.get(3);
                    TabBarExample.this.Vehiclemake = (String) Insurancelist.get(4);
                    TabBarExample.this.Vehiclemodel = (String) Insurancelist.get(5);
                    TabBarExample.this.witnessname = (String) Insurancelist.get(6);
                    TabBarExample.this.PhoneNumber = (String) Insurancelist.get(7);
                    TabBarExample.this.injuredname = (String) Insurancelist.get(8);
                    TabBarExample.this.injuredPhoneNUmber = (String) Insurancelist.get(9);
                    TabBarExample.this.policename = (String) Insurancelist.get(10);
                    TabBarExample.this.policeNumber = (String) Insurancelist.get(11);
                    TabBarExample.this.reportnumber = (String) Insurancelist.get(12);
                }
                ArrayList<String> images = TabBarExample.this.droid.getImage(TabBarExample.this.time1, TabBarExample.this.date1);
                ArrayList<String> audioRecord = TabBarExample.this.droid.getmusic(TabBarExample.this.date1, TabBarExample.this.time1);
                ArrayList<String> textNote = TabBarExample.this.droid.getNote(TabBarExample.this.date1, TabBarExample.this.time1);
                int size = 0;
                try {
                    size = images.size() + audioRecord.size() + textNote.size();
                    TabBarExample.this.Docs = new String[size];
                    int temp = 0;
                    for (int i3 = 0; i3 < images.size(); i3++) {
                        TabBarExample.this.Docs[i3] = "/sdcard/" + images.get(i3);
                        System.out.println("image: " + images.get(i3));
                        temp++;
                    }
                    for (int i4 = 0; i4 < audioRecord.size(); i4++) {
                        TabBarExample.this.Docs[temp] = "/sdcard/com.hascode.recorder/" + audioRecord.get(i4);
                        temp++;
                    }
                    for (int i5 = 0; i5 < textNote.size(); i5++) {
                        if (textNote.get(i5).length() >= 3) {
                            temperory = String.valueOf(textNote.get(i5).substring(0, 3)) + TabBarExample.this.date1 + ".txt";
                        } else {
                            temperory = String.valueOf(textNote.get(i5)) + TabBarExample.this.date1 + ".txt";
                        }
                        try {
                            File root = Environment.getExternalStorageDirectory();
                            if (root.canWrite()) {
                                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(root, temperory)));
                                bufferedWriter.write(textNote.get(i5));
                                bufferedWriter.close();
                            }
                        } catch (IOException e) {
                        }
                        TabBarExample.this.Docs[temp] = "/sdcard/" + temperory;
                        temp++;
                    }
                    if (size > 0) {
                        TabBarExample.this.zipfiles();
                        TabBarExample.this.uri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "zipfiles.zip"));
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("jpeg/image");
                intent.putExtra("android.intent.extra.EMAIL", new String[]{"mike@michaelpadway.com"});
                intent.putExtra("android.intent.extra.SUBJECT", "Accident Report form Motorcycle Accident Toolkit");
                intent.putExtra("android.intent.extra.TEXT", "firstname:" + TabBarExample.this.Firstname + "\n lastname:" + TabBarExample.this.Lastname + "\n" + "\n Phoneno:" + TabBarExample.this.PhoneNo + "\n Email:\t" + TabBarExample.this.Email + "\n LicensePlate: \t" + TabBarExample.this.LicensePlate + "\n Make: \t" + TabBarExample.this.Make + "\n Model: \t" + TabBarExample.this.Model + "\n Date:\t" + TabBarExample.this.Timedate + "\n Time:\t" + TabBarExample.this.Time + "\n Address:\t" + TabBarExample.this.Address + "\n City:" + TabBarExample.this.City + "\n Country:" + TabBarExample.this.Country + "\n State:\t" + TabBarExample.this.State + "\n RoadSurfaceCondition:\t" + TabBarExample.this.Roadcond + "\n lightCondition:\t" + TabBarExample.this.Lightcond + "\n WeatherCondition:\t" + TabBarExample.this.Weather + "\n Driver:\t" + dr + "\n LicensePlate:\t" + licensePlate + "\n InsuranceCompany:\t" + insurComp + "\n PolicyNumber:\t" + policyNo + "\n VehicleMake:\t" + vehicleMake + "\n VehicleModel:\t" + vehicleModel + "\n WitnessName:\t" + witnessName + "\n WitnessPhoneNumber:\t" + witnessPh + "\n Injured Name:\t" + injuredName + "\n Injured Phone Number:\t" + injuredPh + "\n PoliceName:\t" + policeName + "\n Police Phone Number:\t" + policePh + "\n Report Number:\t" + reportNo);
                System.out.println("firstname:" + TabBarExample.this.Firstname + "\n lastname:" + TabBarExample.this.Lastname + "\n" + TabBarExample.this.PhoneNo + "\n Email:\t" + TabBarExample.this.Email + "\n LicensePlate: \t" + TabBarExample.this.LicensePlate + "\n Make: \t" + TabBarExample.this.Make + "\n Model: \t" + TabBarExample.this.Model + "\n Date:\t" + TabBarExample.this.Timedate + "\n Time:\t" + TabBarExample.this.Time + "\n Address:\t" + TabBarExample.this.Address + "\n City:" + TabBarExample.this.City + "\n Country:" + TabBarExample.this.Country + "\n State:\t" + TabBarExample.this.State + "\n RoadSurfaceCondition:\t" + TabBarExample.this.Roadcond + "\n lightCondition:\t" + TabBarExample.this.Lightcond + "\n WeatherCondition:\t" + TabBarExample.this.Weather + "\n Driver:\t" + dr + "\n LicensePlate:\t" + licensePlate + "\n InsuranceCompany:\t" + insurComp + "\n PolicyNumber:\t" + policyNo + "\n VehicleMake:\t" + vehicleMake + "\n VehicleModel:\t" + vehicleModel + "\n WitnessName:\t" + witnessName + "\n WitnessPhoneNumber:\t" + witnessPh + "\n Injured Name:\t" + injuredName + "\n Injured Phone Number:\t" + injuredPh + "\n PoliceName:\t" + policeName + "\n Police Phone Number:\t" + policePh + "\n Report Number:\t" + reportNo);
                if (size > 0) {
                    intent.putExtra("android.intent.extra.STREAM", TabBarExample.this.uri);
                }
                TabBarExample.this.startActivity(Intent.createChooser(intent, "Send mail...."));
            }
        });
    }

    /* access modifiers changed from: private */
    public void zipfiles() {
        try {
            byte[] buffer = new byte[1024];
            ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(new File(Environment.getExternalStorageDirectory(), "zipfiles.zip")));
            for (int i2 = 0; i2 < this.Docs.length; i2++) {
                FileInputStream fin = new FileInputStream(this.Docs[i2]);
                zout.putNextEntry(new ZipEntry(this.Docs[i2]));
                while (true) {
                    int length = fin.read(buffer);
                    if (length <= 0) {
                        break;
                    }
                    zout.write(buffer, 0, length);
                }
                zout.closeEntry();
                fin.close();
            }
            zout.close();
        } catch (IOException e) {
        }
    }
}
