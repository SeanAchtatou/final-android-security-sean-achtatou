package com.tritone;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.THOMSONROGERS.R;
import java.util.ArrayList;

public class EditFormList extends Activity {
    public static final String DATABASE_NAME = "CarInsurance.dbnew";
    public static final String USER_TABLE_NAME = "EditFormLatest";
    SQLiteDatabase db;
    DBDriod droid;
    ArrayList<String> firstname = new ArrayList<>();
    ListView list;
    String name;
    String[] str = new String[5];

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        this.droid = new DBDriod(this);
        setContentView((int) R.layout.users_list);
        this.firstname = this.droid.getEditCount();
        for (int i = 0; i < this.firstname.size(); i++) {
            System.out.println("firstname:  " + this.firstname.get(i));
        }
        this.list = (ListView) findViewById(R.id.ListView01);
        this.list.setAdapter((ListAdapter) new adapter(this));
        System.out.println(this.firstname + "happy:");
        this.list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                EditFormList.this.getEdit(EditFormList.this.firstname.get(arg2).toString());
                System.out.println("firstname:::::::::::::::::::::::" + EditFormList.this.firstname.get(arg2) + "i:::::" + arg2);
                Intent returnIntent = new Intent();
                returnIntent.putExtra("firstname", EditFormList.this.str[0]);
                System.out.println("str[0]::::::::::::::::::::::::::::::::::::::::" + EditFormList.this.str[0]);
                returnIntent.putExtra("lastname", EditFormList.this.str[1]);
                returnIntent.putExtra("DLNO", EditFormList.this.str[2]);
                returnIntent.putExtra("Pno", EditFormList.this.str[3]);
                returnIntent.putExtra("Email", EditFormList.this.str[4]);
                EditFormList.this.setResult(-1, returnIntent);
                EditFormList.this.finish();
            }
        });
    }

    public void getEdit(String firstname2) {
        String WHERE = " FirstName = '" + firstname2 + "'";
        try {
            System.out.println("database Called");
            openConnection();
            System.out.println("database Called1");
            Cursor c = this.db.query("EditFormLatest", new String[]{"FirstName", "LastName", "DLNO", "Pno", "Email"}, WHERE, null, null, null, null);
            System.out.println("database Called2");
            int numRows = c.getCount();
            System.out.println("database Called3");
            c.moveToFirst();
            System.out.println("database Called4");
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    this.str[0] = c.getString(0);
                    System.out.println("db" + this.str[0]);
                    this.str[1] = c.getString(1);
                    System.out.println("db" + this.str[1]);
                    this.str[2] = c.getString(2);
                    System.out.println("db" + this.str[2]);
                    this.str[3] = c.getString(3);
                    System.out.println("db" + this.str[3]);
                    this.str[4] = c.getString(4);
                    System.out.println("db" + this.str[4]);
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    public void openConnection() {
        this.db = getApplicationContext().openOrCreateDatabase("CarInsurance.dbnew", 0, null);
    }

    public void closeConnection() {
        this.db.close();
    }

    class adapter extends BaseAdapter {
        Activity contex;
        private LayoutInflater inflater;

        adapter(Activity context) {
            this.contex = context;
            this.inflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return EditFormList.this.firstname.size();
        }

        public Object getItem(int position) {
            return EditFormList.this.firstname;
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                this.inflater = this.contex.getLayoutInflater();
                convertView = this.inflater.inflate((int) R.layout.report_list, (ViewGroup) null);
            }
            TextView tv = (TextView) convertView.findViewById(R.id.TextView01);
            System.out.println("Pos: " + position + " val: " + EditFormList.this.firstname.get(position));
            tv.setTextColor(-16777216);
            tv.setTextSize(25.0f);
            tv.setText(EditFormList.this.firstname.get(position));
            return convertView;
        }
    }
}
