package com.tritone;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.THOMSONROGERS.R;
import java.util.ArrayList;

public class DriverList extends Activity {
    public static final String DATABASE_NAME = "CarInsurance.dbnew";
    public static final String USER_TABLE_NAME = "Insurance";
    SQLiteDatabase db;
    DBDriod droid;
    ListView list;
    ArrayList<String> policyNumber = new ArrayList<>();
    String[] str = new String[14];

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        this.droid = new DBDriod(this);
        setContentView((int) R.layout.users_list);
        this.policyNumber = this.droid.getInsuranceCount();
        System.out.println(this.policyNumber + "::::::::::::::::::::");
        this.list = (ListView) findViewById(R.id.ListView01);
        this.list.setAdapter((ListAdapter) new adapter(this));
        this.list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                DriverList.this.getInsurance(DriverList.this.policyNumber.get(arg2).toString());
                System.out.println("firstname:::::::::::::::::::::::" + DriverList.this.policyNumber);
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Driver", DriverList.this.str[0]);
                System.out.println("str[0]::::::::::::::::::::::::::::::::::::::::" + DriverList.this.str[0]);
                returnIntent.putExtra("licensePlate", DriverList.this.str[1]);
                returnIntent.putExtra("InsuranceCompany", DriverList.this.str[2]);
                returnIntent.putExtra("policyNumber", DriverList.this.str[3]);
                returnIntent.putExtra("vehicleMake", DriverList.this.str[4]);
                returnIntent.putExtra("vehicleModel", DriverList.this.str[5]);
                returnIntent.putExtra("witnessName", DriverList.this.str[6]);
                returnIntent.putExtra("witnessPhoneNo", DriverList.this.str[7]);
                returnIntent.putExtra("DLNo", DriverList.this.str[8]);
                returnIntent.putExtra("InturerName", DriverList.this.str[9]);
                returnIntent.putExtra("InturerPhNo", DriverList.this.str[10]);
                returnIntent.putExtra("Policenmeedit", DriverList.this.str[11]);
                returnIntent.putExtra("PoliceNoedit", DriverList.this.str[12]);
                returnIntent.putExtra("ReportNoedit", DriverList.this.str[13]);
                DriverList.this.setResult(-1, returnIntent);
                DriverList.this.finish();
            }
        });
    }

    public void getInsurance(String policyNumber2) {
        String WHERE = " PolicyNumber = '" + policyNumber2 + "'";
        try {
            openConnection();
            Cursor c = this.db.query("Insurance", new String[]{"Driver", "licensePlate", "InsuranceCompany", "policyNumber", "vehicleMake", "vehicleModel", "witnessName", "witnessPhoneNo", "DLNo", "InturerName", "InturerPhNo"}, WHERE, null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            if (c != null) {
                for (int i = 0; i < numRows; i++) {
                    this.str[0] = c.getString(0);
                    this.str[1] = c.getString(1);
                    this.str[2] = c.getString(2);
                    this.str[3] = c.getString(3);
                    this.str[4] = c.getString(4);
                    this.str[5] = c.getString(5);
                    this.str[6] = c.getString(6);
                    this.str[7] = c.getString(7);
                    this.str[8] = c.getString(8);
                    this.str[9] = c.getString(9);
                    this.str[10] = c.getString(10);
                    this.str[11] = c.getString(11);
                    c.moveToNext();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    public void openConnection() {
        this.db = getApplicationContext().openOrCreateDatabase("CarInsurance.dbnew", 0, null);
    }

    public void closeConnection() {
        this.db.close();
    }

    class adapter extends BaseAdapter {
        Activity context;
        LayoutInflater inflater;

        adapter(Activity context2) {
            this.context = context2;
            this.inflater = LayoutInflater.from(context2);
        }

        public int getCount() {
            return DriverList.this.policyNumber.size();
        }

        public Object getItem(int position) {
            return DriverList.this.policyNumber;
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                System.out.println("inside getview");
                convertView = this.context.getLayoutInflater().inflate((int) R.layout.report_list, (ViewGroup) null);
            }
            TextView tv = (TextView) convertView.findViewById(R.id.TextView01);
            tv.setTextColor(-16777216);
            tv.setTextSize(20.0f);
            tv.setText(DriverList.this.policyNumber.get(position));
            return convertView;
        }
    }
}
