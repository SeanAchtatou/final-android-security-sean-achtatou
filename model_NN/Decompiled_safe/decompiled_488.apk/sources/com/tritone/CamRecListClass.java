package com.tritone;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.THOMSONROGERS.R;
import com.hascode.android.PlayRecorded;
import com.pictures.Image_Preview;
import java.util.ArrayList;

public class CamRecListClass extends Activity {
    String date;
    DBDriod droid;
    ArrayList<String> image_files = new ArrayList<>();
    String operation;
    int record_image = R.drawable.mic2;
    String time;
    ListView view;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.camreclist);
        ((Button) findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CamRecListClass.this.finish();
            }
        });
        this.droid = new DBDriod(this);
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", 1);
        SharedPreferences.Editor edit = myPrefs.edit();
        this.time = myPrefs.getString("time", "time");
        this.date = myPrefs.getString("date", "date");
        Bundle b = getIntent().getExtras();
        if (b != null) {
            this.operation = b.getString("mode");
        }
        this.view = (ListView) findViewById(R.id.ListView01);
        if (this.operation.equals("camera")) {
            this.image_files = this.droid.getImage(this.time, this.date);
        } else {
            this.image_files = this.droid.getmusic(this.date, this.time);
        }
        if (this.image_files.size() == 0) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Attention");
            alertDialog.setMessage("No records available.");
            alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    CamRecListClass.this.finish();
                }
            });
            alertDialog.create().show();
        }
        this.view.setDividerHeight(1);
        this.view.setAdapter((ListAdapter) new Base(this));
        this.view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if (CamRecListClass.this.operation.equals("camera")) {
                    Intent intent = new Intent(CamRecListClass.this, Image_Preview.class);
                    intent.putExtra("uri", "/sdcard/" + CamRecListClass.this.image_files.get(arg2));
                    intent.putExtra("image", CamRecListClass.this.image_files.get(arg2));
                    CamRecListClass.this.startActivity(intent);
                    return;
                }
                Intent intent2 = new Intent(CamRecListClass.this, PlayRecorded.class);
                intent2.putExtra("path", "/sdcard/com.hascode.recorder/" + CamRecListClass.this.image_files.get(arg2));
                System.out.println("image file: " + CamRecListClass.this.image_files.get(arg2));
                intent2.putExtra("record", CamRecListClass.this.image_files.get(arg2));
                CamRecListClass.this.startActivity(intent2);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.operation.equals("camera")) {
            this.image_files = this.droid.getImage(this.time, this.date);
        } else {
            this.image_files = this.droid.getmusic(this.date, this.time);
        }
        this.view.setAdapter((ListAdapter) new Base(this));
    }

    class Base extends BaseAdapter {
        Activity con;
        LayoutInflater inflater = LayoutInflater.from(this.con);

        public Base(Activity context) {
            this.con = context;
        }

        public int getCount() {
            return CamRecListClass.this.image_files.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                this.inflater = this.con.getLayoutInflater();
                convertView = this.inflater.inflate((int) R.layout.listelements, (ViewGroup) null);
            }
            ImageView ivimagephotodummy = (ImageView) convertView.findViewById(R.id.imagecam2);
            TextView tvphotonums = (TextView) convertView.findViewById(R.id.TextView02);
            if (CamRecListClass.this.operation.equals("camera")) {
                ivimagephotodummy.setBackgroundResource(R.drawable.photodummy);
                tvphotonums.setText("Photo-" + position);
            } else {
                ivimagephotodummy.setBackgroundResource(R.drawable.mic2);
                tvphotonums.setText("Recorder" + position);
            }
            return convertView;
        }
    }
}
