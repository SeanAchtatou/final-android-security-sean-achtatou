package com.tritone;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.THOMSONROGERS.R;
import java.util.ArrayList;

public class ThirdTab extends Activity {
    private static final String CLASS_TAG = "Validate";
    public static final String DATABASE_NAME = "CarInsurance.dbneww";
    public static final String USER_TABLE_NAME = "EditFormLatest";
    EditText DLNOedit;
    EditText DriverPhNoedit;
    EditText Insurancecompanyedit;
    EditText InturerNoedit;
    EditText Inturernmeedit;
    EditText LicencePlateedit;
    EditText PoliceNoedit;
    EditText Policenmeedit;
    EditText PolicyNoedit;
    EditText ReportNoedit;
    EditText VehicleModel;
    EditText WitnessPhNoedit;
    EditText Witnessnmeedit;
    Button add;
    private String[] array_spinner;
    String date1;
    SQLiteDatabase db;
    Button delete;
    Button done;
    String driver;
    DBDriod droid;
    ArrayList<String> list = new ArrayList<>();
    String operation;
    String passengerNo;
    SharedPreferences.Editor prefsEditor;
    ArrayList<String> records = new ArrayList<>();
    String s1 = "";
    EditText spinner1;
    String[] str = new String[5];
    String time1;
    EditText vehicleMakeedit;
    String warning = "";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.driver);
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", 1);
        this.prefsEditor = myPrefs.edit();
        this.time1 = myPrefs.getString("time", "time");
        this.date1 = myPrefs.getString("date", "date");
        this.operation = myPrefs.getString("operation", "");
        this.prefsEditor.putBoolean("database2", false);
        this.prefsEditor.commit();
        this.LicencePlateedit = (EditText) findViewById(R.id.LicencePlateedit);
        this.Insurancecompanyedit = (EditText) findViewById(R.id.Insurancecompanyedit);
        this.PolicyNoedit = (EditText) findViewById(R.id.PolicyNoedit);
        this.DriverPhNoedit = (EditText) findViewById(R.id.DriverPhNoedit);
        this.vehicleMakeedit = (EditText) findViewById(R.id.vehicleMakeedit);
        this.spinner1 = (EditText) findViewById(R.id.Drivernmeedit);
        this.VehicleModel = (EditText) findViewById(R.id.Passangeredit12);
        this.Witnessnmeedit = (EditText) findViewById(R.id.Witnessnmeedit);
        this.WitnessPhNoedit = (EditText) findViewById(R.id.WitnessPhNoedit);
        this.Inturernmeedit = (EditText) findViewById(R.id.Inturernmeedit);
        this.InturerNoedit = (EditText) findViewById(R.id.InturerNoedit);
        this.Policenmeedit = (EditText) findViewById(R.id.Policenmeedit);
        this.PoliceNoedit = (EditText) findViewById(R.id.PoliceNoedit);
        this.ReportNoedit = (EditText) findViewById(R.id.ReportNoedit);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            this.driver = b.getString("Driver1");
            try {
                this.s1 = b.getString("operation");
            } catch (NullPointerException e) {
            }
            this.spinner1.setText(b.getString("Driver1"));
            this.DriverPhNoedit.setText(b.getString("DriverphoneNumber"));
            this.LicencePlateedit.setText(b.getString("LicensePlate1"));
            this.Insurancecompanyedit.setText(b.getString("InsuranceCompany1"));
            this.PolicyNoedit.setText(b.getString("PolicyNumber1"));
            this.passengerNo = b.getString("PassengerNumber1");
            this.vehicleMakeedit.setText(b.getString("VehicleMake1"));
            this.VehicleModel.setText(b.getString("VehicleModel1"));
            this.Witnessnmeedit.setText(b.getString("Witnessnmeedit1"));
            this.WitnessPhNoedit.setText(b.getString("WitnessPhNoedit1"));
            this.Inturernmeedit.setText(b.getString("Inturernmeedit1"));
            this.InturerNoedit.setText(b.getString("InturerNoedit1"));
            this.Policenmeedit.setText(b.getString("Policenmeedit1"));
            this.PoliceNoedit.setText(b.getString("PoliceNoedit1"));
            this.ReportNoedit.setText(b.getString("ReportNoedit1"));
        }
        this.droid = new DBDriod(this);
        this.spinner1 = (EditText) findViewById(R.id.Drivernmeedit);
        ((RelativeLayout) findViewById(R.id.drvab)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) ThirdTab.this.getSystemService("input_method");
                imm.hideSoftInputFromWindow(ThirdTab.this.spinner1.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab.this.DriverPhNoedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab.this.LicencePlateedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab.this.Insurancecompanyedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab.this.PolicyNoedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab.this.vehicleMakeedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab.this.VehicleModel.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab.this.Witnessnmeedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab.this.WitnessPhNoedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab.this.Inturernmeedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab.this.InturerNoedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab.this.Policenmeedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab.this.PoliceNoedit.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(ThirdTab.this.ReportNoedit.getWindowToken(), 0);
            }
        });
        this.done = (Button) findViewById(R.id.done);
        this.done.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ThirdTab.this.droid.getSpecificInsuranceRecordCount(ThirdTab.this.date1, ThirdTab.this.time1) == 0) {
                    ThirdTab.this.droid.createInsurance(ThirdTab.this.date1, ThirdTab.this.time1, ThirdTab.this.spinner1.getText().toString(), ThirdTab.this.DriverPhNoedit.getText().toString(), ThirdTab.this.LicencePlateedit.getText().toString(), ThirdTab.this.Insurancecompanyedit.getText().toString(), ThirdTab.this.PolicyNoedit.getText().toString(), ThirdTab.this.vehicleMakeedit.getText().toString(), ThirdTab.this.VehicleModel.getText().toString(), ThirdTab.this.Witnessnmeedit.getText().toString(), ThirdTab.this.WitnessPhNoedit.getText().toString(), ThirdTab.this.Inturernmeedit.getText().toString(), ThirdTab.this.InturerNoedit.getText().toString(), ThirdTab.this.Policenmeedit.getText().toString(), ThirdTab.this.PoliceNoedit.getText().toString(), ThirdTab.this.ReportNoedit.getText().toString());
                    System.out.println("1");
                } else {
                    ThirdTab.this.droid.updateInsurance(ThirdTab.this.date1, ThirdTab.this.time1, ThirdTab.this.date1, ThirdTab.this.time1, ThirdTab.this.spinner1.getText().toString(), ThirdTab.this.DriverPhNoedit.getText().toString(), ThirdTab.this.LicencePlateedit.getText().toString(), ThirdTab.this.Insurancecompanyedit.getText().toString(), ThirdTab.this.PolicyNoedit.getText().toString(), ThirdTab.this.vehicleMakeedit.getText().toString(), ThirdTab.this.VehicleModel.getText().toString(), ThirdTab.this.Witnessnmeedit.getText().toString(), ThirdTab.this.WitnessPhNoedit.getText().toString(), ThirdTab.this.Inturernmeedit.getText().toString(), ThirdTab.this.InturerNoedit.getText().toString(), ThirdTab.this.Policenmeedit.getText().toString(), ThirdTab.this.PoliceNoedit.getText().toString(), ThirdTab.this.ReportNoedit.getText().toString());
                    System.out.println("2");
                }
                Toast.makeText(ThirdTab.this.getBaseContext(), "saved successfully", 1).show();
                ThirdTab.this.done.setBackgroundResource(R.drawable.saveblue);
                ThirdTab.this.prefsEditor.putBoolean("database2", true);
                ThirdTab.this.prefsEditor.commit();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.droid.getSpecificInsuranceRecordCount(this.date1, this.time1) == 0) {
            this.droid.createInsurance(this.date1, this.time1, this.spinner1.getText().toString(), this.DriverPhNoedit.getText().toString(), this.LicencePlateedit.getText().toString(), this.Insurancecompanyedit.getText().toString(), this.PolicyNoedit.getText().toString(), this.vehicleMakeedit.getText().toString(), this.VehicleModel.getText().toString(), this.Witnessnmeedit.getText().toString(), this.WitnessPhNoedit.getText().toString(), this.Inturernmeedit.getText().toString(), this.InturerNoedit.getText().toString(), this.Policenmeedit.getText().toString(), this.PoliceNoedit.getText().toString(), this.ReportNoedit.getText().toString());
            System.out.println("1");
        } else {
            this.droid.updateInsurance(this.date1, this.time1, this.date1, this.time1, this.spinner1.getText().toString(), this.DriverPhNoedit.getText().toString(), this.LicencePlateedit.getText().toString(), this.Insurancecompanyedit.getText().toString(), this.PolicyNoedit.getText().toString(), this.vehicleMakeedit.getText().toString(), this.VehicleModel.getText().toString(), this.Witnessnmeedit.getText().toString(), this.WitnessPhNoedit.getText().toString(), this.Inturernmeedit.getText().toString(), this.InturerNoedit.getText().toString(), this.Policenmeedit.getText().toString(), this.PoliceNoedit.getText().toString(), this.ReportNoedit.getText().toString());
            System.out.println("2");
        }
        this.prefsEditor.putBoolean("database2", true);
        this.prefsEditor.commit();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == -1) {
                    String stringExtra = data.getStringExtra("Driver");
                    String licensePlate = data.getStringExtra("licensePlate");
                    String insuranceCompany = data.getStringExtra("InsuranceCompany");
                    String policyNumber = data.getStringExtra("policyNumber");
                    String stringExtra2 = data.getStringExtra("passengerNews");
                    String vehicleMake = data.getStringExtra("vehicleMake");
                    String stringExtra3 = data.getStringExtra("vehicleModel");
                    this.Insurancecompanyedit.setText(insuranceCompany);
                    this.PolicyNoedit.setText(policyNumber);
                    this.LicencePlateedit.setText(licensePlate);
                    this.vehicleMakeedit.setText(vehicleMake);
                    break;
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, "Update");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                this.droid.updateInsuranceForm(this.passengerNo, this.DriverPhNoedit.getText().toString(), this.LicencePlateedit.getText().toString(), this.Insurancecompanyedit.getText().toString(), this.PolicyNoedit.getText().toString(), this.driver, this.vehicleMakeedit.getText().toString());
                Toast.makeText(getBaseContext(), "deleted", 0).show();
                this.Insurancecompanyedit.setText(" ");
                this.PolicyNoedit.setText(" ");
                this.LicencePlateedit.setText(" ");
                this.vehicleMakeedit.setText(" ");
                this.spinner1.setText(" ");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public static boolean hasText(EditText editText) {
        String text = editText.getText().toString().trim();
        if (text.length() != 0) {
            return true;
        }
        editText.setText(text);
        return false;
    }

    public void AlertDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Required...");
        alertDialog.setMessage("Please enter the " + this.warning + " field");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.setIcon((int) R.drawable.warning);
        alertDialog.show();
    }

    /* access modifiers changed from: protected */
    public boolean validated() {
        boolean validated = true;
        if (!FirstTab.hasText(this.spinner1)) {
            this.warning = "firstname";
            validated = false;
        }
        if (!FirstTab.hasText(this.DriverPhNoedit)) {
            this.warning = "phone Number";
            validated = false;
        }
        if (!FirstTab.hasText(this.LicencePlateedit)) {
            this.warning = "licensePlate";
            validated = false;
        }
        if (!FirstTab.hasText(this.Insurancecompanyedit)) {
            this.warning = "insurance company";
            validated = false;
        }
        if (!FirstTab.hasText(this.PolicyNoedit)) {
            this.warning = "policy Number";
            validated = false;
        }
        if (!FirstTab.hasText(this.vehicleMakeedit)) {
            this.warning = "vehicle Make";
            validated = false;
        }
        if (FirstTab.hasText(this.VehicleModel)) {
            return validated;
        }
        this.warning = "vehicle Model";
        return false;
    }
}
