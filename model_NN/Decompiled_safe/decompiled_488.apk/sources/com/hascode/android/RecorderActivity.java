package com.hascode.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import com.THOMSONROGERS.R;
import com.tritone.DBDriod;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class RecorderActivity extends Activity {
    private static final String APP_TAG = "com.hascode.android.soundrecorder";
    String audiofile;
    private Button btPlay;
    private Button btRecord;
    Chronometer ch;
    public int click = 1;
    String date;
    DBDriod droid;
    Boolean fileSaved = false;
    private final View.OnClickListener handlePlayClick = new View.OnClickListener() {
        public void onClick(View view) {
            if (!RecorderActivity.this.playing) {
                RecorderActivity.this.startPlay();
                RecorderActivity.this.timeInterval = RecorderActivity.this.player.getDuration();
                RecorderActivity.this.ch.stop();
                RecorderActivity.this.ch.setBase(SystemClock.elapsedRealtime());
                RecorderActivity.this.ch.start();
                RecorderActivity.this.showElapsedTime();
                RecorderActivity.this.timer.scheduleAtFixedRate(new TimerTask() {
                    public void run() {
                        if (!RecorderActivity.this.player.isPlaying()) {
                            RecorderActivity.this.ch.stop();
                            RecorderActivity.this.timer.cancel();
                            RecorderActivity.this.temp = 0;
                        }
                        RecorderActivity.this.temp = 1;
                    }
                }, 0, 1000);
            }
        }
    };
    private final View.OnClickListener handleRecordClick = new View.OnClickListener() {
        public void onClick(View view) {
            if (!RecorderActivity.this.recording) {
                if (RecorderActivity.this.click == 1) {
                    RecorderActivity.this.ch.stop();
                    RecorderActivity.this.ch.setBase(SystemClock.elapsedRealtime());
                    RecorderActivity.this.ch.start();
                    RecorderActivity.this.showElapsedTime();
                    RecorderActivity.this.startRecord();
                    System.out.println("record click1 " + RecorderActivity.this.click);
                }
                if (RecorderActivity.this.click == 2) {
                    System.out.println("record click3 " + RecorderActivity.this.click);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(RecorderActivity.this);
                    alertDialog.setTitle("Attention");
                    alertDialog.setMessage("Sorry! You cant record twice on this screen. Click OK to save this recording");
                    alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            RecorderActivity.this.finish();
                        }
                    });
                    alertDialog.create().show();
                }
                RecorderActivity.this.click++;
            }
        }
    };
    private File outfile = null;
    /* access modifiers changed from: private */
    public MediaPlayer player = new MediaPlayer();
    /* access modifiers changed from: private */
    public boolean playing = false;
    /* access modifiers changed from: private */
    public MediaRecorder recorder = new MediaRecorder();
    /* access modifiers changed from: private */
    public boolean recording = false;
    private TextView resultView;
    private Button stop;
    private final View.OnClickListener stoprecording = new View.OnClickListener() {
        public void onClick(View v) {
            if (RecorderActivity.this.recording) {
                RecorderActivity.this.ch.stop();
                RecorderActivity.this.fileSaved = true;
                RecorderActivity.this.recorder.stop();
                RecorderActivity.this.recorder.reset();
                RecorderActivity.this.recorder.release();
                RecorderActivity.this.recording = false;
                RecorderActivity.this.droid.createmusic(RecorderActivity.this.date, RecorderActivity.this.time, RecorderActivity.this.audiofile);
                RecorderActivity.this.droid.getmusic(RecorderActivity.this.date, RecorderActivity.this.time);
                RecorderActivity.this.showElapsedTime();
                RecorderActivity.this.ch.setBase(SystemClock.elapsedRealtime());
                RecorderActivity.this.showElapsedTime();
            } else if (RecorderActivity.this.playing) {
                RecorderActivity.this.stopPlay();
                RecorderActivity.this.ch.stop();
                RecorderActivity.this.showElapsedTime();
                RecorderActivity.this.ch.setBase(SystemClock.elapsedRealtime());
                RecorderActivity.this.showElapsedTime();
            }
        }
    };
    int temp = 0;
    String time;
    int timeInterval;
    Timer timer;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.record);
        this.ch = (Chronometer) findViewById(R.id.Chronometer01);
        this.droid = new DBDriod(this);
        this.timer = new Timer();
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", 1);
        this.time = myPrefs.getString("time", "time");
        this.date = myPrefs.getString("date", "date");
        this.fileSaved = false;
        ((Button) findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (RecorderActivity.this.playing) {
                    RecorderActivity.this.stopPlay();
                }
                RecorderActivity.this.ch.stop();
                if (!RecorderActivity.this.fileSaved.booleanValue()) {
                    RecorderActivity.this.stopRecord();
                }
                RecorderActivity.this.finish();
            }
        });
        try {
            File storageDir = new File(Environment.getExternalStorageDirectory(), "com.hascode.recorder");
            storageDir.mkdir();
            Log.d(APP_TAG, "Storage directory set to " + storageDir);
            this.outfile = File.createTempFile("hascode", ".mp3", storageDir);
            String[] temp2 = this.outfile.getAbsolutePath().split("/");
            if (temp2.length > 0) {
                this.audiofile = temp2[temp2.length - 1];
            }
            System.out.println("audio: " + this.audiofile);
            System.out.println("output: " + this.outfile);
            this.recorder.setAudioSource(1);
            this.recorder.setOutputFormat(1);
            this.recorder.setAudioEncoder(1);
            this.recorder.setOutputFile(this.outfile.getAbsolutePath());
            this.player.setDataSource(this.outfile.getAbsolutePath());
        } catch (IOException e) {
            Log.w(APP_TAG, "File not accessible ", e);
        } catch (IllegalArgumentException e2) {
            Log.w(APP_TAG, "Illegal argument ", e2);
        } catch (IllegalStateException e3) {
            Log.w(APP_TAG, "Illegal state, call reset/restore", e3);
        }
        this.btRecord = (Button) findViewById(R.id.btRecord);
        this.btRecord.setOnClickListener(this.handleRecordClick);
        this.btPlay = (Button) findViewById(R.id.btPlay);
        this.btPlay.setOnClickListener(this.handlePlayClick);
        this.stop = (Button) findViewById(R.id.stop);
        this.stop.setOnClickListener(this.stoprecording);
    }

    /* access modifiers changed from: private */
    public void showElapsedTime() {
        long elapsedRealtime = SystemClock.elapsedRealtime() - this.ch.getBase();
    }

    /* access modifiers changed from: private */
    public void startRecord() {
        Log.d(APP_TAG, "start recording..");
        try {
            this.recorder.prepare();
            this.recorder.start();
            this.recording = true;
        } catch (IllegalStateException e) {
            Log.w(APP_TAG, "Invalid recorder state .. reset/release should have been called");
        } catch (IOException e2) {
            Log.w(APP_TAG, "Could not write to sd card");
        }
    }

    /* access modifiers changed from: private */
    public void stopRecord() {
        Log.d(APP_TAG, "stop recording..");
        if (this.recording) {
            this.fileSaved = true;
            this.recorder.stop();
            this.recorder.reset();
            this.recorder.release();
            this.recording = false;
            this.droid.createmusic(this.date, this.time, this.audiofile);
            this.droid.getmusic(this.date, this.time);
        }
    }

    /* access modifiers changed from: private */
    public void startPlay() {
        Log.d(APP_TAG, "starting playback..");
        try {
            this.playing = true;
            this.player.prepare();
            this.player.start();
        } catch (IllegalStateException e) {
            Log.w(APP_TAG, "illegal state .. player should be reset");
        } catch (IOException e2) {
            Log.w(APP_TAG, "Could not write to sd card");
        }
    }

    /* access modifiers changed from: private */
    public void stopPlay() {
        Log.d(APP_TAG, "stopping playback..");
        this.player.stop();
        this.player.reset();
        this.player.release();
        this.playing = false;
    }
}
