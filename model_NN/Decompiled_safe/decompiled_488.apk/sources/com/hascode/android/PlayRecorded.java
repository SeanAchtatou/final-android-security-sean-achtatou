package com.hascode.android;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import com.THOMSONROGERS.R;
import com.tritone.DBDriod;
import java.io.File;
import java.io.IOException;

public class PlayRecorded extends Activity {
    private static final String APP_TAG = "com.hascode.android.soundrecorder";
    DBDriod droid;
    /* access modifiers changed from: private */
    public String outfile = "";
    private MediaPlayer player = new MediaPlayer();
    boolean playing;
    /* access modifiers changed from: private */
    public String record;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.playrec);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            this.outfile = b.getString("path");
            this.record = b.getString("record");
        }
        this.droid = new DBDriod(this);
        ImageButton play = (ImageButton) findViewById(R.id.ImageButton01);
        Button back = (Button) findViewById(R.id.PlayBack);
        Button delete = (Button) findViewById(R.id.PlayTrash);
        try {
            this.player.setDataSource(this.outfile);
        } catch (IOException e) {
            Log.w(APP_TAG, "File not accessible ", e);
        } catch (IllegalArgumentException e2) {
            Log.w(APP_TAG, "Illegal argument ", e2);
        } catch (IllegalStateException e3) {
            Log.w(APP_TAG, "Illegal state, call reset/restore", e3);
        }
        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlayRecorded.this.finish();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlayRecorded.this.droid.deleteAudio(PlayRecorded.this.record);
                if (new File(PlayRecorded.this.outfile).delete()) {
                    System.out.println("File Deleted");
                }
                PlayRecorded.this.finish();
            }
        });
        play.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!PlayRecorded.this.playing) {
                    PlayRecorded.this.startPlay();
                }
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && this.playing) {
            stopPlay();
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: private */
    public void startPlay() {
        System.out.println("play called...");
        try {
            this.playing = true;
            this.player.prepare();
            this.player.start();
        } catch (IllegalStateException e) {
            Log.w(APP_TAG, "illegal state .. player should be reset");
        } catch (IOException e2) {
            Log.w(APP_TAG, "Could not write to sd card");
        }
    }

    private void stopPlay() {
        Log.d(APP_TAG, "stopping playback..");
        this.player.stop();
        this.player.reset();
        this.player.release();
        this.playing = false;
    }
}
