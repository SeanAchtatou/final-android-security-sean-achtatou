package com.hascode.android;

import android.app.Activity;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.Toast;
import com.THOMSONROGERS.R;
import com.tritone.DBDriod;
import java.io.File;
import java.io.IOException;

public class ChronoExample extends Activity {
    private static final String APP_TAG = "com.hascode.android.soundrecorder";
    String audiofile;
    Button b1;
    Button b2;
    Button b3;
    Chronometer ch;
    String date;
    DBDriod droid;
    private File outfile = null;
    private MediaPlayer player = new MediaPlayer();
    private boolean playing = false;
    private MediaRecorder recorder = new MediaRecorder();
    private boolean recording = false;
    String time;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.audiomainn);
        this.droid = new DBDriod(this);
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", 1);
        this.time = myPrefs.getString("time", "time");
        this.date = myPrefs.getString("date", "date");
        try {
            File storageDir = new File(Environment.getExternalStorageDirectory(), "com.hascode.recorder");
            storageDir.mkdir();
            Log.d(APP_TAG, "Storage directory set to " + storageDir);
            this.outfile = File.createTempFile("hascode", ".mp3", storageDir);
            this.audiofile = this.outfile.getAbsolutePath().substring(29);
            this.recorder.setAudioSource(1);
            this.recorder.setOutputFormat(1);
            this.recorder.setAudioEncoder(1);
            this.recorder.setOutputFile(this.outfile.getAbsolutePath());
            this.player.setDataSource(this.outfile.getAbsolutePath());
        } catch (IOException e) {
            Log.w(APP_TAG, "File not accessible ", e);
        } catch (IllegalArgumentException e2) {
            Log.w(APP_TAG, "Illegal argument ", e2);
        } catch (IllegalStateException e3) {
            Log.w(APP_TAG, "Illegal state, call reset/restore", e3);
        }
        this.ch = (Chronometer) findViewById(R.id.Chronometer01);
        this.b1 = (Button) findViewById(R.id.Button01);
        this.b1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ChronoExample.this.ch.start();
                ChronoExample.this.showElapsedTime();
                ChronoExample.this.beginRecording();
            }
        });
        this.b2 = (Button) findViewById(R.id.Button02);
        this.b2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ChronoExample.this.ch.stop();
                ChronoExample.this.showElapsedTime();
                ChronoExample.this.stopRecording();
                ChronoExample.this.ch.setBase(SystemClock.elapsedRealtime());
                ChronoExample.this.showElapsedTime();
            }
        });
        this.b3 = (Button) findViewById(R.id.Button03);
        this.b3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ChronoExample.this.ch.setBase(SystemClock.elapsedRealtime());
                ChronoExample.this.showElapsedTime();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showElapsedTime() {
        Toast.makeText(this, "Elapsed milliseconds: " + (SystemClock.elapsedRealtime() - this.ch.getBase()), 0).show();
    }

    /* access modifiers changed from: private */
    public void beginRecording() {
        System.out.println("1");
        killMediaRecorder();
        System.out.println("2");
        try {
            System.out.println(3);
            File storageDir = new File(Environment.getExternalStorageDirectory(), "com.hascode.record");
            System.out.println(4);
            storageDir.mkdir();
            System.out.println(5);
            Log.d(APP_TAG, "Storage directory set to " + storageDir);
            System.out.println(6);
            this.outfile = File.createTempFile("hascode", ".mp3", storageDir);
            System.out.println(7);
            this.recorder = new MediaRecorder();
            System.out.println(8);
            this.recorder.setAudioSource(1);
            System.out.println(9);
            this.recorder.setOutputFormat(1);
            System.out.println(10);
            this.recorder.setAudioEncoder(1);
            System.out.println(11);
            this.recorder.setOutputFile(this.outfile.getAbsolutePath());
            System.out.println(12);
        } catch (IOException e) {
            Log.w(APP_TAG, "File not accessible ", e);
        } catch (IllegalArgumentException e2) {
            Log.w(APP_TAG, "Illegal argument ", e2);
        } catch (IllegalStateException e3) {
            Log.w(APP_TAG, "Illegal state, call reset/restore", e3);
        }
    }

    private void killMediaRecorder() {
        if (this.recorder != null) {
            this.recorder.release();
        }
    }

    /* access modifiers changed from: private */
    public void stopRecording() {
        if (this.recorder != null) {
            try {
                this.recorder.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
