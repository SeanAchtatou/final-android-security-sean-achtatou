package com.THOMSONROGERS;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int gesture_color = 2131034112;
        public static final int white = 2131034113;
    }

    public static final class dimen {
        public static final int gesture_thumbnail_inset = 2131099648;
        public static final int gesture_thumbnail_size = 2131099649;
    }

    public static final class drawable {
        public static final int aboutourfirm = 2130837504;
        public static final int aboutourrfirm = 2130837505;
        public static final int aboutus = 2130837506;
        public static final int aboutusbutton = 2130837507;
        public static final int acc1 = 2130837508;
        public static final int accountinfobutton = 2130837509;
        public static final int address = 2130837510;
        public static final int androidmarker = 2130837511;
        public static final int applylocation = 2130837512;
        public static final int arow = 2130837513;
        public static final int attoneryinformation = 2130837514;
        public static final int attorneyinfo = 2130837515;
        public static final int background = 2130837516;
        public static final int balloon_overlay_bg_selector = 2130837517;
        public static final int balloon_overlay_close = 2130837518;
        public static final int balloon_overlay_focused = 2130837519;
        public static final int balloon_overlay_unfocused = 2130837520;
        public static final int bgblack = 2130837521;
        public static final int bgtab = 2130837522;
        public static final int bluescreen = 2130837523;
        public static final int brown = 2130837524;
        public static final int brown1 = 2130837525;
        public static final int callinsurancecompany = 2130837526;
        public static final int callmyemergency = 2130837527;
        public static final int callmylawyer = 2130837528;
        public static final int calltruckservice = 2130837529;
        public static final int camara = 2130837530;
        public static final int camera = 2130837531;
        public static final int cameraclick = 2130837532;
        public static final int cameralist = 2130837533;
        public static final int caraccidentreport = 2130837534;
        public static final int carinfo = 2130837535;
        public static final int carrental = 2130837536;
        public static final int carrentalinformation = 2130837537;
        public static final int carrepair = 2130837538;
        public static final int carrepairinformation = 2130837539;
        public static final int city = 2130837540;
        public static final int contactus = 2130837541;
        public static final int country = 2130837542;
        public static final int d1 = 2130837543;
        public static final int d2 = 2130837544;
        public static final int d3 = 2130837545;
        public static final int d4 = 2130837546;
        public static final int delete = 2130837547;
        public static final int done = 2130837548;
        public static final int doneblue = 2130837549;
        public static final int draw = 2130837550;
        public static final int drawing = 2130837551;
        public static final int driver = 2130837552;
        public static final int driverbg = 2130837553;
        public static final int driverblue = 2130837554;
        public static final int drivericon = 2130837555;
        public static final int drivername = 2130837556;
        public static final int driverph = 2130837557;
        public static final int driverphone = 2130837558;
        public static final int ediform = 2130837559;
        public static final int editrecentcasebutton = 2130837560;
        public static final int email1 = 2130837561;
        public static final int email60 = 2130837562;
        public static final int emailid = 2130837563;
        public static final int emergency = 2130837564;
        public static final int emergencybg = 2130837565;
        public static final int emergencycall = 2130837566;
        public static final int emergencycallmylawyer = 2130837567;
        public static final int emergencyhome = 2130837568;
        public static final int faq = 2130837569;
        public static final int faqbg = 2130837570;
        public static final int firstname = 2130837571;
        public static final int fourthtabbg = 2130837572;
        public static final int green = 2130837573;
        public static final int homepage = 2130837574;
        public static final int ic_gesturebuilder = 2130837575;
        public static final int icon = 2130837576;
        public static final int impints = 2130837577;
        public static final int infromation = 2130837578;
        public static final int injured = 2130837579;
        public static final int injuredblue = 2130837580;
        public static final int injuredimage = 2130837581;
        public static final int injuredinfo = 2130837582;
        public static final int injuredname = 2130837583;
        public static final int injuredphone = 2130837584;
        public static final int insurancecompany = 2130837585;
        public static final int insuranceinformation = 2130837586;
        public static final int lady = 2130837587;
        public static final int lastname = 2130837588;
        public static final int lasttab = 2130837589;
        public static final int legalpad_blue = 2130837695;
        public static final int legalpad_red = 2130837696;
        public static final int legalpad_yellow = 2130837694;
        public static final int licenceplate = 2130837590;
        public static final int licensenumber = 2130837591;
        public static final int licenseplate = 2130837592;
        public static final int lightcondition = 2130837593;
        public static final int location = 2130837594;
        public static final int locationbg = 2130837595;
        public static final int locationinfo = 2130837596;
        public static final int locationinformation = 2130837597;
        public static final int main = 2130837598;
        public static final int mainn = 2130837599;
        public static final int md1 = 2130837600;
        public static final int media = 2130837601;
        public static final int mediainfo = 2130837602;
        public static final int mediainformation = 2130837603;
        public static final int menu = 2130837604;
        public static final int menu60 = 2130837605;
        public static final int mic1 = 2130837606;
        public static final int mic2 = 2130837607;
        public static final int newcase = 2130837608;
        public static final int newcollision = 2130837609;
        public static final int newcollisionbutton = 2130837610;
        public static final int no = 2130837611;
        public static final int noblue = 2130837612;
        public static final int notepad = 2130837613;
        public static final int noteslist = 2130837614;
        public static final int nowinnofees = 2130837615;
        public static final int oldrecordbutton = 2130837616;
        public static final int oldrecords = 2130837617;
        public static final int oldrecordsbg = 2130837618;
        public static final int otherdriverinfo = 2130837619;
        public static final int personal = 2130837620;
        public static final int personalbg = 2130837621;
        public static final int personalinfo = 2130837622;
        public static final int personalinformation = 2130837623;
        public static final int personalinjured = 2130837624;
        public static final int personallocation = 2130837625;
        public static final int phonenum = 2130837626;
        public static final int photobg = 2130837627;
        public static final int photodummy = 2130837628;
        public static final int physicalinformation = 2130837629;
        public static final int play = 2130837630;
        public static final int police = 2130837631;
        public static final int policeinfo = 2130837632;
        public static final int policename = 2130837633;
        public static final int policephone = 2130837634;
        public static final int policereportnum = 2130837635;
        public static final int policynum = 2130837636;
        public static final int policynumber = 2130837637;
        public static final int pushpingreen = 2130837638;
        public static final int rec = 2130837639;
        public static final int recordinglist = 2130837640;
        public static final int roadcondition = 2130837641;
        public static final int rocket_thrust = 2130837642;
        public static final int saveblue = 2130837643;
        public static final int send = 2130837644;
        public static final int sendblue = 2130837645;
        public static final int splashscreen = 2130837646;
        public static final int startrecording = 2130837647;
        public static final int state = 2130837648;
        public static final int stopplaying = 2130837649;
        public static final int stoprecording = 2130837650;
        public static final int tabbg = 2130837651;
        public static final int tabfour = 2130837652;
        public static final int tabone = 2130837653;
        public static final int tabthree = 2130837654;
        public static final int tabtwo = 2130837655;
        public static final int tabtwoo = 2130837656;
        public static final int text = 2130837657;
        public static final int time = 2130837658;
        public static final int towing = 2130837659;
        public static final int towinginformation = 2130837660;
        public static final int trash = 2130837661;
        public static final int unknown = 2130837662;
        public static final int unknownblue = 2130837663;
        public static final int userinfobutton = 2130837664;
        public static final int v1 = 2130837665;
        public static final int vehiclemake = 2130837666;
        public static final int vehiclemake1 = 2130837667;
        public static final int vehiclemodel = 2130837668;
        public static final int voicerecord = 2130837669;
        public static final int voicerecordpage = 2130837670;
        public static final int warning = 2130837671;
        public static final int weather = 2130837672;
        public static final int websiteicon = 2130837673;
        public static final int whitesave = 2130837674;
        public static final int witness = 2130837675;
        public static final int witnessblue = 2130837676;
        public static final int witnessimage = 2130837677;
        public static final int witnessinfo = 2130837678;
        public static final int witnessname = 2130837679;
        public static final int witnessphone = 2130837680;
        public static final int wtd1 = 2130837681;
        public static final int wtd2 = 2130837682;
        public static final int wtd3 = 2130837683;
        public static final int wtd4 = 2130837684;
        public static final int wtd5 = 2130837685;
        public static final int wtd6 = 2130837686;
        public static final int wtd7 = 2130837687;
        public static final int wtdheading = 2130837688;
        public static final int xrayinformation = 2130837689;
        public static final int yellow = 2130837690;
        public static final int yellowpagewithicons = 2130837691;
        public static final int yes = 2130837692;
        public static final int yesblue = 2130837693;
    }

    public static final class id {
        public static final int BackButton = 2131230866;
        public static final int Button01 = 2131230732;
        public static final int Button02 = 2131230733;
        public static final int Button03 = 2131230734;
        public static final int Buttoncallmyemergency = 2131230863;
        public static final int Buttoncallmylawyer = 2131230864;
        public static final int Buttoncancel = 2131230970;
        public static final int Buttoninvisible = 2131230894;
        public static final int Chronometer01 = 2131230731;
        public static final int DLNOtext = 2131230772;
        public static final int DLnumber = 2131230940;
        public static final int Done = 2131230955;
        public static final int Done1 = 2131230928;
        public static final int Donebtn = 2131230969;
        public static final int DriverPhNoedit = 2131230753;
        public static final int Drivernme = 2131230750;
        public static final int Drivernmeedit = 2131230751;
        public static final int Driverphone = 2131230752;
        public static final int Email = 2131230947;
        public static final int Fotter = 2131230961;
        public static final int Header = 2131230960;
        public static final int ImageButton01 = 2131230994;
        public static final int ImagePreV = 2131230887;
        public static final int ImageView01 = 2131230874;
        public static final int ImageView02 = 2131230875;
        public static final int ImageView1 = 2131231008;
        public static final int ImageView2 = 2131231009;
        public static final int ImageView3 = 2131231010;
        public static final int ImageView4 = 2131231011;
        public static final int ImageView5 = 2131231012;
        public static final int ImageView6 = 2131231013;
        public static final int ImageView7 = 2131231014;
        public static final int ImageViewLicenseplate = 2131230949;
        public static final int ImageViewaddress = 2131230907;
        public static final int ImageViewbg = 2131230899;
        public static final int ImageViewcameraclick = 2131230957;
        public static final int ImageViewcity = 2131230909;
        public static final int ImageViewcountry = 2131230913;
        public static final int ImageViewemailid = 2131230946;
        public static final int ImageViewfirstname = 2131230935;
        public static final int ImageViewheading = 2131231007;
        public static final int ImageViewinjured = 2131230943;
        public static final int ImageViewinjuredph = 2131230774;
        public static final int ImageViewlastname = 2131230937;
        public static final int ImageViewlicensenumber = 2131230939;
        public static final int ImageViewlightcondition = 2131230919;
        public static final int ImageViewpersonalbg1 = 2131230930;
        public static final int ImageViewpersonalbg2 = 2131230906;
        public static final int ImageViewpersonalbg3 = 2131230916;
        public static final int ImageViewphonenum = 2131230941;
        public static final int ImageViewpolicename = 2131230778;
        public static final int ImageViewpolicenphonenum = 2131230780;
        public static final int ImageViewpolicenreportnum = 2131230782;
        public static final int ImageViewroadcondition = 2131230917;
        public static final int ImageViewstate = 2131230911;
        public static final int ImageViewvehiclemake = 2131230951;
        public static final int ImageViewvehiclemodel = 2131230953;
        public static final int ImageViewweather = 2131230921;
        public static final int Imageviewcallourlawfirm = 2131230739;
        public static final int Imageviewwebsiteicon = 2131230741;
        public static final int Insurancecompany = 2131230756;
        public static final int Insurancecompanyedit = 2131230757;
        public static final int InturerNoedit = 2131230775;
        public static final int Inturerinftxt = 2131230770;
        public static final int Inturernmeedit = 2131230773;
        public static final int Licence = 2131230786;
        public static final int LicencePlateedit = 2131230755;
        public static final int Licenceplate = 2131230754;
        public static final int LinearLayout01 = 2131230722;
        public static final int ListView01 = 2131230736;
        public static final int No = 2131230945;
        public static final int PHnumber = 2131230942;
        public static final int Passanger = 2131230788;
        public static final int Passangeredit12 = 2131230763;
        public static final int PlayBack = 2131230992;
        public static final int PlayTrash = 2131230993;
        public static final int PoliceNoedit = 2131230781;
        public static final int Policenmeedit = 2131230779;
        public static final int Policenmetext = 2131230789;
        public static final int PolicyNoedit = 2131230759;
        public static final int Policynumber = 2131230787;
        public static final int RelativeLayoutinjuredinformation = 2131230771;
        public static final int RelativeLayoutpoliceinformation = 2131230777;
        public static final int RelativeLayoutwitnessinformation = 2131230765;
        public static final int ReportNoedit = 2131230783;
        public static final int ReportNotext = 2131230790;
        public static final int TextView01 = 2131230735;
        public static final int TextView02 = 2131230730;
        public static final int TextView03 = 2131230890;
        public static final int TextView04 = 2131230891;
        public static final int TextView05 = 2131230892;
        public static final int TextView06 = 2131230893;
        public static final int TextView07 = 2131231000;
        public static final int TextView08 = 2131231001;
        public static final int TextView09 = 2131231002;
        public static final int TextView10 = 2131231003;
        public static final int TextView11 = 2131231004;
        public static final int TextView12 = 2131231005;
        public static final int TextViewlocationinformation = 2131230905;
        public static final int TextViewtime = 2131230900;
        public static final int TextViewtimeanddate = 2131230898;
        public static final int TextViewweathercondition = 2131230915;
        public static final int TextViewweathercontributed = 2131230923;
        public static final int Textviewpoliceinformation = 2131230776;
        public static final int Trash = 2131230867;
        public static final int Vehicletxt = 2131230984;
        public static final int VhcleLiceedit = 2131230986;
        public static final int VhcleLicetxt = 2131230985;
        public static final int VhcleMdledit = 2131230990;
        public static final int VhcleMdltxt = 2131230989;
        public static final int Vhclemakeedit = 2131230988;
        public static final int Vhclemaketxt = 2131230987;
        public static final int WitnessPhNoedit = 2131230769;
        public static final int Witnessnmeedit = 2131230767;
        public static final int Yes = 2131230944;
        public static final int ab = 2131230897;
        public static final int activitytext = 2131230802;
        public static final int add = 2131230791;
        public static final int addButton = 2131230877;
        public static final int addrredit = 2131230908;
        public static final int amounttext = 2131230816;
        public static final int attonary = 2131230729;
        public static final int attoneryInformation = 2131230870;
        public static final int back = 2131230721;
        public static final int balloon_inner_layout = 2131230964;
        public static final int balloon_item_snippet = 2131230966;
        public static final int balloon_item_title = 2131230965;
        public static final int balloon_main_layout = 2131230963;
        public static final int btPlay = 2131230996;
        public static final int btRecord = 2131230995;
        public static final int button01 = 2131230881;
        public static final int button02 = 2131230882;
        public static final int button03 = 2131230883;
        public static final int button04 = 2131230884;
        public static final int button05 = 2131230885;
        public static final int button06 = 2131230886;
        public static final int camera = 2131230888;
        public static final int carRentalInformation = 2131230723;
        public static final int carRepairInformation = 2131230871;
        public static final int carrentalinfo = 2131230727;
        public static final int cityedit = 2131230910;
        public static final int clear = 2131230933;
        public static final int clientmattertext = 2131230800;
        public static final int close_img_button = 2131230967;
        public static final int composetext = 2131230796;
        public static final int countryedit = 2131230914;
        public static final int curpos = 2131230904;
        public static final int dateedit = 2131230901;
        public static final int datetext = 2131230798;
        public static final int deleteButton = 2131230999;
        public static final int desctext = 2131230818;
        public static final int done = 2131230744;
        public static final int driver = 2131230826;
        public static final int driver1 = 2131230839;
        public static final int driver12 = 2131230838;
        public static final int driverself = 2131230931;
        public static final int drvab = 2131230747;
        public static final int edit_note = 2131230869;
        public static final int emailSend = 2131230862;
        public static final int emailaddress = 2131230793;
        public static final int emailaddressText = 2131230792;
        public static final int emailsubject = 2131230795;
        public static final int emailsubjectText = 2131230794;
        public static final int feditname = 2131230936;
        public static final int fusrnameedit = 2131230973;
        public static final int gesture_name = 2131230742;
        public static final int gestures = 2131230876;
        public static final int gestures_overlay = 2131230743;
        public static final int header_lbl = 2131230868;
        public static final int help = 2131230880;
        public static final int hourtext = 2131230806;
        public static final int id1 = 2131230958;
        public static final int image1 = 2131230934;
        public static final int imagecam2 = 2131230895;
        public static final int imgview = 2131230720;
        public static final int injuredName = 2131230832;
        public static final int injuredName1 = 2131230854;
        public static final int injuredname12 = 2131230853;
        public static final int injuredphNo = 2131230833;
        public static final int injuredphNo1 = 2131230855;
        public static final int injury = 2131230927;
        public static final int insuranceInformation = 2131230726;
        public static final int insurancecompany12 = 2131230841;
        public static final int insuranceinfo = 2131230728;
        public static final int insuransecomp = 2131230828;
        public static final int insuransecomp1 = 2131230842;
        public static final int label = 2131230745;
        public static final int leditname = 2131230938;
        public static final int licensePlatedit = 2131230950;
        public static final int licenseplate = 2131230809;
        public static final int licenseplate1 = 2131230840;
        public static final int licenseplate12 = 2131230827;
        public static final int licenseplatetext = 2131230808;
        public static final int lightedit = 2131230920;
        public static final int mail = 2131230929;
        public static final int maildesc = 2131230797;
        public static final int make = 2131230811;
        public static final int makeedit = 2131230952;
        public static final int maketext = 2131230810;
        public static final int mapView = 2131230879;
        public static final int model = 2131230813;
        public static final int modeledit = 2131230954;
        public static final int modeltext = 2131230812;
        public static final int myGMap = 2131230959;
        public static final int name = 2131230746;
        public static final int notepad = 2131230865;
        public static final int otherdriverinfo = 2131230748;
        public static final int otherdriverinformationlayout = 2131230749;
        public static final int personalHeader = 2131230932;
        public static final int phonenumber12 = 2131230851;
        public static final int physicianInformation = 2131230872;
        public static final int pinpointeditdit = 2131230924;
        public static final int policeName = 2131230834;
        public static final int policeName1 = 2131230857;
        public static final int policephNo = 2131230835;
        public static final int policephNo1 = 2131230859;
        public static final int policyname12 = 2131230856;
        public static final int policyno = 2131230829;
        public static final int policyno1 = 2131230844;
        public static final int policyno12 = 2131230843;
        public static final int policynumber = 2131230758;
        public static final int policyphonenumber12 = 2131230858;
        public static final int ratetext = 2131230814;
        public static final int recorder = 2131230991;
        public static final int reloadButton = 2131230878;
        public static final int reportNumber = 2131230836;
        public static final int reportNumber1 = 2131230861;
        public static final int reportnumber12 = 2131230860;
        public static final int roadedit = 2131230918;
        public static final int settingbtn = 2131230968;
        public static final int stateedit = 2131230912;
        public static final int stop = 2131230997;
        public static final int surface_camera = 2131230956;
        public static final int temptext = 2131230784;
        public static final int text1 = 2131230998;
        public static final int textView = 2131230737;
        public static final int textView01 = 2131230738;
        public static final int textView02 = 2131230740;
        public static final int textview = 2131230837;
        public static final int textview01 = 2131230889;
        public static final int timeedit = 2131230903;
        public static final int timekeepertext = 2131230804;
        public static final int timetext = 2131230902;
        public static final int towingInformation = 2131230724;
        public static final int towinginfo = 2131230725;
        public static final int tt = 2131230971;
        public static final int txtaddress = 2131230819;
        public static final int txtcity = 2131230820;
        public static final int txtcountry = 2131230821;
        public static final int txtdate = 2131230815;
        public static final int txtdlno = 2131230803;
        public static final int txtemailid = 2131230807;
        public static final int txtfirst = 2131230799;
        public static final int txtlast = 2131230801;
        public static final int txtlightcond = 2131230824;
        public static final int txtphoneno = 2131230805;
        public static final int txtroadsurf = 2131230823;
        public static final int txtstate = 2131230822;
        public static final int txttime = 2131230817;
        public static final int unknown = 2131230925;
        public static final int userinftxt = 2131230785;
        public static final int usrCntxt = 2131230976;
        public static final int usrContatctedit = 2131230977;
        public static final int usrDrvedit = 2131230981;
        public static final int usrDrvtxt = 2131230980;
        public static final int usrEmaledit = 2131230979;
        public static final int usrEmaltxt = 2131230978;
        public static final int usrfnametxt = 2131230972;
        public static final int usrlnameedit = 2131230975;
        public static final int usrlnametxt = 2131230974;
        public static final int usrstateedit = 2131230983;
        public static final int usrstatetxt = 2131230982;
        public static final int vehicleInfo = 2131230948;
        public static final int vehicleMakeedit = 2131230761;
        public static final int vehiclemake = 2131230760;
        public static final int vehiclemake1 = 2131230846;
        public static final int vehiclemake12 = 2131230845;
        public static final int vehiclemodel = 2131230762;
        public static final int vehiclemodel1 = 2131230848;
        public static final int vehiclemodel12 = 2131230847;
        public static final int weathercond = 2131230825;
        public static final int weatheredit = 2131230922;
        public static final int widget28 = 2131231006;
        public static final int widget54 = 2131230896;
        public static final int witness = 2131230926;
        public static final int witnessName = 2131230830;
        public static final int witnessName1 = 2131230850;
        public static final int witnessinftxt = 2131230764;
        public static final int witnessname12 = 2131230849;
        public static final int witnessphNo = 2131230831;
        public static final int witnessphNo1 = 2131230852;
        public static final int witnessphone = 2131230768;
        public static final int witnesstext = 2131230766;
        public static final int xrayInformation = 2131230873;
        public static final int zoom = 2131230962;
    }

    public static final class layout {
        public static final int aboutus = 2130903040;
        public static final int accident = 2130903041;
        public static final int attoneryinfo = 2130903042;
        public static final int audiomainn = 2130903043;
        public static final int camreclist = 2130903044;
        public static final int contact = 2130903045;
        public static final int create_gesture = 2130903046;
        public static final int dialog_rename = 2130903047;
        public static final int driver = 2130903048;
        public static final int driver1 = 2130903049;
        public static final int email = 2130903050;
        public static final int emergency = 2130903051;
        public static final int existnotes = 2130903052;
        public static final int faq = 2130903053;
        public static final int fourthtablisttext = 2130903054;
        public static final int gestmain = 2130903055;
        public static final int gestures_item = 2130903056;
        public static final int gestures_list = 2130903057;
        public static final int google_map = 2130903058;
        public static final int help = 2130903059;
        public static final int home = 2130903060;
        public static final int imageprev = 2130903061;
        public static final int inro = 2130903062;
        public static final int listelements = 2130903063;
        public static final int location = 2130903064;
        public static final int main = 2130903065;
        public static final int main1 = 2130903066;
        public static final int maincam = 2130903067;
        public static final int mainrec = 2130903068;
        public static final int map = 2130903069;
        public static final int map_overlay = 2130903070;
        public static final int nofee = 2130903071;
        public static final int note_edit = 2130903072;
        public static final int notelist = 2130903073;
        public static final int personal = 2130903074;
        public static final int playrec = 2130903075;
        public static final int record = 2130903076;
        public static final int record_item = 2130903077;
        public static final int recordlist = 2130903078;
        public static final int recordlisttext = 2130903079;
        public static final int recordlistview = 2130903080;
        public static final int report_list = 2130903081;
        public static final int splash = 2130903082;
        public static final int stastics = 2130903083;
        public static final int users_list = 2130903084;
        public static final int whattodo = 2130903085;
    }

    public static final class raw {
        public static final int actions = 2130968576;
    }

    public static final class string {
        public static final int add = 2131165191;
        public static final int app_name = 2131165184;
        public static final int button_add = 2131165208;
        public static final int button_discard = 2131165210;
        public static final int button_done = 2131165211;
        public static final int button_reload = 2131165209;
        public static final int cancel = 2131165188;
        public static final int cancel_action = 2131165216;
        public static final int close = 2131165193;
        public static final int date = 2131165199;
        public static final int delete = 2131165192;
        public static final int edit = 2131165190;
        public static final int email = 2131165196;
        public static final int empty = 2131165186;
        public static final int error_missing_name = 2131165213;
        public static final int gestures_activity = 2131165219;
        public static final int gestures_delete = 2131165221;
        public static final int gestures_delete_success = 2131165222;
        public static final int gestures_empty = 2131165218;
        public static final int gestures_error_loading = 2131165225;
        public static final int gestures_loading = 2131165217;
        public static final int gestures_rename = 2131165220;
        public static final int gestures_rename_label = 2131165224;
        public static final int gestures_rename_title = 2131165223;
        public static final int help = 2131165194;
        public static final int index = 2131165206;
        public static final int label_create_gesture = 2131165207;
        public static final int notes = 2131165187;
        public static final int pc = 2131165198;
        public static final int play = 2131165205;
        public static final int pn = 2131165197;
        public static final int prompt_gesture_name = 2131165212;
        public static final int record = 2131165204;
        public static final int reg_ex = 2131165185;
        public static final int regex_email = 2131165202;
        public static final int regex_phone = 2131165203;
        public static final int regex_req = 2131165201;
        public static final int rename_action = 2131165215;
        public static final int save = 2131165189;
        public static final int save_success = 2131165214;
        public static final int settings = 2131165195;
        public static final int time = 2131165200;
    }
}
