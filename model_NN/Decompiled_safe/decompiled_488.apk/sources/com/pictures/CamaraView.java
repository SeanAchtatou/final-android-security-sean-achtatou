package com.pictures;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import com.THOMSONROGERS.R;
import com.tritone.DBDriod;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class CamaraView extends Activity implements SurfaceHolder.Callback, View.OnClickListener {
    static final int FOTO_MODE = 0;
    private static final String TAG = "CameraTest";
    int counter = FOTO_MODE;
    DBDriod droid;
    Camera mCamera;
    /* access modifiers changed from: private */
    public Context mContext = this;
    Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] imageData, Camera c) {
            if (imageData != null) {
                Intent mIntent = new Intent();
                CamaraView.this.StoreByteImage(CamaraView.this.mContext, imageData, 50, "ImageName");
                CamaraView.this.mCamera.startPreview();
                CamaraView.this.setResult(CamaraView.FOTO_MODE, mIntent);
            }
        }
    };
    boolean mPreviewRunning = false;
    private SurfaceHolder mSurfaceHolder;
    private SurfaceView mSurfaceView;
    int m_rotation = FOTO_MODE;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        Log.e(TAG, "onCreate");
        this.droid = new DBDriod(this);
        Bundle extras = getIntent().getExtras();
        getWindow().setFormat(-3);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.maincam);
        this.mSurfaceView = (SurfaceView) findViewById(R.id.surface_camera);
        this.mSurfaceView.setOnClickListener(this);
        this.mSurfaceHolder = this.mSurfaceView.getHolder();
        this.mSurfaceHolder.addCallback(this);
        this.mSurfaceHolder.setType(3);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.mCamera = Camera.open();
        if (this.mCamera == null) {
            this.mCamera.setParameters(this.mCamera.getParameters());
        }
        System.out.println(" surface created");
        try {
            this.mCamera.setPreviewDisplay(holder);
            this.mCamera.setPreviewCallback(new Camera.PreviewCallback() {
                public void onPreviewFrame(byte[] data, Camera arg1) {
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (this.mCamera != null) {
            this.mPreviewRunning = false;
            this.mCamera.stopPreview();
            this.mCamera.setPreviewCallback(null);
            this.mCamera.release();
            this.mCamera = null;
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        if (this.mPreviewRunning) {
            this.mCamera.stopPreview();
        }
        System.out.println("surface changed");
        Camera.Parameters parameters = this.mCamera.getParameters();
        parameters.setPictureFormat(256);
        this.mCamera.setParameters(parameters);
        try {
            this.mCamera.setPreviewDisplay(holder);
            this.mCamera.startPreview();
            this.mPreviewRunning = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onClick(View arg0) {
        Camera.Parameters parameters = this.mCamera.getParameters();
        int nCamRotate = 90;
        if ((this.m_rotation > 45 && this.m_rotation < 135) || (this.m_rotation > 225 && this.m_rotation < 315)) {
            nCamRotate = FOTO_MODE;
        }
        parameters.set("rotation", nCamRotate);
        this.mCamera.setParameters(parameters);
        this.mCamera.takePicture(null, this.mPictureCallback, this.mPictureCallback);
    }

    public boolean StoreByteImage(Context mContext2, byte[] imageData, int quality, String expName) {
        IOException e;
        FileNotFoundException e2;
        File file = new File("/sdcard");
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 3;
            Bitmap myImage = BitmapFactory.decodeByteArray(imageData, FOTO_MODE, imageData.length, options);
            this.counter = this.counter + 1;
            SharedPreferences myPrefs = getSharedPreferences("myPrefs", 1);
            String time = myPrefs.getString("time", "time");
            String date = myPrefs.getString("date", "date");
            Calendar cal = Calendar.getInstance();
            String temp = new StringBuilder(String.valueOf(cal.get(10) + cal.get(12) + cal.get(13))).toString();
            FileOutputStream fileOutputStream = new FileOutputStream(String.valueOf(file.toString()) + "/driver" + date + temp + this.counter + ".jpg");
            try {
                this.droid.createimage(time, date, "driver" + date + temp + this.counter + ".jpg");
                BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);
                myImage.compress(Bitmap.CompressFormat.JPEG, quality, bos);
                bos.flush();
                bos.close();
                return true;
            } catch (FileNotFoundException e3) {
                e2 = e3;
                e2.printStackTrace();
                return true;
            } catch (IOException e4) {
                e = e4;
                e.printStackTrace();
                return true;
            }
        } catch (FileNotFoundException e5) {
            e2 = e5;
            e2.printStackTrace();
            return true;
        } catch (IOException e6) {
            e = e6;
            e.printStackTrace();
            return true;
        }
    }
}
