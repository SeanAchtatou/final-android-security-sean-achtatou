package com.pictures;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.THOMSONROGERS.R;
import com.tritone.DBDriod;
import java.io.File;

public class Image_Preview extends Activity {
    DBDriod droid;
    String image = "";
    String uri_text = "";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.imageprev);
        final SharedPreferences.Editor prefsEditor = getSharedPreferences("myPrefs", 1).edit();
        prefsEditor.putString("opertion", "");
        prefsEditor.putString("module", "");
        prefsEditor.commit();
        this.droid = new DBDriod(this);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            this.uri_text = b.getString("uri");
            this.image = b.getString("image");
        }
        ((ImageView) findViewById(R.id.ImagePreV)).setImageURI(Uri.parse(this.uri_text));
        ((Button) findViewById(R.id.Trash)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Image_Preview.this.droid.deleteImage(Image_Preview.this.image);
                if (new File(Image_Preview.this.uri_text).delete()) {
                    System.out.println("File Deleted");
                }
                prefsEditor.putString("opertion", "delete");
                prefsEditor.putString("module", "camera");
                prefsEditor.commit();
                Intent data = new Intent();
                data.putExtra("opertion", "delete");
                Image_Preview.this.setResult(-1, data);
                Image_Preview.this.finish();
            }
        });
        ((Button) findViewById(R.id.BackButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Image_Preview.this.finish();
            }
        });
    }
}
