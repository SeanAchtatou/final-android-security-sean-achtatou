package com.mapp;

import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import com.THOMSONROGERS.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import java.lang.reflect.Method;
import java.util.List;

public abstract class CustomItemizedOverlay<Item> extends ItemizedOverlay<OverlayItem> {
    public static List<Overlay> mapOverlays;
    public static Boolean visible = false;
    private View clickRegion;
    private MapView mapView;
    final MapController mc;
    public CustomOverlayView toastView;
    private int viewOffset = 0;

    public CustomItemizedOverlay(Drawable defaultMarker, MapView mapView2) {
        super(defaultMarker);
        this.mapView = mapView2;
        this.mc = mapView2.getController();
    }

    public void setToastBottomOffset(int pixels) {
        this.viewOffset = pixels;
    }

    /* access modifiers changed from: protected */
    public boolean onToastTap(int index) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean onTap(int index) {
        boolean isRecycled;
        int thisIndex = index;
        GeoPoint point = createItem(index).getPoint();
        if (this.toastView == null) {
            this.toastView = new CustomOverlayView(this.mapView.getContext(), this.viewOffset);
            this.clickRegion = this.toastView.findViewById(R.id.balloon_inner_layout);
            isRecycled = false;
        } else {
            isRecycled = true;
        }
        this.toastView.setVisibility(8);
        mapOverlays = this.mapView.getOverlays();
        if (mapOverlays.size() > 1) {
            hideOtherToasts(mapOverlays);
        }
        this.toastView.setData(createItem(index));
        MapView.LayoutParams params = new MapView.LayoutParams(-2, -2, point, 81);
        params.mode = 0;
        params.y = -40;
        setToastTouchListener(thisIndex);
        this.toastView.setVisibility(0);
        if (isRecycled) {
            this.toastView.setLayoutParams(params);
        } else {
            this.mapView.addView(this.toastView, params);
        }
        return true;
    }

    public void hideToast() {
        if (this.toastView != null) {
            this.toastView.setVisibility(8);
        }
    }

    public void hideOtherToasts(List<Overlay> overlays) {
        for (Overlay overlay : overlays) {
            if ((overlay instanceof CustomItemizedOverlay) && overlay != this) {
                ((CustomItemizedOverlay) overlay).hideToast();
            }
        }
    }

    private void setToastTouchListener(final int thisIndex) {
        try {
            Method declaredMethod = getClass().getDeclaredMethod("onBalloonTap", Integer.TYPE);
            this.clickRegion.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    Drawable d = ((View) v.getParent()).findViewById(R.id.balloon_main_layout).getBackground();
                    if (event.getAction() == 0) {
                        if (d.setState(new int[]{16842919})) {
                            d.invalidateSelf();
                        }
                        return true;
                    } else if (event.getAction() != 1) {
                        return false;
                    } else {
                        if (d.setState(new int[0])) {
                            d.invalidateSelf();
                        }
                        CustomItemizedOverlay.this.onToastTap(thisIndex);
                        return true;
                    }
                }
            });
        } catch (SecurityException e) {
        } catch (NoSuchMethodException e2) {
        }
    }
}
