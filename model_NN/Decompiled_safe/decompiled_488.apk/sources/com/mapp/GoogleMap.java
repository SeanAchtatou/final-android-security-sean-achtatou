package com.mapp;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GoogleMap extends MapActivity {
    public static List<Overlay> listOfOverlays;
    Bitmap currentLocationImage;
    double fromLatitude;
    double fromLongitude;
    ArrayList<Double> latitude;
    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (location != null) {
                GoogleMap.this.fromLatitude = location.getLatitude();
                GoogleMap.this.fromLongitude = location.getLongitude();
            }
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    LocationManager locationManager;
    ArrayList<Double> longitude;
    MapController mapController;
    MapView mapView;
    int maxlat = -81000000;
    int maxlong = -181000000;
    int minlat = 81000000;
    int minlong = 181000000;
    GeoPoint moveTo;
    String search = "";
    ArrayList<String> snippet;
    ArrayList<String> title;

    /* JADX WARN: Type inference failed for: r8v0, types: [android.content.Context, com.mapp.GoogleMap, com.google.android.maps.MapActivity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onCreate(android.os.Bundle r9) {
        /*
            r8 = this;
            r7 = 1
            r5 = 0
            com.mapp.GoogleMap.super.onCreate(r9)
            r3 = 2130903058(0x7f030012, float:1.7412923E38)
            r8.setContentView(r3)
            android.content.res.Resources r3 = r8.getResources()
            r4 = 2130837638(0x7f020086, float:1.7280236E38)
            android.graphics.Bitmap r3 = android.graphics.BitmapFactory.decodeResource(r3, r4)
            r8.currentLocationImage = r3
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r8.latitude = r3
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r8.longitude = r3
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r8.title = r3
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r8.snippet = r3
            android.content.Intent r3 = r8.getIntent()
            android.os.Bundle r0 = r3.getExtras()
            if (r0 == 0) goto L_0x0047
            java.lang.String r3 = "search"
            java.lang.String r3 = r0.getString(r3)
            r8.search = r3
        L_0x0047:
            r3 = 2131230879(0x7f08009f, float:1.8077823E38)
            android.view.View r3 = r8.findViewById(r3)
            com.google.android.maps.MapView r3 = (com.google.android.maps.MapView) r3
            r8.mapView = r3
            com.google.android.maps.MapView r3 = r8.mapView
            r3.setBuiltInZoomControls(r7)
            com.google.android.maps.MapView r3 = r8.mapView
            com.google.android.maps.MapController r3 = r3.getController()
            r8.mapController = r3
            r8.gpsInitialize()
            boolean r3 = r8.IsNetworkAvailable()
            if (r3 == 0) goto L_0x00b7
            java.lang.String r1 = "32.953,-96.89"
            double r3 = r8.fromLatitude
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 == 0) goto L_0x00b6
            double r3 = r8.fromLongitude
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 == 0) goto L_0x00b6
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            double r4 = r8.fromLatitude
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r3.<init>(r4)
            java.lang.String r4 = ","
            java.lang.StringBuilder r3 = r3.append(r4)
            double r4 = r8.fromLongitude
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r1 = r3.toString()
            java.lang.String r3 = r8.search
            r8.fetchCoordinates(r3, r1)
            com.mapp.GoogleMap$MapOverlay r2 = new com.mapp.GoogleMap$MapOverlay
            com.google.android.maps.MapView r3 = r8.mapView
            android.graphics.Bitmap r4 = r8.currentLocationImage
            r2.<init>(r3, r4)
            com.google.android.maps.MapView r3 = r8.mapView
            java.util.List r3 = r3.getOverlays()
            com.mapp.GoogleMap.listOfOverlays = r3
            java.util.List<com.google.android.maps.Overlay> r3 = com.mapp.GoogleMap.listOfOverlays
            r3.clear()
            java.util.List<com.google.android.maps.Overlay> r3 = com.mapp.GoogleMap.listOfOverlays
            r3.add(r2)
            com.google.android.maps.MapView r3 = r8.mapView
            r3.invalidate()
        L_0x00b6:
            return
        L_0x00b7:
            java.lang.String r3 = "No Wifi/Internet Connection Available."
            android.widget.Toast r3 = android.widget.Toast.makeText(r8, r3, r7)
            r3.show()
            goto L_0x00b6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapp.GoogleMap.onCreate(android.os.Bundle):void");
    }

    /* JADX WARN: Type inference failed for: r9v0, types: [android.content.Context, com.mapp.GoogleMap] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void gpsInitialize() {
        /*
            r9 = this;
            r2 = 1000(0x3e8, double:4.94E-321)
            r4 = 1140457472(0x43fa0000, float:500.0)
            r7 = 0
            java.lang.String r0 = "location"
            java.lang.Object r0 = r9.getSystemService(r0)
            android.location.LocationManager r0 = (android.location.LocationManager) r0
            r9.locationManager = r0
            java.lang.String r1 = "gps"
            android.location.LocationManager r0 = r9.locationManager
            java.lang.String r5 = "gps"
            boolean r0 = r0.isProviderEnabled(r5)
            if (r0 != 0) goto L_0x001e
            java.lang.String r1 = "network"
        L_0x001e:
            android.location.LocationManager r0 = r9.locationManager
            android.location.LocationListener r5 = r9.locationListener
            r0.requestLocationUpdates(r1, r2, r4, r5)
            android.location.LocationManager r0 = r9.locationManager
            android.location.Location r6 = r0.getLastKnownLocation(r1)
            if (r6 == 0) goto L_0x0050
            double r2 = r6.getLatitude()
            r9.fromLatitude = r2
            double r2 = r6.getLongitude()
            r9.fromLongitude = r2
        L_0x0039:
            double r2 = r9.fromLatitude
            int r0 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r0 != 0) goto L_0x004f
            double r2 = r9.fromLongitude
            int r0 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r0 != 0) goto L_0x004f
            java.lang.String r0 = "No GPS Service Available."
            r2 = 1
            android.widget.Toast r0 = android.widget.Toast.makeText(r9, r0, r2)
            r0.show()
        L_0x004f:
            return
        L_0x0050:
            java.lang.String r1 = "network"
            android.location.LocationManager r0 = r9.locationManager
            android.location.LocationListener r5 = r9.locationListener
            r0.requestLocationUpdates(r1, r2, r4, r5)
            android.location.LocationManager r0 = r9.locationManager
            android.location.Location r6 = r0.getLastKnownLocation(r1)
            if (r6 == 0) goto L_0x0039
            double r2 = r6.getLatitude()
            r9.fromLatitude = r2
            double r2 = r6.getLongitude()
            r9.fromLongitude = r2
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapp.GoogleMap.gpsInitialize():void");
    }

    public boolean IsNetworkAvailable() {
        ConnectivityManager connMgr = (ConnectivityManager) getApplicationContext().getSystemService("connectivity");
        if (connMgr == null) {
            return false;
        }
        try {
            if (connMgr.getActiveNetworkInfo() != null && connMgr.getActiveNetworkInfo().isAvailable() && connMgr.getActiveNetworkInfo().isConnected()) {
                return true;
            }
            Log.v("connection", "Internet Connection Not Present");
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public void fetchCoordinates(String search2, String coordinates) {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(new URL("http://www.google.com/uds/GlocalSearch?key=ABQIAAAAzr2EBOXUKnm_jVnk0OJI7xSosDVG8KKPE1-m51RBrvYughuyMxQ-i1QfUnH94QxWIa6N4U6MouMmBA&v=1.0&rsz=large&sll=" + coordinates + "&sspn=0.284718,0.343323&q=" + search2).openConnection().getInputStream()));
            while (true) {
                String line = in.readLine();
                if (line != null) {
                    JSONArray menuitemArray = new JSONObject(line).getJSONObject("responseData").getJSONArray("results");
                    for (int i = 0; i < menuitemArray.length(); i++) {
                        this.latitude.add(Double.valueOf(Double.parseDouble(menuitemArray.getJSONObject(i).getString("lat").toString())));
                        this.longitude.add(Double.valueOf(Double.parseDouble(menuitemArray.getJSONObject(i).getString("lng").toString())));
                        String[] data = menuitemArray.getJSONObject(i).getString("addressLines").toString().split("\"");
                        String snippetData = "";
                        for (int j = 0; j < data.length; j++) {
                            if (data[j].length() > 1) {
                                snippetData = String.valueOf(snippetData) + data[j];
                            }
                        }
                        this.title.add(menuitemArray.getJSONObject(i).getString("title").toString().replaceAll("\\<.*?>", ""));
                        this.snippet.add(snippetData);
                    }
                } else {
                    return;
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (JSONException e3) {
            e3.printStackTrace();
        }
    }

    class MapOverlay extends Overlay {
        ArrayList<Integer> X;
        ArrayList<Integer> Y;
        Bitmap bitmap;
        List<GeoPoint> mapPoints = new ArrayList();

        MapOverlay(MapView mapView, Bitmap image) {
            int i;
            this.bitmap = image;
            if (GoogleMap.this.latitude.size() > 0) {
                for (int i2 = 0; i2 < GoogleMap.this.latitude.size(); i2++) {
                    this.mapPoints.add(new GeoPoint((int) (GoogleMap.this.latitude.get(i2).doubleValue() * 1000000.0d), (int) (GoogleMap.this.longitude.get(i2).doubleValue() * 1000000.0d)));
                }
                GoogleMap.this.moveTo = new GeoPoint(this.mapPoints.get(0).getLatitudeE6() + ((this.mapPoints.get(this.mapPoints.size() - 1).getLatitudeE6() - this.mapPoints.get(0).getLatitudeE6()) / 2), this.mapPoints.get(0).getLongitudeE6() + ((this.mapPoints.get(this.mapPoints.size() - 1).getLongitudeE6() - this.mapPoints.get(0).getLongitudeE6()) / 2));
                GoogleMap.this.maxlat = this.mapPoints.get(0).getLatitudeE6();
                GoogleMap.this.minlat = this.mapPoints.get(0).getLatitudeE6();
                GoogleMap.this.maxlong = this.mapPoints.get(0).getLongitudeE6();
                GoogleMap.this.minlong = this.mapPoints.get(0).getLongitudeE6();
                for (int i3 = 0; i3 < this.mapPoints.size(); i3++) {
                    GoogleMap.this.minlat = GoogleMap.this.minlat > this.mapPoints.get(i3).getLatitudeE6() ? this.mapPoints.get(i3).getLatitudeE6() : GoogleMap.this.minlat;
                    GoogleMap.this.maxlat = GoogleMap.this.maxlat < this.mapPoints.get(i3).getLatitudeE6() ? this.mapPoints.get(i3).getLatitudeE6() : GoogleMap.this.maxlat;
                    GoogleMap.this.minlong = GoogleMap.this.minlong > this.mapPoints.get(i3).getLongitudeE6() ? this.mapPoints.get(i3).getLongitudeE6() : GoogleMap.this.minlong;
                    if (GoogleMap.this.maxlong < this.mapPoints.get(i3).getLongitudeE6()) {
                        i = this.mapPoints.get(i3).getLongitudeE6();
                    } else {
                        i = GoogleMap.this.maxlong;
                    }
                    GoogleMap.this.maxlong = i;
                }
                if (this.mapPoints.size() > 1) {
                    GoogleMap.this.mapController.zoomToSpan(Math.abs(GoogleMap.this.maxlat - GoogleMap.this.minlat), Math.abs(GoogleMap.this.maxlong - GoogleMap.this.minlong));
                    GoogleMap.this.mapController.animateTo(GoogleMap.this.moveTo);
                }
            }
        }

        public boolean onTouchEvent(MotionEvent event, MapView mapView) {
            if (event.getAction() != 1) {
                return false;
            }
            for (int i = 0; i < this.X.size(); i++) {
                if (((int) event.getX()) > this.X.get(i).intValue() && ((int) event.getX()) < this.X.get(i).intValue() + this.bitmap.getWidth() && ((int) event.getY()) > this.Y.get(i).intValue() && ((int) event.getY()) < this.Y.get(i).intValue() + this.bitmap.getHeight()) {
                    System.out.println("enter");
                    ItemizedToast itemizedOverlay = new ItemizedToast(new Drawable() {
                        public void setColorFilter(ColorFilter cf) {
                        }

                        public void setAlpha(int alpha) {
                        }

                        public int getOpacity() {
                            return 0;
                        }

                        public void draw(Canvas canvas) {
                        }
                    }, mapView);
                    itemizedOverlay.addOverlay(new OverlayItem(new GeoPoint((int) (GoogleMap.this.latitude.get(i).doubleValue() * 1000000.0d), (int) (GoogleMap.this.longitude.get(i).doubleValue() * 1000000.0d)), GoogleMap.this.title.get(i), GoogleMap.this.snippet.get(i)));
                    GoogleMap.listOfOverlays.add(itemizedOverlay);
                    System.out.println(" click: " + i + " x: " + event.getX() + " ix: " + this.X.get(i));
                }
            }
            return false;
        }

        public void draw(Canvas canvas, MapView mapView, boolean shadow) {
            drawPath(mapView, canvas);
        }

        public void drawPath(MapView mv, Canvas canvas) {
            this.X = new ArrayList<>();
            this.Y = new ArrayList<>();
            Paint paint = new Paint();
            paint.setColor(-65536);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(5.0f);
            for (int i = 0; i < this.mapPoints.size(); i++) {
                Point point = new Point();
                mv.getProjection().toPixels(this.mapPoints.get(i), point);
                int x2 = point.x;
                int y2 = point.y;
                int greenX = x2 - (this.bitmap.getWidth() / 2);
                int greenY = (y2 - this.bitmap.getHeight()) + 2;
                this.X.add(Integer.valueOf(greenX));
                this.Y.add(Integer.valueOf(greenY));
                canvas.drawBitmap(this.bitmap, (float) (x2 - (this.bitmap.getWidth() / 2)), (float) ((y2 - this.bitmap.getHeight()) + 6), (Paint) null);
                canvas.drawPoint((float) x2, (float) y2, paint);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }
}
