package com.mapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.Toast;
import com.THOMSONROGERS.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GPS extends MapActivity {
    Button b1;
    /* access modifiers changed from: private */
    public int firstLocation = 0;
    GeoPoint geoPoint;
    /* access modifiers changed from: private */
    public int gpsActive = 0;
    String gpsProvider;
    double latitude = 12.937875d;
    private LocationManager lm;
    Location loc;
    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            GPS.this.pbar.dismiss();
            GPS.this.providerName = location.getProvider();
            if (GPS.this.firstLocation == 0) {
                if (GPS.this.providerName.toString().equals("gps")) {
                    GPS.this.gpsActive = 1;
                }
                GPS.this.firstLocation = 1;
                GPS.this.mapView.displayZoomControls(true);
                GPS.this.geoPoint = new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d));
                GPS.this.mc.animateTo(GPS.this.geoPoint);
                GPS.this.mc.setZoom(16);
                System.out.println(" animated 1" + location.getLatitude() + " : " + location.getLongitude());
                MapOverlay mapOverlay = new MapOverlay(GPS.this.geoPoint);
                List<Overlay> listOfOverlays = GPS.this.mapView.getOverlays();
                listOfOverlays.clear();
                listOfOverlays.add(mapOverlay);
                GPS.this.mapView.invalidate();
            }
            if (GPS.this.gpsActive == 0 && GPS.this.providerName.toString().equals("gps")) {
                GPS.this.gpsActive = 1;
                GPS.this.mapView.displayZoomControls(true);
                GPS.this.geoPoint = new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d));
                GPS.this.mc.animateTo(GPS.this.geoPoint);
                GPS.this.mc.setZoom(16);
                System.out.println(" animated 2" + location.getLatitude() + " : " + location.getLongitude());
                MapOverlay mapOverlay2 = new MapOverlay(GPS.this.geoPoint);
                List<Overlay> listOfOverlays2 = GPS.this.mapView.getOverlays();
                listOfOverlays2.clear();
                listOfOverlays2.add(mapOverlay2);
                GPS.this.mapView.invalidate();
            }
            if (GPS.this.gpsActive == 1 && GPS.this.providerName.toString().equals("gps")) {
                GPS.this.mapView.displayZoomControls(true);
                GPS.this.geoPoint = new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d));
                GPS.this.mc.animateTo(GPS.this.geoPoint);
                GPS.this.mc.setZoom(16);
                System.out.println(" animated 3" + location.getLatitude() + " : " + location.getLongitude());
                MapOverlay mapOverlay3 = new MapOverlay(GPS.this.geoPoint);
                List<Overlay> listOfOverlays3 = GPS.this.mapView.getOverlays();
                listOfOverlays3.clear();
                listOfOverlays3.add(mapOverlay3);
                GPS.this.mapView.invalidate();
            }
            if (GPS.this.gpsActive == 0 && GPS.this.providerName.toString().equals("network")) {
                GPS.this.mapView.displayZoomControls(true);
                GPS.this.geoPoint = new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d));
                GPS.this.mc.animateTo(GPS.this.geoPoint);
                GPS.this.mc.setZoom(16);
                System.out.println(" animated 4" + location.getLatitude() + " : " + location.getLongitude());
                MapOverlay mapOverlay4 = new MapOverlay(GPS.this.geoPoint);
                List<Overlay> listOfOverlays4 = GPS.this.mapView.getOverlays();
                listOfOverlays4.clear();
                listOfOverlays4.add(mapOverlay4);
                GPS.this.mapView.invalidate();
            }
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    double longitude = 77.622313d;
    /* access modifiers changed from: private */
    public MapView mapView;
    /* access modifiers changed from: private */
    public MapController mc;
    MapView myMapView;
    String networkProvider;
    ProgressDialog pbar;
    String providerName;
    String[] s1 = new String[3];
    StringBuilder sb;

    /* JADX WARN: Type inference failed for: r14v0, types: [android.content.Context, com.mapp.GPS, com.google.android.maps.MapActivity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onCreate(android.os.Bundle r15) {
        /*
            r14 = this;
            r12 = 10
            r1 = 1024(0x400, float:1.435E-42)
            r2 = 1
            r11 = -2
            r10 = 1092616192(0x41200000, float:10.0)
            com.mapp.GPS.super.onCreate(r15)
            android.view.Window r0 = r14.getWindow()
            r0.setFlags(r1, r1)
            r14.requestWindowFeature(r2)
            r0 = 2130903069(0x7f03001d, float:1.7412946E38)
            r14.setContentView(r0)
            r0 = 2131230732(0x7f08000c, float:1.8077525E38)
            android.view.View r0 = r14.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r14.b1 = r0
            android.app.ProgressDialog r0 = new android.app.ProgressDialog
            r0.<init>(r14)
            r14.pbar = r0
            java.lang.String r0 = ""
            java.lang.String r1 = " Loading. Please wait ... "
            android.app.ProgressDialog r0 = android.app.ProgressDialog.show(r14, r0, r1, r2)
            r14.pbar = r0
            java.lang.String r0 = "location"
            java.lang.Object r0 = r14.getSystemService(r0)
            android.location.LocationManager r0 = (android.location.LocationManager) r0
            r14.lm = r0
            android.location.LocationManager r0 = r14.lm
            java.lang.String r1 = "gps"
            r2 = 0
            r4 = 0
            android.location.LocationListener r5 = r14.locationListener
            r0.requestLocationUpdates(r1, r2, r4, r5)
            java.lang.String r0 = "gps"
            r14.gpsProvider = r0
            android.location.LocationManager r0 = r14.lm
            java.lang.String r1 = r14.gpsProvider
            android.location.LocationListener r5 = r14.locationListener
            r2 = r12
            r4 = r10
            r0.requestLocationUpdates(r1, r2, r4, r5)
            java.lang.String r0 = "network"
            r14.networkProvider = r0
            android.location.LocationManager r0 = r14.lm
            java.lang.String r1 = r14.networkProvider
            android.location.LocationListener r5 = r14.locationListener
            r2 = r12
            r4 = r10
            r0.requestLocationUpdates(r1, r2, r4, r5)
            r0 = 2131230959(0x7f0800ef, float:1.8077985E38)
            android.view.View r0 = r14.findViewById(r0)
            com.google.android.maps.MapView r0 = (com.google.android.maps.MapView) r0
            r14.mapView = r0
            r0 = 2131230962(0x7f0800f2, float:1.8077992E38)
            android.view.View r8 = r14.findViewById(r0)
            android.widget.LinearLayout r8 = (android.widget.LinearLayout) r8
            com.google.android.maps.MapView r0 = r14.mapView
            android.view.View r9 = r0.getZoomControls()
            android.view.ViewGroup$LayoutParams r0 = new android.view.ViewGroup$LayoutParams
            r0.<init>(r11, r11)
            r8.addView(r9, r0)
            com.google.android.maps.MapView r0 = r14.mapView
            r1 = 0
            r0.displayZoomControls(r1)
            com.google.android.maps.MapView r0 = r14.mapView
            com.google.android.maps.MapController r0 = r0.getController()
            r14.mc = r0
            r0 = 4720291523659300864(0x4181d821b0000000, double:3.7422134E7)
            java.lang.Double r6 = java.lang.Double.valueOf(r0)
            r0 = -4495406704187408384(0xc19d1b6b94000000, double:-1.22084069E8)
            java.lang.Double r7 = java.lang.Double.valueOf(r0)
            com.google.android.maps.GeoPoint r0 = new com.google.android.maps.GeoPoint
            int r1 = r6.intValue()
            int r2 = r7.intValue()
            r0.<init>(r1, r2)
            r14.geoPoint = r0
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.String r1 = "MyLocation"
            r0.println(r1)
            android.widget.Button r0 = r14.b1
            com.mapp.GPS$2 r1 = new com.mapp.GPS$2
            r1.<init>()
            r0.setOnClickListener(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapp.GPS.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    class MapOverlay extends Overlay {
        Bitmap bmp;
        GeoPoint gp1;

        public MapOverlay(GeoPoint gp12) {
            GPS.this.pbar.dismiss();
            this.gp1 = gp12;
            this.bmp = BitmapFactory.decodeResource(GPS.this.getResources(), R.drawable.androidmarker);
        }

        public void draw(Canvas canvas, MapView mapView, boolean shadow) {
            Projection projection = mapView.getProjection();
            Paint paint = new Paint();
            Point point = new Point();
            projection.toPixels(this.gp1, point);
            paint.setStrokeWidth(5.0f);
            paint.setAlpha(120);
            paint.setColor(-16777216);
            canvas.drawBitmap(this.bmp, (float) point.x, (float) point.y, paint);
            canvas.drawText("Here I am...", (float) point.x, (float) point.y, paint);
            GPS.super.draw(canvas, mapView, shadow);
        }

        public boolean onTouchEvent(MotionEvent event, MapView mapView) {
            if (event.getAction() != 1) {
                return false;
            }
            GeoPoint p = mapView.getProjection().fromPixels((int) event.getX(), (int) event.getY());
            try {
                List<Address> addresses = new Geocoder(GPS.this.getBaseContext(), Locale.getDefault()).getFromLocation(((double) p.getLatitudeE6()) / 1000000.0d, ((double) p.getLongitudeE6()) / 1000000.0d, 1);
                GPS.this.sb = new StringBuilder();
                if (addresses.size() > 0) {
                    Address address = addresses.get(0);
                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                        GPS.this.sb.append(String.valueOf(address.getAddressLine(i)) + "\n");
                    }
                    GPS.this.sb.append(address.getLocality()).append("\n");
                    GPS.this.sb.append(address.getCountryName()).append(".");
                    GPS.this.s1[0] = address.getLocality();
                    GPS.this.s1[1] = address.getCountryName();
                    GPS.this.s1[2] = address.getAdminArea();
                }
                Toast.makeText(GPS.this.getBaseContext(), GPS.this.sb, 0).show();
                Intent i2 = new Intent();
                i2.putExtra("address", GPS.this.s1[0]);
                i2.putExtra("locality", GPS.this.s1[1]);
                i2.putExtra("wholeaddress", GPS.this.sb.toString());
                i2.putExtra("adminArea", GPS.this.s1[2]);
                GPS.this.setResult(-1, i2);
                GPS.this.finish();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
    }
}
