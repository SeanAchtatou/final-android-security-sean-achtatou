package com.mapp;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.Toast;
import com.THOMSONROGERS.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends MapActivity implements LocationListener {
    GeoPoint geoPoint;
    double latitude = 12.937875d;
    double longitude = 77.622313d;
    MapController myMC;
    MapView myMapView;
    String[] s1 = new String[3];
    StringBuilder sb;

    public void onCreate(Bundle savedInstanceState) {
        MapsActivity.super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
        String str = "My location is: Lat: " + this.latitude + " Lng: " + this.longitude;
        this.myMapView = findViewById(R.id.myGMap);
        this.geoPoint = new GeoPoint((int) (this.latitude * 1000000.0d), (int) (this.longitude * 1000000.0d));
        this.myMapView.setSatellite(false);
        this.myMC = this.myMapView.getController();
        this.myMC.setCenter(this.geoPoint);
        this.myMC.setZoom(15);
        this.myMapView.getOverlays().add(new MyLocationOverlay());
        this.myMapView.setBuiltInZoomControls(true);
        this.myMapView.displayZoomControls(true);
        ((LocationManager) getSystemService("location")).requestLocationUpdates("gps", 1000, 500.0f, this);
    }

    public void onLocationChanged(Location location) {
        if (location != null) {
            double lat = location.getLatitude();
            double lng = location.getLongitude();
            String str = "The location is changed to Lat: " + lat + " Lng: " + lng;
            this.geoPoint = new GeoPoint(((int) lat) * 1000000, ((int) lng) * 1000000);
            this.myMC.animateTo(this.geoPoint);
        }
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 37) {
            this.myMapView.getController().setZoom(this.myMapView.getZoomLevel() + 1);
            return true;
        } else if (keyCode == 43) {
            this.myMapView.getController().setZoom(this.myMapView.getZoomLevel() - 1);
            return true;
        } else if (keyCode == 47) {
            this.myMapView.setSatellite(true);
            return true;
        } else if (keyCode == 41) {
            this.myMapView.setSatellite(false);
            return true;
        } else {
            if (keyCode == 4) {
                Toast.makeText(getBaseContext(), "back", 0).show();
                Intent i = new Intent();
                i.putExtra("address", this.s1[0]);
                i.putExtra("locality", this.s1[1]);
                i.putExtra("wholeaddress", this.sb.toString());
                setResult(-1, i);
                finish();
            }
            return false;
        }
    }

    protected class MyLocationOverlay extends Overlay {
        protected MyLocationOverlay() {
        }

        public boolean draw(Canvas canvas, MapView mapView, boolean shadow, long when) {
            Paint paint = new Paint();
            MapsActivity.super.draw(canvas, mapView, shadow);
            Point myScreenCoords = new Point();
            mapView.getProjection().toPixels(MapsActivity.this.geoPoint, myScreenCoords);
            paint.setStrokeWidth(1.0f);
            paint.setARGB(255, 255, 255, 255);
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawBitmap(BitmapFactory.decodeResource(MapsActivity.this.getResources(), R.drawable.androidmarker), (float) myScreenCoords.x, (float) myScreenCoords.y, paint);
            canvas.drawText("I am here...", (float) myScreenCoords.x, (float) myScreenCoords.y, paint);
            return true;
        }

        public boolean onTouchEvent(MotionEvent event, MapView mapView) {
            if (event.getAction() != 1) {
                return false;
            }
            GeoPoint p = mapView.getProjection().fromPixels((int) event.getX(), (int) event.getY());
            try {
                List<Address> addresses = new Geocoder(MapsActivity.this.getBaseContext(), Locale.getDefault()).getFromLocation(((double) p.getLatitudeE6()) / 1000000.0d, ((double) p.getLongitudeE6()) / 1000000.0d, 1);
                MapsActivity.this.sb = new StringBuilder();
                if (addresses.size() > 0) {
                    Address address = addresses.get(0);
                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                        MapsActivity.this.sb.append(String.valueOf(address.getAddressLine(i)) + "\n");
                    }
                    MapsActivity.this.sb.append(address.getLocality()).append("\n");
                    MapsActivity.this.sb.append(address.getCountryName()).append(".");
                    MapsActivity.this.s1[0] = address.getLocality();
                    MapsActivity.this.s1[1] = address.getCountryName();
                }
                System.out.println("getLoaclity::::::" + MapsActivity.this.s1[0]);
                Toast.makeText(MapsActivity.this.getBaseContext(), MapsActivity.this.sb, 0).show();
                Intent i2 = new Intent();
                i2.putExtra("address", MapsActivity.this.s1[0]);
                i2.putExtra("locality", MapsActivity.this.s1[1]);
                i2.putExtra("wholeaddress", MapsActivity.this.sb.toString());
                i2.putExtra("adminArea", MapsActivity.this.s1[2]);
                MapsActivity.this.setResult(-1, i2);
                MapsActivity.this.finish();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
    }
}
