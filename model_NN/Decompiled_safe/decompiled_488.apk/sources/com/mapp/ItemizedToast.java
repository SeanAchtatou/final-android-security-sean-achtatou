package com.mapp;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import java.util.ArrayList;

public class ItemizedToast extends CustomItemizedOverlay<OverlayItem> {
    private Context c;
    private ArrayList<OverlayItem> m_overlays = new ArrayList<>();

    public ItemizedToast(Drawable defaultMarker, MapView mapView) {
        super(boundCenter(defaultMarker), mapView);
        this.c = mapView.getContext();
    }

    public void addOverlay(OverlayItem overlay) {
        this.m_overlays.add(overlay);
        populate();
    }

    public void removeOverlay() {
        hideToast();
    }

    /* access modifiers changed from: protected */
    public OverlayItem createItem(int i) {
        return this.m_overlays.get(i);
    }

    public int size() {
        return this.m_overlays.size();
    }

    /* access modifiers changed from: protected */
    public boolean onToastTap(int index) {
        return true;
    }
}
