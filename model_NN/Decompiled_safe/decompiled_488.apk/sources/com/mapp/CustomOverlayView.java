package com.mapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.THOMSONROGERS.R;
import com.google.android.maps.OverlayItem;

public class CustomOverlayView extends FrameLayout {
    /* access modifiers changed from: private */
    public LinearLayout layout;
    private TextView snippet;
    private TextView title;

    public CustomOverlayView(Context context, int toastnBottomOffset) {
        super(context);
        setPadding(10, 0, 10, toastnBottomOffset);
        this.layout = new LinearLayout(context);
        this.layout.setVisibility(0);
        View v = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.map_overlay, this.layout);
        this.title = (TextView) v.findViewById(R.id.balloon_item_title);
        this.snippet = (TextView) v.findViewById(R.id.balloon_item_snippet);
        ((ImageView) v.findViewById(R.id.close_img_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CustomOverlayView.this.layout.setVisibility(8);
            }
        });
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(-2, -2);
        params.gravity = 0;
        addView(this.layout, params);
    }

    public void removeToasts() {
        if (this.layout != null) {
            this.layout.setVisibility(8);
        }
    }

    public void setData(OverlayItem item) {
        this.layout.setVisibility(0);
        if (item.getTitle() != null) {
            this.title.setVisibility(0);
            this.title.setText(item.getTitle());
        } else {
            this.title.setVisibility(8);
        }
        if (item.getSnippet() != null) {
            this.snippet.setVisibility(0);
            this.snippet.setText(item.getSnippet());
            return;
        }
        this.snippet.setVisibility(8);
    }
}
