package com.bitsetters.android.notes;

import java.sql.Timestamp;

public class NoteEntry {
    public int id;
    public Timestamp lastedit;
    public String note;
}
