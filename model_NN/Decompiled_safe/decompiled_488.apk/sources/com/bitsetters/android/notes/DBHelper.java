package com.bitsetters.android.notes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public class DBHelper {
    private static final String DATABASE_NAME = "CarInsurancelatest.dbnew12345";
    private static final int DATABASE_VERSION = 1;
    private static final String DBVERSION_CREATE = "create table dbversionnew (version integer not null);";
    private static final String NOTES_CREATE = "create table notesnew (id integer primary key autoincrement, note text, lastedit text, date text, time text);";
    private static final String NOTES_DROP = "drop table notesnew;";
    private static final String TABLE_DBVERSION = "dbversionnew";
    private static final String TABLE_NOTES = "notesnew";
    private static String TAG = "DBHelper";
    private SQLiteDatabase db;
    Context myCtx;

    public DBHelper(Context ctx) {
        this.myCtx = ctx;
        try {
            this.db = this.myCtx.openOrCreateDatabase("CarInsurancelatest.dbnew12345", 0, null);
            Cursor c = this.db.query("sqlite_master", new String[]{"name"}, "type='table' and name='dbversionnew'", null, null, null, null);
            if (c.getCount() < 1) {
                CreateDatabase(this.db);
            } else {
                int version = 0;
                Cursor vc = this.db.query(true, TABLE_DBVERSION, new String[]{"version"}, null, null, null, null, null, null);
                if (vc.getCount() > 0) {
                    vc.moveToFirst();
                    version = vc.getInt(0);
                }
                vc.close();
                if (version != 1) {
                    Log.e(TAG, "database version mismatch");
                }
            }
            c.close();
        } catch (SQLException e) {
            Log.d(TAG, "SQLite exception: " + e.getLocalizedMessage());
        } finally {
            this.db.close();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void CreateDatabase(SQLiteDatabase db2) {
        try {
            db2.execSQL(DBVERSION_CREATE);
            ContentValues args = new ContentValues();
            args.put("version", (Integer) 1);
            db2.insert(TABLE_DBVERSION, null, args);
            db2.execSQL(NOTES_CREATE);
        } catch (SQLException e) {
            Log.d(TAG, "SQLite exception: " + e.getLocalizedMessage());
        }
    }

    public void deleteDatabase() {
        try {
            this.db = this.myCtx.openOrCreateDatabase("CarInsurancelatest.dbnew12345", 0, null);
            this.db.execSQL(NOTES_DROP);
            this.db.execSQL(NOTES_CREATE);
        } catch (SQLException e) {
            Log.d(TAG, "SQLite exception: " + e.getLocalizedMessage());
        } finally {
            this.db.close();
        }
    }

    public void close() {
        try {
            this.db.close();
        } catch (SQLException e) {
            Log.d(TAG, "close exception: " + e.getLocalizedMessage());
        }
    }

    public void addNote(NoteEntry entry, String time, String date) {
        ContentValues initialValues = new ContentValues();
        initialValues.put("note", entry.note);
        initialValues.put("time", time);
        initialValues.put("date", date);
        try {
            this.db = this.myCtx.openOrCreateDatabase("CarInsurancelatest.dbnew12345", 0, null);
            this.db.insert(TABLE_NOTES, null, initialValues);
        } catch (SQLException e) {
            Log.d(TAG, "SQLite exception: " + e.getLocalizedMessage());
        } finally {
            this.db.close();
        }
    }

    public void deleteNote(long Id) {
        try {
            this.db = this.myCtx.openOrCreateDatabase("CarInsurancelatest.dbnew12345", 0, null);
            this.db.delete(TABLE_NOTES, "id=" + Id, null);
        } catch (SQLException e) {
            Log.d(TAG, "SQLite exception: " + e.getLocalizedMessage());
        } finally {
            this.db.close();
        }
    }

    public List<NoteEntry> fetchAllRows(String date, String time) {
        ArrayList<NoteEntry> ret = new ArrayList<>();
        try {
            this.db = this.myCtx.openOrCreateDatabase("CarInsurancelatest.dbnew12345", 0, null);
            String[] strArr = {"id", "note"};
            Cursor c = this.db.query(TABLE_NOTES, strArr, " date = '" + date + "' AND time='" + time + "'", null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            for (int i = 0; i < numRows; i++) {
                NoteEntry row = new NoteEntry();
                row.id = c.getInt(0);
                row.note = c.getString(1);
                ret.add(row);
                c.moveToNext();
            }
            c.close();
        } catch (SQLException e) {
            Log.d(TAG, "SQLite exception: " + e.getLocalizedMessage());
        } finally {
            this.db.close();
        }
        return ret;
    }

    public NoteEntry fetchNote(int Id) {
        NoteEntry row = new NoteEntry();
        try {
            this.db = this.myCtx.openOrCreateDatabase("CarInsurancelatest.dbnew12345", 0, null);
            Cursor c = this.db.query(true, TABLE_NOTES, new String[]{"id", "note", "lastedit"}, "id=" + Id, null, null, null, null, null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                row.id = c.getInt(0);
                row.note = c.getString(1);
            } else {
                row.id = -1;
            }
            c.close();
        } catch (SQLException e) {
            Log.d(TAG, "SQLite exception: " + e.getLocalizedMessage());
        } finally {
            this.db.close();
        }
        return row;
    }

    public void updateNote(long Id, NoteEntry entry) {
        ContentValues args = new ContentValues();
        args.put("note", entry.note);
        try {
            this.db = this.myCtx.openOrCreateDatabase("CarInsurancelatest.dbnew12345", 0, null);
            this.db.update(TABLE_NOTES, args, "id=" + Id, null);
        } catch (SQLException e) {
            Log.d(TAG, "SQLite exception: " + e.getLocalizedMessage());
        } finally {
            this.db.close();
        }
    }
}
