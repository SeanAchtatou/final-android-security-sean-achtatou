package com.bitsetters.android.notes;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import com.THOMSONROGERS.R;
import java.io.IOException;
import java.io.InputStream;

public class Help extends Activity {
    public static final int CLOSE_HELP_INDEX = 1;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.help);
        setTitle(String.valueOf(getResources().getString(R.string.app_name)) + " - " + getResources().getString(R.string.help));
        try {
            InputStream is = getAssets().open("help.html");
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            is.close();
            ((WebView) findViewById(R.id.help)).loadData(new String(buffer), "text/html", "utf-8");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.close).setIcon(17301560).setShortcut('0', 'w');
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
