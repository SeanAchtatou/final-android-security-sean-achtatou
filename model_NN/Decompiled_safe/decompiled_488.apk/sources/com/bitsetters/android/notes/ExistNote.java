package com.bitsetters.android.notes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.THOMSONROGERS.R;
import com.tritone.DBDriod;

public class ExistNote extends Activity {
    public static final int DELETE_INDEX = 1;
    public static final int EMAIL_INDEX = 2;
    public static final String TAG = "NoteEdit";
    String date;
    private DBHelper dbHelper = null;
    Button delete;
    Button done;
    DBDriod droid;
    protected EditText edit_txt;
    View.OnKeyListener key_listener = new View.OnKeyListener() {
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            ExistNote.this.updateTitle();
            return false;
        }
    };
    /* access modifiers changed from: private */
    public NoteEntry note;
    String time;

    public static class NoteHeader extends TextView {
        private Paint mLine = new Paint();

        public NoteHeader(Context context, AttributeSet attrs) {
            super(context, attrs);
            setPadding(20, 0, 0, 0);
            setHeight(20);
            this.mLine.setStyle(Paint.Style.STROKE);
            this.mLine.setARGB(100, 255, 0, 0);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            Paint paint = this.mLine;
            canvas.drawLine((float) getLeft(), (float) (getHeight() - 1), (float) getRight(), (float) (getHeight() - 1), paint);
            canvas.drawLine(10.0f, (float) getTop(), 10.0f, (float) getBottom(), paint);
            canvas.drawLine(15.0f, (float) getTop(), 15.0f, (float) getBottom(), paint);
            super.onDraw(canvas);
        }
    }

    public static class LinedEditText extends EditText {
        private Paint mPaint = new Paint();
        private Paint mVert;

        public LinedEditText(Context context, AttributeSet attrs) {
            super(context, attrs);
            this.mPaint.setStyle(Paint.Style.STROKE);
            this.mPaint.setARGB(100, 0, 0, 255);
            this.mVert = new Paint();
            this.mVert.setStyle(Paint.Style.STROKE);
            this.mVert.setARGB(100, 255, 0, 0);
            setPadding(20, 0, 0, 0);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            int count = getLineCount();
            Paint paint = this.mPaint;
            int height = getHeight();
            int line_height = getLineHeight();
            int page_size = height / line_height;
            if (count < page_size) {
                count = page_size;
            }
            for (int i = 1; i < count; i++) {
                int posY = i * line_height;
                canvas.drawLine((float) getLeft(), (float) (posY - 1), (float) getRight(), (float) (posY - 1), paint);
            }
            canvas.drawLine(10.0f, 0.0f, 10.0f, (float) getBottom(), this.mVert);
            canvas.drawLine(15.0f, 0.0f, 15.0f, (float) getBottom(), this.mVert);
            super.onDraw(canvas);
        }
    }

    /* access modifiers changed from: private */
    public void updateTitle() {
        if (this.edit_txt != null) {
            String first = this.edit_txt.getText().toString().split("\\n")[0];
            if (first.length() > 30) {
                first = String.valueOf(first.substring(0, 27)) + "...";
            }
            setTitle("Note - " + first);
        }
    }

    private void sendEmail() {
        Intent sendIntent = new Intent("android.intent.action.SEND");
        sendIntent.putExtra("android.intent.extra.TEXT", this.edit_txt.getText().toString());
        sendIntent.putExtra("android.intent.extra.SUBJECT", "Subject");
        sendIntent.setType("message/rfc822");
        startActivity(Intent.createChooser(sendIntent, "Title:"));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                deleteNote();
                finish();
                break;
            case 2:
                sendEmail();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onMenuOpened(int featureId, Menu menu) {
        menu.findItem(1).setEnabled(this.note.id >= 0);
        return super.onMenuOpened(featureId, menu);
    }

    private void deleteNote() {
        this.dbHelper.deleteNote((long) this.note.id);
    }

    private void save() {
        this.note.note = this.edit_txt.getText().toString();
        if (this.note.id >= 0) {
            this.dbHelper.updateNote((long) this.note.id, this.note);
            return;
        }
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", 1);
        String time2 = myPrefs.getString("time", "time");
        String date2 = myPrefs.getString("date", "date");
        this.dbHelper.addNote(this.note, time2, date2);
        System.out.println("notes: " + time2 + " : " + date2);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Log.d("NoteEdit", "onPause()");
        if (this.dbHelper != null) {
            save();
            this.dbHelper = null;
        }
    }

    public void onStop() {
        super.onStop();
        Log.d("NoteEdit", "onStop()");
        if (this.dbHelper != null) {
            save();
            this.dbHelper = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Log.d("NoteEdit", "onResume()");
        if (this.dbHelper == null) {
            this.dbHelper = new DBHelper(this);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        this.droid = new DBDriod(this);
        requestWindowFeature(1);
        setContentView((int) R.layout.existnotes);
        this.edit_txt = (EditText) findViewById(R.id.edit_note);
        this.done = (Button) findViewById(R.id.BackButton);
        this.delete = (Button) findViewById(R.id.Trash);
        this.edit_txt.setLongClickable(true);
        this.edit_txt.setOnKeyListener(this.key_listener);
        if (this.dbHelper == null) {
            this.dbHelper = new DBHelper(this);
        }
        int id = getIntent().getIntExtra("id", -1);
        if (id >= 0) {
            this.note = this.dbHelper.fetchNote(id);
            this.edit_txt.setText(this.note.note);
        } else {
            this.note = new NoteEntry();
            this.note.id = -1;
        }
        this.delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExistNote.this.droid.deleteNotepad(ExistNote.this.note.note);
                ExistNote.this.finish();
            }
        });
        this.done.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExistNote.this.finish();
            }
        });
        updateTitle();
    }
}
