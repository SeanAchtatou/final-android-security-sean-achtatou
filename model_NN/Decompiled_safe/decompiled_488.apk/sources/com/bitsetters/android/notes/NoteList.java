package com.bitsetters.android.notes;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import com.THOMSONROGERS.R;
import java.util.ArrayList;
import java.util.List;

public class NoteList extends ListActivity {
    public static final int ADD_INDEX = 1;
    public static final int HELP_INDEX = 3;
    public static final int SETTINGS_INDEX = 2;
    public static final String TAG = "NoteList";
    private DBHelper dbHelper = null;
    private List<NoteEntry> rows;
    ListView view;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        if (this.dbHelper == null) {
            this.dbHelper = new DBHelper(this);
        }
        setContentView((int) R.layout.notelist);
        ((Button) findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NoteList.this.finish();
            }
        });
        fillData();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.dbHelper != null) {
            this.dbHelper = null;
        }
    }

    public void onStop() {
        super.onStop();
        if (this.dbHelper != null) {
            this.dbHelper = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.dbHelper == null) {
            this.dbHelper = new DBHelper(this);
        }
        fillData();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                addNote();
                break;
            case HELP_INDEX /*3*/:
                startActivity(new Intent(this, Help.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private String get_description(NoteEntry ent) {
        String first = ent.note.split("\\n")[0];
        if (first.length() > 10) {
            return String.valueOf(first.substring(0, 7)) + "...";
        }
        return first;
    }

    private void fillData() {
        SharedPreferences myPrefs = getSharedPreferences("myPrefs", 1);
        SharedPreferences.Editor edit = myPrefs.edit();
        String time = myPrefs.getString("time", "time");
        this.rows = this.dbHelper.fetchAllRows(myPrefs.getString("date", "date"), time);
        List<String> items = new ArrayList<>();
        for (NoteEntry ent : this.rows) {
            items.add(get_description(ent));
        }
        if (items.size() == 0) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Attention");
            alertDialog.setMessage("No records available.");
            alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    NoteList.this.finish();
                }
            });
            alertDialog.create().show();
        }
        setListAdapter(new ArrayAdapter<>(this, 17367043, items));
    }

    /* access modifiers changed from: protected */
    public void addNote() {
        Intent i = new Intent(this, NoteEdit.class);
        i.putExtra("id", -1);
        startActivity(i);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.add).setIcon(17301555).setShortcut('1', 'a');
        return super.onCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent i = new Intent(this, ExistNote.class);
        i.putExtra("id", this.rows.get(position).id);
        startActivity(i);
    }
}
