package defpackage;

import android.net.Uri;
import android.webkit.WebView;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: a  reason: default package */
public final class a {
    public static final Map<String, i> a;
    public static final Map<String, i> b;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put("/invalidRequest", new o());
        hashMap.put("/loadAdURL", new p());
        a = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("/open", new r());
        hashMap2.put("/canOpenURLs", new j());
        hashMap2.put("/close", new l());
        hashMap2.put("/evalInOpener", new m());
        hashMap2.put("/log", new q());
        hashMap2.put("/click", new k());
        hashMap2.put("/httpTrack", new n());
        hashMap2.put("/reloadRequest", new s());
        hashMap2.put("/settings", new t());
        hashMap2.put("/touch", new u());
        hashMap2.put("/video", new v());
        b = Collections.unmodifiableMap(hashMap2);
    }

    private a() {
    }

    public static void a(WebView webView) {
        com.google.ads.util.a.d("Calling onshow.");
        a(webView, "onshow", "{'version': 'afma-sdk-a-v4.1.1'}");
    }

    public static void a(WebView webView, String str) {
        webView.loadUrl("javascript:" + str);
    }

    public static void a(WebView webView, String str, String str2) {
        if (str2 != null) {
            a(webView, "AFMA_ReceiveMessage" + "('" + str + "', " + str2 + ");");
        } else {
            a(webView, "AFMA_ReceiveMessage" + "('" + str + "');");
        }
    }

    public static void a(WebView webView, Map<String, Boolean> map) {
        a(webView, "openableURLs", new JSONObject(map).toString());
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.lang.String.equals(java.lang.Object):boolean in method: a.a(d, java.util.Map<java.lang.String, i>, android.net.Uri, android.webkit.WebView):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.lang.String.equals(java.lang.Object):boolean
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:540)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    static void a(defpackage.d r1, java.util.Map<java.lang.String, defpackage.i> r2, android.net.Uri r3, android.webkit.WebView r4) {
        /*
            r3 = 0
            java.util.HashMap r1 = com.google.ads.util.AdUtil.b(r6)
            if (r1 != 0) goto L_0x000d
            java.lang.String r0 = "An error occurred while parsing the message parameters."
            com.google.ads.util.a.e(r0)
        L_0x000c:
            return
        L_0x000d:
            boolean r0 = c(r6)
            if (r0 == 0) goto L_0x007e
            java.lang.String r0 = r6.getHost()
            if (r0 != 0) goto L_0x0027
            java.lang.String r0 = "An error occurred while parsing the AMSG parameters."
            com.google.ads.util.a.e(r0)
            r2 = r3
        L_0x001f:
            if (r2 != 0) goto L_0x0091
            java.lang.String r0 = "An error occurred while parsing the message."
            com.google.ads.util.a.e(r0)
            goto L_0x000c
        L_0x0027:
            java.lang.String r2 = "launch"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x004a
            java.lang.String r0 = "a"
            java.lang.String r2 = "intent"
            r1.put(r0, r2)
            java.lang.String r0 = "u"
            java.lang.String r2 = "url"
            java.lang.Object r2 = r1.get(r2)
            r1.put(r0, r2)
            java.lang.String r0 = "url"
            r1.remove(r0)
            java.lang.String r0 = "/open"
            r2 = r0
            goto L_0x001f
        L_0x004a:
            java.lang.String r2 = "closecanvas"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x0056
            java.lang.String r0 = "/close"
            r2 = r0
            goto L_0x001f
        L_0x0056:
            java.lang.String r2 = "log"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0062
            java.lang.String r0 = "/log"
            r2 = r0
            goto L_0x001f
        L_0x0062:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "An error occurred while parsing the AMSG: "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = r6.toString()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.google.ads.util.a.e(r0)
            r2 = r3
            goto L_0x001f
        L_0x007e:
            boolean r0 = b(r6)
            if (r0 == 0) goto L_0x008a
            java.lang.String r0 = r6.getPath()
            r2 = r0
            goto L_0x001f
        L_0x008a:
            java.lang.String r0 = "Message was neither a GMSG nor an AMSG."
            com.google.ads.util.a.e(r0)
            r2 = r3
            goto L_0x001f
        L_0x0091:
            java.lang.Object r0 = r5.get(r2)
            i r0 = (defpackage.i) r0
            if (r0 != 0) goto L_0x00b7
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "No AdResponse found, <message: "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r1 = ">"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.google.ads.util.a.e(r0)
            goto L_0x000c
        L_0x00b7:
            r0.a(r4, r1, r7)
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.a.a(d, java.util.Map, android.net.Uri, android.webkit.WebView):void");
    }

    public static boolean a(Uri uri) {
        if (uri == null || !uri.isHierarchical()) {
            return false;
        }
        return b(uri) || c(uri);
    }

    public static void b(WebView webView) {
        com.google.ads.util.a.d("Calling onhide.");
        a(webView, "onhide", null);
    }

    private static boolean b(Uri uri) {
        String scheme = uri.getScheme();
        if (scheme == null || !scheme.equals("gmsg")) {
            return false;
        }
        String authority = uri.getAuthority();
        return authority != null && authority.equals("mobileads.google.com");
    }

    private static boolean c(Uri uri) {
        String scheme = uri.getScheme();
        return scheme != null && scheme.equals("admob");
    }
}
