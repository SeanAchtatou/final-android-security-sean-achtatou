package com.facebook.android;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int bckg1 = 2130837504;
        public static final int bckg2 = 2130837505;
        public static final int bee1_1 = 2130837506;
        public static final int bee1_2 = 2130837507;
        public static final int bee1_glow = 2130837508;
        public static final int btn_arrow_left = 2130837509;
        public static final int btn_arrow_left_dis = 2130837510;
        public static final int btn_arrow_right = 2130837511;
        public static final int btn_arrow_right_dis = 2130837512;
        public static final int btn_back = 2130837513;
        public static final int btn_cancel = 2130837514;
        public static final int btn_help = 2130837515;
        public static final int btn_level = 2130837516;
        public static final int btn_level_dis = 2130837517;
        public static final int btn_level_glow = 2130837518;
        public static final int btn_more = 2130837519;
        public static final int btn_more_on = 2130837520;
        public static final int btn_music_off = 2130837521;
        public static final int btn_music_on = 2130837522;
        public static final int btn_next = 2130837523;
        public static final int btn_ok = 2130837524;
        public static final int btn_play = 2130837525;
        public static final int btn_quit = 2130837526;
        public static final int btn_restart = 2130837527;
        public static final int btn_settings = 2130837528;
        public static final int btn_settings_fb = 2130837529;
        public static final int btn_settings_on = 2130837530;
        public static final int btn_sound_off = 2130837531;
        public static final int btn_sound_on = 2130837532;
        public static final int btn_submit = 2130837533;
        public static final int buy = 2130837534;
        public static final int circle_green = 2130837535;
        public static final int circle_red = 2130837536;
        public static final int circle_white = 2130837537;
        public static final int circle_yellow = 2130837538;
        public static final int dynamic1 = 2130837539;
        public static final int dynamic2 = 2130837540;
        public static final int facebook = 2130837541;
        public static final int facebook_icon = 2130837542;
        public static final int finished_bckg = 2130837543;
        public static final int finished_highscore = 2130837544;
        public static final int fly1_1 = 2130837545;
        public static final int fly1_2 = 2130837546;
        public static final int glowworm1_1 = 2130837547;
        public static final int glowworm1_2 = 2130837548;
        public static final int glowworm1_glow = 2130837549;
        public static final int help1 = 2130837550;
        public static final int help2 = 2130837551;
        public static final int help3 = 2130837552;
        public static final int ic_icon = 2130837553;
        public static final int lbl_paused = 2130837554;
        public static final int lbl_quit = 2130837555;
        public static final int lbl_restart = 2130837556;
        public static final int lizard1 = 2130837557;
        public static final int lizard2 = 2130837558;
        public static final int lizard3 = 2130837559;
        public static final int lizard4 = 2130837560;
        public static final int lizard_eat = 2130837561;
        public static final int lizard_skin = 2130837562;
        public static final int market = 2130837563;
        public static final int menu_title = 2130837564;
        public static final int mosquito1_1 = 2130837565;
        public static final int mosquito1_2 = 2130837566;
        public static final int noalm_logo = 2130837567;
        public static final int percent_bckg = 2130837568;
        public static final int token = 2130837569;
        public static final int token_glow = 2130837570;
        public static final int tongue = 2130837571;
        public static final int twitter = 2130837572;
        public static final int wall1 = 2130837573;
        public static final int wall2 = 2130837574;
        public static final int world1 = 2130837575;
        public static final int world2 = 2130837576;
        public static final int world_info_bckg = 2130837577;
        public static final int world_locked = 2130837578;
    }

    public static final class id {
        public static final int adView = 2131034113;
        public static final int lizard = 2131034112;
    }

    public static final class layout {
        public static final int lizard_layout = 2130903040;
    }

    public static final class raw {
        public static final int button1 = 2130968576;
        public static final int eat = 2130968577;
        public static final int hurt1 = 2130968578;
        public static final int hurt2 = 2130968579;
        public static final int music1 = 2130968580;
        public static final int music2 = 2130968581;
        public static final int new_highscore = 2130968582;
        public static final int yeah = 2130968583;
        public static final int yuppie = 2130968584;
    }
}
