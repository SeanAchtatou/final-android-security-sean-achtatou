package com.noalm.lizard;

import android.graphics.Canvas;

public class Dynamic {
    private static final float MOVE_RADIUS = 25.0f;
    private static final float MOVE_SPEED = 0.07f;
    private boolean ActorRel = false;
    private boolean ActorRelChecked = false;
    private float Angle = 0.0f;
    private Vector2 CurPos = new Vector2(0.0f, 0.0f);
    private boolean Move = false;
    private boolean MoveDir = false;
    private float MoveLength = 0.0f;
    private Vector2 MovePos = new Vector2(0.0f, 0.0f);
    private float MoveVal = 0.0f;
    private Vector2 Pos = new Vector2(0.0f, 0.0f);
    private float Scale = 1.0f;
    private int Side = 0;

    public void init(float posX, float posY, float ScaleMultiply, int _Side) {
        this.Pos.set(posX, posY);
        this.CurPos.set(this.Pos);
        this.Scale = 0.8f + (((float) LizardView.Random(0, 1)) * 0.1f);
        this.Scale *= ScaleMultiply;
        this.Angle = ((float) Math.random()) * 360.0f;
        this.Side = _Side;
        this.ActorRelChecked = false;
        this.ActorRel = false;
        this.Move = false;
        this.MoveDir = false;
        this.MoveVal = 0.0f;
        this.MoveLength = 0.0f;
        this.MovePos.set(0.0f, 0.0f);
    }

    public void update() {
        boolean rel;
        if (this.Side == Actor.Side) {
            if (this.Side == 1 || this.Side == 2) {
                rel = Actor.HeadPos.x < this.Pos.x;
            } else {
                rel = Actor.HeadPos.y < this.Pos.y;
            }
            if (this.ActorRelChecked && rel != this.ActorRel) {
                this.Move = true;
                this.MoveDir = true;
                this.MoveVal = 0.0f;
                this.MovePos.set(((float) Math.random()) - 0.5f, ((float) Math.random()) - 0.5f);
                this.MovePos.x *= MOVE_RADIUS;
                this.MovePos.y *= MOVE_RADIUS;
                this.MoveLength = this.MovePos.length();
                this.MovePos.x += this.Pos.x;
                this.MovePos.y += this.Pos.y;
            }
            this.ActorRelChecked = true;
            this.ActorRel = rel;
        } else {
            this.ActorRelChecked = false;
        }
        if (!this.Move) {
            return;
        }
        if (this.MoveDir) {
            this.MoveVal += Game.fTimeMultiplier * MOVE_SPEED;
            if (this.MoveVal > 1.0f) {
                this.MoveDir = false;
                this.MoveVal = 1.0f;
                this.CurPos.set(this.MovePos);
                return;
            }
            this.CurPos.set(this.MovePos);
            this.CurPos.subtract(this.Pos);
            this.CurPos.normalize();
            this.CurPos.multiply(this.MoveLength * this.MoveVal);
            this.CurPos.add(this.Pos);
            return;
        }
        this.MoveVal -= Game.fTimeMultiplier * MOVE_SPEED;
        if (this.MoveVal < 0.0f) {
            this.MoveDir = false;
            this.MoveVal = 0.0f;
            this.Move = false;
            this.CurPos.set(this.Pos);
            return;
        }
        this.CurPos.set(this.MovePos);
        this.CurPos.subtract(this.Pos);
        this.CurPos.normalize();
        this.CurPos.multiply(this.MoveLength * this.MoveVal);
        this.CurPos.add(this.Pos);
    }

    public void draw(Canvas canvas) {
        UI.drawBitmapTSRotT(canvas, UI.ImgDynamic, UI.DynamicNegHSize.x, UI.DynamicNegHSize.y, this.CurPos.x, this.CurPos.y, this.Scale, this.Scale, this.Angle);
    }
}
