package com.noalm.lizard;

import android.graphics.Bitmap;
import android.graphics.Canvas;

public class Object {
    public static final int BH_ROAM = 0;
    public static final int BH_STING = 1;
    private static Vector2 Dir = new Vector2(0.0f, 0.0f);
    public static final int TYPE_BEE = 3;
    public static final int TYPE_FLY = 0;
    public static final int TYPE_GLOWWORM = 2;
    public static final int TYPE_MOSQUITO = 1;
    public static final int TYPE_TOKEN = 4;
    private float Acceleration = 0.0f;
    private boolean Active = true;
    private BitmapAnimation Anim = new BitmapAnimation();
    private int Behavior = 0;
    private Bitmap Bmp = null;
    private float CurAngle = 0.0f;
    private float CurSpeed = 0.0f;
    private long CurTime = 0;
    private Vector2 Dest = new Vector2(0.0f, 0.0f);
    private float DestAngle = 0.0f;
    public boolean Eaten = false;
    private boolean FlyAway = false;
    private boolean FlyIn = false;
    private boolean Glow = false;
    private Bitmap GlowBmp = null;
    private boolean GlowDir = true;
    private boolean GlowOnTop = true;
    private float GlowVal = 0.0f;
    private long LifeTime = 0;
    private float MaxSpeed = 0.0f;
    private Vector2 NegHSize = new Vector2(0.0f, 0.0f);
    private Vector2 Pos = new Vector2(0.0f, 0.0f);
    private float Radius = 20.0f;
    private int RectInd = 0;
    private float RotAngle = 0.0f;
    private float Scale = 1.0f;
    public int Score = 0;
    private int Type = 0;

    public void init(int _Type, float _MaxSpeedMpl, float _AccelerationMpl, long _LifeTime, int _RectInd) {
        this.Active = true;
        this.Eaten = false;
        this.FlyIn = true;
        this.FlyAway = false;
        this.Type = _Type;
        if (this.Type == 0) {
            this.Behavior = 0;
            this.NegHSize.set(-20.0f, -20.0f);
            this.Scale = 0.6f;
            this.Radius = (-this.NegHSize.x) * this.Scale;
            this.CurAngle = 0.0f;
            this.DestAngle = 0.0f;
            this.RotAngle = 4.0f;
            this.CurSpeed = 0.0f;
            this.MaxSpeed = 1.7f * _MaxSpeedMpl;
            this.Acceleration = 0.02f * _AccelerationMpl;
            this.LifeTime = _LifeTime;
            this.CurTime = 0;
            this.RectInd = _RectInd;
            this.Glow = false;
            this.GlowOnTop = true;
            this.GlowDir = true;
            this.GlowVal = 0.0f;
            this.Score = 100;
            this.Anim.init(30, true, false, UI.ImgFly1);
            this.Bmp = null;
            this.GlowBmp = null;
            this.Dest.set(LizardView.RandomF(LevelRule.RoamRects[this.RectInd].left, LevelRule.RoamRects[this.RectInd].right), LizardView.RandomF(LevelRule.RoamRects[this.RectInd].top, LevelRule.RoamRects[this.RectInd].bottom));
            setInitPos();
        } else if (this.Type == 1) {
            this.Behavior = 0;
            this.NegHSize.set(-20.0f, -24.0f);
            this.Scale = 0.7f;
            this.Radius = (-this.NegHSize.x) * this.Scale;
            this.CurAngle = 0.0f;
            this.DestAngle = 0.0f;
            this.RotAngle = 4.0f;
            this.CurSpeed = 0.0f;
            this.MaxSpeed = 1.7f * _MaxSpeedMpl;
            this.Acceleration = 0.02f * _AccelerationMpl;
            this.LifeTime = _LifeTime;
            this.CurTime = 0;
            this.RectInd = _RectInd;
            this.Glow = false;
            this.GlowOnTop = true;
            this.GlowDir = true;
            this.GlowVal = 0.0f;
            this.Score = 100;
            this.Anim.init(30, true, false, UI.ImgMosquito1);
            this.Bmp = null;
            this.GlowBmp = null;
            this.Dest.set(LizardView.RandomF(LevelRule.RoamRects[this.RectInd].left, LevelRule.RoamRects[this.RectInd].right), LizardView.RandomF(LevelRule.RoamRects[this.RectInd].top, LevelRule.RoamRects[this.RectInd].bottom));
            setInitPos();
        } else if (this.Type == 2) {
            this.Behavior = 0;
            this.NegHSize.set(-20.0f, -17.0f);
            this.Scale = 0.9f;
            this.Radius = (-this.NegHSize.x) * this.Scale;
            this.CurAngle = 0.0f;
            this.DestAngle = 0.0f;
            this.RotAngle = 4.0f;
            this.CurSpeed = 0.0f;
            this.MaxSpeed = 1.7f * _MaxSpeedMpl;
            this.Acceleration = 0.02f * _AccelerationMpl;
            this.LifeTime = _LifeTime;
            this.CurTime = 0;
            this.RectInd = _RectInd;
            this.Glow = true;
            this.GlowOnTop = true;
            this.GlowDir = true;
            this.GlowVal = 0.0f;
            this.Score = 100;
            this.Anim.init(30, true, false, UI.ImgGlowworm1);
            this.Bmp = null;
            this.GlowBmp = UI.ImgGlowworm1Glow;
            this.Dest.set(LizardView.RandomF(LevelRule.RoamRects[this.RectInd].left, LevelRule.RoamRects[this.RectInd].right), LizardView.RandomF(LevelRule.RoamRects[this.RectInd].top, LevelRule.RoamRects[this.RectInd].bottom));
            setInitPos();
        } else if (this.Type == 3) {
            this.Behavior = 1;
            this.NegHSize.set(-31.0f, -31.0f);
            this.Scale = 0.7f;
            this.Radius = (-this.NegHSize.x) * this.Scale;
            this.CurAngle = 0.0f;
            this.DestAngle = 0.0f;
            this.RotAngle = 4.0f;
            this.CurSpeed = 0.0f;
            this.MaxSpeed = 8.0f * _MaxSpeedMpl;
            this.Acceleration = 0.1f * _AccelerationMpl;
            this.LifeTime = _LifeTime;
            this.CurTime = 0;
            this.RectInd = _RectInd;
            this.Glow = true;
            this.GlowOnTop = false;
            this.GlowDir = true;
            this.GlowVal = 0.0f;
            this.Score = 0;
            this.Anim.init(30, true, false, UI.ImgBee1);
            this.Bmp = null;
            this.GlowBmp = UI.ImgBee1Glow;
            this.Dest.set(LizardView.RandomF((UI.ScreenWidth / 2.0f) - 30.0f, (UI.ScreenWidth / 2.0f) + 30.0f), LizardView.RandomF((UI.ScreenHeight / 2.0f) - 30.0f, (UI.ScreenHeight / 2.0f) + 30.0f));
            setInitPos();
        } else if (this.Type == 4) {
            this.Behavior = 0;
            this.Pos.set(UI.ScreenWidth / 2.0f, UI.ScreenHeight / 2.0f);
            this.NegHSize.set(-24.0f, -24.0f);
            this.Scale = 0.1f;
            this.Radius = (-this.NegHSize.x) * this.Scale;
            this.CurAngle = 0.0f;
            this.DestAngle = 0.0f;
            this.RotAngle = 1.0f;
            this.CurSpeed = 0.8f * _MaxSpeedMpl;
            this.MaxSpeed = 0.0f;
            this.Acceleration = 0.0f;
            this.LifeTime = _LifeTime;
            this.CurTime = 0;
            this.RectInd = _RectInd;
            this.Glow = true;
            this.GlowOnTop = false;
            this.GlowDir = true;
            this.GlowVal = 0.0f;
            this.Score = 0;
            this.Anim.init(30, true, false, UI.ImgFly1);
            this.Bmp = UI.ImgToken;
            this.GlowBmp = UI.ImgTokenGlow;
            this.Dest.set(0.0f, 0.0f);
            int side = LizardView.Random(1, 4);
            if (side == 1) {
                this.Dest.set(UI.ScreenWidth / 2.0f, 10.0f);
            } else if (side == 2) {
                this.Dest.set(UI.ScreenWidth / 2.0f, UI.ScreenHeight - 10.0f);
            } else if (side == 3) {
                this.Dest.set(10.0f, UI.ScreenHeight / 2.0f);
            } else if (side == 4) {
                this.Dest.set(UI.ScreenWidth - 10.0f, UI.ScreenHeight / 2.0f);
            }
            this.Dest.subtract(this.Pos);
            this.Dest.multiply(2.0f);
            this.Dest.add(this.Pos);
        }
        Dir.set(this.Dest);
        Dir.subtract(this.Pos);
        this.DestAngle = Dir.angle();
    }

    public boolean update() {
        if (!this.Active) {
            return false;
        }
        if (!this.Eaten || !Actor.Eat) {
            if (this.Type == 4) {
                if (this.Scale < 0.8f) {
                    this.Scale += 0.03f * Game.fTimeMultiplier;
                    if (this.Scale > 1.0f) {
                        this.Scale = 1.0f;
                    }
                    this.Radius = (-this.NegHSize.x) * this.Scale;
                }
                this.CurAngle += this.RotAngle * Game.fTimeMultiplier;
                if (this.CurAngle > 360.0f) {
                    this.CurAngle -= 360.0f;
                }
                Dir.set(this.Dest);
                Dir.subtract(this.Pos);
                Dir.normalize();
                Dir.multiply(this.CurSpeed * Game.fTimeMultiplier);
                this.Pos.add(Dir);
                if (this.Pos.distance(Actor.HeadPos) < this.Radius + 40.0f && Actor.tokenPickUp(this.Pos)) {
                    this.Active = false;
                    return false;
                } else if (this.Pos.x + this.Radius < 0.0f) {
                    this.Active = false;
                    return false;
                } else if (this.Pos.x - this.Radius > UI.ScreenWidth) {
                    this.Active = false;
                    return false;
                } else if (this.Pos.y + this.Radius < 0.0f) {
                    this.Active = false;
                    return false;
                } else if (this.Pos.y - this.Radius > UI.ScreenHeight) {
                    this.Active = false;
                    return false;
                }
            } else {
                if (this.FlyIn) {
                    if (this.CurSpeed < 5.0f) {
                        this.CurSpeed += 0.05f * Game.fTimeMultiplier;
                        if (this.CurSpeed > 5.0f) {
                            this.CurSpeed = 5.0f;
                        }
                    }
                } else if (this.CurSpeed < this.MaxSpeed) {
                    this.CurSpeed += this.Acceleration * Game.fTimeMultiplier;
                    if (this.CurSpeed > this.MaxSpeed) {
                        this.CurSpeed = this.MaxSpeed;
                    }
                }
                Dir.set(this.Dest);
                Dir.subtract(this.Pos);
                Dir.normalize();
                Dir.multiply(this.CurSpeed * Game.fTimeMultiplier);
                this.Pos.add(Dir);
                if (this.Behavior == 0) {
                    if (this.Pos.distance(this.Dest) < 20.0f) {
                        this.FlyIn = false;
                        this.Dest.set(LizardView.RandomF(LevelRule.RoamRects[this.RectInd].left, LevelRule.RoamRects[this.RectInd].right), LizardView.RandomF(LevelRule.RoamRects[this.RectInd].top, LevelRule.RoamRects[this.RectInd].bottom));
                        Dir.set(this.Dest);
                        Dir.subtract(this.Pos);
                        this.DestAngle = Dir.angle();
                        this.CurSpeed = 0.0f;
                    }
                    if (this.LifeTime > 0) {
                        this.CurTime += Game.DeltaTime;
                        if (this.CurTime > this.LifeTime) {
                            this.FlyAway = true;
                            this.LifeTime = 0;
                            this.Dest.set(LizardView.RandomF(-10.0f, 10.0f), LizardView.RandomF(-10.0f, 10.0f));
                            this.Dest.normalize();
                            this.Dest.multiply(600.0f);
                            this.Dest.add(UI.ScreenWidth / 2.0f, UI.ScreenHeight / 2.0f);
                            Dir.set(this.Dest);
                            Dir.subtract(this.Pos);
                            this.DestAngle = Dir.angle();
                            this.CurSpeed = 0.0f;
                            this.MaxSpeed = 8.0f;
                            this.Acceleration = 0.2f;
                        }
                    }
                } else if (this.Behavior == 1) {
                    if (!this.FlyIn && !this.FlyAway && this.Pos.distance(Actor.HeadPos) < this.Radius + 30.0f) {
                        Actor.damage();
                        this.FlyAway = true;
                        this.LifeTime = 0;
                        this.Dest.set(LizardView.RandomF(-10.0f, 10.0f), LizardView.RandomF(-10.0f, 10.0f));
                        this.Dest.normalize();
                        this.Dest.multiply(600.0f);
                        this.Dest.add(UI.ScreenWidth / 2.0f, UI.ScreenHeight / 2.0f);
                        Dir.set(this.Dest);
                        Dir.subtract(this.Pos);
                        this.DestAngle = Dir.angle();
                        this.RotAngle /= 2.0f;
                        this.CurSpeed = 0.0f;
                        this.MaxSpeed = 8.0f;
                        this.Acceleration = 0.2f;
                    }
                    if (this.Pos.distance(this.Dest) < 20.0f) {
                        if (this.FlyIn) {
                            this.FlyIn = false;
                            this.LifeTime = 0;
                            this.Dest.set(Actor.HeadPos);
                            this.Dest.subtract(this.Pos);
                            this.Dest.normalize();
                            this.Dest.multiply(1600.0f);
                            this.Dest.add(this.Pos);
                            Dir.set(this.Dest);
                            Dir.subtract(this.Pos);
                            this.DestAngle = Dir.angle() - 180.0f;
                            this.RotAngle *= 2.0f;
                            this.CurSpeed = 0.0f;
                        } else {
                            this.Active = false;
                            return false;
                        }
                    }
                }
                if (this.CurAngle < this.DestAngle) {
                    this.CurAngle += this.RotAngle * Game.fTimeMultiplier;
                    if (this.CurAngle > this.DestAngle) {
                        this.CurAngle = this.DestAngle;
                    }
                } else if (this.CurAngle > this.DestAngle) {
                    this.CurAngle -= this.RotAngle * Game.fTimeMultiplier;
                    if (this.CurAngle < this.DestAngle) {
                        this.CurAngle = this.DestAngle;
                    }
                }
                if (this.FlyAway) {
                    if (this.Pos.x + this.Radius < -20.0f) {
                        this.Active = false;
                        return false;
                    } else if (this.Pos.x - this.Radius > UI.ScreenWidth + 20.0f) {
                        this.Active = false;
                        return false;
                    } else if (this.Pos.y + this.Radius < -20.0f) {
                        this.Active = false;
                        return false;
                    } else if (this.Pos.y - this.Radius > UI.ScreenHeight + 20.0f) {
                        this.Active = false;
                        return false;
                    }
                }
                this.Anim.update();
            }
            if (this.Glow) {
                if (this.GlowDir) {
                    this.GlowVal = (float) (((double) this.GlowVal) + (0.04d * ((double) Game.fTimeMultiplier)));
                    if (this.GlowVal > 1.0f) {
                        this.GlowDir = false;
                        this.GlowVal = 1.0f;
                    }
                } else {
                    this.GlowVal = (float) (((double) this.GlowVal) - (0.04d * ((double) Game.fTimeMultiplier)));
                    if (this.GlowVal < 0.0f) {
                        this.GlowDir = true;
                        this.GlowVal = 0.0f;
                    }
                }
            }
        } else {
            this.Pos.set(Actor.TongueEndPos);
        }
        return true;
    }

    public void draw(Canvas canvas) {
        if (this.Glow && !this.GlowOnTop) {
            UI.drawBitmapTSRotTA(canvas, this.GlowBmp, this.NegHSize.x, this.NegHSize.y, this.Pos.x, this.Pos.y, this.Scale, this.Scale, this.CurAngle, (int) (this.GlowVal * 255.0f));
        }
        if (this.Type != 4) {
            UI.drawBitmapTSRotTA(canvas, this.Anim.getCurFrameBmp(), this.NegHSize.x, this.NegHSize.y, this.Pos.x, this.Pos.y, this.Scale, this.Scale, this.CurAngle, 255);
        }
        if (this.Bmp != null) {
            UI.drawBitmapTSRotTA(canvas, this.Bmp, this.NegHSize.x, this.NegHSize.y, this.Pos.x, this.Pos.y, this.Scale, this.Scale, this.CurAngle, 255);
        }
        if (this.Glow && this.GlowOnTop) {
            UI.drawBitmapTSRotTA(canvas, this.GlowBmp, this.NegHSize.x, this.NegHSize.y, this.Pos.x, this.Pos.y, this.Scale, this.Scale, this.CurAngle, (int) (this.GlowVal * 255.0f));
        }
    }

    public boolean checkEat(float posX, float posY) {
        if (this.Eaten) {
            return false;
        }
        if (this.Behavior != 0) {
            return false;
        }
        if (this.Type == 4) {
            return false;
        }
        if (this.Pos.distance(posX, posY) > this.Radius + 10.0f) {
            return false;
        }
        this.Eaten = true;
        return true;
    }

    public boolean checkEatIntersect() {
        if (this.Eaten) {
            return false;
        }
        if (this.Behavior != 0) {
            return false;
        }
        if (this.Type == 4) {
            return false;
        }
        if (!LizardView.isRayIntersectsSphere(Actor.TongueStartPos, Actor.TongueNormalizedDir, Actor.EatLength * Actor.EatVal, this.Pos, this.Radius + 10.0f)) {
            return false;
        }
        this.Eaten = true;
        return true;
    }

    public int finishEat() {
        this.Active = false;
        return 0;
    }

    public void setInitPos() {
        this.Pos.set(LizardView.RandomF(-10.0f, 10.0f), LizardView.RandomF(-10.0f, 10.0f));
        this.Pos.normalize();
        this.Pos.multiply(600.0f);
        this.Pos.add(UI.ScreenWidth / 2.0f, UI.ScreenHeight / 2.0f);
        float rad = this.Radius * 1.5f;
        if (this.Pos.x < (-rad)) {
            this.Pos.x = -rad;
        }
        if (this.Pos.y < (-rad)) {
            this.Pos.y = -rad;
        }
        if (this.Pos.x > UI.ScreenWidth + rad) {
            this.Pos.x = UI.ScreenWidth + rad;
        }
        if (this.Pos.y > UI.ScreenHeight + rad) {
            this.Pos.y = UI.ScreenHeight + rad;
        }
    }
}
