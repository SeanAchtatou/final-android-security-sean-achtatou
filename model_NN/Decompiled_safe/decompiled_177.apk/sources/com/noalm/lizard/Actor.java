package com.noalm.lizard;

import android.graphics.Canvas;

public class Actor {
    public static float Angle = 0.0f;
    public static BitmapAnimation Anim = new BitmapAnimation();
    public static final float BORDER = 10.0f;
    public static boolean Blink = false;
    public static boolean BlinkShow = false;
    public static long BlinkTime = 0;
    public static Vector2 CurTailPos = new Vector2(0.0f, 0.0f);
    public static Vector2 Dir = new Vector2(0.0f, 0.0f);
    public static final float EAT_SPEED = 0.17f;
    public static boolean Eat = false;
    private static boolean EatCircle = false;
    private static float EatCircleAlpha = 1.0f;
    private static Vector2 EatCirclePos = new Vector2(0.0f, 0.0f);
    private static float EatCircleScale = 0.1f;
    private static float EatCircleVal = 0.0f;
    public static boolean EatDir = true;
    public static float EatLength = 0.0f;
    public static Vector2 EatPos = new Vector2(0.0f, 0.0f);
    public static float EatVal = 0.0f;
    public static final float HEAD_RADIUS = 40.0f;
    public static Vector2 HeadPos = new Vector2(0.0f, 0.0f);
    public static boolean MoveHead = false;
    public static final float SPEED = 6.0f;
    public static final int STATE_MOVE = 1;
    public static final int STATE_READY = 0;
    public static int Side = 0;
    public static int State = 0;
    public static final float TAIL_DIST = 64.0f;
    public static Vector2 TailPos = new Vector2(0.0f, 0.0f);
    public static int TargetSide = 0;
    public static float TongueAngle = 0.0f;
    public static Vector2 TongueEndPos = new Vector2(0.0f, 0.0f);
    public static Vector2 TongueNormalizedDir = new Vector2(0.0f, 0.0f);
    public static Vector2 TongueStartPos = new Vector2(0.0f, 0.0f);
    public static Vector2 WallIntersectPos = new Vector2(0.0f, 0.0f);

    public static void init() {
        Side = Game.CurSide;
        TargetSide = Side;
        State = 0;
        if (Side == 1) {
            HeadPos.set(UI.ScreenWidth / 2.0f, 10.0f);
            TailPos.set(HeadPos.x, HeadPos.y - 64.0f);
        } else if (Side == 2) {
            HeadPos.set(UI.ScreenWidth / 2.0f, UI.ScreenHeight - 10.0f);
            TailPos.set(HeadPos.x, HeadPos.y + 64.0f);
        } else if (Side == 3) {
            HeadPos.set(10.0f, UI.ScreenHeight / 2.0f);
            TailPos.set(HeadPos.x - 64.0f, HeadPos.y);
        } else if (Side == 4) {
            HeadPos.set(UI.ScreenWidth - 10.0f, UI.ScreenHeight / 2.0f);
            TailPos.set(HeadPos.x + 64.0f, HeadPos.y);
        }
        CurTailPos.set(TailPos);
        Dir.set(0.0f, 0.0f);
        Anim.init(50, false, true, UI.ImgLizard);
        Anim.setCurFrame(1);
        Eat = false;
        EatDir = true;
        EatVal = 0.0f;
        EatLength = 0.0f;
        EatPos.set(0.0f, 0.0f);
        TongueStartPos.set(0.0f, 0.0f);
        TongueEndPos.set(0.0f, 0.0f);
        TongueNormalizedDir.set(0.0f, 0.0f);
        TongueAngle = 0.0f;
        Blink = false;
        BlinkShow = false;
        BlinkTime = 0;
        EatCircle = false;
        EatCircleVal = 0.0f;
        EatCirclePos.set(0.0f, 0.0f);
        EatCircleScale = 0.1f;
        EatCircleAlpha = 1.0f;
    }

    public static void update() {
        if (State == 0) {
            if (Eat) {
                if (EatDir) {
                    EatVal += 0.17f * Game.fTimeMultiplier;
                    if (EatVal > 1.0f) {
                        EatDir = false;
                        EatVal = 1.0f;
                        int i = 0;
                        while (true) {
                            if (i >= Game.NumObjects) {
                                break;
                            } else if (Game.Objects[i].checkEat(EatPos.x, EatPos.y)) {
                                if (Game.Objects[i].Score > 0) {
                                    Game.addScore(28.0f, 255, 255, 0, EatPos.x, EatPos.y, Game.Objects[i].Score);
                                }
                            } else if (Game.Objects[i].checkEatIntersect()) {
                                EatPos.set(LizardView.IntersectionPos);
                                EatLength = TongueStartPos.distance(EatPos);
                                if (Game.Objects[i].Score > 0) {
                                    Game.addScore(28.0f, 255, 255, 0, EatPos.x, EatPos.y, Game.Objects[i].Score);
                                }
                            } else {
                                i++;
                            }
                        }
                        EatCircle = true;
                        EatCircleVal = 0.0f;
                        EatCirclePos.set(EatPos);
                        EatCircleScale = 0.1f;
                        EatCircleAlpha = 1.0f;
                    }
                } else {
                    EatVal -= 0.102000006f * Game.fTimeMultiplier;
                    if (EatVal < 0.0f) {
                        EatDir = false;
                        EatVal = 0.0f;
                        Eat = false;
                        int i2 = 0;
                        while (true) {
                            if (i2 >= Game.NumObjects) {
                                break;
                            } else if (Game.Objects[i2].Eaten) {
                                Game.Objects[i2].finishEat();
                                Sounds.playSound(1, 0, 1.0f);
                                break;
                            } else {
                                i2++;
                            }
                        }
                    }
                }
                TongueEndPos.set(EatPos);
                TongueEndPos.subtract(TongueStartPos);
                TongueEndPos.normalize();
                TongueEndPos.multiply(EatLength * EatVal);
                TongueEndPos.add(TongueStartPos);
                if (EatDir) {
                    int i3 = 0;
                    while (true) {
                        if (i3 >= Game.NumObjects) {
                            break;
                        } else if (Game.Objects[i3].checkEatIntersect()) {
                            EatDir = false;
                            EatVal = 1.0f;
                            EatPos.set(LizardView.IntersectionPos);
                            TongueEndPos.set(EatPos);
                            EatLength = TongueStartPos.distance(EatPos);
                            if (Game.Objects[i3].Score > 0) {
                                Game.addScore(28.0f, 255, 255, 0, EatPos.x, EatPos.y, Game.Objects[i3].Score);
                            }
                            EatCircle = true;
                            EatCircleVal = 0.0f;
                            EatCirclePos.set(EatPos);
                            EatCircleScale = 0.1f;
                            EatCircleAlpha = 1.0f;
                        } else {
                            i3++;
                        }
                    }
                }
            } else if (Game.CurSide != Side) {
                TargetSide = Game.CurSide;
                if (Side == 1) {
                    if (TargetSide == 2) {
                        Dir.set(1.0f, 0.0f);
                    } else if (TargetSide == 3) {
                        Dir.set(-1.0f, 0.0f);
                    } else if (TargetSide == 4) {
                        Dir.set(1.0f, 0.0f);
                    }
                } else if (Side == 2) {
                    if (TargetSide == 1) {
                        Dir.set(1.0f, 0.0f);
                    } else if (TargetSide == 3) {
                        Dir.set(-1.0f, 0.0f);
                    } else if (TargetSide == 4) {
                        Dir.set(1.0f, 0.0f);
                    }
                } else if (Side == 3) {
                    if (TargetSide == 1) {
                        Dir.set(0.0f, -1.0f);
                    } else if (TargetSide == 2) {
                        Dir.set(0.0f, 1.0f);
                    } else if (TargetSide == 4) {
                        Dir.set(0.0f, 1.0f);
                    }
                } else if (Side == 4) {
                    if (TargetSide == 1) {
                        Dir.set(0.0f, -1.0f);
                    } else if (TargetSide == 2) {
                        Dir.set(0.0f, 1.0f);
                    } else if (TargetSide == 3) {
                        Dir.set(0.0f, 1.0f);
                    }
                }
                State = 1;
                MoveHead = true;
            } else if (Anim.getCurFrame() != 1) {
                Anim.update();
            }
        } else if (State == 1) {
            float spd = 6.0f * Game.fTimeMultiplier;
            if (MoveHead) {
                HeadPos.add(Dir.x * spd, Dir.y * spd);
            }
            if (TargetSide == Side) {
                if (MoveHead) {
                    if (Side == 1) {
                        float x = UI.ScreenWidth / 2.0f;
                        if (HeadPos.x > x && Dir.x > 0.0f) {
                            HeadPos.x = x;
                            Dir.set(0.0f, 1.0f);
                            MoveHead = false;
                        } else if (HeadPos.x < x && Dir.x < 0.0f) {
                            HeadPos.x = x;
                            Dir.set(0.0f, 1.0f);
                            MoveHead = false;
                        }
                    } else if (Side == 2) {
                        float x2 = UI.ScreenWidth / 2.0f;
                        if (HeadPos.x > x2 && Dir.x > 0.0f) {
                            HeadPos.x = x2;
                            Dir.set(0.0f, -1.0f);
                            MoveHead = false;
                        } else if (HeadPos.x < x2 && Dir.x < 0.0f) {
                            HeadPos.x = x2;
                            Dir.set(0.0f, -1.0f);
                            MoveHead = false;
                        }
                    } else if (Side == 3) {
                        float y = UI.ScreenHeight / 2.0f;
                        if (HeadPos.y > y && Dir.y > 0.0f) {
                            HeadPos.y = y;
                            Dir.set(1.0f, 0.0f);
                            MoveHead = false;
                        } else if (HeadPos.y < y && Dir.y < 0.0f) {
                            HeadPos.y = y;
                            Dir.set(1.0f, 0.0f);
                            MoveHead = false;
                        }
                    } else if (Side == 4) {
                        float y2 = UI.ScreenHeight / 2.0f;
                        if (HeadPos.y > y2 && Dir.y > 0.0f) {
                            HeadPos.y = y2;
                            Dir.set(-1.0f, 0.0f);
                            MoveHead = false;
                        } else if (HeadPos.y < y2 && Dir.y < 0.0f) {
                            HeadPos.y = y2;
                            Dir.set(-1.0f, 0.0f);
                            MoveHead = false;
                        }
                    }
                }
            } else if (Side == 1) {
                if (HeadPos.x < 10.0f) {
                    Side = 3;
                    HeadPos.x = 10.0f;
                    Dir.set(0.0f, 1.0f);
                } else if (HeadPos.x > UI.ScreenWidth - 10.0f) {
                    Side = 4;
                    HeadPos.x = UI.ScreenWidth - 10.0f;
                    Dir.set(0.0f, 1.0f);
                }
            } else if (Side == 2) {
                if (HeadPos.x < 10.0f) {
                    Side = 3;
                    HeadPos.x = 10.0f;
                    Dir.set(0.0f, -1.0f);
                } else if (HeadPos.x > UI.ScreenWidth - 10.0f) {
                    Side = 4;
                    HeadPos.x = UI.ScreenWidth - 10.0f;
                    Dir.set(0.0f, -1.0f);
                }
            } else if (Side == 3) {
                if (HeadPos.y < 10.0f) {
                    Side = 1;
                    HeadPos.y = 10.0f;
                    Dir.set(1.0f, 0.0f);
                } else if (HeadPos.y > UI.ScreenHeight - 10.0f) {
                    Side = 2;
                    HeadPos.y = UI.ScreenHeight - 10.0f;
                    Dir.set(1.0f, 0.0f);
                }
            } else if (Side == 4) {
                if (HeadPos.y < 10.0f) {
                    Side = 1;
                    HeadPos.y = 10.0f;
                    Dir.set(-1.0f, 0.0f);
                } else if (HeadPos.y > UI.ScreenHeight - 10.0f) {
                    Side = 2;
                    HeadPos.y = UI.ScreenHeight - 10.0f;
                    Dir.set(-1.0f, 0.0f);
                }
            }
            TailPos.set(HeadPos.x - (Dir.x * 64.0f), HeadPos.y - (Dir.y * 64.0f));
            float tail_diff = TailPos.distance(CurTailPos);
            if (tail_diff <= spd) {
                CurTailPos.set(TailPos);
                if (!MoveHead) {
                    State = 0;
                }
            } else {
                CurTailPos.subtract(TailPos);
                CurTailPos.normalize();
                CurTailPos.multiply(tail_diff - spd);
                CurTailPos.add(TailPos);
            }
            Anim.update();
        }
        if (Blink) {
            BlinkTime += Game.DeltaTime;
            if (BlinkTime > 3000) {
                Blink = false;
                BlinkShow = false;
            } else {
                BlinkShow = BlinkTime % 360 > 180;
            }
        }
        if (EatCircle) {
            EatCircleVal += 0.04f * Game.fTimeMultiplier;
            if (EatCircleVal > 1.0f) {
                EatCircle = false;
                EatCircleVal = 0.0f;
            }
            EatCircleScale = 0.1f + (0.9f * EatCircleVal);
            EatCircleScale *= 0.8f;
            if (EatCircleVal > 0.5f) {
                EatCircleAlpha = ((1.0f - EatCircleVal) / 0.5f) * 0.7f;
            } else {
                EatCircleAlpha = 0.7f;
            }
        }
    }

    public static void draw(Canvas canvas) {
        UI.mPaint.setARGB((!Blink || BlinkShow) ? 255 : 50, 255, 255, 255);
        if (Eat) {
            UI.drawBitmapTSRotT(canvas, UI.ImgLizardTongue, -5.0f, 0.0f, TongueStartPos.x, TongueStartPos.y, 0.66f, (EatVal * EatLength) / 50.0f, TongueAngle);
            TailPos.set(CurTailPos);
            TailPos.subtract(HeadPos);
            Angle = TailPos.angle() - 180.0f;
            UI.drawBitmapTSRotT(canvas, UI.ImgLizardEat, -60.0f, -60.0f, HeadPos.x, HeadPos.y, 0.66f, 0.66f, Angle);
        } else {
            TailPos.set(CurTailPos);
            TailPos.subtract(HeadPos);
            Angle = TailPos.angle() - 180.0f;
            UI.drawBitmapTSRotT(canvas, Anim.getCurFrameBmp(), -60.0f, -60.0f, HeadPos.x, HeadPos.y, 0.66f, 0.66f, Angle);
        }
        if (EatCircle) {
            UI.drawBitmapTSTA(canvas, UI.ImgCircleWhite, -48.0f, -48.0f, EatCirclePos.x, EatCirclePos.y, EatCircleScale, EatCircleScale, (int) (255.0f * EatCircleAlpha));
        }
    }

    public static void eat(float posX, float posY) {
        if (State == 0 && !Eat) {
            Eat = true;
            EatDir = true;
            EatVal = 0.0f;
            EatPos.set(posX, posY);
            TongueStartPos.set(HeadPos);
            if (Side == 1) {
                TongueStartPos.y += 28.0f;
            } else if (Side == 2) {
                TongueStartPos.y -= 28.0f;
            } else if (Side == 3) {
                TongueStartPos.x += 28.0f;
            } else if (Side == 4) {
                TongueStartPos.x -= 28.0f;
            }
            if (checkWallIntersect()) {
                EatPos.set(WallIntersectPos);
            }
            EatLength = TongueStartPos.distance(EatPos);
            TongueEndPos.set(EatPos);
            TongueEndPos.subtract(TongueStartPos);
            TongueAngle = TongueEndPos.angle() - 180.0f;
            TongueEndPos.set(TongueStartPos);
            TongueNormalizedDir.set(EatPos);
            TongueNormalizedDir.subtract(TongueStartPos);
            TongueNormalizedDir.normalize();
        }
    }

    public static boolean checkWallIntersect() {
        boolean inter = false;
        float min_dist = 100000.0f;
        for (int i = 0; i < LevelRule.NumWalls; i++) {
            if (LizardView.isLineIntersectsLine(TongueStartPos.x, TongueStartPos.y, EatPos.x, EatPos.y, LevelRule.Walls[i].Point1.x, LevelRule.Walls[i].Point1.y, LevelRule.Walls[i].Point2.x, LevelRule.Walls[i].Point2.y)) {
                if (LizardView.IntersectionDist < min_dist) {
                    min_dist = LizardView.IntersectionDist;
                    WallIntersectPos.set(LizardView.IntersectionPos);
                }
                inter = true;
            }
        }
        return inter;
    }

    public static boolean tokenPickUp(Vector2 _Pos) {
        if (Game.NumLives > 2) {
            return false;
        }
        Game.setTokenPick(_Pos);
        Game.setLizardSkinCircle(2, HeadPos);
        int snd = LizardView.Random(4, 5);
        Sounds.playSound(snd, 0, 1.0f);
        if (snd == 4) {
            Game.addLetters(24.0f, 255, 255, 0, _Pos.x, _Pos.y, "YEAH!");
        } else if (snd == 5) {
            Game.addLetters(24.0f, 255, 255, 0, _Pos.x, _Pos.y, "YUPPIE!");
        }
        return true;
    }

    public static void damage() {
        if (!Blink && Game.NumLives >= 1) {
            Game.setLizardSkinCircle(1, HeadPos);
            int snd = LizardView.Random(2, 3);
            Sounds.playSound(snd, 0, 1.0f);
            if (snd == 2) {
                Game.addLetters(24.0f, 255, 0, 0, HeadPos.x, HeadPos.y, "OUCH!");
            } else if (snd == 3) {
                Game.addLetters(24.0f, 255, 0, 0, HeadPos.x, HeadPos.y, "OUGH!");
            }
            Game.NumLives--;
            Blink = true;
            BlinkTime = 0;
            if (Game.NumLives < 1) {
                Game.finishGame(false);
                LizardView.mVibrator.vibrate(600);
                return;
            }
            LizardView.mVibrator.vibrate(200);
        }
    }

    public static boolean checkClick(float posX, float posY) {
        if (Side == 0) {
            return false;
        }
        return HeadPos.distance(posX, posY) < 40.0f;
    }
}
