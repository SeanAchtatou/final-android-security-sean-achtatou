package com.noalm.lizard;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class LizardView extends View {
    public static boolean AutoConnectFB = true;
    public static boolean AutoSubmitScore = true;
    public static int CurLevel = 0;
    public static long CurTime = 0;
    public static int CurWorld = 0;
    public static long DeltaTime = 20;
    public static final float FSLEEP_TIME = 20.0f;
    public static boolean FirstTime = false;
    public static float IntersectionDist = 0.0f;
    public static Vector2 IntersectionOffset = new Vector2(0.0f, 0.0f);
    public static Vector2 IntersectionPos = new Vector2(0.0f, 0.0f);
    public static boolean MusicEnabled = true;
    public static String Name = "";
    public static int NumWorldsUnlocked = 1;
    public static final int SAVEFILE_VERSION = 1;
    public static final long SLEEP_TIME = 20;
    public static boolean ShowTutorial = true;
    public static boolean SoundEnabled = true;
    public static long TimeStarted = 0;
    public static long TimeUpdated = 0;
    public static WorldInfo[] WorldInfos = new WorldInfo[1];
    public static float fDeltaTime = 0.0f;
    public static float fTimeMultiplier = 1.0f;
    public static Game mGame = new Game();
    public static Lizard mLizard;
    public static Sounds mSounds = null;
    public static UI mUI = new UI();
    public static Vibrator mVibrator;
    public RefreshHandler mRedrawHandler = new RefreshHandler();

    class RefreshHandler extends Handler {
        RefreshHandler() {
        }

        public void handleMessage(Message msg) {
            LizardView.this.update();
            LizardView.this.invalidate();
        }

        public void sleep(long delayMillis) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), delayMillis);
        }
    }

    public LizardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LizardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void init(Lizard lizard) {
        boolean z;
        mLizard = lizard;
        if (loadGame()) {
            z = false;
        } else {
            z = true;
        }
        FirstTime = z;
        setFocusable(true);
        Rect screen_size = new Rect(0, 0, 320, 480);
        getWindowVisibleDisplayFrame(screen_size);
        UI.onSizeChanged((float) screen_size.width(), (float) screen_size.height());
        Resources r = getContext().getResources();
        UI.loadMenus();
        mSounds = new Sounds(getContext());
        mVibrator = (Vibrator) mLizard.getSystemService("vibrator");
        UI.loadGraphics(r);
        CurTime = System.currentTimeMillis();
        TimeStarted = CurTime;
        update();
    }

    public void update() {
        CurTime = System.currentTimeMillis();
        if (CurTime > TimeUpdated + 20) {
            DeltaTime = CurTime - TimeUpdated;
            fDeltaTime = (float) DeltaTime;
            fTimeMultiplier = fDeltaTime / 20.0f;
            TimeUpdated = CurTime;
            if (UI.CurPage == 4) {
                Game.update();
            } else {
                UI.update();
            }
        }
        this.mRedrawHandler.sleep(40 - DeltaTime);
    }

    public void destroy() {
        Game.destroy();
        Sounds.destroy();
    }

    public static boolean loadGame() {
        for (int i = 0; i < 1; i++) {
            WorldInfos[i] = new WorldInfo(i);
        }
        FileStream fs = new FileStream();
        if (!fs.openRead(mLizard, "nlmlizardsaves.dat")) {
            return false;
        }
        ShowTutorial = fs.readBoolean(true, 0);
        SoundEnabled = fs.readBoolean(true, 0);
        MusicEnabled = fs.readBoolean(true, 0);
        AutoConnectFB = fs.readBoolean(true, 0);
        AutoSubmitScore = fs.readBoolean(true, 0);
        Name = fs.readString("", 0);
        NumWorldsUnlocked = fs.readInt(1, 0);
        if (NumWorldsUnlocked > 1) {
            NumWorldsUnlocked = 1;
        }
        for (int i2 = 0; i2 < NumWorldsUnlocked; i2++) {
            WorldInfos[i2].read(fs);
        }
        fs.close();
        CurWorld = NumWorldsUnlocked - 1;
        return true;
    }

    public static void saveGame() {
        FileStream fs = new FileStream();
        if (fs.openWrite(mLizard, "nlmlizardsaves.dat", 1)) {
            fs.writeBoolean(ShowTutorial);
            fs.writeBoolean(SoundEnabled);
            fs.writeBoolean(MusicEnabled);
            fs.writeBoolean(AutoConnectFB);
            fs.writeBoolean(AutoSubmitScore);
            fs.writeString(Name);
            fs.writeInt(NumWorldsUnlocked);
            for (int i = 0; i < NumWorldsUnlocked; i++) {
                WorldInfos[i].write(fs);
            }
            fs.close();
        }
    }

    public static int Random(int down, int up) {
        return ((int) (Math.random() * ((double) ((float) ((up - down) + 1))))) + down;
    }

    public static long Random(long down, long up) {
        return ((long) (Math.random() * ((double) ((float) ((up - down) + 1))))) + down;
    }

    public static float RandomF(float down, float up) {
        return (((float) Math.random()) * (up - down)) + down;
    }

    public static boolean isRayIntersectsSphere(Vector2 ray_start, Vector2 ray_dir, float length, Vector2 sphere_pos, float sphere_rad) {
        IntersectionOffset.set(sphere_pos);
        IntersectionOffset.subtract(ray_start);
        float ray_dist = ray_dir.dot(IntersectionOffset);
        if (ray_dist <= 0.0f || ray_dist - length > sphere_rad) {
            return false;
        }
        float off2 = IntersectionOffset.dot(IntersectionOffset);
        float rad2 = sphere_rad * sphere_rad;
        if (off2 <= rad2) {
            IntersectionPos.set(ray_start);
            IntersectionDist = 0.0f;
            return true;
        }
        float d = rad2 - (off2 - (ray_dist * ray_dist));
        if (d < 0.0f) {
            return false;
        }
        IntersectionDist = (float) (((double) ray_dist) - Math.sqrt((double) d));
        if (IntersectionDist > length) {
            return false;
        }
        IntersectionPos.set(ray_dir);
        IntersectionPos.multiply(IntersectionDist);
        IntersectionPos.add(ray_start);
        IntersectionDist /= length;
        return true;
    }

    /* JADX INFO: Multiple debug info for r1v10 float: [D('post' float), D('y' float)] */
    /* JADX INFO: Multiple debug info for r0v4 float: [D('d' float), D('x' float)] */
    public static boolean isLineIntersectsLine(float l1p1x, float l1p1y, float l1p2x, float l1p2y, float l2p1x, float l2p1y, float l2p2x, float l2p2y) {
        float d = ((l1p1x - l1p2x) * (l2p1y - l2p2y)) - ((l1p1y - l1p2y) * (l2p1x - l2p2x));
        if (d == 0.0f) {
            return false;
        }
        float pre = (l1p1x * l1p2y) - (l1p1y * l1p2x);
        float post = (l2p1x * l2p2y) - (l2p1y * l2p2x);
        float y = ((pre * (l2p1y - l2p2y)) - (post * (l1p1y - l1p2y))) / d;
        float x = (float) ((int) ((((l2p1x - l2p2x) * pre) - ((l1p1x - l1p2x) * post)) / d));
        float y2 = (float) ((int) y);
        if (x < Math.min(l1p1x, l1p2x) - 1.0f || x > Math.max(l1p1x, l1p2x) + 1.0f || x < Math.min(l2p1x, l2p2x) - 1.0f || x > Math.max(l2p1x, l2p2x) + 1.0f || y2 < Math.min(l1p1y, l1p2y) - 1.0f || y2 > Math.max(l1p1y, l1p2y) + 1.0f || y2 < Math.min(l2p1y, l2p2y) - 1.0f || y2 > Math.max(l2p1y, l2p2y) + 1.0f) {
            return false;
        }
        IntersectionPos.set(x, y2);
        IntersectionDist = Vector2.length(l1p1x - x, l1p1y - y2);
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x005d A[SYNTHETIC, Splitter:B:21:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0066 A[SYNTHETIC, Splitter:B:27:0x0066] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x006e A[SYNTHETIC, Splitter:B:32:0x006e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getWebPage(java.lang.String r14) {
        /*
            r13 = 0
            r4 = 0
            org.apache.http.impl.client.DefaultHttpClient r1 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ IOException -> 0x0059, URISyntaxException -> 0x0062, all -> 0x006b }
            r1.<init>()     // Catch:{ IOException -> 0x0059, URISyntaxException -> 0x0062, all -> 0x006b }
            org.apache.http.client.methods.HttpGet r8 = new org.apache.http.client.methods.HttpGet     // Catch:{ IOException -> 0x0059, URISyntaxException -> 0x0062, all -> 0x006b }
            r8.<init>()     // Catch:{ IOException -> 0x0059, URISyntaxException -> 0x0062, all -> 0x006b }
            java.net.URI r11 = new java.net.URI     // Catch:{ IOException -> 0x0059, URISyntaxException -> 0x0062, all -> 0x006b }
            r11.<init>(r14)     // Catch:{ IOException -> 0x0059, URISyntaxException -> 0x0062, all -> 0x006b }
            r8.setURI(r11)     // Catch:{ IOException -> 0x0059, URISyntaxException -> 0x0062, all -> 0x006b }
            org.apache.http.HttpResponse r9 = r1.execute(r8)     // Catch:{ IOException -> 0x0059, URISyntaxException -> 0x0062, all -> 0x006b }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0059, URISyntaxException -> 0x0062, all -> 0x006b }
            java.io.InputStreamReader r11 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0059, URISyntaxException -> 0x0062, all -> 0x006b }
            org.apache.http.HttpEntity r12 = r9.getEntity()     // Catch:{ IOException -> 0x0059, URISyntaxException -> 0x0062, all -> 0x006b }
            java.io.InputStream r12 = r12.getContent()     // Catch:{ IOException -> 0x0059, URISyntaxException -> 0x0062, all -> 0x006b }
            r11.<init>(r12)     // Catch:{ IOException -> 0x0059, URISyntaxException -> 0x0062, all -> 0x006b }
            r5.<init>(r11)     // Catch:{ IOException -> 0x0059, URISyntaxException -> 0x0062, all -> 0x006b }
            r3 = 0
            java.lang.StringBuffer r10 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x0081, URISyntaxException -> 0x007d, all -> 0x007a }
            java.lang.String r11 = ""
            r10.<init>(r11)     // Catch:{ IOException -> 0x0081, URISyntaxException -> 0x007d, all -> 0x007a }
            java.lang.String r6 = ""
            java.lang.String r11 = "line.separator"
            java.lang.String r0 = java.lang.System.getProperty(r11)     // Catch:{ IOException -> 0x0081, URISyntaxException -> 0x007d, all -> 0x007a }
        L_0x003a:
            java.lang.String r6 = r5.readLine()     // Catch:{ IOException -> 0x0081, URISyntaxException -> 0x007d, all -> 0x007a }
            if (r6 != 0) goto L_0x004f
            r5.close()     // Catch:{ IOException -> 0x0081, URISyntaxException -> 0x007d, all -> 0x007a }
            java.lang.String r7 = r10.toString()     // Catch:{ IOException -> 0x0081, URISyntaxException -> 0x007d, all -> 0x007a }
            if (r5 == 0) goto L_0x004c
            r5.close()     // Catch:{ IOException -> 0x0072 }
        L_0x004c:
            r4 = r5
            r11 = r7
        L_0x004e:
            return r11
        L_0x004f:
            if (r3 == 0) goto L_0x0054
            r10.append(r0)     // Catch:{ IOException -> 0x0081, URISyntaxException -> 0x007d, all -> 0x007a }
        L_0x0054:
            r10.append(r6)     // Catch:{ IOException -> 0x0081, URISyntaxException -> 0x007d, all -> 0x007a }
            r3 = 1
            goto L_0x003a
        L_0x0059:
            r11 = move-exception
            r2 = r11
        L_0x005b:
            if (r4 == 0) goto L_0x0060
            r4.close()     // Catch:{ IOException -> 0x0074 }
        L_0x0060:
            r11 = r13
            goto L_0x004e
        L_0x0062:
            r11 = move-exception
            r2 = r11
        L_0x0064:
            if (r4 == 0) goto L_0x0069
            r4.close()     // Catch:{ IOException -> 0x0076 }
        L_0x0069:
            r11 = r13
            goto L_0x004e
        L_0x006b:
            r11 = move-exception
        L_0x006c:
            if (r4 == 0) goto L_0x0071
            r4.close()     // Catch:{ IOException -> 0x0078 }
        L_0x0071:
            throw r11
        L_0x0072:
            r11 = move-exception
            goto L_0x004c
        L_0x0074:
            r11 = move-exception
            goto L_0x0060
        L_0x0076:
            r11 = move-exception
            goto L_0x0069
        L_0x0078:
            r12 = move-exception
            goto L_0x0071
        L_0x007a:
            r11 = move-exception
            r4 = r5
            goto L_0x006c
        L_0x007d:
            r11 = move-exception
            r2 = r11
            r4 = r5
            goto L_0x0064
        L_0x0081:
            r11 = move-exception
            r2 = r11
            r4 = r5
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.noalm.lizard.LizardView.getWebPage(java.lang.String):java.lang.String");
    }

    public static String padString(String source) {
        int padLength = 16 - (source.length() % 16);
        for (int i = 0; i < padLength; i++) {
            source = String.valueOf(source) + ' ';
        }
        return source;
    }

    public static String encrypt(String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        IvParameterSpec ivspec = new IvParameterSpec("fedcba9876543210".getBytes());
        SecretKeySpec keyspec = new SecretKeySpec("0123456789abcdef".getBytes(), "AES");
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            try {
                cipher.init(1, keyspec, ivspec);
                return cipher.doFinal(padString(str).getBytes()).toString();
            } catch (Exception e) {
                return "";
            }
        } catch (NoSuchAlgorithmException e2) {
            return "";
        } catch (NoSuchPaddingException e3) {
            return "";
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == 0) {
            UI.onActionDown(event.getX(), event.getY());
        } else if (event.getAction() == 1) {
            UI.onActionUp(event.getX(), event.getY());
        } else if (event.getAction() == 2) {
            UI.onActionMove(event.getX(), event.getY());
        }
        return true;
    }

    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        UI.onSizeChanged((float) w, (float) h);
        UI.loadMenus();
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        UI.draw(canvas);
    }
}
