package com.noalm.lizard;

import android.os.Bundle;
import android.widget.Toast;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;
import org.json.JSONException;
import org.json.JSONObject;

public class FBManager {
    public static Facebook API = null;
    public static String AccessToken = null;
    public static String AppID = "";
    public static boolean Authorized = false;
    public static FBUser Me = null;

    public FBManager(String app_id) {
        AppID = app_id;
        API = new Facebook(AppID);
    }

    public static boolean authorize(String[] permissions) {
        if (API == null) {
            return false;
        }
        Authorized = false;
        AccessToken = null;
        Me = null;
        API.authorize(LizardView.mLizard, permissions, new Facebook.DialogListener() {
            public void onComplete(Bundle values) {
                FBManager.Authorized = true;
                FBManager.AccessToken = FBManager.API.getAccessToken();
                if (FBManager.getMyFBInfo()) {
                    FBManager.getScores();
                } else if (LizardView.Name != "") {
                    Toast.makeText(LizardView.mLizard, "Welcome, " + LizardView.Name, 1).show();
                }
            }

            public void onFacebookError(FacebookError error) {
            }

            public void onError(DialogError e) {
            }

            public void onCancel() {
            }
        });
        return Authorized;
    }

    public static boolean getMyFBInfo() {
        Me = null;
        try {
            try {
                JSONObject response = Util.parseJson(API.request("me"));
                if (response == null) {
                    return false;
                }
                Me = new FBUser(response);
                Toast.makeText(LizardView.mLizard, "Welcome, " + Me.FirstName, 1).show();
                LizardView.Name = Me.FirstName;
                LizardView.saveGame();
                return true;
            } catch (JSONException e) {
                return false;
            } catch (FacebookError e2) {
                return false;
            }
        } catch (Exception e3) {
            return false;
        }
    }

    public static boolean getScores() {
        if (Me == null) {
            return false;
        }
        if (AccessToken == null) {
            return false;
        }
        String page = LizardView.getWebPage("http://www.noalm.com/lizard.php?" + ("act=get&fbid=" + Me.ID));
        if (page == null) {
            return false;
        }
        if (page.charAt(0) != '0') {
            return false;
        }
        String[] p = page.split(",");
        if (p.length < 1) {
            return false;
        }
        for (int i = 0; i < 1; i++) {
            LizardView.WorldInfos[i].Place = Integer.parseInt(p[(i * 2) + 1].trim());
            LizardView.WorldInfos[i].Highscore = Integer.parseInt(p[(i * 2) + 2].trim());
        }
        LizardView.saveGame();
        return true;
    }

    public static boolean postScores(int world, int score) {
        if (Me == null) {
            return false;
        }
        if (AccessToken == null) {
            return false;
        }
        String page = LizardView.getWebPage("http://www.noalm.com/lizard.php?" + ("act=post&fbid=" + Me.ID + "&w=" + Integer.toString(world) + "&s=" + Integer.toString(score)));
        if (page == null) {
            return false;
        }
        if (page.charAt(0) != '0') {
            return false;
        }
        String[] p = page.split(",");
        if (p.length < 1) {
            return false;
        }
        for (int i = 0; i < 1; i++) {
            LizardView.WorldInfos[i].Place = Integer.parseInt(p[(i * 2) + 1].trim());
            LizardView.WorldInfos[i].Highscore = Integer.parseInt(p[(i * 2) + 2].trim());
        }
        LizardView.saveGame();
        Toast.makeText(LizardView.mLizard, "Score submitted", 1).show();
        return true;
    }
}
