package com.noalm.lizard;

public class WorldInfo {
    public static final float LEVELS_COLUMN_OFFSET = 80.0f;
    public static final int LEVELS_PER_COLUMN = 2;
    public static final int LEVELS_PER_ROW = 3;
    public static final int LEVELS_PER_WORLD = 6;
    public static final float LEVELS_ROW_OFFSET = 110.0f;
    public static final float[][] LEVELS_SCORE_GOAL = {new float[]{12000.0f, 11000.0f, 10000.0f, 10000.0f, 10000.0f, 10000.0f}, new float[]{10000.0f, 10000.0f, 10000.0f, 10000.0f, 10000.0f, 10000.0f}};
    public static final int NUM_WORLDS = 1;
    public static final int[] WORLDS_PERCENT_GOAL = {660, 700};
    public static final String[] WORLD_NAMES = {"SANDY", "PLASH"};
    public int Highscore = 0;
    public int[] LevelsPercentage = new int[6];
    public int[] LevelsScore = new int[6];
    public int LevelsUnlocked = 1;
    public int Place = 0;
    private int WorldInd = 0;
    public int WorldPercentage = 0;
    public int WorldScore = 0;

    public WorldInfo(int _WorldInd) {
        clear();
        this.WorldInd = _WorldInd;
    }

    public void clear() {
        this.LevelsUnlocked = 1;
        for (int i = 0; i < 6; i++) {
            this.LevelsScore[i] = 0;
        }
        this.Place = 0;
        this.Highscore = 0;
        this.WorldScore = 0;
        for (int i2 = 0; i2 < 6; i2++) {
            this.LevelsPercentage[i2] = 0;
        }
        this.WorldPercentage = 0;
    }

    public boolean read(FileStream in) {
        clear();
        try {
            this.LevelsUnlocked = in.readInt(1, 0);
            if (this.LevelsUnlocked > 6) {
                this.LevelsUnlocked = 6;
            } else if (this.LevelsUnlocked < 1) {
                this.LevelsUnlocked = 1;
            }
            for (int i = 0; i < this.LevelsUnlocked; i++) {
                this.LevelsScore[i] = in.readInt(0, 0);
            }
            this.Place = in.readInt(0, 0);
            this.Highscore = in.readInt(0, 0);
            countScoresAndPercentages();
            return true;
        } catch (Exception e) {
            Exception exc = e;
            return false;
        }
    }

    public boolean write(FileStream out) {
        try {
            out.writeInt(this.LevelsUnlocked);
            for (int i = 0; i < this.LevelsUnlocked; i++) {
                out.writeInt(this.LevelsScore[i]);
            }
            out.writeInt(this.Place);
            out.writeInt(this.Highscore);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean setScore(int curLevel, int score) {
        if (curLevel >= this.LevelsUnlocked) {
            return false;
        }
        if (score <= this.LevelsScore[curLevel]) {
            return false;
        }
        this.LevelsScore[curLevel] = score;
        countScoresAndPercentages();
        return true;
    }

    public int getLevelScore(int curLevel) {
        if (curLevel >= this.LevelsUnlocked) {
            return 0;
        }
        return this.LevelsScore[curLevel];
    }

    public boolean isLevelScoreGoal(int curLevel) {
        if (curLevel >= this.LevelsUnlocked) {
            return false;
        }
        return this.LevelsPercentage[curLevel] >= 100;
    }

    public int getLevelScoreGoal(int curLevel) {
        if (curLevel >= this.LevelsUnlocked) {
            return 0;
        }
        return (int) LEVELS_SCORE_GOAL[this.WorldInd][curLevel];
    }

    public boolean unlockNext(int curLevel) {
        if (curLevel != this.LevelsUnlocked - 1 || this.LevelsUnlocked >= 6) {
            return false;
        }
        this.LevelsScore[this.LevelsUnlocked] = 0;
        this.LevelsPercentage[this.LevelsUnlocked] = 0;
        this.LevelsUnlocked++;
        return true;
    }

    public boolean isWorldScoreGoal() {
        if (this.LevelsUnlocked < 6) {
            return false;
        }
        if (this.LevelsScore[5] < 1) {
            return false;
        }
        return this.WorldPercentage >= WORLDS_PERCENT_GOAL[this.WorldInd];
    }

    public String getWorldPercentageStr() {
        return String.valueOf(Integer.toString(this.WorldPercentage)) + " / " + Integer.toString(WORLDS_PERCENT_GOAL[this.WorldInd]);
    }

    private void countScoresAndPercentages() {
        this.WorldScore = 0;
        this.WorldPercentage = 0;
        for (int i = 0; i < this.LevelsUnlocked; i++) {
            this.WorldScore += this.LevelsScore[i];
            this.LevelsPercentage[i] = (int) ((((float) this.LevelsScore[i]) / LEVELS_SCORE_GOAL[this.WorldInd][i]) * 100.0f);
            this.WorldPercentage += this.LevelsPercentage[i];
        }
    }
}
