package com.noalm.lizard;

import android.graphics.Canvas;
import android.graphics.Paint;

public class Game {
    public static int CurSide = 2;
    public static long DeltaTime = 0;
    private static final float FMOVE_LIZARD_PERIOD = 100.0f;
    public static final int LIZARD_CIRCLE_GREEN = 0;
    public static final int LIZARD_CIRCLE_RED = 1;
    public static final int LIZARD_CIRCLE_YELLOW = 2;
    private static Letters[] LettersStr = new Letters[8];
    public static boolean LevelUnlocked = false;
    private static boolean LizardCircle = false;
    private static float LizardCircleAlpha = 1.0f;
    private static Vector2 LizardCirclePos = new Vector2(0.0f, 0.0f);
    private static float LizardCircleScale = 0.1f;
    private static int LizardCircleType = 0;
    private static float LizardCircleVal = 0.0f;
    private static final int MOVE_LIZARD_PERIOD = 100;
    private static final float MOVE_LIZARD_RADIUS = 10.0f;
    private static boolean MoveLizard = false;
    private static float MoveLizardAngle = 0.0f;
    private static Vector2 MoveLizardDir = new Vector2(0.0f, 0.0f);
    private static Vector2 MoveLizardPos = new Vector2(0.0f, 0.0f);
    private static long MoveLizardTime = 0;
    private static int MoveToSide = 0;
    private static final int NUM_LETTERS = 8;
    private static final int NUM_OBJECTS = 24;
    public static boolean NewHighscore = false;
    private static int NumLettersStr = 0;
    public static int NumLives = 3;
    public static int NumObjects = 0;
    public static Object[] Objects = new Object[NUM_OBJECTS];
    public static int Percent = 0;
    public static float PercentBckgWidth = 0.0f;
    public static String PercentStr = "";
    public static final int SIDE_DOWN = 2;
    public static final int SIDE_LEFT = 3;
    public static final int SIDE_RIGHT = 4;
    public static final int SIDE_UP = 1;
    public static int Score = 0;
    public static String ScoreStr = "";
    private static boolean TokenPick = false;
    private static Vector2 TokenPickEnd = new Vector2(0.0f, 0.0f);
    private static float TokenPickLength = 0.0f;
    private static Vector2 TokenPickPos = new Vector2(0.0f, 0.0f);
    private static Vector2 TokenPickStart = new Vector2(0.0f, 0.0f);
    private static float TokenPickVal = 0.0f;
    public static boolean WorldUnlocked = false;
    public static float fTimeMultiplier = 1.0f;
    public static Actor mActor = new Actor();
    public static LevelRule mLevelRule = new LevelRule();

    public Game() {
        for (int i = 0; i < NUM_OBJECTS; i++) {
            Objects[i] = new Object();
        }
        for (int i2 = 0; i2 < 8; i2++) {
            LettersStr[i2] = new Letters();
        }
    }

    public static void init() {
        destroy();
        Actor.init();
        LevelRule.init(LizardView.CurWorld, LizardView.CurLevel);
    }

    public static void update() {
        DeltaTime = LizardView.DeltaTime;
        fTimeMultiplier = LizardView.fTimeMultiplier;
        Actor.update();
        for (int i = 0; i < UI.NumDynamics; i++) {
            UI.Dynamics[i].update();
        }
        if (MoveLizard) {
            MoveLizardTime += DeltaTime;
            if (MoveLizardTime > 100) {
                MoveLizardTime = 100;
            }
        } else if (MoveLizardTime > 0) {
            MoveLizardTime -= DeltaTime;
            if (MoveLizardTime < 0) {
                MoveLizardTime = 0;
            }
        }
        int i2 = 0;
        while (i2 < NumObjects) {
            if (!Objects[i2].update()) {
                Object t = Objects[i2];
                Objects[i2] = Objects[NumObjects - 1];
                Objects[NumObjects - 1] = t;
                NumObjects--;
                i2--;
            }
            i2++;
        }
        LevelRule.updateSpawnToken();
        LevelRule.updateSpawnObject();
        LevelRule.updateSpawnSting();
        if (TokenPick) {
            TokenPickVal += fTimeMultiplier * 0.04f;
            if (TokenPickVal > 1.0f) {
                TokenPick = false;
                TokenPickVal = 1.0f;
                NumLives++;
            }
            TokenPickPos.set(TokenPickEnd);
            TokenPickPos.subtract(TokenPickStart);
            TokenPickPos.normalize();
            TokenPickPos.multiply(TokenPickLength * TokenPickVal);
            TokenPickPos.add(TokenPickStart);
        }
        if (LizardCircle) {
            LizardCircleVal += fTimeMultiplier * 0.04f;
            if (LizardCircleVal > 1.0f) {
                LizardCircle = false;
                LizardCircleVal = 0.0f;
            }
            LizardCircleScale = 0.1f + (0.9f * LizardCircleVal);
            LizardCircleScale *= 0.8f;
            if (LizardCircleVal > 0.5f) {
                LizardCircleAlpha = ((1.0f - LizardCircleVal) / 0.5f) * 0.7f;
            } else {
                LizardCircleAlpha = 0.7f;
            }
        }
        int i3 = 0;
        while (i3 < NumLettersStr) {
            if (!LettersStr[i3].update()) {
                Letters t2 = LettersStr[i3];
                LettersStr[i3] = LettersStr[NumLettersStr - 1];
                LettersStr[NumLettersStr - 1] = t2;
                NumLettersStr--;
                i3--;
            }
            i3++;
        }
    }

    public static void draw(Canvas canvas) {
        if (LizardCircle) {
            if (LizardCircleType == 0) {
                UI.drawBitmapTSTA(canvas, UI.ImgCircleGreen, -110.0f, -110.0f, LizardCirclePos.x, LizardCirclePos.y, LizardCircleScale, LizardCircleScale, (int) (255.0f * LizardCircleAlpha));
            } else if (LizardCircleType == 1) {
                UI.drawBitmapTSTA(canvas, UI.ImgCircleRed, -110.0f, -110.0f, LizardCirclePos.x, LizardCirclePos.y, LizardCircleScale, LizardCircleScale, (int) (255.0f * LizardCircleAlpha));
            } else if (LizardCircleType == 2) {
                UI.drawBitmapTSTA(canvas, UI.ImgCircleYellow, -110.0f, -110.0f, LizardCirclePos.x, LizardCirclePos.y, LizardCircleScale, LizardCircleScale, (int) (255.0f * LizardCircleAlpha));
            }
        }
        Actor.draw(canvas);
        UI.mPaint.setARGB(255, 255, 255, 255);
        for (int i = 0; i < LevelRule.NumWalls; i++) {
            LevelRule.Walls[i].draw(canvas);
        }
        for (int i2 = 0; i2 < NumObjects; i2++) {
            Objects[i2].draw(canvas);
        }
        UI.mPaint.setTextAlign(Paint.Align.CENTER);
        for (int i3 = 0; i3 < NumLettersStr; i3++) {
            LettersStr[i3].draw(canvas);
        }
        if (MoveLizardTime > 0) {
            float a = ((float) MoveLizardTime) / FMOVE_LIZARD_PERIOD;
            UI.mPaint.setARGB((int) (120.0f * a), 0, 0, 0);
            canvas.drawRect(0.0f, 0.0f, UI.ScreenWidth, UI.ScreenHeight, UI.mPaint);
            if (Actor.Side != 1) {
                UI.drawBitmapTSRotTA(canvas, UI.ImgLizardSkin, -100.0f, -106.0f, UI.ScreenWidth / 2.0f, 0.0f, 0.7f, 0.7f, 180.0f, MoveToSide == 1 ? (int) (255.0f * a) : (int) (80.0f * a));
            }
            if (Actor.Side != 2) {
                UI.drawBitmapTSRotTA(canvas, UI.ImgLizardSkin, -100.0f, -106.0f, UI.ScreenWidth / 2.0f, UI.ScreenHeight, 0.7f, 0.7f, 0.0f, MoveToSide == 2 ? (int) (255.0f * a) : (int) (80.0f * a));
            }
            if (Actor.Side != 3) {
                UI.drawBitmapTSRotTA(canvas, UI.ImgLizardSkin, -100.0f, -106.0f, 0.0f, UI.ScreenHeight / 2.0f, 0.7f, 0.7f, 90.0f, MoveToSide == 3 ? (int) (255.0f * a) : (int) (80.0f * a));
            }
            if (Actor.Side != 4) {
                UI.drawBitmapTSRotTA(canvas, UI.ImgLizardSkin, -100.0f, -106.0f, UI.ScreenWidth, UI.ScreenHeight / 2.0f, 0.7f, 0.7f, -90.0f, MoveToSide == 4 ? (int) (255.0f * a) : (int) (80.0f * a));
            }
        }
    }

    public static void drawScore(Canvas canvas) {
        UI.drawBitmapTSTA(canvas, UI.ImgPercentBckg, -47.0f, -32.0f, UI.ScreenWidth / 2.0f, 24.0f + UI.ScreenHeight, 0.8f, 0.8f, 200);
        if (PercentBckgWidth > 0.0f) {
            UI.mPaint.setARGB(150, 0, 0, 0);
            canvas.drawRect(((UI.ScreenWidth / 2.0f) + 37.0f) - PercentBckgWidth, UI.ScreenHeight, 37.0f + (UI.ScreenWidth / 2.0f), 1.0f + UI.ScreenHeight + 24.0f, UI.mPaint);
        }
        UI.mPaint.setTextAlign(Paint.Align.CENTER);
        UI.mPaint.setTextSize(18.0f);
        UI.mPaint.setARGB(255, 0, 0, 0);
        canvas.drawText(PercentStr, (UI.ScreenWidth / 2.0f) - 1.0f, UI.ScreenHeight + 22.0f, UI.mPaint);
        UI.mPaint.setARGB(255, 255, 255, 255);
        canvas.drawText(PercentStr, UI.ScreenWidth / 2.0f, UI.ScreenHeight + 21.0f, UI.mPaint);
        if (TokenPick) {
            UI.drawBitmapTSTA(canvas, UI.ImgToken, -24.0f, -24.0f, TokenPickPos.x, TokenPickPos.y, 0.65f + ((1.0f - TokenPickVal) * 0.15f), 0.65f, 255);
        }
        if (NumLives > 0) {
            UI.drawBitmapTSTA(canvas, UI.ImgToken, -24.0f, -24.0f, 13.0f, UI.ScreenHeight + 12.0f, 0.65f, 0.65f, 255);
        }
        if (NumLives > 1) {
            UI.drawBitmapTSTA(canvas, UI.ImgToken, -24.0f, -24.0f, 36.0f, UI.ScreenHeight + 12.0f, 0.65f, 0.65f, 255);
        }
        if (NumLives > 2) {
            UI.drawBitmapTSTA(canvas, UI.ImgToken, -24.0f, -24.0f, 59.0f, UI.ScreenHeight + 12.0f, 0.65f, 0.65f, 255);
        }
        UI.mPaint.setTextAlign(Paint.Align.RIGHT);
        UI.mPaint.setTextSize(24.0f);
        UI.mPaint.setARGB(255, 255, 255, 0);
        canvas.drawText(ScoreStr, UI.ScreenWidth - 5.0f, UI.ScreenHeight + 20.0f, UI.mPaint);
    }

    public static void drawScore(Canvas canvas, int score) {
        UI.mPaint.setTextAlign(Paint.Align.RIGHT);
        UI.mPaint.setTextSize(24.0f);
        UI.mPaint.setARGB(255, 255, 255, 0);
        canvas.drawText(Integer.toString(score), UI.ScreenWidth - 5.0f, UI.ScreenHeight + 20.0f, UI.mPaint);
    }

    public static void destroy() {
        CurSide = 2;
        LevelRule.destroy();
        NumObjects = 0;
        NumLettersStr = 0;
        MoveLizard = false;
        MoveLizardTime = 0;
        MoveToSide = 0;
        NewHighscore = false;
        LevelUnlocked = false;
        WorldUnlocked = false;
        Score = 0;
        Percent = 0;
        NumLives = 3;
        ScoreStr = "0";
        PercentStr = "0%";
        PercentBckgWidth = 74.0f;
        TokenPick = false;
        TokenPickVal = 0.0f;
        TokenPickLength = 0.0f;
        TokenPickStart.set(0.0f, 0.0f);
        TokenPickEnd.set(0.0f, 0.0f);
        TokenPickPos.set(0.0f, 0.0f);
        LizardCircle = false;
        LizardCircleType = 0;
        LizardCircleVal = 0.0f;
        LizardCirclePos.set(0.0f, 0.0f);
        LizardCircleScale = 0.1f;
        LizardCircleAlpha = 1.0f;
    }

    public static void checkActionDown(float posX, float posY) {
        if (Actor.checkClick(posX, posY)) {
            MoveLizard = true;
            MoveLizardTime = 0;
            MoveToSide = Actor.Side;
            MoveLizardPos.set(posX, posY);
            return;
        }
        Actor.eat(posX, posY);
    }

    public static void checkActionUp(float posX, float posY) {
        if (MoveLizard) {
            MoveLizard = false;
            if (MoveToSide > 0 && MoveToSide != Actor.Side) {
                CurSide = MoveToSide;
            }
        }
    }

    public static void checkActionMove(float posX, float posY) {
        if (MoveLizard) {
            getMoveToSide(posX, posY);
        }
    }

    public static void getMoveToSide(float posX, float posY) {
        MoveToSide = 0;
        if (MoveLizardPos.distance(posX, posY) >= 10.0f) {
            MoveLizardDir.set(posX, posY);
            MoveLizardDir.subtract(MoveLizardPos);
            MoveLizardAngle = MoveLizardDir.angle();
            if (MoveLizardAngle >= 315.0f || MoveLizardAngle < 45.0f) {
                MoveToSide = 1;
            } else if (MoveLizardAngle >= 45.0f && MoveLizardAngle < 135.0f) {
                MoveToSide = 4;
            } else if (MoveLizardAngle < 135.0f || MoveLizardAngle >= 225.0f) {
                MoveToSide = 3;
            } else {
                MoveToSide = 2;
            }
        }
    }

    public static boolean addObject(int _RectInd, int _Type, float _MaxSpeedMpl, float _AccelerationMpl, long _LifeTime) {
        if (NumObjects >= NUM_OBJECTS) {
            return false;
        }
        Objects[NumObjects].init(_Type, _MaxSpeedMpl, _AccelerationMpl, _LifeTime, _RectInd);
        NumObjects++;
        return true;
    }

    public static boolean addToken(float _MaxSpeedMpl) {
        if (NumObjects >= NUM_OBJECTS) {
            return false;
        }
        Objects[NumObjects].init(4, _MaxSpeedMpl, 1.0f, 0, 0);
        NumObjects++;
        return true;
    }

    public static boolean addScore(float _Size, int _R, int _G, int _B, float posX, float posY, int _Score) {
        Score += _Score;
        Percent = (int) ((((float) Score) / ((float) LizardView.WorldInfos[LizardView.CurWorld].getLevelScoreGoal(LizardView.CurLevel))) * FMOVE_LIZARD_PERIOD);
        ScoreStr = Integer.toString(Score);
        PercentStr = String.valueOf(Integer.toString(Percent)) + "%";
        PercentBckgWidth = 74.0f * (1.0f - (((float) Math.min((int) MOVE_LIZARD_PERIOD, Percent)) / FMOVE_LIZARD_PERIOD));
        return addLetters(_Size, _R, _G, _B, posX, posY, Integer.toString(_Score));
    }

    public static boolean addLetters(float _Size, int _R, int _G, int _B, float posX, float posY, String str) {
        if (NumLettersStr >= 8) {
            return false;
        }
        LettersStr[NumLettersStr].init(_Size, _R, _G, _B, posX, posY, str);
        NumLettersStr++;
        return true;
    }

    public static void setTokenPick(Vector2 _StartPos) {
        TokenPick = true;
        TokenPickVal = 0.0f;
        TokenPickStart.set(_StartPos);
        if (NumLives == 1) {
            TokenPickEnd.set(36.0f, UI.ScreenHeight + 12.0f);
        }
        if (NumLives == 2) {
            TokenPickEnd.set(59.0f, UI.ScreenHeight + 12.0f);
        }
        TokenPickPos.set(TokenPickStart);
        TokenPickLength = TokenPickEnd.distance(TokenPickStart);
    }

    public static void setLizardSkinCircle(int _LizardSkinCircleType, Vector2 _Pos) {
        LizardCircle = true;
        LizardCircleType = _LizardSkinCircleType;
        LizardCircleVal = 0.0f;
        LizardCirclePos.set(_Pos);
        LizardCircleScale = 0.1f;
        LizardCircleAlpha = 1.0f;
    }

    public static void finishGame(boolean fromQuitMenu) {
        NewHighscore = false;
        LevelUnlocked = false;
        WorldUnlocked = false;
        if (LizardView.WorldInfos[LizardView.CurWorld].setScore(LizardView.CurLevel, Score)) {
            NewHighscore = true;
            if (LizardView.WorldInfos[LizardView.CurWorld].isLevelScoreGoal(LizardView.CurLevel) && LizardView.WorldInfos[LizardView.CurWorld].unlockNext(LizardView.CurLevel)) {
                LevelUnlocked = true;
            }
            if (LizardView.WorldInfos[LizardView.CurWorld].isWorldScoreGoal() && LizardView.CurWorld >= LizardView.NumWorldsUnlocked - 1 && LizardView.NumWorldsUnlocked < 1) {
                LizardView.WorldInfos[LizardView.NumWorldsUnlocked].clear();
                LizardView.NumWorldsUnlocked++;
                WorldUnlocked = true;
            }
            LizardView.saveGame();
        }
        UI.FinishedScore = 0;
        UI.FinishedGoal = LizardView.WorldInfos[LizardView.CurWorld].getLevelScoreGoal(LizardView.CurLevel);
        UI.FinishedHighscore = LizardView.WorldInfos[LizardView.CurWorld].getLevelScore(LizardView.CurLevel);
        UI.FinishedPercent = 0;
        UI.FinishedRG.set(255.0f, 0.0f);
        UI.FinishedHighscoreScale = 0.0f;
        UI.goToPage(8, fromQuitMenu, true);
    }
}
