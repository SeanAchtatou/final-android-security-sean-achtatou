package com.noalm.lizard;

public class Vector2 {
    public static final float DEGRAD = 57.295776f;
    public float x;
    public float y;

    public Vector2(float newX, float newY) {
        this.x = newX;
        this.y = newY;
    }

    public Vector2(Vector2 v) {
        this.x = v.x;
        this.y = v.y;
    }

    public void set(float newX, float newY) {
        this.x = newX;
        this.y = newY;
    }

    public void set(Vector2 v) {
        this.x = v.x;
        this.y = v.y;
    }

    public void add(float addX, float addY) {
        this.x += addX;
        this.y += addY;
    }

    public void add(Vector2 v) {
        this.x += v.x;
        this.y += v.y;
    }

    public void subtract(float subX, float subY) {
        this.x -= subX;
        this.y -= subY;
    }

    public void subtract(Vector2 v) {
        this.x -= v.x;
        this.y -= v.y;
    }

    public void multiply(float l) {
        this.x *= l;
        this.y *= l;
    }

    public void divide(float l) {
        this.x /= l;
        this.y /= l;
    }

    public float length() {
        return (float) Math.sqrt((double) ((this.x * this.x) + (this.y * this.y)));
    }

    public static float length(float lx, float ly) {
        return (float) Math.sqrt((double) ((lx * lx) + (ly * ly)));
    }

    public float distance(float dx, float dy) {
        return (float) Math.sqrt((double) (((dx - this.x) * (dx - this.x)) + ((dy - this.y) * (dy - this.y))));
    }

    public float distance(Vector2 v) {
        return (float) Math.sqrt((double) (((v.x - this.x) * (v.x - this.x)) + ((v.y - this.y) * (v.y - this.y))));
    }

    public void normalize() {
        float l = length();
        if (l > 0.0f) {
            this.x /= l;
            this.y /= l;
            return;
        }
        this.x = 0.0f;
        this.y = 1.0f;
    }

    public float angle() {
        float ang = (((float) Math.atan2((double) this.y, (double) this.x)) * 57.295776f) + 90.0f;
        if (ang < 0.0f) {
            return ang + 360.0f;
        }
        return ang;
    }

    public float angle(Vector2 dirV) {
        float ang = (((float) Math.atan2((double) (dirV.y - this.y), (double) (dirV.x - this.x))) * 57.295776f) + 90.0f;
        if (ang < 0.0f) {
            return ang + 360.0f;
        }
        return ang;
    }

    public float dot(Vector2 v) {
        return (this.x * v.x) + (this.y * v.y);
    }
}
