package com.noalm.lizard;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

public class MenuButton {
    float BottomClick;
    public boolean Enabled = true;
    public RectF PosRect;
    public boolean Shadow = false;
    public float Size = 30.0f;
    public String Text = "";
    public Paint.Align TextAlign;
    float TopClick;

    public void init(float left, float top, float right, float bottom, float y_click_offset, float _Size, String _Text, Paint.Align align, boolean shadow) {
        this.PosRect = new RectF(left, top, right, bottom);
        this.TopClick = top + y_click_offset;
        this.BottomClick = bottom + y_click_offset;
        this.Size = _Size;
        this.Text = _Text;
        this.TextAlign = align;
        this.Shadow = shadow;
    }

    public boolean isClicked(float posX, float posY) {
        if (!this.Enabled) {
            return false;
        }
        return posX > this.PosRect.left && posX < this.PosRect.right && posY > this.TopClick && posY < this.BottomClick;
    }

    public void draw(Canvas canvas) {
        UI.mPaint.setTextAlign(this.TextAlign);
        UI.mPaint.setTextSize(this.Size);
        if (this.Shadow) {
            UI.mPaint.setARGB(255, 0, 0, 0);
            if (this.TextAlign == Paint.Align.LEFT) {
                canvas.drawText(this.Text, this.PosRect.left - 2.0f, this.PosRect.bottom + 2.0f, UI.mPaint);
            } else if (this.TextAlign == Paint.Align.RIGHT) {
                canvas.drawText(this.Text, this.PosRect.right - 2.0f, this.PosRect.bottom + 2.0f, UI.mPaint);
            } else if (this.TextAlign == Paint.Align.CENTER) {
                canvas.drawText(this.Text, this.PosRect.centerX() - 2.0f, this.PosRect.bottom + 2.0f, UI.mPaint);
            }
        }
        if (this.Enabled) {
            UI.mPaint.setARGB(255, 220, 220, 220);
        } else {
            UI.mPaint.setARGB(255, 120, 120, 120);
        }
        if (this.TextAlign == Paint.Align.LEFT) {
            canvas.drawText(this.Text, this.PosRect.left, this.PosRect.bottom, UI.mPaint);
        } else if (this.TextAlign == Paint.Align.RIGHT) {
            canvas.drawText(this.Text, this.PosRect.right, this.PosRect.bottom, UI.mPaint);
        } else if (this.TextAlign == Paint.Align.CENTER) {
            canvas.drawText(this.Text, this.PosRect.centerX(), this.PosRect.bottom, UI.mPaint);
        }
    }

    public void draw(Canvas canvas, Bitmap Bmp, float scaleX, float scaleY, int a) {
        UI.drawBitmapSTA(canvas, Bmp, this.PosRect.left, this.TopClick, scaleX, scaleY, a);
    }

    public void draw(Canvas canvas, Bitmap Bmp, float scaleX, float scaleY, float offsetX, float offsetY, int a) {
        UI.drawBitmapSTA(canvas, Bmp, this.PosRect.left + offsetX, this.TopClick + offsetY, scaleX, scaleY, a);
    }
}
