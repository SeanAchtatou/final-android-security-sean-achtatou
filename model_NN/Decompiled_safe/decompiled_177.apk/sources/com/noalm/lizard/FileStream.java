package com.noalm.lizard;

import android.app.Activity;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class FileStream {
    private static int HEADER = 1404;
    private FileInputStream FIS = null;
    private FileOutputStream FOS = null;
    private int FileVersion = 0;
    private DataInputStream IN = null;
    private DataOutputStream OUT = null;

    public boolean openRead(Activity activity, String filename) {
        try {
            this.FIS = activity.openFileInput(filename);
            this.IN = new DataInputStream(this.FIS);
            if (this.IN.readInt() != HEADER) {
                close();
                return false;
            }
            this.FileVersion = this.IN.readInt();
            return true;
        } catch (Exception e) {
            close();
            return false;
        }
    }

    public boolean openWrite(Activity activity, String filename, int file_version) {
        try {
            this.FOS = activity.openFileOutput(filename, 3);
            this.OUT = new DataOutputStream(this.FOS);
            this.OUT.writeInt(HEADER);
            this.OUT.writeInt(file_version);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void close() {
        try {
            if (this.IN != null) {
                this.IN.close();
            }
            if (this.OUT != null) {
                this.OUT.close();
            }
        } catch (Exception e) {
        }
    }

    public int readInt(int default_val, int start_ver) {
        if (start_ver != 0 && this.FileVersion < start_ver) {
            return default_val;
        }
        try {
            return this.IN.readInt();
        } catch (Exception e) {
            return default_val;
        }
    }

    public boolean readBoolean(boolean default_val, int start_ver) {
        if (start_ver != 0 && this.FileVersion < start_ver) {
            return default_val;
        }
        try {
            return this.IN.readBoolean();
        } catch (Exception e) {
            return default_val;
        }
    }

    public String readString(String default_val, int start_ver) {
        if (start_ver != 0 && this.FileVersion < start_ver) {
            return default_val;
        }
        try {
            return this.IN.readLine();
        } catch (Exception e) {
            return default_val;
        }
    }

    public void writeInt(int val) {
        try {
            this.OUT.writeInt(val);
        } catch (Exception e) {
        }
    }

    public void writeBoolean(boolean val) {
        try {
            this.OUT.writeBoolean(val);
        } catch (Exception e) {
        }
    }

    public void writeString(String val) {
        try {
            this.OUT.writeChars(val);
            this.OUT.writeChars("\n");
        } catch (Exception e) {
        }
    }
}
