package com.noalm.lizard;

import android.graphics.Canvas;

public class Wall {
    public static final float WALL_BLOCK_SIZE = 20.0f;
    public static final float WALL_MIN_SPACE = 0.0f;
    private Vector2 Dir = new Vector2(0.0f, 0.0f);
    private float Length = 0.0f;
    private int NumBlocks = 0;
    public Vector2 Point1 = new Vector2(0.0f, 0.0f);
    public Vector2 Point1Draw = new Vector2(0.0f, 0.0f);
    public Vector2 Point2 = new Vector2(0.0f, 0.0f);
    private float Space = 5.0f;

    public void init(float p1x, float p1y, float p2x, float p2y) {
        this.Point1.set((float) ((int) p1x), (float) ((int) p1y));
        this.Point2.set((float) ((int) p2x), (float) ((int) p2y));
        this.Dir.set(this.Point2);
        this.Dir.subtract(this.Point1);
        this.Dir.normalize();
        this.Length = this.Point1.distance(this.Point2);
        this.Space = 5.0f;
        this.NumBlocks = 0;
        float len = 0.0f;
        while (len < this.Length - 20.0f) {
            this.NumBlocks++;
            if (len > 0.0f) {
                len += 0.0f;
            }
            len += 20.0f;
        }
        this.NumBlocks--;
        this.Space = (((this.Length - 20.0f) - 20.0f) - (((float) (this.NumBlocks - 2)) * 20.0f)) / ((float) (this.NumBlocks - 1));
        this.Point1Draw.set(this.Dir);
        this.Point1Draw.multiply(10.0f);
        this.Point1Draw.add(this.Point1);
    }

    public void draw(Canvas canvas) {
        for (int i = 0; i < this.NumBlocks; i++) {
            UI.drawBitmapTSRotT(canvas, UI.ImgWall, -15.0f, -15.0f, this.Point1Draw.x + (this.Dir.x * ((float) i) * (this.Space + 20.0f)), this.Point1Draw.y + (this.Dir.y * ((float) i) * (this.Space + 20.0f)), 0.66f, 0.66f, 0.0f);
        }
    }
}
