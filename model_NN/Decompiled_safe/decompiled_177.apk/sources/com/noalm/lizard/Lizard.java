package com.noalm.lizard;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import com.facebook.android.Facebook;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class Lizard extends Activity implements AdListener {
    public static FBManager FB = new FBManager("184851008244984");
    public static Facebook FacebookAPI = new Facebook("184851008244984");
    public static String FacebookAccessToken = null;
    public static String FacebookUserID = null;
    public static Typeface LizardFont;
    public static AdView mAdView;
    public static LizardView mLizardView = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.lizard_layout);
        setVolumeControlStream(3);
        mLizardView = (LizardView) findViewById(R.id.lizard);
        LizardFont = Typeface.createFromAsset(getAssets(), "Lizard.ttf");
        mLizardView.init(this);
        mAdView = (AdView) findViewById(R.id.adView);
        mAdView.setAdListener(this);
        mAdView.loadAd(new AdRequest());
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (UI.CurPage == 4) {
            UI.goToPage(5, false, true);
        }
        if (mLizardView != null && LizardView.mSounds != null) {
            Sounds.pauseMusic();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (mLizardView != null && LizardView.mSounds != null) {
            Sounds.resumeMusic();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        LizardView.saveGame();
    }

    public void onSaveInstanceState(Bundle outState) {
        LizardView.saveGame();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            if (event.getRepeatCount() == 0 && UI.onActionBack()) {
                return true;
            }
        } else if (keyCode == 24) {
            if (LizardView.mSounds != null) {
                Sounds.mAudioManager.adjustStreamVolume(3, 1, 1);
                return true;
            }
        } else if (keyCode == 25 && LizardView.mSounds != null) {
            Sounds.mAudioManager.adjustStreamVolume(3, -1, 1);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onReceiveAd(Ad ad) {
    }

    public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode error) {
    }

    public void onPresentScreen(Ad ad) {
    }

    public void onDismissScreen(Ad ad) {
    }

    public void onLeaveApplication(Ad ad) {
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FacebookAPI.authorizeCallback(requestCode, resultCode, data);
    }
}
