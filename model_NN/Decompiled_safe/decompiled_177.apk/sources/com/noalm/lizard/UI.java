package com.noalm.lizard;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.util.SparseBooleanArray;

public class UI {
    private static final float AD_HEIGHT = 50.0f;
    private static MenuButton BtnCLBack = new MenuButton();
    private static MenuButton BtnFNBack = new MenuButton();
    private static MenuButton BtnFNRestart = new MenuButton();
    private static MenuButton BtnFNSubmit = new MenuButton();
    private static MenuButton BtnHLBack = new MenuButton();
    private static MenuButton BtnHLNext = new MenuButton();
    private static MenuButton BtnMMHelp = new MenuButton();
    private static MenuButton BtnMMMore = new MenuButton();
    private static MenuButton BtnMMPlay = new MenuButton();
    private static MenuButton BtnMMSettings = new MenuButton();
    private static MenuButton BtnMRBuy = new MenuButton();
    private static MenuButton BtnMRFacebook = new MenuButton();
    private static MenuButton BtnMRMarket = new MenuButton();
    private static MenuButton BtnMRTwitter = new MenuButton();
    private static MenuButton BtnPLBack = new MenuButton();
    private static MenuButton BtnPLLeft = new MenuButton();
    private static MenuButton BtnPLRight = new MenuButton();
    private static MenuButton BtnPSContinue = new MenuButton();
    private static MenuButton BtnPSQuit = new MenuButton();
    private static MenuButton BtnPSRestart = new MenuButton();
    private static MenuButton BtnQTBack = new MenuButton();
    private static MenuButton BtnQTOk = new MenuButton();
    private static MenuButton BtnRSBack = new MenuButton();
    private static MenuButton BtnRSOk = new MenuButton();
    private static MenuButton BtnSTFacebook = new MenuButton();
    private static MenuButton BtnSTMusic = new MenuButton();
    private static MenuButton BtnSTSound = new MenuButton();
    private static MenuButton BtnTTNext = new MenuButton();
    public static final int CHOOSE_LEVEL = 3;
    public static int CurHelpPage = 0;
    public static int CurPage = 0;
    public static Vector2 DynamicNegHSize = new Vector2(0.0f, 0.0f);
    public static Dynamic[] Dynamics = new Dynamic[80];
    public static final int FINISHED = 8;
    public static boolean FadeIn = false;
    public static boolean FadeOut = false;
    public static int FinishedGoal = 0;
    public static int FinishedHighscore = 0;
    public static float FinishedHighscoreScale = 0.0f;
    public static int FinishedPercent = 0;
    public static Vector2 FinishedRG = new Vector2(0.0f, 0.0f);
    public static int FinishedScore = 0;
    public static final int GAME = 4;
    public static final int HELP = 10;
    public static Bitmap ImgBckg1 = null;
    public static Bitmap ImgBckg2 = null;
    public static BitmapFrames ImgBee1 = new BitmapFrames(2);
    public static Bitmap ImgBee1Glow = null;
    public static Bitmap ImgBtnArrowLeft = null;
    public static Bitmap ImgBtnArrowLeftDis = null;
    public static Bitmap ImgBtnArrowRight = null;
    public static Bitmap ImgBtnArrowRightDis = null;
    public static Bitmap ImgBtnBack = null;
    public static Bitmap ImgBtnBuy = null;
    public static Bitmap ImgBtnCancel = null;
    public static Bitmap ImgBtnFacebook = null;
    public static Bitmap ImgBtnHelp = null;
    public static Bitmap ImgBtnLevel = null;
    public static Bitmap ImgBtnLevelDis = null;
    public static Bitmap ImgBtnLevelGlow = null;
    public static Bitmap ImgBtnMarket = null;
    public static Bitmap ImgBtnMore = null;
    public static Bitmap ImgBtnMoreOn = null;
    public static Bitmap ImgBtnMusicOff = null;
    public static Bitmap ImgBtnMusicOn = null;
    public static Bitmap ImgBtnNext = null;
    public static Bitmap ImgBtnOk = null;
    public static Bitmap ImgBtnPlay = null;
    public static Bitmap ImgBtnQuit = null;
    public static Bitmap ImgBtnRestart = null;
    public static Bitmap ImgBtnSettings = null;
    public static Bitmap ImgBtnSettingsFacebook = null;
    public static Bitmap ImgBtnSettingsOn = null;
    public static Bitmap ImgBtnSoundOff = null;
    public static Bitmap ImgBtnSoundOn = null;
    public static Bitmap ImgBtnSubmit = null;
    public static Bitmap ImgBtnTwitter = null;
    public static Bitmap ImgCircleGreen = null;
    public static Bitmap ImgCircleRed = null;
    public static Bitmap ImgCircleWhite = null;
    public static Bitmap ImgCircleYellow = null;
    public static Bitmap ImgDynamic = null;
    public static Bitmap ImgDynamic1 = null;
    public static Bitmap ImgDynamic2 = null;
    public static Bitmap ImgFinishedBckg = null;
    public static Bitmap ImgFinishedHighscore = null;
    public static BitmapFrames ImgFly1 = new BitmapFrames(2);
    public static BitmapFrames ImgGlowworm1 = new BitmapFrames(2);
    public static Bitmap ImgGlowworm1Glow = null;
    public static Bitmap[] ImgHelp = new Bitmap[3];
    public static Bitmap ImgLblPaused = null;
    public static Bitmap ImgLblQuit = null;
    public static Bitmap ImgLblRestart = null;
    public static BitmapFrames ImgLizard = new BitmapFrames(4);
    public static Bitmap ImgLizardEat = null;
    public static Bitmap ImgLizardSkin = null;
    public static Bitmap ImgLizardTongue = null;
    public static Bitmap ImgMenuTitle = null;
    public static BitmapFrames ImgMosquito1 = new BitmapFrames(2);
    public static Bitmap ImgNoalmLogo = null;
    public static Bitmap ImgPercentBckg = null;
    public static Bitmap ImgToken = null;
    public static Bitmap ImgTokenGlow = null;
    public static Bitmap ImgWall = null;
    public static Bitmap ImgWall1 = null;
    public static Bitmap ImgWall2 = null;
    public static Bitmap ImgWorld1 = null;
    public static Bitmap ImgWorld2 = null;
    public static Bitmap ImgWorldBckg = null;
    public static Bitmap ImgWorldInfoBckg = null;
    public static Bitmap ImgWorldLocked = null;
    public static boolean LevelUnlocking = false;
    public static boolean LevelUnlockingDir = true;
    public static int LevelUnlockingInd = 0;
    public static float LevelUnlockingVal = 0.0f;
    public static final int MAIN_MENU = 1;
    public static final int MORE = 12;
    public static final int NOALM_LOGO = 0;
    public static final int NUM_HELP_PAGES = 3;
    public static int NextPage = 0;
    public static int NumDynamics = 0;
    public static final int PAUSED = 5;
    public static final int PLAY = 2;
    public static int PageTransition = 0;
    public static int PageTransitionAlpha = 255;
    public static float PageTransitionVal = 1.0f;
    public static float PageTransitionValInv = 0.0f;
    public static final int QUIT = 7;
    public static final int RESTART = 6;
    public static final float SCORE_HEIGHT = 24.0f;
    private static final float SCREEN_HEIGHT = 576.0f;
    private static final float SCREEN_WIDTH = 320.0f;
    public static final int SETTINGS = 9;
    public static float ScreenHeight = 0.0f;
    public static float ScreenWidth = 0.0f;
    public static float ScreenYMultiplier = 0.0f;
    public static final int TUTORIAL = 11;
    public static int TutorialPageLimit = 0;
    public static Matrix mMatrix = new Matrix();
    public static final Paint mPaint = new Paint();

    public UI() {
        for (int i = 0; i < 80; i++) {
            Dynamics[i] = new Dynamic();
        }
    }

    public static void loadGraphics(Resources r) {
        mPaint.setFilterBitmap(true);
        mPaint.setTypeface(Lizard.LizardFont);
        mPaint.setAntiAlias(true);
        ImgNoalmLogo = BitmapFactory.decodeResource(r, R.drawable.noalm_logo);
        ImgMenuTitle = BitmapFactory.decodeResource(r, R.drawable.menu_title);
        ImgBtnArrowLeft = BitmapFactory.decodeResource(r, R.drawable.btn_arrow_left);
        ImgBtnArrowLeftDis = BitmapFactory.decodeResource(r, R.drawable.btn_arrow_left_dis);
        ImgBtnArrowRight = BitmapFactory.decodeResource(r, R.drawable.btn_arrow_right);
        ImgBtnArrowRightDis = BitmapFactory.decodeResource(r, R.drawable.btn_arrow_right_dis);
        ImgBtnBack = BitmapFactory.decodeResource(r, R.drawable.btn_back);
        ImgBtnNext = BitmapFactory.decodeResource(r, R.drawable.btn_next);
        ImgBtnCancel = BitmapFactory.decodeResource(r, R.drawable.btn_cancel);
        ImgBtnHelp = BitmapFactory.decodeResource(r, R.drawable.btn_help);
        ImgBtnLevel = BitmapFactory.decodeResource(r, R.drawable.btn_level);
        ImgBtnLevelDis = BitmapFactory.decodeResource(r, R.drawable.btn_level_dis);
        ImgBtnLevelGlow = BitmapFactory.decodeResource(r, R.drawable.btn_level_glow);
        ImgBtnMore = BitmapFactory.decodeResource(r, R.drawable.btn_more);
        ImgBtnMoreOn = BitmapFactory.decodeResource(r, R.drawable.btn_more_on);
        ImgBtnSettings = BitmapFactory.decodeResource(r, R.drawable.btn_settings);
        ImgBtnSettingsOn = BitmapFactory.decodeResource(r, R.drawable.btn_settings_on);
        ImgBtnOk = BitmapFactory.decodeResource(r, R.drawable.btn_ok);
        ImgBtnPlay = BitmapFactory.decodeResource(r, R.drawable.btn_play);
        ImgBtnQuit = BitmapFactory.decodeResource(r, R.drawable.btn_quit);
        ImgBtnRestart = BitmapFactory.decodeResource(r, R.drawable.btn_restart);
        ImgBtnSoundOff = BitmapFactory.decodeResource(r, R.drawable.btn_sound_off);
        ImgBtnSoundOn = BitmapFactory.decodeResource(r, R.drawable.btn_sound_on);
        ImgBtnMusicOff = BitmapFactory.decodeResource(r, R.drawable.btn_music_off);
        ImgBtnMusicOn = BitmapFactory.decodeResource(r, R.drawable.btn_music_on);
        ImgBtnSettingsFacebook = BitmapFactory.decodeResource(r, R.drawable.btn_settings_fb);
        ImgBtnMarket = BitmapFactory.decodeResource(r, R.drawable.market);
        ImgBtnFacebook = BitmapFactory.decodeResource(r, R.drawable.facebook);
        ImgBtnTwitter = BitmapFactory.decodeResource(r, R.drawable.twitter);
        ImgBtnBuy = BitmapFactory.decodeResource(r, R.drawable.buy);
        ImgBtnSubmit = BitmapFactory.decodeResource(r, R.drawable.btn_submit);
        ImgLblPaused = BitmapFactory.decodeResource(r, R.drawable.lbl_paused);
        ImgLblRestart = BitmapFactory.decodeResource(r, R.drawable.lbl_restart);
        ImgLblQuit = BitmapFactory.decodeResource(r, R.drawable.lbl_quit);
        ImgFinishedBckg = BitmapFactory.decodeResource(r, R.drawable.finished_bckg);
        ImgFinishedHighscore = BitmapFactory.decodeResource(r, R.drawable.finished_highscore);
        ImgToken = BitmapFactory.decodeResource(r, R.drawable.token);
        ImgTokenGlow = BitmapFactory.decodeResource(r, R.drawable.token_glow);
        ImgLizard.destroy();
        ImgLizard.addFrame(BitmapFactory.decodeResource(r, R.drawable.lizard1));
        ImgLizard.addFrame(BitmapFactory.decodeResource(r, R.drawable.lizard2));
        ImgLizard.addFrame(BitmapFactory.decodeResource(r, R.drawable.lizard3));
        ImgLizard.addFrame(BitmapFactory.decodeResource(r, R.drawable.lizard4));
        ImgLizardEat = BitmapFactory.decodeResource(r, R.drawable.lizard_eat);
        ImgLizardTongue = BitmapFactory.decodeResource(r, R.drawable.tongue);
        ImgFly1.destroy();
        ImgFly1.addFrame(BitmapFactory.decodeResource(r, R.drawable.fly1_1));
        ImgFly1.addFrame(BitmapFactory.decodeResource(r, R.drawable.fly1_2));
        ImgMosquito1.destroy();
        ImgMosquito1.addFrame(BitmapFactory.decodeResource(r, R.drawable.mosquito1_1));
        ImgMosquito1.addFrame(BitmapFactory.decodeResource(r, R.drawable.mosquito1_2));
        ImgGlowworm1.destroy();
        ImgGlowworm1.addFrame(BitmapFactory.decodeResource(r, R.drawable.glowworm1_1));
        ImgGlowworm1.addFrame(BitmapFactory.decodeResource(r, R.drawable.glowworm1_2));
        ImgGlowworm1Glow = BitmapFactory.decodeResource(r, R.drawable.glowworm1_glow);
        ImgBee1.destroy();
        ImgBee1.addFrame(BitmapFactory.decodeResource(r, R.drawable.bee1_1));
        ImgBee1.addFrame(BitmapFactory.decodeResource(r, R.drawable.bee1_2));
        ImgBee1Glow = BitmapFactory.decodeResource(r, R.drawable.bee1_glow);
        ImgBckg1 = BitmapFactory.decodeResource(r, R.drawable.bckg1);
        ImgBckg2 = BitmapFactory.decodeResource(r, R.drawable.bckg2);
        ImgLizardSkin = BitmapFactory.decodeResource(r, R.drawable.lizard_skin);
        ImgPercentBckg = BitmapFactory.decodeResource(r, R.drawable.percent_bckg);
        ImgCircleGreen = BitmapFactory.decodeResource(r, R.drawable.circle_green);
        ImgCircleRed = BitmapFactory.decodeResource(r, R.drawable.circle_red);
        ImgCircleYellow = BitmapFactory.decodeResource(r, R.drawable.circle_yellow);
        ImgCircleWhite = BitmapFactory.decodeResource(r, R.drawable.circle_white);
        ImgWorldInfoBckg = BitmapFactory.decodeResource(r, R.drawable.world_info_bckg);
        ImgWorldLocked = BitmapFactory.decodeResource(r, R.drawable.world_locked);
        ImgWorld1 = BitmapFactory.decodeResource(r, R.drawable.world1);
        ImgWorld2 = BitmapFactory.decodeResource(r, R.drawable.world2);
        ImgWall1 = BitmapFactory.decodeResource(r, R.drawable.wall1);
        ImgWall2 = BitmapFactory.decodeResource(r, R.drawable.wall2);
        ImgDynamic1 = BitmapFactory.decodeResource(r, R.drawable.dynamic1);
        ImgDynamic2 = BitmapFactory.decodeResource(r, R.drawable.dynamic2);
        ImgHelp[0] = BitmapFactory.decodeResource(r, R.drawable.help1);
        ImgHelp[1] = BitmapFactory.decodeResource(r, R.drawable.help2);
        ImgHelp[2] = BitmapFactory.decodeResource(r, R.drawable.help3);
        setWorld();
    }

    public static void loadMenus() {
        float w = ScreenWidth;
        float h = ScreenHeight;
        BtnMMPlay.init(10.0f, h - 74.0f, 74.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnMMSettings.init(84.0f, h - 74.0f, 148.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnMMHelp.init(158.0f, h - 74.0f, 222.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnMMMore.init(232.0f, h - 74.0f, 296.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnPLBack.init(10.0f, h - 74.0f, 74.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnPLLeft.init(5.0f, 107.0f, 53.0f, 203.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnPLRight.init(w - 53.0f, 107.0f, w - 5.0f, 203.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnCLBack.init(10.0f, h - 74.0f, 74.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnPSContinue.init(10.0f, h - 74.0f, 74.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnPSRestart.init(84.0f, h - 74.0f, 148.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnPSQuit.init(158.0f, h - 74.0f, 222.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnRSBack.init(10.0f, h - 74.0f, 74.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnRSOk.init(84.0f, h - 74.0f, 148.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnQTBack.init(10.0f, h - 74.0f, 74.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnQTOk.init(84.0f, h - 74.0f, 148.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnFNBack.init(10.0f, h - 74.0f, 74.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnFNRestart.init(84.0f, h - 74.0f, 148.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnFNSubmit.init(158.0f, h - 74.0f, 242.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnSTSound.init(10.0f, h - 152.0f, 58.0f, h - 104.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnSTMusic.init(68.0f, h - 152.0f, 116.0f, h - 104.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnSTFacebook.init(126.0f, h - 152.0f, 174.0f, h - 104.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnHLBack.init(10.0f, h - 74.0f, 74.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnHLNext.init(84.0f, h - 74.0f, 148.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnTTNext.init(10.0f, h - 74.0f, 74.0f, h - 10.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnMRMarket.init(10.0f, h - 152.0f, AD_HEIGHT, h - 104.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnMRFacebook.init(60.0f, h - 152.0f, 108.0f, h - 104.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnMRTwitter.init(118.0f, h - 152.0f, 166.0f, h - 104.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        BtnMRBuy.init(176.0f, h - 152.0f, 224.0f, h - 104.0f, 0.0f, 0.0f, "", Paint.Align.LEFT, false);
        NumDynamics = 0;
        for (int i = 0; i < 9 && NumDynamics <= 78; i += 2) {
            float h2 = (float) ((i * 32) + 30);
            Dynamic[] dynamicArr = Dynamics;
            int i2 = NumDynamics;
            NumDynamics = i2 + 1;
            dynamicArr[i2].init(h2, 10.0f, 1.0f, 1);
            float h3 = h2 + 32.0f;
            if (NumDynamics < 9) {
                Dynamic[] dynamicArr2 = Dynamics;
                int i3 = NumDynamics;
                NumDynamics = i3 + 1;
                dynamicArr2[i3].init(h3, 22.0f, 1.0f, 1);
            }
        }
        for (int i4 = 0; i4 < 9 && NumDynamics <= 78; i4 += 2) {
            float h4 = (float) ((i4 * 32) + 30);
            Dynamic[] dynamicArr3 = Dynamics;
            int i5 = NumDynamics;
            NumDynamics = i5 + 1;
            dynamicArr3[i5].init(h4, ScreenHeight - 10.0f, 1.0f, 2);
            float h5 = h4 + 32.0f;
            if (NumDynamics < 18) {
                Dynamic[] dynamicArr4 = Dynamics;
                int i6 = NumDynamics;
                NumDynamics = i6 + 1;
                dynamicArr4[i6].init(h5, ScreenHeight - 22.0f, 1.0f, 2);
            }
        }
        for (int i7 = 0; i7 < 32 && NumDynamics <= 78; i7 += 2) {
            float h6 = (float) ((i7 * 32) + 30);
            if (h6 > ScreenHeight) {
                break;
            }
            Dynamic[] dynamicArr5 = Dynamics;
            int i8 = NumDynamics;
            NumDynamics = i8 + 1;
            dynamicArr5[i8].init(10.0f, h6, 1.0f, 3);
            float h7 = h6 + 32.0f;
            if (h7 > ScreenHeight - 10.0f) {
                break;
            }
            Dynamic[] dynamicArr6 = Dynamics;
            int i9 = NumDynamics;
            NumDynamics = i9 + 1;
            dynamicArr6[i9].init(22.0f, h7, 1.0f, 3);
        }
        int i10 = 0;
        while (i10 < 32 && NumDynamics <= 78) {
            float h8 = (float) ((i10 * 32) + 30);
            if (h8 <= ScreenHeight) {
                Dynamic[] dynamicArr7 = Dynamics;
                int i11 = NumDynamics;
                NumDynamics = i11 + 1;
                dynamicArr7[i11].init(ScreenWidth - 10.0f, h8, 1.0f, 4);
                float h9 = h8 + 32.0f;
                if (h9 <= ScreenHeight - 10.0f) {
                    Dynamic[] dynamicArr8 = Dynamics;
                    int i12 = NumDynamics;
                    NumDynamics = i12 + 1;
                    dynamicArr8[i12].init(ScreenWidth - 22.0f, h9, 1.0f, 4);
                    i10 += 2;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    public static void setWorld() {
        if (LizardView.CurWorld < LizardView.NumWorldsUnlocked) {
            if (LizardView.CurWorld == 0) {
                ImgWorldBckg = ImgBckg1;
                ImgWall = ImgWall1;
                ImgDynamic = ImgDynamic1;
                DynamicNegHSize.set(-23.0f, -30.0f);
            } else if (LizardView.CurWorld == 1) {
                ImgWorldBckg = ImgBckg2;
                ImgWall = ImgWall2;
                ImgDynamic = ImgDynamic2;
                DynamicNegHSize.set(-23.0f, -30.0f);
            }
        }
    }

    public static void update() {
        if (CurPage == 3 && PageTransition == 0 && LevelUnlocking) {
            if (LevelUnlockingDir) {
                LevelUnlockingVal += 0.04f * LizardView.fTimeMultiplier;
                if (LevelUnlockingVal > 1.1f) {
                    LevelUnlockingDir = false;
                    LevelUnlockingVal = 1.1f;
                }
            } else {
                LevelUnlockingVal -= 0.02f * LizardView.fTimeMultiplier;
                if (LevelUnlockingVal < 0.0f) {
                    LevelUnlocking = false;
                    LevelUnlockingVal = 0.0f;
                }
            }
        }
        if (CurPage != 8 || PageTransition != 0) {
            return;
        }
        if (FinishedScore < Game.Score) {
            FinishedScore += 200;
            if (FinishedScore > Game.Score) {
                FinishedScore = Game.Score;
            }
            FinishedPercent = (int) ((((float) FinishedScore) / ((float) FinishedGoal)) * 100.0f);
            if (FinishedPercent >= 100) {
                FinishedRG.set(80.0f, 255.0f);
            } else {
                FinishedRG.set(255.0f - ((((float) FinishedPercent) / 100.0f) * 175.0f), (((float) FinishedPercent) / 100.0f) * 255.0f);
            }
        } else if (Game.NewHighscore && FinishedHighscoreScale < 1.0f) {
            if (FinishedHighscoreScale == 0.0f) {
                Sounds.playSound(6, 0, 1.0f);
            }
            FinishedHighscoreScale += 0.03f * LizardView.fTimeMultiplier;
            if (FinishedHighscoreScale >= 1.0f) {
                FinishedHighscoreScale = 1.0f;
                if (LizardView.AutoSubmitScore) {
                    FBManager.postScores(LizardView.CurWorld, LizardView.WorldInfos[LizardView.CurWorld].WorldScore);
                }
            }
        }
    }

    public static void onSizeChanged(float w, float h) {
        ScreenWidth = w;
        if (ScreenWidth > SCREEN_WIDTH) {
            ScreenWidth = SCREEN_WIDTH;
        }
        ScreenHeight = h;
        if (ScreenHeight > SCREEN_HEIGHT) {
            ScreenHeight = SCREEN_HEIGHT;
        }
        ScreenHeight -= 74.0f;
        ScreenYMultiplier = ScreenHeight / SCREEN_HEIGHT;
    }

    public static void onActionDown(float posX, float posY) {
        if (CurPage == 4) {
            Game.checkActionDown(posX, posY);
        }
    }

    public static void onActionUp(float posX, float posY) {
        if (CurPage == 1) {
            if (BtnMMPlay.isClicked(posX, posY)) {
                goToPage(2, true, true);
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnMMSettings.isClicked(posX, posY)) {
                goToPage(9, false, true);
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnMMHelp.isClicked(posX, posY)) {
                CurHelpPage = 0;
                goToPage(10, true, true);
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnMMMore.isClicked(posX, posY)) {
                goToPage(12, false, true);
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage == 2) {
            if (posX <= 64.0f || posX >= 256.0f || posY <= 59.0f || posY >= 251.0f) {
                if (BtnPLBack.isClicked(posX, posY)) {
                    goToPage(1, true, true);
                    Sounds.playSound(0, 0, 1.0f);
                } else if (BtnPLLeft.isClicked(posX, posY)) {
                    if (LizardView.CurWorld > 0) {
                        LizardView.CurWorld--;
                        setWorld();
                        Sounds.playSound(0, 0, 1.0f);
                    }
                } else if (BtnPLRight.isClicked(posX, posY) && LizardView.CurWorld < 0) {
                    LizardView.CurWorld++;
                    setWorld();
                    Sounds.playSound(0, 0, 1.0f);
                }
            } else if (LizardView.CurWorld < LizardView.NumWorldsUnlocked) {
                goToPage(3, true, true);
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage == 3) {
            if (BtnCLBack.isClicked(posX, posY)) {
                goToPage(2, true, true);
                Sounds.playSound(0, 0, 1.0f);
                return;
            }
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < 3) {
                    int j = 0;
                    while (true) {
                        int j2 = j;
                        if (j2 >= 2) {
                            break;
                        }
                        float l = (77.0f + (((float) i2) * 80.0f)) - 34.32f;
                        float t = 55.0f + (((float) j2) * 110.0f);
                        float r = l + 69.3f;
                        float b = 71.94f + t;
                        if (posX <= l || posX >= r || posY <= t || posY >= b) {
                            j = j2 + 1;
                        } else if ((j2 * 3) + i2 < LizardView.WorldInfos[LizardView.CurWorld].LevelsUnlocked) {
                            LizardView.CurLevel = (j2 * 3) + i2;
                            Sounds.playSound(0, 0, 1.0f);
                            Sounds.playMusic(1, true);
                            if (LizardView.ShowTutorial) {
                                if (LizardView.CurWorld == 0) {
                                    CurHelpPage = 0;
                                    TutorialPageLimit = 3;
                                }
                                goToPage(11, true, true);
                                Sounds.playSound(0, 0, 1.0f);
                            } else {
                                Game.init();
                                goToPage(4, true, true);
                            }
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        } else if (CurPage == 4) {
            Game.checkActionUp(posX, posY);
        } else if (CurPage == 5) {
            if (BtnPSContinue.isClicked(posX, posY)) {
                goToPage(4, true, true);
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnPSRestart.isClicked(posX, posY)) {
                goToPage(6, true, true);
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnPSQuit.isClicked(posX, posY)) {
                goToPage(7, true, true);
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage == 6) {
            if (BtnRSBack.isClicked(posX, posY)) {
                goToPage(5, true, true);
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnRSOk.isClicked(posX, posY)) {
                Game.init();
                goToPage(4, true, true);
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage == 7) {
            if (BtnQTBack.isClicked(posX, posY)) {
                goToPage(5, true, true);
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnQTOk.isClicked(posX, posY)) {
                Game.finishGame(true);
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage == 8) {
            if (BtnFNRestart.isClicked(posX, posY)) {
                Game.init();
                goToPage(4, true, true);
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnFNBack.isClicked(posX, posY)) {
                if (Game.LevelUnlocked) {
                    LevelUnlocking = true;
                    LevelUnlockingDir = true;
                    LevelUnlockingInd = LizardView.WorldInfos[LizardView.CurWorld].LevelsUnlocked - 1;
                    LevelUnlockingVal = 0.0f;
                    goToPage(3, true, true);
                    Sounds.playSound(0, 0, 1.0f);
                } else if (Game.WorldUnlocked) {
                    LizardView.CurWorld = LizardView.NumWorldsUnlocked - 1;
                    setWorld();
                    goToPage(2, true, true);
                    Sounds.playSound(0, 0, 1.0f);
                } else {
                    goToPage(3, true, true);
                    Sounds.playSound(0, 0, 1.0f);
                }
                Sounds.playMusic(0, true);
            } else if (!LizardView.AutoSubmitScore && Game.NewHighscore && BtnFNSubmit.isClicked(posX, posY)) {
                Sounds.playSound(0, 0, 1.0f);
                FBManager.postScores(LizardView.CurWorld, LizardView.WorldInfos[LizardView.CurWorld].WorldScore);
            }
        } else if (CurPage == 9) {
            if (BtnMMSettings.isClicked(posX, posY)) {
                goToPage(1, true, false);
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnSTSound.isClicked(posX, posY)) {
                LizardView.SoundEnabled = !LizardView.SoundEnabled;
                Sounds.playSound(0, 0, 1.0f);
                LizardView.saveGame();
            } else if (BtnSTMusic.isClicked(posX, posY)) {
                LizardView.MusicEnabled = !LizardView.MusicEnabled;
                Sounds.playSound(0, 0, 1.0f);
                LizardView.saveGame();
                if (LizardView.MusicEnabled) {
                    Sounds.playMusic(0, true);
                } else {
                    Sounds.stopCurMusic();
                }
            } else if (BtnSTFacebook.isClicked(posX, posY)) {
                Sounds.playSound(0, 0, 1.0f);
                boolean[] states = {LizardView.AutoConnectFB, LizardView.AutoSubmitScore};
                AlertDialog.Builder builder = new AlertDialog.Builder(LizardView.mLizard);
                builder.setTitle("Facebook connect settings");
                builder.setMultiChoiceItems(new CharSequence[]{"Connect on start", "Auto submit score"}, states, new DialogInterface.OnMultiChoiceClickListener() {
                    public void onClick(DialogInterface dialogInterface, int item, boolean state) {
                    }
                });
                builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SparseBooleanArray CheCked = ((AlertDialog) dialog).getListView().getCheckedItemPositions();
                        if (CheCked.get(0)) {
                            LizardView.AutoConnectFB = true;
                        } else {
                            LizardView.AutoConnectFB = false;
                        }
                        if (CheCked.get(1)) {
                            LizardView.AutoSubmitScore = true;
                        } else {
                            LizardView.AutoSubmitScore = false;
                        }
                        LizardView.saveGame();
                        if (LizardView.AutoConnectFB && !FBManager.Authorized) {
                            FBManager.authorize(new String[]{"email", "publish_stream", "offline_access"});
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                builder.create().show();
            }
        } else if (CurPage == 10) {
            if (BtnHLBack.isClicked(posX, posY)) {
                goToPage(1, true, true);
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnHLNext.isClicked(posX, posY)) {
                CurHelpPage++;
                if (CurHelpPage >= 3) {
                    CurHelpPage = 0;
                }
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage == 11) {
            if (BtnTTNext.isClicked(posX, posY)) {
                CurHelpPage++;
                if (CurHelpPage >= TutorialPageLimit) {
                    CurHelpPage = TutorialPageLimit - 1;
                    LizardView.ShowTutorial = false;
                    LizardView.saveGame();
                    Game.init();
                    goToPage(4, true, true);
                }
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage != 12) {
        } else {
            if (BtnMMMore.isClicked(posX, posY)) {
                goToPage(1, true, false);
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnMRMarket.isClicked(posX, posY)) {
                Sounds.playSound(0, 0, 1.0f);
                LizardView.mLizard.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.noalm.lizard")));
            } else if (BtnMRFacebook.isClicked(posX, posY)) {
                Sounds.playSound(0, 0, 1.0f);
                LizardView.mLizard.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.facebook.com/apps/application.php?id=184851008244984")));
            } else if (BtnMRTwitter.isClicked(posX, posY)) {
                Sounds.playSound(0, 0, 1.0f);
                LizardView.mLizard.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.twitter.com/noalmmobile")));
            } else if (BtnMRBuy.isClicked(posX, posY)) {
                Sounds.playSound(0, 0, 1.0f);
                LizardView.mLizard.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.noalm.lizardfull")));
            }
        }
    }

    public static void onActionMove(float posX, float posY) {
        if (CurPage == 4) {
            Game.checkActionMove(posX, posY);
        }
    }

    public static boolean onActionBack() {
        if (CurPage == 1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(LizardView.mLizard);
            builder.setMessage("Are you sure you want to exit?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    LizardView.mLizard.finish();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            builder.create().show();
        } else if (CurPage == 2) {
            goToPage(1, true, true);
            Sounds.playSound(0, 0, 1.0f);
        } else if (CurPage == 3) {
            goToPage(2, true, true);
            Sounds.playSound(0, 0, 1.0f);
        }
        if (CurPage == 4) {
            goToPage(5, false, true);
            Sounds.playSound(0, 0, 1.0f);
        } else if (CurPage == 5) {
            goToPage(4, true, true);
            Sounds.playSound(0, 0, 1.0f);
        } else if (CurPage == 6) {
            goToPage(5, true, true);
            Sounds.playSound(0, 0, 1.0f);
        } else if (CurPage == 7) {
            goToPage(5, true, true);
            Sounds.playSound(0, 0, 1.0f);
        } else if (CurPage != 8) {
            if (CurPage == 9) {
                goToPage(1, true, false);
                Sounds.playSound(0, 0, 1.0f);
            } else if (CurPage == 10) {
                goToPage(1, true, true);
                Sounds.playSound(0, 0, 1.0f);
            } else if (CurPage != 11 && CurPage == 12) {
                goToPage(1, true, false);
                Sounds.playSound(0, 0, 1.0f);
            }
        }
        return true;
    }

    public static void draw(Canvas canvas) {
        if (PageTransition == 1) {
            PageTransitionVal -= 0.05f * LizardView.fTimeMultiplier;
            if (PageTransitionVal < 0.0f || !FadeOut) {
                PageTransition = 2;
                PageTransitionVal = 0.0f;
                CurPage = NextPage;
                if (!FadeIn) {
                    PageTransition = 0;
                    PageTransitionVal = 1.0f;
                }
            }
        } else if (PageTransition == 2) {
            PageTransitionVal += 0.05f * LizardView.fTimeMultiplier;
            if (PageTransitionVal > 1.0f || !FadeIn) {
                PageTransition = 0;
                PageTransitionVal = 1.0f;
            }
        }
        PageTransitionAlpha = (int) (255.0f * PageTransitionVal);
        PageTransitionValInv = 1.0f - PageTransitionVal;
        if (CurPage == 0) {
            mPaint.setARGB(255, 255, 255, 255);
            canvas.drawRect(0.0f, 0.0f, ScreenWidth, ScreenHeight, mPaint);
            long delta = LizardView.CurTime - LizardView.TimeStarted;
            drawBitmapT(canvas, ImgNoalmLogo, (ScreenWidth / 2.0f) - 120.0f, (ScreenHeight / 2.0f) - 120.0f);
            if (delta > 4000) {
                goToPage(1, false, true);
                Sounds.playMusic(0, true);
                if (LizardView.FirstTime) {
                    boolean[] states = {LizardView.AutoConnectFB, LizardView.AutoSubmitScore};
                    AlertDialog.Builder builder = new AlertDialog.Builder(LizardView.mLizard);
                    builder.setTitle("Facebook connect settings");
                    builder.setMultiChoiceItems(new CharSequence[]{"Connect on start", "Auto submit score"}, states, new DialogInterface.OnMultiChoiceClickListener() {
                        public void onClick(DialogInterface dialogInterface, int item, boolean state) {
                        }
                    });
                    builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SparseBooleanArray CheCked = ((AlertDialog) dialog).getListView().getCheckedItemPositions();
                            if (CheCked.get(0)) {
                                LizardView.AutoConnectFB = true;
                            } else {
                                LizardView.AutoConnectFB = false;
                            }
                            if (CheCked.get(1)) {
                                LizardView.AutoSubmitScore = true;
                            } else {
                                LizardView.AutoSubmitScore = false;
                            }
                            LizardView.saveGame();
                            if (LizardView.AutoConnectFB && !FBManager.Authorized) {
                                FBManager.authorize(new String[]{"email", "publish_stream", "offline_access"});
                            }
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
                    builder.create().show();
                } else if (LizardView.AutoConnectFB) {
                    FBManager.authorize(new String[]{"email", "publish_stream", "offline_access"});
                }
            }
        } else if (CurPage == 1) {
            drawBitmapS(canvas, ImgWorldBckg, 0.67f, 0.67f * ScreenYMultiplier);
            for (int i = 0; i < NumDynamics; i++) {
                Dynamics[i].draw(canvas);
            }
            drawBitmapTSTA(canvas, ImgMenuTitle, -162.0f, -90.0f, 160.0f, 100.0f, 0.67f, 0.67f, PageTransitionAlpha);
            mPaint.setARGB(170, 0, 0, 0);
            canvas.drawRect(0.0f, (ScreenHeight - 84.0f) + (PageTransitionValInv * 84.0f), ScreenWidth, ScreenHeight, mPaint);
            BtnMMPlay.draw(canvas, ImgBtnPlay, 0.66f, 0.66f, 0.0f, 84.0f * PageTransitionValInv, 255);
            BtnMMSettings.draw(canvas, ImgBtnSettings, 0.66f, 0.66f, 0.0f, 84.0f * PageTransitionValInv, 255);
            BtnMMHelp.draw(canvas, ImgBtnHelp, 0.66f, 0.66f, 0.0f, 84.0f * PageTransitionValInv, 255);
            BtnMMMore.draw(canvas, ImgBtnMore, 0.66f, 0.66f, 0.0f, 84.0f * PageTransitionValInv, 255);
        } else if (CurPage == 2) {
            drawBitmapS(canvas, ImgWorldBckg, 0.67f, 0.67f * ScreenYMultiplier);
            for (int i2 = 0; i2 < NumDynamics; i2++) {
                Dynamics[i2].draw(canvas);
            }
            mPaint.setARGB(170, 0, 0, 0);
            canvas.drawRect(0.0f, (ScreenHeight - 84.0f) + (PageTransitionValInv * 84.0f), ScreenWidth, ScreenHeight, mPaint);
            if (LizardView.CurWorld >= LizardView.NumWorldsUnlocked) {
                drawBitmapTSTA(canvas, ImgWorldLocked, -160.0f, -160.0f, 160.0f, 155.0f, 0.67f, 0.67f, PageTransitionAlpha);
            } else if (LizardView.CurWorld == 0) {
                drawBitmapTSTA(canvas, ImgWorld1, -160.0f, -160.0f, 160.0f, 155.0f, 0.67f, 0.67f, PageTransitionAlpha);
            } else if (LizardView.CurWorld == 1) {
                drawBitmapTSTA(canvas, ImgWorld2, -160.0f, -160.0f, 160.0f, 155.0f, 0.67f, 0.67f, PageTransitionAlpha);
            }
            BtnPLBack.draw(canvas, ImgBtnBack, 0.66f, 0.66f, 0.0f, 84.0f * PageTransitionValInv, 255);
            BtnPLLeft.draw(canvas, LizardView.CurWorld > 0 ? ImgBtnArrowLeft : ImgBtnArrowLeftDis, 0.66f, 0.66f, PageTransitionAlpha);
            BtnPLRight.draw(canvas, LizardView.CurWorld < 0 ? ImgBtnArrowRight : ImgBtnArrowRightDis, 0.66f, 0.66f, PageTransitionAlpha);
            mPaint.setTextAlign(Paint.Align.CENTER);
            mPaint.setARGB(255, 255, 255, 255);
            if (LizardView.CurWorld < LizardView.NumWorldsUnlocked) {
                drawBitmapSTA(canvas, ImgWorldInfoBckg, 95.0f, (ScreenHeight - 74.0f) + (PageTransitionValInv * 84.0f), 0.67f, 0.67f, 255);
                drawBitmapSTA(canvas, ImgWorldInfoBckg, 210.0f, (ScreenHeight - 74.0f) + (PageTransitionValInv * 84.0f), 0.67f, 0.67f, 255);
                mPaint.setTextSize(18.0f);
                canvas.drawText("HIGHSCORE", 145.0f, (ScreenHeight - 49.0f) + (PageTransitionValInv * 84.0f), mPaint);
                canvas.drawText("%", 260.0f, (ScreenHeight - 47.0f) + (PageTransitionValInv * 84.0f), mPaint);
                mPaint.setTextSize(20.0f);
                canvas.drawText(Integer.toString(LizardView.WorldInfos[LizardView.CurWorld].WorldScore), 145.0f, (ScreenHeight - 23.0f) + (PageTransitionValInv * 84.0f), mPaint);
                canvas.drawText(LizardView.WorldInfos[LizardView.CurWorld].getWorldPercentageStr(), 260.0f, (ScreenHeight - 23.0f) + (PageTransitionValInv * 84.0f), mPaint);
                mPaint.setTextAlign(Paint.Align.LEFT);
                mPaint.setTextSize(16.0f);
                canvas.drawText("#1 place", 5.0f, ScreenHeight + 18.0f, mPaint);
            }
        } else if (CurPage == 3) {
            drawBitmapS(canvas, ImgWorldBckg, 0.67f, 0.67f * ScreenYMultiplier);
            for (int i3 = 0; i3 < NumDynamics; i3++) {
                Dynamics[i3].draw(canvas);
            }
            mPaint.setARGB(170, 0, 0, 0);
            canvas.drawRect(0.0f, (ScreenHeight - 84.0f) + (PageTransitionValInv * 84.0f), ScreenWidth, ScreenHeight, mPaint);
            mPaint.setTextAlign(Paint.Align.CENTER);
            int i4 = 0;
            while (true) {
                int i5 = i4;
                if (i5 >= 3) {
                    break;
                }
                int j = 0;
                while (true) {
                    int j2 = j;
                    if (j2 >= 2) {
                        break;
                    }
                    int l = (j2 * 3) + i5;
                    boolean unlocked = l < LizardView.WorldInfos[LizardView.CurWorld].LevelsUnlocked;
                    if (LevelUnlocking && LevelUnlockingDir && l == LevelUnlockingInd) {
                        unlocked = false;
                    }
                    if (unlocked) {
                        int a = PageTransitionAlpha;
                        if (LevelUnlocking && l == LevelUnlockingInd) {
                            a = LevelUnlockingVal < 1.0f ? (int) (255.0f * (1.0f - LevelUnlockingVal)) : 0;
                        }
                        drawBitmapTSTA(canvas, ImgBtnLevel, -52.0f, 0.0f, 77.0f + (((float) i5) * 80.0f), 55.0f + (((float) j2) * 110.0f), 0.66f, 0.66f, a);
                        mPaint.setTextSize(28.0f);
                        mPaint.setARGB(a, 0, 0, 0);
                        canvas.drawText(Integer.toString(l + 1), 75.0f + (((float) i5) * 80.0f), 105.0f + (((float) j2) * 110.0f), mPaint);
                        mPaint.setARGB(a, 255, 255, 255);
                        canvas.drawText(Integer.toString(l + 1), 77.0f + (((float) i5) * 80.0f), 103.0f + (((float) j2) * 110.0f), mPaint);
                        mPaint.setTextSize(18.0f);
                        mPaint.setARGB(a, 200, 255, 200);
                        canvas.drawText(String.valueOf(Integer.toString(LizardView.WorldInfos[LizardView.CurWorld].LevelsPercentage[l])) + "%", 77.0f + (((float) i5) * 80.0f), 147.0f + (((float) j2) * 110.0f), mPaint);
                    } else {
                        drawBitmapTSTA(canvas, ImgBtnLevelDis, -52.0f, 0.0f, 77.0f + (((float) i5) * 80.0f), 55.0f + (((float) j2) * 110.0f), 0.66f, 0.66f, PageTransitionAlpha);
                    }
                    if (LevelUnlocking && l == LevelUnlockingInd) {
                        int a2 = (int) (255.0f * PageTransitionVal * LevelUnlockingVal);
                        if (a2 > 255) {
                            a2 = 255;
                        }
                        drawBitmapTSTA(canvas, ImgBtnLevelGlow, -72.0f, -20.0f, 77.0f + (((float) i5) * 80.0f), 55.0f + (((float) j2) * 110.0f), 0.66f, 0.66f, a2);
                    }
                    j = j2 + 1;
                }
                i4 = i5 + 1;
            }
            BtnCLBack.draw(canvas, ImgBtnBack, 0.66f, 0.66f, 0.0f, PageTransitionValInv * 84.0f, 255);
            mPaint.setTextAlign(Paint.Align.CENTER);
            mPaint.setARGB(255, 255, 255, 255);
            if (LizardView.CurWorld < LizardView.NumWorldsUnlocked) {
                drawBitmapSTA(canvas, ImgWorldInfoBckg, 95.0f, (ScreenHeight - 74.0f) + (PageTransitionValInv * 84.0f), 0.67f, 0.67f, 255);
                drawBitmapSTA(canvas, ImgWorldInfoBckg, 210.0f, (ScreenHeight - 74.0f) + (PageTransitionValInv * 84.0f), 0.67f, 0.67f, 255);
                mPaint.setTextSize(18.0f);
                canvas.drawText("HIGHSCORE", 145.0f, (ScreenHeight - 49.0f) + (PageTransitionValInv * 84.0f), mPaint);
                canvas.drawText("%", 260.0f, (ScreenHeight - 47.0f) + (PageTransitionValInv * 84.0f), mPaint);
                mPaint.setTextSize(20.0f);
                canvas.drawText(Integer.toString(LizardView.WorldInfos[LizardView.CurWorld].WorldScore), 145.0f, (ScreenHeight - 23.0f) + (PageTransitionValInv * 84.0f), mPaint);
                canvas.drawText(LizardView.WorldInfos[LizardView.CurWorld].getWorldPercentageStr(), 260.0f, (ScreenHeight - 23.0f) + (PageTransitionValInv * 84.0f), mPaint);
            }
        } else if (CurPage == 4) {
            drawBitmapS(canvas, ImgWorldBckg, 0.67f, 0.67f * ScreenYMultiplier);
            for (int i6 = 0; i6 < NumDynamics; i6++) {
                Dynamics[i6].draw(canvas);
            }
            Game.draw(canvas);
            Game.drawScore(canvas);
        } else if (CurPage == 5) {
            drawBitmapS(canvas, ImgWorldBckg, 0.67f, 0.67f * ScreenYMultiplier);
            for (int i7 = 0; i7 < NumDynamics; i7++) {
                Dynamics[i7].draw(canvas);
            }
            Game.draw(canvas);
            mPaint.setARGB(170, 0, 0, 0);
            canvas.drawRect(0.0f, (ScreenHeight - 84.0f) + (PageTransitionValInv * 84.0f), ScreenWidth, ScreenHeight, mPaint);
            drawBitmapTSTA(canvas, ImgLblPaused, -100.0f, -55.0f, 160.0f, 120.0f, 0.67f, 0.67f, PageTransitionAlpha);
            BtnPSContinue.draw(canvas, ImgBtnPlay, 0.66f, 0.66f, 0.0f, PageTransitionValInv * 84.0f, 255);
            BtnPSRestart.draw(canvas, ImgBtnRestart, 0.66f, 0.66f, 0.0f, PageTransitionValInv * 84.0f, 255);
            BtnPSQuit.draw(canvas, ImgBtnQuit, 0.66f, 0.66f, 0.0f, PageTransitionValInv * 84.0f, 255);
        } else if (CurPage == 6) {
            drawBitmapS(canvas, ImgWorldBckg, 0.67f, 0.67f * ScreenYMultiplier);
            for (int i8 = 0; i8 < NumDynamics; i8++) {
                Dynamics[i8].draw(canvas);
            }
            Game.draw(canvas);
            mPaint.setARGB(170, 0, 0, 0);
            canvas.drawRect(0.0f, (ScreenHeight - 84.0f) + (PageTransitionValInv * 84.0f), ScreenWidth, ScreenHeight, mPaint);
            drawBitmapTSTA(canvas, ImgLblRestart, -100.0f, -55.0f, 160.0f, 120.0f, 0.67f, 0.67f, PageTransitionAlpha);
            BtnRSBack.draw(canvas, ImgBtnCancel, 0.66f, 0.66f, 0.0f, PageTransitionValInv * 84.0f, 255);
            BtnRSOk.draw(canvas, ImgBtnOk, 0.66f, 0.66f, 0.0f, PageTransitionValInv * 84.0f, 255);
        } else if (CurPage == 7) {
            drawBitmapS(canvas, ImgWorldBckg, 0.67f, 0.67f * ScreenYMultiplier);
            for (int i9 = 0; i9 < NumDynamics; i9++) {
                Dynamics[i9].draw(canvas);
            }
            Game.draw(canvas);
            mPaint.setARGB(170, 0, 0, 0);
            canvas.drawRect(0.0f, (ScreenHeight - 84.0f) + (PageTransitionValInv * 84.0f), ScreenWidth, ScreenHeight, mPaint);
            drawBitmapTSTA(canvas, ImgLblQuit, -100.0f, -55.0f, 160.0f, 120.0f, 0.67f, 0.67f, PageTransitionAlpha);
            BtnQTBack.draw(canvas, ImgBtnCancel, 0.66f, 0.66f, 0.0f, PageTransitionValInv * 84.0f, 255);
            BtnQTOk.draw(canvas, ImgBtnOk, 0.66f, 0.66f, 0.0f, PageTransitionValInv * 84.0f, 255);
        } else if (CurPage == 8) {
            drawBitmapS(canvas, ImgWorldBckg, 0.67f, 0.67f * ScreenYMultiplier);
            for (int i10 = 0; i10 < NumDynamics; i10++) {
                Dynamics[i10].draw(canvas);
            }
            Game.draw(canvas);
            mPaint.setARGB(170, 0, 0, 0);
            canvas.drawRect(0.0f, (ScreenHeight - 84.0f) + (PageTransitionValInv * 84.0f), ScreenWidth, ScreenHeight, mPaint);
            drawBitmapTSTA(canvas, ImgFinishedBckg, -185.0f, -165.0f, 160.0f, 155.0f, 0.67f, 0.67f, PageTransitionAlpha);
            if (Game.NewHighscore && FinishedHighscoreScale > 0.0f) {
                drawBitmapTSTA(canvas, ImgFinishedHighscore, -74.0f, -33.0f, 125.0f, 215.0f, FinishedHighscoreScale * 0.67f, FinishedHighscoreScale * 0.67f, PageTransitionAlpha);
            }
            mPaint.setTextAlign(Paint.Align.LEFT);
            mPaint.setTextSize(22.0f);
            mPaint.setARGB(PageTransitionAlpha, 255, 255, 255);
            canvas.drawText("score:", 80.0f, 105.0f, mPaint);
            canvas.drawText("goal:", 80.0f, 142.0f, mPaint);
            canvas.drawText("highscore:", 80.0f, 179.0f, mPaint);
            mPaint.setTextAlign(Paint.Align.RIGHT);
            mPaint.setTextSize(20.0f);
            mPaint.setARGB(PageTransitionAlpha, (int) FinishedRG.x, (int) FinishedRG.y, 80);
            canvas.drawText(Integer.toString(FinishedScore), 240.0f, 105.0f, mPaint);
            mPaint.setARGB(PageTransitionAlpha, 255, 255, 255);
            canvas.drawText(Integer.toString(FinishedGoal), 240.0f, 142.0f, mPaint);
            canvas.drawText(Integer.toString(FinishedHighscore), 240.0f, 179.0f, mPaint);
            mPaint.setTextAlign(Paint.Align.RIGHT);
            mPaint.setTextSize(26.0f);
            mPaint.setARGB(PageTransitionAlpha, (int) FinishedRG.x, (int) FinishedRG.y, 80);
            canvas.drawText(String.valueOf(Integer.toString(FinishedPercent)) + "%", 240.0f, 225.0f, mPaint);
            BtnFNBack.draw(canvas, ImgBtnNext, 0.66f, 0.66f, 0.0f, PageTransitionValInv * 84.0f, 255);
            BtnFNRestart.draw(canvas, ImgBtnRestart, 0.66f, 0.66f, 0.0f, PageTransitionValInv * 84.0f, 255);
            if (!LizardView.AutoSubmitScore && Game.NewHighscore) {
                BtnFNSubmit.draw(canvas, ImgBtnSubmit, 0.66f, 0.66f, 0.0f, PageTransitionValInv * 84.0f, 255);
            }
        } else if (CurPage == 9) {
            drawBitmapS(canvas, ImgWorldBckg, 0.67f, 0.67f * ScreenYMultiplier);
            for (int i11 = 0; i11 < NumDynamics; i11++) {
                Dynamics[i11].draw(canvas);
            }
            drawBitmapTST(canvas, ImgMenuTitle, -162.0f, -90.0f, 160.0f, 100.0f, 0.67f, 0.67f);
            mPaint.setARGB(170, 0, 0, 0);
            canvas.drawRect(PageTransitionValInv * SCREEN_WIDTH, ScreenHeight - 162.0f, ScreenWidth, ScreenHeight - 94.0f, mPaint);
            mPaint.setARGB(170, 0, 0, 0);
            canvas.drawRect(0.0f, ScreenHeight - 84.0f, ScreenWidth, ScreenHeight, mPaint);
            BtnMMPlay.draw(canvas, ImgBtnPlay, 0.66f, 0.66f, 255);
            BtnMMHelp.draw(canvas, ImgBtnHelp, 0.66f, 0.66f, 255);
            BtnMMMore.draw(canvas, ImgBtnMore, 0.66f, 0.66f, 255);
            mPaint.setARGB(150, 0, 0, 0);
            canvas.drawRect(0.0f, ScreenHeight - 84.0f, ScreenWidth, ScreenHeight, mPaint);
            BtnMMSettings.draw(canvas, ImgBtnSettingsOn, 0.66f, 0.66f, 255);
            BtnSTSound.draw(canvas, LizardView.SoundEnabled ? ImgBtnSoundOn : ImgBtnSoundOff, 0.66f, 0.66f, PageTransitionValInv * SCREEN_WIDTH, 0.0f, 255);
            BtnSTMusic.draw(canvas, LizardView.MusicEnabled ? ImgBtnMusicOn : ImgBtnMusicOff, 0.66f, 0.66f, PageTransitionValInv * SCREEN_WIDTH, 0.0f, 255);
            BtnSTFacebook.draw(canvas, ImgBtnSettingsFacebook, 0.66f, 0.66f, PageTransitionValInv * SCREEN_WIDTH, 0.0f, 255);
        } else if (CurPage == 10) {
            drawBitmapS(canvas, ImgWorldBckg, 0.67f, 0.67f * ScreenYMultiplier);
            for (int i12 = 0; i12 < NumDynamics; i12++) {
                Dynamics[i12].draw(canvas);
            }
            drawBitmapTSTA(canvas, ImgHelp[CurHelpPage], -185.0f, 0.0f, 160.0f, 15.0f, 0.8f, 0.8f, PageTransitionAlpha);
            mPaint.setARGB(170, 0, 0, 0);
            canvas.drawRect(0.0f, (ScreenHeight - 84.0f) + (PageTransitionValInv * 84.0f), ScreenWidth, ScreenHeight, mPaint);
            BtnHLBack.draw(canvas, ImgBtnBack, 0.66f, 0.66f, 0.0f, PageTransitionValInv * 84.0f, 255);
            BtnHLNext.draw(canvas, ImgBtnNext, 0.66f, 0.66f, 0.0f, PageTransitionValInv * 84.0f, 255);
        } else if (CurPage == 11) {
            drawBitmapS(canvas, ImgWorldBckg, 0.67f, 0.67f * ScreenYMultiplier);
            for (int i13 = 0; i13 < NumDynamics; i13++) {
                Dynamics[i13].draw(canvas);
            }
            drawBitmapTSTA(canvas, ImgHelp[CurHelpPage], -185.0f, 0.0f, 160.0f, 15.0f, 0.8f, 0.8f, PageTransitionAlpha);
            mPaint.setARGB(170, 0, 0, 0);
            canvas.drawRect(0.0f, (ScreenHeight - 84.0f) + (PageTransitionValInv * 84.0f), ScreenWidth, ScreenHeight, mPaint);
            if (CurHelpPage == TutorialPageLimit - 1) {
                BtnTTNext.draw(canvas, ImgBtnPlay, 0.66f, 0.66f, 0.0f, PageTransitionValInv * 84.0f, 255);
            } else {
                BtnTTNext.draw(canvas, ImgBtnNext, 0.66f, 0.66f, 0.0f, PageTransitionValInv * 84.0f, 255);
            }
        } else if (CurPage == 12) {
            drawBitmapS(canvas, ImgWorldBckg, 0.67f, 0.67f * ScreenYMultiplier);
            for (int i14 = 0; i14 < NumDynamics; i14++) {
                Dynamics[i14].draw(canvas);
            }
            drawBitmapTST(canvas, ImgMenuTitle, -162.0f, -90.0f, 160.0f, 100.0f, 0.67f, 0.67f);
            mPaint.setARGB(170, 0, 0, 0);
            canvas.drawRect(PageTransitionValInv * SCREEN_WIDTH, ScreenHeight - 162.0f, ScreenWidth, ScreenHeight - 94.0f, mPaint);
            mPaint.setARGB(170, 0, 0, 0);
            canvas.drawRect(0.0f, ScreenHeight - 84.0f, ScreenWidth, ScreenHeight, mPaint);
            BtnMMPlay.draw(canvas, ImgBtnPlay, 0.66f, 0.66f, 255);
            BtnMMSettings.draw(canvas, ImgBtnSettings, 0.66f, 0.66f, 255);
            BtnMMHelp.draw(canvas, ImgBtnHelp, 0.66f, 0.66f, 255);
            mPaint.setARGB(150, 0, 0, 0);
            canvas.drawRect(0.0f, ScreenHeight - 84.0f, ScreenWidth, ScreenHeight, mPaint);
            BtnMMMore.draw(canvas, ImgBtnMoreOn, 0.66f, 0.66f, 255);
            BtnMRMarket.draw(canvas, ImgBtnMarket, 0.66f, 0.66f, PageTransitionValInv * SCREEN_WIDTH, 0.0f, 255);
            BtnMRFacebook.draw(canvas, ImgBtnFacebook, 0.66f, 0.66f, PageTransitionValInv * SCREEN_WIDTH, 0.0f, 255);
            BtnMRTwitter.draw(canvas, ImgBtnTwitter, 0.66f, 0.66f, PageTransitionValInv * SCREEN_WIDTH, 0.0f, 255);
            BtnMRBuy.draw(canvas, ImgBtnBuy, 0.66f, 0.66f, PageTransitionValInv * SCREEN_WIDTH, 0.0f, 255);
        }
        mPaint.setARGB(255, 0, 0, 0);
        canvas.drawRect(0.0f, ScreenHeight, ScreenWidth, ScreenHeight + AD_HEIGHT + 24.0f, mPaint);
        if (CurPage == 2 || CurPage == 3) {
            mPaint.setARGB(255, 255, 255, 255);
            if (LizardView.CurWorld < LizardView.NumWorldsUnlocked) {
                mPaint.setTextAlign(Paint.Align.LEFT);
                mPaint.setTextSize(14.0f);
                if (LizardView.WorldInfos[LizardView.CurWorld].Place > 0) {
                    canvas.drawText("YOU ARE ON " + Integer.toString(LizardView.WorldInfos[LizardView.CurWorld].Place) + ". PLACE IN " + WorldInfo.WORLD_NAMES[LizardView.CurWorld] + " (BEST " + Integer.toString(LizardView.WorldInfos[LizardView.CurWorld].Highscore) + ")", 5.0f, ScreenHeight + 17.0f, mPaint);
                } else {
                    canvas.drawText("CONNECT WITH FACEBOOK TO GET HIGHSCORES", 5.0f, ScreenHeight + 17.0f, mPaint);
                }
            } else {
                mPaint.setTextAlign(Paint.Align.LEFT);
                mPaint.setTextSize(14.0f);
                canvas.drawText("COMPLETE PREVIOUS WORLD TO UNLOCK", 5.0f, ScreenHeight + 17.0f, mPaint);
            }
        } else if (CurPage == 4) {
            Game.drawScore(canvas);
        } else if (CurPage == 5) {
            Game.drawScore(canvas);
        } else if (CurPage == 6) {
            Game.drawScore(canvas);
        } else if (CurPage == 7) {
            Game.drawScore(canvas);
        } else if (CurPage == 8) {
            Game.drawScore(canvas, Game.Score - FinishedScore);
        }
    }

    public static void drawBitmap(Canvas canvas, Bitmap bitmap) {
        mMatrix.reset();
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapS(Canvas canvas, Bitmap bitmap, float scaleX, float scaleY) {
        mMatrix.reset();
        mMatrix.postScale(scaleX, scaleY);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapT(Canvas canvas, Bitmap bitmap, float posX, float posY) {
        mMatrix.reset();
        mMatrix.postTranslate(posX, posY);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapST(Canvas canvas, Bitmap bitmap, float posX, float posY, float scaleX, float scaleY) {
        mMatrix.reset();
        mMatrix.postScale(scaleX, scaleY);
        mMatrix.postTranslate(posX, posY);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapSRotT(Canvas canvas, Bitmap bitmap, float posX, float posY, float scaleX, float scaleY, float angle) {
        mMatrix.reset();
        mMatrix.postScale(scaleX, scaleY);
        mMatrix.postRotate(angle);
        mMatrix.postTranslate(posX, posY);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapSTA(Canvas canvas, Bitmap bitmap, float posX, float posY, float scaleX, float scaleY, int a) {
        mMatrix.reset();
        mMatrix.postScale(scaleX, scaleY);
        mMatrix.postTranslate(posX, posY);
        mPaint.setARGB(a, 255, 255, 255);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapTST(Canvas canvas, Bitmap bitmap, float pposX, float pposY, float posX, float posY, float scaleX, float scaleY) {
        mMatrix.reset();
        mMatrix.postTranslate(pposX, pposY);
        mMatrix.postScale(scaleX, scaleY);
        mMatrix.postTranslate(posX, posY);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapTRotT(Canvas canvas, Bitmap bitmap, float pposX, float pposY, float posX, float posY, float angle) {
        mMatrix.reset();
        mMatrix.postTranslate(pposX, pposY);
        mMatrix.postRotate(angle);
        mMatrix.postTranslate(posX, posY);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapTSRotT(Canvas canvas, Bitmap bitmap, float pposX, float pposY, float posX, float posY, float scaleX, float scaleY, float angle) {
        mMatrix.reset();
        mMatrix.postTranslate(pposX, pposY);
        mMatrix.postScale(scaleX, scaleY);
        mMatrix.postRotate(angle);
        mMatrix.postTranslate(posX, posY);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapTSRotTA(Canvas canvas, Bitmap bitmap, float pposX, float pposY, float posX, float posY, float scaleX, float scaleY, float angle, int a) {
        mMatrix.reset();
        mMatrix.postTranslate(pposX, pposY);
        mMatrix.postScale(scaleX, scaleY);
        mMatrix.postRotate(angle);
        mMatrix.postTranslate(posX, posY);
        mPaint.setARGB(a, 255, 255, 255);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapTSTA(Canvas canvas, Bitmap bitmap, float pposX, float pposY, float posX, float posY, float scaleX, float scaleY, int a) {
        mMatrix.reset();
        mMatrix.postTranslate(pposX, pposY);
        mMatrix.postScale(scaleX, scaleY);
        mMatrix.postTranslate(posX, posY);
        mPaint.setARGB(a, 255, 255, 255);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapSTTileX(Canvas canvas, Bitmap bitmap, float posX, float posY, float scaleX, float scaleY, int tilesX) {
        float w = ((float) bitmap.getWidth()) * scaleX;
        float p = posX;
        for (int i = 0; i < tilesX && p <= ScreenWidth; i++) {
            mMatrix.reset();
            mMatrix.postScale(scaleX, scaleY);
            mMatrix.postTranslate(p, posY);
            canvas.drawBitmap(bitmap, mMatrix, mPaint);
            p += w - 1.0f;
        }
    }

    public static void goToPage(int _NextPage, boolean _FadeOut, boolean _FadeIn) {
        NextPage = _NextPage;
        PageTransition = 1;
        PageTransitionVal = 1.0f;
        PageTransitionValInv = 0.0f;
        PageTransitionAlpha = 255;
        FadeOut = _FadeOut;
        FadeIn = _FadeIn;
    }
}
