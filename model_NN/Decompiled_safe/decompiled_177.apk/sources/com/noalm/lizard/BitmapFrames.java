package com.noalm.lizard;

import android.graphics.Bitmap;

public class BitmapFrames {
    public Bitmap[] Bmps;
    public int MaxNumFrames = 0;
    public int NumFrames = 0;

    public BitmapFrames(int _MaxNumFrames) {
        this.MaxNumFrames = _MaxNumFrames;
        this.Bmps = new Bitmap[this.MaxNumFrames];
    }

    public boolean addFrame(Bitmap Bmp) {
        if (this.NumFrames >= this.MaxNumFrames) {
            return false;
        }
        this.Bmps[this.NumFrames] = Bmp;
        this.NumFrames++;
        return true;
    }

    public void destroy() {
        this.NumFrames = 0;
    }
}
