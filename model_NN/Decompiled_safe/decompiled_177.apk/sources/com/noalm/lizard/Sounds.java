package com.noalm.lizard;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

public class Sounds {
    public static int CurMusic = -1;
    public static final int MUSIC_GAME = 1;
    public static final int MUSIC_MENU = 0;
    public static MediaPlayer[] Music = new MediaPlayer[4];
    public static final int SOUND_BUTTON = 0;
    public static final int SOUND_EAT = 1;
    public static final int SOUND_HURT1 = 2;
    public static final int SOUND_HURT2 = 3;
    public static final int SOUND_NEW_HIGHSCORE = 6;
    public static final int SOUND_YEAH = 4;
    public static final int SOUND_YUPPIE = 5;
    public static final int[] SoundIDs = new int[16];
    public static AudioManager mAudioManager = null;
    public static Context mContext;
    public static SoundPool mSoundPool = null;

    public Sounds(Context context) {
        mContext = context;
        mSoundPool = new SoundPool(16, 3, 0);
        mAudioManager = (AudioManager) context.getSystemService("audio");
        SoundIDs[0] = mSoundPool.load(mContext, R.raw.button1, 1);
        SoundIDs[1] = mSoundPool.load(mContext, R.raw.eat, 1);
        SoundIDs[2] = mSoundPool.load(mContext, R.raw.hurt1, 1);
        SoundIDs[3] = mSoundPool.load(mContext, R.raw.hurt2, 1);
        SoundIDs[4] = mSoundPool.load(mContext, R.raw.yeah, 1);
        SoundIDs[5] = mSoundPool.load(mContext, R.raw.yuppie, 1);
        SoundIDs[6] = mSoundPool.load(mContext, R.raw.new_highscore, 1);
        Music[0] = MediaPlayer.create(LizardView.mLizard, (int) R.raw.music1);
        Music[1] = MediaPlayer.create(LizardView.mLizard, (int) R.raw.music2);
    }

    static void destroy() {
        if (mSoundPool != null) {
            mSoundPool.release();
        }
        Music[0].release();
        Music[1].release();
        CurMusic = -1;
    }

    public static void playSound(int soundInd, int times, float frequency) {
        if (LizardView.SoundEnabled) {
            float media_vol = getMediaFinalVolume(0.99f);
            int play = mSoundPool.play(SoundIDs[soundInd], media_vol, media_vol, 1, times, frequency);
        }
    }

    public static void playMusic(int musicInd, boolean loop) {
        if (LizardView.MusicEnabled) {
            if (CurMusic >= 0 && CurMusic != musicInd) {
                stopCurMusic();
            }
            if (Music[musicInd] != null) {
                if (!Music[musicInd].isPlaying()) {
                    float media_vol = getMediaFinalVolume(0.99f);
                    Music[musicInd].setVolume(media_vol, media_vol);
                    Music[musicInd].setLooping(loop);
                    Music[musicInd].start();
                }
                CurMusic = musicInd;
            }
        }
    }

    public static void pauseMusic() {
        if (CurMusic >= 0 && Music[CurMusic] != null && Music[CurMusic].isPlaying()) {
            Music[CurMusic].pause();
        }
    }

    public static void resumeMusic() {
        if (CurMusic >= 0 && Music[CurMusic] != null && !Music[CurMusic].isPlaying()) {
            Music[CurMusic].start();
        }
    }

    public static void stopCurMusic() {
        if (CurMusic >= 0) {
            if (Music[CurMusic] == null) {
                CurMusic = -1;
                return;
            }
            Music[CurMusic].pause();
            Music[CurMusic].seekTo(0);
            CurMusic = -1;
        }
    }

    public static float getMediaFinalVolume(float volume) {
        return (((float) mAudioManager.getStreamVolume(3)) / ((float) mAudioManager.getStreamMaxVolume(3))) * volume;
    }

    public static void setCurMusicVolume() {
        if (LizardView.MusicEnabled && CurMusic >= 0 && Music[CurMusic] != null && Music[CurMusic].isPlaying()) {
            float media_vol = getMediaFinalVolume(0.99f);
            Music[CurMusic].setVolume(media_vol, media_vol);
        }
    }
}
