package com.noalm.lizard;

import org.json.JSONObject;

public class FBUser {
    public String FirstName = null;
    public String ID = null;
    public String LastName = null;

    public FBUser(JSONObject json_obj) {
        try {
            this.ID = json_obj.getString("id");
            this.FirstName = json_obj.getString("first_name");
            this.LastName = json_obj.getString("last_name");
        } catch (Exception e) {
        }
    }
}
