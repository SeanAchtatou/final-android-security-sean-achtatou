package com.noalm.lizard;

import android.graphics.Canvas;

public class Letters {
    private boolean Active = false;
    private float Alpha = 1.0f;
    private float MaxSize = 0.0f;
    private float MinSize = 0.0f;
    private Vector2 Pos = new Vector2(0.0f, 0.0f);
    private int[] Rgb = new int[3];
    private float Size = 1.0f;
    private String Text = "";
    private float Val = 0.0f;

    public void init(float _MaxSize, int _R, int _G, int _B, float posX, float posY, String _Text) {
        this.MinSize = 0.1f * _MaxSize;
        this.MaxSize = _MaxSize;
        this.Rgb[0] = _R;
        this.Rgb[1] = _G;
        this.Rgb[2] = _B;
        this.Pos.set(posX, posY);
        this.Text = _Text;
        this.Active = true;
        this.Val = 0.0f;
        this.Size = this.MinSize;
        this.Alpha = 1.0f;
        if (this.Pos.x < 30.0f) {
            this.Pos.x = 30.0f;
        } else if (this.Pos.x > UI.ScreenWidth - 30.0f) {
            this.Pos.x = UI.ScreenWidth - 30.0f;
        }
        if (this.Pos.y < 40.0f) {
            this.Pos.y = 40.0f;
        } else if (this.Pos.y > UI.ScreenHeight - 20.0f) {
            this.Pos.y = UI.ScreenHeight - 20.0f;
        }
    }

    public boolean update() {
        if (!this.Active) {
            return false;
        }
        this.Val += 0.02f * Game.fTimeMultiplier;
        if (this.Val > 1.0f) {
            this.Active = false;
            return false;
        }
        this.Pos.y -= 0.5f * Game.fTimeMultiplier;
        if (this.Val < 0.3f) {
            this.Size = this.MinSize + (((this.MaxSize - this.MinSize) * this.Val) / 0.3f);
            this.Alpha = 1.0f;
        } else {
            this.Size = this.MaxSize;
            if (this.Val > 0.7f) {
                this.Alpha = (1.0f - this.Val) / 0.3f;
            }
        }
        return true;
    }

    public void draw(Canvas canvas) {
        UI.mPaint.setTextSize(this.Size);
        UI.mPaint.setARGB((int) (this.Alpha * 255.0f), 0, 0, 0);
        canvas.drawText(this.Text, this.Pos.x - 1.0f, this.Pos.y + 1.0f, UI.mPaint);
        UI.mPaint.setARGB((int) (this.Alpha * 255.0f), this.Rgb[0], this.Rgb[1], this.Rgb[2]);
        canvas.drawText(this.Text, this.Pos.x, this.Pos.y, UI.mPaint);
    }
}
