package com.noalm.lizard;

import android.graphics.Bitmap;

public class BitmapAnimation {
    private BitmapFrames BmpFrames = null;
    private boolean ChangeDirection = false;
    private int CurFrame = 0;
    private long Delay = 0;
    private boolean FrameDir = true;
    private long FrameTime = 0;
    private boolean Loop = false;

    public void init(long _Delay, boolean _Loop, boolean _ChangeDirection, BitmapFrames _BmpFrames) {
        destroy();
        this.Delay = _Delay;
        this.Loop = _Loop;
        this.ChangeDirection = _ChangeDirection;
        this.BmpFrames = _BmpFrames;
    }

    public void update() {
        this.FrameTime += Game.DeltaTime;
        if (this.FrameTime > this.Delay) {
            if (this.ChangeDirection) {
                if (this.FrameDir) {
                    this.CurFrame++;
                    if (this.CurFrame > this.BmpFrames.NumFrames - 2) {
                        this.FrameDir = false;
                    }
                } else {
                    this.CurFrame--;
                    if (this.CurFrame < 1) {
                        this.FrameDir = true;
                    }
                }
            } else if (this.Loop) {
                this.CurFrame++;
                if (this.CurFrame >= this.BmpFrames.NumFrames) {
                    this.CurFrame = 0;
                }
            } else if (this.CurFrame < this.BmpFrames.NumFrames - 1) {
                this.CurFrame++;
            }
            this.FrameTime = 0;
        }
    }

    public void destroy() {
        this.Delay = 0;
        this.Loop = false;
        this.ChangeDirection = false;
        this.CurFrame = 0;
        this.FrameTime = 0;
        this.FrameDir = true;
        this.BmpFrames = null;
    }

    public int getCurFrame() {
        return this.CurFrame;
    }

    public void setCurFrame(int _CurFrame) {
        if (this.BmpFrames != null && _CurFrame >= 0 && _CurFrame < this.BmpFrames.NumFrames) {
            this.CurFrame = _CurFrame;
        }
    }

    public Bitmap getCurFrameBmp() {
        return this.BmpFrames.Bmps[this.CurFrame];
    }
}
