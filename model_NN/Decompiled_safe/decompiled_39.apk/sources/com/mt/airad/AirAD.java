package com.mt.airad;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.Display;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

public class AirAD extends RelativeLayout {
    protected static final int ANIMATION_FADE_IN_FADE_OUT = 7;
    public static final int ANIMATION_FIXED = 8;
    protected static final int ANIMATION_SCROLL_LEFT = 3;
    protected static final int ANIMATION_SCROLL_RIGHT = 2;
    public static final int ANIMATION_SCROLL_VERTICAL = 1;
    protected static final int ANIMATION_ZOOM = 4;
    public static final int POSITION_BOTTOM = 11;
    public static final int POSITION_TOP = 12;
    private String _$1 = "";
    private String _$2 = null;
    /* access modifiers changed from: private */
    public String _$3 = null;
    /* access modifiers changed from: private */
    public WebView _$4 = null;
    /* access modifiers changed from: private */
    public Handler _$5;
    private boolean _$6 = false;
    protected lIlIllIIIlIlIlII _$7 = null;
    protected Activity activity = null;
    protected AdListener adListener = null;
    protected IlIlIlllIIlIIIIl animationController = null;
    protected int animationType = 0;
    protected int positionType = 0;

    public interface AdListener {
        void onAdBannerClicked();

        void onAdBannerReceive();

        void onAdBannerReceiveFailed();

        void onMultiAdDismiss();
    }

    public AirAD(Activity activity2, int i, int i2) {
        super(activity2);
        if (activity2 != null) {
            this.activity = activity2;
            this.positionType = i;
            this.animationType = i2;
            activity2.getWindow().setSoftInputMode(32);
            this._$5 = new lIIIIIllIlIIllll(this);
            if (llIIlllIlIllIIll._$7 == null) {
                _$1((Context) activity2);
                IllIIIIllllIIIll._$12(activity2.getPackageName());
                lllllIllllIllIIl._$1 += activity2.getPackageName();
                Display defaultDisplay = activity2.getWindowManager().getDefaultDisplay();
                IllIIIIllllIIIll._$2(defaultDisplay.getHeight() + "");
                IllIIIIllllIIIll._$1(defaultDisplay.getWidth() + "");
                if (IllIIIIllllIIIll._$7) {
                    llIIlllIlIllIIll._$7 = llIIlllIlIllIIll._$3(activity2.getApplicationContext());
                } else {
                    lllllIllllIllIIl._$8();
                }
            }
            if (_$7(this)) {
                _$2(activity2);
                if (getChildCount() == 0) {
                    this._$7 = new lIlIllIIIlIlIlII(this.activity, this, this._$4, this.positionType);
                }
                this.animationController = new IlIlIlllIIlIIIIl(i, i2);
            }
        }
    }

    private TextView _$1(Activity activity2) {
        TextView textView = new TextView(activity2);
        textView.setText("");
        textView.setBackgroundDrawable(new ColorDrawable(0));
        textView.setOnClickListener(new llllllllIlIIllll(this));
        return textView;
    }

    private JSONObject _$1() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("P", IllIIIIllllIIIll._$12());
            jSONObject.put("C", IllIIIIllllIIIll._$11());
            jSONObject.put("A", IllIIIIllllIIIll._$10());
            jSONObject.put("LA", IllIIIIllllIIIll._$14());
            jSONObject.put("LO", IllIIIIllllIIIll._$13());
            jSONObject.put("AI", IllIIIIllllIIIll._$7());
            jSONObject.put("SA", IllIIIIllllIIIll._$5());
            jSONObject.put("SID", IllIIIIllllIIIll._$6());
            jSONObject.put("ID", this._$2);
            jSONObject.put("SW", IllIIIIllllIIIll._$2());
            jSONObject.put("SH", IllIIIIllllIIIll._$3());
            return jSONObject;
        } catch (JSONException e) {
            return null;
        } catch (NullPointerException e2) {
            return null;
        }
    }

    private void _$1(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 1);
            if (packageInfo != null && packageInfo.activities != null) {
                for (ActivityInfo activityInfo : packageInfo.activities) {
                    if ("com.mt.airad.MultiAD".equals(activityInfo.name)) {
                        IllIIIIllllIIIll._$7 = true;
                    }
                }
                if (!IllIIIIllllIIIll._$7) {
                    lllllIllllIllIIl._$8();
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            IllIIIIllllIIIll._$7 = false;
        }
    }

    private void _$2() {
        String str;
        try {
            str = _$1().toString();
        } catch (Exception e) {
            str = null;
        }
        llIIlllIlIllIIll._$7._$1(str);
    }

    private void _$2(Activity activity2) {
        this._$4 = new WebView(activity2);
        this._$4.addView(_$1(activity2), new RelativeLayout.LayoutParams(-1, -1));
        WebSettings settings = this._$4.getSettings();
        settings.setSupportZoom(true);
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(1);
        settings.setSupportMultipleWindows(false);
        this._$4.setVerticalScrollBarEnabled(false);
        this._$4.setHorizontalScrollBarEnabled(false);
        this._$4.setWebViewClient(new IlllllllIlIIllll(this));
    }

    /* access modifiers changed from: private */
    public void _$4() {
        if (this._$6 || IllIIIIllllIIIll._$5) {
            lllllIllllIllIIl._$2();
            return;
        }
        this._$1 = this._$2;
        _$2();
        this._$6 = true;
        _$6();
    }

    /* access modifiers changed from: private */
    public void _$5() {
        if (llIIlllIlIllIIll._$7 != null) {
            llIIlllIlIllIIll._$7._$7();
        }
    }

    /* access modifiers changed from: private */
    public void _$6() {
        if (this.animationType != 8) {
            this.animationController.hideAction(this);
        }
    }

    private boolean _$7(AirAD airAD) {
        return llIIlllIlIllIIll._$7._$2(airAD);
    }

    public static void setGlobalParameter(String str, double d) {
        IllIIIIllllIIIll._$5(str);
        IllIIIIllllIIIll._$1(Math.round(1000.0d * d));
    }

    /* access modifiers changed from: protected */
    public void _$3() {
        this._$5.sendEmptyMessage(6);
    }

    public void destory() {
        if (llIIlllIlIllIIll._$7 != null) {
            llIIlllIlIllIIll._$7._$1(this);
        }
    }

    /* access modifiers changed from: protected */
    public void judgeActivityState() {
        if (this.activity != null && this.activity.isFinishing()) {
            destory();
        }
    }

    public void setAdListener(AdListener adListener2) {
        this.adListener = adListener2;
    }

    /* access modifiers changed from: protected */
    public void showAd(String str, boolean z) {
        if (z && this.activity != null) {
            lllllIllllIllIIl._$10();
            Intent intent = new Intent();
            intent.setClass(this.activity, MultiAD.class);
            intent.putExtra("adURL", str);
            intent.putExtra("adID", this._$1);
            this.activity.startActivity(intent);
        } else if (!z) {
            lllllIllllIllIIl._$9();
        }
        this._$6 = false;
    }

    /* access modifiers changed from: protected */
    public void showBanner(String str, String str2, boolean z) {
        if (this.activity != null && this.activity.isFinishing()) {
            destory();
        } else if (!this.activity.hasWindowFocus()) {
        } else {
            if (z) {
                this._$2 = str;
                this._$3 = str2;
                this._$5.sendEmptyMessage(3);
                return;
            }
            this._$5.sendEmptyMessage(4);
        }
    }

    /* access modifiers changed from: protected */
    public void startAdmob() {
        this._$5.sendEmptyMessage(5);
    }
}
