package com.mt.airad;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import org.json.JSONException;
import org.json.JSONObject;

public class MultiAD extends Activity implements Animation.AnimationListener {
    private ImageButton _$1;
    private RelativeLayout _$2;
    private Animation _$3;
    private Animation _$4;
    /* access modifiers changed from: private */
    public WebView _$5;
    private String _$6 = null;
    /* access modifiers changed from: private */
    public boolean _$7 = false;

    /* access modifiers changed from: private */
    public void _$1() {
        IllIIIIllllIIIll._$5 = false;
        try {
            _$1(this._$2);
            _$2();
        } catch (Exception e) {
            finish();
        }
    }

    private void _$1(View view) {
        if (view.getVisibility() == 0) {
            view.setVisibility(8);
            this._$3 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
            this._$3.setAnimationListener(this);
            this._$3.setDuration(400);
            view.startAnimation(this._$3);
        }
    }

    private void _$2() {
        Intent intent = new Intent(IllIIIIllllIIIll._$15());
        try {
            intent.putExtra("adViewLeaveParameter", _$3().toString());
        } catch (NullPointerException e) {
            intent.putExtra("adViewLeaveParameter", "0");
        }
        sendBroadcast(intent);
    }

    private void _$2(View view) {
        if (view.getVisibility() == 8) {
            view.setVisibility(0);
            this._$4 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
            this._$4.setDuration(400);
            view.startAnimation(this._$4);
        }
    }

    private JSONObject _$3() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("ID", this._$6);
            jSONObject.put("AI", IllIIIIllllIIIll._$7());
            jSONObject.put("SID", IllIIIIllllIIIll._$6());
            jSONObject.put("SA", IllIIIIllllIIIll._$5());
            return jSONObject;
        } catch (JSONException e) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void _$4() {
        if (this._$1.getVisibility() != 0 && IllIIIIllllIIIll._$18 != null) {
            this._$1.setImageBitmap(IllIIIIllllIIIll._$18);
            this._$1.setVisibility(0);
        }
    }

    private void _$5() {
        this._$2 = new RelativeLayout(this);
        this._$2.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this._$2.addView(this._$5, new ViewGroup.LayoutParams(-1, -1));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(10, -1);
        layoutParams.addRule(11, -1);
        layoutParams.topMargin = 5;
        layoutParams.rightMargin = 10;
        this._$1.setVisibility(4);
        this._$2.addView(this._$1, layoutParams);
        setContentView(this._$2, new ViewGroup.LayoutParams(-1, -1));
        this._$2.setVisibility(8);
    }

    private void _$6() {
        this._$1 = new ImageButton(this);
        this._$1.setBackgroundColor(Color.argb(0, 0, 0, 0));
        this._$1.setOnClickListener(new llllIllIlllIIIII(this));
    }

    private void _$7() {
        this._$5 = new WebView(this);
        WebSettings settings = this._$5.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(1);
        this._$5.setVerticalScrollBarEnabled(false);
        this._$5.setHorizontalScrollBarEnabled(false);
        this._$5.setWebViewClient(new IlllIllIlllIIIII(this));
    }

    /* access modifiers changed from: protected */
    public void _$1(String str) {
        if (str == null) {
            finish();
            return;
        }
        this._$5.loadUrl(str);
        _$2(this._$2);
        this._$5.setFocusableInTouchMode(true);
        this._$5.requestFocus();
    }

    /* access modifiers changed from: protected */
    public void _$8() {
        requestWindowFeature(1);
    }

    public void onAnimationEnd(Animation animation) {
        if (animation == this._$3) {
            finish();
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        _$8();
        setRequestedOrientation(1);
        _$7();
        _$6();
        _$5();
        Intent intent = getIntent();
        _$1(intent.getStringExtra("adURL"));
        this._$6 = intent.getStringExtra("adID");
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            if (this._$5.canGoBack()) {
                this._$5.goBack();
                return true;
            }
            _$1();
            return true;
        } else if (i == 82) {
            return true;
        } else {
            super.onKeyDown(i, keyEvent);
            return false;
        }
    }
}
