package com.waps.ads.adapters;

import android.graphics.Color;
import android.util.Log;
import android.view.ViewGroup;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.AdGroupTargeting;
import com.waps.ads.a.a;
import com.waps.ads.b.b;
import com.waps.ads.b.c;
import com.waps.ads.g;
import com.wooboo.adlib_android.AdListener;
import com.wooboo.adlib_android.WoobooAdView;

public class WoobooAdapter extends a implements AdListener {
    public WoobooAdapter(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
    }

    public void handle() {
        if (AdGroupTargeting.getTestMode()) {
            Log.d("AdView SDK", "Into Wooboo");
        }
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            b bVar = adGroupLayout.d;
            WoobooAdView woobooAdView = new WoobooAdView(adGroupLayout.getContext(), this.d.e, Color.rgb(bVar.e, bVar.f, bVar.g), Color.rgb(bVar.a, bVar.b, bVar.c), AdGroupTargeting.getTestMode(), 600);
            woobooAdView.setHorizontalScrollBarEnabled(false);
            woobooAdView.setVerticalScrollBarEnabled(false);
            woobooAdView.setAdListener(this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void
     arg types: [com.waps.ads.AdGroupLayout, com.wooboo.adlib_android.WoobooAdView]
     candidates:
      com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.View):void
      com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void */
    public void onFailedToReceiveAd(Object obj) {
        WoobooAdView woobooAdView = (WoobooAdView) obj;
        if (AdGroupTargeting.getTestMode()) {
            Log.d("AdView SDK", "Woboo failure");
        }
        woobooAdView.setAdListener((AdListener) null);
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            adGroupLayout.j.resetRollover();
            adGroupLayout.b.post(new g(adGroupLayout, (ViewGroup) woobooAdView));
            adGroupLayout.rotateThreadedDelayed();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void
     arg types: [com.waps.ads.AdGroupLayout, com.wooboo.adlib_android.WoobooAdView]
     candidates:
      com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.View):void
      com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void */
    public void onReceiveAd(Object obj) {
        WoobooAdView woobooAdView = (WoobooAdView) obj;
        if (AdGroupTargeting.getTestMode()) {
            Log.d("AdView SDK", "Wooboo success");
        }
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            adGroupLayout.j.resetRollover();
            adGroupLayout.b.post(new g(adGroupLayout, (ViewGroup) woobooAdView));
            adGroupLayout.rotateThreadedDelayed();
        }
    }
}
