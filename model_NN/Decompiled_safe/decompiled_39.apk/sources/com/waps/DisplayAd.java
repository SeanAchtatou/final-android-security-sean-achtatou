package com.waps;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.ViewGroup;
import java.io.ByteArrayInputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DisplayAd {
    /* access modifiers changed from: private */
    public static DisplayAdNotifier e;
    /* access modifiers changed from: private */
    public static n f = null;
    /* access modifiers changed from: private */
    public static String k;
    /* access modifiers changed from: private */
    public static String l;
    /* access modifiers changed from: private */
    public static String m;
    private static final byte[] n = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, 62, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51};
    Bitmap a;
    View b;
    final String c = "Display Ad";
    private p d = null;
    /* access modifiers changed from: private */
    public Context g;
    /* access modifiers changed from: private */
    public String h = "";
    private String i = "";
    /* access modifiers changed from: private */
    public String j = "";
    private byte[] o;
    private int p;
    private int q;
    private boolean r;
    private int s;

    public DisplayAd(Context context) {
        this.g = context;
        f = new n();
        Context context2 = this.g;
        Context context3 = this.g;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context2.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.getExtraInfo() != null && activeNetworkInfo.getExtraInfo().equals("cmwap")) {
            f.a(true);
        }
    }

    /* access modifiers changed from: private */
    public boolean buildResponse(String str) {
        DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
        try {
            Document parse = newInstance.newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
            k = getNodeTrimValue(parse.getElementsByTagName("ClickUrl"));
            l = getNodeTrimValue(parse.getElementsByTagName("AdPackage"));
            m = getNodeTrimValue(parse.getElementsByTagName("NewBrowser"));
            String nodeTrimValue = getNodeTrimValue(parse.getElementsByTagName("Image"));
            decodeBase64(nodeTrimValue.getBytes(), 0, nodeTrimValue.getBytes().length);
            this.a = BitmapFactory.decodeByteArray(this.o, 0, this.p);
            this.b = new View(this.g);
            this.b.setLayoutParams(new ViewGroup.LayoutParams(this.a.getWidth(), this.a.getHeight()));
            this.b.setBackgroundDrawable(new BitmapDrawable(this.a));
            this.b.setOnClickListener(new o(this));
            e.getDisplayAdResponse(this.b);
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    private String getNodeTrimValue(NodeList nodeList) {
        Element element = (Element) nodeList.item(0);
        if (element == null) {
            return null;
        }
        NodeList childNodes = element.getChildNodes();
        int length = childNodes.getLength();
        String str = "";
        for (int i2 = 0; i2 < length; i2++) {
            Node item = childNodes.item(i2);
            if (item != null) {
                str = str + item.getNodeValue();
            }
        }
        if (str == null || str.equals("")) {
            return null;
        }
        return str.trim();
    }

    /* access modifiers changed from: package-private */
    public void decodeBase64(byte[] bArr, int i2, int i3) {
        byte b2;
        this.o = new byte[bArr.length];
        this.p = 0;
        this.r = false;
        this.q = 0;
        if (i3 < 0) {
            this.r = true;
        }
        int i4 = 0;
        int i5 = i2;
        while (true) {
            if (i4 >= i3) {
                break;
            }
            int i6 = i5 + 1;
            byte b3 = bArr[i5];
            if (b3 == 61) {
                this.r = true;
                break;
            }
            if (b3 >= 0 && b3 < n.length && (b2 = n[b3]) >= 0) {
                int i7 = this.q + 1;
                this.q = i7;
                this.q = i7 % 4;
                this.s = b2 + (this.s << 6);
                if (this.q == 0) {
                    byte[] bArr2 = this.o;
                    int i8 = this.p;
                    this.p = i8 + 1;
                    bArr2[i8] = (byte) ((this.s >> 16) & 255);
                    byte[] bArr3 = this.o;
                    int i9 = this.p;
                    this.p = i9 + 1;
                    bArr3[i9] = (byte) ((this.s >> 8) & 255);
                    byte[] bArr4 = this.o;
                    int i10 = this.p;
                    this.p = i10 + 1;
                    bArr4[i10] = (byte) (this.s & 255);
                }
            }
            i4++;
            i5 = i6;
        }
        if (this.r && this.q != 0) {
            this.s <<= 6;
            switch (this.q) {
                case 2:
                    this.s <<= 6;
                    byte[] bArr5 = this.o;
                    int i11 = this.p;
                    this.p = i11 + 1;
                    bArr5[i11] = (byte) ((this.s >> 16) & 255);
                    return;
                case 3:
                    byte[] bArr6 = this.o;
                    int i12 = this.p;
                    this.p = i12 + 1;
                    bArr6[i12] = (byte) ((this.s >> 16) & 255);
                    byte[] bArr7 = this.o;
                    int i13 = this.p;
                    this.p = i13 + 1;
                    bArr7[i13] = (byte) ((this.s >> 8) & 255);
                    return;
                default:
                    return;
            }
        }
    }

    public void getDisplayAdDataFromServer(String str, String str2, DisplayAdNotifier displayAdNotifier) {
        this.i = str;
        this.h = this.i + "display/ad?";
        this.j = str2;
        e = displayAdNotifier;
        this.d = new p(this, null);
        this.d.execute(new Void[0]);
    }
}
