package com.waps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class m extends BroadcastReceiver {
    public static String a = "";
    int b;
    l c;
    private String d;

    public m(l lVar, int i, String str) {
        this.b = i;
        this.c = lVar;
        this.d = str;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
            a = intent.getDataString().substring(8);
            AppConnect.getInstanceNoConnect(context).package_receiver(a, 0);
            try {
                this.c.a(this.b, this.d);
                context.startActivity(context.getPackageManager().getLaunchIntentForPackage(a));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
            AppConnect.getInstanceNoConnect(context).package_receiver(intent.getDataString().substring(8), 3);
        }
        context.unregisterReceiver(this);
    }
}
