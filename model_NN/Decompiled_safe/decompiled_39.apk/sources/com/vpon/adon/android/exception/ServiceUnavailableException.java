package com.vpon.adon.android.exception;

public class ServiceUnavailableException extends Exception {
    private static final long serialVersionUID = 5231379959825858930L;
    private final String msg;

    public ServiceUnavailableException(String msg2) {
        this.msg = msg2;
    }

    public String getMessage() {
        return this.msg;
    }
}
