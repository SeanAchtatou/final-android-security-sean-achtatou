package com.vpon.adon.android.entity;

public class ScreenSize {
    int screenHeight = 0;
    int screenWidth = 0;

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public void setScreenWidth(int screenWidth2) {
        this.screenWidth = screenWidth2;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public void setScreenHeight(int screenHeight2) {
        this.screenHeight = screenHeight2;
    }
}
