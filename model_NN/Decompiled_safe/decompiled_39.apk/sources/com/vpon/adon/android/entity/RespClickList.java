package com.vpon.adon.android.entity;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class RespClickList implements Serializable {
    private static final long serialVersionUID = 3887859748700692751L;
    private List<RespClick> respClicks = new LinkedList();

    public RespClickList(List<RespClick> respClicks2) {
        this.respClicks = respClicks2;
    }

    public void setRespClicks(List<RespClick> respClicks2) {
        this.respClicks = respClicks2;
    }

    public List<RespClick> getRespClicks() {
        return this.respClicks;
    }
}
