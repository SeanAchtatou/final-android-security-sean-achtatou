package com.x25.apps.PenguinLink;

import android.app.Application;

public class GameApplication extends Application {
    public boolean bCgOpen = true;
    private boolean bPlayMusic = true;
    public ConnectGame cg;
    public boolean isRun = true;

    public boolean getPlayMusic() {
        return this.bPlayMusic;
    }

    public void setPlayMusic(boolean bPlay) {
        this.bPlayMusic = bPlay;
    }

    public void setCgOpen(boolean bOpen) {
        this.bCgOpen = bOpen;
    }

    public boolean getCgoOpen() {
        return this.bCgOpen;
    }
}
