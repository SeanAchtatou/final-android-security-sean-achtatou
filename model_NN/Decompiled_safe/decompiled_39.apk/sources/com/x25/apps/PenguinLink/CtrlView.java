package com.x25.apps.PenguinLink;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class CtrlView extends GameView {
    public static boolean CURRENT_CH = false;
    public int CURRENT_TYPE = 0;
    /* access modifiers changed from: private */
    public Point C_POINT;
    public final int GAMETIME = 300;
    public int PROCESS_VALUE = 300;
    /* access modifiers changed from: private */
    public Point P_POINT;
    public final int UPTIME = 1;
    LinkedList<Line> li;
    private RefreshHandler mRedrawHandler = new RefreshHandler();
    private Store store;

    public CtrlView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.Gamecontext = context;
        initType();
        initGrid();
        this.much = (this.row - 2) * (this.col - 2);
        this.store = new Store(context);
    }

    public CtrlView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initType();
        initGrid();
        this.much = (this.row - 2) * (this.col - 2);
    }

    public void setbg() {
        if (this.Mybackground == 1) {
            this.Mybackground = 2;
        } else if (this.Mybackground == 2) {
            this.Mybackground = 3;
        } else if (this.Mybackground == 3) {
            this.Mybackground = 4;
        } else if (this.Mybackground == 4) {
            this.Mybackground = 5;
        } else if (this.Mybackground == 5) {
            this.Mybackground = 6;
        } else {
            this.Mybackground = 1;
        }
        setbg(this.Gamecontext);
        invalidate();
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != 0) {
            return super.onTouchEvent(event);
        }
        if (this.isLine) {
            return false;
        }
        int selX = (int) (event.getX() / this.width);
        int selY = (int) ((event.getY() - ((float) this.step)) / this.height);
        if (selX < 0 || selX >= this.row || selY < 0 || selY >= this.col) {
            return false;
        }
        if (this.grid[selX][selY] == 0) {
            return true;
        }
        if (!CURRENT_CH) {
            select(selX, selY);
            CURRENT_CH = true;
            this.P_POINT = new Point(selX, selY);
            if (this.store.load("effect") == 0) {
                ((ConnectGame) this.Gamecontext).music.click();
            }
        } else {
            this.C_POINT = new Point(selX, selY);
            this.lineType = 0;
            if (checkLink(this.P_POINT, this.C_POINT)) {
                this.isLine = true;
                this.much -= 2;
                if (this.PROCESS_VALUE > 0 && this.PROCESS_VALUE + 1 < 300) {
                    this.PROCESS_VALUE++;
                }
                CURRENT_CH = false;
                if (this.store.load("effect") == 0) {
                    ((ConnectGame) this.Gamecontext).music.yes();
                }
                this.nowScore = this.nowScore + this.lineType + Math.abs(this.P_POINT.x - this.C_POINT.x) + Math.abs((this.P_POINT.y - this.C_POINT.y) - 1);
                invalidate();
                this.mRedrawHandler.sleep(300);
            } else {
                select(selX, selY);
                CURRENT_CH = true;
                if (this.store.load("effect") == 0) {
                    if (this.C_POINT.equals(this.P_POINT)) {
                        ((ConnectGame) this.Gamecontext).music.no();
                    } else {
                        ((ConnectGame) this.Gamecontext).music.click();
                    }
                }
                this.P_POINT = new Point(selX, selY);
            }
        }
        return true;
    }

    public void changeStytle() {
        if (this.stytle == 1) {
            this.stytle = 2;
        } else {
            this.stytle = 1;
        }
        ReplaceType();
        fillImage((ConnectGame) this.Gamecontext);
        invalidate();
    }

    public void reset() {
        this.level = 1;
        CURRENT_CH = false;
        this.CURRENT_TYPE = 0;
        this.C_POINT = null;
        this.P_POINT = null;
        this.lineType = 0;
        this.isLine = false;
        initType();
        initGrid();
        this.much = (this.row - 2) * (this.col - 2);
        invalidate();
    }

    public void rearrange() {
        CURRENT_CH = false;
        this.CURRENT_TYPE = 0;
        this.C_POINT = null;
        this.P_POINT = null;
        this.lineType = 0;
        this.isLine = false;
        List<Integer> temp = new ArrayList<>();
        for (int i = 0; i < this.row; i++) {
            for (int j = 0; j < this.col; j++) {
                if (this.grid[i][j] != 0) {
                    temp.add(Integer.valueOf(this.grid[i][j]));
                }
            }
        }
        this.type.clear();
        Random ad = new Random();
        for (int i2 = 0; i2 < temp.size(); i2++) {
            this.type.add((Integer) temp.get(i2));
        }
        temp.clear();
        for (int i3 = 0; i3 < this.row; i3++) {
            for (int j2 = 0; j2 < this.col; j2++) {
                if (this.grid[i3][j2] != 0) {
                    int index = ad.nextInt(this.type.size());
                    this.grid[i3][j2] = ((Integer) this.type.get(index)).intValue();
                    this.type.remove(index);
                }
            }
        }
        invalidate();
    }

    class RefreshHandler extends Handler {
        RefreshHandler() {
        }

        public void handleMessage(Message msg) {
            CtrlView.this.isLine = false;
            CtrlView.this.grid[CtrlView.this.P_POINT.x][CtrlView.this.P_POINT.y] = 0;
            CtrlView.this.grid[CtrlView.this.C_POINT.x][CtrlView.this.C_POINT.y] = 0;
            CtrlView.this.invalidate();
        }

        public void sleep(long delayMillis) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), delayMillis);
        }
    }

    public class Point {
        public int x;
        public int y;

        public Point(int newx, int newy) {
            this.x = newx;
            this.y = newy;
        }

        public boolean equals(Point p) {
            if (p.x == this.x && p.y == this.y) {
                return true;
            }
            return false;
        }
    }

    private boolean horizon(Point a, Point b) {
        if (a.x == b.x && a.y == b.y) {
            return false;
        }
        int x_start = a.y <= b.y ? a.y : b.y;
        int x_end = a.y <= b.y ? b.y : a.y;
        for (int x = x_start + 1; x < x_end; x++) {
            if (this.grid[a.x][x] != 0) {
                return false;
            }
        }
        this.p = new Point[]{a, b};
        this.lineType = 1;
        return true;
    }

    private boolean vertical(Point a, Point b) {
        if (a.x == b.x && a.y == b.y) {
            return false;
        }
        int y_start = a.x <= b.x ? a.x : b.x;
        int y_end = a.x <= b.x ? b.x : a.x;
        for (int y = y_start + 1; y < y_end; y++) {
            if (this.grid[y][a.y] != 0) {
                return false;
            }
        }
        this.p = new Point[]{a, b};
        this.lineType = 1;
        return true;
    }

    private boolean oneCorner(Point a, Point b) {
        boolean method2;
        Point c = new Point(a.x, b.y);
        Point d = new Point(b.x, a.y);
        if (this.grid[c.x][c.y] == 0) {
            boolean method1 = horizon(a, c) && vertical(b, c);
            this.p = new Point[]{a, new Point(c.x, c.y), b};
            this.lineType = 2;
            return method1;
        } else if (this.grid[d.x][d.y] != 0) {
            return false;
        } else {
            if (!vertical(a, d) || !horizon(b, d)) {
                method2 = false;
            } else {
                method2 = true;
            }
            this.p = new Point[]{a, new Point(d.x, d.y), b};
            this.lineType = 2;
            return method2;
        }
    }

    class Line {
        public Point a;
        public Point b;
        public int direct;

        public Line() {
        }

        public Line(int direct2, Point a2, Point b2) {
            this.direct = direct2;
            this.a = a2;
            this.b = b2;
        }
    }

    private LinkedList<Line> scan(Point a, Point b) {
        this.li = new LinkedList<>();
        for (int y = a.y; y >= 0; y--) {
            if (this.grid[a.x][y] == 0 && this.grid[b.x][y] == 0 && vertical(new Point(a.x, y), new Point(b.x, y))) {
                this.li.add(new Line(0, new Point(a.x, y), new Point(b.x, y)));
            }
        }
        for (int y2 = a.y; y2 < this.col; y2++) {
            if (this.grid[a.x][y2] == 0 && this.grid[b.x][y2] == 0 && vertical(new Point(a.x, y2), new Point(b.x, y2))) {
                this.li.add(new Line(0, new Point(a.x, y2), new Point(b.x, y2)));
            }
        }
        for (int x = a.x; x >= 0; x--) {
            if (this.grid[x][a.y] == 0 && this.grid[x][b.y] == 0 && horizon(new Point(x, a.y), new Point(x, b.y))) {
                this.li.add(new Line(1, new Point(x, a.y), new Point(x, b.y)));
            }
        }
        for (int x2 = a.x; x2 < this.row; x2++) {
            if (this.grid[x2][a.y] == 0 && this.grid[x2][b.y] == 0 && horizon(new Point(x2, a.y), new Point(x2, b.y))) {
                this.li.add(new Line(1, new Point(x2, a.y), new Point(x2, b.y)));
            }
        }
        return this.li;
    }

    private boolean twoCorner(Point a, Point b) {
        this.li = scan(a, b);
        if (this.li.isEmpty()) {
            return false;
        }
        for (int index = 0; index < this.li.size(); index++) {
            Line line = this.li.get(index);
            if (line.direct == 1) {
                if (vertical(a, line.a) && vertical(b, line.b)) {
                    this.p = new Point[]{a, line.a, line.b, b};
                    this.lineType = 3;
                    return true;
                }
            } else if (horizon(a, line.a) && horizon(b, line.b)) {
                this.p = new Point[]{a, line.a, line.b, b};
                this.lineType = 3;
                return true;
            }
        }
        return false;
    }

    public boolean checkLink(Point a, Point b) {
        if (this.grid[a.x][a.y] != this.grid[b.x][b.y]) {
            return false;
        }
        if (a.x == b.x && horizon(a, b)) {
            return true;
        }
        if (a.y == b.y && vertical(a, b)) {
            return true;
        }
        if (oneCorner(a, b)) {
            return true;
        }
        return twoCorner(a, b);
    }
}
