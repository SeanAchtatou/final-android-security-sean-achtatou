package com.x25.apps.PenguinLink;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import com.x25.apps.PenguinLink.CtrlView;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class GameView extends View {
    public final int BACKGROUND_1 = 1;
    public final int BACKGROUND_2 = 2;
    public final int BACKGROUND_3 = 3;
    public final int BACKGROUND_4 = 4;
    public final int BACKGROUND_5 = 5;
    public final int BACKGROUND_6 = 6;
    public Context Gamecontext;
    public final int H_LINE = 1;
    public int Mybackground = 6;
    public final int ONE_C_LINE = 2;
    int SCREEN_H = 480;
    int SCREEN_W = 320;
    public final int STYTLE_DEFAULT = 1;
    public final int STYTLE_OTHER = 2;
    public final int TWO_C_LINE = 3;
    public final int V_LINE = 1;
    private Bitmap bg;
    public int col = 10;
    private Bitmap cursor;
    public int[][] grid = ((int[][]) Array.newInstance(Integer.TYPE, this.row, this.col));
    public float height;
    public Bitmap[] image;
    public int[] imageType = {R.drawable.pa, R.drawable.pb, R.drawable.pc, R.drawable.pd, R.drawable.pe, R.drawable.pf, R.drawable.pg, R.drawable.ph, R.drawable.pi, R.drawable.pj, R.drawable.pk, R.drawable.pl, R.drawable.pm, R.drawable.pn};
    public int[] imageType2 = {R.drawable.fa, R.drawable.fb, R.drawable.fc, R.drawable.fd, R.drawable.fe, R.drawable.ff, R.drawable.fg, R.drawable.fh, R.drawable.fi, R.drawable.fj, R.drawable.fk, R.drawable.fl, R.drawable.fm, R.drawable.fn};
    private Bitmap imgNum;
    private Bitmap imgbg;
    public boolean isLine = false;
    public int level = 1;
    public int lineType = 0;
    public int much = 0;
    public int nowScore = 0;
    CtrlView.Point[] p;
    public int row = 7;
    private Rect selRect = new Rect();
    private int selX;
    private int selY;
    public int step = 0;
    public int stytle = 1;
    public int topScore = 0;
    public List<Integer> type = new ArrayList();
    public float width;

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    public GameView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    public void reset() {
    }

    public void setSize(int w, int h) {
        this.SCREEN_W = w;
        this.SCREEN_H = h;
        this.width = (float) (w / this.row);
        this.height = this.width;
        fillImage(getContext());
    }

    public void setbg(Context context) {
        Drawable drw;
        Bitmap bitmap = Bitmap.createBitmap(this.SCREEN_W, this.SCREEN_H, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        switch (this.Mybackground) {
            case 1:
                drw = context.getResources().getDrawable(R.drawable.bg1);
                break;
            case 2:
                drw = context.getResources().getDrawable(R.drawable.bg2);
                break;
            case 3:
                drw = context.getResources().getDrawable(R.drawable.bg3);
                break;
            case 4:
                drw = context.getResources().getDrawable(R.drawable.bg4);
                break;
            case 5:
                drw = context.getResources().getDrawable(R.drawable.bg5);
                break;
            case 6:
                drw = context.getResources().getDrawable(R.drawable.bg6);
                break;
            default:
                drw = context.getResources().getDrawable(R.drawable.bg6);
                break;
        }
        drw.setBounds(1, 1, this.SCREEN_W, this.SCREEN_H);
        drw.draw(canvas);
        this.bg = bitmap;
    }

    public void fillImage(Context context) {
        int lth = this.imageType.length;
        switch (this.stytle) {
            case 1:
                lth = this.imageType.length;
                break;
            case 2:
                lth = this.imageType2.length;
                break;
        }
        this.image = new Bitmap[lth];
        for (int i = 0; i < lth; i++) {
            Bitmap bitmap = Bitmap.createBitmap((int) this.width, (int) this.height, Bitmap.Config.ARGB_8888);
            Drawable drw = null;
            Canvas canvas = new Canvas(bitmap);
            switch (this.stytle) {
                case 1:
                    drw = context.getResources().getDrawable(this.imageType[i]);
                    break;
                case 2:
                    drw = context.getResources().getDrawable(this.imageType2[i]);
                    break;
            }
            drw.setBounds(1, 1, (int) this.width, (int) this.height);
            drw.draw(canvas);
            this.image[i] = bitmap;
        }
        Bitmap bitmap2 = Bitmap.createBitmap((int) this.width, (int) this.height, Bitmap.Config.ARGB_8888);
        Canvas canvas2 = new Canvas(bitmap2);
        Drawable drw2 = context.getResources().getDrawable(R.drawable.cursor);
        drw2.setBounds(1, 1, (int) this.width, (int) this.height);
        drw2.draw(canvas2);
        this.cursor = bitmap2;
        Bitmap bitmap3 = Bitmap.createBitmap((int) this.width, (int) this.height, Bitmap.Config.ARGB_8888);
        Canvas canvas3 = new Canvas(bitmap3);
        Drawable drw3 = context.getResources().getDrawable(R.drawable.imgbg);
        drw3.setBounds(1, 1, (int) this.width, (int) this.height);
        drw3.draw(canvas3);
        this.imgbg = bitmap3;
        setbg(context);
    }

    public void paintNumber(Canvas canvas, int num, int x, int y, int w, int h) {
        Paint paint = new Paint();
        paint.setColor(-7829368);
        paint.setTextSize(22.0f);
        String tmpStr = new StringBuilder().append(Math.abs(num)).toString();
        for (int i = 0; i < tmpStr.length(); i++) {
            canvas.save();
            canvas.clipRect((i * w) + x, y, ((i + 1) * w) + x, y + h);
            canvas.drawBitmap(this.imgNum, (float) (((i - Integer.parseInt(new StringBuilder(String.valueOf(tmpStr.charAt(i))).toString())) * w) + x), (float) y, paint);
            canvas.restore();
        }
    }

    public void ReplaceType() {
        int len = this.imageType.length;
        for (int i = 0; i < this.row; i++) {
            for (int j = 0; j < this.col; j++) {
                if (this.grid[i][j] != 0) {
                    int k = 0;
                    while (true) {
                        if (k >= len) {
                            break;
                        }
                        if (this.stytle != 2) {
                            if (this.stytle == 1 && this.grid[i][j] == this.imageType2[k]) {
                                this.grid[i][j] = this.imageType[k];
                                break;
                            }
                        } else if (this.grid[i][j] == this.imageType[k]) {
                            this.grid[i][j] = this.imageType2[k];
                            break;
                        }
                        k++;
                    }
                }
            }
        }
    }

    public void initType() {
        int size = (this.row - 2) * (this.col - 2);
        int len = this.imageType.length;
        int times = size / 2;
        int times1 = times;
        int times2 = times;
        if (times % 2 != 0) {
            times1 = times + 1;
            times2 = times - 1;
        }
        for (int i = 0; i < times1; i = i + 1 + 1) {
            int j = i % len;
            switch (this.stytle) {
                case 1:
                    this.type.add(Integer.valueOf(this.imageType[j]));
                    this.type.add(Integer.valueOf(this.imageType[j]));
                    break;
                case 2:
                    this.type.add(Integer.valueOf(this.imageType2[j]));
                    this.type.add(Integer.valueOf(this.imageType2[j]));
                    break;
            }
        }
        for (int i2 = 0; i2 < times2; i2 = i2 + 1 + 1) {
            int j2 = (i2 % len) + 1;
            switch (this.stytle) {
                case 1:
                    this.type.add(Integer.valueOf(this.imageType[j2]));
                    this.type.add(Integer.valueOf(this.imageType[j2]));
                    break;
                case 2:
                    this.type.add(Integer.valueOf(this.imageType2[j2]));
                    this.type.add(Integer.valueOf(this.imageType2[j2]));
                    break;
            }
        }
    }

    public void initTypeOld() {
        int size = (this.row - 2) * (this.col - 2);
        int len = this.imageType.length;
        int count = size / len;
        int iSize2 = 0;
        if (count % 2 != 0) {
            count = (count / 2) * 2;
        }
        for (int i = 0; i < count + 2; i++) {
            for (int j = 0; j < len; j++) {
                if (i < count) {
                    switch (this.stytle) {
                        case 1:
                            this.type.add(Integer.valueOf(this.imageType[j]));
                            break;
                        case 2:
                            this.type.add(Integer.valueOf(this.imageType2[j]));
                            break;
                    }
                    iSize2++;
                } else {
                    switch (this.stytle) {
                        case 1:
                            this.type.add(Integer.valueOf(this.imageType[j]));
                            this.type.add(Integer.valueOf(this.imageType[j]));
                            break;
                        case 2:
                            this.type.add(Integer.valueOf(this.imageType2[j]));
                            this.type.add(Integer.valueOf(this.imageType2[j]));
                            break;
                    }
                    iSize2 = iSize2 + 1 + 1;
                    if (iSize2 == size) {
                        return;
                    }
                }
            }
        }
    }

    public void select(int x, int y) {
        invalidate(this.selRect);
        this.selX = Math.min(Math.max(x, 0), this.row);
        this.selY = Math.min(Math.max(y, 0), this.col);
        getRect(this.selX, this.selY, this.selRect);
        invalidate(this.selRect);
    }

    private void getRect(int x, int y, Rect rect) {
        rect.set((int) (((float) x) * this.width), (int) ((((float) y) * this.height) + ((float) this.step)), (int) ((((float) x) * this.width) + this.width), (int) ((((float) y) * this.height) + this.height + ((float) this.step)));
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        canvas.clipRect(0, 0, this.SCREEN_W, this.SCREEN_H);
        new Paint().setColor(getResources().getColor(R.color.hilite));
        Paint light = new Paint();
        light.setColor(getResources().getColor(R.color.light));
        canvas.drawBitmap(this.bg, 0.0f, 0.0f, (Paint) null);
        for (int i = 0; i <= this.col; i++) {
            canvas.drawLine(0.0f, ((float) this.step) + (((float) i) * this.height), (float) getWidth(), ((float) this.step) + (((float) i) * this.height), light);
            if (i <= this.row) {
                if (i == this.row) {
                    canvas.drawLine((((float) i) * this.width) - 1.0f, (float) (this.step + 0), this.width * ((float) i), ((float) this.step) + (((float) this.col) * this.height), light);
                } else {
                    canvas.drawLine(this.width * ((float) i), 0.0f, this.width * ((float) i), ((float) this.step) + (((float) this.col) * this.height), light);
                }
            }
        }
        if (CtrlView.CURRENT_CH) {
            Paint selected = new Paint();
            selected.setColor(getResources().getColor(R.color.puzzle_selected));
            canvas.drawRect(this.selRect, selected);
        }
        for (int i2 = 0; i2 < this.row; i2++) {
            for (int j = 0; j < this.col; j++) {
                if (this.grid[i2][j] != 0) {
                    canvas.drawBitmap(this.imgbg, ((float) i2) * this.width, (((float) j) * this.height) + ((float) this.step), (Paint) null);
                    switch (this.stytle) {
                        case 1:
                            canvas.drawBitmap(this.image[Arrays.binarySearch(this.imageType, this.grid[i2][j])], ((float) i2) * this.width, (((float) j) * this.height) + ((float) this.step), (Paint) null);
                            break;
                        case 2:
                            canvas.drawBitmap(this.image[Arrays.binarySearch(this.imageType2, this.grid[i2][j])], ((float) i2) * this.width, (((float) j) * this.height) + ((float) this.step), (Paint) null);
                            break;
                    }
                }
            }
        }
        if (this.isLine) {
            Paint lineColor = new Paint();
            lineColor.setColor(-65536);
            switch (this.lineType) {
                case 1:
                    canvas.drawBitmap(this.cursor, ((float) this.p[1].x) * this.width, (((float) this.p[1].y) * this.height) + ((float) this.step), (Paint) null);
                    canvas.drawLine((((float) this.p[0].x) * this.width) + (this.width / 2.0f), (((float) this.p[0].y) * this.height) + (this.height / 2.0f) + ((float) this.step), (((float) this.p[1].x) * this.width) + (this.width / 2.0f), (((float) this.p[1].y) * this.height) + (this.height / 2.0f) + ((float) this.step), lineColor);
                    break;
                case 2:
                    canvas.drawBitmap(this.cursor, ((float) this.p[2].x) * this.width, (((float) this.p[2].y) * this.height) + ((float) this.step), (Paint) null);
                    canvas.drawLine((((float) this.p[0].x) * this.width) + (this.width / 2.0f), (((float) this.p[0].y) * this.height) + (this.height / 2.0f) + ((float) this.step), (((float) this.p[1].x) * this.width) + (this.width / 2.0f), (((float) this.p[1].y) * this.height) + (this.height / 2.0f) + ((float) this.step), lineColor);
                    canvas.drawLine((((float) this.p[1].x) * this.width) + (this.width / 2.0f), (((float) this.p[1].y) * this.height) + (this.height / 2.0f) + ((float) this.step), (((float) this.p[2].x) * this.width) + (this.width / 2.0f), (((float) this.p[2].y) * this.height) + (this.height / 2.0f) + ((float) this.step), lineColor);
                    break;
                case 3:
                    canvas.drawBitmap(this.cursor, ((float) this.p[3].x) * this.width, (((float) this.p[3].y) * this.height) + ((float) this.step), (Paint) null);
                    canvas.drawLine((((float) this.p[0].x) * this.width) + (this.width / 2.0f), (((float) this.p[0].y) * this.height) + (this.height / 2.0f) + ((float) this.step), (((float) this.p[1].x) * this.width) + (this.width / 2.0f), (((float) this.p[1].y) * this.height) + (this.height / 2.0f) + ((float) this.step), lineColor);
                    canvas.drawLine((((float) this.p[1].x) * this.width) + (this.width / 2.0f), (((float) this.p[1].y) * this.height) + (this.height / 2.0f) + ((float) this.step), (((float) this.p[2].x) * this.width) + (this.width / 2.0f), (((float) this.p[2].y) * this.height) + (this.height / 2.0f) + ((float) this.step), lineColor);
                    canvas.drawLine((((float) this.p[3].x) * this.width) + (this.width / 2.0f), (((float) this.p[3].y) * this.height) + (this.height / 2.0f) + ((float) this.step), (((float) this.p[2].x) * this.width) + (this.width / 2.0f), (((float) this.p[2].y) * this.height) + (this.height / 2.0f) + ((float) this.step), lineColor);
                    break;
            }
            this.selX = -1;
            this.selY = -1;
        }
        if (!(this.selX == -1 || this.selY == -1 || this.selX == 0 || this.selY == 0 || this.selX == this.row - 1 || this.selY == this.col - 1)) {
            canvas.drawBitmap(this.cursor, ((float) this.selX) * this.width, (((float) this.selY) * this.height) + ((float) this.step), (Paint) null);
        }
        ((ConnectGame) this.Gamecontext).levelShow.setText("Level " + this.level);
        ((ConnectGame) this.Gamecontext).nowScoreShow.setText(String.valueOf(((ConnectGame) this.Gamecontext).getResources().getString(R.string.score)) + ": " + this.nowScore);
        super.onDraw(canvas);
    }

    public void initGrid() {
        Random ad = new Random();
        for (int i = 0; i < this.row; i++) {
            for (int j = 0; j < this.col; j++) {
                if (i == 0 || i == this.row - 1 || j == 0 || j == this.col - 1) {
                    this.grid[i][j] = 0;
                } else if (this.type == null || this.type.size() <= 0) {
                    this.grid[i][j] = 0;
                } else {
                    int index = ad.nextInt(this.type.size());
                    this.grid[i][j] = this.type.get(index).intValue();
                    this.type.remove(index);
                }
            }
        }
    }
}
