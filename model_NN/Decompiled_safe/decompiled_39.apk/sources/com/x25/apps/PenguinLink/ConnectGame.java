package com.x25.apps.PenguinLink;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.ContextMenu;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobclick.android.MobclickAgent;
import java.lang.reflect.Array;
import java.util.Random;

public class ConnectGame extends Activity {
    public static final int END_ID = 7;
    public static final int REARRARY_ID = 2;
    public static final int SETTYTLE_ID = 3;
    public static final int START_ID = 1;
    public static final int TYTLE1_ID = 4;
    public static final int TYTLE2_ID = 5;
    public static final int TYTLE3_ID = 6;
    private int SCREEN_H = 480;
    private int SCREEN_W = 320;
    public Ads ads;
    public TextView cooldown;
    public int cooldown_count = 0;
    public int cooldown_count_max = 15;
    public CtrlView cv;
    public int difficulty = 0;
    public int[] fail_talk = {R.string.fail_talk_1, R.string.fail_talk_2, R.string.fail_talk_3, R.string.fail_talk_4, R.string.fail_talk_5, R.string.fail_talk_6};
    final Handler handler = new Handler();
    public TextView levelShow;
    private RefreshHandler mRedrawHandler = new RefreshHandler();
    public Sound music;
    public TextView nowScoreShow;
    public ProgressBar pb;
    public RelativeLayout reset;
    public Runnable runnable = new Runnable() {
        public void run() {
            ConnectGame.this.cooldown_count--;
            ConnectGame.this.cooldown.setText(new StringBuilder(String.valueOf(ConnectGame.this.cooldown_count)).toString());
            if (ConnectGame.this.cooldown_count == 0) {
                ConnectGame.this.reset.setBackgroundResource(R.drawable.reset);
                ConnectGame.this.cooldown.setVisibility(8);
                ConnectGame.this.handler.removeCallbacks(this);
                return;
            }
            ConnectGame.this.handler.postDelayed(this, 1000);
        }
    };
    private Store store;
    public int[] succ_talk = {R.string.succ_talk_1, R.string.succ_talk_2, R.string.succ_talk_3};
    public TextView topScoreShow;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        Display disp = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        this.SCREEN_W = disp.getWidth();
        this.SCREEN_H = disp.getHeight();
        this.store = new Store(this);
        findViews();
        this.mRedrawHandler.sleep(1000);
        ((GameApplication) getApplication()).cg = this;
        ((GameApplication) getApplication()).setCgOpen(true);
        this.ads = new Ads(this);
        this.ads.getAd().getWb();
    }

    class RefreshHandler extends Handler {
        RefreshHandler() {
        }

        public void handleMessage(Message msg) {
            ConnectGame.this.run();
        }

        public void sleep(long delayMillis) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), delayMillis);
        }
    }

    public void run() {
        if (!((GameApplication) getApplication()).isRun) {
            this.mRedrawHandler.sleep(1000);
        } else if (this.cv.PROCESS_VALUE > 0 && this.cv.much != 0) {
            this.cv.PROCESS_VALUE--;
            this.pb.setProgress(this.cv.PROCESS_VALUE);
            this.mRedrawHandler.sleep(1000);
        } else if (this.cv.PROCESS_VALUE == 0 && this.cv.much != 0) {
            if (this.store.load("effect") == 0) {
                this.music.fail();
            }
            if (this.cv.nowScore > this.cv.topScore) {
                this.store.save("score" + this.difficulty, this.cv.nowScore);
            }
            this.cv.setEnabled(false);
            new AlertDialog.Builder(this).setTitle((int) R.string.app_name).setMessage(this.fail_talk[getRandom(0, 5)]).setPositiveButton((int) R.string.btn_restart, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    ConnectGame.this.newGame();
                }
            }).setNeutralButton((int) R.string.close, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    ConnectGame.this.finish();
                }
            }).create().show();
        } else if (this.cv.PROCESS_VALUE != 0 && this.cv.much == 0) {
            if (this.store.load("effect") == 0) {
                this.music.pass();
            }
            if (this.cv.nowScore > this.cv.topScore) {
                this.store.save("score" + this.difficulty, this.cv.nowScore);
            }
            new AlertDialog.Builder(this).setTitle((int) R.string.app_name).setMessage(this.succ_talk[getRandom(0, 2)]).setPositiveButton((int) R.string.btn_next, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    ConnectGame.this.nextLevel();
                }
            }).setNeutralButton((int) R.string.close, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    ConnectGame.this.finish();
                }
            }).show();
        }
    }

    public void rearrange() {
        this.cv.rearrange();
    }

    public void setbg() {
        this.cv.setbg();
    }

    public void newGame() {
        this.cv.setEnabled(false);
        this.cv.reset();
        this.cv.nowScore = 0;
        CtrlView ctrlView = this.cv;
        this.cv.getClass();
        ctrlView.PROCESS_VALUE = 300;
        ProgressBar progressBar = this.pb;
        this.cv.getClass();
        progressBar.setMax(300);
        this.pb.incrementProgressBy(-1);
        this.pb.setProgress(this.cv.PROCESS_VALUE);
        this.mRedrawHandler.sleep(1000);
        this.ads.getAd().getWb();
    }

    public void nextLevel() {
        int iLv = this.cv.level;
        this.cv.setEnabled(false);
        this.cv.reset();
        this.cv.level = iLv + 1;
        CtrlView ctrlView = this.cv;
        this.cv.getClass();
        ctrlView.PROCESS_VALUE = 300 - ((this.cv.level - 1) * 10);
        this.pb.setMax(this.cv.PROCESS_VALUE);
        this.pb.incrementProgressBy(-1);
        this.pb.setProgress(this.cv.PROCESS_VALUE);
        this.mRedrawHandler.sleep(1000);
        this.ads.getAd().getWb();
    }

    public void changeStytle() {
        this.cv.changeStytle();
    }

    private void findViews() {
        this.pb = (ProgressBar) findViewById(R.id.pb);
        this.reset = (RelativeLayout) findViewById(R.id.reset);
        this.reset.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (ConnectGame.this.cooldown_count == 0 && ((GameApplication) ConnectGame.this.getApplicationContext()).bCgOpen) {
                    ((GameApplication) ConnectGame.this.getApplicationContext()).cg.rearrange();
                    ConnectGame.this.cooldown_count = ConnectGame.this.cooldown_count_max;
                    ConnectGame.this.reset.setBackgroundResource(R.drawable.reset_off);
                    ConnectGame.this.cooldown.setVisibility(0);
                    ConnectGame.this.cooldown.setText(new StringBuilder(String.valueOf(ConnectGame.this.cooldown_count)).toString());
                    ConnectGame.this.handler.postDelayed(ConnectGame.this.runnable, 1000);
                }
            }
        });
        this.cooldown = (TextView) findViewById(R.id.cooldown);
        this.levelShow = (TextView) findViewById(R.id.levelShow);
        this.topScoreShow = (TextView) findViewById(R.id.topScoreShow);
        this.nowScoreShow = (TextView) findViewById(R.id.nowScoreShow);
        this.cv = (CtrlView) findViewById(R.id.cv);
        this.difficulty = getIntent().getExtras().getInt("difficulty");
        if (this.difficulty == 1) {
            this.cv.row = 9;
            this.cv.col = 12;
            this.cv.step = -3;
        } else if (this.difficulty == 2) {
            this.cv.row = 11;
            this.cv.col = 14;
            this.cv.step = 5;
        } else {
            this.cv.row = 7;
            this.cv.col = 10;
            this.cv.step = -25;
        }
        this.cv.grid = (int[][]) Array.newInstance(Integer.TYPE, this.cv.row, this.cv.col);
        this.cv.reset();
        ProgressBar progressBar = this.pb;
        this.cv.getClass();
        progressBar.setMax(300);
        this.cv.setSize(this.SCREEN_W, this.SCREEN_H);
        this.cv.setBackgroundColor(0);
        this.pb.incrementProgressBy(-1);
        this.pb.setProgress(this.cv.PROCESS_VALUE);
        this.cv.topScore = this.store.load("score" + this.difficulty);
        this.topScoreShow.setText(String.valueOf(getResources().getString(R.string.top)) + ": " + this.cv.topScore);
    }

    public int getRandom(int a, int b) {
        int b2 = b + 1;
        if (a <= b2) {
            return new Random().nextInt(b2 - a) + a;
        }
        try {
            return new Random().nextInt(a - b2) + b2;
        } catch (Exception e) {
            e.printStackTrace();
            return 0 + a;
        }
    }

    public boolean onMenuOpened(int featureId, Menu menu) {
        GridDialog dialog = new GridDialog(this);
        dialog.bindEvent(this);
        dialog.show();
        return false;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, (int) R.string.newgame);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.music.StopBg();
        ((GameApplication) getApplication()).isRun = false;
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.music = new Sound(this);
        if (this.store.load("music") == 0) {
            this.music.playBg();
        }
        ((GameApplication) getApplication()).isRun = true;
        MobclickAgent.onResume(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        ((GameApplication) getApplication()).setCgOpen(false);
        this.music.destroyBg();
        this.ads.finalize();
    }
}
