package com.x25.apps.PenguinLink;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int hilite = 2131034112;
        public static final int light = 2131034113;
        public static final int puzzle_dark = 2131034114;
        public static final int puzzle_foreground = 2131034115;
        public static final int puzzle_hint_0 = 2131034116;
        public static final int puzzle_hint_1 = 2131034117;
        public static final int puzzle_hint_2 = 2131034118;
        public static final int puzzle_selected = 2131034119;
    }

    public static final class drawable {
        public static final int about = 2130837504;
        public static final int background = 2130837505;
        public static final int bg1 = 2130837506;
        public static final int bg2 = 2130837507;
        public static final int bg3 = 2130837508;
        public static final int bg4 = 2130837509;
        public static final int bg5 = 2130837510;
        public static final int bg6 = 2130837511;
        public static final int button_bg = 2130837512;
        public static final int button_bg_sel = 2130837513;
        public static final int cursor = 2130837514;
        public static final int exit = 2130837515;
        public static final int fa = 2130837516;
        public static final int fb = 2130837517;
        public static final int fc = 2130837518;
        public static final int fd = 2130837519;
        public static final int fe = 2130837520;
        public static final int ff = 2130837521;
        public static final int fg = 2130837522;
        public static final int fh = 2130837523;
        public static final int fi = 2130837524;
        public static final int fj = 2130837525;
        public static final int fk = 2130837526;
        public static final int fl = 2130837527;
        public static final int fm = 2130837528;
        public static final int fn = 2130837529;
        public static final int grid_bg = 2130837530;
        public static final int help = 2130837531;
        public static final int icon = 2130837532;
        public static final int imgbg = 2130837533;
        public static final int music = 2130837534;
        public static final int newgame = 2130837535;
        public static final int notify_alert = 2130837536;
        public static final int pa = 2130837537;
        public static final int pb = 2130837538;
        public static final int pc = 2130837539;
        public static final int pd = 2130837540;
        public static final int pe = 2130837541;
        public static final int pf = 2130837542;
        public static final int pg = 2130837543;
        public static final int ph = 2130837544;
        public static final int pi = 2130837545;
        public static final int pj = 2130837546;
        public static final int pk = 2130837547;
        public static final int pl = 2130837548;
        public static final int pm = 2130837549;
        public static final int pn = 2130837550;
        public static final int rearrage = 2130837551;
        public static final int reset = 2130837552;
        public static final int reset_off = 2130837553;
        public static final int reset_on = 2130837554;
        public static final int reset_on_click = 2130837555;
        public static final int setbg = 2130837556;
        public static final int titleicon = 2130837557;
        public static final int tytle = 2130837558;
    }

    public static final class id {
        public static final int LinearLayout01 = 2131165202;
        public static final int OfferProgressBar = 2131165204;
        public static final int RelativeLayout01 = 2131165186;
        public static final int app = 2131165205;
        public static final int appIcon = 2131165206;
        public static final int btnexit = 2131165201;
        public static final int btnhelp = 2131165200;
        public static final int btnoption = 2131165199;
        public static final int cooldown = 2131165194;
        public static final int cv = 2131165195;
        public static final int description = 2131165210;
        public static final int easy = 2131165196;
        public static final int hard = 2131165198;
        public static final int item_image = 2131165187;
        public static final int item_text = 2131165188;
        public static final int layout_root = 2131165184;
        public static final int levelShow = 2131165190;
        public static final int medium = 2131165197;
        public static final int mygridview = 2131165185;
        public static final int notification = 2131165208;
        public static final int nowScoreShow = 2131165192;
        public static final int offersWebView = 2131165203;
        public static final int pb = 2131165189;
        public static final int progress_bar = 2131165211;
        public static final int progress_text = 2131165207;
        public static final int reset = 2131165193;
        public static final int title = 2131165209;
        public static final int topScoreShow = 2131165191;
    }

    public static final class layout {
        public static final int button = 2130903040;
        public static final int grid_dialog = 2130903041;
        public static final int grid_item = 2130903042;
        public static final int main = 2130903043;
        public static final int mymain = 2130903044;
        public static final int offers_web_view = 2130903045;
        public static final int umeng_download_notification = 2130903046;
    }

    public static final class raw {
        public static final int click = 2130968576;
        public static final int fail = 2130968577;
        public static final int music = 2130968578;
        public static final int no = 2130968579;
        public static final int pass = 2130968580;
        public static final int yes = 2130968581;
    }

    public static final class string {
        public static final int about = 2131099658;
        public static final int app_name = 2131099649;
        public static final int btn_next = 2131099662;
        public static final int btn_restart = 2131099663;
        public static final int close = 2131099665;
        public static final int di = 2131099670;
        public static final int easy = 2131099672;
        public static final int exit = 2131099659;
        public static final int fail_talk_1 = 2131099678;
        public static final int fail_talk_2 = 2131099679;
        public static final int fail_talk_3 = 2131099680;
        public static final int fail_talk_4 = 2131099681;
        public static final int fail_talk_5 = 2131099682;
        public static final int fail_talk_6 = 2131099683;
        public static final int guan = 2131099671;
        public static final int hard = 2131099674;
        public static final int help = 2131099653;
        public static final int help1 = 2131099654;
        public static final int help_text = 2131099667;
        public static final int help_title = 2131099666;
        public static final int lang = 2131099648;
        public static final int medium = 2131099673;
        public static final int music = 2131099655;
        public static final int music1 = 2131099656;
        public static final int newgame = 2131099650;
        public static final int next = 2131099669;
        public static final int ok = 2131099661;
        public static final int over = 2131099668;
        public static final int rearrage = 2131099651;
        public static final int score = 2131099684;
        public static final int setbg = 2131099657;
        public static final int start = 2131099664;
        public static final int succ_talk_1 = 2131099675;
        public static final int succ_talk_2 = 2131099676;
        public static final int succ_talk_3 = 2131099677;
        public static final int tishi = 2131099660;
        public static final int top = 2131099685;
        public static final int tytle = 2131099652;
    }
}
