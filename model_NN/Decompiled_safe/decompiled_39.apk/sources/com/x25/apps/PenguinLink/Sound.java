package com.x25.apps.PenguinLink;

import android.content.Context;
import android.media.MediaPlayer;

public class Sound {
    private Context context;
    public int isMusic;
    private MediaPlayer mp_bg;
    private MediaPlayer mp_click;
    private MediaPlayer mp_fail;
    private MediaPlayer mp_no;
    private MediaPlayer mp_pass;
    private MediaPlayer mp_yes;

    public Sound(Context c) {
        this.context = c;
        initMp();
    }

    public void initMp() {
        this.mp_bg = MediaPlayer.create(this.context, (int) R.raw.music);
    }

    public void click() {
        try {
            this.mp_click = MediaPlayer.create(this.context, (int) R.raw.click);
            this.mp_click.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void yes() {
        try {
            this.mp_yes = MediaPlayer.create(this.context, (int) R.raw.yes);
            this.mp_yes.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void no() {
        try {
            this.mp_no = MediaPlayer.create(this.context, (int) R.raw.no);
            this.mp_no.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pass() {
        try {
            this.mp_pass = MediaPlayer.create(this.context, (int) R.raw.pass);
            this.mp_pass.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fail() {
        try {
            this.mp_fail = MediaPlayer.create(this.context, (int) R.raw.fail);
            this.mp_fail.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playBg() {
        this.mp_bg.setLooping(true);
        this.mp_bg.start();
    }

    public void switchBg() {
        if (this.mp_bg.isPlaying()) {
            this.mp_bg.pause();
        } else {
            playBg();
        }
    }

    public void destroyBg() {
        StopBg();
        this.mp_bg.release();
    }

    public void StopBg() {
        this.mp_bg.stop();
    }
}
