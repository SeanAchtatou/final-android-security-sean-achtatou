package com.x25.apps.PenguinLink;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.mobclick.android.MobclickAgent;
import com.waps.AppConnect;
import com.waps.UpdatePointsNotifier;

public class MainActivity extends Activity implements UpdatePointsNotifier {
    private Ads ads;
    /* access modifiers changed from: private */
    public int easyPoint = 80;
    /* access modifiers changed from: private */
    public int hardPoint = 100;
    /* access modifiers changed from: private */
    public int mediumPoint = 100;
    /* access modifiers changed from: private */
    public Sound music;
    /* access modifiers changed from: private */
    public int point = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.mymain);
        Global.disableWaps("2011-08-19 18:00:00");
        MobclickAgent.update(this);
        AppConnect.getInstance(this);
        ((GameApplication) getApplication()).setCgOpen(false);
        ((Button) findViewById(R.id.easy)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Global.isWaps(MainActivity.this)) {
                    Store store = new Store(MainActivity.this);
                    if (store.load("isBuyEasy") == 0) {
                        if (MainActivity.this.point < MainActivity.this.easyPoint) {
                            new AlertDialog.Builder(MainActivity.this).setTitle("激活游戏").setMessage("请先激活，永久开启此游戏模式需要 " + MainActivity.this.easyPoint + " 积分，你当前积分余额为：" + MainActivity.this.point + " ，您可以通过下载安装精品应用 ‘免费赚积分’。为确保积分到账，安装后请启动一次！").setPositiveButton("免费赚积分", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialoginterface, int i) {
                                    AppConnect.getInstance(MainActivity.this).showOffers(MainActivity.this);
                                }
                            }).show();
                            return;
                        }
                        MainActivity mainActivity = MainActivity.this;
                        mainActivity.point = mainActivity.point - MainActivity.this.easyPoint;
                        AppConnect.getInstance(MainActivity.this).spendPoints(MainActivity.this.easyPoint, MainActivity.this);
                        store.save("isBuyEasy", 1);
                        new AlertDialog.Builder(MainActivity.this).setTitle("开启游戏模式成功").setMessage("恭喜您，你已经成功永久开启此游戏模式，重新点击进入此游戏模式吧！").setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i) {
                            }
                        }).show();
                        return;
                    }
                }
                Intent intent = new Intent(MainActivity.this, ConnectGame.class);
                intent.setFlags(67108864);
                intent.putExtra("difficulty", 0);
                MainActivity.this.startActivity(intent);
            }
        });
        ((Button) findViewById(R.id.medium)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Global.isWaps(MainActivity.this)) {
                    Store store = new Store(MainActivity.this);
                    if (store.load("isBuyMedium") == 0) {
                        if (MainActivity.this.point < MainActivity.this.mediumPoint) {
                            new AlertDialog.Builder(MainActivity.this).setTitle("激活游戏").setMessage("请先激活，永久开启此游戏模式需要 " + MainActivity.this.mediumPoint + " 积分，你当前积分余额为：" + MainActivity.this.point + " ，您可以通过下载安装精品应用 ‘免费赚积分’。为确保积分到账，安装后请启动一次！").setPositiveButton("免费赚积分", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialoginterface, int i) {
                                    AppConnect.getInstance(MainActivity.this).showOffers(MainActivity.this);
                                }
                            }).show();
                            return;
                        }
                        MainActivity mainActivity = MainActivity.this;
                        mainActivity.point = mainActivity.point - MainActivity.this.mediumPoint;
                        AppConnect.getInstance(MainActivity.this).spendPoints(MainActivity.this.mediumPoint, MainActivity.this);
                        store.save("isBuyMedium", 1);
                        new AlertDialog.Builder(MainActivity.this).setTitle("开启游戏模式成功").setMessage("恭喜您，你已经成功永久开启此游戏模式，重新点击进入此游戏模式吧！").setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i) {
                            }
                        }).show();
                        return;
                    }
                }
                Intent intent = new Intent(MainActivity.this, ConnectGame.class);
                intent.setFlags(67108864);
                intent.putExtra("difficulty", 1);
                MainActivity.this.startActivity(intent);
            }
        });
        ((Button) findViewById(R.id.hard)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Global.isWaps(MainActivity.this)) {
                    Store store = new Store(MainActivity.this);
                    if (store.load("isBuyHard") == 0) {
                        if (MainActivity.this.point < MainActivity.this.hardPoint) {
                            new AlertDialog.Builder(MainActivity.this).setTitle("激活游戏").setMessage("请先激活，永久开启此游戏模式需要 " + MainActivity.this.hardPoint + " 积分，你当前积分余额为：" + MainActivity.this.point + " ，您可以通过下载安装精品应用 ‘免费赚积分’。为确保积分到账，安装后请启动一次！").setPositiveButton("免费赚积分", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialoginterface, int i) {
                                    AppConnect.getInstance(MainActivity.this).showOffers(MainActivity.this);
                                }
                            }).show();
                            return;
                        }
                        MainActivity mainActivity = MainActivity.this;
                        mainActivity.point = mainActivity.point - MainActivity.this.hardPoint;
                        AppConnect.getInstance(MainActivity.this).spendPoints(MainActivity.this.hardPoint, MainActivity.this);
                        store.save("isBuyHard", 1);
                        new AlertDialog.Builder(MainActivity.this).setTitle("开启游戏模式成功").setMessage("恭喜您，你已经成功永久开启此游戏模式，重新点击进入此游戏模式吧！").setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i) {
                            }
                        }).show();
                        return;
                    }
                }
                Intent intent = new Intent(MainActivity.this, ConnectGame.class);
                intent.setFlags(67108864);
                intent.putExtra("difficulty", 2);
                MainActivity.this.startActivity(intent);
            }
        });
        Button btnhelp = (Button) findViewById(R.id.btnhelp);
        if (Global.isWaps(this)) {
            btnhelp.setText("免费赚积分");
        }
        btnhelp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Global.isWaps(MainActivity.this)) {
                    AppConnect.getInstance(MainActivity.this).showOffers(MainActivity.this);
                } else {
                    new AlertDialog.Builder(MainActivity.this).setTitle((int) R.string.help).setIcon((int) R.drawable.titleicon).setMessage((int) R.string.help_text).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    }).show();
                }
            }
        });
        ((Button) findViewById(R.id.btnoption)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final Store store = new Store(MainActivity.this);
                int isMusic = store.load("music");
                int isEffect = store.load("effect");
                boolean musicOn = true;
                boolean effectOn = true;
                if (isMusic == 1) {
                    musicOn = false;
                }
                if (isEffect == 1) {
                    effectOn = false;
                }
                new AlertDialog.Builder(MainActivity.this).setTitle((int) R.string.app_name).setIcon((int) R.drawable.titleicon).setMultiChoiceItems(new String[]{"背景音乐", "游戏音效"}, new boolean[]{musicOn, effectOn}, new DialogInterface.OnMultiChoiceClickListener() {
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (which == 0) {
                            MainActivity.this.music.switchBg();
                            if (isChecked) {
                                store.save("music", 0);
                            } else {
                                store.save("music", 1);
                            }
                        } else if (isChecked) {
                            store.save("effect", 0);
                        } else {
                            store.save("effect", 1);
                        }
                    }
                }).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
            }
        });
        ((Button) findViewById(R.id.btnexit)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((GameApplication) MainActivity.this.getApplication()).cg != null) {
                    ((GameApplication) MainActivity.this.getApplication()).cg.finish();
                }
                MainActivity.this.finish();
            }
        });
        this.ads = new Ads(this);
        this.ads.getAd().getWbs();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.music.StopBg();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.music = new Sound(this);
        if (new Store(this).load("music") == 0) {
            this.music.playBg();
        }
        MobclickAgent.onResume(this);
        AppConnect.getInstance(this).getPoints(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.music.destroyBg();
        if (((GameApplication) getApplication()).cg != null) {
            ((GameApplication) getApplication()).cg.finish();
        }
        AppConnect.getInstance(this).finalize();
        this.ads.finalize();
    }

    public void getUpdatePoints(String currencyName, int pointTotal) {
        this.point = pointTotal;
    }

    public void getUpdatePointsFailed(String error) {
        this.point = 0;
    }
}
