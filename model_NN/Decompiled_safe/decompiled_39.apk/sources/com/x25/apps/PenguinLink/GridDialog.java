package com.x25.apps.PenguinLink;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import com.waps.AnimationType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GridDialog extends Dialog {
    Context Thecontext;
    private List<int[]> griditem = new ArrayList();
    private GridView gridview;

    public GridDialog(Context context, boolean cancelable, DialogInterface.OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.griditem.add(new int[]{R.drawable.tytle, R.string.tytle});
        this.griditem.add(new int[]{R.drawable.setbg, R.string.setbg});
        this.griditem.add(new int[]{R.drawable.music, R.string.music1});
        this.griditem.add(new int[]{R.drawable.newgame, R.string.newgame});
        this.griditem.add(new int[]{R.drawable.help, R.string.help1});
        this.griditem.add(new int[]{R.drawable.exit, R.string.close});
        this.Thecontext = context;
    }

    public GridDialog(Context context, int theme) {
        super(context, theme);
        this.griditem.add(new int[]{R.drawable.tytle, R.string.tytle});
        this.griditem.add(new int[]{R.drawable.setbg, R.string.setbg});
        this.griditem.add(new int[]{R.drawable.music, R.string.music1});
        this.griditem.add(new int[]{R.drawable.newgame, R.string.newgame});
        this.griditem.add(new int[]{R.drawable.help, R.string.help1});
        this.griditem.add(new int[]{R.drawable.exit, R.string.close});
        this.Thecontext = context;
    }

    private void initGrid() {
        List<Map<String, Object>> items = new ArrayList<>();
        for (int[] item : this.griditem) {
            Map<String, Object> map = new HashMap<>();
            map.put("image", Integer.valueOf(item[0]));
            map.put("title", getContext().getString(item[1]));
            items.add(map);
        }
        SimpleAdapter adapter = new SimpleAdapter(getContext(), items, R.layout.grid_item, new String[]{"title", "image"}, new int[]{R.id.item_text, R.id.item_image});
        this.gridview = (GridView) findViewById(R.id.mygridview);
        this.gridview.setAdapter((ListAdapter) adapter);
    }

    public GridDialog(Context context) {
        super(context);
        this.griditem.add(new int[]{R.drawable.tytle, R.string.tytle});
        this.griditem.add(new int[]{R.drawable.setbg, R.string.setbg});
        this.griditem.add(new int[]{R.drawable.music, R.string.music1});
        this.griditem.add(new int[]{R.drawable.newgame, R.string.newgame});
        this.griditem.add(new int[]{R.drawable.help, R.string.help1});
        this.griditem.add(new int[]{R.drawable.exit, R.string.close});
        requestWindowFeature(1);
        setContentView((int) R.layout.grid_dialog);
        setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        getWindow().setAttributes(lp);
        getWindow().addFlags(128);
        lp.alpha = 1.0f;
        lp.dimAmount = 0.3f;
        new DisplayMetrics();
        lp.y = context.getResources().getDisplayMetrics().heightPixels - 200;
        initGrid();
    }

    public void bindEvent(Activity activity) {
        setOwnerActivity(activity);
        this.gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* Debug info: failed to restart local var, previous not found, register: 1 */
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                switch (position) {
                    case AnimationType.RANDOM:
                        if (((GameApplication) GridDialog.this.getContext().getApplicationContext()).bCgOpen) {
                            ((GameApplication) GridDialog.this.getContext().getApplicationContext()).cg.changeStytle();
                        }
                        GridDialog.this.dismiss();
                        return;
                    case 1:
                        if (((GameApplication) GridDialog.this.getContext().getApplicationContext()).bCgOpen) {
                            ((GameApplication) GridDialog.this.getContext().getApplicationContext()).cg.setbg();
                        }
                        GridDialog.this.dismiss();
                        return;
                    case 2:
                        if (((GameApplication) GridDialog.this.getContext().getApplicationContext()).bCgOpen) {
                            GridDialog.this.MusicOption();
                        }
                        GridDialog.this.dismiss();
                        return;
                    case 3:
                        if (((GameApplication) GridDialog.this.getContext().getApplicationContext()).bCgOpen) {
                            ((GameApplication) GridDialog.this.getContext().getApplicationContext()).cg.newGame();
                        }
                        GridDialog.this.dismiss();
                        return;
                    case 4:
                        if (((GameApplication) GridDialog.this.getContext().getApplicationContext()).bCgOpen) {
                            GridDialog.this.help();
                        }
                        GridDialog.this.dismiss();
                        return;
                    case 5:
                        GridDialog.this.dismiss();
                        if (((GameApplication) GridDialog.this.getContext().getApplicationContext()).bCgOpen) {
                            ((GameApplication) GridDialog.this.getContext().getApplicationContext()).cg.finish();
                        }
                        ((GameApplication) GridDialog.this.getContext().getApplicationContext()).bCgOpen = false;
                        return;
                    default:
                        return;
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void MusicOption() {
        final Store store = new Store(((GameApplication) getContext().getApplicationContext()).cg);
        int isMusic = store.load("music");
        int isEffect = store.load("effect");
        boolean musicOn = true;
        boolean effectOn = true;
        if (isMusic == 1) {
            musicOn = false;
        }
        if (isEffect == 1) {
            effectOn = false;
        }
        new AlertDialog.Builder(((GameApplication) getContext().getApplicationContext()).cg).setTitle((int) R.string.app_name).setIcon((int) R.drawable.titleicon).setMultiChoiceItems(new String[]{"背景音乐", "游戏音效"}, new boolean[]{musicOn, effectOn}, new DialogInterface.OnMultiChoiceClickListener() {
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (which == 0) {
                    ((GameApplication) GridDialog.this.getContext().getApplicationContext()).cg.music.switchBg();
                    if (isChecked) {
                        store.save("music", 0);
                    } else {
                        store.save("music", 1);
                    }
                } else if (isChecked) {
                    store.save("effect", 0);
                } else {
                    store.save("effect", 1);
                }
            }
        }).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void help() {
        new AlertDialog.Builder(((GameApplication) getContext().getApplicationContext()).cg).setTitle((int) R.string.help).setIcon((int) R.drawable.titleicon).setMessage((int) R.string.help_text).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }
}
