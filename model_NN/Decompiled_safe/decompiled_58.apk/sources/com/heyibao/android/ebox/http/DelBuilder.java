package com.heyibao.android.ebox.http;

import ClientProtocol.ClientMeta;
import android.app.Service;
import android.util.Log;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;

public abstract class DelBuilder implements Runnable, HttpInterface {
    private Service activity;
    private String fileName;
    private String objectType;
    private String objectUUID;
    private String parentUUID;
    private String shareUUID;

    public DelBuilder(Service activity2, String fileName2, String objectUUID2, String parentUUID2, String shareUUID2, String objectType2) {
        this.activity = activity2;
        this.fileName = fileName2;
        this.parentUUID = parentUUID2;
        this.objectUUID = objectUUID2;
        this.objectType = objectType2;
        this.shareUUID = shareUUID2;
    }

    public void run() {
        Log.d(DelBuilder.class.getSimpleName(), "## start DelBuilder ");
        delStart();
        Log.d(DelBuilder.class.getSimpleName(), "end DelBuilder ");
    }

    private void delStart() {
        int errcount = 0;
        boolean success = false;
        while (true) {
            if (Thread.interrupted() || errcount >= 3) {
                break;
            }
            try {
                ClientMeta.UploadMetadataRequest.Builder request = ClientMeta.UploadMetadataRequest.newBuilder();
                if (this.objectType.equals(EboxConst.FILE)) {
                    ClientMeta.FileDetails.Builder fileDetails = ClientMeta.FileDetails.newBuilder();
                    fileDetails.setShowUuid(this.objectUUID);
                    fileDetails.setParentShowUuid(this.parentUUID);
                    fileDetails.setName(this.fileName);
                    ClientMeta.Action.Builder action = ClientMeta.Action.newBuilder();
                    action.setActionType(ClientMeta.ActionType.REMOVE_FILE);
                    action.setShareDirectoryUuid(this.shareUUID);
                    action.setFileDetails(fileDetails);
                    request.setSession(EboxContext.mDevice.getSession());
                    request.addAction(action);
                } else {
                    ClientMeta.DirectoryDetails.Builder dirDetails = ClientMeta.DirectoryDetails.newBuilder();
                    dirDetails.setShowUuid(this.objectUUID);
                    dirDetails.setParentShowUuid(this.parentUUID);
                    dirDetails.setName(this.fileName);
                    ClientMeta.Action.Builder action2 = ClientMeta.Action.newBuilder();
                    action2.setActionType(ClientMeta.ActionType.REMOVE_DIRECTORY);
                    action2.setShareDirectoryUuid(this.shareUUID);
                    action2.setDirDetails(dirDetails);
                    request.setSession(EboxContext.mDevice.getSession());
                    request.addAction(action2);
                }
                Thread.sleep(10);
                ClientMeta.UploadMetadataResponse response = ClientMeta.UploadMetadataResponse.parseFrom(new GPBUtils(EboxConst.UPLOADMETA_URL).getResult(request.build().toByteArray()));
                ClientMeta.Status status = response.getAction(0).getStatus();
                if (!response.getStatus().getSuccess()) {
                    EboxContext.message = EboxConst.SESSIONEXIT;
                    break;
                }
                if (status.getSuccess()) {
                    success = true;
                    EboxContext.mDetails.setShowUuid(this.objectUUID);
                    EboxContext.mDetails.setName(this.fileName);
                }
                EboxContext.message = status.getMsg();
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(DelBuilder.class.getSimpleName(), "## DelBuilder is Force stop");
                EboxContext.message = this.activity.getString(R.string.stop_thread);
            } catch (Exception e2) {
                Exception error = e2;
                error.printStackTrace();
                Log.e(DelBuilder.class.getSimpleName(), "## DelBuilder error : " + error.getMessage() + " for " + errcount);
                errcount++;
                EboxContext.message = this.activity.getResources().getString(R.string.net_error);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
            }
        }
        reportSuccess(success);
    }
}
