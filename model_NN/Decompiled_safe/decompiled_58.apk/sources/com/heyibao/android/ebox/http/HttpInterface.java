package com.heyibao.android.ebox.http;

public interface HttpInterface {
    void reportProgress(int i, int i2);

    void reportSuccess(boolean z);
}
