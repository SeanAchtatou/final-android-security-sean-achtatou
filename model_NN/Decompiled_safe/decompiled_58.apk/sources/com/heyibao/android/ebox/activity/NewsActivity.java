package com.heyibao.android.ebox.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.model.NewsAdapter;
import com.heyibao.android.ebox.util.Utils;

public class NewsActivity extends HomeMenuActivity {
    private NewsAdapter newsAdapter;
    private NewsReceiver receiver;

    public class NewsReceiver extends BroadcastReceiver {
        public NewsReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("action");
            Log.d(NewsActivity.class.getSimpleName(), "## onReceive : " + action);
            if (EboxConst.ACTION_NEWS.equals(action)) {
                NewsActivity.this.mHandler.sendMessage(NewsActivity.this.mHandler.obtainMessage(1, intent));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.details_view);
        Log.d(NewsActivity.class.getSimpleName(), "## create NewsActivity now");
        newsInit();
    }

    public void onPrepareDialog(int id, Dialog dialog) {
        AlertDialog mDialog = (AlertDialog) dialog;
        switch (id) {
            case 9:
                mDialog.setMessage(Utils.replace(getResources().getString(R.string.del_htt_file), EboxContext.mDetails.getName()));
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 12:
                return stopDialog();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.receiver = new NewsReceiver();
        registerReceiver(this.receiver, new IntentFilter(EboxConst.EBOXBROAD), null, this.mHandler);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        unregisterReceiver(this.receiver);
    }

    private void newsInit() {
        initTitleBar();
        this.midTV.setText(getString(R.string.my_news));
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                boolean success = ((Intent) msg.obj).getBooleanExtra("success", false);
                Log.d(ContactsActivity.class.getSimpleName(), "broadcast reval " + success);
                switch (msg.what) {
                    case 1:
                        NewsActivity.this.getNewsEnd(success);
                        break;
                }
                super.handleMessage(msg);
            }
        };
        this.runnableUI = new Runnable() {
            public void run() {
                if (EboxContext.mSystemInfo.hasRefresh()) {
                    NewsActivity.this.refreshNews();
                } else {
                    NewsActivity.this.detailsView();
                }
                NewsActivity.this.mHandler.removeCallbacks(this);
            }
        };
        reloadUI();
    }

    private void reloadUI() {
        this.mHandler.postDelayed(this.runnableUI, 10);
    }

    /* access modifiers changed from: private */
    public void refreshNews() {
        if (Utils.hasNet(this)) {
        }
    }

    /* access modifiers changed from: private */
    public void detailsView() {
        if (this.newsAdapter == null) {
            ((ListView) findViewById(16908298)).setOnTouchListener(this);
        }
    }

    /* access modifiers changed from: private */
    public void getNewsEnd(boolean success) {
        showProgressBar();
        if (success) {
            detailsView();
        } else {
            Utils.MessageBox(this, getString(R.string.net_error));
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Log.d(NewsActivity.class.getSimpleName(), "## list click item : " + position);
        NewsAdapter c = (NewsAdapter) l.getAdapter();
        int rowId = c.getCursor().getInt(0);
        String uuid = c.getCursor().getString(6);
        Log.d(NewsActivity.class.getSimpleName(), "## list click id : " + rowId);
        Log.d(NewsActivity.class.getSimpleName(), "## list click uuid : " + uuid);
        c.toggle(rowId);
    }

    public void onClick(View v) {
        EboxContext.mDetails.reSet();
        switch (v.getId()) {
            case R.id.retreat_btn:
                if (!EboxContext.threadIsable) {
                    EboxContext.mSystemNotify.setNotifyThreadWait(true);
                    showDialog(12);
                    return;
                }
                MainTabActivity.setCurrentTab(3);
                return;
            case R.id.title_text:
            default:
                return;
            case R.id.refresh_btn:
                refreshNews();
                return;
        }
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        try {
            if (e1.getX() - e2.getX() > 160.0f) {
                if (!EboxContext.threadIsable) {
                    EboxContext.mSystemNotify.setNotifyThreadWait(true);
                    showDialog(12);
                } else {
                    MainTabActivity.setCurrentTab(true);
                }
                return true;
            }
            if (e1.getX() - e2.getX() < -160.0f) {
                return true;
            }
            return false;
        } catch (Exception e) {
            Log.e(NewsActivity.class.getSimpleName(), "## onFling error : " + e.getMessage());
        }
    }

    public void onSubmitUpload() {
    }
}
