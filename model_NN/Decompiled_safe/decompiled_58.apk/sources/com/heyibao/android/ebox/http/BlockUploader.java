package com.heyibao.android.ebox.http;

import android.app.Service;
import android.util.Log;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.model.BlockInfo;
import java.io.RandomAccessFile;

public abstract class BlockUploader implements Runnable, BlockInterface {
    private Service activity;
    private RandomAccessFile input;
    private int offset;
    private int size;
    private int threadNum;

    public BlockUploader(Service activity2, int threadNum2) {
        this.activity = activity2;
        this.threadNum = threadNum2;
    }

    public int read(byte[] retVal) throws Exception {
        if (this.size <= 0) {
            return -1;
        }
        this.input.seek((long) this.offset);
        int length = this.input.read(retVal);
        if (length == -1) {
            return -1;
        }
        this.offset += length;
        this.size -= length;
        return length;
    }

    public void run() {
        BlockInfo currentBlock;
        Log.e(BlockInterface.class.getSimpleName(), "## start blockuploader " + this.threadNum);
        while (true) {
            if (EboxContext.blockInfo.isSuccess() || (currentBlock = EboxContext.blockInfo.getNextBlock()) == null) {
                break;
            }
            this.offset = currentBlock.getOffset();
            this.size = currentBlock.getCount();
            int retVal = getResponse((this.offset / EboxConst.BLOCK_SIZE) + 1);
            Log.d(BlockUploader.class.getSimpleName(), "## server info:" + retVal);
            if (retVal != 200) {
                reSuccess(false);
                break;
            }
            EboxContext.blockInfo.updateBlockSuccess(currentBlock.getOffset(), true);
            reSuccess(true);
            try {
                Thread.sleep(500);
            } catch (Exception e) {
            }
        }
        Log.e(BlockInterface.class.getSimpleName(), "## end blockuploader " + this.threadNum);
    }

    /* JADX WARN: Type inference failed for: r9v0, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0273 A[SYNTHETIC, Splitter:B:14:0x0273] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0278 A[Catch:{ IOException -> 0x03b5 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int getResponse(int r27) {
        /*
            r26 = this;
            r6 = 0
            r8 = 0
            r7 = 0
            boolean r23 = java.lang.Thread.interrupted()
            if (r23 != 0) goto L_0x0271
            java.lang.String r17 = "\r\n"
            java.lang.String r20 = "--"
            java.lang.String r4 = "*****"
            r23 = 8192(0x2000, float:1.14794E-41)
            r0 = r23
            byte[] r0 = new byte[r0]     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r5 = r0
            r16 = 0
            java.io.File r11 = new java.io.File     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            com.heyibao.android.ebox.model.Details r23 = com.heyibao.android.ebox.common.EboxContext.mDetails     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r23 = r23.getPath()     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r0 = r11
            r1 = r23
            r0.<init>(r1)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r12 = r11.getName()     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.io.RandomAccessFile r23 = new java.io.RandomAccessFile     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            com.heyibao.android.ebox.model.Details r24 = com.heyibao.android.ebox.common.EboxContext.mDetails     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r24 = r24.getPath()     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r25 = "r"
            r23.<init>(r24, r25)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r0 = r23
            r1 = r26
            r1.input = r0     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            com.heyibao.android.ebox.model.Details r23 = com.heyibao.android.ebox.common.EboxContext.mDetails     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r19 = r23.getPost()     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            org.json.JSONObject r14 = new org.json.JSONObject     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r0 = r14
            r1 = r19
            r0.<init>(r1)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.util.Hashtable r18 = new java.util.Hashtable     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r18.<init>()     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r23 = "Filename"
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r24.<init>()     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            com.heyibao.android.ebox.model.Details r25 = com.heyibao.android.ebox.common.EboxContext.mDetails     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r25 = r25.getSignature()     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.StringBuilder r24 = r24.append(r25)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r25 = "_part_"
            java.lang.StringBuilder r24 = r24.append(r25)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r0 = r24
            r1 = r27
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r24 = r24.toString()     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r0 = r18
            r1 = r23
            r2 = r24
            r0.put(r1, r2)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r23 = "key"
            java.lang.String r24 = "key"
            r0 = r14
            r1 = r24
            java.lang.String r24 = r0.getString(r1)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r0 = r18
            r1 = r23
            r2 = r24
            r0.put(r1, r2)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r23 = "expiration_date"
            java.lang.String r24 = "expiration_date"
            r0 = r14
            r1 = r24
            java.lang.String r24 = r0.getString(r1)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r0 = r18
            r1 = r23
            r2 = r24
            r0.put(r1, r2)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r23 = "digital_signature"
            java.lang.String r24 = "digital_signature"
            r0 = r14
            r1 = r24
            java.lang.String r24 = r0.getString(r1)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r0 = r18
            r1 = r23
            r2 = r24
            r0.put(r1, r2)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.net.URL r21 = new java.net.URL     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r23.<init>()     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            com.heyibao.android.ebox.model.Details r24 = com.heyibao.android.ebox.common.EboxContext.mDetails     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r24 = r24.getUrl()     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.StringBuilder r23 = r23.append(r24)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r24 = "/part_upload.php"
            java.lang.StringBuilder r23 = r23.append(r24)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r23 = r23.toString()     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r0 = r21
            r1 = r23
            r0.<init>(r1)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.net.URLConnection r9 = r21.openConnection()     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r0 = r9
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r7 = r0
            r23 = 1
            r0 = r7
            r1 = r23
            r0.setDoInput(r1)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r23 = 1
            r0 = r7
            r1 = r23
            r0.setDoOutput(r1)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r23 = 0
            r0 = r7
            r1 = r23
            r0.setUseCaches(r1)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r23 = 30000(0x7530, float:4.2039E-41)
            r0 = r7
            r1 = r23
            r0.setReadTimeout(r1)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r23 = 10000(0x2710, float:1.4013E-41)
            r0 = r7
            r1 = r23
            r0.setConnectTimeout(r1)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r23 = "POST"
            r0 = r7
            r1 = r23
            r0.setRequestMethod(r1)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r23 = "User-Agent"
            java.lang.String r24 = com.heyibao.android.ebox.common.EboxConst.USER_AGENT     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r0 = r7
            r1 = r23
            r2 = r24
            r0.setRequestProperty(r1, r2)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r23 = "Connection"
            java.lang.String r24 = "Keep-Alive"
            r0 = r7
            r1 = r23
            r2 = r24
            r0.setRequestProperty(r1, r2)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r23 = "Content-Type"
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r24.<init>()     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r25 = "multipart/form-data;boundary="
            java.lang.StringBuilder r24 = r24.append(r25)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r0 = r24
            r1 = r4
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.String r24 = r24.toString()     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r0 = r7
            r1 = r23
            r2 = r24
            r0.setRequestProperty(r1, r2)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r7.connect()     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.io.DataOutputStream r9 = new java.io.DataOutputStream     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.io.OutputStream r23 = r7.getOutputStream()     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            r0 = r9
            r1 = r23
            r0.<init>(r1)     // Catch:{ InterruptedException -> 0x03c2, Exception -> 0x03bd }
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r23.<init>()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r20
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r4
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r17
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.String r23 = r23.toString()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r9
            r1 = r23
            r0.writeBytes(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.util.Set r23 = r18.keySet()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.util.Iterator r13 = r23.iterator()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
        L_0x0187:
            boolean r23 = r13.hasNext()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            if (r23 == 0) goto L_0x02a8
            java.lang.Object r15 = r13.next()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.String r15 = (java.lang.String) r15     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r18
            r1 = r15
            java.lang.Object r22 = r0.get(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.String r22 = (java.lang.String) r22     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r23.<init>()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r20
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r4
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r17
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.String r23 = r23.toString()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r9
            r1 = r23
            r0.writeBytes(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r23.<init>()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.String r24 = "Content-Disposition: form-data; name=\""
            java.lang.StringBuilder r23 = r23.append(r24)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r15
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.String r24 = "\""
            java.lang.StringBuilder r23 = r23.append(r24)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.String r23 = r23.toString()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r9
            r1 = r23
            r0.writeBytes(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r23.<init>()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r17
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r17
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.String r23 = r23.toString()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r9
            r1 = r23
            r0.writeBytes(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r9
            r1 = r22
            r0.writeBytes(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.String r23 = "\r\n"
            r0 = r9
            r1 = r23
            r0.writeBytes(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r23.<init>()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r20
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r4
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r17
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.String r23 = r23.toString()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r9
            r1 = r23
            r0.writeBytes(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            goto L_0x0187
        L_0x0239:
            r23 = move-exception
            r10 = r23
            r8 = r9
        L_0x023d:
            r10.printStackTrace()
            r0 = r26
            android.app.Service r0 = r0.activity
            r23 = r0
            r24 = 2131230850(0x7f080082, float:1.8077764E38)
            java.lang.String r23 = r23.getString(r24)
            com.heyibao.android.ebox.common.EboxContext.message = r23
            java.lang.Class<com.heyibao.android.ebox.http.UploaderBuilder> r23 = com.heyibao.android.ebox.http.UploaderBuilder.class
            java.lang.String r23 = r23.getSimpleName()
            java.lang.StringBuilder r24 = new java.lang.StringBuilder
            r24.<init>()
            java.lang.String r25 = "## doBuilder is Force stop "
            java.lang.StringBuilder r24 = r24.append(r25)
            r0 = r26
            int r0 = r0.offset
            r25 = r0
            java.lang.StringBuilder r24 = r24.append(r25)
            java.lang.String r24 = r24.toString()
            android.util.Log.e(r23, r24)
        L_0x0271:
            if (r8 == 0) goto L_0x0276
            r8.close()     // Catch:{ IOException -> 0x03b5 }
        L_0x0276:
            if (r7 == 0) goto L_0x027c
            r7.disconnect()     // Catch:{ IOException -> 0x03b5 }
            r7 = 0
        L_0x027c:
            r0 = r26
            java.io.RandomAccessFile r0 = r0.input     // Catch:{ IOException -> 0x03b5 }
            r23 = r0
            r23.close()     // Catch:{ IOException -> 0x03b5 }
        L_0x0285:
            java.lang.Class<com.heyibao.android.ebox.http.UploaderBuilder> r23 = com.heyibao.android.ebox.http.UploaderBuilder.class
            java.lang.String r23 = r23.getSimpleName()
            java.lang.StringBuilder r24 = new java.lang.StringBuilder
            r24.<init>()
            java.lang.String r25 = "## doBuilder end "
            java.lang.StringBuilder r24 = r24.append(r25)
            r0 = r26
            int r0 = r0.offset
            r25 = r0
            java.lang.StringBuilder r24 = r24.append(r25)
            java.lang.String r24 = r24.toString()
            android.util.Log.e(r23, r24)
            return r6
        L_0x02a8:
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r23.<init>()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.String r24 = "Content-Disposition: form-data; name=\"file\";filename=\""
            java.lang.StringBuilder r23 = r23.append(r24)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r12
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.String r24 = "\""
            java.lang.StringBuilder r23 = r23.append(r24)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r17
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.String r23 = r23.toString()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r9
            r1 = r23
            r0.writeBytes(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r9
            r1 = r17
            r0.writeBytes(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
        L_0x02d8:
            r0 = r26
            r1 = r5
            int r16 = r0.read(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r23 = -1
            r0 = r16
            r1 = r23
            if (r0 == r1) goto L_0x0377
        L_0x02e7:
            com.heyibao.android.ebox.model.SystemNotify r23 = com.heyibao.android.ebox.common.EboxContext.mSystemNotify     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            boolean r23 = r23.hasNotifyThreadWait()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            if (r23 == 0) goto L_0x0341
            r23 = 1000(0x3e8, double:4.94E-321)
            java.lang.Thread.sleep(r23)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            goto L_0x02e7
        L_0x02f5:
            r23 = move-exception
            r10 = r23
            r8 = r9
        L_0x02f9:
            r10.printStackTrace()
            r0 = r26
            android.app.Service r0 = r0.activity
            r23 = r0
            android.content.res.Resources r23 = r23.getResources()
            r24 = 2131230824(0x7f080068, float:1.8077712E38)
            java.lang.String r23 = r23.getString(r24)
            com.heyibao.android.ebox.common.EboxContext.message = r23
            java.lang.Class<com.heyibao.android.ebox.http.UploaderBuilder> r23 = com.heyibao.android.ebox.http.UploaderBuilder.class
            java.lang.String r23 = r23.getSimpleName()
            java.lang.StringBuilder r24 = new java.lang.StringBuilder
            r24.<init>()
            java.lang.String r25 = "## doBuilder er blockID "
            java.lang.StringBuilder r24 = r24.append(r25)
            r0 = r26
            int r0 = r0.offset
            r25 = r0
            java.lang.StringBuilder r24 = r24.append(r25)
            java.lang.String r25 = " "
            java.lang.StringBuilder r24 = r24.append(r25)
            java.lang.String r25 = r10.getMessage()
            java.lang.StringBuilder r24 = r24.append(r25)
            java.lang.String r24 = r24.toString()
            android.util.Log.e(r23, r24)
            goto L_0x0271
        L_0x0341:
            r23 = 8192(0x2000, float:1.14794E-41)
            r0 = r16
            r1 = r23
            if (r0 != r1) goto L_0x0359
            r0 = r26
            int r0 = r0.size     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r23 = r0
            if (r23 >= 0) goto L_0x0359
            r0 = r26
            int r0 = r0.size     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r23 = r0
            int r16 = r16 + r23
        L_0x0359:
            r23 = 0
            r0 = r9
            r1 = r5
            r2 = r23
            r3 = r16
            r0.write(r1, r2, r3)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r9.flush()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r26
            r1 = r27
            r2 = r16
            r0.reProgress(r1, r2)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r23 = 50
            java.lang.Thread.sleep(r23)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            goto L_0x02d8
        L_0x0377:
            r0 = r9
            r1 = r17
            r0.writeBytes(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r23.<init>()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r20
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r4
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r20
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r23
            r1 = r17
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            java.lang.String r23 = r23.toString()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r0 = r9
            r1 = r23
            r0.writeBytes(r1)     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r9.flush()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            int r6 = r7.getResponseCode()     // Catch:{ InterruptedException -> 0x0239, Exception -> 0x02f5 }
            r8 = r9
            goto L_0x0271
        L_0x03b5:
            r23 = move-exception
            r10 = r23
            r10.printStackTrace()
            goto L_0x0285
        L_0x03bd:
            r23 = move-exception
            r10 = r23
            goto L_0x02f9
        L_0x03c2:
            r23 = move-exception
            r10 = r23
            goto L_0x023d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heyibao.android.ebox.http.BlockUploader.getResponse(int):int");
    }
}
