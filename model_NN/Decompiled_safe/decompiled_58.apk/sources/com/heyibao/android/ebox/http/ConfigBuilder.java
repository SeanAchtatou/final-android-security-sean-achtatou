package com.heyibao.android.ebox.http;

import ClientProtocol.ClientMeta;
import android.app.Service;
import android.util.Log;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;

public abstract class ConfigBuilder implements Runnable, HttpInterface {
    private int action;
    private Service activity;

    public ConfigBuilder(Service activity2, int action2) {
        this.activity = activity2;
        this.action = action2;
    }

    public void run() {
        Log.d(ConfigBuilder.class.getSimpleName(), "start ConfigBuilder ");
        configStart();
        Log.d(ConfigBuilder.class.getSimpleName(), "end ConfigBuilder ");
    }

    private void configStart() {
        int errcount = 0;
        boolean success = false;
        while (true) {
            if (Thread.interrupted() || errcount >= 3) {
                break;
            }
            try {
                ClientMeta.ServiceSystemConfigRequest.Builder request = ClientMeta.ServiceSystemConfigRequest.newBuilder();
                request.setSession(EboxContext.mDevice.getSession());
                request.setAction(this.action);
                Thread.sleep(10);
                ClientMeta.ServiceSystemConfigResponse response = ClientMeta.ServiceSystemConfigResponse.parseFrom(new GPBUtils(EboxConst.CONFIG_URL).getResult(request.build().toByteArray()));
                ClientMeta.Status status = response.getStatus();
                if (!status.getSuccess()) {
                    EboxContext.message = EboxConst.SESSIONEXIT;
                    break;
                }
                success = true;
                if (this.action == 1) {
                    EboxContext.configRet.setPhone(response.getPhone());
                    EboxContext.configRet.setEmail(response.getEmail());
                    EboxContext.configRet.setIsAdmin(response.getUserStatus().getIsAdmin());
                    EboxContext.configRet.setCurrentSize((double) response.getUserStatus().getCurrentSize());
                    EboxContext.configRet.setSpaceSize((double) response.getUserStatus().getSpaceSize());
                    EboxContext.configRet.setStartTime((double) response.getUserStatus().getStartTime());
                    EboxContext.configRet.setEndTime((double) response.getUserStatus().getEndTime());
                }
                EboxContext.message = status.getMsg();
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(ConfigBuilder.class.getSimpleName(), "## ConfigBuilder is Force stop");
                EboxContext.message = this.activity.getString(R.string.stop_thread);
            } catch (Exception e2) {
                Exception error = e2;
                error.printStackTrace();
                EboxContext.message = this.activity.getResources().getString(R.string.net_error);
                Log.e(ConfigBuilder.class.getSimpleName(), "post config error info " + error.getMessage() + " for " + errcount);
                errcount++;
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
            }
        }
        reportSuccess(success);
    }
}
