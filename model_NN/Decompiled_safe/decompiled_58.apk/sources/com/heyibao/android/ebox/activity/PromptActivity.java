package com.heyibao.android.ebox.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;

public class PromptActivity extends HomeMenuActivity {
    private ImageView imageVI;

    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(null);
        if (getResources().getConfiguration().orientation == 2) {
            this.mainLayout.setBackgroundResource(R.drawable.setting_bg);
            LinearLayout.LayoutParams mp = new LinearLayout.LayoutParams(new ViewGroup.MarginLayoutParams(-2, -2));
            mp.setMargins(0, 10, 0, 0);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams((ViewGroup.MarginLayoutParams) mp);
            lp.addRule(14);
            this.imageVI.setLayoutParams(lp);
        } else if (getResources().getConfiguration().orientation == 1) {
            this.mainLayout.setBackgroundResource(R.drawable.set_bg);
            LinearLayout.LayoutParams mp2 = new LinearLayout.LayoutParams(new ViewGroup.MarginLayoutParams(-2, -2));
            mp2.setMargins(0, 90, 0, 0);
            RelativeLayout.LayoutParams lp2 = new RelativeLayout.LayoutParams((ViewGroup.MarginLayoutParams) mp2);
            lp2.addRule(14);
            this.imageVI.setLayoutParams(lp2);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (EboxContext.retread) {
            this.refreshBtn.setVisibility(0);
            this.retreatBtn.setVisibility(0);
            return;
        }
        this.refreshBtn.setVisibility(4);
        this.retreatBtn.setVisibility(4);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.prompt_view);
        promptInit();
    }

    private void promptInit() {
        this.imageVI = (ImageView) findViewById(R.id.image_cloud);
        ((ScrollView) findViewById(R.id.main_panel)).setOnTouchListener(this);
        initTitleBar();
        this.refreshBtn.setVisibility(4);
        this.retreatBtn.setVisibility(4);
        this.midTV.setText(EboxContext.mDevice.getNick());
        findViewById(R.id.bt_prompt).setOnClickListener(this);
    }

    public void onClick(View v) {
        EboxContext.mDetails.reSet();
        switch (v.getId()) {
            case R.id.bt_prompt:
                MainTabActivity.detailsAction = EboxConst.PROMPT_UPLOADER;
                MainTabActivity.setCurrentTab(0);
                return;
            case R.id.retreat_btn:
                MainTabActivity.detailsAction = EboxConst.PROMPT_RETREAT;
                MainTabActivity.setCurrentTab(0);
                return;
            case R.id.refresh_btn:
                MainTabActivity.detailsAction = EboxConst.PROMPT_REFRESH;
                MainTabActivity.setCurrentTab(0);
                return;
            default:
                return;
        }
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        try {
            if (e1.getX() - e2.getX() > 160.0f) {
                MainTabActivity.setCurrentTab(true);
                return true;
            }
        } catch (Exception e) {
            Log.e(DetailsActivity.class.getSimpleName(), "## onFling error : " + e.getMessage());
        }
        return false;
    }
}
