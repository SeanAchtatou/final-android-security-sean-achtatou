package com.heyibao.android.ebox.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.model.MyDialog;
import com.heyibao.android.ebox.util.Utils;

public class SettingActivity extends HomeMenuActivity {
    /* access modifiers changed from: private */
    public boolean downSuccess = false;
    protected Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            Intent intent = (Intent) msg.obj;
            boolean success = intent.getBooleanExtra("success", false);
            switch (msg.what) {
                case 2:
                    SettingActivity.this.checkEnd(success);
                    return;
                case 3:
                    SettingActivity.this.downUpGrade(success, intent);
                    return;
                default:
                    return;
            }
        }
    };
    protected MyDialog proDialog;
    private SetReceiver receiver;

    private static class EfficientAdapter extends BaseAdapter {
        private String[] DATA = new String[6];
        private Bitmap[] mIcon = new Bitmap[6];
        private LayoutInflater mInflater;

        public EfficientAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
            this.DATA[0] = context.getString(R.string.set_system);
            this.DATA[1] = context.getString(R.string.set_accout);
            this.DATA[2] = context.getString(R.string.set_flow);
            this.DATA[3] = context.getString(R.string.set_share);
            this.DATA[4] = context.getString(R.string.set_back);
            this.DATA[5] = context.getString(R.string.set_about);
            this.mIcon[0] = BitmapFactory.decodeResource(context.getResources(), R.drawable.xi_tong_she_zhi_72);
            this.mIcon[1] = BitmapFactory.decodeResource(context.getResources(), R.drawable.zhang_hao_she_zhi_72);
            this.mIcon[2] = BitmapFactory.decodeResource(context.getResources(), R.drawable.huo_qu_zui_xin_ban_ben_72);
            this.mIcon[3] = BitmapFactory.decodeResource(context.getResources(), R.drawable.li_ji_fen_xiang_72);
            this.mIcon[4] = BitmapFactory.decodeResource(context.getResources(), R.drawable.yi_jian_fan_kui_72);
            this.mIcon[5] = BitmapFactory.decodeResource(context.getResources(), R.drawable.guan_yu_wo_men_72);
        }

        public int getCount() {
            return this.DATA.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.setting_row, (ViewGroup) null);
                holder = new ViewHolder();
                holder.text = (TextView) convertView.findViewById(R.id.TextView01);
                holder.icon2 = (ImageView) convertView.findViewById(R.id.ImageView01);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.text.setText(this.DATA[position]);
            holder.icon2.setImageBitmap(this.mIcon[position]);
            return convertView;
        }

        static class ViewHolder {
            ImageView icon2;
            TextView text;

            ViewHolder() {
            }
        }
    }

    public class SetReceiver extends BroadcastReceiver {
        public SetReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("action");
            Log.d(SettingActivity.class.getSimpleName(), "## onReceive : " + action);
            if (EboxConst.ACTION_AUTOUPDATE.equals(action)) {
                SettingActivity.this.mHandler.sendMessage(SettingActivity.this.mHandler.obtainMessage(2, intent));
            } else if (EboxConst.ACTION_DOWNUPDATE.equals(action)) {
                SettingActivity.this.mHandler.sendMessage(SettingActivity.this.mHandler.obtainMessage(3, intent));
            } else if (EboxConst.ACTION_LOGIN.equals(action)) {
                SettingActivity.this.mHandler.sendMessage(SettingActivity.this.mHandler.obtainMessage(4, intent));
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.setting_view);
        Log.d(SettingActivity.class.getSimpleName(), "## start MySettingActivity : ");
        settingInit();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 2:
                MyDialog mDialog = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.bt_ok:
                                SettingActivity.this.removeDialog(3);
                                SettingActivity.this.showDialog(3);
                                Utils.startService(SettingActivity.this, EboxConst.ACTION_DOWNUPDATE, null, R.drawable.app_icon);
                                dismiss();
                                return;
                            case R.id.bt_cancel:
                                SettingActivity.this.cancelFuncation(this);
                                return;
                            default:
                                return;
                        }
                    }
                };
                mDialog.setDefaultView();
                mDialog.setMessage(Utils.replace(getResources().getString(R.string.upgrade_info), EboxContext.mUpGrade.getVersion()));
                return mDialog;
            case 3:
                return proDialog();
            case 12:
                return stopDialog();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.receiver = new SetReceiver();
        registerReceiver(this.receiver, new IntentFilter(EboxConst.EBOXBROAD), null, this.mHandler);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        unregisterReceiver(this.receiver);
    }

    /* access modifiers changed from: protected */
    public void reLoader() {
        if (EboxContext.mSystemNotify.hasNotifySetting()) {
            EboxContext.mSystemNotify.setNotifySetting(false);
            showProgressBar();
        }
    }

    private void settingInit() {
        initTitleBar();
        this.midTV.setText(getString(R.string.my_setting));
        this.retreatBtn.setVisibility(4);
        this.refreshBtn.setVisibility(4);
        ListView lv = (ListView) findViewById(16908298);
        lv.setAdapter((ListAdapter) new EfficientAdapter(this));
        lv.setOnTouchListener(this);
    }

    private MyDialog proDialog() {
        if (this.proDialog == null) {
            this.proDialog = new MyDialog(this, R.style.dialog) {
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.bt_ok:
                            if (SettingActivity.this.downSuccess) {
                                Utils.installAPK(SettingActivity.this);
                                return;
                            } else {
                                Utils.MessageBox(SettingActivity.this, SettingActivity.this.getString(R.string.download_ing));
                                return;
                            }
                        case R.id.bt_cancel:
                            Utils.startService(SettingActivity.this, EboxConst.ACTION_STOP, null, 0);
                            dismiss();
                            return;
                        default:
                            return;
                    }
                }

                public void onAttachedToWindow() {
                    super.onAttachedToWindow();
                }

                public void onBackPressed() {
                    super.onBackPressed();
                }
            };
            this.proDialog.setProgressView();
            this.proDialog.setMessage(getResources().getString(R.string.loading));
            this.proDialog.setCancelable(false);
            this.proDialog.setCanceledOnTouchOutside(false);
        }
        return this.proDialog;
    }

    private void checkUpGrade() {
        if (hasWorking()) {
            showProgressBar();
            Log.d(SettingActivity.class.getSimpleName(), "## nofiy upgrading to service ");
            Utils.startService(this, EboxConst.ACTION_AUTOUPDATE, null, R.drawable.app_sync);
        }
    }

    /* access modifiers changed from: private */
    public void checkEnd(boolean success) {
        Utils.startService(this, EboxConst.ACTION_NORMAL, null, 0);
        showProgressBar();
        if (!success) {
            Log.d(SettingActivity.class.getSimpleName(), "## no need upgrade");
            Utils.MessageBox(this, Utils.replace(getString(R.string.upgrade_info_end), getString(R.string.version)));
        } else if (!Utils.hasSdcard(this)) {
            showDialog(2);
        }
    }

    /* access modifiers changed from: private */
    public void downUpGrade(boolean success, Intent intent) {
        if (success) {
            this.downSuccess = success;
            this.proDialog.setMessage(getResources().getString(R.string.download_end));
            return;
        }
        int val = intent.getIntExtra("retval", 0);
        double total = intent.getDoubleExtra("total", -1.0d);
        if (total == -1.0d) {
            removeDialog(3);
            Utils.MessageBox(this, getResources().getString(R.string.download_false));
        }
        if (EboxContext.message != null) {
            this.proDialog.setMessage(EboxContext.message);
        }
        if (val != 0 && val > this.proDialog.progress.getProgress()) {
            this.proDialog.setProgress((int) (((double) (val * 100)) / total));
        }
        Log.d(NewAccActivity.class.getSimpleName(), "## programs : " + val);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void share() {
        Intent intent = new Intent(EboxConst.ACTION_GET_SEND);
        intent.putExtra(EboxConst.POINT_GET_SEND, true);
        intent.setType("text/plain");
        intent.putExtra("android.intent.extra.SUBJECT", getString(R.string.share_title));
        intent.putExtra("android.intent.extra.TEXT", getString(R.string.share_context));
        startActivity(Intent.createChooser(intent, getTitle()));
    }

    /* access modifiers changed from: protected */
    public void feedBack() {
        Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse(getString(R.string.feed_back_email)));
        intent.putExtra("android.intent.extra.SUBJECT", getString(R.string.feed_back_title));
        intent.putExtra("android.intent.extra.TEXT", getString(R.string.feed_back_context));
        startActivity(Intent.createChooser(intent, getTitle()));
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        switch (position) {
            case 0:
                if (!EboxContext.threadIsable) {
                    EboxContext.mSystemNotify.setNotifyThreadWait(true);
                    showDialog(12);
                    return;
                }
                MainTabActivity.setCurrentTab(5);
                return;
            case 1:
                if (!EboxContext.threadIsable) {
                    EboxContext.mSystemNotify.setNotifyThreadWait(true);
                    showDialog(12);
                    return;
                }
                MainTabActivity.setCurrentTab(6);
                return;
            case 2:
                if (!Utils.hasNet(this)) {
                    checkUpGrade();
                    return;
                }
                return;
            case 3:
                share();
                return;
            case 4:
                feedBack();
                return;
            case 5:
                if (!EboxContext.threadIsable) {
                    EboxContext.mSystemNotify.setNotifyThreadWait(true);
                    showDialog(12);
                    return;
                }
                MainTabActivity.setCurrentTab(7);
                return;
            default:
                return;
        }
    }
}
