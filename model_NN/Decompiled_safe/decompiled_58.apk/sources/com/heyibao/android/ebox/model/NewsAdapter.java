package com.heyibao.android.ebox.model;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;
import com.heyibao.android.ebox.R;

public class NewsAdapter extends ResourceCursorAdapter {
    public static final int RESOURCE = 2130903053;
    private boolean onTouchBoolean = false;
    private int onTouchItemIndex = -1;

    static class ViewHolder {
        ImageView imgIV;
        TextView nameTV;
        TextView sizeTV;

        ViewHolder() {
        }
    }

    public void toggle(int position) {
        if (this.onTouchItemIndex == position) {
            this.onTouchBoolean = !this.onTouchBoolean;
        } else {
            this.onTouchItemIndex = position;
            this.onTouchBoolean = true;
        }
        notifyDataSetChanged();
    }

    public NewsAdapter(Context context, int layout, Cursor c) {
        super(context, layout, c);
    }

    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder cache = (ViewHolder) view.getTag();
        cache.nameTV.setText(cursor.getString(1));
        cache.sizeTV.setText("asdfffffffffffffffffffffffffffnasdffffffffffffffffffffffffffffffffffffasdffffffffffffffffffffffffffffffffffffasdffffffffffffffffffffffffffffffffffffasdffffffffffffnasdffffffffffffffffffffffffffffffffffffasdffffffffffffffffffffffffffffffffffffasdffffffffffffffffffffffffffffffffffffasdffffffffffffnasdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffasdffffffffffffnasdffffffffffffffffffffffffffffffffffffasdffffffffffffffffffffffffffffffffffffasdffffffffffffffffffffffffffffffffffffasdffffffffffffnasdffffffffffffffffffffffffffffffffffffasdffffffffffffffffffffffffffffffffffffasdffffffffffffffffffffffffffffffffffffasdffffffffffffffffffffffffffffffffffff");
        boolean reSize = true;
        if (cursor.getInt(0) == this.onTouchItemIndex) {
            if (this.onTouchBoolean) {
                reSize = false;
            } else {
                reSize = true;
            }
        }
        LinearLayout l = (LinearLayout) view.findViewById(R.id.item_btns_layout);
        if (reSize) {
            l.setVisibility(8);
            cache.sizeTV.setEllipsize(TextUtils.TruncateAt.END);
            cache.imgIV.setVisibility(8);
            return;
        }
        l.setVisibility(0);
        cache.sizeTV.setEllipsize(null);
        cache.imgIV.setImageResource(R.drawable.text);
        cache.imgIV.setVisibility(0);
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = super.newView(context, cursor, parent);
        ViewHolder cache = new ViewHolder();
        cache.imgIV = (ImageView) view.findViewById(R.id.avatar);
        cache.nameTV = (TextView) view.findViewById(R.id.d_name_tv);
        cache.sizeTV = (TextView) view.findViewById(R.id.d_size_tv);
        view.setTag(cache);
        return view;
    }
}
