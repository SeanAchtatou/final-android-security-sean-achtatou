package com.heyibao.android.ebox.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.db.ActionDB;
import com.heyibao.android.ebox.model.MyDialog;
import com.heyibao.android.ebox.util.Utils;
import java.io.File;
import java.util.HashMap;

public class ShareActivity extends Activity {
    protected DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            ShareActivity.this.finish();
        }
    };
    protected Handler mHandler = new Handler();
    protected Runnable runnableShare = new Runnable() {
        public void run() {
            ShareActivity.this.getShare();
            ShareActivity.this.mHandler.removeCallbacks(ShareActivity.this.runnableShare);
        }
    };

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 9:
                MyDialog mDialog = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.bt_ok:
                                Utils.exitAppEnd(ShareActivity.this);
                                return;
                            case R.id.bt_cancel:
                                dismiss();
                                return;
                            default:
                                return;
                        }
                    }
                };
                mDialog.setDefaultView();
                mDialog.setMessage(getResources().getString(R.string.share_error_1));
                return mDialog;
            case 14:
                ProgressDialog proDialog = new ProgressDialog(this);
                proDialog.setProgressStyle(0);
                proDialog.setTitle(getString(R.string.app_name));
                proDialog.setMessage(getString(R.string.share_loading));
                proDialog.setCancelable(false);
                proDialog.setIndeterminate(true);
                proDialog.setButton(getString(R.string.cancel), this.cancelListener);
                return proDialog;
            default:
                return null;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        Log.d(ShareActivity.class.getSimpleName(), " ## start ShareActivity");
        try {
            if (getIntent().getExtras().get(EboxConst.POINT_GET_SEND) != null) {
                Utils.MessageBox(this, getString(R.string.share_error_2));
                finish();
                return;
            }
            Uri uri = (Uri) getIntent().getExtras().get("android.intent.extra.STREAM");
            Log.d(ShareActivity.class.getSimpleName(), uri.toString());
            HashMap<String, Object> thisHash = ActionDB.getThumbnail(this, uri);
            if (thisHash != null) {
                EboxContext.mSystemInfo.setShareData(thisHash.get("path").toString());
                Log.e(ShareActivity.class.getSimpleName(), "get share data file " + EboxContext.mSystemInfo.getShareData());
            }
            init();
            Log.d(ShareActivity.class.getSimpleName(), " ## end ShareActivity");
        } catch (Exception e) {
            Exception e2 = e;
            e2.printStackTrace();
            Log.e(ShareActivity.class.getSimpleName(), "get share data error" + e2.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    public void init() {
        showDialog(14);
        this.mHandler.postDelayed(this.runnableShare, 500);
    }

    /* access modifiers changed from: protected */
    public void getShare() {
        if (EboxContext.curActivity == null) {
            EboxContext.mSystemInfo.setRefresh(false);
            EboxContext.mSystemInfo.setNet(Utils.checkNetWork(this));
            EboxContext.mSystemInfo.setDBVersion(Integer.valueOf(getString(R.string.db_version)).intValue());
            EboxContext.mSystemInfo.setAppName(getString(R.string.app_type));
            EboxContext.mSystemInfo.setSdPath(Environment.getExternalStorageDirectory() + File.separator + getString(R.string.app_type) + File.separator);
            EboxContext.mSystemInfo.setSdcard(Utils.checkSDCard());
            Display display = getWindowManager().getDefaultDisplay();
            Log.e("view", "height:" + display.getHeight());
            Log.e("view", "width:" + display.getWidth());
            if (display.getHeight() > EboxConst.displayHeigth) {
                EboxContext.mSystemInfo.setDisplayChange(true);
            }
            if (!needRun()) {
                removeDialog(14);
                showDialog(9);
                return;
            }
            startActivity(new Intent(this, MainTabActivity.class));
            finish();
            return;
        }
        if (!EboxContext.curActivity.getClass().getSimpleName().equals("MainTabActivity")) {
            Log.d(ShareActivity.class.getSimpleName(), "app still run but no login out");
            Utils.MessageBox(this, getString(R.string.share_error_1));
        } else {
            Log.d(ShareActivity.class.getSimpleName(), " ## app still run ");
            Log.d(ShareActivity.class.getSimpleName(), "## create ShareActivity now");
            Log.d(ShareActivity.class.getSimpleName(), "## nick " + EboxContext.mDevice.getNick());
            Log.d(ShareActivity.class.getSimpleName(), "## Session " + EboxContext.mDevice.getSession());
            Log.d(ShareActivity.class.getSimpleName(), "## TeamName " + EboxContext.mDevice.getTeamName());
            Log.d(ShareActivity.class.getSimpleName(), "## userName " + EboxContext.mDevice.getUserName());
            Log.d(ShareActivity.class.getSimpleName(), "## PassWord " + EboxContext.mDevice.getPassWord());
            Log.d(ShareActivity.class.getSimpleName(), "## DeviceName " + EboxContext.mDevice.getDeviceName());
            Log.d(ShareActivity.class.getSimpleName(), "## DeviceInfo " + EboxContext.mDevice.getDeviceInfo());
            Log.d(ShareActivity.class.getSimpleName(), "## AppVersion22  " + getResources().getString(R.string.version));
            Log.d(ShareActivity.class.getSimpleName(), "## AppVersion  " + EboxContext.mUpGrade.getVersion());
            Log.d(ShareActivity.class.getSimpleName(), "## Appname  " + EboxContext.mSystemInfo.getAppName());
            Log.d(ShareActivity.class.getSimpleName(), "## DBversion  " + EboxContext.mSystemInfo.getDBVersion());
            EboxContext.mSystemInfo.setRefresh(false);
            if (EboxContext.mSystemInfo.getShareData() == null) {
                Utils.MessageBox(this, getString(R.string.share_error));
            }
        }
        Intent intent = new Intent(this, EboxContext.curActivity.getClass());
        intent.addFlags(268697600);
        startActivity(intent);
        finish();
    }

    /* access modifiers changed from: protected */
    public boolean needRun() {
        if (EboxContext.mSystemInfo.getShareData() == null) {
            return false;
        }
        EboxContext.mSystemInfo.setSdcard(Utils.checkSDCard());
        EboxContext.mSystemInfo.setNet(Utils.checkNetWork(this));
        if (Utils.hasNet(this)) {
            return false;
        }
        HashMap<String, Object> accounts = ActionDB.getAccounts(this);
        if (accounts == null) {
            return false;
        }
        EboxContext.mDevice.setTeamName(accounts.get("teamName").toString());
        EboxContext.mDevice.setUserName(accounts.get("userName").toString());
        EboxContext.mDevice.setPassWord(accounts.get("passWord").toString());
        EboxContext.mDevice.setDeviceName(accounts.get("deviceName").toString());
        EboxContext.mDevice.setDeviceInfo(accounts.get("deviceInfo").toString());
        EboxContext.mDevice.setNick(accounts.get("nick").toString());
        EboxContext.mDevice.setCode(2);
        EboxContext.mDevice.setSuccess(false);
        Utils.startService(this, EboxConst.ACTION_LOGIN, null, 0);
        return true;
    }

    public boolean onKeyDown(int i, KeyEvent event) {
        if (i != 4) {
            return false;
        }
        finish();
        return false;
    }
}
