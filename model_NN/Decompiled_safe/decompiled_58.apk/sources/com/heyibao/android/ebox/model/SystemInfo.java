package com.heyibao.android.ebox.model;

public class SystemInfo {
    private String appName;
    private boolean autoLogin = true;
    private boolean autoNotify = true;
    private boolean autoUpgrade = true;
    private int curRowNum = -1;
    private int dbVersion;
    private boolean displayChange = false;
    private boolean isFirst = false;
    private boolean isLogin = false;
    private boolean isNet = false;
    private boolean isSdcard = false;
    private boolean isUpdate = false;
    private boolean offLine = false;
    private int optionRefre = 0;
    private boolean refresh = true;
    private String sdPath;
    private String shareData = null;
    private String signTime;
    private String syncContactsTime;
    private long time;
    private String userSdPath;

    public void reSet() {
        this.autoLogin = true;
        this.autoUpgrade = true;
        this.autoNotify = true;
        this.offLine = false;
        this.isLogin = false;
        this.isFirst = false;
        this.isUpdate = false;
        this.isNet = true;
        this.refresh = true;
        this.optionRefre = 0;
        this.curRowNum = -1;
        this.shareData = null;
        this.syncContactsTime = null;
        this.signTime = null;
    }

    public void setAppName(String appName2) {
        this.appName = appName2;
    }

    public String getAppName() {
        return this.appName;
    }

    public void setUserSdPath(String userSdPath2) {
        this.userSdPath = userSdPath2;
    }

    public String getUserSdPath() {
        return this.userSdPath;
    }

    public void setSdPath(String sdPath2) {
        this.sdPath = sdPath2;
    }

    public String getSdPath() {
        return this.sdPath;
    }

    public int getDBVersion() {
        return this.dbVersion;
    }

    public void setDBVersion(int dbVersion2) {
        this.dbVersion = dbVersion2;
    }

    public void setAutoLogin(boolean autoLogin2) {
        this.autoLogin = autoLogin2;
    }

    public boolean hasAutoLogin() {
        return this.autoLogin;
    }

    public void setAutoUpgrade(boolean autoUpgrade2) {
        this.autoUpgrade = autoUpgrade2;
    }

    public boolean hasAutoUpgrade() {
        return this.autoUpgrade;
    }

    public void setNotify(boolean hasNotify) {
        this.autoNotify = hasNotify;
    }

    public boolean hasNotify() {
        return this.autoNotify;
    }

    public void setFirst(boolean isFirst2) {
        this.isFirst = isFirst2;
    }

    public boolean hasFirst() {
        return this.isFirst;
    }

    public void setUpdate(boolean isUpdate2) {
        this.isUpdate = isUpdate2;
    }

    public boolean hasUpdate() {
        return this.isUpdate;
    }

    public void setLogin(boolean isLogin2) {
        this.isLogin = isLogin2;
    }

    public boolean hasLogin() {
        return this.isLogin;
    }

    public void setSdcard(boolean isSdcard2) {
        this.isSdcard = isSdcard2;
    }

    public boolean hasSdcard() {
        return this.isSdcard;
    }

    public void setNet(boolean isNet2) {
        this.isNet = isNet2;
    }

    public boolean hasNet() {
        return this.isNet;
    }

    public void setOffLine(boolean offLine2) {
        this.offLine = offLine2;
    }

    public boolean hasOffLine() {
        return this.offLine;
    }

    public void setRefresh(boolean refresh2) {
        this.refresh = refresh2;
    }

    public boolean hasRefresh() {
        return this.refresh;
    }

    public void setOptionRefre(int optionRefre2) {
        this.optionRefre = optionRefre2;
    }

    public int getOptionRefre() {
        return this.optionRefre;
    }

    public void setLastTime(long time2) {
        this.time = time2;
    }

    public long getLastTime() {
        return this.time;
    }

    public void setSyncContactsTime(String syncContactsTime2) {
        this.syncContactsTime = syncContactsTime2;
    }

    public String getSyncContactsTime() {
        return this.syncContactsTime;
    }

    public void setSignTime(String signTime2) {
        this.signTime = signTime2;
    }

    public String getSignTime() {
        return this.signTime;
    }

    public int getCurRowNum() {
        return this.curRowNum;
    }

    public void setCurRowNum(int curRowNum2) {
        this.curRowNum = curRowNum2;
    }

    public void reSetCurRowNum() {
        this.curRowNum = -1;
    }

    public String getShareData() {
        return this.shareData;
    }

    public void setShareData(String shareData2) {
        this.shareData = shareData2;
    }

    public void setDisplayChange(boolean displayChange2) {
        this.displayChange = displayChange2;
    }

    public boolean hasDisplayChange() {
        return this.displayChange;
    }
}
