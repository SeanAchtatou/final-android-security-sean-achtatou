package com.heyibao.android.ebox.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.db.ActionDB;
import com.heyibao.android.ebox.model.Details;
import com.heyibao.android.ebox.model.DetailsAdapter;
import com.heyibao.android.ebox.util.Utils;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ControlActivity extends HomeMenuActivity {
    protected ArrayList<Details> arrList = new ArrayList<>();
    protected Details curDetails;
    protected List<Details> curDetailsList = new ArrayList();
    protected DetailsAdapter detailsAdapter;
    private boolean isRefresh;
    private List<String> localList = new ArrayList();
    private Runnable runnableDownload = new Runnable() {
        public void run() {
            Utils.startService(ControlActivity.this, EboxConst.ACTION_NORMAL, null, 0);
            HashMap<String, String> reHash = ActionDB.getThumbnail(ControlActivity.this, EboxContext.mDetails.getPath(), EboxContext.mDetails.getIcon().intValue());
            if (reHash != null) {
                try {
                    EboxContext.mDetails.setThumbnail(reHash.get("thumbnail"));
                    EboxContext.mDetails.setLatitude(reHash.get("latitude"));
                    EboxContext.mDetails.setLongitude(reHash.get("longitude"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                EboxContext.mDetails.setThumbnail(Utils.getSuffix(EboxContext.mDetails.getName()) + EboxConst.SUFFIX);
            }
            EboxContext.mDetails.setSuccess(true);
            EboxContext.mDetails.setUpdown(0);
            EboxContext.mDetails.setProgress(0);
            if (!ActionDB.updateOneDetail(ControlActivity.this, EboxContext.mDetails)) {
                Utils.MessageBox(ControlActivity.this, ControlActivity.this.getResources().getString(R.string.db_error));
            }
            Details d = ControlActivity.this.arrList.get(ControlActivity.this.detailsAdapter.getSelectedId());
            d.setSuccess(true);
            d.setUpdown(0);
            d.setProgress(0);
            try {
                d.setThumbnail(reHash.get("thumbnail"));
                d.setLatitude(reHash.get("latitude"));
                d.setLongitude(reHash.get("longitude"));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            ControlActivity.this.detailsAdapter.notifyDataSetChanged();
            EboxContext.mSystemNotify.setNotifyDownload(true);
            EboxContext.mSystemNotify.setNotifySystem(true);
            ControlActivity.this.showProgressBar();
            ControlActivity.this.mHandler.removeCallbacks(this);
        }
    };
    private Runnable runnableShare = new Runnable() {
        public void run() {
            EboxContext.mSystemInfo.setUserSdPath(EboxContext.mSystemInfo.getSdPath() + EboxContext.mDevice.getNick() + File.separator);
            Utils.isExitParent(EboxContext.mSystemInfo.getUserSdPath() + EboxConst.CATCHICON);
            if (EboxContext.mSystemInfo.getShareData() == null) {
                Utils.MessageBox(ControlActivity.this, ControlActivity.this.getString(R.string.share_error));
                ControlActivity.this.startGetDBHanding();
            } else if (Utils.isExitFile(EboxContext.mSystemInfo.getShareData())) {
                Utils.MessageBox(ControlActivity.this, ControlActivity.this.getString(R.string.share_error_2));
                ControlActivity.this.startGetDBHanding();
                return;
            } else {
                String path = EboxContext.mSystemInfo.getShareData();
                File f = new File(path);
                if (!f.exists()) {
                    Utils.MessageBox(ControlActivity.this, ControlActivity.this.getString(R.string.share_error));
                    return;
                }
                long l = f.length();
                EboxContext.mDetails.setIcon(Integer.valueOf(Utils.getDetailsIcon(ControlActivity.this, path)));
                EboxContext.mDetails.setSize((double) l);
                EboxContext.mDetails.setName(f.getName());
                EboxContext.mDetails.setPath(path);
                ControlActivity.this.startUploadHanding();
            }
            ControlActivity.this.mHandler.removeCallbacks(this);
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void controlInit() {
        initTitleBar();
        this.retreatBtn.setVisibility(4);
        this.midTV.setText(EboxContext.mDevice.getNick());
        this.isRefresh = EboxContext.mSystemInfo.hasRefresh();
        reCurDetails();
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                Intent intent = (Intent) msg.obj;
                boolean success = intent.getBooleanExtra("success", false);
                switch (msg.what) {
                    case 4:
                        if (success) {
                            ControlActivity.this.startShare();
                            return;
                        } else {
                            Utils.MessageBox(ControlActivity.this, ControlActivity.this.getString(R.string.share_error_1));
                            return;
                        }
                    case 5:
                        ControlActivity.this.listFileEnd(success);
                        return;
                    case 6:
                        ControlActivity.this.uploadEnd(success);
                        return;
                    case 7:
                        ControlActivity.this.setProgress(intent);
                        return;
                    case 8:
                        ControlActivity.this.downloadEnd(success, false);
                        return;
                    case 9:
                        ControlActivity.this.setProgress(intent);
                        return;
                    case 10:
                        ControlActivity.this.createDirEnd(success);
                        return;
                    case 11:
                        ControlActivity.this.reNameEnd(success);
                        return;
                    case 12:
                        ControlActivity.this.delEnd(success);
                        return;
                    case 13:
                        ControlActivity.this.downloadEnd(success, true);
                        return;
                    case 14:
                    case 15:
                    default:
                        return;
                    case 16:
                        ControlActivity.this.detailsView();
                        return;
                }
            }
        };
        Thread threadUI = new Thread(new Runnable() {
            public void run() {
                while (!Thread.interrupted()) {
                    synchronized (ControlActivity.this.object) {
                        try {
                            ControlActivity.this.object.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    ControlActivity.this.arrList = ActionDB.getFileListByParentUUID(ControlActivity.this, ControlActivity.this.curDetails.getShowUuid());
                    EboxContext.mDetails.reSet();
                    ControlActivity.this.mHandler.sendMessage(ControlActivity.this.mHandler.obtainMessage(16, new Intent()));
                }
            }
        });
        synchronized (this.object) {
            threadUI.start();
        }
        if (EboxContext.mSystemInfo.getShareData() != null) {
            showProgressBar();
            if (EboxContext.mDevice.hasSuccess()) {
                startShare();
                return;
            }
            return;
        }
        this.mHandler.postDelayed(new Runnable() {
            public void run() {
                ControlActivity.this.getFileDetails();
            }
        }, 500);
    }

    /* access modifiers changed from: protected */
    public void reLoader() {
        if (EboxContext.mSystemNotify.hasNotifyListDetails()) {
            EboxContext.mSystemNotify.setNotifyListDetails(false);
            EboxContext.mDetails.reSet();
            startGetDBHanding();
        }
    }

    /* access modifiers changed from: protected */
    public void startShare() {
        this.mHandler.postDelayed(this.runnableShare, 500);
    }

    /* access modifiers changed from: protected */
    public void reCurDetails() {
        this.curDetails = new Details(getString(R.string.app_type), R.drawable.back_up_level, EboxConst.RETREAT_ICON);
        this.curDetailsList.clear();
        this.curDetailsList.add(this.curDetails);
    }

    /* access modifiers changed from: protected */
    public void setLocalStr() {
        String uuid = this.curDetails.getShowUuid();
        if (!this.isRefresh) {
            if (!this.localList.contains(uuid)) {
                this.localList.add(uuid);
            }
        } else if (!this.localList.contains(uuid)) {
            EboxContext.mSystemInfo.setRefresh(true);
            this.localList.add(uuid);
        } else {
            EboxContext.mSystemInfo.setRefresh(false);
        }
    }

    /* access modifiers changed from: protected */
    public void getFileDetails() {
        setLocalStr();
        if (!EboxContext.mSystemInfo.hasRefresh() || !EboxContext.mSystemInfo.hasNet()) {
            startGetDBHanding();
        } else {
            startListFileHanding();
        }
    }

    /* access modifiers changed from: protected */
    public void stopWorking() {
        Utils.startService(this, EboxConst.ACTION_STOP, null, 0);
        EboxContext.mDetails.reSet();
        startGetDBHanding();
    }

    /* access modifiers changed from: protected */
    public void timerListFileHanding() {
        if (EboxContext.threadIsable) {
            showProgressBar();
            Utils.startService(this, EboxConst.ACTION_LISTFILE, this.curDetails, R.drawable.app_sync);
        }
    }

    /* access modifiers changed from: protected */
    public void startListFileHanding() {
        if (hasWorking()) {
            showProgressBar();
            Utils.startService(this, EboxConst.ACTION_LISTFILE, this.curDetails, R.drawable.app_sync);
        }
    }

    /* access modifiers changed from: protected */
    public void startGetDBHanding() {
        if (this.detailsAdapter != null) {
            this.detailsAdapter.clear();
        }
        closeProgressBar();
        synchronized (this.object) {
            this.object.notify();
        }
    }

    /* access modifiers changed from: protected */
    public void startUploadHanding() {
        if (hasWorking()) {
            showProgressBar();
            EboxContext.mDetails.setObjectType(this.curDetails.getObjectType());
            EboxContext.mDetails.setShowUuid(this.curDetails.getShowUuid());
            EboxContext.mDetails.setUpdown(6);
            addDetailsView(EboxContext.mDetails);
            Utils.startService(this, EboxConst.ACTION_UPLOAD, null, R.drawable.app_sync);
        }
    }

    /* access modifiers changed from: protected */
    public void startDownloadHanding() {
        if (hasWorking()) {
            showProgressBar();
            EboxContext.mDetails.setUpdown(8);
            Utils.startService(this, EboxConst.ACTION_DOWNLOAD, null, R.drawable.app_sync);
        }
    }

    /* access modifiers changed from: protected */
    public void startCreateDirHanding(String name) {
        if (hasWorking()) {
            EboxContext.mDetails.reSet();
            EboxContext.mDetails.setName(name);
            EboxContext.mDetails.setObjectType(this.curDetails.getObjectType());
            EboxContext.mDetails.setShowUuid(this.curDetails.getShowUuid());
            showProgressBar();
            Utils.startService(this, EboxConst.ACTION_CREATEFOLDER, null, R.drawable.app_sync);
        }
    }

    /* access modifiers changed from: protected */
    public void startDelHanding() {
        if (hasWorking()) {
            showProgressBar();
            Utils.startService(this, EboxConst.ACTION_DELETE, null, R.drawable.app_sync);
        }
    }

    /* access modifiers changed from: protected */
    public void startReNameHanding() {
        if (hasWorking()) {
            showProgressBar();
            Utils.startService(this, EboxConst.ACTION_RENAME, null, R.drawable.app_sync);
        }
    }

    /* access modifiers changed from: protected */
    public void startSendEmailHanding() {
        if (!hasWorking()) {
            return;
        }
        if (EboxContext.mDetails.hasSuccess()) {
            sendEmail(EboxContext.mSystemInfo.getUserSdPath() + EboxContext.mDetails.getName());
            return;
        }
        showProgressBar();
        EboxContext.mDetails.setUpdown(8);
        Utils.startService(this, EboxConst.ACTION_SENDEMAIL, null, R.drawable.app_sync);
    }

    /* access modifiers changed from: protected */
    public void listFileEnd(boolean success) {
        if (success) {
            Utils.startService(this, EboxConst.ACTION_NORMAL, null, 0);
            startGetDBHanding();
            return;
        }
        httpFalse();
    }

    /* access modifiers changed from: protected */
    public void uploadEnd(boolean success) {
        EboxContext.mSystemInfo.setShareData(null);
        MainTabActivity.detailsAction = EboxConst.PROMPT_DEFAULT;
        if (success) {
            Utils.startService(this, EboxConst.ACTION_NORMAL, null, 0);
            Utils.MessageBox(this, getResources().getString(R.string.upload_success));
            HashMap<String, String> reHash = ActionDB.getThumbnail(this, EboxContext.mDetails.getPath(), EboxContext.mDetails.getIcon().intValue());
            if (reHash != null) {
                try {
                    EboxContext.mDetails.setThumbnail(reHash.get("thumbnail"));
                    EboxContext.mDetails.setLatitude(reHash.get("latitude"));
                    EboxContext.mDetails.setLongitude(reHash.get("longitude"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                EboxContext.mDetails.setThumbnail(Utils.getSuffix(EboxContext.mDetails.getName()) + EboxConst.SUFFIX);
            }
            EboxContext.mSystemNotify.setNotifyDownload(true);
            EboxContext.mSystemNotify.setNotifySystem(true);
            EboxContext.mDetails.setSuccess(true);
            insertDetailsView(EboxContext.mDetails, 6);
            return;
        }
        httpFalse();
    }

    /* access modifiers changed from: protected */
    public void downloadEnd(boolean success, boolean isSend) {
        if (success) {
            Utils.mediaStoreFileScan(this, new File(EboxContext.mDetails.getPath()));
            this.mHandler.postDelayed(this.runnableDownload, 2000);
            if (isSend) {
                sendEmail(EboxContext.mSystemInfo.getUserSdPath() + EboxContext.mDetails.getName());
            } else {
                Utils.MessageBox(this, getResources().getString(R.string.download_success));
            }
        } else {
            httpFalse();
        }
    }

    /* access modifiers changed from: protected */
    public void createDirEnd(boolean success) {
        if (success) {
            Utils.startService(this, EboxConst.ACTION_NORMAL, null, 0);
            Utils.MessageBox(this, getResources().getString(R.string.createdir_success));
            EboxContext.mDetails.setSuccess(false);
            insertDetailsView(EboxContext.mDetails, 10);
            return;
        }
        httpFalse();
    }

    /* access modifiers changed from: protected */
    public void delEnd(boolean success) {
        if (success) {
            delDetailsView(EboxContext.mDetails);
        } else {
            httpFalse();
        }
        EboxContext.mSystemNotify.setNotifyDownload(true);
        EboxContext.mSystemNotify.setNotifySystem(true);
    }

    /* access modifiers changed from: protected */
    public void reNameEnd(boolean success) {
        showProgressBar();
        if (success) {
            reNameDetailsView(EboxContext.mDetails);
        } else {
            httpFalse();
        }
    }

    /* access modifiers changed from: protected */
    public void setProgress(Intent intent) {
        int val = intent.getIntExtra("retval", 0);
        if (EboxContext.mDetails.getUpdown().intValue() == 8) {
            String uuid = EboxContext.mDetails.getShowUuid();
            Iterator i$ = this.arrList.iterator();
            while (i$.hasNext()) {
                Details d = i$.next();
                if (d.getShowUuid().equals(uuid)) {
                    d.setUpdown(EboxContext.mDetails.getUpdown());
                    if (val != -1 && d.getProgress().intValue() < val) {
                        d.setProgress(Integer.valueOf(val));
                    }
                }
            }
        } else {
            try {
                Details d2 = (Details) this.detailsAdapter.getItem(this.detailsAdapter.getCount() - 1);
                d2.setUpdown(EboxContext.mDetails.getUpdown());
                if (val != -1 && d2.getProgress().intValue() < val) {
                    d2.setProgress(Integer.valueOf(val));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.detailsAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void addDetailsView(Details details) {
        if (this.detailsAdapter == null) {
            shareDetailsView();
        }
        this.detailsAdapter.add(details);
        this.detailsAdapter.setSelectedId(this.detailsAdapter.getCount() - 1);
        EboxContext.mSystemInfo.setCurRowNum(this.detailsAdapter.getCount());
        getListView().requestFocus();
        getListView().setTranscriptMode(2);
        this.detailsAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void insertDetailsView(Details details, int flag) {
        if (!ActionDB.addToDetails(this, details, this.curDetails.getShowUuid())) {
            Utils.MessageBox(this, getResources().getString(R.string.db_error));
            return;
        }
        if (flag == 10) {
            int position = 0;
            Iterator i$ = this.arrList.iterator();
            while (i$.hasNext()) {
                position++;
                if (i$.next().getObjectType().endsWith(EboxConst.FILE)) {
                    break;
                }
            }
            if (position == 0) {
                EboxContext.mSystemInfo.setCurRowNum(position);
            } else {
                EboxContext.mSystemInfo.setCurRowNum(position - 1);
            }
        } else if (this.arrList.size() == 0) {
            EboxContext.mSystemInfo.setCurRowNum(EboxConst.SELFILE);
        } else {
            EboxContext.mSystemInfo.setCurRowNum(this.arrList.size());
        }
        startGetDBHanding();
    }

    /* access modifiers changed from: protected */
    public void delDetailsView(Details details) {
        Utils.startService(this, EboxConst.ACTION_NORMAL, null, 0);
        if (!ActionDB.delTODetails(this, details.getShowUuid())) {
            Utils.MessageBox(this, getResources().getString(R.string.db_error));
            return;
        }
        this.localList.remove(((Object) getTitle()) + "/" + details.getName());
        Utils.toDelFile(details.getName());
        ActionDB.deleteThumbnail(this, details.getName(), details.getIcon().intValue());
        startGetDBHanding();
    }

    /* access modifiers changed from: protected */
    public void reNameDetailsView(Details details) {
        Utils.startService(this, EboxConst.ACTION_NORMAL, null, 0);
        if (!ActionDB.reNameDetails(this, details)) {
            Utils.MessageBox(this, getResources().getString(R.string.db_error));
            return;
        }
        if (details.getIcon().intValue() == R.drawable.bt_folder) {
            String path = ((Object) getTitle()) + "/" + details.getMsg();
            List<String> local = new ArrayList<>();
            for (String tmp : this.localList) {
                if (!tmp.contains(path)) {
                    local.add(tmp);
                }
            }
            this.localList = local;
        } else {
            Utils.toReName(details.getMsg(), details.getName(), details.getIcon().intValue());
        }
        this.arrList.get(this.detailsAdapter.getSelectedId()).setName(details.getName());
        this.detailsAdapter.notifyDataSetChanged();
        EboxContext.mSystemNotify.setNotifyDownload(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void sendEmail(String path) {
        try {
            File file = new File(path);
            if (file.exists()) {
                Intent intent = new Intent(EboxConst.ACTION_GET_SEND);
                intent.putExtra(EboxConst.POINT_GET_SEND, true);
                intent.putExtra("android.intent.extra.EMAIL", EboxConst.TAB_UPLOADER_TAG);
                intent.putExtra("android.intent.extra.SUBJECT", getString(R.string.email_title));
                intent.putExtra("android.intent.extra.TEXT", getString(R.string.email_context));
                intent.setType("application/octet-stream");
                intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(file));
                startActivity(Intent.createChooser(intent, getTitle()));
            }
        } catch (Exception e) {
            Utils.MessageBox(this, getResources().getString(R.string.send_email_false));
        }
    }

    /* access modifiers changed from: protected */
    public void detailsView() {
    }

    /* access modifiers changed from: protected */
    public void shareDetailsView() {
    }

    /* access modifiers changed from: protected */
    public void forwardDir() {
    }
}
