package com.heyibao.android.ebox.model;

public class SystemNotify {
    private boolean notifyDownload = false;
    private boolean notifyListDetails = false;
    private boolean notifySetting;
    private boolean notifySystem = false;
    private boolean notifyThreadWait = false;

    public void setNotifyListDetails(boolean notifyListDetails2) {
        this.notifyListDetails = notifyListDetails2;
    }

    public boolean hasNotifyListDetails() {
        return this.notifyListDetails;
    }

    public void setNotifyDownload(boolean notifyDownload2) {
        this.notifyDownload = notifyDownload2;
    }

    public boolean hasNotifyDownload() {
        return this.notifyDownload;
    }

    public void setNotifySystem(boolean notifySystem2) {
        this.notifySystem = notifySystem2;
    }

    public boolean hasNotifySystem() {
        return this.notifySystem;
    }

    public void setNotifySetting(boolean notifySetting2) {
        this.notifySetting = notifySetting2;
    }

    public boolean hasNotifySetting() {
        return this.notifySetting;
    }

    public void setNotifyThreadWait(boolean notifyThreadWait2) {
        this.notifyThreadWait = notifyThreadWait2;
    }

    public boolean hasNotifyThreadWait() {
        return this.notifyThreadWait;
    }
}
