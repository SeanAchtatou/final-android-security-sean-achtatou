package com.heyibao.android.ebox.http;

import android.content.Context;
import android.util.Log;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.util.Utils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;

public abstract class EboxDownload implements DownloadInterface {
    public EboxDownload(Context context, String url, File file) {
        int bufferLength;
        Log.d(EboxDownload.class.getSimpleName(), "## start startDownLoad url: " + url + " name: " + file);
        Log.d(DownloaderBuilder.class.getSimpleName(), "## start startDown ");
        long startTime = new Date().getTime();
        boolean success = false;
        Utils.isExitParent(file);
        GPBUtils gPBUtils = new GPBUtils(url);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            Thread.sleep(10);
            InputStream inputStream = gPBUtils.getInputResult();
            int totalSize = gPBUtils.getConnLength();
            int downloadedSize = 0;
            byte[] buffer = new byte[102400];
            String content = Utils.replace(context.getString(R.string.download_info), Utils.getSize(Double.valueOf((double) totalSize)));
            while (!Thread.interrupted() && (bufferLength = inputStream.read(buffer)) > 0) {
                while (EboxContext.mSystemNotify.hasNotifyThreadWait()) {
                    Thread.sleep(1000);
                }
                fileOutputStream.write(buffer, 0, bufferLength);
                downloadedSize += bufferLength;
                long i = (new Date().getTime() - startTime) / 1000;
                float speed = i > 0 ? (float) (((long) downloadedSize) / (1024 * i)) : 40.0f;
                if (url.equals(EboxContext.mUpGrade.getPath())) {
                    EboxContext.message = content + Utils.replace(context.getString(R.string.speed_info), (double) speed);
                }
                reProgress(downloadedSize, totalSize);
                Thread.sleep(50);
            }
            fileOutputStream.close();
            inputStream.close();
            if (gPBUtils.getResponseCode() == 200) {
                success = true;
                EboxContext.mDetails.setPath(file.getPath());
                EboxContext.message = context.getString(R.string.download_suc);
                reProgress(0, 0);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            Utils.toDelFile(file.getPath());
            Log.e(EboxDownload.class.getSimpleName(), "## Download getUrlThread is Force stop");
            EboxContext.message = context.getString(R.string.stop_thread);
        } catch (Exception e2) {
            Exception e3 = e2;
            e3.printStackTrace();
            Log.e(EboxDownload.class.getSimpleName(), "## Download error for : " + e3.getMessage());
            EboxContext.message = context.getResources().getString(R.string.download_info_4) + "-2";
        }
        gPBUtils.close();
        reSuccess(success);
    }
}
