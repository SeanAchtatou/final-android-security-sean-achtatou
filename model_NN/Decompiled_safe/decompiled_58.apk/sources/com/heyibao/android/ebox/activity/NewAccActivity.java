package com.heyibao.android.ebox.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.db.ActionDB;
import com.heyibao.android.ebox.model.ImageBt;
import com.heyibao.android.ebox.model.MyDialog;
import com.heyibao.android.ebox.util.Utils;
import java.io.File;
import java.util.Date;
import java.util.HashMap;

public class NewAccActivity extends Activity implements View.OnClickListener {
    private int action = -1;
    /* access modifiers changed from: private */
    public boolean downSuccess = false;
    protected Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            Intent intent = (Intent) msg.obj;
            boolean success = intent.getBooleanExtra("success", false);
            switch (msg.what) {
                case 2:
                    NewAccActivity.this.checkEnd(success);
                    return;
                case 3:
                    NewAccActivity.this.downUpGrade(success, intent);
                    return;
                case 4:
                    NewAccActivity.this.loginEnd(success);
                    return;
                case 14:
                    NewAccActivity.this.offLineLogin();
                    return;
                case 15:
                    Log.e(NewAccActivity.class.getSimpleName(), "## broadcast : " + success);
                    Log.e(NewAccActivity.class.getSimpleName(), "## broadcast : " + EboxContext.message);
                    NewAccActivity.this.registEnd(success);
                    return;
                default:
                    return;
            }
        }
    };
    private PowerManager.WakeLock mWakeLock;
    private ScrollView mainLayout;
    private MyDialog proDialog;
    private NewReceiver receiver;

    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(null);
        if (getResources().getConfiguration().orientation == 2) {
            this.mainLayout.setBackgroundResource(R.drawable.bg_guide_land);
        } else if (getResources().getConfiguration().orientation == 1) {
            this.mainLayout.setBackgroundResource(R.drawable.bg_guide);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.guide);
        Log.d(NewAccActivity.class.getSimpleName(), "## create NewAccActivity now");
        newAccInit();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.receiver = new NewReceiver();
        registerReceiver(this.receiver, new IntentFilter(EboxConst.EBOXBROAD), null, this.mHandler);
        if (this.mWakeLock == null) {
            this.mWakeLock = ((PowerManager) getSystemService("power")).newWakeLock(10, EboxConst.USER_AGENT);
        }
        this.mWakeLock.acquire();
        onConfigurationChanged(getResources().getConfiguration());
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        unregisterReceiver(this.receiver);
        if (this.mWakeLock != null) {
            this.mWakeLock.release();
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                MyDialog mDialog = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.bt_ok:
                                ActionDB.atFirst(NewAccActivity.this);
                                NewAccActivity.this.checkUpGrade();
                                dismiss();
                                return;
                            case R.id.bt_cancel:
                                NewAccActivity.this.showDialog(15);
                                return;
                            default:
                                return;
                        }
                    }
                };
                mDialog.setDefaultView();
                mDialog.setMyTitle(getResources().getString(R.string.law_title));
                mDialog.setMessage(getResources().getString(R.string.law_context));
                mDialog.setCancelable(false);
                mDialog.setCanceledOnTouchOutside(false);
                addShortcut();
                return mDialog;
            case 2:
                MyDialog mDialog2 = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.bt_ok:
                                NewAccActivity.this.showDialog(3);
                                Utils.startService(NewAccActivity.this, EboxConst.ACTION_DOWNUPDATE, null, R.drawable.app_unnet);
                                dismiss();
                                return;
                            case R.id.bt_cancel:
                                NewAccActivity.this.showDialog(15);
                                return;
                            default:
                                return;
                        }
                    }
                };
                mDialog2.setDefaultView();
                mDialog2.setMessage(Utils.replace(getResources().getString(R.string.upgrade_info), EboxContext.mUpGrade.getVersion()));
                mDialog2.setCancelable(false);
                mDialog2.setCanceledOnTouchOutside(false);
                return mDialog2;
            case 3:
                return proDialog();
            case 9:
                MyDialog mDialog3 = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.bt_ok:
                                NewAccActivity.this.loginOrRegist(2);
                                dismiss();
                                return;
                            case R.id.bt_cancel:
                                NewAccActivity.this.mHandler.sendMessage(NewAccActivity.this.mHandler.obtainMessage(14, new Intent()));
                                dismiss();
                                return;
                            default:
                                return;
                        }
                    }
                };
                mDialog3.setDefaultView();
                mDialog3.setMessage(getResources().getString(R.string.offline_login_ver));
                return mDialog3;
            case 13:
                MyDialog mDialog4 = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                        NewAccActivity.this.checkUpGrade();
                        dismiss();
                    }
                };
                mDialog4.setDefaultView();
                mDialog4.setMyTitle(getResources().getString(R.string.upgrade_title));
                mDialog4.setMessage(getResources().getString(R.string.upgrade_context));
                return mDialog4;
            case 15:
                return new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.bt_ok:
                                Utils.exitAppEnd(NewAccActivity.this);
                                return;
                            case R.id.bt_cancel:
                                cancel();
                                return;
                            default:
                                return;
                        }
                    }
                };
            default:
                return null;
        }
    }

    private MyDialog proDialog() {
        if (this.proDialog == null) {
            this.proDialog = new MyDialog(this, R.style.dialog) {
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.bt_ok:
                            if (NewAccActivity.this.downSuccess) {
                                Utils.installAPK(NewAccActivity.this);
                                return;
                            } else {
                                Utils.MessageBox(NewAccActivity.this, NewAccActivity.this.getString(R.string.download_ing));
                                return;
                            }
                        case R.id.bt_cancel:
                            NewAccActivity.this.showDialog(15);
                            return;
                        default:
                            return;
                    }
                }
            };
            this.proDialog.setProgressView();
            this.proDialog.setMessage(getResources().getString(R.string.loading));
            this.proDialog.setCancelable(false);
            this.proDialog.setCanceledOnTouchOutside(false);
        }
        return this.proDialog;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void addShortcut() {
        Intent shortcutIntent = new Intent(EboxConst.ACTION_INSTALL_SHORTCUT);
        shortcutIntent.putExtra("android.intent.extra.shortcut.NAME", getString(R.string.app_name));
        shortcutIntent.putExtra("duplicate", false);
        Intent intent2 = new Intent("android.intent.action.MAIN");
        intent2.addCategory("android.intent.category.LAUNCHER");
        intent2.setComponent(new ComponentName(getPackageName(), getPackageName() + ".SplashScreen"));
        shortcutIntent.putExtra("android.intent.extra.shortcut.INTENT", intent2);
        shortcutIntent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this, R.drawable.app_icon_64));
        sendBroadcast(shortcutIntent);
    }

    /* access modifiers changed from: protected */
    public void newAccInit() {
        this.mainLayout = (ScrollView) findViewById(R.id.auto_panel);
        EboxContext.curActivity = this;
        Utils.startService(this, EboxConst.ACTION_NORMAL, null, R.drawable.app_unnet);
        EboxContext.mSystemInfo.setSdcard(Utils.checkSDCard());
        EboxContext.mSystemInfo.setNet(Utils.checkNetWork(this));
        ActionDB.getConfigs(this);
        if (EboxContext.mSystemInfo.hasFirst()) {
            showDialog(1);
        } else if (EboxContext.mSystemInfo.hasUpdate()) {
            showDialog(13);
        } else {
            showOrHideLoading(true);
            checkUpGrade();
        }
    }

    /* access modifiers changed from: private */
    public void checkUpGrade() {
        if (EboxContext.mSystemInfo.hasNet()) {
            Log.d(NewAccActivity.class.getSimpleName(), "## nofiy upgrading to service ");
            Utils.startService(this, EboxConst.ACTION_AUTOUPDATE, null, R.drawable.app_unnet);
            return;
        }
        EboxContext.mUpGrade.setVersion(getResources().getString(R.string.version));
        loginInit();
    }

    private void loginInit() {
        HashMap<String, Object> accounts = ActionDB.getAccounts(this);
        if (accounts != null) {
            EboxContext.mDevice.setTeamName(accounts.get("teamName").toString());
            EboxContext.mDevice.setUserName(accounts.get("userName").toString());
            EboxContext.mDevice.setPassWord(accounts.get("passWord").toString());
            EboxContext.mDevice.setDeviceName(accounts.get("deviceName").toString());
            EboxContext.mDevice.setDeviceInfo(accounts.get("deviceInfo").toString());
            EboxContext.mDevice.setNick(accounts.get("nick").toString());
            EboxContext.mDevice.setCode(2);
            if (!EboxContext.mSystemInfo.hasAutoLogin()) {
                Intent i = new Intent(this, LoginActivity.class);
                i.putExtra("flag", 4);
                startActivityForResult(i, 4);
            } else if (!EboxContext.mSystemInfo.hasNet()) {
                offLineLogin();
            } else if (EboxContext.mSystemInfo.hasOffLine()) {
                showDialog(9);
            } else {
                loginOrRegist(2);
            }
        } else {
            regButtonInit();
        }
    }

    /* access modifiers changed from: protected */
    public void requsetLogin(int flag) {
        Intent i = new Intent(this, LoginActivity.class);
        i.putExtra("flag", flag);
        i.putExtra("user", EboxContext.mDevice);
        startActivityForResult(i, flag);
    }

    /* access modifiers changed from: protected */
    public void regButtonInit() {
        EboxContext.mDevice.reSet();
        EboxContext.mSystemInfo.reSet();
        EboxContext.mSystemInfo.setLogin(true);
        showOrHideLoading(false);
        ImageBt registButton = (ImageBt) findViewById(R.id.regest);
        registButton.setVisibility(0);
        registButton.setTextViewText(getString(R.string.registration));
        registButton.setImageResource(R.drawable.yun_cai_72);
        registButton.setOnClickListener(this);
        ImageBt accountButton = (ImageBt) findViewById(R.id.comlogin);
        accountButton.setVisibility(0);
        accountButton.setTextViewText(getString(R.string.account_to_ebox));
        accountButton.setImageResource(R.drawable.qi_ye_jian_zhu_72);
        accountButton.setOnClickListener(this);
        ImageBt phoneAndEmailButton = (ImageBt) findViewById(R.id.usrlogin);
        phoneAndEmailButton.setVisibility(0);
        phoneAndEmailButton.setTextViewText(getString(R.string.phone_email_ebox));
        phoneAndEmailButton.setImageResource(R.drawable.you_jian_dian_hua_72);
        phoneAndEmailButton.setOnClickListener(this);
    }

    /* access modifiers changed from: private */
    public void checkEnd(boolean success) {
        showOrHideLoading(false);
        if (!success) {
            Log.d(NewAccActivity.class.getSimpleName(), "## no need upgrade");
            loginInit();
        } else if (!EboxContext.mSystemInfo.hasSdcard()) {
            Log.d(NewAccActivity.class.getSimpleName(), "## can't upgrade no sdcard");
            Utils.MessageBox(this, getString(R.string.upgrade_nosdcard));
            loginInit();
        } else if (EboxContext.mSystemInfo.hasAutoUpgrade()) {
            showDialog(2);
        } else {
            Utils.MessageBox(this, Utils.replace(getString(R.string.upgrade_noauto), EboxContext.mUpGrade.getVersion()));
            loginInit();
        }
    }

    /* access modifiers changed from: private */
    public void downUpGrade(boolean success, Intent intent) {
        if (success) {
            this.downSuccess = success;
            this.proDialog.setMessage(getResources().getString(R.string.download_end));
            return;
        }
        int val = intent.getIntExtra("retval", 0);
        double total = intent.getDoubleExtra("total", -1.0d);
        if (total == -1.0d) {
            removeDialog(3);
            Utils.MessageBox(this, getResources().getString(R.string.download_false));
        }
        if (EboxContext.message != null) {
            this.proDialog.setMessage(EboxContext.message);
        }
        if (val != 0 && val > this.proDialog.progress.getProgress()) {
            this.proDialog.setProgress((int) (((double) (val * 100)) / total));
        }
        Log.d(NewAccActivity.class.getSimpleName(), "## programs : " + val);
    }

    /* access modifiers changed from: protected */
    public void offLineLogin() {
        showOrHideLoading(false);
        EboxContext.mSystemInfo.setNet(false);
        EboxContext.mSystemInfo.setLogin(false);
        EboxContext.mSystemInfo.setRefresh(false);
        accountSuccess();
    }

    /* access modifiers changed from: protected */
    public void loginOrRegist(int flag) {
        if (!Utils.hasNet(this)) {
            this.action = flag;
            ((ImageBt) findViewById(R.id.regest)).setVisibility(8);
            ((ImageBt) findViewById(R.id.comlogin)).setVisibility(8);
            ((ImageBt) findViewById(R.id.usrlogin)).setVisibility(8);
            showOrHideLoading(true);
            if (EboxContext.mDevice.getCode().intValue() == 1) {
                Utils.startService(this, EboxConst.ACTION_REGIST, null, R.drawable.app_sync);
            } else {
                Utils.startService(this, EboxConst.ACTION_LOGIN, null, R.drawable.app_sync);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void showOrHideLoading(boolean is) {
        ProgressBar pb = (ProgressBar) findViewById(R.id.loadprogress);
        if (is) {
            pb.setVisibility(0);
        } else {
            pb.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void registEnd(boolean success) {
        showOrHideLoading(false);
        Utils.startService(this, EboxConst.ACTION_NORMAL, null, R.drawable.app_unnet);
        if (success) {
            Utils.MessageBox(this, getString(R.string.regist_success));
            regButtonInit();
            return;
        }
        requsetLogin(this.action);
    }

    /* access modifiers changed from: protected */
    public void loginEnd(boolean _success) {
        showOrHideLoading(false);
        if (EboxContext.message != null && EboxContext.message.equals(getString(R.string.net_error)) && !EboxContext.mSystemInfo.hasLogin()) {
            offLineLogin();
        } else if (_success) {
            accountSuccess();
        } else {
            Utils.startService(this, EboxConst.ACTION_NORMAL, null, R.drawable.app_unnet);
            accountFalse();
        }
    }

    /* access modifiers changed from: protected */
    public void accountFalse() {
        requsetLogin(this.action);
    }

    /* access modifiers changed from: protected */
    public void logOut() {
        Utils.reSetSystem(this);
        regButtonInit();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heyibao.android.ebox.db.ActionDB.updateConfigs(android.app.Activity, boolean, boolean):boolean
     arg types: [com.heyibao.android.ebox.activity.NewAccActivity, boolean, boolean]
     candidates:
      com.heyibao.android.ebox.db.ActionDB.updateConfigs(android.app.Activity, long, boolean):boolean
      com.heyibao.android.ebox.db.ActionDB.updateConfigs(android.app.Activity, boolean[], int):boolean
      com.heyibao.android.ebox.db.ActionDB.updateConfigs(android.app.Activity, boolean, boolean):boolean */
    /* access modifiers changed from: protected */
    public void accountSuccess() {
        EboxContext.message = null;
        EboxContext.mSystemInfo.setUserSdPath(EboxContext.mSystemInfo.getSdPath() + EboxContext.mDevice.getNick() + File.separator);
        Utils.isExitParent(EboxContext.mSystemInfo.getUserSdPath() + EboxConst.CATCHICON);
        if (EboxContext.mSystemInfo.hasNet()) {
            ActionDB.updateConfigs(this, new Date().getTime() / 1000, EboxContext.mSystemInfo.hasAutoLogin());
            ActionDB.addAccounts(this, EboxContext.mDevice);
        } else {
            ActionDB.updateConfigs((Activity) this, EboxContext.mSystemInfo.hasAutoLogin(), EboxContext.mSystemInfo.hasOffLine());
        }
        Intent viewIntent = new Intent(this, MainTabActivity.class);
        if (!EboxContext.mSystemInfo.hasLogin()) {
            EboxContext.mDevice.setSuccess(true);
            startActivity(viewIntent);
            overridePendingTransition(R.anim.fade, R.anim.hold);
            finish();
        } else if (ActionDB.addAccounts(this, EboxContext.mDevice)) {
            ActionDB.reSetDetails(this);
            startActivity(viewIntent);
            overridePendingTransition(R.anim.fade, R.anim.hold);
            finish();
        } else {
            accountFalse();
        }
    }

    public class NewReceiver extends BroadcastReceiver {
        public NewReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("action");
            Log.d(NewAccActivity.class.getSimpleName(), "## onReceive : " + action);
            if (EboxConst.ACTION_AUTOUPDATE.equals(action)) {
                NewAccActivity.this.mHandler.sendMessage(NewAccActivity.this.mHandler.obtainMessage(2, intent));
            } else if (EboxConst.ACTION_DOWNUPDATE.equals(action)) {
                NewAccActivity.this.mHandler.sendMessage(NewAccActivity.this.mHandler.obtainMessage(3, intent));
            } else if (EboxConst.ACTION_LOGIN.equals(action)) {
                NewAccActivity.this.mHandler.sendMessage(NewAccActivity.this.mHandler.obtainMessage(4, intent));
            } else if (EboxConst.ACTION_REGIST.equals(action)) {
                NewAccActivity.this.mHandler.sendMessage(NewAccActivity.this.mHandler.obtainMessage(15, intent));
            }
        }
    }

    public void onClick(View v) {
        Intent i = new Intent(this, LoginActivity.class);
        switch (v.getId()) {
            case R.id.regest:
                i.putExtra("flag", 1);
                startActivityForResult(i, 1);
                return;
            case R.id.usrlogin:
                i.putExtra("flag", 3);
                startActivityForResult(i, 3);
                return;
            case R.id.comlogin:
                i.putExtra("flag", 2);
                startActivityForResult(i, 2);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(NewAccActivity.class.getSimpleName(), "## onActivityResult flag : " + resultCode);
        if (resultCode == 1) {
            loginOrRegist(1);
        } else if (resultCode == 2 || resultCode == 3) {
            loginOrRegist(resultCode);
        } else if (resultCode == 4) {
            if (!EboxContext.mSystemInfo.hasOffLine()) {
                loginOrRegist(4);
            } else {
                offLineLogin();
            }
        } else if (resultCode == 0) {
            logOut();
        } else {
            Utils.exitAppEnd(this);
        }
    }

    public boolean onKeyDown(int i, KeyEvent event) {
        if (i != 4) {
            return false;
        }
        showDialog(15);
        return false;
    }
}
