package com.heyibao.android.ebox.db;

import android.app.Activity;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.activity.NewAccActivity;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.model.Details;
import com.heyibao.android.ebox.model.DeviceInfo;
import com.heyibao.android.ebox.model.MyContacts;
import com.heyibao.android.ebox.model.SpaceObject;
import com.heyibao.android.ebox.util.Utils;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

public class ActionDB {
    public static void atFirst(Activity activity) {
        try {
            EboxDB.getInstance(activity).atFirst(activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean reSetAccounts(Context activity) {
        try {
            EboxDB.getInstance(activity).reSetAccounts(activity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean reSetContacts(Context activity) {
        try {
            EboxDB.getInstance(activity).reSetContacts(activity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean reSetCongfigs(Context activity) {
        try {
            EboxDB.getInstance(activity).reSetConfigs(activity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean reSetDetails(Context activity) {
        try {
            EboxDB.getInstance(activity).reSetDetails(activity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean reSetLocations(Context activity) {
        try {
            EboxDB.getInstance(activity).reSetLocatoins(activity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean addAccounts(Activity activity, DeviceInfo devInfo) {
        try {
            return EboxDB.getInstance(activity).addAccount(activity, devInfo.getDeviceName(), devInfo.getDeviceInfo(), devInfo.getTeamName(), devInfo.getUserName(), devInfo.getPassWord(), devInfo.getNick());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static HashMap<String, Object> getAccounts(Activity activity) {
        return EboxDB.getInstance(activity).getAccount(activity);
    }

    public static boolean updateUserInfos(Activity activity, SpaceObject spaceObject) {
        try {
            return EboxDB.getInstance(activity).updateUserInfos(activity, spaceObject.getPhone(), spaceObject.getEmail(), spaceObject.getCurrentSize(), spaceObject.getSpaceSize(), spaceObject.getIsAdmin());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static HashMap<String, Object> getUserInfos(Activity activity) {
        return EboxDB.getInstance(activity).getUserInfos(activity);
    }

    /* JADX INFO: Multiple debug info for r0v1 com.heyibao.android.ebox.db.EboxDB: [D('success' boolean), D('settingDB' com.heyibao.android.ebox.db.EboxDB)] */
    /* JADX INFO: Multiple debug info for r11v3 boolean: [D('activity' android.app.Activity), D('success' boolean)] */
    public static boolean updateConfigs(Activity activity, long time, boolean autoLogin) {
        try {
            return EboxDB.getInstance(activity).updateConfigs(activity, time, autoLogin ? 1 : 0, 0, -1, -1, -1, null, null);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean updateContactsConfigs(Context activity, String time) {
        try {
            return EboxDB.getInstance(activity).updateConfigs(activity, 0, -1, -1, -1, -1, -1, time, null);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean updateSignConfigs(Context activity, String time) {
        try {
            return EboxDB.getInstance(activity).updateConfigs(activity, 0, -1, -1, -1, -1, -1, null, time);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean updateConfigs(Activity activity, boolean autoLogin, boolean offLine) {
        int i;
        int i2 = 1;
        try {
            EboxDB settingDB = EboxDB.getInstance(activity);
            if (autoLogin) {
                i = 1;
            } else {
                i = 0;
            }
            if (!offLine) {
                i2 = 0;
            }
            return settingDB.updateConfigs(activity, 0, i, i2, -1, -1, -1, null, null);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean updateConfigs(Activity activity, boolean[] bool, int fresh) {
        int i;
        int i2 = 0;
        try {
            EboxDB settingDB = EboxDB.getInstance(activity);
            if (bool[0]) {
                i = 1;
            } else {
                i = 0;
            }
            int i3 = bool[1] ? 1 : 0;
            int i4 = bool[2] ? 1 : 0;
            if (bool[3]) {
                i2 = 1;
            }
            return settingDB.updateConfigs(activity, 0, i, i3, i4, i2, fresh, null, null);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /* JADX INFO: Multiple debug info for r0v1 java.util.HashMap<java.lang.String, java.lang.Object>: [D('settingDB' com.heyibao.android.ebox.db.EboxDB), D('configs' java.util.HashMap<java.lang.String, java.lang.Object>)] */
    public static void getConfigs(Context activity) {
        HashMap<String, Object> configs = EboxDB.getInstance(activity).getConfig(activity);
        if (configs == null) {
            Log.d(NewAccActivity.class.getSimpleName(), "## db is error config is null ");
            reSetCongfigs(activity);
            return;
        }
        if (((Long) configs.get("last_time")).longValue() >= (new Date().getTime() / 1000) - 86400) {
            EboxContext.mSystemInfo.setRefresh(false);
        }
        if (((Integer) configs.get("auto_update")).intValue() == 0) {
            EboxContext.mSystemInfo.setAutoUpgrade(false);
        }
        if (((Integer) configs.get("auto_login")).intValue() == 0) {
            EboxContext.mSystemInfo.setAutoLogin(false);
        }
        if (((Integer) configs.get("offline_login")).intValue() == 1) {
            EboxContext.mSystemInfo.setOffLine(true);
        }
        if (((Integer) configs.get("notify_info")).intValue() == 0) {
            EboxContext.mSystemInfo.setNotify(false);
        }
        EboxContext.mSystemInfo.setOptionRefre(((Integer) configs.get("update_freq")).intValue());
        EboxContext.mSystemInfo.setSyncContactsTime((String) configs.get("sync_contacts_time"));
        EboxContext.mSystemInfo.setSignTime((String) configs.get("sign_time"));
    }

    public static ArrayList<Details> getFileListByParentUUID(Activity activity, String parentUUID) {
        ArrayList<Details> lf = new ArrayList<>();
        try {
            EboxDB settingDB = EboxDB.getInstance(activity);
            new Vector();
            Vector<HashMap<String, Object>> details = settingDB.getDetails(activity, parentUUID);
            if (details != null) {
                for (int i = 0; i < details.size(); i++) {
                    Details Detail = new Details();
                    HashMap<String, Object> curHash = details.get(i);
                    Detail.setName(curHash.get("name").toString());
                    Detail.setSize(Double.valueOf(curHash.get("size").toString()).doubleValue());
                    Detail.setCreateTime((Integer) curHash.get("createTime"));
                    Detail.setModifyTime((Integer) curHash.get("modityTime"));
                    Detail.setObjectType(curHash.get("objectType").toString());
                    Detail.setShowUuid(curHash.get("objectUUID").toString());
                    Detail.setParentShowUuid(curHash.get("parentUUID").toString());
                    Detail.setSignature(curHash.get("signature").toString());
                    Detail.setSuccess(((Integer) curHash.get("exist")).intValue() == 1);
                    Detail.setIcon((Integer) curHash.get("image"));
                    if (curHash.get("thumbnail") != null) {
                        Detail.setThumbnail(curHash.get("thumbnail").toString());
                    }
                    if (curHash.get("latitude") != null) {
                        Detail.setLatitude(curHash.get("latitude").toString());
                    }
                    if (curHash.get("longitude") != null) {
                        Detail.setLongitude(curHash.get("longitude").toString());
                    }
                    lf.add(Detail);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lf;
    }

    public static boolean updateToDetails(Service activity, Vector<HashMap<String, Object>> detailVectors, String parentUUID) {
        EboxDB settingDB = EboxDB.getInstance(activity);
        if (detailVectors == null || detailVectors.size() > 0) {
            parentUUID = null;
        }
        return settingDB.updateDetails(activity, detailVectors, parentUUID);
    }

    public static boolean updateOneDetail(Activity activity, Details inDetails) {
        boolean success = false;
        if (inDetails == null) {
            return false;
        }
        try {
            success = EboxDB.getInstance(activity).updateDetail(activity, inDetails.getShowUuid(), inDetails.hasSuccess() ? 1 : 0, inDetails.getLatitude(), inDetails.getLongitude(), inDetails.getThumbnail());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    /* JADX INFO: Multiple debug info for r17v1 boolean: [D('inDetails' com.heyibao.android.ebox.model.Details), D('success' int)] */
    public static boolean addToDetails(Activity activity, Details inDetails, String _parentUUID) {
        boolean success;
        if (inDetails == null) {
            return false;
        }
        try {
            success = EboxDB.getInstance(activity).addDetails(activity, inDetails.getName(), (int) inDetails.getSize(), inDetails.getCreateTime().intValue(), inDetails.getModifyTime().intValue(), inDetails.getObjectType(), inDetails.getShowUuid(), _parentUUID, inDetails.getSignature(), inDetails.getIcon().intValue(), inDetails.hasSuccess() ? 1 : 0, inDetails.getLatitude(), inDetails.getLongitude(), inDetails.getThumbnail());
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }

    public static boolean delTODetails(Activity activity, String objectUUID) {
        try {
            EboxDB.getInstance(activity).delDetails(activity, objectUUID);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean reNameDetails(Activity activity, Details inDetails) {
        try {
            return EboxDB.getInstance(activity).reNameDetails(activity, inDetails.getShowUuid(), inDetails.getName(), inDetails.getModifyTime().intValue());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean queryFileExist(Activity activity, String name, String parentUUID) {
        if (name == null || name.equals(EboxConst.TAB_UPLOADER_TAG)) {
            return false;
        }
        HashMap<String, Object> detailHash = null;
        try {
            detailHash = EboxDB.getInstance(activity).queryDetailsByName(activity, name, parentUUID);
        } catch (Exception e) {
        }
        return detailHash == null;
    }

    public static Details queryDetailsByName(Activity activity, String name) {
        Details d = new Details();
        if (name == null || name.equals(EboxConst.TAB_UPLOADER_TAG)) {
            return null;
        }
        try {
            HashMap<String, Object> curHash = EboxDB.getInstance(activity).queryDetailsByName(activity, name, null);
            if (curHash == null) {
                return null;
            }
            d.setName(curHash.get("name").toString());
            d.setShowUuid(curHash.get("objectUUID").toString());
            d.setParentShowUuid(curHash.get("parentUUID").toString());
            d.setObjectType(curHash.get("objectType").toString());
            d.setSize(Double.valueOf(curHash.get("size").toString()).doubleValue());
            d.setModifyTime((Integer) curHash.get("modityTime"));
            d.setSignature(curHash.get("signature").toString());
            d.setSuccess(((Integer) curHash.get("exist")).intValue() == 1);
            d.setIcon((Integer) curHash.get("image"));
            if (curHash.get("thumbnail") != null) {
                d.setThumbnail(curHash.get("thumbnail").toString());
            }
            if (curHash.get("latitude") != null) {
                d.setLatitude(curHash.get("latitude").toString());
            }
            if (curHash.get("longitude") != null) {
                d.setLongitude(curHash.get("longitude").toString());
            }
            return d;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Details getShareDetails(Activity activity, Details details, int find) {
        String shareUUID;
        String type = details.getObjectType();
        String fType = type.substring(0, find + 1);
        if (fType.equals(type)) {
            shareUUID = details.getShowUuid();
        } else {
            String query = EboxDB.getInstance(activity).queryShareUUIDByType(activity, details.getParentShowUuid(), fType);
            if (query == null) {
                return null;
            }
            shareUUID = query;
        }
        details.setObjectType(type.substring(type.length() - 1));
        details.setShareUuid(shareUUID);
        return details;
    }

    public static HashMap<String, Object> getThumbnail(Activity activity, Uri uri) {
        String[] projection;
        Log.d(ActionDB.class.getSimpleName(), "get Thumbnails getMediaStore");
        if (uri == null) {
            return null;
        }
        HashMap<String, Object> thisHash = new HashMap<>();
        String path = null;
        if (uri.getEncodedPath().contains(EboxConst.MEDIA_IMAGE)) {
            projection = new String[]{"_id", "_data"};
            thisHash.put(EboxConst.CATCHICON, Integer.valueOf((int) R.drawable.bt_jpg));
        } else if (uri.getEncodedPath().contains(EboxConst.MEDIA_VIDEO)) {
            projection = new String[]{"_id", "_data"};
            thisHash.put(EboxConst.CATCHICON, Integer.valueOf((int) R.drawable.bt_avi));
        } else {
            projection = new String[]{"_id", "_data"};
            thisHash.put(EboxConst.CATCHICON, Integer.valueOf((int) R.drawable.text));
        }
        try {
            Cursor cur = activity.managedQuery(uri, projection, null, null, null);
            if (cur != null) {
                if (cur.moveToFirst()) {
                    path = cur.getString(cur.getColumnIndex(projection[1]));
                }
                cur.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (path == null) {
            path = uri.toString().replace("file://", EboxConst.TAB_UPLOADER_TAG);
            thisHash.put(EboxConst.CATCHICON, Integer.valueOf(Utils.getDetailsIcon(activity, path)));
        }
        File f = new File(path);
        if (!f.exists()) {
            return null;
        }
        thisHash.put("size", Double.valueOf((double) f.length()));
        thisHash.put("name", f.getName());
        thisHash.put("path", path);
        Log.d(ActionDB.class.getSimpleName(), "get Thumbnails getMediaStore right end");
        return thisHash;
    }

    public static HashMap<String, String> getThumbnail(Activity activity, String path, int icon) {
        Uri uri;
        String whereClause;
        String[] projection;
        Log.d(ActionDB.class.getSimpleName(), "get Thumbnails getMediaStore");
        if (icon == R.drawable.bt_jpg) {
            uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            whereClause = "_data = '" + path + "'";
            projection = new String[]{"_id", "latitude", "longitude"};
        } else if (icon != R.drawable.bt_avi) {
            return null;
        } else {
            uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            whereClause = "_data = '" + path + "'";
            projection = new String[]{"_id", "latitude", "longitude"};
        }
        Cursor cursor = EboxContext.curActivity.getContentResolver().query(uri, projection, whereClause, null, null);
        if (cursor == null || cursor.getCount() == 0) {
            Log.d(ActionDB.class.getSimpleName(), "out");
            return null;
        }
        cursor.moveToFirst();
        String thumbId = cursor.getString(cursor.getColumnIndex(projection[0]));
        if (thumbId == null) {
            Log.d(ActionDB.class.getSimpleName(), "no MediaInfo ");
            return null;
        }
        String latitude = cursor.getString(cursor.getColumnIndex(projection[1]));
        String longitude = cursor.getString(cursor.getColumnIndex(projection[2]));
        HashMap<String, String> thisHash = new HashMap<>();
        Log.d(ActionDB.class.getSimpleName(), "thumbId = " + thumbId);
        thisHash.put("thumbnail", thumbId);
        if (latitude != null) {
            Log.d(ActionDB.class.getSimpleName(), "latitude = " + latitude);
            thisHash.put("latitude", latitude);
        }
        if (longitude != null) {
            Log.d(ActionDB.class.getSimpleName(), "longitude = " + longitude);
            thisHash.put("longitude", longitude);
        }
        cursor.close();
        return thisHash;
    }

    public static void updateThumbnail(Activity activity, String oldName, String name, int icon) {
        Uri uri;
        String whereClause;
        String path = EboxContext.mSystemInfo.getUserSdPath() + name;
        String oldPath = EboxContext.mSystemInfo.getUserSdPath() + oldName;
        Log.d(ActionDB.class.getSimpleName(), "get getMediaStore oldpath " + oldPath);
        Log.d(ActionDB.class.getSimpleName(), "get getMediaStore path " + path);
        ContentValues values = new ContentValues();
        if (icon == R.drawable.bt_jpg) {
            uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            whereClause = "_data = '" + oldPath + "'";
            values.put("_data", path);
        } else if (icon == R.drawable.bt_avi) {
            uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            whereClause = "_data = '" + oldPath + "'";
            values.put("_data", path);
        } else if (icon == R.drawable.bt_mp3) {
            uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            whereClause = "_data = '" + oldPath + "'";
            values.put("_data", path);
        } else {
            return;
        }
        activity.getContentResolver().update(uri, values, whereClause, null);
    }

    public static void deleteThumbnail(Activity activity, String name, int icon) {
        Uri uri;
        String whereClause;
        String path = EboxContext.mSystemInfo.getUserSdPath() + name;
        Log.d(ActionDB.class.getSimpleName(), "get getMediaStore path " + path);
        if (icon == R.drawable.bt_jpg) {
            uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            whereClause = "_data = '" + path + "'";
        } else if (icon == R.drawable.bt_avi) {
            uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            whereClause = "_data = '" + path + "'";
        } else if (icon == R.drawable.bt_mp3) {
            uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            whereClause = "_data = '" + path + "'";
        } else {
            return;
        }
        activity.getContentResolver().delete(uri, whereClause, null);
    }

    public static void getSysContacts(Context context, boolean isRefresh) throws InterruptedException {
        Cursor cursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, EboxConst.CONTACTS_SUMMARY_PROJECTION, EboxConst.CONTACTS_SUMMARY_SELECT, null, "display_name COLLATE LOCALIZED ASC");
        int mod = 0;
        int sysCount = cursor.getCount();
        int loclCount = getCount(context, "contacts");
        if (isRefresh && sysCount < 200) {
            isRefresh = false;
        }
        if (isRefresh) {
            cursor.close();
            EboxContext.mSyncContacts.setLocalCount(sysCount);
            EboxContext.mSyncContacts.setLocalMod(0);
            return;
        }
        if (sysCount < loclCount) {
            mod = loclCount - sysCount;
        }
        if (sysCount <= 0) {
            mod = loclCount;
        } else {
            cursor.moveToFirst();
            do {
                try {
                    if (compareLocalContacts(context, cursor)) {
                        mod++;
                    }
                } catch (Exception e) {
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        EboxContext.mSyncContacts.setLocalCount(sysCount);
        EboxContext.mSyncContacts.setLocalMod(mod);
    }

    /* JADX INFO: Multiple debug info for r0v10 int: [D('ddStart' int), D('start' int)] */
    /* JADX INFO: Multiple debug info for r10v8 int: [D('start' int), D('end' int)] */
    /* JADX INFO: Multiple debug info for r1v4 java.lang.String[]: [D('tt' java.lang.String[]), D('name' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r10v18 java.lang.String: [D('tmp' java.lang.String), D('end' int)] */
    /* JADX INFO: Multiple debug info for r1v5 java.lang.String[]: [D('tt' java.lang.String[]), D('name' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v26 com.heyibao.android.ebox.db.EboxDB: [D('tmp' java.lang.String), D('settingDB' com.heyibao.android.ebox.db.EboxDB)] */
    public static boolean compareLocalContacts(Context context, Cursor cursor) throws InterruptedException {
        int version;
        String name;
        String tmp;
        int id = cursor.getInt(0);
        if (id < 0) {
            return false;
        }
        Cursor curInfo = context.getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI, new String[]{"version"}, "contact_id=" + id, null, null);
        if (curInfo.getCount() > 0) {
            curInfo.moveToFirst();
            version = curInfo.getInt(0);
        } else {
            version = 0;
        }
        curInfo.close();
        if (version == 0) {
            return false;
        }
        String name2 = cursor.getString(1).trim();
        int dStart = name2.indexOf(", ");
        int ddStart = name2.indexOf("  ");
        if (dStart > 0) {
            name = name2.replace(", ", EboxConst.TAB_UPLOADER_TAG);
        } else if (ddStart > 0) {
            String tmp2 = EboxConst.TAB_UPLOADER_TAG;
            String[] tt = name2.split("  ");
            for (int i = tt.length - 1; i >= 0; i--) {
                tmp2 = tmp2 + tt[i];
            }
            name = tmp2;
        } else {
            int start = name2.indexOf(" ");
            int end = name2.lastIndexOf(" ");
            if (start > 0) {
                if (end != start) {
                    tmp = name2.substring(start + 1, end);
                } else {
                    tmp = name2.substring(start + 1);
                }
                if (tmp.matches("^[a-zA-Z]+$") != 0) {
                    String tmp3 = EboxConst.TAB_UPLOADER_TAG;
                    String[] tt2 = name2.split(" ");
                    for (int i2 = tt2.length - 1; i2 >= 0; i2--) {
                        tmp3 = tmp3 + tt2[i2];
                    }
                    name = tmp3;
                } else {
                    name = name2;
                }
            } else {
                name = name2;
            }
        }
        return EboxDB.getInstance(context).comperaContacts(context, id, name, version);
    }

    /* JADX INFO: Multiple debug info for r14v30 int: [D('typeString' java.lang.String), D('key' int)] */
    /* JADX INFO: Multiple debug info for r14v34 'value'  java.lang.String: [D('value' java.lang.String), D('typeString' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r14v40 'typeString'  java.lang.String: [D('value' java.lang.String), D('typeString' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r14v43 int: [D('typeString' java.lang.String), D('key' int)] */
    /* JADX INFO: Multiple debug info for r14v54 byte[]: [D('typeString' java.lang.String), D('bt' byte[])] */
    /* JADX INFO: Multiple debug info for r14v59 int: [D('typeString' java.lang.String), D('key' int)] */
    /* JADX INFO: Multiple debug info for r14v69 int: [D('typeString' java.lang.String), D('key' int)] */
    /* JADX INFO: Multiple debug info for r14v74 com.heyibao.android.ebox.model.MyContacts: [D('phone' java.lang.String), D('context' android.content.Context)] */
    /* JADX INFO: Multiple debug info for r14v76 int: [D('typeString' java.lang.String), D('key' int)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static MyContacts getContacts(Context context, int id) {
        String value;
        Exception th;
        String temp;
        String value2;
        String value3;
        String value4;
        String phone;
        String phone2;
        String value5;
        String typeString;
        String value6;
        String tmp;
        if (id < 0) {
            return null;
        }
        MyContacts contacts = new MyContacts();
        Cursor curInfo = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, EboxConst.CONTACTS_DATA_PROJECTION, "contact_id=" + id, null, null);
        contacts.setContactId(id);
        if (curInfo.getCount() <= 0) {
            return contacts;
        }
        curInfo.moveToFirst();
        String value7 = null;
        String temp2 = null;
        String phone3 = EboxConst.TAB_UPLOADER_TAG;
        while (true) {
            try {
                contacts.setRawContactId(curInfo.getInt(12));
                String typeString2 = curInfo.getString(0);
                if (typeString2.equals("vnd.android.cursor.item/email_v2")) {
                    int key = curInfo.getInt(2);
                    value4 = curInfo.getString(1);
                    if (value4 != null) {
                        try {
                            if (!value4.equals(EboxConst.TAB_UPLOADER_TAG)) {
                                contacts.addEmailV2(Integer.valueOf(key), value4);
                                phone = phone3;
                                phone2 = value4;
                                value5 = temp2;
                            }
                        } catch (Exception e) {
                            e = e;
                            String value8 = tmp;
                            temp = phone3;
                            value3 = temp2;
                            value2 = value8;
                            try {
                                e.printStackTrace();
                                curInfo.close();
                                return null;
                            } catch (Throwable e2) {
                                th = e2;
                                curInfo.close();
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            curInfo.close();
                            throw th;
                        }
                    }
                    phone = phone3;
                    phone2 = value4;
                    value5 = temp2;
                } else if (typeString2.equals("vnd.android.cursor.item/postal-address_v2")) {
                    int key2 = curInfo.getInt(2);
                    String value9 = curInfo.getString(4);
                    if (value9 != null) {
                        if (!value9.equals(EboxConst.TAB_UPLOADER_TAG)) {
                            contacts.getInstanceAddress().setAddress(value9);
                        }
                    }
                    String value10 = curInfo.getString(7);
                    if (value10 != null && !value10.equals(EboxConst.TAB_UPLOADER_TAG)) {
                        contacts.getInstanceAddress().setCity(value10);
                    }
                    String value11 = curInfo.getString(8);
                    if (value11 != null && !value11.equals(EboxConst.TAB_UPLOADER_TAG)) {
                        contacts.getInstanceAddress().setProvince(value11);
                    }
                    String value12 = curInfo.getString(9);
                    if (value12 != null && !value12.equals(EboxConst.TAB_UPLOADER_TAG)) {
                        contacts.getInstanceAddress().setZip(value12);
                    }
                    value4 = curInfo.getString(10);
                    if (value4 != null && !value4.equals(EboxConst.TAB_UPLOADER_TAG)) {
                        contacts.getInstanceAddress().setCountry(value4);
                    }
                    if (contacts.address != null) {
                        contacts.addPostalAddressV2(Integer.valueOf(key2), contacts.getInstanceAddress());
                        phone = phone3;
                        phone2 = value4;
                        value5 = temp2;
                    }
                    phone = phone3;
                    phone2 = value4;
                    value5 = temp2;
                } else if (typeString2.equals("vnd.android.cursor.item/phone_v2")) {
                    int key3 = curInfo.getInt(2);
                    String value13 = curInfo.getString(1);
                    if (value13 == null || value13.equals(EboxConst.TAB_UPLOADER_TAG)) {
                        value5 = temp2;
                    } else {
                        value5 = value13;
                        try {
                            contacts.addPhoneV2(Integer.valueOf(key3), value13);
                        } catch (Exception e3) {
                            e = e3;
                            value2 = value13;
                            String temp3 = value5;
                            temp = phone3;
                            value3 = temp3;
                            e.printStackTrace();
                            curInfo.close();
                            return null;
                        } catch (Throwable th3) {
                            th = th3;
                            curInfo.close();
                            throw th;
                        }
                    }
                    if (key3 != 2 || value13 == null) {
                        phone = phone3;
                    } else {
                        phone = value13;
                    }
                    if (value5 != null) {
                        try {
                            if (phone.equals(EboxConst.TAB_UPLOADER_TAG)) {
                                phone = value5;
                                phone2 = value13;
                            }
                        } catch (Exception e4) {
                            value2 = value13;
                            Exception exc = e4;
                            value3 = value5;
                            temp = phone;
                            e = exc;
                        } catch (Throwable th4) {
                            th = th4;
                            curInfo.close();
                            throw th;
                        }
                    }
                    phone2 = value13;
                } else if (typeString2.equals("vnd.android.cursor.item/photo")) {
                    byte[] bt = curInfo.getBlob(11);
                    if (bt != null) {
                        Bitmap bm = BitmapFactory.decodeStream(new ByteArrayInputStream(bt));
                        int w = bm.getWidth();
                        int h = bm.getHeight();
                        Matrix matrix = new Matrix();
                        matrix.postScale(54.0f / ((float) w), 54.0f / ((float) h));
                        contacts.setPhotoMap(Bitmap.createBitmap(bm, 0, 0, w, h, matrix, true));
                        contacts.setPhotoBt(bt);
                        contacts.setPhotoVer(curInfo.getInt(13));
                    }
                    value5 = temp2;
                    phone = phone3;
                    phone2 = value;
                } else if (typeString2.equals("vnd.android.cursor.item/name")) {
                    String name = EboxConst.TAB_UPLOADER_TAG;
                    String value14 = curInfo.getString(3);
                    if (value14 != null) {
                        if (!value14.equals(EboxConst.TAB_UPLOADER_TAG)) {
                            name = value14;
                            contacts.setFirstName(value14);
                        }
                    }
                    String value15 = curInfo.getString(5);
                    value = (value15 == null || value15.equals(EboxConst.TAB_UPLOADER_TAG) || value15.equals("null")) ? EboxConst.TAB_UPLOADER_TAG : value15;
                    String tmp2 = curInfo.getString(2);
                    if (tmp2 == null || tmp2.equals("null")) {
                        tmp = value;
                    } else {
                        tmp = value + tmp2;
                    }
                    contacts.setLastName(tmp);
                    contacts.setName(name + tmp);
                    phone = phone3;
                    phone2 = tmp;
                    value5 = temp2;
                } else if (typeString2.equals("vnd.android.cursor.item/organization")) {
                    int key4 = curInfo.getInt(2);
                    String value16 = curInfo.getString(1);
                    if (value16 != null) {
                        if (!value16.equals(EboxConst.TAB_UPLOADER_TAG)) {
                            contacts.getInstanceOrganization().setCompany(value16);
                        }
                    }
                    value4 = curInfo.getString(4);
                    if (value4 != null && !value4.equals(EboxConst.TAB_UPLOADER_TAG)) {
                        contacts.getInstanceOrganization().setDuties(value4);
                    }
                    if (contacts.organization != null) {
                        contacts.addOrganization(Integer.valueOf(key4), contacts.getInstanceOrganization());
                        phone = phone3;
                        phone2 = value4;
                        value5 = temp2;
                    }
                    phone = phone3;
                    phone2 = value4;
                    value5 = temp2;
                } else {
                    if (typeString2.equals("vnd.android.cursor.item/nickname")) {
                        typeString = curInfo.getString(1);
                        if (typeString != null) {
                            try {
                                if (!typeString.equals(EboxConst.TAB_UPLOADER_TAG)) {
                                    contacts.setNick(typeString);
                                    value5 = temp2;
                                    String value17 = typeString;
                                    phone = phone3;
                                    phone2 = value17;
                                }
                            } catch (Exception e5) {
                                Exception exc2 = e5;
                                temp = phone3;
                                value3 = temp2;
                                value2 = value6;
                                e = exc2;
                                e.printStackTrace();
                                curInfo.close();
                                return null;
                            } catch (Throwable th5) {
                                th = th5;
                                curInfo.close();
                                throw th;
                            }
                        }
                    } else if (typeString2.equals("vnd.android.cursor.item/group_membership")) {
                        contacts.setGroup(curInfo.getInt(1));
                        value5 = temp2;
                        phone = phone3;
                        phone2 = value;
                    } else if (typeString2.equals("vnd.android.cursor.item/note")) {
                        typeString = curInfo.getString(1);
                        if (typeString != null) {
                            if (!typeString.equals(EboxConst.TAB_UPLOADER_TAG)) {
                                contacts.setNote(typeString);
                                value5 = temp2;
                                String value18 = typeString;
                                phone = phone3;
                                phone2 = value18;
                            }
                        }
                    } else if (typeString2.equals("vnd.android.cursor.item/website")) {
                        int key5 = curInfo.getInt(2);
                        value4 = curInfo.getString(1);
                        if (value4 != null) {
                            if (!value4.equals(EboxConst.TAB_UPLOADER_TAG)) {
                                contacts.addWebsite(Integer.valueOf(key5), value4);
                                phone = phone3;
                                phone2 = value4;
                                value5 = temp2;
                            }
                        }
                        phone = phone3;
                        phone2 = value4;
                        value5 = temp2;
                    } else if (typeString2.equals("vnd.android.cursor.item/im")) {
                        int key6 = curInfo.getInt(5);
                        value4 = curInfo.getString(1);
                        if (value4 != null) {
                            if (!value4.equals(EboxConst.TAB_UPLOADER_TAG)) {
                                contacts.addIm(Integer.valueOf(key6), value4);
                            }
                        }
                        phone = phone3;
                        phone2 = value4;
                        value5 = temp2;
                    } else {
                        value5 = temp2;
                        phone = phone3;
                        phone2 = value;
                    }
                    value5 = temp2;
                    String value19 = typeString;
                    phone = phone3;
                    phone2 = value19;
                }
                try {
                    if (!curInfo.moveToNext()) {
                        curInfo.close();
                        return contacts;
                    }
                    value7 = phone2;
                    temp2 = value5;
                    phone3 = phone;
                } catch (Exception e6) {
                    Exception exc3 = e6;
                    value2 = phone2;
                    value3 = value5;
                    temp = phone;
                    e = exc3;
                } catch (Throwable th6) {
                    th = th6;
                    curInfo.close();
                    throw th;
                }
            } catch (Exception e7) {
                e = e7;
                temp = phone3;
                value3 = temp2;
                value2 = value;
                e.printStackTrace();
                curInfo.close();
                return null;
            } catch (Throwable th7) {
                th = th7;
                curInfo.close();
                throw th;
            }
        }
    }

    /* JADX INFO: Multiple debug info for r0v22 android.content.ContentProviderResult[]: [D('arr$' android.content.ContentProviderResult[]), D('results' android.content.ContentProviderResult[])] */
    /* JADX INFO: Multiple debug info for r3v1 int: [D('ops' java.util.ArrayList<android.content.ContentProviderOperation>), D('len$' int)] */
    /*  JADX ERROR: UnsupportedOperationException in pass: MethodInvokeVisitor
        java.lang.UnsupportedOperationException: ArgType.getObject(), call class: class jadx.core.dex.instructions.args.ArgType$ArrayArg
        	at jadx.core.dex.instructions.args.ArgType.getObject(ArgType.java:513)
        	at jadx.core.clsp.ClspGraph.getClsDetails(ClspGraph.java:76)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:100)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static void insertContacts(android.content.Context r8, com.heyibao.android.ebox.model.MyContacts r9) {
        /*
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r4 = 0
            android.net.Uri r0 = android.provider.ContactsContract.RawContacts.CONTENT_URI
            android.content.ContentProviderOperation$Builder r0 = android.content.ContentProviderOperation.newInsert(r0)
            java.lang.String r1 = "account_type"
            r2 = 0
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r1, r2)
            java.lang.String r1 = "account_name"
            r2 = 0
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r1, r2)
            java.lang.String r1 = "aggregation_mode"
            r2 = 3
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r1, r2)
            android.content.ContentProviderOperation r0 = r0.build()
            r3.add(r0)
            java.lang.String r0 = r9.getName()
            if (r0 == 0) goto L_0x006b
            android.net.Uri r0 = android.provider.ContactsContract.Data.CONTENT_URI
            android.content.ContentProviderOperation$Builder r0 = android.content.ContentProviderOperation.newInsert(r0)
            java.lang.String r1 = "raw_contact_id"
            android.content.ContentProviderOperation$Builder r0 = r0.withValueBackReference(r1, r4)
            java.lang.String r1 = "mimetype"
            java.lang.String r2 = "vnd.android.cursor.item/name"
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r1, r2)
            java.lang.String r1 = "data1"
            java.lang.String r2 = r9.getName()
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r1, r2)
            java.lang.String r1 = "data3"
            java.lang.String r2 = r9.getFirstName()
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r1, r2)
            java.lang.String r1 = "data2"
            java.lang.String r2 = r9.getLastName()
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r1, r2)
            android.content.ContentProviderOperation r0 = r0.build()
            r3.add(r0)
        L_0x006b:
            byte[] r0 = r9.getPhotoBt()
            if (r0 == 0) goto L_0x00a3
            byte[] r0 = r9.getPhotoBt()
            java.lang.String r1 = ""
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x00a3
            android.net.Uri r0 = android.provider.ContactsContract.Data.CONTENT_URI
            android.content.ContentProviderOperation$Builder r0 = android.content.ContentProviderOperation.newInsert(r0)
            java.lang.String r1 = "raw_contact_id"
            r2 = 0
            android.content.ContentProviderOperation$Builder r0 = r0.withValueBackReference(r1, r2)
            java.lang.String r1 = "mimetype"
            java.lang.String r2 = "vnd.android.cursor.item/photo"
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r1, r2)
            java.lang.String r1 = "data15"
            byte[] r2 = r9.getPhotoBt()
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r1, r2)
            android.content.ContentProviderOperation r0 = r0.build()
            r3.add(r0)
        L_0x00a3:
            java.lang.String r0 = r9.getNick()
            if (r0 == 0) goto L_0x00da
            java.lang.String r0 = r9.getNick()
            java.lang.String r1 = ""
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x00da
            android.net.Uri r0 = android.provider.ContactsContract.Data.CONTENT_URI
            android.content.ContentProviderOperation$Builder r0 = android.content.ContentProviderOperation.newInsert(r0)
            java.lang.String r1 = "raw_contact_id"
            android.content.ContentProviderOperation$Builder r0 = r0.withValueBackReference(r1, r4)
            java.lang.String r1 = "mimetype"
            java.lang.String r2 = "vnd.android.cursor.item/nickname"
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r1, r2)
            java.lang.String r1 = "data1"
            java.lang.String r2 = r9.getNick()
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r1, r2)
            android.content.ContentProviderOperation r0 = r0.build()
            r3.add(r0)
        L_0x00da:
            java.lang.String r0 = r9.getNote()
            if (r0 == 0) goto L_0x0111
            java.lang.String r0 = r9.getNote()
            java.lang.String r1 = ""
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0111
            android.net.Uri r0 = android.provider.ContactsContract.Data.CONTENT_URI
            android.content.ContentProviderOperation$Builder r0 = android.content.ContentProviderOperation.newInsert(r0)
            java.lang.String r1 = "raw_contact_id"
            android.content.ContentProviderOperation$Builder r0 = r0.withValueBackReference(r1, r4)
            java.lang.String r1 = "mimetype"
            java.lang.String r2 = "vnd.android.cursor.item/note"
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r1, r2)
            java.lang.String r1 = "data1"
            java.lang.String r2 = r9.getNote()
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r1, r2)
            android.content.ContentProviderOperation r0 = r0.build()
            r3.add(r0)
        L_0x0111:
            java.util.HashMap r0 = r9.getPhoneV2()
            if (r0 == 0) goto L_0x0162
            r0 = 1
        L_0x0118:
            r1 = 13
            if (r0 > r1) goto L_0x0162
            java.util.HashMap r1 = r9.getPhoneV2()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)
            java.lang.Object r1 = r1.get(r2)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 == 0) goto L_0x015f
            java.lang.String r2 = ""
            boolean r2 = r1.equals(r2)
            if (r2 != 0) goto L_0x015f
            android.net.Uri r2 = android.provider.ContactsContract.Data.CONTENT_URI
            android.content.ContentProviderOperation$Builder r2 = android.content.ContentProviderOperation.newInsert(r2)
            java.lang.String r5 = "raw_contact_id"
            android.content.ContentProviderOperation$Builder r2 = r2.withValueBackReference(r5, r4)
            java.lang.String r5 = "mimetype"
            java.lang.String r6 = "vnd.android.cursor.item/phone_v2"
            android.content.ContentProviderOperation$Builder r2 = r2.withValue(r5, r6)
            java.lang.String r5 = "data1"
            android.content.ContentProviderOperation$Builder r1 = r2.withValue(r5, r1)
            java.lang.String r2 = "data2"
            java.lang.Integer r5 = java.lang.Integer.valueOf(r0)
            android.content.ContentProviderOperation$Builder r1 = r1.withValue(r2, r5)
            android.content.ContentProviderOperation r1 = r1.build()
            r3.add(r1)
        L_0x015f:
            int r0 = r0 + 1
            goto L_0x0118
        L_0x0162:
            java.util.HashMap r0 = r9.getEmailV2()
            if (r0 == 0) goto L_0x01b2
            r0 = 1
        L_0x0169:
            r1 = 4
            if (r0 > r1) goto L_0x01b2
            java.util.HashMap r1 = r9.getEmailV2()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)
            java.lang.Object r1 = r1.get(r2)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 == 0) goto L_0x01af
            java.lang.String r2 = ""
            boolean r2 = r1.equals(r2)
            if (r2 != 0) goto L_0x01af
            android.net.Uri r2 = android.provider.ContactsContract.Data.CONTENT_URI
            android.content.ContentProviderOperation$Builder r2 = android.content.ContentProviderOperation.newInsert(r2)
            java.lang.String r5 = "raw_contact_id"
            android.content.ContentProviderOperation$Builder r2 = r2.withValueBackReference(r5, r4)
            java.lang.String r5 = "mimetype"
            java.lang.String r6 = "vnd.android.cursor.item/email_v2"
            android.content.ContentProviderOperation$Builder r2 = r2.withValue(r5, r6)
            java.lang.String r5 = "data1"
            android.content.ContentProviderOperation$Builder r1 = r2.withValue(r5, r1)
            java.lang.String r2 = "data2"
            java.lang.Integer r5 = java.lang.Integer.valueOf(r0)
            android.content.ContentProviderOperation$Builder r1 = r1.withValue(r2, r5)
            android.content.ContentProviderOperation r1 = r1.build()
            r3.add(r1)
        L_0x01af:
            int r0 = r0 + 1
            goto L_0x0169
        L_0x01b2:
            java.util.HashMap r0 = r9.getWebsite()
            if (r0 == 0) goto L_0x0202
            r0 = 0
        L_0x01b9:
            r1 = 7
            if (r0 > r1) goto L_0x0202
            java.util.HashMap r1 = r9.getWebsite()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)
            java.lang.Object r1 = r1.get(r2)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 == 0) goto L_0x01ff
            java.lang.String r2 = ""
            boolean r2 = r1.equals(r2)
            if (r2 != 0) goto L_0x01ff
            android.net.Uri r2 = android.provider.ContactsContract.Data.CONTENT_URI
            android.content.ContentProviderOperation$Builder r2 = android.content.ContentProviderOperation.newInsert(r2)
            java.lang.String r5 = "raw_contact_id"
            android.content.ContentProviderOperation$Builder r2 = r2.withValueBackReference(r5, r4)
            java.lang.String r5 = "mimetype"
            java.lang.String r6 = "vnd.android.cursor.item/website"
            android.content.ContentProviderOperation$Builder r2 = r2.withValue(r5, r6)
            java.lang.String r5 = "data1"
            android.content.ContentProviderOperation$Builder r1 = r2.withValue(r5, r1)
            java.lang.String r2 = "data2"
            java.lang.Integer r5 = java.lang.Integer.valueOf(r0)
            android.content.ContentProviderOperation$Builder r1 = r1.withValue(r2, r5)
            android.content.ContentProviderOperation r1 = r1.build()
            r3.add(r1)
        L_0x01ff:
            int r0 = r0 + 1
            goto L_0x01b9
        L_0x0202:
            java.util.HashMap r0 = r9.getIm()
            if (r0 == 0) goto L_0x0252
            r0 = 0
        L_0x0209:
            r1 = 7
            if (r0 > r1) goto L_0x0252
            java.util.HashMap r1 = r9.getIm()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)
            java.lang.Object r1 = r1.get(r2)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 == 0) goto L_0x024f
            java.lang.String r2 = ""
            boolean r2 = r1.equals(r2)
            if (r2 != 0) goto L_0x024f
            android.net.Uri r2 = android.provider.ContactsContract.Data.CONTENT_URI
            android.content.ContentProviderOperation$Builder r2 = android.content.ContentProviderOperation.newInsert(r2)
            java.lang.String r5 = "raw_contact_id"
            android.content.ContentProviderOperation$Builder r2 = r2.withValueBackReference(r5, r4)
            java.lang.String r5 = "mimetype"
            java.lang.String r6 = "vnd.android.cursor.item/im"
            android.content.ContentProviderOperation$Builder r2 = r2.withValue(r5, r6)
            java.lang.String r5 = "data1"
            android.content.ContentProviderOperation$Builder r1 = r2.withValue(r5, r1)
            java.lang.String r2 = "data5"
            java.lang.Integer r5 = java.lang.Integer.valueOf(r0)
            android.content.ContentProviderOperation$Builder r1 = r1.withValue(r2, r5)
            android.content.ContentProviderOperation r1 = r1.build()
            r3.add(r1)
        L_0x024f:
            int r0 = r0 + 1
            goto L_0x0209
        L_0x0252:
            java.util.HashMap r0 = r9.getPostalAddressV2()
            if (r0 == 0) goto L_0x0323
            r0 = 0
            r1 = r0
        L_0x025a:
            r0 = 3
            if (r1 > r0) goto L_0x0323
            java.util.HashMap r0 = r9.getPostalAddressV2()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r1)
            java.lang.Object r2 = r0.get(r2)
            com.heyibao.android.ebox.model.MyContacts$Address r2 = (com.heyibao.android.ebox.model.MyContacts.Address) r2
            if (r2 != 0) goto L_0x0271
        L_0x026d:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x025a
        L_0x0271:
            android.net.Uri r0 = android.provider.ContactsContract.Data.CONTENT_URI
            android.content.ContentProviderOperation$Builder r0 = android.content.ContentProviderOperation.newInsert(r0)
            java.lang.String r5 = "raw_contact_id"
            android.content.ContentProviderOperation$Builder r0 = r0.withValueBackReference(r5, r4)
            java.lang.String r5 = "mimetype"
            java.lang.String r6 = "vnd.android.cursor.item/postal-address_v2"
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r5, r6)
            java.lang.String r5 = "data2"
            java.lang.Integer r6 = java.lang.Integer.valueOf(r1)
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r5, r6)
            java.lang.String r5 = r2.getAddress()
            if (r5 == 0) goto L_0x02aa
            java.lang.String r5 = r2.getAddress()
            java.lang.String r6 = ""
            boolean r5 = r5.equals(r6)
            if (r5 != 0) goto L_0x02aa
            java.lang.String r5 = "data4"
            java.lang.String r6 = r2.getAddress()
            r0.withValue(r5, r6)
        L_0x02aa:
            java.lang.String r5 = r2.getCity()
            if (r5 == 0) goto L_0x02c7
            java.lang.String r5 = r2.getCity()
            java.lang.String r6 = ""
            boolean r5 = r5.equals(r6)
            if (r5 != 0) goto L_0x02c7
            if (r0 == 0) goto L_0x026d
            java.lang.String r5 = "data7"
            java.lang.String r6 = r2.getCity()
            r0.withValue(r5, r6)
        L_0x02c7:
            java.lang.String r5 = r2.getProvince()
            if (r5 == 0) goto L_0x02e2
            java.lang.String r5 = r2.getProvince()
            java.lang.String r6 = ""
            boolean r5 = r5.equals(r6)
            if (r5 != 0) goto L_0x02e2
            java.lang.String r5 = "data8"
            java.lang.String r6 = r2.getProvince()
            r0.withValue(r5, r6)
        L_0x02e2:
            java.lang.String r5 = r2.getZip()
            if (r5 == 0) goto L_0x02fd
            java.lang.String r5 = r2.getZip()
            java.lang.String r6 = ""
            boolean r5 = r5.equals(r6)
            if (r5 != 0) goto L_0x02fd
            java.lang.String r5 = "data9"
            java.lang.String r6 = r2.getZip()
            r0.withValue(r5, r6)
        L_0x02fd:
            java.lang.String r5 = r2.getCountry()
            if (r5 == 0) goto L_0x0318
            java.lang.String r5 = r2.getCountry()
            java.lang.String r6 = ""
            boolean r5 = r5.equals(r6)
            if (r5 != 0) goto L_0x0318
            java.lang.String r5 = "data10"
            java.lang.String r2 = r2.getCountry()
            r0.withValue(r5, r2)
        L_0x0318:
            if (r0 == 0) goto L_0x026d
            android.content.ContentProviderOperation r0 = r0.build()
            r3.add(r0)
            goto L_0x026d
        L_0x0323:
            java.util.HashMap r0 = r9.getOrganization()
            if (r0 == 0) goto L_0x03a0
            r0 = 0
            r1 = r0
        L_0x032b:
            r0 = 2
            if (r1 > r0) goto L_0x03a0
            java.util.HashMap r0 = r9.getOrganization()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r1)
            java.lang.Object r2 = r0.get(r2)
            com.heyibao.android.ebox.model.MyContacts$Organization r2 = (com.heyibao.android.ebox.model.MyContacts.Organization) r2
            if (r2 != 0) goto L_0x0342
        L_0x033e:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x032b
        L_0x0342:
            android.net.Uri r0 = android.provider.ContactsContract.Data.CONTENT_URI
            android.content.ContentProviderOperation$Builder r0 = android.content.ContentProviderOperation.newInsert(r0)
            java.lang.String r5 = "raw_contact_id"
            android.content.ContentProviderOperation$Builder r0 = r0.withValueBackReference(r5, r4)
            java.lang.String r5 = "mimetype"
            java.lang.String r6 = "vnd.android.cursor.item/organization"
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r5, r6)
            java.lang.String r5 = "data2"
            java.lang.Integer r6 = java.lang.Integer.valueOf(r1)
            android.content.ContentProviderOperation$Builder r0 = r0.withValue(r5, r6)
            java.lang.String r5 = r2.getCompany()
            if (r5 == 0) goto L_0x037b
            java.lang.String r5 = r2.getCompany()
            java.lang.String r6 = ""
            boolean r5 = r5.equals(r6)
            if (r5 != 0) goto L_0x037b
            java.lang.String r5 = "data1"
            java.lang.String r6 = r2.getCompany()
            r0.withValue(r5, r6)
        L_0x037b:
            java.lang.String r5 = r2.getDuties()
            if (r5 == 0) goto L_0x0396
            java.lang.String r5 = r2.getDuties()
            java.lang.String r6 = ""
            boolean r5 = r5.equals(r6)
            if (r5 != 0) goto L_0x0396
            java.lang.String r5 = "data4"
            java.lang.String r2 = r2.getDuties()
            r0.withValue(r5, r2)
        L_0x0396:
            if (r0 == 0) goto L_0x033e
            android.content.ContentProviderOperation r0 = r0.build()
            r3.add(r0)
            goto L_0x033e
        L_0x03a0:
            r1 = 0
            android.content.ContentResolver r0 = r8.getContentResolver()     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            java.lang.String r2 = "com.android.contacts"
            android.content.ContentProviderResult[] r0 = r0.applyBatch(r2, r3)     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            r0 = r0
            int r3 = r0.length     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            r2 = 0
        L_0x03ae:
            if (r2 >= r3) goto L_0x03eb
            r4 = r0[r2]     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            android.net.Uri r5 = r4.uri     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            java.lang.String r5 = r5.toString()     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            android.net.Uri r6 = android.provider.ContactsContract.RawContacts.CONTENT_URI     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            java.lang.String r6 = r6.toString()     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            boolean r5 = r5.startsWith(r6)     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            if (r5 == 0) goto L_0x03c6
            android.net.Uri r1 = r4.uri     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
        L_0x03c6:
            java.lang.Class<com.heyibao.android.ebox.db.ActionDB> r5 = com.heyibao.android.ebox.db.ActionDB.class
            java.lang.String r5 = r5.getSimpleName()     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            r6.<init>()     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            java.lang.String r7 = "insert contact uri : "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            android.net.Uri r4 = r4.uri     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            java.lang.String r4 = r4.toString()     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            java.lang.String r4 = r4.toString()     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            android.util.Log.d(r5, r4)     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            int r2 = r2 + 1
            goto L_0x03ae
        L_0x03eb:
            android.content.ContentResolver r0 = r8.getContentResolver()     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            r3 = 0
            java.lang.String r4 = "contact_id"
            r2[r3] = r4     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            r3 = 1
            java.lang.String r4 = "version"
            r2[r3] = r4     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            r5 = -1
            r3 = -1
            if (r0 == 0) goto L_0x041a
            boolean r2 = r0.moveToFirst()     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            if (r2 == 0) goto L_0x0417
            r2 = 0
            int r3 = r0.getInt(r2)     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            r2 = 1
            int r5 = r0.getInt(r2)     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
        L_0x0417:
            r0.close()     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
        L_0x041a:
            r0 = -1
            if (r5 == r0) goto L_0x0430
            r0 = -1
            if (r3 == r0) goto L_0x0430
            int r4 = r9.getSyncId()     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            java.lang.String r6 = r9.getName()     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            java.lang.String r7 = r9.getModifiedTime()     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
            r2 = r8
            insertSyncContacts(r2, r3, r4, r5, r6, r7)     // Catch:{ RemoteException -> 0x0431, OperationApplicationException -> 0x0456 }
        L_0x0430:
            return
        L_0x0431:
            r8 = move-exception
            r8.printStackTrace()
            java.lang.Class<com.heyibao.android.ebox.db.ActionDB> r9 = com.heyibao.android.ebox.db.ActionDB.class
            java.lang.String r9 = r9.getSimpleName()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "insert contact error: "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r8 = r8.getMessage()
            java.lang.StringBuilder r8 = r0.append(r8)
            java.lang.String r8 = r8.toString()
            android.util.Log.e(r9, r8)
            goto L_0x0430
        L_0x0456:
            r8 = move-exception
            r8.printStackTrace()
            java.lang.Class<com.heyibao.android.ebox.db.ActionDB> r9 = com.heyibao.android.ebox.db.ActionDB.class
            java.lang.String r9 = r9.getSimpleName()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "insert contact error: "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r8 = r8.getMessage()
            java.lang.StringBuilder r8 = r0.append(r8)
            java.lang.String r8 = r8.toString()
            android.util.Log.e(r9, r8)
            goto L_0x0430
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heyibao.android.ebox.db.ActionDB.insertContacts(android.content.Context, com.heyibao.android.ebox.model.MyContacts):void");
    }

    public static void updateContacts(Context activity, MyContacts myContacts) {
        try {
            delContacts(activity, myContacts);
            insertContacts(activity, myContacts);
        } catch (Exception e) {
            Exception e2 = e;
            Log.e(ActionDB.class.getSimpleName(), "update contact error: " + e2.getMessage());
            e2.printStackTrace();
        }
    }

    public static void delContacts(Context activity, MyContacts myContacts) {
        activity.getContentResolver().delete(ContactsContract.RawContacts.CONTENT_URI, "contact_id=" + myContacts.getContactId(), null);
        delSyncContacts(activity, myContacts.getContactId(), myContacts.getSyncId(), myContacts.getModifiedTime());
    }

    public static boolean getSysContactById(Context activity, int contactId) {
        return activity.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, new StringBuilder().append("_id='").append(contactId).append("'").toString(), null, null).getCount() <= 0;
    }

    public static HashMap<String, Object> getSyncContactById(Context activity, int contactId) {
        return EboxDB.getInstance(activity).getSnapContactsById(activity, contactId);
    }

    public static HashMap<String, Object> getSyncContactBySyncId(Context activity, int SyncId) {
        return EboxDB.getInstance(activity).getSnapContactsBySync(activity, SyncId);
    }

    public static HashMap<String, Object> getSyncContactByName(Context activity, String name, int syncId) {
        return EboxDB.getInstance(activity).getSnapContactsByName(activity, name, syncId);
    }

    public static Vector<HashMap<String, Integer>> getSyncContactByStatus(Context activity, int status, boolean isTrue) {
        return EboxDB.getInstance(activity).getSnapContactsByStatus(activity, status, isTrue);
    }

    public static void updateSyncContactsOK(Context activity, String time) {
        EboxDB.getInstance(activity).updateContactsOK(activity);
        if (time != null && !time.equals(EboxConst.TAB_UPLOADER_TAG)) {
            updateContactsConfigs(activity, time);
            EboxContext.mSystemInfo.setSyncContactsTime(time);
        }
    }

    public static boolean insertSyncContacts(Context activity, int contactId, int syncId, int version, String name, String time) {
        if (!EboxDB.getInstance(activity).insertContacts(activity, contactId, syncId, name, version, 3)) {
            return false;
        }
        if (time != null && !time.equals(EboxConst.TAB_UPLOADER_TAG)) {
            updateContactsConfigs(activity, time);
            EboxContext.mSystemInfo.setSyncContactsTime(time);
        }
        return true;
    }

    public static boolean updateSyncContacts(Context activity, int contactId, int syncId, String name, String time) {
        if (!EboxDB.getInstance(activity).updateContacts(activity, contactId, syncId, name, -1, 4)) {
            return false;
        }
        if (time != null && !time.equals(EboxConst.TAB_UPLOADER_TAG)) {
            updateContactsConfigs(activity, time);
            EboxContext.mSystemInfo.setSyncContactsTime(time);
        }
        return true;
    }

    public static boolean delSyncContacts(Context activity, int contactId, int syncId, String time) {
        if (!EboxDB.getInstance(activity).delContacts(activity, contactId, syncId)) {
            return false;
        }
        if (time != null && !time.equals(EboxConst.TAB_UPLOADER_TAG)) {
            updateContactsConfigs(activity, time);
            EboxContext.mSystemInfo.setSyncContactsTime(time);
        }
        return true;
    }

    public static int getCount(Context activity, String table) {
        return EboxDB.getInstance(activity).getCount(activity, table);
    }

    public static boolean insertLocations(Context activity, String latitude, String longitude, String name, String time) {
        if (EboxDB.getInstance(activity).insertLocations(activity, latitude, longitude, name, time)) {
            return updateSignConfigs(activity, time);
        }
        return false;
    }
}
