package com.heyibao.android.ebox;

import ClientProtocol.ClientMeta;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;
import com.google.protobuf.CodedOutputStream;
import com.heyibao.android.ebox.activity.NewAccActivity;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.util.Utils;
import java.io.File;

public class SplashScreen extends Activity {
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        getWindow().setFormat(1);
        getWindow().addFlags(CodedOutputStream.DEFAULT_BUFFER_SIZE);
        setContentView((int) R.layout.splash);
        Log.d(SplashScreen.class.getSimpleName(), "## create SplashScreen now");
        if (EboxContext.curActivity != null) {
            startActivity(new Intent(this, EboxContext.curActivity.getClass()));
            overridePendingTransition(R.anim.fade, R.anim.hold);
            finish();
            return;
        }
        SplashInit();
    }

    private void SplashInit() {
        Utils.startService(this, EboxConst.ACTION_NORMAL, null, R.drawable.app_unnet);
        ((TextView) findViewById(R.id.versionNumber)).setText("Version " + getString(R.string.version));
        EboxContext.mSystemInfo.setSdPath(Environment.getExternalStorageDirectory() + File.separator + getString(R.string.app_type) + File.separator);
        if (getWindowManager().getDefaultDisplay().getHeight() > EboxConst.displayHeigth) {
            EboxContext.mSystemInfo.setDisplayChange(true);
        }
        new Handler().postDelayed(new Runnable() {
            public void run() {
                ClientMeta.DeviceInfoRequest.newBuilder();
                SplashScreen.this.startActivity(new Intent(SplashScreen.this, NewAccActivity.class));
                SplashScreen.this.overridePendingTransition(R.anim.fade, R.anim.hold);
                SplashScreen.this.finish();
            }
        }, 500);
    }
}
