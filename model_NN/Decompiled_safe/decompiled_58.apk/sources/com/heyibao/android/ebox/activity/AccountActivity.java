package com.heyibao.android.ebox.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.db.ActionDB;
import com.heyibao.android.ebox.model.MyDialog;
import com.heyibao.android.ebox.model.StandResult;
import com.heyibao.android.ebox.util.Utils;
import java.util.HashMap;
import java.util.Map;

public class AccountActivity extends HomeMenuActivity {
    /* access modifiers changed from: private */
    public static String[] DATA = new String[8];
    /* access modifiers changed from: private */
    public static String[] DATA2 = new String[8];
    private AccountAdapter adapter;
    /* access modifiers changed from: private */
    public int flag;
    private AccountReceiver receiver;

    public static class AccountAdapter extends BaseAdapter implements View.OnClickListener {
        AccountActivity accountActivity = new AccountActivity();
        private String all;
        private String last;
        private LayoutInflater mInflater;
        private String percent;
        private int progress;
        private Map<Integer, View> viewMap = new HashMap();

        public AccountAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
            AccountActivity.DATA[0] = context.getString(R.string.teamname);
            AccountActivity.DATA[1] = context.getString(R.string.username);
            AccountActivity.DATA[2] = context.getString(R.string.phone);
            AccountActivity.DATA[3] = context.getString(R.string.email);
            AccountActivity.DATA[4] = context.getString(R.string.isadmin);
            AccountActivity.DATA[5] = context.getString(R.string.endtime);
            AccountActivity.DATA[6] = EboxConst.TAB_UPLOADER_TAG;
            AccountActivity.DATA[7] = EboxConst.TAB_UPLOADER_TAG;
        }

        static class ViewHolder {
            Button bt;
            ProgressBar pg;
            TextView text1;
            TextView text2;
            TextView tv_all;
            TextView tv_last;
            TextView tv_percent;

            ViewHolder() {
            }
        }

        public void setRoomValue(int progress2, String all2, String last2, String percent2) {
            this.progress = progress2;
            this.all = all2;
            this.last = last2;
            this.percent = percent2;
        }

        public int getCount() {
            return AccountActivity.DATA.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = new ViewHolder();
            View convertView2 = this.viewMap.get(Integer.valueOf(position));
            if (convertView2 == null) {
                if (position == 6) {
                    convertView2 = this.mInflater.inflate((int) R.layout.account_lv_2, (ViewGroup) null);
                    holder.pg = (ProgressBar) convertView2.findViewById(R.id.pg_room);
                    holder.tv_all = (TextView) convertView2.findViewById(R.id.tv_all);
                    holder.tv_last = (TextView) convertView2.findViewById(R.id.tv_last);
                    holder.tv_percent = (TextView) convertView2.findViewById(R.id.tv_percent);
                } else if (position == 7) {
                    convertView2 = this.mInflater.inflate((int) R.layout.account_lv_3, (ViewGroup) null);
                    holder.bt = (Button) convertView2.findViewById(R.id.bt_zhuxiao);
                    holder.bt.setOnClickListener(this);
                } else {
                    convertView2 = this.mInflater.inflate((int) R.layout.account_lv_1, (ViewGroup) null);
                    holder.text1 = (TextView) convertView2.findViewById(R.id.tv_lv_actitle);
                    holder.text2 = (TextView) convertView2.findViewById(R.id.tv_lv_accontent);
                }
                convertView2.setTag(holder);
                this.viewMap.put(Integer.valueOf(position), convertView2);
            }
            ViewHolder holder2 = (ViewHolder) convertView2.getTag();
            if (position < 6) {
                holder2.text1.setText(AccountActivity.DATA[position]);
                holder2.text2.setText(AccountActivity.DATA2[position]);
            }
            if (position == 6) {
                holder2.pg.setProgress(this.progress);
                holder2.tv_all.setText(this.all);
                holder2.tv_last.setText(this.last);
                holder2.tv_percent.setText(this.percent);
            }
            return convertView2;
        }

        public void onClick(View v) {
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.account);
        Log.d(AccountActivity.class.getSimpleName(), "## start MyAccountActivity : ");
        initView();
    }

    /* access modifiers changed from: protected */
    public void reLoader() {
        closeProgressBar();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.receiver = new AccountReceiver();
        registerReceiver(this.receiver, new IntentFilter(EboxConst.EBOXBROAD), null, this.mHandler);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        unregisterReceiver(this.receiver);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 10:
                MyDialog mDialog = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.bt_ok:
                                int unused = AccountActivity.this.flag = 0;
                                AccountActivity.this.startConfigHanding();
                                dismiss();
                                return;
                            case R.id.bt_cancel:
                                dismiss();
                                return;
                            default:
                                return;
                        }
                    }
                };
                mDialog.setDefaultView();
                mDialog.setMessage(getResources().getString(R.string.del_device));
                return mDialog;
            default:
                return null;
        }
    }

    private void initView() {
        initTitleBar();
        this.midTV.setText(getString(R.string.my_account));
        this.adapter = new AccountAdapter(this) {
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.bt_zhuxiao:
                        if (!Utils.hasNet(AccountActivity.this)) {
                            AccountActivity.this.showDialog(10);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        };
        setListAdapter(this.adapter);
        getListView().setOnTouchListener(this);
        DATA2[0] = EboxContext.mDevice.getTeamName();
        DATA2[1] = EboxContext.mDevice.getUserName();
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                boolean success = ((Intent) msg.obj).getBooleanExtra("success", false);
                switch (msg.what) {
                    case 1:
                        AccountActivity.this.configEnd(success);
                        return;
                    default:
                        return;
                }
            }
        };
        getData();
    }

    private void getData() {
        this.flag = 1;
        if (!EboxContext.mSystemInfo.hasNet()) {
            HashMap<String, Object> user = ActionDB.getUserInfos(this);
            if (user != null) {
                EboxContext.configRet.setPhone(user.get("phone").toString());
                EboxContext.configRet.setEmail(user.get("email").toString());
                EboxContext.configRet.setCurrentSize(((Double) user.get("currentSize")).doubleValue());
                EboxContext.configRet.setSpaceSize(((Double) user.get("spaceSize")).doubleValue());
                EboxContext.configRet.setIsAdmin(((Integer) user.get("isAdmin")).intValue());
                loadData();
                return;
            }
            return;
        }
        startConfigHanding();
    }

    /* access modifiers changed from: protected */
    public void startConfigHanding() {
        this.mHandler.postDelayed(runnable(), 1);
    }

    /* access modifiers changed from: protected */
    public void configEnd(boolean success) {
        closeProgressBar();
        int icon = R.drawable.app_icon;
        if (!EboxContext.mSystemInfo.hasSdcard()) {
            icon = R.drawable.app_uncard;
        }
        if (success) {
            if (this.flag == 0) {
                Utils.reSetSystem(this);
                Utils.startService(this, EboxConst.ACTION_NORMAL, null, R.drawable.app_unnet);
                startActivity(new Intent(this, NewAccActivity.class));
                finish();
            } else if (this.flag == 1) {
                Utils.startService(this, EboxConst.ACTION_NORMAL, null, icon);
                loadData();
                if (!ActionDB.updateUserInfos(this, EboxContext.configRet)) {
                    Log.e(AccountActivity.class.getSimpleName(), "## update userinfo to db erorr : ");
                }
            }
            EboxContext.message = null;
        } else {
            httpFalse();
        }
        this.flag = 1;
    }

    public void loadData() {
        try {
            DATA2[2] = EboxContext.configRet.getPhone();
            DATA2[3] = EboxContext.configRet.getEmail();
        } catch (Exception e) {
        }
        if (EboxContext.configRet.getIsAdmin() == 1) {
            DATA2[4] = getResources().getString(R.string.yes);
        } else {
            DATA2[4] = getResources().getString(R.string.no);
        }
        DATA2[5] = Utils.toDate((int) EboxContext.configRet.getEndTime());
        double usedSpace = EboxContext.configRet.getCurrentSize();
        double totalSpace = EboxContext.configRet.getSpaceSize();
        this.adapter.setRoomValue((int) Math.ceil((usedSpace * 100.0d) / totalSpace), String.valueOf(Utils.getSize(Double.valueOf(totalSpace))), String.valueOf(Utils.getSize(Double.valueOf(usedSpace))), String.valueOf((int) Math.ceil((usedSpace * 100.0d) / totalSpace)) + "%");
        this.adapter.notifyDataSetChanged();
    }

    public class AccountReceiver extends BroadcastReceiver {
        public AccountReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("action");
            Log.d(AccountActivity.class.getSimpleName(), "## onReceive : " + action);
            if (EboxConst.ACTION_CONFIG.equals(action)) {
                AccountActivity.this.mHandler.sendMessage(AccountActivity.this.mHandler.obtainMessage(1, intent));
            }
        }
    }

    private Runnable runnable() {
        return new Runnable() {
            public void run() {
                if (AccountActivity.this.hasWorking()) {
                    AccountActivity.this.showProgressBar();
                    StandResult configRet = new StandResult();
                    configRet.setCode(Integer.valueOf(AccountActivity.this.flag));
                    Utils.startService(AccountActivity.this, EboxConst.ACTION_CONFIG, configRet, R.drawable.app_sync);
                }
                AccountActivity.this.mHandler.removeCallbacks(this);
            }
        };
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_zhuxiao:
                if (!Utils.hasNet(this)) {
                    showDialog(10);
                    return;
                }
                return;
            case R.id.retreat_btn:
                if (!EboxContext.threadIsable) {
                    EboxContext.mSystemNotify.setNotifyThreadWait(true);
                    showDialog(12);
                    return;
                }
                MainTabActivity.setCurrentTab(4);
                return;
            case R.id.refresh_btn:
                if (!Utils.hasNet(this)) {
                    getData();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
