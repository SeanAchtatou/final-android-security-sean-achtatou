package com.heyibao.android.ebox.model;

public class SpaceObject extends StandResult {
    private static final long serialVersionUID = 1;
    private double currentSize;
    private String email;
    private double endTime;
    private int isAdmin;
    private String phone;
    private double spaceSize;
    private double startTime;

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone2) {
        this.phone = phone2;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email2) {
        this.email = email2;
    }

    public double getSpaceSize() {
        return this.spaceSize;
    }

    public void setSpaceSize(double spaceSize2) {
        this.spaceSize = spaceSize2;
    }

    public double getCurrentSize() {
        return this.currentSize;
    }

    public void setCurrentSize(double currentSize2) {
        this.currentSize = currentSize2;
    }

    public int getIsAdmin() {
        return this.isAdmin;
    }

    public void setIsAdmin(int isAdmin2) {
        this.isAdmin = isAdmin2;
    }

    public double getStartTime() {
        return this.startTime;
    }

    public void setStartTime(double startTime2) {
        this.startTime = startTime2;
    }

    public double getEndTime() {
        return this.endTime;
    }

    public void setEndTime(double endTime2) {
        this.endTime = endTime2;
    }
}
