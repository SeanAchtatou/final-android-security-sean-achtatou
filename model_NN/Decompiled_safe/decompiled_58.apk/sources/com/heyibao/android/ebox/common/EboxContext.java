package com.heyibao.android.ebox.common;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Handler;
import com.heyibao.android.ebox.model.Details;
import com.heyibao.android.ebox.model.DeviceInfo;
import com.heyibao.android.ebox.model.FileBlockDataObject;
import com.heyibao.android.ebox.model.Json.JsonContacts;
import com.heyibao.android.ebox.model.SpaceObject;
import com.heyibao.android.ebox.model.SystemInfo;
import com.heyibao.android.ebox.model.SystemNotify;
import com.heyibao.android.ebox.model.UpGrade;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class EboxContext {
    public static FileBlockDataObject blockInfo;
    public static SpaceObject configRet = new SpaceObject();
    public static Activity curActivity = null;
    public static Map<String, SoftReference<Bitmap>> imageCache = new HashMap();
    public static Details mDetails = new Details();
    public static DeviceInfo mDevice = new DeviceInfo();
    public static JsonContacts mSyncContacts = new JsonContacts();
    public static SystemInfo mSystemInfo = new SystemInfo();
    public static SystemNotify mSystemNotify = new SystemNotify();
    public static UpGrade mUpGrade = new UpGrade();
    public static String message = null;
    public static boolean retread = false;
    public static boolean threadIsable = true;
    public static ThreadPoolExecutor threadPool = new ThreadPoolExecutor(2, 3, 5, TimeUnit.SECONDS, new ArrayBlockingQueue(10));
    public static Handler timerHandler = new Handler();
    public static Thread[] uploaderThread;
}
