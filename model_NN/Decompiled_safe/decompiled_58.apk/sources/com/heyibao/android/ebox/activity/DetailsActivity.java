package com.heyibao.android.ebox.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.db.ActionDB;
import com.heyibao.android.ebox.model.Details;
import com.heyibao.android.ebox.model.DetailsAdapter;
import com.heyibao.android.ebox.model.MyDialog;
import com.heyibao.android.ebox.util.Utils;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;

public class DetailsActivity extends ControlActivity {
    protected AdapterView.OnItemClickListener choiceListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View arg1, int which, long arg3) {
            if (which == 0) {
                DetailsActivity.this.showDialog(5);
            } else if (which == 1) {
                DetailsActivity.this.showDialog(6);
            }
            DetailsActivity.this.removeDialog(4);
        }
    };
    protected AdapterView.OnItemClickListener newListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View arg1, int which, long arg3) {
            if (which == 0) {
                DetailsActivity.this.onSubmitCreateDir();
            } else {
                UploadActivity.getInstance(DetailsActivity.this).newComments(which);
            }
            DetailsActivity.this.removeDialog(5);
        }
    };
    private DetailsReceiver receiver;
    protected AdapterView.OnItemClickListener uploadListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View arg1, int which, long arg3) {
            UploadActivity.getInstance(DetailsActivity.this).uploadComments(which);
            DetailsActivity.this.removeDialog(6);
        }
    };

    public class DetailsReceiver extends BroadcastReceiver {
        public DetailsReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("action");
            Log.d(DetailsActivity.class.getSimpleName(), "## onReceive : " + action);
            if (EboxConst.ACTION_LISTFILE.equals(action)) {
                DetailsActivity.this.mHandler.sendMessage(DetailsActivity.this.mHandler.obtainMessage(5, intent));
            } else if (EboxConst.ACTION_UPLOADING.equals(action)) {
                DetailsActivity.this.mHandler.sendMessage(DetailsActivity.this.mHandler.obtainMessage(7, intent));
            } else if (EboxConst.ACTION_UPLOAD.equals(action)) {
                DetailsActivity.this.mHandler.sendMessage(DetailsActivity.this.mHandler.obtainMessage(6, intent));
            } else if (EboxConst.ACTION_DOWNLOADING.equals(action)) {
                DetailsActivity.this.mHandler.sendMessage(DetailsActivity.this.mHandler.obtainMessage(9, intent));
            } else if (EboxConst.ACTION_DOWNLOAD.equals(action)) {
                DetailsActivity.this.mHandler.sendMessage(DetailsActivity.this.mHandler.obtainMessage(8, intent));
            } else if (EboxConst.ACTION_CREATEFOLDER.equals(action)) {
                DetailsActivity.this.mHandler.sendMessage(DetailsActivity.this.mHandler.obtainMessage(10, intent));
            } else if (EboxConst.ACTION_RENAME.equals(action)) {
                DetailsActivity.this.mHandler.sendMessage(DetailsActivity.this.mHandler.obtainMessage(11, intent));
            } else if (EboxConst.ACTION_DELETE.equals(action)) {
                DetailsActivity.this.mHandler.sendMessage(DetailsActivity.this.mHandler.obtainMessage(12, intent));
            } else if (EboxConst.ACTION_SENDEMAIL.equals(action)) {
                DetailsActivity.this.mHandler.sendMessage(DetailsActivity.this.mHandler.obtainMessage(13, intent));
            } else if (EboxConst.ACTION_LOGIN.equals(action)) {
                DetailsActivity.this.mHandler.sendMessage(DetailsActivity.this.mHandler.obtainMessage(4, intent));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.details_view);
        Log.d(DetailsActivity.class.getSimpleName(), "## create DetailsActivity now");
        controlInit();
    }

    public void onPrepareDialog(int id, Dialog dialog) {
        MyDialog mDialog = (MyDialog) dialog;
        switch (id) {
            case 9:
                mDialog.setMessage(Utils.replace(getResources().getString(R.string.del_htt_file), EboxContext.mDetails.getName()));
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 4:
                MyDialog mDialog = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                    }
                };
                mDialog.setListView(R.array.upload_choice, this.choiceListener);
                return mDialog;
            case 5:
                MyDialog mDialog2 = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                    }
                };
                mDialog2.setListView(R.array.upload_option_1, this.newListener);
                return mDialog2;
            case 6:
                MyDialog mDialog3 = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                    }
                };
                mDialog3.setListView(R.array.upload_option_2, this.uploadListener);
                return mDialog3;
            case 7:
                MyDialog mDialog4 = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.bt_ok:
                                if (!this.myEdit.isVerifiy()) {
                                    Utils.MessageBox(DetailsActivity.this, DetailsActivity.this.getResources().getString(R.string.file_error));
                                    return;
                                }
                                String value = this.myEdit.getMessage();
                                if (ActionDB.queryFileExist(DetailsActivity.this, value, DetailsActivity.this.curDetails.getShowUuid())) {
                                    DetailsActivity.this.startCreateDirHanding(value);
                                    dismiss();
                                    return;
                                }
                                Utils.MessageBox(DetailsActivity.this, DetailsActivity.this.getResources().getString(R.string.createdir_error2));
                                return;
                            case R.id.bt_cancel:
                                dismiss();
                                return;
                            default:
                                return;
                        }
                    }
                };
                mDialog4.setEditView(getString(R.string.add_folder));
                mDialog4.setMyTitle(getString(R.string.add_folder));
                return mDialog4;
            case 8:
                MyDialog mDialog5 = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.bt_ok:
                                if (!this.myEdit.isVerifiy()) {
                                    Utils.MessageBox(DetailsActivity.this, DetailsActivity.this.getResources().getString(R.string.file_error));
                                    return;
                                }
                                String value = this.myEdit.getMessage();
                                if (EboxContext.mDetails.getObjectType().endsWith(EboxConst.FILE)) {
                                    String suffix = Utils.getSuffix(EboxContext.mDetails.getName());
                                    if (!suffix.equals(EboxConst.UNKNOWN_ICON) && !value.toLowerCase().endsWith("." + suffix)) {
                                        value = value + "." + suffix;
                                    }
                                }
                                if (ActionDB.queryFileExist(DetailsActivity.this, value, null)) {
                                    EboxContext.mDetails.setMsg(value);
                                    DetailsActivity.this.startReNameHanding();
                                    dismiss();
                                    return;
                                }
                                Utils.MessageBox(DetailsActivity.this, DetailsActivity.this.getResources().getString(R.string.file_exist));
                                return;
                            case R.id.bt_cancel:
                                dismiss();
                                return;
                            default:
                                return;
                        }
                    }
                };
                mDialog5.setEditView(EboxContext.mDetails.getName());
                mDialog5.setMyTitle(getString(R.string.rename));
                return mDialog5;
            case 9:
                MyDialog mDialog6 = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.bt_ok:
                                DetailsActivity.this.onSubmitDel();
                                dismiss();
                                return;
                            case R.id.bt_cancel:
                                DetailsActivity.this.cancelFuncation(this);
                                return;
                            default:
                                return;
                        }
                    }
                };
                mDialog6.setDefaultView();
                mDialog6.setMessage(Utils.replace(getResources().getString(R.string.del_htt_file), EboxContext.mDetails.getName()));
                return mDialog6;
            case 10:
            case 11:
            default:
                return null;
            case 12:
                return stopDialog();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.receiver = new DetailsReceiver();
        registerReceiver(this.receiver, new IntentFilter(EboxConst.EBOXBROAD), null, this.mHandler);
        if (MainTabActivity.detailsAction == EboxConst.PROMPT_DEFAULT && this.detailsAdapter != null && this.arrList.size() == 0) {
            showPrompt();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        unregisterReceiver(this.receiver);
    }

    private void showPrompt() {
        MainTabActivity.setCurrentTab(8);
    }

    /* access modifiers changed from: protected */
    public void detailsView() {
        ListView detailsListView = (ListView) findViewById(16908298);
        detailsListView.setOnTouchListener(this);
        if (this.detailsAdapter == null) {
            this.detailsAdapter = new DetailsAdapter(this, this.arrList) {
                public void onClick(View v) {
                    if (v.getId() != R.id.bt_cancel_du) {
                        int position = DetailsActivity.this.detailsAdapter.getSelectedId();
                        EboxContext.mSystemInfo.setCurRowNum(position);
                        EboxContext.mDetails.setValue((Details) DetailsActivity.this.arrList.get(position));
                        if (!EboxContext.mDetails.getValue()) {
                            Utils.MessageBox(DetailsActivity.this, DetailsActivity.this.getResources().getString(R.string.selected_error));
                            return;
                        }
                        Log.d(DetailsActivity.class.getSimpleName(), "## detailsAdapter selected id " + position);
                    }
                    switch (v.getId()) {
                        case R.id.bt_cancel_du:
                            EboxContext.mSystemNotify.setNotifyThreadWait(true);
                            DetailsActivity.this.showDialog(12);
                            return;
                        case R.id.tv_msg:
                        case R.id.iv_msg:
                        case R.id.tv_size:
                        case R.id.item_btns_layout:
                        default:
                            return;
                        case R.id.open_btn:
                            if (EboxContext.mDetails.getObjectType().endsWith(EboxConst.FILE)) {
                                DetailsActivity.this.onSubmitDownload();
                                return;
                            } else {
                                DetailsActivity.this.forwardDir();
                                return;
                            }
                        case R.id.email_btn:
                            DetailsActivity.this.onSubmitEmail();
                            return;
                        case R.id.rename_btn:
                            DetailsActivity.this.onSubmitReName();
                            return;
                        case R.id.del_btn:
                            if (!Utils.hasNet(DetailsActivity.this)) {
                                DetailsActivity.this.showDialog(9);
                                return;
                            }
                            return;
                    }
                }
            };
            AnimationSet animationSet = new AnimationSet(true);
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(50);
            animationSet.addAnimation(alphaAnimation);
            TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
            translateAnimation.setDuration(100);
            animationSet.addAnimation(translateAnimation);
            detailsListView.setLayoutAnimation(new LayoutAnimationController(animationSet, 0.5f));
            detailsListView.setAdapter((ListAdapter) this.detailsAdapter);
        } else {
            Iterator i$ = this.arrList.iterator();
            while (i$.hasNext()) {
                this.detailsAdapter.add((Details) i$.next());
            }
        }
        detailsListView.setFocusableInTouchMode(true);
        getListView().setTranscriptMode(1);
        detailsListView.setFocusable(true);
        detailsListView.requestFocus();
        if (this.arrList.size() == 0) {
            showPrompt();
            return;
        }
        try {
            int position = EboxContext.mSystemInfo.getCurRowNum();
            if (200000 == position) {
                position = this.arrList.size() - 1;
            }
            detailsListView.setSelection(position);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.detailsAdapter.notifyDataSetChanged();
        TextView titleTV = (TextView) findViewById(R.id.title_path);
        if (this.curDetailsList.size() > 1) {
            titleTV.setVisibility(0);
        }
        String title = "    ";
        for (int i = 0; i < this.curDetailsList.size(); i++) {
            title = title + ((Details) this.curDetailsList.get(i)).getName() + File.separator;
        }
        titleTV.setText(title);
    }

    /* access modifiers changed from: protected */
    public void shareDetailsView() {
        ListView detailsListView = (ListView) findViewById(16908298);
        detailsListView.setOnTouchListener(this);
        this.detailsAdapter = new DetailsAdapter(this, this.arrList) {
            public void onClick(View v) {
                if (v.getId() != R.id.bt_cancel_du) {
                    int position = DetailsActivity.this.detailsAdapter.getSelectedId();
                    EboxContext.mSystemInfo.setCurRowNum(position);
                    EboxContext.mDetails.setValue((Details) DetailsActivity.this.arrList.get(position));
                    if (!EboxContext.mDetails.getValue()) {
                        Utils.MessageBox(DetailsActivity.this, DetailsActivity.this.getResources().getString(R.string.selected_error));
                        return;
                    }
                    Log.d(DetailsActivity.class.getSimpleName(), "## detailsAdapter selected id " + position);
                }
                switch (v.getId()) {
                    case R.id.bt_cancel_du:
                        EboxContext.mSystemNotify.setNotifyThreadWait(true);
                        DetailsActivity.this.showDialog(12);
                        return;
                    case R.id.tv_msg:
                    case R.id.iv_msg:
                    case R.id.tv_size:
                    case R.id.item_btns_layout:
                    default:
                        return;
                    case R.id.open_btn:
                        if (EboxContext.mDetails.getObjectType().endsWith(EboxConst.FILE)) {
                            DetailsActivity.this.onSubmitDownload();
                            return;
                        } else {
                            DetailsActivity.this.forwardDir();
                            return;
                        }
                    case R.id.email_btn:
                        DetailsActivity.this.onSubmitEmail();
                        return;
                    case R.id.rename_btn:
                        DetailsActivity.this.onSubmitReName();
                        return;
                    case R.id.del_btn:
                        if (!Utils.hasNet(DetailsActivity.this)) {
                            DetailsActivity.this.showDialog(9);
                            return;
                        }
                        return;
                }
            }
        };
        AnimationSet set = new AnimationSet(true);
        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(50);
        set.addAnimation(animation);
        Animation animation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
        animation2.setDuration(100);
        set.addAnimation(animation2);
        detailsListView.setLayoutAnimation(new LayoutAnimationController(set, 0.5f));
        detailsListView.setAdapter((ListAdapter) this.detailsAdapter);
        detailsListView.setFocusableInTouchMode(true);
        getListView().setTranscriptMode(1);
        detailsListView.setFocusable(true);
        detailsListView.requestFocus();
    }

    /* access modifiers changed from: private */
    public void onSubmitDel() {
        if (!EboxContext.mDetails.getObjectType().endsWith(EboxConst.FILE) && ActionDB.getFileListByParentUUID(this, EboxContext.mDetails.getShowUuid()).size() > 0) {
            Utils.MessageBox(this, getResources().getString(R.string.createdir_error1));
        } else if (isShareUuid()) {
            startDelHanding();
        }
    }

    /* access modifiers changed from: protected */
    public void onSubmitUpload() {
        if (!Utils.hasSdcard(this) && !Utils.hasNet(this)) {
            showDialog(4);
        }
    }

    /* access modifiers changed from: private */
    public void onSubmitDownload() {
        if (EboxContext.mDetails.hasSuccess()) {
            onSubmitOpen(EboxContext.mSystemInfo.getUserSdPath() + EboxContext.mDetails.getName(), EboxContext.mDetails.getIcon().intValue());
        } else if (!Utils.hasSdcard(this) && !Utils.hasNet(this)) {
            startDownloadHanding();
        }
    }

    /* access modifiers changed from: protected */
    public void onSubmitCreateDir() {
        if (!Utils.hasNet(this)) {
            showDialog(7);
        }
    }

    /* access modifiers changed from: protected */
    public void onSubmitReName() {
        if (!Utils.hasNet(this) && isShareUuid()) {
            removeDialog(8);
            showDialog(8);
        }
    }

    /* access modifiers changed from: private */
    public void onSubmitEmail() {
        if (!Utils.hasSdcard(this) && !Utils.hasNet(this)) {
            startSendEmailHanding();
        }
    }

    private boolean isShareUuid() {
        String type = EboxContext.mDetails.getObjectType();
        int find = type.indexOf(EboxConst.FOLDERSHARE);
        if (find == -1) {
            EboxContext.mDetails.setShareUuid(EboxConst.DEFAULTSHARE);
            EboxContext.mDetails.setObjectType(type.substring(type.length() - 1));
        } else {
            EboxContext.mDetails.setValue(ActionDB.getShareDetails(this, EboxContext.mDetails, find));
            if (!EboxContext.mDetails.getValue()) {
                Utils.MessageBox(this, getResources().getString(R.string.selected_error));
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void forwardDir() {
        if (hasWorking()) {
            this.detailsAdapter.reSet();
            this.curDetails = new Details(EboxContext.mDetails.getName(), R.drawable.back_up_level, EboxConst.RETREAT_ICON);
            this.curDetails.setShowUuid(EboxContext.mDetails.getShowUuid());
            this.curDetails.setParentShowUuid(EboxContext.mDetails.getParentShowUuid());
            this.curDetails.setObjectType(EboxContext.mDetails.getObjectType());
            this.curDetailsList.add(this.curDetails);
            this.retreatBtn.setVisibility(0);
            EboxContext.retread = true;
            EboxContext.mSystemInfo.setCurRowNum(-1);
            getFileDetails();
        }
    }

    /* access modifiers changed from: protected */
    public void retreatDir() {
        if (hasWorking()) {
            this.detailsAdapter.reSet();
            ((TextView) findViewById(R.id.title_path)).setVisibility(8);
            Details retreatDetails = (Details) this.curDetailsList.get(this.curDetailsList.size() - 2);
            this.curDetails = new Details(null, 0, null);
            this.curDetails.setShowUuid(retreatDetails.getShowUuid());
            this.curDetails.setParentShowUuid(retreatDetails.getParentShowUuid());
            this.curDetailsList.remove(this.curDetailsList.size() - 1);
            if (this.curDetailsList.size() < 2) {
                reCurDetails();
                this.retreatBtn.setVisibility(4);
                EboxContext.retread = false;
            }
            EboxContext.mSystemInfo.setCurRowNum(-1);
            startGetDBHanding();
        }
    }

    /* access modifiers changed from: protected */
    public void onSubmitRefresh() {
        if (!Utils.hasNet(this)) {
            startListFileHanding();
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Log.d(DetailsActivity.class.getSimpleName(), "## list click item : " + position);
        if (EboxContext.threadIsable) {
            Details mDetails = (Details) this.arrList.get(position);
            EboxContext.mDetails.setValue(mDetails);
            if (mDetails.getObjectType().endsWith(EboxConst.FOLDER)) {
                forwardDir();
                return;
            }
            this.detailsAdapter.setSelectedId(position);
            onSubmitDownload();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        HashMap<String, Object> thisHash;
        super.onActivityResult(requestCode, resultCode, data);
        boolean success = false;
        try {
            UploadActivity ua = UploadActivity.getInstance(this);
            switch (requestCode) {
                case EboxConst.SELFILE /*200000*/:
                    if (resultCode == -1) {
                        ua.getMedia(data.getStringExtra("path"), data.getIntExtra(EboxConst.CATCHICON, R.drawable.text));
                        if (EboxContext.mDetails.getPath() != null) {
                            success = true;
                            break;
                        }
                    }
                    break;
                case EboxConst.SELIMG /*200001*/:
                    if (data != null) {
                        ua.getMedia(data.getData());
                        if (EboxContext.mDetails.getPath() != null) {
                            success = true;
                            break;
                        }
                    }
                    break;
                case EboxConst.SELVID /*200002*/:
                    if (data != null) {
                        ua.getMedia(data.getData());
                        if (EboxContext.mDetails.getPath() != null) {
                            success = true;
                            break;
                        }
                    }
                    break;
                case EboxConst.TAKEPHONE /*200003*/:
                    if (resultCode == -1) {
                        ua.getMedia(ua.getFilePath(), R.drawable.bt_jpg);
                        if (EboxContext.mDetails.getPath() != null) {
                            success = true;
                            break;
                        }
                    }
                    break;
                case EboxConst.MAKEVID /*200004*/:
                    if (!(resultCode != -1 || data == null || (thisHash = ActionDB.getThumbnail(this, data.getData())) == null)) {
                        String fileName = Utils.toDate(System.currentTimeMillis()) + EboxConst.VIDEOSUFFIX;
                        if (Utils.toCopy(thisHash.get("path").toString(), fileName)) {
                            ua.getMedia(EboxContext.mSystemInfo.getUserSdPath() + fileName, R.drawable.bt_avi);
                            if (EboxContext.mDetails.getPath() != null) {
                                success = true;
                                break;
                            }
                        }
                    }
                    break;
            }
            if (success) {
                startUploadHanding();
                return;
            }
            Utils.toDelFile(this.curDetails.getPath() + EboxConst.VIDEOSUFFIX);
            Utils.MessageBox(this, getResources().getString(R.string.getfile_error));
        } catch (Exception e) {
            Exception e2 = e;
            e2.printStackTrace();
            Log.e(DetailsActivity.class.getSimpleName(), "## create camera error : " + e2.getMessage());
        }
    }

    public void onClick(View v) {
        EboxContext.mDetails.reSet();
        switch (v.getId()) {
            case R.id.retreat_btn:
                if (this.curDetailsList.size() >= 2) {
                    retreatDir();
                    return;
                }
                return;
            case R.id.title_text:
            default:
                return;
            case R.id.refresh_btn:
                onSubmitRefresh();
                return;
        }
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        try {
            if (e1.getX() - e2.getX() > 160.0f) {
                if (!EboxContext.threadIsable) {
                    EboxContext.mSystemNotify.setNotifyThreadWait(true);
                    showDialog(12);
                } else {
                    MainTabActivity.setCurrentTab(true);
                }
                return true;
            }
            if (e1.getX() - e2.getX() < -160.0f) {
                if (this.curDetailsList.size() >= 2) {
                    retreatDir();
                }
                return true;
            }
            return false;
        } catch (Exception e) {
            Log.e(DetailsActivity.class.getSimpleName(), "## onFling error : " + e.getMessage());
        }
    }
}
