package com.heyibao.android.ebox.model;

public class BlockInfo {
    private int count;
    private int offset;
    private String signature;

    public int getOffset() {
        return this.offset;
    }

    public void setOffset(int offset2) {
        this.offset = offset2;
    }

    public int getCount() {
        return this.count;
    }

    public void setCount(int count2) {
        this.count = count2;
    }

    public void setSignature(String signature2) {
        this.signature = signature2;
    }

    public String getSignature() {
        return this.signature;
    }
}
