package com.heyibao.android.ebox.model;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.heyibao.android.ebox.R;

public class ImageBtlo extends LinearLayout {
    private ImageView iv;
    private TextView tv;

    public ImageBtlo(Context context) {
        this(context, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.heyibao.android.ebox.model.ImageBtlo, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ImageBtlo(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate((int) R.layout.custombt_login, (ViewGroup) this, true);
        this.iv = (ImageView) findViewById(R.id.iv);
        this.tv = (TextView) findViewById(R.id.tv);
    }

    public void setImageResource(int resId) {
        this.iv.setImageResource(resId);
    }

    public void setTextViewText(String text) {
        this.tv.setText(text);
    }
}
