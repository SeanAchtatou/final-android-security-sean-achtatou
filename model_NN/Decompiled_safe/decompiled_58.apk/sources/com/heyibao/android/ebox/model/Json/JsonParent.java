package com.heyibao.android.ebox.model.Json;

import com.heyibao.android.ebox.model.MyException;
import org.json.JSONObject;

public class JsonParent {
    protected JSONObject json;
    protected String message;

    public JsonParent() {
    }

    public JsonParent(JSONObject json2) {
        this.json = json2;
        this.message = null;
    }

    public boolean getStatus() {
        if (this.json == null) {
            return false;
        }
        if (this.json.optString(JsonModel.STATUS).equals("ok")) {
            return true;
        }
        this.message = this.json.optString(JsonModel.ERROR);
        return false;
    }

    public int getId() {
        if (this.json == null) {
            return -1;
        }
        return this.json.optInt("id");
    }

    public String getModifiedTime() {
        if (this.json == null) {
            return null;
        }
        return this.json.optString("modified");
    }

    public void setMessage(String message2) {
        this.message = message2;
    }

    public String getMessage() {
        return this.message;
    }

    public Object getData(String item) throws MyException {
        if (this.json == null) {
            return null;
        }
        return this.json.opt(item);
    }

    public String toString() {
        return this.json.toString();
    }
}
