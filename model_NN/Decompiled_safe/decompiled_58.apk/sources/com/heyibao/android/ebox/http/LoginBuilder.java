package com.heyibao.android.ebox.http;

import ClientProtocol.ClientMeta;
import android.app.Service;
import android.util.Log;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;

public abstract class LoginBuilder implements Runnable, HttpInterface {
    private Service activity;
    private String deviceInfo;
    private String deviceName;
    private int flag;
    private String passWord;
    private String teamName;
    private String userName;

    public LoginBuilder(Service activity2, String deviceName2, String deviceInfo2, String teamName2, String userName2, String passWord2, int flag2) {
        this.activity = activity2;
        this.teamName = teamName2;
        this.userName = userName2;
        this.passWord = passWord2;
        this.deviceName = deviceName2;
        this.deviceInfo = deviceInfo2;
        this.flag = flag2;
    }

    public void run() {
        Log.d(LoginBuilder.class.getSimpleName(), "## start login");
        loginStart();
        Log.d(LoginBuilder.class.getSimpleName(), "end LoginBuilder ");
    }

    private void loginStart() {
        int errcount = 0;
        boolean success = false;
        String url = EboxConst.LOGIN_URL;
        while (true) {
            if (Thread.interrupted() || errcount >= 3) {
                break;
            }
            try {
                ClientMeta.DeviceInfoRequest.Builder request = ClientMeta.DeviceInfoRequest.newBuilder();
                request.setDomain(this.activity.getResources().getString(R.string.domain));
                request.setDeviceType(4);
                request.setDeviceName(this.deviceName);
                request.setDeviceInfo(this.deviceInfo);
                request.setPassword(this.passWord);
                if (this.flag == 3) {
                    url = EboxConst.LOGIN_URL_1;
                    request.setTeamName(EboxConst.TAB_UPLOADER_TAG);
                    request.setUserName(this.userName);
                } else {
                    request.setTeamName(this.teamName);
                    request.setUserName(this.userName);
                }
                Thread.sleep(10);
                ClientMeta.DeviceInfoResponse response = ClientMeta.DeviceInfoResponse.parseFrom(new GPBUtils(url).getResult(request.build().toByteArray()));
                ClientMeta.Status status = response.getStatus();
                if (!status.getSuccess()) {
                    Log.e(LoginBuilder.class.getSimpleName(), " ## wrong: " + status.getMsg());
                    EboxContext.message = status.getMsg();
                    break;
                }
                success = true;
                EboxContext.mDevice.setToken(response.getToken());
                EboxContext.mDevice.setSecret(response.getSecret());
                EboxContext.mDevice.setNick(response.getNick());
                EboxContext.mDevice.setSession(response.getSession());
                EboxContext.mDevice.setCode(Integer.valueOf(this.flag));
                EboxContext.mDevice.setSuccess(true);
                if (this.flag == 3) {
                    EboxContext.mDevice.setUserName(response.getUserName());
                    EboxContext.mDevice.setTeamName(response.getTeamName());
                }
                Log.d(LoginBuilder.class.getSimpleName(), "## token : " + EboxContext.mDevice.getToken());
                Log.d(LoginBuilder.class.getSimpleName(), "## secret : " + EboxContext.mDevice.getSecret());
                Log.d(LoginBuilder.class.getSimpleName(), "## nick : " + EboxContext.mDevice.getNick());
                Log.d(LoginBuilder.class.getSimpleName(), "## session : " + EboxContext.mDevice.getSession());
                Log.d(LoginBuilder.class.getSimpleName(), "## team : " + EboxContext.mDevice.getUserName());
                Log.d(LoginBuilder.class.getSimpleName(), "## user : " + EboxContext.mDevice.getTeamName());
                Log.d(LoginBuilder.class.getSimpleName(), "## password : " + EboxContext.mDevice.getPassWord());
                Log.d(LoginBuilder.class.getSimpleName(), "## deviceName : " + EboxContext.mDevice.getDeviceName());
                Log.d(LoginBuilder.class.getSimpleName(), "## deviceInfo : " + EboxContext.mDevice.getDeviceInfo());
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(LoginBuilder.class.getSimpleName(), "## LoginBuilder is Force stop");
                EboxContext.message = this.activity.getString(R.string.stop_thread);
            } catch (Exception e2) {
                Exception error = e2;
                error.printStackTrace();
                EboxContext.message = this.activity.getResources().getString(R.string.net_error);
                Log.e(LoginBuilder.class.getSimpleName(), "## login error info: " + error.getMessage() + " for " + errcount);
                errcount++;
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
            }
        }
        reportSuccess(success);
    }
}
