package com.heyibao.android.ebox.model;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.heyibao.android.ebox.R;

public abstract class MyDialog extends Dialog implements View.OnClickListener {
    private int RESOUCRE = R.layout.custom_dialog_confirm;
    private Button cancelBtn;
    private Context context;
    private boolean init = false;
    private ListView listView;
    private TextView messageView;
    public EditView myEdit;
    private Button okBtn;
    public TextView proTV;
    public ProgressBar progress;
    public String temp;
    private TextView titleView;
    private View view;

    public MyDialog(Context context2) {
        super(context2);
        this.context = context2;
    }

    public MyDialog(Context context2, int theme) {
        super(context2, theme);
        this.context = context2;
    }

    public MyDialog(Context context2, boolean cancelable, DialogInterface.OnCancelListener cancelListener) {
        super(context2, cancelable, cancelListener);
        this.context = context2;
    }

    public void setDefaultView() {
        this.init = true;
        this.view = View.inflate(this.context, this.RESOUCRE, null);
        setContentView(this.view);
        if (this.okBtn == null) {
            this.okBtn = (Button) this.view.findViewById(R.id.bt_ok);
            this.okBtn.setOnClickListener(this);
        }
        if (this.cancelBtn == null) {
            this.cancelBtn = (Button) this.view.findViewById(R.id.bt_cancel);
            this.cancelBtn.setOnClickListener(this);
        }
        setCancelable(true);
        setCanceledOnTouchOutside(true);
    }

    public void setMyTitle(String title) {
        if (this.view != null) {
            this.titleView = (TextView) this.view.findViewById(R.id.tv_title);
            this.titleView.setText(title);
        }
    }

    public void setMessage(String msg) {
        if (this.view != null && this.RESOUCRE != R.layout.custom_dialog_list) {
            this.messageView = (TextView) this.view.findViewById(R.id.tv_message);
            this.messageView.setText(msg);
            this.messageView.setMovementMethod(ScrollingMovementMethod.getInstance());
        }
    }

    public void reProgress() {
        if (this.view != null) {
            if (this.progress == null) {
                this.progress = (ProgressBar) this.view.findViewById(R.id.progress_bar);
            }
            if (this.proTV == null) {
                this.proTV = (TextView) this.view.findViewById(R.id.tv_percent);
            }
            this.proTV.setText("0%");
            this.progress.setProgress(0);
        }
    }

    public void reProgressLoading() {
        if (this.view != null) {
            this.progress = (ProgressBar) this.view.findViewById(R.id.progress_bar);
            this.progress.invalidate();
        }
    }

    public void setProgress(int val) {
        if (this.view != null) {
            if (this.progress == null) {
                this.progress = (ProgressBar) this.view.findViewById(R.id.progress_bar);
            }
            if (this.proTV == null) {
                this.proTV = (TextView) this.view.findViewById(R.id.tv_percent);
            }
            this.proTV.setText(val + "%");
            if (this.progress.getProgress() < val) {
                this.progress.setProgress(val);
            }
        }
    }

    public void setEditView(String text) {
        this.RESOUCRE = R.layout.custom_dialog_edit;
        setDefaultView();
        this.myEdit = (EditView) this.view.findViewById(R.id.ed_dg);
        this.myEdit.isObjectEdt();
        this.myEdit.setText(text);
        this.temp = text;
    }

    public void setListView(int array, AdapterView.OnItemClickListener cl) {
        this.RESOUCRE = R.layout.custom_dialog_list;
        this.init = true;
        this.view = View.inflate(this.context, this.RESOUCRE, null);
        setContentView(this.view);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.context, (int) R.layout.dialog_row, this.context.getResources().getStringArray(array));
        this.listView = (ListView) this.view.findViewById(R.id.lv_dialog);
        this.listView.setAdapter((ListAdapter) adapter);
        this.listView.setOnItemClickListener(cl);
        setCancelable(true);
        setCanceledOnTouchOutside(true);
    }

    public void setProgressView() {
        this.RESOUCRE = R.layout.custom_dialog_progress;
        setDefaultView();
        this.progress = (ProgressBar) this.view.findViewById(R.id.progress_bar);
    }

    public void setProgressLoading() {
        this.RESOUCRE = R.layout.custom_dialog_progress_loading;
        this.init = true;
        this.view = View.inflate(this.context, this.RESOUCRE, null);
        setContentView(this.view);
        if (this.cancelBtn == null) {
            this.cancelBtn = (Button) this.view.findViewById(R.id.bt_cancel);
            this.cancelBtn.setOnClickListener(this);
        }
        this.progress = (ProgressBar) this.view.findViewById(R.id.progress_bar);
    }

    public void show() {
        if (!this.init) {
            setDefaultView();
        }
        super.show();
    }
}
