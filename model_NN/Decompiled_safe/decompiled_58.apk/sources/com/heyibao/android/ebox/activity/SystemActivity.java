package com.heyibao.android.ebox.activity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.db.ActionDB;
import com.heyibao.android.ebox.model.MyDialog;
import com.heyibao.android.ebox.model.SystemInfo;
import com.heyibao.android.ebox.util.Utils;
import java.util.HashMap;
import java.util.Map;

public class SystemActivity extends HomeMenuActivity {
    /* access modifiers changed from: private */
    public static boolean[] BOOL = {EboxContext.mSystemInfo.hasAutoLogin(), EboxContext.mSystemInfo.hasOffLine(), EboxContext.mSystemInfo.hasNotify(), EboxContext.mSystemInfo.hasAutoUpgrade(), false, false};
    /* access modifiers changed from: private */
    public static String[] DATA;
    /* access modifiers changed from: private */
    public static String[] DATA2;
    /* access modifiers changed from: private */
    public static long[] DATAX;
    /* access modifiers changed from: private */
    public static SystemActivity activity;
    /* access modifiers changed from: private */
    public static ArrayAdapter<String> adapter;
    /* access modifiers changed from: private */
    public static boolean clickCatch = false;
    /* access modifiers changed from: private */
    public static boolean hasCatch1 = true;
    /* access modifiers changed from: private */
    public static boolean hasCatch2 = true;
    /* access modifiers changed from: private */
    public EfficientAdapter dp;
    private Runnable runnable;

    private static class EfficientAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private Map<Integer, View> viewMap = new HashMap();

        static class ViewHolder {
            ProgressBar pgUsrDown;
            ProgressBar pgUsrcache;
            Spinner spinner;
            TextView text1;
            TextView text2;
            TextView textCache;
            TextView textDown;
            CheckBox togBtn;
            Button userCatch;
            Button userDown;

            ViewHolder() {
            }
        }

        public EfficientAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return SystemActivity.DATA.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* access modifiers changed from: private */
        public void saveConfig() {
            boolean z;
            if (!ActionDB.updateConfigs(SystemActivity.activity, SystemActivity.BOOL, EboxContext.mSystemInfo.getOptionRefre())) {
                Utils.MessageBox(SystemActivity.activity, SystemActivity.activity.getString(R.string.db_error));
                return;
            }
            EboxContext.mSystemInfo.setAutoLogin(SystemActivity.BOOL[0]);
            EboxContext.mSystemInfo.setAutoUpgrade(SystemActivity.BOOL[3]);
            if (EboxContext.mSystemInfo.hasOffLine() == SystemActivity.BOOL[1]) {
                EboxContext.mSystemInfo.setOffLine(!SystemActivity.BOOL[1]);
                SystemInfo systemInfo = EboxContext.mSystemInfo;
                if (!SystemActivity.BOOL[1]) {
                    z = true;
                } else {
                    z = false;
                }
                systemInfo.setNet(z);
                if (EboxContext.mSystemInfo.hasNet()) {
                    Utils.startService(SystemActivity.activity, EboxConst.ACTION_NORMAL, null, 0);
                } else {
                    Utils.startService(SystemActivity.activity, EboxConst.ACTION_NORMAL, null, R.drawable.app_unnet);
                }
            }
            if (EboxContext.mSystemInfo.hasNotify() != SystemActivity.BOOL[2]) {
                EboxContext.mSystemInfo.setNotify(SystemActivity.BOOL[2]);
                Utils.startService(SystemActivity.activity, EboxConst.ACTION_NORMAL, null, 0);
            }
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            View convertView2 = this.viewMap.get(Integer.valueOf(position));
            if (convertView2 == null) {
                ViewHolder holder = new ViewHolder();
                if (position == 5) {
                    convertView2 = this.mInflater.inflate((int) R.layout.system_row_1, (ViewGroup) null);
                    holder.textDown = (TextView) convertView2.findViewById(R.id.tv_usrdown_data);
                    holder.textCache = (TextView) convertView2.findViewById(R.id.tv_usrcache_data);
                    String catch1 = Utils.getSize(Double.valueOf((double) SystemActivity.DATAX[0]));
                    String catch2 = Utils.getSize(Double.valueOf((double) SystemActivity.DATAX[1]));
                    holder.textDown.setText(catch1);
                    holder.textCache.setText(catch2);
                    holder.pgUsrDown = (ProgressBar) convertView2.findViewById(R.id.pg_usrdown);
                    holder.pgUsrcache = (ProgressBar) convertView2.findViewById(R.id.pg_usrcache);
                    holder.userDown = (Button) convertView2.findViewById(R.id.bt_clearusr);
                    holder.userCatch = (Button) convertView2.findViewById(R.id.bt_clearusrcache);
                    holder.pgUsrDown.setMax(((int) SystemActivity.DATAX[0]) * 8);
                    holder.pgUsrDown.setProgress((int) SystemActivity.DATAX[0]);
                    holder.pgUsrcache.setMax(((int) SystemActivity.DATAX[0]) * 7);
                    holder.pgUsrcache.setProgress((int) SystemActivity.DATAX[1]);
                    holder.userDown.setEnabled(SystemActivity.hasCatch1);
                    holder.userCatch.setEnabled(SystemActivity.hasCatch2);
                    holder.userDown.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            boolean unused = SystemActivity.clickCatch = true;
                            SystemActivity.activity.showDialog(9);
                        }
                    });
                    holder.userCatch.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            boolean unused = SystemActivity.clickCatch = false;
                            SystemActivity.activity.showDialog(9);
                        }
                    });
                } else if (position == 4) {
                    convertView2 = this.mInflater.inflate((int) R.layout.system_row_0, (ViewGroup) null);
                    holder.text1 = (TextView) convertView2.findViewById(R.id.text_view_1);
                    holder.text2 = (TextView) convertView2.findViewById(R.id.text_view_2);
                    holder.togBtn = (CheckBox) convertView2.findViewById(R.id.check_b);
                    holder.spinner = (Spinner) convertView2.findViewById(R.id.spinner);
                    holder.togBtn.setVisibility(8);
                    holder.spinner.setVisibility(0);
                    holder.text1.setText(SystemActivity.DATA[position]);
                    holder.text2.setText(SystemActivity.DATA2[position]);
                    holder.spinner.setAdapter((SpinnerAdapter) SystemActivity.adapter);
                    holder.spinner.setSelection(EboxContext.mSystemInfo.getOptionRefre());
                    holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                            EboxContext.mSystemInfo.setOptionRefre(arg2);
                            EfficientAdapter.this.saveConfig();
                        }

                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });
                } else {
                    convertView2 = this.mInflater.inflate((int) R.layout.system_row_0, (ViewGroup) null);
                    holder.text1 = (TextView) convertView2.findViewById(R.id.text_view_1);
                    holder.text2 = (TextView) convertView2.findViewById(R.id.text_view_2);
                    holder.togBtn = (CheckBox) convertView2.findViewById(R.id.check_b);
                    holder.spinner = (Spinner) convertView2.findViewById(R.id.spinner);
                    holder.togBtn.setVisibility(0);
                    holder.spinner.setVisibility(8);
                    holder.togBtn.setChecked(SystemActivity.BOOL[position]);
                    holder.togBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            SystemActivity.BOOL[position] = !SystemActivity.BOOL[position];
                            EfficientAdapter.this.saveConfig();
                        }
                    });
                    holder.text1.setText(SystemActivity.DATA[position]);
                    holder.text2.setText(SystemActivity.DATA2[position]);
                }
                convertView2.setTag(holder);
                this.viewMap.put(Integer.valueOf(position), convertView2);
            } else {
                ViewHolder holder2 = (ViewHolder) convertView2.getTag();
                if (position == 5) {
                    String catch12 = Utils.getSize(Double.valueOf((double) SystemActivity.DATAX[0]));
                    String catch22 = Utils.getSize(Double.valueOf((double) SystemActivity.DATAX[1]));
                    holder2.textDown.setText(catch12);
                    holder2.textCache.setText(catch22);
                    holder2.pgUsrDown = (ProgressBar) convertView2.findViewById(R.id.pg_usrdown);
                    holder2.pgUsrcache = (ProgressBar) convertView2.findViewById(R.id.pg_usrcache);
                    holder2.userDown = (Button) convertView2.findViewById(R.id.bt_clearusr);
                    holder2.userCatch = (Button) convertView2.findViewById(R.id.bt_clearusrcache);
                    holder2.pgUsrDown.setMax(((int) SystemActivity.DATAX[0]) * 8);
                    holder2.pgUsrDown.setProgress((int) SystemActivity.DATAX[0]);
                    holder2.pgUsrcache.setMax(((int) SystemActivity.DATAX[0]) * 7);
                    holder2.pgUsrcache.setProgress((int) SystemActivity.DATAX[1]);
                    holder2.userDown.setEnabled(SystemActivity.hasCatch1);
                    holder2.userCatch.setEnabled(SystemActivity.hasCatch2);
                    holder2.userDown.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            boolean unused = SystemActivity.clickCatch = true;
                            SystemActivity.activity.showDialog(9);
                        }
                    });
                    holder2.userCatch.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            boolean unused = SystemActivity.clickCatch = false;
                            SystemActivity.activity.showDialog(9);
                        }
                    });
                }
            }
            return convertView2;
        }
    }

    public void onPrepareDialog(int id, Dialog dialog) {
        MyDialog mDialog = (MyDialog) dialog;
        switch (id) {
            case 9:
                if (clickCatch) {
                    mDialog.setMessage(getString(R.string.verify_remove_1));
                    return;
                } else {
                    mDialog.setMessage(getString(R.string.verify_remove_2));
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 9:
                MyDialog mDialog = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.bt_ok:
                                if (SystemActivity.clickCatch) {
                                    Utils.removeCatch(EboxContext.mSystemInfo.getUserSdPath());
                                    EboxContext.mSystemNotify.setNotifyListDetails(true);
                                    EboxContext.mSystemNotify.setNotifyDownload(true);
                                    SystemActivity.DATAX[0] = 0;
                                    boolean unused = SystemActivity.hasCatch1 = false;
                                } else {
                                    EboxContext.imageCache.clear();
                                    SystemActivity.DATAX[1] = 0;
                                    boolean unused2 = SystemActivity.hasCatch2 = false;
                                }
                                SystemActivity.this.dp.notifyDataSetChanged();
                                dismiss();
                                return;
                            case R.id.bt_cancel:
                                dismiss();
                                return;
                            default:
                                return;
                        }
                    }
                };
                mDialog.setDefaultView();
                mDialog.setMessage(getResources().getString(R.string.verify_remove_2));
                return mDialog;
            case 10:
            case 11:
            default:
                return null;
            case 12:
                return stopDialog();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.system_view);
        activity = this;
        Log.d(SystemActivity.class.getSimpleName(), "## start MySettingActivity : ");
        settingInit();
    }

    /* access modifiers changed from: protected */
    public void reLoader() {
        if (EboxContext.mSystemNotify.hasNotifySystem()) {
            EboxContext.mSystemNotify.setNotifySystem(false);
            this.mHandler.postDelayed(this.runnable, 500);
        }
    }

    private void settingInit() {
        initTitleBar();
        this.midTV.setText(getString(R.string.set_system));
        this.refreshBtn.setVisibility(4);
        adapter = new ArrayAdapter<>(this, 17367048, getResources().getStringArray(R.array.update_freq_options));
        adapter.setDropDownViewResource(17367049);
        this.runnable = getDataRunnable();
        this.mHandler.postDelayed(this.runnable, 50);
    }

    private Runnable getDataRunnable() {
        return new Runnable() {
            public void run() {
                boolean z;
                boolean z2;
                long data1 = Utils.getCatch(EboxContext.mSystemInfo.getUserSdPath());
                long data2 = Utils.getCatch(EboxContext.mSystemInfo.getSdPath() + EboxConst.CATCHFODLER);
                if (data1 != 0) {
                    z = true;
                } else {
                    z = false;
                }
                boolean unused = SystemActivity.hasCatch1 = z;
                if (data2 != 0) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                boolean unused2 = SystemActivity.hasCatch2 = z2;
                String[] unused3 = SystemActivity.DATA = new String[SystemActivity.BOOL.length];
                String[] unused4 = SystemActivity.DATA2 = new String[SystemActivity.BOOL.length];
                long[] unused5 = SystemActivity.DATAX = new long[2];
                SystemActivity.DATA[0] = SystemActivity.this.getString(R.string.system_login);
                SystemActivity.DATA[1] = SystemActivity.this.getString(R.string.system_offline);
                SystemActivity.DATA[2] = SystemActivity.this.getString(R.string.system_notfiy);
                SystemActivity.DATA[3] = SystemActivity.this.getString(R.string.system_update);
                SystemActivity.DATA[4] = SystemActivity.this.getString(R.string.system_refresh);
                SystemActivity.DATA2[0] = SystemActivity.this.getString(R.string.system_login_l);
                SystemActivity.DATA2[1] = SystemActivity.this.getString(R.string.system_offline_l);
                SystemActivity.DATA2[2] = SystemActivity.this.getString(R.string.system_notfiy_l);
                SystemActivity.DATA2[3] = SystemActivity.this.getString(R.string.system_update_l);
                SystemActivity.DATA2[4] = SystemActivity.this.getString(R.string.system_refresh_l);
                SystemActivity.DATAX[0] = data1;
                SystemActivity.DATAX[1] = data2;
                ListView lv = (ListView) SystemActivity.activity.findViewById(16908298);
                EfficientAdapter unused6 = SystemActivity.this.dp = new EfficientAdapter(SystemActivity.activity);
                lv.setAdapter((ListAdapter) SystemActivity.this.dp);
                lv.setOnTouchListener(SystemActivity.activity);
                SystemActivity.this.mHandler.removeCallbacks(this);
            }
        };
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retreat_btn:
                MainTabActivity.setCurrentTab(4);
                return;
            default:
                return;
        }
    }
}
