package com.heyibao.android.ebox.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.model.DeviceInfo;
import com.heyibao.android.ebox.model.EditView;
import com.heyibao.android.ebox.model.ImageBtlo;
import com.heyibao.android.ebox.util.Utils;

public class LoginActivity extends Activity {
    private CheckBox autoLogin;
    /* access modifiers changed from: private */
    public int flag;
    private LinearLayout mainLayout;
    private CheckBox offLineLogin;
    private EditView pasdEdit;
    private EditView teamEdit;
    private EditView userEdit;
    private EditView verifypasdEdit;

    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(null);
        if (getResources().getConfiguration().orientation == 2) {
            this.mainLayout.setBackgroundResource(R.drawable.login_bg_land2);
        } else if (getResources().getConfiguration().orientation == 1) {
            this.mainLayout.setBackgroundResource(R.drawable.login_bg3);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        onConfigurationChanged(getResources().getConfiguration());
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.login);
        Log.d(LoginActivity.class.getSimpleName(), "## start LoginActivity ");
        LoginInit();
    }

    private void LoginInit() {
        this.mainLayout = (LinearLayout) findViewById(R.id.auto_panel);
        EboxContext.curActivity = this;
        Utils.startService(this, EboxConst.ACTION_NORMAL, null, R.drawable.app_unnet);
        this.flag = getIntent().getIntExtra("flag", -1);
        DeviceInfo user = (DeviceInfo) getIntent().getSerializableExtra("user");
        setTitle(((Object) getText(R.string.app_name)) + " - " + ((Object) getText(R.string.login)));
        TextView titleTV = (TextView) findViewById(R.id.l_section1);
        TextView userNameTV = (TextView) findViewById(R.id.l_username);
        this.userEdit = (EditView) findViewById(R.id.username);
        this.userEdit.isNameEdt();
        this.pasdEdit = (EditView) findViewById(R.id.password);
        this.pasdEdit.isPassWord();
        this.autoLogin = (CheckBox) findViewById(R.id.check_auto);
        this.autoLogin.setChecked(EboxContext.mSystemInfo.hasAutoLogin());
        this.offLineLogin = (CheckBox) findViewById(R.id.check_offline);
        ImageBtlo saveButton = (ImageBtlo) findViewById(R.id.bt_confirm);
        ImageBtlo cancelButton = (ImageBtlo) findViewById(R.id.bt_cancel);
        cancelButton.setImageResource(R.drawable.cancel);
        cancelButton.setTextViewText(getString(R.string.cancel));
        if (this.flag == 1) {
            titleTV.setText(getText(R.string.registration).toString());
            TextView teamNameTV = (TextView) findViewById(R.id.l_teamname);
            teamNameTV.setText(getText(R.string.email).toString());
            teamNameTV.setVisibility(0);
            this.teamEdit = (EditView) findViewById(R.id.teamname);
            this.teamEdit.isEmailEdt();
            ((TextView) findViewById(R.id.l_username)).setText(getText(R.string.regist_nick).toString());
            ((TextView) findViewById(R.id.l_verify_password)).setVisibility(0);
            this.verifypasdEdit = (EditView) findViewById(R.id.verify_password);
            this.verifypasdEdit.isPassWord();
            this.verifypasdEdit.setVisibility(0);
            this.verifypasdEdit.isImeOptionDone();
            if (user != null) {
                this.teamEdit.setText(user.getEmail());
                this.userEdit.setText(user.getNick());
                Utils.MessageBox(this, getString(R.string.regist_false));
            } else {
                Utils.MessageBox(this, getString(R.string.regist_info));
            }
            this.autoLogin.setVisibility(8);
            this.offLineLogin.setVisibility(8);
        } else if (this.flag == 2) {
            ((TextView) findViewById(R.id.l_teamname)).setVisibility(0);
            this.teamEdit = (EditView) findViewById(R.id.teamname);
            this.teamEdit.isNameEdt();
            this.pasdEdit.isImeOptionDone();
            if (user != null) {
                this.teamEdit.setText(user.getTeamName());
                this.userEdit.setText(user.getUserName());
                this.pasdEdit.requestFocus();
                Utils.MessageBox(this, getString(R.string.error_login_required));
            }
            this.autoLogin.setVisibility(0);
            this.offLineLogin.setVisibility(8);
        } else if (this.flag == 3) {
            titleTV.setText(getText(R.string.phone_email).toString());
            userNameTV.setText(getText(R.string.phone_email_ebox).toString());
            this.pasdEdit.isImeOptionDone();
            if (user != null) {
                this.userEdit.setText(user.getUserName());
                this.pasdEdit.requestFocus();
                Utils.MessageBox(this, getString(R.string.error_login_required));
            }
            this.autoLogin.setVisibility(0);
            this.offLineLogin.setVisibility(8);
        } else if (this.flag == 4) {
            cancelButton.setTextViewText(getString(R.string.deldevice));
            titleTV.setText(getText(R.string.change_login_ebox).toString());
            userNameTV.setText(getText(R.string.regist_nick).toString());
            this.userEdit.setText(EboxContext.mDevice.getNick());
            this.userEdit.setEdit(false);
            this.pasdEdit.requestFocus();
            this.pasdEdit.isImeOptionDone();
            this.autoLogin.setVisibility(0);
            this.offLineLogin.setVisibility(0);
            this.offLineLogin.setChecked(EboxContext.mSystemInfo.hasOffLine());
            if (!EboxContext.mSystemInfo.hasNet()) {
                Utils.MessageBox(this, getResources().getString(R.string.nonet_t));
                this.offLineLogin.setChecked(true);
                this.offLineLogin.setEnabled(false);
            }
            if (user != null) {
                Utils.MessageBox(this, getString(R.string.error_login_required));
            }
        }
        cancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LoginActivity.this.finish();
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (LoginActivity.this.checkAccount()) {
                    EboxContext.mDevice.setDeviceName(LoginActivity.this.getString(R.string.mobile));
                    EboxContext.mDevice.setDeviceInfo(Utils.getDeviceInfo(LoginActivity.this));
                    EboxContext.mDevice.setCode(Integer.valueOf(LoginActivity.this.flag));
                    LoginActivity.this.setResult(LoginActivity.this.flag);
                    LoginActivity.this.finish();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean checkAccount() {
        try {
            if (this.flag == 2) {
                if (!this.teamEdit.isVerifiy() || !this.userEdit.isVerifiy() || !this.pasdEdit.isVerifiy()) {
                    String msg = EboxConst.TAB_UPLOADER_TAG;
                    if (!this.teamEdit.isVerifiy()) {
                        msg = this.teamEdit.getMessage();
                    } else if (!this.userEdit.isVerifiy()) {
                        msg = this.userEdit.getMessage();
                    } else if (!this.pasdEdit.isVerifiy()) {
                        msg = this.pasdEdit.getMessage();
                    }
                    Utils.MessageBox(this, msg);
                    return false;
                }
                EboxContext.mDevice.setTeamName(this.teamEdit.getMessage());
                EboxContext.mDevice.setUserName(this.userEdit.getMessage());
                EboxContext.mDevice.setPassWord(this.pasdEdit.getMessage());
                EboxContext.mSystemInfo.setAutoLogin(this.autoLogin.isChecked());
                return true;
            } else if (this.flag == 3) {
                if (!this.userEdit.isVerifiy() || !this.pasdEdit.isVerifiy()) {
                    String msg2 = EboxConst.TAB_UPLOADER_TAG;
                    if (!this.userEdit.isVerifiy()) {
                        msg2 = this.userEdit.getMessage();
                    } else if (!this.pasdEdit.isVerifiy()) {
                        msg2 = this.pasdEdit.getMessage();
                    }
                    Utils.MessageBox(this, msg2);
                    return false;
                }
                EboxContext.mDevice.setUserName(this.userEdit.getMessage());
                EboxContext.mDevice.setPassWord(this.pasdEdit.getMessage());
                EboxContext.mSystemInfo.setAutoLogin(this.autoLogin.isChecked());
                return true;
            } else if (this.flag == 4) {
                if (this.pasdEdit.isVerifiy()) {
                    EboxContext.mDevice.setPassWord(this.pasdEdit.getMessage());
                    EboxContext.mSystemInfo.setAutoLogin(this.autoLogin.isChecked());
                    EboxContext.mSystemInfo.setOffLine(this.offLineLogin.isChecked());
                    return true;
                } else if (this.pasdEdit.isVerifiy()) {
                    return false;
                } else {
                    Utils.MessageBox(this, this.pasdEdit.getMessage());
                    return false;
                }
            } else if (this.flag != 1) {
                return false;
            } else {
                if (!this.pasdEdit.getMessage().equals(this.verifypasdEdit.getMessage()) || !this.teamEdit.isVerifiy() || !this.userEdit.isVerifiy() || !this.pasdEdit.isVerifiy()) {
                    String msg3 = getString(R.string.regist_verify_false).toString();
                    if (!this.teamEdit.isVerifiy()) {
                        msg3 = this.teamEdit.getMessage();
                    } else if (!this.userEdit.isVerifiy()) {
                        msg3 = this.userEdit.getMessage();
                    } else if (!this.pasdEdit.isVerifiy()) {
                        msg3 = this.pasdEdit.getMessage();
                    }
                    Utils.MessageBox(this, msg3);
                    return false;
                }
                EboxContext.mDevice.setEmail(this.teamEdit.getMessage());
                EboxContext.mDevice.setNick(this.userEdit.getMessage());
                EboxContext.mDevice.setPassWord(this.pasdEdit.getMessage());
                EboxContext.mDevice.setCode(1);
                return true;
            }
        } catch (Exception e) {
            Exception e2 = e;
            e2.printStackTrace();
            Log.d(LoginActivity.class.getSimpleName(), "## checkAccount error : " + e2.getMessage());
            return false;
        }
    }

    public boolean onKeyDown(int i, KeyEvent event) {
        if (this.flag == 4 && i == 4) {
            new AlertDialog.Builder(this).setTitle(getString(R.string.info)).setMessage(getString(R.string.exit_app)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    LoginActivity.this.setResult(-1);
                    LoginActivity.this.finish();
                }
            }).setNeutralButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            }).create().show();
            return false;
        } else if (i != 4) {
            return false;
        } else {
            finish();
            return false;
        }
    }
}
