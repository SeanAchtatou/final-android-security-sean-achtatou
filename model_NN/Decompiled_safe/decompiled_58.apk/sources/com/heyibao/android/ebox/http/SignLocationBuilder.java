package com.heyibao.android.ebox.http;

import ClientProtocol.ClientMeta;
import android.app.Service;
import android.util.Log;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.db.ActionDB;
import com.heyibao.android.ebox.util.Utils;

public abstract class SignLocationBuilder implements Runnable, HttpInterface {
    private Service activity;
    private String latitude;
    private String longitude;

    public SignLocationBuilder(Service activity2, String latitude2, String longitude2) {
        this.activity = activity2;
        this.latitude = latitude2;
        this.longitude = longitude2;
    }

    public void run() {
        Log.d(SignLocationBuilder.class.getSimpleName(), "## start SignLocationBuilder ");
        signStart();
        Log.d(SignLocationBuilder.class.getSimpleName(), "end SignLocationBuilder ");
    }

    private void signStart() {
        int errcount = 0;
        boolean success = false;
        while (true) {
            if (Thread.interrupted() || errcount >= 3) {
                break;
            }
            try {
                ClientMeta.ServiceSignRequest.Builder request = ClientMeta.ServiceSignRequest.newBuilder();
                request.setSession(EboxContext.mDevice.getSession());
                request.setAction("add");
                request.setLatitude(this.latitude);
                request.setLongitude(this.longitude);
                Thread.sleep(10);
                ClientMeta.ServiceSignResponse response = ClientMeta.ServiceSignResponse.parseFrom(new GPBUtils(EboxConst.SIGN_URL).getResult(request.build().toByteArray()));
                if (response.getStatus().getSuccess()) {
                    success = true;
                    ActionDB.insertLocations(this.activity, this.latitude, this.longitude, response.getPlace(), response.getCreatedTime());
                    EboxContext.mSystemInfo.setSignTime(response.getCreatedTime());
                    EboxContext.message = Utils.replace(this.activity.getResources().getString(R.string.sign_success), response.getPlace());
                    break;
                }
                EboxContext.message = EboxConst.SESSIONEXIT;
                break;
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(SignLocationBuilder.class.getSimpleName(), "## SignLocationBuilder is Force stop");
                EboxContext.message = this.activity.getString(R.string.stop_thread);
            } catch (Exception e2) {
                Exception error = e2;
                error.printStackTrace();
                Log.e(SignLocationBuilder.class.getSimpleName(), "## SignLocationBuilder error : " + error.getMessage() + " for " + errcount);
                errcount++;
                EboxContext.message = this.activity.getResources().getString(R.string.net_error);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
            }
        }
        reportSuccess(success);
    }
}
