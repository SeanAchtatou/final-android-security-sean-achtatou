package com.heyibao.android.ebox.http;

import ClientProtocol.ClientMeta;
import android.app.Service;
import android.util.Log;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import java.io.File;

public abstract class DownloaderBuilder implements Runnable, HttpInterface {
    private Service activity;
    private String fileName;
    private String ftpUrl;
    private String objectUUID;

    public DownloaderBuilder(Service active, String objectUUID2, String fileName2) {
        this.activity = active;
        this.objectUUID = objectUUID2;
        this.fileName = fileName2;
    }

    public void run() {
        Log.d(DownloaderBuilder.class.getSimpleName(), "## start DownloadBuilder ");
        if (!getUrl() || this.ftpUrl == null) {
            reportSuccess(false);
        } else {
            File file = new File(new File(EboxContext.mSystemInfo.getUserSdPath()), this.fileName);
            EboxContext.message = this.activity.getString(R.string.download_info_2);
            new EboxDownload(this.activity, this.ftpUrl, file) {
                public void reSuccess(boolean is) {
                    DownloaderBuilder.this.reportSuccess(is);
                }

                public void reProgress(int blockIndex, int currentSize) {
                    DownloaderBuilder.this.reportProgress(blockIndex, currentSize);
                }
            };
        }
        Log.d(DownloaderBuilder.class.getSimpleName(), "end DownloadBuilder ");
    }

    private boolean getUrl() {
        Log.d(DownloaderBuilder.class.getSimpleName(), "## start getUrl ");
        int errcount = 0;
        boolean success = false;
        while (true) {
            if (Thread.interrupted() || errcount >= 3) {
                break;
            }
            try {
                EboxContext.message = this.activity.getString(R.string.download_info_1);
                reportProgress(0, 0);
                ClientMeta.ServiceDownLoadRequest.Builder request = ClientMeta.ServiceDownLoadRequest.newBuilder();
                request.setSession(EboxContext.mDevice.getSession());
                request.setObjectShowUuidName(this.objectUUID);
                Thread.sleep(10);
                ClientMeta.ServiceDownLoadResponse response = ClientMeta.ServiceDownLoadResponse.parseFrom(new GPBUtils(EboxConst.DOWNLOAD_URL).getResult(request.build().toByteArray()));
                ClientMeta.Status status = response.getStatus();
                this.ftpUrl = response.getDownloadUrl();
                success = status.getSuccess();
                if (!success) {
                    EboxContext.message = this.activity.getResources().getString(R.string.download_info_3);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(DownloaderBuilder.class.getSimpleName(), "## DownloadBuilder getUrlThread is Force stop");
                EboxContext.message = this.activity.getString(R.string.stop_thread);
            } catch (Exception e2) {
                Exception error = e2;
                Log.e(DownloaderBuilder.class.getSimpleName(), "## getUrl error : " + error.getMessage() + " for " + errcount);
                errcount++;
                EboxContext.message = this.activity.getResources().getString(R.string.download_info_3) + errcount;
                error.printStackTrace();
                try {
                    Thread.sleep(500);
                } catch (Exception e3) {
                }
            }
        }
        return success;
    }
}
