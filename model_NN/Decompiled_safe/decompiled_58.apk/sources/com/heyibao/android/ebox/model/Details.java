package com.heyibao.android.ebox.model;

import com.heyibao.android.ebox.common.EboxConst;

public class Details extends StandResult {
    private static final long serialVersionUID = 1;
    private Integer createTime = 0;
    private int icon = 0;
    private String latitude = null;
    private String longitude = null;
    private Integer modifyTime = 0;
    private String name = null;
    private String objectType = null;
    private String objectUUID = null;
    private String parentUUID = null;
    private String path = null;
    private String post = null;
    private int progress;
    private String shareUUID = null;
    private String signature = EboxConst.TAB_UPLOADER_TAG;
    private double size = 0.0d;
    private String thumbnail = null;
    private int updown;
    private Integer uploadId = 0;
    private boolean value = true;

    public Details() {
    }

    public Details(String name2, int icon2, String thumbnail2) {
        this.name = name2;
        this.objectUUID = EboxConst.DEFAULTSHARE;
        this.parentUUID = EboxConst.DEFAULTSHARE;
        this.objectType = EboxConst.TAB_UPLOADER_TAG;
        this.icon = icon2;
        this.thumbnail = thumbnail2;
    }

    public Details(String name2, String objectUUID2, String parentUUID2, String signature2, double size2, int createTime2, int modifyTime2, String objectType2) {
        this.name = name2;
        this.objectUUID = objectUUID2;
        this.parentUUID = parentUUID2.equals(EboxConst.TAB_UPLOADER_TAG) ? EboxConst.DEFAULTSHARE : parentUUID2;
        this.signature = signature2;
        this.size = size2;
        this.createTime = Integer.valueOf(createTime2);
        this.modifyTime = Integer.valueOf(modifyTime2);
        this.objectType = objectType2;
    }

    public Integer getProgress() {
        return Integer.valueOf(this.progress);
    }

    public void setProgress(Integer progress2) {
        this.progress = progress2.intValue();
    }

    public Integer getUpdown() {
        return Integer.valueOf(this.updown);
    }

    public void setUpdown(Integer updown2) {
        this.updown = updown2.intValue();
    }

    public Integer getIcon() {
        return Integer.valueOf(this.icon);
    }

    public void setIcon(Integer icon2) {
        this.icon = icon2.intValue();
    }

    public String getPost() {
        return this.post;
    }

    public void setPost(String post2) {
        this.post = post2;
    }

    public Integer getUploadId() {
        return this.uploadId;
    }

    public void setUploadId(Integer uploadId2) {
        this.uploadId = uploadId2;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path2) {
        this.path = path2;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public void setShowUuid(String objectUUID2) {
        this.objectUUID = objectUUID2;
    }

    public String getShowUuid() {
        return this.objectUUID;
    }

    public void setParentShowUuid(String parentUUID2) {
        if (parentUUID2.equals(EboxConst.TAB_UPLOADER_TAG)) {
            parentUUID2 = EboxConst.DEFAULTSHARE;
        }
        this.parentUUID = parentUUID2;
    }

    public String getParentShowUuid() {
        return this.parentUUID;
    }

    public void setShareUuid(String shareUUID2) {
        this.shareUUID = shareUUID2;
    }

    public String getShareUuid() {
        return this.shareUUID;
    }

    public void setSignature(String signature2) {
        this.signature = signature2;
    }

    public String getSignature() {
        return this.signature;
    }

    public void setSize(double size2) {
        this.size = size2;
    }

    public double getSize() {
        return this.size;
    }

    public void setCreateTime(Integer createTime2) {
        this.createTime = createTime2;
    }

    public Integer getCreateTime() {
        return this.createTime;
    }

    public void setModifyTime(Integer modifyTime2) {
        this.modifyTime = modifyTime2;
    }

    public Integer getModifyTime() {
        return this.modifyTime;
    }

    public void setObjectType(String objectType2) {
        this.objectType = objectType2;
    }

    public String getObjectType() {
        return this.objectType;
    }

    public boolean getValue() {
        return this.value;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String latitude2) {
        this.latitude = latitude2;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String longitude2) {
        this.longitude = longitude2;
    }

    public String getThumbnail() {
        return this.thumbnail;
    }

    public void setThumbnail(String thumbnail2) {
        this.thumbnail = thumbnail2;
    }

    public void setValue(Details d) {
        this.value = true;
        if (d == null) {
            this.value = false;
            return;
        }
        this.path = d.getPath();
        this.uploadId = d.getUploadId();
        this.name = d.getName();
        this.objectUUID = d.getShowUuid();
        this.parentUUID = d.getParentShowUuid();
        this.signature = d.signature;
        this.size = d.getSize();
        this.createTime = d.getCreateTime();
        this.modifyTime = d.modifyTime;
        this.objectType = d.getObjectType();
        this.post = d.getPost();
        this.shareUUID = d.getShareUuid();
        this.icon = d.getIcon().intValue();
        this.latitude = d.getLatitude();
        this.longitude = d.getLongitude();
        this.thumbnail = d.getThumbnail();
        this.progress = d.getProgress().intValue();
        this.updown = d.getUpdown().intValue();
        setSuccess(d.hasSuccess());
        setCode(d.getCode());
        setMsg(d.getMsg());
        setSession(d.getSession());
        setUrl(d.getUrl());
    }

    public void reSet() {
        this.path = null;
        this.uploadId = 0;
        this.name = null;
        this.objectUUID = null;
        this.parentUUID = null;
        this.signature = EboxConst.TAB_UPLOADER_TAG;
        this.size = 0.0d;
        this.createTime = 0;
        this.modifyTime = 0;
        this.objectType = null;
        this.post = null;
        this.shareUUID = null;
        this.icon = 0;
        setSuccess(false);
        setCode(0);
        setMsg(null);
        setSession(null);
        setUrl(EboxConst.TAB_UPLOADER_TAG);
        this.value = true;
        this.latitude = null;
        this.longitude = null;
        this.thumbnail = null;
        this.progress = 0;
        this.updown = 0;
    }
}
