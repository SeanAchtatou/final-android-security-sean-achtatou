package com.heyibao.android.ebox.model;

import android.graphics.Bitmap;
import com.heyibao.android.ebox.http.GPBUtils;
import com.heyibao.android.ebox.model.Json.JsonModel;
import com.heyibao.android.ebox.model.Json.JsonParent;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;

public class News {
    private String content;
    private String createdTime;
    private String modifiedTime;
    private byte[] photoBt;
    private Bitmap photoMap;
    private String photoUrl;
    private String status;
    private int syncId = -1;
    private String title;
    private String url;

    public News() {
    }

    public News(JsonParent news) throws MyException, JSONException {
        setSyncId(Integer.valueOf(news.getData("id").toString()).intValue());
        setStatus(news.getData(JsonModel.STATUS).toString());
        setTitle(news.getData("title").toString());
        setContent(news.getData("content").toString());
        setModifiedTime(news.getData("modified").toString());
        setCreatedTime(news.getData("date").toString());
        setUrl(news.getData("url").toString());
        if (news.getData("thumbnail").toString() == null) {
            setPhotoUrl(this.photoUrl);
        }
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setStatus(String status2) {
        this.status = status2;
    }

    public String getStatus() {
        return this.status;
    }

    public void setCreatedTime(String createdTime2) {
        this.createdTime = createdTime2;
    }

    public String getCreatedTime() {
        return this.createdTime;
    }

    public void setModifiedTime(String modifiedTime2) {
        this.modifiedTime = modifiedTime2;
    }

    public String getModifiedTime() {
        return this.modifiedTime;
    }

    public void setSyncId(int syncId2) {
        this.syncId = syncId2;
    }

    public int getSyncId() {
        return this.syncId;
    }

    public void setPhotoMap(Bitmap bp) {
        this.photoMap = bp;
    }

    public Bitmap getPhotoMap() {
        return this.photoMap;
    }

    public void setPhotoBt(String url2) {
        if (url2 != null) {
            try {
                byte[] result = new GPBUtils(url2).getResult(null);
                if (result.length != 0) {
                    setPhotoBt(result);
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void setPhotoBt(byte[] bt) {
        this.photoBt = bt;
    }

    public byte[] getPhotoBt() {
        return this.photoBt;
    }

    public void setPhotoUrl(String photoUrl2) {
        this.photoUrl = photoUrl2;
    }

    public String getPhotoUrl() {
        return this.photoUrl;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public String getContent() {
        return this.content;
    }

    public String toString() {
        return null;
    }
}
