package com.heyibao.android.ebox.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.model.MyDialog;
import com.heyibao.android.ebox.util.Utils;

public class SignActivity extends HomeMenuActivity {
    /* access modifiers changed from: private */
    public boolean gpsError = false;
    private GpsStatus.Listener gpsListener = new GpsStatus.Listener() {
        public void onGpsStatusChanged(int event) {
            switch (event) {
                case 1:
                    Log.e("MyGps", "Started");
                    SignActivity.this.mHandler.postDelayed(SignActivity.this.stopGps, 30000);
                    return;
                case 2:
                    Log.e("MyGps", "Stopped");
                    SignActivity.this.mHandler.removeCallbacks(SignActivity.this.stopGps);
                    if (SignActivity.this.gpsError) {
                        SignActivity.this.locationManager.requestLocationUpdates("network", 1000, 0.0f, SignActivity.this.locationListener);
                        SignActivity.this.mHandler.postDelayed(SignActivity.this.stopGps, 30000);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    private ImageView imageVI;
    /* access modifiers changed from: private */
    public LocationListener locationListener;
    /* access modifiers changed from: private */
    public LocationManager locationManager;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            boolean success = ((Intent) msg.obj).getBooleanExtra("success", false);
            switch (msg.what) {
                case EboxConst.SIGN /*17*/:
                    SignActivity.this.signEnd(success);
                    return;
                default:
                    return;
            }
        }
    };
    private SignReceiver receiver;
    /* access modifiers changed from: private */
    public Runnable stopGps = new Runnable() {
        public void run() {
            SignActivity.this.reSetInit();
            if (!SignActivity.this.gpsError) {
                Utils.MessageBox(SignActivity.this, SignActivity.this.getResources().getString(R.string.sign_false));
                SignActivity.this.removeDialog(16);
            }
        }
    };

    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(null);
        if (getResources().getConfiguration().orientation == 2) {
            this.mainLayout.setBackgroundResource(R.drawable.bg_cloud_land);
            LinearLayout.LayoutParams mp = new LinearLayout.LayoutParams(new ViewGroup.MarginLayoutParams(-2, -2));
            mp.setMargins(0, 10, 0, 0);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams((ViewGroup.MarginLayoutParams) mp);
            lp.addRule(14);
            this.imageVI.setLayoutParams(lp);
        } else if (getResources().getConfiguration().orientation == 1) {
            this.mainLayout.setBackgroundResource(R.drawable.bg_cloud);
            LinearLayout.LayoutParams mp2 = new LinearLayout.LayoutParams(new ViewGroup.MarginLayoutParams(-2, -2));
            mp2.setMargins(0, 60, 0, 0);
            RelativeLayout.LayoutParams lp2 = new RelativeLayout.LayoutParams((ViewGroup.MarginLayoutParams) mp2);
            lp2.addRule(14);
            this.imageVI.setLayoutParams(lp2);
        }
    }

    public class SignReceiver extends BroadcastReceiver {
        public SignReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("action");
            Log.d(SignActivity.class.getSimpleName(), "## onReceive : " + action);
            if (EboxConst.ACTION_SIGN.equals(action)) {
                SignActivity.this.mHandler.sendMessage(SignActivity.this.mHandler.obtainMessage(17, intent));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.receiver = new SignReceiver();
        registerReceiver(this.receiver, new IntentFilter(EboxConst.EBOXBROAD), null, this.mHandler);
        this.locationManager.addGpsStatusListener(this.gpsListener);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        unregisterReceiver(this.receiver);
        reSetInit();
        this.gpsError = false;
    }

    public void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case 16:
                Dialog dialog2 = proDialog();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 9:
                MyDialog mDialog = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.bt_ok:
                                SignActivity.this.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
                                dismiss();
                                return;
                            case R.id.bt_cancel:
                                SignActivity.this.removeDialog(16);
                                SignActivity.this.showDialog(16);
                                SignActivity.this.locationManager.requestLocationUpdates("network", 1000, 0.0f, SignActivity.this.locationListener);
                                SignActivity.this.mHandler.postDelayed(SignActivity.this.stopGps, 30000);
                                dismiss();
                                return;
                            default:
                                return;
                        }
                    }
                };
                mDialog.setDefaultView();
                mDialog.setMessage(getResources().getString(R.string.open_gps));
                return mDialog;
            case 16:
                return proDialog();
            default:
                return null;
        }
    }

    private MyDialog proDialog() {
        MyDialog proDialog = new MyDialog(this, R.style.dialog) {
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.bt_cancel:
                        SignActivity.this.reSetInit();
                        boolean unused = SignActivity.this.gpsError = false;
                        Utils.startService(SignActivity.this, EboxConst.ACTION_STOP, null, 0);
                        dismiss();
                        return;
                    default:
                        return;
                }
            }

            public void onBackPressed() {
                SignActivity.this.reSetInit();
                boolean unused = SignActivity.this.gpsError = false;
                Utils.startService(SignActivity.this, EboxConst.ACTION_STOP, null, 0);
                super.onBackPressed();
            }
        };
        proDialog.setProgressLoading();
        proDialog.setCanceledOnTouchOutside(false);
        return proDialog;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sign_view);
        signInit();
    }

    private void signInit() {
        initTitleBar();
        this.imageVI = (ImageView) findViewById(R.id.image_cloud);
        this.midTV.setText(getString(R.string.my_sign));
        this.refreshBtn.setVisibility(4);
        this.retreatBtn.setVisibility(4);
        ((ScrollView) findViewById(R.id.main_panel)).setOnTouchListener(this);
        ((Button) findViewById(R.id.bt_prompt)).setOnClickListener(this);
        if (EboxContext.mSystemInfo.getSignTime() != null) {
            ((TextView) findViewById(R.id.text_cloud)).setText(Utils.replace(getResources().getString(R.string.sign_context_lasttime), EboxContext.mSystemInfo.getSignTime()));
        }
        this.locationManager = (LocationManager) getSystemService("location");
        this.locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                if (location != null) {
                    EboxContext.mDevice.setLatitude(String.valueOf(location.getLatitude()));
                    EboxContext.mDevice.setLongitude(String.valueOf(location.getLongitude()));
                    SignActivity.this.locationManager.removeUpdates(this);
                    SignActivity.this.mHandler.removeCallbacks(SignActivity.this.stopGps);
                    SignActivity.this.startSign();
                }
            }

            public void onProviderDisabled(String provider) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }
        };
    }

    private void getLocation() {
        this.gpsError = true;
        if (this.locationManager.isProviderEnabled("gps")) {
            removeDialog(16);
            showDialog(16);
            this.locationManager.requestLocationUpdates("gps", 1000, 0.0f, this.locationListener);
            return;
        }
        showDialog(9);
    }

    /* access modifiers changed from: private */
    public void startSign() {
        if (hasWorking()) {
            Utils.startService(this, EboxConst.ACTION_SIGN, null, R.drawable.app_sync);
        }
    }

    /* access modifiers changed from: private */
    public void signEnd(boolean success) {
        removeDialog(16);
        reSetInit();
        this.gpsError = false;
        if (success) {
            Utils.startService(this, EboxConst.ACTION_NORMAL, null, 0);
            if (EboxContext.message != null) {
                Utils.MessageBox(this, EboxContext.message);
            }
            if (EboxContext.mSystemInfo.getSignTime() != null) {
                ((TextView) findViewById(R.id.text_cloud)).setText(Utils.replace(getResources().getString(R.string.sign_context_lasttime), EboxContext.mSystemInfo.getSignTime()));
                return;
            }
            return;
        }
        httpFalse();
    }

    /* access modifiers changed from: private */
    public void reSetInit() {
        this.locationManager.removeUpdates(this.locationListener);
        this.mHandler.removeCallbacks(this.stopGps);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_prompt:
                getLocation();
                return;
            default:
                return;
        }
    }
}
