package com.heyibao.android.ebox.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;

public class AboutActivity extends HomeMenuActivity {
    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(null);
        if (getResources().getConfiguration().orientation == 2) {
            this.mainLayout.setBackgroundResource(R.drawable.bg_cloud_land);
        } else if (getResources().getConfiguration().orientation == 1) {
            this.mainLayout.setBackgroundResource(R.drawable.bg_cloud);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about);
        aboutInit();
    }

    private void aboutInit() {
        initTitleBar();
        this.midTV.setText(getString(R.string.about));
        this.refreshBtn.setVisibility(4);
        ((ScrollView) findViewById(R.id.main_panel)).setOnTouchListener(this);
        ((Button) findViewById(R.id.about_btn)).setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.about_btn:
                openURL(EboxConst.ABOUT_URL);
                return;
            case R.id.retreat_btn:
                MainTabActivity.setCurrentTab(4);
                return;
            default:
                return;
        }
    }
}
