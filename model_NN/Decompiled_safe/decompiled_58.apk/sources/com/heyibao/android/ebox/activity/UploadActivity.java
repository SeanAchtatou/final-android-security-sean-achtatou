package com.heyibao.android.ebox.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.db.ActionDB;
import com.heyibao.android.ebox.util.Utils;
import java.io.File;
import java.util.HashMap;

public class UploadActivity {
    private static UploadActivity sInstance;
    private String CREATE_FILE_PATH;
    private Activity activity;

    public static UploadActivity getInstance(Activity activity2) {
        if (sInstance == null) {
            sInstance = new UploadActivity(activity2);
        }
        return sInstance;
    }

    public UploadActivity(Activity activity2) {
        this.activity = activity2;
    }

    public String getFilePath() {
        return this.CREATE_FILE_PATH;
    }

    public void newComments(int which) {
        switch (which) {
            case 1:
                String fileName = Utils.toDate(System.currentTimeMillis()) + EboxConst.PHONESUFFIX;
                this.CREATE_FILE_PATH = EboxContext.mSystemInfo.getUserSdPath() + fileName;
                Intent takePictureFromCameraIntent = new Intent("android.media.action.IMAGE_CAPTURE");
                takePictureFromCameraIntent.putExtra("output", Uri.fromFile(new File(EboxContext.mSystemInfo.getUserSdPath(), fileName)));
                takePictureFromCameraIntent.putExtra("android.intent.extra.videoQuality", 1);
                this.activity.startActivityForResult(takePictureFromCameraIntent, EboxConst.TAKEPHONE);
                return;
            case 2:
                Intent takeVideoIntent = new Intent("android.media.action.VIDEO_CAPTURE");
                takeVideoIntent.putExtra("android.intent.extra.videoQuality", 0);
                this.activity.startActivityForResult(takeVideoIntent, EboxConst.MAKEVID);
                return;
            default:
                return;
        }
    }

    public void uploadComments(int which) {
        switch (which) {
            case 0:
                Intent photoPickerIntent = new Intent("android.intent.action.PICK");
                photoPickerIntent.setType("image/*");
                this.activity.startActivityForResult(photoPickerIntent, EboxConst.SELIMG);
                return;
            case 1:
                Intent videoPickerIntent = new Intent("android.intent.action.PICK");
                videoPickerIntent.setType("video/*");
                this.activity.startActivityForResult(videoPickerIntent, EboxConst.SELVID);
                return;
            case 2:
                this.activity.startActivityForResult(new Intent(this.activity, FileBrowserActivity.class), EboxConst.SELFILE);
                return;
            default:
                return;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void getMedia(Uri uri) {
        EboxContext.mDetails.reSet();
        HashMap<String, Object> thisHash = ActionDB.getThumbnail(this.activity, uri);
        if (thisHash != null) {
            EboxContext.mDetails.setIcon((Integer) thisHash.get(EboxConst.CATCHICON));
            EboxContext.mDetails.setName(thisHash.get("name").toString());
            EboxContext.mDetails.setPath(thisHash.get("path").toString());
            EboxContext.mDetails.setSize(((Double) thisHash.get("size")).doubleValue());
        }
    }

    public boolean getMedia(String path, int icon) {
        EboxContext.mDetails.reSet();
        File f = new File(path);
        if (!f.exists()) {
            return false;
        }
        Utils.mediaStoreFileScan(this.activity, f);
        EboxContext.mDetails.setIcon(Integer.valueOf(icon));
        EboxContext.mDetails.setSize((double) f.length());
        EboxContext.mDetails.setName(f.getName());
        EboxContext.mDetails.setPath(path);
        return true;
    }
}
