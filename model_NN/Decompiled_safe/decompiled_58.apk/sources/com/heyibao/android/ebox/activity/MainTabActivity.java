package com.heyibao.android.ebox.activity;

import android.app.Dialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import com.google.protobuf.CodedOutputStream;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.model.MyDialog;
import com.heyibao.android.ebox.util.Utils;

public class MainTabActivity extends TabActivity implements TabHost.OnTabChangeListener {
    public static int detailsAction = EboxConst.PROMPT_DEFAULT;
    /* access modifiers changed from: private */
    public static TabHost mTabhost;
    protected DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            boolean unused = MainTabActivity.this.isClick = true;
            EboxContext.mSystemNotify.setNotifyThreadWait(false);
            ((RadioButton) MainTabActivity.this.mRadioGroup.getChildAt(MainTabActivity.this.curTab)).setChecked(true);
        }
    };
    /* access modifiers changed from: private */
    public int curRadio;
    /* access modifiers changed from: private */
    public int curTab;
    /* access modifiers changed from: private */
    public boolean isClick;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public RadioGroup mRadioGroup;
    private PowerManager.WakeLock mWakeLock;
    private Runnable runnable = new Runnable() {
        public void run() {
            ((ControlActivity) MainTabActivity.this.getLocalActivityManager().getActivity(MainTabActivity.mTabhost.getCurrentTabTag())).timerListFileHanding();
            EboxContext.timerHandler.postDelayed(this, EboxContext.mSystemInfo.getLastTime());
        }
    };
    /* access modifiers changed from: private */
    public Runnable runnableUI = new Runnable() {
        public void run() {
            int icon = R.drawable.app_icon;
            if (!EboxContext.mSystemInfo.hasNet()) {
                icon = R.drawable.app_unnet;
                Utils.MessageBox(MainTabActivity.this, MainTabActivity.this.getResources().getString(R.string.nonet_t));
            } else if (!EboxContext.mSystemInfo.hasSdcard()) {
                Utils.MessageBox(MainTabActivity.this, MainTabActivity.this.getResources().getString(R.string.nocard_t));
                icon = R.drawable.app_uncard;
            }
            Utils.startService(MainTabActivity.this, EboxConst.ACTION_NORMAL, null, icon);
            MainTabActivity.this.mHandler.removeCallbacks(MainTabActivity.this.runnableUI);
        }
    };
    private MyDialog stopDialog;
    protected DialogInterface.OnClickListener verifyStopListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            MainTabActivity.this.stopWorking();
        }
    };

    private MyDialog stopDialog() {
        if (this.stopDialog == null) {
            this.stopDialog = new MyDialog(this, R.style.dialog) {
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.bt_ok:
                            MainTabActivity.this.stopWorking();
                            dismiss();
                            return;
                        case R.id.bt_cancel:
                            MainTabActivity.this.onCancel();
                            dismiss();
                            return;
                        default:
                            return;
                    }
                }

                public void onBackPressed() {
                    MainTabActivity.this.onCancel();
                    super.onBackPressed();
                }
            };
            this.stopDialog.setDefaultView();
            this.stopDialog.setMessage(getResources().getString(R.string.verify_stop));
            this.stopDialog.setCanceledOnTouchOutside(false);
        }
        return this.stopDialog;
    }

    /* access modifiers changed from: private */
    public void onCancel() {
        this.isClick = true;
        EboxContext.mSystemNotify.setNotifyThreadWait(false);
        ((RadioButton) this.mRadioGroup.getChildAt(this.curTab)).setChecked(true);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(CodedOutputStream.DEFAULT_BUFFER_SIZE);
        setContentView((int) R.layout.main_tabs);
        init();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 12:
                return stopDialog();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mWakeLock == null) {
            this.mWakeLock = ((PowerManager) getSystemService("power")).newWakeLock(10, EboxConst.USER_AGENT);
        }
        this.mWakeLock.acquire();
        if (EboxContext.mSystemInfo.getShareData() != null && EboxContext.mDevice.hasSuccess()) {
            if (!EboxConst.TAB_DETAILS_TAG.equals(mTabhost.getCurrentTabTag())) {
                mTabhost.setCurrentTab(0);
            }
            ((ControlActivity) getLocalActivityManager().getActivity(EboxConst.TAB_DETAILS_TAG)).startShare();
            if (!EboxContext.threadIsable) {
                EboxContext.mSystemNotify.setNotifyListDetails(true);
                EboxContext.mSystemNotify.setNotifySystem(true);
                EboxContext.mSystemNotify.setNotifySetting(true);
                Utils.startService(this, EboxConst.ACTION_STOP, null, 0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.mWakeLock != null) {
            this.mWakeLock.release();
        }
    }

    private void init() {
        EboxContext.curActivity = this;
        this.mHandler.postDelayed(this.runnableUI, 1000);
        mTabhost = getTabHost();
        mTabhost.addTab(mTabhost.newTabSpec(EboxConst.TAB_DETAILS_TAG).setIndicator(null, null).setContent(new Intent(this, DetailsActivity.class)));
        mTabhost.addTab(mTabhost.newTabSpec(EboxConst.TAB_DOWNLOAD_TAG).setIndicator(null, null).setContent(new Intent(this, DownLoadActivity.class)));
        mTabhost.addTab(mTabhost.newTabSpec(EboxConst.TAB_UPLOADER_TAG).setIndicator(null, null).setContent(new Intent()));
        mTabhost.addTab(mTabhost.newTabSpec(EboxConst.TAB_MORE_TAG).setIndicator(null, null).setContent(new Intent(this, ContactsActivity.class)));
        mTabhost.addTab(mTabhost.newTabSpec(EboxConst.TAB_SETTING_TAG).setIndicator(null, null).setContent(new Intent(this, SettingActivity.class)));
        mTabhost.addTab(mTabhost.newTabSpec(EboxConst.TAB_SYSTEM_TAG).setIndicator(null, null).setContent(new Intent(this, SystemActivity.class)));
        mTabhost.addTab(mTabhost.newTabSpec(EboxConst.TAB_ACCOUNT_TAG).setIndicator(null, null).setContent(new Intent(this, AccountActivity.class)));
        mTabhost.addTab(mTabhost.newTabSpec(EboxConst.TAB_ABOUT_TAG).setIndicator(null, null).setContent(new Intent(this, AboutActivity.class)));
        mTabhost.addTab(mTabhost.newTabSpec(EboxConst.TAB_PROMPT_TAG).setIndicator(null, null).setContent(new Intent(this, PromptActivity.class)));
        mTabhost.setOnTabChangedListener(this);
        final RadioButton mRadio0 = (RadioButton) findViewById(R.id.buttono);
        this.mRadioGroup = (RadioGroup) findViewById(R.id.main_radio);
        this.mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int unused = MainTabActivity.this.curRadio = checkedId - mRadio0.getId();
                if (MainTabActivity.this.isClick) {
                    boolean unused2 = MainTabActivity.this.isClick = false;
                } else if (EboxContext.mDevice.hasSuccess() && !EboxContext.threadIsable) {
                    EboxContext.mSystemNotify.setNotifyThreadWait(true);
                    MainTabActivity.this.showDialog(12);
                } else if (MainTabActivity.this.curRadio == 2) {
                    MainTabActivity.detailsAction = EboxConst.PROMPT_UPLOADER;
                    MainTabActivity.this.onClickUpload();
                } else {
                    MainTabActivity.mTabhost.setCurrentTab(MainTabActivity.this.curRadio);
                }
            }
        });
        this.isClick = false;
        this.curTab = 0;
        mRadio0.setChecked(true);
        startTimer();
    }

    private void startTimer() {
        if (EboxContext.mSystemInfo.hasNet()) {
            int time = EboxContext.mSystemInfo.getOptionRefre();
            switch (time) {
                case 0:
                    return;
                case 1:
                    EboxContext.mSystemInfo.setLastTime((long) (120000 * time));
                    break;
                case 2:
                    EboxContext.mSystemInfo.setLastTime((long) (300000 * time));
                    break;
                case 3:
                    EboxContext.mSystemInfo.setLastTime((long) (3600000 * time));
                    break;
            }
            EboxContext.timerHandler.postDelayed(this.runnable, EboxContext.mSystemInfo.getLastTime());
        }
    }

    private void notifyReLoad() {
        ((HomeMenuActivity) getLocalActivityManager().getActivity(mTabhost.getCurrentTabTag())).reLoader();
    }

    /* access modifiers changed from: private */
    public void onClickUpload() {
        mTabhost.setCurrentTab(0);
        ((RadioButton) this.mRadioGroup.getChildAt(0)).setChecked(true);
        ((DetailsActivity) getLocalActivityManager().getCurrentActivity()).onSubmitUpload();
    }

    private void onClickRetreat() {
        detailsAction = EboxConst.PROMPT_DEFAULT;
        mTabhost.setCurrentTab(0);
        ((RadioButton) this.mRadioGroup.getChildAt(0)).setChecked(true);
        ((DetailsActivity) getLocalActivityManager().getCurrentActivity()).retreatDir();
    }

    private void onClickRefresh() {
        detailsAction = EboxConst.PROMPT_DEFAULT;
        mTabhost.setCurrentTab(0);
        ((RadioButton) this.mRadioGroup.getChildAt(0)).setChecked(true);
        ((DetailsActivity) getLocalActivityManager().getCurrentActivity()).onSubmitRefresh();
    }

    /* access modifiers changed from: protected */
    public void stopWorking() {
        EboxContext.mSystemNotify.setNotifyListDetails(true);
        EboxContext.mSystemNotify.setNotifySystem(true);
        EboxContext.mSystemNotify.setNotifySetting(true);
        Utils.startService(this, EboxConst.ACTION_STOP, null, 0);
        if (this.curRadio == 2) {
            mTabhost.setCurrentTab(0);
            ((RadioButton) this.mRadioGroup.getChildAt(this.curTab)).setChecked(true);
            ((DetailsActivity) getLocalActivityManager().getCurrentActivity()).onSubmitUpload();
            return;
        }
        mTabhost.setCurrentTab(this.curRadio);
    }

    public static void setCurrentTab(int cur) {
        mTabhost.setCurrentTab(cur);
    }

    public static void setCurrentTab(boolean isRight) {
        int cur;
        int cur2;
        int cur3 = mTabhost.getCurrentTab();
        if (isRight) {
            if (cur3 < 4) {
                if (cur3 == 1) {
                    cur2 = cur3 + 1 + 1;
                } else {
                    cur2 = cur3 + 1;
                }
                mTabhost.setCurrentTab(cur2);
            } else if (cur3 == 8) {
                mTabhost.setCurrentTab(9);
            }
        } else if (cur3 <= 4) {
            if (cur3 == 3) {
                cur = (cur3 - 1) - 1;
            } else {
                cur = cur3 - 1;
            }
            mTabhost.setCurrentTab(cur);
        } else {
            mTabhost.setCurrentTab(4);
        }
    }

    public void onTabChanged(String tabId) {
        EboxContext.timerHandler.removeCallbacks(this.runnable);
        this.isClick = false;
        if (tabId.equals(EboxConst.TAB_DETAILS_TAG)) {
            this.curTab = 0;
            startTimer();
            if (detailsAction == EboxConst.PROMPT_UPLOADER) {
                onClickUpload();
            } else if (detailsAction == EboxConst.PROMPT_RETREAT) {
                onClickRetreat();
            } else if (detailsAction == EboxConst.PROMPT_REFRESH) {
                onClickRefresh();
            }
        } else if (tabId.equals(EboxConst.TAB_DOWNLOAD_TAG)) {
            this.curTab = 1;
            detailsAction = EboxConst.PROMPT_DEFAULT;
        } else if (tabId.equals(EboxConst.TAB_MORE_TAG)) {
            this.curTab = 3;
            detailsAction = EboxConst.PROMPT_DEFAULT;
        } else if (tabId.equals(EboxConst.TAB_SETTING_TAG)) {
            this.curTab = 4;
            detailsAction = EboxConst.PROMPT_DEFAULT;
        } else if (tabId.equals(EboxConst.TAB_PROMPT_TAG)) {
            this.curTab = 0;
            detailsAction = EboxConst.PROMPT_DEFAULT;
        }
        notifyReLoad();
        ((RadioButton) this.mRadioGroup.getChildAt(this.curTab)).setChecked(true);
    }
}
