package com.heyibao.android.ebox.http;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.model.Details;
import com.heyibao.android.ebox.model.StandResult;
import com.heyibao.android.ebox.util.Utils;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class EboxService extends Service {
    private static final Class<?>[] mStartSignature = {Integer.TYPE, Notification.class};
    private static final Class<?>[] mStopSignature = {Boolean.TYPE};
    private NotificationManager mNM;
    private Method mStartForeground;
    private Object[] mStartForegroundArgs = new Object[2];
    private Method mStopForeground;
    private Object[] mStopForegroundArgs = new Object[1];
    private volatile Thread runner;

    public void onCreate() {
        EboxContext.mSystemInfo.setDBVersion(Integer.valueOf(getString(R.string.db_version)).intValue());
        EboxContext.mSystemInfo.setAppName(getString(R.string.app_type));
        this.mNM = (NotificationManager) getSystemService("notification");
        try {
            this.mStartForeground = getClass().getMethod("startForeground", mStartSignature);
            this.mStopForeground = getClass().getMethod("stopForeground", mStopSignature);
        } catch (NoSuchMethodException e) {
            Log.w(EboxService.class.getSimpleName(), "## service unCreate" + e);
            this.mStopForeground = null;
            this.mStartForeground = null;
        }
    }

    private void startForegroundCompat(int id, Notification notification) {
        if (this.mStartForeground != null) {
            this.mStartForegroundArgs[0] = Integer.valueOf(id);
            this.mStartForegroundArgs[1] = notification;
            try {
                this.mStartForeground.invoke(this, this.mStartForegroundArgs);
            } catch (InvocationTargetException e) {
                Log.w(EboxService.class.getSimpleName(), "## Unable to invoke startForeground" + e);
            } catch (IllegalAccessException e2) {
                Log.w(EboxService.class.getSimpleName(), "## Unable to invoke startForeground" + e2);
            }
        } else {
            setForeground(true);
            this.mNM.notify(id, notification);
        }
    }

    private void stopForegroundCompat(int id) {
        if (this.mStopForeground != null) {
            this.mStopForegroundArgs[0] = Boolean.TRUE;
            try {
                this.mStopForeground.invoke(this, this.mStopForegroundArgs);
            } catch (InvocationTargetException e) {
                Log.w(EboxService.class.getSimpleName(), "## Unable to invoke stopForeground" + e);
            } catch (IllegalAccessException e2) {
                Log.w(EboxService.class.getSimpleName(), "## Unable to invoke stopForeground" + e2);
            }
        } else {
            this.mNM.cancel(id);
            setForeground(false);
        }
    }

    public void onDestroy() {
        EboxContext.threadIsable = true;
        stopForegroundCompat(R.string.app_name);
    }

    public void onStart(Intent intent, int startId) {
        Log.d(EboxService.class.getSimpleName(), "## server start");
        handleCommand(intent);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        handleCommand(intent);
        return 1;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public boolean onUnbind(Intent i) {
        return false;
    }

    private void handleCommand(Intent intent) {
        Intent intent2;
        EboxContext.message = null;
        if (intent != null) {
            if (!EboxConst.ACTION_NORMAL.equals(intent.getAction())) {
                if (EboxConst.ACTION_LOGIN.equals(intent.getAction())) {
                    startLogin();
                } else if (EboxConst.ACTION_AUTOUPDATE.equals(intent.getAction())) {
                    autoUpdate();
                } else if (EboxConst.ACTION_DOWNUPDATE.equals(intent.getAction())) {
                    downUpdate();
                } else if (EboxConst.ACTION_REGIST.equals(intent.getAction())) {
                    regist();
                } else if (Utils.hasSession()) {
                    Log.e(EboxService.class.getSimpleName(), "no seession service return ");
                    sendBroad(intent.getAction(), false);
                } else if (EboxConst.ACTION_LISTFILE.equals(intent.getAction())) {
                    listFile((Details) intent.getSerializableExtra("data"));
                } else if (EboxConst.ACTION_UPLOAD.equals(intent.getAction())) {
                    upLoad();
                } else if (EboxConst.ACTION_DOWNLOAD.equals(intent.getAction())) {
                    downLoad(EboxConst.ACTION_DOWNLOAD);
                } else if (EboxConst.ACTION_CREATEFOLDER.equals(intent.getAction())) {
                    createDir();
                } else if (EboxConst.ACTION_RENAME.equals(intent.getAction())) {
                    reName();
                } else if (EboxConst.ACTION_DELETE.equals(intent.getAction())) {
                    delDetails();
                } else if (EboxConst.ACTION_CONFIG.equals(intent.getAction())) {
                    configPost(((StandResult) intent.getSerializableExtra("data")).getCode().intValue());
                } else if (EboxConst.ACTION_SENDEMAIL.equals(intent.getAction())) {
                    downLoad(EboxConst.ACTION_SENDEMAIL);
                } else if (EboxConst.ACTION_STOP.equals(intent.getAction())) {
                    stopThread();
                } else if (EboxConst.ACTION_CONTACTS.equals(intent.getAction())) {
                    contactsPost(((StandResult) intent.getSerializableExtra("data")).getCode().intValue());
                } else if (EboxConst.ACTION_SIGN.equals(intent.getAction())) {
                    signLocationPost();
                }
            }
            if (EboxContext.mSystemInfo.hasNotify()) {
                Notification notification = new Notification(intent.getIntExtra(EboxConst.CATCHICON, R.drawable.app_icon), null, System.currentTimeMillis());
                if (EboxContext.curActivity == null) {
                    intent2 = null;
                } else {
                    intent2 = new Intent(this, EboxContext.curActivity.getClass());
                }
                notification.setLatestEventInfo(this, getText(R.string.app_name), null, PendingIntent.getActivity(this, 0, intent2, 0));
                startForegroundCompat(R.string.app_name, notification);
                return;
            }
            stopForegroundCompat(R.string.app_name);
        }
    }

    private void startThread(Thread runner2) {
        EboxContext.threadIsable = !EboxContext.threadIsable;
        runner2.start();
    }

    private void stopThread() {
        if (this.runner != null) {
            Thread moribund = this.runner;
            this.runner = null;
            moribund.interrupt();
        }
        Utils.stopThreadList();
        EboxContext.threadIsable = true;
        EboxContext.mSystemNotify.setNotifyThreadWait(false);
    }

    private void autoUpdate() {
        if (this.runner == null) {
            EboxContext.mUpGrade.reSet();
            this.runner = new Thread(new UpdateBuilder(this) {
                public void reportSuccess(boolean is) {
                    EboxService.this.sendBroad(EboxConst.ACTION_AUTOUPDATE, is);
                }

                public void reportProgress(int currentSize, int total) {
                }
            });
            startThread(this.runner);
        }
    }

    private void downUpdate() {
        if (this.runner == null) {
            this.runner = new Thread(new UpdateBuilder(this, EboxContext.mUpGrade.getPath(), EboxContext.mUpGrade.getName()) {
                public void reportSuccess(boolean is) {
                    EboxService.this.sendBroad(EboxConst.ACTION_DOWNUPDATE, is);
                }

                public void reportProgress(int startPos, int currentSize) {
                    EboxService.this.sendBroad(EboxConst.ACTION_DOWNUPDATE, startPos, (double) currentSize);
                }
            });
            startThread(this.runner);
        }
    }

    private void regist() {
        if (this.runner == null) {
            this.runner = new Thread(new RegistBuilder(this, EboxContext.mDevice.getEmail(), EboxContext.mDevice.getNick(), EboxContext.mDevice.getPassWord(), EboxContext.mDevice.getCode().intValue()) {
                public void reportSuccess(boolean is) {
                    EboxService.this.sendBroad(EboxConst.ACTION_REGIST, is);
                }

                public void reportProgress(int startPos, int currentSize) {
                }
            });
            startThread(this.runner);
        }
    }

    private void startLogin() {
        if (this.runner == null) {
            this.runner = new Thread(new LoginBuilder(this, EboxContext.mDevice.getDeviceName(), EboxContext.mDevice.getDeviceInfo(), EboxContext.mDevice.getTeamName(), EboxContext.mDevice.getUserName(), EboxContext.mDevice.getPassWord(), EboxContext.mDevice.getCode().intValue()) {
                public void reportSuccess(boolean is) {
                    EboxService.this.sendBroad(EboxConst.ACTION_LOGIN, is);
                }

                public void reportProgress(int startPos, int currentSize) {
                }
            });
            startThread(this.runner);
        }
    }

    private void listFile(Details details) {
        if (this.runner == null) {
            this.runner = new Thread(new ListFileBuilder(this, details.getShowUuid(), details.getObjectType(), details.getParentShowUuid()) {
                public void reportSuccess(boolean is) {
                    EboxService.this.sendBroad(EboxConst.ACTION_LISTFILE, is);
                }

                public void reportProgress(int startPos, int currentSize) {
                }
            });
            startThread(this.runner);
        }
    }

    private void upLoad() {
        if (this.runner == null) {
            this.runner = new Thread(new UploaderBuilder(this, EboxContext.mDetails.getPath(), EboxContext.mDetails.getName(), EboxContext.mDetails.getSize(), EboxContext.mDetails.getShowUuid(), EboxContext.mDetails.getObjectType(), EboxContext.mDetails.getIcon().intValue()) {
                public void reportProgress(int startPos, int currentSize) {
                    EboxService.this.sendBroad(EboxConst.ACTION_UPLOADING, startPos, (double) currentSize);
                }

                public void reportSuccess(boolean is) {
                    EboxService.this.sendBroad(EboxConst.ACTION_UPLOAD, is);
                }
            });
            startThread(this.runner);
        }
    }

    private void downLoad(String action) {
        if (this.runner == null) {
            final String str = action;
            this.runner = new Thread(new DownloaderBuilder(this, EboxContext.mDetails.getShowUuid(), EboxContext.mDetails.getName()) {
                public void reportProgress(int startPos, int currentSize) {
                    EboxService.this.sendBroad(EboxConst.ACTION_DOWNLOADING, startPos, (double) currentSize);
                }

                public void reportSuccess(boolean is) {
                    EboxService.this.sendBroad(str, is);
                }
            });
            startThread(this.runner);
        }
    }

    private void createDir() {
        if (this.runner == null) {
            this.runner = new Thread(new CreateDirBuilder(this, EboxContext.mDetails.getName(), EboxContext.mDetails.getShowUuid(), EboxContext.mDetails.getObjectType()) {
                public void reportSuccess(boolean is) {
                    EboxService.this.sendBroad(EboxConst.ACTION_CREATEFOLDER, is);
                }

                public void reportProgress(int startPos, int currentSize) {
                }
            });
            startThread(this.runner);
        }
    }

    private void reName() {
        if (this.runner == null) {
            this.runner = new Thread(new ReNameBuilder(this, EboxContext.mDetails.getMsg(), EboxContext.mDetails.getName(), EboxContext.mDetails.getShowUuid(), EboxContext.mDetails.getParentShowUuid(), EboxContext.mDetails.getShareUuid(), EboxContext.mDetails.getObjectType(), EboxContext.mDetails.getCreateTime().intValue()) {
                public void reportSuccess(boolean is) {
                    EboxService.this.sendBroad(EboxConst.ACTION_RENAME, is);
                }

                public void reportProgress(int startPos, int currentSize) {
                }
            });
            startThread(this.runner);
        }
    }

    private void delDetails() {
        if (this.runner == null) {
            this.runner = new Thread(new DelBuilder(this, EboxContext.mDetails.getName(), EboxContext.mDetails.getShowUuid(), EboxContext.mDetails.getParentShowUuid(), EboxContext.mDetails.getShareUuid(), EboxContext.mDetails.getObjectType()) {
                public void reportSuccess(boolean is) {
                    EboxService.this.sendBroad(EboxConst.ACTION_DELETE, is);
                }

                public void reportProgress(int startPos, int currentSize) {
                }
            });
            startThread(this.runner);
        }
    }

    private void configPost(int flag) {
        if (this.runner == null) {
            this.runner = new Thread(new ConfigBuilder(this, flag) {
                public void reportSuccess(boolean is) {
                    EboxService.this.sendBroad(EboxConst.ACTION_CONFIG, is);
                }

                public void reportProgress(int startPos, int currentSize) {
                }
            });
            startThread(this.runner);
        }
    }

    private void contactsPost(int flag) {
        if (this.runner == null) {
            this.runner = new Thread(new ContactsBuilder(this, flag) {
                public void reportSuccess(boolean is) {
                    EboxService.this.sendBroad(EboxConst.ACTION_CONTACTS, is);
                }

                public void reportProgress(int startPos, int currentSize) {
                    EboxService.this.sendBroad(EboxConst.ACTION_CONTACTSING, startPos, 0.0d);
                }
            });
            startThread(this.runner);
        }
    }

    private void signLocationPost() {
        if (this.runner == null) {
            this.runner = new Thread(new SignLocationBuilder(this, EboxContext.mDevice.getLatitude(), EboxContext.mDevice.getLongitude()) {
                public void reportSuccess(boolean is) {
                    EboxService.this.sendBroad(EboxConst.ACTION_SIGN, is);
                }

                public void reportProgress(int startPos, int currentSize) {
                }
            });
            startThread(this.runner);
        }
    }

    /* access modifiers changed from: private */
    public void sendBroad(String action, int currentSize, double total) {
        Intent intent = new Intent(EboxConst.EBOXBROAD);
        intent.putExtra("action", action);
        intent.putExtra("retval", currentSize);
        intent.putExtra("total", total);
        sendBroadcast(intent);
    }

    /* access modifiers changed from: private */
    public void sendBroad(String action, boolean success) {
        stopThread();
        Intent intent = new Intent(EboxConst.EBOXBROAD);
        intent.putExtra("action", action);
        intent.putExtra("success", success);
        sendBroadcast(intent);
    }
}
