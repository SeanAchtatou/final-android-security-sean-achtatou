package com.heyibao.android.ebox.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.db.ActionDB;
import com.heyibao.android.ebox.http.EboxService;
import com.heyibao.android.ebox.model.Details;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.ref.SoftReference;
import java.security.MessageDigest;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;
import org.json.JSONObject;

public class Utils {
    private static final String HEXES = "0123456789ABCDEF";

    public static boolean hasSession() {
        if (EboxContext.mDevice.getSession() != null) {
            return false;
        }
        EboxContext.message = EboxConst.SESSIONEXIT;
        return true;
    }

    public static boolean hasSdcard(Activity activity) {
        if (!EboxContext.mSystemInfo.hasSdcard()) {
            MessageBox(activity, activity.getResources().getString(R.string.nocard_a));
        }
        return !EboxContext.mSystemInfo.hasSdcard();
    }

    public static boolean hasNet(Activity activity) {
        if (!EboxContext.mSystemInfo.hasNet()) {
            MessageBox(activity, activity.getResources().getString(R.string.nonet_a));
        }
        return !EboxContext.mSystemInfo.hasNet();
    }

    public static void MessageBox(Activity activity, String message) {
        Toast.makeText(activity, message, 0).show();
    }

    public static void startService(Activity activity, String action, Serializable data, int icon) {
        if (icon == 0) {
            if (!EboxContext.mSystemInfo.hasNet()) {
                icon = R.drawable.app_unnet;
            } else if (!EboxContext.mSystemInfo.hasSdcard()) {
                icon = R.drawable.app_uncard;
            } else {
                icon = R.drawable.app_icon;
            }
        }
        Intent i = new Intent(action);
        i.putExtra("data", data);
        i.putExtra(EboxConst.CATCHICON, icon);
        i.setClass(activity, EboxService.class);
        activity.startService(i);
    }

    public static String getDeviceInfo(Activity active) {
        TelephonyManager mTelephonyMgr = (TelephonyManager) active.getSystemService("phone");
        try {
            JSONObject json = new JSONObject();
            JSONObject device_info = new JSONObject();
            device_info.put("category", "android");
            device_info.put("product", Build.MODEL);
            device_info.put("SDK", Build.VERSION.RELEASE);
            device_info.put("info", mTelephonyMgr.getDeviceId());
            json.put("name", Build.MODEL);
            json.put("device_info", device_info);
            return json.toString();
        } catch (Exception e) {
            Exception error = e;
            error.printStackTrace();
            Log.e(Utils.class.getSimpleName(), " getDeviceInfo error " + error.getMessage());
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x005f A[SYNTHETIC, Splitter:B:24:0x005f] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x006b A[SYNTHETIC, Splitter:B:30:0x006b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getSignature(java.lang.String r10) {
        /*
            r6 = 0
            r2 = 0
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0038 }
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0038 }
            r7.<init>(r10)     // Catch:{ Exception -> 0x0038 }
            r3.<init>(r7)     // Catch:{ Exception -> 0x0038 }
            java.lang.String r7 = "SHA-256"
            java.security.MessageDigest r4 = java.security.MessageDigest.getInstance(r7)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            r7 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r7]     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
        L_0x0016:
            int r5 = r3.read(r0)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            if (r5 <= 0) goto L_0x0020
            r7 = 0
            r4.update(r0, r7, r5)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
        L_0x0020:
            r7 = -1
            if (r5 != r7) goto L_0x0016
            byte[] r7 = r4.digest()     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            java.lang.String r6 = bytes2Hex(r7)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            if (r3 == 0) goto L_0x007b
            r3.close()     // Catch:{ Exception -> 0x0032 }
            r2 = r3
        L_0x0031:
            return r6
        L_0x0032:
            r1 = move-exception
            r1.printStackTrace()
            r2 = r3
            goto L_0x0031
        L_0x0038:
            r7 = move-exception
            r1 = r7
        L_0x003a:
            r1.printStackTrace()     // Catch:{ all -> 0x0068 }
            java.lang.Class<com.heyibao.android.ebox.util.Utils> r7 = com.heyibao.android.ebox.util.Utils.class
            java.lang.String r7 = r7.getSimpleName()     // Catch:{ all -> 0x0068 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0068 }
            r8.<init>()     // Catch:{ all -> 0x0068 }
            java.lang.String r9 = " getSignature error "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0068 }
            java.lang.String r9 = r1.getMessage()     // Catch:{ all -> 0x0068 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0068 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0068 }
            android.util.Log.e(r7, r8)     // Catch:{ all -> 0x0068 }
            if (r2 == 0) goto L_0x0031
            r2.close()     // Catch:{ Exception -> 0x0063 }
            goto L_0x0031
        L_0x0063:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0031
        L_0x0068:
            r7 = move-exception
        L_0x0069:
            if (r2 == 0) goto L_0x006e
            r2.close()     // Catch:{ Exception -> 0x006f }
        L_0x006e:
            throw r7
        L_0x006f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006e
        L_0x0074:
            r7 = move-exception
            r2 = r3
            goto L_0x0069
        L_0x0077:
            r7 = move-exception
            r1 = r7
            r2 = r3
            goto L_0x003a
        L_0x007b:
            r2 = r3
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heyibao.android.ebox.util.Utils.getSignature(java.lang.String):java.lang.String");
    }

    private static String bytes2Hex(byte[] raw) {
        if (raw == null) {
            return null;
        }
        StringBuilder hex = new StringBuilder(raw.length * 2);
        for (byte b : raw) {
            hex.append(HEXES.charAt((b & 240) >> 4)).append(HEXES.charAt(b & 15));
        }
        return hex.toString().toLowerCase();
    }

    public static String sanitizeFilePath(String path) {
        if (path.startsWith("file://")) {
            return path;
        }
        if (path.startsWith("/")) {
            return "file://" + path;
        }
        return "file:///" + path;
    }

    public static void installAPK(Context context) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(EboxContext.mSystemInfo.getSdPath() + EboxConst.CATCHFODLER + File.separator + context.getResources().getString(R.string.setup))), "application/vnd.android.package-archive");
        context.startActivity(intent);
    }

    public static void exitApp1(final Context context) {
        new AlertDialog.Builder(context).setTitle(context.getResources().getString(R.string.info)).setMessage(context.getResources().getString(R.string.exit_app)).setPositiveButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Utils.exitAppEnd(context);
            }
        }).setNeutralButton(context.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create().show();
    }

    public static void exitAppEnd(Context context) {
        try {
            context.stopService(new Intent(context, EboxService.class));
        } catch (Exception e) {
            Log.e(Utils.class.getSimpleName(), " exitAppEnd error " + e.getMessage());
        }
        killProcess(context);
    }

    private static void killProcess(Context mAct) {
        String inline;
        String packageName = mAct.getPackageName();
        String processId = EboxConst.TAB_UPLOADER_TAG;
        try {
            Runtime r = Runtime.getRuntime();
            BufferedReader br = new BufferedReader(new InputStreamReader(r.exec("ps").getInputStream()));
            do {
                inline = br.readLine();
                if (inline == null) {
                    break;
                }
            } while (!inline.contains(packageName));
            br.close();
            StringTokenizer processInfoTokenizer = new StringTokenizer(inline);
            int count = 0;
            while (processInfoTokenizer.hasMoreTokens()) {
                count++;
                processId = processInfoTokenizer.nextToken();
                if (count == 2) {
                    break;
                }
            }
            r.exec("kill -9 " + processId);
        } catch (IOException e) {
            IOException error = e;
            error.printStackTrace();
            Log.e(Utils.class.getSimpleName(), " killProcess error " + error.getMessage());
        }
    }

    public static String splicSignature(String signature) {
        if (signature.length() < 8) {
            return signature;
        }
        String one = signature.substring(0, 2);
        String two = signature.substring(2, 4);
        String three = signature.substring(4, 6);
        return "/" + one + "/" + two + "/" + three + "/" + signature.substring(6, 8) + "/";
    }

    public static boolean checkSDCard() {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return false;
        }
        isExitParent(EboxContext.mSystemInfo.getSdPath() + EboxConst.CATCHFODLER + File.separator + EboxConst.CATCHICON + File.separator);
        return true;
    }

    public static boolean checkNetWork(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService("connectivity");
        if (cm.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED || cm.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }
        return false;
    }

    public static String toDate(int val) {
        try {
            return new SimpleDateFormat("yyyy/MM/dd").format(new Date(((long) val) * 1000));
        } catch (Exception e) {
            Exception error = e;
            error.printStackTrace();
            Log.e(Utils.class.getSimpleName(), " toDate error " + error.getMessage());
            return EboxConst.TAB_UPLOADER_TAG;
        }
    }

    public static String toDate(long val) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH.mm.ss").format(new Date(val));
        } catch (Exception e) {
            Exception error = e;
            error.printStackTrace();
            Log.e(Utils.class.getSimpleName(), " toDate error " + error.getMessage());
            return EboxConst.TAB_UPLOADER_TAG;
        }
    }

    public static String getSize(Double size) {
        NumberFormat nFormat = NumberFormat.getInstance();
        double bsize = size.doubleValue();
        if (bsize < 1024.0d) {
            return String.valueOf((int) bsize) + "KB";
        }
        nFormat.setMaximumFractionDigits(1);
        double ksize = bsize / 1024.0d;
        if (ksize < 1024.0d) {
            return nFormat.format(ksize) + "KB";
        }
        nFormat.setMaximumFractionDigits(2);
        double msize = ksize / 1024.0d;
        if (msize < 1024.0d) {
            return nFormat.format(msize) + "M";
        }
        return nFormat.format(msize / 1024.0d) + "G";
    }

    public static void isExitParent(String path) {
        File directory = new File(path).getParentFile();
        if (!directory.exists() && !directory.mkdirs()) {
            try {
                throw new IOException("Path to file could not be created.");
            } catch (IOException e) {
                IOException error = e;
                error.printStackTrace();
                Log.e(Utils.class.getSimpleName(), " isExitParent error " + error.getMessage());
            }
        }
    }

    public static void isExitParent(File file) {
        File directory = file.getParentFile();
        if (!directory.exists() && !directory.mkdirs()) {
            try {
                throw new IOException("Path to file could not be created.");
            } catch (IOException e) {
                IOException error = e;
                error.printStackTrace();
                Log.e(Utils.class.getSimpleName(), " isExitParent error " + error.getMessage());
            }
        }
    }

    public static boolean isExitFile(String path) {
        File f = new File(path);
        String name = f.getName();
        long size = f.length();
        File ref = new File(EboxContext.mSystemInfo.getUserSdPath(), name);
        if (!ref.exists() || ref.length() != size) {
            return false;
        }
        return true;
    }

    public static Bitmap getCatchThumbnail(String key) {
        Bitmap bm = null;
        if (key == null) {
            return null;
        }
        String key2 = key.toLowerCase();
        if (EboxContext.imageCache.containsKey(key2)) {
            SoftReference<Bitmap> softReference = EboxContext.imageCache.get(key2);
            if (softReference.get() != null) {
                bm = softReference.get();
            }
        }
        return bm;
    }

    public static boolean getThumbnailForListFile(HashMap<String, Object> thisHash, double size, int icon) {
        if (!EboxContext.mSystemInfo.hasSdcard()) {
            Log.d(Utils.class.getSimpleName(), "no sdcard getThumbnail out");
            return false;
        }
        String name = thisHash.get("name").toString();
        Log.d(Utils.class.getSimpleName(), "getThumbnail  name " + name);
        if (name == null) {
            Log.d(Utils.class.getSimpleName(), "filename error getTHumbnail out");
            return false;
        }
        String path = EboxContext.mSystemInfo.getUserSdPath() + name;
        File file = new File(path);
        if (!file.exists() || ((double) file.length()) != size) {
            thisHash.put("thumbnail", getSuffix(name) + EboxConst.SUFFIX);
            return false;
        }
        Log.d(Utils.class.getSimpleName(), "get Thumbnails now");
        mediaStoreFileScan(EboxContext.curActivity, file);
        HashMap<String, String> reHash = ActionDB.getThumbnail(EboxContext.curActivity, path, icon);
        if (reHash != null) {
            try {
                thisHash.put("thumbnail", reHash.get("thumbnail"));
                thisHash.put("latitude", reHash.get("latitude"));
                thisHash.put("longitude", reHash.get("longitude"));
            } catch (Exception e) {
                Exception error = e;
                error.printStackTrace();
                Log.d(Utils.class.getSimpleName(), " getThumbnailForListFile error " + error.getMessage());
            }
            return true;
        }
        thisHash.put("thumbnail", getSuffix(name) + EboxConst.SUFFIX);
        return true;
    }

    public static void mediaStoreFileScan(Activity activity, File file) {
        activity.sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.fromFile(file)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap getThumbnail(Context context, String thumbnail, int icon, boolean value) {
        if (thumbnail == null) {
            return null;
        }
        Bitmap bm = getCatchThumbnail(thumbnail);
        if (bm != null) {
            Log.d(Utils.class.getSimpleName(), "catchHash has thumbnail prefect");
            return bm;
        }
        if (value && !thumbnail.contains(".")) {
            if (icon == R.drawable.bt_avi) {
                try {
                    long id = Long.parseLong(thumbnail);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inDither = false;
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    bm = MediaStore.Video.Thumbnails.getThumbnail(context.getContentResolver(), id, 3, options);
                } catch (Exception e) {
                    Exception error = e;
                    error.printStackTrace();
                    Log.e(Utils.class.getSimpleName(), " getThumbnail error " + error.getMessage());
                }
            } else if (icon == R.drawable.bt_jpg) {
                long id2 = Long.parseLong(thumbnail);
                BitmapFactory.Options options2 = new BitmapFactory.Options();
                options2.inDither = false;
                options2.inPreferredConfig = Bitmap.Config.ARGB_8888;
                bm = MediaStore.Images.Thumbnails.getThumbnail(context.getContentResolver(), id2, 3, options2);
            }
            if (bm != null) {
                Log.d(Utils.class.getSimpleName(), " MediaStore get thumbnail success");
                if (EboxContext.mSystemInfo.hasDisplayChange()) {
                    return bm;
                }
                int w = bm.getWidth();
                int h = bm.getHeight();
                Matrix matrix = new Matrix();
                matrix.postScale(48.0f / ((float) w), 48.0f / ((float) h));
                Bitmap resizeBmp = Bitmap.createBitmap(bm, 0, 0, w, h, matrix, true);
                EboxContext.imageCache.put(thumbnail, new SoftReference(resizeBmp));
                return resizeBmp;
            }
        }
        Log.d(Utils.class.getSimpleName(), " local didn't thumbnail, need gotoUrl");
        return bm;
    }

    public static ArrayList<Details> getThumbnailForLocal(Activity activity, String path) {
        Details d;
        ArrayList<Details> arrList = new ArrayList<>();
        try {
            File file = new File(path);
            if (file.exists()) {
                for (File currentFile : file.listFiles()) {
                    String name = currentFile.getName();
                    if (!name.equals(EboxConst.CATCHFODLER)) {
                        String p = currentFile.getAbsolutePath();
                        boolean isUp = false;
                        if (currentFile.isDirectory()) {
                            isUp = true;
                            d = new Details();
                            d.setName(name);
                            d.setPath(p);
                            d.setSize(0.0d);
                            d.setIcon(Integer.valueOf((int) R.drawable.bt_folder));
                            d.setThumbnail("folder.png");
                            d.setSuccess(false);
                        } else {
                            d = ActionDB.queryDetailsByName(activity, name);
                            if (d == null) {
                                d = new Details();
                                d.setName(name);
                                d.setSize((double) currentFile.length());
                                d.setIcon(Integer.valueOf(getDetailsIcon(activity, name)));
                                d.setThumbnail(getSuffix(name) + EboxConst.SUFFIX);
                                d.setSuccess(false);
                            }
                            d.setPath(p);
                        }
                        if (isUp) {
                            arrList.add(0, d);
                        } else {
                            arrList.add(d);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Exception error = e;
            error.printStackTrace();
            Log.e(Utils.class.getSimpleName(), " getLocalListFile error " + error.getMessage());
        }
        return arrList;
    }

    public static int getDetailsIcon(Context context, String fileName) {
        if (checkEndsWithInStringArray(fileName, context.getResources().getStringArray(R.array.fileEndingImage))) {
            return R.drawable.bt_jpg;
        }
        if (checkEndsWithInStringArray(fileName, context.getResources().getStringArray(R.array.fileEndingPackage))) {
            return R.drawable.bt_rar;
        }
        if (checkEndsWithInStringArray(fileName, context.getResources().getStringArray(R.array.fileEndingVideo))) {
            return R.drawable.bt_avi;
        }
        if (checkEndsWithInStringArray(fileName, context.getResources().getStringArray(R.array.fileEndingAudio))) {
            return R.drawable.bt_mp3;
        }
        if (checkEndsWithInStringArray(fileName, context.getResources().getStringArray(R.array.fileEndingWeb))) {
            return R.drawable.bt_html;
        }
        if (checkEndsWithInStringArray(fileName, context.getResources().getStringArray(R.array.fileEndingText))) {
            return R.drawable.bt_txt;
        }
        if (checkEndsWithInStringArray(fileName, context.getResources().getStringArray(R.array.fileEndingDoc))) {
            return R.drawable.bt_doc;
        }
        if (checkEndsWithInStringArray(fileName, context.getResources().getStringArray(R.array.fileEndingPpt))) {
            return R.drawable.bt_ppt;
        }
        if (checkEndsWithInStringArray(fileName, context.getResources().getStringArray(R.array.fileEndingXls))) {
            return R.drawable.bt_xls;
        }
        if (checkEndsWithInStringArray(fileName, context.getResources().getStringArray(R.array.fileEndingPdf))) {
            return R.drawable.bt_pdf;
        }
        return R.drawable.bt_file;
    }

    public static String getSuffix(String name) {
        String filesuffix = EboxConst.UNKNOWN_ICON;
        if (name == null) {
            return filesuffix;
        }
        if (!name.contains(".")) {
            return filesuffix;
        }
        StringTokenizer fx = new StringTokenizer(name.toLowerCase(), ".");
        if (fx.countTokens() < 0) {
            return EboxConst.UNKNOWN_ICON;
        }
        while (fx.hasMoreTokens()) {
            filesuffix = fx.nextToken();
        }
        return filesuffix;
    }

    public static boolean checkEndsWithInStringArray(String checkItsEnd, String[] fileEndings) {
        for (String aEnd : fileEndings) {
            if (checkItsEnd.toLowerCase().endsWith(aEnd)) {
                return true;
            }
        }
        return false;
    }

    public static boolean toDelFile(String name) {
        try {
            File file = new File(name.startsWith(EboxContext.mSystemInfo.getSdPath()) ? name : EboxContext.mSystemInfo.getUserSdPath() + name);
            if (file.exists()) {
                return file.delete();
            }
            return false;
        } catch (Exception e) {
            Exception error = e;
            error.printStackTrace();
            Log.e(Utils.class.getSimpleName(), " toDelFile error " + error.getMessage());
            return false;
        }
    }

    public static boolean toReName(String oldName, String fileName, int icon) {
        File file = new File(EboxContext.mSystemInfo.getUserSdPath() + oldName);
        if (!file.exists()) {
            return false;
        }
        try {
            file.renameTo(new File(EboxContext.mSystemInfo.getUserSdPath() + fileName));
            ActionDB.updateThumbnail(EboxContext.curActivity, oldName, fileName, icon);
            return true;
        } catch (Exception e) {
            Exception error = e;
            error.printStackTrace();
            Log.e(Utils.class.getSimpleName(), " toReName error " + error.getMessage());
            return false;
        }
    }

    public static boolean toCopy(String path, String fileName) {
        Log.d(Utils.class.getSimpleName(), " toCopy " + path + " to  " + fileName);
        boolean success = false;
        File file = new File(path);
        if (file.getAbsolutePath().startsWith(EboxContext.mSystemInfo.getUserSdPath())) {
            return true;
        }
        try {
            FileInputStream fin = new FileInputStream(file);
            FileOutputStream fout = new FileOutputStream(new File(EboxContext.mSystemInfo.getUserSdPath() + fileName));
            try {
                byte[] buffer = new byte[EboxConst.BLOCK_SIZE];
                while (true) {
                    int bytesRead = fin.read(buffer);
                    if (bytesRead < 0) {
                        break;
                    }
                    fout.write(buffer, 0, bytesRead);
                    Thread.sleep(1);
                }
                success = true;
                fout.close();
                fin.close();
                mediaStoreFileScan(EboxContext.curActivity, new File(EboxContext.mSystemInfo.getUserSdPath() + fileName));
            } catch (Exception e) {
                e.printStackTrace();
                fout.close();
                fin.close();
                mediaStoreFileScan(EboxContext.curActivity, new File(EboxContext.mSystemInfo.getUserSdPath() + fileName));
            } catch (Throwable th) {
                fout.close();
                fin.close();
                mediaStoreFileScan(EboxContext.curActivity, new File(EboxContext.mSystemInfo.getUserSdPath() + fileName));
                throw th;
            }
        } catch (FileNotFoundException e2) {
            FileNotFoundException error = e2;
            error.printStackTrace();
            Log.e(Utils.class.getSimpleName(), " toCopy error " + error.getMessage());
        } catch (IOException e3) {
            IOException error2 = e3;
            error2.printStackTrace();
            Log.e(Utils.class.getSimpleName(), " toCopy error " + error2.getMessage());
        } catch (Exception e4) {
            Exception error3 = e4;
            error3.printStackTrace();
            Log.e(Utils.class.getSimpleName(), " toCopy error " + error3.getMessage());
        }
        return success;
    }

    public static long getCatch(String path) {
        long sum = 0;
        if (!path.contains(EboxContext.mSystemInfo.getSdPath())) {
            return 0;
        }
        File file = new File(path);
        try {
            if (file.exists()) {
                for (File currentFile : file.listFiles()) {
                    if (!currentFile.getName().equals(EboxConst.CATCHFODLER)) {
                        sum += currentFile.length();
                    }
                }
            }
        } catch (Exception e) {
            Exception error = e;
            error.printStackTrace();
            Log.e(Utils.class.getSimpleName(), " getCatch error " + error.getMessage());
        }
        return sum;
    }

    public static boolean removeCatch(String path) {
        if (path.contains(EboxContext.mSystemInfo.getSdPath())) {
            File file = new File(path);
            try {
                if (file.exists()) {
                    for (File currentFile : file.listFiles()) {
                        if (!currentFile.getName().equals(EboxConst.CATCHFODLER)) {
                            currentFile.delete();
                        }
                    }
                }
            } catch (Exception e) {
                Exception error = e;
                error.printStackTrace();
                Log.e(Utils.class.getSimpleName(), " getCatch error " + error.getMessage());
            }
        }
        return false;
    }

    public static void stopThreadList() {
        if (EboxContext.uploaderThread != null) {
            for (Thread thread : EboxContext.uploaderThread) {
                if (thread != null) {
                    thread.interrupt();
                }
            }
        }
    }

    public static String replace(String cur, int reString) {
        return cur.replace("%%", reString + EboxConst.TAB_UPLOADER_TAG);
    }

    public static String replace(String cur, double reString) {
        return cur.replace("%%", reString + EboxConst.TAB_UPLOADER_TAG);
    }

    public static String replace(String cur, String reString) {
        return cur.replace("%%", reString);
    }

    public static void reSetSystem(Context context) {
        ActionDB.reSetAccounts(context);
        ActionDB.reSetCongfigs(context);
        ActionDB.reSetContacts(context);
        ActionDB.reSetLocations(context);
        ActionDB.reSetDetails(context);
    }

    public static String md5(String source) {
        char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(source.getBytes());
            byte[] tmp = md.digest();
            char[] str = new char[32];
            int k = 0;
            for (int i = 0; i < 16; i++) {
                byte byte0 = tmp[i];
                int k2 = k + 1;
                str[k] = hexDigits[(byte0 >>> 4) & 15];
                k = k2 + 1;
                str[k2] = hexDigits[byte0 & 15];
            }
            return new String(str);
        } catch (Exception e) {
            Exception error = e;
            error.printStackTrace();
            Log.e(Utils.class.getSimpleName(), " md5 error " + error.getMessage());
            return null;
        }
    }
}
