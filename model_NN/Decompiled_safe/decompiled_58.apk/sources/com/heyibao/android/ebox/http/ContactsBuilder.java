package com.heyibao.android.ebox.http;

import android.app.Service;
import android.util.Log;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.db.ActionDB;
import com.heyibao.android.ebox.model.Json.JsonContacts;
import com.heyibao.android.ebox.model.Json.JsonModel;
import com.heyibao.android.ebox.model.Json.JsonParent;
import com.heyibao.android.ebox.model.MyContacts;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class ContactsBuilder implements Runnable, HttpInterface {
    private Service activity;
    private JSONArray array;
    private Vector<HashMap<String, Integer>> contactsHash;
    private int flag;
    private JSONObject json;
    private String name;
    private int offset = 0;
    private byte[] result;
    private HashMap<String, Object> syncContact;
    private int total = 20;

    public ContactsBuilder(Service activity2, int flag2) {
        this.activity = activity2;
        this.flag = flag2;
    }

    public void run() {
        Log.d(ContactsBuilder.class.getSimpleName(), "## start contactsStart");
        if (this.flag == 0) {
            syncContacts(EboxConst.GET_CONTACTS_COUNT_URL);
        } else if (this.flag == 1) {
            backupStart(EboxConst.BACKUP_CONTACTS_URL, false);
        } else if (this.flag == 2) {
            backupStart(EboxConst.BACKUP_CONTACTS_URL, true);
        } else if (this.flag == 3) {
            restoreStart(false);
        } else if (this.flag == 4) {
            restoreStart(true);
        }
        Log.d(ContactsBuilder.class.getSimpleName(), "end contactsStart");
    }

    private void syncContacts(String url) {
        boolean success = false;
        if (!Thread.interrupted()) {
            try {
                Thread.sleep(10);
                String context = null;
                if (EboxContext.mSystemInfo.getSyncContactsTime() != null) {
                    context = URLEncoder.encode(JsonModel.ContactsJson.LAST_DATE, "UTF-8") + "=" + URLEncoder.encode(EboxContext.mSystemInfo.getSyncContactsTime(), "UTF-8");
                    Log.d(ContactsBuilder.class.getSimpleName(), "## " + context);
                }
                byte[] result2 = new GPBUtils(url).getResult(context == null ? null : context.getBytes());
                if (result2 != null) {
                    JSONObject json2 = new JSONObject(new String(result2));
                    Log.d(ContactsBuilder.class.getSimpleName(), "## " + json2.toString());
                    EboxContext.mSyncContacts.setJson(json2);
                    EboxContext.mSyncContacts.setSyncCount(EboxContext.mSyncContacts.getTotalCount().intValue());
                    EboxContext.mSyncContacts.setRemoteMod(EboxContext.mSyncContacts.getRemoteCount().intValue());
                    success = EboxContext.mSyncContacts.getStatus();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(ContactsBuilder.class.getSimpleName(), "## restoreStart is Force stop");
                EboxContext.message = this.activity.getString(R.string.stop_thread);
            } catch (Exception e2) {
                Exception error = e2;
                error.printStackTrace();
                Log.e(ContactsBuilder.class.getSimpleName(), "## restoreStart is wronging info: " + error.getMessage());
            }
        }
        reportSuccess(success);
    }

    private void backupStart(String url, boolean isAll) {
        String str;
        String str2;
        boolean success = false;
        if (!Thread.interrupted()) {
            try {
                ActionDB.getSysContacts(this.activity, false);
                Vector<Integer> add = new Vector<>();
                Vector<Integer[]> mod = new Vector<>();
                Vector<Integer> del = new Vector<>();
                if (isAll || EboxContext.mSystemInfo.getSyncContactsTime() == null) {
                    while (this.total >= 20) {
                        String context = URLEncoder.encode(JsonModel.ContactsJson.OFFSET, "UTF-8") + "=" + URLEncoder.encode(String.valueOf(this.offset), "UTF-8");
                        Log.d(ContactsBuilder.class.getSimpleName(), "## " + context);
                        this.result = new GPBUtils(EboxConst.GET_CONTACTS_URL).getResult(context == null ? null : context.getBytes());
                        this.json = new JSONObject(new String(this.result));
                        Log.d(ContactsBuilder.class.getSimpleName(), "## " + this.json.toString());
                        EboxContext.mSyncContacts.setJson(this.json);
                        this.total = EboxContext.mSyncContacts.getRemoteCount().intValue();
                        this.offset = EboxContext.mSyncContacts.getOffset().intValue();
                        this.array = (JSONArray) EboxContext.mSyncContacts.getData("contacts");
                        for (int i = 0; i < this.array.length(); i++) {
                            Thread.sleep(1);
                            JsonParent jsonParent = new JsonParent(this.array.getJSONObject(i));
                            if (!jsonParent.getData(JsonModel.STATUS).toString().equals(EboxConst.CONTACT_DELETE)) {
                                int syncId = jsonParent.getId();
                                if (EboxContext.mSystemInfo.getSyncContactsTime() != null) {
                                    this.syncContact = ActionDB.getSyncContactBySyncId(this.activity, syncId);
                                    if (this.syncContact != null) {
                                        mod.add(new Integer[]{(Integer) this.syncContact.get("contact_id"), Integer.valueOf(syncId)});
                                    }
                                }
                                String firstName = jsonParent.getData(JsonModel.ContactsJson.C_FIRST_NAME).toString();
                                String lastName = jsonParent.getData(JsonModel.ContactsJson.C_LAST_NAME).toString();
                                StringBuilder sb = new StringBuilder();
                                if (firstName.equals("null")) {
                                    str = EboxConst.TAB_UPLOADER_TAG;
                                } else {
                                    str = firstName;
                                }
                                StringBuilder append = sb.append(str);
                                if (lastName.equals("null")) {
                                    str2 = EboxConst.TAB_UPLOADER_TAG;
                                } else {
                                    str2 = lastName;
                                }
                                this.name = append.append(str2).toString();
                                this.syncContact = ActionDB.getSyncContactByName(this.activity, this.name, syncId);
                                if (this.syncContact != null) {
                                    mod.add(new Integer[]{(Integer) this.syncContact.get("contact_id"), Integer.valueOf(syncId)});
                                } else if (isAll) {
                                    del.add(Integer.valueOf(syncId));
                                }
                            }
                        }
                    }
                    this.contactsHash = ActionDB.getSyncContactByStatus(this.activity, 3, false);
                    if (this.contactsHash != null) {
                        Iterator i$ = this.contactsHash.iterator();
                        while (i$.hasNext()) {
                            add.add(i$.next().get("contact_id"));
                        }
                    }
                } else {
                    this.contactsHash = ActionDB.getSyncContactByStatus(this.activity, 0, true);
                    Iterator i$2 = this.contactsHash.iterator();
                    while (i$2.hasNext()) {
                        add.add(i$2.next().get("contact_id"));
                    }
                    this.contactsHash = ActionDB.getSyncContactByStatus(this.activity, 1, true);
                    Iterator i$3 = this.contactsHash.iterator();
                    while (i$3.hasNext()) {
                        HashMap<String, Integer> item = i$3.next();
                        mod.add(new Integer[]{(Integer) item.get("contact_id"), (Integer) item.get("sync_id")});
                    }
                    this.contactsHash = ActionDB.getSyncContactByStatus(this.activity, 3, false);
                    Iterator i$4 = this.contactsHash.iterator();
                    while (i$4.hasNext()) {
                        HashMap<String, Integer> item2 = i$4.next();
                        int contactId = ((Integer) item2.get("contact_id")).intValue();
                        int syncId2 = ((Integer) item2.get("sync_id")).intValue();
                        if (ActionDB.getSysContactById(this.activity, contactId)) {
                            del.add(Integer.valueOf(syncId2));
                        } else {
                            ActionDB.updateSyncContacts(this.activity, contactId, -1, null, null);
                        }
                    }
                }
                EboxContext.mSyncContacts.setInsert(add.size());
                EboxContext.mSyncContacts.setModify(mod.size());
                EboxContext.mSyncContacts.setDelete(del.size());
                while (EboxContext.mSystemNotify.hasNotifyThreadWait()) {
                    Thread.sleep(1000);
                }
                Iterator i$5 = add.iterator();
                int num = 1;
                while (i$5.hasNext()) {
                    int item3 = ((Integer) i$5.next()).intValue();
                    Thread.sleep(10);
                    MyContacts contact = ActionDB.getContacts(this.activity, item3);
                    if (contact != null) {
                        JsonContacts retval = sendPost(url, contact);
                        if (!retval.getStatus()) {
                            Log.d(ContactsBuilder.class.getSimpleName(), "## backupStart is false ");
                        } else {
                            ActionDB.updateSyncContacts(this.activity, contact.getContactId(), retval.getId(), null, retval.getModifiedTime());
                            int num2 = num + 1;
                            reportProgress(num, 0);
                            while (EboxContext.mSystemNotify.hasNotifyThreadWait()) {
                                Thread.sleep(1000);
                            }
                            num = num2;
                        }
                    }
                }
                Iterator i$6 = mod.iterator();
                while (i$6.hasNext()) {
                    Integer[] item4 = (Integer[]) i$6.next();
                    Thread.sleep(10);
                    MyContacts contact2 = ActionDB.getContacts(this.activity, item4[0].intValue());
                    if (contact2 != null) {
                        contact2.setRemoteId(item4[1].intValue());
                        JsonContacts retval2 = sendPost(url, contact2);
                        if (!retval2.getStatus()) {
                            Log.d(ContactsBuilder.class.getSimpleName(), "## backupStart is false ");
                        } else {
                            ActionDB.updateSyncContacts(this.activity, contact2.getContactId(), retval2.getId(), contact2.getName(), retval2.getModifiedTime());
                            int num3 = num + 1;
                            reportProgress(num, 0);
                            while (EboxContext.mSystemNotify.hasNotifyThreadWait()) {
                                Thread.sleep(1000);
                            }
                            num = num3;
                        }
                    }
                }
                Iterator i$7 = del.iterator();
                while (i$7.hasNext()) {
                    Integer item5 = (Integer) i$7.next();
                    Thread.sleep(10);
                    JsonContacts contacts = sendPost(EboxConst.DEL_CONTACTS_URL, item5.intValue());
                    if (!contacts.getStatus()) {
                        Log.d(ContactsBuilder.class.getSimpleName(), "## backupStart is false ");
                    } else {
                        ActionDB.delSyncContacts(this.activity, -1, item5.intValue(), contacts.getModifiedTime());
                        int num4 = num + 1;
                        reportProgress(num, 0);
                        while (EboxContext.mSystemNotify.hasNotifyThreadWait()) {
                            Thread.sleep(1000);
                        }
                        num = num4;
                    }
                }
                EboxContext.mSyncContacts.setSyncCount((EboxContext.mSyncContacts.getSyncCount() + add.size()) - del.size());
                success = true;
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(ContactsBuilder.class.getSimpleName(), "## backupStart is Force stop");
                EboxContext.message = this.activity.getString(R.string.stop_thread);
            } catch (Exception e2) {
                Exception error = e2;
                error.printStackTrace();
                Log.e(ContactsBuilder.class.getSimpleName(), "## backupStart is wronging info: " + error.getMessage());
            }
        }
        reportSuccess(success);
    }

    private void restoreStart(boolean isAll) {
        String str;
        int num;
        int num2;
        boolean success = false;
        if (!Thread.interrupted()) {
            try {
                ActionDB.getSysContacts(this.activity, false);
                int num3 = 1;
                String time = EboxConst.TAB_UPLOADER_TAG;
                if (isAll || EboxContext.mSystemInfo.getSyncContactsTime() == null) {
                    while (this.total >= 20) {
                        this.result = new GPBUtils(EboxConst.GET_CONTACTS_URL).getResult((URLEncoder.encode(JsonModel.ContactsJson.OFFSET, "UTF-8") + "=" + URLEncoder.encode(String.valueOf(this.offset), "UTF-8")).getBytes());
                        this.json = new JSONObject(new String(this.result));
                        Log.d(ContactsBuilder.class.getSimpleName(), "## " + this.json.toString());
                        EboxContext.mSyncContacts.setJson(this.json);
                        this.offset = EboxContext.mSyncContacts.getOffset().intValue();
                        this.total = EboxContext.mSyncContacts.getRemoteCount().intValue();
                        this.array = (JSONArray) EboxContext.mSyncContacts.getData("contacts");
                        int i = 0;
                        int num4 = num3;
                        while (i < this.array.length()) {
                            Thread.sleep(1);
                            JsonParent jsonParent = new JsonParent(this.array.getJSONObject(i));
                            String firstName = jsonParent.getData(JsonModel.ContactsJson.C_FIRST_NAME).toString();
                            String lastName = jsonParent.getData(JsonModel.ContactsJson.C_LAST_NAME).toString();
                            StringBuilder sb = new StringBuilder();
                            if (firstName.equals("null")) {
                                str = EboxConst.TAB_UPLOADER_TAG;
                            } else {
                                str = firstName;
                            }
                            this.name = sb.append(str).append(lastName.equals("null") ? EboxConst.TAB_UPLOADER_TAG : lastName).toString();
                            if (time.compareTo(jsonParent.getModifiedTime()) <= 0) {
                                time = jsonParent.getModifiedTime();
                            }
                            this.syncContact = ActionDB.getSyncContactBySyncId(this.activity, jsonParent.getId());
                            if (this.syncContact == null) {
                                this.syncContact = ActionDB.getSyncContactByName(this.activity, this.name, jsonParent.getId());
                                if (this.syncContact != null) {
                                    if (jsonParent.getData(JsonModel.STATUS).toString().equals(EboxConst.CONTACT_DELETE)) {
                                        new MyContacts();
                                        MyContacts curContact = new MyContacts();
                                        curContact.setRemoteId(jsonParent.getId());
                                        curContact.setContactId(((Integer) this.syncContact.get("contact_id")).intValue());
                                        curContact.setModifiedTime(time);
                                        num = num4 + 1;
                                        reStoreDel(curContact, num4);
                                    } else {
                                        MyContacts curContact2 = new MyContacts(jsonParent);
                                        curContact2.setContactId(((Integer) this.syncContact.get("contact_id")).intValue());
                                        curContact2.setPhotoBt(curContact2.getPhotoUrl());
                                        num = num4 + 1;
                                        reStoreMod(curContact2, num4);
                                    }
                                } else if (jsonParent.getData(JsonModel.STATUS).toString().equals(EboxConst.CONTACT_DELETE)) {
                                    num = num4;
                                } else {
                                    MyContacts curContact3 = new MyContacts(jsonParent);
                                    curContact3.setPhotoBt(curContact3.getPhotoUrl());
                                    num = num4 + 1;
                                    reStoreAdd(curContact3, num4);
                                }
                            } else if (jsonParent.getData(JsonModel.STATUS).toString().equals(EboxConst.CONTACT_DELETE)) {
                                MyContacts curContact4 = new MyContacts();
                                curContact4.setRemoteId(jsonParent.getId());
                                curContact4.setContactId(((Integer) this.syncContact.get("contact_id")).intValue());
                                curContact4.setModifiedTime(time);
                                num = num4 + 1;
                                reStoreDel(curContact4, num4);
                            } else {
                                MyContacts curContact5 = new MyContacts(jsonParent);
                                curContact5.setPhotoBt(curContact5.getPhotoUrl());
                                curContact5.setContactId(((Integer) this.syncContact.get("contact_id")).intValue());
                                num = num4 + 1;
                                reStoreMod(curContact5, num4);
                            }
                            i++;
                            num4 = num;
                        }
                        num3 = num4;
                    }
                    if (isAll) {
                        this.contactsHash = ActionDB.getSyncContactByStatus(this.activity, 3, false);
                        Iterator i$ = this.contactsHash.iterator();
                        while (i$.hasNext()) {
                            MyContacts curContact6 = new MyContacts();
                            curContact6.setContactId(((Integer) i$.next().get("contact_id")).intValue());
                            curContact6.setModifiedTime(time);
                            reStoreDel(curContact6, num3);
                        }
                    }
                } else {
                    time = EboxContext.mSystemInfo.getSyncContactsTime();
                    while (this.total >= 20) {
                        this.result = new GPBUtils(EboxConst.GET_CONTACTS_URL).getResult(((URLEncoder.encode(JsonModel.ContactsJson.OFFSET, "UTF-8") + "=" + URLEncoder.encode(String.valueOf(this.offset), "UTF-8")) + "&" + URLEncoder.encode(JsonModel.ContactsJson.LAST_DATE, "UTF-8") + "=" + URLEncoder.encode(EboxContext.mSystemInfo.getSyncContactsTime(), "UTF-8")).getBytes());
                        this.json = new JSONObject(new String(this.result));
                        Log.d(ContactsBuilder.class.getSimpleName(), "## " + this.json.toString());
                        EboxContext.mSyncContacts.setJson(this.json);
                        this.total = EboxContext.mSyncContacts.getRemoteCount().intValue();
                        this.offset = EboxContext.mSyncContacts.getOffset().intValue();
                        this.array = (JSONArray) EboxContext.mSyncContacts.getData("contacts");
                        int i2 = 0;
                        int num5 = num3;
                        while (i2 < this.array.length()) {
                            Thread.sleep(1);
                            JsonParent jsonParent2 = new JsonParent(this.array.getJSONObject(i2));
                            if (time.compareTo(jsonParent2.getModifiedTime()) <= 0) {
                                time = jsonParent2.getModifiedTime();
                            }
                            this.syncContact = ActionDB.getSyncContactBySyncId(this.activity, jsonParent2.getId());
                            if (this.syncContact != null) {
                                if (jsonParent2.getData(JsonModel.STATUS).toString().equals(EboxConst.CONTACT_DELETE)) {
                                    MyContacts curContact7 = new MyContacts();
                                    curContact7.setContactId(((Integer) this.syncContact.get("contact_id")).intValue());
                                    curContact7.setRemoteId(jsonParent2.getId());
                                    curContact7.setModifiedTime(jsonParent2.getData("modified").toString());
                                    num2 = num5 + 1;
                                    reStoreDel(curContact7, num5);
                                } else {
                                    MyContacts curContact8 = new MyContacts(jsonParent2);
                                    curContact8.setContactId(((Integer) this.syncContact.get("contact_id")).intValue());
                                    curContact8.setPhotoBt(curContact8.getPhotoUrl());
                                    num2 = num5 + 1;
                                    reStoreMod(curContact8, num5);
                                }
                            } else if (jsonParent2.getData(JsonModel.STATUS).toString().equals(EboxConst.CONTACT_DELETE)) {
                                num2 = num5;
                            } else {
                                MyContacts curContact9 = new MyContacts(jsonParent2);
                                curContact9.setPhotoBt(curContact9.getPhotoUrl());
                                num2 = num5 + 1;
                                reStoreAdd(curContact9, num5);
                            }
                            i2++;
                            num5 = num2;
                        }
                        num3 = num5;
                    }
                }
                ActionDB.updateSyncContactsOK(this.activity, time);
                reportProgress(EboxContext.mSyncContacts.getSyncCount(), 0);
                EboxContext.mSyncContacts.setLocalCount((EboxContext.mSyncContacts.getLocalCount() + EboxContext.mSyncContacts.getInsert()) - EboxContext.mSyncContacts.getDelete());
                success = true;
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(ContactsBuilder.class.getSimpleName(), "## restoreStart is Force stop");
                EboxContext.message = this.activity.getString(R.string.stop_thread);
            } catch (Exception e2) {
                Exception error = e2;
                error.printStackTrace();
                Log.e(ContactsBuilder.class.getSimpleName(), "## restoreStart is wronging info: " + error.getMessage());
            }
        }
        reportSuccess(success);
    }

    private boolean reStoreAdd(MyContacts curContact, int num) throws InterruptedException {
        while (EboxContext.mSystemNotify.hasNotifyThreadWait()) {
            Thread.sleep(1000);
        }
        if (curContact.getName() == null || curContact.getName().equals(EboxConst.TAB_UPLOADER_TAG)) {
            return false;
        }
        ActionDB.insertContacts(this.activity, curContact);
        EboxContext.mSyncContacts.setInsert(EboxContext.mSyncContacts.getInsert() + 1);
        reportProgress(num, 0);
        return true;
    }

    private boolean reStoreMod(MyContacts curContact, int num) throws InterruptedException {
        while (EboxContext.mSystemNotify.hasNotifyThreadWait()) {
            Thread.sleep(1000);
        }
        if (curContact.getName() == null || curContact.getName().equals(EboxConst.TAB_UPLOADER_TAG)) {
            return false;
        }
        ActionDB.updateContacts(this.activity, curContact);
        EboxContext.mSyncContacts.setModify(EboxContext.mSyncContacts.getModify() + 1);
        reportProgress(num, 0);
        return true;
    }

    private boolean reStoreDel(MyContacts curContact, int num) throws InterruptedException {
        while (EboxContext.mSystemNotify.hasNotifyThreadWait()) {
            Thread.sleep(1000);
        }
        ActionDB.delContacts(this.activity, curContact);
        EboxContext.mSyncContacts.setDelete(EboxContext.mSyncContacts.getDelete() + 1);
        reportProgress(num, 0);
        return true;
    }

    private JsonContacts sendPost(String url, MyContacts m) throws Exception {
        byte[] result2;
        if (m.getPhotoBt() == null) {
            String content = upload(m);
            result2 = new GPBUtils(url).getResult(content == null ? null : content.getBytes());
        } else {
            result2 = new GPBUtils(url).getResult(uploadWithPhoto(m), m.getPhotoBt());
        }
        JSONObject json2 = new JSONObject(new String(result2));
        Log.d(ContactsBuilder.class.getSimpleName(), json2.toString());
        return new JsonContacts(json2);
    }

    private JsonContacts sendPost(String url, int syncId) throws Exception {
        JSONObject json2 = new JSONObject(new String(new GPBUtils(url).getResult((URLEncoder.encode("id", "UTF-8") + "=" + URLEncoder.encode(syncId + EboxConst.TAB_UPLOADER_TAG, "UTF-8")).getBytes())));
        Log.d(ContactsBuilder.class.getSimpleName(), json2.toString());
        return new JsonContacts(json2);
    }

    private Hashtable<String, String> uploadWithPhoto(MyContacts myContacts) {
        MyContacts.Address add;
        MyContacts.Organization org;
        Hashtable<String, String> parameter = new Hashtable<>();
        if (myContacts.getSyncId() != -1) {
            parameter.put("id", String.valueOf(myContacts.getSyncId()));
        }
        if (myContacts.getFirstName() != null) {
            parameter.put(JsonModel.ContactsJson.C_FIRST_NAME, myContacts.getFirstName());
        }
        if (myContacts.getLastName() != null) {
            parameter.put(JsonModel.ContactsJson.C_LAST_NAME, myContacts.getLastName());
        }
        if (myContacts.getNote() != null) {
            parameter.put("content", myContacts.getNote());
        }
        if (myContacts.getPhoneV2() != null) {
            for (int i = 1; i <= 13; i++) {
                String phone = myContacts.getPhoneV2().get(Integer.valueOf(i));
                if (phone != null) {
                    if (i == 1) {
                        parameter.put(JsonModel.ContactsJson.C_HOME_PHONE, phone);
                    } else if (i == 2) {
                        parameter.put(JsonModel.ContactsJson.C_MOBLIE_PHONE, phone);
                    } else if (i == 3) {
                        parameter.put(JsonModel.ContactsJson.C_WORK_PHONE, phone);
                    } else if (i == 4) {
                        parameter.put(JsonModel.ContactsJson.C_FAX_PHONT, phone);
                    } else if (i == 7) {
                        parameter.put(JsonModel.ContactsJson.C_OTHER_PHONE, phone);
                    }
                }
            }
        }
        if (myContacts.getOrganization() != null) {
            for (int i2 = 0; i2 <= 2; i2++) {
                if (i2 == 1 && (org = myContacts.getOrganization().get(Integer.valueOf(i2))) != null) {
                    String value = org.getCompany();
                    if (value != null) {
                        parameter.put(JsonModel.ContactsJson.C_COMPANY, value);
                    }
                    String value2 = org.getDuties();
                    if (value2 != null) {
                        parameter.put(JsonModel.ContactsJson.C_TITLE, value2);
                    }
                }
            }
        }
        if (myContacts.getEmailV2() != null) {
            for (int i3 = 0; i3 <= 4; i3++) {
                String email = myContacts.getEmailV2().get(Integer.valueOf(i3));
                if (email != null && i3 == 1) {
                    parameter.put(JsonModel.ContactsJson.C_EMAIL, email);
                }
            }
        }
        if (myContacts.getIm() != null) {
            for (int i4 = 0; i4 <= 8; i4++) {
                String qq = myContacts.getIm().get(Integer.valueOf(i4));
                if (qq != null && i4 == 4) {
                    parameter.put(JsonModel.ContactsJson.C_IM_QQ, qq);
                }
            }
        }
        if (myContacts.getPostalAddressV2() != null) {
            for (int i5 = 0; i5 <= 3; i5++) {
                if (i5 == 1 && (add = myContacts.getPostalAddressV2().get(Integer.valueOf(i5))) != null) {
                    String value3 = add.getAddress();
                    if (value3 != null) {
                        parameter.put(JsonModel.ContactsJson.C_ADDRESS, value3);
                    }
                    String value4 = add.getCity();
                    if (value4 != null) {
                        parameter.put(JsonModel.ContactsJson.C_CITY, value4);
                    }
                    String value5 = add.getCountry();
                    if (value5 != null) {
                        parameter.put(JsonModel.ContactsJson.C_COUNTRY, value5);
                    }
                    String value6 = add.getZip();
                    if (value6 != null) {
                        parameter.put(JsonModel.ContactsJson.C_ZIP, value6);
                    }
                    String value7 = add.getProvince();
                    if (value7 != null) {
                        parameter.put(JsonModel.ContactsJson.C_PROVINCE, value7);
                    }
                }
            }
        }
        if (myContacts.getWebsite() != null) {
            for (int i6 = 0; i6 <= 7; i6++) {
                String webSite = myContacts.getWebsite().get(Integer.valueOf(i6));
                if (webSite != null && i6 == 7) {
                    parameter.put(JsonModel.ContactsJson.C_WEB_SITE, webSite);
                }
            }
        }
        return parameter;
    }

    private String upload(MyContacts myContacts) {
        MyContacts.Address add;
        MyContacts.Organization org;
        String content = EboxConst.TAB_UPLOADER_TAG;
        try {
            if (myContacts.getSyncId() != -1) {
                content = content + URLEncoder.encode("id", "UTF-8") + "=" + URLEncoder.encode(myContacts.getSyncId() + EboxConst.TAB_UPLOADER_TAG, "UTF-8") + "&";
            }
            if (myContacts.getFirstName() != null) {
                content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_FIRST_NAME, "UTF-8") + "=" + URLEncoder.encode(myContacts.getFirstName(), "UTF-8");
            }
            if (myContacts.getLastName() != null) {
                content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_LAST_NAME, "UTF-8") + "=" + URLEncoder.encode(myContacts.getLastName(), "UTF-8");
            }
            if (myContacts.getNote() != null) {
                content = content + "&" + URLEncoder.encode("content", "UTF-8") + "=" + URLEncoder.encode(myContacts.getNote(), "UTF-8");
            }
            if (myContacts.getPhoneV2() != null) {
                for (int i = 0; i <= 13; i++) {
                    String phone = myContacts.getPhoneV2().get(Integer.valueOf(i));
                    if (phone != null) {
                        if (i == 0) {
                            content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_MOBLIE_PHONE, "UTF-8") + "=" + URLEncoder.encode(phone, "UTF-8");
                        } else if (i == 1) {
                            content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_HOME_PHONE, "UTF-8") + "=" + URLEncoder.encode(phone, "UTF-8");
                        } else if (i == 2) {
                            content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_MOBLIE_PHONE, "UTF-8") + "=" + URLEncoder.encode(phone, "UTF-8");
                        } else if (i == 3) {
                            content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_WORK_PHONE, "UTF-8") + "=" + URLEncoder.encode(phone, "UTF-8");
                        } else if (i == 4) {
                            content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_FAX_PHONT, "UTF-8") + "=" + URLEncoder.encode(phone, "UTF-8");
                        } else if (i == 7) {
                            content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_OTHER_PHONE, "UTF-8") + "=" + URLEncoder.encode(phone, "UTF-8");
                        }
                    }
                }
            }
            if (myContacts.getOrganization() != null) {
                for (int i2 = 0; i2 <= 2; i2++) {
                    if (i2 == 1 && (org = myContacts.getOrganization().get(Integer.valueOf(i2))) != null) {
                        String value = org.getCompany();
                        if (value != null) {
                            content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_COMPANY, "UTF-8") + "=" + URLEncoder.encode(value, "UTF-8");
                        }
                        String value2 = org.getDuties();
                        if (value2 != null) {
                            content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_TITLE, "UTF-8") + "=" + URLEncoder.encode(value2, "UTF-8");
                        }
                    }
                }
            }
            if (myContacts.getEmailV2() != null) {
                for (int i3 = 0; i3 <= 4; i3++) {
                    String email = myContacts.getEmailV2().get(Integer.valueOf(i3));
                    if (email != null && i3 == 1) {
                        content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_EMAIL, "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8");
                    }
                }
            }
            if (myContacts.getIm() != null) {
                for (int i4 = 0; i4 <= 8; i4++) {
                    String qq = myContacts.getIm().get(Integer.valueOf(i4));
                    if (qq != null && i4 == 4) {
                        content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_IM_QQ, "UTF-8") + "=" + URLEncoder.encode(qq, "UTF-8");
                    }
                }
            }
            if (myContacts.getPostalAddressV2() != null) {
                for (int i5 = 0; i5 <= 3; i5++) {
                    if (i5 == 1 && (add = myContacts.getPostalAddressV2().get(Integer.valueOf(i5))) != null) {
                        String value3 = add.getAddress();
                        if (value3 != null) {
                            content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_ADDRESS, "UTF-8") + "=" + URLEncoder.encode(value3, "UTF-8");
                        }
                        String value4 = add.getCity();
                        if (value4 != null) {
                            content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_CITY, "UTF-8") + "=" + URLEncoder.encode(value4, "UTF-8");
                        }
                        String value5 = add.getCountry();
                        if (value5 != null) {
                            content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_COUNTRY, "UTF-8") + "=" + URLEncoder.encode(value5, "UTF-8");
                        }
                        String value6 = add.getZip();
                        if (value6 != null) {
                            content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_ZIP, "UTF-8") + "=" + URLEncoder.encode(value6, "UTF-8");
                        }
                        String value7 = add.getProvince();
                        if (value7 != null) {
                            content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_PROVINCE, "UTF-8") + "=" + URLEncoder.encode(value7, "UTF-8");
                        }
                    }
                }
            }
            if (myContacts.getWebsite() != null) {
                for (int i6 = 0; i6 <= 7; i6++) {
                    String webSite = myContacts.getWebsite().get(Integer.valueOf(i6));
                    if (webSite != null && i6 == 7) {
                        content = content + "&" + URLEncoder.encode(JsonModel.ContactsJson.C_WEB_SITE, "UTF-8") + "=" + URLEncoder.encode(webSite, "UTF-8");
                    }
                }
            }
        } catch (UnsupportedEncodingException e) {
            UnsupportedEncodingException e2 = e;
            e2.printStackTrace();
            Log.e(ContactsBuilder.class.getSimpleName(), "contacts upload to json error: " + e2.getMessage());
        }
        return content;
    }
}
