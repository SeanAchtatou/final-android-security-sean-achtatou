package com.heyibao.android.ebox.http;

import ClientProtocol.ClientMeta;
import android.app.Service;
import android.util.Log;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.db.ActionDB;
import com.heyibao.android.ebox.util.Utils;
import java.util.HashMap;
import java.util.Vector;

public abstract class ListFileBuilder implements Runnable, HttpInterface {
    private Service activity;
    private String objectType;
    private String objectUUID;
    private String parentUUID;

    public ListFileBuilder(Service activity2, String objectUUID2, String objectType2, String parentUUID2) {
        this.activity = activity2;
        this.objectUUID = objectUUID2;
        this.parentUUID = parentUUID2;
        this.objectType = objectType2;
    }

    public void run() {
        Log.d(ListFileBuilder.class.getSimpleName(), "## start listfile");
        listFileStart();
        Log.d(ListFileBuilder.class.getSimpleName(), "end ListFileBuilder ");
    }

    private void listFileStart() {
        Log.d(ListFileBuilder.class.getSimpleName(), "##### listfile");
        boolean success = false;
        if (!Thread.interrupted() && 0 < 3) {
            try {
                ClientMeta.ListFile.Builder listFile = ClientMeta.ListFile.newBuilder();
                listFile.setShowUuid(this.objectUUID);
                ClientMeta.DirectoryDetails.Builder dirDetails = ClientMeta.DirectoryDetails.newBuilder();
                dirDetails.setShowUuid(this.objectUUID);
                dirDetails.setParentShowUuid(this.parentUUID);
                ClientMeta.Action.Builder action = ClientMeta.Action.newBuilder();
                action.setActionType(ClientMeta.ActionType.LIST_FILE);
                action.setListFile(listFile);
                action.setDirDetails(dirDetails);
                ClientMeta.UploadMetadataRequest.Builder request = ClientMeta.UploadMetadataRequest.newBuilder();
                request.setSession(EboxContext.mDevice.getSession());
                request.addAction(action);
                Thread.sleep(10);
                ClientMeta.UploadMetadataResponse response = ClientMeta.UploadMetadataResponse.parseFrom(new GPBUtils(EboxConst.UPLOADMETA_URL).getResult(request.build().toByteArray()));
                ClientMeta.Status status = response.getAction(0).getStatus();
                if (!response.getStatus().getSuccess()) {
                    EboxContext.message = EboxConst.SESSIONEXIT;
                } else if (status.getSuccess()) {
                    success = true;
                    ClientMeta.ListFile lf = response.getActionList().get(0).getListFile();
                    Vector<HashMap<String, Object>> detailVectors = new Vector<>();
                    for (int i = 0; i < lf.getFileDetailsCount(); i++) {
                        ClientMeta.FileDetails fd = lf.getFileDetails(i);
                        HashMap<String, Object> thisHash = new HashMap<>();
                        thisHash.put("name", fd.getName());
                        thisHash.put("size", Integer.valueOf(fd.getSize()));
                        thisHash.put("createTime", Integer.valueOf(fd.getCreateTime()));
                        thisHash.put("modifyTime", Integer.valueOf(fd.getModifyTime()));
                        thisHash.put("showUUID", fd.getShowUuid());
                        thisHash.put("parentShowUUID", this.objectUUID);
                        thisHash.put("signature", fd.getSignature());
                        thisHash.put("objectType", this.objectType + fd.getObjectType());
                        thisHash.put("exist", null);
                        if ((fd.getName().equals(this.activity.getString(R.string.delivery_folder)) || this.objectType.indexOf(EboxConst.FOLDERDELIVERY) >= 0) && fd.getObjectType() == 1) {
                            thisHash.put("objectType", this.objectType + EboxConst.FOLDERDELIVERY);
                            thisHash.put("image", Integer.valueOf((int) R.drawable.bt_folder));
                            thisHash.put("thumbnail", "mail_receive.png");
                        } else if (fd.getObjectType() == 2 || this.objectType.indexOf(EboxConst.FOLDERSHARE) >= 0) {
                            thisHash.put("image", Integer.valueOf((int) R.drawable.bt_folder));
                            thisHash.put("thumbnail", "folder_share.png");
                        } else if (fd.getObjectType() == 3 || this.objectType.indexOf(EboxConst.FOLDERSHAREED) >= 0) {
                            thisHash.put("image", Integer.valueOf((int) R.drawable.bt_folder));
                            thisHash.put("thumbnail", "folder_share.png");
                        } else if (fd.getObjectType() == 1) {
                            thisHash.put("image", Integer.valueOf((int) R.drawable.bt_folder));
                            thisHash.put("thumbnail", "folder.png");
                        } else {
                            int icon = Utils.getDetailsIcon(this.activity, fd.getName());
                            thisHash.put("image", Integer.valueOf(icon));
                            thisHash.put("exist", Integer.valueOf(Utils.getThumbnailForListFile(thisHash, (double) fd.getSize(), icon) ? 1 : 0));
                        }
                        detailVectors.add(i, thisHash);
                    }
                    if (!ActionDB.updateToDetails(this.activity, detailVectors, this.objectUUID)) {
                        Log.e(ListFileBuilder.class.getSimpleName(), "## listfile success but to sqlit is wrong");
                        success = false;
                    }
                } else {
                    EboxContext.message = status.getMsg();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(ListFileBuilder.class.getSimpleName(), "## listfile is Force stop");
                EboxContext.message = this.activity.getString(R.string.stop_thread);
            } catch (Exception e2) {
                Exception error = e2;
                error.printStackTrace();
                Log.e(ListFileBuilder.class.getSimpleName(), "## listfile is wrong error info " + error.getMessage() + " for " + 0);
                int errcount = 0 + 1;
                EboxContext.message = this.activity.getResources().getString(R.string.net_error);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
            }
        }
        reportSuccess(success);
    }
}
