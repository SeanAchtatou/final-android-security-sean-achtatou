package com.heyibao.android.ebox.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.model.Json.JsonModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

public class EboxDB {
    private static final String ACCOUNTS_TABLE = "accounts";
    private static final String ALTER_CONFIG_SIGN_TIME = "ALTER TABLE configs ADD sign_time text;";
    private static final String ALTER_CONFIG_SYNC_CONTACTS_TIME = "ALTER TABLE configs ADD sync_contacts_time text;";
    private static final String ALTER_DETAILS_EXIST = "ALTER TABLE details ADD exist integer DEFAULT 0 NOT NULL;";
    private static final String ALTER_DETAILS_LATITUDE = "ALTER TABLE details ADD latitude text;";
    private static final String ALTER_DETAILS_LONGITUDE = "ALTER TABLE details ADD longitude text;";
    private static final String ALTER_DETAILS_THUMBNAIL = "ALTER TABLE details ADD thumbnail text;";
    private static final String CONFIGS_TABLE = "configs";
    public static final String CONTACTS_TABLE = "contacts";
    private static final String CREATE_TABLE_ACCOUNTS = "create table if not exists accounts (id integer primary key autoincrement, device_name text, device_info text, team_name text, user_name text, password text, nick text, phone text, email text, team_uuid text, user_uuid text, device_uuid text, is_admin  integer DEFAULT 0 NOT NULL, current_size float DEFAULT 0.0 NOT NULL, space_size float DEFAULT 0.0 NOT NULL);";
    private static final String CREATE_TABLE_CONFIGS = "create table if not exists configs (id integer primary key autoincrement, last_time integer DEFAULT 0 NOT NULL, auto_update integer DEFAULT 1 NOT NULL, auto_login integer DEFAULT 1 NOT NULL, offline_login integer DEFAULT 0 NOT NULL, notify_info integer DEFAULT 1 NOT NULL, update_freq integer DEFAULT 0 NOT NULL);";
    private static final String CREATE_TABLE_CONTACTS = "create table if not exists contacts (id integer primary key autoincrement, sync_id integer DEFAULT 0 NOT NULL, contact_id integer, contact_name text NOT NULL, version integer DEFAULT 0 NOT NULL, status integer DEFAULT 0 NOT NULL );";
    private static final String CREATE_TABLE_DETAILS = "create table if not exists details (id integer primary key autoincrement, name text, size integer, create_time integer, update_time integer, image integer, object_type text, object_uuid text, parent_uuid text, signature text);";
    private static final String CREATE_TABLE_LOCATION_SOURCE = "create table if not exists locations (id integer primary key autoincrement, latitude text, longitude text, name text, create_time text);";
    private static final String DETAILS_TABLE = "details";
    private static final String INSERT_CONFIGS = "insert into configs (id) values (0)";
    private static final String LOCATIONS_TABLE = "locations";
    private static EboxDB sInstance;
    private final String DATABASE_NAME = EboxContext.mSystemInfo.getAppName();
    private final int DATABASE_VERSION = EboxContext.mSystemInfo.getDBVersion();
    private SQLiteDatabase db;

    public EboxDB(Context ctx) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        this.db.execSQL(CREATE_TABLE_ACCOUNTS);
        this.db.execSQL(CREATE_TABLE_DETAILS);
        this.db.execSQL(CREATE_TABLE_CONFIGS);
        if (this.db.getVersion() < 1) {
            EboxContext.mSystemInfo.setFirst(true);
        }
        if (this.db.getVersion() < 2) {
            try {
                this.db.execSQL(ALTER_DETAILS_THUMBNAIL);
                this.db.execSQL(ALTER_DETAILS_EXIST);
                this.db.execSQL(ALTER_DETAILS_LATITUDE);
                this.db.execSQL(ALTER_DETAILS_LONGITUDE);
            } catch (Exception e) {
            }
        }
        if (this.db.getVersion() < 3) {
            try {
                this.db.execSQL(CREATE_TABLE_CONTACTS);
                this.db.execSQL(ALTER_CONFIG_SYNC_CONTACTS_TIME);
            } catch (Exception e2) {
            }
        }
        if (this.db.getVersion() < 4) {
            try {
                this.db.execSQL(CREATE_TABLE_LOCATION_SOURCE);
                this.db.execSQL(ALTER_CONFIG_SIGN_TIME);
            } catch (Exception e3) {
            }
        }
        if (this.db.getVersion() < this.DATABASE_VERSION) {
            EboxContext.mSystemInfo.setUpdate(true);
            if (!EboxContext.mSystemInfo.hasFirst()) {
                this.db.setVersion(this.DATABASE_VERSION);
            }
        }
        this.db.close();
    }

    public static EboxDB getInstance(Context activity) {
        if (sInstance == null) {
            sInstance = new EboxDB(activity);
        }
        return sInstance;
    }

    public void atFirst(Context ctx) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        EboxContext.mSystemInfo.setFirst(false);
        this.db.setVersion(this.DATABASE_VERSION);
        this.db.close();
    }

    public boolean addAccount(Context ctx, String deviceName, String deviceInfo, String teamName, String userName, String passWord, String nick) {
        boolean returnValue;
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        this.db.delete(ACCOUNTS_TABLE, null, null);
        ContentValues values = new ContentValues();
        values.put("device_name", deviceName);
        values.put("device_info", deviceInfo);
        values.put("team_name", teamName);
        values.put("user_name", userName);
        values.put("password", passWord);
        values.put("nick", nick);
        if (this.db.insert(ACCOUNTS_TABLE, null, values) > 0) {
            returnValue = true;
        } else {
            returnValue = false;
        }
        this.db.close();
        return returnValue;
    }

    public HashMap<String, Object> getAccount(Context ctx) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        Cursor c = this.db.query(ACCOUNTS_TABLE, new String[]{"id", "team_name", "user_name", "password", "device_name", "device_info", "nick"}, null, null, null, null, null);
        HashMap<String, Object> account = null;
        if (c.getCount() > 0) {
            c.moveToFirst();
            String id = c.getString(0);
            String teamName = c.getString(1);
            String userName = c.getString(2);
            String passWord = c.getString(3);
            String deviceName = c.getString(4);
            String deviceInfo = c.getString(5);
            String nick = c.getString(6);
            if (id != null) {
                account = new HashMap<>();
                account.put("teamName", teamName);
                account.put("userName", userName);
                account.put("passWord", passWord);
                account.put("deviceName", deviceName);
                account.put("deviceInfo", deviceInfo);
                account.put("nick", nick);
            }
        }
        c.close();
        this.db.close();
        return account;
    }

    public boolean updateUserInfos(Context ctx, String phone, String email, double curSize, double spaceSize, int isAdmin) {
        boolean returnValue;
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        ContentValues values = new ContentValues();
        values.put("phone", phone);
        values.put("email", email);
        values.put("current_size", Double.valueOf(curSize));
        values.put("space_size", Double.valueOf(spaceSize));
        values.put("is_admin", Integer.valueOf(isAdmin));
        if (this.db.update(ACCOUNTS_TABLE, values, null, null) > 0) {
            returnValue = true;
        } else {
            returnValue = false;
        }
        this.db.close();
        return returnValue;
    }

    public HashMap<String, Object> getUserInfos(Context ctx) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        Cursor c = this.db.query(ACCOUNTS_TABLE, new String[]{"id", "phone", "email", "current_size", "space_size", "is_admin"}, null, null, null, null, null);
        HashMap<String, Object> account = null;
        if (c.getCount() > 0) {
            c.moveToFirst();
            String id = c.getString(0);
            String phone = c.getString(1);
            String email = c.getString(2);
            double currentSize = c.getDouble(3);
            double spaceSize = c.getDouble(4);
            int isAdmin = c.getInt(5);
            if (!(id == null || phone == null)) {
                account = new HashMap<>();
                account.put("phone", phone);
                account.put("email", email);
                account.put("currentSize", Double.valueOf(currentSize));
                account.put("spaceSize", Double.valueOf(spaceSize));
                account.put("isAdmin", Integer.valueOf(isAdmin));
            }
        }
        c.close();
        this.db.close();
        return account;
    }

    public void reSetAccounts(Context ctx) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        this.db.delete(ACCOUNTS_TABLE, null, null);
        this.db.close();
    }

    public void reSetDetails(Context ctx) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        this.db.delete(DETAILS_TABLE, null, null);
        this.db.close();
    }

    public boolean updateDetails(Context ctx, Vector<HashMap<String, Object>> detailValues, String parentUUID) {
        boolean z;
        boolean returnValue = false;
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        if (parentUUID != null) {
            this.db.delete(DETAILS_TABLE, "parent_uuid='" + parentUUID + "'", null);
            returnValue = true;
        } else {
            String parentShowUUID = detailValues.get(0).get("parentShowUUID").toString();
            if (!parentShowUUID.equals(EboxConst.TAB_UPLOADER_TAG) || parentShowUUID != null) {
                this.db.delete(DETAILS_TABLE, "parent_uuid='" + parentShowUUID + "'", null);
            }
            for (int i = 0; i < detailValues.size(); i++) {
                ContentValues values = new ContentValues();
                HashMap<String, Object> thisHash = detailValues.get(i);
                values.put("name", thisHash.get("name").toString());
                values.put("size", thisHash.get("size").toString());
                values.put("create_time", thisHash.get("createTime").toString());
                values.put("update_time", thisHash.get("modifyTime").toString());
                values.put("object_type", thisHash.get("objectType").toString());
                values.put("object_uuid", thisHash.get("showUUID").toString());
                values.put("parent_uuid", thisHash.get("parentShowUUID").toString());
                values.put("signature", thisHash.get("signature").toString());
                values.put("exist", thisHash.get("exist").toString());
                values.put("image", thisHash.get("image").toString());
                if (thisHash.get("thumbnail") != null) {
                    values.put("thumbnail", thisHash.get("thumbnail").toString());
                }
                if (thisHash.get("latitude") != null) {
                    values.put("latitude", thisHash.get("latitude").toString());
                }
                if (thisHash.get("longitude") != null) {
                    values.put("longitude", thisHash.get("longitude").toString());
                }
                if (this.db.insert(DETAILS_TABLE, null, values) > 0) {
                    z = true;
                } else {
                    z = false;
                }
                returnValue = z;
            }
        }
        Cursor c = null;
        while (true) {
            if (1 == 0) {
                break;
            }
            c = this.db.query(DETAILS_TABLE, new String[]{"object_uuid", "parent_uuid"}, null, null, null, null, null);
            int numRows = c.getCount();
            if (numRows <= 0) {
                break;
            }
            if (numRows > 0) {
                ArrayList arrayList = new ArrayList();
                ArrayList<String> arrayList2 = new ArrayList<>();
                c.moveToFirst();
                for (int i2 = 0; i2 < numRows; i2++) {
                    if (c.getString(0) != null) {
                        arrayList.add(c.getString(0));
                        arrayList2.add(c.getString(1));
                    }
                    c.moveToNext();
                }
                ArrayList arrayList3 = new ArrayList();
                for (String tt : arrayList2) {
                    if (!arrayList3.contains(tt)) {
                        arrayList3.add(tt);
                    }
                }
                arrayList3.remove(EboxConst.DEFAULTSHARE);
                arrayList3.removeAll(arrayList);
                int ctmp = arrayList3.size();
                if (ctmp == 0) {
                    break;
                }
                for (int i3 = 0; i3 < ctmp; i3++) {
                    this.db.delete(DETAILS_TABLE, "parent_uuid='" + ((String) arrayList3.get(i3)) + "'", null);
                }
            }
            c.close();
        }
        c.close();
        this.db.close();
        return returnValue;
    }

    public Vector<HashMap<String, Object>> getDetails(Context ctx, String parentUUID) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        Vector<HashMap<String, Object>> DetailVector = new Vector<>();
        Cursor c = this.db.query(DETAILS_TABLE, new String[]{"id", "name", "size", "create_time", "update_time", "object_type", "object_uuid", "parent_uuid", "signature", "image", "exist", "latitude", "longitude", "thumbnail"}, "parent_uuid='" + parentUUID + "'", null, null, null, "object_type desc");
        int numRows = c.getCount();
        c.moveToFirst();
        for (int i = 0; i < numRows; i++) {
            if (c.getString(0) != null) {
                HashMap<String, Object> returnHash = new HashMap<>();
                returnHash.put("id", Integer.valueOf(c.getInt(0)));
                returnHash.put("name", c.getString(1));
                returnHash.put("size", Integer.valueOf(c.getInt(2)));
                returnHash.put("createTime", Integer.valueOf(c.getInt(3)));
                returnHash.put("modityTime", Integer.valueOf(c.getInt(4)));
                returnHash.put("objectType", c.getString(5));
                returnHash.put("objectUUID", c.getString(6));
                returnHash.put("parentUUID", c.getString(7));
                returnHash.put("signature", c.getString(8));
                returnHash.put("image", Integer.valueOf(c.getInt(9)));
                returnHash.put("exist", Integer.valueOf(c.getInt(10)));
                returnHash.put("latitude", c.getString(11));
                returnHash.put("longitude", c.getString(12));
                returnHash.put("thumbnail", c.getString(13));
                DetailVector.add(i, returnHash);
            }
            c.moveToNext();
        }
        c.close();
        this.db.close();
        if (numRows == 0) {
            return null;
        }
        return DetailVector;
    }

    public boolean updateDetail(Context ctx, String objectUUID, int exist, String latitude, String longitude, String thumbnail) {
        boolean returnValue;
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        ContentValues values = new ContentValues();
        values.put("object_uuid", objectUUID);
        values.put("exist", Integer.valueOf(exist));
        values.put("thumbnail", thumbnail);
        values.put("latitude", latitude);
        values.put("longitude", longitude);
        if (this.db.update(DETAILS_TABLE, values, "object_uuid='" + objectUUID + "'", null) > 0) {
            returnValue = true;
        } else {
            returnValue = false;
        }
        this.db.close();
        return returnValue;
    }

    public boolean addDetails(Context ctx, String name, int size, int createTime, int modityTime, String objectType, String objectUUID, String parentUUID, String signature, int icon, int exist, String latitude, String longitude, String thumbnail) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        this.db.delete(DETAILS_TABLE, "name='" + name + "' and parent_uuid='" + parentUUID + "'", null);
        ContentValues values = new ContentValues();
        values.put("name", name);
        values.put("size", Integer.valueOf(size));
        values.put("create_time", Integer.valueOf(createTime));
        values.put("update_time", Integer.valueOf(modityTime));
        values.put("object_type", objectType);
        values.put("object_uuid", objectUUID);
        values.put("parent_uuid", parentUUID);
        values.put("signature", signature);
        values.put("image", Integer.valueOf(icon));
        values.put("exist", Integer.valueOf(exist));
        values.put("thumbnail", thumbnail);
        values.put("latitude", latitude);
        values.put("longitude", longitude);
        boolean returnValue = this.db.insert(DETAILS_TABLE, null, values) > 0;
        this.db.close();
        return returnValue;
    }

    public boolean delDetails(Context ctx, String objectUUID) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        if (objectUUID == null || objectUUID.equals(EboxConst.TAB_UPLOADER_TAG)) {
            return false;
        }
        this.db.delete(DETAILS_TABLE, "object_uuid='" + objectUUID + "'", null);
        this.db.close();
        return true;
    }

    public boolean reNameDetails(Context ctx, String objectUUID, String name, int modify) {
        boolean returnValue;
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        if (objectUUID == null || objectUUID.equals(EboxConst.TAB_UPLOADER_TAG)) {
            return false;
        }
        ContentValues values = new ContentValues();
        values.put("name", name);
        values.put("update_time", Integer.valueOf(modify));
        if (this.db.update(DETAILS_TABLE, values, "object_uuid='" + objectUUID + "'", null) > 0) {
            returnValue = true;
        } else {
            returnValue = false;
        }
        this.db.close();
        return returnValue;
    }

    public HashMap<String, Object> queryDetailsByName(Context ctx, String name, String parentUUID) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        String where = "name='" + name + "' and exist = 1 ";
        if (parentUUID != null) {
            where = "name='" + name + "' and parent_uuid='" + parentUUID + "'";
        }
        HashMap<String, Object> returnHash = new HashMap<>();
        Cursor c = this.db.query(DETAILS_TABLE, new String[]{"id", "name", "size", "create_time", "update_time", "object_type", "object_uuid", "parent_uuid", "signature", "image", "exist", "latitude", "longitude", "thumbnail"}, where, null, null, null, "object_type desc");
        int numRows = c.getCount();
        c.moveToFirst();
        for (int i = 0; i < numRows; i++) {
            if (c.getString(0) != null) {
                returnHash = new HashMap<>();
                returnHash.put("id", Integer.valueOf(c.getInt(0)));
                returnHash.put("name", c.getString(1));
                returnHash.put("size", Integer.valueOf(c.getInt(2)));
                returnHash.put("createTime", Integer.valueOf(c.getInt(3)));
                returnHash.put("modityTime", Integer.valueOf(c.getInt(4)));
                returnHash.put("objectType", c.getString(5));
                returnHash.put("objectUUID", c.getString(6));
                returnHash.put("parentUUID", c.getString(7));
                returnHash.put("signature", c.getString(8));
                returnHash.put("image", Integer.valueOf(c.getInt(9)));
                returnHash.put("exist", Integer.valueOf(c.getInt(10)));
                returnHash.put("latitude", c.getString(11));
                returnHash.put("longitude", c.getString(12));
                returnHash.put("thumbnail", c.getString(13));
            }
            c.moveToNext();
        }
        c.close();
        this.db.close();
        if (numRows == 0) {
            return null;
        }
        return returnHash;
    }

    public String queryShareUUIDByType(Context ctx, String parentUUID, String type) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        String tType = EboxConst.TAB_UPLOADER_TAG;
        String tShowUUID = null;
        String tParentUUID = parentUUID;
        while (true) {
            if (tType.equals(type) && tParentUUID.equals(EboxConst.DEFAULTSHARE)) {
                break;
            }
            Cursor c = this.db.query(DETAILS_TABLE, new String[]{"id", "object_type", "object_uuid", "parent_uuid"}, "object_uuid='" + tParentUUID + "'", null, null, null, null);
            if (c.getCount() == 0) {
                break;
            }
            c.moveToFirst();
            tType = c.getString(1);
            tShowUUID = c.getString(2);
            tParentUUID = c.getString(3);
            c.close();
        }
        this.db.close();
        if (!tType.equals(type)) {
            return null;
        }
        return tShowUUID;
    }

    public void reSetConfigs(Context ctx) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        this.db.delete(CONFIGS_TABLE, null, null);
        this.db.execSQL(INSERT_CONFIGS);
        this.db.close();
    }

    public boolean updateConfigs(Context ctx, long lastTime, int autoLogin, int offlineLogin, int notifyInfo, int autoUpdate, int updateFreq, String syncContactsTime, String signTime) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        ContentValues values = new ContentValues();
        if (lastTime != 0) {
            values.put("last_time", Long.valueOf(lastTime));
        }
        if (autoUpdate != -1) {
            values.put("auto_update", Integer.valueOf(autoUpdate));
        }
        if (autoLogin != -1) {
            values.put("auto_login", Integer.valueOf(autoLogin));
        }
        if (offlineLogin != -1) {
            values.put("offline_login", Integer.valueOf(offlineLogin));
        }
        if (notifyInfo != -1) {
            values.put("notify_info", Integer.valueOf(notifyInfo));
        }
        if (updateFreq != -1) {
            values.put("update_freq", Integer.valueOf(updateFreq));
        }
        if (syncContactsTime != null) {
            values.put("sync_contacts_time", syncContactsTime);
        }
        if (signTime != null) {
            values.put("sign_time", signTime);
        }
        boolean returnValue = this.db.update(CONFIGS_TABLE, values, null, null) > 0;
        this.db.close();
        return returnValue;
    }

    public HashMap<String, Object> getConfig(Context ctx) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        Cursor c = this.db.query(CONFIGS_TABLE, new String[]{"last_time", "auto_update", "auto_login", "offline_login", "notify_info", "update_freq", "sync_contacts_time", "sign_time"}, null, null, null, null, null, null);
        HashMap<String, Object> config = null;
        if (c.getCount() > 0) {
            c.moveToFirst();
            long last_time = c.getLong(0);
            int autoUpGrade = c.getInt(1);
            int autoLogin = c.getInt(2);
            int offlineLogin = c.getInt(3);
            int notifyInfo = c.getInt(4);
            int updateFreq = c.getInt(5);
            String syncContacts = c.getString(6);
            String signTime = c.getString(7);
            config = new HashMap<>();
            config.put("last_time", Long.valueOf(last_time));
            config.put("auto_update", Integer.valueOf(autoUpGrade));
            config.put("auto_login", Integer.valueOf(autoLogin));
            config.put("offline_login", Integer.valueOf(offlineLogin));
            config.put("notify_info", Integer.valueOf(notifyInfo));
            config.put("update_freq", Integer.valueOf(updateFreq));
            config.put("sync_contacts_time", syncContacts);
            config.put("sign_time", signTime);
        }
        c.close();
        this.db.close();
        return config;
    }

    public void reSetContacts(Context ctx) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        this.db.delete("contacts", null, null);
        this.db.close();
    }

    public boolean comperaContacts(Context ctx, int contactId, String name, int version) throws InterruptedException {
        boolean success = false;
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        this.db.delete("contacts", "status='3'", null);
        this.db.close();
        HashMap<String, Object> hash = getSnapContactsById(ctx, contactId);
        if (hash == null) {
            insertContacts(ctx, contactId, -1, name, version, 0);
            success = true;
        } else if (hash.get("sync_id").equals(0)) {
            updateContacts(ctx, contactId, -1, name, version, 0);
            success = true;
        } else if (!hash.get(JsonModel.STATUS).equals(1) && (!hash.get("contact_name").equals(name) || !hash.get("version").equals(Integer.valueOf(version)))) {
            updateContacts(ctx, contactId, -1, name, version, 1);
            success = true;
        } else if (!hash.get(JsonModel.STATUS).equals(4)) {
            success = true;
        }
        return success;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void updateContactsOK(Context ctx) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        ContentValues values = new ContentValues();
        values.put(JsonModel.STATUS, (Integer) 4);
        this.db.update("contacts", values, "status='3'", null);
        this.db.close();
    }

    public boolean updateContacts(Context ctx, int contactId, int syncId, String name, int version, int status) {
        boolean returnValue;
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        ContentValues values = new ContentValues();
        if (syncId != -1) {
            values.put("sync_id", Integer.valueOf(syncId));
        }
        if (name != null) {
            values.put("contact_name", name);
        }
        if (version != -1) {
            values.put("version", Integer.valueOf(version));
        }
        if (status != -1) {
            values.put(JsonModel.STATUS, Integer.valueOf(status));
        }
        if (this.db.update("contacts", values, "contact_id='" + contactId + "'", null) > 0) {
            returnValue = true;
        } else {
            returnValue = false;
        }
        this.db.close();
        return returnValue;
    }

    public boolean insertContacts(Context ctx, int contactId, int syncId, String name, int version, int status) {
        boolean returnValue;
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        ContentValues values = new ContentValues();
        if (contactId != -1) {
            values.put("contact_id", Integer.valueOf(contactId));
        }
        if (syncId != -1) {
            values.put("sync_id", Integer.valueOf(syncId));
        }
        if (name != null) {
            values.put("contact_name", name);
        }
        if (version != -1) {
            values.put("version", Integer.valueOf(version));
        }
        values.put(JsonModel.STATUS, Integer.valueOf(status));
        if (this.db.insert("contacts", null, values) > 0) {
            returnValue = true;
        } else {
            returnValue = false;
        }
        this.db.close();
        return returnValue;
    }

    public boolean delContacts(Context ctx, int contactId, int syncId) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        if (contactId != -1) {
            this.db.delete("contacts", "contact_id='" + contactId + "'", null);
        } else {
            this.db.delete("contacts", "sync_id='" + syncId + "'", null);
        }
        this.db.close();
        return true;
    }

    public int getCount(Context ctx, String table) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        Cursor mCursor = this.db.rawQuery("select count(*) from " + table, null);
        mCursor.moveToFirst();
        int count = mCursor.getInt(0);
        mCursor.close();
        this.db.close();
        return count;
    }

    public Vector<HashMap<String, Integer>> getSnapContactsByStatus(Context ctx, int status, boolean isTure) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        String seletion = "status!='" + status + "'";
        if (isTure) {
            seletion = "status='" + status + "'";
        }
        Cursor c = this.db.query("contacts", new String[]{"contact_id", "sync_id"}, seletion, null, null, null, null, null);
        int numRows = c.getCount();
        Vector<HashMap<String, Integer>> contacts = new Vector<>();
        if (numRows > 0) {
            c.moveToFirst();
            do {
                int contactId = c.getInt(0);
                int syncId = c.getInt(1);
                HashMap<String, Integer> contact = new HashMap<>();
                contact.put("sync_id", Integer.valueOf(syncId));
                contact.put("contact_id", Integer.valueOf(contactId));
                contacts.add(contact);
                ContentValues values = new ContentValues();
                values.put(JsonModel.STATUS, (Integer) 3);
                this.db.update("contacts", values, "contact_id='" + contactId + "'", null);
            } while (c.moveToNext());
        }
        c.close();
        this.db.close();
        return contacts;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public HashMap<String, Object> getSnapContactsByName(Context ctx, String name, int syncId) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        Cursor c = this.db.query("contacts", new String[]{"contact_id", "contact_name"}, "contact_name='" + name + "' and status!='" + 3 + "' and sync_id=='0'", null, null, null, null);
        int contactId = -1;
        HashMap<String, Object> contact = null;
        if (c.getCount() > 0) {
            c.moveToFirst();
            contactId = c.getInt(0);
            String contactName = c.getString(1);
            contact = new HashMap<>();
            contact.put("contact_name", contactName);
            contact.put("contact_id", Integer.valueOf(contactId));
        }
        c.close();
        if (contactId != -1) {
            ContentValues values = new ContentValues();
            values.put("sync_id", Integer.valueOf(syncId));
            values.put(JsonModel.STATUS, (Integer) 3);
            this.db.update("contacts", values, "contact_id='" + contactId + "'", null);
        }
        this.db.close();
        return contact;
    }

    public HashMap<String, Object> getSnapContactsById(Context ctx, int contactId) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        Cursor c = this.db.query("contacts", new String[]{"contact_name", "sync_id", "version", JsonModel.STATUS}, "contact_id='" + contactId + "'", null, null, null, null);
        HashMap<String, Object> contact = null;
        if (c.getCount() > 0) {
            c.moveToFirst();
            String contactName = c.getString(0);
            int syncId = c.getInt(1);
            int version = c.getInt(2);
            int status = c.getInt(3);
            contact = new HashMap<>();
            contact.put("contact_name", contactName.trim());
            contact.put("sync_id", Integer.valueOf(syncId));
            contact.put("contact_id", Integer.valueOf(contactId));
            contact.put("version", Integer.valueOf(version));
            contact.put(JsonModel.STATUS, Integer.valueOf(status));
        }
        c.close();
        this.db.close();
        return contact;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public HashMap<String, Object> getSnapContactsBySync(Context ctx, int syncId) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        Cursor c = this.db.query("contacts", new String[]{"contact_id", "version"}, "sync_id='" + syncId + "'", null, null, null, null);
        int contactId = -1;
        HashMap<String, Object> contact = null;
        if (c.getCount() > 0) {
            c.moveToFirst();
            contactId = c.getInt(0);
            int version = c.getInt(1);
            contact = new HashMap<>();
            contact.put("sync_id", Integer.valueOf(syncId));
            contact.put("contact_id", Integer.valueOf(contactId));
            contact.put("version", Integer.valueOf(version));
        }
        c.close();
        if (contactId != -1) {
            ContentValues values = new ContentValues();
            values.put(JsonModel.STATUS, (Integer) 3);
            this.db.update("contacts", values, "contact_id='" + contactId + "'", null);
        }
        this.db.close();
        return contact;
    }

    public boolean insertLocations(Context ctx, String latitude, String longitude, String name, String time) {
        boolean returnValue;
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        ContentValues values = new ContentValues();
        if (latitude != null) {
            values.put("latitude", latitude);
        }
        if (longitude != null) {
            values.put("longitude", longitude);
        }
        if (name != null) {
            values.put("name", name);
        }
        if (time != null) {
            values.put("create_time", time);
        }
        if (this.db.insert(LOCATIONS_TABLE, null, values) > 0) {
            returnValue = true;
        } else {
            returnValue = false;
        }
        this.db.close();
        return returnValue;
    }

    public HashMap<String, String> getLastLocation(Context ctx) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        HashMap<String, String> locatoins = null;
        Cursor c = this.db.query(LOCATIONS_TABLE, new String[]{"id", "latitude", "longitude", "name", "create_time"}, null, null, null, null, "id desc");
        if (c.getCount() != 0) {
            c.moveToFirst();
            locatoins = new HashMap<>();
            locatoins.put("latitude", c.getString(1));
            locatoins.put("longitude", c.getString(2));
            locatoins.put("name", c.getString(3));
            locatoins.put("create_tome", c.getString(4));
        }
        c.close();
        this.db.close();
        return locatoins;
    }

    public void reSetLocatoins(Context ctx) {
        this.db = ctx.openOrCreateDatabase(this.DATABASE_NAME, 0, null);
        this.db.delete(LOCATIONS_TABLE, null, null);
        this.db.close();
    }
}
