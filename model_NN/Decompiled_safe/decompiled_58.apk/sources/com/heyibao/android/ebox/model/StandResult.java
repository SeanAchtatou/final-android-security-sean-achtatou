package com.heyibao.android.ebox.model;

import com.heyibao.android.ebox.common.EboxConst;
import java.io.Serializable;

public class StandResult implements Serializable {
    private static final long serialVersionUID = 1;
    private Integer code;
    private String msg;
    private String session;
    private boolean success;
    private String url = EboxConst.TAB_UPLOADER_TAG;

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public boolean hasSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success2) {
        this.success = success2;
    }

    public Integer getCode() {
        return this.code;
    }

    public void setCode(Integer code2) {
        this.code = code2;
    }

    public void reSetCode() {
        this.code = 0;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg2) {
        this.msg = msg2;
    }

    public String getSession() {
        return this.session;
    }

    public void setSession(String session2) {
        this.session = session2;
    }
}
