package com.heyibao.android.ebox.model;

import android.content.Context;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;

public class EditView extends RelativeLayout {
    private static final int EMAIL = 3;
    private static final int NAME = 5;
    private static final int OBJECT = 1;
    private static final int PASSWORD = 4;
    private static final int PHONE = 2;
    /* access modifiers changed from: private */
    public ImageButton btn;
    private Context context;
    /* access modifiers changed from: private */
    public Medt edt;
    /* access modifiers changed from: private */
    public boolean isVerify;
    /* access modifiers changed from: private */
    public String message;
    /* access modifiers changed from: private */
    public int point = 0;
    private final View.OnTouchListener tl = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == 1 || event.getAction() == 3) {
                EditView.this.edt.setText(EboxConst.TAB_UPLOADER_TAG);
                boolean unused = EditView.this.isVerify = false;
            }
            return false;
        }
    };

    public EditView(Context context2) {
        super(context2);
        this.context = context2;
        this.message = EboxConst.TAB_UPLOADER_TAG;
    }

    public EditView(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        this.context = context2;
        this.message = EboxConst.TAB_UPLOADER_TAG;
    }

    public EditView isNameEdt() {
        this.point = 5;
        init();
        return this;
    }

    public EditView isObjectEdt() {
        this.point = 1;
        init();
        return this;
    }

    public EditView isPhoneEdt() {
        this.point = 2;
        init();
        if (this.edt != null) {
            this.edt.setInputType(3);
        }
        return this;
    }

    public EditView isEmailEdt() {
        this.point = 3;
        init();
        return this;
    }

    public EditView isPassWord() {
        this.point = 4;
        init();
        if (this.edt != null) {
            this.edt.setInputType(129);
        }
        return this;
    }

    public void isImeOptionDone() {
        if (this.edt != null) {
            this.edt.setImeOptions(6);
        }
    }

    public String getMessage() {
        return this.message;
    }

    public boolean isVerifiy() {
        return this.isVerify;
    }

    public void setText(String str) {
        if (this.edt != null) {
            this.edt.setText(str);
        }
    }

    public void setText(int id) {
        if (this.edt != null) {
            this.edt.setText(id);
        }
    }

    public void setEdit(boolean value) {
        this.btn.setVisibility(4);
        if (!value) {
            this.edt.setFocusable(false);
            this.edt.setFilters(new InputFilter[]{new InputFilter() {
                public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                    return source.length() < 1 ? dest.subSequence(dstart, dend) : EboxConst.TAB_UPLOADER_TAG;
                }
            }});
        }
    }

    private void init() {
        this.btn = new ImageButton(this.context);
        this.btn.setImageResource(R.drawable.edit_bt_stop);
        this.btn.setBackgroundColor(0);
        this.btn.setVisibility(4);
        this.btn.setOnTouchListener(this.tl);
        this.edt = new Medt(this.context) {
            public void verifyText(String msg) {
                boolean unused = EditView.this.isVerify = false;
                String unused2 = EditView.this.message = EboxConst.TAB_UPLOADER_TAG;
                switch (EditView.this.point) {
                    case 1:
                        EditView.this.verifyOjbect(msg);
                        return;
                    case 2:
                        EditView.this.verifyPhone(msg);
                        return;
                    case 3:
                        EditView.this.verifyEmail(msg);
                        return;
                    case 4:
                        EditView.this.verifyOther(msg);
                        return;
                    case 5:
                        EditView.this.verifyOther(msg);
                        return;
                    default:
                        return;
                }
            }

            public void showBtn() {
                if (!EditView.this.btn.isShown()) {
                    EditView.this.btn.setVisibility(0);
                }
            }

            public void hideBtn() {
                if (EditView.this.btn.isShown()) {
                    EditView.this.btn.setVisibility(8);
                }
                boolean unused = EditView.this.isVerify = false;
                String unused2 = EditView.this.message = getResources().getString(R.string.error_field_required);
            }
        };
        this.edt.setSingleLine();
        this.edt.setBackgroundResource(17301528);
        this.edt.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        if (EboxContext.mSystemInfo.hasDisplayChange()) {
            this.edt.setHeight(70);
        } else {
            this.edt.setHeight(45);
        }
        addView(this.edt);
        RelativeLayout.LayoutParams sLayoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        sLayoutParams2.addRule(11);
        addView(this.btn, sLayoutParams2);
    }

    /* access modifiers changed from: private */
    public void verifyOther(String msg) {
        this.isVerify = true;
        this.message = msg.trim();
    }

    /* access modifiers changed from: private */
    public void verifyOjbect(String msg) {
        String name = msg.trim();
        if (name.length() > 255 || !name.matches("^[^\\\\/\\<>\\*\\?\\:\"\\|]+$")) {
            this.message = getResources().getString(R.string.file_error);
            return;
        }
        this.isVerify = true;
        this.message = name;
    }

    /* access modifiers changed from: private */
    public void verifyPhone(String msg) {
        String phone = msg.trim();
        if (phone.length() < 11 || !phone.matches("^(0|[0-9]\\d*)$")) {
            this.message = getResources().getString(R.string.phone_error);
            return;
        }
        this.isVerify = true;
        this.message = phone;
    }

    /* access modifiers changed from: private */
    public void verifyEmail(String msg) {
        String email = msg.trim();
        if (email.length() > 255 || !email.matches("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*")) {
            this.message = getResources().getString(R.string.email_error);
            return;
        }
        this.isVerify = true;
        this.message = email;
    }
}
