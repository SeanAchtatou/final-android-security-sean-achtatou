package com.heyibao.android.ebox.model.Json;

import com.heyibao.android.ebox.model.Json.JsonModel;
import org.json.JSONObject;

public class JsonContacts extends JsonParent {
    private int delete = 0;
    private int insert = 0;
    private int localCount = 0;
    private int localMod = 0;
    private int modify = 0;
    private int remoteMod = 0;
    private int syncCount = 0;

    public JsonContacts() {
    }

    public JsonContacts(JSONObject json) {
        this.json = json;
        this.message = null;
    }

    public void setJson(JSONObject json) {
        this.json = json;
        this.message = null;
    }

    public Integer getOffset() {
        if (this.json == null) {
            return null;
        }
        return Integer.valueOf(this.json.optInt(JsonModel.ContactsJson.OFFSET));
    }

    public Integer getTotalCount() {
        if (this.json == null) {
            return null;
        }
        return Integer.valueOf(this.json.optInt(JsonModel.ContactsJson.TOTAL_COUNT));
    }

    public Integer getRemoteCount() {
        if (this.json == null) {
            return null;
        }
        return Integer.valueOf(this.json.optInt("count"));
    }

    public int getLocalCount() {
        return this.localCount;
    }

    public void setLocalCount(int localCount2) {
        if (localCount2 < 0) {
            localCount2 = 0;
        }
        this.localCount = localCount2;
    }

    public void setSyncCount(int syncCount2) {
        if (syncCount2 < 0) {
            syncCount2 = 0;
        }
        this.syncCount = syncCount2;
    }

    public int getSyncCount() {
        return this.syncCount;
    }

    public int getInsert() {
        return this.insert;
    }

    public void setInsert(int insert2) {
        this.insert = insert2;
    }

    public int getModify() {
        return this.modify;
    }

    public void setModify(int modify2) {
        this.modify = modify2;
    }

    public int getDelete() {
        return this.delete;
    }

    public void setDelete(int delete2) {
        this.delete = delete2;
    }

    public void reSetNum() {
        this.insert = 0;
        this.modify = 0;
        this.delete = 0;
    }

    public int getMaxNumber() {
        return this.insert + this.modify + this.delete;
    }

    public int getLocalMod() {
        return this.localMod;
    }

    public void setLocalMod(int localMod2) {
        this.localMod = localMod2;
    }

    public int getRemoteMod() {
        return this.remoteMod;
    }

    public void setRemoteMod(int remoteMod2) {
        this.remoteMod = remoteMod2;
    }
}
