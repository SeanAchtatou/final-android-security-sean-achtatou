package com.heyibao.android.ebox.http;

import android.util.Log;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Hashtable;

public class GPBUtils {
    HttpURLConnection conn;
    private String url;

    public GPBUtils(String url2) {
        Log.d(GPBUtils.class.getSimpleName(), "## start GPBUtils 1");
        this.url = url2;
        Log.d(DownloaderBuilder.class.getSimpleName(), "## GPBUtils input url : " + this.url);
    }

    private boolean setConn(boolean isPost) {
        boolean success = false;
        try {
            if (this.url == null) {
                return false;
            }
            this.url = this.url.replace("https://", "http://");
            this.conn = (HttpURLConnection) new URL(this.url).openConnection();
            if (EboxContext.mDevice.getToken() != null) {
                this.conn.setRequestProperty("oauth_token", EboxContext.mDevice.getToken());
            }
            if (EboxContext.mDevice.getSecret() != null) {
                this.conn.setRequestProperty("oauth_secret", EboxContext.mDevice.getSecret());
            }
            this.conn.setUseCaches(false);
            this.conn.setRequestProperty("User-Agent", EboxConst.USER_AGENT);
            this.conn.setRequestProperty("Connection", "Keep-Alive");
            this.conn.setRequestProperty("Accept-Charset", "UTF-8");
            this.conn.setDoOutput(true);
            this.conn.setDoInput(true);
            this.conn.setConnectTimeout(10000);
            if (isPost) {
                this.conn.setReadTimeout(100000);
                this.conn.setRequestMethod("POST");
            } else {
                this.conn.setRequestMethod("GET");
                this.conn.setReadTimeout(30000);
            }
            success = true;
            return success;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public int getResponseCode() {
        if (this.conn == null) {
            return 0;
        }
        try {
            return this.conn.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public byte[] getResponseByte() {
        if (this.conn == null) {
            return null;
        }
        try {
            return inputStreamToByte(this.conn.getInputStream());
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public int getConnLength() {
        if (this.conn != null) {
            return this.conn.getContentLength();
        }
        return 0;
    }

    public void close() {
        try {
            this.conn.disconnect();
            this.conn = null;
        } catch (Exception e) {
            Exception e2 = e;
            Log.e(GPBUtils.class.getSimpleName(), "## close conn error " + e2.getMessage());
            e2.printStackTrace();
        }
    }

    public InputStream getInputResult() throws Exception {
        Log.d(GPBUtils.class.getSimpleName(), this.url);
        if (setConn(false)) {
            return this.conn.getInputStream();
        }
        return null;
    }

    public DataOutputStream getOutputResult() throws Exception {
        Log.d(GPBUtils.class.getSimpleName(), this.url);
        if (!setConn(true)) {
            return null;
        }
        this.conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=***PIUQWERLJLZXCV***");
        return new DataOutputStream(this.conn.getOutputStream());
    }

    public byte[] getResult(byte[] input) throws Exception {
        Log.d(GPBUtils.class.getSimpleName(), this.url);
        if (!setConn(true)) {
            return null;
        }
        OutputStream os = this.conn.getOutputStream();
        if (input != null) {
            try {
                os.write(input);
            } catch (Throwable th) {
                os.close();
                this.conn.disconnect();
                this.conn = null;
                throw th;
            }
        }
        byte[] result = inputStreamToByte(this.conn.getInputStream());
        os.close();
        this.conn.disconnect();
        this.conn = null;
        Log.d(GPBUtils.class.getSimpleName(), "## end GPBUtils ");
        return result;
    }

    public byte[] getResult(Hashtable<String, String> parameter, byte[] bt) throws Exception {
        Log.d(GPBUtils.class.getSimpleName(), this.url);
        byte[] bytes = new byte[EboxConst.POST_SIZE];
        DataOutputStream dos = getOutputResult();
        dos.writeBytes("--***PIUQWERLJLZXCV***\r\n");
        for (String key : parameter.keySet()) {
            dos.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"");
            dos.writeBytes("\r\n\r\n");
            dos.write(parameter.get(key).getBytes());
            dos.writeBytes(EboxConst.LINEEND);
            dos.writeBytes("--***PIUQWERLJLZXCV***\r\n");
        }
        dos.writeBytes("Content-Disposition: form-data; name=\"attachment\";filename=\"" + (new Date().getTime() / 1000) + ".jpg\"" + EboxConst.LINEEND);
        dos.writeBytes("Content-Type: image/jpeg\r\n");
        dos.writeBytes(EboxConst.LINEEND);
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bt);
        byte[] result = null;
        try {
            int size = bt.length;
            while (true) {
                int length = byteArrayInputStream.read(bytes);
                if (length < 0) {
                    break;
                }
                if (length == 8192 && size < 0) {
                    length += size;
                }
                dos.write(bytes, 0, length);
                dos.flush();
                Thread.sleep(50);
            }
            dos.writeBytes(EboxConst.LINEEND);
            dos.writeBytes("--***PIUQWERLJLZXCV***--\r\n");
            dos.flush();
            result = inputStreamToByte(this.conn.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            dos.close();
            this.conn.disconnect();
            this.conn = null;
        }
        Log.d(GPBUtils.class.getSimpleName(), "## end GPBUtils ");
        return result;
    }

    private byte[] inputStreamToByte(InputStream inputStream) throws IOException, InterruptedException {
        ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
        byte[] buffer = new byte[EboxConst.POST_SIZE];
        while (true) {
            int len = inputStream.read(buffer);
            if (len != -1) {
                bytestream.write(buffer, 0, len);
                Thread.sleep(1);
            } else {
                byte[] returnbyte = bytestream.toByteArray();
                bytestream.close();
                return returnbyte;
            }
        }
    }
}
