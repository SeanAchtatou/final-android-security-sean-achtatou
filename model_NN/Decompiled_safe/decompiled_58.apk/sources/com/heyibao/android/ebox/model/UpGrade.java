package com.heyibao.android.ebox.model;

import java.io.Serializable;

public class UpGrade implements Serializable {
    private static final long serialVersionUID = 1;
    private String memo;
    private String name;
    private String path;
    private String signature;
    private String time;
    private String version;

    public void reSet() {
        this.name = null;
        this.signature = null;
        this.version = null;
        this.path = null;
        this.memo = null;
        this.time = null;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public void setSignature(String signature2) {
        this.signature = signature2;
    }

    public String getSignature() {
        return this.signature;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public String getVersion() {
        return this.version;
    }

    public void setMemo(String memo2) {
        this.memo += memo2;
    }

    public String getMemo() {
        return this.memo;
    }

    public void setPath(String path2) {
        this.path = path2;
    }

    public String getPath() {
        return this.path;
    }

    public void setTime(String time2) {
        this.time = time2;
    }

    public String getTime() {
        return this.time;
    }
}
