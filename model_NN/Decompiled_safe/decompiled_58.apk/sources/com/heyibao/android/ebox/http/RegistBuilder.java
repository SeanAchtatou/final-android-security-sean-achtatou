package com.heyibao.android.ebox.http;

import ClientProtocol.ClientMeta;
import android.app.Service;
import android.util.Log;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;

public abstract class RegistBuilder implements Runnable, HttpInterface {
    private Service activity;
    private String email;
    private int flag;
    private String nick;
    private String password;

    public RegistBuilder(Service activity2, String email2, String nick2, String password2, int flag2) {
        this.activity = activity2;
        this.email = email2;
        this.nick = nick2;
        this.password = password2;
        this.flag = flag2;
    }

    public void run() {
        Log.d(RegistBuilder.class.getSimpleName(), "## start regist");
        registStart();
        Log.d(RegistBuilder.class.getSimpleName(), "end regist");
    }

    private void registStart() {
        int errcount = 2;
        boolean success = false;
        while (!Thread.interrupted() && errcount < 3) {
            try {
                ClientMeta.RegProtocol.Builder registInfo = ClientMeta.RegProtocol.newBuilder();
                registInfo.setEmail(this.email);
                registInfo.setDomain(this.activity.getResources().getString(R.string.domain));
                registInfo.setNick(this.nick);
                registInfo.setPassword(this.password);
                ClientMeta.ServiceRegisterRequest.Builder request = ClientMeta.ServiceRegisterRequest.newBuilder();
                request.setRegisterInfo(registInfo);
                request.setRegisterType(this.flag);
                Thread.sleep(10);
                ClientMeta.Status status = ClientMeta.ServiceRegisterResponse.parseFrom(new GPBUtils(EboxConst.REGIST_URL).getResult(request.build().toByteArray())).getStatus();
                success = status.getSuccess();
                EboxContext.message = status.getMsg();
                break;
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(UpdateBuilder.class.getSimpleName(), "## UpdateBuilder is Force stop");
                EboxContext.message = this.activity.getString(R.string.stop_thread);
            } catch (Exception e2) {
                Exception error = e2;
                error.printStackTrace();
                Log.e(UpdateBuilder.class.getSimpleName(), "## regist is wronging info: " + error.getMessage() + " for " + errcount);
                errcount++;
            }
        }
        reportSuccess(success);
        Log.d(UpdateBuilder.class.getSimpleName(), "## regist end");
    }
}
