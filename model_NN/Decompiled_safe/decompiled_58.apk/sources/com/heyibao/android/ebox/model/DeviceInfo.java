package com.heyibao.android.ebox.model;

import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.model.Json.JsonModel;

public class DeviceInfo extends StandResult {
    private static final long serialVersionUID = 1;
    private String deviceInfo;
    private String deviceName;
    private String deviceUuid;
    private String email;
    private String latitude;
    private String longitude;
    private String nick;
    private String passWord;
    private String phone;
    private String secret;
    private String teamName;
    private String token;
    private String userName;

    public void setLatitude(String latitude2) {
        this.latitude = latitude2;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public void setLongitude(String longitude2) {
        this.longitude = longitude2;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public String getDeviceUuid() {
        return this.deviceUuid;
    }

    public void setDeviceUuid(String deviceUuid2) {
        this.deviceUuid = deviceUuid2;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token2) {
        this.token = token2;
    }

    public String getSecret() {
        return this.secret;
    }

    public void setSecret(String secret2) {
        this.secret = secret2;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName2) {
        this.userName = userName2;
    }

    public String getTeamName() {
        return this.teamName == null ? EboxConst.TAB_UPLOADER_TAG : this.teamName;
    }

    public void setTeamName(String teamName2) {
        this.teamName = teamName2;
    }

    public String getPassWord() {
        return this.passWord;
    }

    public void setPassWord(String passWord2) {
        this.passWord = passWord2;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    public void setDeviceName(String deviceName2) {
        this.deviceName = deviceName2;
    }

    public String getDeviceInfo() {
        return this.deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo2) {
        this.deviceInfo = deviceInfo2;
    }

    public String getNick() {
        return this.nick == null ? JsonModel.ERROR : this.nick;
    }

    public void setNick(String nick2) {
        this.nick = nick2;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone2) {
        this.phone = phone2;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email2) {
        this.email = email2;
    }

    public void reSet() {
        this.userName = null;
        this.teamName = null;
        this.passWord = null;
        this.deviceName = null;
        this.deviceInfo = null;
        this.nick = null;
        this.email = null;
        this.phone = null;
        setSuccess(false);
        setCode(0);
        setMsg(null);
        setSession(null);
        setUrl(EboxConst.TAB_UPLOADER_TAG);
    }
}
