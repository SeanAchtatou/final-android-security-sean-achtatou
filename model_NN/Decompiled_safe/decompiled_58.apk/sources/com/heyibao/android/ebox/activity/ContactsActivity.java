package com.heyibao.android.ebox.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.database.ContentObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.db.ActionDB;
import com.heyibao.android.ebox.model.ImageBt;
import com.heyibao.android.ebox.model.MyDialog;
import com.heyibao.android.ebox.model.StandResult;
import com.heyibao.android.ebox.util.Utils;

public class ContactsActivity extends HomeMenuActivity {
    private ImageBt backUpBtn;
    protected AdapterView.OnItemClickListener backupListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View arg1, int which, long arg3) {
            if (which == 0) {
                ContactsActivity.this.flagRet.setCode(1);
                ContactsActivity.this.backupContacts();
            } else if (which == 1) {
                ContactsActivity.this.flagRet.setCode(2);
                ContactsActivity.this.showDialog(9);
            }
            ContactsActivity.this.removeDialog(6);
        }
    };
    private MyContentObserver contentObserver = new MyContentObserver();
    /* access modifiers changed from: private */
    public StandResult flagRet = new StandResult();
    /* access modifiers changed from: private */
    public boolean isChange;
    private ProgressBar localProgressBar;
    protected MyDialog proDialog;
    private ContactsReceiver receiver;
    private ProgressBar remoteProgressBar;
    protected AdapterView.OnItemClickListener restoreListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View arg1, int which, long arg3) {
            if (which == 0) {
                ContactsActivity.this.flagRet.setCode(3);
                ContactsActivity.this.restoreContacts();
            } else if (which == 1) {
                ContactsActivity.this.flagRet.setCode(4);
                ContactsActivity.this.showDialog(1);
            }
            ContactsActivity.this.removeDialog(4);
        }
    };
    private ScrollView sv;

    private class MyContentObserver extends ContentObserver {
        public MyContentObserver() {
            super(null);
        }

        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            Log.d(ContactsActivity.class.getSimpleName(), "contacts changes ");
            boolean unused = ContactsActivity.this.isChange = true;
        }
    }

    public class ContactsReceiver extends BroadcastReceiver {
        public ContactsReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("action");
            Log.d(ContactsActivity.class.getSimpleName(), "## onReceive : " + action);
            if (EboxConst.ACTION_CONTACTSING.equals(action)) {
                ContactsActivity.this.mHandler.sendMessage(ContactsActivity.this.mHandler.obtainMessage(5, intent));
            } else if (EboxConst.ACTION_CONTACTS.equals(action)) {
                ContactsActivity.this.mHandler.sendMessage(ContactsActivity.this.mHandler.obtainMessage(ContactsActivity.this.flagRet.getCode().intValue(), intent));
            }
        }
    }

    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(null);
        if (getResources().getConfiguration().orientation == 2) {
            this.mainLayout.setBackgroundResource(R.drawable.bg_cloud_land);
        } else if (getResources().getConfiguration().orientation == 1) {
            this.mainLayout.setBackgroundResource(R.drawable.bg_cloud);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.receiver = new ContactsReceiver();
        registerReceiver(this.receiver, new IntentFilter(EboxConst.EBOXBROAD), null, this.mHandler);
        getApplicationContext().getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, this.contentObserver);
        if (this.isChange) {
            reloadUI();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        unregisterReceiver(this.receiver);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.contact_view);
        contactInit();
    }

    public void onPrepareDialog(int id, Dialog dialog) {
        MyDialog mDialog = (MyDialog) dialog;
        switch (id) {
            case 3:
                mDialog.reProgress();
                return;
            case 9:
                if (this.flagRet.getCode().intValue() != 2) {
                    return;
                }
                if (EboxContext.mSyncContacts.getLocalCount() == 0) {
                    mDialog.setMessage(getString(R.string.back_contacts_info_1));
                    return;
                } else if (EboxContext.mSyncContacts.getLocalCount() > 0 && EboxContext.mSyncContacts.getSyncCount() > 0) {
                    mDialog.setMessage(getString(R.string.back_contacts_info));
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                MyDialog mDialog = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.bt_ok:
                                ContactsActivity.this.restoreContacts();
                                dismiss();
                                return;
                            case R.id.bt_cancel:
                                ContactsActivity.this.cancelFuncation(this);
                                return;
                            default:
                                return;
                        }
                    }
                };
                mDialog.setDefaultView();
                mDialog.setMessage(getResources().getString(R.string.restore_contacts_info));
                return mDialog;
            case 2:
            case 5:
            case 7:
            case 8:
            case 10:
            case 11:
            default:
                return null;
            case 3:
                return proDialog();
            case 4:
                MyDialog mDialog2 = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                    }
                };
                mDialog2.setListView(R.array.contacts_option, this.restoreListener);
                return mDialog2;
            case 6:
                MyDialog mDialog3 = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                    }
                };
                mDialog3.setListView(R.array.contacts_option_1, this.backupListener);
                return mDialog3;
            case 9:
                MyDialog mDialog4 = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.bt_ok:
                                ContactsActivity.this.backupContacts();
                                dismiss();
                                return;
                            case R.id.bt_cancel:
                                ContactsActivity.this.cancelFuncation(this);
                                return;
                            default:
                                return;
                        }
                    }
                };
                mDialog4.setDefaultView();
                mDialog4.setMessage(getResources().getString(R.string.back_contacts_info_2));
                return mDialog4;
            case 12:
                return stopDialog();
        }
    }

    private void contactInit() {
        initTitleBar();
        this.midTV.setText(getString(R.string.my_contacts));
        this.retreatBtn.setVisibility(4);
        this.sv = (ScrollView) findViewById(R.id.sv);
        this.sv.setOnTouchListener(this);
        this.backUpBtn = (ImageBt) findViewById(R.id.con_bak);
        this.backUpBtn.setTextViewText(getString(R.string.backup_contact_txt));
        this.backUpBtn.setImageResource(R.drawable.tong_xun_lu_bei_fen_72);
        this.backUpBtn.setOnClickListener(this);
        ImageBt reStoreBtn = (ImageBt) findViewById(R.id.con_huf);
        reStoreBtn.setTextViewText(getString(R.string.restore_contact_txt));
        reStoreBtn.setImageResource(R.drawable.tong_xun_lu_hui_fu_72);
        reStoreBtn.setOnClickListener(this);
        this.localProgressBar = (ProgressBar) findViewById(R.id.progress_local_contacts);
        this.remoteProgressBar = (ProgressBar) findViewById(R.id.progress_remote_contacts);
        if (EboxContext.mSystemInfo.hasDisplayChange()) {
        }
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                Intent intent = (Intent) msg.obj;
                boolean success = intent.getBooleanExtra("success", false);
                Log.d(ContactsActivity.class.getSimpleName(), "broadcast reval " + success);
                switch (msg.what) {
                    case 0:
                        ContactsActivity.this.getContactsEnd(success);
                        break;
                    case 1:
                        ContactsActivity.this.backupEnd(success);
                        break;
                    case 2:
                        ContactsActivity.this.backupEnd(success);
                        break;
                    case 3:
                        ContactsActivity.this.restoreEnd(success);
                        break;
                    case 4:
                        ContactsActivity.this.restoreEnd(success);
                        break;
                    case 5:
                        ContactsActivity.this.setProgress(intent);
                        break;
                    case 16:
                        ContactsActivity.this.updateUI(success);
                        break;
                }
                super.handleMessage(msg);
            }
        };
        Thread threadUI = new Thread(new Runnable() {
            public void run() {
                while (!Thread.interrupted()) {
                    synchronized (ContactsActivity.this.object) {
                        try {
                            ContactsActivity.this.object.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                        ContactsActivity.this.getLocalContacts();
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        });
        synchronized (this.object) {
            threadUI.start();
        }
        this.runnableUI = new Runnable() {
            public void run() {
                EboxContext.mSyncContacts.reSetNum();
                boolean unused = ContactsActivity.this.isChange = false;
                synchronized (ContactsActivity.this.object) {
                    ContactsActivity.this.object.notify();
                }
                if (EboxContext.mSystemInfo.hasNet()) {
                    ContactsActivity.this.getRemoteContacts();
                }
                ContactsActivity.this.mHandler.removeCallbacks(this);
            }
        };
        reloadUI();
    }

    private void reloadUI() {
        ((TextView) findViewById(R.id.local_contacts_count)).setText(getString(R.string.local_contacts_txt));
        this.localProgressBar.setVisibility(0);
        ((TextView) findViewById(R.id.remote_contacts_count)).setText(getString(R.string.remote_contacts_txt));
        this.remoteProgressBar.setVisibility(0);
        this.mHandler.postDelayed(this.runnableUI, 10);
    }

    /* access modifiers changed from: private */
    public void getRemoteContacts() {
        showProgressBar();
        this.remoteProgressBar.setVisibility(0);
        this.flagRet.setCode(0);
        Utils.startService(this, EboxConst.ACTION_CONTACTS, this.flagRet, R.drawable.app_sync);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void getLocalContacts() throws InterruptedException {
        ActionDB.getSysContacts(this, true);
        Intent intent = new Intent();
        intent.putExtra("success", false);
        this.mHandler.sendMessage(this.mHandler.obtainMessage(16, intent));
    }

    /* access modifiers changed from: private */
    public void updateUI(boolean isSync) {
        TextView local_number = (TextView) findViewById(R.id.local_contacts_count);
        if (isSync) {
            this.remoteProgressBar.setVisibility(8);
            if (!this.localProgressBar.isShown()) {
                local_number.setText(Utils.replace(getResources().getString(R.string.local_contacts_number), EboxContext.mSyncContacts.getLocalCount()));
            }
            ((TextView) findViewById(R.id.remote_contacts_count)).setText(Utils.replace(getResources().getString(R.string.remote_contacts_number), EboxContext.mSyncContacts.getSyncCount()));
            TextView sync_info = (TextView) findViewById(R.id.remote_contacts_info);
            if (EboxContext.mSyncContacts.getRemoteMod() <= 0 || EboxContext.mSyncContacts.getSyncCount() <= 0) {
                sync_info.setVisibility(8);
            } else {
                String context = Utils.replace(getString(R.string.remote_contacts_change), EboxContext.mSyncContacts.getRemoteMod());
                sync_info.setVisibility(0);
                sync_info.setText(context);
            }
        } else {
            this.localProgressBar.setVisibility(8);
            local_number.setText(Utils.replace(getResources().getString(R.string.local_contacts_number), EboxContext.mSyncContacts.getLocalCount()));
        }
        String time = EboxContext.mSystemInfo.getSyncContactsTime();
        TextView sync_time = (TextView) findViewById(R.id.sync_contacts_time);
        if (time != null) {
            TextView local_info = (TextView) findViewById(R.id.local_contacts_info);
            if (EboxContext.mSyncContacts.getLocalMod() > 0) {
                String context2 = Utils.replace(getString(R.string.local_contacts_change), EboxContext.mSyncContacts.getLocalMod());
                local_info.setVisibility(0);
                local_info.setText(context2);
            } else {
                local_info.setVisibility(8);
            }
            sync_time.setText(Utils.replace(getString(R.string.sync_contacts_time), time));
            return;
        }
        sync_time.setText(getString(R.string.sync_contacts_no));
    }

    /* access modifiers changed from: private */
    public void backupContacts() {
        if (hasWorking()) {
            if (EboxContext.mSyncContacts.getSyncCount() > 200 || EboxContext.mSyncContacts.getLocalCount() > 200 || EboxContext.mSyncContacts.getLocalMod() > 200 || EboxContext.mSyncContacts.getRemoteMod() > 200) {
                Utils.MessageBox(this, getString(R.string.contact_too_large));
            }
            showProgressBar();
            removeDialog(3);
            showDialog(3);
            this.proDialog.setMessage(getString(R.string.loading_contacts));
            Utils.startService(this, EboxConst.ACTION_CONTACTS, this.flagRet, R.drawable.app_sync);
        }
    }

    /* access modifiers changed from: private */
    public void restoreContacts() {
        if (hasWorking()) {
            if (EboxContext.mSyncContacts.getSyncCount() > 200 || EboxContext.mSyncContacts.getLocalCount() > 200) {
                Utils.MessageBox(this, getString(R.string.contact_too_large));
            }
            removeDialog(3);
            showDialog(3);
            this.proDialog.setMessage(getString(R.string.loading_contacts));
            Utils.startService(this, EboxConst.ACTION_CONTACTS, this.flagRet, R.drawable.app_sync);
        }
    }

    /* access modifiers changed from: private */
    public void setProgress(Intent intent) {
        String temp;
        int max;
        int val = intent.getIntExtra("retval", 0);
        if (this.flagRet.getCode().intValue() == 3 || this.flagRet.getCode().intValue() == 4) {
            temp = val + "/" + EboxContext.mSyncContacts.getSyncCount();
            max = EboxContext.mSyncContacts.getSyncCount();
        } else {
            temp = val + "/" + EboxContext.mSyncContacts.getMaxNumber();
            max = EboxContext.mSyncContacts.getMaxNumber();
            this.proDialog.setProgress((val * 100) / EboxContext.mSyncContacts.getMaxNumber());
        }
        this.proDialog.setProgress((val * 100) / max);
        this.proDialog.setMessage(Utils.replace(getString(R.string.sycn_contactsing), temp));
    }

    /* access modifiers changed from: private */
    public void getContactsEnd(boolean success) {
        closeProgressBar();
        Utils.startService(this, EboxConst.ACTION_NORMAL, null, 0);
        if (!success) {
            Utils.MessageBox(this, getString(R.string.net_error));
        } else {
            updateUI(true);
        }
        EboxContext.mSyncContacts.reSetNum();
    }

    /* access modifiers changed from: private */
    public void backupEnd(boolean success) {
        closeProgressBar();
        Utils.startService(this, EboxConst.ACTION_NORMAL, null, 0);
        if (success) {
            EboxContext.mSyncContacts.setLocalMod(0);
            EboxContext.mSyncContacts.setRemoteMod(0);
            updateUI(true);
            this.proDialog.setMessage(getResources().getString(R.string.sync_contact_success));
            String content = ((getString(R.string.backup_contact_success) + Utils.replace(getString(R.string.insert_contacts), EboxContext.mSyncContacts.getInsert())) + Utils.replace(getString(R.string.modify_contacts), EboxContext.mSyncContacts.getModify())) + Utils.replace(getString(R.string.delete_contacts), EboxContext.mSyncContacts.getDelete());
            Log.d(ContactsActivity.class.getSimpleName(), content);
            Utils.MessageBox(this, content);
        } else {
            Utils.MessageBox(this, getString(R.string.sync_contact_false));
        }
        removeDialog(3);
        EboxContext.mSyncContacts.reSetNum();
    }

    /* access modifiers changed from: private */
    public void restoreEnd(boolean success) {
        closeProgressBar();
        Utils.startService(this, EboxConst.ACTION_NORMAL, null, 0);
        if (success) {
            EboxContext.mSyncContacts.setLocalMod(0);
            EboxContext.mSyncContacts.setRemoteMod(0);
            updateUI(true);
            this.proDialog.setMessage(getResources().getString(R.string.sync_contact_success));
            String content = ((getString(R.string.sync_contact_success) + Utils.replace(getString(R.string.insert_contacts), EboxContext.mSyncContacts.getInsert())) + Utils.replace(getString(R.string.modify_contacts), EboxContext.mSyncContacts.getModify())) + Utils.replace(getString(R.string.delete_contacts), EboxContext.mSyncContacts.getDelete());
            Log.d(ContactsActivity.class.getSimpleName(), content);
            Utils.MessageBox(this, content);
        } else {
            Utils.MessageBox(this, getString(R.string.sync_contact_false));
        }
        removeDialog(3);
        EboxContext.mSyncContacts.reSetNum();
    }

    private void openContactDetails(long id) {
        startActivity(new Intent("android.intent.action.VIEW", ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id)));
    }

    public void onClick(View v) {
        if (Utils.hasNet(this)) {
            Utils.MessageBox(this, getResources().getString(R.string.frequent));
            return;
        }
        this.flagRet.reSetCode();
        switch (v.getId()) {
            case R.id.con_bak:
                if (EboxContext.mSyncContacts.getLocalCount() == 0 && EboxContext.mSyncContacts.getSyncCount() == 0) {
                    Utils.MessageBox(this, getString(R.string.not_need_back_contacts));
                    return;
                } else {
                    showDialog(6);
                    return;
                }
            case R.id.con_huf:
                if (EboxContext.mSyncContacts.getSyncCount() == 0) {
                    Utils.MessageBox(this, getString(R.string.not_need_restore_contacts));
                    return;
                } else {
                    showDialog(4);
                    return;
                }
            case R.id.refresh_btn:
                reloadUI();
                return;
            default:
                return;
        }
    }

    private MyDialog proDialog() {
        if (this.proDialog == null) {
            this.proDialog = new MyDialog(this, R.style.dialog) {
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.bt_ok:
                            Utils.MessageBox(ContactsActivity.this, ContactsActivity.this.getString(R.string.download_ing));
                            return;
                        case R.id.bt_cancel:
                            Utils.startService(ContactsActivity.this, EboxConst.ACTION_STOP, null, 0);
                            dismiss();
                            return;
                        default:
                            return;
                    }
                }
            };
            this.proDialog.setProgressView();
            this.proDialog.setMessage(getResources().getString(R.string.loading));
            this.proDialog.setCancelable(false);
            this.proDialog.setCanceledOnTouchOutside(false);
        }
        return this.proDialog;
    }
}
