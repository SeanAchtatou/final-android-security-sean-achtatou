package com.heyibao.android.ebox.activity;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.model.Details;
import com.heyibao.android.ebox.model.FileBrowserAdapter;
import com.heyibao.android.ebox.model.MyDialog;
import com.heyibao.android.ebox.util.Utils;
import java.io.File;
import java.util.ArrayList;

public class FileBrowserActivity extends ListActivity {
    private static final File ROOT_FILE = Environment.getExternalStorageDirectory();
    private ArrayList<Details> arrList = new ArrayList<>();
    private File currentDirectory;
    private final DISPLAYMODE displayMode = DISPLAYMODE.RELATIVE;

    private enum DISPLAYMODE {
        ABSOLUTE,
        RELATIVE
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.filebrowser_view);
        this.currentDirectory = ROOT_FILE;
        browseToRoot();
    }

    private void browseToRoot() {
        browseTo(ROOT_FILE, 0);
    }

    private void upOneLevel() {
        if (!this.currentDirectory.getAbsolutePath().equals(ROOT_FILE.getPath())) {
            browseTo(this.currentDirectory.getParentFile(), 0);
        }
    }

    private void browseTo(File aDirectory, int position) {
        if (aDirectory.isDirectory()) {
            setTitle(aDirectory.getAbsolutePath() + " :: " + getString(R.string.filebrowser));
            this.currentDirectory = aDirectory;
            this.arrList = Utils.getThumbnailForLocal(this, aDirectory.getAbsolutePath());
            currentView();
            return;
        }
        getVerifyDialog(this.arrList.get(position)).show();
    }

    public Dialog getVerifyDialog(final Details details) {
        if (details.getSize() >= 2.097152E8d) {
            String msg = getResources().getString(R.string.upload_info);
            MyDialog mDialog = new MyDialog(this, R.style.dialog) {
                public void onClick(View v) {
                    dismiss();
                }
            };
            mDialog.setDefaultView();
            mDialog.setMessage(msg);
            return mDialog;
        }
        String msg2 = Utils.replace(getResources().getString(R.string.upload_ver_file), details.getName());
        MyDialog mDialog2 = new MyDialog(this, R.style.dialog) {
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.bt_ok:
                        FileBrowserActivity.this.openFile(details.getPath(), details.getIcon().intValue());
                        dismiss();
                        return;
                    case R.id.bt_cancel:
                        dismiss();
                        return;
                    default:
                        return;
                }
            }
        };
        mDialog2.setDefaultView();
        mDialog2.setMessage(msg2);
        return mDialog2;
    }

    /* access modifiers changed from: private */
    public void openFile(String path, int icon) {
        Intent intent = new Intent();
        intent.putExtra("path", path);
        intent.putExtra(EboxConst.CATCHICON, icon);
        setResult(-1, intent);
        finish();
    }

    private void currentView() {
        ListView currentListView = (ListView) findViewById(16908298);
        FileBrowserAdapter dp = new FileBrowserAdapter(this, this.arrList, R.layout.filebrowser_row);
        if (!this.currentDirectory.getAbsolutePath().equals(ROOT_FILE.getPath())) {
            dp.insert(new Details(getString(R.string.up_one_level), R.drawable.back_up_level, EboxConst.RETREAT_ICON), 0);
        }
        if (dp.getCount() > 0) {
            currentListView.setAdapter((ListAdapter) dp);
            currentListView.setFocusableInTouchMode(true);
            currentListView.setFocusable(true);
            dp.notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (this.arrList.get(position).getName().equals(getString(R.string.up_one_level))) {
            upOneLevel();
            return;
        }
        File clickedFile = null;
        switch (this.displayMode) {
            case RELATIVE:
                clickedFile = new File(this.currentDirectory.getAbsolutePath() + "/" + this.arrList.get(position).getName());
                break;
            case ABSOLUTE:
                clickedFile = new File(this.arrList.get(position).getName());
                break;
        }
        if (clickedFile != null) {
            browseTo(clickedFile, position);
        }
    }
}
