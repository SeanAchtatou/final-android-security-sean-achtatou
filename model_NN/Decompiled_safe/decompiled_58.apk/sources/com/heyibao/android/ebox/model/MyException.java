package com.heyibao.android.ebox.model;

import org.json.JSONException;

public class MyException extends Exception {
    private static final long serialVersionUID = -2623309261327598087L;
    private int statusCode = -1;

    public MyException(JSONException cause) {
        super(cause);
    }

    public MyException(Exception cause) {
        super(cause);
    }

    public MyException(String msg, int statusCode2) {
        super(msg);
        this.statusCode = statusCode2;
    }

    public MyException(String msg, Exception cause) {
        super(msg, cause);
    }

    public MyException(String msg, Exception cause, int statusCode2) {
        super(msg, cause);
        this.statusCode = statusCode2;
    }

    public int getStatusCode() {
        return this.statusCode;
    }
}
