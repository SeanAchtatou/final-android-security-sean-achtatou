package com.heyibao.android.ebox.http;

import ClientProtocol.ClientMeta;
import android.app.Service;
import android.util.Log;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import java.io.File;

public abstract class UpdateBuilder implements Runnable, HttpInterface {
    private Service activity;
    private String name;
    private String url;

    public UpdateBuilder(Service activity2) {
        this.activity = activity2;
    }

    public UpdateBuilder(Service activity2, String url2, String name2) {
        this.activity = activity2;
        this.url = url2;
        this.name = name2;
    }

    public void run() {
        if (this.url == null) {
            Log.d(UpdateBuilder.class.getSimpleName(), "## start upgrade");
            updateStart();
        } else {
            Log.d(UpdateBuilder.class.getSimpleName(), "## start download apk");
            new EboxDownload(this.activity, this.url, new File(new File(EboxContext.mSystemInfo.getSdPath() + EboxConst.CATCHFODLER + File.separator), this.name)) {
                public void reSuccess(boolean is) {
                    UpdateBuilder.this.reportSuccess(is);
                }

                public void reProgress(int blockIndex, int currentSize) {
                    UpdateBuilder.this.reportProgress(blockIndex, currentSize);
                }
            };
        }
        Log.d(UpdateBuilder.class.getSimpleName(), "end UpdateBuilder ");
    }

    private void updateStart() {
        int errcount = 2;
        boolean success = false;
        while (true) {
            if (Thread.interrupted() || errcount >= 3) {
                break;
            }
            try {
                ClientMeta.UpDateInfo.Builder upDateInfo = ClientMeta.UpDateInfo.newBuilder();
                upDateInfo.setId(0);
                upDateInfo.setDomain(this.activity.getResources().getString(R.string.domain));
                upDateInfo.setName(this.activity.getResources().getString(R.string.setup));
                upDateInfo.setVersion(this.activity.getResources().getString(R.string.version));
                ClientMeta.UpGradeRequest.Builder request = ClientMeta.UpGradeRequest.newBuilder();
                request.addUpdateinfolist(upDateInfo);
                Thread.sleep(10);
                ClientMeta.UpGradeResponse response = ClientMeta.UpGradeResponse.parseFrom(new GPBUtils(EboxConst.UPGRADE_URL).getResult(request.build().toByteArray()));
                if (response.getResult().getSuccess()) {
                    if (response.getUpdateinfolistCount() > 0) {
                        Log.d(UpdateBuilder.class.getSimpleName(), "## there's need to update,get info now ");
                        success = true;
                        ClientMeta.UpDateInfo info = response.getUpdateinfolist(0);
                        EboxContext.mUpGrade.setPath(info.getPath());
                        EboxContext.mUpGrade.setName(info.getName());
                        EboxContext.mUpGrade.setVersion(info.getVersion());
                        EboxContext.mUpGrade.setSignature(info.getSignature());
                        break;
                    }
                    Log.d(UpdateBuilder.class.getSimpleName(), "## there's no need to update");
                    break;
                }
                break;
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(UpdateBuilder.class.getSimpleName(), "## UpdateBuilder is Force stop");
                EboxContext.message = this.activity.getString(R.string.stop_thread);
            } catch (Exception e2) {
                Exception error = e2;
                error.printStackTrace();
                Log.e(UpdateBuilder.class.getSimpleName(), "## upgrade is wronging info: " + error.getMessage() + " for " + errcount);
                errcount++;
            }
        }
        reportSuccess(success);
        Log.d(UpdateBuilder.class.getSimpleName(), "## upgrade end");
    }
}
