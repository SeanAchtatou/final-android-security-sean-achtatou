package com.heyibao.android.ebox.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.util.Utils;
import java.util.ArrayList;

public class DetailsAdapter extends ArrayAdapter<Details> implements View.OnClickListener {
    public static final int RESOURCE = 2130903053;
    private Context context;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public boolean onTouchBoolean = false;
    /* access modifiers changed from: private */
    public int onTouchItemIndex = -1;

    private static class ViewHolder {
        LinearLayout btnLayout;
        Button cancelBtn;
        TextView createTimeTV;
        Button delBtn;
        Button emailBtn;
        ImageView imgIV;
        ImageView msgIV;
        TextView msgTV;
        TextView nameTV;
        Button openBtn;
        LinearLayout pgLayout;
        ProgressBar progressBar;
        Button reNameBtn;
        LinearLayout rowLayout;
        TextView sizeTV;
        ImageView upIV;

        private ViewHolder() {
        }
    }

    public DetailsAdapter(Context context2, ArrayList<Details> _listFiles) {
        super(context2, (int) R.layout.details_row, _listFiles);
        this.context = context2;
        this.inflater = LayoutInflater.from(context2);
    }

    public void toggle(int position) {
        if (this.onTouchItemIndex == position) {
            this.onTouchBoolean = !this.onTouchBoolean;
        } else {
            this.onTouchItemIndex = position;
            this.onTouchBoolean = true;
        }
        notifyDataSetChanged();
    }

    public void reSet() {
        this.onTouchItemIndex = -1;
        this.onTouchBoolean = false;
        EboxContext.mSystemInfo.reSetCurRowNum();
    }

    public boolean hasSelected() {
        return this.onTouchBoolean;
    }

    public void setSelectedId(int position) {
        this.onTouchItemIndex = position;
    }

    public int getSelectedId() {
        return this.onTouchItemIndex;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Details details = (Details) getItem(position);
        if (details == null) {
        }
        if (convertView == null) {
            convertView = this.inflater.inflate((int) R.layout.details_row, (ViewGroup) null);
            holder = new ViewHolder();
            holder.rowLayout = (LinearLayout) convertView.findViewById(R.id.ll_file);
            holder.imgIV = (ImageView) convertView.findViewById(R.id.ImageView01);
            holder.nameTV = (TextView) convertView.findViewById(R.id.tv_name);
            holder.sizeTV = (TextView) convertView.findViewById(R.id.tv_size);
            holder.createTimeTV = (TextView) convertView.findViewById(R.id.tv_time);
            holder.pgLayout = (LinearLayout) convertView.findViewById(R.id.item_progress_layout);
            holder.cancelBtn = (Button) convertView.findViewById(R.id.bt_cancel_du);
            holder.cancelBtn.setOnClickListener(this);
            holder.msgTV = (TextView) convertView.findViewById(R.id.tv_msg);
            holder.msgIV = (ImageView) convertView.findViewById(R.id.iv_msg);
            holder.progressBar = (ProgressBar) convertView.findViewById(R.id.pg_download);
            holder.upIV = (ImageView) convertView.findViewById(R.id.iv_updown);
            holder.btnLayout = (LinearLayout) convertView.findViewById(R.id.item_btns_layout);
            holder.reNameBtn = (Button) holder.btnLayout.findViewById(R.id.rename_btn);
            holder.reNameBtn.setOnClickListener(this);
            holder.delBtn = (Button) holder.btnLayout.findViewById(R.id.del_btn);
            holder.delBtn.setOnClickListener(this);
            holder.emailBtn = (Button) holder.btnLayout.findViewById(R.id.email_btn);
            holder.emailBtn.setOnClickListener(this);
            holder.openBtn = (Button) holder.btnLayout.findViewById(R.id.open_btn);
            holder.openBtn.setOnClickListener(this);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        setValue(holder, details, position);
        holder.imgIV.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (DetailsAdapter.this.onTouchItemIndex == position) {
                    boolean unused = DetailsAdapter.this.onTouchBoolean = !DetailsAdapter.this.onTouchBoolean;
                } else {
                    int unused2 = DetailsAdapter.this.onTouchItemIndex = position;
                    boolean unused3 = DetailsAdapter.this.onTouchBoolean = false;
                }
                DetailsAdapter.this.notifyDataSetChanged();
            }
        });
        return convertView;
    }

    private void setValue(ViewHolder holder, Details details, int position) {
        if (details != null) {
            holder.imgIV.setImageBitmap(null);
            holder.msgIV.setImageBitmap(null);
            holder.nameTV.setText(details.getName());
            holder.msgTV.setText((CharSequence) null);
            Bitmap bm = Utils.getThumbnail(this.context, details.getThumbnail(), details.getIcon().intValue(), details.hasSuccess());
            if (bm != null) {
                holder.imgIV.setImageBitmap(bm);
            } else {
                holder.imgIV.setImageResource(details.getIcon().intValue());
            }
            if (details.getObjectType().endsWith(EboxConst.FILE)) {
                holder.emailBtn.setVisibility(0);
                if (details.hasSuccess()) {
                    holder.openBtn.setBackgroundResource(R.drawable.bt_openfile);
                    holder.msgTV.setVisibility(4);
                    holder.msgIV.setVisibility(0);
                    holder.msgIV.setImageResource(R.drawable.ok);
                } else {
                    holder.openBtn.setBackgroundResource(R.drawable.bt_download);
                }
                holder.sizeTV.setText(Utils.getSize(Double.valueOf(details.getSize())));
                holder.createTimeTV.setText(Utils.toDate(details.getCreateTime().intValue()) + EboxConst.TAB_UPLOADER_TAG);
            } else {
                holder.createTimeTV.setText(Utils.toDate(details.getCreateTime().intValue()) + EboxConst.TAB_UPLOADER_TAG);
                holder.openBtn.setBackgroundResource(R.drawable.bt_openfolder);
                holder.emailBtn.setVisibility(8);
                if ("mail_receive.png".equals(details.getThumbnail())) {
                    holder.sizeTV.setText(this.context.getResources().getString(R.string.deliv_folder));
                } else if ("folder_share.png".equals(details.getThumbnail())) {
                    holder.sizeTV.setText(this.context.getResources().getString(R.string.share_folder));
                } else {
                    holder.sizeTV.setText(this.context.getResources().getString(R.string.normal_folder));
                }
            }
            if (details.getUpdown().intValue() > 0) {
                if (details.getUpdown().intValue() == 8) {
                    holder.upIV.setImageResource(R.drawable.download_arrow);
                } else {
                    holder.upIV.setImageResource(R.drawable.upload_arrow);
                    holder.sizeTV.setText(Utils.getSize(Double.valueOf(details.getSize())));
                }
                holder.createTimeTV.setVisibility(8);
                holder.pgLayout.setVisibility(0);
                holder.cancelBtn.setVisibility(0);
                if (EboxContext.message != null) {
                    holder.msgTV.setVisibility(0);
                    holder.msgIV.setVisibility(8);
                    holder.msgTV.setText(EboxContext.message);
                }
                int pro = details.getProgress().intValue();
                holder.progressBar.setMax((int) details.getSize());
                holder.progressBar.setProgress(pro);
            } else {
                holder.progressBar.setProgress(0);
                holder.pgLayout.setVisibility(8);
                holder.cancelBtn.setVisibility(4);
                holder.createTimeTV.setVisibility(0);
            }
            if (position != this.onTouchItemIndex) {
                holder.btnLayout.setVisibility(8);
                holder.rowLayout.setBackgroundResource(R.drawable.lvbg);
                holder.imgIV.setPressed(false);
            } else if (this.onTouchBoolean) {
                holder.btnLayout.setVisibility(8);
                holder.rowLayout.setBackgroundResource(R.drawable.lvbg);
                holder.imgIV.setPressed(false);
            } else {
                holder.btnLayout.setVisibility(0);
                holder.rowLayout.setBackgroundResource(R.drawable.clicklvbg);
                holder.imgIV.setPressed(true);
            }
        }
    }

    public void showOrhidePro(boolean isTrue) {
    }

    public void onClick(View arg0) {
    }
}
