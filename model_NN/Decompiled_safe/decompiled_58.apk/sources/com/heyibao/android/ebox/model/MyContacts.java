package com.heyibao.android.ebox.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.http.GPBUtils;
import com.heyibao.android.ebox.model.Json.JsonModel;
import com.heyibao.android.ebox.model.Json.JsonParent;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;

public class MyContacts {
    public Address address;
    private int contactId = -1;
    private String createdTime;
    private HashMap<Integer, String> emailV2;
    private String firstName;
    private int groupMembership = -1;
    private HashMap<Integer, String> im;
    private String lastName;
    private String modifiedTime;
    private String name;
    private String nickName;
    private String note;
    private HashMap<Integer, Organization> organV2;
    public Organization organization;
    private HashMap<Integer, String> phoneV2;
    private byte[] photoBt;
    private Bitmap photoMap;
    private String photoUrl;
    private int photoVer = 0;
    private HashMap<Integer, Address> postalAddressV2;
    private int rawContactId = -1;
    private int remoteId = -1;
    private String status;
    private int version = -1;
    private HashMap<Integer, String> website;

    public class Organization {
        private String company;
        private String duties;

        public Organization() {
        }

        public void setCompany(String company2) {
            this.company = company2;
        }

        public String getCompany() {
            return this.company;
        }

        public void setDuties(String duties2) {
            this.duties = duties2;
        }

        public String getDuties() {
            return this.duties;
        }
    }

    public class Address {
        private String address;
        private String city;
        private String country;
        private String province;
        private String zip;

        public Address() {
        }

        public void setAddress(String address2) {
            this.address = address2;
        }

        public String getAddress() {
            return this.address;
        }

        public void setCity(String city2) {
            this.city = city2;
        }

        public String getCity() {
            return this.city;
        }

        public void setProvince(String province2) {
            this.province = province2;
        }

        public String getProvince() {
            return this.province;
        }

        public void setZip(String zip2) {
            this.zip = zip2;
        }

        public String getZip() {
            return this.zip;
        }

        public void setCountry(String country2) {
            this.country = country2;
        }

        public String getCountry() {
            return this.country;
        }
    }

    public MyContacts() {
    }

    public Organization getInstanceOrganization() {
        if (this.organization == null) {
            this.organization = new Organization();
        }
        return this.organization;
    }

    public Address getInstanceAddress() {
        if (this.address == null) {
            this.address = new Address();
        }
        return this.address;
    }

    public MyContacts(JsonParent contacts) throws MyException, JSONException {
        Object value = contacts.getData("id");
        if (value != null) {
            String temp = value.toString();
            if (!temp.equals("null")) {
                setRemoteId(Integer.valueOf(temp).intValue());
            }
        }
        Object value2 = contacts.getData(JsonModel.STATUS);
        if (value2 != null) {
            String temp2 = value2.toString();
            if (!temp2.equals("null")) {
                setStatus(temp2);
            }
        }
        Object value3 = contacts.getData(JsonModel.ContactsJson.C_FIRST_NAME);
        if (value3 != null) {
            String temp3 = value3.toString();
            temp3.replace(" ", EboxConst.TAB_UPLOADER_TAG);
            if (!temp3.equals("null")) {
                setFirstName(temp3);
            }
        }
        Object value4 = contacts.getData(JsonModel.ContactsJson.C_LAST_NAME);
        if (value4 != null) {
            String temp4 = value4.toString();
            temp4.replace(" ", EboxConst.TAB_UPLOADER_TAG);
            if (!temp4.equals("null")) {
                setLastName(temp4);
            }
        }
        String temp5 = (getFirstName() == null ? EboxConst.TAB_UPLOADER_TAG : getFirstName()) + (getLastName() == null ? EboxConst.TAB_UPLOADER_TAG : getLastName());
        if (temp5 != null && !temp5.equals(EboxConst.TAB_UPLOADER_TAG) && !temp5.equals("null")) {
            setName(temp5);
        }
        Object value5 = contacts.getData("content");
        if (value5 != null) {
            String temp6 = value5.toString();
            if (!temp6.equals("null")) {
                Matcher matcher = Pattern.compile(EboxConst.REGXPFORHTML).matcher(temp6);
                StringBuffer sb = new StringBuffer();
                for (boolean result = matcher.find(); result; result = matcher.find()) {
                    matcher.appendReplacement(sb, EboxConst.TAB_UPLOADER_TAG);
                }
                matcher.appendTail(sb);
                String temp7 = sb.toString();
                if (!temp7.equals("null") && !temp7.equals(EboxConst.TAB_UPLOADER_TAG)) {
                    setNote(temp7);
                }
            }
        }
        Object value6 = contacts.getData("modified");
        if (value6 != null) {
            String temp8 = value6.toString();
            if (!temp8.equals("null")) {
                setModifiedTime(temp8);
            }
        }
        Object value7 = contacts.getData("date");
        if (value7 != null) {
            String temp9 = value7.toString();
            if (!temp9.equals("null")) {
                setCreatedTime(temp9);
            }
        }
        Object value8 = contacts.getData(JsonModel.ContactsJson.C_EMAIL);
        if (value8 != null) {
            String temp10 = value8.toString();
            if (!temp10.equals("null")) {
                addEmailV2(1, temp10);
            }
        }
        Object value9 = contacts.getData(JsonModel.ContactsJson.C_IM_QQ);
        if (value9 != null) {
            String temp11 = value9.toString();
            if (!temp11.equals("null")) {
                addIm(4, temp11);
            }
        }
        Object value10 = contacts.getData(JsonModel.ContactsJson.C_HOME_PHONE);
        if (value10 != null) {
            String temp12 = value10.toString();
            if (!temp12.equals("null")) {
                addPhoneV2(1, temp12);
            }
        }
        Object value11 = contacts.getData(JsonModel.ContactsJson.C_WORK_PHONE);
        if (value11 != null) {
            String temp13 = value11.toString();
            if (!temp13.equals("null")) {
                addPhoneV2(3, temp13);
            }
        }
        Object value12 = contacts.getData(JsonModel.ContactsJson.C_MOBLIE_PHONE);
        if (value12 != null) {
            String temp14 = value12.toString();
            if (!temp14.equals("null")) {
                addPhoneV2(2, temp14);
            }
        }
        Object value13 = contacts.getData(JsonModel.ContactsJson.C_FAX_PHONT);
        if (value13 != null) {
            String temp15 = value13.toString();
            if (!temp15.equals("null")) {
                addPhoneV2(4, temp15);
            }
        }
        Object value14 = contacts.getData(JsonModel.ContactsJson.C_OTHER_PHONE);
        if (value14 != null) {
            String temp16 = value14.toString();
            if (!temp16.equals("null")) {
                addPhoneV2(7, temp16);
            }
        }
        Object value15 = contacts.getData(JsonModel.ContactsJson.C_ADDRESS);
        if (value15 != null) {
            String temp17 = value15.toString();
            if (!temp17.equals("null")) {
                this.address = getInstanceAddress();
                this.address.setAddress(temp17);
            }
        }
        Object value16 = contacts.getData(JsonModel.ContactsJson.C_CITY);
        if (value16 != null) {
            String temp18 = value16.toString();
            if (!temp18.equals("null")) {
                this.address = getInstanceAddress();
                this.address.setCity(temp18);
            }
        }
        Object value17 = contacts.getData(JsonModel.ContactsJson.C_COUNTRY);
        if (value17 != null) {
            String temp19 = value17.toString();
            if (!temp19.equals("null")) {
                this.address = getInstanceAddress();
                this.address.setCountry(temp19);
            }
        }
        Object value18 = contacts.getData(JsonModel.ContactsJson.C_ZIP);
        if (value18 != null) {
            String temp20 = value18.toString();
            if (!temp20.equals("null")) {
                this.address = getInstanceAddress();
                this.address.setZip(temp20);
            }
        }
        Object value19 = contacts.getData(JsonModel.ContactsJson.C_PROVINCE);
        if (value19 != null) {
            String temp21 = value19.toString();
            if (!temp21.equals("null")) {
                this.address = getInstanceAddress();
                this.address.setProvince(temp21);
            }
        }
        if (this.address != null) {
            addPostalAddressV2(1, this.address);
        }
        Object value20 = contacts.getData(JsonModel.ContactsJson.C_COMPANY);
        if (value20 != null) {
            String temp22 = value20.toString();
            if (!temp22.equals("null")) {
                this.organization = getInstanceOrganization();
                this.organization.setCompany(temp22);
            }
        }
        Object value21 = contacts.getData(JsonModel.ContactsJson.C_TITLE);
        if (value21 != null) {
            String temp23 = value21.toString();
            if (!temp23.equals("null")) {
                this.organization = getInstanceOrganization();
                this.organization.setDuties(temp23);
            }
        }
        if (this.organization != null) {
            addOrganization(1, this.organization);
        }
        Object value22 = contacts.getData(JsonModel.ContactsJson.C_WEB_SITE);
        if (value22 != null) {
            String temp24 = value22.toString();
            if (!temp24.equals("null")) {
                addWebsite(7, temp24);
            }
        }
        JSONArray arr = (JSONArray) contacts.getData("attachments");
        if (!arr.isNull(0)) {
            setPhotoUrl(arr.getJSONObject(0).getJSONObject(JsonModel.ContactsJson.PHOTO_IMAGES).getJSONObject("thumbnail").getString("url"));
        }
    }

    public void setStatus(String status2) {
        this.status = status2;
    }

    public String getStatus() {
        return this.status;
    }

    public void setCreatedTime(String createdTime2) {
        this.createdTime = createdTime2;
    }

    public String getCreatedTime() {
        return this.createdTime;
    }

    public void setModifiedTime(String modifiedTime2) {
        this.modifiedTime = modifiedTime2;
    }

    public String getModifiedTime() {
        return this.modifiedTime;
    }

    public void setRemoteId(int syncId) {
        this.remoteId = syncId;
    }

    public int getSyncId() {
        return this.remoteId;
    }

    public void setVersion(int version2) {
        this.version = version2;
    }

    public int getVersion() {
        return this.version;
    }

    public void setRawContactId(int rawContactId2) {
        this.rawContactId = rawContactId2;
    }

    public int getRawContactId() {
        return this.rawContactId;
    }

    public void setContactId(int contactId2) {
        this.contactId = contactId2;
    }

    public int getContactId() {
        return this.contactId;
    }

    public void setPhotoMap(Bitmap bp) {
        this.photoMap = bp;
    }

    public Bitmap getPhotoMap() {
        return this.photoMap;
    }

    public void setPhotoBt(String url) {
        if (url != null) {
            try {
                byte[] result = new GPBUtils(url).getResult(null);
                int len = result.length;
                if (len == 0) {
                    return;
                }
                if (len < 524288) {
                    setPhotoBt(result);
                    return;
                }
                BitmapFactory.Options o = new BitmapFactory.Options();
                o.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(new ByteArrayInputStream(result), null, o);
                int width_tmp = o.outWidth;
                int height_tmp = o.outHeight;
                int scale = 1;
                while (width_tmp / 2 >= 75 && height_tmp / 2 >= 75) {
                    width_tmp /= 2;
                    height_tmp /= 2;
                    scale *= 2;
                }
                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize = scale;
                Bitmap bm = BitmapFactory.decodeStream(new ByteArrayInputStream(result), null, o2);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 0, bos);
                byte[] result2 = bos.toByteArray();
                if (result2.length != 0) {
                    setPhotoBt(result2);
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void setPhotoBt(byte[] bt) {
        this.photoBt = bt;
    }

    public void reSetPhoteBt() {
        this.photoBt = null;
    }

    public byte[] getPhotoBt() {
        return this.photoBt;
    }

    public void setPhotoVer(int photoVer2) {
        this.photoVer = photoVer2;
    }

    public int getPhotoVer() {
        return this.photoVer;
    }

    public void setPhotoUrl(String photoUrl2) {
        this.photoUrl = photoUrl2;
    }

    public String getPhotoUrl() {
        return this.photoUrl;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public void setFirstName(String firstName2) {
        this.firstName = firstName2;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setLastName(String lastName2) {
        this.lastName = lastName2;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setNick(String nickName2) {
        this.nickName = nickName2;
    }

    public String getNick() {
        return this.nickName;
    }

    public void setGroup(int groupMembership2) {
        this.groupMembership = groupMembership2;
    }

    public int getGroup() {
        return this.groupMembership;
    }

    public void setNote(String note2) {
        this.note = note2;
    }

    public String getNote() {
        return this.note == null ? EboxConst.TAB_UPLOADER_TAG : this.note;
    }

    public void addEmailV2(Integer key, String emailV22) {
        if (this.emailV2 == null) {
            this.emailV2 = new HashMap<>();
        }
        this.emailV2.put(key, emailV22);
    }

    public HashMap<Integer, String> getEmailV2() {
        return this.emailV2;
    }

    public void addOrganization(Integer key, Organization organization2) {
        if (this.organV2 == null) {
            this.organV2 = new HashMap<>();
        }
        this.organV2.put(key, organization2);
    }

    public HashMap<Integer, Organization> getOrganization() {
        return this.organV2;
    }

    public void addPostalAddressV2(Integer key, Address postalAddressV22) {
        if (this.postalAddressV2 == null) {
            this.postalAddressV2 = new HashMap<>();
        }
        this.postalAddressV2.put(key, postalAddressV22);
    }

    public HashMap<Integer, Address> getPostalAddressV2() {
        return this.postalAddressV2;
    }

    public void addWebsite(Integer key, String website2) {
        if (this.website == null) {
            this.website = new HashMap<>();
        }
        this.website.put(key, website2);
    }

    public HashMap<Integer, String> getWebsite() {
        return this.website;
    }

    public void addPhoneV2(Integer key, String phoneV22) {
        if (this.phoneV2 == null) {
            this.phoneV2 = new HashMap<>();
        }
        this.phoneV2.put(key, phoneV22);
    }

    public HashMap<Integer, String> getPhoneV2() {
        return this.phoneV2;
    }

    public void addIm(Integer key, String im2) {
        if (this.im == null) {
            this.im = new HashMap<>();
        }
        this.im.put(key, im2);
    }

    public HashMap<Integer, String> getIm() {
        return this.im;
    }

    public String toString() {
        return null;
    }
}
