package com.heyibao.android.ebox.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.db.ActionDB;
import com.heyibao.android.ebox.model.Details;
import com.heyibao.android.ebox.model.FileBrowserAdapter;
import com.heyibao.android.ebox.model.MyDialog;
import com.heyibao.android.ebox.util.Utils;
import java.util.ArrayList;

public class DownLoadActivity extends HomeMenuActivity {
    /* access modifiers changed from: private */
    public ArrayList<Details> arrList;
    /* access modifiers changed from: private */
    public Details curDetails;
    private FileBrowserAdapter fileAdpater;
    protected AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View arg1, int which, long arg3) {
            if (which == 0) {
                DownLoadActivity.this.onSubmitOpen(DownLoadActivity.this.curDetails.getPath(), DownLoadActivity.this.curDetails.getIcon().intValue());
            } else if (which == 1) {
                DownLoadActivity.this.removeDialog(9);
                DownLoadActivity.this.showDialog(9);
            }
            DownLoadActivity.this.removeDialog(11);
        }
    };
    private Runnable runnable;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.download_view);
        Log.d(DownLoadActivity.class.getSimpleName(), "## start MyDownLoadActivity : ");
        notesInit();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 9:
                MyDialog mDialog = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.bt_ok:
                                DownLoadActivity.this.onSubmitDel();
                                dismiss();
                                return;
                            case R.id.bt_cancel:
                                DownLoadActivity.this.cancelFuncation(this);
                                return;
                            default:
                                return;
                        }
                    }
                };
                mDialog.setDefaultView();
                mDialog.setMessage(Utils.replace(getResources().getString(R.string.del_loc_file), this.curDetails.getName()));
                return mDialog;
            case 10:
            default:
                return null;
            case 11:
                MyDialog mDialog2 = new MyDialog(this, R.style.dialog) {
                    public void onClick(View v) {
                    }
                };
                mDialog2.setListView(R.array.details_option, this.itemClickListener);
                return mDialog2;
        }
    }

    /* access modifiers changed from: protected */
    public void reLoader() {
        if (EboxContext.mSystemNotify.hasNotifyDownload()) {
            EboxContext.mSystemNotify.setNotifyDownload(false);
            this.mHandler.postDelayed(this.runnable, 500);
        }
    }

    private void notesInit() {
        initTitleBar();
        this.midTV.setText(getString(R.string.my_download));
        this.refreshBtn.setVisibility(4);
        this.retreatBtn.setVisibility(4);
        if (this.fileAdpater != null) {
            this.fileAdpater.clear();
        }
        this.runnable = getDataRunnable();
        this.mHandler.postDelayed(this.runnable, 500);
    }

    private Runnable getDataRunnable() {
        return new Runnable() {
            public void run() {
                if (!Utils.hasSdcard(DownLoadActivity.this)) {
                    ArrayList unused = DownLoadActivity.this.arrList = Utils.getThumbnailForLocal(DownLoadActivity.this, EboxContext.mSystemInfo.getUserSdPath());
                    if (DownLoadActivity.this.arrList.size() <= 0) {
                        Utils.MessageBox(DownLoadActivity.this, DownLoadActivity.this.getResources().getString(R.string.no_info_donwload));
                    }
                    DownLoadActivity.this.notesView();
                    DownLoadActivity.this.mHandler.removeCallbacks(this);
                }
            }
        };
    }

    /* access modifiers changed from: private */
    public void notesView() {
        ListView notesListView = (ListView) findViewById(16908298);
        notesListView.setOnTouchListener(this);
        this.fileAdpater = new FileBrowserAdapter(this, this.arrList, R.layout.download_row);
        notesListView.setAdapter((ListAdapter) this.fileAdpater);
        notesListView.setFocusableInTouchMode(true);
        notesListView.setFocusable(true);
        this.fileAdpater.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void onSubmitDel() {
        Utils.toDelFile(this.curDetails.getName());
        if (this.curDetails.hasSuccess()) {
            this.curDetails.setSuccess(false);
            this.curDetails.setThumbnail(Utils.getSuffix(this.curDetails.getName()) + EboxConst.SUFFIX);
            ActionDB.updateOneDetail(this, this.curDetails);
        }
        ActionDB.deleteThumbnail(this, this.curDetails.getName(), this.curDetails.getIcon().intValue());
        this.mHandler.postDelayed(this.runnable, 500);
        this.curDetails = null;
        EboxContext.mSystemNotify.setNotifyListDetails(true);
        EboxContext.mSystemNotify.setNotifySystem(true);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retreat_btn:
                MainTabActivity.setCurrentTab(4);
                return;
            default:
                return;
        }
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        this.curDetails = this.arrList.get(position);
        this.curDetails.setCode(Integer.valueOf(position));
        if (!this.curDetails.hasSuccess()) {
            Utils.MessageBox(this, getResources().getString(R.string.file_no_exist));
        }
        showDialog(11);
    }
}
