package com.heyibao.android.ebox.model;

import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.model.Json.JsonModel;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileBlockDataObject {
    private List<Map<String, Integer>> blockList;
    private String filePath;

    public FileBlockDataObject() {
    }

    public FileBlockDataObject(String filePath2) {
        this.filePath = filePath2;
        initConfig();
    }

    public synchronized BlockInfo getNextBlock() {
        BlockInfo blockInfo;
        int i = 0;
        while (true) {
            if (i >= this.blockList.size()) {
                blockInfo = null;
                break;
            }
            Map<String, Integer> object = this.blockList.get(i);
            if (((Integer) object.get(JsonModel.STATUS)).intValue() == -1) {
                object.put(JsonModel.STATUS, 0);
                BlockInfo retval = new BlockInfo();
                retval.setCount(((Integer) object.get("count")).intValue());
                retval.setOffset(((Integer) object.get(JsonModel.ContactsJson.OFFSET)).intValue());
                blockInfo = retval;
                break;
            }
            i++;
        }
        return blockInfo;
    }

    public int getBlockCount() {
        return this.blockList.size();
    }

    private void initConfig() {
        File file = new File(this.filePath);
        int blocks = (int) (file.length() / 1048576);
        if (file.length() % 1048576 > 0) {
            blocks++;
        }
        this.blockList = new ArrayList();
        for (int i = 0; i < blocks; i++) {
            Map<String, Integer> item = new HashMap<>();
            item.put(JsonModel.ContactsJson.OFFSET, new Integer(i * EboxConst.BLOCK_SIZE));
            item.put("count", new Integer((int) EboxConst.BLOCK_SIZE));
            item.put(JsonModel.STATUS, new Integer(-1));
            this.blockList.add(item);
        }
    }

    public synchronized int getUploadedSize() {
        int count;
        count = 0;
        for (int i = 0; i < this.blockList.size(); i++) {
            Map<String, Integer> item = this.blockList.get(i);
            if (((Integer) item.get(JsonModel.STATUS)).intValue() != 1) {
                break;
            }
            count += ((Integer) item.get("count")).intValue();
        }
        return count;
    }

    public boolean isSuccess() {
        for (int i = 0; i < this.blockList.size(); i++) {
            if (((Integer) this.blockList.get(i).get(JsonModel.STATUS)).intValue() != 1) {
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r1.put(com.heyibao.android.ebox.model.Json.JsonModel.STATUS, -1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0020, code lost:
        if (r6 == false) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        r1.put(com.heyibao.android.ebox.model.Json.JsonModel.STATUS, 1);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void updateBlockSuccess(int r5, boolean r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            r0 = 0
        L_0x0002:
            java.util.List<java.util.Map<java.lang.String, java.lang.Integer>> r2 = r4.blockList     // Catch:{ all -> 0x0039 }
            int r2 = r2.size()     // Catch:{ all -> 0x0039 }
            if (r0 >= r2) goto L_0x002c
            java.util.List<java.util.Map<java.lang.String, java.lang.Integer>> r2 = r4.blockList     // Catch:{ all -> 0x0039 }
            java.lang.Object r1 = r2.get(r0)     // Catch:{ all -> 0x0039 }
            java.util.Map r1 = (java.util.Map) r1     // Catch:{ all -> 0x0039 }
            java.lang.String r2 = "offset"
            java.lang.Object r2 = r1.get(r2)     // Catch:{ all -> 0x0039 }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ all -> 0x0039 }
            int r2 = r2.intValue()     // Catch:{ all -> 0x0039 }
            if (r2 != r5) goto L_0x003c
            if (r6 == 0) goto L_0x002e
            java.lang.String r2 = "status"
            r3 = 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0039 }
            r1.put(r2, r3)     // Catch:{ all -> 0x0039 }
        L_0x002c:
            monitor-exit(r4)
            return
        L_0x002e:
            java.lang.String r2 = "status"
            r3 = -1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0039 }
            r1.put(r2, r3)     // Catch:{ all -> 0x0039 }
            goto L_0x002c
        L_0x0039:
            r2 = move-exception
            monitor-exit(r4)
            throw r2
        L_0x003c:
            int r0 = r0 + 1
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heyibao.android.ebox.model.FileBlockDataObject.updateBlockSuccess(int, boolean):void");
    }
}
