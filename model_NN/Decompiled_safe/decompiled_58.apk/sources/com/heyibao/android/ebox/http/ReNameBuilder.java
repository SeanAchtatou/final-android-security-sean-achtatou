package com.heyibao.android.ebox.http;

import ClientProtocol.ClientMeta;
import android.app.Service;
import android.util.Log;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import java.util.Date;

public abstract class ReNameBuilder implements Runnable, HttpInterface {
    private Service activity;
    private int createTime;
    private String fileName;
    private String objectType;
    private String objectUUID;
    private String oldName;
    private String parentUUID;
    private String shareUUID;

    public ReNameBuilder(Service activity2, String newName, String fileName2, String objectUUID2, String parentUUID2, String shareUUID2, String objectType2, int createTime2) {
        this.activity = activity2;
        this.oldName = fileName2;
        this.fileName = newName;
        this.parentUUID = parentUUID2;
        this.objectUUID = objectUUID2;
        this.objectType = objectType2;
        this.shareUUID = shareUUID2;
        this.createTime = createTime2;
    }

    public void run() {
        Log.d(ReNameBuilder.class.getSimpleName(), "## start ReNameBuilder ");
        reNameStart();
        Log.d(ReNameBuilder.class.getSimpleName(), "end ReNameBuilder ");
    }

    private void reNameStart() {
        int errcount = 0;
        boolean success = false;
        while (true) {
            if (Thread.interrupted() || errcount >= 3) {
                break;
            }
            try {
                ClientMeta.UploadMetadataRequest.Builder request = ClientMeta.UploadMetadataRequest.newBuilder();
                Integer time = Integer.valueOf((int) (((double) new Date().getTime()) * 0.001d));
                if (this.objectType.equals(EboxConst.FILE)) {
                    ClientMeta.FileDetails.Builder fileDetails = ClientMeta.FileDetails.newBuilder();
                    fileDetails.setShowUuid(this.objectUUID);
                    fileDetails.setParentShowUuid(this.parentUUID);
                    fileDetails.setName(this.fileName);
                    fileDetails.setCreateTime(this.createTime);
                    fileDetails.setModifyTime(time.intValue());
                    ClientMeta.Action.Builder action = ClientMeta.Action.newBuilder();
                    action.setActionType(ClientMeta.ActionType.RENAME_FILE);
                    action.setShareDirectoryUuid(this.shareUUID);
                    action.setFileDetails(fileDetails);
                    request.setSession(EboxContext.mDevice.getSession());
                    request.addAction(action);
                } else {
                    ClientMeta.DirectoryDetails.Builder dirDetails = ClientMeta.DirectoryDetails.newBuilder();
                    dirDetails.setShowUuid(this.objectUUID);
                    dirDetails.setParentShowUuid(this.parentUUID);
                    dirDetails.setCreateTime(this.createTime);
                    dirDetails.setModifyTime(time.intValue());
                    dirDetails.setName(this.fileName);
                    ClientMeta.Action.Builder action2 = ClientMeta.Action.newBuilder();
                    action2.setActionType(ClientMeta.ActionType.RENAME_DIRECTORY);
                    action2.setShareDirectoryUuid(this.shareUUID);
                    action2.setDirDetails(dirDetails);
                    request.setSession(EboxContext.mDevice.getSession());
                    request.addAction(action2);
                }
                Thread.sleep(10);
                ClientMeta.UploadMetadataResponse response = ClientMeta.UploadMetadataResponse.parseFrom(new GPBUtils(EboxConst.UPLOADMETA_URL).getResult(request.build().toByteArray()));
                ClientMeta.Status status = response.getAction(0).getStatus();
                if (!response.getStatus().getSuccess()) {
                    EboxContext.message = EboxConst.SESSIONEXIT;
                    break;
                }
                if (status.getSuccess()) {
                    success = true;
                    EboxContext.mDetails.setShowUuid(this.objectUUID);
                    EboxContext.mDetails.setName(this.fileName);
                    EboxContext.mDetails.setMsg(this.oldName);
                    EboxContext.mDetails.setModifyTime(time);
                }
                EboxContext.message = status.getMsg();
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(ReNameBuilder.class.getSimpleName(), "## ReNameBuilder is Force stop");
                EboxContext.message = this.activity.getString(R.string.stop_thread);
            } catch (Exception e2) {
                Exception error = e2;
                error.printStackTrace();
                Log.e(ReNameBuilder.class.getSimpleName(), "## ReNameBuilder error : " + error.getMessage() + " for " + errcount);
                errcount++;
                EboxContext.message = this.activity.getResources().getString(R.string.net_error);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
            }
        }
        reportSuccess(success);
    }
}
