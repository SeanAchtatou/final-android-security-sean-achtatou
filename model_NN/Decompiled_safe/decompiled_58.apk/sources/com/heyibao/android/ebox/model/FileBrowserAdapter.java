package com.heyibao.android.ebox.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.util.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FileBrowserAdapter extends ArrayAdapter<Details> {
    private int RESOURCE;
    private Context context;
    private LayoutInflater inflater;
    private Map<Integer, View> viewMap = new HashMap();

    static class ViewHolder {
        ImageView imgIV;
        TextView nameTV;
        TextView sizeTV;

        ViewHolder() {
        }
    }

    public FileBrowserAdapter(Context context2, ArrayList<Details> _listFiles, int row) {
        super(context2, row, _listFiles);
        this.context = context2;
        this.RESOURCE = row;
        this.inflater = LayoutInflater.from(context2);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = this.viewMap.get(Integer.valueOf(position));
        Details details = (Details) getItem(position);
        if (details == null) {
        }
        if (rowView == null) {
            rowView = this.inflater.inflate(this.RESOURCE, (ViewGroup) null);
            ViewHolder holder = new ViewHolder();
            holder.imgIV = (ImageView) rowView.findViewById(R.id.avatar);
            holder.nameTV = (TextView) rowView.findViewById(R.id.d_name_tv);
            holder.sizeTV = (TextView) rowView.findViewById(R.id.d_size_tv);
            holder.nameTV.setText(details.getName());
            if (details.getIcon().intValue() == R.drawable.bt_folder) {
                holder.sizeTV.setText(EboxConst.TAB_UPLOADER_TAG);
            } else if (details.getIcon().intValue() == R.drawable.back_up_level) {
                holder.sizeTV.setText(EboxConst.TAB_UPLOADER_TAG);
            } else {
                holder.sizeTV.setText(this.context.getResources().getString(R.string.file_size) + Utils.getSize(Double.valueOf(details.getSize())));
            }
            Bitmap bm = Utils.getThumbnail(this.context, details.getThumbnail(), details.getIcon().intValue(), true);
            if (bm != null) {
                holder.imgIV.setImageBitmap(bm);
            } else {
                holder.imgIV.setImageResource(details.getIcon().intValue());
            }
            this.viewMap.put(Integer.valueOf(position), rowView);
        }
        return rowView;
    }
}
