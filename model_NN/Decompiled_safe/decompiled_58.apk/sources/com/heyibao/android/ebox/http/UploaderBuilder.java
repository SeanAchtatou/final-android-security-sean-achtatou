package com.heyibao.android.ebox.http;

import ClientProtocol.ClientMeta;
import android.app.Service;
import android.util.Log;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.model.Json.JsonModel;
import com.heyibao.android.ebox.util.Utils;
import java.io.File;
import java.net.URLEncoder;
import java.util.Date;
import org.json.JSONObject;

public abstract class UploaderBuilder implements Runnable, HttpInterface {
    /* access modifiers changed from: private */
    public Service activity;
    /* access modifiers changed from: private */
    public String fileName;
    /* access modifiers changed from: private */
    public String filePath;
    /* access modifiers changed from: private */
    public double fileSize;
    /* access modifiers changed from: private */
    public boolean hasEndFile = false;
    private String objectType;
    private String parentUUID;
    private String signature = EboxConst.TAB_UPLOADER_TAG;
    /* access modifiers changed from: private */
    public long startTime;
    /* access modifiers changed from: private */
    public int startUploadedSize;
    /* access modifiers changed from: private */
    public int threadUploadedSize;

    static /* synthetic */ int access$012(UploaderBuilder x0, int x1) {
        int i = x0.threadUploadedSize + x1;
        x0.threadUploadedSize = i;
        return i;
    }

    public UploaderBuilder(Service active, String path, String name, double size, String parentUUID2, String objectType2, int icon) {
        this.activity = active;
        this.parentUUID = parentUUID2;
        this.objectType = objectType2;
        this.fileName = name;
        this.filePath = path;
        this.fileSize = size;
    }

    public void run() {
        Log.d(UploaderBuilder.class.getSimpleName(), "## start UploaderBuilder ");
        EboxContext.message = this.activity.getResources().getString(R.string.upload_info_1);
        reportProgress(0, 0);
        this.signature = Utils.getSignature(this.filePath);
        startCheckFile();
        Log.d(UploaderBuilder.class.getSimpleName(), "## checkfile end ");
        if (!EboxContext.mDetails.hasSuccess()) {
            EboxContext.message = this.activity.getResources().getString(R.string.upload_false);
            reportSuccess(false);
        } else if (EboxContext.mDetails.getCode().intValue() == 20) {
            startUpload();
        } else {
            Log.d(UploaderBuilder.class.getSimpleName(), "## no need upload ");
            EboxContext.mDetails.setObjectType(this.objectType + EboxConst.FILE);
            EboxContext.message = this.activity.getResources().getString(R.string.upload_info_2);
            reportProgress((int) this.fileSize, (int) (this.fileSize * 0.1d));
            Utils.toCopy(this.filePath, this.fileName);
            reportProgress((int) this.fileSize, (int) (this.fileSize * 0.2d));
            reportSuccess(true);
        }
        Log.d(UploaderBuilder.class.getSimpleName(), "end UploaderBuilder ");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x006b, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x006c, code lost:
        r4.printStackTrace();
        android.util.Log.e(com.heyibao.android.ebox.http.UploaderBuilder.class.getSimpleName(), "## go is Force stop");
        com.heyibao.android.ebox.util.Utils.stopThreadList();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x007f, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0080, code lost:
        r1 = r4;
        r1.printStackTrace();
        android.util.Log.e(com.heyibao.android.ebox.http.UploaderBuilder.class.getSimpleName(), "## go er " + r1.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x006b A[ExcHandler: InterruptedException (r4v6 'e' java.lang.InterruptedException A[CUSTOM_DECLARE]), Splitter:B:2:0x0011] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void startUpload() {
        /*
            r7 = this;
            java.lang.Class<com.heyibao.android.ebox.http.UploaderBuilder> r4 = com.heyibao.android.ebox.http.UploaderBuilder.class
            java.lang.String r4 = r4.getSimpleName()
            java.lang.String r5 = "## start upload "
            android.util.Log.d(r4, r5)
            boolean r4 = java.lang.Thread.interrupted()
            if (r4 != 0) goto L_0x007e
            java.util.Date r4 = new java.util.Date     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            r4.<init>()     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            long r4 = r4.getTime()     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            r7.startTime = r4     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            com.heyibao.android.ebox.model.FileBlockDataObject r4 = new com.heyibao.android.ebox.model.FileBlockDataObject     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            java.lang.String r5 = r7.filePath     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            r4.<init>(r5)     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            com.heyibao.android.ebox.common.EboxContext.blockInfo = r4     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            com.heyibao.android.ebox.model.FileBlockDataObject r4 = com.heyibao.android.ebox.common.EboxContext.blockInfo     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            int r4 = r4.getUploadedSize()     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            r7.startUploadedSize = r4     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            r4 = 0
            r7.threadUploadedSize = r4     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            com.heyibao.android.ebox.model.FileBlockDataObject r4 = com.heyibao.android.ebox.common.EboxContext.blockInfo     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            int r0 = r4.getBlockCount()     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            android.app.Service r4 = r7.activity     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            r5 = 2131230823(0x7f080067, float:1.807771E38)
            java.lang.String r4 = r4.getString(r5)     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            com.heyibao.android.ebox.common.EboxContext.message = r4     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            r4 = 0
            r5 = 0
            r7.reportProgress(r4, r5)     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            r4 = 100
            java.lang.Thread.sleep(r4)     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            r4 = 3
            java.lang.Thread[] r4 = new java.lang.Thread[r4]     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            com.heyibao.android.ebox.common.EboxContext.uploaderThread = r4     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            r2 = 0
        L_0x0051:
            r4 = 3
            int r4 = java.lang.Math.min(r0, r4)     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            if (r2 >= r4) goto L_0x007e
            java.lang.Thread r3 = r7.blockUploadThread(r2)     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            java.lang.Thread[] r4 = com.heyibao.android.ebox.common.EboxContext.uploaderThread     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            r4[r2] = r3     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            r3.start()     // Catch:{ InterruptedException -> 0x006b, Exception -> 0x007f }
            r4 = 3000(0xbb8, double:1.482E-320)
            java.lang.Thread.sleep(r4)     // Catch:{ Exception -> 0x00a5, InterruptedException -> 0x006b }
        L_0x0068:
            int r2 = r2 + 1
            goto L_0x0051
        L_0x006b:
            r4 = move-exception
            r1 = r4
            r1.printStackTrace()
            java.lang.Class<com.heyibao.android.ebox.http.UploaderBuilder> r4 = com.heyibao.android.ebox.http.UploaderBuilder.class
            java.lang.String r4 = r4.getSimpleName()
            java.lang.String r5 = "## go is Force stop"
            android.util.Log.e(r4, r5)
            com.heyibao.android.ebox.util.Utils.stopThreadList()
        L_0x007e:
            return
        L_0x007f:
            r4 = move-exception
            r1 = r4
            r1.printStackTrace()
            java.lang.Class<com.heyibao.android.ebox.http.UploaderBuilder> r4 = com.heyibao.android.ebox.http.UploaderBuilder.class
            java.lang.String r4 = r4.getSimpleName()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "## go er "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = r1.getMessage()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            android.util.Log.e(r4, r5)
            goto L_0x007e
        L_0x00a5:
            r4 = move-exception
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heyibao.android.ebox.http.UploaderBuilder.startUpload():void");
    }

    private Thread blockUploadThread(int num) {
        return new Thread(new BlockUploader(this.activity, num) {
            public void reProgress(int blockIndex, int uploadedSize) {
                UploaderBuilder.access$012(UploaderBuilder.this, uploadedSize);
                long i = (new Date().getTime() - UploaderBuilder.this.startTime) / 1000;
                if (i > 0) {
                    float speed = (float) (((long) UploaderBuilder.this.threadUploadedSize) / (1024 * i));
                }
                UploaderBuilder.this.reportProgress(UploaderBuilder.this.startUploadedSize + UploaderBuilder.this.threadUploadedSize, 0);
            }

            public void reSuccess(boolean value) {
                if (!value) {
                    Utils.stopThreadList();
                    UploaderBuilder.this.reportSuccess(false);
                } else if (EboxContext.blockInfo.isSuccess()) {
                    Log.d(UploaderBuilder.class.getSimpleName(), "## end upload ");
                    if (!UploaderBuilder.this.hasEndFile) {
                        boolean unused = UploaderBuilder.this.hasEndFile = true;
                        EboxContext.message = UploaderBuilder.this.activity.getResources().getString(R.string.upload_info_2);
                        if (UploaderBuilder.this.megerFile() == 200) {
                            UploaderBuilder.this.reportProgress((int) (UploaderBuilder.this.fileSize * 1.1d), 0);
                            if (!UploaderBuilder.this.endFile() || EboxContext.mDetails.getCode().intValue() != 0) {
                                EboxContext.message = UploaderBuilder.this.activity.getResources().getString(R.string.upload_false) + EboxContext.mDetails.getMsg();
                                UploaderBuilder.this.reportSuccess(false);
                                return;
                            }
                            Utils.toCopy(UploaderBuilder.this.filePath, UploaderBuilder.this.fileName);
                            UploaderBuilder.this.reportProgress((int) (UploaderBuilder.this.fileSize * 1.2d), 0);
                            UploaderBuilder.this.reportSuccess(true);
                            return;
                        }
                        UploaderBuilder.this.reportSuccess(false);
                    }
                }
            }
        });
    }

    private void startCheckFile() {
        Log.d(UploaderBuilder.class.getSimpleName(), "## start checkfile ");
        int errcount = 0;
        while (!Thread.interrupted() && errcount < 3) {
            try {
                File file = new File(this.filePath);
                Integer createTime = Integer.valueOf((int) (((double) file.lastModified()) * 0.001d));
                Integer updateTime = Integer.valueOf((int) (((double) file.lastModified()) * 0.001d));
                ClientMeta.ObjectBlock.Builder block = ClientMeta.ObjectBlock.newBuilder();
                block.setSignature(this.signature);
                block.setBlockType(0);
                ClientMeta.FileDetails.Builder fileDetail = ClientMeta.FileDetails.newBuilder();
                fileDetail.setName(this.fileName);
                fileDetail.setSignature(this.signature);
                fileDetail.setSize((int) this.fileSize);
                fileDetail.setParentShowUuid(this.parentUUID);
                fileDetail.setObjectType(0);
                fileDetail.setCreateTime(createTime.intValue());
                fileDetail.setModifyTime(updateTime.intValue());
                fileDetail.addBlockList(block);
                ClientMeta.Action.Builder action = ClientMeta.Action.newBuilder();
                action.setActionType(ClientMeta.ActionType.CREATE_CHECK_FILE);
                action.setFileDetails(fileDetail);
                ClientMeta.UploadMetadataRequest.Builder request = ClientMeta.UploadMetadataRequest.newBuilder();
                request.setSession(EboxContext.mDevice.getSession());
                request.addAction(action);
                Thread.sleep(10);
                ClientMeta.UploadMetadataResponse reponse = ClientMeta.UploadMetadataResponse.parseFrom(new GPBUtils(EboxConst.UPLOADMETA_URL).getResult(request.build().toByteArray()));
                ClientMeta.Status status = reponse.getAction(0).getStatus();
                String post = reponse.getAction(0).getFileDetails().getBlockList(0).getPost();
                String uploadUrl = reponse.getAction(0).getFileDetails().getBlockList(0).getPath();
                int uploadingID = reponse.getAction(0).getFileDetails().getBlockList(0).getUploadingId();
                EboxContext.mDetails.setShowUuid(reponse.getAction(0).getFileDetails().getShowUuid());
                EboxContext.mDetails.setSuccess(status.getSuccess());
                EboxContext.mDetails.setCode(Integer.valueOf(status.getCode()));
                EboxContext.mDetails.setMsg(status.getMsg());
                EboxContext.mDetails.setUrl(uploadUrl.replace("https://", "http://"));
                EboxContext.mDetails.setPost(post);
                EboxContext.mDetails.setUploadId(Integer.valueOf(uploadingID));
                EboxContext.mDetails.setSignature(this.signature);
                EboxContext.mDetails.setPath(this.filePath);
                EboxContext.mDetails.setName(this.fileName);
                EboxContext.mDetails.setSize(this.fileSize);
                EboxContext.mDetails.setSession(EboxContext.mDevice.getSession());
                EboxContext.mDetails.setCreateTime(createTime);
                EboxContext.mDetails.setModifyTime(updateTime);
                EboxContext.mDetails.setParentShowUuid(this.parentUUID);
                return;
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(UploaderBuilder.class.getSimpleName(), "## UploaderBuilder checkfile is Force stop");
                EboxContext.message = this.activity.getString(R.string.stop_thread);
                EboxContext.mDetails.setSuccess(false);
                return;
            } catch (Exception e2) {
                Exception error = e2;
                error.printStackTrace();
                Log.e(UploaderBuilder.class.getSimpleName(), "## uploader checkfile error :" + error.getMessage() + " for " + errcount);
                EboxContext.mDetails.setSuccess(false);
                errcount++;
                EboxContext.message = this.activity.getResources().getString(R.string.upload_false) + errcount;
                try {
                    Thread.sleep(500);
                } catch (Exception e3) {
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public int megerFile() {
        Log.d(UploaderBuilder.class.getSimpleName(), "## start megerFile ");
        String data = EboxConst.TAB_UPLOADER_TAG;
        int status = 0;
        for (int i = 0; i < EboxContext.blockInfo.getBlockCount(); i++) {
            if (i == EboxContext.blockInfo.getBlockCount() - 1) {
                data = data + Utils.splicSignature(this.signature) + this.signature + "_part_" + (i + 1);
            } else {
                data = data + Utils.splicSignature(this.signature) + this.signature + "_part_" + (i + 1) + "|";
            }
        }
        try {
            JSONObject jSONObject = new JSONObject(EboxContext.mDetails.getPost());
            String expiration_date = jSONObject.getString("expiration_date");
            String digital_signature = jSONObject.getString("digital_signature");
            String key = jSONObject.getString("key");
            String megerUrl = EboxContext.mDetails.getUrl() + "/merge_part.php";
            Log.d(UploaderBuilder.class.getSimpleName(), "## megerFile url " + megerUrl);
            if (!Thread.interrupted()) {
                try {
                    String content = ((((URLEncoder.encode("Filename", "UTF-8") + "=" + URLEncoder.encode(this.signature, "UTF-8")) + "&" + URLEncoder.encode("key", "UTF-8") + "=" + URLEncoder.encode(key, "UTF-8")) + "&" + URLEncoder.encode("partlist", "UTF-8") + "=" + URLEncoder.encode(data, "UTF-8")) + "&" + URLEncoder.encode("expiration_date", "UTF-8") + "=" + URLEncoder.encode(expiration_date, "UTF-8")) + "&" + URLEncoder.encode("digital_signature", "UTF-8") + "=" + URLEncoder.encode(digital_signature, "UTF-8");
                    Log.d(UploaderBuilder.class.getSimpleName(), "## megerFile url " + megerUrl + "?" + content);
                    Thread.sleep(10);
                    JSONObject jSONObject2 = new JSONObject(new String(new GPBUtils(megerUrl).getResult(content.getBytes())));
                    status = jSONObject2.getInt(JsonModel.STATUS);
                    if (status != 200) {
                        Log.e(UploaderBuilder.class.getSimpleName(), "## megerFile error response !200 " + jSONObject2.getString("msg"));
                        EboxContext.message = this.activity.getResources().getString(R.string.upload_info_4);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Log.e(UploaderBuilder.class.getSimpleName(), "## UploaderBuilder megerFile is Force stop");
                    EboxContext.message = this.activity.getString(R.string.stop_thread);
                } catch (Exception e2) {
                    Exception error = e2;
                    Log.e(UploaderBuilder.class.getSimpleName(), "## megerFile error : " + error.getMessage() + " for ");
                    EboxContext.message = this.activity.getResources().getString(R.string.upload_info_4) + error.getMessage();
                }
            }
            return status;
        } catch (Exception e3) {
            Exception error2 = e3;
            Log.e(UploaderBuilder.class.getSimpleName(), "## megerFile error : " + error2.getMessage() + " for ");
            EboxContext.message = this.activity.getResources().getString(R.string.upload_info_4) + error2.getMessage();
            return 0;
        }
    }

    /* access modifiers changed from: private */
    public boolean endFile() {
        Log.d(UploaderBuilder.class.getSimpleName(), "## start endFile ");
        int errcount = 0;
        while (!Thread.interrupted() && errcount < 3) {
            try {
                String name = EboxContext.mDetails.getName();
                String signatrue = EboxContext.mDetails.getSignature();
                String parentShowUUID = EboxContext.mDetails.getParentShowUuid();
                Integer createTime = EboxContext.mDetails.getCreateTime();
                Integer updateTime = EboxContext.mDetails.getModifyTime();
                double size = EboxContext.mDetails.getSize();
                Integer uploadID = EboxContext.mDetails.getUploadId();
                ClientMeta.ObjectBlock.Builder block = ClientMeta.ObjectBlock.newBuilder();
                block.setSignature(signatrue);
                block.setBlockType(0);
                block.setUploadingId(uploadID.intValue());
                ClientMeta.FileDetails.Builder fileDetail = ClientMeta.FileDetails.newBuilder();
                fileDetail.setName(name);
                fileDetail.setSignature(signatrue);
                fileDetail.setSize((int) size);
                fileDetail.setParentShowUuid(parentShowUUID);
                fileDetail.setObjectType(0);
                fileDetail.setCreateTime(createTime.intValue());
                fileDetail.setModifyTime(updateTime.intValue());
                fileDetail.addBlockList(block);
                ClientMeta.Action.Builder action = ClientMeta.Action.newBuilder();
                action.setActionType(ClientMeta.ActionType.CREATE_FILE);
                action.setFileDetails(fileDetail);
                ClientMeta.UploadMetadataRequest.Builder request = ClientMeta.UploadMetadataRequest.newBuilder();
                request.setSession(EboxContext.mDevice.getSession());
                request.addAction(action);
                Thread.sleep(10);
                ClientMeta.UploadMetadataResponse reponse = ClientMeta.UploadMetadataResponse.parseFrom(new GPBUtils(EboxConst.UPLOADMETA_URL).getResult(request.build().toByteArray()));
                ClientMeta.Status status = reponse.getAction(0).getStatus();
                EboxContext.mDetails.setSuccess(status.getSuccess());
                EboxContext.mDetails.setCode(Integer.valueOf(status.getCode()));
                EboxContext.mDetails.setShowUuid(reponse.getAction(0).getFileDetails().getShowUuid());
                EboxContext.mDetails.setMsg(status.getMsg());
                EboxContext.mDetails.setSignature(signatrue);
                EboxContext.mDetails.setName(this.fileName);
                EboxContext.mDetails.setSize(size);
                EboxContext.mDetails.setSession(EboxContext.mDevice.getSession());
                EboxContext.mDetails.setCreateTime(createTime);
                EboxContext.mDetails.setModifyTime(updateTime);
                EboxContext.mDetails.setObjectType(this.objectType + EboxConst.FILE);
                return true;
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(UploaderBuilder.class.getSimpleName(), "## UploaderBuilder endfile is Force stop");
                EboxContext.message = this.activity.getString(R.string.stop_thread);
                return false;
            } catch (Exception e2) {
                Exception error = e2;
                error.printStackTrace();
                Log.e(UploaderBuilder.class.getSimpleName(), "## endfile error : " + error.getMessage() + " for " + errcount);
                errcount++;
                EboxContext.message = this.activity.getResources().getString(R.string.upload_info_4) + errcount;
                try {
                    Thread.sleep(500);
                } catch (Exception e3) {
                }
            }
        }
        return false;
    }
}
