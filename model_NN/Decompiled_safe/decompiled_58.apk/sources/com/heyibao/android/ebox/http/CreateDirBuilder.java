package com.heyibao.android.ebox.http;

import ClientProtocol.ClientMeta;
import android.app.Service;
import android.util.Log;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import java.util.Date;

public abstract class CreateDirBuilder implements Runnable, HttpInterface {
    private Service activity;
    private String fileName;
    private String objectType;
    private String parentUUID;

    public CreateDirBuilder(Service activity2, String fileName2, String parentUUID2, String objectType2) {
        this.activity = activity2;
        this.fileName = fileName2;
        this.parentUUID = parentUUID2;
        this.objectType = objectType2;
    }

    public void run() {
        Log.d(CreateDirBuilder.class.getSimpleName(), "## start CreateDirBuilder ");
        createDirStart();
        Log.d(CreateDirBuilder.class.getSimpleName(), "end CreateDirBuilder ");
    }

    private void createDirStart() {
        int errcount = 0;
        boolean success = false;
        Integer time = Integer.valueOf((int) (((double) new Date().getTime()) * 0.001d));
        while (true) {
            if (Thread.interrupted() || errcount >= 3) {
                break;
            }
            try {
                ClientMeta.DirectoryDetails.Builder dirDetails = ClientMeta.DirectoryDetails.newBuilder();
                dirDetails.setParentShowUuid(this.parentUUID);
                dirDetails.setName(this.fileName);
                dirDetails.setCreateTime(time.intValue());
                dirDetails.setModifyTime(time.intValue());
                ClientMeta.Action.Builder action = ClientMeta.Action.newBuilder();
                action.setActionType(ClientMeta.ActionType.CREATE_DIRECTORY);
                action.setDirDetails(dirDetails);
                ClientMeta.UploadMetadataRequest.Builder request = ClientMeta.UploadMetadataRequest.newBuilder();
                request.setSession(EboxContext.mDevice.getSession());
                request.addAction(action);
                Thread.sleep(10);
                ClientMeta.UploadMetadataResponse response = ClientMeta.UploadMetadataResponse.parseFrom(new GPBUtils(EboxConst.UPLOADMETA_URL).getResult(request.build().toByteArray()));
                ClientMeta.Status status = response.getAction(0).getStatus();
                if (response.getStatus().getSuccess()) {
                    if (!status.getSuccess()) {
                        EboxContext.message = status.getMsg();
                        break;
                    }
                    success = true;
                    ClientMeta.DirectoryDetails retVal = response.getActionList().get(0).getDirDetails();
                    EboxContext.mDetails.setName(retVal.getName());
                    EboxContext.mDetails.setShowUuid(retVal.getShowUuid());
                    EboxContext.mDetails.setParentShowUuid(retVal.getParentShowUuid());
                    EboxContext.mDetails.setName(response.getActionList().get(0).getDirDetails().getName());
                    EboxContext.mDetails.setSize(0.0d);
                    EboxContext.mDetails.setCreateTime(Integer.valueOf(retVal.getCreateTime()));
                    EboxContext.mDetails.setUploadId(Integer.valueOf(retVal.getModifyTime()));
                    EboxContext.mDetails.setObjectType(this.objectType + EboxConst.FOLDER);
                    if (EboxContext.mDetails.getName().equals(this.activity.getString(R.string.delivery_folder)) || this.objectType.indexOf(EboxConst.FOLDERDELIVERY) >= 0) {
                        EboxContext.mDetails.setThumbnail("mail_receive.png");
                        EboxContext.mDetails.setIcon(Integer.valueOf((int) R.drawable.bt_folder));
                    } else if (this.objectType.indexOf(EboxConst.FOLDERSHARE) >= 0) {
                        EboxContext.mDetails.setThumbnail("folder_share.png");
                        break;
                    } else if (this.objectType.indexOf(EboxConst.FOLDERSHAREED) >= 0) {
                        EboxContext.mDetails.setIcon(Integer.valueOf((int) R.drawable.bt_folder));
                        EboxContext.mDetails.setThumbnail("folder_share.png");
                    } else {
                        EboxContext.mDetails.setIcon(Integer.valueOf((int) R.drawable.bt_folder));
                        EboxContext.mDetails.setThumbnail("folder.png");
                    }
                } else {
                    EboxContext.message = EboxConst.SESSIONEXIT;
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(CreateDirBuilder.class.getSimpleName(), "## CreateDirBuilder is Force stop");
                EboxContext.message = this.activity.getString(R.string.stop_thread);
            } catch (Exception e2) {
                Exception error = e2;
                error.printStackTrace();
                Log.e(CreateDirBuilder.class.getSimpleName(), "## CreateDirBuilder error : " + error.getMessage() + " for " + errcount);
                errcount++;
                EboxContext.message = this.activity.getResources().getString(R.string.net_error);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
            }
        }
        EboxContext.mDetails.setThumbnail("mail_receive.png");
        EboxContext.mDetails.setIcon(Integer.valueOf((int) R.drawable.bt_folder));
        reportSuccess(success);
    }
}
