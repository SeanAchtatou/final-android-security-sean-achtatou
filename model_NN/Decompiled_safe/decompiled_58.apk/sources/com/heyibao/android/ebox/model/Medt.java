package com.heyibao.android.ebox.model;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

/* compiled from: EditView */
abstract class Medt extends EditText implements EdtInterface {
    TextWatcher tx = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
        }

        public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
        }

        public void afterTextChanged(Editable s) {
            if (s.length() == 0) {
                Medt.this.hideBtn();
                return;
            }
            Medt.this.showBtn();
            Medt.this.verifyText(s.toString());
        }
    };

    public Medt(Context context) {
        super(context);
        addTextChangedListener(this.tx);
    }

    public Medt(Context context, AttributeSet attrs) {
        super(context, attrs);
        addTextChangedListener(this.tx);
    }
}
