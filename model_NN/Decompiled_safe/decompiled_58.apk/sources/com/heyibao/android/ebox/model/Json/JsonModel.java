package com.heyibao.android.ebox.model.Json;

public class JsonModel {
    public static final String ERROR = "error";
    public static final String STATUS = "status";

    public static class ContactsJson {
        public static final String ATTACHMENTS = "attachments";
        public static final String COMMENTS = "comments";
        public static final String COMMENT_COUNT = "comment_count";
        public static final String CONTENT = "content";
        public static final String COUNT = "count";
        public static final String CREATED_TIME = "date";
        public static final String C_ADDRESS = "c_address";
        public static final String C_CITY = "c_city";
        public static final String C_COMPANY = "c_company";
        public static final String C_COUNTRY = "c_country";
        public static final String C_EMAIL = "c_email";
        public static final String C_FAX_PHONT = "c_fax_phone";
        public static final String C_FIRST_NAME = "c_first_name";
        public static final String C_HOME_PHONE = "c_home_phone";
        public static final String C_IM_QQ = "c_im_qq";
        public static final String C_LAST_NAME = "c_last_name";
        public static final String C_MOBLIE_PHONE = "c_mobile_phone";
        public static final String C_OTHER_PHONE = "c_other_phone";
        public static final String C_PROVINCE = "c_province";
        public static final String C_QQ_WEIBO = "c_qq_weibo";
        public static final String C_SINA_WEIBO = "c_sian_weibo";
        public static final String C_TAG = "c_tag";
        public static final String C_TITLE = "c_title";
        public static final String C_WEB_SITE = "c_web_site";
        public static final String C_WORK_PHONE = "c_work_phone";
        public static final String C_ZIP = "c_zip";
        public static final String ID = "id";
        public static final String KEY = "contacts";
        public static final String LAST_DATE = "last_date";
        public static final String MODIFIED_TIME = "modified";
        public static final String NAME = "title";
        public static final String OFFSET = "offset";
        public static final String PHOTO = "attachments";
        public static final String PHOTO_IMAGES = "images";
        public static final String PHOTO_THUMBNAIL = "thumbnail";
        public static final String PHOTO_URL = "url";
        public static final String SLUG = "slug";
        public static final String TITLE_PLAIN = "title_plain";
        public static final String TOTAL_COUNT = "total_count";
        public static final String URL = "url";
    }

    public static class NewsJson {
        public static final String ATTACHMENTS = "attachments";
        public static final String COMMENTS = "comments";
        public static final String COMMENT_COUNT = "comment_count";
        public static final String CONTENT = "content";
        public static final String COUNT = "count";
        public static final String CREATED_TIME = "date";
        public static final String ID = "id";
        public static final String KEY = "news";
        public static final String MODIFIED_TIME = "modified";
        public static final String PHOTO_URL = "thumbnail";
        public static final String SLUG = "slug";
        public static final String TITLE = "title";
        public static final String URL = "url";
        public static boolean isArray = true;
    }
}
