package com.heyibao.android.ebox.activity;

import android.app.ListActivity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.heyibao.android.ebox.R;
import com.heyibao.android.ebox.common.EboxConst;
import com.heyibao.android.ebox.common.EboxContext;
import com.heyibao.android.ebox.model.MyDialog;
import com.heyibao.android.ebox.util.Utils;

public class HomeMenuActivity extends ListActivity implements View.OnTouchListener, GestureDetector.OnGestureListener, View.OnClickListener {
    protected static final int HOME_ID = 1;
    private MyDialog exitDialog;
    private GestureDetector gestureScanner;
    protected Handler mHandler = new Handler();
    protected LinearLayout mainLayout;
    protected TextView midTV;
    protected Object object = new Object();
    protected Button refreshBtn;
    protected Button retreatBtn;
    protected Runnable runnableUI;
    private MyDialog stopDialog;

    /* access modifiers changed from: protected */
    public MyDialog exitDialog() {
        if (this.exitDialog == null) {
            this.exitDialog = new MyDialog(this, R.style.dialog) {
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.bt_ok:
                            Utils.exitAppEnd(HomeMenuActivity.this);
                            return;
                        case R.id.bt_cancel:
                            cancel();
                            return;
                        default:
                            return;
                    }
                }
            };
        }
        return this.exitDialog;
    }

    /* access modifiers changed from: protected */
    public MyDialog stopDialog() {
        if (this.stopDialog == null) {
            this.stopDialog = new MyDialog(this, R.style.dialog) {
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.bt_ok:
                            HomeMenuActivity.this.verifyStop(this);
                            return;
                        case R.id.bt_cancel:
                            HomeMenuActivity.this.cancelFuncation(this);
                            return;
                        default:
                            return;
                    }
                }

                public void onBackPressed() {
                    EboxContext.mSystemNotify.setNotifyThreadWait(false);
                    super.onBackPressed();
                }
            };
            this.stopDialog.setDefaultView();
            this.stopDialog.setMessage(getResources().getString(R.string.verify_stop));
            this.stopDialog.setCanceledOnTouchOutside(false);
        }
        return this.stopDialog;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.gestureScanner = new GestureDetector(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        onConfigurationChanged(getResources().getConfiguration());
    }

    /* access modifiers changed from: protected */
    public void reLoader() {
    }

    /* access modifiers changed from: protected */
    public void initTitleBar() {
        this.mainLayout = (LinearLayout) findViewById(R.id.auto_panel);
        View titleLayout = findViewById(R.id.title_bar);
        this.retreatBtn = (Button) titleLayout.findViewById(R.id.retreat_btn);
        this.retreatBtn.setOnClickListener(this);
        this.midTV = (TextView) titleLayout.findViewById(R.id.title_text);
        this.refreshBtn = (Button) titleLayout.findViewById(R.id.refresh_btn);
        this.refreshBtn.setBackgroundResource(R.anim.redo);
        this.refreshBtn.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void showProgressBar() {
        if (this.refreshBtn != null) {
            AnimationDrawable ani = (AnimationDrawable) this.refreshBtn.getBackground();
            if (ani.isRunning()) {
                ani.stop();
            } else {
                ani.start();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void closeProgressBar() {
        if (this.refreshBtn != null) {
            AnimationDrawable ani = (AnimationDrawable) this.refreshBtn.getBackground();
            if (ani.isRunning()) {
                ani.stop();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void stopWorking() {
        closeProgressBar();
        Utils.startService(this, EboxConst.ACTION_STOP, null, 0);
    }

    /* access modifiers changed from: protected */
    public boolean hasWorking() {
        if (EboxContext.threadIsable) {
            return true;
        }
        Utils.MessageBox(this, getResources().getString(R.string.frequent));
        return false;
    }

    /* access modifiers changed from: protected */
    public void httpFalse() {
        closeProgressBar();
        if (EboxConst.SESSIONEXIT.equals(EboxContext.message)) {
            Utils.MessageBox(this, getResources().getString(R.string.error_info_required));
            Utils.startService(this, EboxConst.ACTION_NORMAL, null, R.drawable.app_unnet);
            startActivity(new Intent(this, NewAccActivity.class));
            finish();
            return;
        }
        Utils.startService(this, EboxConst.ACTION_NORMAL, null, 0);
        Utils.MessageBox(this, EboxContext.message);
    }

    /* access modifiers changed from: protected */
    public void openURL(String url) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
    }

    /* access modifiers changed from: protected */
    public void onSubmitOpen(String path, int icon) {
        try {
            String path2 = path.replace(" ", "%20");
            Intent intent = new Intent("android.intent.action.VIEW");
            Uri data = Uri.parse(Utils.sanitizeFilePath(path2));
            if (icon == R.drawable.bt_jpg) {
                intent.setDataAndType(data, "image/*");
            } else if (icon == R.drawable.bt_file) {
                intent.setDataAndType(data, "video/*");
            } else if (icon == R.drawable.bt_mp3) {
                intent.setDataAndType(data, "audio/*");
            } else if (icon == R.drawable.text) {
                intent.setDataAndType(data, "text/*");
            } else {
                intent.setDataAndType(data, "application/*");
            }
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            Utils.MessageBox(this, getResources().getString(R.string.type_error));
        }
    }

    /* access modifiers changed from: protected */
    public void verifyStop(MyDialog dialog) {
        stopWorking();
        dialog.dismiss();
    }

    /* access modifiers changed from: protected */
    public void cancelFuncation(MyDialog dialog) {
        EboxContext.mSystemNotify.setNotifyThreadWait(false);
        dialog.dismiss();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        boolean b = super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.back_home).setIcon((int) R.drawable.arrow);
        return b;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                Intent intent = new Intent("android.intent.action.MAIN");
                intent.addCategory("android.intent.category.HOME");
                intent.setFlags(268435456);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);
        if (config != null && this.mainLayout != null) {
            if (getResources().getConfiguration().orientation == 2) {
                this.mainLayout.setBackgroundResource(R.drawable.setting_bg);
            } else if (getResources().getConfiguration().orientation == 1) {
                this.mainLayout.setBackgroundResource(R.drawable.set_bg);
            }
        }
    }

    public boolean onKeyDown(int i, KeyEvent event) {
        if (i != 4) {
            return false;
        }
        exitDialog().show();
        return false;
    }

    public boolean onTouch(View v, MotionEvent event) {
        return this.gestureScanner.onTouchEvent(event);
    }

    public boolean onDown(MotionEvent arg0) {
        return false;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float arg2, float arg3) {
        try {
            if (e1.getX() - e2.getX() > 160.0f) {
                if (!EboxContext.threadIsable) {
                    EboxContext.mSystemNotify.setNotifyThreadWait(true);
                    showDialog(12);
                } else {
                    MainTabActivity.setCurrentTab(true);
                }
                return true;
            }
            if (e1.getX() - e2.getX() < -160.0f) {
                if (!EboxContext.threadIsable) {
                    EboxContext.mSystemNotify.setNotifyThreadWait(true);
                    showDialog(12);
                } else {
                    MainTabActivity.setCurrentTab(false);
                }
                return true;
            }
            return false;
        } catch (Exception e) {
            Log.e(HomeMenuActivity.class.getSimpleName(), "## onFling error : " + e.getMessage());
        }
    }

    public void onLongPress(MotionEvent arg0) {
    }

    public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2, float arg3) {
        return false;
    }

    public void onShowPress(MotionEvent arg0) {
    }

    public boolean onSingleTapUp(MotionEvent arg0) {
        return false;
    }

    public void onClick(View arg0) {
    }
}
