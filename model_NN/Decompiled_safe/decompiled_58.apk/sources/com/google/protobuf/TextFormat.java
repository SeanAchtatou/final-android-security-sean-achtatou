package com.google.protobuf;

import com.google.protobuf.Descriptors;
import com.google.protobuf.ExtensionRegistry;
import com.google.protobuf.Message;
import com.google.protobuf.UnknownFieldSet;
import com.heyibao.android.ebox.common.EboxConst;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.CharBuffer;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class TextFormat {
    private static final int BUFFER_SIZE = 4096;

    private TextFormat() {
    }

    public static void print(Message message, Appendable output) throws IOException {
        print(message, new TextGenerator(output, null));
    }

    public static void print(UnknownFieldSet fields, Appendable output) throws IOException {
        printUnknownFields(fields, new TextGenerator(output, null));
    }

    public static String printToString(Message message) {
        try {
            StringBuilder text = new StringBuilder();
            print(message, text);
            return text.toString();
        } catch (IOException e) {
            throw new RuntimeException("Writing to a StringBuilder threw an IOException (should never happen).", e);
        }
    }

    public static String printToString(UnknownFieldSet fields) {
        try {
            StringBuilder text = new StringBuilder();
            print(fields, text);
            return text.toString();
        } catch (IOException e) {
            throw new RuntimeException("Writing to a StringBuilder threw an IOException (should never happen).", e);
        }
    }

    private static void print(Message message, TextGenerator generator) throws IOException {
        for (Map.Entry<Descriptors.FieldDescriptor, Object> field : message.getAllFields().entrySet()) {
            printField((Descriptors.FieldDescriptor) field.getKey(), field.getValue(), generator);
        }
        printUnknownFields(message.getUnknownFields(), generator);
    }

    public static void printField(Descriptors.FieldDescriptor field, Object value, Appendable output) throws IOException {
        printField(field, value, new TextGenerator(output, null));
    }

    public static String printFieldToString(Descriptors.FieldDescriptor field, Object value) {
        try {
            StringBuilder text = new StringBuilder();
            printField(field, value, text);
            return text.toString();
        } catch (IOException e) {
            throw new RuntimeException("Writing to a StringBuilder threw an IOException (should never happen).", e);
        }
    }

    private static void printField(Descriptors.FieldDescriptor field, Object value, TextGenerator generator) throws IOException {
        if (field.isRepeated()) {
            for (Object element : (List) value) {
                printSingleField(field, element, generator);
            }
            return;
        }
        printSingleField(field, value, generator);
    }

    private static void printSingleField(Descriptors.FieldDescriptor field, Object value, TextGenerator generator) throws IOException {
        if (field.isExtension()) {
            generator.print("[");
            if (!field.getContainingType().getOptions().getMessageSetWireFormat() || field.getType() != Descriptors.FieldDescriptor.Type.MESSAGE || !field.isOptional() || field.getExtensionScope() != field.getMessageType()) {
                generator.print(field.getFullName());
            } else {
                generator.print(field.getMessageType().getFullName());
            }
            generator.print("]");
        } else if (field.getType() == Descriptors.FieldDescriptor.Type.GROUP) {
            generator.print(field.getMessageType().getName());
        } else {
            generator.print(field.getName());
        }
        if (field.getJavaType() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
            generator.print(" {\n");
            generator.indent();
        } else {
            generator.print(": ");
        }
        printFieldValue(field, value, generator);
        if (field.getJavaType() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
            generator.outdent();
            generator.print("}");
        }
        generator.print("\n");
    }

    /* renamed from: com.google.protobuf.TextFormat$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type = new int[Descriptors.FieldDescriptor.Type.values().length];

        static {
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.INT32.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.INT64.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.SINT32.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.SINT64.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.SFIXED32.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.SFIXED64.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.FLOAT.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.DOUBLE.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.BOOL.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.UINT32.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.FIXED32.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.UINT64.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.FIXED64.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.STRING.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.BYTES.ordinal()] = 15;
            } catch (NoSuchFieldError e15) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.ENUM.ordinal()] = 16;
            } catch (NoSuchFieldError e16) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.MESSAGE.ordinal()] = 17;
            } catch (NoSuchFieldError e17) {
            }
            try {
                $SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[Descriptors.FieldDescriptor.Type.GROUP.ordinal()] = 18;
            } catch (NoSuchFieldError e18) {
            }
        }
    }

    private static void printFieldValue(Descriptors.FieldDescriptor field, Object value, TextGenerator generator) throws IOException {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[field.getType().ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                generator.print(value.toString());
                return;
            case 10:
            case 11:
                generator.print(unsignedToString(((Integer) value).intValue()));
                return;
            case 12:
            case 13:
                generator.print(unsignedToString(((Long) value).longValue()));
                return;
            case 14:
                generator.print("\"");
                generator.print(escapeText((String) value));
                generator.print("\"");
                return;
            case 15:
                generator.print("\"");
                generator.print(escapeBytes((ByteString) value));
                generator.print("\"");
                return;
            case 16:
                generator.print(((Descriptors.EnumValueDescriptor) value).getName());
                return;
            case EboxConst.SIGN /*17*/:
            case 18:
                print((Message) value, generator);
                return;
            default:
                return;
        }
    }

    private static void printUnknownFields(UnknownFieldSet unknownFields, TextGenerator generator) throws IOException {
        for (Map.Entry<Integer, UnknownFieldSet.Field> entry : unknownFields.asMap().entrySet()) {
            String str = ((Integer) entry.getKey()).toString() + ": ";
            UnknownFieldSet.Field field = (UnknownFieldSet.Field) entry.getValue();
            for (Long longValue : field.getVarintList()) {
                long value = longValue.longValue();
                generator.print(((Integer) entry.getKey()).toString());
                generator.print(": ");
                generator.print(unsignedToString(value));
                generator.print("\n");
            }
            for (Integer intValue : field.getFixed32List()) {
                int value2 = intValue.intValue();
                generator.print(((Integer) entry.getKey()).toString());
                generator.print(": ");
                generator.print(String.format(null, "0x%08x", Integer.valueOf(value2)));
                generator.print("\n");
            }
            for (Long longValue2 : field.getFixed64List()) {
                long value3 = longValue2.longValue();
                generator.print(((Integer) entry.getKey()).toString());
                generator.print(": ");
                generator.print(String.format(null, "0x%016x", Long.valueOf(value3)));
                generator.print("\n");
            }
            for (ByteString value4 : field.getLengthDelimitedList()) {
                generator.print(((Integer) entry.getKey()).toString());
                generator.print(": \"");
                generator.print(escapeBytes(value4));
                generator.print("\"\n");
            }
            for (UnknownFieldSet value5 : field.getGroupList()) {
                generator.print(((Integer) entry.getKey()).toString());
                generator.print(" {\n");
                generator.indent();
                printUnknownFields(value5, generator);
                generator.outdent();
                generator.print("}\n");
            }
        }
    }

    private static String unsignedToString(int value) {
        if (value >= 0) {
            return Integer.toString(value);
        }
        return Long.toString(((long) value) & 4294967295L);
    }

    private static String unsignedToString(long value) {
        if (value >= 0) {
            return Long.toString(value);
        }
        return BigInteger.valueOf(Long.MAX_VALUE & value).setBit(63).toString();
    }

    private static final class TextGenerator {
        private boolean atStartOfLine;
        private final StringBuilder indent;
        private Appendable output;

        /* synthetic */ TextGenerator(Appendable x0, AnonymousClass1 x1) {
            this(x0);
        }

        private TextGenerator(Appendable output2) {
            this.atStartOfLine = true;
            this.indent = new StringBuilder();
            this.output = output2;
        }

        public void indent() {
            this.indent.append("  ");
        }

        public void outdent() {
            int length = this.indent.length();
            if (length == 0) {
                throw new IllegalArgumentException(" Outdent() without matching Indent().");
            }
            this.indent.delete(length - 2, length);
        }

        public void print(CharSequence text) throws IOException {
            int size = text.length();
            int pos = 0;
            for (int i = 0; i < size; i++) {
                if (text.charAt(i) == 10) {
                    write(text.subSequence(pos, size), (i - pos) + 1);
                    pos = i + 1;
                    this.atStartOfLine = true;
                }
            }
            write(text.subSequence(pos, size), size - pos);
        }

        private void write(CharSequence data, int size) throws IOException {
            if (size != 0) {
                if (this.atStartOfLine) {
                    this.atStartOfLine = false;
                    this.output.append(this.indent);
                }
                this.output.append(data);
            }
        }
    }

    private static final class Tokenizer {
        private static final Pattern DOUBLE_INFINITY = Pattern.compile("-?inf(inity)?", 2);
        private static final Pattern FLOAT_INFINITY = Pattern.compile("-?inf(inity)?f?", 2);
        private static final Pattern FLOAT_NAN = Pattern.compile("nanf?", 2);
        private static final Pattern TOKEN = Pattern.compile("[a-zA-Z_][0-9a-zA-Z_+-]*+|[0-9+-][0-9a-zA-Z_.+-]*+|\"([^\"\n\\\\]|\\\\.)*+(\"|\\\\?$)|'([^\"\n\\\\]|\\\\.)*+('|\\\\?$)", 8);
        private static final Pattern WHITESPACE = Pattern.compile("(\\s|(#.*$))++", 8);
        private int column;
        private String currentToken;
        private int line;
        private final Matcher matcher;
        private int pos;
        private int previousColumn;
        private int previousLine;
        private final CharSequence text;

        /* synthetic */ Tokenizer(CharSequence x0, AnonymousClass1 x1) {
            this(x0);
        }

        private Tokenizer(CharSequence text2) {
            this.pos = 0;
            this.line = 0;
            this.column = 0;
            this.previousLine = 0;
            this.previousColumn = 0;
            this.text = text2;
            this.matcher = WHITESPACE.matcher(text2);
            skipWhitespace();
            nextToken();
        }

        public boolean atEnd() {
            return this.currentToken.length() == 0;
        }

        public void nextToken() {
            this.previousLine = this.line;
            this.previousColumn = this.column;
            while (this.pos < this.matcher.regionStart()) {
                if (this.text.charAt(this.pos) == 10) {
                    this.line++;
                    this.column = 0;
                } else {
                    this.column++;
                }
                this.pos++;
            }
            if (this.matcher.regionStart() == this.matcher.regionEnd()) {
                this.currentToken = EboxConst.TAB_UPLOADER_TAG;
                return;
            }
            this.matcher.usePattern(TOKEN);
            if (this.matcher.lookingAt()) {
                this.currentToken = this.matcher.group();
                this.matcher.region(this.matcher.end(), this.matcher.regionEnd());
            } else {
                this.currentToken = String.valueOf(this.text.charAt(this.pos));
                this.matcher.region(this.pos + 1, this.matcher.regionEnd());
            }
            skipWhitespace();
        }

        private void skipWhitespace() {
            this.matcher.usePattern(WHITESPACE);
            if (this.matcher.lookingAt()) {
                this.matcher.region(this.matcher.end(), this.matcher.regionEnd());
            }
        }

        public boolean tryConsume(String token) {
            if (!this.currentToken.equals(token)) {
                return false;
            }
            nextToken();
            return true;
        }

        public void consume(String token) throws ParseException {
            if (!tryConsume(token)) {
                throw parseException("Expected \"" + token + "\".");
            }
        }

        public boolean lookingAtInteger() {
            if (this.currentToken.length() == 0) {
                return false;
            }
            char c = this.currentToken.charAt(0);
            return ('0' <= c && c <= '9') || c == '-' || c == '+';
        }

        public String consumeIdentifier() throws ParseException {
            for (int i = 0; i < this.currentToken.length(); i++) {
                char c = this.currentToken.charAt(i);
                if (('a' > c || c > 'z') && (('A' > c || c > 'Z') && !(('0' <= c && c <= '9') || c == '_' || c == '.'))) {
                    throw parseException("Expected identifier.");
                }
            }
            String result = this.currentToken;
            nextToken();
            return result;
        }

        public int consumeInt32() throws ParseException {
            try {
                int result = TextFormat.parseInt32(this.currentToken);
                nextToken();
                return result;
            } catch (NumberFormatException e) {
                throw integerParseException(e);
            }
        }

        public int consumeUInt32() throws ParseException {
            try {
                int result = TextFormat.parseUInt32(this.currentToken);
                nextToken();
                return result;
            } catch (NumberFormatException e) {
                throw integerParseException(e);
            }
        }

        public long consumeInt64() throws ParseException {
            try {
                long result = TextFormat.parseInt64(this.currentToken);
                nextToken();
                return result;
            } catch (NumberFormatException e) {
                throw integerParseException(e);
            }
        }

        public long consumeUInt64() throws ParseException {
            try {
                long result = TextFormat.parseUInt64(this.currentToken);
                nextToken();
                return result;
            } catch (NumberFormatException e) {
                throw integerParseException(e);
            }
        }

        public double consumeDouble() throws ParseException {
            if (DOUBLE_INFINITY.matcher(this.currentToken).matches()) {
                boolean negative = this.currentToken.startsWith("-");
                nextToken();
                return negative ? Double.NEGATIVE_INFINITY : Double.POSITIVE_INFINITY;
            } else if (this.currentToken.equalsIgnoreCase("nan")) {
                nextToken();
                return Double.NaN;
            } else {
                try {
                    double result = Double.parseDouble(this.currentToken);
                    nextToken();
                    return result;
                } catch (NumberFormatException e) {
                    throw floatParseException(e);
                }
            }
        }

        public float consumeFloat() throws ParseException {
            if (FLOAT_INFINITY.matcher(this.currentToken).matches()) {
                boolean negative = this.currentToken.startsWith("-");
                nextToken();
                return negative ? Float.NEGATIVE_INFINITY : Float.POSITIVE_INFINITY;
            } else if (FLOAT_NAN.matcher(this.currentToken).matches()) {
                nextToken();
                return Float.NaN;
            } else {
                try {
                    float result = Float.parseFloat(this.currentToken);
                    nextToken();
                    return result;
                } catch (NumberFormatException e) {
                    throw floatParseException(e);
                }
            }
        }

        public boolean consumeBoolean() throws ParseException {
            if (this.currentToken.equals("true")) {
                nextToken();
                return true;
            } else if (this.currentToken.equals("false")) {
                nextToken();
                return false;
            } else {
                throw parseException("Expected \"true\" or \"false\".");
            }
        }

        public String consumeString() throws ParseException {
            return consumeByteString().toStringUtf8();
        }

        public ByteString consumeByteString() throws ParseException {
            char quote;
            if (this.currentToken.length() > 0) {
                quote = this.currentToken.charAt(0);
            } else {
                quote = 0;
            }
            if (quote != '\"' && quote != '\'') {
                throw parseException("Expected string.");
            } else if (this.currentToken.length() < 2 || this.currentToken.charAt(this.currentToken.length() - 1) != quote) {
                throw parseException("String missing ending quote.");
            } else {
                try {
                    ByteString result = TextFormat.unescapeBytes(this.currentToken.substring(1, this.currentToken.length() - 1));
                    nextToken();
                    return result;
                } catch (InvalidEscapeSequenceException e) {
                    throw parseException(e.getMessage());
                }
            }
        }

        public ParseException parseException(String description) {
            return new ParseException((this.line + 1) + ":" + (this.column + 1) + ": " + description);
        }

        public ParseException parseExceptionPreviousToken(String description) {
            return new ParseException((this.previousLine + 1) + ":" + (this.previousColumn + 1) + ": " + description);
        }

        private ParseException integerParseException(NumberFormatException e) {
            return parseException("Couldn't parse integer: " + e.getMessage());
        }

        private ParseException floatParseException(NumberFormatException e) {
            return parseException("Couldn't parse number: " + e.getMessage());
        }
    }

    public static class ParseException extends IOException {
        private static final long serialVersionUID = 3196188060225107702L;

        public ParseException(String message) {
            super(message);
        }
    }

    public static void merge(Readable input, Message.Builder builder) throws IOException {
        merge(input, ExtensionRegistry.getEmptyRegistry(), builder);
    }

    public static void merge(CharSequence input, Message.Builder builder) throws ParseException {
        merge(input, ExtensionRegistry.getEmptyRegistry(), builder);
    }

    public static void merge(Readable input, ExtensionRegistry extensionRegistry, Message.Builder builder) throws IOException {
        merge(toStringBuilder(input), extensionRegistry, builder);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder}
     arg types: [java.nio.CharBuffer, int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(char[], int, int):java.lang.StringBuilder}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder} */
    private static StringBuilder toStringBuilder(Readable input) throws IOException {
        StringBuilder text = new StringBuilder();
        CharBuffer buffer = CharBuffer.allocate(4096);
        while (true) {
            int n = input.read(buffer);
            if (n == -1) {
                return text;
            }
            buffer.flip();
            text.append((CharSequence) buffer, 0, n);
        }
    }

    public static void merge(CharSequence input, ExtensionRegistry extensionRegistry, Message.Builder builder) throws ParseException {
        Tokenizer tokenizer = new Tokenizer(input, null);
        while (!tokenizer.atEnd()) {
            mergeField(tokenizer, extensionRegistry, builder);
        }
    }

    /* JADX INFO: Multiple debug info for r2v0 java.lang.String: [D('field' com.google.protobuf.Descriptors$FieldDescriptor), D('name' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v8 java.lang.String: [D('lowerName' java.lang.String), D('field' com.google.protobuf.Descriptors$FieldDescriptor)] */
    /* JADX INFO: Multiple debug info for r1v9 'field'  com.google.protobuf.Descriptors$FieldDescriptor: [D('field' com.google.protobuf.Descriptors$FieldDescriptor), D('lowerName' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v3 java.lang.String: [D('number' int), D('id' java.lang.String)] */
    private static void mergeField(Tokenizer tokenizer, ExtensionRegistry extensionRegistry, Message.Builder builder) throws ParseException {
        Descriptors.FieldDescriptor field;
        ExtensionRegistry.ExtensionInfo extension;
        Object obj;
        String endToken;
        Message.Builder subBuilder;
        Descriptors.Descriptor type = builder.getDescriptorForType();
        if (tokenizer.tryConsume("[")) {
            StringBuilder name = new StringBuilder(tokenizer.consumeIdentifier());
            while (tokenizer.tryConsume(".")) {
                name.append('.');
                name.append(tokenizer.consumeIdentifier());
            }
            ExtensionRegistry.ExtensionInfo extension2 = extensionRegistry.findExtensionByName(name.toString());
            if (extension2 == null) {
                throw tokenizer.parseExceptionPreviousToken("Extension \"" + ((Object) name) + "\" not found in the ExtensionRegistry.");
            } else if (extension2.descriptor.getContainingType() != type) {
                throw tokenizer.parseExceptionPreviousToken("Extension \"" + ((Object) name) + "\" does not extend message type \"" + type.getFullName() + "\".");
            } else {
                tokenizer.consume("]");
                field = extension2.descriptor;
                extension = extension2;
            }
        } else {
            String name2 = tokenizer.consumeIdentifier();
            Descriptors.FieldDescriptor field2 = type.findFieldByName(name2);
            if (!(field2 != null || (field2 = type.findFieldByName(name2.toLowerCase(Locale.US))) == null || field2.getType() == Descriptors.FieldDescriptor.Type.GROUP)) {
                field2 = null;
            }
            if (field2 != null && field2.getType() == Descriptors.FieldDescriptor.Type.GROUP && !field2.getMessageType().getName().equals(name2)) {
                field2 = null;
            }
            if (field2 == null) {
                throw tokenizer.parseExceptionPreviousToken("Message type \"" + type.getFullName() + "\" has no field named \"" + name2 + "\".");
            }
            field = field2;
            extension = null;
        }
        if (field.getJavaType() != Descriptors.FieldDescriptor.JavaType.MESSAGE) {
            tokenizer.consume(":");
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$Descriptors$FieldDescriptor$Type[field.getType().ordinal()]) {
                case 1:
                case 3:
                case 5:
                    obj = Integer.valueOf(tokenizer.consumeInt32());
                    break;
                case 2:
                case 4:
                case 6:
                    obj = Long.valueOf(tokenizer.consumeInt64());
                    break;
                case 7:
                    obj = Float.valueOf(tokenizer.consumeFloat());
                    break;
                case 8:
                    obj = Double.valueOf(tokenizer.consumeDouble());
                    break;
                case 9:
                    obj = Boolean.valueOf(tokenizer.consumeBoolean());
                    break;
                case 10:
                case 11:
                    obj = Integer.valueOf(tokenizer.consumeUInt32());
                    break;
                case 12:
                case 13:
                    obj = Long.valueOf(tokenizer.consumeUInt64());
                    break;
                case 14:
                    obj = tokenizer.consumeString();
                    break;
                case 15:
                    obj = tokenizer.consumeByteString();
                    break;
                case 16:
                    Descriptors.EnumDescriptor enumType = field.getEnumType();
                    if (!tokenizer.lookingAtInteger()) {
                        String id = tokenizer.consumeIdentifier();
                        Descriptors.EnumValueDescriptor value = enumType.findValueByName(id);
                        if (value != null) {
                            obj = value;
                            break;
                        } else {
                            throw tokenizer.parseExceptionPreviousToken("Enum type \"" + enumType.getFullName() + "\" has no value named \"" + id + "\".");
                        }
                    } else {
                        int number = tokenizer.consumeInt32();
                        Descriptors.EnumValueDescriptor value2 = enumType.findValueByNumber(number);
                        if (value2 != null) {
                            obj = value2;
                            break;
                        } else {
                            throw tokenizer.parseExceptionPreviousToken("Enum type \"" + enumType.getFullName() + "\" has no value with number " + number + '.');
                        }
                    }
                case EboxConst.SIGN /*17*/:
                case 18:
                    throw new RuntimeException("Can't get here.");
                default:
                    obj = null;
                    break;
            }
        } else {
            tokenizer.tryConsume(":");
            if (tokenizer.tryConsume("<")) {
                endToken = ">";
            } else {
                tokenizer.consume("{");
                endToken = "}";
            }
            if (extension == null) {
                subBuilder = builder.newBuilderForField(field);
            } else {
                subBuilder = extension.defaultInstance.newBuilderForType();
            }
            while (!tokenizer.tryConsume(endToken)) {
                if (tokenizer.atEnd()) {
                    throw tokenizer.parseException("Expected \"" + endToken + "\".");
                }
                mergeField(tokenizer, extensionRegistry, subBuilder);
            }
            obj = subBuilder.build();
        }
        if (field.isRepeated()) {
            builder.addRepeatedField(field, obj);
        } else {
            builder.setField(field, obj);
        }
    }

    static String escapeBytes(ByteString input) {
        StringBuilder builder = new StringBuilder(input.size());
        for (int i = 0; i < input.size(); i++) {
            byte b = input.byteAt(i);
            switch (b) {
                case 7:
                    builder.append("\\a");
                    break;
                case 8:
                    builder.append("\\b");
                    break;
                case 9:
                    builder.append("\\t");
                    break;
                case 10:
                    builder.append("\\n");
                    break;
                case 11:
                    builder.append("\\v");
                    break;
                case 12:
                    builder.append("\\f");
                    break;
                case 13:
                    builder.append("\\r");
                    break;
                case 34:
                    builder.append("\\\"");
                    break;
                case 39:
                    builder.append("\\'");
                    break;
                case 92:
                    builder.append("\\\\");
                    break;
                default:
                    if (b < 32) {
                        builder.append('\\');
                        builder.append((char) (((b >>> 6) & 3) + 48));
                        builder.append((char) (((b >>> 3) & 7) + 48));
                        builder.append((char) ((b & 7) + 48));
                        break;
                    } else {
                        builder.append((char) b);
                        break;
                    }
            }
        }
        return builder.toString();
    }

    static ByteString unescapeBytes(CharSequence input) throws InvalidEscapeSequenceException {
        int i;
        int pos;
        byte[] result = new byte[input.length()];
        int pos2 = 0;
        int i2 = 0;
        while (i < input.length()) {
            char c = input.charAt(i);
            if (c != '\\') {
                pos = pos2 + 1;
                result[pos2] = (byte) c;
            } else if (i + 1 < input.length()) {
                i++;
                char c2 = input.charAt(i);
                if (isOctal(c2)) {
                    int code = digitValue(c2);
                    if (i + 1 < input.length() && isOctal(input.charAt(i + 1))) {
                        i++;
                        code = (code * 8) + digitValue(input.charAt(i));
                    }
                    if (i + 1 < input.length() && isOctal(input.charAt(i + 1))) {
                        i++;
                        code = (code * 8) + digitValue(input.charAt(i));
                    }
                    pos = pos2 + 1;
                    result[pos2] = (byte) code;
                } else {
                    switch (c2) {
                        case '\"':
                            pos = pos2 + 1;
                            result[pos2] = 34;
                            continue;
                        case '\'':
                            pos = pos2 + 1;
                            result[pos2] = 39;
                            continue;
                        case '\\':
                            pos = pos2 + 1;
                            result[pos2] = 92;
                            continue;
                        case 'a':
                            pos = pos2 + 1;
                            result[pos2] = 7;
                            continue;
                        case 'b':
                            pos = pos2 + 1;
                            result[pos2] = 8;
                            continue;
                        case 'f':
                            pos = pos2 + 1;
                            result[pos2] = 12;
                            continue;
                        case 'n':
                            pos = pos2 + 1;
                            result[pos2] = 10;
                            continue;
                        case 'r':
                            pos = pos2 + 1;
                            result[pos2] = 13;
                            continue;
                        case 't':
                            pos = pos2 + 1;
                            result[pos2] = 9;
                            continue;
                        case 'v':
                            pos = pos2 + 1;
                            result[pos2] = 11;
                            continue;
                        case 'x':
                            if (i + 1 >= input.length() || !isHex(input.charAt(i + 1))) {
                                throw new InvalidEscapeSequenceException("Invalid escape sequence: '\\x' with no digits");
                            }
                            i++;
                            int code2 = digitValue(input.charAt(i));
                            if (i + 1 < input.length() && isHex(input.charAt(i + 1))) {
                                i++;
                                code2 = (code2 * 16) + digitValue(input.charAt(i));
                            }
                            pos = pos2 + 1;
                            result[pos2] = (byte) code2;
                            continue;
                        default:
                            throw new InvalidEscapeSequenceException("Invalid escape sequence: '\\" + c2 + '\'');
                    }
                }
            } else {
                throw new InvalidEscapeSequenceException("Invalid escape sequence: '\\' at end of string.");
            }
            pos2 = pos;
            i2 = i + 1;
        }
        return ByteString.copyFrom(result, 0, pos2);
    }

    static class InvalidEscapeSequenceException extends IOException {
        private static final long serialVersionUID = -8164033650142593304L;

        InvalidEscapeSequenceException(String description) {
            super(description);
        }
    }

    static String escapeText(String input) {
        return escapeBytes(ByteString.copyFromUtf8(input));
    }

    static String unescapeText(String input) throws InvalidEscapeSequenceException {
        return unescapeBytes(input).toStringUtf8();
    }

    private static boolean isOctal(char c) {
        return '0' <= c && c <= '7';
    }

    private static boolean isHex(char c) {
        return ('0' <= c && c <= '9') || ('a' <= c && c <= 'f') || ('A' <= c && c <= 'F');
    }

    private static int digitValue(char c) {
        if ('0' <= c && c <= '9') {
            return c - '0';
        }
        if ('a' > c || c > 'z') {
            return (c - 'A') + 10;
        }
        return (c - 'a') + 10;
    }

    static int parseInt32(String text) throws NumberFormatException {
        return (int) parseInteger(text, true, false);
    }

    static int parseUInt32(String text) throws NumberFormatException {
        return (int) parseInteger(text, false, false);
    }

    static long parseInt64(String text) throws NumberFormatException {
        return parseInteger(text, true, true);
    }

    static long parseUInt64(String text) throws NumberFormatException {
        return parseInteger(text, false, true);
    }

    private static long parseInteger(String text, boolean isSigned, boolean isLong) throws NumberFormatException {
        int pos = 0;
        boolean negative = false;
        if (text.startsWith("-", 0)) {
            if (!isSigned) {
                throw new NumberFormatException("Number must be positive: " + text);
            }
            pos = 0 + 1;
            negative = true;
        }
        int radix = 10;
        if (text.startsWith("0x", pos)) {
            pos += 2;
            radix = 16;
        } else if (text.startsWith(EboxConst.FILE, pos)) {
            radix = 8;
        }
        String numberText = text.substring(pos);
        if (numberText.length() < 16) {
            long result = Long.parseLong(numberText, radix);
            if (negative) {
                result = -result;
            }
            if (isLong) {
                return result;
            }
            if (isSigned) {
                if (result <= 2147483647L && result >= -2147483648L) {
                    return result;
                }
                throw new NumberFormatException("Number out of range for 32-bit signed integer: " + text);
            } else if (result < 4294967296L && result >= 0) {
                return result;
            } else {
                throw new NumberFormatException("Number out of range for 32-bit unsigned integer: " + text);
            }
        } else {
            BigInteger bigValue = new BigInteger(numberText, radix);
            if (negative) {
                bigValue = bigValue.negate();
            }
            if (!isLong) {
                if (isSigned) {
                    if (bigValue.bitLength() > 31) {
                        throw new NumberFormatException("Number out of range for 32-bit signed integer: " + text);
                    }
                } else if (bigValue.bitLength() > 32) {
                    throw new NumberFormatException("Number out of range for 32-bit unsigned integer: " + text);
                }
            } else if (isSigned) {
                if (bigValue.bitLength() > 63) {
                    throw new NumberFormatException("Number out of range for 64-bit signed integer: " + text);
                }
            } else if (bigValue.bitLength() > 64) {
                throw new NumberFormatException("Number out of range for 64-bit unsigned integer: " + text);
            }
            return bigValue.longValue();
        }
    }
}
