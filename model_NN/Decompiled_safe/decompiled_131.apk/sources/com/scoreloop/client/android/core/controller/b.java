package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Device;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.util.Logger;
import java.nio.channels.IllegalSelectorException;
import org.json.JSONException;
import org.json.JSONObject;

class b extends RequestController {
    private Request b;
    private Device c;
    private Request d;

    private static class a extends Request {
        private final Device a;
        private final C0000b b;

        public a(RequestCompletionCallback requestCompletionCallback, Device device, C0000b bVar) {
            super(requestCompletionCallback);
            this.a = device;
            this.b = bVar;
        }

        public String a() {
            return "/service/device";
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                switch (this.b) {
                    case VERIFY:
                        jSONObject.put("uuid", this.a.h());
                        jSONObject.put("system", this.a.c());
                        break;
                    case CREATE:
                        jSONObject.put(Device.a, this.a.d());
                        break;
                    case RESET:
                        JSONObject jSONObject2 = new JSONObject();
                        jSONObject2.put("uuid", this.a.h());
                        jSONObject2.put("id", this.a.getIdentifier());
                        jSONObject2.put("system", this.a.c());
                        jSONObject2.put("state", "freed");
                        jSONObject.put(Device.a, jSONObject2);
                        break;
                    case UPDATE:
                        jSONObject.put(Device.a, this.a.d());
                        break;
                    default:
                        throw new IllegalSelectorException();
                }
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid device data", e);
            }
        }

        public RequestMethod c() {
            switch (this.b) {
                case VERIFY:
                    return RequestMethod.GET;
                case CREATE:
                    return RequestMethod.POST;
                case RESET:
                case UPDATE:
                    return RequestMethod.PUT;
                default:
                    throw new IllegalSelectorException();
            }
        }

        public C0000b d() {
            return this.b;
        }
    }

    /* renamed from: com.scoreloop.client.android.core.controller.b$b  reason: collision with other inner class name */
    enum C0000b {
        CREATE(21),
        RESET(22),
        UPDATE(23),
        VERIFY(20);
        
        private final int a;

        private C0000b(int i) {
            this.a = i;
        }
    }

    b(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver, false);
        this.c = session.a();
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        int f = response.f();
        JSONObject optJSONObject = response.e().optJSONObject(Device.a);
        if (((a) request).d() == C0000b.VERIFY) {
            if (f == 404) {
                return false;
            }
            if (this.b != null) {
                this.b.o();
            }
            this.b = null;
        }
        if ((f == 200 || f == 201) && optJSONObject != null) {
            this.c.b(optJSONObject.getString("id"));
            if ("freed".equalsIgnoreCase(optJSONObject.optString("state"))) {
                this.c.a(Device.State.FREED);
            } else {
                this.c.a(f == 200 ? Device.State.VERIFIED : Device.State.CREATED);
            }
            return true;
        }
        throw new Exception("Request failed with status: " + f);
    }

    /* access modifiers changed from: protected */
    public void a_() {
        Logger.a("DeviceController", "reset()");
        super.a_();
        if (this.d != null) {
            if (!this.d.m()) {
                Logger.a("DeviceController", "reset() - canceling verify request");
                f().b().b(this.d);
            }
            this.d = null;
        }
    }

    /* access modifiers changed from: package-private */
    public Device b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        a_();
        this.d = new a(e(), b(), C0000b.VERIFY);
        a(this.d);
        this.b = new a(e(), b(), C0000b.CREATE);
        a(this.b);
    }
}
