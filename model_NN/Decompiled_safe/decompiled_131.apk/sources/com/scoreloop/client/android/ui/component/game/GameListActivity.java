package com.scoreloop.client.android.ui.component.game;

import android.os.Bundle;
import android.widget.ListView;
import com.scoreloop.client.android.core.controller.GamesController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.ui.component.base.CaptionListItem;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.EmptyListItem;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import com.scoreloop.client.android.ui.framework.PagingDirection;
import com.scoreloop.client.android.ui.framework.PagingListAdapter;
import com.skyd.bestpuzzle.n1666.R;
import java.util.List;

public class GameListActivity extends ComponentListActivity<GameListItem> implements RequestControllerObserver, PagingListAdapter.OnListItemClickListener<GameListItem> {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$scoreloop$client$android$ui$framework$PagingDirection;
    private GamesController _gamesController;
    private int _mode;
    /* access modifiers changed from: private */
    public PagingDirection _pagingDirection;

    static /* synthetic */ int[] $SWITCH_TABLE$com$scoreloop$client$android$ui$framework$PagingDirection() {
        int[] iArr = $SWITCH_TABLE$com$scoreloop$client$android$ui$framework$PagingDirection;
        if (iArr == null) {
            iArr = new int[PagingDirection.values().length];
            try {
                iArr[PagingDirection.PAGE_TO_NEXT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[PagingDirection.PAGE_TO_OWN.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[PagingDirection.PAGE_TO_PREV.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[PagingDirection.PAGE_TO_RECENT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[PagingDirection.PAGE_TO_TOP.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            $SWITCH_TABLE$com$scoreloop$client$android$ui$framework$PagingDirection = iArr;
        }
        return iArr;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new PagingListAdapter(this, 1));
        this._mode = ((Integer) getActivityArguments().getValue("mode")).intValue();
        this._gamesController = new GamesController(this);
        this._gamesController.setRangeLength(Constant.getOptimalRangeLength(getListView(), new StandardListItem(this, null, "title", "subtitle", null)));
        setNeedsRefresh(PagingDirection.PAGE_TO_TOP);
    }

    private void setNeedsRefresh(PagingDirection pagingDirection) {
        this._pagingDirection = pagingDirection;
        setNeedsRefresh();
    }

    private PagingListAdapter<GameListItem> getPagingListAdapter() {
        return (PagingListAdapter) getBaseListAdapter();
    }

    private void onGames(List<Game> games) {
        int id;
        final PagingListAdapter<GameListItem> adapter = getPagingListAdapter();
        adapter.clear();
        switch (this._mode) {
            case 0:
                if (!isSessionUser()) {
                    id = R.string.sl_games;
                    break;
                } else {
                    id = R.string.sl_my_games;
                    break;
                }
            case 1:
                id = R.string.sl_popular_games;
                break;
            case 2:
                id = R.string.sl_new_games;
                break;
            case 3:
                id = R.string.sl_friends_games;
                break;
            default:
                id = R.string.sl_games;
                break;
        }
        adapter.add(new CaptionListItem(this, null, getString(id)));
        for (Game game : games) {
            adapter.add(new GameListItem(this, getResources().getDrawable(R.drawable.sl_icon_games_loading), game));
        }
        if (games.size() == 0) {
            adapter.add(new EmptyListItem(this, getString(R.string.sl_no_games)));
            return;
        }
        boolean showTop = this._gamesController.hasPreviousRange();
        adapter.addPagingItems(showTop, showTop, this._gamesController.hasNextRange());
        final ListView listView = getListView();
        listView.post(new Runnable() {
            public void run() {
                if (GameListActivity.this._pagingDirection == PagingDirection.PAGE_TO_TOP) {
                    listView.setSelection(0);
                } else if (GameListActivity.this._pagingDirection == PagingDirection.PAGE_TO_NEXT) {
                    listView.setSelection(adapter.getFirstContentPosition());
                } else if (GameListActivity.this._pagingDirection == PagingDirection.PAGE_TO_PREV) {
                    listView.setSelectionFromTop(adapter.getLastContentPosition() + 1, listView.getHeight());
                }
            }
        });
    }

    public void onListItemClick(GameListItem item) {
        if (item.getType() == 12) {
            display(getFactory().createGameDetailScreenDescription((Game) item.getTarget()));
        }
    }

    public void onRefresh(int flags) {
        showSpinnerFor(this._gamesController);
        switch ($SWITCH_TABLE$com$scoreloop$client$android$ui$framework$PagingDirection()[this._pagingDirection.ordinal()]) {
            case 1:
                this._gamesController.loadNextRange();
                return;
            case 2:
            case 4:
            default:
                return;
            case 3:
                this._gamesController.loadPreviousRange();
                return;
            case 5:
                switch (this._mode) {
                    case 0:
                        onRefreshUser();
                        return;
                    case 1:
                        onRefreshPopular();
                        return;
                    case 2:
                        onRefreshNew();
                        return;
                    case 3:
                        onRefreshBuddies();
                        return;
                    default:
                        return;
                }
        }
    }

    private void onRefreshBuddies() {
        showSpinnerFor(this._gamesController);
        this._gamesController.loadRangeForBuddies();
    }

    private void onRefreshNew() {
        showSpinnerFor(this._gamesController);
        this._gamesController.loadRangeForNew();
    }

    private void onRefreshPopular() {
        showSpinnerFor(this._gamesController);
        this._gamesController.loadRangeForPopular();
    }

    private void onRefreshUser() {
        showSpinnerFor(this._gamesController);
        this._gamesController.loadRangeForUser(getUser());
    }

    public void requestControllerDidReceiveResponseSafe(RequestController aRequestController) {
        if (aRequestController == this._gamesController) {
            onGames(this._gamesController.getGames());
        }
    }

    public void onPagingListItemClick(PagingDirection pagingDirection) {
        setNeedsRefresh(pagingDirection);
    }
}
