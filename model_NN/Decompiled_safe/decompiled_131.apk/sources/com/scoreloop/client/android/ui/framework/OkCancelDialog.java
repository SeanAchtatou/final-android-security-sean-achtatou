package com.scoreloop.client.android.ui.framework;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.skyd.bestpuzzle.n1666.R;

public class OkCancelDialog extends BaseDialog {
    public static final int BUTTON_CANCEL = 1;
    public static final int BUTTON_OK = 0;
    private String title;

    public OkCancelDialog(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public int getContentViewLayoutId() {
        return R.layout.sl_dialog_ok_cancel;
    }

    public void setTitle(String title2) {
        this.title = title2;
        refresh();
    }

    public void onClick(View view) {
        if (this._listener != null) {
            if (view.getId() == R.id.sl_button_ok) {
                this._listener.onAction(this, 0);
            } else if (view.getId() == R.id.sl_button_cancel) {
                this._listener.onAction(this, 1);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((Button) findViewById(R.id.sl_button_ok)).setOnClickListener(this);
        ((Button) findViewById(R.id.sl_button_cancel)).setOnClickListener(this);
        refresh();
    }

    private void refresh() {
        TextView tvTitle = (TextView) findViewById(R.id.sl_title);
        if (this.title != null) {
            tvTitle.setText(this.title);
            tvTitle.setVisibility(0);
            return;
        }
        tvTitle.setVisibility(4);
    }
}
