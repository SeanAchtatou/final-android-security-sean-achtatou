package com.scoreloop.client.android.ui.framework;

import android.app.ActivityGroup;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ViewSwitcher;
import com.skyd.bestpuzzle.n1666.R;

public abstract class ActivityHelper {
    public static final int ANIM_NEXT = 1;
    public static final int ANIM_NONE = 0;
    public static final int ANIM_PREVIOUS = 2;

    public static void startLocalActivity(ActivityGroup activityGroup, Intent intent, String identifier, int regionId, int anim) {
        View paneView = activityGroup.getLocalActivityManager().startActivity(identifier, intent).getDecorView();
        ViewParent parent = paneView.getParent();
        if (parent == null || !(parent instanceof ViewGroup)) {
            ViewGroup region = (ViewGroup) activityGroup.findViewById(regionId);
            if (anim == 0 || !(region instanceof ViewSwitcher)) {
                region.removeAllViews();
                region.addView(paneView, new ViewGroup.LayoutParams(-1, -1));
                return;
            }
            ViewSwitcher animator = (ViewSwitcher) region;
            if (anim == 1) {
                animator.setInAnimation(activityGroup, R.anim.sl_next_in);
                animator.setOutAnimation(activityGroup, R.anim.sl_next_out);
            } else {
                animator.setInAnimation(activityGroup, R.anim.sl_previous_in);
                animator.setOutAnimation(activityGroup, R.anim.sl_previous_out);
            }
            int numChilds = animator.getChildCount();
            if (numChilds == 0) {
                animator.addView(paneView, 0, new ViewGroup.LayoutParams(-1, -1));
            } else if (numChilds == 1) {
                animator.addView(paneView, 1, new ViewGroup.LayoutParams(-1, -1));
                animator.showNext();
            } else {
                animator.removeViewAt(0);
                animator.addView(paneView, 1, new ViewGroup.LayoutParams(-1, -1));
                animator.showNext();
            }
        } else {
            throw new IllegalStateException("should not happen - currently we don't recycle activities");
        }
    }
}
