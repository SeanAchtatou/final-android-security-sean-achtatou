package com.scoreloop.client.android.ui.component.challenge;

import android.app.Dialog;
import android.os.Bundle;
import com.scoreloop.client.android.ui.component.base.CaptionListItem;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.framework.BaseDialog;
import com.scoreloop.client.android.ui.framework.BaseListAdapter;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.scoreloop.client.android.ui.framework.TextButtonDialog;
import com.skyd.bestpuzzle.n1666.R;

public abstract class ChallengeActionListActivity extends ComponentListActivity<BaseListItem> implements BaseDialog.OnActionListener {
    /* access modifiers changed from: package-private */
    public abstract CaptionListItem getCaptionListItem();

    /* access modifiers changed from: package-private */
    public abstract ChallengeControlsListItem getChallengeControlsListItem();

    /* access modifiers changed from: package-private */
    public abstract ChallengeParticipantsListItem getChallengeParticipantsListItem();

    /* access modifiers changed from: package-private */
    public abstract ChallengeSettingsListItem getChallengeStakeAndModeListItem();

    /* access modifiers changed from: package-private */
    public void initAdapter() {
        BaseListAdapter<BaseListItem> adapter = new BaseListAdapter<>(this);
        adapter.add(getCaptionListItem());
        adapter.add(getChallengeParticipantsListItem());
        adapter.add(getChallengeStakeAndModeListItem());
        adapter.add(getChallengeControlsListItem());
        setListAdapter(adapter);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 5:
                TextButtonDialog dialogOngoing = new TextButtonDialog(this);
                dialogOngoing.setText(getResources().getString(R.string.sl_error_message_challenge_ongoing));
                dialogOngoing.setOnActionListener(this);
                return dialogOngoing;
            case 6:
                TextButtonDialog dialogGameNotReady = new TextButtonDialog(this);
                dialogGameNotReady.setText(getResources().getString(R.string.sl_error_message_challenge_game_not_ready));
                dialogGameNotReady.setOnActionListener(this);
                return dialogGameNotReady;
            default:
                return super.onCreateDialog(id);
        }
    }

    /* access modifiers changed from: protected */
    public boolean challengeGamePlayAllowed() {
        if (getManager().isChallengeOngoing()) {
            showDialogSafe(5);
            return false;
        } else if (getManager().canStartGamePlay()) {
            return true;
        } else {
            showDialogSafe(6);
            return false;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
