package com.scoreloop.client.android.ui.component.profile;

import android.graphics.drawable.Drawable;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import com.skyd.bestpuzzle.n1666.R;

public class ProfilePictureListItem extends StandardListItem<Void> {
    public ProfilePictureListItem(ComponentActivity context, Drawable drawable, String title) {
        super(context, drawable, title, null, null);
    }

    /* access modifiers changed from: protected */
    public int getIconId() {
        return R.id.sl_list_item_main_icon;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_main;
    }

    /* access modifiers changed from: protected */
    public int getSubTitleId() {
        return R.id.sl_list_item_main_subtitle;
    }

    /* access modifiers changed from: protected */
    public int getTitleId() {
        return R.id.sl_list_item_main_title;
    }

    public int getType() {
        return 18;
    }
}
