package com.scoreloop.client.android.ui.component.user;

import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import com.skyd.bestpuzzle.n1666.R;

public class UserAddBuddiesListItem extends StandardListItem<Void> {
    public UserAddBuddiesListItem(ComponentActivity activity) {
        super(activity, null, activity.getString(R.string.sl_add_friends), null, null);
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_user_add_buddies;
    }

    public int getType() {
        return 24;
    }
}
