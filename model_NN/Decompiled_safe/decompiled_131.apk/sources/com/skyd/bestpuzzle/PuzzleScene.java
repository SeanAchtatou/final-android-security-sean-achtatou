package com.skyd.bestpuzzle;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.widget.Toast;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.skyd.bestpuzzle.n1666.R;
import com.skyd.core.android.game.GameImageSpirit;
import com.skyd.core.android.game.GameMaster;
import com.skyd.core.android.game.GameObject;
import com.skyd.core.android.game.GameScene;
import com.skyd.core.android.game.GameSpirit;
import com.skyd.core.draw.DrawHelper;
import com.skyd.core.vector.Vector2DF;
import com.skyd.core.vector.VectorRect2DF;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class PuzzleScene extends GameScene {
    public Controller Controller;
    public Desktop Desktop;
    boolean IsDrawingPuzzle;
    boolean IsDrawnSelectRect;
    public GameImageSpirit MoveButton;
    public UI MoveButtonSlot;
    public OriginalImage OriginalImage;
    public long PastTime;
    public long StartTime;
    public UI SubmitScoreButton;
    /* access modifiers changed from: private */
    public RectF SubmitScoreButtonRect;
    public GameSpirit Time;
    public UI ViewFullButton;
    /* access modifiers changed from: private */
    public RectF ViewFullButtonRect;
    public UI ZoomInButton;
    public UI ZoomOutButton;
    private boolean _IsFinished = false;
    private ArrayList<OnIsFinishedChangedListener> _IsFinishedChangedListenerList = null;
    long savetime = Long.MIN_VALUE;
    RectF selectRect;

    public interface OnIsFinishedChangedListener {
        void OnIsFinishedChangedEvent(Object obj, boolean z);
    }

    public PuzzleScene() {
        setMaxDrawCacheLayer(25.0f);
        this.StartTime = new Date().getTime();
        loadGameState();
    }

    public void loadGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        this.PastTime = c.getPastTime().longValue();
        this._IsFinished = c.getIsFinished().booleanValue();
    }

    public void saveGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        if (getIsFinished()) {
            c.setPastTime(Long.valueOf(this.PastTime));
        } else {
            c.setPastTime(Long.valueOf(this.PastTime + (new Date().getTime() - this.StartTime)));
        }
        c.setIsFinished(Boolean.valueOf(getIsFinished()));
    }

    public void setFinished() {
        if (!getIsFinished()) {
            this.SubmitScoreButton.show();
        }
        setIsFinished(true);
        this.PastTime += new Date().getTime() - this.StartTime;
    }

    public boolean getIsFinished() {
        return this._IsFinished;
    }

    private void setIsFinished(boolean value) {
        onIsFinishedChanged(value);
        this._IsFinished = value;
    }

    private void setIsFinishedToDefault() {
        setIsFinished(false);
    }

    public boolean addOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null) {
            this._IsFinishedChangedListenerList = new ArrayList<>();
        } else if (this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null || !this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnIsFinishedChangedListeners() {
        if (this._IsFinishedChangedListenerList != null) {
            this._IsFinishedChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onIsFinishedChanged(boolean newValue) {
        if (this._IsFinishedChangedListenerList != null) {
            Iterator<OnIsFinishedChangedListener> it = this._IsFinishedChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnIsFinishedChangedEvent(this, newValue);
            }
        }
    }

    public int getTargetCacheID() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void drawChilds(Canvas c, Rect drawArea) {
        this.IsDrawingPuzzle = false;
        this.IsDrawnSelectRect = false;
        super.drawChilds(c, drawArea);
    }

    /* access modifiers changed from: protected */
    public boolean onDrawingChild(GameObject child, Canvas c, Rect drawArea) {
        float lvl = ((GameSpirit) child).getLevel();
        if (!this.IsDrawingPuzzle && lvl <= 10.0f) {
            this.IsDrawingPuzzle = true;
            c.setMatrix(this.Desktop.getMatrix());
        } else if (lvl > 10.0f && this.IsDrawingPuzzle) {
            this.IsDrawingPuzzle = false;
            c.setMatrix(new Matrix());
        }
        if (lvl > 25.0f && this.selectRect != null && !this.IsDrawnSelectRect) {
            this.IsDrawnSelectRect = true;
            Paint p = new Paint();
            p.setARGB(100, 180, 225, 255);
            c.drawRect(this.selectRect, p);
        }
        return super.onDrawingChild(child, c, drawArea);
    }

    public void load(Context c) {
        loadOriginalImage(c);
        loadDesktop(c);
        loadInterface(c);
        loadPuzzle(c);
        loadPuzzleState();
        this.Desktop.updateCurrentObserveAreaRectF();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF
     arg types: [int, int, float, float, int]
     candidates:
      com.skyd.core.draw.DrawHelper.calculateScaleSize(double, double, double, double, boolean):com.skyd.core.vector.Vector2D
      com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF */
    public void loadOriginalImage(Context c) {
        this.OriginalImage = new OriginalImage() {
            /* access modifiers changed from: protected */
            public boolean onDrawing(Canvas c, Rect drawArea) {
                c.drawARGB(180, 0, 0, 0);
                return super.onDrawing(c, drawArea);
            }
        };
        this.OriginalImage.setLevel(50.0f);
        this.OriginalImage.setName("OriginalImage");
        this.OriginalImage.getSize().resetWith(DrawHelper.calculateScaleSize(384.0f, 240.0f, (float) (GameMaster.getScreenWidth() - 40), (float) (GameMaster.getScreenHeight() - 40), true));
        this.OriginalImage.setTotalSeparateColumn(8);
        this.OriginalImage.setTotalSeparateRow(5);
        this.OriginalImage.getOriginalSize().reset(800.0f, 500.0f);
        this.OriginalImage.hide();
        this.OriginalImage.setIsUseAbsolutePosition(true);
        this.OriginalImage.setIsUseAbsoluteSize(true);
        this.OriginalImage.getImage().setIsUseAbsolutePosition(true);
        this.OriginalImage.getImage().setIsUseAbsoluteSize(true);
        this.OriginalImage.getPosition().reset(((float) (GameMaster.getScreenWidth() / 2)) - (this.OriginalImage.getSize().getX() / 2.0f), ((float) (GameMaster.getScreenHeight() / 2)) - (this.OriginalImage.getSize().getY() / 2.0f));
        getSpiritList().add(this.OriginalImage);
    }

    public void loadDesktop(Context c) {
        this.Desktop = new Desktop();
        this.Desktop.setLevel(-9999.0f);
        this.Desktop.setName("Desktop");
        this.Desktop.getSize().reset(1600.0f, 1000.0f);
        this.Desktop.getCurrentObservePosition().reset(800.0f, 500.0f);
        this.Desktop.setOriginalImage(this.OriginalImage);
        this.Desktop.setIsUseAbsolutePosition(true);
        this.Desktop.setIsUseAbsoluteSize(true);
        this.Desktop.ColorAPaint = new Paint();
        this.Desktop.ColorAPaint.setColor(Color.argb(255, 32, 30, 43));
        this.Desktop.ColorBPaint = new Paint();
        this.Desktop.ColorBPaint.setColor(Color.argb(255, 26, 24, 37));
        getSpiritList().add(this.Desktop);
    }

    public void loadPuzzle(Context c) {
        this.Controller = new Controller();
        this.Controller.setLevel(10.0f);
        this.Controller.setName("Controller");
        this.Controller.setDesktop(this.Desktop);
        this.Controller.setIsUseAbsolutePosition(true);
        this.Controller.setIsUseAbsoluteSize(true);
        getSpiritList().add(this.Controller);
        Puzzle.getRealitySize().reset(100.0f, 100.0f);
        Puzzle.getMaxRadius().reset(76.66666f, 76.66666f);
        c525130452(c);
        c1909320204(c);
        c1297624591(c);
        c2006193649(c);
        c1231949303(c);
        c768123798(c);
        c1740486235(c);
        c995751331(c);
        c1508861472(c);
        c752353802(c);
        c180988013(c);
        c1328845637(c);
        c1364815339(c);
        c1166173088(c);
        c1541020853(c);
        c485145566(c);
        c782023876(c);
        c496912582(c);
        c517696563(c);
        c2053814480(c);
        c1576919550(c);
        c1900623420(c);
        c737660695(c);
        c240652433(c);
        c1149214648(c);
        c286405353(c);
        c1627103793(c);
        c255769629(c);
        c459579215(c);
        c2042882337(c);
        c1917798868(c);
        c686898821(c);
        c1019488942(c);
        c285089697(c);
        c1462457296(c);
        c1321273556(c);
        c765706416(c);
        c19475550(c);
        c177713629(c);
        c1539547768(c);
    }

    public void loadInterface(Context c) {
        this.MoveButton = new GameImageSpirit();
        this.MoveButton.setLevel(22.0f);
        this.MoveButton.getImage().loadImageFromResource(c, R.drawable.movebutton);
        this.MoveButton.getSize().reset(155.0f, 155.0f);
        this.MoveButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButton);
        final Vector2DF mvds = this.MoveButton.getDisplaySize();
        mvds.scale(0.5f);
        this.MoveButton.getPosition().reset(5.0f + mvds.getX(), ((float) (GameMaster.getScreenHeight() - 5)) - mvds.getY());
        this.MoveButton.getPositionOffset().resetWith(mvds.negateNew());
        this.MoveButtonSlot = new UI() {
            Vector2DF movevector;
            boolean needmove;

            public void executive(Vector2DF point) {
                this.movevector = point.minusNew(getPosition()).restrainLength(mvds.getX() * 0.4f);
                this.needmove = true;
            }

            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.needmove) {
                    PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition().plusNew(this.movevector));
                    PuzzleScene.this.Desktop.getCurrentObservePosition().plus(this.movevector);
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < mvds.getX() + 5.0f;
            }

            public void reset() {
                this.needmove = false;
                PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition());
                PuzzleScene.this.refreshDrawCacheBitmap();
            }
        };
        this.MoveButtonSlot.setLevel(21.0f);
        this.MoveButtonSlot.getImage().loadImageFromResource(c, R.drawable.movebuttonslot);
        this.MoveButtonSlot.getSize().reset(155.0f, 155.0f);
        this.MoveButtonSlot.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButtonSlot);
        this.MoveButtonSlot.getPosition().resetWith(this.MoveButton.getPosition());
        this.MoveButtonSlot.getPositionOffset().resetWith(this.MoveButton.getPositionOffset());
        this.ZoomInButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.min(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.min(double, double):double}
              ClspMth{java.lang.Math.min(long, long):long}
              ClspMth{java.lang.Math.min(int, int):int}
              ClspMth{java.lang.Math.min(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.min(1.3f, PuzzleScene.this.Desktop.getZoom() * 1.05f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }
        };
        this.ZoomInButton.setLevel(21.0f);
        this.ZoomInButton.getImage().loadImageFromResource(c, R.drawable.zoombutton1);
        this.ZoomInButton.getSize().reset(77.0f, 77.0f);
        this.ZoomInButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomInButton);
        Vector2DF zibds = this.ZoomInButton.getDisplaySize().scale(0.5f);
        this.ZoomInButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 50)) - zibds.getX(), ((float) (GameMaster.getScreenHeight() - 10)) - zibds.getY());
        this.ZoomInButton.getPositionOffset().resetWith(zibds.negateNew());
        this.ZoomOutButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.max(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.max(double, double):double}
              ClspMth{java.lang.Math.max(int, int):int}
              ClspMth{java.lang.Math.max(long, long):long}
              ClspMth{java.lang.Math.max(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.max(0.3f, PuzzleScene.this.Desktop.getZoom() * 0.95f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                    GameMaster.log(this, Float.valueOf(PuzzleScene.this.Desktop.getCurrentObserveAreaRectF().width()));
                }
                super.updateSelf();
            }
        };
        this.ZoomOutButton.setLevel(21.0f);
        this.ZoomOutButton.getImage().loadImageFromResource(c, R.drawable.zoombutton2);
        this.ZoomOutButton.getSize().reset(77.0f, 77.0f);
        this.ZoomOutButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomOutButton);
        Vector2DF zobds = this.ZoomOutButton.getDisplaySize().scale(0.5f);
        this.ZoomOutButton.getPosition().reset((((float) (GameMaster.getScreenWidth() - 100)) - zobds.getX()) - (zibds.getX() * 2.0f), ((float) (GameMaster.getScreenHeight() - 10)) - zobds.getY());
        this.ZoomOutButton.getPositionOffset().resetWith(zobds.negateNew());
        this.ViewFullButton = new UI() {
            public void executive(Vector2DF point) {
                if (PuzzleScene.this.OriginalImage.getImage().getImage() == null) {
                    PuzzleScene.this.OriginalImage.getImage().loadImageFromResource(GameMaster.getContext(), R.drawable.oim);
                }
                PuzzleScene.this.OriginalImage.show();
            }

            public boolean isInArea(Vector2DF point) {
                return point.isIn(PuzzleScene.this.ViewFullButtonRect);
            }

            public void reset() {
                PuzzleScene.this.OriginalImage.hide();
            }
        };
        this.ViewFullButton.getImage().loadImageFromResource(c, R.drawable.viewfull);
        this.ViewFullButton.setLevel(21.0f);
        this.ViewFullButton.setIsUseAbsolutePosition(true);
        this.ViewFullButton.getSize().reset(83.0f, 38.0f);
        getSpiritList().add(this.ViewFullButton);
        this.ViewFullButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 15)) - this.ViewFullButton.getDisplaySize().getX(), 15.0f);
        this.ViewFullButtonRect = new VectorRect2DF(this.ViewFullButton.getPosition(), this.ViewFullButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton = new UI() {
            public void executive(Vector2DF point) {
                Score score = new Score(Double.valueOf(Math.ceil((double) (PuzzleScene.this.PastTime / 1000))), null);
                score.setMode(17);
                ScoreloopManagerSingleton.get().onGamePlayEnded(score);
                hide();
                Toast.makeText(GameMaster.getContext(), (int) R.string.Submitting, 1).show();
            }

            public boolean isInArea(Vector2DF point) {
                return getVisibleOriginalValue() && point.isIn(PuzzleScene.this.SubmitScoreButtonRect);
            }

            public void reset() {
            }
        };
        this.SubmitScoreButton.getImage().loadImageFromResource(c, R.drawable.submitscore);
        this.SubmitScoreButton.setLevel(21.0f);
        this.SubmitScoreButton.setIsUseAbsolutePosition(true);
        this.SubmitScoreButton.getSize().reset(114.0f, 40.0f);
        getSpiritList().add(this.SubmitScoreButton);
        this.SubmitScoreButton.getPosition().reset(15.0f, 60.0f);
        this.SubmitScoreButtonRect = new VectorRect2DF(this.SubmitScoreButton.getPosition(), this.SubmitScoreButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton.hide();
        this.Time = new GameSpirit() {
            DecimalFormat FMT = new DecimalFormat("00");
            Paint p1;
            Paint p2;

            public GameObject getDisplayContentChild() {
                return null;
            }

            /* access modifiers changed from: protected */
            public void drawSelf(Canvas c, Rect drawArea) {
                if (this.p1 == null) {
                    this.p1 = new Paint();
                    this.p1.setARGB(160, 255, 255, 255);
                    this.p1.setAntiAlias(true);
                    this.p1.setTextSize(28.0f);
                    this.p1.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                if (this.p2 == null) {
                    this.p2 = new Paint();
                    this.p2.setARGB(100, 0, 0, 0);
                    this.p2.setAntiAlias(true);
                    this.p2.setTextSize(28.0f);
                    this.p2.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                long t = PuzzleScene.this.getIsFinished() ? PuzzleScene.this.PastTime : PuzzleScene.this.PastTime + (new Date().getTime() - PuzzleScene.this.StartTime);
                long h = t / 3600000;
                long m = (t - (((1000 * h) * 60) * 60)) / 60000;
                String str = String.valueOf(this.FMT.format(h)) + ":" + this.FMT.format(m) + ":" + this.FMT.format(((t - (((1000 * h) * 60) * 60)) - ((1000 * m) * 60)) / 1000) + (PuzzleScene.this.getIsFinished() ? " Finished" : "");
                c.drawText(str, 16.0f, 44.0f, this.p2);
                c.drawText(str, 15.0f, 43.0f, this.p1);
                super.drawSelf(c, drawArea);
            }
        };
        this.Time.setLevel(40.0f);
        getSpiritList().add(this.Time);
    }

    public float getMaxPuzzleLevel() {
        float maxlvl = 0.0f;
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getLevel() <= 10.0f) {
                maxlvl = Math.max(f.getLevel(), maxlvl);
            }
        }
        return maxlvl;
    }

    public Puzzle getTouchPuzzle(Vector2DF v) {
        Vector2DF rv = this.Desktop.reMapPoint(v);
        Puzzle o = null;
        float olen = 99999.0f;
        for (int i = getSpiritList().size() - 1; i >= 0; i--) {
            if (getSpiritList().get(i) instanceof Puzzle) {
                Puzzle p = (Puzzle) getSpiritList().get(i);
                float len = p.getPositionInDesktop().minusNew(rv).getLength();
                if (len <= Puzzle.getRealitySize().getX() * 0.4f) {
                    return p;
                }
                if (len <= Puzzle.getRealitySize().getX() && olen > len) {
                    o = p;
                    olen = len;
                }
            }
        }
        return o;
    }

    public void startDrawSelectRect(Vector2DF startDragPoint, Vector2DF v) {
        this.selectRect = new VectorRect2DF(startDragPoint, v.minusNew(startDragPoint)).getFixedRectF();
    }

    public void stopDrawSelectRect() {
        this.selectRect = null;
    }

    public ArrayList<Puzzle> getSelectPuzzle(Vector2DF startDragPoint, Vector2DF v) {
        Vector2DF rs = this.Desktop.reMapPoint(startDragPoint);
        ArrayList<Puzzle> l = new ArrayList<>();
        RectF rect = new VectorRect2DF(rs, this.Desktop.reMapPoint(v).minusNew(rs)).getFixedRectF();
        float xo = Puzzle.getRealitySize().getX() / 3.0f;
        float yo = Puzzle.getRealitySize().getY() / 3.0f;
        RectF sr = new RectF(rect.left - xo, rect.top - yo, rect.right + xo, rect.bottom + yo);
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getPositionInDesktop().isIn(sr)) {
                l.add(f);
            }
        }
        return l;
    }

    public void loadPuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences s = c.getSharedPreferences();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().load(c, s);
        }
    }

    public void savePuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences.Editor e = c.getSharedPreferences().edit();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().save(c, e);
        }
        e.commit();
    }

    public void reset() {
        this.Controller.reset();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            f.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
            this.Desktop.RandomlyPlaced(f);
        }
        refreshDrawCacheBitmap();
        setIsFinishedToDefault();
        this.StartTime = new Date().getTime();
        this.PastTime = 0;
        this.SubmitScoreButton.hide();
        saveGameState();
        savePuzzleState();
    }

    /* access modifiers changed from: package-private */
    public void c525130452(Context c) {
        Puzzle p525130452 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load525130452(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save525130452(editor, this);
            }
        };
        p525130452.setID(525130452);
        p525130452.setName("525130452");
        p525130452.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p525130452);
        this.Desktop.RandomlyPlaced(p525130452);
        p525130452.setTopEdgeType(EdgeType.Flat);
        p525130452.setBottomEdgeType(EdgeType.Concave);
        p525130452.setLeftEdgeType(EdgeType.Flat);
        p525130452.setRightEdgeType(EdgeType.Convex);
        p525130452.setTopExactPuzzleID(-1);
        p525130452.setBottomExactPuzzleID(1909320204);
        p525130452.setLeftExactPuzzleID(-1);
        p525130452.setRightExactPuzzleID(768123798);
        p525130452.getDisplayImage().loadImageFromResource(c, R.drawable.p525130452h);
        p525130452.setExactRow(0);
        p525130452.setExactColumn(0);
        p525130452.getSize().reset(126.6667f, 100.0f);
        p525130452.getPositionOffset().reset(-50.0f, -50.0f);
        p525130452.setIsUseAbsolutePosition(true);
        p525130452.setIsUseAbsoluteSize(true);
        p525130452.getImage().setIsUseAbsolutePosition(true);
        p525130452.getImage().setIsUseAbsoluteSize(true);
        p525130452.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p525130452.resetPosition();
        getSpiritList().add(p525130452);
    }

    /* access modifiers changed from: package-private */
    public void c1909320204(Context c) {
        Puzzle p1909320204 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1909320204(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1909320204(editor, this);
            }
        };
        p1909320204.setID(1909320204);
        p1909320204.setName("1909320204");
        p1909320204.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1909320204);
        this.Desktop.RandomlyPlaced(p1909320204);
        p1909320204.setTopEdgeType(EdgeType.Convex);
        p1909320204.setBottomEdgeType(EdgeType.Concave);
        p1909320204.setLeftEdgeType(EdgeType.Flat);
        p1909320204.setRightEdgeType(EdgeType.Concave);
        p1909320204.setTopExactPuzzleID(525130452);
        p1909320204.setBottomExactPuzzleID(1297624591);
        p1909320204.setLeftExactPuzzleID(-1);
        p1909320204.setRightExactPuzzleID(1740486235);
        p1909320204.getDisplayImage().loadImageFromResource(c, R.drawable.p1909320204h);
        p1909320204.setExactRow(1);
        p1909320204.setExactColumn(0);
        p1909320204.getSize().reset(100.0f, 126.6667f);
        p1909320204.getPositionOffset().reset(-50.0f, -76.66666f);
        p1909320204.setIsUseAbsolutePosition(true);
        p1909320204.setIsUseAbsoluteSize(true);
        p1909320204.getImage().setIsUseAbsolutePosition(true);
        p1909320204.getImage().setIsUseAbsoluteSize(true);
        p1909320204.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1909320204.resetPosition();
        getSpiritList().add(p1909320204);
    }

    /* access modifiers changed from: package-private */
    public void c1297624591(Context c) {
        Puzzle p1297624591 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1297624591(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1297624591(editor, this);
            }
        };
        p1297624591.setID(1297624591);
        p1297624591.setName("1297624591");
        p1297624591.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1297624591);
        this.Desktop.RandomlyPlaced(p1297624591);
        p1297624591.setTopEdgeType(EdgeType.Convex);
        p1297624591.setBottomEdgeType(EdgeType.Convex);
        p1297624591.setLeftEdgeType(EdgeType.Flat);
        p1297624591.setRightEdgeType(EdgeType.Convex);
        p1297624591.setTopExactPuzzleID(1909320204);
        p1297624591.setBottomExactPuzzleID(2006193649);
        p1297624591.setLeftExactPuzzleID(-1);
        p1297624591.setRightExactPuzzleID(995751331);
        p1297624591.getDisplayImage().loadImageFromResource(c, R.drawable.p1297624591h);
        p1297624591.setExactRow(2);
        p1297624591.setExactColumn(0);
        p1297624591.getSize().reset(126.6667f, 153.3333f);
        p1297624591.getPositionOffset().reset(-50.0f, -76.66666f);
        p1297624591.setIsUseAbsolutePosition(true);
        p1297624591.setIsUseAbsoluteSize(true);
        p1297624591.getImage().setIsUseAbsolutePosition(true);
        p1297624591.getImage().setIsUseAbsoluteSize(true);
        p1297624591.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1297624591.resetPosition();
        getSpiritList().add(p1297624591);
    }

    /* access modifiers changed from: package-private */
    public void c2006193649(Context c) {
        Puzzle p2006193649 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2006193649(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2006193649(editor, this);
            }
        };
        p2006193649.setID(2006193649);
        p2006193649.setName("2006193649");
        p2006193649.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2006193649);
        this.Desktop.RandomlyPlaced(p2006193649);
        p2006193649.setTopEdgeType(EdgeType.Concave);
        p2006193649.setBottomEdgeType(EdgeType.Convex);
        p2006193649.setLeftEdgeType(EdgeType.Flat);
        p2006193649.setRightEdgeType(EdgeType.Convex);
        p2006193649.setTopExactPuzzleID(1297624591);
        p2006193649.setBottomExactPuzzleID(1231949303);
        p2006193649.setLeftExactPuzzleID(-1);
        p2006193649.setRightExactPuzzleID(1508861472);
        p2006193649.getDisplayImage().loadImageFromResource(c, R.drawable.p2006193649h);
        p2006193649.setExactRow(3);
        p2006193649.setExactColumn(0);
        p2006193649.getSize().reset(126.6667f, 126.6667f);
        p2006193649.getPositionOffset().reset(-50.0f, -50.0f);
        p2006193649.setIsUseAbsolutePosition(true);
        p2006193649.setIsUseAbsoluteSize(true);
        p2006193649.getImage().setIsUseAbsolutePosition(true);
        p2006193649.getImage().setIsUseAbsoluteSize(true);
        p2006193649.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2006193649.resetPosition();
        getSpiritList().add(p2006193649);
    }

    /* access modifiers changed from: package-private */
    public void c1231949303(Context c) {
        Puzzle p1231949303 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1231949303(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1231949303(editor, this);
            }
        };
        p1231949303.setID(1231949303);
        p1231949303.setName("1231949303");
        p1231949303.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1231949303);
        this.Desktop.RandomlyPlaced(p1231949303);
        p1231949303.setTopEdgeType(EdgeType.Concave);
        p1231949303.setBottomEdgeType(EdgeType.Flat);
        p1231949303.setLeftEdgeType(EdgeType.Flat);
        p1231949303.setRightEdgeType(EdgeType.Concave);
        p1231949303.setTopExactPuzzleID(2006193649);
        p1231949303.setBottomExactPuzzleID(-1);
        p1231949303.setLeftExactPuzzleID(-1);
        p1231949303.setRightExactPuzzleID(752353802);
        p1231949303.getDisplayImage().loadImageFromResource(c, R.drawable.p1231949303h);
        p1231949303.setExactRow(4);
        p1231949303.setExactColumn(0);
        p1231949303.getSize().reset(100.0f, 100.0f);
        p1231949303.getPositionOffset().reset(-50.0f, -50.0f);
        p1231949303.setIsUseAbsolutePosition(true);
        p1231949303.setIsUseAbsoluteSize(true);
        p1231949303.getImage().setIsUseAbsolutePosition(true);
        p1231949303.getImage().setIsUseAbsoluteSize(true);
        p1231949303.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1231949303.resetPosition();
        getSpiritList().add(p1231949303);
    }

    /* access modifiers changed from: package-private */
    public void c768123798(Context c) {
        Puzzle p768123798 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load768123798(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save768123798(editor, this);
            }
        };
        p768123798.setID(768123798);
        p768123798.setName("768123798");
        p768123798.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p768123798);
        this.Desktop.RandomlyPlaced(p768123798);
        p768123798.setTopEdgeType(EdgeType.Flat);
        p768123798.setBottomEdgeType(EdgeType.Concave);
        p768123798.setLeftEdgeType(EdgeType.Concave);
        p768123798.setRightEdgeType(EdgeType.Convex);
        p768123798.setTopExactPuzzleID(-1);
        p768123798.setBottomExactPuzzleID(1740486235);
        p768123798.setLeftExactPuzzleID(525130452);
        p768123798.setRightExactPuzzleID(180988013);
        p768123798.getDisplayImage().loadImageFromResource(c, R.drawable.p768123798h);
        p768123798.setExactRow(0);
        p768123798.setExactColumn(1);
        p768123798.getSize().reset(126.6667f, 100.0f);
        p768123798.getPositionOffset().reset(-50.0f, -50.0f);
        p768123798.setIsUseAbsolutePosition(true);
        p768123798.setIsUseAbsoluteSize(true);
        p768123798.getImage().setIsUseAbsolutePosition(true);
        p768123798.getImage().setIsUseAbsoluteSize(true);
        p768123798.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p768123798.resetPosition();
        getSpiritList().add(p768123798);
    }

    /* access modifiers changed from: package-private */
    public void c1740486235(Context c) {
        Puzzle p1740486235 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1740486235(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1740486235(editor, this);
            }
        };
        p1740486235.setID(1740486235);
        p1740486235.setName("1740486235");
        p1740486235.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1740486235);
        this.Desktop.RandomlyPlaced(p1740486235);
        p1740486235.setTopEdgeType(EdgeType.Convex);
        p1740486235.setBottomEdgeType(EdgeType.Convex);
        p1740486235.setLeftEdgeType(EdgeType.Convex);
        p1740486235.setRightEdgeType(EdgeType.Concave);
        p1740486235.setTopExactPuzzleID(768123798);
        p1740486235.setBottomExactPuzzleID(995751331);
        p1740486235.setLeftExactPuzzleID(1909320204);
        p1740486235.setRightExactPuzzleID(1328845637);
        p1740486235.getDisplayImage().loadImageFromResource(c, R.drawable.p1740486235h);
        p1740486235.setExactRow(1);
        p1740486235.setExactColumn(1);
        p1740486235.getSize().reset(126.6667f, 153.3333f);
        p1740486235.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1740486235.setIsUseAbsolutePosition(true);
        p1740486235.setIsUseAbsoluteSize(true);
        p1740486235.getImage().setIsUseAbsolutePosition(true);
        p1740486235.getImage().setIsUseAbsoluteSize(true);
        p1740486235.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1740486235.resetPosition();
        getSpiritList().add(p1740486235);
    }

    /* access modifiers changed from: package-private */
    public void c995751331(Context c) {
        Puzzle p995751331 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load995751331(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save995751331(editor, this);
            }
        };
        p995751331.setID(995751331);
        p995751331.setName("995751331");
        p995751331.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p995751331);
        this.Desktop.RandomlyPlaced(p995751331);
        p995751331.setTopEdgeType(EdgeType.Concave);
        p995751331.setBottomEdgeType(EdgeType.Convex);
        p995751331.setLeftEdgeType(EdgeType.Concave);
        p995751331.setRightEdgeType(EdgeType.Convex);
        p995751331.setTopExactPuzzleID(1740486235);
        p995751331.setBottomExactPuzzleID(1508861472);
        p995751331.setLeftExactPuzzleID(1297624591);
        p995751331.setRightExactPuzzleID(1364815339);
        p995751331.getDisplayImage().loadImageFromResource(c, R.drawable.p995751331h);
        p995751331.setExactRow(2);
        p995751331.setExactColumn(1);
        p995751331.getSize().reset(126.6667f, 126.6667f);
        p995751331.getPositionOffset().reset(-50.0f, -50.0f);
        p995751331.setIsUseAbsolutePosition(true);
        p995751331.setIsUseAbsoluteSize(true);
        p995751331.getImage().setIsUseAbsolutePosition(true);
        p995751331.getImage().setIsUseAbsoluteSize(true);
        p995751331.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p995751331.resetPosition();
        getSpiritList().add(p995751331);
    }

    /* access modifiers changed from: package-private */
    public void c1508861472(Context c) {
        Puzzle p1508861472 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1508861472(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1508861472(editor, this);
            }
        };
        p1508861472.setID(1508861472);
        p1508861472.setName("1508861472");
        p1508861472.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1508861472);
        this.Desktop.RandomlyPlaced(p1508861472);
        p1508861472.setTopEdgeType(EdgeType.Concave);
        p1508861472.setBottomEdgeType(EdgeType.Concave);
        p1508861472.setLeftEdgeType(EdgeType.Concave);
        p1508861472.setRightEdgeType(EdgeType.Convex);
        p1508861472.setTopExactPuzzleID(995751331);
        p1508861472.setBottomExactPuzzleID(752353802);
        p1508861472.setLeftExactPuzzleID(2006193649);
        p1508861472.setRightExactPuzzleID(1166173088);
        p1508861472.getDisplayImage().loadImageFromResource(c, R.drawable.p1508861472h);
        p1508861472.setExactRow(3);
        p1508861472.setExactColumn(1);
        p1508861472.getSize().reset(126.6667f, 100.0f);
        p1508861472.getPositionOffset().reset(-50.0f, -50.0f);
        p1508861472.setIsUseAbsolutePosition(true);
        p1508861472.setIsUseAbsoluteSize(true);
        p1508861472.getImage().setIsUseAbsolutePosition(true);
        p1508861472.getImage().setIsUseAbsoluteSize(true);
        p1508861472.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1508861472.resetPosition();
        getSpiritList().add(p1508861472);
    }

    /* access modifiers changed from: package-private */
    public void c752353802(Context c) {
        Puzzle p752353802 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load752353802(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save752353802(editor, this);
            }
        };
        p752353802.setID(752353802);
        p752353802.setName("752353802");
        p752353802.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p752353802);
        this.Desktop.RandomlyPlaced(p752353802);
        p752353802.setTopEdgeType(EdgeType.Convex);
        p752353802.setBottomEdgeType(EdgeType.Flat);
        p752353802.setLeftEdgeType(EdgeType.Convex);
        p752353802.setRightEdgeType(EdgeType.Concave);
        p752353802.setTopExactPuzzleID(1508861472);
        p752353802.setBottomExactPuzzleID(-1);
        p752353802.setLeftExactPuzzleID(1231949303);
        p752353802.setRightExactPuzzleID(1541020853);
        p752353802.getDisplayImage().loadImageFromResource(c, R.drawable.p752353802h);
        p752353802.setExactRow(4);
        p752353802.setExactColumn(1);
        p752353802.getSize().reset(126.6667f, 126.6667f);
        p752353802.getPositionOffset().reset(-76.66666f, -76.66666f);
        p752353802.setIsUseAbsolutePosition(true);
        p752353802.setIsUseAbsoluteSize(true);
        p752353802.getImage().setIsUseAbsolutePosition(true);
        p752353802.getImage().setIsUseAbsoluteSize(true);
        p752353802.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p752353802.resetPosition();
        getSpiritList().add(p752353802);
    }

    /* access modifiers changed from: package-private */
    public void c180988013(Context c) {
        Puzzle p180988013 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load180988013(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save180988013(editor, this);
            }
        };
        p180988013.setID(180988013);
        p180988013.setName("180988013");
        p180988013.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p180988013);
        this.Desktop.RandomlyPlaced(p180988013);
        p180988013.setTopEdgeType(EdgeType.Flat);
        p180988013.setBottomEdgeType(EdgeType.Concave);
        p180988013.setLeftEdgeType(EdgeType.Concave);
        p180988013.setRightEdgeType(EdgeType.Convex);
        p180988013.setTopExactPuzzleID(-1);
        p180988013.setBottomExactPuzzleID(1328845637);
        p180988013.setLeftExactPuzzleID(768123798);
        p180988013.setRightExactPuzzleID(485145566);
        p180988013.getDisplayImage().loadImageFromResource(c, R.drawable.p180988013h);
        p180988013.setExactRow(0);
        p180988013.setExactColumn(2);
        p180988013.getSize().reset(126.6667f, 100.0f);
        p180988013.getPositionOffset().reset(-50.0f, -50.0f);
        p180988013.setIsUseAbsolutePosition(true);
        p180988013.setIsUseAbsoluteSize(true);
        p180988013.getImage().setIsUseAbsolutePosition(true);
        p180988013.getImage().setIsUseAbsoluteSize(true);
        p180988013.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p180988013.resetPosition();
        getSpiritList().add(p180988013);
    }

    /* access modifiers changed from: package-private */
    public void c1328845637(Context c) {
        Puzzle p1328845637 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1328845637(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1328845637(editor, this);
            }
        };
        p1328845637.setID(1328845637);
        p1328845637.setName("1328845637");
        p1328845637.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1328845637);
        this.Desktop.RandomlyPlaced(p1328845637);
        p1328845637.setTopEdgeType(EdgeType.Convex);
        p1328845637.setBottomEdgeType(EdgeType.Convex);
        p1328845637.setLeftEdgeType(EdgeType.Convex);
        p1328845637.setRightEdgeType(EdgeType.Convex);
        p1328845637.setTopExactPuzzleID(180988013);
        p1328845637.setBottomExactPuzzleID(1364815339);
        p1328845637.setLeftExactPuzzleID(1740486235);
        p1328845637.setRightExactPuzzleID(782023876);
        p1328845637.getDisplayImage().loadImageFromResource(c, R.drawable.p1328845637h);
        p1328845637.setExactRow(1);
        p1328845637.setExactColumn(2);
        p1328845637.getSize().reset(153.3333f, 153.3333f);
        p1328845637.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1328845637.setIsUseAbsolutePosition(true);
        p1328845637.setIsUseAbsoluteSize(true);
        p1328845637.getImage().setIsUseAbsolutePosition(true);
        p1328845637.getImage().setIsUseAbsoluteSize(true);
        p1328845637.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1328845637.resetPosition();
        getSpiritList().add(p1328845637);
    }

    /* access modifiers changed from: package-private */
    public void c1364815339(Context c) {
        Puzzle p1364815339 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1364815339(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1364815339(editor, this);
            }
        };
        p1364815339.setID(1364815339);
        p1364815339.setName("1364815339");
        p1364815339.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1364815339);
        this.Desktop.RandomlyPlaced(p1364815339);
        p1364815339.setTopEdgeType(EdgeType.Concave);
        p1364815339.setBottomEdgeType(EdgeType.Concave);
        p1364815339.setLeftEdgeType(EdgeType.Concave);
        p1364815339.setRightEdgeType(EdgeType.Convex);
        p1364815339.setTopExactPuzzleID(1328845637);
        p1364815339.setBottomExactPuzzleID(1166173088);
        p1364815339.setLeftExactPuzzleID(995751331);
        p1364815339.setRightExactPuzzleID(496912582);
        p1364815339.getDisplayImage().loadImageFromResource(c, R.drawable.p1364815339h);
        p1364815339.setExactRow(2);
        p1364815339.setExactColumn(2);
        p1364815339.getSize().reset(126.6667f, 100.0f);
        p1364815339.getPositionOffset().reset(-50.0f, -50.0f);
        p1364815339.setIsUseAbsolutePosition(true);
        p1364815339.setIsUseAbsoluteSize(true);
        p1364815339.getImage().setIsUseAbsolutePosition(true);
        p1364815339.getImage().setIsUseAbsoluteSize(true);
        p1364815339.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1364815339.resetPosition();
        getSpiritList().add(p1364815339);
    }

    /* access modifiers changed from: package-private */
    public void c1166173088(Context c) {
        Puzzle p1166173088 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1166173088(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1166173088(editor, this);
            }
        };
        p1166173088.setID(1166173088);
        p1166173088.setName("1166173088");
        p1166173088.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1166173088);
        this.Desktop.RandomlyPlaced(p1166173088);
        p1166173088.setTopEdgeType(EdgeType.Convex);
        p1166173088.setBottomEdgeType(EdgeType.Convex);
        p1166173088.setLeftEdgeType(EdgeType.Concave);
        p1166173088.setRightEdgeType(EdgeType.Convex);
        p1166173088.setTopExactPuzzleID(1364815339);
        p1166173088.setBottomExactPuzzleID(1541020853);
        p1166173088.setLeftExactPuzzleID(1508861472);
        p1166173088.setRightExactPuzzleID(517696563);
        p1166173088.getDisplayImage().loadImageFromResource(c, R.drawable.p1166173088h);
        p1166173088.setExactRow(3);
        p1166173088.setExactColumn(2);
        p1166173088.getSize().reset(126.6667f, 153.3333f);
        p1166173088.getPositionOffset().reset(-50.0f, -76.66666f);
        p1166173088.setIsUseAbsolutePosition(true);
        p1166173088.setIsUseAbsoluteSize(true);
        p1166173088.getImage().setIsUseAbsolutePosition(true);
        p1166173088.getImage().setIsUseAbsoluteSize(true);
        p1166173088.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1166173088.resetPosition();
        getSpiritList().add(p1166173088);
    }

    /* access modifiers changed from: package-private */
    public void c1541020853(Context c) {
        Puzzle p1541020853 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1541020853(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1541020853(editor, this);
            }
        };
        p1541020853.setID(1541020853);
        p1541020853.setName("1541020853");
        p1541020853.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1541020853);
        this.Desktop.RandomlyPlaced(p1541020853);
        p1541020853.setTopEdgeType(EdgeType.Concave);
        p1541020853.setBottomEdgeType(EdgeType.Flat);
        p1541020853.setLeftEdgeType(EdgeType.Convex);
        p1541020853.setRightEdgeType(EdgeType.Convex);
        p1541020853.setTopExactPuzzleID(1166173088);
        p1541020853.setBottomExactPuzzleID(-1);
        p1541020853.setLeftExactPuzzleID(752353802);
        p1541020853.setRightExactPuzzleID(2053814480);
        p1541020853.getDisplayImage().loadImageFromResource(c, R.drawable.p1541020853h);
        p1541020853.setExactRow(4);
        p1541020853.setExactColumn(2);
        p1541020853.getSize().reset(153.3333f, 100.0f);
        p1541020853.getPositionOffset().reset(-76.66666f, -50.0f);
        p1541020853.setIsUseAbsolutePosition(true);
        p1541020853.setIsUseAbsoluteSize(true);
        p1541020853.getImage().setIsUseAbsolutePosition(true);
        p1541020853.getImage().setIsUseAbsoluteSize(true);
        p1541020853.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1541020853.resetPosition();
        getSpiritList().add(p1541020853);
    }

    /* access modifiers changed from: package-private */
    public void c485145566(Context c) {
        Puzzle p485145566 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load485145566(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save485145566(editor, this);
            }
        };
        p485145566.setID(485145566);
        p485145566.setName("485145566");
        p485145566.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p485145566);
        this.Desktop.RandomlyPlaced(p485145566);
        p485145566.setTopEdgeType(EdgeType.Flat);
        p485145566.setBottomEdgeType(EdgeType.Convex);
        p485145566.setLeftEdgeType(EdgeType.Concave);
        p485145566.setRightEdgeType(EdgeType.Concave);
        p485145566.setTopExactPuzzleID(-1);
        p485145566.setBottomExactPuzzleID(782023876);
        p485145566.setLeftExactPuzzleID(180988013);
        p485145566.setRightExactPuzzleID(1576919550);
        p485145566.getDisplayImage().loadImageFromResource(c, R.drawable.p485145566h);
        p485145566.setExactRow(0);
        p485145566.setExactColumn(3);
        p485145566.getSize().reset(100.0f, 126.6667f);
        p485145566.getPositionOffset().reset(-50.0f, -50.0f);
        p485145566.setIsUseAbsolutePosition(true);
        p485145566.setIsUseAbsoluteSize(true);
        p485145566.getImage().setIsUseAbsolutePosition(true);
        p485145566.getImage().setIsUseAbsoluteSize(true);
        p485145566.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p485145566.resetPosition();
        getSpiritList().add(p485145566);
    }

    /* access modifiers changed from: package-private */
    public void c782023876(Context c) {
        Puzzle p782023876 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load782023876(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save782023876(editor, this);
            }
        };
        p782023876.setID(782023876);
        p782023876.setName("782023876");
        p782023876.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p782023876);
        this.Desktop.RandomlyPlaced(p782023876);
        p782023876.setTopEdgeType(EdgeType.Concave);
        p782023876.setBottomEdgeType(EdgeType.Convex);
        p782023876.setLeftEdgeType(EdgeType.Concave);
        p782023876.setRightEdgeType(EdgeType.Convex);
        p782023876.setTopExactPuzzleID(485145566);
        p782023876.setBottomExactPuzzleID(496912582);
        p782023876.setLeftExactPuzzleID(1328845637);
        p782023876.setRightExactPuzzleID(1900623420);
        p782023876.getDisplayImage().loadImageFromResource(c, R.drawable.p782023876h);
        p782023876.setExactRow(1);
        p782023876.setExactColumn(3);
        p782023876.getSize().reset(126.6667f, 126.6667f);
        p782023876.getPositionOffset().reset(-50.0f, -50.0f);
        p782023876.setIsUseAbsolutePosition(true);
        p782023876.setIsUseAbsoluteSize(true);
        p782023876.getImage().setIsUseAbsolutePosition(true);
        p782023876.getImage().setIsUseAbsoluteSize(true);
        p782023876.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p782023876.resetPosition();
        getSpiritList().add(p782023876);
    }

    /* access modifiers changed from: package-private */
    public void c496912582(Context c) {
        Puzzle p496912582 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load496912582(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save496912582(editor, this);
            }
        };
        p496912582.setID(496912582);
        p496912582.setName("496912582");
        p496912582.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p496912582);
        this.Desktop.RandomlyPlaced(p496912582);
        p496912582.setTopEdgeType(EdgeType.Concave);
        p496912582.setBottomEdgeType(EdgeType.Convex);
        p496912582.setLeftEdgeType(EdgeType.Concave);
        p496912582.setRightEdgeType(EdgeType.Concave);
        p496912582.setTopExactPuzzleID(782023876);
        p496912582.setBottomExactPuzzleID(517696563);
        p496912582.setLeftExactPuzzleID(1364815339);
        p496912582.setRightExactPuzzleID(737660695);
        p496912582.getDisplayImage().loadImageFromResource(c, R.drawable.p496912582h);
        p496912582.setExactRow(2);
        p496912582.setExactColumn(3);
        p496912582.getSize().reset(100.0f, 126.6667f);
        p496912582.getPositionOffset().reset(-50.0f, -50.0f);
        p496912582.setIsUseAbsolutePosition(true);
        p496912582.setIsUseAbsoluteSize(true);
        p496912582.getImage().setIsUseAbsolutePosition(true);
        p496912582.getImage().setIsUseAbsoluteSize(true);
        p496912582.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p496912582.resetPosition();
        getSpiritList().add(p496912582);
    }

    /* access modifiers changed from: package-private */
    public void c517696563(Context c) {
        Puzzle p517696563 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load517696563(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save517696563(editor, this);
            }
        };
        p517696563.setID(517696563);
        p517696563.setName("517696563");
        p517696563.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p517696563);
        this.Desktop.RandomlyPlaced(p517696563);
        p517696563.setTopEdgeType(EdgeType.Concave);
        p517696563.setBottomEdgeType(EdgeType.Concave);
        p517696563.setLeftEdgeType(EdgeType.Concave);
        p517696563.setRightEdgeType(EdgeType.Concave);
        p517696563.setTopExactPuzzleID(496912582);
        p517696563.setBottomExactPuzzleID(2053814480);
        p517696563.setLeftExactPuzzleID(1166173088);
        p517696563.setRightExactPuzzleID(240652433);
        p517696563.getDisplayImage().loadImageFromResource(c, R.drawable.p517696563h);
        p517696563.setExactRow(3);
        p517696563.setExactColumn(3);
        p517696563.getSize().reset(100.0f, 100.0f);
        p517696563.getPositionOffset().reset(-50.0f, -50.0f);
        p517696563.setIsUseAbsolutePosition(true);
        p517696563.setIsUseAbsoluteSize(true);
        p517696563.getImage().setIsUseAbsolutePosition(true);
        p517696563.getImage().setIsUseAbsoluteSize(true);
        p517696563.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p517696563.resetPosition();
        getSpiritList().add(p517696563);
    }

    /* access modifiers changed from: package-private */
    public void c2053814480(Context c) {
        Puzzle p2053814480 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2053814480(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2053814480(editor, this);
            }
        };
        p2053814480.setID(2053814480);
        p2053814480.setName("2053814480");
        p2053814480.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2053814480);
        this.Desktop.RandomlyPlaced(p2053814480);
        p2053814480.setTopEdgeType(EdgeType.Convex);
        p2053814480.setBottomEdgeType(EdgeType.Flat);
        p2053814480.setLeftEdgeType(EdgeType.Concave);
        p2053814480.setRightEdgeType(EdgeType.Convex);
        p2053814480.setTopExactPuzzleID(517696563);
        p2053814480.setBottomExactPuzzleID(-1);
        p2053814480.setLeftExactPuzzleID(1541020853);
        p2053814480.setRightExactPuzzleID(1149214648);
        p2053814480.getDisplayImage().loadImageFromResource(c, R.drawable.p2053814480h);
        p2053814480.setExactRow(4);
        p2053814480.setExactColumn(3);
        p2053814480.getSize().reset(126.6667f, 126.6667f);
        p2053814480.getPositionOffset().reset(-50.0f, -76.66666f);
        p2053814480.setIsUseAbsolutePosition(true);
        p2053814480.setIsUseAbsoluteSize(true);
        p2053814480.getImage().setIsUseAbsolutePosition(true);
        p2053814480.getImage().setIsUseAbsoluteSize(true);
        p2053814480.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2053814480.resetPosition();
        getSpiritList().add(p2053814480);
    }

    /* access modifiers changed from: package-private */
    public void c1576919550(Context c) {
        Puzzle p1576919550 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1576919550(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1576919550(editor, this);
            }
        };
        p1576919550.setID(1576919550);
        p1576919550.setName("1576919550");
        p1576919550.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1576919550);
        this.Desktop.RandomlyPlaced(p1576919550);
        p1576919550.setTopEdgeType(EdgeType.Flat);
        p1576919550.setBottomEdgeType(EdgeType.Concave);
        p1576919550.setLeftEdgeType(EdgeType.Convex);
        p1576919550.setRightEdgeType(EdgeType.Convex);
        p1576919550.setTopExactPuzzleID(-1);
        p1576919550.setBottomExactPuzzleID(1900623420);
        p1576919550.setLeftExactPuzzleID(485145566);
        p1576919550.setRightExactPuzzleID(286405353);
        p1576919550.getDisplayImage().loadImageFromResource(c, R.drawable.p1576919550h);
        p1576919550.setExactRow(0);
        p1576919550.setExactColumn(4);
        p1576919550.getSize().reset(153.3333f, 100.0f);
        p1576919550.getPositionOffset().reset(-76.66666f, -50.0f);
        p1576919550.setIsUseAbsolutePosition(true);
        p1576919550.setIsUseAbsoluteSize(true);
        p1576919550.getImage().setIsUseAbsolutePosition(true);
        p1576919550.getImage().setIsUseAbsoluteSize(true);
        p1576919550.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1576919550.resetPosition();
        getSpiritList().add(p1576919550);
    }

    /* access modifiers changed from: package-private */
    public void c1900623420(Context c) {
        Puzzle p1900623420 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1900623420(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1900623420(editor, this);
            }
        };
        p1900623420.setID(1900623420);
        p1900623420.setName("1900623420");
        p1900623420.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1900623420);
        this.Desktop.RandomlyPlaced(p1900623420);
        p1900623420.setTopEdgeType(EdgeType.Convex);
        p1900623420.setBottomEdgeType(EdgeType.Convex);
        p1900623420.setLeftEdgeType(EdgeType.Concave);
        p1900623420.setRightEdgeType(EdgeType.Convex);
        p1900623420.setTopExactPuzzleID(1576919550);
        p1900623420.setBottomExactPuzzleID(737660695);
        p1900623420.setLeftExactPuzzleID(782023876);
        p1900623420.setRightExactPuzzleID(1627103793);
        p1900623420.getDisplayImage().loadImageFromResource(c, R.drawable.p1900623420h);
        p1900623420.setExactRow(1);
        p1900623420.setExactColumn(4);
        p1900623420.getSize().reset(126.6667f, 153.3333f);
        p1900623420.getPositionOffset().reset(-50.0f, -76.66666f);
        p1900623420.setIsUseAbsolutePosition(true);
        p1900623420.setIsUseAbsoluteSize(true);
        p1900623420.getImage().setIsUseAbsolutePosition(true);
        p1900623420.getImage().setIsUseAbsoluteSize(true);
        p1900623420.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1900623420.resetPosition();
        getSpiritList().add(p1900623420);
    }

    /* access modifiers changed from: package-private */
    public void c737660695(Context c) {
        Puzzle p737660695 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load737660695(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save737660695(editor, this);
            }
        };
        p737660695.setID(737660695);
        p737660695.setName("737660695");
        p737660695.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p737660695);
        this.Desktop.RandomlyPlaced(p737660695);
        p737660695.setTopEdgeType(EdgeType.Concave);
        p737660695.setBottomEdgeType(EdgeType.Concave);
        p737660695.setLeftEdgeType(EdgeType.Convex);
        p737660695.setRightEdgeType(EdgeType.Convex);
        p737660695.setTopExactPuzzleID(1900623420);
        p737660695.setBottomExactPuzzleID(240652433);
        p737660695.setLeftExactPuzzleID(496912582);
        p737660695.setRightExactPuzzleID(255769629);
        p737660695.getDisplayImage().loadImageFromResource(c, R.drawable.p737660695h);
        p737660695.setExactRow(2);
        p737660695.setExactColumn(4);
        p737660695.getSize().reset(153.3333f, 100.0f);
        p737660695.getPositionOffset().reset(-76.66666f, -50.0f);
        p737660695.setIsUseAbsolutePosition(true);
        p737660695.setIsUseAbsoluteSize(true);
        p737660695.getImage().setIsUseAbsolutePosition(true);
        p737660695.getImage().setIsUseAbsoluteSize(true);
        p737660695.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p737660695.resetPosition();
        getSpiritList().add(p737660695);
    }

    /* access modifiers changed from: package-private */
    public void c240652433(Context c) {
        Puzzle p240652433 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load240652433(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save240652433(editor, this);
            }
        };
        p240652433.setID(240652433);
        p240652433.setName("240652433");
        p240652433.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p240652433);
        this.Desktop.RandomlyPlaced(p240652433);
        p240652433.setTopEdgeType(EdgeType.Convex);
        p240652433.setBottomEdgeType(EdgeType.Concave);
        p240652433.setLeftEdgeType(EdgeType.Convex);
        p240652433.setRightEdgeType(EdgeType.Concave);
        p240652433.setTopExactPuzzleID(737660695);
        p240652433.setBottomExactPuzzleID(1149214648);
        p240652433.setLeftExactPuzzleID(517696563);
        p240652433.setRightExactPuzzleID(459579215);
        p240652433.getDisplayImage().loadImageFromResource(c, R.drawable.p240652433h);
        p240652433.setExactRow(3);
        p240652433.setExactColumn(4);
        p240652433.getSize().reset(126.6667f, 126.6667f);
        p240652433.getPositionOffset().reset(-76.66666f, -76.66666f);
        p240652433.setIsUseAbsolutePosition(true);
        p240652433.setIsUseAbsoluteSize(true);
        p240652433.getImage().setIsUseAbsolutePosition(true);
        p240652433.getImage().setIsUseAbsoluteSize(true);
        p240652433.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p240652433.resetPosition();
        getSpiritList().add(p240652433);
    }

    /* access modifiers changed from: package-private */
    public void c1149214648(Context c) {
        Puzzle p1149214648 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1149214648(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1149214648(editor, this);
            }
        };
        p1149214648.setID(1149214648);
        p1149214648.setName("1149214648");
        p1149214648.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1149214648);
        this.Desktop.RandomlyPlaced(p1149214648);
        p1149214648.setTopEdgeType(EdgeType.Convex);
        p1149214648.setBottomEdgeType(EdgeType.Flat);
        p1149214648.setLeftEdgeType(EdgeType.Concave);
        p1149214648.setRightEdgeType(EdgeType.Convex);
        p1149214648.setTopExactPuzzleID(240652433);
        p1149214648.setBottomExactPuzzleID(-1);
        p1149214648.setLeftExactPuzzleID(2053814480);
        p1149214648.setRightExactPuzzleID(2042882337);
        p1149214648.getDisplayImage().loadImageFromResource(c, R.drawable.p1149214648h);
        p1149214648.setExactRow(4);
        p1149214648.setExactColumn(4);
        p1149214648.getSize().reset(126.6667f, 126.6667f);
        p1149214648.getPositionOffset().reset(-50.0f, -76.66666f);
        p1149214648.setIsUseAbsolutePosition(true);
        p1149214648.setIsUseAbsoluteSize(true);
        p1149214648.getImage().setIsUseAbsolutePosition(true);
        p1149214648.getImage().setIsUseAbsoluteSize(true);
        p1149214648.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1149214648.resetPosition();
        getSpiritList().add(p1149214648);
    }

    /* access modifiers changed from: package-private */
    public void c286405353(Context c) {
        Puzzle p286405353 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load286405353(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save286405353(editor, this);
            }
        };
        p286405353.setID(286405353);
        p286405353.setName("286405353");
        p286405353.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p286405353);
        this.Desktop.RandomlyPlaced(p286405353);
        p286405353.setTopEdgeType(EdgeType.Flat);
        p286405353.setBottomEdgeType(EdgeType.Convex);
        p286405353.setLeftEdgeType(EdgeType.Concave);
        p286405353.setRightEdgeType(EdgeType.Convex);
        p286405353.setTopExactPuzzleID(-1);
        p286405353.setBottomExactPuzzleID(1627103793);
        p286405353.setLeftExactPuzzleID(1576919550);
        p286405353.setRightExactPuzzleID(1917798868);
        p286405353.getDisplayImage().loadImageFromResource(c, R.drawable.p286405353h);
        p286405353.setExactRow(0);
        p286405353.setExactColumn(5);
        p286405353.getSize().reset(126.6667f, 126.6667f);
        p286405353.getPositionOffset().reset(-50.0f, -50.0f);
        p286405353.setIsUseAbsolutePosition(true);
        p286405353.setIsUseAbsoluteSize(true);
        p286405353.getImage().setIsUseAbsolutePosition(true);
        p286405353.getImage().setIsUseAbsoluteSize(true);
        p286405353.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p286405353.resetPosition();
        getSpiritList().add(p286405353);
    }

    /* access modifiers changed from: package-private */
    public void c1627103793(Context c) {
        Puzzle p1627103793 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1627103793(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1627103793(editor, this);
            }
        };
        p1627103793.setID(1627103793);
        p1627103793.setName("1627103793");
        p1627103793.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1627103793);
        this.Desktop.RandomlyPlaced(p1627103793);
        p1627103793.setTopEdgeType(EdgeType.Concave);
        p1627103793.setBottomEdgeType(EdgeType.Concave);
        p1627103793.setLeftEdgeType(EdgeType.Concave);
        p1627103793.setRightEdgeType(EdgeType.Concave);
        p1627103793.setTopExactPuzzleID(286405353);
        p1627103793.setBottomExactPuzzleID(255769629);
        p1627103793.setLeftExactPuzzleID(1900623420);
        p1627103793.setRightExactPuzzleID(686898821);
        p1627103793.getDisplayImage().loadImageFromResource(c, R.drawable.p1627103793h);
        p1627103793.setExactRow(1);
        p1627103793.setExactColumn(5);
        p1627103793.getSize().reset(100.0f, 100.0f);
        p1627103793.getPositionOffset().reset(-50.0f, -50.0f);
        p1627103793.setIsUseAbsolutePosition(true);
        p1627103793.setIsUseAbsoluteSize(true);
        p1627103793.getImage().setIsUseAbsolutePosition(true);
        p1627103793.getImage().setIsUseAbsoluteSize(true);
        p1627103793.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1627103793.resetPosition();
        getSpiritList().add(p1627103793);
    }

    /* access modifiers changed from: package-private */
    public void c255769629(Context c) {
        Puzzle p255769629 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load255769629(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save255769629(editor, this);
            }
        };
        p255769629.setID(255769629);
        p255769629.setName("255769629");
        p255769629.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p255769629);
        this.Desktop.RandomlyPlaced(p255769629);
        p255769629.setTopEdgeType(EdgeType.Convex);
        p255769629.setBottomEdgeType(EdgeType.Concave);
        p255769629.setLeftEdgeType(EdgeType.Concave);
        p255769629.setRightEdgeType(EdgeType.Concave);
        p255769629.setTopExactPuzzleID(1627103793);
        p255769629.setBottomExactPuzzleID(459579215);
        p255769629.setLeftExactPuzzleID(737660695);
        p255769629.setRightExactPuzzleID(1019488942);
        p255769629.getDisplayImage().loadImageFromResource(c, R.drawable.p255769629h);
        p255769629.setExactRow(2);
        p255769629.setExactColumn(5);
        p255769629.getSize().reset(100.0f, 126.6667f);
        p255769629.getPositionOffset().reset(-50.0f, -76.66666f);
        p255769629.setIsUseAbsolutePosition(true);
        p255769629.setIsUseAbsoluteSize(true);
        p255769629.getImage().setIsUseAbsolutePosition(true);
        p255769629.getImage().setIsUseAbsoluteSize(true);
        p255769629.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p255769629.resetPosition();
        getSpiritList().add(p255769629);
    }

    /* access modifiers changed from: package-private */
    public void c459579215(Context c) {
        Puzzle p459579215 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load459579215(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save459579215(editor, this);
            }
        };
        p459579215.setID(459579215);
        p459579215.setName("459579215");
        p459579215.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p459579215);
        this.Desktop.RandomlyPlaced(p459579215);
        p459579215.setTopEdgeType(EdgeType.Convex);
        p459579215.setBottomEdgeType(EdgeType.Convex);
        p459579215.setLeftEdgeType(EdgeType.Convex);
        p459579215.setRightEdgeType(EdgeType.Concave);
        p459579215.setTopExactPuzzleID(255769629);
        p459579215.setBottomExactPuzzleID(2042882337);
        p459579215.setLeftExactPuzzleID(240652433);
        p459579215.setRightExactPuzzleID(285089697);
        p459579215.getDisplayImage().loadImageFromResource(c, R.drawable.p459579215h);
        p459579215.setExactRow(3);
        p459579215.setExactColumn(5);
        p459579215.getSize().reset(126.6667f, 153.3333f);
        p459579215.getPositionOffset().reset(-76.66666f, -76.66666f);
        p459579215.setIsUseAbsolutePosition(true);
        p459579215.setIsUseAbsoluteSize(true);
        p459579215.getImage().setIsUseAbsolutePosition(true);
        p459579215.getImage().setIsUseAbsoluteSize(true);
        p459579215.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p459579215.resetPosition();
        getSpiritList().add(p459579215);
    }

    /* access modifiers changed from: package-private */
    public void c2042882337(Context c) {
        Puzzle p2042882337 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2042882337(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2042882337(editor, this);
            }
        };
        p2042882337.setID(2042882337);
        p2042882337.setName("2042882337");
        p2042882337.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2042882337);
        this.Desktop.RandomlyPlaced(p2042882337);
        p2042882337.setTopEdgeType(EdgeType.Concave);
        p2042882337.setBottomEdgeType(EdgeType.Flat);
        p2042882337.setLeftEdgeType(EdgeType.Concave);
        p2042882337.setRightEdgeType(EdgeType.Concave);
        p2042882337.setTopExactPuzzleID(459579215);
        p2042882337.setBottomExactPuzzleID(-1);
        p2042882337.setLeftExactPuzzleID(1149214648);
        p2042882337.setRightExactPuzzleID(1462457296);
        p2042882337.getDisplayImage().loadImageFromResource(c, R.drawable.p2042882337h);
        p2042882337.setExactRow(4);
        p2042882337.setExactColumn(5);
        p2042882337.getSize().reset(100.0f, 100.0f);
        p2042882337.getPositionOffset().reset(-50.0f, -50.0f);
        p2042882337.setIsUseAbsolutePosition(true);
        p2042882337.setIsUseAbsoluteSize(true);
        p2042882337.getImage().setIsUseAbsolutePosition(true);
        p2042882337.getImage().setIsUseAbsoluteSize(true);
        p2042882337.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2042882337.resetPosition();
        getSpiritList().add(p2042882337);
    }

    /* access modifiers changed from: package-private */
    public void c1917798868(Context c) {
        Puzzle p1917798868 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1917798868(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1917798868(editor, this);
            }
        };
        p1917798868.setID(1917798868);
        p1917798868.setName("1917798868");
        p1917798868.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1917798868);
        this.Desktop.RandomlyPlaced(p1917798868);
        p1917798868.setTopEdgeType(EdgeType.Flat);
        p1917798868.setBottomEdgeType(EdgeType.Convex);
        p1917798868.setLeftEdgeType(EdgeType.Concave);
        p1917798868.setRightEdgeType(EdgeType.Concave);
        p1917798868.setTopExactPuzzleID(-1);
        p1917798868.setBottomExactPuzzleID(686898821);
        p1917798868.setLeftExactPuzzleID(286405353);
        p1917798868.setRightExactPuzzleID(1321273556);
        p1917798868.getDisplayImage().loadImageFromResource(c, R.drawable.p1917798868h);
        p1917798868.setExactRow(0);
        p1917798868.setExactColumn(6);
        p1917798868.getSize().reset(100.0f, 126.6667f);
        p1917798868.getPositionOffset().reset(-50.0f, -50.0f);
        p1917798868.setIsUseAbsolutePosition(true);
        p1917798868.setIsUseAbsoluteSize(true);
        p1917798868.getImage().setIsUseAbsolutePosition(true);
        p1917798868.getImage().setIsUseAbsoluteSize(true);
        p1917798868.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1917798868.resetPosition();
        getSpiritList().add(p1917798868);
    }

    /* access modifiers changed from: package-private */
    public void c686898821(Context c) {
        Puzzle p686898821 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load686898821(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save686898821(editor, this);
            }
        };
        p686898821.setID(686898821);
        p686898821.setName("686898821");
        p686898821.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p686898821);
        this.Desktop.RandomlyPlaced(p686898821);
        p686898821.setTopEdgeType(EdgeType.Concave);
        p686898821.setBottomEdgeType(EdgeType.Convex);
        p686898821.setLeftEdgeType(EdgeType.Convex);
        p686898821.setRightEdgeType(EdgeType.Convex);
        p686898821.setTopExactPuzzleID(1917798868);
        p686898821.setBottomExactPuzzleID(1019488942);
        p686898821.setLeftExactPuzzleID(1627103793);
        p686898821.setRightExactPuzzleID(765706416);
        p686898821.getDisplayImage().loadImageFromResource(c, R.drawable.p686898821h);
        p686898821.setExactRow(1);
        p686898821.setExactColumn(6);
        p686898821.getSize().reset(153.3333f, 126.6667f);
        p686898821.getPositionOffset().reset(-76.66666f, -50.0f);
        p686898821.setIsUseAbsolutePosition(true);
        p686898821.setIsUseAbsoluteSize(true);
        p686898821.getImage().setIsUseAbsolutePosition(true);
        p686898821.getImage().setIsUseAbsoluteSize(true);
        p686898821.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p686898821.resetPosition();
        getSpiritList().add(p686898821);
    }

    /* access modifiers changed from: package-private */
    public void c1019488942(Context c) {
        Puzzle p1019488942 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1019488942(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1019488942(editor, this);
            }
        };
        p1019488942.setID(1019488942);
        p1019488942.setName("1019488942");
        p1019488942.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1019488942);
        this.Desktop.RandomlyPlaced(p1019488942);
        p1019488942.setTopEdgeType(EdgeType.Concave);
        p1019488942.setBottomEdgeType(EdgeType.Concave);
        p1019488942.setLeftEdgeType(EdgeType.Convex);
        p1019488942.setRightEdgeType(EdgeType.Concave);
        p1019488942.setTopExactPuzzleID(686898821);
        p1019488942.setBottomExactPuzzleID(285089697);
        p1019488942.setLeftExactPuzzleID(255769629);
        p1019488942.setRightExactPuzzleID(19475550);
        p1019488942.getDisplayImage().loadImageFromResource(c, R.drawable.p1019488942h);
        p1019488942.setExactRow(2);
        p1019488942.setExactColumn(6);
        p1019488942.getSize().reset(126.6667f, 100.0f);
        p1019488942.getPositionOffset().reset(-76.66666f, -50.0f);
        p1019488942.setIsUseAbsolutePosition(true);
        p1019488942.setIsUseAbsoluteSize(true);
        p1019488942.getImage().setIsUseAbsolutePosition(true);
        p1019488942.getImage().setIsUseAbsoluteSize(true);
        p1019488942.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1019488942.resetPosition();
        getSpiritList().add(p1019488942);
    }

    /* access modifiers changed from: package-private */
    public void c285089697(Context c) {
        Puzzle p285089697 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load285089697(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save285089697(editor, this);
            }
        };
        p285089697.setID(285089697);
        p285089697.setName("285089697");
        p285089697.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p285089697);
        this.Desktop.RandomlyPlaced(p285089697);
        p285089697.setTopEdgeType(EdgeType.Convex);
        p285089697.setBottomEdgeType(EdgeType.Convex);
        p285089697.setLeftEdgeType(EdgeType.Convex);
        p285089697.setRightEdgeType(EdgeType.Convex);
        p285089697.setTopExactPuzzleID(1019488942);
        p285089697.setBottomExactPuzzleID(1462457296);
        p285089697.setLeftExactPuzzleID(459579215);
        p285089697.setRightExactPuzzleID(177713629);
        p285089697.getDisplayImage().loadImageFromResource(c, R.drawable.p285089697h);
        p285089697.setExactRow(3);
        p285089697.setExactColumn(6);
        p285089697.getSize().reset(153.3333f, 153.3333f);
        p285089697.getPositionOffset().reset(-76.66666f, -76.66666f);
        p285089697.setIsUseAbsolutePosition(true);
        p285089697.setIsUseAbsoluteSize(true);
        p285089697.getImage().setIsUseAbsolutePosition(true);
        p285089697.getImage().setIsUseAbsoluteSize(true);
        p285089697.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p285089697.resetPosition();
        getSpiritList().add(p285089697);
    }

    /* access modifiers changed from: package-private */
    public void c1462457296(Context c) {
        Puzzle p1462457296 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1462457296(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1462457296(editor, this);
            }
        };
        p1462457296.setID(1462457296);
        p1462457296.setName("1462457296");
        p1462457296.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1462457296);
        this.Desktop.RandomlyPlaced(p1462457296);
        p1462457296.setTopEdgeType(EdgeType.Concave);
        p1462457296.setBottomEdgeType(EdgeType.Flat);
        p1462457296.setLeftEdgeType(EdgeType.Convex);
        p1462457296.setRightEdgeType(EdgeType.Concave);
        p1462457296.setTopExactPuzzleID(285089697);
        p1462457296.setBottomExactPuzzleID(-1);
        p1462457296.setLeftExactPuzzleID(2042882337);
        p1462457296.setRightExactPuzzleID(1539547768);
        p1462457296.getDisplayImage().loadImageFromResource(c, R.drawable.p1462457296h);
        p1462457296.setExactRow(4);
        p1462457296.setExactColumn(6);
        p1462457296.getSize().reset(126.6667f, 100.0f);
        p1462457296.getPositionOffset().reset(-76.66666f, -50.0f);
        p1462457296.setIsUseAbsolutePosition(true);
        p1462457296.setIsUseAbsoluteSize(true);
        p1462457296.getImage().setIsUseAbsolutePosition(true);
        p1462457296.getImage().setIsUseAbsoluteSize(true);
        p1462457296.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1462457296.resetPosition();
        getSpiritList().add(p1462457296);
    }

    /* access modifiers changed from: package-private */
    public void c1321273556(Context c) {
        Puzzle p1321273556 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1321273556(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1321273556(editor, this);
            }
        };
        p1321273556.setID(1321273556);
        p1321273556.setName("1321273556");
        p1321273556.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1321273556);
        this.Desktop.RandomlyPlaced(p1321273556);
        p1321273556.setTopEdgeType(EdgeType.Flat);
        p1321273556.setBottomEdgeType(EdgeType.Convex);
        p1321273556.setLeftEdgeType(EdgeType.Convex);
        p1321273556.setRightEdgeType(EdgeType.Flat);
        p1321273556.setTopExactPuzzleID(-1);
        p1321273556.setBottomExactPuzzleID(765706416);
        p1321273556.setLeftExactPuzzleID(1917798868);
        p1321273556.setRightExactPuzzleID(-1);
        p1321273556.getDisplayImage().loadImageFromResource(c, R.drawable.p1321273556h);
        p1321273556.setExactRow(0);
        p1321273556.setExactColumn(7);
        p1321273556.getSize().reset(126.6667f, 126.6667f);
        p1321273556.getPositionOffset().reset(-76.66666f, -50.0f);
        p1321273556.setIsUseAbsolutePosition(true);
        p1321273556.setIsUseAbsoluteSize(true);
        p1321273556.getImage().setIsUseAbsolutePosition(true);
        p1321273556.getImage().setIsUseAbsoluteSize(true);
        p1321273556.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1321273556.resetPosition();
        getSpiritList().add(p1321273556);
    }

    /* access modifiers changed from: package-private */
    public void c765706416(Context c) {
        Puzzle p765706416 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load765706416(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save765706416(editor, this);
            }
        };
        p765706416.setID(765706416);
        p765706416.setName("765706416");
        p765706416.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p765706416);
        this.Desktop.RandomlyPlaced(p765706416);
        p765706416.setTopEdgeType(EdgeType.Concave);
        p765706416.setBottomEdgeType(EdgeType.Concave);
        p765706416.setLeftEdgeType(EdgeType.Concave);
        p765706416.setRightEdgeType(EdgeType.Flat);
        p765706416.setTopExactPuzzleID(1321273556);
        p765706416.setBottomExactPuzzleID(19475550);
        p765706416.setLeftExactPuzzleID(686898821);
        p765706416.setRightExactPuzzleID(-1);
        p765706416.getDisplayImage().loadImageFromResource(c, R.drawable.p765706416h);
        p765706416.setExactRow(1);
        p765706416.setExactColumn(7);
        p765706416.getSize().reset(100.0f, 100.0f);
        p765706416.getPositionOffset().reset(-50.0f, -50.0f);
        p765706416.setIsUseAbsolutePosition(true);
        p765706416.setIsUseAbsoluteSize(true);
        p765706416.getImage().setIsUseAbsolutePosition(true);
        p765706416.getImage().setIsUseAbsoluteSize(true);
        p765706416.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p765706416.resetPosition();
        getSpiritList().add(p765706416);
    }

    /* access modifiers changed from: package-private */
    public void c19475550(Context c) {
        Puzzle p19475550 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load19475550(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save19475550(editor, this);
            }
        };
        p19475550.setID(19475550);
        p19475550.setName("19475550");
        p19475550.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p19475550);
        this.Desktop.RandomlyPlaced(p19475550);
        p19475550.setTopEdgeType(EdgeType.Convex);
        p19475550.setBottomEdgeType(EdgeType.Convex);
        p19475550.setLeftEdgeType(EdgeType.Convex);
        p19475550.setRightEdgeType(EdgeType.Flat);
        p19475550.setTopExactPuzzleID(765706416);
        p19475550.setBottomExactPuzzleID(177713629);
        p19475550.setLeftExactPuzzleID(1019488942);
        p19475550.setRightExactPuzzleID(-1);
        p19475550.getDisplayImage().loadImageFromResource(c, R.drawable.p19475550h);
        p19475550.setExactRow(2);
        p19475550.setExactColumn(7);
        p19475550.getSize().reset(126.6667f, 153.3333f);
        p19475550.getPositionOffset().reset(-76.66666f, -76.66666f);
        p19475550.setIsUseAbsolutePosition(true);
        p19475550.setIsUseAbsoluteSize(true);
        p19475550.getImage().setIsUseAbsolutePosition(true);
        p19475550.getImage().setIsUseAbsoluteSize(true);
        p19475550.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p19475550.resetPosition();
        getSpiritList().add(p19475550);
    }

    /* access modifiers changed from: package-private */
    public void c177713629(Context c) {
        Puzzle p177713629 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load177713629(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save177713629(editor, this);
            }
        };
        p177713629.setID(177713629);
        p177713629.setName("177713629");
        p177713629.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p177713629);
        this.Desktop.RandomlyPlaced(p177713629);
        p177713629.setTopEdgeType(EdgeType.Concave);
        p177713629.setBottomEdgeType(EdgeType.Convex);
        p177713629.setLeftEdgeType(EdgeType.Concave);
        p177713629.setRightEdgeType(EdgeType.Flat);
        p177713629.setTopExactPuzzleID(19475550);
        p177713629.setBottomExactPuzzleID(1539547768);
        p177713629.setLeftExactPuzzleID(285089697);
        p177713629.setRightExactPuzzleID(-1);
        p177713629.getDisplayImage().loadImageFromResource(c, R.drawable.p177713629h);
        p177713629.setExactRow(3);
        p177713629.setExactColumn(7);
        p177713629.getSize().reset(100.0f, 126.6667f);
        p177713629.getPositionOffset().reset(-50.0f, -50.0f);
        p177713629.setIsUseAbsolutePosition(true);
        p177713629.setIsUseAbsoluteSize(true);
        p177713629.getImage().setIsUseAbsolutePosition(true);
        p177713629.getImage().setIsUseAbsoluteSize(true);
        p177713629.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p177713629.resetPosition();
        getSpiritList().add(p177713629);
    }

    /* access modifiers changed from: package-private */
    public void c1539547768(Context c) {
        Puzzle p1539547768 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1539547768(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1539547768(editor, this);
            }
        };
        p1539547768.setID(1539547768);
        p1539547768.setName("1539547768");
        p1539547768.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1539547768);
        this.Desktop.RandomlyPlaced(p1539547768);
        p1539547768.setTopEdgeType(EdgeType.Concave);
        p1539547768.setBottomEdgeType(EdgeType.Flat);
        p1539547768.setLeftEdgeType(EdgeType.Convex);
        p1539547768.setRightEdgeType(EdgeType.Flat);
        p1539547768.setTopExactPuzzleID(177713629);
        p1539547768.setBottomExactPuzzleID(-1);
        p1539547768.setLeftExactPuzzleID(1462457296);
        p1539547768.setRightExactPuzzleID(-1);
        p1539547768.getDisplayImage().loadImageFromResource(c, R.drawable.p1539547768h);
        p1539547768.setExactRow(4);
        p1539547768.setExactColumn(7);
        p1539547768.getSize().reset(126.6667f, 100.0f);
        p1539547768.getPositionOffset().reset(-76.66666f, -50.0f);
        p1539547768.setIsUseAbsolutePosition(true);
        p1539547768.setIsUseAbsoluteSize(true);
        p1539547768.getImage().setIsUseAbsolutePosition(true);
        p1539547768.getImage().setIsUseAbsoluteSize(true);
        p1539547768.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1539547768.resetPosition();
        getSpiritList().add(p1539547768);
    }
}
