package com.skyd.core.game.crosswisewar;

public interface IValuable {
    int getNeedPay();
}
