package com.skyd.core.game.crosswisewar;

public interface IMagic extends IValuable {
    ICooling getCooling();

    IWarrior getTarget();

    void setTarget(IWarrior iWarrior);
}
