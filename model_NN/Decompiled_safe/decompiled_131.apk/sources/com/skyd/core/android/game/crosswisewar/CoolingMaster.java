package com.skyd.core.android.game.crosswisewar;

import java.util.HashMap;

public class CoolingMaster {
    static HashMap<Class<?>, Cooling> _CoolingMap;

    public static Cooling getCooling(Class<?> className) {
        if (_CoolingMap.containsKey(className)) {
            return _CoolingMap.get(className);
        }
        return null;
    }

    public static void initCooling(Class<?> className, int baseCoolingTime) {
        if (!_CoolingMap.containsKey(className)) {
            Cooling c = new Cooling();
            c.setBaseCoolingTime(baseCoolingTime);
            _CoolingMap.put(className, c);
        }
    }
}
