package com.skyd.core.android.game;

import android.graphics.Color;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GameColorTest {
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testGetIntValue() {
        GameColor c = new GameColor();
        Assert.assertEquals((long) c.getIntValue(), -16777216);
        c.setAlpha(0);
        Assert.assertEquals((long) c.getIntValue(), 0);
        c.setBlue(12);
        Assert.assertEquals((long) c.getIntValue(), 12);
    }

    @Test
    public void testSetIntValue() {
        GameColor c = new GameColor(-16776961);
        Color.blue(-16776961);
        Assert.assertEquals((long) c.getAlpha(), 255);
        Assert.assertEquals((long) c.getBlue(), 255);
        c.setIntValue(-16645372);
        Assert.assertEquals((long) c.getAlpha(), 255);
        Assert.assertEquals((long) c.getBlue(), 4);
        Assert.assertEquals((long) c.getGreen(), 3);
    }

    @Test
    public void testGetClone() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testResetARGB() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testResetRGB() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testResetAHSB() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testResetHSB() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testGetAlpha() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testGetRed() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testGetGreen() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testGetBlue() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testSetAlpha() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testSetRed() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testSetGreen() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testSetBlue() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testGetHSB() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testGetHue() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testGetSaturation() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testGetBrightness() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testSetHue() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testSetSaturation() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testSetBrightness() {
        Assert.fail("Not yet implemented");
    }
}
