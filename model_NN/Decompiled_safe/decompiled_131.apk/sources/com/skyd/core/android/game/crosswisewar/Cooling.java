package com.skyd.core.android.game.crosswisewar;

import com.skyd.core.game.crosswisewar.ICooling;

public class Cooling implements ICooling {
    private int _BaseCoolingTime = 0;
    private int _CoolingTime = 0;

    public int getBaseCoolingTime() {
        return this._BaseCoolingTime;
    }

    public void setBaseCoolingTime(int value) {
        this._BaseCoolingTime = value;
    }

    public void setBaseCoolingTimeToDefault() {
        setBaseCoolingTime(0);
    }

    public int getCoolingTime() {
        return this._CoolingTime;
    }

    public void setCoolingTime(int value) {
        this._CoolingTime = value;
    }

    public void setCoolingTimeToDefault() {
        setCoolingTime(0);
    }

    public boolean getIsCanUse() {
        return getCoolingTime() <= 0;
    }

    public void resetCoolingTime() {
        setCoolingTime(getBaseCoolingTime());
    }

    public void update() {
        setCoolingTime(getCoolingTime() - 1);
    }
}
