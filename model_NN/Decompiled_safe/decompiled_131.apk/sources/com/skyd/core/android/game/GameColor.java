package com.skyd.core.android.game;

import android.graphics.Color;

public class GameColor {
    private int _IntValue = -16777216;

    public int getIntValue() {
        return this._IntValue;
    }

    public void setIntValue(int value) {
        this._IntValue = value;
    }

    public void setIntValueToDefault() {
        setIntValue(-16777216);
    }

    public GameColor getClone() {
        return new GameColor(getIntValue());
    }

    /* access modifiers changed from: protected */
    public Object clone() throws CloneNotSupportedException {
        return getClone();
    }

    public String toString() {
        return "[A:" + getAlpha() + ",R:" + getRed() + ",G:" + getGreen() + ",B:" + getBlue() + "]";
    }

    public GameColor() {
    }

    public GameColor(int color) {
        setIntValue(color);
    }

    public GameColor(int alpha, int red, int green, int blue) {
        resetARGB(alpha, red, green, blue);
    }

    public GameColor(int alpha, float hue, float saturation, float brightness) {
        resetAHSB(alpha, hue, saturation, brightness);
    }

    public void resetARGB(int alpha, int red, int green, int blue) {
        setAlpha(alpha);
        resetRGB(red, green, blue);
    }

    public void resetRGB(int red, int green, int blue) {
        setRed(red);
        setGreen(green);
        setBlue(blue);
    }

    public void resetAHSB(int alpha, float hue, float saturation, float brightness) {
        setIntValue(Color.HSVToColor(alpha, new float[]{hue, saturation, brightness}));
    }

    public void resetHSB(float hue, float saturation, float brightness) {
        resetAHSB(getAlpha(), hue, saturation, brightness);
    }

    public int getAlpha() {
        return Color.alpha(getIntValue());
    }

    public int getRed() {
        return Color.red(getIntValue());
    }

    public int getGreen() {
        return Color.green(getIntValue());
    }

    public int getBlue() {
        return Color.blue(getIntValue());
    }

    public void setAlpha(int value) {
        setIntValue((getIntValue() & 16777215) | (fixValue(value, 0, 255) << 24));
    }

    public void setRed(int value) {
        setIntValue((getIntValue() & -16711681) | (fixValue(value, 0, 255) << 16));
    }

    public void setGreen(int value) {
        setIntValue((getIntValue() & -65281) | (fixValue(value, 0, 255) << 8));
    }

    public void setBlue(int value) {
        setIntValue((getIntValue() & -256) | fixValue(value, 0, 255));
    }

    private int fixValue(int value, int min, int max) {
        if (value <= min) {
            return min;
        }
        if (value >= max) {
            return max;
        }
        return value;
    }

    private float fixValue(float value, float min, float max) {
        if (value <= min) {
            return min;
        }
        if (value >= max) {
            return max;
        }
        return value;
    }

    public float[] getHSB() {
        float[] hsv = new float[3];
        Color.RGBToHSV(getRed(), getGreen(), getBlue(), hsv);
        return hsv;
    }

    public float getHue() {
        return getHSB()[0];
    }

    public float getSaturation() {
        return getHSB()[1];
    }

    public float getBrightness() {
        return getHSB()[2];
    }

    public void setHue(float value) {
        float v = fixValue(value, 0.0f, 360.0f);
        float[] hsb = getHSB();
        resetHSB(v, hsb[1], hsb[2]);
    }

    public void setSaturation(float value) {
        float v = fixValue(value, 0.0f, 1.0f);
        float[] hsb = getHSB();
        resetHSB(hsb[0], v, hsb[2]);
    }

    public void setBrightness(float value) {
        float v = fixValue(value, 0.0f, 1.0f);
        float[] hsb = getHSB();
        resetHSB(hsb[0], hsb[1], v);
    }
}
