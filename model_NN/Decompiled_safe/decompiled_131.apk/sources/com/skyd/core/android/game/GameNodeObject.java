package com.skyd.core.android.game;

import android.graphics.Canvas;
import android.graphics.Rect;

public abstract class GameNodeObject extends GameObject {
    /* access modifiers changed from: protected */
    public void drawChilds(Canvas c, Rect drawArea) {
        GameObject o = getDisplayContentChild();
        if (o != null) {
            drawChild(o, c, drawArea);
        }
    }

    /* access modifiers changed from: protected */
    public void drawSelf(Canvas c, Rect drawArea) {
    }

    /* access modifiers changed from: protected */
    public void updateChilds() {
        GameObject o = getDisplayContentChild();
        if (o != null) {
            o.update();
        }
    }

    /* access modifiers changed from: protected */
    public void updateSelf() {
    }
}
