package com.skyd.core.android.game;

import com.skyd.core.android.game.GameObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class GameObjectList<T extends GameObject> implements Iterator<T>, Iterable<T> {
    private static final long serialVersionUID = -5847464784170298290L;
    private ArrayList<OnAddItemListener<T>> _AddItemListenerList = null;
    private ArrayList<OnChangedListener> _ChangedListenerList = null;
    private GameObject _CommonParent = null;
    ArrayList<T> _List = new ArrayList<>();
    ArrayList<T> _MIRROR;
    private ArrayList<OnRemoveItemListener<T>> _RemoveItemListenerList = null;

    public interface OnAddItemListener<T> {
        void OnAddItemEvent(Object obj, Object obj2);
    }

    public interface OnChangedListener {
        void OnChangedEvent(Object obj, int i);
    }

    public interface OnRemoveItemListener<T> {
        void OnRemoveItemEvent(Object obj, Object obj2);
    }

    public GameObjectList(GameObject parent) {
        setCommonParent(parent);
    }

    public GameObject getCommonParent() {
        return this._CommonParent;
    }

    public void setCommonParent(GameObject value) {
        this._CommonParent = value;
    }

    public void setCommonParentToDefault() {
        setCommonParent(null);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void add(int r3, T r4) {
        /*
            r2 = this;
            r2.onAddItem(r4)
            com.skyd.core.android.game.GameObject r1 = r2.getCommonParent()     // Catch:{ GameException -> 0x0019 }
            r4.setParent(r1)     // Catch:{ GameException -> 0x0019 }
        L_0x000a:
            java.util.ArrayList<T> r1 = r2._List
            r1.add(r3, r4)
            java.util.ArrayList<T> r1 = r2._List
            int r1 = r1.size()
            r2.onChanged(r1)
            return
        L_0x0019:
            r1 = move-exception
            r0 = r1
            r0.printStackTrace()
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.skyd.core.android.game.GameObjectList.add(int, com.skyd.core.android.game.GameObject):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean add(T r4) {
        /*
            r3 = this;
            r3.onAddItem(r4)
            com.skyd.core.android.game.GameObject r2 = r3.getCommonParent()     // Catch:{ GameException -> 0x001a }
            r4.setParent(r2)     // Catch:{ GameException -> 0x001a }
        L_0x000a:
            java.util.ArrayList<T> r2 = r3._List
            boolean r0 = r2.add(r4)
            java.util.ArrayList<T> r2 = r3._List
            int r2 = r2.size()
            r3.onChanged(r2)
            return r0
        L_0x001a:
            r2 = move-exception
            r1 = r2
            r1.printStackTrace()
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.skyd.core.android.game.GameObjectList.add(com.skyd.core.android.game.GameObject):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean addAll(java.util.Collection<? extends T> r6) {
        /*
            r5 = this;
            java.util.Iterator r3 = r6.iterator()
        L_0x0004:
            boolean r4 = r3.hasNext()
            if (r4 != 0) goto L_0x001a
            java.util.ArrayList<T> r3 = r5._List
            boolean r0 = r3.addAll(r6)
            java.util.ArrayList<T> r3 = r5._List
            int r3 = r3.size()
            r5.onChanged(r3)
            return r0
        L_0x001a:
            java.lang.Object r2 = r3.next()
            com.skyd.core.android.game.GameObject r2 = (com.skyd.core.android.game.GameObject) r2
            r5.onAddItem(r2)
            com.skyd.core.android.game.GameObject r4 = r5.getCommonParent()     // Catch:{ GameException -> 0x002b }
            r2.setParent(r4)     // Catch:{ GameException -> 0x002b }
            goto L_0x0004
        L_0x002b:
            r4 = move-exception
            r1 = r4
            r1.printStackTrace()
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.skyd.core.android.game.GameObjectList.addAll(java.util.Collection):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean addAll(int r6, java.util.Collection<? extends T> r7) {
        /*
            r5 = this;
            java.util.Iterator r3 = r7.iterator()
        L_0x0004:
            boolean r4 = r3.hasNext()
            if (r4 != 0) goto L_0x001a
            java.util.ArrayList<T> r3 = r5._List
            boolean r0 = r3.addAll(r6, r7)
            java.util.ArrayList<T> r3 = r5._List
            int r3 = r3.size()
            r5.onChanged(r3)
            return r0
        L_0x001a:
            java.lang.Object r2 = r3.next()
            com.skyd.core.android.game.GameObject r2 = (com.skyd.core.android.game.GameObject) r2
            r5.onAddItem(r2)
            com.skyd.core.android.game.GameObject r4 = r5.getCommonParent()     // Catch:{ GameException -> 0x002b }
            r2.setParent(r4)     // Catch:{ GameException -> 0x002b }
            goto L_0x0004
        L_0x002b:
            r4 = move-exception
            r1 = r4
            r1.printStackTrace()
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.skyd.core.android.game.GameObjectList.addAll(int, java.util.Collection):boolean");
    }

    public T get(int index) {
        return (GameObject) this._List.get(index);
    }

    public T remove(int index) {
        onRemoveItem(get(index));
        T r = (GameObject) this._List.remove(index);
        onChanged(this._List.size());
        return r;
    }

    public boolean remove(T o) {
        onRemoveItem(o);
        boolean b = this._List.remove(o);
        onChanged(this._List.size());
        return b;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public T set(int r4, T r5) {
        /*
            r3 = this;
            com.skyd.core.android.game.GameObject r2 = r3.get(r4)
            r3.onRemoveItem(r2)
            r3.onAddItem(r5)
            com.skyd.core.android.game.GameObject r2 = r3.get(r4)     // Catch:{ GameException -> 0x002a }
            r2.setParentToDefault()     // Catch:{ GameException -> 0x002a }
            com.skyd.core.android.game.GameObject r2 = r3.getCommonParent()     // Catch:{ GameException -> 0x002a }
            r5.setParent(r2)     // Catch:{ GameException -> 0x002a }
        L_0x0018:
            java.util.ArrayList<T> r2 = r3._List
            java.lang.Object r1 = r2.set(r4, r5)
            com.skyd.core.android.game.GameObject r1 = (com.skyd.core.android.game.GameObject) r1
            java.util.ArrayList<T> r2 = r3._List
            int r2 = r2.size()
            r3.onChanged(r2)
            return r1
        L_0x002a:
            r2 = move-exception
            r0 = r2
            r0.printStackTrace()
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.skyd.core.android.game.GameObjectList.set(int, com.skyd.core.android.game.GameObject):com.skyd.core.android.game.GameObject");
    }

    public boolean removeAll(Collection<T> c) {
        for (T f : c) {
            onRemoveItem(f);
        }
        boolean b = this._List.removeAll(c);
        onChanged(this._List.size());
        return b;
    }

    public boolean retainAll(Collection<T> c) {
        Iterator<T> it = this._List.iterator();
        while (it.hasNext()) {
            T f = (GameObject) it.next();
            if (!c.contains(f)) {
                onRemoveItem(f);
            }
        }
        boolean b = this._List.retainAll(c);
        onChanged(this._List.size());
        return b;
    }

    public boolean addOnChangedListener(OnChangedListener listener) {
        if (this._ChangedListenerList == null) {
            this._ChangedListenerList = new ArrayList<>();
        } else if (this._ChangedListenerList.contains(listener)) {
            return false;
        }
        this._ChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnChangedListener(OnChangedListener listener) {
        if (this._ChangedListenerList == null || !this._ChangedListenerList.contains(listener)) {
            return false;
        }
        this._ChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnChangedListeners() {
        if (this._ChangedListenerList != null) {
            this._ChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onChanged(int newSize) {
        if (this._ChangedListenerList != null) {
            Iterator<OnChangedListener> it = this._ChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnChangedEvent(this, newSize);
            }
        }
    }

    public boolean addOnAddItemListener(OnAddItemListener<T> listener) {
        if (this._AddItemListenerList == null) {
            this._AddItemListenerList = new ArrayList<>();
        } else if (this._AddItemListenerList.contains(listener)) {
            return false;
        }
        this._AddItemListenerList.add(listener);
        return true;
    }

    public boolean removeOnAddItemListener(OnAddItemListener<T> listener) {
        if (this._AddItemListenerList == null || !this._AddItemListenerList.contains(listener)) {
            return false;
        }
        this._AddItemListenerList.remove(listener);
        return true;
    }

    public void clearOnAddItemListeners() {
        if (this._AddItemListenerList != null) {
            this._AddItemListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onAddItem(T newItem) {
        if (this._AddItemListenerList != null) {
            Iterator<OnAddItemListener<T>> it = this._AddItemListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnAddItemEvent(this, newItem);
            }
        }
    }

    public boolean addOnRemoveItemListener(OnRemoveItemListener<T> listener) {
        if (this._RemoveItemListenerList == null) {
            this._RemoveItemListenerList = new ArrayList<>();
        } else if (this._RemoveItemListenerList.contains(listener)) {
            return false;
        }
        this._RemoveItemListenerList.add(listener);
        return true;
    }

    public boolean removeOnRemoveItemListener(OnRemoveItemListener<T> listener) {
        if (this._RemoveItemListenerList == null || !this._RemoveItemListenerList.contains(listener)) {
            return false;
        }
        this._RemoveItemListenerList.remove(listener);
        return true;
    }

    public void clearOnRemoveItemListeners() {
        if (this._RemoveItemListenerList != null) {
            this._RemoveItemListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onRemoveItem(T oldItem) {
        if (this._RemoveItemListenerList != null) {
            Iterator<OnRemoveItemListener<T>> it = this._RemoveItemListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnRemoveItemEvent(this, oldItem);
            }
        }
    }

    public boolean hasNext() {
        return this._List.iterator().hasNext();
    }

    public T next() {
        return (GameObject) this._List.iterator().next();
    }

    public void remove() {
        this._List.iterator().remove();
    }

    public Iterator<T> iterator() {
        this._MIRROR = (ArrayList) this._List.clone();
        return this._MIRROR.iterator();
    }

    public void sort(Comparator<T> c) {
        Collections.sort(this._List, c);
    }

    public int size() {
        return this._List.size();
    }
}
