package com.scoreloop.client.android.core.spi.twitter;

import com.scoreloop.client.android.core.spi.AuthRequest;
import com.scoreloop.client.android.core.spi.AuthRequestDelegate;
import com.scoreloop.client.android.core.util.HTTPUtils;
import com.scoreloop.client.android.core.util.OAuthBuilder;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.json.JSONException;
import org.json.JSONObject;

class b extends AuthRequest {
    private JSONObject a;

    b(AuthRequestDelegate authRequestDelegate) {
        super(authRequestDelegate);
    }

    private void a(int i, String str) {
        a(new IllegalStateException(new Integer(i).toString() + " ; " + str));
    }

    private void a(Throwable th) {
        a().a(this, th);
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2) {
        OAuthBuilder oAuthBuilder = new OAuthBuilder();
        oAuthBuilder.a("1fKnfLmiZ2hDnn3mxr1Gg");
        oAuthBuilder.b(str);
        try {
            oAuthBuilder.a(new URL("http://twitter.com/oauth/access_token"));
            a((HttpUriRequest) oAuthBuilder.a("gPm0dGnuOkwLtZ8mr3y8AjUSdVjF1d0yXJFeQm0xExY", str2));
        } catch (MalformedURLException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: protected */
    public void a(HttpResponse httpResponse) {
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        if (statusCode != 200) {
            a(statusCode, (String) null);
            return;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            for (String split : HTTPUtils.a(httpResponse).split("&")) {
                String[] split2 = split.split("=");
                if (split2.length != 2) {
                    throw new IllegalStateException("serious error parsin twitter response, this should not happen");
                }
                jSONObject.put(split2[0], split2[1]);
            }
        } catch (IOException e) {
            a(0, "invalid result to token request");
        } catch (JSONException e2) {
            a(0, "invalid result to token request");
        }
        if (this.a != null) {
            throw new IllegalStateException("responseParameters must be nil");
        }
        this.a = jSONObject;
        a().a(this);
    }

    /* access modifiers changed from: protected */
    public void a(HttpResponse httpResponse, Throwable th) {
        a(th);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        OAuthBuilder oAuthBuilder = new OAuthBuilder();
        oAuthBuilder.a("1fKnfLmiZ2hDnn3mxr1Gg");
        try {
            oAuthBuilder.a(new URL("http://twitter.com/oauth/request_token"));
            a((HttpUriRequest) oAuthBuilder.a("gPm0dGnuOkwLtZ8mr3y8AjUSdVjF1d0yXJFeQm0xExY", (String) null));
        } catch (MalformedURLException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: package-private */
    public String c() {
        try {
            return this.a.getString("oauth_token");
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: package-private */
    public String d() {
        try {
            return this.a.getString("oauth_token_secret");
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: package-private */
    public String e() {
        try {
            return this.a.getString("user_id");
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }
}
