package com.scoreloop.client.android.core.server;

class b extends Exception {
    private static final long serialVersionUID = -2669708330654835714L;

    public b() {
    }

    public b(String str) {
        super(str);
    }

    public b(String str, Throwable th) {
        super(str, th);
    }

    public b(Throwable th) {
        super(th);
    }
}
