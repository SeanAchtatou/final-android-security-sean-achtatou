package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_1_0;
import com.scoreloop.client.android.core.PublishedFor__2_0_0;
import com.scoreloop.client.android.core.model.Device;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GamesController extends RequestController {
    private final a<Game> b = new a<>();
    private boolean c = true;
    private String d = null;
    private User e = null;

    private class a extends Request {
        private final Device b;
        private final int c;
        private final int d;
        private final String e;
        private final User f;

        public a(RequestCompletionCallback requestCompletionCallback, User user, String str, Device device, int i, int i2) {
            super(requestCompletionCallback);
            this.c = i;
            this.d = i2;
            this.f = user;
            this.e = str;
            this.b = device;
        }

        public String a() {
            if (this.f == null) {
                return "/service/games";
            }
            return String.format("/service/users/%s/games", this.f.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("offset", this.c);
                jSONObject.put("per_page", this.d);
                if (this.e != null) {
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("id", this.e);
                    if (this.b != null) {
                        SearchSpec searchSpec = new SearchSpec();
                        searchSpec.a(new c("playable_by_device", d.EXACT, this.b.getIdentifier()));
                        jSONObject2.put("definition", searchSpec.a());
                    }
                    jSONObject.put("search_list", jSONObject2);
                }
                return jSONObject;
            } catch (JSONException e2) {
                throw new IllegalStateException(e2);
            }
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    @PublishedFor__1_1_0
    public GamesController(RequestControllerObserver requestControllerObserver) {
        super(null, requestControllerObserver, false);
    }

    @PublishedFor__1_1_0
    public GamesController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver, false);
    }

    private void a(int i) {
        a_();
        this.b.a(i);
        a aVar = new a(e(), this.e, this.d, this.c ? f().a() : null, i, this.b.a());
        aVar.a(600000);
        b(aVar);
    }

    private void a(String str, User user) {
        if (this.d != str) {
            b();
            this.d = str;
            this.e = user;
        }
        a(0);
    }

    private void b() {
        this.e = null;
        this.d = null;
        this.b.j();
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        if (response.f() != 200 || response.d() == null) {
            throw new Exception("Request failed");
        }
        ArrayList arrayList = new ArrayList();
        JSONArray d2 = response.d();
        int length = d2.length();
        Game game = f().getGame();
        for (int i = 0; i < length; i++) {
            JSONObject jSONObject = d2.getJSONObject(i).getJSONObject(Game.a);
            Game game2 = new Game(jSONObject);
            if (game == null || !game.equals(game2)) {
                arrayList.add(game2);
            } else {
                game.a(jSONObject);
                arrayList.add(game);
            }
        }
        this.b.a(arrayList);
        return true;
    }

    @PublishedFor__1_1_0
    public List<Game> getGames() {
        return this.b.d();
    }

    @PublishedFor__1_1_0
    public boolean getLoadsDevicesPlatformOnly() {
        return this.c;
    }

    @PublishedFor__1_1_0
    public int getRangeLength() {
        return this.b.f();
    }

    @PublishedFor__1_1_0
    public boolean hasNextRange() {
        return this.b.h();
    }

    @PublishedFor__1_1_0
    public boolean hasPreviousRange() {
        return this.b.i();
    }

    @PublishedFor__2_0_0
    public void loadFirstRange() {
        a(0);
    }

    @PublishedFor__1_1_0
    public void loadNextRange() {
        if (!hasNextRange()) {
            throw new IllegalStateException("There's no next range");
        } else if (this.b.g()) {
            a(this.b.c());
        } else {
            a(0);
        }
    }

    @PublishedFor__1_1_0
    public void loadPreviousRange() {
        if (!hasPreviousRange()) {
            throw new IllegalStateException("There's no previous range");
        } else if (this.b.g()) {
            a(this.b.e());
        } else {
            a(0);
        }
    }

    @PublishedFor__1_1_0
    public void loadRangeForBuddies() {
        a("#buddy_games", g());
    }

    @PublishedFor__1_1_0
    public void loadRangeForFeatured() {
        a("#featured_games", (User) null);
    }

    @PublishedFor__1_1_0
    public void loadRangeForNew() {
        a("#new_games", (User) null);
    }

    @PublishedFor__1_1_0
    public void loadRangeForPopular() {
        a("#popular_games", (User) null);
    }

    @PublishedFor__1_1_0
    public void loadRangeForUser(User user) {
        if (user == null) {
            throw new IllegalArgumentException("user must not be null");
        }
        if (!user.equals(this.e)) {
            b();
            this.e = user;
        }
        a(0);
    }

    @PublishedFor__1_1_0
    public void setLoadsDevicesPlatformOnly(boolean z) {
        this.c = z;
    }

    @PublishedFor__1_1_0
    public void setRangeLength(int i) {
        this.b.b(i);
    }
}
