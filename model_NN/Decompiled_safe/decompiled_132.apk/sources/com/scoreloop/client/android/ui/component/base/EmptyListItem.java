package com.scoreloop.client.android.ui.component.base;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.skyd.bestpuzzle.n1668.R;

public class EmptyListItem extends BaseListItem {
    public EmptyListItem(Context context, String title) {
        super(context, null, title);
    }

    public int getType() {
        return 16;
    }

    public View getView(View view, ViewGroup parent) {
        if (view == null) {
            view = getLayoutInflater().inflate((int) R.layout.sl_list_item_empty, (ViewGroup) null);
        }
        ((TextView) view.findViewById(R.id.sl_title)).setText(getTitle());
        return view;
    }

    public boolean isEnabled() {
        return false;
    }
}
