package com.skyd.bestpuzzle;

import com.skyd.core.vector.Vector2DF;

public interface IOnDesktop {
    Desktop getDesktop();

    Vector2DF getPositionInDesktop();

    void setDesktop(Desktop desktop);
}
