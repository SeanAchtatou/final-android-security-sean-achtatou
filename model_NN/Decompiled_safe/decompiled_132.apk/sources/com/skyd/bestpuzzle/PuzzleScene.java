package com.skyd.bestpuzzle;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.widget.Toast;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.skyd.bestpuzzle.n1668.R;
import com.skyd.core.android.game.GameImageSpirit;
import com.skyd.core.android.game.GameMaster;
import com.skyd.core.android.game.GameObject;
import com.skyd.core.android.game.GameScene;
import com.skyd.core.android.game.GameSpirit;
import com.skyd.core.draw.DrawHelper;
import com.skyd.core.vector.Vector2DF;
import com.skyd.core.vector.VectorRect2DF;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class PuzzleScene extends GameScene {
    public Controller Controller;
    public Desktop Desktop;
    boolean IsDrawingPuzzle;
    boolean IsDrawnSelectRect;
    public GameImageSpirit MoveButton;
    public UI MoveButtonSlot;
    public OriginalImage OriginalImage;
    public long PastTime;
    public long StartTime;
    public UI SubmitScoreButton;
    /* access modifiers changed from: private */
    public RectF SubmitScoreButtonRect;
    public GameSpirit Time;
    public UI ViewFullButton;
    /* access modifiers changed from: private */
    public RectF ViewFullButtonRect;
    public UI ZoomInButton;
    public UI ZoomOutButton;
    private boolean _IsFinished = false;
    private ArrayList<OnIsFinishedChangedListener> _IsFinishedChangedListenerList = null;
    long savetime = Long.MIN_VALUE;
    RectF selectRect;

    public interface OnIsFinishedChangedListener {
        void OnIsFinishedChangedEvent(Object obj, boolean z);
    }

    public PuzzleScene() {
        setMaxDrawCacheLayer(25.0f);
        this.StartTime = new Date().getTime();
        loadGameState();
    }

    public void loadGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        this.PastTime = c.getPastTime().longValue();
        this._IsFinished = c.getIsFinished().booleanValue();
    }

    public void saveGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        if (getIsFinished()) {
            c.setPastTime(Long.valueOf(this.PastTime));
        } else {
            c.setPastTime(Long.valueOf(this.PastTime + (new Date().getTime() - this.StartTime)));
        }
        c.setIsFinished(Boolean.valueOf(getIsFinished()));
    }

    public void setFinished() {
        if (!getIsFinished()) {
            this.SubmitScoreButton.show();
        }
        setIsFinished(true);
        this.PastTime += new Date().getTime() - this.StartTime;
    }

    public boolean getIsFinished() {
        return this._IsFinished;
    }

    private void setIsFinished(boolean value) {
        onIsFinishedChanged(value);
        this._IsFinished = value;
    }

    private void setIsFinishedToDefault() {
        setIsFinished(false);
    }

    public boolean addOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null) {
            this._IsFinishedChangedListenerList = new ArrayList<>();
        } else if (this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null || !this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnIsFinishedChangedListeners() {
        if (this._IsFinishedChangedListenerList != null) {
            this._IsFinishedChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onIsFinishedChanged(boolean newValue) {
        if (this._IsFinishedChangedListenerList != null) {
            Iterator<OnIsFinishedChangedListener> it = this._IsFinishedChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnIsFinishedChangedEvent(this, newValue);
            }
        }
    }

    public int getTargetCacheID() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void drawChilds(Canvas c, Rect drawArea) {
        this.IsDrawingPuzzle = false;
        this.IsDrawnSelectRect = false;
        super.drawChilds(c, drawArea);
    }

    /* access modifiers changed from: protected */
    public boolean onDrawingChild(GameObject child, Canvas c, Rect drawArea) {
        float lvl = ((GameSpirit) child).getLevel();
        if (!this.IsDrawingPuzzle && lvl <= 10.0f) {
            this.IsDrawingPuzzle = true;
            c.setMatrix(this.Desktop.getMatrix());
        } else if (lvl > 10.0f && this.IsDrawingPuzzle) {
            this.IsDrawingPuzzle = false;
            c.setMatrix(new Matrix());
        }
        if (lvl > 25.0f && this.selectRect != null && !this.IsDrawnSelectRect) {
            this.IsDrawnSelectRect = true;
            Paint p = new Paint();
            p.setARGB(100, 180, 225, 255);
            c.drawRect(this.selectRect, p);
        }
        return super.onDrawingChild(child, c, drawArea);
    }

    public void load(Context c) {
        loadOriginalImage(c);
        loadDesktop(c);
        loadInterface(c);
        loadPuzzle(c);
        loadPuzzleState();
        this.Desktop.updateCurrentObserveAreaRectF();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF
     arg types: [int, int, float, float, int]
     candidates:
      com.skyd.core.draw.DrawHelper.calculateScaleSize(double, double, double, double, boolean):com.skyd.core.vector.Vector2D
      com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF */
    public void loadOriginalImage(Context c) {
        this.OriginalImage = new OriginalImage() {
            /* access modifiers changed from: protected */
            public boolean onDrawing(Canvas c, Rect drawArea) {
                c.drawARGB(180, 0, 0, 0);
                return super.onDrawing(c, drawArea);
            }
        };
        this.OriginalImage.setLevel(50.0f);
        this.OriginalImage.setName("OriginalImage");
        this.OriginalImage.getSize().resetWith(DrawHelper.calculateScaleSize(384.0f, 240.0f, (float) (GameMaster.getScreenWidth() - 40), (float) (GameMaster.getScreenHeight() - 40), true));
        this.OriginalImage.setTotalSeparateColumn(8);
        this.OriginalImage.setTotalSeparateRow(5);
        this.OriginalImage.getOriginalSize().reset(800.0f, 500.0f);
        this.OriginalImage.hide();
        this.OriginalImage.setIsUseAbsolutePosition(true);
        this.OriginalImage.setIsUseAbsoluteSize(true);
        this.OriginalImage.getImage().setIsUseAbsolutePosition(true);
        this.OriginalImage.getImage().setIsUseAbsoluteSize(true);
        this.OriginalImage.getPosition().reset(((float) (GameMaster.getScreenWidth() / 2)) - (this.OriginalImage.getSize().getX() / 2.0f), ((float) (GameMaster.getScreenHeight() / 2)) - (this.OriginalImage.getSize().getY() / 2.0f));
        getSpiritList().add(this.OriginalImage);
    }

    public void loadDesktop(Context c) {
        this.Desktop = new Desktop();
        this.Desktop.setLevel(-9999.0f);
        this.Desktop.setName("Desktop");
        this.Desktop.getSize().reset(1600.0f, 1000.0f);
        this.Desktop.getCurrentObservePosition().reset(800.0f, 500.0f);
        this.Desktop.setOriginalImage(this.OriginalImage);
        this.Desktop.setIsUseAbsolutePosition(true);
        this.Desktop.setIsUseAbsoluteSize(true);
        this.Desktop.ColorAPaint = new Paint();
        this.Desktop.ColorAPaint.setColor(Color.argb(255, 32, 30, 43));
        this.Desktop.ColorBPaint = new Paint();
        this.Desktop.ColorBPaint.setColor(Color.argb(255, 26, 24, 37));
        getSpiritList().add(this.Desktop);
    }

    public void loadPuzzle(Context c) {
        this.Controller = new Controller();
        this.Controller.setLevel(10.0f);
        this.Controller.setName("Controller");
        this.Controller.setDesktop(this.Desktop);
        this.Controller.setIsUseAbsolutePosition(true);
        this.Controller.setIsUseAbsoluteSize(true);
        getSpiritList().add(this.Controller);
        Puzzle.getRealitySize().reset(100.0f, 100.0f);
        Puzzle.getMaxRadius().reset(76.66666f, 76.66666f);
        c474949285(c);
        c1305600334(c);
        c1924439304(c);
        c207279706(c);
        c1658961480(c);
        c1121619901(c);
        c490167355(c);
        c1935722945(c);
        c2136562270(c);
        c285595532(c);
        c933218803(c);
        c1139808316(c);
        c1450669099(c);
        c336359812(c);
        c1851712086(c);
        c825795961(c);
        c1145161698(c);
        c542503285(c);
        c1180948098(c);
        c1959844210(c);
        c1401085694(c);
        c515165135(c);
        c498297356(c);
        c2137297214(c);
        c1236637002(c);
        c232638785(c);
        c1806539944(c);
        c100338288(c);
        c65399397(c);
        c1573799327(c);
        c209836318(c);
        c1981138172(c);
        c616197156(c);
        c159001623(c);
        c1629498411(c);
        c657135789(c);
        c343948412(c);
        c1514068830(c);
        c1438166234(c);
        c444589784(c);
    }

    public void loadInterface(Context c) {
        this.MoveButton = new GameImageSpirit();
        this.MoveButton.setLevel(22.0f);
        this.MoveButton.getImage().loadImageFromResource(c, R.drawable.movebutton);
        this.MoveButton.getSize().reset(155.0f, 155.0f);
        this.MoveButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButton);
        final Vector2DF mvds = this.MoveButton.getDisplaySize();
        mvds.scale(0.5f);
        this.MoveButton.getPosition().reset(5.0f + mvds.getX(), ((float) (GameMaster.getScreenHeight() - 5)) - mvds.getY());
        this.MoveButton.getPositionOffset().resetWith(mvds.negateNew());
        this.MoveButtonSlot = new UI() {
            Vector2DF movevector;
            boolean needmove;

            public void executive(Vector2DF point) {
                this.movevector = point.minusNew(getPosition()).restrainLength(mvds.getX() * 0.4f);
                this.needmove = true;
            }

            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.needmove) {
                    PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition().plusNew(this.movevector));
                    PuzzleScene.this.Desktop.getCurrentObservePosition().plus(this.movevector);
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < mvds.getX() + 5.0f;
            }

            public void reset() {
                this.needmove = false;
                PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition());
                PuzzleScene.this.refreshDrawCacheBitmap();
            }
        };
        this.MoveButtonSlot.setLevel(21.0f);
        this.MoveButtonSlot.getImage().loadImageFromResource(c, R.drawable.movebuttonslot);
        this.MoveButtonSlot.getSize().reset(155.0f, 155.0f);
        this.MoveButtonSlot.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButtonSlot);
        this.MoveButtonSlot.getPosition().resetWith(this.MoveButton.getPosition());
        this.MoveButtonSlot.getPositionOffset().resetWith(this.MoveButton.getPositionOffset());
        this.ZoomInButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.min(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.min(double, double):double}
              ClspMth{java.lang.Math.min(long, long):long}
              ClspMth{java.lang.Math.min(int, int):int}
              ClspMth{java.lang.Math.min(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.min(1.3f, PuzzleScene.this.Desktop.getZoom() * 1.05f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }
        };
        this.ZoomInButton.setLevel(21.0f);
        this.ZoomInButton.getImage().loadImageFromResource(c, R.drawable.zoombutton1);
        this.ZoomInButton.getSize().reset(77.0f, 77.0f);
        this.ZoomInButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomInButton);
        Vector2DF zibds = this.ZoomInButton.getDisplaySize().scale(0.5f);
        this.ZoomInButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 50)) - zibds.getX(), ((float) (GameMaster.getScreenHeight() - 10)) - zibds.getY());
        this.ZoomInButton.getPositionOffset().resetWith(zibds.negateNew());
        this.ZoomOutButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.max(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.max(double, double):double}
              ClspMth{java.lang.Math.max(int, int):int}
              ClspMth{java.lang.Math.max(long, long):long}
              ClspMth{java.lang.Math.max(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.max(0.3f, PuzzleScene.this.Desktop.getZoom() * 0.95f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                    GameMaster.log(this, Float.valueOf(PuzzleScene.this.Desktop.getCurrentObserveAreaRectF().width()));
                }
                super.updateSelf();
            }
        };
        this.ZoomOutButton.setLevel(21.0f);
        this.ZoomOutButton.getImage().loadImageFromResource(c, R.drawable.zoombutton2);
        this.ZoomOutButton.getSize().reset(77.0f, 77.0f);
        this.ZoomOutButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomOutButton);
        Vector2DF zobds = this.ZoomOutButton.getDisplaySize().scale(0.5f);
        this.ZoomOutButton.getPosition().reset((((float) (GameMaster.getScreenWidth() - 100)) - zobds.getX()) - (zibds.getX() * 2.0f), ((float) (GameMaster.getScreenHeight() - 10)) - zobds.getY());
        this.ZoomOutButton.getPositionOffset().resetWith(zobds.negateNew());
        this.ViewFullButton = new UI() {
            public void executive(Vector2DF point) {
                if (PuzzleScene.this.OriginalImage.getImage().getImage() == null) {
                    PuzzleScene.this.OriginalImage.getImage().loadImageFromResource(GameMaster.getContext(), R.drawable.oim);
                }
                PuzzleScene.this.OriginalImage.show();
            }

            public boolean isInArea(Vector2DF point) {
                return point.isIn(PuzzleScene.this.ViewFullButtonRect);
            }

            public void reset() {
                PuzzleScene.this.OriginalImage.hide();
            }
        };
        this.ViewFullButton.getImage().loadImageFromResource(c, R.drawable.viewfull);
        this.ViewFullButton.setLevel(21.0f);
        this.ViewFullButton.setIsUseAbsolutePosition(true);
        this.ViewFullButton.getSize().reset(83.0f, 38.0f);
        getSpiritList().add(this.ViewFullButton);
        this.ViewFullButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 15)) - this.ViewFullButton.getDisplaySize().getX(), 15.0f);
        this.ViewFullButtonRect = new VectorRect2DF(this.ViewFullButton.getPosition(), this.ViewFullButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton = new UI() {
            public void executive(Vector2DF point) {
                Score score = new Score(Double.valueOf(Math.ceil((double) (PuzzleScene.this.PastTime / 1000))), null);
                score.setMode(17);
                ScoreloopManagerSingleton.get().onGamePlayEnded(score);
                hide();
                Toast.makeText(GameMaster.getContext(), (int) R.string.Submitting, 1).show();
            }

            public boolean isInArea(Vector2DF point) {
                return getVisibleOriginalValue() && point.isIn(PuzzleScene.this.SubmitScoreButtonRect);
            }

            public void reset() {
            }
        };
        this.SubmitScoreButton.getImage().loadImageFromResource(c, R.drawable.submitscore);
        this.SubmitScoreButton.setLevel(21.0f);
        this.SubmitScoreButton.setIsUseAbsolutePosition(true);
        this.SubmitScoreButton.getSize().reset(114.0f, 40.0f);
        getSpiritList().add(this.SubmitScoreButton);
        this.SubmitScoreButton.getPosition().reset(15.0f, 60.0f);
        this.SubmitScoreButtonRect = new VectorRect2DF(this.SubmitScoreButton.getPosition(), this.SubmitScoreButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton.hide();
        this.Time = new GameSpirit() {
            DecimalFormat FMT = new DecimalFormat("00");
            Paint p1;
            Paint p2;

            public GameObject getDisplayContentChild() {
                return null;
            }

            /* access modifiers changed from: protected */
            public void drawSelf(Canvas c, Rect drawArea) {
                if (this.p1 == null) {
                    this.p1 = new Paint();
                    this.p1.setARGB(160, 255, 255, 255);
                    this.p1.setAntiAlias(true);
                    this.p1.setTextSize(28.0f);
                    this.p1.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                if (this.p2 == null) {
                    this.p2 = new Paint();
                    this.p2.setARGB(100, 0, 0, 0);
                    this.p2.setAntiAlias(true);
                    this.p2.setTextSize(28.0f);
                    this.p2.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                long t = PuzzleScene.this.getIsFinished() ? PuzzleScene.this.PastTime : PuzzleScene.this.PastTime + (new Date().getTime() - PuzzleScene.this.StartTime);
                long h = t / 3600000;
                long m = (t - (((1000 * h) * 60) * 60)) / 60000;
                String str = String.valueOf(this.FMT.format(h)) + ":" + this.FMT.format(m) + ":" + this.FMT.format(((t - (((1000 * h) * 60) * 60)) - ((1000 * m) * 60)) / 1000) + (PuzzleScene.this.getIsFinished() ? " Finished" : "");
                c.drawText(str, 16.0f, 44.0f, this.p2);
                c.drawText(str, 15.0f, 43.0f, this.p1);
                super.drawSelf(c, drawArea);
            }
        };
        this.Time.setLevel(40.0f);
        getSpiritList().add(this.Time);
    }

    public float getMaxPuzzleLevel() {
        float maxlvl = 0.0f;
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getLevel() <= 10.0f) {
                maxlvl = Math.max(f.getLevel(), maxlvl);
            }
        }
        return maxlvl;
    }

    public Puzzle getTouchPuzzle(Vector2DF v) {
        Vector2DF rv = this.Desktop.reMapPoint(v);
        Puzzle o = null;
        float olen = 99999.0f;
        for (int i = getSpiritList().size() - 1; i >= 0; i--) {
            if (getSpiritList().get(i) instanceof Puzzle) {
                Puzzle p = (Puzzle) getSpiritList().get(i);
                float len = p.getPositionInDesktop().minusNew(rv).getLength();
                if (len <= Puzzle.getRealitySize().getX() * 0.4f) {
                    return p;
                }
                if (len <= Puzzle.getRealitySize().getX() && olen > len) {
                    o = p;
                    olen = len;
                }
            }
        }
        return o;
    }

    public void startDrawSelectRect(Vector2DF startDragPoint, Vector2DF v) {
        this.selectRect = new VectorRect2DF(startDragPoint, v.minusNew(startDragPoint)).getFixedRectF();
    }

    public void stopDrawSelectRect() {
        this.selectRect = null;
    }

    public ArrayList<Puzzle> getSelectPuzzle(Vector2DF startDragPoint, Vector2DF v) {
        Vector2DF rs = this.Desktop.reMapPoint(startDragPoint);
        ArrayList<Puzzle> l = new ArrayList<>();
        RectF rect = new VectorRect2DF(rs, this.Desktop.reMapPoint(v).minusNew(rs)).getFixedRectF();
        float xo = Puzzle.getRealitySize().getX() / 3.0f;
        float yo = Puzzle.getRealitySize().getY() / 3.0f;
        RectF sr = new RectF(rect.left - xo, rect.top - yo, rect.right + xo, rect.bottom + yo);
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getPositionInDesktop().isIn(sr)) {
                l.add(f);
            }
        }
        return l;
    }

    public void loadPuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences s = c.getSharedPreferences();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().load(c, s);
        }
    }

    public void savePuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences.Editor e = c.getSharedPreferences().edit();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().save(c, e);
        }
        e.commit();
    }

    public void reset() {
        this.Controller.reset();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            f.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
            this.Desktop.RandomlyPlaced(f);
        }
        refreshDrawCacheBitmap();
        setIsFinishedToDefault();
        this.StartTime = new Date().getTime();
        this.PastTime = 0;
        this.SubmitScoreButton.hide();
        saveGameState();
        savePuzzleState();
    }

    /* access modifiers changed from: package-private */
    public void c474949285(Context c) {
        Puzzle p474949285 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load474949285(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save474949285(editor, this);
            }
        };
        p474949285.setID(474949285);
        p474949285.setName("474949285");
        p474949285.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p474949285);
        this.Desktop.RandomlyPlaced(p474949285);
        p474949285.setTopEdgeType(EdgeType.Flat);
        p474949285.setBottomEdgeType(EdgeType.Convex);
        p474949285.setLeftEdgeType(EdgeType.Flat);
        p474949285.setRightEdgeType(EdgeType.Concave);
        p474949285.setTopExactPuzzleID(-1);
        p474949285.setBottomExactPuzzleID(1305600334);
        p474949285.setLeftExactPuzzleID(-1);
        p474949285.setRightExactPuzzleID(1121619901);
        p474949285.getDisplayImage().loadImageFromResource(c, R.drawable.p474949285h);
        p474949285.setExactRow(0);
        p474949285.setExactColumn(0);
        p474949285.getSize().reset(100.0f, 126.6667f);
        p474949285.getPositionOffset().reset(-50.0f, -50.0f);
        p474949285.setIsUseAbsolutePosition(true);
        p474949285.setIsUseAbsoluteSize(true);
        p474949285.getImage().setIsUseAbsolutePosition(true);
        p474949285.getImage().setIsUseAbsoluteSize(true);
        p474949285.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p474949285.resetPosition();
        getSpiritList().add(p474949285);
    }

    /* access modifiers changed from: package-private */
    public void c1305600334(Context c) {
        Puzzle p1305600334 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1305600334(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1305600334(editor, this);
            }
        };
        p1305600334.setID(1305600334);
        p1305600334.setName("1305600334");
        p1305600334.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1305600334);
        this.Desktop.RandomlyPlaced(p1305600334);
        p1305600334.setTopEdgeType(EdgeType.Concave);
        p1305600334.setBottomEdgeType(EdgeType.Concave);
        p1305600334.setLeftEdgeType(EdgeType.Flat);
        p1305600334.setRightEdgeType(EdgeType.Convex);
        p1305600334.setTopExactPuzzleID(474949285);
        p1305600334.setBottomExactPuzzleID(1924439304);
        p1305600334.setLeftExactPuzzleID(-1);
        p1305600334.setRightExactPuzzleID(490167355);
        p1305600334.getDisplayImage().loadImageFromResource(c, R.drawable.p1305600334h);
        p1305600334.setExactRow(1);
        p1305600334.setExactColumn(0);
        p1305600334.getSize().reset(126.6667f, 100.0f);
        p1305600334.getPositionOffset().reset(-50.0f, -50.0f);
        p1305600334.setIsUseAbsolutePosition(true);
        p1305600334.setIsUseAbsoluteSize(true);
        p1305600334.getImage().setIsUseAbsolutePosition(true);
        p1305600334.getImage().setIsUseAbsoluteSize(true);
        p1305600334.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1305600334.resetPosition();
        getSpiritList().add(p1305600334);
    }

    /* access modifiers changed from: package-private */
    public void c1924439304(Context c) {
        Puzzle p1924439304 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1924439304(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1924439304(editor, this);
            }
        };
        p1924439304.setID(1924439304);
        p1924439304.setName("1924439304");
        p1924439304.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1924439304);
        this.Desktop.RandomlyPlaced(p1924439304);
        p1924439304.setTopEdgeType(EdgeType.Convex);
        p1924439304.setBottomEdgeType(EdgeType.Concave);
        p1924439304.setLeftEdgeType(EdgeType.Flat);
        p1924439304.setRightEdgeType(EdgeType.Convex);
        p1924439304.setTopExactPuzzleID(1305600334);
        p1924439304.setBottomExactPuzzleID(207279706);
        p1924439304.setLeftExactPuzzleID(-1);
        p1924439304.setRightExactPuzzleID(1935722945);
        p1924439304.getDisplayImage().loadImageFromResource(c, R.drawable.p1924439304h);
        p1924439304.setExactRow(2);
        p1924439304.setExactColumn(0);
        p1924439304.getSize().reset(126.6667f, 126.6667f);
        p1924439304.getPositionOffset().reset(-50.0f, -76.66666f);
        p1924439304.setIsUseAbsolutePosition(true);
        p1924439304.setIsUseAbsoluteSize(true);
        p1924439304.getImage().setIsUseAbsolutePosition(true);
        p1924439304.getImage().setIsUseAbsoluteSize(true);
        p1924439304.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1924439304.resetPosition();
        getSpiritList().add(p1924439304);
    }

    /* access modifiers changed from: package-private */
    public void c207279706(Context c) {
        Puzzle p207279706 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load207279706(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save207279706(editor, this);
            }
        };
        p207279706.setID(207279706);
        p207279706.setName("207279706");
        p207279706.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p207279706);
        this.Desktop.RandomlyPlaced(p207279706);
        p207279706.setTopEdgeType(EdgeType.Convex);
        p207279706.setBottomEdgeType(EdgeType.Concave);
        p207279706.setLeftEdgeType(EdgeType.Flat);
        p207279706.setRightEdgeType(EdgeType.Concave);
        p207279706.setTopExactPuzzleID(1924439304);
        p207279706.setBottomExactPuzzleID(1658961480);
        p207279706.setLeftExactPuzzleID(-1);
        p207279706.setRightExactPuzzleID(2136562270);
        p207279706.getDisplayImage().loadImageFromResource(c, R.drawable.p207279706h);
        p207279706.setExactRow(3);
        p207279706.setExactColumn(0);
        p207279706.getSize().reset(100.0f, 126.6667f);
        p207279706.getPositionOffset().reset(-50.0f, -76.66666f);
        p207279706.setIsUseAbsolutePosition(true);
        p207279706.setIsUseAbsoluteSize(true);
        p207279706.getImage().setIsUseAbsolutePosition(true);
        p207279706.getImage().setIsUseAbsoluteSize(true);
        p207279706.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p207279706.resetPosition();
        getSpiritList().add(p207279706);
    }

    /* access modifiers changed from: package-private */
    public void c1658961480(Context c) {
        Puzzle p1658961480 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1658961480(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1658961480(editor, this);
            }
        };
        p1658961480.setID(1658961480);
        p1658961480.setName("1658961480");
        p1658961480.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1658961480);
        this.Desktop.RandomlyPlaced(p1658961480);
        p1658961480.setTopEdgeType(EdgeType.Convex);
        p1658961480.setBottomEdgeType(EdgeType.Flat);
        p1658961480.setLeftEdgeType(EdgeType.Flat);
        p1658961480.setRightEdgeType(EdgeType.Concave);
        p1658961480.setTopExactPuzzleID(207279706);
        p1658961480.setBottomExactPuzzleID(-1);
        p1658961480.setLeftExactPuzzleID(-1);
        p1658961480.setRightExactPuzzleID(285595532);
        p1658961480.getDisplayImage().loadImageFromResource(c, R.drawable.p1658961480h);
        p1658961480.setExactRow(4);
        p1658961480.setExactColumn(0);
        p1658961480.getSize().reset(100.0f, 126.6667f);
        p1658961480.getPositionOffset().reset(-50.0f, -76.66666f);
        p1658961480.setIsUseAbsolutePosition(true);
        p1658961480.setIsUseAbsoluteSize(true);
        p1658961480.getImage().setIsUseAbsolutePosition(true);
        p1658961480.getImage().setIsUseAbsoluteSize(true);
        p1658961480.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1658961480.resetPosition();
        getSpiritList().add(p1658961480);
    }

    /* access modifiers changed from: package-private */
    public void c1121619901(Context c) {
        Puzzle p1121619901 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1121619901(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1121619901(editor, this);
            }
        };
        p1121619901.setID(1121619901);
        p1121619901.setName("1121619901");
        p1121619901.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1121619901);
        this.Desktop.RandomlyPlaced(p1121619901);
        p1121619901.setTopEdgeType(EdgeType.Flat);
        p1121619901.setBottomEdgeType(EdgeType.Concave);
        p1121619901.setLeftEdgeType(EdgeType.Convex);
        p1121619901.setRightEdgeType(EdgeType.Concave);
        p1121619901.setTopExactPuzzleID(-1);
        p1121619901.setBottomExactPuzzleID(490167355);
        p1121619901.setLeftExactPuzzleID(474949285);
        p1121619901.setRightExactPuzzleID(933218803);
        p1121619901.getDisplayImage().loadImageFromResource(c, R.drawable.p1121619901h);
        p1121619901.setExactRow(0);
        p1121619901.setExactColumn(1);
        p1121619901.getSize().reset(126.6667f, 100.0f);
        p1121619901.getPositionOffset().reset(-76.66666f, -50.0f);
        p1121619901.setIsUseAbsolutePosition(true);
        p1121619901.setIsUseAbsoluteSize(true);
        p1121619901.getImage().setIsUseAbsolutePosition(true);
        p1121619901.getImage().setIsUseAbsoluteSize(true);
        p1121619901.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1121619901.resetPosition();
        getSpiritList().add(p1121619901);
    }

    /* access modifiers changed from: package-private */
    public void c490167355(Context c) {
        Puzzle p490167355 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load490167355(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save490167355(editor, this);
            }
        };
        p490167355.setID(490167355);
        p490167355.setName("490167355");
        p490167355.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p490167355);
        this.Desktop.RandomlyPlaced(p490167355);
        p490167355.setTopEdgeType(EdgeType.Convex);
        p490167355.setBottomEdgeType(EdgeType.Concave);
        p490167355.setLeftEdgeType(EdgeType.Concave);
        p490167355.setRightEdgeType(EdgeType.Concave);
        p490167355.setTopExactPuzzleID(1121619901);
        p490167355.setBottomExactPuzzleID(1935722945);
        p490167355.setLeftExactPuzzleID(1305600334);
        p490167355.setRightExactPuzzleID(1139808316);
        p490167355.getDisplayImage().loadImageFromResource(c, R.drawable.p490167355h);
        p490167355.setExactRow(1);
        p490167355.setExactColumn(1);
        p490167355.getSize().reset(100.0f, 126.6667f);
        p490167355.getPositionOffset().reset(-50.0f, -76.66666f);
        p490167355.setIsUseAbsolutePosition(true);
        p490167355.setIsUseAbsoluteSize(true);
        p490167355.getImage().setIsUseAbsolutePosition(true);
        p490167355.getImage().setIsUseAbsoluteSize(true);
        p490167355.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p490167355.resetPosition();
        getSpiritList().add(p490167355);
    }

    /* access modifiers changed from: package-private */
    public void c1935722945(Context c) {
        Puzzle p1935722945 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1935722945(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1935722945(editor, this);
            }
        };
        p1935722945.setID(1935722945);
        p1935722945.setName("1935722945");
        p1935722945.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1935722945);
        this.Desktop.RandomlyPlaced(p1935722945);
        p1935722945.setTopEdgeType(EdgeType.Convex);
        p1935722945.setBottomEdgeType(EdgeType.Concave);
        p1935722945.setLeftEdgeType(EdgeType.Concave);
        p1935722945.setRightEdgeType(EdgeType.Concave);
        p1935722945.setTopExactPuzzleID(490167355);
        p1935722945.setBottomExactPuzzleID(2136562270);
        p1935722945.setLeftExactPuzzleID(1924439304);
        p1935722945.setRightExactPuzzleID(1450669099);
        p1935722945.getDisplayImage().loadImageFromResource(c, R.drawable.p1935722945h);
        p1935722945.setExactRow(2);
        p1935722945.setExactColumn(1);
        p1935722945.getSize().reset(100.0f, 126.6667f);
        p1935722945.getPositionOffset().reset(-50.0f, -76.66666f);
        p1935722945.setIsUseAbsolutePosition(true);
        p1935722945.setIsUseAbsoluteSize(true);
        p1935722945.getImage().setIsUseAbsolutePosition(true);
        p1935722945.getImage().setIsUseAbsoluteSize(true);
        p1935722945.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1935722945.resetPosition();
        getSpiritList().add(p1935722945);
    }

    /* access modifiers changed from: package-private */
    public void c2136562270(Context c) {
        Puzzle p2136562270 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2136562270(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2136562270(editor, this);
            }
        };
        p2136562270.setID(2136562270);
        p2136562270.setName("2136562270");
        p2136562270.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2136562270);
        this.Desktop.RandomlyPlaced(p2136562270);
        p2136562270.setTopEdgeType(EdgeType.Convex);
        p2136562270.setBottomEdgeType(EdgeType.Concave);
        p2136562270.setLeftEdgeType(EdgeType.Convex);
        p2136562270.setRightEdgeType(EdgeType.Convex);
        p2136562270.setTopExactPuzzleID(1935722945);
        p2136562270.setBottomExactPuzzleID(285595532);
        p2136562270.setLeftExactPuzzleID(207279706);
        p2136562270.setRightExactPuzzleID(336359812);
        p2136562270.getDisplayImage().loadImageFromResource(c, R.drawable.p2136562270h);
        p2136562270.setExactRow(3);
        p2136562270.setExactColumn(1);
        p2136562270.getSize().reset(153.3333f, 126.6667f);
        p2136562270.getPositionOffset().reset(-76.66666f, -76.66666f);
        p2136562270.setIsUseAbsolutePosition(true);
        p2136562270.setIsUseAbsoluteSize(true);
        p2136562270.getImage().setIsUseAbsolutePosition(true);
        p2136562270.getImage().setIsUseAbsoluteSize(true);
        p2136562270.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2136562270.resetPosition();
        getSpiritList().add(p2136562270);
    }

    /* access modifiers changed from: package-private */
    public void c285595532(Context c) {
        Puzzle p285595532 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load285595532(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save285595532(editor, this);
            }
        };
        p285595532.setID(285595532);
        p285595532.setName("285595532");
        p285595532.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p285595532);
        this.Desktop.RandomlyPlaced(p285595532);
        p285595532.setTopEdgeType(EdgeType.Convex);
        p285595532.setBottomEdgeType(EdgeType.Flat);
        p285595532.setLeftEdgeType(EdgeType.Convex);
        p285595532.setRightEdgeType(EdgeType.Concave);
        p285595532.setTopExactPuzzleID(2136562270);
        p285595532.setBottomExactPuzzleID(-1);
        p285595532.setLeftExactPuzzleID(1658961480);
        p285595532.setRightExactPuzzleID(1851712086);
        p285595532.getDisplayImage().loadImageFromResource(c, R.drawable.p285595532h);
        p285595532.setExactRow(4);
        p285595532.setExactColumn(1);
        p285595532.getSize().reset(126.6667f, 126.6667f);
        p285595532.getPositionOffset().reset(-76.66666f, -76.66666f);
        p285595532.setIsUseAbsolutePosition(true);
        p285595532.setIsUseAbsoluteSize(true);
        p285595532.getImage().setIsUseAbsolutePosition(true);
        p285595532.getImage().setIsUseAbsoluteSize(true);
        p285595532.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p285595532.resetPosition();
        getSpiritList().add(p285595532);
    }

    /* access modifiers changed from: package-private */
    public void c933218803(Context c) {
        Puzzle p933218803 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load933218803(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save933218803(editor, this);
            }
        };
        p933218803.setID(933218803);
        p933218803.setName("933218803");
        p933218803.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p933218803);
        this.Desktop.RandomlyPlaced(p933218803);
        p933218803.setTopEdgeType(EdgeType.Flat);
        p933218803.setBottomEdgeType(EdgeType.Convex);
        p933218803.setLeftEdgeType(EdgeType.Convex);
        p933218803.setRightEdgeType(EdgeType.Concave);
        p933218803.setTopExactPuzzleID(-1);
        p933218803.setBottomExactPuzzleID(1139808316);
        p933218803.setLeftExactPuzzleID(1121619901);
        p933218803.setRightExactPuzzleID(825795961);
        p933218803.getDisplayImage().loadImageFromResource(c, R.drawable.p933218803h);
        p933218803.setExactRow(0);
        p933218803.setExactColumn(2);
        p933218803.getSize().reset(126.6667f, 126.6667f);
        p933218803.getPositionOffset().reset(-76.66666f, -50.0f);
        p933218803.setIsUseAbsolutePosition(true);
        p933218803.setIsUseAbsoluteSize(true);
        p933218803.getImage().setIsUseAbsolutePosition(true);
        p933218803.getImage().setIsUseAbsoluteSize(true);
        p933218803.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p933218803.resetPosition();
        getSpiritList().add(p933218803);
    }

    /* access modifiers changed from: package-private */
    public void c1139808316(Context c) {
        Puzzle p1139808316 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1139808316(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1139808316(editor, this);
            }
        };
        p1139808316.setID(1139808316);
        p1139808316.setName("1139808316");
        p1139808316.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1139808316);
        this.Desktop.RandomlyPlaced(p1139808316);
        p1139808316.setTopEdgeType(EdgeType.Concave);
        p1139808316.setBottomEdgeType(EdgeType.Concave);
        p1139808316.setLeftEdgeType(EdgeType.Convex);
        p1139808316.setRightEdgeType(EdgeType.Concave);
        p1139808316.setTopExactPuzzleID(933218803);
        p1139808316.setBottomExactPuzzleID(1450669099);
        p1139808316.setLeftExactPuzzleID(490167355);
        p1139808316.setRightExactPuzzleID(1145161698);
        p1139808316.getDisplayImage().loadImageFromResource(c, R.drawable.p1139808316h);
        p1139808316.setExactRow(1);
        p1139808316.setExactColumn(2);
        p1139808316.getSize().reset(126.6667f, 100.0f);
        p1139808316.getPositionOffset().reset(-76.66666f, -50.0f);
        p1139808316.setIsUseAbsolutePosition(true);
        p1139808316.setIsUseAbsoluteSize(true);
        p1139808316.getImage().setIsUseAbsolutePosition(true);
        p1139808316.getImage().setIsUseAbsoluteSize(true);
        p1139808316.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1139808316.resetPosition();
        getSpiritList().add(p1139808316);
    }

    /* access modifiers changed from: package-private */
    public void c1450669099(Context c) {
        Puzzle p1450669099 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1450669099(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1450669099(editor, this);
            }
        };
        p1450669099.setID(1450669099);
        p1450669099.setName("1450669099");
        p1450669099.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1450669099);
        this.Desktop.RandomlyPlaced(p1450669099);
        p1450669099.setTopEdgeType(EdgeType.Convex);
        p1450669099.setBottomEdgeType(EdgeType.Convex);
        p1450669099.setLeftEdgeType(EdgeType.Convex);
        p1450669099.setRightEdgeType(EdgeType.Concave);
        p1450669099.setTopExactPuzzleID(1139808316);
        p1450669099.setBottomExactPuzzleID(336359812);
        p1450669099.setLeftExactPuzzleID(1935722945);
        p1450669099.setRightExactPuzzleID(542503285);
        p1450669099.getDisplayImage().loadImageFromResource(c, R.drawable.p1450669099h);
        p1450669099.setExactRow(2);
        p1450669099.setExactColumn(2);
        p1450669099.getSize().reset(126.6667f, 153.3333f);
        p1450669099.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1450669099.setIsUseAbsolutePosition(true);
        p1450669099.setIsUseAbsoluteSize(true);
        p1450669099.getImage().setIsUseAbsolutePosition(true);
        p1450669099.getImage().setIsUseAbsoluteSize(true);
        p1450669099.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1450669099.resetPosition();
        getSpiritList().add(p1450669099);
    }

    /* access modifiers changed from: package-private */
    public void c336359812(Context c) {
        Puzzle p336359812 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load336359812(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save336359812(editor, this);
            }
        };
        p336359812.setID(336359812);
        p336359812.setName("336359812");
        p336359812.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p336359812);
        this.Desktop.RandomlyPlaced(p336359812);
        p336359812.setTopEdgeType(EdgeType.Concave);
        p336359812.setBottomEdgeType(EdgeType.Concave);
        p336359812.setLeftEdgeType(EdgeType.Concave);
        p336359812.setRightEdgeType(EdgeType.Concave);
        p336359812.setTopExactPuzzleID(1450669099);
        p336359812.setBottomExactPuzzleID(1851712086);
        p336359812.setLeftExactPuzzleID(2136562270);
        p336359812.setRightExactPuzzleID(1180948098);
        p336359812.getDisplayImage().loadImageFromResource(c, R.drawable.p336359812h);
        p336359812.setExactRow(3);
        p336359812.setExactColumn(2);
        p336359812.getSize().reset(100.0f, 100.0f);
        p336359812.getPositionOffset().reset(-50.0f, -50.0f);
        p336359812.setIsUseAbsolutePosition(true);
        p336359812.setIsUseAbsoluteSize(true);
        p336359812.getImage().setIsUseAbsolutePosition(true);
        p336359812.getImage().setIsUseAbsoluteSize(true);
        p336359812.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p336359812.resetPosition();
        getSpiritList().add(p336359812);
    }

    /* access modifiers changed from: package-private */
    public void c1851712086(Context c) {
        Puzzle p1851712086 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1851712086(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1851712086(editor, this);
            }
        };
        p1851712086.setID(1851712086);
        p1851712086.setName("1851712086");
        p1851712086.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1851712086);
        this.Desktop.RandomlyPlaced(p1851712086);
        p1851712086.setTopEdgeType(EdgeType.Convex);
        p1851712086.setBottomEdgeType(EdgeType.Flat);
        p1851712086.setLeftEdgeType(EdgeType.Convex);
        p1851712086.setRightEdgeType(EdgeType.Convex);
        p1851712086.setTopExactPuzzleID(336359812);
        p1851712086.setBottomExactPuzzleID(-1);
        p1851712086.setLeftExactPuzzleID(285595532);
        p1851712086.setRightExactPuzzleID(1959844210);
        p1851712086.getDisplayImage().loadImageFromResource(c, R.drawable.p1851712086h);
        p1851712086.setExactRow(4);
        p1851712086.setExactColumn(2);
        p1851712086.getSize().reset(153.3333f, 126.6667f);
        p1851712086.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1851712086.setIsUseAbsolutePosition(true);
        p1851712086.setIsUseAbsoluteSize(true);
        p1851712086.getImage().setIsUseAbsolutePosition(true);
        p1851712086.getImage().setIsUseAbsoluteSize(true);
        p1851712086.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1851712086.resetPosition();
        getSpiritList().add(p1851712086);
    }

    /* access modifiers changed from: package-private */
    public void c825795961(Context c) {
        Puzzle p825795961 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load825795961(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save825795961(editor, this);
            }
        };
        p825795961.setID(825795961);
        p825795961.setName("825795961");
        p825795961.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p825795961);
        this.Desktop.RandomlyPlaced(p825795961);
        p825795961.setTopEdgeType(EdgeType.Flat);
        p825795961.setBottomEdgeType(EdgeType.Concave);
        p825795961.setLeftEdgeType(EdgeType.Convex);
        p825795961.setRightEdgeType(EdgeType.Convex);
        p825795961.setTopExactPuzzleID(-1);
        p825795961.setBottomExactPuzzleID(1145161698);
        p825795961.setLeftExactPuzzleID(933218803);
        p825795961.setRightExactPuzzleID(1401085694);
        p825795961.getDisplayImage().loadImageFromResource(c, R.drawable.p825795961h);
        p825795961.setExactRow(0);
        p825795961.setExactColumn(3);
        p825795961.getSize().reset(153.3333f, 100.0f);
        p825795961.getPositionOffset().reset(-76.66666f, -50.0f);
        p825795961.setIsUseAbsolutePosition(true);
        p825795961.setIsUseAbsoluteSize(true);
        p825795961.getImage().setIsUseAbsolutePosition(true);
        p825795961.getImage().setIsUseAbsoluteSize(true);
        p825795961.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p825795961.resetPosition();
        getSpiritList().add(p825795961);
    }

    /* access modifiers changed from: package-private */
    public void c1145161698(Context c) {
        Puzzle p1145161698 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1145161698(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1145161698(editor, this);
            }
        };
        p1145161698.setID(1145161698);
        p1145161698.setName("1145161698");
        p1145161698.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1145161698);
        this.Desktop.RandomlyPlaced(p1145161698);
        p1145161698.setTopEdgeType(EdgeType.Convex);
        p1145161698.setBottomEdgeType(EdgeType.Convex);
        p1145161698.setLeftEdgeType(EdgeType.Convex);
        p1145161698.setRightEdgeType(EdgeType.Concave);
        p1145161698.setTopExactPuzzleID(825795961);
        p1145161698.setBottomExactPuzzleID(542503285);
        p1145161698.setLeftExactPuzzleID(1139808316);
        p1145161698.setRightExactPuzzleID(515165135);
        p1145161698.getDisplayImage().loadImageFromResource(c, R.drawable.p1145161698h);
        p1145161698.setExactRow(1);
        p1145161698.setExactColumn(3);
        p1145161698.getSize().reset(126.6667f, 153.3333f);
        p1145161698.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1145161698.setIsUseAbsolutePosition(true);
        p1145161698.setIsUseAbsoluteSize(true);
        p1145161698.getImage().setIsUseAbsolutePosition(true);
        p1145161698.getImage().setIsUseAbsoluteSize(true);
        p1145161698.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1145161698.resetPosition();
        getSpiritList().add(p1145161698);
    }

    /* access modifiers changed from: package-private */
    public void c542503285(Context c) {
        Puzzle p542503285 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load542503285(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save542503285(editor, this);
            }
        };
        p542503285.setID(542503285);
        p542503285.setName("542503285");
        p542503285.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p542503285);
        this.Desktop.RandomlyPlaced(p542503285);
        p542503285.setTopEdgeType(EdgeType.Concave);
        p542503285.setBottomEdgeType(EdgeType.Convex);
        p542503285.setLeftEdgeType(EdgeType.Convex);
        p542503285.setRightEdgeType(EdgeType.Convex);
        p542503285.setTopExactPuzzleID(1145161698);
        p542503285.setBottomExactPuzzleID(1180948098);
        p542503285.setLeftExactPuzzleID(1450669099);
        p542503285.setRightExactPuzzleID(498297356);
        p542503285.getDisplayImage().loadImageFromResource(c, R.drawable.p542503285h);
        p542503285.setExactRow(2);
        p542503285.setExactColumn(3);
        p542503285.getSize().reset(153.3333f, 126.6667f);
        p542503285.getPositionOffset().reset(-76.66666f, -50.0f);
        p542503285.setIsUseAbsolutePosition(true);
        p542503285.setIsUseAbsoluteSize(true);
        p542503285.getImage().setIsUseAbsolutePosition(true);
        p542503285.getImage().setIsUseAbsoluteSize(true);
        p542503285.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p542503285.resetPosition();
        getSpiritList().add(p542503285);
    }

    /* access modifiers changed from: package-private */
    public void c1180948098(Context c) {
        Puzzle p1180948098 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1180948098(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1180948098(editor, this);
            }
        };
        p1180948098.setID(1180948098);
        p1180948098.setName("1180948098");
        p1180948098.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1180948098);
        this.Desktop.RandomlyPlaced(p1180948098);
        p1180948098.setTopEdgeType(EdgeType.Concave);
        p1180948098.setBottomEdgeType(EdgeType.Convex);
        p1180948098.setLeftEdgeType(EdgeType.Convex);
        p1180948098.setRightEdgeType(EdgeType.Convex);
        p1180948098.setTopExactPuzzleID(542503285);
        p1180948098.setBottomExactPuzzleID(1959844210);
        p1180948098.setLeftExactPuzzleID(336359812);
        p1180948098.setRightExactPuzzleID(2137297214);
        p1180948098.getDisplayImage().loadImageFromResource(c, R.drawable.p1180948098h);
        p1180948098.setExactRow(3);
        p1180948098.setExactColumn(3);
        p1180948098.getSize().reset(153.3333f, 126.6667f);
        p1180948098.getPositionOffset().reset(-76.66666f, -50.0f);
        p1180948098.setIsUseAbsolutePosition(true);
        p1180948098.setIsUseAbsoluteSize(true);
        p1180948098.getImage().setIsUseAbsolutePosition(true);
        p1180948098.getImage().setIsUseAbsoluteSize(true);
        p1180948098.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1180948098.resetPosition();
        getSpiritList().add(p1180948098);
    }

    /* access modifiers changed from: package-private */
    public void c1959844210(Context c) {
        Puzzle p1959844210 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1959844210(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1959844210(editor, this);
            }
        };
        p1959844210.setID(1959844210);
        p1959844210.setName("1959844210");
        p1959844210.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1959844210);
        this.Desktop.RandomlyPlaced(p1959844210);
        p1959844210.setTopEdgeType(EdgeType.Concave);
        p1959844210.setBottomEdgeType(EdgeType.Flat);
        p1959844210.setLeftEdgeType(EdgeType.Concave);
        p1959844210.setRightEdgeType(EdgeType.Convex);
        p1959844210.setTopExactPuzzleID(1180948098);
        p1959844210.setBottomExactPuzzleID(-1);
        p1959844210.setLeftExactPuzzleID(1851712086);
        p1959844210.setRightExactPuzzleID(1236637002);
        p1959844210.getDisplayImage().loadImageFromResource(c, R.drawable.p1959844210h);
        p1959844210.setExactRow(4);
        p1959844210.setExactColumn(3);
        p1959844210.getSize().reset(126.6667f, 100.0f);
        p1959844210.getPositionOffset().reset(-50.0f, -50.0f);
        p1959844210.setIsUseAbsolutePosition(true);
        p1959844210.setIsUseAbsoluteSize(true);
        p1959844210.getImage().setIsUseAbsolutePosition(true);
        p1959844210.getImage().setIsUseAbsoluteSize(true);
        p1959844210.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1959844210.resetPosition();
        getSpiritList().add(p1959844210);
    }

    /* access modifiers changed from: package-private */
    public void c1401085694(Context c) {
        Puzzle p1401085694 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1401085694(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1401085694(editor, this);
            }
        };
        p1401085694.setID(1401085694);
        p1401085694.setName("1401085694");
        p1401085694.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1401085694);
        this.Desktop.RandomlyPlaced(p1401085694);
        p1401085694.setTopEdgeType(EdgeType.Flat);
        p1401085694.setBottomEdgeType(EdgeType.Concave);
        p1401085694.setLeftEdgeType(EdgeType.Concave);
        p1401085694.setRightEdgeType(EdgeType.Convex);
        p1401085694.setTopExactPuzzleID(-1);
        p1401085694.setBottomExactPuzzleID(515165135);
        p1401085694.setLeftExactPuzzleID(825795961);
        p1401085694.setRightExactPuzzleID(232638785);
        p1401085694.getDisplayImage().loadImageFromResource(c, R.drawable.p1401085694h);
        p1401085694.setExactRow(0);
        p1401085694.setExactColumn(4);
        p1401085694.getSize().reset(126.6667f, 100.0f);
        p1401085694.getPositionOffset().reset(-50.0f, -50.0f);
        p1401085694.setIsUseAbsolutePosition(true);
        p1401085694.setIsUseAbsoluteSize(true);
        p1401085694.getImage().setIsUseAbsolutePosition(true);
        p1401085694.getImage().setIsUseAbsoluteSize(true);
        p1401085694.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1401085694.resetPosition();
        getSpiritList().add(p1401085694);
    }

    /* access modifiers changed from: package-private */
    public void c515165135(Context c) {
        Puzzle p515165135 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load515165135(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save515165135(editor, this);
            }
        };
        p515165135.setID(515165135);
        p515165135.setName("515165135");
        p515165135.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p515165135);
        this.Desktop.RandomlyPlaced(p515165135);
        p515165135.setTopEdgeType(EdgeType.Convex);
        p515165135.setBottomEdgeType(EdgeType.Convex);
        p515165135.setLeftEdgeType(EdgeType.Convex);
        p515165135.setRightEdgeType(EdgeType.Concave);
        p515165135.setTopExactPuzzleID(1401085694);
        p515165135.setBottomExactPuzzleID(498297356);
        p515165135.setLeftExactPuzzleID(1145161698);
        p515165135.setRightExactPuzzleID(1806539944);
        p515165135.getDisplayImage().loadImageFromResource(c, R.drawable.p515165135h);
        p515165135.setExactRow(1);
        p515165135.setExactColumn(4);
        p515165135.getSize().reset(126.6667f, 153.3333f);
        p515165135.getPositionOffset().reset(-76.66666f, -76.66666f);
        p515165135.setIsUseAbsolutePosition(true);
        p515165135.setIsUseAbsoluteSize(true);
        p515165135.getImage().setIsUseAbsolutePosition(true);
        p515165135.getImage().setIsUseAbsoluteSize(true);
        p515165135.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p515165135.resetPosition();
        getSpiritList().add(p515165135);
    }

    /* access modifiers changed from: package-private */
    public void c498297356(Context c) {
        Puzzle p498297356 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load498297356(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save498297356(editor, this);
            }
        };
        p498297356.setID(498297356);
        p498297356.setName("498297356");
        p498297356.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p498297356);
        this.Desktop.RandomlyPlaced(p498297356);
        p498297356.setTopEdgeType(EdgeType.Concave);
        p498297356.setBottomEdgeType(EdgeType.Concave);
        p498297356.setLeftEdgeType(EdgeType.Concave);
        p498297356.setRightEdgeType(EdgeType.Concave);
        p498297356.setTopExactPuzzleID(515165135);
        p498297356.setBottomExactPuzzleID(2137297214);
        p498297356.setLeftExactPuzzleID(542503285);
        p498297356.setRightExactPuzzleID(100338288);
        p498297356.getDisplayImage().loadImageFromResource(c, R.drawable.p498297356h);
        p498297356.setExactRow(2);
        p498297356.setExactColumn(4);
        p498297356.getSize().reset(100.0f, 100.0f);
        p498297356.getPositionOffset().reset(-50.0f, -50.0f);
        p498297356.setIsUseAbsolutePosition(true);
        p498297356.setIsUseAbsoluteSize(true);
        p498297356.getImage().setIsUseAbsolutePosition(true);
        p498297356.getImage().setIsUseAbsoluteSize(true);
        p498297356.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p498297356.resetPosition();
        getSpiritList().add(p498297356);
    }

    /* access modifiers changed from: package-private */
    public void c2137297214(Context c) {
        Puzzle p2137297214 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2137297214(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2137297214(editor, this);
            }
        };
        p2137297214.setID(2137297214);
        p2137297214.setName("2137297214");
        p2137297214.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2137297214);
        this.Desktop.RandomlyPlaced(p2137297214);
        p2137297214.setTopEdgeType(EdgeType.Convex);
        p2137297214.setBottomEdgeType(EdgeType.Concave);
        p2137297214.setLeftEdgeType(EdgeType.Concave);
        p2137297214.setRightEdgeType(EdgeType.Convex);
        p2137297214.setTopExactPuzzleID(498297356);
        p2137297214.setBottomExactPuzzleID(1236637002);
        p2137297214.setLeftExactPuzzleID(1180948098);
        p2137297214.setRightExactPuzzleID(65399397);
        p2137297214.getDisplayImage().loadImageFromResource(c, R.drawable.p2137297214h);
        p2137297214.setExactRow(3);
        p2137297214.setExactColumn(4);
        p2137297214.getSize().reset(126.6667f, 126.6667f);
        p2137297214.getPositionOffset().reset(-50.0f, -76.66666f);
        p2137297214.setIsUseAbsolutePosition(true);
        p2137297214.setIsUseAbsoluteSize(true);
        p2137297214.getImage().setIsUseAbsolutePosition(true);
        p2137297214.getImage().setIsUseAbsoluteSize(true);
        p2137297214.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2137297214.resetPosition();
        getSpiritList().add(p2137297214);
    }

    /* access modifiers changed from: package-private */
    public void c1236637002(Context c) {
        Puzzle p1236637002 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1236637002(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1236637002(editor, this);
            }
        };
        p1236637002.setID(1236637002);
        p1236637002.setName("1236637002");
        p1236637002.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1236637002);
        this.Desktop.RandomlyPlaced(p1236637002);
        p1236637002.setTopEdgeType(EdgeType.Convex);
        p1236637002.setBottomEdgeType(EdgeType.Flat);
        p1236637002.setLeftEdgeType(EdgeType.Concave);
        p1236637002.setRightEdgeType(EdgeType.Concave);
        p1236637002.setTopExactPuzzleID(2137297214);
        p1236637002.setBottomExactPuzzleID(-1);
        p1236637002.setLeftExactPuzzleID(1959844210);
        p1236637002.setRightExactPuzzleID(1573799327);
        p1236637002.getDisplayImage().loadImageFromResource(c, R.drawable.p1236637002h);
        p1236637002.setExactRow(4);
        p1236637002.setExactColumn(4);
        p1236637002.getSize().reset(100.0f, 126.6667f);
        p1236637002.getPositionOffset().reset(-50.0f, -76.66666f);
        p1236637002.setIsUseAbsolutePosition(true);
        p1236637002.setIsUseAbsoluteSize(true);
        p1236637002.getImage().setIsUseAbsolutePosition(true);
        p1236637002.getImage().setIsUseAbsoluteSize(true);
        p1236637002.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1236637002.resetPosition();
        getSpiritList().add(p1236637002);
    }

    /* access modifiers changed from: package-private */
    public void c232638785(Context c) {
        Puzzle p232638785 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load232638785(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save232638785(editor, this);
            }
        };
        p232638785.setID(232638785);
        p232638785.setName("232638785");
        p232638785.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p232638785);
        this.Desktop.RandomlyPlaced(p232638785);
        p232638785.setTopEdgeType(EdgeType.Flat);
        p232638785.setBottomEdgeType(EdgeType.Concave);
        p232638785.setLeftEdgeType(EdgeType.Concave);
        p232638785.setRightEdgeType(EdgeType.Convex);
        p232638785.setTopExactPuzzleID(-1);
        p232638785.setBottomExactPuzzleID(1806539944);
        p232638785.setLeftExactPuzzleID(1401085694);
        p232638785.setRightExactPuzzleID(209836318);
        p232638785.getDisplayImage().loadImageFromResource(c, R.drawable.p232638785h);
        p232638785.setExactRow(0);
        p232638785.setExactColumn(5);
        p232638785.getSize().reset(126.6667f, 100.0f);
        p232638785.getPositionOffset().reset(-50.0f, -50.0f);
        p232638785.setIsUseAbsolutePosition(true);
        p232638785.setIsUseAbsoluteSize(true);
        p232638785.getImage().setIsUseAbsolutePosition(true);
        p232638785.getImage().setIsUseAbsoluteSize(true);
        p232638785.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p232638785.resetPosition();
        getSpiritList().add(p232638785);
    }

    /* access modifiers changed from: package-private */
    public void c1806539944(Context c) {
        Puzzle p1806539944 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1806539944(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1806539944(editor, this);
            }
        };
        p1806539944.setID(1806539944);
        p1806539944.setName("1806539944");
        p1806539944.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1806539944);
        this.Desktop.RandomlyPlaced(p1806539944);
        p1806539944.setTopEdgeType(EdgeType.Convex);
        p1806539944.setBottomEdgeType(EdgeType.Concave);
        p1806539944.setLeftEdgeType(EdgeType.Convex);
        p1806539944.setRightEdgeType(EdgeType.Concave);
        p1806539944.setTopExactPuzzleID(232638785);
        p1806539944.setBottomExactPuzzleID(100338288);
        p1806539944.setLeftExactPuzzleID(515165135);
        p1806539944.setRightExactPuzzleID(1981138172);
        p1806539944.getDisplayImage().loadImageFromResource(c, R.drawable.p1806539944h);
        p1806539944.setExactRow(1);
        p1806539944.setExactColumn(5);
        p1806539944.getSize().reset(126.6667f, 126.6667f);
        p1806539944.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1806539944.setIsUseAbsolutePosition(true);
        p1806539944.setIsUseAbsoluteSize(true);
        p1806539944.getImage().setIsUseAbsolutePosition(true);
        p1806539944.getImage().setIsUseAbsoluteSize(true);
        p1806539944.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1806539944.resetPosition();
        getSpiritList().add(p1806539944);
    }

    /* access modifiers changed from: package-private */
    public void c100338288(Context c) {
        Puzzle p100338288 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load100338288(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save100338288(editor, this);
            }
        };
        p100338288.setID(100338288);
        p100338288.setName("100338288");
        p100338288.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p100338288);
        this.Desktop.RandomlyPlaced(p100338288);
        p100338288.setTopEdgeType(EdgeType.Convex);
        p100338288.setBottomEdgeType(EdgeType.Convex);
        p100338288.setLeftEdgeType(EdgeType.Convex);
        p100338288.setRightEdgeType(EdgeType.Convex);
        p100338288.setTopExactPuzzleID(1806539944);
        p100338288.setBottomExactPuzzleID(65399397);
        p100338288.setLeftExactPuzzleID(498297356);
        p100338288.setRightExactPuzzleID(616197156);
        p100338288.getDisplayImage().loadImageFromResource(c, R.drawable.p100338288h);
        p100338288.setExactRow(2);
        p100338288.setExactColumn(5);
        p100338288.getSize().reset(153.3333f, 153.3333f);
        p100338288.getPositionOffset().reset(-76.66666f, -76.66666f);
        p100338288.setIsUseAbsolutePosition(true);
        p100338288.setIsUseAbsoluteSize(true);
        p100338288.getImage().setIsUseAbsolutePosition(true);
        p100338288.getImage().setIsUseAbsoluteSize(true);
        p100338288.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p100338288.resetPosition();
        getSpiritList().add(p100338288);
    }

    /* access modifiers changed from: package-private */
    public void c65399397(Context c) {
        Puzzle p65399397 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load65399397(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save65399397(editor, this);
            }
        };
        p65399397.setID(65399397);
        p65399397.setName("65399397");
        p65399397.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p65399397);
        this.Desktop.RandomlyPlaced(p65399397);
        p65399397.setTopEdgeType(EdgeType.Concave);
        p65399397.setBottomEdgeType(EdgeType.Concave);
        p65399397.setLeftEdgeType(EdgeType.Concave);
        p65399397.setRightEdgeType(EdgeType.Convex);
        p65399397.setTopExactPuzzleID(100338288);
        p65399397.setBottomExactPuzzleID(1573799327);
        p65399397.setLeftExactPuzzleID(2137297214);
        p65399397.setRightExactPuzzleID(159001623);
        p65399397.getDisplayImage().loadImageFromResource(c, R.drawable.p65399397h);
        p65399397.setExactRow(3);
        p65399397.setExactColumn(5);
        p65399397.getSize().reset(126.6667f, 100.0f);
        p65399397.getPositionOffset().reset(-50.0f, -50.0f);
        p65399397.setIsUseAbsolutePosition(true);
        p65399397.setIsUseAbsoluteSize(true);
        p65399397.getImage().setIsUseAbsolutePosition(true);
        p65399397.getImage().setIsUseAbsoluteSize(true);
        p65399397.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p65399397.resetPosition();
        getSpiritList().add(p65399397);
    }

    /* access modifiers changed from: package-private */
    public void c1573799327(Context c) {
        Puzzle p1573799327 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1573799327(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1573799327(editor, this);
            }
        };
        p1573799327.setID(1573799327);
        p1573799327.setName("1573799327");
        p1573799327.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1573799327);
        this.Desktop.RandomlyPlaced(p1573799327);
        p1573799327.setTopEdgeType(EdgeType.Convex);
        p1573799327.setBottomEdgeType(EdgeType.Flat);
        p1573799327.setLeftEdgeType(EdgeType.Convex);
        p1573799327.setRightEdgeType(EdgeType.Concave);
        p1573799327.setTopExactPuzzleID(65399397);
        p1573799327.setBottomExactPuzzleID(-1);
        p1573799327.setLeftExactPuzzleID(1236637002);
        p1573799327.setRightExactPuzzleID(1629498411);
        p1573799327.getDisplayImage().loadImageFromResource(c, R.drawable.p1573799327h);
        p1573799327.setExactRow(4);
        p1573799327.setExactColumn(5);
        p1573799327.getSize().reset(126.6667f, 126.6667f);
        p1573799327.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1573799327.setIsUseAbsolutePosition(true);
        p1573799327.setIsUseAbsoluteSize(true);
        p1573799327.getImage().setIsUseAbsolutePosition(true);
        p1573799327.getImage().setIsUseAbsoluteSize(true);
        p1573799327.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1573799327.resetPosition();
        getSpiritList().add(p1573799327);
    }

    /* access modifiers changed from: package-private */
    public void c209836318(Context c) {
        Puzzle p209836318 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load209836318(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save209836318(editor, this);
            }
        };
        p209836318.setID(209836318);
        p209836318.setName("209836318");
        p209836318.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p209836318);
        this.Desktop.RandomlyPlaced(p209836318);
        p209836318.setTopEdgeType(EdgeType.Flat);
        p209836318.setBottomEdgeType(EdgeType.Convex);
        p209836318.setLeftEdgeType(EdgeType.Concave);
        p209836318.setRightEdgeType(EdgeType.Convex);
        p209836318.setTopExactPuzzleID(-1);
        p209836318.setBottomExactPuzzleID(1981138172);
        p209836318.setLeftExactPuzzleID(232638785);
        p209836318.setRightExactPuzzleID(657135789);
        p209836318.getDisplayImage().loadImageFromResource(c, R.drawable.p209836318h);
        p209836318.setExactRow(0);
        p209836318.setExactColumn(6);
        p209836318.getSize().reset(126.6667f, 126.6667f);
        p209836318.getPositionOffset().reset(-50.0f, -50.0f);
        p209836318.setIsUseAbsolutePosition(true);
        p209836318.setIsUseAbsoluteSize(true);
        p209836318.getImage().setIsUseAbsolutePosition(true);
        p209836318.getImage().setIsUseAbsoluteSize(true);
        p209836318.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p209836318.resetPosition();
        getSpiritList().add(p209836318);
    }

    /* access modifiers changed from: package-private */
    public void c1981138172(Context c) {
        Puzzle p1981138172 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1981138172(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1981138172(editor, this);
            }
        };
        p1981138172.setID(1981138172);
        p1981138172.setName("1981138172");
        p1981138172.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1981138172);
        this.Desktop.RandomlyPlaced(p1981138172);
        p1981138172.setTopEdgeType(EdgeType.Concave);
        p1981138172.setBottomEdgeType(EdgeType.Convex);
        p1981138172.setLeftEdgeType(EdgeType.Convex);
        p1981138172.setRightEdgeType(EdgeType.Concave);
        p1981138172.setTopExactPuzzleID(209836318);
        p1981138172.setBottomExactPuzzleID(616197156);
        p1981138172.setLeftExactPuzzleID(1806539944);
        p1981138172.setRightExactPuzzleID(343948412);
        p1981138172.getDisplayImage().loadImageFromResource(c, R.drawable.p1981138172h);
        p1981138172.setExactRow(1);
        p1981138172.setExactColumn(6);
        p1981138172.getSize().reset(126.6667f, 126.6667f);
        p1981138172.getPositionOffset().reset(-76.66666f, -50.0f);
        p1981138172.setIsUseAbsolutePosition(true);
        p1981138172.setIsUseAbsoluteSize(true);
        p1981138172.getImage().setIsUseAbsolutePosition(true);
        p1981138172.getImage().setIsUseAbsoluteSize(true);
        p1981138172.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1981138172.resetPosition();
        getSpiritList().add(p1981138172);
    }

    /* access modifiers changed from: package-private */
    public void c616197156(Context c) {
        Puzzle p616197156 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load616197156(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save616197156(editor, this);
            }
        };
        p616197156.setID(616197156);
        p616197156.setName("616197156");
        p616197156.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p616197156);
        this.Desktop.RandomlyPlaced(p616197156);
        p616197156.setTopEdgeType(EdgeType.Concave);
        p616197156.setBottomEdgeType(EdgeType.Concave);
        p616197156.setLeftEdgeType(EdgeType.Concave);
        p616197156.setRightEdgeType(EdgeType.Concave);
        p616197156.setTopExactPuzzleID(1981138172);
        p616197156.setBottomExactPuzzleID(159001623);
        p616197156.setLeftExactPuzzleID(100338288);
        p616197156.setRightExactPuzzleID(1514068830);
        p616197156.getDisplayImage().loadImageFromResource(c, R.drawable.p616197156h);
        p616197156.setExactRow(2);
        p616197156.setExactColumn(6);
        p616197156.getSize().reset(100.0f, 100.0f);
        p616197156.getPositionOffset().reset(-50.0f, -50.0f);
        p616197156.setIsUseAbsolutePosition(true);
        p616197156.setIsUseAbsoluteSize(true);
        p616197156.getImage().setIsUseAbsolutePosition(true);
        p616197156.getImage().setIsUseAbsoluteSize(true);
        p616197156.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p616197156.resetPosition();
        getSpiritList().add(p616197156);
    }

    /* access modifiers changed from: package-private */
    public void c159001623(Context c) {
        Puzzle p159001623 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load159001623(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save159001623(editor, this);
            }
        };
        p159001623.setID(159001623);
        p159001623.setName("159001623");
        p159001623.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p159001623);
        this.Desktop.RandomlyPlaced(p159001623);
        p159001623.setTopEdgeType(EdgeType.Convex);
        p159001623.setBottomEdgeType(EdgeType.Convex);
        p159001623.setLeftEdgeType(EdgeType.Concave);
        p159001623.setRightEdgeType(EdgeType.Concave);
        p159001623.setTopExactPuzzleID(616197156);
        p159001623.setBottomExactPuzzleID(1629498411);
        p159001623.setLeftExactPuzzleID(65399397);
        p159001623.setRightExactPuzzleID(1438166234);
        p159001623.getDisplayImage().loadImageFromResource(c, R.drawable.p159001623h);
        p159001623.setExactRow(3);
        p159001623.setExactColumn(6);
        p159001623.getSize().reset(100.0f, 153.3333f);
        p159001623.getPositionOffset().reset(-50.0f, -76.66666f);
        p159001623.setIsUseAbsolutePosition(true);
        p159001623.setIsUseAbsoluteSize(true);
        p159001623.getImage().setIsUseAbsolutePosition(true);
        p159001623.getImage().setIsUseAbsoluteSize(true);
        p159001623.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p159001623.resetPosition();
        getSpiritList().add(p159001623);
    }

    /* access modifiers changed from: package-private */
    public void c1629498411(Context c) {
        Puzzle p1629498411 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1629498411(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1629498411(editor, this);
            }
        };
        p1629498411.setID(1629498411);
        p1629498411.setName("1629498411");
        p1629498411.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1629498411);
        this.Desktop.RandomlyPlaced(p1629498411);
        p1629498411.setTopEdgeType(EdgeType.Concave);
        p1629498411.setBottomEdgeType(EdgeType.Flat);
        p1629498411.setLeftEdgeType(EdgeType.Convex);
        p1629498411.setRightEdgeType(EdgeType.Convex);
        p1629498411.setTopExactPuzzleID(159001623);
        p1629498411.setBottomExactPuzzleID(-1);
        p1629498411.setLeftExactPuzzleID(1573799327);
        p1629498411.setRightExactPuzzleID(444589784);
        p1629498411.getDisplayImage().loadImageFromResource(c, R.drawable.p1629498411h);
        p1629498411.setExactRow(4);
        p1629498411.setExactColumn(6);
        p1629498411.getSize().reset(153.3333f, 100.0f);
        p1629498411.getPositionOffset().reset(-76.66666f, -50.0f);
        p1629498411.setIsUseAbsolutePosition(true);
        p1629498411.setIsUseAbsoluteSize(true);
        p1629498411.getImage().setIsUseAbsolutePosition(true);
        p1629498411.getImage().setIsUseAbsoluteSize(true);
        p1629498411.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1629498411.resetPosition();
        getSpiritList().add(p1629498411);
    }

    /* access modifiers changed from: package-private */
    public void c657135789(Context c) {
        Puzzle p657135789 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load657135789(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save657135789(editor, this);
            }
        };
        p657135789.setID(657135789);
        p657135789.setName("657135789");
        p657135789.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p657135789);
        this.Desktop.RandomlyPlaced(p657135789);
        p657135789.setTopEdgeType(EdgeType.Flat);
        p657135789.setBottomEdgeType(EdgeType.Convex);
        p657135789.setLeftEdgeType(EdgeType.Concave);
        p657135789.setRightEdgeType(EdgeType.Flat);
        p657135789.setTopExactPuzzleID(-1);
        p657135789.setBottomExactPuzzleID(343948412);
        p657135789.setLeftExactPuzzleID(209836318);
        p657135789.setRightExactPuzzleID(-1);
        p657135789.getDisplayImage().loadImageFromResource(c, R.drawable.p657135789h);
        p657135789.setExactRow(0);
        p657135789.setExactColumn(7);
        p657135789.getSize().reset(100.0f, 126.6667f);
        p657135789.getPositionOffset().reset(-50.0f, -50.0f);
        p657135789.setIsUseAbsolutePosition(true);
        p657135789.setIsUseAbsoluteSize(true);
        p657135789.getImage().setIsUseAbsolutePosition(true);
        p657135789.getImage().setIsUseAbsoluteSize(true);
        p657135789.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p657135789.resetPosition();
        getSpiritList().add(p657135789);
    }

    /* access modifiers changed from: package-private */
    public void c343948412(Context c) {
        Puzzle p343948412 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load343948412(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save343948412(editor, this);
            }
        };
        p343948412.setID(343948412);
        p343948412.setName("343948412");
        p343948412.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p343948412);
        this.Desktop.RandomlyPlaced(p343948412);
        p343948412.setTopEdgeType(EdgeType.Concave);
        p343948412.setBottomEdgeType(EdgeType.Convex);
        p343948412.setLeftEdgeType(EdgeType.Convex);
        p343948412.setRightEdgeType(EdgeType.Flat);
        p343948412.setTopExactPuzzleID(657135789);
        p343948412.setBottomExactPuzzleID(1514068830);
        p343948412.setLeftExactPuzzleID(1981138172);
        p343948412.setRightExactPuzzleID(-1);
        p343948412.getDisplayImage().loadImageFromResource(c, R.drawable.p343948412h);
        p343948412.setExactRow(1);
        p343948412.setExactColumn(7);
        p343948412.getSize().reset(126.6667f, 126.6667f);
        p343948412.getPositionOffset().reset(-76.66666f, -50.0f);
        p343948412.setIsUseAbsolutePosition(true);
        p343948412.setIsUseAbsoluteSize(true);
        p343948412.getImage().setIsUseAbsolutePosition(true);
        p343948412.getImage().setIsUseAbsoluteSize(true);
        p343948412.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p343948412.resetPosition();
        getSpiritList().add(p343948412);
    }

    /* access modifiers changed from: package-private */
    public void c1514068830(Context c) {
        Puzzle p1514068830 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1514068830(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1514068830(editor, this);
            }
        };
        p1514068830.setID(1514068830);
        p1514068830.setName("1514068830");
        p1514068830.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1514068830);
        this.Desktop.RandomlyPlaced(p1514068830);
        p1514068830.setTopEdgeType(EdgeType.Concave);
        p1514068830.setBottomEdgeType(EdgeType.Concave);
        p1514068830.setLeftEdgeType(EdgeType.Convex);
        p1514068830.setRightEdgeType(EdgeType.Flat);
        p1514068830.setTopExactPuzzleID(343948412);
        p1514068830.setBottomExactPuzzleID(1438166234);
        p1514068830.setLeftExactPuzzleID(616197156);
        p1514068830.setRightExactPuzzleID(-1);
        p1514068830.getDisplayImage().loadImageFromResource(c, R.drawable.p1514068830h);
        p1514068830.setExactRow(2);
        p1514068830.setExactColumn(7);
        p1514068830.getSize().reset(126.6667f, 100.0f);
        p1514068830.getPositionOffset().reset(-76.66666f, -50.0f);
        p1514068830.setIsUseAbsolutePosition(true);
        p1514068830.setIsUseAbsoluteSize(true);
        p1514068830.getImage().setIsUseAbsolutePosition(true);
        p1514068830.getImage().setIsUseAbsoluteSize(true);
        p1514068830.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1514068830.resetPosition();
        getSpiritList().add(p1514068830);
    }

    /* access modifiers changed from: package-private */
    public void c1438166234(Context c) {
        Puzzle p1438166234 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1438166234(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1438166234(editor, this);
            }
        };
        p1438166234.setID(1438166234);
        p1438166234.setName("1438166234");
        p1438166234.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1438166234);
        this.Desktop.RandomlyPlaced(p1438166234);
        p1438166234.setTopEdgeType(EdgeType.Convex);
        p1438166234.setBottomEdgeType(EdgeType.Convex);
        p1438166234.setLeftEdgeType(EdgeType.Convex);
        p1438166234.setRightEdgeType(EdgeType.Flat);
        p1438166234.setTopExactPuzzleID(1514068830);
        p1438166234.setBottomExactPuzzleID(444589784);
        p1438166234.setLeftExactPuzzleID(159001623);
        p1438166234.setRightExactPuzzleID(-1);
        p1438166234.getDisplayImage().loadImageFromResource(c, R.drawable.p1438166234h);
        p1438166234.setExactRow(3);
        p1438166234.setExactColumn(7);
        p1438166234.getSize().reset(126.6667f, 153.3333f);
        p1438166234.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1438166234.setIsUseAbsolutePosition(true);
        p1438166234.setIsUseAbsoluteSize(true);
        p1438166234.getImage().setIsUseAbsolutePosition(true);
        p1438166234.getImage().setIsUseAbsoluteSize(true);
        p1438166234.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1438166234.resetPosition();
        getSpiritList().add(p1438166234);
    }

    /* access modifiers changed from: package-private */
    public void c444589784(Context c) {
        Puzzle p444589784 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load444589784(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save444589784(editor, this);
            }
        };
        p444589784.setID(444589784);
        p444589784.setName("444589784");
        p444589784.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p444589784);
        this.Desktop.RandomlyPlaced(p444589784);
        p444589784.setTopEdgeType(EdgeType.Concave);
        p444589784.setBottomEdgeType(EdgeType.Flat);
        p444589784.setLeftEdgeType(EdgeType.Concave);
        p444589784.setRightEdgeType(EdgeType.Flat);
        p444589784.setTopExactPuzzleID(1438166234);
        p444589784.setBottomExactPuzzleID(-1);
        p444589784.setLeftExactPuzzleID(1629498411);
        p444589784.setRightExactPuzzleID(-1);
        p444589784.getDisplayImage().loadImageFromResource(c, R.drawable.p444589784h);
        p444589784.setExactRow(4);
        p444589784.setExactColumn(7);
        p444589784.getSize().reset(100.0f, 100.0f);
        p444589784.getPositionOffset().reset(-50.0f, -50.0f);
        p444589784.setIsUseAbsolutePosition(true);
        p444589784.setIsUseAbsoluteSize(true);
        p444589784.getImage().setIsUseAbsolutePosition(true);
        p444589784.getImage().setIsUseAbsoluteSize(true);
        p444589784.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p444589784.resetPosition();
        getSpiritList().add(p444589784);
    }
}
