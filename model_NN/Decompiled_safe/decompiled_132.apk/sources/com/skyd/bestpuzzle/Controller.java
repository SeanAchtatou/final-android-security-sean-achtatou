package com.skyd.bestpuzzle;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.skyd.core.android.game.GameObject;
import com.skyd.core.android.game.GameScene;
import com.skyd.core.android.game.GameSpirit;
import com.skyd.core.vector.Vector2DF;
import com.skyd.core.vector.VectorRect2DF;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Controller extends GameSpirit implements IOnDesktop, IUI {
    private Desktop _Desktop = null;
    private Vector2DF _PositionInDesktop = new Vector2DF();
    private ArrayList<Puzzle> _PuzzleList = new ArrayList<>();
    HashMap<Puzzle, Vector2DF> positionMap = new HashMap<>();
    HashMap<Puzzle, Float> rotationMap = new HashMap<>();
    float startAngle;
    Vector2DF startPosition;

    public GameObject getDisplayContentChild() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void drawSelf(Canvas c, Rect drawArea) {
        if (getPuzzleList().size() > 0) {
            c.save();
            Paint p = new Paint();
            p.setAntiAlias(true);
            p.setARGB(100, 255, 122, 82);
            c.drawCircle(getSize().getX() / 2.0f, getSize().getX() / 2.0f, getSize().getX() / 2.0f, p);
            p.setARGB(100, 255, 255, 255);
            c.drawCircle(getSize().getX() / 2.0f, getSize().getX() / 2.0f, (getSize().getX() - (Puzzle.getMaxRadius().getLength() / 2.0f)) / 2.0f, p);
            c.restore();
            super.drawSelf(c, drawArea);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 12 */
    public void updateState() {
        double sqrt;
        RectF rect = null;
        for (int i = 0; i < getPuzzleList().size(); i++) {
            Puzzle c = getPuzzleList().get(i);
            if (i == 0) {
                rect = getRect(c);
            } else {
                rect.union(getRect(c));
            }
        }
        if (rect != null) {
            float max = Math.max(rect.width(), rect.height());
            if (getPuzzleList().size() == 1) {
                sqrt = (double) (Puzzle.getMaxRadius().getLength() + max);
            } else {
                sqrt = Math.sqrt(Math.pow((double) max, 2.0d) * 2.0d) + ((double) Puzzle.getMaxRadius().getLength());
            }
            float size = (float) sqrt;
            getSize().reset(size, size);
            setPositionOffsetToHalfSize();
            if (getPuzzleList().size() == 1) {
                setRotation(getPuzzleList().get(0).getRotation());
                getPositionInDesktop().resetWith(getPuzzleList().get(0).getPositionInDesktop());
                return;
            }
            getPositionInDesktop().reset(rect.left + (rect.width() / 2.0f), rect.top + (rect.height() / 2.0f));
        }
    }

    /* access modifiers changed from: package-private */
    public RectF getRect(Puzzle p) {
        return new VectorRect2DF(p.getPositionInDesktop().minusNew(Puzzle.getRealitySize().scaleNew(0.5f)), Puzzle.getRealitySize()).getRectF();
    }

    public Vector2DF getPosition() {
        return getPositionInDesktop();
    }

    public Vector2DF getPositionInDesktop() {
        return this._PositionInDesktop;
    }

    public Desktop getDesktop() {
        return this._Desktop;
    }

    public void setDesktop(Desktop value) {
        this._Desktop = value;
    }

    public void setDesktopToDefault() {
        setDesktop(null);
    }

    public ArrayList<Puzzle> getPuzzleList() {
        return this._PuzzleList;
    }

    public void setPuzzleList(ArrayList<Puzzle> value) {
        this._PuzzleList = value;
    }

    public void setPuzzleListToDefault() {
        setPuzzleList(new ArrayList());
    }

    public void executive(Vector2DF point) {
        rotatePuzzlesTo(this._Desktop.reMapPoint(point).minus(this._PositionInDesktop).getAngle() - this.startAngle, this._PositionInDesktop.getX(), this._PositionInDesktop.getY());
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public void rotatePuzzlesTo(float angle, float centerx, float centery) {
        Matrix m = new Matrix();
        m.preRotate(angle, centerx, centery);
        Iterator<Puzzle> it = this._PuzzleList.iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            f.getPositionInDesktop().resetWith(this.positionMap.get(f).mapNew(m));
            f.setRotation(this.rotationMap.get(f).floatValue());
            f.setRotation(f.getDisplayAngularPointB().map(m).minus(f.getDisplayAngularPointA().map(m)).getAngle());
        }
        ((GameScene) getRoot()).refreshDrawCacheBitmap();
    }

    public boolean isInArea(Vector2DF point) {
        boolean b;
        if (getPuzzleList().size() == 0) {
            return false;
        }
        if (((double) this._Desktop.reMapPoint(point).minus(this._PositionInDesktop).getLength()) < ((double) ((getSize().getX() - (Puzzle.getMaxRadius().getLength() / 2.0f)) / 2.0f)) * 0.7d) {
            b = true;
        } else {
            b = false;
        }
        if (b) {
            initDrag();
        }
        return b;
    }

    public void initDrag() {
        this.startPosition = this._PositionInDesktop.getClone();
        Iterator<Puzzle> it = this._PuzzleList.iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            this.positionMap.put(f, f.getPositionInDesktop().getClone());
        }
    }

    public void reset() {
        this._Desktop.PiecePuzzle(getPuzzleList(), this);
        getPuzzleList().clear();
        updateState();
    }

    public boolean isInRotateArea(Vector2DF point) {
        boolean b;
        if (getPuzzleList().size() == 0) {
            return false;
        }
        Vector2DF v = this._Desktop.reMapPoint(point).minus(this._PositionInDesktop);
        if (v.getLength() > getSize().getX() / 2.0f || ((double) v.getLength()) < ((double) ((getSize().getX() - (Puzzle.getMaxRadius().getLength() / 2.0f)) / 2.0f)) * 0.7d) {
            b = false;
        } else {
            b = true;
        }
        if (b) {
            this.startAngle = v.getAngle();
            saveToMapList();
        }
        return b;
    }

    public void saveToMapList() {
        this.positionMap.clear();
        this.rotationMap.clear();
        Iterator<Puzzle> it = this._PuzzleList.iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            this.positionMap.put(f, f.getPositionInDesktop().getClone());
            this.rotationMap.put(f, Float.valueOf(f.getRotation()));
        }
    }

    public void movePuzzlesTo(Vector2DF startDragPoint, Vector2DF v) {
        movePuzzlesTo(this._Desktop.reMapPoint(v).minus(this._Desktop.reMapPoint(startDragPoint)));
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public void movePuzzlesTo(Vector2DF relativeV) {
        Iterator<Puzzle> it = this._PuzzleList.iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            f.getPositionInDesktop().resetWith(this.positionMap.get(f).plusNew(relativeV));
        }
        this._PositionInDesktop.resetWith((this.startPosition == null ? this._PositionInDesktop : this.startPosition).plusNew(relativeV));
        ((GameScene) getRoot()).refreshDrawCacheBitmap();
    }
}
