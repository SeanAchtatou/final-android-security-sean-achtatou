package com.skyd.core.common;

public enum Direction {
    Left(1),
    Right(2),
    Top(4),
    Bottom(8),
    Center(0);
    
    private int Value;

    private Direction(int value) {
        this.Value = value;
    }

    public Direction Combination(Direction d) {
        this.Value |= d.Value;
        return this;
    }

    public boolean Is(Direction d) {
        return this.Value == d.Value;
    }

    public boolean IsContain(Direction d) {
        return (this.Value & d.Value) > 0;
    }

    public Direction Intersection(Direction d) {
        this.Value &= d.Value;
        return this;
    }
}
