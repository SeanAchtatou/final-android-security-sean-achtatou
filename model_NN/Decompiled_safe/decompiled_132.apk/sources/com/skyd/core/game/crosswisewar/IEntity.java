package com.skyd.core.game.crosswisewar;

public interface IEntity extends IObj {
    void attack(IEntity iEntity);

    int getATK();

    int getAttackDelay();

    int getAttackRange();

    IEntity getBuff();

    IEntity getBuffTarget();

    IEntity getBuffed();

    int getDEF();

    int getHP();

    int getMaxHP();

    IEntity getNative();

    int getOccupyHeight();

    int getOccupyWidth();

    int hurt(IEntity iEntity);

    int hurtTo(IEntity iEntity);

    void setATK(int i);

    void setAttackDelay(int i);

    void setAttackRange(int i);

    void setBuff(IEntity iEntity);

    void setBuffTarget(IEntity iEntity);

    void setDEF(int i);

    void setHP(int i);

    void setMaxHP(int i);

    void setOccupyHeight(int i);

    void setOccupyWidth(int i);
}
