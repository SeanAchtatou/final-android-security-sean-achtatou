package com.skyd.core.game.crosswisewar;

public interface IWarrior extends IEntity, IValuable, IMove {
    ICooling getCooling();

    boolean getIsCanMoveAttac();

    boolean move();
}
