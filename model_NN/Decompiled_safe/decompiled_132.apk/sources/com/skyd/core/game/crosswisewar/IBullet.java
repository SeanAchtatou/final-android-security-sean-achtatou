package com.skyd.core.game.crosswisewar;

public interface IBullet extends IObj, IMove {
    int getHeight();

    IEntity getSource();

    void setHeight(int i);

    void setSource(IEntity iEntity);
}
