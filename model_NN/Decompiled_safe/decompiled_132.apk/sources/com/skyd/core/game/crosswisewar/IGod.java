package com.skyd.core.game.crosswisewar;

public interface IGod {
    int getScore();

    void setScore(int i);
}
