package com.skyd.core.android;

import android.app.Application;
import android.content.SharedPreferences;

public abstract class Global extends Application {
    public static final String FirstRunKey = "CommonSharedPreferences_FirstRun";
    public static final String FullScreenKey = "CommonSharedPreferences_FullScreen";
    public static final String KeepScreenOnKey = "CommonSharedPreferences_KeepScreenOn";
    private static final String Label = "CommonSharedPreferences_";

    public abstract String getAppKey();

    public boolean getIsFullScreen() {
        return getSharedPreferences().getBoolean(FullScreenKey, false);
    }

    public void setIsFullScreen(int permission, boolean b) {
        getSharedPreferences(permission).edit().putBoolean(FullScreenKey, b).commit();
    }

    public boolean getIsKeepScreenOn() {
        return getSharedPreferences().getBoolean(KeepScreenOnKey, false);
    }

    public void setIsKeepScreenOn(int permission, boolean b) {
        getSharedPreferences(permission).edit().putBoolean(KeepScreenOnKey, b).commit();
    }

    public boolean getIsFirstRun() {
        return getSharedPreferences().getBoolean(FirstRunKey, true);
    }

    public void setIsFirstRun(int permission, boolean b) {
        getSharedPreferences(permission).edit().putBoolean(FirstRunKey, b).commit();
    }

    public SharedPreferences getSharedPreferences() {
        return getSharedPreferences(0);
    }

    public SharedPreferences getSharedPreferences(int permission) {
        return getSharedPreferences(getAppKey(), permission);
    }
}
