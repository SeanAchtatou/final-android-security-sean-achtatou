package com.skyd.core.android.game;

import android.graphics.Canvas;
import android.graphics.Rect;

public class GameMultifunctionSpirit extends GamePixelImageSpirit implements IGameImageHolder {
    private int _DisplayStateIndex = 0;
    private GameObjectList<GameSpiritState> _StateList = new GameObjectList<>(this);

    public GameObjectList<GameSpiritState> getStateList() {
        return this._StateList;
    }

    public void setStateList(GameObjectList<GameSpiritState> value) {
        this._StateList = value;
    }

    public void setStateListToDefault() {
        setStateList(new GameObjectList(this));
    }

    public int getDisplayStateIndex() {
        return this._DisplayStateIndex;
    }

    public void setDisplayStateIndex(int value) {
        this._DisplayStateIndex = value;
    }

    public void setDisplayStateIndexToDefault() {
        setDisplayStateIndex(0);
    }

    public void setDisplayStateName(String name) throws Exception {
        for (int i = 0; i < this._StateList.size(); i++) {
            if (this._StateList.get(i).getName() == name) {
                this._DisplayStateIndex = i;
                return;
            }
        }
        throw new Exception("未找到名为" + name + "的精灵状态");
    }

    public String getDisplayStateName() {
        return getDisplayState().getName();
    }

    public GameSpiritState getDisplayState() {
        return this._StateList.get(this._DisplayStateIndex);
    }

    public void updateChilds() {
        if (this._StateList.size() > 0) {
            getDisplayState().update();
        }
    }

    /* access modifiers changed from: protected */
    public void drawChilds(Canvas c, Rect drawArea) {
        if (this._StateList.size() > 0) {
            getDisplayState().draw(c, getDisplayRect());
        }
    }

    public GameObject getDisplayContentChild() {
        return getDisplayState();
    }

    public GameImage getDisplayImage() {
        return getDisplayState().getDisplayImage();
    }
}
