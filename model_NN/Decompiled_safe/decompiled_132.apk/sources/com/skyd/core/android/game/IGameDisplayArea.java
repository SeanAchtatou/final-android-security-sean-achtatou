package com.skyd.core.android.game;

import android.graphics.Canvas;

public interface IGameDisplayArea {
    void addScene(GameScene gameScene) throws Exception;

    void draw(Canvas canvas);

    GameScene getDisplayScene();

    int getDisplaySceneIndex();

    String getDisplaySceneName();

    int getHeight();

    GameScene getSceneByName(String str);

    int getWidth();

    void removeScene(GameScene gameScene) throws Exception;

    void setDisplaySceneIndex(int i);

    void setDisplaySceneName(String str) throws Exception;

    void update();
}
