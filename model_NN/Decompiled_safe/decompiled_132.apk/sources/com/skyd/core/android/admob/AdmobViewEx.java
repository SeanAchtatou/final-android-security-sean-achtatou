package com.skyd.core.android.admob;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import com.admob.android.ads.AdView;
import java.util.ArrayList;
import java.util.Iterator;

public class AdmobViewEx extends AdView {
    private ArrayList<OnSizeChangedListener> _SizeChangedListenerList = null;

    public interface OnSizeChangedListener {
        void OnSizeChangedEvent(Object obj, int i, int i2, int i3, int i4);
    }

    public AdmobViewEx(Context arg0, AttributeSet arg1) {
        super(arg0, arg1);
    }

    public AdmobViewEx(Context arg0, AttributeSet arg1, int arg2) {
        super(arg0, arg1, arg2);
    }

    public void removeFrom(ViewGroup layout) {
        layout.removeView(this);
        setEnabled(false);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (this._SizeChangedListenerList != null) {
            Iterator<OnSizeChangedListener> it = this._SizeChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnSizeChangedEvent(this, w, h, oldw, oldh);
            }
        }
        super.onSizeChanged(w, h, oldw, oldh);
    }

    public boolean addOnSizeChangedListener(OnSizeChangedListener listener) {
        if (this._SizeChangedListenerList == null) {
            this._SizeChangedListenerList = new ArrayList<>();
        } else if (this._SizeChangedListenerList.contains(listener)) {
            return false;
        }
        this._SizeChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnSizeChangedListener(OnSizeChangedListener listener) {
        if (this._SizeChangedListenerList == null || !this._SizeChangedListenerList.contains(listener)) {
            return false;
        }
        this._SizeChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnSizeChangedListeners() {
        if (this._SizeChangedListenerList != null) {
            this._SizeChangedListenerList.clear();
        }
    }
}
