package com.skyd.core.android.game.crosswisewar;

import com.skyd.core.game.crosswisewar.IGod;

public abstract class God implements IGod {
    private int _Score = 0;

    public int getScore() {
        return this._Score;
    }

    public void setScore(int value) {
        this._Score = value;
    }

    public void setScoreToDefault() {
        setScore(0);
    }
}
