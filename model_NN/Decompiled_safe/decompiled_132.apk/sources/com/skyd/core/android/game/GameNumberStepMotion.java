package com.skyd.core.android.game;

import java.lang.Number;

public abstract class GameNumberStepMotion<T extends Number> extends GameMotion {
    private T _StepLength;
    private T _TargetValue;
    private T _Tolerance;

    /* access modifiers changed from: protected */
    public abstract T getCurrentValue(GameObject gameObject);

    /* access modifiers changed from: protected */
    public abstract T plus(Number number, Number number2);

    /* access modifiers changed from: protected */
    public abstract void setCurrentValue(GameObject gameObject, Number number);

    public GameNumberStepMotion(T targetValue, T stepLength, T tolerance) {
        setTargetValue(targetValue);
        setStepLength(stepLength);
        setTolerance(tolerance);
    }

    /* access modifiers changed from: protected */
    public boolean getIsComplete(GameObject obj) {
        return Math.abs(getTargetValue().doubleValue() - getCurrentValue(obj).doubleValue()) <= getTolerance().doubleValue();
    }

    /* access modifiers changed from: protected */
    public void restart(GameObject obj) {
    }

    /* access modifiers changed from: protected */
    public void updateSelf(GameObject obj) {
    }

    /* access modifiers changed from: protected */
    public void updateTarget(GameObject obj) {
        setCurrentValue(obj, plus(getCurrentValue(obj), getStepLength()));
    }

    public T getTargetValue() {
        return this._TargetValue;
    }

    public void setTargetValue(T value) {
        this._TargetValue = value;
    }

    public T getStepLength() {
        return this._StepLength;
    }

    public void setStepLength(T value) {
        this._StepLength = value;
    }

    public T getTolerance() {
        return this._Tolerance;
    }

    public void setTolerance(T value) {
        this._Tolerance = value;
    }
}
