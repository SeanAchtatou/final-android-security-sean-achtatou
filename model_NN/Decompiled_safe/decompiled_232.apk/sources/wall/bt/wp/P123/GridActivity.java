package wall.bt.wp.P123;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.xl.util.BookMarkUtil;
import com.xl.util.Constant;
import com.xl.util.FileUtil;
import com.xl.util.Folder;
import com.xl.util.GenUtil;
import com.xl.util.MultiDownloadNew;
import com.xl.util.WifiUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import wall.bt.wp.P123.Protocals;

public class GridActivity extends BaseImgActivity {
    private static final int DIALOG_ADDFAVORITE = 0;
    private static final int NumPerPage = 100;
    private static final String TAG = "SortByViews";
    private static boolean bPageInterrupt;
    private static String mPath;
    /* access modifiers changed from: private */
    public boolean bIndownload = false;
    private Boolean bLocal = false;
    private Boolean bResume = false;
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    private MyAdapter mAdapter;
    private List<Protocals.Album> mDataAlbum = new ArrayList();
    /* access modifiers changed from: private */
    public List<String> mDataAlbumName = new ArrayList();
    /* access modifiers changed from: private */
    public List<Protocals.Album> mDataAlbumProc = new ArrayList();
    private List mDataImg = new ArrayList();
    private List<String> mDataURLList = new ArrayList();
    private TextView mDownloadPercent;
    private GridView mGridView;
    private ProgressBar mProgress;
    private RelativeLayout mProgressLayout;
    /* access modifiers changed from: private */
    public MyHandler myHandler = new MyHandler();
    /* access modifiers changed from: private */
    public String strBookMark = "";
    private String strRelativePath = "";
    /* access modifiers changed from: private */
    public String strWebImgRootURL = "";
    private String strWebImgRootURLOriginal = "";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        fullScreen();
        setContentView((int) R.layout.grid);
        initView();
        initData();
        Bundle bundle2 = getIntent().getExtras();
        this.strWebImgRootURL = bundle2.getString("url");
        this.strWebImgRootURLOriginal = this.strWebImgRootURL;
        this.strBookMark = bundle2.getString("bookmark");
        showAlbums(this.strWebImgRootURL);
    }

    private void showAlbums(String strURL) {
        this.mDataAlbum.clear();
        this.mDataAlbumName.clear();
        this.mDataAlbumProc.clear();
        clearImg();
        this.bIndownload = false;
        this.mDataURLList.clear();
        if (this.strWebImgRootURL.indexOf(Environment.getExternalStorageState().equals("mounted") ? Folder.MYCOLLECTION : Folder.MYCOLLECTION1) > -1) {
            this.bLocal = 1;
            this.mDataURLList.add(Environment.getExternalStorageState().equals("mounted") ? Folder.MYCOLLECTION : Folder.MYCOLLECTION1);
        } else {
            this.mDataURLList.add(getResources().getString(R.string.webimgrooturl));
        }
        this.mDataURLList.add(this.strWebImgRootURL);
        this.mAdapter.notifyDataSetChanged();
        this.mDataAlbum = Protocals.getInstance().getCategories(strURL, getResources().getString(R.string.webimgrooturl), Environment.getExternalStorageState().equals("mounted") ? Folder.MYCACHE : Folder.MYCACHE1);
        GenUtil.systemPrintln("------------> mDataAlbum.size() = " + this.mDataAlbum.size());
        GenUtil.systemPrintln("------------> mDataImg.size()begin = " + this.mDataImg.size());
        if (this.mDataAlbum.size() == 0) {
            showInfoNoWifi(getResources().getString(R.string.txt_wifi_none_title), getResources().getString(R.string.txt_no_wifi_data));
            return;
        }
        Boolean bConn = Boolean.valueOf(new WifiUtil(this).wifiConnect());
        this.myHandler.showMyDialog();
        GenUtil.systemPrintln("getRelativePath = " + getRelativePath(this.mDataURLList));
        this.strRelativePath = getRelativePath(this.mDataURLList);
        int iTask = 0;
        int iMax = 51;
        int iDataCount = 0;
        if (this.mDataAlbum.size() < 51) {
            iMax = this.mDataAlbum.size();
        }
        for (int iIndex = 0; iIndex < iMax; iIndex++) {
            Protocals.Album album = this.mDataAlbum.get(iIndex);
            if (album != null) {
                Object[] arrObjects = getObjectList(album);
                String strLocalImgPath = (String) arrObjects[1];
                if (FileUtil.fileExist(strLocalImgPath)) {
                    iDataCount++;
                    if (this.bLocal.booleanValue()) {
                        addAdapterDataLocal(strLocalImgPath);
                    } else {
                        addAdapterData(album.name);
                    }
                } else if (!this.bLocal.booleanValue() && bConn.booleanValue()) {
                    iDataCount++;
                    if (iTask <= 2) {
                        try {
                            Thread.currentThread();
                            Thread.sleep(300);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        this.bIndownload = true;
                        new QueryImgTask().execute(arrObjects);
                        iTask++;
                    } else {
                        this.mDataAlbumProc.add(album);
                    }
                }
            }
        }
        this.myHandler.dismissMyDialog();
        GenUtil.systemPrintln("------------> mDataImg.size() = " + this.mDataImg.size());
        if (iDataCount == 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.txt_no_wifi_data), 1).show();
        }
    }

    /* access modifiers changed from: private */
    public Object[] getObjectList(Protocals.Album album) {
        Object[] arrObjects = new Object[4];
        if (this.bLocal.booleanValue()) {
            arrObjects[0] = "";
            arrObjects[1] = (Environment.getExternalStorageState().equals("mounted") ? Folder.MYCOLLECTION : Folder.MYCOLLECTION1) + this.strRelativePath + album.name;
            arrObjects[2] = "";
            arrObjects[3] = "";
        } else {
            arrObjects[0] = this.strWebImgRootURL + album.name;
            arrObjects[1] = mPath + this.strRelativePath + album.name;
            arrObjects[2] = String.valueOf(1);
            arrObjects[3] = album.name;
        }
        GenUtil.systemPrintln("arrObjects[0] = " + arrObjects[0]);
        GenUtil.systemPrintln("arrObjects[1] = " + arrObjects[1]);
        GenUtil.systemPrintln("arrObjects[2] = " + arrObjects[2]);
        return arrObjects;
    }

    private void clearImg() {
        for (int i = 0; i < this.mDataImg.size(); i++) {
            Bitmap bitmap = (Bitmap) this.mDataImg.get(i);
        }
        this.mDataImg.clear();
    }

    private String getRelativePath(List<String> mURLList) {
        if (mURLList.size() <= 1) {
            return "";
        }
        return mURLList.get(mURLList.size() - 1).substring(mURLList.get(0).length());
    }

    private void initData() {
        this.dialog = new ProgressDialog(this);
        if (Environment.getExternalStorageState().equals("mounted")) {
            mPath = Folder.MYCACHE;
        } else {
            mPath = Folder.MYCACHE1;
        }
    }

    private void initView() {
        this.mProgressLayout = (RelativeLayout) findViewById(R.id.progress_layout);
        this.mProgress = (ProgressBar) findViewById(R.id.progress);
        this.mDownloadPercent = (TextView) findViewById(R.id.download_percent);
        this.mGridView = (GridView) findViewById(R.id.grid);
        this.mAdapter = new MyAdapter();
        this.mGridView.setAdapter((ListAdapter) this.mAdapter);
        this.mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                GenUtil.systemPrintln("arg2 = " + arg2);
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                String strName = (String) GridActivity.this.mDataAlbumName.get(arg2);
                int iStart = strName.indexOf("sm_");
                if (iStart >= 0) {
                    boolean unused = GridActivity.this.bIndownload = false;
                    bundle.putString("album_name", strName.substring(iStart + "sm_".length()));
                    bundle.putInt("image_pos", arg2);
                    bundle.putString("base_url", GridActivity.this.strWebImgRootURL);
                    bundle.putString("bookmark", GridActivity.this.strBookMark);
                    intent.putExtras(bundle);
                    intent.setClass(GridActivity.this, ImgActivity.class);
                    GridActivity.this.startActivityForResult(intent, 0);
                }
            }
        });
        this.mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                return true;
            }
        });
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Protocals.Album getAlbum(String strAlbumName) {
        for (int i = 0; i < this.mDataAlbum.size(); i++) {
            if (this.mDataAlbum.get(i).name.equals(strAlbumName)) {
                return this.mDataAlbum.get(i);
            }
        }
        return null;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (keyCode == 4) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        GenUtil.systemPrintln("requestCode = " + requestCode);
        GenUtil.systemPrintln("resultCode = " + resultCode);
        switch (requestCode) {
            case 0:
                switch (resultCode) {
                    case -1:
                        setResult(-1, new Intent());
                        finish();
                        return;
                    default:
                        return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public int getSize() {
        if (this.mDataImg != null) {
            return this.mDataImg.size();
        }
        return 0;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    /* access modifiers changed from: private */
    public Bitmap getElement(int iIndex) {
        if (this.mDataImg == null || iIndex < 0 || iIndex >= this.mDataImg.size()) {
            return null;
        }
        return (Bitmap) this.mDataImg.get(iIndex);
    }

    class AlbumInfo {
        Protocals.Album mAlbum;
        Bitmap mThumbnail;
        int tag = -1;

        public AlbumInfo() {
        }
    }

    public final class ViewHolder {
        public ImageView iv_img_cat;

        public ViewHolder() {
        }
    }

    class MyAdapter extends BaseAdapter {
        public MyAdapter() {
        }

        public int getCount() {
            return GridActivity.this.getSize();
        }

        public Object getItem(int iIndex) {
            return GridActivity.this.getElement(iIndex);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(GridActivity.this).inflate((int) R.layout.grid_img, (ViewGroup) null);
                holder.iv_img_cat = (ImageView) convertView.findViewById(R.id.grid_img);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.iv_img_cat.setImageBitmap(GridActivity.this.getElement(position));
            return convertView;
        }
    }

    class MyHandler extends Handler {
        private static final int DISMISS_DIALOG = 7;
        private static final int DISMISS_PROGRESSBAR = 3;
        private static final String INFO = "info";
        private static final int NOTIFY_DATA = 1;
        private static final String REMOTE_VERSION = "remote_version";
        private static final int SET_PAGEINFO = 5;
        private static final int SHOW_DIALOG = 6;
        private static final int SHOW_DOWNLOAD_PERCENT = 4;
        private static final int SHOW_MSG = 8;
        private static final int SHOW_PROGRESSBAR = 2;

        MyHandler() {
        }

        /* access modifiers changed from: private */
        public void dismissMyDialog() {
            sendEmptyMessage(DISMISS_DIALOG);
        }

        private void notifyDataSetChanged() {
            Message localMessage = new Message();
            localMessage.what = 1;
            sendMessage(localMessage);
        }

        /* access modifiers changed from: private */
        public void showMyDialog() {
            sendEmptyMessage(SHOW_DIALOG);
        }

        public void handleMessage(Message paramMessage) {
            switch (paramMessage.what) {
                case 1:
                    if (GridActivity.this.dialog != null) {
                        GridActivity.this.dialog.dismiss();
                    }
                    GridActivity.this.addAdapterData((String) paramMessage.obj);
                    if (GridActivity.this.mDataAlbumProc.size() > 0) {
                        new QueryImgTask().execute(GridActivity.this.getObjectList((Protocals.Album) GridActivity.this.mDataAlbumProc.get(0)));
                        GridActivity.this.mDataAlbumProc.remove(0);
                        break;
                    }
                    break;
                case SHOW_DIALOG /*6*/:
                    if (GridActivity.this.dialog != null) {
                        GridActivity.this.dialog.setMessage("Loading...");
                        GridActivity.this.dialog.show();
                        break;
                    }
                    break;
                case DISMISS_DIALOG /*7*/:
                    if (GridActivity.this.dialog != null) {
                        GridActivity.this.dialog.dismiss();
                        break;
                    }
                    break;
                case SHOW_MSG /*8*/:
                    Bundle bundle = paramMessage.getData();
                    if (bundle != null) {
                        new AlertDialog.Builder(GridActivity.this).setTitle("Message:").setIcon((int) R.drawable.toast_warnning).setMessage(bundle.getString(INFO)).setPositiveButton("Never show", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();
                        break;
                    }
                    break;
            }
            super.handleMessage(paramMessage);
        }
    }

    private String GetImagePathByName(String strImgName) {
        for (int i = 0; i < this.mDataAlbum.size(); i++) {
            Protocals.Album album = this.mDataAlbum.get(i);
            if (album.name.equals(strImgName)) {
                return album.image;
            }
        }
        return "";
    }

    /* access modifiers changed from: private */
    public void addAdapterData(String strImgName) {
        this.mDataAlbumName.add(strImgName);
        String strImgPath = mPath + this.strRelativePath + strImgName;
        GenUtil.systemPrintln("strImgPath = " + strImgPath);
        this.mDataImg.add(BitmapFactory.decodeFile(strImgPath));
        this.mAdapter.notifyDataSetChanged();
    }

    private void addAdapterDataLocal(String strImgPath) {
        this.mDataAlbumName.add(strImgPath);
        GenUtil.systemPrintln("strImgPath = " + strImgPath);
        this.mDataImg.add(BitmapFactory.decodeFile(strImgPath));
        this.mAdapter.notifyDataSetChanged();
    }

    class QueryImgTask extends AsyncTask {
        QueryImgTask() {
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String strtxt) {
            GridActivity.this.myHandler.sendEmptyMessage(1);
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(int iPercent) {
            Message m = new Message();
            m.what = 6;
            m.obj = iPercent + "%";
            GridActivity.this.myHandler.sendMessageDelayed(m, 0);
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object[] arrParams) {
            String bookFolderTMP = (Environment.getExternalStorageState().equals("mounted") ? Folder.POPIMG : Folder.POPIMG1) + Constant.TMP;
            int startThread = Integer.parseInt((String) arrParams[2]);
            System.out.println((String) arrParams[0]);
            System.out.println((String) arrParams[1]);
            System.out.println(bookFolderTMP);
            final String strAlbumImage = (String) arrParams[3];
            final MultiDownloadNew multiDownload = new MultiDownloadNew(startThread, (String) arrParams[0], (String) arrParams[1], bookFolderTMP);
            multiDownload.start();
            new Thread(new Runnable() {
                public void run() {
                    long time = new Date().getTime();
                    long lastdata = 0;
                    while (multiDownload.getPercntInt() < GridActivity.NumPerPage && GridActivity.this.bIndownload) {
                        try {
                            Thread.sleep(200);
                            long currTime = new Date().getTime();
                            long currData = multiDownload.getFileDownloadTotal();
                            if (currData != lastdata) {
                                long lasttime = currTime;
                                lastdata = currData;
                            }
                            Message m = new Message();
                            m.obj = multiDownload.getPercntInt() + "";
                            m.what = 2;
                            GridActivity.this.myHandler.sendMessage(m);
                        } catch (InterruptedException e) {
                            Message m1 = new Message();
                            m1.what = -1;
                            GridActivity.this.myHandler.sendMessage(m1);
                        }
                    }
                    Message m2 = new Message();
                    if (multiDownload.getPercntInt() != GridActivity.NumPerPage || !GridActivity.this.bIndownload) {
                        m2.what = 7;
                    } else {
                        m2.obj = strAlbumImage;
                        m2.what = 1;
                    }
                    GridActivity.this.myHandler.sendMessage(m2);
                }
            }).start();
            return true;
        }
    }

    public boolean onCreateOptionsMenu(Menu paramMenu) {
        getMenuInflater().inflate(R.menu.grid_menu, paramMenu);
        return super.onCreateOptionsMenu(paramMenu);
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        String msgString;
        super.onOptionsItemSelected(paramMenuItem);
        switch (paramMenuItem.getItemId()) {
            case R.id.savegrid /*2131230784*/:
                if (this.strBookMark == null || this.strBookMark.equals("")) {
                    msgString = getResources().getString(R.string.txt_album_save_exist);
                } else if (setBookMark()) {
                    msgString = getResources().getString(R.string.txt_album_save);
                } else {
                    msgString = getResources().getString(R.string.txt_album_save_exist);
                }
                Toast.makeText(getApplicationContext(), msgString, 1).show();
                break;
        }
        return true;
    }

    public boolean setBookMark() {
        return new BookMarkUtil(Environment.getExternalStorageState().equals("mounted") ? Folder.MYBOOKMARK : Folder.MYBOOKMARK1, "bookmark").add(this.strBookMark).booleanValue();
    }

    public void showInfoNoWifi(String msgTitle, String msgString) {
        new AlertDialog.Builder(this).setTitle(msgTitle).setMessage(msgString).setPositiveButton(getResources().getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GridActivity.this.finish();
            }
        }).show();
    }

    public void onResume() {
        GenUtil.systemPrintln("-------------- OnResume called !");
        if (this.bResume.booleanValue()) {
            showAlbums(this.strWebImgRootURLOriginal);
        } else {
            this.bResume = true;
        }
        super.onResume();
    }
}
