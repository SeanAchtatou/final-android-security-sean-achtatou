package com.google.gson;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Collection;

abstract class av implements k {
    av() {
    }

    public final String a(FieldAttributes fieldAttributes) {
        at.a(fieldAttributes);
        return a(fieldAttributes.getName(), fieldAttributes.getDeclaredType(), fieldAttributes.getAnnotations());
    }

    /* access modifiers changed from: protected */
    public abstract String a(String str, Type type, Collection<Annotation> collection);
}
