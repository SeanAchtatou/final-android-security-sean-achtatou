package com.xl.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;

public class Download extends Thread {
    private int byteread = 0;
    private long bytesum = 0;
    URLConnection conn = null;
    private String desFile;
    private long downSize;
    private long endPos = -1;
    private boolean flag = true;
    private FileOutputStream fsOutput;
    private int iRun = 1;
    private int iRunTotle = 10;
    private int index = 0;
    private boolean intercept = false;
    private double percent;
    private int percntInt;
    private long startPos = -1;
    private boolean stop = false;
    private int timeout;
    private String tmpFile;
    URL url;
    private String webURL;

    public Download(int index2, long startPos2, long endPos2, String strWebURL, String strDesFile) {
        this.startPos = startPos2;
        this.endPos = endPos2;
        this.downSize = (endPos2 - startPos2) + 1;
        this.index = index2;
        this.desFile = new String(strDesFile);
        this.webURL = new String(strWebURL);
        GenUtil.systemPrintln("webURL = " + this.webURL);
        initDownload(strWebURL);
    }

    public Download(int index2, String strWebURL, String strDesFile) {
        this.index = index2;
        this.desFile = new String(strDesFile);
        this.webURL = new String(strWebURL);
        initDownload(strWebURL);
        this.downSize = (long) getFileSize(strWebURL);
        this.startPos = 0;
        this.endPos = this.downSize - 1;
    }

    public void run() {
        runControl();
    }

    private void runControl() {
        this.iRun++;
        if (this.iRun > this.iRunTotle) {
            setFlag(false);
        } else {
            runCont();
        }
    }

    private void initDownload(String strWebURL) {
        String strFoldTmp = this.desFile.substring(0, this.desFile.lastIndexOf("/"));
        if (!FileUtil.fileExist(strFoldTmp)) {
            GenUtil.systemPrintln("----------1-------- > " + strFoldTmp);
            try {
                FileUtil.createFolders(strFoldTmp);
                GenUtil.systemPrintln("--------2---------- > " + strFoldTmp);
            } catch (Exception e) {
                e.printStackTrace();
                GenUtil.systemPrintln("---------3--------- > " + strFoldTmp);
            }
        }
        GenUtil.systemPrintln("-----------4------- > " + this.desFile);
        System.out.println("------------------ > " + this.desFile);
        try {
            this.tmpFile = this.desFile;
            this.fsOutput = new FileOutputStream(this.tmpFile);
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            setFlag(false);
        }
        try {
            this.url = new URL(strWebURL);
            this.conn = null;
            try {
                this.conn = this.url.openConnection();
            } catch (SocketTimeoutException e3) {
                setFlag(false);
            } catch (IOException e4) {
                e4.printStackTrace();
                setFlag(false);
            }
        } catch (MalformedURLException e5) {
            e5.printStackTrace();
            setFlag(false);
        }
    }

    private int getFileSize(String strWebURL) {
        try {
            return this.conn.getContentLength();
        } catch (Exception e) {
            e.printStackTrace();
            setFlag(false);
            return 0;
        }
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    private void runCont() {
        /*
            r13 = this;
            r12 = 1
            r11 = -1
            r6 = 0
            r13.conn = r6     // Catch:{ SocketTimeoutException -> 0x008e }
            java.net.URL r6 = r13.url     // Catch:{ SocketTimeoutException -> 0x008e }
            java.net.URLConnection r6 = r6.openConnection()     // Catch:{ SocketTimeoutException -> 0x008e }
            r13.conn = r6     // Catch:{ SocketTimeoutException -> 0x008e }
            java.net.URLConnection r6 = r13.conn     // Catch:{ SocketTimeoutException -> 0x008e }
            r7 = 10000(0x2710, float:1.4013E-41)
            r6.setConnectTimeout(r7)     // Catch:{ SocketTimeoutException -> 0x008e }
            java.net.URLConnection r6 = r13.conn     // Catch:{ SocketTimeoutException -> 0x008e }
            r7 = 20000(0x4e20, float:2.8026E-41)
            r6.setReadTimeout(r7)     // Catch:{ SocketTimeoutException -> 0x008e }
            java.text.DecimalFormat r3 = new java.text.DecimalFormat     // Catch:{ IOException -> 0x0106 }
            java.lang.String r6 = "0"
            r3.<init>(r6)     // Catch:{ IOException -> 0x0106 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0106 }
            r6.<init>()     // Catch:{ IOException -> 0x0106 }
            java.lang.String r7 = "bytes="
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ IOException -> 0x0106 }
            long r7 = r13.startPos     // Catch:{ IOException -> 0x0106 }
            java.lang.String r7 = r3.format(r7)     // Catch:{ IOException -> 0x0106 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ IOException -> 0x0106 }
            java.lang.String r7 = "-"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ IOException -> 0x0106 }
            long r7 = r13.endPos     // Catch:{ IOException -> 0x0106 }
            java.lang.String r7 = r3.format(r7)     // Catch:{ IOException -> 0x0106 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ IOException -> 0x0106 }
            java.lang.String r5 = r6.toString()     // Catch:{ IOException -> 0x0106 }
            java.net.URLConnection r6 = r13.conn     // Catch:{ IOException -> 0x0106 }
            java.lang.String r7 = "Range"
            r6.setRequestProperty(r7, r5)     // Catch:{ IOException -> 0x0106 }
            java.net.URLConnection r6 = r13.conn     // Catch:{ IOException -> 0x0106 }
            int r6 = r6.getConnectTimeout()     // Catch:{ IOException -> 0x0106 }
            r13.timeout = r6     // Catch:{ IOException -> 0x0106 }
            java.net.URLConnection r6 = r13.conn     // Catch:{ IOException -> 0x0106 }
            java.io.InputStream r4 = r6.getInputStream()     // Catch:{ IOException -> 0x0106 }
            r6 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r6]     // Catch:{ IOException -> 0x0106 }
        L_0x0064:
            int r6 = r4.read(r0)     // Catch:{ IOException -> 0x0106 }
            r13.byteread = r6     // Catch:{ IOException -> 0x0106 }
            if (r6 == r11) goto L_0x0080
            long r6 = r13.bytesum     // Catch:{ IOException -> 0x0106 }
            int r8 = r13.byteread     // Catch:{ IOException -> 0x0106 }
            long r8 = (long) r8     // Catch:{ IOException -> 0x0106 }
            long r6 = r6 + r8
            r13.bytesum = r6     // Catch:{ IOException -> 0x0106 }
            boolean r6 = r13.stop     // Catch:{ IOException -> 0x0106 }
            if (r6 == 0) goto L_0x0231
            int r6 = r13.iRunTotle     // Catch:{ IOException -> 0x0106 }
            r13.iRun = r6     // Catch:{ IOException -> 0x0106 }
            r6 = 0
            r13.setFlag(r6)     // Catch:{ IOException -> 0x0106 }
        L_0x0080:
            boolean r6 = r13.stop     // Catch:{ IOException -> 0x0106 }
            if (r6 == 0) goto L_0x008d
            r4.close()     // Catch:{ IOException -> 0x0106 }
            r6 = 0
            r13.conn = r6     // Catch:{ IOException -> 0x0106 }
            r6 = 0
            r13.url = r6     // Catch:{ IOException -> 0x0106 }
        L_0x008d:
            return
        L_0x008e:
            r6 = move-exception
            r1 = r6
            r13.runControl()     // Catch:{ all -> 0x0133 }
            java.text.DecimalFormat r3 = new java.text.DecimalFormat     // Catch:{ IOException -> 0x0106 }
            java.lang.String r6 = "0"
            r3.<init>(r6)     // Catch:{ IOException -> 0x0106 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0106 }
            r6.<init>()     // Catch:{ IOException -> 0x0106 }
            java.lang.String r7 = "bytes="
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ IOException -> 0x0106 }
            long r7 = r13.startPos     // Catch:{ IOException -> 0x0106 }
            java.lang.String r7 = r3.format(r7)     // Catch:{ IOException -> 0x0106 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ IOException -> 0x0106 }
            java.lang.String r7 = "-"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ IOException -> 0x0106 }
            long r7 = r13.endPos     // Catch:{ IOException -> 0x0106 }
            java.lang.String r7 = r3.format(r7)     // Catch:{ IOException -> 0x0106 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ IOException -> 0x0106 }
            java.lang.String r5 = r6.toString()     // Catch:{ IOException -> 0x0106 }
            java.net.URLConnection r6 = r13.conn     // Catch:{ IOException -> 0x0106 }
            java.lang.String r7 = "Range"
            r6.setRequestProperty(r7, r5)     // Catch:{ IOException -> 0x0106 }
            java.net.URLConnection r6 = r13.conn     // Catch:{ IOException -> 0x0106 }
            int r6 = r6.getConnectTimeout()     // Catch:{ IOException -> 0x0106 }
            r13.timeout = r6     // Catch:{ IOException -> 0x0106 }
            java.net.URLConnection r6 = r13.conn     // Catch:{ IOException -> 0x0106 }
            java.io.InputStream r4 = r6.getInputStream()     // Catch:{ IOException -> 0x0106 }
            r6 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r6]     // Catch:{ IOException -> 0x0106 }
        L_0x00dc:
            int r6 = r4.read(r0)     // Catch:{ IOException -> 0x0106 }
            r13.byteread = r6     // Catch:{ IOException -> 0x0106 }
            if (r6 == r11) goto L_0x00f8
            long r6 = r13.bytesum     // Catch:{ IOException -> 0x0106 }
            int r8 = r13.byteread     // Catch:{ IOException -> 0x0106 }
            long r8 = (long) r8     // Catch:{ IOException -> 0x0106 }
            long r6 = r6 + r8
            r13.bytesum = r6     // Catch:{ IOException -> 0x0106 }
            boolean r6 = r13.stop     // Catch:{ IOException -> 0x0106 }
            if (r6 == 0) goto L_0x01eb
            int r6 = r13.iRunTotle     // Catch:{ IOException -> 0x0106 }
            r13.iRun = r6     // Catch:{ IOException -> 0x0106 }
            r6 = 0
            r13.setFlag(r6)     // Catch:{ IOException -> 0x0106 }
        L_0x00f8:
            boolean r6 = r13.stop     // Catch:{ IOException -> 0x0106 }
            if (r6 == 0) goto L_0x008d
            r4.close()     // Catch:{ IOException -> 0x0106 }
            r6 = 0
            r13.conn = r6     // Catch:{ IOException -> 0x0106 }
            r6 = 0
            r13.url = r6     // Catch:{ IOException -> 0x0106 }
            goto L_0x008d
        L_0x0106:
            r6 = move-exception
            r2 = r6
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "\nindex = "
            java.lang.StringBuilder r6 = r6.append(r7)
            int r7 = r13.index
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = ""
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = r2.toString()
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            com.xl.util.GenUtil.systemPrintln(r6)
            r13.runControl()
            goto L_0x008d
        L_0x0133:
            r6 = move-exception
            java.text.DecimalFormat r3 = new java.text.DecimalFormat     // Catch:{ IOException -> 0x0106 }
            java.lang.String r7 = "0"
            r3.<init>(r7)     // Catch:{ IOException -> 0x0106 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0106 }
            r7.<init>()     // Catch:{ IOException -> 0x0106 }
            java.lang.String r8 = "bytes="
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ IOException -> 0x0106 }
            long r8 = r13.startPos     // Catch:{ IOException -> 0x0106 }
            java.lang.String r8 = r3.format(r8)     // Catch:{ IOException -> 0x0106 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ IOException -> 0x0106 }
            java.lang.String r8 = "-"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ IOException -> 0x0106 }
            long r8 = r13.endPos     // Catch:{ IOException -> 0x0106 }
            java.lang.String r8 = r3.format(r8)     // Catch:{ IOException -> 0x0106 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ IOException -> 0x0106 }
            java.lang.String r5 = r7.toString()     // Catch:{ IOException -> 0x0106 }
            java.net.URLConnection r7 = r13.conn     // Catch:{ IOException -> 0x0106 }
            java.lang.String r8 = "Range"
            r7.setRequestProperty(r8, r5)     // Catch:{ IOException -> 0x0106 }
            java.net.URLConnection r7 = r13.conn     // Catch:{ IOException -> 0x0106 }
            int r7 = r7.getConnectTimeout()     // Catch:{ IOException -> 0x0106 }
            r13.timeout = r7     // Catch:{ IOException -> 0x0106 }
            java.net.URLConnection r7 = r13.conn     // Catch:{ IOException -> 0x0106 }
            java.io.InputStream r4 = r7.getInputStream()     // Catch:{ IOException -> 0x0106 }
            r7 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r7]     // Catch:{ IOException -> 0x0106 }
        L_0x017d:
            int r7 = r4.read(r0)     // Catch:{ IOException -> 0x0106 }
            r13.byteread = r7     // Catch:{ IOException -> 0x0106 }
            if (r7 == r11) goto L_0x0199
            long r7 = r13.bytesum     // Catch:{ IOException -> 0x0106 }
            int r9 = r13.byteread     // Catch:{ IOException -> 0x0106 }
            long r9 = (long) r9     // Catch:{ IOException -> 0x0106 }
            long r7 = r7 + r9
            r13.bytesum = r7     // Catch:{ IOException -> 0x0106 }
            boolean r7 = r13.stop     // Catch:{ IOException -> 0x0106 }
            if (r7 == 0) goto L_0x01a7
            int r7 = r13.iRunTotle     // Catch:{ IOException -> 0x0106 }
            r13.iRun = r7     // Catch:{ IOException -> 0x0106 }
            r7 = 0
            r13.setFlag(r7)     // Catch:{ IOException -> 0x0106 }
        L_0x0199:
            boolean r7 = r13.stop     // Catch:{ IOException -> 0x0106 }
            if (r7 == 0) goto L_0x01a6
            r4.close()     // Catch:{ IOException -> 0x0106 }
            r7 = 0
            r13.conn = r7     // Catch:{ IOException -> 0x0106 }
            r7 = 0
            r13.url = r7     // Catch:{ IOException -> 0x0106 }
        L_0x01a6:
            throw r6     // Catch:{ IOException -> 0x0106 }
        L_0x01a7:
            boolean r7 = r13.intercept     // Catch:{ IOException -> 0x0106 }
            if (r7 != r12) goto L_0x01af
            r13.runControl()     // Catch:{ IOException -> 0x0106 }
            goto L_0x0199
        L_0x01af:
            long r7 = r13.bytesum     // Catch:{ IOException -> 0x0106 }
            long r9 = r13.downSize     // Catch:{ IOException -> 0x0106 }
            int r7 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r7 != 0) goto L_0x01de
            java.lang.String r7 = r13.tmpFile     // Catch:{ IOException -> 0x0106 }
            java.lang.String r8 = r13.desFile     // Catch:{ IOException -> 0x0106 }
            boolean r7 = r7.equals(r8)     // Catch:{ IOException -> 0x0106 }
            if (r7 != 0) goto L_0x01c8
            java.lang.String r7 = r13.tmpFile     // Catch:{ IOException -> 0x0106 }
            java.lang.String r8 = r13.desFile     // Catch:{ IOException -> 0x0106 }
            com.xl.util.FileUtil.fileRename(r7, r8)     // Catch:{ IOException -> 0x0106 }
        L_0x01c8:
            r7 = 4636737291354636288(0x4059000000000000, double:100.0)
            r13.setPercent(r7)     // Catch:{ IOException -> 0x0106 }
        L_0x01cd:
            java.io.FileOutputStream r7 = r13.fsOutput     // Catch:{ IOException -> 0x0106 }
            r8 = 0
            int r9 = r13.byteread     // Catch:{ IOException -> 0x0106 }
            r7.write(r0, r8, r9)     // Catch:{ IOException -> 0x0106 }
            long r7 = r13.startPos     // Catch:{ IOException -> 0x0106 }
            int r9 = r13.byteread     // Catch:{ IOException -> 0x0106 }
            long r9 = (long) r9     // Catch:{ IOException -> 0x0106 }
            long r7 = r7 + r9
            r13.startPos = r7     // Catch:{ IOException -> 0x0106 }
            goto L_0x017d
        L_0x01de:
            long r7 = r13.bytesum     // Catch:{ IOException -> 0x0106 }
            long r9 = r13.downSize     // Catch:{ IOException -> 0x0106 }
            long r7 = r7 / r9
            r9 = 100
            long r7 = r7 * r9
            double r7 = (double) r7     // Catch:{ IOException -> 0x0106 }
            r13.setPercent(r7)     // Catch:{ IOException -> 0x0106 }
            goto L_0x01cd
        L_0x01eb:
            boolean r6 = r13.intercept     // Catch:{ IOException -> 0x0106 }
            if (r6 != r12) goto L_0x01f4
            r13.runControl()     // Catch:{ IOException -> 0x0106 }
            goto L_0x00f8
        L_0x01f4:
            long r6 = r13.bytesum     // Catch:{ IOException -> 0x0106 }
            long r8 = r13.downSize     // Catch:{ IOException -> 0x0106 }
            int r6 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r6 != 0) goto L_0x0224
            java.lang.String r6 = r13.tmpFile     // Catch:{ IOException -> 0x0106 }
            java.lang.String r7 = r13.desFile     // Catch:{ IOException -> 0x0106 }
            boolean r6 = r6.equals(r7)     // Catch:{ IOException -> 0x0106 }
            if (r6 != 0) goto L_0x020d
            java.lang.String r6 = r13.tmpFile     // Catch:{ IOException -> 0x0106 }
            java.lang.String r7 = r13.desFile     // Catch:{ IOException -> 0x0106 }
            com.xl.util.FileUtil.fileRename(r6, r7)     // Catch:{ IOException -> 0x0106 }
        L_0x020d:
            r6 = 4636737291354636288(0x4059000000000000, double:100.0)
            r13.setPercent(r6)     // Catch:{ IOException -> 0x0106 }
        L_0x0212:
            java.io.FileOutputStream r6 = r13.fsOutput     // Catch:{ IOException -> 0x0106 }
            r7 = 0
            int r8 = r13.byteread     // Catch:{ IOException -> 0x0106 }
            r6.write(r0, r7, r8)     // Catch:{ IOException -> 0x0106 }
            long r6 = r13.startPos     // Catch:{ IOException -> 0x0106 }
            int r8 = r13.byteread     // Catch:{ IOException -> 0x0106 }
            long r8 = (long) r8     // Catch:{ IOException -> 0x0106 }
            long r6 = r6 + r8
            r13.startPos = r6     // Catch:{ IOException -> 0x0106 }
            goto L_0x00dc
        L_0x0224:
            long r6 = r13.bytesum     // Catch:{ IOException -> 0x0106 }
            long r8 = r13.downSize     // Catch:{ IOException -> 0x0106 }
            long r6 = r6 / r8
            r8 = 100
            long r6 = r6 * r8
            double r6 = (double) r6     // Catch:{ IOException -> 0x0106 }
            r13.setPercent(r6)     // Catch:{ IOException -> 0x0106 }
            goto L_0x0212
        L_0x0231:
            boolean r6 = r13.intercept     // Catch:{ IOException -> 0x0106 }
            if (r6 != r12) goto L_0x023a
            r13.runControl()     // Catch:{ IOException -> 0x0106 }
            goto L_0x0080
        L_0x023a:
            long r6 = r13.bytesum     // Catch:{ IOException -> 0x0106 }
            long r8 = r13.downSize     // Catch:{ IOException -> 0x0106 }
            int r6 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r6 != 0) goto L_0x026a
            java.lang.String r6 = r13.tmpFile     // Catch:{ IOException -> 0x0106 }
            java.lang.String r7 = r13.desFile     // Catch:{ IOException -> 0x0106 }
            boolean r6 = r6.equals(r7)     // Catch:{ IOException -> 0x0106 }
            if (r6 != 0) goto L_0x0253
            java.lang.String r6 = r13.tmpFile     // Catch:{ IOException -> 0x0106 }
            java.lang.String r7 = r13.desFile     // Catch:{ IOException -> 0x0106 }
            com.xl.util.FileUtil.fileRename(r6, r7)     // Catch:{ IOException -> 0x0106 }
        L_0x0253:
            r6 = 4636737291354636288(0x4059000000000000, double:100.0)
            r13.setPercent(r6)     // Catch:{ IOException -> 0x0106 }
        L_0x0258:
            java.io.FileOutputStream r6 = r13.fsOutput     // Catch:{ IOException -> 0x0106 }
            r7 = 0
            int r8 = r13.byteread     // Catch:{ IOException -> 0x0106 }
            r6.write(r0, r7, r8)     // Catch:{ IOException -> 0x0106 }
            long r6 = r13.startPos     // Catch:{ IOException -> 0x0106 }
            int r8 = r13.byteread     // Catch:{ IOException -> 0x0106 }
            long r8 = (long) r8     // Catch:{ IOException -> 0x0106 }
            long r6 = r6 + r8
            r13.startPos = r6     // Catch:{ IOException -> 0x0106 }
            goto L_0x0064
        L_0x026a:
            long r6 = r13.bytesum     // Catch:{ IOException -> 0x0106 }
            long r8 = r13.downSize     // Catch:{ IOException -> 0x0106 }
            long r6 = r6 / r8
            r8 = 100
            long r6 = r6 * r8
            double r6 = (double) r6     // Catch:{ IOException -> 0x0106 }
            r13.setPercent(r6)     // Catch:{ IOException -> 0x0106 }
            goto L_0x0258
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xl.util.Download.runCont():void");
    }

    public void runIntercept(boolean iFlag) {
        this.intercept = iFlag;
    }

    public void runStop(boolean iFlag) {
        this.stop = iFlag;
    }

    public int getInt(double b) {
        String strb = (b + 0.5d) + "";
        return Integer.parseInt(strb.substring(0, strb.indexOf(46)));
    }

    public double getPercent() {
        return this.percent;
    }

    public int getTimeOut() {
        return this.timeout;
    }

    public void setPercent(double percent2) {
        this.percent = percent2;
    }

    public boolean isFlag() {
        return this.flag;
    }

    public void setFlag(boolean flag2) {
        this.flag = flag2;
    }

    public long getStartPos() {
        return this.startPos;
    }

    public void setStartPos(int startPos2) {
        this.startPos = (long) startPos2;
    }

    public long getEndPos() {
        return this.endPos;
    }

    public void setEndPos(int endPos2) {
        this.endPos = (long) endPos2;
    }

    public long getBytesum() {
        return this.bytesum;
    }
}
