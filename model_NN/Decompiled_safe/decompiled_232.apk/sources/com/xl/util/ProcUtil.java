package com.xl.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class ProcUtil {
    public static void createTitle(String folderName, String author, String Name, String bookfile) {
        if (!FileUtil.fileExist(folderName)) {
            reCreateFolder(folderName);
        }
        String strFilePath = fillFolderTail(folderName) + "title.txt";
        FileUtil.delFile(strFilePath);
        PrintWriter pWriter = getFileWriter(strFilePath);
        pWriter.println("<book>");
        pWriter.println("<name>" + Name + "</name>");
        pWriter.println("<author>" + author + "</author>");
        pWriter.println("<bookfile>" + bookfile + "</bookfile>");
        pWriter.println("</book>");
        pWriter.flush();
        pWriter.close();
    }

    public static void createTitleLocal(String folderName, String author, String Name, String bookfile) {
        if (!FileUtil.fileExist(folderName)) {
            reCreateFolder(folderName);
        }
        String strFilePath = fillFolderTail(folderName) + "title.txt";
        FileUtil.delFile(strFilePath);
        PrintWriter pWriter = getFileWriter(strFilePath);
        pWriter.println("<book>");
        pWriter.println("<name>" + Name + "</name>");
        pWriter.println("<author>" + author + "</author>");
        pWriter.println("<bookfile>" + bookfile + "</bookfile>");
        pWriter.println("<enc>N</enc>");
        pWriter.println("</book>");
        pWriter.flush();
        pWriter.close();
    }

    public static void createIndex(String folderName, String[] chapterName, String[] chapterFile) {
        if (!FileUtil.fileExist(folderName)) {
            reCreateFolder(folderName);
        }
        String strFilePath = fillFolderTail(folderName) + "index.txt";
        FileUtil.delFile(strFilePath);
        PrintWriter pWriter = getFileWriter(strFilePath);
        System.out.println(chapterName.length);
        pWriter.println("<chapters>");
        for (int i = 0; i < chapterName.length; i++) {
            pWriter.println(((("" + "<chapter>") + "<title>" + chapterName[i] + "</title>") + "<fname>" + chapterFile[i] + "</fname>") + "</chapter>");
        }
        pWriter.println("</chapters>");
        pWriter.flush();
        pWriter.close();
    }

    public static void reCreateFolder(String folderPath) {
        try {
            FileUtil.delAllFile(folderPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileUtil.createFolder(folderPath);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static PrintWriter getFileWriter(String filePath) {
        File myFilePath = new File(filePath);
        if (myFilePath.exists()) {
            return null;
        }
        try {
            myFilePath.createNewFile();
            return new PrintWriter(new FileWriter(myFilePath));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void createFolder(String foldName) {
        try {
            FileUtil.createFolders(foldName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void keyReplacer(String fSample, String fDest, String keyIn, String KeyOut, String strEncode) {
        FileUtil.delFile(fDest);
        String strContent = "";
        try {
            strContent = FileUtil.readTxt(fSample, strEncode);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String strContent2 = strContent.replaceAll(keyIn, KeyOut);
        System.out.println(fDest + " processed!");
        try {
            FileUtil.createFile(fDest, strContent2, strEncode);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void printOut(Process p) {
        try {
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            System.out.println("Here is the standard output of the command:\n");
            while (true) {
                String s = stdInput.readLine();
                if (s == null) {
                    break;
                }
                System.out.println(s);
            }
            System.out.println("Here is the standard error of the command (if any):\n");
            while (true) {
                String s2 = stdError.readLine();
                if (s2 != null) {
                    System.out.println(s2);
                } else {
                    return;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String fillFolderTail(String folderName) {
        String strFolderName = folderName;
        if (!strFolderName.substring(strFolderName.length() - 1).equals("\\")) {
            return strFolderName + "\\";
        }
        return strFolderName;
    }
}
