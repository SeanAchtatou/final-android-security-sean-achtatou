package com.moose.game.bubble.fruit;

public final class R {

    public static final class anim {
        public static final int logo_move = 2130968576;
    }

    public static final class array {
        public static final int game_modes = 2131099648;
        public static final int sl_game_modes = 2131099649;
    }

    public static final class attr {
        public static final int adSize = 2130771974;
        public static final int adUnitId = 2130771975;
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
        public static final int testing = 2130771973;
    }

    public static final class color {
        public static final int default_error_text = 2131230722;
        public static final int default_highlight_background = 2131230720;
        public static final int sl_selector_color = 2131230723;
        public static final int user_info_background = 2131230721;
    }

    public static final class drawable {
        public static final int a1 = 2130837504;
        public static final int a1_1 = 2130837505;
        public static final int a1_2 = 2130837506;
        public static final int a2 = 2130837507;
        public static final int a2_1 = 2130837508;
        public static final int a2_2 = 2130837509;
        public static final int a3 = 2130837510;
        public static final int a3_1 = 2130837511;
        public static final int a3_2 = 2130837512;
        public static final int a4 = 2130837513;
        public static final int a4_1 = 2130837514;
        public static final int a4_2 = 2130837515;
        public static final int a5 = 2130837516;
        public static final int a5_1 = 2130837517;
        public static final int a5_2 = 2130837518;
        public static final int a6 = 2130837519;
        public static final int a6_1 = 2130837520;
        public static final int a6_2 = 2130837521;
        public static final int a7 = 2130837522;
        public static final int a7_1 = 2130837523;
        public static final int a7_2 = 2130837524;
        public static final int bg1 = 2130837525;
        public static final int bg2 = 2130837526;
        public static final int bg3 = 2130837527;
        public static final int bg_mask = 2130837528;
        public static final int bg_mask2 = 2130837529;
        public static final int bg_menu = 2130837530;
        public static final int bg_welldone = 2130837531;
        public static final int btn_press_bottom = 2130837532;
        public static final int button_background_focus = 2130837533;
        public static final int button_background_normal = 2130837534;
        public static final int dialog_divider_horizontal_light = 2130837535;
        public static final int gad_bg_splash = 2130837536;
        public static final int gad_btn_close = 2130837537;
        public static final int gad_btn_download = 2130837538;
        public static final int gad_btn_moregame = 2130837539;
        public static final int gad_close_down = 2130837540;
        public static final int gad_close_up = 2130837541;
        public static final int gad_download_down = 2130837542;
        public static final int gad_download_up = 2130837543;
        public static final int gad_more_down = 2130837544;
        public static final int gad_more_up = 2130837545;
        public static final int game_logo = 2130837546;
        public static final int icon = 2130837547;
        public static final int level = 2130837548;
        public static final int menu_my = 2130837549;
        public static final int num = 2130837550;
        public static final int select = 2130837551;
        public static final int sl_bg_btn = 2130837552;
        public static final int sl_bg_btn_pre = 2130837553;
        public static final int sl_bg_dropdown = 2130837554;
        public static final int sl_bg_dropdown_pre = 2130837555;
        public static final int sl_bg_h1 = 2130837556;
        public static final int sl_bg_list = 2130837557;
        public static final int sl_bg_list_pre = 2130837558;
        public static final int sl_divider = 2130837559;
        public static final int sl_divider_list = 2130837560;
        public static final int sl_logo = 2130837561;
        public static final int sl_menu_highscores = 2130837562;
        public static final int sl_menu_profile = 2130837563;
        public static final int sl_selector_btn = 2130837564;
        public static final int sl_selector_dropdown = 2130837565;
        public static final int sl_selector_list = 2130837566;
        public static final int timebar = 2130837567;
        public static final int timebar2 = 2130837568;
        public static final int timebar_fill = 2130837569;
        public static final int timebar_fill2 = 2130837570;
        public static final int top = 2130837571;
        public static final int tv_wave = 2130837572;
        public static final int tv_wave_1 = 2130837573;
        public static final int tv_wave_2 = 2130837574;
        public static final int tv_wave_3 = 2130837575;
        public static final int tv_wave_4 = 2130837576;
    }

    public static final class id {
        public static final int BANNER = 2131165184;
        public static final int Button01 = 2131165195;
        public static final int Button02 = 2131165196;
        public static final int IAB_BANNER = 2131165186;
        public static final int IAB_LEADERBOARD = 2131165187;
        public static final int IAB_MRECT = 2131165185;
        public static final int TextView02 = 2131165219;
        public static final int ad1 = 2131165203;
        public static final int ad2 = 2131165198;
        public static final int ad_bottom = 2131165194;
        public static final int ad_splash_layout = 2131165188;
        public static final int ad_top = 2131165193;
        public static final int btn_back = 2131165202;
        public static final int btn_close = 2131165191;
        public static final int btn_download = 2131165189;
        public static final int btn_exit = 2131165205;
        public static final int btn_more = 2131165190;
        public static final int btn_profile = 2131165211;
        public static final int bubblebreaker = 2131165192;
        public static final int email = 2131165218;
        public static final int exit = 2131165228;
        public static final int game_logo = 2131165222;
        public static final int game_mode_spinner = 2131165210;
        public static final int highscores_list_item = 2131165214;
        public static final int img_icon = 2131165206;
        public static final int list_view = 2131165212;
        public static final int login = 2131165216;
        public static final int lv = 2131165204;
        public static final int menu = 2131165197;
        public static final int more_game = 2131165227;
        public static final int myscore_view = 2131165213;
        public static final int options_sounds_checkbox = 2131165201;
        public static final int options_vibrate_checkbox = 2131165200;
        public static final int progress_indicator = 2131165209;
        public static final int rank = 2131165215;
        public static final int resume = 2131165224;
        public static final int score = 2131165217;
        public static final int score_board = 2131165226;
        public static final int scrollview2 = 2131165199;
        public static final int setting = 2131165225;
        public static final int spinnerTarget = 2131165221;
        public static final int start_game = 2131165223;
        public static final int title_login = 2131165208;
        public static final int tv_name = 2131165207;
        public static final int update_button = 2131165220;
    }

    public static final class layout {
        public static final int ad_splash = 2130903040;
        public static final int bb_layout = 2130903041;
        public static final int main = 2130903042;
        public static final int menu = 2130903043;
        public static final int options = 2130903044;
        public static final int popup_lv = 2130903045;
        public static final int popup_lv_item = 2130903046;
        public static final int sl_highscores = 2130903047;
        public static final int sl_highscores_list_item = 2130903048;
        public static final int sl_profile = 2130903049;
        public static final int sl_spinner_item = 2130903050;
        public static final int splash = 2130903051;
    }

    public static final class raw {
        public static final int bg_g1 = 2131034112;
        public static final int bg_g2 = 2131034113;
        public static final int bg_g3 = 2131034114;
        public static final int bg_g4 = 2131034115;
        public static final int bg_g5 = 2131034116;
        public static final int bg_menu = 2131034117;
        public static final int drip1 = 2131034118;
        public static final int drip2 = 2131034119;
        public static final int drip3 = 2131034120;
        public static final int drip4 = 2131034121;
        public static final int drip5 = 2131034122;
        public static final int drip6 = 2131034123;
        public static final int game_over = 2131034124;
        public static final int press = 2131034125;
        public static final int press_btn = 2131034126;
        public static final int well_done = 2131034127;
    }

    public static final class string {
        public static final int Menu_Close_Bg = 2131296363;
        public static final int Menu_Close_Ck = 2131296365;
        public static final int Menu_Open_Bg = 2131296362;
        public static final int Menu_Open_Ck = 2131296364;
        public static final int app_name = 2131296269;
        public static final int bb_layout_text_text = 2131296357;
        public static final int btn_accept_challenge = 2131296289;
        public static final int btn_check_rank = 2131296281;
        public static final int btn_choose_opponent = 2131296288;
        public static final int btn_directly_challenge_this_user = 2131296286;
        public static final int btn_game_over = 2131296280;
        public static final int btn_load_next = 2131296283;
        public static final int btn_load_prev = 2131296282;
        public static final int btn_open_url = 2131296277;
        public static final int btn_play_challenge = 2131296287;
        public static final int btn_random_score = 2131296279;
        public static final int btn_reject_challenge = 2131296290;
        public static final int btn_show_me = 2131296284;
        public static final int btn_update_profile = 2131296285;
        public static final int bubble_breaker = 2131296356;
        public static final int chalenge_stake_cap = 2131296296;
        public static final int challenge_anyone = 2131296299;
        public static final int challenge_anyone_cap = 2131296298;
        public static final int challenge_assigned = 2131296304;
        public static final int challenge_complete = 2131296305;
        public static final int challenge_invalid_cap = 2131296297;
        public static final int challenge_new_menu_item_direct = 2131296295;
        public static final int challenge_new_menu_item_open = 2131296294;
        public static final int challenge_open = 2131296306;
        public static final int challenge_open_cap = 2131296301;
        public static final int challenge_other_cap = 2131296302;
        public static final int challenge_pending_cap = 2131296303;
        public static final int challenge_rejected_cap = 2131296300;
        public static final int challenge_tab_history = 2131296292;
        public static final int challenge_tab_new = 2131296293;
        public static final int challenge_tab_open = 2131296291;
        public static final int challenge_won_lost_format = 2131296307;
        public static final int error_balance_to_low = 2131296327;
        public static final int error_message_cannot_accept_challenge = 2131296316;
        public static final int error_message_cannot_reject_challenge = 2131296317;
        public static final int error_message_challenge_upload = 2131296319;
        public static final int error_message_email_already_taken = 2131296325;
        public static final int error_message_insufficient_balance = 2131296318;
        public static final int error_message_invalid_email_format = 2131296326;
        public static final int error_message_name_already_taken = 2131296324;
        public static final int error_message_network = 2131296323;
        public static final int error_message_not_on_highscore_list = 2131296322;
        public static final int error_message_request_cancelled = 2131296320;
        public static final int error_message_self_challenge = 2131296315;
        public static final int error_message_user_info_update_failed = 2131296321;
        public static final int facebook_comm_cancelled = 2131296346;
        public static final int facebook_comm_failed = 2131296345;
        public static final int facebook_comm_ok = 2131296344;
        public static final int facebook_invalid_credentials = 2131296347;
        public static final int game_mode_label = 2131296328;
        public static final int game_score_label = 2131296329;
        public static final int lost = 2131296341;
        public static final int mExit = 2131296367;
        public static final int mScore = 2131296366;
        public static final int menu_item_challenges = 2131296272;
        public static final int menu_item_highscores = 2131296271;
        public static final int menu_item_payment = 2131296276;
        public static final int menu_item_play = 2131296270;
        public static final int menu_item_post_message = 2131296275;
        public static final int menu_item_profile = 2131296274;
        public static final int menu_item_users = 2131296273;
        public static final int message_no_auth = 2131296278;
        public static final int message_post_failed = 2131296343;
        public static final int message_post_ok = 2131296342;
        public static final int mode_lose_prefix = 2131296360;
        public static final int mode_lose_suffix = 2131296361;
        public static final int mode_pause = 2131296359;
        public static final int mode_ready = 2131296358;
        public static final int myspace_comm_cancelled = 2131296350;
        public static final int myspace_comm_failed = 2131296349;
        public static final int myspace_comm_ok = 2131296348;
        public static final int myspace_invalid_credentials = 2131296351;
        public static final int name_cap = 2131296338;
        public static final int new_challenge_balance = 2131296313;
        public static final int new_challenge_stake_header = 2131296314;
        public static final int new_direct_challenge = 2131296311;
        public static final int new_open_challenge = 2131296312;
        public static final int play_for_random_score = 2131296336;
        public static final int profile_email_label = 2131296310;
        public static final int profile_name_label = 2131296309;
        public static final int progress_message_default = 2131296337;
        public static final int ranking_check_failed = 2131296334;
        public static final int ranking_check_label = 2131296335;
        public static final int score_submit_failed = 2131296333;
        public static final int sl_email = 2131296260;
        public static final int sl_error_message_email_already_taken = 2131296267;
        public static final int sl_error_message_invalid_email_format = 2131296268;
        public static final int sl_error_message_name_already_taken = 2131296266;
        public static final int sl_error_message_network = 2131296265;
        public static final int sl_error_message_not_on_highscore_list = 2131296264;
        public static final int sl_highscores = 2131296257;
        public static final int sl_login = 2131296259;
        public static final int sl_next = 2131296262;
        public static final int sl_prev = 2131296261;
        public static final int sl_profile = 2131296256;
        public static final int sl_top = 2131296263;
        public static final int sl_update_profile = 2131296258;
        public static final int status_cap = 2131296339;
        public static final int submitted_challenge_score_info = 2131296332;
        public static final int submitted_challenge_score_label = 2131296331;
        public static final int submitted_score_label = 2131296330;
        public static final int twitter_comm_cancelled = 2131296354;
        public static final int twitter_comm_failed = 2131296353;
        public static final int twitter_comm_ok = 2131296352;
        public static final int twitter_invalid_credentials = 2131296355;
        public static final int user_info_format = 2131296308;
        public static final int won = 2131296340;
    }

    public static final class style {
        public static final int sl_heading = 2131361792;
        public static final int sl_normal = 2131361794;
        public static final int sl_title_bar = 2131361793;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval, R.attr.testing, R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 6;
        public static final int com_google_ads_AdView_adUnitId = 7;
        public static final int com_google_ads_AdView_backgroundColor = 0;
        public static final int com_google_ads_AdView_keywords = 3;
        public static final int com_google_ads_AdView_primaryTextColor = 1;
        public static final int com_google_ads_AdView_refreshInterval = 4;
        public static final int com_google_ads_AdView_secondaryTextColor = 2;
        public static final int com_google_ads_AdView_testing = 5;
    }
}
