package com.moose.game.bubble.fruit;

public interface BBConstants {
    public static final int BLUE_BUBBLE = 0;
    public static final int BUBBLE_COLOR_COUNT = 6;
    public static final int BUBBLE_DIAMETER = 38;
    public static final int CYAN_BUBBLE = 1;
    public static final int EXIT = 5;
    public static final int GREEN_BUBBLE = 2;
    public static final String KEY_CFG_BASE = "com.moose.game.bubble.fruit";
    public static final String KEY_CFG_SOUND = "com.moose.game.bubble.fruit.sound";
    public static final String KEY_CFG_VIBRATE = "com.moose.game.bubble.fruit.vibrate";
    public static final int MAGENTA_BUBBLE = 3;
    public static final int MUSIC_ID_GAME = 2;
    public static final int MUSIC_ID_MENU = 1;
    public static final int NULL_BUBBLE = -1;
    public static final int ORANGE_BUBBLE = 5;
    public static final int PAUSE = 0;
    public static final int READY = 1;
    public static final int RED_BUBBLE = 4;
    public static final int RUNNING = 2;
    public static final String SL_GAME_ID = "ff143c51-87bd-4a40-8e27-71afb8c265fa";
    public static final String SL_GAME_SECRET = "G3WroMSW/0Ggow1lkE7hw5pgWp+UX0ZG46lIkALSFAXM5gSn3AlxmA==";
    public static final int SOUND_ID_BASE = 0;
    public static final int SOUND_ID_DRIP1 = 1;
    public static final int SOUND_ID_DRIP2 = 2;
    public static final int SOUND_ID_DRIP3 = 3;
    public static final int SOUND_ID_DRIP4 = 4;
    public static final int SOUND_ID_DRIP5 = 5;
    public static final int SOUND_ID_DRIP6 = 6;
    public static final int SOUND_ID_GAMEOVER = 7;
    public static final int SOUND_ID_MAX = 11;
    public static final int SOUND_ID_PRESS_BALL = 9;
    public static final int SOUND_ID_PRESS_BTN = 10;
    public static final int SOUND_ID_WELLDONE = 8;
    public static final int UPGRADE = 4;
    public static final int WHITE_BUBBLE = 6;
}
