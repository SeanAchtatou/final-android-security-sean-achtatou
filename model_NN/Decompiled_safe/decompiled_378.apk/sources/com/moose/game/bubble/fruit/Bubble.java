package com.moose.game.bubble.fruit;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import com.google.ads.AdView;
import com.scoreloop.android.coreui.ScoreloopManager;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;

public class Bubble extends Activity {
    public static final int CBGMUSIC = 2;
    public static final int CCKMUSIC = 4;
    public static final int OBGMUSIC = 1;
    public static final int OCKMUSIC = 3;
    private AdView mAdBottom;
    private AdView mAdTop;
    /* access modifiers changed from: private */
    public BBView mBBView;
    public MediaPlayer mBurstMP;
    public MediaPlayer mClickMP;
    public boolean mIsSoundOn = false;
    public boolean mIsVibrateOn = false;
    public MediaPlayer mMusicMP;
    public SoundEffect mSoundEffect;
    private ProgressDialog waitDlg = null;
    private Window window;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mMusicMP = null;
        this.mClickMP = null;
        this.mBurstMP = null;
        requestWindowFeature(1);
        this.window = getWindow();
        this.window.setFlags(1024, 1024);
        setContentView((int) R.layout.bb_layout);
        this.mBBView = (BBView) findViewById(R.id.bubblebreaker);
        this.mBBView.setMain(this);
        this.mAdTop = (AdView) findViewById(R.id.ad_top);
        this.mAdBottom = (AdView) findViewById(R.id.ad_bottom);
        showAd(false);
        this.mBBView.setMode(1);
        loadGameSetting();
        this.mSoundEffect = new SoundEffect(getBaseContext());
        this.mSoundEffect.setSoundOn(this.mIsSoundOn);
        boolean isResume = false;
        Bundle b = getIntent().getExtras();
        if (b != null) {
            isResume = b.getBoolean("resume", false);
        }
        this.mBBView.initBBView(isResume);
    }

    public void showAd(boolean b) {
        if (this.mAdTop != null) {
            if (b) {
                this.mAdTop.setVisibility(0);
                this.mAdBottom.setVisibility(8);
                return;
            }
            this.mAdTop.setVisibility(8);
            this.mAdBottom.setVisibility(0);
        }
    }

    public void finish() {
        try {
            this.mBBView.mMode = 0;
            this.mSoundEffect.free();
            this.mBBView.exitView();
        } finally {
            super.finish();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.mBBView.mMode == 5) {
            return false;
        }
        if (this.mBBView.mTotalScore == 0) {
            exitMain();
            return false;
        }
        showSubmitScoreDlg("Submit Score?", "Do you want to submit your score (" + this.mBBView.mTotalScore + ") to the global high scores list?");
        return false;
    }

    public void onStop() {
        super.onStop();
    }

    public void onPause() {
        super.onPause();
        this.mSoundEffect.pauseMusic();
    }

    public void onResume() {
        super.onResume();
        this.mSoundEffect.resumeMusic(2);
    }

    public void playVibrate() {
        if (this.mIsVibrateOn) {
            try {
                ((Vibrator) getSystemService("vibrator")).vibrate(new long[]{10, 30, 20, 10}, -1);
            } catch (Exception e) {
            }
        }
    }

    public void ToMainView(int score) {
        finish();
    }

    public void ToFirstView() {
        finish();
    }

    /* access modifiers changed from: private */
    public void waitDlgShow() {
        this.waitDlg = ProgressDialog.show(this, "", "Please wait,Submit Score...", true);
        this.waitDlg.setCancelable(true);
    }

    /* access modifiers changed from: private */
    public void waitDlgClose() {
        this.waitDlg.dismiss();
        this.waitDlg = null;
    }

    public void showSubmitScoreDlg(String sTitle, String sMsg) {
        AlertDialog dlg;
        int saveGameMode = this.mBBView.mMode;
        this.mBBView.setMode(0);
        if (saveGameMode == 2) {
            dlg = new AlertDialog.Builder(this).setMessage(sMsg).setTitle(sTitle).setIcon((int) R.drawable.icon).setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Bubble.this.waitDlgShow();
                    ScoreloopManager.submitScore(Bubble.this.mBBView.mTotalScore, 0, new ScoreSubmitObserver(Bubble.this, null));
                }
            }).setNeutralButton("Discard", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Bubble.this.exitMain();
                }
            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                }
            }).create();
        } else {
            dlg = new AlertDialog.Builder(this).setMessage(sMsg).setTitle(sTitle).setIcon((int) R.drawable.icon).setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Bubble.this.waitDlgShow();
                    ScoreloopManager.submitScore(Bubble.this.mBBView.mTotalScore, 0, new ScoreSubmitObserver(Bubble.this, null));
                }
            }).setNeutralButton("Discard", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Bubble.this.exitMain();
                }
            }).create();
        }
        dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialog) {
                Log.v("Bubble", "onDismiss()...call");
                Bubble.this.mBBView.setMode(2);
            }
        });
        dlg.show();
    }

    /* access modifiers changed from: private */
    public void exitMain() {
        showGameMenu();
        finish();
    }

    private void showGameMenu() {
        Intent itMenu = new Intent();
        itMenu.setClass(this, MenuActivity.class);
        startActivity(itMenu);
    }

    private void loadGameSetting() {
        SharedPreferences mBaseSettings = getSharedPreferences(BBConstants.KEY_CFG_BASE, 0);
        this.mIsSoundOn = mBaseSettings.getBoolean(BBConstants.KEY_CFG_SOUND, true);
        this.mIsVibrateOn = mBaseSettings.getBoolean(BBConstants.KEY_CFG_VIBRATE, true);
    }

    private class ScoreSubmitObserver implements RequestControllerObserver {
        private ScoreSubmitObserver() {
        }

        /* synthetic */ ScoreSubmitObserver(Bubble bubble, ScoreSubmitObserver scoreSubmitObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            Bubble.this.showSubmitScoreDlg("Error", "Network error, submit failed, Try again?");
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            Log.v("Bubble", "ScoreSubmitObserver(): ok");
            Bubble.this.waitDlgClose();
            Bubble.this.exitMain();
        }
    }
}
