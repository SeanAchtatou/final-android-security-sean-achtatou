package com.moose.game.bubble.fruit;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import java.util.Timer;
import java.util.TimerTask;

public class AdSplash extends Activity {
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (AdSplash.this.mIsRunning) {
                AdSplash.this.finish();
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean mIsRunning = false;
    TimerTask mTask = new TimerTask() {
        public void run() {
            Message message = new Message();
            message.what = 1;
            AdSplash.this.handler.sendMessage(message);
        }
    };
    Timer mTimer = new Timer();

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(1024, 1024);
        setContentView(R.layout.ad_splash);
        int delay = 15000;
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.ad_splash_layout);
        Button btnDownload = (Button) findViewById(R.id.btn_download);
        Button btnMoreGame = (Button) findViewById(R.id.btn_more);
        ((Button) findViewById(R.id.btn_close)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GameAd.doGameAdClick();
                AdSplash.this.finish();
            }
        });
        if (GameAd.checkShowGameAd()) {
            String fullName = GameAd.getLocalImgFullNameById(GameAd.getShowGameId());
            Log.v("AdSplash", "show adsplash...imgFile=" + fullName);
            try {
                Bitmap tempBmp = BitmapFactory.decodeFile(fullName);
                if (tempBmp != null) {
                    if (tempBmp.getWidth() > tempBmp.getHeight()) {
                        setRequestedOrientation(0);
                    } else {
                        setRequestedOrientation(1);
                    }
                    layout.setBackgroundDrawable(new BitmapDrawable(tempBmp));
                    btnDownload.setVisibility(0);
                    btnMoreGame.setVisibility(8);
                    btnDownload.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Log.v("AdSplash", "btnDownload, getShowGameId()");
                            GameAd.doGameAdClick();
                            AdSplash.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(GameAd.getShowGameUrl())));
                        }
                    });
                }
            } catch (Exception e) {
            }
        } else {
            delay = 8000;
            btnDownload.setVisibility(8);
            btnMoreGame.setVisibility(0);
            btnMoreGame.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.v("AdSplash", "moreGame.click(), more url=" + GameAd.getMoreGameUrl());
                    try {
                        AdSplash.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(GameAd.getMoreGameUrl())));
                    } catch (Exception e) {
                    }
                }
            });
        }
        this.mTimer.schedule(this.mTask, (long) delay, (long) 100);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mIsRunning = false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mIsRunning = true;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.mIsRunning = true;
    }

    public void finish() {
        if (this.mTimer != null) {
            this.mTimer.cancel();
            this.mTimer = null;
        }
        Log.v("AdSplash", "finish()....is call!");
        super.finish();
        Process.killProcess(Process.myPid());
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        GameAd.doGameAdClick();
        finish();
        return true;
    }
}
