package com.moose.game.bubble.fruit;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

public class Prefs extends Activity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public SharedPreferences mBaseSettings;
    private SharedPreferences mRankingSettings;
    private EditText mUserNameEditText;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.options);
        this.mBaseSettings = getSharedPreferences(BBConstants.KEY_CFG_BASE, 0);
        CheckBox vibrateCheckbox = (CheckBox) findViewById(R.id.options_vibrate_checkbox);
        vibrateCheckbox.setChecked(this.mBaseSettings.getBoolean(BBConstants.KEY_CFG_VIBRATE, true));
        vibrateCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Prefs.this.mBaseSettings.edit().putBoolean(BBConstants.KEY_CFG_VIBRATE, true).commit();
                } else {
                    Prefs.this.mBaseSettings.edit().putBoolean(BBConstants.KEY_CFG_VIBRATE, false).commit();
                }
            }
        });
        CheckBox soundsCheckbox = (CheckBox) findViewById(R.id.options_sounds_checkbox);
        soundsCheckbox.setChecked(this.mBaseSettings.getBoolean(BBConstants.KEY_CFG_SOUND, true));
        soundsCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Prefs.this.mBaseSettings.edit().putBoolean(BBConstants.KEY_CFG_SOUND, true).commit();
                } else {
                    Prefs.this.mBaseSettings.edit().putBoolean(BBConstants.KEY_CFG_SOUND, false).commit();
                }
            }
        });
        ((Button) findViewById(R.id.btn_back)).setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back /*2131165202*/:
                finish();
                return;
            default:
                return;
        }
    }

    private void showToast(int strId) {
        Toast toast = Toast.makeText(this, strId, 0);
        toast.setGravity(48, 0, 220);
        toast.show();
    }
}
