package com.moose.game.bubble.fruit;

import java.lang.reflect.Array;
import java.util.Random;

public class BBMatrix {
    public int[][] mBubbleGrid;
    public boolean[][] mBubbleMarked;
    private Random mRand = new Random();
    public int mSameBubbleCount;
    private int mTypeCount = 6;
    public int mXBubbleCount;
    public int mYBubbleCount;

    public void setTypeCountByLevel(int level) {
        switch (level) {
            case 1:
                this.mTypeCount = 2;
                return;
            case 2:
                this.mTypeCount = 3;
                return;
            case 3:
                this.mTypeCount = 4;
                return;
            case 4:
            case 5:
                this.mTypeCount = 5;
                return;
            case 6:
            case 7:
                this.mTypeCount = 6;
                return;
            case BBConstants.SOUND_ID_WELLDONE:
                this.mTypeCount = 6;
                return;
            default:
                this.mTypeCount = 6;
                return;
        }
    }

    public BBMatrix(int viewWidth, int viewHeight) {
        this.mXBubbleCount = (int) Math.floor((double) (viewWidth / 40));
        this.mYBubbleCount = (int) Math.floor((double) (viewHeight / 40));
        this.mBubbleGrid = (int[][]) Array.newInstance(Integer.TYPE, this.mXBubbleCount, this.mYBubbleCount);
        this.mBubbleMarked = (boolean[][]) Array.newInstance(Boolean.TYPE, this.mXBubbleCount, this.mYBubbleCount);
        clearBubbles();
    }

    public void fillBBMatrix() {
        int[] idxArray = new int[this.mTypeCount];
        for (int i = 0; i < this.mTypeCount; i++) {
            idxArray[i] = this.mRand.nextInt(6);
        }
        for (int i2 = 0; i2 < this.mXBubbleCount; i2++) {
            for (int j = 0; j < this.mYBubbleCount; j++) {
                newBubble(i2, j, (int) (Math.random() * ((double) this.mTypeCount)));
            }
        }
    }

    public int getRandomBubbleId() {
        return (int) (Math.random() * ((double) this.mTypeCount));
    }

    public void newBubble(int x, int y, int pBubbleType) {
        this.mBubbleGrid[x][y] = pBubbleType;
        this.mBubbleMarked[x][y] = false;
    }

    public void findSameBubble(int x, int y) {
        if (x >= 0 && y >= 0 && x < this.mXBubbleCount && y < this.mYBubbleCount && this.mBubbleGrid[x][y] != -1) {
            this.mSameBubbleCount++;
            this.mBubbleMarked[x][y] = true;
            int color = this.mBubbleGrid[x][y];
            if (getColor(x, y + 1) == color && !isMarked(x, y + 1)) {
                findSameBubble(x, y + 1);
            }
            if (getColor(x, y - 1) == color && !isMarked(x, y - 1)) {
                findSameBubble(x, y - 1);
            }
            if (getColor(x + 1, y) == color && !isMarked(x + 1, y)) {
                findSameBubble(x + 1, y);
            }
            if (getColor(x - 1, y) == color && !isMarked(x - 1, y)) {
                findSameBubble(x - 1, y);
            }
        }
    }

    public boolean isEmptyColumn(int x) {
        return isNullBubble(x, this.mYBubbleCount - 1);
    }

    public void removeMarkedBubbles() {
        for (int y = 0; y < this.mYBubbleCount; y++) {
            for (int x = 0; x < this.mXBubbleCount; x++) {
                if (isMarked(x, y)) {
                    removeBubble(x, y);
                    for (int i = y; i > 0; i--) {
                        moveBubble(x, i - 1, x, i);
                    }
                }
            }
        }
        for (int x2 = 0; x2 < this.mXBubbleCount; x2++) {
            if (isEmptyColumn(x2)) {
                int i2 = x2 + 1;
                while (i2 < this.mXBubbleCount && isEmptyColumn(i2)) {
                    i2++;
                }
                if (i2 < this.mXBubbleCount) {
                    for (int y2 = 0; y2 < this.mYBubbleCount; y2++) {
                        moveBubble(i2, y2, x2, y2);
                    }
                }
            }
        }
    }

    public void removeBubble(int x, int y) {
        if (!isNullBubble(x, y)) {
            this.mBubbleGrid[x][y] = -1;
        }
    }

    public void removeMark() {
        this.mSameBubbleCount = 0;
        for (int i = 0; i < this.mXBubbleCount; i++) {
            for (int j = 0; j < this.mYBubbleCount; j++) {
                this.mBubbleMarked[i][j] = false;
            }
        }
    }

    public void moveBubble(int x, int y, int toX, int toY) {
        if (x != toX || y != toY) {
            this.mBubbleGrid[toX][toY] = this.mBubbleGrid[x][y];
            this.mBubbleGrid[x][y] = -1;
        }
    }

    public boolean isNullBubble(int x, int y) {
        if (x < 0 || y < 0 || x >= this.mXBubbleCount || y >= this.mYBubbleCount || -1 == this.mBubbleGrid[x][y]) {
            return true;
        }
        return false;
    }

    public int getColor(int x, int y) {
        if (isNullBubble(x, y)) {
            return -1;
        }
        return this.mBubbleGrid[x][y];
    }

    public boolean isMarked(int x, int y) {
        if (isNullBubble(x, y)) {
            return false;
        }
        return this.mBubbleMarked[x][y];
    }

    public void clearBubbles() {
        for (int x = 0; x < this.mXBubbleCount; x++) {
            for (int y = 0; y < this.mYBubbleCount; y++) {
                setBubble(x, y, -1);
            }
        }
    }

    public void setBubble(int x, int y, int bubbleIndex) {
        this.mBubbleGrid[x][y] = bubbleIndex;
    }

    public boolean isBBMatrixSolvable() {
        for (int y = this.mYBubbleCount - 1; y > -1; y--) {
            for (int x = 0; x < this.mXBubbleCount; x++) {
                if (!isNullBubble(x, y)) {
                    if (!isNullBubble(x + 1, y) && this.mBubbleGrid[x][y] == this.mBubbleGrid[x + 1][y]) {
                        return true;
                    }
                    if (!isNullBubble(x, y - 1) && this.mBubbleGrid[x][y] == this.mBubbleGrid[x][y - 1]) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
