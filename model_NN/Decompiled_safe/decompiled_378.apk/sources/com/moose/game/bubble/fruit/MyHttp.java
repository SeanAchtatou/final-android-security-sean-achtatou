package com.moose.game.bubble.fruit;

import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public class MyHttp {
    private HttpClient httpClient;
    private HttpParams httpParams;

    public HttpClient getHttpClient() {
        this.httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(this.httpParams, 20000);
        HttpConnectionParams.setSoTimeout(this.httpParams, 20000);
        HttpConnectionParams.setSocketBufferSize(this.httpParams, 8192);
        HttpClientParams.setRedirecting(this.httpParams, true);
        HttpProtocolParams.setUserAgent(this.httpParams, "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16");
        this.httpClient = new DefaultHttpClient(this.httpParams);
        return this.httpClient;
    }

    public InputStream doGet(String url, Map params) {
        if (params != null) {
            String paramStr = "";
            for (Map.Entry entry : params.entrySet()) {
                Object key = entry.getKey();
                paramStr = String.valueOf(paramStr) + ("&" + key + "=" + entry.getValue());
            }
            if (!paramStr.equals("")) {
                url = String.valueOf(url) + paramStr.replaceFirst("&", "?");
            }
        }
        try {
            HttpGet httpRequest = new HttpGet(url);
            if (this.httpClient == null) {
                getHttpClient();
            }
            HttpResponse httpResponse = this.httpClient.execute(httpRequest);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                return httpResponse.getEntity().getContent();
            }
            Log.v("DownSongList", "doGet(): error");
            Log.v("DownSongList", "doGet(): null");
            return null;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }
}
