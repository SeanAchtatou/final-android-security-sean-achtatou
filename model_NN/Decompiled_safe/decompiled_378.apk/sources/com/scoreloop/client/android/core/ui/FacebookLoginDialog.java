package com.scoreloop.client.android.core.ui;

import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import com.a.a.g;
import com.a.a.l;
import com.a.a.p;

public class FacebookLoginDialog extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    private p f107a;
    private l b;

    public FacebookLoginDialog(Context context, p pVar) {
        super(context, 16973841);
        this.f107a = pVar;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            this.f107a.b(this.b);
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.b = new l(getContext(), g.a());
        this.b.a("Scoreloop");
        this.b.a(this.f107a);
        this.b.c();
        setContentView(this.b);
    }
}
