package com.scoreloop.client.android.core.controller;

import com.a.a.g;
import com.scoreloop.client.android.core.model.FacebookSocialProvider;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.ui.FacebookAuthViewController;

public class FacebookSocialProviderController extends SocialProviderController {
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public UserController d;
    /* access modifiers changed from: private */
    public FacebookAuthViewController e;

    public FacebookSocialProviderController(Session session, SocialProviderControllerObserver socialProviderControllerObserver) {
        super(session, socialProviderControllerObserver);
    }

    private g h() {
        return ((FacebookSocialProvider) f()).d();
    }

    private void i() {
        h().d().remove(this);
    }

    public void a() {
        ((FacebookSocialProvider) f()).a(e().getUser(), h().h(), c());
        i();
        this.c = true;
        this.e.a(true);
        d().didSucceed();
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (f().isUserConnected(e().getUser())) {
            a();
            return;
        }
        this.e = new FacebookAuthViewController(new C0006e(this));
        a(c(), this.e);
    }
}
