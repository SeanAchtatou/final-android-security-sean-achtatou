package com.scoreloop.client.android.core.ui;

import android.view.View;
import android.widget.EditText;

class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ EditText f118a;
    final /* synthetic */ EditText b;
    final /* synthetic */ MyspaceCredentialsDialog c;

    c(MyspaceCredentialsDialog myspaceCredentialsDialog, EditText editText, EditText editText2) {
        this.c = myspaceCredentialsDialog;
        this.f118a = editText;
        this.b = editText2;
    }

    public void onClick(View view) {
        this.c.f111a.a(this.f118a.getText().toString(), this.b.getText().toString());
    }
}
