package com.scoreloop.client.android.core.ui;

import android.content.Context;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class MyspaceCaptchaDialog extends ScoreloopCustomDialog {

    /* renamed from: a  reason: collision with root package name */
    private String f110a;
    /* access modifiers changed from: private */
    public CaptchaCompletionListener b;

    public interface CaptchaCompletionListener {
        void a(String str);
    }

    public MyspaceCaptchaDialog(Context context, CaptchaCompletionListener captchaCompletionListener, String str) {
        super(context);
        this.b = captchaCompletionListener;
        this.f110a = str;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        WebView webView = new WebView(getContext());
        webView.loadUrl(this.f110a);
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.addView(webView);
        EditText editText = new EditText(getContext());
        editText.setWidth(100);
        linearLayout.addView(editText);
        Button button = new Button(getContext());
        button.setWidth(100);
        button.setText("submit");
        button.setOnClickListener(new p(this, editText));
        linearLayout.addView(button);
        setContentView(linearLayout);
    }
}
