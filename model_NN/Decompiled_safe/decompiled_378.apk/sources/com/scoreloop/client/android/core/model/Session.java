package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.SessionObserver;
import com.scoreloop.client.android.core.server.Server;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class Session {

    /* renamed from: a  reason: collision with root package name */
    private static Session f85a;
    private final Device b = new Device();
    private Game c;
    private final SessionObserver d;
    private final Server e;
    private String f;
    private Challenge g;
    private List h = new ArrayList();
    private State i = State.INITIAL;
    private final User j = new User();

    public enum State {
        AUTHENTICATED,
        AUTHENTICATING,
        DEVICE_KNOWN,
        DEVICE_UNKNOWN,
        DEVICE_VERIFIED,
        FAILED,
        INITIAL,
        READY
    }

    public Session(SessionObserver sessionObserver, Server server) {
        this.e = server;
        this.d = sessionObserver;
    }

    static void a(Session session) {
        f85a = session;
    }

    public static Session getCurrentSession() {
        return f85a;
    }

    public Device a() {
        return this.b;
    }

    public void a(Game game) {
        this.c = game;
    }

    public void a(State state) {
        this.i = state;
    }

    public void a(List list) {
        this.h = list;
    }

    public void a(JSONObject jSONObject) {
        this.f = jSONObject.getString("direct_pay_url");
    }

    public Server b() {
        return this.e;
    }

    public State c() {
        return this.i;
    }

    public Money getBalance() {
        return getUser().a();
    }

    public Challenge getChallenge() {
        return this.g;
    }

    public List getChallengeStakes() {
        return this.h;
    }

    public Game getGame() {
        return this.c;
    }

    public String getPaymentURL() {
        return this.f;
    }

    public List getScoreSearchLists() {
        return getUser().c();
    }

    public User getUser() {
        return this.j;
    }

    public boolean isAuthenticated() {
        return this.i == State.AUTHENTICATED;
    }

    public void setChallenge(Challenge challenge) {
        this.g = challenge;
    }
}
