package com.scoreloop.client.android.core.ui;

import android.content.DialogInterface;

class b implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FacebookAuthViewController f117a;

    b(FacebookAuthViewController facebookAuthViewController) {
        this.f117a = facebookAuthViewController;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f117a.a().userDidCancel();
    }
}
