package com.scoreloop.client.android.core.ui;

import com.a.a.d;
import com.a.a.q;

final class a extends d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f116a;

    private a(h hVar) {
        this.f116a = hVar;
    }

    /* synthetic */ a(h hVar, b bVar) {
        this(hVar);
    }

    public void a(q qVar) {
        this.f116a.f123a.c();
    }

    public void a(q qVar, Throwable th) {
        this.f116a.f123a.a().didFail(th);
    }

    public void b(q qVar) {
        this.f116a.f123a.a().userDidCancel();
    }
}
