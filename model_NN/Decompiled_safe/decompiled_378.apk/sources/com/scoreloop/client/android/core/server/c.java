package com.scoreloop.client.android.core.server;

import com.scoreloop.client.android.core.utils.Logger;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class c {

    /* renamed from: a  reason: collision with root package name */
    private String f100a = null;
    private Boolean b = false;
    /* access modifiers changed from: private */
    public g c;
    private final BayeuxConnectionObserver d;
    private String e;
    private URI f;

    c(URL url, BayeuxConnectionObserver bayeuxConnectionObserver, byte[] bArr) {
        if (bayeuxConnectionObserver == null) {
            throw new IllegalArgumentException("Observer can't be null");
        }
        try {
            this.f = url.toURI();
            this.d = bayeuxConnectionObserver;
            if (bArr != null) {
                this.c = new b(this.f, bArr);
            } else {
                this.c = new g(this.f);
            }
        } catch (URISyntaxException e2) {
            throw new IllegalArgumentException("Malformed URL");
        }
    }

    private void a(i iVar, Integer num, JSONObject jSONObject, String str, JSONObject jSONObject2) {
        JSONObject jSONObject3;
        if (str == null) {
            throw new IllegalArgumentException("Request's channel can't be null !");
        }
        if (this.f100a == null) {
            Logger.a("BayeuxConnection", "executeRequest(): about to handshake");
            b(iVar);
            Logger.a("ServerCommThread", "executeRequest(): handshake completed");
        }
        JSONObject jSONObject4 = new JSONObject();
        try {
            jSONObject4.put("clientId", b());
            jSONObject4.put("channel", str);
            jSONObject4.put("data", jSONObject);
            jSONObject4.put("id", num);
            if (this.e != null) {
                jSONObject3 = jSONObject2 == null ? new JSONObject() : jSONObject2;
                jSONObject3.put("token", this.e);
            } else {
                jSONObject3 = jSONObject2;
            }
            jSONObject4.putOpt("ext", jSONObject3);
            a(iVar, jSONObject4);
        } catch (JSONException e2) {
            throw new d(e2);
        }
    }

    private void a(i iVar, JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(jSONObject);
        JSONArray a2 = this.c.a(iVar.b(), jSONArray);
        int length = a2.length();
        int i = 0;
        int i2 = 0;
        while (i < length) {
            try {
                JSONObject jSONObject2 = a2.getJSONObject(i);
                String string = jSONObject2.getString("channel");
                Object opt = jSONObject2.opt("data");
                Integer valueOf = jSONObject2.has("id") ? Integer.valueOf(jSONObject2.optInt("id")) : null;
                JSONObject optJSONObject = jSONObject2.optJSONObject("ext");
                if (optJSONObject != null) {
                    i2 = optJSONObject.optInt("status");
                    if (optJSONObject.has("token")) {
                        this.e = optJSONObject.getString("token");
                    }
                }
                int i3 = i2;
                if ("/meta/handshake".equalsIgnoreCase(string)) {
                    c(jSONObject2);
                } else if ("/meta/connect".equalsIgnoreCase(string)) {
                    a(jSONObject2);
                } else if ("/meta/disconnect".equalsIgnoreCase(string)) {
                    b(jSONObject2);
                } else {
                    this.d.a(this, valueOf, opt, string, i3);
                }
                i++;
                i2 = i3;
            } catch (JSONException e2) {
                throw new f();
            }
        }
    }

    private void a(JSONObject jSONObject) {
    }

    private void b(i iVar) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("channel", "/meta/handshake");
            jSONObject.put("version", "1");
            JSONArray jSONArray = new JSONArray();
            jSONArray.put("request-response");
            jSONObject.put("supportedConnectionTypes", jSONArray);
            JSONObject jSONObject2 = new JSONObject();
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("version", "2");
            jSONObject2.put("api", jSONObject3);
            jSONObject.put("ext", jSONObject2);
            a(iVar, this.d.b(this, jSONObject));
        } catch (JSONException e2) {
            throw new IllegalStateException();
        }
    }

    private void b(JSONObject jSONObject) {
    }

    private void c(JSONObject jSONObject) {
        try {
            this.f100a = jSONObject.getString("clientId");
            this.b = true;
            this.d.a(this, jSONObject);
        } catch (JSONException e2) {
            throw new f(e2);
        }
    }

    /* access modifiers changed from: package-private */
    public i a() {
        return new i(this, this.c);
    }

    /* access modifiers changed from: package-private */
    public void a(i iVar) {
        a(iVar, Integer.valueOf(iVar.f103a), iVar.c, iVar.b, iVar.d);
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.c.a(str);
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.f100a;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.c.b(str);
    }
}
