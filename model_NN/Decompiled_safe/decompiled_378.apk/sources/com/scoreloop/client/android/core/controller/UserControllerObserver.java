package com.scoreloop.client.android.core.controller;

public interface UserControllerObserver extends RequestControllerObserver {
    void onEmailAlreadyTaken(UserController userController);

    void onEmailInvalidFormat(UserController userController);

    void onUsernameAlreadyTaken(UserController userController);
}
