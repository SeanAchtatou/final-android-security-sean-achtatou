package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.ui.MySpaceAuthViewController;
import com.scoreloop.client.android.core.utils.Logger;
import org.json.JSONException;

public class MySpaceSocialProviderController extends SocialProviderController implements UserControllerObserver {
    /* access modifiers changed from: private */
    public UserController c;

    public MySpaceSocialProviderController(Session session, SocialProviderControllerObserver socialProviderControllerObserver) {
        super(session, socialProviderControllerObserver);
    }

    public void b() {
        a(c(), new MySpaceAuthViewController(new P(this)));
    }

    public void onEmailAlreadyTaken(UserController userController) {
        throw new IllegalStateException();
    }

    public void onEmailInvalidFormat(UserController userController) {
        throw new IllegalStateException();
    }

    public void onUsernameAlreadyTaken(UserController userController) {
        throw new IllegalStateException();
    }

    public void requestControllerDidFail(RequestController requestController, Exception exc) {
        d().didFail(exc);
    }

    public void requestControllerDidReceiveResponse(RequestController requestController) {
        d().didSucceed();
        try {
            Logger.a("user object: ", "USER: " + Session.getCurrentSession().getUser().d().toString(4));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
