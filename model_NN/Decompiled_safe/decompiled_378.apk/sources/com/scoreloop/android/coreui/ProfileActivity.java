package com.scoreloop.android.coreui;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.moose.game.bubble.fruit.R;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.controller.UserControllerObserver;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;

public class ProfileActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public EditText emailView;
    /* access modifiers changed from: private */
    public EditText loginView;
    private String oldEmail;
    private String oldLogin;
    private boolean shouldShowDialogs;
    private TextView titleLoginView;
    /* access modifiers changed from: private */
    public UserController userController;

    private class UserUpdateObserver implements UserControllerObserver {
        private UserUpdateObserver() {
        }

        /* synthetic */ UserUpdateObserver(ProfileActivity profileActivity, UserUpdateObserver userUpdateObserver) {
            this();
        }

        public void onEmailAlreadyTaken(UserController controller) {
            ProfileActivity.this.onErrorUpdateUIAndResetData(8, true);
        }

        public void onEmailInvalidFormat(UserController controller) {
            ProfileActivity.this.onErrorUpdateUIAndResetData(10, true);
        }

        public void onUsernameAlreadyTaken(UserController controller) {
            ProfileActivity.this.onErrorUpdateUIAndResetData(11, true);
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            ProfileActivity.this.onErrorUpdateUIAndResetData(3, false);
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            ProfileActivity.this.setProgressIndicator(false);
            ProfileActivity.this.blockUI(false);
            ProfileActivity.this.setUIToSessionUser();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sl_profile);
        this.userController = new UserController(new UserUpdateObserver(this, null));
        this.loginView = (EditText) findViewById(R.id.login);
        this.emailView = (EditText) findViewById(R.id.email);
        this.titleLoginView = (TextView) findViewById(R.id.title_login);
        if (Session.getCurrentSession().isAuthenticated()) {
            this.titleLoginView.setText(Session.getCurrentSession().getUser().getLogin());
        }
        ((Button) findViewById(R.id.update_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ProfileActivity.this.setOldDataToSessionUser();
                String login = ProfileActivity.this.loginView.getText().toString().trim();
                String email = ProfileActivity.this.emailView.getText().toString().trim();
                ProfileActivity.this.loginView.setText(login);
                ProfileActivity.this.emailView.setText(email);
                User user = Session.getCurrentSession().getUser();
                user.setLogin(login);
                user.setEmailAddress(email);
                ProfileActivity.this.blockUI(true);
                ProfileActivity.this.setProgressIndicator(true);
                ProfileActivity.this.userController.updateUser();
            }
        });
        setOldDataToSessionUser();
        blockUI(true);
        setProgressIndicator(true);
        this.userController.updateUser();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, (int) R.string.sl_highscores).setIcon((int) R.drawable.sl_menu_highscores);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, HighscoresActivity.class));
        finish();
        return true;
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    /* access modifiers changed from: private */
    public void blockUI(boolean flg) {
        boolean z;
        boolean z2;
        boolean z3;
        Button button = (Button) findViewById(R.id.update_button);
        if (flg) {
            z = false;
        } else {
            z = true;
        }
        button.setEnabled(z);
        EditText editText = (EditText) findViewById(R.id.login);
        if (flg) {
            z2 = false;
        } else {
            z2 = true;
        }
        editText.setEnabled(z2);
        EditText editText2 = (EditText) findViewById(R.id.email);
        if (flg) {
            z3 = false;
        } else {
            z3 = true;
        }
        editText2.setEnabled(z3);
    }

    /* access modifiers changed from: private */
    public void onErrorUpdateUIAndResetData(int error, boolean enableUI) {
        setProgressIndicator(false);
        if (enableUI) {
            blockUI(false);
        }
        if (this.shouldShowDialogs) {
            showDialog(error);
        }
        setSessionUserToOldData();
        setUIToSessionUser();
    }

    /* access modifiers changed from: private */
    public void setOldDataToSessionUser() {
        User user = Session.getCurrentSession().getUser();
        this.oldLogin = user.getLogin();
        this.oldEmail = user.getEmailAddress();
    }

    private void setSessionUserToOldData() {
        User user = Session.getCurrentSession().getUser();
        user.setLogin(this.oldLogin);
        user.setEmailAddress(this.oldEmail);
    }

    /* access modifiers changed from: private */
    public void setUIToSessionUser() {
        User user = Session.getCurrentSession().getUser();
        this.loginView.setText(user.getLogin());
        this.emailView.setText(user.getEmailAddress());
        this.titleLoginView.setText(user.getLogin());
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.shouldShowDialogs = false;
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.shouldShowDialogs = true;
    }
}
