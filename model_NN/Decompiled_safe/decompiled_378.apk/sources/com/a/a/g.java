package com.a.a;

import a.c;
import android.content.Context;
import android.content.SharedPreferences;
import com.scoreloop.client.android.core.utils.Logger;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private static g f12a;
    private List b = new ArrayList();
    private String c;
    private String d;
    private String e;
    private Long f;
    private String g;
    private String h;
    private Date i;
    private List j;
    private Date k;
    private int l;
    /* access modifiers changed from: private */
    public Timer m;
    private TimerTask n = new c(this);

    private g(String str, String str2, String str3) {
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = 0L;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = new ArrayList();
        this.k = new Date();
        this.l = 0;
        this.m = null;
    }

    public static g a() {
        return f12a;
    }

    public static g a(String str, String str2, k kVar) {
        g a2 = a(str, str2, (String) null);
        a2.d().add(kVar);
        return a2;
    }

    private static g a(String str, String str2, String str3) {
        g gVar = new g(str, str2, str3);
        if (f12a == null) {
            f12a = gVar;
        }
        return gVar;
    }

    private boolean a(a aVar, boolean z) {
        boolean z2 = this.k != null ? new Date().getTime() - this.k.getTime() < 2 : false;
        if (this.k != null && z2) {
            int i2 = this.l + 1;
            this.l = i2;
            if (i2 > 3) {
                if (z) {
                    b(aVar);
                }
                return false;
            }
        }
        try {
            aVar.b();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        if (!z2) {
            this.l = 0;
            this.k = aVar.a();
        }
        return true;
    }

    private void b(a aVar) {
        this.j.add(aVar);
        k();
    }

    private void k() {
        if (this.m == null) {
            this.m = new Timer();
            this.m.schedule(this.n, (c.a(this.k) + 2) * 1000);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.g.a(com.a.a.a, boolean):boolean
     arg types: [com.a.a.a, int]
     candidates:
      com.a.a.g.a(com.a.a.g, java.util.Timer):java.util.Timer
      com.a.a.g.a(com.a.a.a, boolean):boolean */
    /* access modifiers changed from: private */
    public void l() {
        while (this.j.size() > 0) {
            if (a((a) this.j.get(0), false)) {
                this.j.remove(0);
            } else {
                k();
                return;
            }
        }
    }

    public void a(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("FBSessionPreferences", 0).edit();
        if (this.f != null) {
            edit.putLong("FBUserId", this.f.longValue());
        } else {
            edit.remove("FBUserId");
        }
        if (this.g != null) {
            edit.putString("FBSessionKey", this.g);
        } else {
            edit.remove("FBSessionKey");
        }
        if (this.h != null) {
            edit.putString("FBSessionSecret", this.h);
        } else {
            edit.remove("FBSessionSecret");
        }
        if (this.i != null) {
            edit.putLong("FBSessionExpires", this.i.getTime());
        } else {
            edit.remove("FBSessionExpires");
        }
        edit.commit();
    }

    public void a(Context context, Long l2, String str, String str2, Date date) {
        this.f = l2;
        this.g = str;
        this.h = str2;
        this.i = (Date) date.clone();
        a(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.g.a(com.a.a.a, boolean):boolean
     arg types: [com.a.a.a, int]
     candidates:
      com.a.a.g.a(com.a.a.g, java.util.Timer):java.util.Timer
      com.a.a.g.a(com.a.a.a, boolean):boolean */
    public void a(a aVar) {
        a(aVar, true);
    }

    public String b() {
        return "http://api.facebook.com/restserver.php";
    }

    public boolean b(Context context) {
        boolean z;
        SharedPreferences sharedPreferences = context.getSharedPreferences("FBSessionPreferences", 0);
        Long valueOf = Long.valueOf(sharedPreferences.getLong("FBUserId", 0));
        Logger.a("FBSession", "FBUserId = " + valueOf);
        if (valueOf.longValue() != 0) {
            long j2 = sharedPreferences.getLong("FBSessionExpires", 0);
            if (j2 > 0) {
                Date date = new Date(j2);
                Logger.a("FBSession", new StringBuilder().append("expirationDate = ").append(date).toString() != null ? date.toLocaleString() : "null");
                long a2 = c.a(date);
                Logger.a("FBSession", "Time interval since now = " + a2);
                z = date == null || a2 <= 0;
            } else {
                Logger.a("FBSession", "FBSessionExpires does not exist.  Loading session...");
                z = true;
            }
            if (z) {
                Logger.a("FBSession", "Session can be loaded.  Loading...");
                this.f = valueOf;
                this.g = sharedPreferences.getString("FBSessionKey", null);
                this.h = sharedPreferences.getString("FBSessionSecret", null);
                for (k kVar : this.b) {
                    if (kVar != null) {
                        kVar.a(this, valueOf);
                    }
                }
                return true;
            }
        }
        return false;
    }

    public boolean c() {
        return this.g != null;
    }

    public List d() {
        return this.b;
    }

    public String e() {
        return this.c;
    }

    public String f() {
        return this.d;
    }

    public String g() {
        return this.e;
    }

    public Long h() {
        return this.f;
    }

    public String i() {
        return this.g;
    }

    public String j() {
        return this.h;
    }
}
