package com.ningo.game.ninja;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AsyncPlayer;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import com.google.ads.AdView;
import com.google.ads.R;
import com.ningo.game.ninja.a.m;

public class AgileBuddyActivity extends Activity {
    /* access modifiers changed from: private */
    public AgileBuddyView a;
    private AdView b;
    private SharedPreferences c;
    private MediaPlayer d = null;
    private AsyncPlayer e = null;
    private SensorManager f;
    private Sensor g;
    private SensorEventListener h;
    /* access modifiers changed from: private */
    public boolean i;
    private GestureDetector j = null;

    public final void a(boolean z) {
        if (z) {
            this.b.setVisibility(0);
        } else {
            this.b.setVisibility(8);
        }
    }

    public void finish() {
        if (this.e != null) {
            this.e.stop();
            this.e = null;
        }
        this.f.unregisterListener(this.h, this.g);
        super.finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.media.AsyncPlayer.play(android.content.Context, android.net.Uri, boolean, int):void}
     arg types: [android.content.Context, android.net.Uri, int, int]
     candidates:
      ClspMth{android.media.AsyncPlayer.play(android.content.Context, android.net.Uri, boolean, android.media.AudioAttributes):void throws java.lang.IllegalArgumentException}
      ClspMth{android.media.AsyncPlayer.play(android.content.Context, android.net.Uri, boolean, int):void} */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        this.a = (AgileBuddyView) findViewById(R.id.agile_buddy);
        this.b = (AdView) findViewById(R.id.ad);
        a(false);
        this.a.a(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.i = extras.getBoolean("SENSOR_MODE", false);
        }
        this.i = true;
        this.f = (SensorManager) getSystemService("sensor");
        this.g = this.f.getDefaultSensor(1);
        this.h = new q(this);
        this.f.registerListener(this.h, this.g, 1);
        this.j = new GestureDetector(this, new m(this));
        this.c = getBaseContext().getSharedPreferences("com.gastudio.game.ninja", 0);
        boolean z = this.c.getBoolean("sounds", true);
        boolean z2 = this.c.getBoolean("vibrate", true);
        m.a(z);
        m.b(z2);
        if (z) {
            if (this.e == null) {
                this.e = new AsyncPlayer("aPlayer");
                if (this.e == null) {
                    return;
                }
            }
            this.e.play(getBaseContext(), Uri.parse("android.resource://" + getPackageName() + "/" + ((int) R.raw.m_game)), true, 3);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.a.b();
        a(true);
        return false;
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        this.a.b();
        a(true);
        AlertDialog create = new AlertDialog.Builder(this).setMessage("Do you want to exit?").setTitle("Exit").setIcon((int) R.drawable.tip_awesome).setPositiveButton("Yes", new r(this)).setNeutralButton("Cancel", new s(this)).create();
        create.setOnDismissListener(new t(this));
        create.show();
        return false;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        return this.j.onTouchEvent(motionEvent);
    }
}
