package com.ningo.game.ninja.a;

import android.content.Context;
import android.media.SoundPool;
import android.os.Vibrator;
import android.util.Log;
import com.google.ads.R;
import java.util.HashMap;
import java.util.Random;

public abstract class m {
    private static SoundPool a;
    private static HashMap b;
    private static Vibrator c;
    private static Random d;
    private static boolean e;
    private static boolean f;

    public static void a(int i) {
        int i2;
        if (i != 0) {
            if (e) {
                int i3 = (i == 5 && d.nextInt(3) == 1) ? 9 : i;
                try {
                    a.play(((Integer) b.get(Integer.valueOf(i3))).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
                    i2 = i3;
                } catch (Exception e2) {
                    Log.d("PlaySounds", e2.toString());
                    i2 = i3;
                }
            } else {
                i2 = i;
            }
            if (i2 != 19 && f && c != null) {
                c.vibrate(25);
            }
        }
    }

    public static void a(Context context) {
        d = new Random();
        a = new SoundPool(30, 3, 100);
        HashMap hashMap = new HashMap();
        b = hashMap;
        hashMap.put(1, Integer.valueOf(a.load(context, R.raw.normal, 1)));
        b.put(2, Integer.valueOf(a.load(context, R.raw.unstable, 1)));
        b.put(3, Integer.valueOf(a.load(context, R.raw.spring, 1)));
        b.put(4, Integer.valueOf(a.load(context, R.raw.spiked, 1)));
        b.put(5, Integer.valueOf(a.load(context, R.raw.moving, 1)));
        b.put(6, Integer.valueOf(a.load(context, R.raw.tools, 1)));
        b.put(7, Integer.valueOf(a.load(context, R.raw.gameover, 1)));
        b.put(8, Integer.valueOf(a.load(context, R.raw.nextlevel, 1)));
        b.put(9, Integer.valueOf(a.load(context, R.raw.moving2, 1)));
        b.put(10, Integer.valueOf(a.load(context, R.raw.power, 1)));
        b.put(19, Integer.valueOf(a.load(context, R.raw.click, 1)));
        b.put(12, Integer.valueOf(a.load(context, R.raw.power_on, 1)));
        b.put(13, Integer.valueOf(a.load(context, R.raw.power_off, 1)));
        b.put(14, Integer.valueOf(a.load(context, R.raw.drop, 1)));
        b.put(15, Integer.valueOf(a.load(context, R.raw.meterwarn, 1)));
        b.put(16, Integer.valueOf(a.load(context, R.raw.drop_disappear, 1)));
        b.put(17, Integer.valueOf(a.load(context, R.raw.roof, 1)));
        b.put(18, Integer.valueOf(a.load(context, R.raw.scene_change, 1)));
        b.put(11, Integer.valueOf(a.load(context, R.raw.pause, 1)));
        if (c == null) {
            c = (Vibrator) context.getSystemService("vibrator");
        }
        e = true;
    }

    public static void a(boolean z) {
        e = z;
    }

    public static void b(boolean z) {
        f = z;
    }
}
