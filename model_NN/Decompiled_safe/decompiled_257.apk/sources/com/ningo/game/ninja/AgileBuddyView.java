package com.ningo.game.ninja;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.google.ads.R;
import com.ningo.game.ninja.a.n;

public class AgileBuddyView extends SurfaceView implements SurfaceHolder.Callback {
    /* access modifiers changed from: private */
    public Bitmap A;
    /* access modifiers changed from: private */
    public Bitmap B;
    /* access modifiers changed from: private */
    public Bitmap C;
    /* access modifiers changed from: private */
    public Bitmap D;
    /* access modifiers changed from: private */
    public Bitmap E;
    /* access modifiers changed from: private */
    public Bitmap F;
    /* access modifiers changed from: private */
    public Bitmap G;
    /* access modifiers changed from: private */
    public Bitmap H;
    /* access modifiers changed from: private */
    public Bitmap I;
    /* access modifiers changed from: private */
    public Bitmap J;
    /* access modifiers changed from: private */
    public Bitmap K;
    /* access modifiers changed from: private */
    public Bitmap L;
    /* access modifiers changed from: private */
    public Bitmap M;
    /* access modifiers changed from: private */
    public Bitmap N;
    /* access modifiers changed from: private */
    public Bitmap O;
    /* access modifiers changed from: private */
    public Bitmap P;
    /* access modifiers changed from: private */
    public Bitmap Q;
    /* access modifiers changed from: private */
    public Bitmap R;
    /* access modifiers changed from: private */
    public Bitmap S;
    /* access modifiers changed from: private */
    public Bitmap T;
    /* access modifiers changed from: private */
    public Bitmap U;
    /* access modifiers changed from: private */
    public Bitmap V;
    /* access modifiers changed from: private */
    public Bitmap W;
    /* access modifiers changed from: private */
    public Bitmap X;
    /* access modifiers changed from: private */
    public Bitmap Y;
    /* access modifiers changed from: private */
    public Bitmap Z;
    private p a;
    /* access modifiers changed from: private */
    public Bitmap aa;
    /* access modifiers changed from: private */
    public Bitmap ab;
    /* access modifiers changed from: private */
    public Bitmap ac;
    /* access modifiers changed from: private */
    public Bitmap ad;
    /* access modifiers changed from: private */
    public Bitmap ae;
    /* access modifiers changed from: private */
    public Bitmap af;
    /* access modifiers changed from: private */
    public Bitmap ag;
    /* access modifiers changed from: private */
    public Bitmap ah;
    /* access modifiers changed from: private */
    public Bitmap ai;
    /* access modifiers changed from: private */
    public Bitmap aj;
    /* access modifiers changed from: private */
    public Drawable ak;
    /* access modifiers changed from: private */
    public Drawable al;
    /* access modifiers changed from: private */
    public Drawable am;
    /* access modifiers changed from: private */
    public Drawable an;
    /* access modifiers changed from: private */
    public Drawable ao;
    /* access modifiers changed from: private */
    public Paint ap;
    /* access modifiers changed from: private */
    public Paint aq;
    /* access modifiers changed from: private */
    public Paint ar;
    private Typeface as;
    /* access modifiers changed from: private */
    public AgileBuddyActivity at;
    /* access modifiers changed from: private */
    public boolean au = false;
    private Context b;
    private Handler c;
    /* access modifiers changed from: private */
    public n d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public Bitmap g;
    /* access modifiers changed from: private */
    public Bitmap h;
    /* access modifiers changed from: private */
    public Bitmap i;
    /* access modifiers changed from: private */
    public Bitmap j;
    /* access modifiers changed from: private */
    public Bitmap k;
    /* access modifiers changed from: private */
    public Bitmap l;
    /* access modifiers changed from: private */
    public Bitmap m;
    /* access modifiers changed from: private */
    public Bitmap n;
    /* access modifiers changed from: private */
    public Bitmap o;
    /* access modifiers changed from: private */
    public Bitmap p;
    /* access modifiers changed from: private */
    public Bitmap q;
    /* access modifiers changed from: private */
    public Bitmap r;
    /* access modifiers changed from: private */
    public Bitmap s;
    /* access modifiers changed from: private */
    public Bitmap t;
    /* access modifiers changed from: private */
    public Bitmap u;
    /* access modifiers changed from: private */
    public Bitmap v;
    /* access modifiers changed from: private */
    public Bitmap w;
    /* access modifiers changed from: private */
    public Bitmap x;
    /* access modifiers changed from: private */
    public Bitmap y;
    /* access modifiers changed from: private */
    public Bitmap z;

    public AgileBuddyView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        this.c = new o(this);
        System.gc();
        this.e = this.b.getSharedPreferences("com.gastudio.game.ninja", 0).getInt("power", 45);
        this.as = Typeface.createFromAsset(getContext().getAssets(), "fonts/b.TTF");
        this.aq = new Paint(1);
        this.aq.setColor(-256);
        this.aq.setStyle(Paint.Style.FILL);
        this.aq.setTextSize(15.0f);
        this.aq.setTextAlign(Paint.Align.LEFT);
        this.aq.setTypeface(this.as);
        this.ap = new Paint(1);
        this.ap.setColor(-256);
        this.ap.setStyle(Paint.Style.FILL);
        this.ap.setTextSize(20.0f);
        this.ap.setTextAlign(Paint.Align.RIGHT);
        this.ap.setTypeface(this.as);
        this.ar = new Paint(1);
        this.ap.setTypeface(this.as);
        Resources resources = this.b.getResources();
        this.ak = resources.getDrawable(R.drawable.top_bar2);
        this.al = resources.getDrawable(R.drawable.hp_bar_total);
        this.am = resources.getDrawable(R.drawable.hp_bar_remain);
        this.an = resources.getDrawable(R.drawable.tp_bar_total);
        this.ao = resources.getDrawable(R.drawable.tp_bar_remain);
        this.k = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.role_deadman), 32, 48, true);
        this.j = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.role_standing), 32, 48, true);
        this.l = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.role_freefall1), 32, 48, true);
        this.m = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.role_freefall2), 32, 48, true);
        this.n = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.role_freefall3), 32, 48, true);
        this.o = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.role_freefall4), 32, 48, true);
        this.p = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.role_moving_left1), 32, 48, true);
        this.q = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.role_moving_left2), 32, 48, true);
        this.r = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.role_moving_left3), 32, 48, true);
        this.s = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.role_moving_left4), 32, 48, true);
        this.t = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.role_moving_right1), 32, 48, true);
        this.u = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.role_moving_right2), 32, 48, true);
        this.v = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.role_moving_right3), 32, 48, true);
        this.w = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.role_moving_right4), 32, 48, true);
        this.x = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.footboard_normal), 100, 20, true);
        this.y = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.footboard_unstable1), 100, 20, true);
        this.z = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.footboard_unstable2), 100, 20, true);
        this.A = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.footboard_spring), 100, 20, true);
        this.B = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.footboard_spring2), 100, 20, true);
        this.C = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.footboard_spiked), 100, 20, true);
        this.D = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.footboard_moving_left1), 100, 20, true);
        this.E = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.footboard_moving_left2), 100, 20, true);
        this.F = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.footboard_moving_right1), 100, 20, true);
        this.G = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.footboard_moving_right2), 100, 20, true);
        this.ab = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.footboard_plane_left), 100, 20, true);
        this.ac = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.footboard_plane_right), 100, 20, true);
        this.H = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.ele1), 25, 60, true);
        this.I = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.ele2), 25, 60, true);
        this.J = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.ele3), 25, 60, true);
        this.K = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.ele4), 25, 60, true);
        this.L = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.ele5), 25, 60, true);
        this.M = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.ele6), 25, 60, true);
        this.N = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.food_1), 24, 24, true);
        this.O = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.food_2), 24, 24, true);
        this.P = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.food_3), 24, 24, true);
        this.Q = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.food_4), 24, 24, true);
        this.R = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.food_5), 24, 24, true);
        this.S = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.food_6), 24, 24, true);
        this.U = BitmapFactory.decodeResource(resources, R.drawable.drop1);
        this.V = BitmapFactory.decodeResource(resources, R.drawable.drop2);
        this.W = BitmapFactory.decodeResource(resources, R.drawable.drop3);
        this.X = BitmapFactory.decodeResource(resources, R.drawable.drop4);
        this.Y = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.shine), 468, 84, true);
        this.aa = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.power_foot), 468, 84, true);
        this.Z = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.saiyan), 468, 84, true);
        this.af = BitmapFactory.decodeResource(resources, R.drawable.power_line1);
        this.ag = BitmapFactory.decodeResource(resources, R.drawable.power_line2);
        this.ah = BitmapFactory.decodeResource(resources, R.drawable.power_line3);
        this.ai = BitmapFactory.decodeResource(resources, R.drawable.scene_line);
        this.aj = BitmapFactory.decodeResource(resources, R.drawable.scene_change);
        this.g = BitmapFactory.decodeResource(resources, R.drawable.bg_game1);
        this.h = BitmapFactory.decodeResource(resources, R.drawable.bg_game2);
        this.i = BitmapFactory.decodeResource(resources, R.drawable.bg_game3);
        this.ad = BitmapFactory.decodeResource(resources, R.drawable.hurt1);
        this.ae = BitmapFactory.decodeResource(resources, R.drawable.hurt2);
        this.T = BitmapFactory.decodeResource(resources, R.drawable.pause);
        this.a = new p(this, holder, context, this.c);
        setFocusable(true);
    }

    static /* synthetic */ void aq(AgileBuddyView agileBuddyView) {
        int i2 = agileBuddyView.f;
        new String("");
        SharedPreferences sharedPreferences = agileBuddyView.b.getSharedPreferences("com.gastudio.game.ninja", 0);
        if (sharedPreferences.getInt("highScore", 0) < i2) {
            sharedPreferences.edit().putInt("highScore", i2).commit();
        }
        Intent intent = new Intent();
        intent.setClass(agileBuddyView.at, GameoverActivity.class);
        intent.putExtra("curScore", agileBuddyView.f);
        new String("");
        intent.putExtra("highScore", agileBuddyView.b.getSharedPreferences("com.gastudio.game.ninja", 0).getInt("highScore", 0));
        agileBuddyView.at.startActivity(intent);
    }

    public final void a(Activity activity) {
        this.at = (AgileBuddyActivity) activity;
    }

    public final void a(boolean z2, float f2) {
        if (this.a != null) {
            this.a.a(z2, f2);
        }
    }

    public final boolean a() {
        return this.au;
    }

    public final void b() {
        this.au = true;
    }

    public final void c() {
        this.au = false;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        this.d = new n(i3, i4);
        this.a.a(this.d);
        this.a.a(true);
        this.a.start();
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        boolean z2 = true;
        this.a.a(false);
        while (z2) {
            try {
                this.a.join();
                z2 = false;
            } catch (InterruptedException e2) {
                Log.d("", "Surface destroy failure:", e2);
            }
        }
        if (!this.g.isRecycled()) {
            this.g.recycle();
        }
        if (!this.h.isRecycled()) {
            this.h.recycle();
        }
        if (!this.i.isRecycled()) {
            this.i.recycle();
        }
        if (!this.j.isRecycled()) {
            this.j.recycle();
        }
        if (!this.k.isRecycled()) {
            this.k.recycle();
        }
        if (!this.l.isRecycled()) {
            this.l.recycle();
        }
        if (!this.m.isRecycled()) {
            this.m.recycle();
        }
        if (!this.n.isRecycled()) {
            this.n.recycle();
        }
        if (!this.o.isRecycled()) {
            this.o.recycle();
        }
        if (!this.x.isRecycled()) {
            this.x.recycle();
        }
        if (!this.y.isRecycled()) {
            this.y.recycle();
        }
        if (!this.z.isRecycled()) {
            this.z.recycle();
        }
        if (!this.A.isRecycled()) {
            this.A.recycle();
        }
        if (!this.B.isRecycled()) {
            this.B.recycle();
        }
        if (!this.C.isRecycled()) {
            this.C.recycle();
        }
        if (!this.C.isRecycled()) {
            this.C.recycle();
        }
        if (!this.E.isRecycled()) {
            this.E.recycle();
        }
        if (!this.F.isRecycled()) {
            this.F.recycle();
        }
        if (!this.G.isRecycled()) {
            this.G.recycle();
        }
        if (!this.C.isRecycled()) {
            this.C.recycle();
        }
        if (!this.H.isRecycled()) {
            this.H.recycle();
        }
        if (!this.I.isRecycled()) {
            this.I.recycle();
        }
        if (!this.J.isRecycled()) {
            this.J.recycle();
        }
        if (!this.K.isRecycled()) {
            this.K.recycle();
        }
        if (!this.L.isRecycled()) {
            this.L.recycle();
        }
        if (!this.M.isRecycled()) {
            this.M.recycle();
        }
        if (!this.N.isRecycled()) {
            this.N.recycle();
        }
        if (!this.O.isRecycled()) {
            this.O.recycle();
        }
        if (!this.P.isRecycled()) {
            this.P.recycle();
        }
        if (!this.Q.isRecycled()) {
            this.Q.recycle();
        }
        if (!this.R.isRecycled()) {
            this.R.recycle();
        }
        if (!this.U.isRecycled()) {
            this.U.recycle();
        }
        if (!this.V.isRecycled()) {
            this.V.recycle();
        }
        if (!this.W.isRecycled()) {
            this.W.recycle();
        }
        if (!this.X.isRecycled()) {
            this.X.recycle();
        }
        if (!this.af.isRecycled()) {
            this.af.recycle();
        }
        if (!this.ag.isRecycled()) {
            this.ag.recycle();
        }
        if (!this.ah.isRecycled()) {
            this.ah.recycle();
        }
        if (!this.ad.isRecycled()) {
            this.ad.recycle();
        }
        if (!this.ae.isRecycled()) {
            this.ae.recycle();
        }
        if (!this.T.isRecycled()) {
            this.T.recycle();
        }
        if (!this.aj.isRecycled()) {
            this.aj.recycle();
        }
        if (!this.ai.isRecycled()) {
            this.ai.recycle();
        }
        if (!this.Y.isRecycled()) {
            this.Y.recycle();
        }
        if (!this.Z.isRecycled()) {
            this.Z.recycle();
        }
        if (!this.aa.isRecycled()) {
            this.aa.recycle();
        }
        this.ak = null;
        this.al = null;
        this.am = null;
        this.an = null;
        this.ao = null;
    }
}
