package com.ningo.game.ninja.a;

public final class b {
    private int a;
    private int b;
    private int c = 100;
    private int d = 20;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;
    private int l;
    private int m;

    public b(int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.a = i2;
        this.b = i3 * 10;
        this.e = i4;
        this.f = i5;
        this.g = i6;
        this.i = 10;
        this.j = 5;
        this.h = 0;
        this.k = i7;
        this.l = i8;
        this.m = 1;
    }

    public final int a() {
        return this.b;
    }

    public final void a(int i2) {
        this.b += i2;
    }

    public final int b() {
        return this.a;
    }

    public final void b(int i2) {
        this.a += i2;
    }

    public final int c() {
        return this.a + this.c;
    }

    public final int d() {
        return this.b / 10;
    }

    public final int e() {
        return (this.b / 10) + this.d;
    }

    public final int f() {
        return this.e;
    }

    public final void g() {
        this.e = 0;
    }

    public final boolean h() {
        int i2 = this.i - 1;
        this.i = i2;
        return i2 == 0;
    }

    public final boolean i() {
        return this.i != 10;
    }

    public final void j() {
        this.j--;
    }

    public final boolean k() {
        if (this.j == 5) {
            return false;
        }
        if (this.j <= 0) {
            this.j = 5;
            return false;
        }
        this.j--;
        return this.j < 5;
    }

    public final int l() {
        try {
            return this.h / this.g;
        } finally {
            this.h++;
            if (this.h == this.f * this.g) {
                this.h = 0;
            }
        }
    }

    public final int m() {
        return this.k;
    }

    public final int n() {
        return this.l;
    }
}
