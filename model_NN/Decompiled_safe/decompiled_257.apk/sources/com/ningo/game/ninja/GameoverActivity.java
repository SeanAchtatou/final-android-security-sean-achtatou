package com.ningo.game.ninja;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.ads.R;
import com.ningo.game.ninja.a.m;
import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.b.n;
import com.scoreloop.client.android.core.d.p;

public class GameoverActivity extends Activity implements View.OnClickListener {
    private ProgressDialog a = null;
    private int b;
    private int c;

    static /* synthetic */ void a(GameoverActivity gameoverActivity) {
        if (gameoverActivity.a != null && gameoverActivity.a.isShowing()) {
            gameoverActivity.a.dismiss();
            gameoverActivity.a = null;
        }
    }

    public final void a() {
        if (this.a != null && this.a.isShowing()) {
            this.a.dismiss();
        }
        this.a = ProgressDialog.show(this, "", "Submit Score to Server...", true);
        this.a.setCancelable(true);
        int i = this.c;
        n nVar = new n(this);
        n nVar2 = new n(Double.valueOf((double) i), k.a().f());
        nVar2.a((Integer) 0);
        new p(nVar).a(nVar2);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                a();
                break;
            case R.id.btn_tryagain:
                startActivity(new Intent(this, TipsActivity.class));
            case R.id.btn_quit:
                finish();
                break;
        }
        m.a(19);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.gameover);
        ((Button) findViewById(R.id.btn_submit)).setOnClickListener(this);
        ((Button) findViewById(R.id.btn_tryagain)).setOnClickListener(this);
        ((Button) findViewById(R.id.btn_quit)).setOnClickListener(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.b = extras.getInt("curScore", 0);
            this.c = extras.getInt("highScore", 0);
        }
        ((TextView) findViewById(R.id.tv_cur_score)).setText(String.valueOf(Integer.toString(this.b)) + "m");
        ((TextView) findViewById(R.id.tv_high_score)).setText(String.valueOf(Integer.toString(this.c)) + "m");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
