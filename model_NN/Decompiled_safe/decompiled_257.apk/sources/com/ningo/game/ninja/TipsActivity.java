package com.ningo.game.ninja;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import com.google.ads.R;
import com.ningo.game.ninja.a.m;

public class TipsActivity extends Activity implements View.OnClickListener {
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.sensor_to_start) {
            m.a(19);
            boolean z = id == R.id.sensor_to_start;
            Intent intent = new Intent(this, AgileBuddyActivity.class);
            intent.putExtra("SENSOR_MODE", z);
            startActivity(intent);
            finish();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.tips);
        if (((WindowManager) getSystemService("window")).getDefaultDisplay().getHeight() > 480) {
            ((RelativeLayout) findViewById(R.id.bg_tips)).setBackgroundResource(R.drawable.bg_tips_569);
        }
        findViewById(R.id.sensor_to_start).setOnClickListener(this);
    }
}
