package com.ningo.game.ninja.a;

public final class f {
    private int a = 19;
    private int b;
    private int c = 0;
    private int d = 250;
    private int e = 0;

    public final int a() {
        return this.a;
    }

    public final void a(int i) {
        if (this.a <= i) {
            this.a = 19;
            this.b = i;
            this.c = 0;
            this.d = 5;
            this.e = 1;
        }
    }

    public final void b() {
        this.c = 0;
    }

    public final int c() {
        return this.c;
    }

    public final int d() {
        return this.e;
    }

    public final void e() {
        this.e = 5;
        this.c = 0;
        this.d = 3;
    }

    public final void f() {
        this.e = 0;
        this.c = 0;
        this.d = 250;
    }

    public final void g() {
        this.c++;
        if (this.e != 0 && this.c >= this.d) {
            this.c = 0;
            if (this.e == 1) {
                this.e = 2;
                this.d = 3;
            } else if (this.e == 2) {
                this.a += 3;
                if (this.a >= this.b) {
                    this.a = this.b;
                    this.e = 3;
                    this.d = 150;
                }
            } else if (this.e == 3) {
                this.e = 4;
                this.d = 5;
                m.a(17);
            } else if (this.e == 4) {
                this.e = 5;
                this.d = 3;
            } else if (this.e == 5) {
                this.a--;
                if (this.a <= 19) {
                    this.a = 19;
                    this.e = 0;
                    this.d = 5;
                }
            }
        }
    }
}
