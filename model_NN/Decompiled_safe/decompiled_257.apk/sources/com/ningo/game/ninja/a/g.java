package com.ningo.game.ninja.a;

public final class g {
    public int a;
    public int b = 0;
    public int c = 0;
    private int d;
    private int e = 3;
    private int f = 0;
    private int g = 1;
    private float h = 1.0f;
    private int i = 13;
    private int j = 25;

    public g(int i2) {
        this.a = i2;
        this.d = i2;
    }

    public final void a() {
        if (this.f == 1) {
            this.c += (int) this.h;
            if (this.h <= 5.0f) {
                this.h *= 1.05f;
            }
        }
    }

    public final void a(int i2) {
        this.g = i2;
    }

    public final int b() {
        return this.b + this.i;
    }

    public final void b(int i2) {
        if (i2 == 1) {
            this.h = 1.0f;
        }
        this.f = i2;
    }

    public final int c() {
        return this.c + this.j;
    }

    public final int d() {
        return this.g;
    }

    public final int e() {
        return this.d;
    }

    public final int f() {
        return this.f;
    }
}
