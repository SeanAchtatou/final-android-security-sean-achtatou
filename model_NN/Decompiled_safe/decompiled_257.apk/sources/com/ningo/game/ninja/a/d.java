package com.ningo.game.ninja.a;

public final class d {
    private int a = 0;
    private int b = 0;
    private int c;
    private boolean d = false;
    private int e = 1;
    private int f = 5;
    private int g = 0;

    public d(int i) {
        this.c = i;
    }

    public final int a() {
        return this.a;
    }

    public final void a(int i) {
        this.d = true;
        this.a = i;
        this.b = i;
        this.e = 1;
    }

    public final int b() {
        return this.b;
    }

    public final boolean c() {
        return this.d;
    }

    public final void d() {
        if (this.d) {
            this.a -= 4;
            this.b += 4;
            if (this.a < 0 && this.b >= this.c) {
                this.d = false;
            }
        }
    }

    public final int e() {
        this.g++;
        if (this.g == this.f) {
            this.g = 0;
            switch (this.e) {
                case 1:
                    this.e = 2;
                    break;
                case 2:
                    this.e = 3;
                    break;
                case 3:
                    this.e = 1;
                    break;
            }
        }
        return this.e;
    }
}
