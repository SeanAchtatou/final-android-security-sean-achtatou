package com.ningo.game.ninja.a;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import java.util.Vector;

public final class l {
    private final int a = 1500;
    private final int b = 120;
    private final int c = 20;
    private final int d = 24;
    private final int e = Color.rgb(34, 138, 34);
    private final int f = Color.rgb(255, 255, 255);
    private int g;
    private int h;
    private Typeface i;
    private Typeface j;
    private Vector k = new Vector();

    public l(int i2, int i3) {
        this.g = i2;
        this.h = i3;
    }

    public final void a(int i2, int i3, int i4) {
        long currentTimeMillis = System.currentTimeMillis();
        int i5 = i2 > this.g - 10 ? i2 - 10 : i2;
        this.k.addElement(new c(this, i5 < 3 ? i5 + 5 : i5, i3 > this.h - 10 ? i3 - 10 : i3, i4, currentTimeMillis));
    }

    public final void a(Canvas canvas, Paint paint) {
        canvas.save();
        canvas.clipRect(0, 0, this.g, this.h);
        this.j = paint.getTypeface();
        int color = paint.getColor();
        float textSize = paint.getTextSize();
        int flags = paint.getFlags();
        if (this.i != null) {
            paint.setTypeface(this.i);
        }
        for (int size = this.k.size() - 1; size >= 0; size--) {
            c cVar = (c) this.k.get(size);
            String str = "+" + Integer.toString(cVar.c);
            if (System.currentTimeMillis() - cVar.d <= 1500) {
                System.currentTimeMillis();
                if (cVar.e < 24) {
                    cVar.e++;
                }
                float f2 = (float) cVar.e;
                cVar.b -= 7;
                int i2 = cVar.a;
                int i3 = cVar.b;
                paint.setColor(this.e);
                paint.setTextSize(1.0f + f2);
                paint.setFlags(1);
                paint.setFakeBoldText(false);
                canvas.drawText(str, (float) (i2 - 1), (float) (i3 - 1), paint);
                canvas.drawText(str, (float) i2, (float) (i3 - 1), paint);
                canvas.drawText(str, (float) (i2 + 1), (float) i3, paint);
                canvas.drawText(str, (float) i2, (float) (i3 + 1), paint);
                paint.setColor(this.f);
                paint.setTextSize(f2);
                canvas.drawText(str, (float) i2, (float) i3, paint);
            } else {
                this.k.remove(size);
            }
        }
        canvas.restore();
        paint.setColor(color);
        paint.setTextSize(textSize);
        paint.setFlags(flags);
        paint.setTypeface(this.j);
    }
}
