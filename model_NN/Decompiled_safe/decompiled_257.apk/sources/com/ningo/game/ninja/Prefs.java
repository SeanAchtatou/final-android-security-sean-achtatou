package com.ningo.game.ninja;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import com.google.ads.R;

public class Prefs extends Activity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    /* access modifiers changed from: private */
    public SharedPreferences a;
    private SharedPreferences b;

    public void onClick(View view) {
        view.getId();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.options);
        this.a = getBaseContext().getSharedPreferences("com.gastudio.game.ninja", 0);
        CheckBox checkBox = (CheckBox) findViewById(R.id.options_vibrate_checkbox);
        checkBox.setChecked(this.a.getBoolean("vibrate", true));
        checkBox.setOnCheckedChangeListener(new d(this));
        CheckBox checkBox2 = (CheckBox) findViewById(R.id.options_sounds_checkbox);
        checkBox2.setChecked(this.a.getBoolean("sounds", true));
        checkBox2.setOnCheckedChangeListener(new c(this));
        SeekBar seekBar = (SeekBar) findViewById(R.id.velocityController);
        seekBar.setProgress(this.a.getInt("power", 40));
        seekBar.setOnSeekBarChangeListener(this);
        ((Button) findViewById(R.id.btn_back)).setOnClickListener(new b(this));
        this.b = getSharedPreferences("com.gastudio.game.ninja", 0);
    }

    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
        this.a.edit().putInt("power", seekBar.getProgress()).commit();
    }
}
