package com.ningo.game.ninja.a;

import android.graphics.Point;

public final class h {
    private n a;
    private final int b = 4;
    private int c;
    private int d;
    private final int e = 8;
    private final int f = 3;
    private final int g = 5;
    private int h;
    private int i;
    private int j;
    private int k;
    private Point l;
    private int m;
    private int n;
    private int o;
    private int p;
    private Point q;
    private Point r;
    private boolean s = false;
    private int t = 0;
    private int u;
    private int v;
    private boolean w = false;
    private int x;
    private int y;
    private int z = 0;

    public h(n nVar, int i2, int i3, int i4) {
        this.a = nVar;
        this.d = 0;
        this.c = 0;
        this.l = new Point();
        this.k = 0;
        this.q = new Point(55, i2);
        this.r = new Point(i3, i4);
        this.h = 0;
        this.i = 0;
        this.j = 0;
        this.u = 0;
        this.s = false;
        this.x = 0;
        this.w = false;
    }

    public final void a() {
        this.d = 0;
        this.c = 1;
    }

    public final void a(int i2) {
        this.j = i2;
        this.i = 1;
    }

    public final void a(int i2, int i3, int i4) {
        int i5;
        int i6;
        this.k = i2;
        this.l.x = i3;
        this.l.y = i4;
        if (i2 == 4 || i2 == 5) {
            int i7 = this.r.x;
            i5 = i7;
            i6 = this.r.y;
        } else {
            int i8 = this.q.x;
            i5 = i8;
            i6 = this.q.y;
        }
        this.m = (i3 - i5) / 8;
        this.n = (i4 - i6) / 8;
        this.o = 0;
        this.p = 0;
    }

    public final void a(boolean z2) {
        this.s = z2;
        if (z2) {
            this.t = this.a.d;
            this.u = 1;
        }
    }

    public final int b() {
        if (this.c == 0) {
            return 0;
        }
        this.d++;
        if (this.d == 4) {
            this.d = 0;
            switch (this.c) {
                case 1:
                    this.c = 2;
                    break;
                case 2:
                    this.c = 0;
                    break;
                default:
                    this.c = 0;
                    break;
            }
        }
        return this.c;
    }

    public final boolean c() {
        return this.k != 0;
    }

    public final void d() {
        if (c()) {
            this.o++;
            if (this.o >= 3) {
                this.o = 0;
                int i2 = this.p;
                this.p = i2 + 1;
                if (i2 == 8) {
                    this.k = 0;
                    return;
                }
                this.l.y -= this.n;
                this.l.x -= this.m;
            }
        }
    }

    public final Point e() {
        return this.l;
    }

    public final int f() {
        return ((8 - this.p) * 15) + 80;
    }

    public final int g() {
        return this.k;
    }

    public final int h() {
        if (this.i == 0) {
            return 0;
        }
        int i2 = this.h;
        this.h = i2 + 1;
        if (i2 == 5) {
            this.h = 0;
            this.i = this.i == 1 ? 2 : this.i == 2 ? 3 : this.i == 3 ? 4 : this.i == 4 ? 5 : this.i == 5 ? 0 : 0;
        }
        return this.i;
    }

    public final int i() {
        return this.j;
    }

    public final boolean j() {
        return this.s;
    }

    public final int k() {
        return this.t;
    }

    public final void l() {
        if (this.s) {
            this.t -= 3;
            if (this.t <= 0) {
                this.s = false;
                this.u = 0;
                this.v = 0;
            }
        }
    }

    public final int m() {
        if (this.u != 0) {
            this.v++;
            if (this.v == 4) {
                this.v = 0;
                if (this.u == 1) {
                    this.u = 2;
                } else if (this.u == 2) {
                    this.u = 3;
                } else if (this.u == 3) {
                    this.u = 1;
                }
            }
        }
        return this.u;
    }

    public final void n() {
        this.w = true;
        this.x = 1;
        this.y = 0;
        this.z++;
    }

    public final boolean o() {
        return this.w;
    }

    public final int p() {
        return this.z;
    }

    public final int q() {
        if (this.x != 0) {
            this.y++;
            if (this.y == 3) {
                this.y = 0;
                if (this.x == 1) {
                    this.x = 2;
                } else if (this.x == 2) {
                    this.x = 3;
                } else if (this.x == 3) {
                    this.x = 4;
                } else if (this.x == 4) {
                    this.x = 5;
                } else if (this.x == 5) {
                    this.x = 6;
                } else if (this.x == 6) {
                    this.x = 0;
                    this.w = false;
                }
            }
        }
        return this.x;
    }
}
