package com.ningo.game.ninja;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

final class o extends Handler {
    private /* synthetic */ AgileBuddyView a;

    o(AgileBuddyView agileBuddyView) {
        this.a = agileBuddyView;
    }

    public final void handleMessage(Message message) {
        this.a.f = message.getData().getInt("1");
        if (this.a.at != null) {
            AgileBuddyView.aq(this.a);
            ((AgileBuddyActivity) this.a.b).finish();
            return;
        }
        Log.v("View", "mParentActivity is null");
    }
}
