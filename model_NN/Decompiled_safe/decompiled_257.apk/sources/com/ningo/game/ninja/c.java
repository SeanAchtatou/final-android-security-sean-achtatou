package com.ningo.game.ninja;

import android.widget.CompoundButton;

final class c implements CompoundButton.OnCheckedChangeListener {
    private /* synthetic */ Prefs a;

    c(Prefs prefs) {
        this.a = prefs;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (z) {
            this.a.a.edit().putBoolean("sounds", true).commit();
        } else {
            this.a.a.edit().putBoolean("sounds", false).commit();
        }
    }
}
