package com.ningo.game.ninja;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.a.a.c;

final class f implements View.OnClickListener {
    private /* synthetic */ AdSplash a;

    f(AdSplash adSplash) {
        this.a = adSplash;
    }

    public final void onClick(View view) {
        c.c();
        this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(c.b())));
    }
}
