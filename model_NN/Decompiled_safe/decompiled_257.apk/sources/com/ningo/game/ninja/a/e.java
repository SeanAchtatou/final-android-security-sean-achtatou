package com.ningo.game.ninja.a;

public final class e {
    private int a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i = 320;
    private int j = 713;
    private int k = 0;
    private int l = 0;
    private int m = 0;
    private int n = 0;

    public e(int i2, int i3) {
        this.g = i3;
        this.h = i2;
        this.a = 0;
        this.b = this.a + this.h;
        this.c = 0;
        this.d = this.c + this.g;
        this.e = 0;
        this.f = 0;
    }

    public final int a() {
        return this.c;
    }

    public final void a(int i2) {
        this.l = i2;
    }

    public final int b() {
        return this.d;
    }

    public final int c() {
        return this.h;
    }

    public final int d() {
        return this.d > this.c ? this.g : this.j - this.c;
    }

    public final int e() {
        if (this.d > this.c) {
            return 0;
        }
        return this.d;
    }

    public final void f() {
        int i2 = this.c + this.l;
        this.e += this.l;
        if (i2 > this.j) {
            this.c = i2 - this.j;
            this.d = this.c + this.g;
            return;
        }
        this.c = i2;
        int i3 = this.c + this.g;
        if (i3 > this.j) {
            this.d = i3 - this.j;
        } else {
            this.d = i3;
        }
    }

    public final int g() {
        return this.e;
    }
}
