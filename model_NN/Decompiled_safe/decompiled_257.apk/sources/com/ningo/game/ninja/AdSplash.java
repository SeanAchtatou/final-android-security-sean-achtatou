package com.ningo.game.ninja;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.a.a.c;
import com.google.ads.R;
import java.util.Timer;
import java.util.TimerTask;

public class AdSplash extends Activity {
    Handler a = new i(this);
    /* access modifiers changed from: private */
    public boolean b = false;
    private Timer c = new Timer();
    private TimerTask d = new j(this);

    public void finish() {
        if (this.c != null) {
            this.c.cancel();
            this.c = null;
        }
        Log.v("AdSplash", "finish()....is call!");
        super.finish();
        Process.killProcess(Process.myPid());
    }

    public void onCreate(Bundle bundle) {
        int i;
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.ad_splash);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.ad_splash_layout);
        Button button = (Button) findViewById(R.id.btn_download);
        Button button2 = (Button) findViewById(R.id.btn_more);
        Button button3 = (Button) findViewById(R.id.btn_more2);
        ((Button) findViewById(R.id.btn_close)).setOnClickListener(new h(this));
        if (c.f()) {
            String a2 = c.a(c.e());
            Log.v("AdSplash", "show adsplash...imgFile=" + a2);
            try {
                Bitmap decodeFile = BitmapFactory.decodeFile(a2);
                if (decodeFile != null) {
                    if (decodeFile.getWidth() > decodeFile.getHeight()) {
                        setRequestedOrientation(0);
                    } else {
                        setRequestedOrientation(1);
                    }
                    relativeLayout.setBackgroundDrawable(new BitmapDrawable(decodeFile));
                    button.setVisibility(0);
                    button3.setVisibility(0);
                    button2.setVisibility(8);
                    button.setOnClickListener(new g(this));
                    button3.setOnClickListener(new f(this));
                    i = 15000;
                } else {
                    i = 15000;
                }
            } catch (Exception e) {
                i = 15000;
            }
        } else {
            button.setVisibility(8);
            button3.setVisibility(8);
            button2.setVisibility(0);
            button2.setOnClickListener(new e(this));
            i = 8000;
        }
        this.c.schedule(this.d, (long) i, 100);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        c.c();
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.b = false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.b = true;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.b = true;
    }
}
