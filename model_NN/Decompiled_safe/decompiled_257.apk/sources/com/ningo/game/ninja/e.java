package com.ningo.game.ninja;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import com.a.a.c;

final class e implements View.OnClickListener {
    private /* synthetic */ AdSplash a;

    e(AdSplash adSplash) {
        this.a = adSplash;
    }

    public final void onClick(View view) {
        Log.v("AdSplash", "moreGame.click(), more url=" + c.b());
        try {
            this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(c.b())));
        } catch (Exception e) {
        }
    }
}
