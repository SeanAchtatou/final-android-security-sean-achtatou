package com.ningo.game.ninja.a;

public final class k {
    private int a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h = 0;
    private int i = 0;

    public k(int i2, int i3) {
        this.a = i2;
        this.b = i3 * 10;
        this.c = 32;
        this.d = 48;
        this.g = 2;
        this.e = 0;
        this.f = 0;
    }

    public final int a() {
        return this.a;
    }

    public final void a(int i2) {
        this.a = i2;
    }

    public final int b() {
        return this.a + this.c;
    }

    public final void b(int i2) {
        this.b = i2;
    }

    public final int c() {
        return this.b / 10;
    }

    public final void c(int i2) {
        this.a += i2;
    }

    public final int d() {
        return (this.b / 10) + this.d;
    }

    public final void d(int i2) {
        this.b += i2;
    }

    public final int e() {
        return this.c;
    }

    public final void e(int i2) {
        this.e = i2;
    }

    public final int f() {
        return this.d;
    }

    public final int g() {
        return this.e;
    }

    public final int h() {
        if (this.e == 0) {
            this.f = 0;
            return this.f;
        }
        this.h++;
        if (this.h == this.g) {
            this.h = 0;
            if (this.e == 3) {
                if (this.f == 1) {
                    this.f = 2;
                } else if (this.f == 2) {
                    this.f = 3;
                } else if (this.f == 3) {
                    this.f = 4;
                } else {
                    this.f = 1;
                }
            } else if (this.e == 5 || this.e == 2) {
                if (this.f == 9) {
                    this.f = 10;
                } else if (this.f == 10) {
                    this.f = 11;
                } else if (this.f == 11) {
                    this.f = 12;
                } else {
                    this.f = 9;
                }
            } else if (this.f == 5) {
                this.f = 6;
            } else if (this.f == 6) {
                this.f = 7;
            } else if (this.f == 7) {
                this.f = 8;
            } else {
                this.f = 5;
            }
        }
        return this.f;
    }
}
