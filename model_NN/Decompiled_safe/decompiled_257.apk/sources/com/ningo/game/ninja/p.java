package com.ningo.game.ninja;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.view.SurfaceHolder;
import com.ningo.game.ninja.a.a;
import com.ningo.game.ninja.a.g;
import com.ningo.game.ninja.a.n;
import java.util.Iterator;

final class p extends Thread {
    private SurfaceHolder a;
    private Context b;
    private Handler c;
    private boolean d = true;
    private long e = 0;
    private boolean f = true;
    private a g;
    private long h;
    private /* synthetic */ AgileBuddyView i;

    public p(AgileBuddyView agileBuddyView, SurfaceHolder surfaceHolder, Context context, Handler handler) {
        this.i = agileBuddyView;
        this.a = surfaceHolder;
        this.b = context;
        this.c = handler;
    }

    private Bitmap a(int i2) {
        switch (i2) {
            case 1:
                return this.i.N;
            case 2:
                return this.i.O;
            case 3:
                return this.i.P;
            case 4:
                return this.i.Q;
            case 5:
                return this.i.R;
            case 6:
                return this.i.S;
            default:
                return null;
        }
    }

    private void a(Canvas canvas) {
        int i2;
        Iterator it = this.g.d.iterator();
        while (it.hasNext()) {
            g gVar = (g) it.next();
            int f2 = gVar.f();
            int d2 = gVar.d();
            if (f2 == 1) {
                Bitmap e2 = d2 == 1 ? this.i.U : d2 == 2 ? this.i.V : d2 == 3 ? this.i.W : this.i.X;
                Paint paint = new Paint();
                paint.setColor(-16777216);
                for (int i3 = 0; i3 < 5; i3++) {
                    switch (i3) {
                        case 0:
                            i2 = 5;
                            break;
                        case 1:
                            i2 = 10;
                            break;
                        case 2:
                            i2 = 20;
                            break;
                        case 3:
                            i2 = 30;
                            break;
                        case 4:
                            i2 = 225;
                            break;
                        default:
                            i2 = 225;
                            break;
                    }
                    paint.setAlpha(i2);
                    canvas.drawBitmap(e2, (float) gVar.b, (float) (gVar.c - ((3 - i3) * 8)), paint);
                }
            }
        }
    }

    public final void a(n nVar) {
        if (this.g != null) {
            this.d = false;
            this.g.b();
        }
        this.g = new a(nVar, this.i.e < 10 ? -2 : this.i.e < 25 ? -1 : this.i.e < 50 ? 0 : this.i.e < 60 ? 1 : this.i.e < 70 ? 2 : this.i.e < 80 ? 3 : this.i.e < 90 ? 4 : 5);
    }

    public final void a(boolean z) {
        this.d = z;
    }

    public final void a(boolean z, float f2) {
        if (this.g == null) {
            return;
        }
        if (z) {
            this.g.a(f2);
        } else {
            this.g.b(f2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:0x0477 A[Catch:{ Exception -> 0x049d }] */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x06e4 A[SYNTHETIC, Splitter:B:189:0x06e4] */
    /* JADX WARNING: Removed duplicated region for block: B:197:0x06e9 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x0000 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0468 A[Catch:{ Exception -> 0x049d }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r11 = this;
        L_0x0000:
            boolean r0 = r11.d
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            r0 = 0
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x049d }
            r11.h = r1     // Catch:{ Exception -> 0x049d }
            com.ningo.game.ninja.AgileBuddyView r1 = r11.i     // Catch:{ Exception -> 0x06ec, all -> 0x06de }
            boolean r1 = r1.au     // Catch:{ Exception -> 0x06ec, all -> 0x06de }
            if (r1 != 0) goto L_0x0019
            com.ningo.game.ninja.a.a r1 = r11.g     // Catch:{ Exception -> 0x06ec, all -> 0x06de }
            r1.a()     // Catch:{ Exception -> 0x06ec, all -> 0x06de }
        L_0x0019:
            android.view.SurfaceHolder r1 = r11.a     // Catch:{ Exception -> 0x06ec, all -> 0x06de }
            r2 = 0
            android.graphics.Canvas r1 = r1.lockCanvas(r2)     // Catch:{ Exception -> 0x06ec, all -> 0x06de }
            android.view.SurfaceHolder r2 = r11.a     // Catch:{ Exception -> 0x04f4 }
            monitor-enter(r2)     // Catch:{ Exception -> 0x04f4 }
            r0 = 0
            com.ningo.game.ninja.a.a r3 = r11.g     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.e r3 = r3.b     // Catch:{ all -> 0x04f1 }
            int r3 = r3.a()     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.a r4 = r11.g     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.e r4 = r4.b     // Catch:{ all -> 0x04f1 }
            int r4 = r4.b()     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.a r5 = r11.g     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.e r5 = r5.b     // Catch:{ all -> 0x04f1 }
            int r5 = r5.c()     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.a r6 = r11.g     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.h r6 = r6.e()     // Catch:{ all -> 0x04f1 }
            int r6 = r6.p()     // Catch:{ all -> 0x04f1 }
            if (r6 != 0) goto L_0x04a7
            com.ningo.game.ninja.AgileBuddyView r6 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r6 = r6.g     // Catch:{ all -> 0x04f1 }
        L_0x004e:
            if (r4 < r3) goto L_0x04ba
            com.ningo.game.ninja.a.a r4 = r11.g     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.e r4 = r4.b     // Catch:{ all -> 0x04f1 }
            int r4 = r4.d()     // Catch:{ all -> 0x04f1 }
            android.graphics.Rect r7 = new android.graphics.Rect     // Catch:{ all -> 0x04f1 }
            r8 = 0
            int r9 = r3 + r4
            r7.<init>(r8, r3, r5, r9)     // Catch:{ all -> 0x04f1 }
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x04f1 }
            r8 = 0
            r9 = 0
            r3.<init>(r8, r9, r5, r4)     // Catch:{ all -> 0x04f1 }
            r4 = 0
            r1.drawBitmap(r6, r7, r3, r4)     // Catch:{ all -> 0x04f1 }
        L_0x006b:
            com.ningo.game.ninja.a.a r3 = r11.g     // Catch:{ all -> 0x04f1 }
            java.util.List r3 = r3.g()     // Catch:{ all -> 0x04f1 }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ all -> 0x04f1 }
            r4 = r0
        L_0x0076:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x04f1 }
            if (r0 != 0) goto L_0x0505
            r11.a(r1)     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.a r0 = r11.g     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.f r0 = r0.f     // Catch:{ all -> 0x04f1 }
            int r0 = r0.a()     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.drawable.Drawable r3 = r3.ak     // Catch:{ all -> 0x04f1 }
            r4 = 0
            r5 = 180(0xb4, float:2.52E-43)
            int r5 = r0 - r5
            com.ningo.game.ninja.AgileBuddyView r6 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r6 = r6.d     // Catch:{ all -> 0x04f1 }
            int r6 = r6.c     // Catch:{ all -> 0x04f1 }
            r3.setBounds(r4, r5, r6, r0)     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r0 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.drawable.Drawable r0 = r0.ak     // Catch:{ all -> 0x04f1 }
            r0.draw(r1)     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.a r0 = r11.g     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.k r0 = r0.c()     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.a r3 = r11.g     // Catch:{ all -> 0x04f1 }
            int r3 = r3.c     // Catch:{ all -> 0x04f1 }
            r4 = 2
            if (r3 != r4) goto L_0x05dc
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.k     // Catch:{ all -> 0x04f1 }
            int r4 = r0.a()     // Catch:{ all -> 0x04f1 }
            float r4 = (float) r4     // Catch:{ all -> 0x04f1 }
            int r0 = r0.c()     // Catch:{ all -> 0x04f1 }
            float r0 = (float) r0     // Catch:{ all -> 0x04f1 }
            r5 = 0
            r1.drawBitmap(r3, r4, r0, r5)     // Catch:{ all -> 0x04f1 }
        L_0x00c7:
            com.ningo.game.ninja.a.a r0 = r11.g     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.i r0 = r0.h()     // Catch:{ all -> 0x04f1 }
            int r3 = r0.a     // Catch:{ all -> 0x04f1 }
            if (r3 == 0) goto L_0x00e5
            int r3 = r0.b     // Catch:{ all -> 0x04f1 }
            if (r3 <= 0) goto L_0x00e5
            int r3 = r0.a     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r11.a(r3)     // Catch:{ all -> 0x04f1 }
            int r4 = r0.c     // Catch:{ all -> 0x04f1 }
            float r4 = (float) r4     // Catch:{ all -> 0x04f1 }
            int r0 = r0.d     // Catch:{ all -> 0x04f1 }
            float r0 = (float) r0     // Catch:{ all -> 0x04f1 }
            r5 = 0
            r1.drawBitmap(r3, r4, r0, r5)     // Catch:{ all -> 0x04f1 }
        L_0x00e5:
            com.ningo.game.ninja.a.a r0 = r11.g     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.d r0 = r0.e     // Catch:{ all -> 0x04f1 }
            boolean r3 = r0.c()     // Catch:{ all -> 0x04f1 }
            if (r3 == 0) goto L_0x0110
            int r3 = r0.e()     // Catch:{ all -> 0x04f1 }
            switch(r3) {
                case 1: goto L_0x0659;
                case 2: goto L_0x0661;
                case 3: goto L_0x0669;
                default: goto L_0x00f6;
            }     // Catch:{ all -> 0x04f1 }
        L_0x00f6:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.af     // Catch:{ all -> 0x04f1 }
        L_0x00fc:
            r4 = 0
            int r5 = r0.a()     // Catch:{ all -> 0x04f1 }
            float r5 = (float) r5     // Catch:{ all -> 0x04f1 }
            r6 = 0
            r1.drawBitmap(r3, r4, r5, r6)     // Catch:{ all -> 0x04f1 }
            r4 = 0
            int r0 = r0.b()     // Catch:{ all -> 0x04f1 }
            float r0 = (float) r0     // Catch:{ all -> 0x04f1 }
            r5 = 0
            r1.drawBitmap(r3, r4, r0, r5)     // Catch:{ all -> 0x04f1 }
        L_0x0110:
            com.ningo.game.ninja.a.a r0 = r11.g     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.j r0 = r0.d()     // Catch:{ all -> 0x04f1 }
            boolean r3 = r0.c()     // Catch:{ all -> 0x04f1 }
            if (r3 == 0) goto L_0x0162
            int r3 = r0.a()     // Catch:{ all -> 0x04f1 }
            r4 = 1
            if (r3 != r4) goto L_0x0671
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.Y     // Catch:{ all -> 0x04f1 }
        L_0x0129:
            int r0 = r0.f()     // Catch:{ all -> 0x04f1 }
            r5 = 1
            int r0 = r0 - r5
            int r0 = r0 * 78
            android.graphics.Rect r5 = new android.graphics.Rect     // Catch:{ all -> 0x04f1 }
            r6 = 0
            int r7 = r0 + 78
            r8 = 84
            r5.<init>(r0, r6, r7, r8)     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.a r0 = r11.g     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.k r0 = r0.c()     // Catch:{ all -> 0x04f1 }
            r6 = 1
            if (r3 == r6) goto L_0x0147
            r6 = 3
            if (r3 != r6) goto L_0x0684
        L_0x0147:
            int r3 = r0.a()     // Catch:{ all -> 0x04f1 }
            r6 = 23
            int r3 = r3 - r6
            int r0 = r0.c()     // Catch:{ all -> 0x04f1 }
            r6 = 18
            int r0 = r0 - r6
        L_0x0155:
            android.graphics.Rect r6 = new android.graphics.Rect     // Catch:{ all -> 0x04f1 }
            int r7 = r3 + 78
            int r8 = r0 + 84
            r6.<init>(r3, r0, r7, r8)     // Catch:{ all -> 0x04f1 }
            r0 = 0
            r1.drawBitmap(r4, r5, r6, r0)     // Catch:{ all -> 0x04f1 }
        L_0x0162:
            com.ningo.game.ninja.a.a r0 = r11.g     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.l r0 = r0.a     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Paint r3 = r3.ar     // Catch:{ all -> 0x04f1 }
            r0.a(r1, r3)     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r0 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.drawable.Drawable r0 = r0.al     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r3 = r3.d     // Catch:{ all -> 0x04f1 }
            int r3 = r3.a     // Catch:{ all -> 0x04f1 }
            int r3 = r3 + 6
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r4 = r4.d     // Catch:{ all -> 0x04f1 }
            int r4 = r4.d     // Catch:{ all -> 0x04f1 }
            r5 = 13
            int r4 = r4 - r5
            com.ningo.game.ninja.AgileBuddyView r5 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r5 = r5.d     // Catch:{ all -> 0x04f1 }
            int r5 = r5.c     // Catch:{ all -> 0x04f1 }
            r6 = 6
            int r5 = r5 - r6
            com.ningo.game.ninja.AgileBuddyView r6 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r6 = r6.d     // Catch:{ all -> 0x04f1 }
            int r6 = r6.d     // Catch:{ all -> 0x04f1 }
            r7 = 5
            int r6 = r6 - r7
            r0.setBounds(r3, r4, r5, r6)     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r0 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.drawable.Drawable r0 = r0.al     // Catch:{ all -> 0x04f1 }
            r0.draw(r1)     // Catch:{ all -> 0x04f1 }
            r0 = 1088421888(0x40e00000, float:7.0)
            r3 = 1134100480(0x43990000, float:306.0)
            com.ningo.game.ninja.a.a r4 = r11.g     // Catch:{ all -> 0x04f1 }
            float r4 = r4.i()     // Catch:{ all -> 0x04f1 }
            float r3 = r3 * r4
            float r0 = r0 + r3
            int r0 = (int) r0     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.drawable.Drawable r3 = r3.am     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r4 = r4.d     // Catch:{ all -> 0x04f1 }
            int r4 = r4.a     // Catch:{ all -> 0x04f1 }
            int r4 = r4 + 7
            com.ningo.game.ninja.AgileBuddyView r5 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r5 = r5.d     // Catch:{ all -> 0x04f1 }
            int r5 = r5.d     // Catch:{ all -> 0x04f1 }
            r6 = 12
            int r5 = r5 - r6
            com.ningo.game.ninja.AgileBuddyView r6 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r6 = r6.d     // Catch:{ all -> 0x04f1 }
            int r6 = r6.d     // Catch:{ all -> 0x04f1 }
            r7 = 6
            int r6 = r6 - r7
            r3.setBounds(r4, r5, r0, r6)     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r0 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.drawable.Drawable r0 = r0.am     // Catch:{ all -> 0x04f1 }
            r0.draw(r1)     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.a r0 = r11.g     // Catch:{ all -> 0x04f1 }
            float r0 = r0.j()     // Catch:{ all -> 0x04f1 }
            r3 = 0
            int r3 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r3 <= 0) goto L_0x028a
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Paint r3 = r3.aq     // Catch:{ all -> 0x04f1 }
            android.graphics.Paint$FontMetrics r3 = r3.getFontMetrics()     // Catch:{ all -> 0x04f1 }
            java.lang.String r4 = "Power"
            r5 = 1084227584(0x40a00000, float:5.0)
            com.ningo.game.ninja.AgileBuddyView r6 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r6 = r6.d     // Catch:{ all -> 0x04f1 }
            int r6 = r6.d     // Catch:{ all -> 0x04f1 }
            float r6 = (float) r6     // Catch:{ all -> 0x04f1 }
            r7 = 1106247680(0x41f00000, float:30.0)
            float r6 = r6 - r7
            float r7 = r3.ascent     // Catch:{ all -> 0x04f1 }
            float r3 = r3.descent     // Catch:{ all -> 0x04f1 }
            float r3 = r3 + r7
            float r3 = r6 - r3
            com.ningo.game.ninja.AgileBuddyView r6 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Paint r6 = r6.aq     // Catch:{ all -> 0x04f1 }
            r1.drawText(r4, r5, r3, r6)     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.drawable.Drawable r3 = r3.an     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r4 = r4.d     // Catch:{ all -> 0x04f1 }
            int r4 = r4.a     // Catch:{ all -> 0x04f1 }
            int r4 = r4 + 55
            com.ningo.game.ninja.AgileBuddyView r5 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r5 = r5.d     // Catch:{ all -> 0x04f1 }
            int r5 = r5.d     // Catch:{ all -> 0x04f1 }
            r6 = 27
            int r5 = r5 - r6
            r6 = 175(0xaf, float:2.45E-43)
            com.ningo.game.ninja.AgileBuddyView r7 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r7 = r7.d     // Catch:{ all -> 0x04f1 }
            int r7 = r7.d     // Catch:{ all -> 0x04f1 }
            r8 = 20
            int r7 = r7 - r8
            r3.setBounds(r4, r5, r6, r7)     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.drawable.Drawable r3 = r3.an     // Catch:{ all -> 0x04f1 }
            r3.draw(r1)     // Catch:{ all -> 0x04f1 }
            r3 = 1113325568(0x425c0000, float:55.0)
            r4 = 1123024896(0x42f00000, float:120.0)
            float r0 = r0 * r4
            float r0 = r0 + r3
            int r0 = (int) r0     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.drawable.Drawable r3 = r3.ao     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r4 = r4.d     // Catch:{ all -> 0x04f1 }
            int r4 = r4.a     // Catch:{ all -> 0x04f1 }
            int r4 = r4 + 55
            com.ningo.game.ninja.AgileBuddyView r5 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r5 = r5.d     // Catch:{ all -> 0x04f1 }
            int r5 = r5.d     // Catch:{ all -> 0x04f1 }
            r6 = 27
            int r5 = r5 - r6
            com.ningo.game.ninja.AgileBuddyView r6 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r6 = r6.d     // Catch:{ all -> 0x04f1 }
            int r6 = r6.d     // Catch:{ all -> 0x04f1 }
            r7 = 20
            int r6 = r6 - r7
            r3.setBounds(r4, r5, r0, r6)     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r0 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.drawable.Drawable r0 = r0.ao     // Catch:{ all -> 0x04f1 }
            r0.draw(r1)     // Catch:{ all -> 0x04f1 }
        L_0x028a:
            com.ningo.game.ninja.a.a r0 = r11.g     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.h r0 = r0.e()     // Catch:{ all -> 0x04f1 }
            int r3 = r0.b()     // Catch:{ all -> 0x04f1 }
            if (r3 == 0) goto L_0x02bb
            r4 = 1
            if (r3 != r4) goto L_0x0694
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.ad     // Catch:{ all -> 0x04f1 }
        L_0x029f:
            android.graphics.Rect r4 = new android.graphics.Rect     // Catch:{ all -> 0x04f1 }
            r5 = 0
            r6 = 0
            com.ningo.game.ninja.AgileBuddyView r7 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r7 = r7.d     // Catch:{ all -> 0x04f1 }
            int r7 = r7.c     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r8 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r8 = r8.d     // Catch:{ all -> 0x04f1 }
            int r8 = r8.d     // Catch:{ all -> 0x04f1 }
            r4.<init>(r5, r6, r7, r8)     // Catch:{ all -> 0x04f1 }
            r5 = 0
            r6 = 0
            r1.drawBitmap(r3, r5, r4, r6)     // Catch:{ all -> 0x04f1 }
        L_0x02bb:
            boolean r3 = r0.c()     // Catch:{ all -> 0x04f1 }
            if (r3 == 0) goto L_0x02ed
            int r3 = r0.g()     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r11.a(r3)     // Catch:{ all -> 0x04f1 }
            if (r3 == 0) goto L_0x02ed
            android.graphics.Point r4 = r0.e()     // Catch:{ all -> 0x04f1 }
            int r4 = r4.x     // Catch:{ all -> 0x04f1 }
            android.graphics.Point r5 = r0.e()     // Catch:{ all -> 0x04f1 }
            int r5 = r5.y     // Catch:{ all -> 0x04f1 }
            int r6 = r0.f()     // Catch:{ all -> 0x04f1 }
            android.graphics.Paint r7 = new android.graphics.Paint     // Catch:{ all -> 0x04f1 }
            r7.<init>()     // Catch:{ all -> 0x04f1 }
            r8 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r7.setColor(r8)     // Catch:{ all -> 0x04f1 }
            r7.setAlpha(r6)     // Catch:{ all -> 0x04f1 }
            float r4 = (float) r4     // Catch:{ all -> 0x04f1 }
            float r5 = (float) r5     // Catch:{ all -> 0x04f1 }
            r1.drawBitmap(r3, r4, r5, r7)     // Catch:{ all -> 0x04f1 }
        L_0x02ed:
            boolean r3 = r0.j()     // Catch:{ all -> 0x04f1 }
            if (r3 == 0) goto L_0x032d
            boolean r3 = r0.j()     // Catch:{ all -> 0x04f1 }
            if (r3 == 0) goto L_0x032d
            int r3 = r0.m()     // Catch:{ all -> 0x04f1 }
            r4 = 1
            int r3 = r3 - r4
            int r3 = r3 * 70
            android.graphics.Rect r4 = new android.graphics.Rect     // Catch:{ all -> 0x04f1 }
            r5 = 0
            r6 = 320(0x140, float:4.48E-43)
            int r7 = r3 + 70
            r4.<init>(r5, r3, r6, r7)     // Catch:{ all -> 0x04f1 }
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x04f1 }
            r5 = 0
            int r6 = r0.k()     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r7 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r7 = r7.d     // Catch:{ all -> 0x04f1 }
            int r7 = r7.c     // Catch:{ all -> 0x04f1 }
            int r8 = r0.k()     // Catch:{ all -> 0x04f1 }
            int r8 = r8 + 70
            r3.<init>(r5, r6, r7, r8)     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r5 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r5 = r5.ai     // Catch:{ all -> 0x04f1 }
            r6 = 0
            r1.drawBitmap(r5, r4, r3, r6)     // Catch:{ all -> 0x04f1 }
        L_0x032d:
            int r3 = r0.h()     // Catch:{ all -> 0x04f1 }
            if (r3 == 0) goto L_0x06b1
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Paint r4 = r4.ap     // Catch:{ all -> 0x04f1 }
            float r4 = r4.getTextSize()     // Catch:{ all -> 0x04f1 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.a r6 = r11.g     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.h r6 = r6.e()     // Catch:{ all -> 0x04f1 }
            int r6 = r6.i()     // Catch:{ all -> 0x04f1 }
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ all -> 0x04f1 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ all -> 0x04f1 }
            r5.<init>(r6)     // Catch:{ all -> 0x04f1 }
            java.lang.String r6 = "m"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x04f1 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x04f1 }
            switch(r3) {
                case 1: goto L_0x069c;
                case 2: goto L_0x06a3;
                case 3: goto L_0x06aa;
                default: goto L_0x0361;
            }     // Catch:{ all -> 0x04f1 }
        L_0x0361:
            r3 = r5
        L_0x0362:
            com.ningo.game.ninja.AgileBuddyView r5 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Paint r5 = r5.ap     // Catch:{ all -> 0x04f1 }
            r5.setTextSize(r4)     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Paint r4 = r4.ap     // Catch:{ all -> 0x04f1 }
            android.graphics.Paint$FontMetrics r4 = r4.getFontMetrics()     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r5 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r5 = r5.d     // Catch:{ all -> 0x04f1 }
            int r5 = r5.d     // Catch:{ all -> 0x04f1 }
            r6 = 10
            int r5 = r5 - r6
            float r4 = r4.descent     // Catch:{ all -> 0x04f1 }
            double r6 = (double) r4     // Catch:{ all -> 0x04f1 }
            double r6 = java.lang.Math.ceil(r6)     // Catch:{ all -> 0x04f1 }
            r8 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r6 = r6 + r8
            int r4 = (int) r6     // Catch:{ all -> 0x04f1 }
            int r4 = r5 - r4
            com.ningo.game.ninja.AgileBuddyView r5 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r5 = r5.d     // Catch:{ all -> 0x04f1 }
            int r5 = r5.c     // Catch:{ all -> 0x04f1 }
            r6 = 5
            int r5 = r5 - r6
            float r5 = (float) r5     // Catch:{ all -> 0x04f1 }
            float r4 = (float) r4     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r6 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Paint r6 = r6.ap     // Catch:{ all -> 0x04f1 }
            r1.drawText(r3, r5, r4, r6)     // Catch:{ all -> 0x04f1 }
            boolean r3 = r0.o()     // Catch:{ all -> 0x04f1 }
            if (r3 == 0) goto L_0x03e1
            boolean r3 = r0.o()     // Catch:{ all -> 0x04f1 }
            if (r3 == 0) goto L_0x03e1
            int r0 = r0.q()     // Catch:{ all -> 0x04f1 }
            r3 = 1
            int r0 = r0 - r3
            int r0 = r0 * 240
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x04f1 }
            r4 = 0
            int r5 = r0 + 240
            r6 = 320(0x140, float:4.48E-43)
            r3.<init>(r0, r4, r5, r6)     // Catch:{ all -> 0x04f1 }
            android.graphics.Rect r0 = new android.graphics.Rect     // Catch:{ all -> 0x04f1 }
            r4 = 0
            r5 = 0
            com.ningo.game.ninja.AgileBuddyView r6 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r6 = r6.d     // Catch:{ all -> 0x04f1 }
            int r6 = r6.c     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r7 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r7 = r7.d     // Catch:{ all -> 0x04f1 }
            int r7 = r7.d     // Catch:{ all -> 0x04f1 }
            r0.<init>(r4, r5, r6, r7)     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.aj     // Catch:{ all -> 0x04f1 }
            r5 = 0
            r1.drawBitmap(r4, r3, r0, r5)     // Catch:{ all -> 0x04f1 }
        L_0x03e1:
            com.ningo.game.ninja.AgileBuddyView r0 = r11.i     // Catch:{ all -> 0x04f1 }
            boolean r0 = r0.au     // Catch:{ all -> 0x04f1 }
            if (r0 == 0) goto L_0x06d8
            android.graphics.Paint r0 = new android.graphics.Paint     // Catch:{ all -> 0x04f1 }
            r0.<init>()     // Catch:{ all -> 0x04f1 }
            r3 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0.setColor(r3)     // Catch:{ all -> 0x04f1 }
            r3 = 150(0x96, float:2.1E-43)
            r0.setAlpha(r3)     // Catch:{ all -> 0x04f1 }
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x04f1 }
            r4 = 0
            r5 = 0
            com.ningo.game.ninja.AgileBuddyView r6 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r6 = r6.d     // Catch:{ all -> 0x04f1 }
            int r6 = r6.c     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.AgileBuddyView r7 = r11.i     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.n r7 = r7.d     // Catch:{ all -> 0x04f1 }
            int r7 = r7.d     // Catch:{ all -> 0x04f1 }
            r3.<init>(r4, r5, r6, r7)     // Catch:{ all -> 0x04f1 }
            r1.drawRect(r3, r0)     // Catch:{ all -> 0x04f1 }
            long r3 = r11.e     // Catch:{ all -> 0x04f1 }
            r5 = 0
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 != 0) goto L_0x0423
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x04f1 }
            r11.e = r3     // Catch:{ all -> 0x04f1 }
            r0 = 1
            r11.f = r0     // Catch:{ all -> 0x04f1 }
        L_0x0423:
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x04f1 }
            long r5 = r11.e     // Catch:{ all -> 0x04f1 }
            long r3 = r3 - r5
            r5 = 800(0x320, double:3.953E-321)
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 < 0) goto L_0x043d
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x04f1 }
            r11.e = r3     // Catch:{ all -> 0x04f1 }
            boolean r0 = r11.f     // Catch:{ all -> 0x04f1 }
            if (r0 == 0) goto L_0x06d5
            r0 = 0
        L_0x043b:
            r11.f = r0     // Catch:{ all -> 0x04f1 }
        L_0x043d:
            boolean r0 = r11.f     // Catch:{ all -> 0x04f1 }
            if (r0 == 0) goto L_0x044f
            com.ningo.game.ninja.AgileBuddyView r0 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r0 = r0.T     // Catch:{ all -> 0x04f1 }
            r3 = 1114374144(0x426c0000, float:59.0)
            r4 = 1117782016(0x42a00000, float:80.0)
            r5 = 0
            r1.drawBitmap(r0, r3, r4, r5)     // Catch:{ all -> 0x04f1 }
        L_0x044f:
            monitor-exit(r2)     // Catch:{ all -> 0x04f1 }
            if (r1 == 0) goto L_0x0457
            android.view.SurfaceHolder r0 = r11.a     // Catch:{ Exception -> 0x049d }
            r0.unlockCanvasAndPost(r1)     // Catch:{ Exception -> 0x049d }
        L_0x0457:
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x049d }
            long r2 = r11.h     // Catch:{ Exception -> 0x049d }
            long r0 = r0 - r2
            r11.h = r0     // Catch:{ Exception -> 0x049d }
            long r0 = r11.h     // Catch:{ Exception -> 0x049d }
            r2 = 35
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x0470
            r0 = 35
            long r2 = r11.h     // Catch:{ Exception -> 0x049d }
            long r0 = r0 - r2
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x049d }
        L_0x0470:
            com.ningo.game.ninja.a.a r0 = r11.g     // Catch:{ Exception -> 0x049d }
            int r0 = r0.c     // Catch:{ Exception -> 0x049d }
            r1 = 2
            if (r0 != r1) goto L_0x0000
            r0 = 7
            com.ningo.game.ninja.a.m.a(r0)     // Catch:{ Exception -> 0x049d }
            android.os.Message r0 = new android.os.Message     // Catch:{ Exception -> 0x049d }
            r0.<init>()     // Catch:{ Exception -> 0x049d }
            android.os.Bundle r1 = new android.os.Bundle     // Catch:{ Exception -> 0x049d }
            r1.<init>()     // Catch:{ Exception -> 0x049d }
            java.lang.String r2 = "1"
            com.ningo.game.ninja.a.a r3 = r11.g     // Catch:{ Exception -> 0x049d }
            int r3 = r3.f()     // Catch:{ Exception -> 0x049d }
            r1.putInt(r2, r3)     // Catch:{ Exception -> 0x049d }
            r0.setData(r1)     // Catch:{ Exception -> 0x049d }
            android.os.Handler r1 = r11.c     // Catch:{ Exception -> 0x049d }
            r1.sendMessage(r0)     // Catch:{ Exception -> 0x049d }
            r0 = 0
            r11.d = r0     // Catch:{ Exception -> 0x049d }
            goto L_0x0000
        L_0x049d:
            r0 = move-exception
            java.lang.String r1 = ""
            java.lang.String r2 = "Error at 'run' method"
            android.util.Log.d(r1, r2, r0)
            goto L_0x0000
        L_0x04a7:
            r7 = 1
            if (r6 != r7) goto L_0x04b2
            com.ningo.game.ninja.AgileBuddyView r6 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r6 = r6.h     // Catch:{ all -> 0x04f1 }
            goto L_0x004e
        L_0x04b2:
            com.ningo.game.ninja.AgileBuddyView r6 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r6 = r6.i     // Catch:{ all -> 0x04f1 }
            goto L_0x004e
        L_0x04ba:
            com.ningo.game.ninja.a.a r4 = r11.g     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.e r4 = r4.b     // Catch:{ all -> 0x04f1 }
            int r4 = r4.d()     // Catch:{ all -> 0x04f1 }
            android.graphics.Rect r7 = new android.graphics.Rect     // Catch:{ all -> 0x04f1 }
            r8 = 0
            int r9 = r3 + r4
            r7.<init>(r8, r3, r5, r9)     // Catch:{ all -> 0x04f1 }
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x04f1 }
            r8 = 0
            r9 = 0
            r3.<init>(r8, r9, r5, r4)     // Catch:{ all -> 0x04f1 }
            r8 = 0
            r1.drawBitmap(r6, r7, r3, r8)     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.a r3 = r11.g     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.e r3 = r3.b     // Catch:{ all -> 0x04f1 }
            int r3 = r3.e()     // Catch:{ all -> 0x04f1 }
            android.graphics.Rect r7 = new android.graphics.Rect     // Catch:{ all -> 0x04f1 }
            r8 = 0
            r9 = 0
            r7.<init>(r8, r9, r5, r3)     // Catch:{ all -> 0x04f1 }
            android.graphics.Rect r8 = new android.graphics.Rect     // Catch:{ all -> 0x04f1 }
            r9 = 0
            int r3 = r3 + r4
            r8.<init>(r9, r4, r5, r3)     // Catch:{ all -> 0x04f1 }
            r3 = 0
            r1.drawBitmap(r6, r7, r8, r3)     // Catch:{ all -> 0x04f1 }
            goto L_0x006b
        L_0x04f1:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x04f1 }
            throw r0     // Catch:{ Exception -> 0x04f4 }
        L_0x04f4:
            r0 = move-exception
        L_0x04f5:
            java.lang.String r2 = ""
            java.lang.String r3 = "Error at 'run' method"
            android.util.Log.d(r2, r3, r0)     // Catch:{ all -> 0x06ea }
            if (r1 == 0) goto L_0x0457
            android.view.SurfaceHolder r0 = r11.a     // Catch:{ Exception -> 0x049d }
            r0.unlockCanvasAndPost(r1)     // Catch:{ Exception -> 0x049d }
            goto L_0x0457
        L_0x0505:
            java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.b r0 = (com.ningo.game.ninja.a.b) r0     // Catch:{ all -> 0x04f1 }
            int r5 = r0.m()     // Catch:{ all -> 0x04f1 }
            if (r5 == 0) goto L_0x052c
            switch(r5) {
                case 1: goto L_0x0549;
                case 2: goto L_0x0550;
                case 3: goto L_0x0557;
                case 4: goto L_0x055e;
                case 5: goto L_0x0565;
                case 6: goto L_0x056c;
                default: goto L_0x0514;
            }     // Catch:{ all -> 0x04f1 }
        L_0x0514:
            int r5 = r0.b()     // Catch:{ all -> 0x04f1 }
            int r6 = r0.n()     // Catch:{ all -> 0x04f1 }
            int r5 = r5 + r6
            float r5 = (float) r5     // Catch:{ all -> 0x04f1 }
            int r6 = r0.d()     // Catch:{ all -> 0x04f1 }
            r7 = 60
            int r6 = r6 - r7
            int r6 = r6 + 3
            float r6 = (float) r6     // Catch:{ all -> 0x04f1 }
            r7 = 0
            r1.drawBitmap(r4, r5, r6, r7)     // Catch:{ all -> 0x04f1 }
        L_0x052c:
            int r4 = r0.f()     // Catch:{ all -> 0x04f1 }
            switch(r4) {
                case 1: goto L_0x0573;
                case 2: goto L_0x0587;
                case 3: goto L_0x059b;
                case 4: goto L_0x05a2;
                case 5: goto L_0x05b6;
                case 6: goto L_0x05cc;
                case 7: goto L_0x05d4;
                default: goto L_0x0533;
            }     // Catch:{ all -> 0x04f1 }
        L_0x0533:
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.x     // Catch:{ all -> 0x04f1 }
        L_0x0539:
            int r5 = r0.b()     // Catch:{ all -> 0x04f1 }
            float r5 = (float) r5     // Catch:{ all -> 0x04f1 }
            int r0 = r0.d()     // Catch:{ all -> 0x04f1 }
            float r0 = (float) r0     // Catch:{ all -> 0x04f1 }
            r6 = 0
            r1.drawBitmap(r4, r5, r0, r6)     // Catch:{ all -> 0x04f1 }
            goto L_0x0076
        L_0x0549:
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.H     // Catch:{ all -> 0x04f1 }
            goto L_0x0514
        L_0x0550:
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.I     // Catch:{ all -> 0x04f1 }
            goto L_0x0514
        L_0x0557:
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.J     // Catch:{ all -> 0x04f1 }
            goto L_0x0514
        L_0x055e:
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.K     // Catch:{ all -> 0x04f1 }
            goto L_0x0514
        L_0x0565:
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.L     // Catch:{ all -> 0x04f1 }
            goto L_0x0514
        L_0x056c:
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.M     // Catch:{ all -> 0x04f1 }
            goto L_0x0514
        L_0x0573:
            boolean r4 = r0.i()     // Catch:{ all -> 0x04f1 }
            if (r4 != 0) goto L_0x0580
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.y     // Catch:{ all -> 0x04f1 }
            goto L_0x0539
        L_0x0580:
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.z     // Catch:{ all -> 0x04f1 }
            goto L_0x0539
        L_0x0587:
            boolean r4 = r0.k()     // Catch:{ all -> 0x04f1 }
            if (r4 != 0) goto L_0x0594
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.A     // Catch:{ all -> 0x04f1 }
            goto L_0x0539
        L_0x0594:
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.B     // Catch:{ all -> 0x04f1 }
            goto L_0x0539
        L_0x059b:
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.C     // Catch:{ all -> 0x04f1 }
            goto L_0x0539
        L_0x05a2:
            int r4 = r0.l()     // Catch:{ all -> 0x04f1 }
            if (r4 != 0) goto L_0x05af
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.D     // Catch:{ all -> 0x04f1 }
            goto L_0x0539
        L_0x05af:
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.E     // Catch:{ all -> 0x04f1 }
            goto L_0x0539
        L_0x05b6:
            int r4 = r0.l()     // Catch:{ all -> 0x04f1 }
            if (r4 != 0) goto L_0x05c4
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.F     // Catch:{ all -> 0x04f1 }
            goto L_0x0539
        L_0x05c4:
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.G     // Catch:{ all -> 0x04f1 }
            goto L_0x0539
        L_0x05cc:
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.ab     // Catch:{ all -> 0x04f1 }
            goto L_0x0539
        L_0x05d4:
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.ac     // Catch:{ all -> 0x04f1 }
            goto L_0x0539
        L_0x05dc:
            int r3 = r0.h()     // Catch:{ all -> 0x04f1 }
            switch(r3) {
                case 1: goto L_0x0605;
                case 2: goto L_0x060c;
                case 3: goto L_0x0613;
                case 4: goto L_0x061a;
                case 5: goto L_0x0621;
                case 6: goto L_0x0628;
                case 7: goto L_0x062f;
                case 8: goto L_0x0636;
                case 9: goto L_0x063d;
                case 10: goto L_0x0644;
                case 11: goto L_0x064b;
                case 12: goto L_0x0652;
                default: goto L_0x05e3;
            }     // Catch:{ all -> 0x04f1 }
        L_0x05e3:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.j     // Catch:{ all -> 0x04f1 }
        L_0x05e9:
            android.graphics.Rect r4 = new android.graphics.Rect     // Catch:{ all -> 0x04f1 }
            int r5 = r0.a()     // Catch:{ all -> 0x04f1 }
            int r6 = r0.c()     // Catch:{ all -> 0x04f1 }
            int r7 = r0.b()     // Catch:{ all -> 0x04f1 }
            int r0 = r0.d()     // Catch:{ all -> 0x04f1 }
            r4.<init>(r5, r6, r7, r0)     // Catch:{ all -> 0x04f1 }
            r0 = 0
            r5 = 0
            r1.drawBitmap(r3, r0, r4, r5)     // Catch:{ all -> 0x04f1 }
            goto L_0x00c7
        L_0x0605:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.l     // Catch:{ all -> 0x04f1 }
            goto L_0x05e9
        L_0x060c:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.m     // Catch:{ all -> 0x04f1 }
            goto L_0x05e9
        L_0x0613:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.n     // Catch:{ all -> 0x04f1 }
            goto L_0x05e9
        L_0x061a:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.o     // Catch:{ all -> 0x04f1 }
            goto L_0x05e9
        L_0x0621:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.p     // Catch:{ all -> 0x04f1 }
            goto L_0x05e9
        L_0x0628:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.q     // Catch:{ all -> 0x04f1 }
            goto L_0x05e9
        L_0x062f:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.r     // Catch:{ all -> 0x04f1 }
            goto L_0x05e9
        L_0x0636:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.s     // Catch:{ all -> 0x04f1 }
            goto L_0x05e9
        L_0x063d:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.t     // Catch:{ all -> 0x04f1 }
            goto L_0x05e9
        L_0x0644:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.u     // Catch:{ all -> 0x04f1 }
            goto L_0x05e9
        L_0x064b:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.v     // Catch:{ all -> 0x04f1 }
            goto L_0x05e9
        L_0x0652:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.w     // Catch:{ all -> 0x04f1 }
            goto L_0x05e9
        L_0x0659:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.af     // Catch:{ all -> 0x04f1 }
            goto L_0x00fc
        L_0x0661:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.ag     // Catch:{ all -> 0x04f1 }
            goto L_0x00fc
        L_0x0669:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.ah     // Catch:{ all -> 0x04f1 }
            goto L_0x00fc
        L_0x0671:
            r4 = 2
            if (r3 != r4) goto L_0x067c
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.aa     // Catch:{ all -> 0x04f1 }
            goto L_0x0129
        L_0x067c:
            com.ningo.game.ninja.AgileBuddyView r4 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r4 = r4.Z     // Catch:{ all -> 0x04f1 }
            goto L_0x0129
        L_0x0684:
            int r3 = r0.a()     // Catch:{ all -> 0x04f1 }
            r6 = 23
            int r3 = r3 - r6
            int r0 = r0.c()     // Catch:{ all -> 0x04f1 }
            r6 = 28
            int r0 = r0 - r6
            goto L_0x0155
        L_0x0694:
            com.ningo.game.ninja.AgileBuddyView r3 = r11.i     // Catch:{ all -> 0x04f1 }
            android.graphics.Bitmap r3 = r3.ae     // Catch:{ all -> 0x04f1 }
            goto L_0x029f
        L_0x069c:
            r3 = 1077936128(0x40400000, float:3.0)
            float r3 = r3 + r4
            r4 = r3
            r3 = r5
            goto L_0x0362
        L_0x06a3:
            r3 = 1082130432(0x40800000, float:4.0)
            float r3 = r3 + r4
            r4 = r3
            r3 = r5
            goto L_0x0362
        L_0x06aa:
            r3 = 1084227584(0x40a00000, float:5.0)
            float r3 = r3 + r4
            r4 = r3
            r3 = r5
            goto L_0x0362
        L_0x06b1:
            r3 = 1101004800(0x41a00000, float:20.0)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x04f1 }
            com.ningo.game.ninja.a.a r5 = r11.g     // Catch:{ all -> 0x04f1 }
            int r5 = r5.f()     // Catch:{ all -> 0x04f1 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ all -> 0x04f1 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ all -> 0x04f1 }
            r4.<init>(r5)     // Catch:{ all -> 0x04f1 }
            java.lang.String r5 = "m"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x04f1 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x04f1 }
            r10 = r4
            r4 = r3
            r3 = r10
            goto L_0x0362
        L_0x06d5:
            r0 = 1
            goto L_0x043b
        L_0x06d8:
            r3 = 0
            r11.e = r3     // Catch:{ all -> 0x04f1 }
            goto L_0x044f
        L_0x06de:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x06e2:
            if (r1 == 0) goto L_0x06e9
            android.view.SurfaceHolder r2 = r11.a     // Catch:{ Exception -> 0x049d }
            r2.unlockCanvasAndPost(r1)     // Catch:{ Exception -> 0x049d }
        L_0x06e9:
            throw r0     // Catch:{ Exception -> 0x049d }
        L_0x06ea:
            r0 = move-exception
            goto L_0x06e2
        L_0x06ec:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x04f5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ningo.game.ninja.p.run():void");
    }
}
