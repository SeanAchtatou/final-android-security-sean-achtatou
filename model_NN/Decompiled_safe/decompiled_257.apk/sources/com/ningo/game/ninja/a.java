package com.ningo.game.ninja;

import android.media.MediaPlayer;
import com.google.ads.R;

final class a {
    private MediaPlayer a = null;
    private boolean b;
    private /* synthetic */ Splash c;

    public a(Splash splash) {
        this.c = splash;
    }

    private void c() {
        if (this.a != null) {
            if (this.a.isPlaying()) {
                this.a.stop();
            }
            this.a.release();
            this.a = null;
        }
    }

    public final void a() {
        c();
    }

    public final void a(boolean z) {
        this.b = z;
    }

    public final void b() {
        if (this.b) {
            c();
            if (this.a == null) {
                this.a = MediaPlayer.create(this.c.getBaseContext(), (int) R.raw.m_menu);
                if (this.a == null) {
                    return;
                }
            }
            this.a.start();
        }
    }
}
