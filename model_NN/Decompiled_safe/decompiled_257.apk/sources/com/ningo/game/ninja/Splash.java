package com.ningo.game.ninja;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import com.a.a.c;
import com.google.ads.R;
import com.ningo.game.ninja.a.m;
import com.scoreloop.android.coreui.HighscoresActivity;
import com.scoreloop.android.coreui.k;

public class Splash extends Activity implements View.OnClickListener {
    private SharedPreferences a;
    private a b;
    private ImageView c = null;

    public void finish() {
        this.b.a();
        super.finish();
    }

    public void onClick(View view) {
        m.a(19);
        Intent intent = null;
        switch (view.getId()) {
            case R.id.start_game:
                intent = new Intent(this, TipsActivity.class);
                break;
            case R.id.options:
                intent = new Intent(this, Prefs.class);
                break;
            case R.id.score_board:
                startActivity(new Intent(this, HighscoresActivity.class));
                break;
            case R.id.more_app:
                intent = new Intent("android.intent.action.VIEW", Uri.parse(c.b()));
                break;
            case R.id.exit:
                startActivity(new Intent(this, AdSplash.class));
                finish();
                return;
        }
        if (intent != null) {
            startActivity(intent);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(1024, 1024);
        int height = ((WindowManager) getSystemService("window")).getDefaultDisplay().getHeight();
        if (height == 480) {
            setContentView((int) R.layout.splash_480);
        } else if (height == 533 || height == 534) {
            setContentView((int) R.layout.splash_533);
        } else if (height == 569) {
            setContentView((int) R.layout.splash_569);
        } else {
            setContentView((int) R.layout.splash_other);
        }
        this.c = (ImageView) findViewById(R.id.img_logo);
        k.a(this, "ad48783d-2861-436b-93e2-61e59b5cd304", "dKVAteAHjWBuWlYECt22JLdIkpSmfOlWNamSi32epabgC63G+oG6Lg==");
        k.a();
        ((Button) findViewById(R.id.start_game)).setOnClickListener(this);
        ((Button) findViewById(R.id.score_board)).setOnClickListener(this);
        ((Button) findViewById(R.id.more_app)).setOnClickListener(this);
        ((Button) findViewById(R.id.options)).setOnClickListener(this);
        ((Button) findViewById(R.id.exit)).setOnClickListener(this);
        this.a = getBaseContext().getSharedPreferences("com.gastudio.game.ninja", 0);
        boolean z = this.a.getBoolean("sounds", true);
        m.a(getBaseContext());
        this.b = new a(this);
        this.b.a(z);
        this.b.b();
        c.a();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            return false;
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
