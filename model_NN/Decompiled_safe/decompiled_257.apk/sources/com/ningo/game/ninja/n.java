package com.ningo.game.ninja;

import android.app.AlertDialog;
import com.google.ads.R;
import com.scoreloop.client.android.core.d.a;
import com.scoreloop.client.android.core.d.o;

final class n implements o {
    final /* synthetic */ GameoverActivity a;

    /* synthetic */ n(GameoverActivity gameoverActivity) {
        this(gameoverActivity, (byte) 0);
    }

    private n(GameoverActivity gameoverActivity, byte b) {
        this.a = gameoverActivity;
    }

    public final void a(a aVar) {
        GameoverActivity.a(this.a);
    }

    public final void a(Exception exc) {
        GameoverActivity.a(this.a);
        new AlertDialog.Builder(this.a).setTitle("Upload Failure!").setMessage("Network error, submit again?").setIcon((int) R.drawable.icon).setPositiveButton("Yes", new l(this)).setNeutralButton("Cancel", new k(this)).create().show();
    }
}
