package com.ningo.game.ninja.a;

public final class j {
    private boolean a = false;
    private int b = 1;
    private int c = 0;
    private int d = 3;
    private int e = 715;
    private int f = 0;
    private int g = 0;

    public final int a() {
        return this.c;
    }

    public final void a(int i) {
        this.c = i;
        if (i == 1) {
            this.e = 715;
        } else if (i == 2) {
            this.e = 715;
        } else {
            this.e = 250;
        }
    }

    public final void a(boolean z) {
        this.a = z;
        if (z) {
            this.b = 1;
            this.f = 0;
        }
    }

    public final int b() {
        return this.e;
    }

    public final boolean c() {
        return this.a;
    }

    public final boolean d() {
        int i = this.f;
        this.f = i + 1;
        if (i < this.e) {
            return true;
        }
        a(false);
        return false;
    }

    public final int e() {
        return this.f;
    }

    public final int f() {
        this.g++;
        if (this.g == this.d) {
            this.g = 0;
            switch (this.b) {
                case 1:
                    this.b = 2;
                    break;
                case 2:
                    this.b = 3;
                    break;
                case 3:
                    this.b = 4;
                    break;
                case 4:
                    this.b = 5;
                    break;
                case 5:
                    this.b = 6;
                    break;
                case 6:
                    this.b = 1;
                    break;
            }
        }
        return this.b;
    }
}
