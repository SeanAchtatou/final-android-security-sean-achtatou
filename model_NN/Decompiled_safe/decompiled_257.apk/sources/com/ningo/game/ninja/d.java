package com.ningo.game.ninja;

import android.widget.CompoundButton;

final class d implements CompoundButton.OnCheckedChangeListener {
    private /* synthetic */ Prefs a;

    d(Prefs prefs) {
        this.a = prefs;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (z) {
            this.a.a.edit().putBoolean("vibrate", true).commit();
        } else {
            this.a.a.edit().putBoolean("vibrate", false).commit();
        }
    }
}
