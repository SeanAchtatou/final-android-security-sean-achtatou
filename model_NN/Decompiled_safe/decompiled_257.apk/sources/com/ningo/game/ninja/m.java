package com.ningo.game.ninja;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class m extends GestureDetector.SimpleOnGestureListener {
    private /* synthetic */ AgileBuddyActivity a;

    m(AgileBuddyActivity agileBuddyActivity) {
        this.a = agileBuddyActivity;
    }

    public final boolean onDoubleTap(MotionEvent motionEvent) {
        return true;
    }

    public final boolean onDown(MotionEvent motionEvent) {
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ningo.game.ninja.AgileBuddyView.a(boolean, float):void
     arg types: [int, float]
     candidates:
      com.ningo.game.ninja.AgileBuddyView.a(com.ningo.game.ninja.AgileBuddyView, int):void
      com.ningo.game.ninja.AgileBuddyView.a(boolean, float):void */
    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (this.a.i) {
            return true;
        }
        this.a.a.a(false, motionEvent.getX() - motionEvent2.getX());
        return true;
    }

    public final void onLongPress(MotionEvent motionEvent) {
    }

    public final boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return true;
    }

    public final void onShowPress(MotionEvent motionEvent) {
    }

    public final boolean onSingleTapUp(MotionEvent motionEvent) {
        com.ningo.game.ninja.a.m.a(11);
        if (this.a.a.a()) {
            this.a.a(false);
            this.a.a.c();
        } else {
            this.a.a(true);
            this.a.a.b();
        }
        return true;
    }
}
