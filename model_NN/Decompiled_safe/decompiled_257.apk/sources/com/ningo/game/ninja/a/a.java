package com.ningo.game.ninja.a;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public final class a {
    public l a;
    public e b;
    public int c = 1;
    public LinkedList d;
    public d e;
    public f f;
    private n g;
    private h h;
    private int i = 0;
    private int j = 1;
    private int k = 8;
    private int l = 0;
    private Random m = new Random();
    private int n = 0;
    private k o;
    private j p;
    private int q;
    private int r;
    private int s;
    private int t = 1200;
    private int u = 0;
    private int v = -30;
    private int w = 0;
    private LinkedList x;
    private i y;

    public a(n nVar, int i2) {
        this.g = nVar;
        this.h = new h(nVar, nVar.d - 27, nVar.c - 6, nVar.d - 12);
        this.b = new e(nVar.c, nVar.d);
        this.b.a(2);
        this.a = new l(nVar.c, nVar.d);
        this.s = i2;
        this.o = new k((nVar.c - 32) / 2, (nVar.d * 3) / 4);
        this.r = 70;
        this.x = new LinkedList();
        this.x.add(new b((nVar.c - 100) / 2, nVar.d, 0, 1, 1, 0, 0));
        this.y = new i();
        this.p = new j();
        this.e = new d(this.g.d);
        this.d = new LinkedList();
        this.d.add(new g(210));
        this.d.add(new g(400));
        this.d.add(new g(550));
        this.d.add(new g(850));
        this.f = new f();
    }

    private void a(int i2) {
        if (i2 == 3) {
            if (this.q > 0) {
                this.o.e(5);
            } else if (this.q < 0) {
                this.o.e(4);
            } else {
                this.o.e(3);
            }
        } else if (this.q > 0) {
            this.o.e(2);
        } else if (this.q < 0) {
            this.o.e(1);
        } else {
            this.o.e(0);
        }
    }

    private void a(int i2, int i3, int i4) {
        if ((i2 == 1 || i2 == 2 || i2 == 6) && this.p.c() && this.p.a() == 3) {
            m();
        }
        if (i2 == 1) {
            this.p.a(1);
            this.p.a(true);
            this.b.a(6);
            m.a(12);
        } else if (i2 == 2) {
            if (this.p.c()) {
                this.p.a(false);
                this.b.a(2);
            }
            this.p.a(2);
            this.p.a(true);
            m.a(12);
            this.b.a(4);
        } else if (i2 == 3) {
            this.e.a(this.o.c() + 24);
            m.a(12);
        } else if (i2 == 4) {
            c(1.0f);
            this.a.a(i3 + 3, i4 - 10, 1);
        } else if (i2 == 5) {
            c(3.0f);
            this.a.a(i3 + 3, i4 - 10, 3);
        } else if (i2 == 6) {
            this.p.a(3);
            this.p.a(true);
            this.o.e(3);
            this.w = this.v;
            this.v = -90;
            this.b.a(9);
            m.a(12);
        }
        if (i2 == 1 || i2 == 2 || i2 == 4 || i2 == 5 || i2 == 6) {
            this.h.a(i2, i3, i4);
        }
    }

    private void c(float f2) {
        this.k = (int) (((float) this.k) + f2);
        if (this.k > 8) {
            this.k = 8;
        } else if (this.k < 0) {
            this.k = 0;
        }
        if (f2 < 0.0f) {
            this.h.a();
        }
    }

    private int k() {
        switch (this.m.nextInt(14)) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 5;
            case 6:
                return 6;
            default:
                return 0;
        }
    }

    private int l() {
        int nextInt = this.m.nextInt(4);
        if (nextInt == 0) {
            return 1;
        }
        if (nextInt == 1) {
            return 2;
        }
        return nextInt == 2 ? 3 : 6;
    }

    private void m() {
        if (this.w != 0) {
            this.v = this.w;
        }
    }

    public final void a() {
        int i2;
        int k2;
        int i3;
        int i4;
        int i5;
        int i6;
        boolean z;
        int nextInt;
        int i7;
        int i8;
        int i9;
        Iterator it = this.x.iterator();
        while (it.hasNext()) {
            b bVar = (b) it.next();
            bVar.a(this.v);
            int f2 = bVar.f();
            if (f2 == 6) {
                bVar.b(-1);
            } else if (f2 == 7) {
                bVar.b(1);
            }
        }
        this.f.g();
        Iterator it2 = this.d.iterator();
        while (it2.hasNext()) {
            g gVar = (g) it2.next();
            if (gVar.f() == 1) {
                gVar.a();
            }
        }
        if (this.e.c()) {
            this.e.d();
        }
        this.o.c(this.q);
        if (!this.p.c() || this.p.a() != 3) {
            this.o.d(this.r);
        }
        if (this.b != null) {
            this.b.f();
        }
        if (this.e.c()) {
            Iterator it3 = this.x.iterator();
            while (it3.hasNext()) {
                b bVar2 = (b) it3.next();
                if (bVar2.f() != 0 && bVar2.e() >= this.e.a() && bVar2.e() <= this.e.b()) {
                    bVar2.g();
                }
            }
            Iterator it4 = this.d.iterator();
            while (it4.hasNext()) {
                g gVar2 = (g) it4.next();
                if (gVar2.f() == 1 && gVar2.c > this.e.a() && gVar2.c() < this.e.b()) {
                    gVar2.b(0);
                }
            }
            int d2 = this.f.d();
            if (d2 == 2 || d2 == 3) {
                if (this.f.a() >= this.e.a()) {
                    this.f.e();
                    m.a(17);
                }
            } else if (d2 == 1) {
                this.f.f();
            }
        }
        if (this.p.c() && !this.p.d()) {
            this.p.a(false);
            m();
            this.b.a(2);
            m.a(13);
        }
        if (this.x.size() > 0 && ((b) this.x.getFirst()).e() <= this.g.b) {
            this.x.remove();
        }
        if (this.o.c() <= this.f.a()) {
            boolean z2 = true;
            if (!this.p.c() || this.p.a() == 2) {
                c(-1.0f);
            } else {
                z2 = false;
            }
            if (this.k <= 0) {
                this.c = 2;
            } else if (this.o.g() == 0 || this.o.g() == 1 || this.o.g() == 2) {
                this.o.d(200);
            }
            this.r = 70;
            if (z2) {
                m.a(4);
            }
        } else if (this.o.c() > this.g.d) {
            this.c = 2;
        } else if (this.o.a() < this.g.a) {
            this.q = 0;
            this.o.a(0);
        } else if (this.o.b() > this.g.c) {
            this.q = 0;
            this.o.a(this.g.c - this.o.e());
        }
        k kVar = this.o;
        if (!this.p.c() || this.p.a() != 3) {
            Iterator it5 = this.x.iterator();
            while (true) {
                if (!it5.hasNext()) {
                    if (this.r < this.v) {
                        this.r += 3;
                    } else {
                        this.r = 70;
                    }
                    a(3);
                } else {
                    b bVar3 = (b) it5.next();
                    if (kVar.d() >= bVar3.d() && kVar.d() < bVar3.e() && kVar.b() > bVar3.b() && kVar.a() < bVar3.c()) {
                        if (kVar.g() != 0 && kVar.g() != 2 && kVar.g() != 1) {
                            this.i += this.j;
                            this.r = this.v;
                            kVar.b(bVar3.a() - (kVar.f() * 10));
                            if (bVar3.f() == 3 && !this.p.c()) {
                                c(-1.0f);
                            }
                            if (this.k <= 0) {
                                this.c = 2;
                            }
                            a(0);
                            switch (bVar3.f()) {
                                case 1:
                                    i9 = 2;
                                    break;
                                case 2:
                                    i9 = 3;
                                    break;
                                case 3:
                                    if (this.p.c()) {
                                        i9 = 1;
                                        break;
                                    } else {
                                        i9 = 4;
                                        break;
                                    }
                                case 4:
                                case 5:
                                    i9 = 5;
                                    break;
                                default:
                                    i9 = 1;
                                    break;
                            }
                            m.a(i9);
                        } else if (bVar3.f() == 2) {
                            this.r = this.v - 70;
                            kVar.d(-10);
                            a(3);
                            bVar3.j();
                        } else {
                            if (bVar3.f() == 4) {
                                kVar.c(-4);
                            } else if (bVar3.f() == 5) {
                                kVar.c(4);
                            } else if (bVar3.f() == 1 && ((!this.p.c() || this.p.a() != 2) && bVar3.h())) {
                                this.x.remove(bVar3);
                            }
                            a(0);
                        }
                    }
                }
            }
        }
        i iVar = this.y;
        if (!this.p.c() || this.p.a() != 3 || this.e.c()) {
            iVar.b--;
            if (iVar.a != 0 && iVar.b > 0 && this.o.b() > iVar.c && this.o.a() < iVar.e && ((this.o.d() >= iVar.d && this.o.d() < iVar.f) || (this.o.c() > iVar.d && this.o.c() <= iVar.f))) {
                m.a(6);
                this.i += iVar.a * 1;
                a(iVar.a, iVar.c, iVar.d);
                iVar.a = 0;
            }
        } else {
            a(3, -1, -1);
        }
        Iterator it6 = this.d.iterator();
        while (it6.hasNext()) {
            g gVar3 = (g) it6.next();
            if (gVar3.f() == 1) {
                if (this.o.c() - 4 <= gVar3.c() && this.o.d() >= gVar3.c && this.o.b() + 4 > gVar3.b && this.o.a() - 4 < gVar3.b()) {
                    if (!this.p.c() || !(this.p.a() == 1 || this.p.a() == 3)) {
                        c(-2.0f);
                        this.o.a(this.o.a() - 20);
                        i8 = 4;
                    } else {
                        i8 = 16;
                    }
                    gVar3.b(0);
                    m.a(i8);
                } else if (gVar3.c() > this.g.d + 20 || gVar3.c < this.g.b - 20) {
                    gVar3.b(0);
                }
            }
        }
        if (this.h.c()) {
            this.h.d();
        }
        int f3 = f();
        boolean z3 = false;
        if (this.h.i() == 0 && f3 >= 500) {
            this.h.a(500);
            z3 = true;
        } else if (this.h.i() == 500 && f3 >= 1000) {
            this.h.a(1000);
            z3 = true;
        } else if (this.h.i() == 1000 && f3 >= 1500) {
            this.h.a(1500);
            z3 = true;
        } else if (this.h.i() == 1500 && f3 >= 2000) {
            this.h.a(2000);
            z3 = true;
        } else if (this.h.i() == 2000 && f3 >= 2500) {
            this.h.a(2500);
            z3 = true;
        } else if (this.h.i() == 2500 && f3 >= 3000) {
            this.h.a(3000);
            z3 = true;
        } else if (this.h.i() == 3000 && f3 >= 3500) {
            this.h.a(3500);
            z3 = true;
        } else if (this.h.i() == 3500 && f3 >= 4000) {
            this.h.a(4000);
            z3 = true;
        } else if (this.h.i() == 4500 && f3 >= 4500) {
            this.h.a(4500);
            z3 = true;
        } else if (this.h.i() == 4500 && f3 >= 5000) {
            this.h.a(5000);
            z3 = true;
        } else if (this.h.i() == 5000 && f3 >= 5500) {
            this.h.a(5500);
            z3 = true;
        }
        if (z3) {
            m.a(15);
        }
        if (!this.h.o()) {
            if (!this.h.j()) {
                if ((this.h.p() != 0 || f3 < 400) ? this.h.p() == 1 && f3 >= 1200 : true) {
                    this.h.a(true);
                }
            } else {
                this.h.l();
                if (this.o.d() > this.h.k()) {
                    this.h.a(false);
                    this.h.n();
                    m.a(18);
                }
            }
        }
        this.u -= this.v;
        if (this.u >= this.t) {
            this.u -= this.t;
            switch (this.m.nextInt(20)) {
                case 0:
                case 1:
                case 2:
                    i5 = 1;
                    k2 = 0;
                    i3 = 1;
                    i4 = 1;
                    break;
                case 3:
                case 4:
                case 5:
                    i5 = 2;
                    k2 = 0;
                    i3 = 1;
                    i4 = 1;
                    break;
                case 6:
                case 7:
                case 8:
                    i5 = 3;
                    k2 = 0;
                    i3 = 1;
                    i4 = 1;
                    break;
                case 9:
                case 10:
                    k2 = k();
                    i3 = 15;
                    i4 = 2;
                    i5 = 4;
                    break;
                case 11:
                case 12:
                    k2 = k();
                    i3 = 15;
                    i4 = 2;
                    i5 = 5;
                    break;
                case 13:
                case 14:
                    k2 = k();
                    i3 = 15;
                    i4 = 2;
                    i5 = 7;
                    break;
                case 15:
                case 16:
                    k2 = k();
                    i3 = 15;
                    i4 = 2;
                    i5 = 6;
                    break;
                default:
                    k2 = k();
                    i3 = 1;
                    i4 = 1;
                    i5 = 0;
                    break;
            }
            this.x.add(new b(this.m.nextInt(this.g.c - 100), this.g.d + 48, i5, i4, i3, k2, this.m.nextInt(60) + 5));
            if (this.y.b <= 0) {
                if (this.n <= 1) {
                    i6 = 6;
                    z = true;
                } else {
                    i6 = 13;
                    z = false;
                }
                boolean z4 = false;
                int f4 = f();
                if (this.y.g == 0 && f4 >= 130) {
                    z4 = true;
                } else if (this.y.g <= 1 && f4 >= 800) {
                    z4 = true;
                } else if (this.y.g <= 2 && f4 >= 1800) {
                    z4 = true;
                } else if (this.y.g <= 3 && f4 >= 2800) {
                    z4 = true;
                } else if (this.y.g <= 4 && f4 >= 3800) {
                    z4 = true;
                }
                if (!z4) {
                    switch (this.m.nextInt(i6)) {
                        case 0:
                        case 1:
                            this.y.a = 1;
                            break;
                        case 2:
                        case 3:
                            this.y.a = 2;
                            break;
                        case 4:
                        case 5:
                            this.y.a = 3;
                            break;
                        case 6:
                        case 7:
                        case 8:
                            if (this.k != 8) {
                                this.y.a = 4;
                                break;
                            } else {
                                this.y.a = l();
                                break;
                            }
                        case 9:
                        case 10:
                            if (this.k != 8 && this.k + 1 != 8) {
                                this.y.a = 5;
                                break;
                            } else {
                                this.y.a = l();
                                break;
                            }
                        case 11:
                            this.y.a = 6;
                            break;
                        case 12:
                            this.y.a = l();
                            break;
                        default:
                            this.y.a = 0;
                            break;
                    }
                } else {
                    this.y.a = 6;
                }
                if (this.y.a != 0) {
                    if (this.y.a == 6) {
                        this.y.g++;
                    }
                    if (this.y.a == 6) {
                        int nextInt2 = this.m.nextInt(80) + 100;
                        nextInt = this.m.nextInt(100) + 80;
                        i7 = nextInt2;
                    } else if (z) {
                        int nextInt3 = this.m.nextInt(160) + 100;
                        nextInt = this.m.nextInt(100) + 80;
                        i7 = nextInt3;
                    } else {
                        int nextInt4 = this.m.nextInt(this.g.d - 120) + 30;
                        nextInt = this.m.nextInt(this.g.c - 100) + 20;
                        i7 = nextInt4;
                    }
                    this.y.c = nextInt;
                    this.y.d = i7;
                    this.y.e = this.y.c + 24;
                    this.y.f = this.y.d + 24;
                    this.y.b = 450;
                    this.n++;
                }
            }
            this.l++;
            if (this.l == 40) {
                this.l = 0;
                this.j++;
                if (this.j < 18 || this.j % 20 == 0) {
                    this.v -= 2;
                    if (this.j >= 2 && this.s == 0) {
                        this.s = 1;
                    }
                    int g2 = this.o.g();
                    if (g2 == 0 || g2 == 2 || g2 == 1) {
                        this.r = this.v;
                    }
                }
            }
        }
        Iterator it7 = this.d.iterator();
        while (it7.hasNext()) {
            g gVar4 = (g) it7.next();
            int i10 = gVar4.a;
            gVar4.a = i10 - 1;
            if (i10 <= 0 && gVar4.a <= 0 && gVar4.f() == 0) {
                int e2 = gVar4.e();
                switch (this.m.nextInt(18)) {
                    case 0:
                    case 1:
                    case 2:
                        i2 = 1;
                        break;
                    case 3:
                    case 4:
                    case 5:
                        i2 = 2;
                        break;
                    case 6:
                    case 7:
                    case 8:
                        i2 = 3;
                        break;
                    case 9:
                    case 10:
                    case 11:
                        i2 = 4;
                        break;
                    default:
                        i2 = 0;
                        break;
                }
                if (i2 == 0) {
                    gVar4.a = 30;
                } else {
                    gVar4.c = 3;
                    gVar4.b = this.m.nextInt(260) + 30;
                    gVar4.a = e2 - (this.j * 20);
                    if (gVar4.a <= 60) {
                        gVar4.a = 60;
                    }
                    gVar4.a(i2);
                    gVar4.b(1);
                    m.a(14);
                }
            }
        }
        if (this.f.d() == 0 && this.f.c() >= 250) {
            boolean z5 = false;
            switch (this.m.nextInt(3)) {
                case 0:
                case 1:
                    z5 = true;
                    break;
                case 2:
                    z5 = false;
                    break;
            }
            if (!z5) {
                this.f.b();
                return;
            }
            int f5 = f();
            this.f.a(f5 < 500 ? this.m.nextInt(4) == 0 ? 120 : 70 : (f5 < 500 || f5 > 1500) ? (f5 < 1500 || f5 > 2500) ? this.m.nextInt(3) == 0 ? 130 : 175 : this.m.nextInt(4) == 0 ? 150 : 130 : this.m.nextInt(4) == 0 ? 130 : 100);
            m.a(17);
        }
    }

    public final void a(float f2) {
        if (f2 < -5.0f) {
            this.q = this.s + 10;
        } else if (f2 >= -5.0f && f2 < -4.0f) {
            this.q = this.s + 8;
        } else if (f2 >= -4.0f && f2 < -3.0f) {
            this.q = this.s + 6;
        } else if (f2 >= -3.0f && f2 < -2.0f) {
            this.q = this.s + 5;
        } else if (f2 >= -2.0f && ((double) f2) < -1.5d) {
            this.q = this.s + 4;
        } else if (((double) f2) >= -1.5d && ((double) f2) < 1.5d) {
            this.q = 0;
        } else if (((double) f2) >= 1.5d && f2 < 2.0f) {
            this.q = -4 - this.s;
        } else if (f2 >= 2.0f && f2 < 3.0f) {
            this.q = -5 - this.s;
        } else if (f2 >= 3.0f && f2 < 4.0f) {
            this.q = -6 - this.s;
        } else if (f2 >= 4.0f && f2 < 5.0f) {
            this.q = -8 - this.s;
        } else if (f2 > 5.0f) {
            this.q = -10 - this.s;
        }
    }

    public final void b() {
        this.g = null;
        this.o = null;
        this.m = null;
        this.x.clear();
        this.x = null;
    }

    public final void b(float f2) {
        if (f2 < -200.0f) {
            this.q = this.s + 10;
        } else if (f2 >= -200.0f && f2 < -140.0f) {
            this.q = this.s + 9;
        } else if (f2 >= -140.0f && f2 < -100.0f) {
            this.q = this.s + 8;
        } else if (f2 >= -100.0f && f2 < -60.0f) {
            this.q = this.s + 6;
        } else if (f2 >= -60.0f && f2 < -20.0f) {
            this.q = this.s + 4;
        } else if (f2 >= -20.0f && f2 <= 20.0f) {
            this.q = 0;
        } else if (f2 > 20.0f && f2 <= 60.0f) {
            this.q = -4 - this.s;
        } else if (f2 > 60.0f && f2 <= 100.0f) {
            this.q = -6 - this.s;
        } else if (f2 > 100.0f && f2 <= 140.0f) {
            this.q = -8 - this.s;
        } else if (f2 > 140.0f && f2 <= 200.0f) {
            this.q = -9 - this.s;
        } else if (f2 > 200.0f) {
            this.q = -10 - this.s;
        }
    }

    public final k c() {
        return this.o;
    }

    public final j d() {
        return this.p;
    }

    public final h e() {
        return this.h;
    }

    public final int f() {
        return this.b.g() / 20;
    }

    public final List g() {
        return this.x;
    }

    public final i h() {
        return this.y;
    }

    public final float i() {
        return ((float) this.k) / 8.0f;
    }

    public final float j() {
        if (!this.p.c()) {
            return -1.0f;
        }
        float e2 = 1.0f - ((((float) this.p.e()) * 1.0f) / ((float) this.p.b()));
        if (((double) e2) <= 0.0d) {
            return 0.0f;
        }
        return e2;
    }
}
