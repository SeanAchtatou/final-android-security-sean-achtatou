package com.scoreloop.android.coreui;

import android.view.View;
import android.widget.AdapterView;
import com.scoreloop.client.android.core.b.n;
import com.scoreloop.client.android.core.b.o;
import com.scoreloop.client.android.core.d.r;

final class h implements AdapterView.OnItemClickListener {
    private /* synthetic */ HighscoresActivity a;

    h(HighscoresActivity highscoresActivity) {
        this.a = highscoresActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, boolean):void
     arg types: [com.scoreloop.android.coreui.HighscoresActivity, int]
     candidates:
      com.scoreloop.android.coreui.HighscoresActivity.a(android.view.View, boolean):void
      com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, int):void
      com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, com.scoreloop.android.coreui.i):void
      com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, boolean):void */
    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (((n) adapterView.getItemAtPosition(i)) == null) {
            switch (i) {
                case 0:
                    this.a.c = i.OTHER;
                    this.a.f.a(new o(0, 20));
                    this.a.b(true);
                    this.a.a(true);
                    this.a.f.m();
                    return;
                case 1:
                    this.a.c = i.OTHER;
                    this.a.b(true);
                    this.a.a(true);
                    r b = this.a.f;
                    if (!b.l()) {
                        throw new IllegalStateException("There's no previous range");
                    }
                    o i2 = b.i();
                    int b2 = b.h().b();
                    i2.a(b2 > i2.a() ? b2 - i2.a() : 0);
                    b.m();
                    return;
                default:
                    this.a.c = i.OTHER;
                    this.a.b(true);
                    this.a.a(true);
                    r b3 = this.a.f;
                    if (!b3.k()) {
                        throw new IllegalStateException("There's no next range");
                    }
                    o i3 = b3.i();
                    i3.a(b3.h().b() + i3.a());
                    b3.m();
                    return;
            }
        }
    }
}
