package com.scoreloop.android.coreui;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.google.ads.R;
import com.scoreloop.client.android.core.b.h;
import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.d.u;

public class ProfileActivity extends j {
    /* access modifiers changed from: private */
    public EditText a;
    /* access modifiers changed from: private */
    public EditText b;
    private String c;
    private String d;
    private boolean e;
    private TextView f;
    /* access modifiers changed from: private */
    public u g;

    /* access modifiers changed from: private */
    public void a() {
        h f2 = k.a().f();
        this.d = f2.f();
        this.c = f2.d();
    }

    static /* synthetic */ void a(ProfileActivity profileActivity, int i, boolean z) {
        profileActivity.a(false);
        if (z) {
            profileActivity.b(false);
        }
        if (profileActivity.e) {
            profileActivity.showDialog(i);
        }
        h f2 = k.a().f();
        f2.c(profileActivity.d);
        f2.b(profileActivity.c);
        profileActivity.b();
    }

    /* access modifiers changed from: private */
    public void b() {
        h f2 = k.a().f();
        this.b.setText(f2.f());
        this.a.setText(f2.d());
        this.f.setText(f2.f());
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        ((Button) findViewById(R.id.update_button)).setEnabled(!z);
        ((EditText) findViewById(R.id.login)).setEnabled(!z);
        ((EditText) findViewById(R.id.email)).setEnabled(!z);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.sl_profile);
        this.g = new u(new c(this));
        this.b = (EditText) findViewById(R.id.login);
        this.a = (EditText) findViewById(R.id.email);
        this.f = (TextView) findViewById(R.id.title_login);
        if (k.a().g()) {
            this.f.setText(k.a().f().f());
        }
        ((Button) findViewById(R.id.update_button)).setOnClickListener(new b(this));
        a();
        b(true);
        a(true);
        this.g.h();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, (int) R.string.sl_highscores).setIcon((int) R.drawable.sl_menu_highscores);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        startActivity(new Intent(this, HighscoresActivity.class));
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.e = false;
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.e = true;
    }
}
