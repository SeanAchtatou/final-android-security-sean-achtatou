package com.scoreloop.android.coreui;

import android.content.Context;
import com.scoreloop.client.android.core.b.j;
import com.scoreloop.client.android.core.b.o;

public abstract class k {
    static j a;

    public static void a() {
        if (a != null) {
            a.a(new o(0, 2));
            return;
        }
        throw new IllegalStateException("client object is null. has ScoreloopManager.init() been called?");
    }

    public static void a(Context context, String str, String str2) {
        if (a == null) {
            a = new j(context, str, str2);
        }
    }
}
