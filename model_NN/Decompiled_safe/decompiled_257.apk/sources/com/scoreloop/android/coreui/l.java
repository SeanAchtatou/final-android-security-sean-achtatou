package com.scoreloop.android.coreui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.google.ads.R;
import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.b.n;
import java.util.List;

final class l extends ArrayAdapter {
    private /* synthetic */ HighscoresActivity a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(HighscoresActivity highscoresActivity, Context context, List list) {
        super(context, (int) R.layout.sl_highscores, list);
        this.a = highscoresActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.android.coreui.HighscoresActivity.a(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, int):void
      com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, com.scoreloop.android.coreui.i):void
      com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, boolean):void
      com.scoreloop.android.coreui.HighscoresActivity.a(android.view.View, boolean):void */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.a.getLayoutInflater().inflate((int) R.layout.sl_highscores_list_item, (ViewGroup) null) : view;
        n nVar = (n) getItem(i);
        int b = this.a.f.h().b();
        TextView textView = (TextView) inflate.findViewById(R.id.rank);
        TextView textView2 = (TextView) inflate.findViewById(R.id.login);
        TextView textView3 = (TextView) inflate.findViewById(R.id.score);
        if (nVar == null) {
            switch (i) {
                case 0:
                    textView.setText("", (TextView.BufferType) null);
                    textView2.setText((int) R.string.sl_top, (TextView.BufferType) null);
                    textView3.setText("", (TextView.BufferType) null);
                    break;
                case 1:
                    textView.setText("", (TextView.BufferType) null);
                    textView2.setText((int) R.string.sl_prev, (TextView.BufferType) null);
                    textView3.setText("", (TextView.BufferType) null);
                    break;
                default:
                    textView.setText("", (TextView.BufferType) null);
                    textView2.setText((int) R.string.sl_next, (TextView.BufferType) null);
                    textView3.setText("", (TextView.BufferType) null);
                    break;
            }
        } else {
            textView.setText(new StringBuilder().append(b + i + 1 + this.a.e).toString(), (TextView.BufferType) null);
            textView2.setText(nVar.e().f(), (TextView.BufferType) null);
            textView3.setText(new StringBuilder().append(nVar.d().intValue()).toString(), (TextView.BufferType) null);
        }
        if (nVar != null) {
            HighscoresActivity.b(inflate, nVar.e().equals(k.a().f()));
        } else {
            HighscoresActivity.b(inflate, false);
        }
        return inflate;
    }
}
