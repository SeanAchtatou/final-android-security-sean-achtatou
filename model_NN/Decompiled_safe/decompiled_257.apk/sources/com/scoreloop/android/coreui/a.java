package com.scoreloop.android.coreui;

import android.widget.ListAdapter;
import android.widget.TextView;
import com.google.ads.R;
import com.scoreloop.client.android.core.b.h;
import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.b.n;
import com.scoreloop.client.android.core.d.o;
import com.scoreloop.client.android.core.d.r;
import java.util.Iterator;
import java.util.List;

final class a implements o {
    private /* synthetic */ HighscoresActivity a;

    /* synthetic */ a(HighscoresActivity highscoresActivity) {
        this(highscoresActivity, (byte) 0);
    }

    private a(HighscoresActivity highscoresActivity, byte b) {
        this.a = highscoresActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, boolean):void
     arg types: [com.scoreloop.android.coreui.HighscoresActivity, int]
     candidates:
      com.scoreloop.android.coreui.HighscoresActivity.a(android.view.View, boolean):void
      com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, int):void
      com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, com.scoreloop.android.coreui.i):void
      com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, boolean):void */
    public final void a(com.scoreloop.client.android.core.d.a aVar) {
        n nVar;
        List j = this.a.f.j();
        this.a.a.setAdapter((ListAdapter) new l(this.a, this.a, j));
        if (this.a.f.l()) {
            j.add(0, null);
            j.add(0, null);
            this.a.e = -2;
        } else {
            this.a.e = 0;
        }
        if (this.a.f.k()) {
            j.add(null);
        }
        h f = k.a().f();
        Iterator it = j.iterator();
        int i = 0;
        while (true) {
            if (it.hasNext()) {
                n nVar2 = (n) it.next();
                if (nVar2 != null && nVar2.e().equals(f)) {
                    nVar = nVar2;
                    break;
                }
                i++;
            } else {
                nVar = null;
                break;
            }
        }
        if (this.a.c == i.ME) {
            if (nVar != null) {
                this.a.a.setSelection(i < 3 ? 0 : i - 3);
            } else {
                this.a.showDialog(1);
            }
        }
        if (nVar != null) {
            ((TextView) this.a.d.findViewById(R.id.rank)).setText(new StringBuilder().append(i + ((r) aVar).h().b() + 1 + this.a.e).toString());
            ((TextView) this.a.d.findViewById(R.id.login)).setText(nVar.e().f());
            ((TextView) this.a.d.findViewById(R.id.score)).setText(new StringBuilder().append(nVar.d().intValue()).toString());
            this.a.d.setVisibility(8);
        } else if (((TextView) this.a.d.findViewById(R.id.rank)).getText().toString().equals("")) {
            this.a.d.setVisibility(8);
        } else {
            this.a.d.setVisibility(0);
        }
        this.a.b = true;
        this.a.a(false);
        this.a.b(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, boolean):void
     arg types: [com.scoreloop.android.coreui.HighscoresActivity, int]
     candidates:
      com.scoreloop.android.coreui.HighscoresActivity.a(android.view.View, boolean):void
      com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, int):void
      com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, com.scoreloop.android.coreui.i):void
      com.scoreloop.android.coreui.HighscoresActivity.a(com.scoreloop.android.coreui.HighscoresActivity, boolean):void */
    public final void a(Exception exc) {
        this.a.b = true;
        this.a.showDialog(3);
        this.a.a(false);
        this.a.b(false);
    }
}
