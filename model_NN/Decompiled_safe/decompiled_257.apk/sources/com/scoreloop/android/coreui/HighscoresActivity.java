package com.scoreloop.android.coreui;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.google.ads.R;
import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.b.o;
import com.scoreloop.client.android.core.b.t;
import com.scoreloop.client.android.core.d.r;
import com.scoreloop.client.android.core.d.u;
import java.util.List;

public class HighscoresActivity extends j {
    /* access modifiers changed from: private */
    public ListView a;
    /* access modifiers changed from: private */
    public boolean b;
    /* access modifiers changed from: private */
    public i c;
    /* access modifiers changed from: private */
    public LinearLayout d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public r f;
    private boolean g;
    /* access modifiers changed from: private */
    public TextView h;

    /* access modifiers changed from: private */
    public void a() {
        startActivity(new Intent(this, ProfileActivity.class));
        finish();
    }

    /* access modifiers changed from: private */
    public void b() {
        List i = k.a().i();
        i.remove(t.a());
        new ArrayAdapter(this, (int) R.layout.sl_spinner_item, i).setDropDownViewResource(17367049);
        this.f.a(new o(0, 20));
        b(true);
        a(true);
        c(true);
    }

    /* access modifiers changed from: private */
    public static void b(View view, boolean z) {
        int i = z ? -13131824 : -1;
        ((TextView) view.findViewById(R.id.rank)).setTextColor(i);
        ((TextView) view.findViewById(R.id.login)).setTextColor(i);
        ((TextView) view.findViewById(R.id.score)).setTextColor(i);
    }

    static /* synthetic */ void b(HighscoresActivity highscoresActivity, int i) {
        highscoresActivity.a(false);
        if (highscoresActivity.g) {
            highscoresActivity.showDialog(i);
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        ((Spinner) findViewById(R.id.game_mode_spinner)).setEnabled(!z);
        ((ListView) findViewById(R.id.list_view)).setEnabled(!z);
        ((LinearLayout) findViewById(R.id.myscore_view)).setEnabled(!z);
    }

    /* access modifiers changed from: private */
    public void c(boolean z) {
        if (z || this.b) {
            this.c = i.ME;
            b(true);
            a(true);
            this.f.a(k.a().f());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.android.coreui.HighscoresActivity.b(android.view.View, boolean):void
     arg types: [android.widget.LinearLayout, int]
     candidates:
      com.scoreloop.android.coreui.HighscoresActivity.b(com.scoreloop.android.coreui.HighscoresActivity, int):void
      com.scoreloop.android.coreui.HighscoresActivity.b(android.view.View, boolean):void */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.sl_highscores);
        this.f = new r(new a(this));
        this.h = (TextView) findViewById(R.id.title_login);
        if (k.a().g()) {
            this.h.setText(k.a().f().f());
        }
        this.d = (LinearLayout) findViewById(R.id.myscore_view);
        this.d.setVisibility(8);
        ((TextView) this.d.findViewById(R.id.rank)).setText("");
        b((View) this.d, true);
        ((Button) findViewById(R.id.btn_profile)).setOnClickListener(new f(this));
        this.d.setOnClickListener(new d(this));
        this.a = (ListView) findViewById(R.id.list_view);
        this.a.setOnItemClickListener(new h(this));
        ArrayAdapter<CharSequence> createFromResource = ArrayAdapter.createFromResource(this, R.array.sl_game_modes, R.layout.sl_spinner_item);
        createFromResource.setDropDownViewResource(17367049);
        Spinner spinner = (Spinner) findViewById(R.id.game_mode_spinner);
        spinner.setAdapter((SpinnerAdapter) createFromResource);
        if (k.a.a().a() > 1) {
            spinner.setOnItemSelectedListener(new g(this));
        } else {
            spinner.setVisibility(8);
        }
        if (!k.a().g()) {
            b(true);
            a(true);
            new u(new e(this)).h();
            return;
        }
        b();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, (int) R.string.sl_profile).setIcon((int) R.drawable.sl_menu_profile);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        a();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.g = false;
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.g = true;
    }
}
