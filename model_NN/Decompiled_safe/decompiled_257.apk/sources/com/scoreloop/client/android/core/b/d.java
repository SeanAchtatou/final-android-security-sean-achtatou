package com.scoreloop.client.android.core.b;

import com.scoreloop.client.android.core.a.a;
import com.scoreloop.client.android.core.a.b;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class d {
    private Date a;
    private h b;
    private n c;
    private Integer d;
    private h e;
    private n f;
    private Integer g;
    private Map h;
    private Date i;
    private String j;
    private String k;
    private Integer l;
    private h m;
    private Integer n;
    private a o;
    private a p;
    private a q;
    private a r;
    private a s;
    private String t;
    private h u;

    private boolean j() {
        return "open".equalsIgnoreCase(this.t);
    }

    public final h a() {
        return this.b;
    }

    public final void a(n nVar) {
        if (this.b == null || this.b.equals(nVar.e())) {
            if (j()) {
                throw new IllegalStateException("Can not modify a already open challenge");
            } else if (f()) {
                throw new IllegalStateException("Can not modify a already open challenge");
            } else {
                this.b = nVar.e();
                this.c = nVar;
                this.t = "open";
            }
        } else if (this.e != null && !this.e.equals(nVar.e())) {
            throw new IllegalStateException("Can not change already assigned contender or contestant");
        } else if (j() || "accepted".equalsIgnoreCase(this.t)) {
            this.e = nVar.e();
            this.f = nVar;
            this.t = "complete";
        } else {
            throw new IllegalStateException("Can not submit a score for a non-open challenge");
        }
    }

    public final void a(JSONObject jSONObject) {
        if (jSONObject.has("id")) {
            this.k = jSONObject.getString("id");
        }
        if (jSONObject.has("state")) {
            this.t = jSONObject.getString("state");
        }
        if (jSONObject.has("level")) {
            this.l = Integer.valueOf(jSONObject.getInt("level"));
        }
        if (jSONObject.has("mode")) {
            this.n = Integer.valueOf(jSONObject.getInt("mode"));
        }
        if (jSONObject.has("game_id")) {
            this.j = jSONObject.getString("game_id");
        }
        if (jSONObject.has("contender_id")) {
            this.b = new h();
            this.b.f(jSONObject.getString("contender_id"));
        }
        if (jSONObject.has("contender")) {
            this.b = new h(jSONObject.getJSONObject("contender"));
        }
        if (jSONObject.has("contestant_id")) {
            this.e = new h();
            this.e.f(jSONObject.getString("contestant_id"));
        }
        if (jSONObject.has("contestant")) {
            this.e = new h(jSONObject.getJSONObject("contestant"));
        }
        if (jSONObject.has("winner")) {
            if (this.b == null || this.e == null) {
                throw new JSONException("winner present but missing contender or contestant");
            } else if (jSONObject.getJSONObject("winner").getString("id").equals(this.b.e())) {
                this.u = this.b;
                this.m = this.e;
            } else {
                this.u = this.e;
                this.m = this.b;
            }
        }
        if (jSONObject.has("contender_score")) {
            this.c = new n(jSONObject.getJSONObject("contender_score"));
        }
        if (jSONObject.has("contestant_score")) {
            this.f = new n(jSONObject.getJSONObject("contestant_score"));
        }
        if (jSONObject.has("contender_skill_value")) {
            this.d = Integer.valueOf(jSONObject.getInt("contender_skill_value"));
        }
        if (jSONObject.has("contestant_skill_value")) {
            this.g = Integer.valueOf(jSONObject.getInt("contestant_skill_value"));
        }
        if (jSONObject.has("stake")) {
            this.q = new a(jSONObject.getJSONObject("stake"));
        }
        if (jSONObject.has("price")) {
            this.o = new a(jSONObject.getJSONObject("price"));
        }
        if (jSONObject.has("stake_in_local_currency")) {
            this.s = new a(jSONObject.getJSONObject("stake_in_local_currency"));
        } else if (this.q != null) {
            this.s = this.q.clone();
        } else {
            this.q = null;
        }
        if (jSONObject.has("stake_in_contestant_currency")) {
            this.r = new a(jSONObject.getJSONObject("stake_in_contestant_currency"));
        }
        if (jSONObject.has("price_in_contestant_currency")) {
            this.p = new a(jSONObject.getJSONObject("price_in_contestant_currency"));
        }
        if (jSONObject.has("created_at")) {
            try {
                this.i = a.a.parse(jSONObject.getString("created_at"));
            } catch (ParseException e2) {
                throw new JSONException("Invalid format of creation date");
            }
        }
        if (jSONObject.has("completed_at")) {
            try {
                this.a = a.a.parse(jSONObject.getString("completed_at"));
            } catch (ParseException e3) {
                throw new JSONException("Invalid format of completion date");
            }
        }
        if (jSONObject.has("context") && !jSONObject.isNull("context")) {
            this.h = b.a(jSONObject.getJSONObject("context"));
        }
    }

    public final n b() {
        return this.c;
    }

    public final h c() {
        return this.e;
    }

    public final String d() {
        return this.k;
    }

    public final Integer e() {
        return this.n;
    }

    public final boolean f() {
        return "complete".equalsIgnoreCase(this.t);
    }

    public final boolean g() {
        return "created".equalsIgnoreCase(this.t);
    }

    public final boolean h() {
        return "rejected".equalsIgnoreCase(this.t);
    }

    public final JSONObject i() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("id", this.k);
        jSONObject.put("state", this.t);
        jSONObject.put("level", this.l);
        jSONObject.put("mode", this.n);
        jSONObject.put("game_id", this.j);
        if (this.b != null) {
            jSONObject.put("contender_id", this.b.e());
        }
        if (this.u != null) {
            jSONObject.put("winner_id", this.u.e());
        }
        if (this.m != null) {
            jSONObject.put("looser_id", this.m.e());
        }
        if (this.q != null) {
            jSONObject.put("stake", this.q.b());
        }
        if (this.e != null) {
            String e2 = this.e.e();
            if (e2 != null) {
                jSONObject.put("contestant_id", e2);
            } else {
                jSONObject.put("contestant", this.e.i());
            }
        }
        if (this.f != null) {
            String a2 = this.f.a();
            if (a2 != null) {
                jSONObject.put("contestant_score_id", a2);
            } else {
                jSONObject.put("contestant_score", this.f.f());
            }
        }
        if (this.c != null) {
            String a3 = this.c.a();
            if (a3 != null) {
                jSONObject.put("contender_score_id", a3);
            } else {
                jSONObject.put("contender_score", this.c.f());
            }
        }
        if (this.h != null) {
            jSONObject.put("context", b.a(this.h));
        }
        return jSONObject;
    }
}
