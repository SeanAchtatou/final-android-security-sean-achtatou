package com.scoreloop.client.android.core.c;

import org.apache.http.client.methods.HttpPost;
import org.json.JSONObject;

final class c {
    protected int a;
    protected String b;
    protected JSONObject c;
    protected JSONObject d;
    private HttpPost e;
    private /* synthetic */ j f;

    /* synthetic */ c(j jVar) {
        this(jVar, (byte) 0);
    }

    private c(j jVar, byte b2) {
        this.f = jVar;
        this.e = null;
    }

    /* access modifiers changed from: private */
    public HttpPost b() {
        HttpPost httpPost;
        synchronized (this) {
            this.e = this.f.c.a();
            httpPost = this.e;
        }
        return httpPost;
    }

    public final void a() {
        synchronized (this) {
            if (this.e != null) {
                this.e.abort();
            }
        }
    }

    public final void a(int i, String str, JSONObject jSONObject, JSONObject jSONObject2) {
        this.a = i;
        this.b = str;
        this.c = jSONObject;
        this.d = jSONObject2;
    }
}
