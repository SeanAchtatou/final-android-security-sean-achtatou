package com.scoreloop.client.android.core.d;

import android.os.Handler;
import android.os.Message;

final class i extends Handler {
    private c a;

    public i(c cVar) {
        this.a = cVar;
    }

    public final void handleMessage(Message message) {
        b bVar = (b) message.obj;
        switch (message.what) {
            case 1:
                bVar.a();
                return;
            case 2:
                bVar.b();
                return;
            case 3:
                bVar.c();
                return;
            default:
                return;
        }
    }
}
