package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.b.q;
import com.scoreloop.client.android.core.c.m;
import com.scoreloop.client.android.core.c.n;
import com.scoreloop.client.android.core.c.p;
import org.json.JSONException;
import org.json.JSONObject;

final class x extends m {
    private final com.scoreloop.client.android.core.b.m a;
    private final q b;

    public x(p pVar, q qVar, com.scoreloop.client.android.core.b.m mVar) {
        super(pVar);
        this.b = qVar;
        this.a = mVar;
    }

    public final String a() {
        if (this.b == null) {
            return "/service/session";
        }
        return String.format("/service/games/%s/session", this.b.a());
    }

    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.put("device_id", this.a.a());
            jSONObject.put("user", jSONObject2);
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException("Invalid device data");
        }
    }

    public final n c() {
        return n.POST;
    }
}
