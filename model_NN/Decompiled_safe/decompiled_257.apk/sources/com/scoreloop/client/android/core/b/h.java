package com.scoreloop.client.android.core.b;

import com.scoreloop.client.android.core.a.a;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class h {
    private static Map a;
    private List A;
    private Map B;
    private e b;
    private boolean c;
    private String d;
    private String e;
    private String f;
    private Integer g;
    private String h;
    private List i;
    private Map j;
    private String k;
    private i l;
    private Date m;
    private String n;
    private String o;
    private u p;
    private String q;
    private e r;
    private Date s;
    private String t;
    private String u;
    private Double v;
    private Integer w;
    private f x;
    private e y;
    private List z;

    static {
        HashMap hashMap = new HashMap();
        a = hashMap;
        hashMap.put("anonymous", f.anonymous);
        a.put("active", f.active);
        a.put("deleted", f.deleted);
        a.put("passive", f.passive);
        a.put("pending", f.pending);
        a.put("suspended", f.suspended);
    }

    public h() {
        this.p = u.UNKNOWN;
        this.B = new HashMap();
        this.l = new i(this);
    }

    public h(JSONObject jSONObject) {
        this();
        a(jSONObject);
    }

    private boolean k() {
        return k.a().f().equals(this);
    }

    public final a a() {
        return (this.g == null || this.h == null) ? new a("SLD", new BigDecimal(0)) : new a(this.h, new BigDecimal(this.g.intValue()));
    }

    public final void a(String str) {
        this.n = str;
    }

    public final void a(Map map) {
        this.j = map;
    }

    public final void a(JSONObject jSONObject) {
        if (jSONObject.has("id")) {
            this.q = jSONObject.getString("id");
        }
        if (jSONObject.has("login")) {
            this.t = jSONObject.optString("login");
        }
        if (jSONObject.has("email")) {
            this.o = jSONObject.optString("email");
        }
        if (jSONObject.has("state")) {
            String lowerCase = jSONObject.optString("state").toLowerCase();
            if (!a.containsKey(lowerCase)) {
                throw new IllegalStateException("could not parse json representation of User due to unknown state given: '" + lowerCase + "'");
            }
            this.x = (f) a.get(lowerCase);
        }
        if (jSONObject.has("device_id")) {
            this.n = jSONObject.optString("device_id");
        }
        if (jSONObject.has("gender")) {
            String string = jSONObject.getString("gender");
            if ("m".equalsIgnoreCase(string)) {
                this.p = u.MALE;
            } else if ("f".equalsIgnoreCase(string)) {
                this.p = u.FEMALE;
            } else {
                this.p = u.UNKNOWN;
            }
        }
        if (jSONObject.has("date_of_birth")) {
            if (jSONObject.isNull("date_of_birth")) {
                this.m = null;
            } else {
                try {
                    this.m = a.b.parse(jSONObject.getString("date_of_birth"));
                } catch (ParseException e2) {
                    throw new JSONException("Invalid format of the birth date");
                }
            }
        }
        JSONObject optJSONObject = jSONObject.optJSONObject("balance");
        if (optJSONObject != null) {
            this.g = Integer.valueOf(optJSONObject.getInt("amount"));
            this.h = optJSONObject.getString("currency");
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject("avatar");
        if (optJSONObject2 != null) {
            this.f = optJSONObject2.getString("head");
            this.e = optJSONObject2.getString("hair");
            this.d = optJSONObject2.getString("body");
        }
        c.b(this, jSONObject);
        JSONObject optJSONObject3 = jSONObject.optJSONObject("skill");
        if (optJSONObject3 != null) {
            this.w = Integer.valueOf(optJSONObject3.getInt("value"));
            this.v = Double.valueOf(optJSONObject3.getDouble("deviation"));
        }
        JSONObject optJSONObject4 = jSONObject.optJSONObject("agility");
        if (optJSONObject4 != null) {
            this.b = new e(optJSONObject4);
        }
        JSONObject optJSONObject5 = jSONObject.optJSONObject("strategy");
        if (optJSONObject5 != null) {
            this.y = new e(optJSONObject5);
        }
        JSONObject optJSONObject6 = jSONObject.optJSONObject("knowledge");
        if (optJSONObject6 != null) {
            this.r = new e(optJSONObject6);
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("score_lists");
        if (optJSONArray != null) {
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                arrayList.add(new t(optJSONArray.getJSONObject(i2)));
            }
            this.z = arrayList;
        }
        JSONArray optJSONArray2 = jSONObject.optJSONArray("challenge_lists");
        if (optJSONArray2 != null) {
            ArrayList arrayList2 = new ArrayList();
            for (int i3 = 0; i3 < optJSONArray2.length(); i3++) {
                arrayList2.add(new t(optJSONArray2.getJSONObject(i3)));
            }
            this.A = arrayList2;
        }
        if (jSONObject.has("last_active_at")) {
            try {
                this.s = a.a.parse(jSONObject.getString("last_active_at"));
            } catch (ParseException e3) {
                throw new JSONException("Invalid format of the 'last active at' date");
            }
        }
        JSONArray optJSONArray3 = jSONObject.optJSONArray("buddies");
        if (optJSONArray3 != null) {
            ArrayList arrayList3 = new ArrayList();
            for (int i4 = 0; i4 < optJSONArray3.length(); i4++) {
                arrayList3.add(new h(optJSONArray3.getJSONObject(i4)));
            }
            this.i = arrayList3;
        }
        this.l.a(jSONObject);
    }

    public final void a(JSONObject jSONObject, String str) {
        this.B.put(str, jSONObject);
    }

    public final Map b() {
        return this.j;
    }

    public final void b(String str) {
        if (k()) {
            this.o = str;
        }
    }

    public final String c() {
        return this.n;
    }

    public final void c(String str) {
        this.t = str;
    }

    public final String d() {
        if (!k()) {
            return null;
        }
        return this.o;
    }

    public final boolean d(String str) {
        return this.B.containsKey(str);
    }

    public final String e() {
        return this.q;
    }

    public final JSONObject e(String str) {
        return (JSONObject) this.B.get(str);
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof h)) {
            return super.equals(obj);
        }
        h hVar = (h) obj;
        if (this.q != null && hVar.q != null) {
            return this.q.equalsIgnoreCase(hVar.q);
        }
        String str = this.t;
        String str2 = hVar.t;
        return (str == null || str2 == null || !str.equalsIgnoreCase(str2)) ? false : true;
    }

    public final String f() {
        return this.t;
    }

    public final void f(String str) {
        this.q = str;
    }

    /* access modifiers changed from: package-private */
    public final List g() {
        return this.z;
    }

    public final void g(String str) {
        this.k = str;
    }

    public final void h() {
        this.c = true;
    }

    public final int hashCode() {
        return this.t == null ? "".hashCode() : this.t.hashCode();
    }

    public final JSONObject i() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("id", this.q);
        jSONObject.put("login", this.t);
        jSONObject.put("device_id", this.n);
        jSONObject.put("password", this.u);
        jSONObject.put("password_confirmation", this.u);
        jSONObject.put("email", this.o);
        if (this.p != u.UNKNOWN) {
            jSONObject.put("gender", this.p.a());
        }
        if (this.m != null) {
            jSONObject.put("date_of_birth", a.b.format(this.m));
        }
        if (!(this.d == null && this.e == null && this.d == null)) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject.put("avatar", jSONObject2);
            jSONObject2.put("hair", this.e);
            jSONObject2.put("head", this.f);
            jSONObject2.put("body", this.d);
        }
        c.a(this, jSONObject);
        return jSONObject;
    }

    public final JSONObject j() {
        return (JSONObject) this.B.get("facebook");
    }

    public final String toString() {
        return this.t == null ? this.q == null ? "[empty user]" : this.q : this.t;
    }
}
