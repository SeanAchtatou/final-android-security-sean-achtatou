package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.a.b;
import com.scoreloop.client.android.core.b.h;
import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.c.m;
import com.scoreloop.client.android.core.c.n;
import com.scoreloop.client.android.core.c.o;
import java.util.HashMap;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

public final class u extends a {
    private static final Pattern b = Pattern.compile(".*taken.*", 2);
    private h c;

    private u(k kVar, j jVar) {
        super(kVar, jVar);
    }

    public u(j jVar) {
        this(k.a(), jVar);
    }

    private h i() {
        if (this.c == null) {
            this.c = super.e();
            if (this.c == null) {
                throw new IllegalStateException("No session user assigned");
            }
        }
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(m mVar, o oVar) {
        int d = oVar.d();
        n c2 = mVar.c();
        JSONObject c3 = oVar.c();
        switch (d) {
            case 200:
            case 201:
                switch (w.a[c2.ordinal()]) {
                    case 1:
                    case 2:
                    case 3:
                        if (!c3.has("user") && !c3.has("context")) {
                            throw new Exception("Request failed");
                        }
                    case 4:
                        if (!c3.has("buddy_id")) {
                            throw new Exception("Request failed");
                        }
                        break;
                }
                if (c2 != n.DELETE) {
                    h i = i();
                    if (c3.has("user")) {
                        i.a(c3.getJSONObject("user"));
                    }
                    if (c3.has("context")) {
                        i.a(b.a(c3.getJSONObject("context")));
                        i.g(c3.getString("version"));
                    }
                }
                return true;
            case 409:
                h i2 = i();
                HashMap hashMap = new HashMap();
                hashMap.put("oldUserContext", new HashMap(i2.b()));
                i2.a(b.a(c3.getJSONObject("context")));
                i2.g(c3.getString("version"));
                hashMap.put("newUserContext", new HashMap(i2.b()));
                throw new Exception("Request failed");
            case 422:
                if (c2 == n.PUT || c2 == n.POST) {
                    JSONObject jSONObject = c3.getJSONObject("error");
                    JSONArray jSONArray = jSONObject.get("args") instanceof JSONArray ? jSONObject.getJSONArray("args") : null;
                    int length = jSONArray == null ? 0 : jSONArray.length();
                    boolean z = false;
                    boolean z2 = false;
                    for (int i3 = 0; i3 < length; i3++) {
                        JSONArray jSONArray2 = jSONArray.getJSONArray(i3);
                        String string = jSONArray2.getString(0);
                        String string2 = jSONArray2.getString(1);
                        if ("email".equalsIgnoreCase(string.trim())) {
                            if (b.matcher(string2).matches()) {
                                z = true;
                                z2 = true;
                            } else {
                                z = true;
                            }
                        }
                    }
                    if (!z) {
                        ((j) b()).c();
                        return false;
                    } else if (z2) {
                        ((j) b()).a();
                        return false;
                    } else {
                        ((j) b()).b();
                        return false;
                    }
                } else {
                    throw new Exception("Request failed");
                }
            default:
                throw new Exception("Request failed");
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean f() {
        return true;
    }

    public final void h() {
        y yVar = new y(c(), a(), i());
        g();
        a(yVar);
    }
}
