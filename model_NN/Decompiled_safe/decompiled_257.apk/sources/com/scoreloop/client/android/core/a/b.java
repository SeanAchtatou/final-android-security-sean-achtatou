package com.scoreloop.client.android.core.a;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class b {
    private static Object a(Object obj) {
        if (JSONObject.NULL.equals(obj)) {
            return null;
        }
        if (!(obj instanceof JSONArray)) {
            return obj instanceof JSONObject ? b((JSONObject) obj) : obj;
        }
        JSONArray jSONArray = (JSONArray) obj;
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(a(jSONArray.opt(i)));
        }
        return arrayList;
    }

    public static Map a(JSONObject jSONObject) {
        return b(jSONObject);
    }

    public static JSONObject a(Map map) {
        return new JSONObject(map);
    }

    private static Map b(JSONObject jSONObject) {
        HashMap hashMap = new HashMap();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            hashMap.put(next, a(jSONObject.opt(next)));
        }
        return hashMap;
    }
}
