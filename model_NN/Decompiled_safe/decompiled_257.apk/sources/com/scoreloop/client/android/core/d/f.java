package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.b.h;
import com.scoreloop.client.android.core.b.q;
import com.scoreloop.client.android.core.c.m;
import com.scoreloop.client.android.core.c.n;
import com.scoreloop.client.android.core.c.p;
import org.json.JSONObject;

class f extends m {
    protected h a;
    private q b;

    public f(p pVar, q qVar, h hVar) {
        super(pVar);
        this.a = hVar;
        this.b = qVar;
    }

    public final String a() {
        if (this.b == null || this.b.a() == null) {
            return String.format("/service/users/%s", this.a.e());
        }
        return String.format("/service/games/%s/users/%s", this.b.a(), this.a.e());
    }

    public JSONObject b() {
        return null;
    }

    public n c() {
        return n.GET;
    }
}
