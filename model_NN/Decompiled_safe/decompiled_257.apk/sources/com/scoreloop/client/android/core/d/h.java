package com.scoreloop.client.android.core.d;

enum h {
    CREATE(21),
    RESET(22),
    UPDATE(23),
    VERIFY(20);
    
    private final int e;

    private h(int i) {
        this.e = i;
    }

    public static h[] a() {
        return (h[]) f.clone();
    }
}
