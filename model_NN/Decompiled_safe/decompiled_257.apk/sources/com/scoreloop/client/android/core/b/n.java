package com.scoreloop.client.android.core.b;

import com.scoreloop.client.android.core.a.a;
import com.scoreloop.client.android.core.a.b;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class n {
    private String a;
    private String b;
    private Integer c;
    private Double d;
    private Integer e;
    private Double f;
    private Integer g;
    private Date h;
    private h i;
    private Map j;

    public n(Double d2, h hVar) {
        this.f = d2;
        this.i = hVar;
        this.a = hVar.c();
    }

    public n(JSONObject jSONObject) {
        a(jSONObject);
    }

    public final String a() {
        return this.b;
    }

    public final void a(h hVar) {
        this.i = hVar;
    }

    public final void a(Integer num) {
        this.e = num;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final void a(JSONObject jSONObject) {
        if (jSONObject.has("id")) {
            this.b = jSONObject.getString("id");
        }
        if (jSONObject.has("device_id")) {
            this.a = jSONObject.getString("device_id");
        }
        if (jSONObject.has("result")) {
            this.f = Double.valueOf(jSONObject.getDouble("result"));
        }
        if (jSONObject.has("minor_result")) {
            this.d = Double.valueOf(jSONObject.getDouble("minor_result"));
        }
        if (jSONObject.has("level")) {
            this.c = Integer.valueOf(jSONObject.getInt("level"));
        }
        if (jSONObject.has("minor_result")) {
            this.e = Integer.valueOf(jSONObject.getInt("mode"));
        }
        if (jSONObject.has("updated_at")) {
            try {
                this.h = a.a.parse(jSONObject.getString("updated_at"));
            } catch (ParseException e2) {
                throw new JSONException("Invalid format of the update date");
            }
        }
        if (jSONObject.has("user")) {
            this.i = new h(jSONObject.getJSONObject("user"));
        }
        if (jSONObject.has("context")) {
            this.j = jSONObject.isNull("context") ? null : b.a(jSONObject.getJSONObject("context"));
        }
    }

    public final Integer b() {
        return this.e;
    }

    public final void b(Integer num) {
        this.g = num;
    }

    public final Integer c() {
        return this.g;
    }

    public final Double d() {
        return this.f;
    }

    public final h e() {
        return this.i;
    }

    public final JSONObject f() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("device_id", this.a);
        jSONObject.put("result", this.f);
        jSONObject.put("level", this.c);
        if (this.i != null) {
            jSONObject.put("user_id", this.i.e());
        }
        if (this.h != null) {
            jSONObject.put("updated_at", a.a.format(this.h));
        }
        jSONObject.put("mode", this.e);
        jSONObject.put("minor_result", this.d);
        if (this.j != null) {
            jSONObject.put("context", b.a(this.j));
        }
        return jSONObject;
    }
}
