package com.scoreloop.client.android.core.b;

import java.math.BigDecimal;
import org.json.JSONException;
import org.json.JSONObject;

public final class a implements Cloneable, Comparable {
    private static String c;
    private static String d;
    private static String e;
    private static String f;
    private BigDecimal a;
    private String b;

    public a(String str, BigDecimal bigDecimal) {
        this.b = str;
        this.a = bigDecimal;
    }

    public a(JSONObject jSONObject) {
        this.b = jSONObject.getString("currency");
        try {
            this.a = new BigDecimal(jSONObject.getString("amount"));
            this.a.divide(new BigDecimal(100));
        } catch (NumberFormatException e2) {
            throw new JSONException("Invalid format of money amount");
        }
    }

    static void a(String str) {
        c = str;
    }

    public static void b(String str) {
        d = str;
    }

    public static void c(String str) {
        e = str;
    }

    public static void d(String str) {
        f = str;
    }

    /* renamed from: a */
    public final int compareTo(a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException();
        } else if (this.b.equalsIgnoreCase(aVar.b)) {
            return this.a.compareTo(aVar.a);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /* renamed from: a */
    public final a clone() {
        return new a(this.b, this.a);
    }

    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("amount", this.a);
        jSONObject.put("currency", this.b);
        return jSONObject;
    }

    public final String c() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof a)) {
            return super.equals(obj);
        }
        a aVar = (a) obj;
        return this.b.equalsIgnoreCase(aVar.b) && this.a.equals(aVar.a);
    }

    public final int hashCode() {
        return this.b.hashCode() ^ this.a.hashCode();
    }

    public final String toString() {
        return this.b.equalsIgnoreCase("SLD") ? this.a.toString() + " " + "Coins" : this.a.toString() + " " + this.b;
    }
}
