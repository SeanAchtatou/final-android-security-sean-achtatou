package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.b.h;
import com.scoreloop.client.android.core.b.q;
import com.scoreloop.client.android.core.b.t;
import com.scoreloop.client.android.core.c.m;
import com.scoreloop.client.android.core.c.n;
import com.scoreloop.client.android.core.c.p;
import org.json.JSONException;
import org.json.JSONObject;

final class aa extends m {
    private final q a;
    private final Integer b;
    private final Integer c;
    private final Integer d;
    private final t e;
    private final h f;

    public aa(p pVar, q qVar, t tVar, h hVar, Integer num, Integer num2, Integer num3) {
        super(pVar);
        this.a = qVar;
        this.e = tVar;
        this.f = hVar;
        this.b = num;
        this.d = num2;
        this.c = num3;
    }

    public final String a() {
        return String.format("/service/games/%s/scores", this.a.a());
    }

    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.e != null) {
                jSONObject.putOpt("search_list_id", this.e.c());
            }
            jSONObject.put("user_id", this.f.e());
            jSONObject.put("offset", this.c);
            jSONObject.put("per_page", this.d);
            jSONObject.put("mode", this.b);
            return jSONObject;
        } catch (JSONException e2) {
            throw new IllegalStateException("Invalid challenge data", e2);
        }
    }

    public final n c() {
        return n.GET;
    }
}
