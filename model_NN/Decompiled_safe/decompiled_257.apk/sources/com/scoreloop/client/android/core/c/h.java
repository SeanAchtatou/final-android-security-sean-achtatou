package com.scoreloop.client.android.core.c;

import android.os.Handler;
import android.os.Message;
import com.scoreloop.client.android.core.b.q;
import org.json.JSONException;
import org.json.JSONObject;

class h extends Thread implements b {
    private static int h = 0;
    private static /* synthetic */ boolean i = (!h.class.desiredAssertionStatus());
    private boolean a;
    private final Handler b;
    private j c;
    private q d;
    private volatile m e;
    private volatile c f;
    private volatile boolean g = true;

    h(Handler handler) {
        this.b = handler;
        StringBuilder append = new StringBuilder().append(getClass().getSimpleName());
        int i2 = h;
        h = i2 + 1;
        setName(append.append(i2).toString());
        setDaemon(true);
    }

    private void a(o oVar, int i2, Exception exc) {
        synchronized (this) {
            this.e = null;
            this.f = null;
        }
        Message obtainMessage = this.b.obtainMessage(i2);
        if (i2 == 1) {
            obtainMessage.obj = oVar;
        } else {
            obtainMessage.obj = exc;
        }
        obtainMessage.sendToTarget();
    }

    public final JSONObject a(JSONObject jSONObject) {
        try {
            if (this.d != null) {
                JSONObject jSONObject2 = jSONObject.getJSONObject("ext");
                JSONObject jSONObject3 = new JSONObject();
                jSONObject2.put("game", jSONObject3);
                jSONObject3.put("id", this.d.a());
                jSONObject3.put("secret", this.d.d());
                jSONObject3.put("version", this.d.e());
            }
            return jSONObject;
        } catch (JSONException e2) {
            throw new IllegalStateException("Should never happen");
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        synchronized (this) {
            this.g = false;
            if (this.f != null) {
                this.f.a();
            } else {
                notify();
            }
        }
        try {
            join();
        } catch (InterruptedException e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(q qVar) {
        this.d = qVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(j jVar) {
        this.c = jVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(m mVar) {
        boolean z = false;
        synchronized (this) {
            if (this.e != null) {
                throw new IllegalStateException("Previous request not finished yet");
            } else if (!mVar.j()) {
                this.e = mVar;
                this.f = new c(this.c);
                this.f.a(this.e.g(), this.e.a(), this.e.b(), this.e.f());
                notify();
            } else {
                z = true;
            }
        }
        if (z) {
            a(null, 2, null);
        }
    }

    public final void a(Integer num, Object obj, String str, int i2) {
        this.a = true;
        a(new o(obj, str, i2, num), 1, null);
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        synchronized (this) {
            if (this.f != null) {
                this.f.a();
            }
        }
    }

    public void run() {
        while (this.g) {
            try {
                synchronized (this) {
                    if (this.f == null && this.g) {
                        wait();
                    }
                    if (this.f != null) {
                        try {
                            this.a = false;
                            this.c.a(this.f);
                            if (!i && !this.a) {
                                throw new AssertionError();
                            }
                        } catch (e e2) {
                            "run(): Request processing was interrupted\n" + e2;
                            a(null, 2, e2);
                        } catch (i e3) {
                            "run(): publish() failed\n" + e3;
                            a(null, 3, e3);
                        } catch (Exception e4) {
                            "run(): publish() failed\n" + e4;
                            a(null, 3, e4);
                        }
                    }
                }
            } catch (InterruptedException e5) {
            }
        }
    }
}
