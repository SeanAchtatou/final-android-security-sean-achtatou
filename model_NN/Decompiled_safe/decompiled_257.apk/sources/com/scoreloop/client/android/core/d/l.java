package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.b.a;
import com.scoreloop.client.android.core.b.g;
import com.scoreloop.client.android.core.b.h;
import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.c.m;
import com.scoreloop.client.android.core.c.o;
import java.math.BigDecimal;
import java.util.ArrayList;
import org.json.JSONObject;

class l extends a {
    /* access modifiers changed from: private */
    public ae b;
    private final o c = new e(this);

    l(k kVar, o oVar) {
        super(kVar, oVar);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(m mVar, o oVar) {
        ArrayList arrayList;
        k d = d();
        int d2 = oVar.d();
        JSONObject c2 = oVar.c();
        JSONObject optJSONObject = c2.optJSONObject("user");
        if ((d2 == 200 || d2 == 201) && optJSONObject != null) {
            h f = d.f();
            f.a(optJSONObject);
            f.h();
            String optString = c2.optString("characteristic");
            if (optString != null) {
                d.d().a(optString);
            }
            a a = f.a();
            String c3 = a.c();
            if (a.compareTo(new a(c3, new BigDecimal(10000))) < 0) {
                arrayList = new ArrayList();
                arrayList.add(new a(c3, new BigDecimal(100)));
                arrayList.add(new a(c3, new BigDecimal(200)));
                arrayList.add(new a(c3, new BigDecimal(500)));
                arrayList.add(new a(c3, new BigDecimal(1000)));
                arrayList.add(new a(c3, new BigDecimal(2000)));
                arrayList.add(new a(c3, new BigDecimal(5000)));
            } else if (a.compareTo(new a(c3, new BigDecimal(100000))) < 0) {
                arrayList = new ArrayList();
                arrayList.add(new a(c3, new BigDecimal(500)));
                arrayList.add(new a(c3, new BigDecimal(1000)));
                arrayList.add(new a(c3, new BigDecimal(2500)));
                arrayList.add(new a(c3, new BigDecimal(5000)));
                arrayList.add(new a(c3, new BigDecimal(10000)));
                arrayList.add(new a(c3, new BigDecimal(20000)));
            } else {
                arrayList = new ArrayList();
                arrayList.add(new a(c3, new BigDecimal(1000)));
                arrayList.add(new a(c3, new BigDecimal(2500)));
                arrayList.add(new a(c3, new BigDecimal(5000)));
                arrayList.add(new a(c3, new BigDecimal(10000)));
                arrayList.add(new a(c3, new BigDecimal(50000)));
                arrayList.add(new a(c3, new BigDecimal(100000)));
            }
            d.a(arrayList);
            d.a(c2);
            d.a(g.AUTHENTICATED);
            return true;
        }
        d.a(g.FAILED);
        throw new Exception("Session authentication request failed with status: " + d2);
    }

    /* access modifiers changed from: package-private */
    public final boolean f() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public final void h() {
        k d = d();
        com.scoreloop.client.android.core.b.m c2 = d.c();
        if (d.h() == g.FAILED) {
            c2.a((String) null);
        }
        x xVar = new x(c(), a(), c2);
        if (c2.a() == null && this.b == null) {
            this.b = new ae(d(), this.c);
        }
        g();
        d.a(g.AUTHENTICATING);
        if (c2.a() == null) {
            this.b.h();
        }
        a(xVar);
    }
}
