package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.b.d;
import com.scoreloop.client.android.core.b.h;
import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.b.n;
import com.scoreloop.client.android.core.c.m;
import com.scoreloop.client.android.core.c.o;
import org.json.JSONObject;

public final class c extends a {
    private d b;
    private i c;

    private c(k kVar, b bVar) {
        super(kVar, bVar);
        this.c = new i(this);
    }

    public c(b bVar) {
        this(k.a(), bVar);
    }

    private d h() {
        if (this.b == null) {
            this.b = d().b();
        }
        return this.b;
    }

    public final void a(n nVar) {
        d h = h();
        if (nVar == null) {
            throw new IllegalArgumentException("aScore parameter can't be null");
        } else if (!e().equals(nVar.e())) {
            throw new IllegalStateException("User is not participating in the challenge");
        } else if (h == null || h.e() == nVar.b()) {
            nVar.a(e());
            h.a(nVar);
            d h2 = h();
            if (h2 == null) {
                throw new IllegalStateException("Set the challenge first");
            }
            m adVar = h2.d() == null ? new ad(c(), d(), a(), h2) : new g(c(), d(), a(), h2);
            d().a(h2);
            g();
            a(adVar);
        } else {
            throw new IllegalStateException("Score mode does not match challenge mode");
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(m mVar, o oVar) {
        b bVar = (b) this.a;
        int d = oVar.d();
        if (d == 200 || d == 201) {
            JSONObject jSONObject = oVar.c().getJSONObject("challenge");
            d h = h();
            h.a(jSONObject);
            h e = e();
            if ((e.equals(h.a()) && !h.g()) || (e.equals(h.c()) && (h.h() || h.f()))) {
                d().a((d) null);
            }
            bVar.a(this);
            return false;
        }
        Integer a = a(oVar.c());
        if (a == null) {
            throw new Exception("Request failed with status:" + d);
        }
        switch (a.intValue()) {
            case 22:
            case 27:
                bVar.a();
                return false;
            case 23:
            case 25:
            case 26:
            default:
                return false;
            case 24:
                bVar.c();
                return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean f() {
        return true;
    }
}
