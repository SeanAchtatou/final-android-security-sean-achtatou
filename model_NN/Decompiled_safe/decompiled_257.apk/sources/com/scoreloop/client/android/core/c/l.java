package com.scoreloop.client.android.core.c;

public enum l {
    CANCELLED,
    COMPLETED,
    ENQUEUED,
    EXECUTING,
    FAILED,
    IDLE;

    public static l[] a() {
        return (l[]) g.clone();
    }
}
