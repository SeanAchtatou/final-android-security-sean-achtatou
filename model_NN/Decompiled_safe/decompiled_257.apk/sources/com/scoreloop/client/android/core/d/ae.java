package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.b.l;
import com.scoreloop.client.android.core.c.m;
import com.scoreloop.client.android.core.c.o;
import org.json.JSONObject;

final class ae extends a {
    private m b;
    private com.scoreloop.client.android.core.b.m c;
    private m d;

    ae(k kVar, o oVar) {
        super(kVar, oVar);
        this.c = kVar.c();
    }

    /* access modifiers changed from: package-private */
    public final boolean a(m mVar, o oVar) {
        int d2 = oVar.d();
        JSONObject optJSONObject = oVar.c().optJSONObject("device");
        if (((v) mVar).n() == h.VERIFY) {
            if (d2 == 404) {
                return false;
            }
            if (this.b != null) {
                this.b.k();
            }
            this.b = null;
        }
        if ((d2 == 200 || d2 == 201) && optJSONObject != null) {
            this.c.a(optJSONObject.getString("id"));
            if ("freed".equalsIgnoreCase(optJSONObject.optString("state"))) {
                this.c.a(l.FREED);
            } else {
                this.c.a(d2 == 200 ? l.VERIFIED : l.CREATED);
            }
            return true;
        }
        throw new Exception("Request failed with status: " + d2);
    }

    /* access modifiers changed from: package-private */
    public final boolean f() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final void g() {
        super.g();
        if (this.d != null) {
            if (!this.d.j()) {
                d().e().b(this.d);
            }
            this.d = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void h() {
        g();
        this.d = new v(c(), this.c, h.VERIFY);
        a(this.d);
        this.b = new v(c(), this.c, h.CREATE);
        a(this.b);
    }
}
