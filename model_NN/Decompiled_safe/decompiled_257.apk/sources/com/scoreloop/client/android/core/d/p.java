package com.scoreloop.client.android.core.d;

import com.scoreloop.client.android.core.b.k;
import com.scoreloop.client.android.core.b.n;
import com.scoreloop.client.android.core.c.m;
import com.scoreloop.client.android.core.c.o;
import org.json.JSONObject;

public final class p extends a {
    private n b;
    private boolean c;
    private c d;

    private p(k kVar, o oVar) {
        super(kVar, oVar);
        this.c = false;
    }

    public p(o oVar) {
        this(k.a(), oVar);
    }

    public final void a(n nVar) {
        if (nVar == null) {
            throw new IllegalArgumentException("aScore parameter cannot be null");
        }
        this.b = nVar;
        if (d().b() == null || !this.c) {
            n nVar2 = new n(c(), a(), d(), this.b);
            g();
            a(nVar2);
            return;
        }
        if (this.d == null) {
            this.d = new c(new z(this));
        }
        this.d.a(this.b);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(m mVar, o oVar) {
        int d2 = oVar.d();
        JSONObject jSONObject = oVar.c().getJSONObject("score");
        if (this.b == null) {
            this.b = new n(jSONObject);
        } else {
            this.b.a(jSONObject);
        }
        if (d2 == 200 || d2 == 201) {
            return true;
        }
        throw new Exception("Request failed");
    }

    /* access modifiers changed from: package-private */
    public final boolean f() {
        return true;
    }
}
