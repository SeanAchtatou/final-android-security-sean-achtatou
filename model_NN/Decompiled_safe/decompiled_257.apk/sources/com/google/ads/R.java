package com.google.ads;

public final class R {

    public final class attr {
        /* added by JADX */
        public static final int backgroundColor = 2130771968;
        /* added by JADX */
        public static final int primaryTextColor = 2130771969;
        /* added by JADX */
        public static final int secondaryTextColor = 2130771970;
        /* added by JADX */
        public static final int keywords = 2130771971;
        /* added by JADX */
        public static final int refreshInterval = 2130771972;
        /* added by JADX */
        public static final int testing = 2130771973;
        /* added by JADX */
        public static final int adSize = 2130771974;
        /* added by JADX */
        public static final int adUnitId = 2130771975;
    }

    public final class id {
        /* added by JADX */
        public static final int BANNER = 2131099648;
        /* added by JADX */
        public static final int IAB_MRECT = 2131099649;
        /* added by JADX */
        public static final int IAB_BANNER = 2131099650;
        /* added by JADX */
        public static final int IAB_LEADERBOARD = 2131099651;
        /* added by JADX */
        public static final int ad_splash_layout = 2131099652;
        /* added by JADX */
        public static final int btn_download = 2131099653;
        /* added by JADX */
        public static final int btn_more = 2131099654;
        /* added by JADX */
        public static final int btn_close = 2131099655;
        /* added by JADX */
        public static final int btn_more2 = 2131099656;
        /* added by JADX */
        public static final int tv_msg = 2131099657;
        /* added by JADX */
        public static final int ad = 2131099658;
        /* added by JADX */
        public static final int iv_gameover = 2131099659;
        /* added by JADX */
        public static final int iv_curscore = 2131099660;
        /* added by JADX */
        public static final int tv_cur_score = 2131099661;
        /* added by JADX */
        public static final int iv_bestscore = 2131099662;
        /* added by JADX */
        public static final int tv_high_score = 2131099663;
        /* added by JADX */
        public static final int btn_submit = 2131099664;
        /* added by JADX */
        public static final int btn_tryagain = 2131099665;
        /* added by JADX */
        public static final int btn_quit = 2131099666;
        /* added by JADX */
        public static final int agile_buddy = 2131099667;
        /* added by JADX */
        public static final int scrollview2 = 2131099668;
        /* added by JADX */
        public static final int options_sounds_checkbox = 2131099669;
        /* added by JADX */
        public static final int options_vibrate_checkbox = 2131099670;
        /* added by JADX */
        public static final int options_power_controller_title = 2131099671;
        /* added by JADX */
        public static final int velocityController = 2131099672;
        /* added by JADX */
        public static final int ad1 = 2131099673;
        /* added by JADX */
        public static final int btn_back = 2131099674;
        /* added by JADX */
        public static final int namefield = 2131099675;
        /* added by JADX */
        public static final int retry = 2131099676;
        /* added by JADX */
        public static final int post_scores = 2131099677;
        /* added by JADX */
        public static final int goback = 2131099678;
        /* added by JADX */
        public static final int title_login = 2131099679;
        /* added by JADX */
        public static final int progress_indicator = 2131099680;
        /* added by JADX */
        public static final int game_mode_spinner = 2131099681;
        /* added by JADX */
        public static final int btn_profile = 2131099682;
        /* added by JADX */
        public static final int list_view = 2131099683;
        /* added by JADX */
        public static final int myscore_view = 2131099684;
        /* added by JADX */
        public static final int highscores_list_item = 2131099685;
        /* added by JADX */
        public static final int rank = 2131099686;
        /* added by JADX */
        public static final int login = 2131099687;
        /* added by JADX */
        public static final int score = 2131099688;
        /* added by JADX */
        public static final int email = 2131099689;
        /* added by JADX */
        public static final int TextView02 = 2131099690;
        /* added by JADX */
        public static final int update_button = 2131099691;
        /* added by JADX */
        public static final int spinnerTarget = 2131099692;
        /* added by JADX */
        public static final int img_logo = 2131099693;
        /* added by JADX */
        public static final int ad2 = 2131099694;
        /* added by JADX */
        public static final int start_game = 2131099695;
        /* added by JADX */
        public static final int options = 2131099696;
        /* added by JADX */
        public static final int score_board = 2131099697;
        /* added by JADX */
        public static final int more_app = 2131099698;
        /* added by JADX */
        public static final int exit = 2131099699;
        /* added by JADX */
        public static final int bg_tips = 2131099700;
        /* added by JADX */
        public static final int sensor_to_start = 2131099701;
    }

    public final class layout {
        /* added by JADX */
        public static final int ad_splash = 2130903040;
        /* added by JADX */
        public static final int dlg_alter = 2130903041;
        /* added by JADX */
        public static final int gameover = 2130903042;
        /* added by JADX */
        public static final int main = 2130903043;
        /* added by JADX */
        public static final int options = 2130903044;
        /* added by JADX */
        public static final int score_post_panel = 2130903045;
        /* added by JADX */
        public static final int sl_highscores = 2130903046;
        /* added by JADX */
        public static final int sl_highscores_list_item = 2130903047;
        /* added by JADX */
        public static final int sl_profile = 2130903048;
        /* added by JADX */
        public static final int sl_spinner_item = 2130903049;
        /* added by JADX */
        public static final int splash_480 = 2130903050;
        /* added by JADX */
        public static final int splash_533 = 2130903051;
        /* added by JADX */
        public static final int splash_569 = 2130903052;
        /* added by JADX */
        public static final int splash_other = 2130903053;
        /* added by JADX */
        public static final int tips = 2130903054;
    }

    /* added by JADX */
    public static final class drawable {
        /* added by JADX */
        public static final int ad1 = 2130837504;
        /* added by JADX */
        public static final int ad2 = 2130837505;
        /* added by JADX */
        public static final int ad3 = 2130837506;
        /* added by JADX */
        public static final int ad4 = 2130837507;
        /* added by JADX */
        public static final int back_down = 2130837508;
        /* added by JADX */
        public static final int back_up = 2130837509;
        /* added by JADX */
        public static final int bestscore = 2130837510;
        /* added by JADX */
        public static final int bg_game1 = 2130837511;
        /* added by JADX */
        public static final int bg_game2 = 2130837512;
        /* added by JADX */
        public static final int bg_game3 = 2130837513;
        /* added by JADX */
        public static final int bg_gameover = 2130837514;
        /* added by JADX */
        public static final int bg_setting = 2130837515;
        /* added by JADX */
        public static final int bg_splash = 2130837516;
        /* added by JADX */
        public static final int bg_tips_480 = 2130837517;
        /* added by JADX */
        public static final int bg_tips_569 = 2130837518;
        /* added by JADX */
        public static final int btn_back = 2130837519;
        /* added by JADX */
        public static final int btn_default = 2130837520;
        /* added by JADX */
        public static final int btn_exit = 2130837521;
        /* added by JADX */
        public static final int btn_go = 2130837522;
        /* added by JADX */
        public static final int btn_highscore = 2130837523;
        /* added by JADX */
        public static final int btn_more = 2130837524;
        /* added by JADX */
        public static final int btn_newgame = 2130837525;
        /* added by JADX */
        public static final int btn_quit = 2130837526;
        /* added by JADX */
        public static final int btn_setting = 2130837527;
        /* added by JADX */
        public static final int btn_submit = 2130837528;
        /* added by JADX */
        public static final int btn_try = 2130837529;
        /* added by JADX */
        public static final int button_bg_down = 2130837530;
        /* added by JADX */
        public static final int button_bg_up = 2130837531;
        /* added by JADX */
        public static final int checkbox_sound = 2130837532;
        /* added by JADX */
        public static final int checkbox_vibrate = 2130837533;
        /* added by JADX */
        public static final int curreentscore = 2130837534;
        /* added by JADX */
        public static final int drop = 2130837535;
        /* added by JADX */
        public static final int drop1 = 2130837536;
        /* added by JADX */
        public static final int drop2 = 2130837537;
        /* added by JADX */
        public static final int drop3 = 2130837538;
        /* added by JADX */
        public static final int drop4 = 2130837539;
        /* added by JADX */
        public static final int ele1 = 2130837540;
        /* added by JADX */
        public static final int ele2 = 2130837541;
        /* added by JADX */
        public static final int ele3 = 2130837542;
        /* added by JADX */
        public static final int ele4 = 2130837543;
        /* added by JADX */
        public static final int ele5 = 2130837544;
        /* added by JADX */
        public static final int ele6 = 2130837545;
        /* added by JADX */
        public static final int exit_down = 2130837546;
        /* added by JADX */
        public static final int exit_up = 2130837547;
        /* added by JADX */
        public static final int food_1 = 2130837548;
        /* added by JADX */
        public static final int food_2 = 2130837549;
        /* added by JADX */
        public static final int food_3 = 2130837550;
        /* added by JADX */
        public static final int food_4 = 2130837551;
        /* added by JADX */
        public static final int food_5 = 2130837552;
        /* added by JADX */
        public static final int food_6 = 2130837553;
        /* added by JADX */
        public static final int footboard_moving_left1 = 2130837554;
        /* added by JADX */
        public static final int footboard_moving_left2 = 2130837555;
        /* added by JADX */
        public static final int footboard_moving_right1 = 2130837556;
        /* added by JADX */
        public static final int footboard_moving_right2 = 2130837557;
        /* added by JADX */
        public static final int footboard_normal = 2130837558;
        /* added by JADX */
        public static final int footboard_plane_left = 2130837559;
        /* added by JADX */
        public static final int footboard_plane_right = 2130837560;
        /* added by JADX */
        public static final int footboard_spiked = 2130837561;
        /* added by JADX */
        public static final int footboard_spring = 2130837562;
        /* added by JADX */
        public static final int footboard_spring2 = 2130837563;
        /* added by JADX */
        public static final int footboard_unstable1 = 2130837564;
        /* added by JADX */
        public static final int footboard_unstable2 = 2130837565;
        /* added by JADX */
        public static final int gad_bg_splash = 2130837566;
        /* added by JADX */
        public static final int gad_btn_close = 2130837567;
        /* added by JADX */
        public static final int gad_btn_download = 2130837568;
        /* added by JADX */
        public static final int gad_btn_moregame = 2130837569;
        /* added by JADX */
        public static final int gad_btn_moregame2 = 2130837570;
        /* added by JADX */
        public static final int gad_close_down = 2130837571;
        /* added by JADX */
        public static final int gad_close_up = 2130837572;
        /* added by JADX */
        public static final int gad_download_down = 2130837573;
        /* added by JADX */
        public static final int gad_download_up = 2130837574;
        /* added by JADX */
        public static final int gad_more2_down = 2130837575;
        /* added by JADX */
        public static final int gad_more2_up = 2130837576;
        /* added by JADX */
        public static final int gad_more_down = 2130837577;
        /* added by JADX */
        public static final int gad_more_up = 2130837578;
        /* added by JADX */
        public static final int game_logo = 2130837579;
        /* added by JADX */
        public static final int gameover = 2130837580;
        /* added by JADX */
        public static final int go_down = 2130837581;
        /* added by JADX */
        public static final int go_up = 2130837582;
        /* added by JADX */
        public static final int hp_bar_remain = 2130837583;
        /* added by JADX */
        public static final int hp_bar_total = 2130837584;
        /* added by JADX */
        public static final int hurt1 = 2130837585;
        /* added by JADX */
        public static final int hurt2 = 2130837586;
        /* added by JADX */
        public static final int ic_send = 2130837587;
        /* added by JADX */
        public static final int ic_share_all = 2130837588;
        /* added by JADX */
        public static final int ic_sound_off = 2130837589;
        /* added by JADX */
        public static final int ic_sound_on = 2130837590;
        /* added by JADX */
        public static final int ic_wallpaper = 2130837591;
        /* added by JADX */
        public static final int icon = 2130837592;
        /* added by JADX */
        public static final int icon2 = 2130837593;
        /* added by JADX */
        public static final int icon3 = 2130837594;
        /* added by JADX */
        public static final int icon_top_title = 2130837595;
        /* added by JADX */
        public static final int menu_button = 2130837596;
        /* added by JADX */
        public static final int more_down = 2130837597;
        /* added by JADX */
        public static final int more_up = 2130837598;
        /* added by JADX */
        public static final int newgame_down = 2130837599;
        /* added by JADX */
        public static final int newgame_up = 2130837600;
        /* added by JADX */
        public static final int pause = 2130837601;
        /* added by JADX */
        public static final int power_foot = 2130837602;
        /* added by JADX */
        public static final int power_line1 = 2130837603;
        /* added by JADX */
        public static final int power_line2 = 2130837604;
        /* added by JADX */
        public static final int power_line3 = 2130837605;
        /* added by JADX */
        public static final int quit_down = 2130837606;
        /* added by JADX */
        public static final int quit_up = 2130837607;
        /* added by JADX */
        public static final int ranking_down = 2130837608;
        /* added by JADX */
        public static final int ranking_up = 2130837609;
        /* added by JADX */
        public static final int role_deadman = 2130837610;
        /* added by JADX */
        public static final int role_freefall1 = 2130837611;
        /* added by JADX */
        public static final int role_freefall2 = 2130837612;
        /* added by JADX */
        public static final int role_freefall3 = 2130837613;
        /* added by JADX */
        public static final int role_freefall4 = 2130837614;
        /* added by JADX */
        public static final int role_moving_left1 = 2130837615;
        /* added by JADX */
        public static final int role_moving_left2 = 2130837616;
        /* added by JADX */
        public static final int role_moving_left3 = 2130837617;
        /* added by JADX */
        public static final int role_moving_left4 = 2130837618;
        /* added by JADX */
        public static final int role_moving_right1 = 2130837619;
        /* added by JADX */
        public static final int role_moving_right2 = 2130837620;
        /* added by JADX */
        public static final int role_moving_right3 = 2130837621;
        /* added by JADX */
        public static final int role_moving_right4 = 2130837622;
        /* added by JADX */
        public static final int role_standing = 2130837623;
        /* added by JADX */
        public static final int saiyan = 2130837624;
        /* added by JADX */
        public static final int scene_change = 2130837625;
        /* added by JADX */
        public static final int scene_line = 2130837626;
        /* added by JADX */
        public static final int setting = 2130837627;
        /* added by JADX */
        public static final int setting_down = 2130837628;
        /* added by JADX */
        public static final int setting_up = 2130837629;
        /* added by JADX */
        public static final int shine = 2130837630;
        /* added by JADX */
        public static final int shine2 = 2130837631;
        /* added by JADX */
        public static final int sl_bg_btn = 2130837632;
        /* added by JADX */
        public static final int sl_bg_btn_pre = 2130837633;
        /* added by JADX */
        public static final int sl_bg_dropdown = 2130837634;
        /* added by JADX */
        public static final int sl_bg_dropdown_pre = 2130837635;
        /* added by JADX */
        public static final int sl_bg_h1 = 2130837636;
        /* added by JADX */
        public static final int sl_bg_list = 2130837637;
        /* added by JADX */
        public static final int sl_bg_list_pre = 2130837638;
        /* added by JADX */
        public static final int sl_divider = 2130837639;
        /* added by JADX */
        public static final int sl_divider_list = 2130837640;
        /* added by JADX */
        public static final int sl_logo = 2130837641;
        /* added by JADX */
        public static final int sl_menu_highscores = 2130837642;
        /* added by JADX */
        public static final int sl_menu_profile = 2130837643;
        /* added by JADX */
        public static final int sl_selector_btn = 2130837644;
        /* added by JADX */
        public static final int sl_selector_dropdown = 2130837645;
        /* added by JADX */
        public static final int sl_selector_list = 2130837646;
        /* added by JADX */
        public static final int sound_checked = 2130837647;
        /* added by JADX */
        public static final int sound_unchecked = 2130837648;
        /* added by JADX */
        public static final int startgame_down = 2130837649;
        /* added by JADX */
        public static final int startgame_up = 2130837650;
        /* added by JADX */
        public static final int submit_down = 2130837651;
        /* added by JADX */
        public static final int submit_up = 2130837652;
        /* added by JADX */
        public static final int tip_awesome = 2130837653;
        /* added by JADX */
        public static final int top_bar2 = 2130837654;
        /* added by JADX */
        public static final int tp_bar_remain = 2130837655;
        /* added by JADX */
        public static final int tp_bar_total = 2130837656;
        /* added by JADX */
        public static final int tryagain_down = 2130837657;
        /* added by JADX */
        public static final int tryagain_up = 2130837658;
        /* added by JADX */
        public static final int vibrate_checked = 2130837659;
        /* added by JADX */
        public static final int vibrate_unchecked = 2130837660;
    }

    /* added by JADX */
    public static final class anim {
        /* added by JADX */
        public static final int logo_move = 2130968576;
    }

    /* added by JADX */
    public static final class raw {
        /* added by JADX */
        public static final int click = 2131034112;
        /* added by JADX */
        public static final int click2 = 2131034113;
        /* added by JADX */
        public static final int drop = 2131034114;
        /* added by JADX */
        public static final int drop_disappear = 2131034115;
        /* added by JADX */
        public static final int gameover = 2131034116;
        /* added by JADX */
        public static final int jumpon = 2131034117;
        /* added by JADX */
        public static final int m_game = 2131034118;
        /* added by JADX */
        public static final int m_menu = 2131034119;
        /* added by JADX */
        public static final int meterwarn = 2131034120;
        /* added by JADX */
        public static final int moving = 2131034121;
        /* added by JADX */
        public static final int moving2 = 2131034122;
        /* added by JADX */
        public static final int nextlevel = 2131034123;
        /* added by JADX */
        public static final int normal = 2131034124;
        /* added by JADX */
        public static final int pause = 2131034125;
        /* added by JADX */
        public static final int power = 2131034126;
        /* added by JADX */
        public static final int power_off = 2131034127;
        /* added by JADX */
        public static final int power_on = 2131034128;
        /* added by JADX */
        public static final int roof = 2131034129;
        /* added by JADX */
        public static final int scene_change = 2131034130;
        /* added by JADX */
        public static final int spiked = 2131034131;
        /* added by JADX */
        public static final int spiked2 = 2131034132;
        /* added by JADX */
        public static final int spring = 2131034133;
        /* added by JADX */
        public static final int tools = 2131034134;
        /* added by JADX */
        public static final int unstable = 2131034135;
    }

    /* added by JADX */
    public static final class array {
        /* added by JADX */
        public static final int sl_game_modes = 2131165184;
    }

    /* added by JADX */
    public static final class string {
        /* added by JADX */
        public static final int sl_profile = 2131230720;
        /* added by JADX */
        public static final int sl_highscores = 2131230721;
        /* added by JADX */
        public static final int sl_update_profile = 2131230722;
        /* added by JADX */
        public static final int sl_login = 2131230723;
        /* added by JADX */
        public static final int sl_email = 2131230724;
        /* added by JADX */
        public static final int sl_prev = 2131230725;
        /* added by JADX */
        public static final int sl_next = 2131230726;
        /* added by JADX */
        public static final int sl_top = 2131230727;
        /* added by JADX */
        public static final int sl_error_message_not_on_highscore_list = 2131230728;
        /* added by JADX */
        public static final int sl_error_message_network = 2131230729;
        /* added by JADX */
        public static final int sl_error_message_name_already_taken = 2131230730;
        /* added by JADX */
        public static final int sl_error_message_email_already_taken = 2131230731;
        /* added by JADX */
        public static final int sl_error_message_invalid_email_format = 2131230732;
        /* added by JADX */
        public static final int app_name = 2131230733;
        /* added by JADX */
        public static final int start_game = 2131230734;
        /* added by JADX */
        public static final int score_board = 2131230735;
        /* added by JADX */
        public static final int options = 2131230736;
        /* added by JADX */
        public static final int more_app = 2131230737;
        /* added by JADX */
        public static final int exit = 2131230738;
        /* added by JADX */
        public static final int sensor_to_start = 2131230739;
        /* added by JADX */
        public static final int touch_to_start = 2131230740;
        /* added by JADX */
        public static final int global_ranking = 2131230741;
        /* added by JADX */
        public static final int gameover_dialog_tip = 2131230742;
        /* added by JADX */
        public static final int gameover_dialog_text_poolguy = 2131230743;
        /* added by JADX */
        public static final int gameover_dialog_text_notbad = 2131230744;
        /* added by JADX */
        public static final int gameover_dialog_text_awesome = 2131230745;
        /* added by JADX */
        public static final int gameover_dialog_text_newrecord = 2131230746;
        /* added by JADX */
        public static final int gameover_username_required = 2131230747;
        /* added by JADX */
        public static final int gameover_dialog_positive_button = 2131230748;
        /* added by JADX */
        public static final int gameover_dialog_post_score_button = 2131230749;
        /* added by JADX */
        public static final int gameover_dialog_negative_button = 2131230750;
        /* added by JADX */
        public static final int ranking_title_loading_info = 2131230751;
        /* added by JADX */
        public static final int ranking_title_no = 2131230752;
        /* added by JADX */
        public static final int ranking_title_name = 2131230753;
        /* added by JADX */
        public static final int ranking_title_score = 2131230754;
        /* added by JADX */
        public static final int ranking_title_date = 2131230755;
        /* added by JADX */
        public static final int options_sounds_summary = 2131230756;
        /* added by JADX */
        public static final int options_vibrate_summary = 2131230757;
        /* added by JADX */
        public static final int options_showtips_summary = 2131230758;
        /* added by JADX */
        public static final int options_velocity_controller_summary = 2131230759;
        /* added by JADX */
        public static final int options_user_name_summary = 2131230760;
        /* added by JADX */
        public static final int options_best_record_summary = 2131230761;
        /* added by JADX */
        public static final int options_okay_summary = 2131230762;
        /* added by JADX */
        public static final int options_upload_score_summary = 2131230763;
        /* added by JADX */
        public static final int options_tips_summary = 2131230764;
        /* added by JADX */
        public static final int options_toast_username_null = 2131230765;
        /* added by JADX */
        public static final int options_toast_username_too_long = 2131230766;
        /* added by JADX */
        public static final int options_toast_upload_success = 2131230767;
        /* added by JADX */
        public static final int options_toast_upload_failure = 2131230768;
        /* added by JADX */
        public static final int tips_text = 2131230769;
        /* added by JADX */
        public static final int profile = 2131230770;
        /* added by JADX */
        public static final int options_save = 2131230771;
        /* added by JADX */
        public static final int options_back = 2131230772;
        /* added by JADX */
        public static final int help = 2131230773;
    }

    /* added by JADX */
    public static final class style {
        /* added by JADX */
        public static final int sl_heading = 2131296256;
        /* added by JADX */
        public static final int sl_title_bar = 2131296257;
        /* added by JADX */
        public static final int sl_normal = 2131296258;
    }

    /* added by JADX */
    public static final class color {
        /* added by JADX */
        public static final int sl_selector_color = 2131361792;
    }
}
