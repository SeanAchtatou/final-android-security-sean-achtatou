package com.google.a;

import java.io.InputStream;
import java.util.ArrayList;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f1100a;

    /* renamed from: b  reason: collision with root package name */
    private int f1101b;
    private int c;
    private int d;
    private final InputStream e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;

    public static b a(InputStream inputStream) {
        return new b(inputStream);
    }

    public static b a(byte[] bArr) {
        return a(bArr, bArr.length);
    }

    public static b a(byte[] bArr, int i2) {
        return new b(bArr, i2);
    }

    public final int a() {
        if (this.d == this.f1101b && !a(false)) {
            this.f = 0;
            return 0;
        }
        this.f = e();
        if (m.b(this.f) != 0) {
            return this.f;
        }
        throw new o("Protocol message contained an invalid tag (zero).");
    }

    public final void a(int i2) {
        if (this.f != i2) {
            throw new o("Protocol message end-group tag did not match expected tag.");
        }
    }

    public final boolean b(int i2) {
        int a2;
        switch (m.a(i2)) {
            case 0:
                e();
                return true;
            case 1:
                g();
                g();
                g();
                g();
                g();
                g();
                g();
                g();
                return true;
            case 2:
                d(e());
                return true;
            case 3:
                break;
            case 4:
                return false;
            case 5:
                g();
                g();
                g();
                g();
                return true;
            default:
                throw new o("Protocol message tag had invalid wire type.");
        }
        do {
            a2 = a();
            if (a2 != 0) {
            }
            a(m.a(m.b(i2), 4));
            return true;
        } while (b(a2));
        a(m.a(m.b(i2), 4));
        return true;
    }

    public final String b() {
        int e2 = e();
        if (e2 > this.f1101b - this.d || e2 <= 0) {
            return new String(c(e2), "UTF-8");
        }
        String str = new String(this.f1100a, this.d, e2, "UTF-8");
        this.d = e2 + this.d;
        return str;
    }

    public final void a(d dVar, p pVar) {
        int e2 = e();
        if (this.i >= this.j) {
            throw new o("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
        } else if (e2 < 0) {
            throw o.b();
        } else {
            int i2 = e2 + this.g + this.d;
            int i3 = this.h;
            if (i2 > i3) {
                throw o.a();
            }
            this.h = i2;
            f();
            this.i++;
            dVar.b(this, pVar);
            a(0);
            this.i--;
            this.h = i3;
            f();
        }
    }

    public final g c() {
        int e2 = e();
        if (e2 > this.f1101b - this.d || e2 <= 0) {
            return g.a(c(e2));
        }
        g a2 = g.a(this.f1100a, this.d, e2);
        this.d = e2 + this.d;
        return a2;
    }

    public final int d() {
        return e();
    }

    private int e() {
        byte g2 = g();
        if (g2 >= 0) {
            return g2;
        }
        byte b2 = g2 & Byte.MAX_VALUE;
        byte g3 = g();
        if (g3 >= 0) {
            return b2 | (g3 << 7);
        }
        byte b3 = b2 | ((g3 & Byte.MAX_VALUE) << 7);
        byte g4 = g();
        if (g4 >= 0) {
            return b3 | (g4 << 14);
        }
        byte b4 = b3 | ((g4 & Byte.MAX_VALUE) << 14);
        byte g5 = g();
        if (g5 >= 0) {
            return b4 | (g5 << 21);
        }
        byte b5 = b4 | ((g5 & Byte.MAX_VALUE) << 21);
        byte g6 = g();
        byte b6 = b5 | (g6 << 28);
        if (g6 >= 0) {
            return b6;
        }
        for (int i2 = 0; i2 < 5; i2++) {
            if (g() >= 0) {
                return b6;
            }
        }
        throw new o("CodedInputStream encountered a malformed varint.");
    }

    private b(byte[] bArr, int i2) {
        this.h = Integer.MAX_VALUE;
        this.j = 64;
        this.k = 67108864;
        this.f1100a = bArr;
        this.f1101b = i2 + 0;
        this.d = 0;
        this.g = 0;
        this.e = null;
    }

    private b(InputStream inputStream) {
        this.h = Integer.MAX_VALUE;
        this.j = 64;
        this.k = 67108864;
        this.f1100a = new byte[4096];
        this.f1101b = 0;
        this.d = 0;
        this.g = 0;
        this.e = inputStream;
    }

    private void f() {
        this.f1101b += this.c;
        int i2 = this.g + this.f1101b;
        if (i2 > this.h) {
            this.c = i2 - this.h;
            this.f1101b -= this.c;
            return;
        }
        this.c = 0;
    }

    private boolean a(boolean z) {
        if (this.d < this.f1101b) {
            throw new IllegalStateException("refillBuffer() called when buffer wasn't empty.");
        } else if (this.g + this.f1101b != this.h) {
            this.g += this.f1101b;
            this.d = 0;
            this.f1101b = this.e == null ? -1 : this.e.read(this.f1100a);
            if (this.f1101b == 0 || this.f1101b < -1) {
                throw new IllegalStateException("InputStream#read(byte[]) returned invalid result: " + this.f1101b + "\nThe InputStream implementation is buggy.");
            } else if (this.f1101b == -1) {
                this.f1101b = 0;
                if (!z) {
                    return false;
                }
                throw o.a();
            } else {
                f();
                int i2 = this.g + this.f1101b + this.c;
                if (i2 <= this.k && i2 >= 0) {
                    return true;
                }
                throw new o("Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit.");
            }
        } else if (!z) {
            return false;
        } else {
            throw o.a();
        }
    }

    private byte g() {
        if (this.d == this.f1101b) {
            a(true);
        }
        byte[] bArr = this.f1100a;
        int i2 = this.d;
        this.d = i2 + 1;
        return bArr[i2];
    }

    public final byte[] c(int i2) {
        if (i2 < 0) {
            throw o.b();
        } else if (this.g + this.d + i2 > this.h) {
            d((this.h - this.g) - this.d);
            throw o.a();
        } else if (i2 <= this.f1101b - this.d) {
            byte[] bArr = new byte[i2];
            System.arraycopy(this.f1100a, this.d, bArr, 0, i2);
            this.d += i2;
            return bArr;
        } else if (i2 < 4096) {
            byte[] bArr2 = new byte[i2];
            int i3 = this.f1101b - this.d;
            System.arraycopy(this.f1100a, this.d, bArr2, 0, i3);
            this.d = this.f1101b;
            a(true);
            while (i2 - i3 > this.f1101b) {
                System.arraycopy(this.f1100a, 0, bArr2, i3, this.f1101b);
                i3 += this.f1101b;
                this.d = this.f1101b;
                a(true);
            }
            System.arraycopy(this.f1100a, 0, bArr2, i3, i2 - i3);
            this.d = i2 - i3;
            return bArr2;
        } else {
            int i4 = this.d;
            int i5 = this.f1101b;
            this.g += this.f1101b;
            this.d = 0;
            this.f1101b = 0;
            int i6 = i2 - (i5 - i4);
            ArrayList<byte[]> arrayList = new ArrayList<>();
            while (i6 > 0) {
                byte[] bArr3 = new byte[Math.min(i6, 4096)];
                int i7 = 0;
                while (i7 < bArr3.length) {
                    int read = this.e == null ? -1 : this.e.read(bArr3, i7, bArr3.length - i7);
                    if (read == -1) {
                        throw o.a();
                    }
                    this.g += read;
                    i7 += read;
                }
                i6 -= bArr3.length;
                arrayList.add(bArr3);
            }
            byte[] bArr4 = new byte[i2];
            int i8 = i5 - i4;
            System.arraycopy(this.f1100a, i4, bArr4, 0, i8);
            for (byte[] bArr5 : arrayList) {
                System.arraycopy(bArr5, 0, bArr4, i8, bArr5.length);
                i8 += bArr5.length;
            }
            return bArr4;
        }
    }

    private void d(int i2) {
        if (i2 < 0) {
            throw o.b();
        } else if (this.g + this.d + i2 > this.h) {
            d((this.h - this.g) - this.d);
            throw o.a();
        } else if (i2 <= this.f1101b - this.d) {
            this.d += i2;
        } else {
            int i3 = this.f1101b - this.d;
            this.g += i3;
            this.d = 0;
            this.f1101b = 0;
            while (i3 < i2) {
                int skip = this.e == null ? -1 : (int) this.e.skip((long) (i2 - i3));
                if (skip <= 0) {
                    throw o.a();
                }
                i3 += skip;
                this.g = skip + this.g;
            }
        }
    }
}
