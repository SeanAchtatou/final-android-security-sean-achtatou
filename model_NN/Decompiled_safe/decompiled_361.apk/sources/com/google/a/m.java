package com.google.a;

public final class m {

    /* renamed from: a  reason: collision with root package name */
    private static int f1108a = 11;

    /* renamed from: b  reason: collision with root package name */
    private static int f1109b = 12;
    private static int c = 16;
    private static int d = 26;

    private m() {
    }

    static int a(int i) {
        return i & 7;
    }

    public static int b(int i) {
        return i >>> 3;
    }

    static int a(int i, int i2) {
        return (i << 3) | i2;
    }
}
