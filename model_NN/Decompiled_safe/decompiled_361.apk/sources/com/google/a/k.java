package com.google.a;

import java.io.IOException;

public abstract class k implements f {
    public final g o() {
        try {
            i a2 = g.a(g());
            a(a2.b());
            return a2.a();
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a ByteString threw an IOException (should never happen).", e);
        }
    }

    public final byte[] p() {
        try {
            byte[] bArr = new byte[g()];
            l a2 = l.a(bArr);
            a(a2);
            a2.b();
            return bArr;
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }
}
