package com.google.a;

import java.util.List;

public final class a extends RuntimeException {

    /* renamed from: a  reason: collision with root package name */
    private final List f1099a = null;

    public a() {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
    }

    public final o a() {
        return new o(getMessage());
    }
}
