package com.google.a;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    public static final g f1102a = new g(new byte[0], (byte) 0);

    /* renamed from: b  reason: collision with root package name */
    private final byte[] f1103b;
    private volatile int c;

    /* synthetic */ g(byte[] bArr) {
        this(bArr, (byte) 0);
    }

    private g(byte[] bArr, byte b2) {
        this.c = 0;
        this.f1103b = bArr;
    }

    public final int a() {
        return this.f1103b.length;
    }

    public static g a(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return new g(bArr2, (byte) 0);
    }

    public static g a(byte[] bArr) {
        return a(bArr, 0, bArr.length);
    }

    public final byte[] b() {
        int length = this.f1103b.length;
        byte[] bArr = new byte[length];
        System.arraycopy(this.f1103b, 0, bArr, 0, length);
        return bArr;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof g)) {
            return false;
        }
        g gVar = (g) obj;
        int length = this.f1103b.length;
        if (length != gVar.f1103b.length) {
            return false;
        }
        byte[] bArr = this.f1103b;
        byte[] bArr2 = gVar.f1103b;
        for (int i = 0; i < length; i++) {
            if (bArr[i] != bArr2[i]) {
                return false;
            }
        }
        return true;
    }

    public final int hashCode() {
        int i = this.c;
        if (i == 0) {
            byte[] bArr = this.f1103b;
            int length = this.f1103b.length;
            int i2 = length;
            for (int i3 = 0; i3 < length; i3++) {
                i2 = (i2 * 31) + bArr[i3];
            }
            if (i2 == 0) {
                i = 1;
            } else {
                i = i2;
            }
            this.c = i;
        }
        return i;
    }

    public final b c() {
        return b.a(this.f1103b);
    }

    static i a(int i) {
        return new i(i);
    }
}
