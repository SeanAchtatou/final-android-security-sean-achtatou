package com.google.a;

final class i {

    /* renamed from: a  reason: collision with root package name */
    private final l f1104a;

    /* renamed from: b  reason: collision with root package name */
    private final byte[] f1105b;

    /* synthetic */ i(int i) {
        this(i, (byte) 0);
    }

    private i(int i, byte b2) {
        this.f1105b = new byte[i];
        this.f1104a = l.a(this.f1105b);
    }

    public final g a() {
        this.f1104a.b();
        return new g(this.f1105b);
    }

    public final l b() {
        return this.f1104a;
    }
}
