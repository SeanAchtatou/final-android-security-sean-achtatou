package com.google.a;

import java.io.IOException;

public final class n extends IOException {
    n() {
        super("CodedOutputStream was writing to a flat byte array and ran out of space.");
    }
}
