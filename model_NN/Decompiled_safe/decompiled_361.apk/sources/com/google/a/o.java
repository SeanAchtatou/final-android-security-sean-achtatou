package com.google.a;

import java.io.IOException;

public final class o extends IOException {
    public o(String str) {
        super(str);
    }

    static o a() {
        return new o("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either than the input has been truncated or that an embedded message misreported its own length.");
    }

    static o b() {
        return new o("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }
}
