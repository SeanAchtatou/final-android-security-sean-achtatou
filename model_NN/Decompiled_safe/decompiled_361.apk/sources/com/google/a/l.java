package com.google.a;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f1106a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1107b;
    private int c;
    private final OutputStream d;

    private l(byte[] bArr, int i) {
        this.d = null;
        this.f1106a = bArr;
        this.c = 0;
        this.f1107b = i + 0;
    }

    private l(OutputStream outputStream, byte[] bArr) {
        this.d = outputStream;
        this.f1106a = bArr;
        this.c = 0;
        this.f1107b = bArr.length;
    }

    public static l a(OutputStream outputStream, int i) {
        return new l(outputStream, new byte[i]);
    }

    public static l a(byte[] bArr) {
        return new l(bArr, bArr.length);
    }

    public final void a(int i, String str) {
        c(i, 2);
        byte[] bytes = str.getBytes("UTF-8");
        c(bytes.length);
        b(bytes);
    }

    public final void a(int i, f fVar) {
        c(i, 2);
        c(fVar.g());
        fVar.a(this);
    }

    public final void a(g gVar) {
        c(2, 2);
        byte[] b2 = gVar.b();
        c(b2.length);
        b(b2);
    }

    public final void a(int i, int i2) {
        c(i, 0);
        c(i2);
    }

    public static int b(int i, String str) {
        return b(i) + a(str);
    }

    public static int b(int i, f fVar) {
        int b2 = b(i);
        int g = fVar.g();
        return b2 + g + d(g);
    }

    public static int b(g gVar) {
        return b(2) + d(gVar.a()) + gVar.a();
    }

    public static int b(int i, int i2) {
        return b(i) + d(i2);
    }

    private static int a(String str) {
        try {
            byte[] bytes = str.getBytes("UTF-8");
            return bytes.length + d(bytes.length);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 not supported.", e);
        }
    }

    private void c() {
        if (this.d == null) {
            throw new n();
        }
        this.d.write(this.f1106a, 0, this.c);
        this.c = 0;
    }

    public final void a() {
        if (this.d != null) {
            c();
        }
    }

    public final void b() {
        if (this.d != null) {
            throw new UnsupportedOperationException("spaceLeft() can only be called on CodedOutputStreams that are writing to a flat array.");
        } else if (this.f1107b - this.c != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    private void a(int i) {
        byte b2 = (byte) i;
        if (this.c == this.f1107b) {
            c();
        }
        byte[] bArr = this.f1106a;
        int i2 = this.c;
        this.c = i2 + 1;
        bArr[i2] = b2;
    }

    private void b(byte[] bArr) {
        int length = bArr.length;
        if (this.f1107b - this.c >= length) {
            System.arraycopy(bArr, 0, this.f1106a, this.c, length);
            this.c = length + this.c;
            return;
        }
        int i = this.f1107b - this.c;
        System.arraycopy(bArr, 0, this.f1106a, this.c, i);
        int i2 = i + 0;
        int i3 = length - i;
        this.c = this.f1107b;
        c();
        if (i3 <= this.f1107b) {
            System.arraycopy(bArr, i2, this.f1106a, 0, i3);
            this.c = i3;
            return;
        }
        this.d.write(bArr, i2, i3);
    }

    private void c(int i, int i2) {
        c(m.a(i, i2));
    }

    private static int b(int i) {
        return d(m.a(i, 0));
    }

    private void c(int i) {
        int i2 = i;
        while ((i2 & -128) != 0) {
            a((i2 & 127) | 128);
            i2 >>>= 7;
        }
        a(i2);
    }

    private static int d(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        if ((-268435456 & i) == 0) {
            return 4;
        }
        return 5;
    }
}
