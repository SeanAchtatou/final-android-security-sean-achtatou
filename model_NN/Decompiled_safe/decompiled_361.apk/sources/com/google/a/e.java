package com.google.a;

import java.io.IOException;
import java.util.Collection;

public abstract class e implements d {
    /* renamed from: a */
    public abstract e b(b bVar, p pVar);

    /* renamed from: d */
    public abstract e clone();

    private e a(b bVar) {
        return b(bVar, p.a());
    }

    public final e b(g gVar) {
        try {
            b c = gVar.c();
            a(c);
            c.a(0);
            return this;
        } catch (o e) {
            throw e;
        } catch (IOException e2) {
            throw new RuntimeException("Reading from a ByteString threw an IOException (should never happen).", e2);
        }
    }

    public final e a(byte[] bArr, int i) {
        try {
            b a2 = b.a(bArr, i);
            a(a2);
            a2.a(0);
            return this;
        } catch (o e) {
            throw e;
        } catch (IOException e2) {
            throw new RuntimeException("Reading from a byte array threw an IOException (should never happen).", e2);
        }
    }

    protected static void a(Iterable iterable, Collection collection) {
        for (Object obj : iterable) {
            if (obj == null) {
                throw new NullPointerException();
            }
        }
        if (iterable instanceof Collection) {
            collection.addAll((Collection) iterable);
            return;
        }
        for (Object add : iterable) {
            collection.add(add);
        }
    }
}
