package com.google.iap;

import android.os.Bundle;

final class i extends c {

    /* renamed from: a  reason: collision with root package name */
    private String[] f1129a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ BillingService f1130b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(BillingService billingService, int i, String[] strArr) {
        super(billingService, i);
        this.f1130b = billingService;
        this.f1129a = strArr;
    }

    /* access modifiers changed from: protected */
    public final long a() {
        Bundle a2 = a("CONFIRM_NOTIFICATIONS");
        a2.putStringArray("NOTIFY_IDS", this.f1129a);
        Bundle a3 = BillingService.f1112a.a(a2);
        a(a3);
        return a3.getLong("REQUEST_ID", g.f1126a);
    }
}
