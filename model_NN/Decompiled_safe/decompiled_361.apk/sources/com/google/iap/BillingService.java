package com.google.iap;

import android.app.Activity;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.android.vending.a.b;
import com.android.vending.a.c;
import com.urbanairship.e;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BillingService extends Service implements ServiceConnection {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static b f1112a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static LinkedList f1113b = new LinkedList();
    /* access modifiers changed from: private */
    public static HashMap c = new HashMap();

    private ArrayList a(String str) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray optJSONArray = new JSONObject(str).optJSONArray("orders");
            int length = optJSONArray != null ? optJSONArray.length() : 0;
            int i = 0;
            while (i < length) {
                try {
                    JSONObject jSONObject = optJSONArray.getJSONObject(i);
                    arrayList.add(new a(this, f.a(jSONObject.getInt("purchaseState")), jSONObject.has("notificationId") ? jSONObject.getString("notificationId") : null, jSONObject.getString("productId"), jSONObject.optString("orderId", ""), jSONObject.getLong("purchaseTime"), jSONObject.optString("developerPayload", null)));
                    i++;
                } catch (JSONException e) {
                    e.a("JSON exception: ", e);
                    return null;
                }
            }
            return arrayList;
        } catch (JSONException e2) {
            return null;
        }
    }

    private boolean a(int i, String[] strArr) {
        return new i(this, i, strArr).c();
    }

    /* access modifiers changed from: private */
    public boolean g() {
        try {
            if (bindService(new Intent("com.android.vending.billing.MarketBillingService.BIND"), this, 1)) {
                return true;
            }
            e.e("Could not bind to service.");
            return false;
        } catch (SecurityException e) {
            e.e("Security exception: " + e);
        }
    }

    public final void a(Context context) {
        attachBaseContext(context);
    }

    public final boolean a() {
        return new l(this).c();
    }

    public final boolean a(Activity activity, String str) {
        return new h(this, activity, str, (byte) 0).c();
    }

    public final boolean b() {
        return new b(this).c();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        f1112a = c.a(iBinder);
        int i = -1;
        while (true) {
            c cVar = (c) f1113b.peek();
            if (cVar != null) {
                e.b("Billing Service - running request: " + cVar.d());
                if (cVar.b()) {
                    e.b("Billing Service - request ran: " + cVar.d());
                    f1113b.remove();
                    if (i < cVar.d()) {
                        i = cVar.d();
                    }
                } else {
                    e.c("Billing Service - bind to market service");
                    g();
                    return;
                }
            } else if (i >= 0) {
                stopSelf(i);
                return;
            } else {
                return;
            }
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        e.a("Billing service disconnected");
        f1112a = null;
    }

    public void onStart(Intent intent, int i) {
        String action = intent.getAction();
        if ("com.example.dungeons.CONFIRM_NOTIFICATION".equals(action)) {
            a(i, intent.getStringArrayExtra("notification_id"));
        } else if ("com.example.dungeons.GET_PURCHASE_INFORMATION".equals(action)) {
            new k(this, i, new String[]{intent.getStringExtra("notification_id")}).c();
        } else if ("com.android.vending.billing.PURCHASE_STATE_CHANGED".equals(action)) {
            String stringExtra = intent.getStringExtra("inapp_signed_data");
            String stringExtra2 = intent.getStringExtra("inapp_signature");
            ArrayList a2 = a(stringExtra);
            if (a2 != null) {
                ArrayList arrayList = new ArrayList();
                Iterator it = a2.iterator();
                while (it.hasNext()) {
                    a aVar = (a) it.next();
                    if (aVar.f1115b != null) {
                        arrayList.add(aVar.f1115b);
                    }
                    m.a(aVar.f1114a, aVar.c, aVar.d, aVar.e, stringExtra, stringExtra2);
                }
                if (!arrayList.isEmpty()) {
                    a(i, (String[]) arrayList.toArray(new String[arrayList.size()]));
                }
            }
        } else if ("com.android.vending.billing.RESPONSE_CODE".equals(action)) {
            long longExtra = intent.getLongExtra("request_id", -1);
            e a3 = e.a(intent.getIntExtra("response_code", e.RESULT_ERROR.ordinal()));
            c cVar = (c) c.get(Long.valueOf(longExtra));
            if (cVar != null) {
                cVar.a(a3);
            }
            c.remove(Long.valueOf(longExtra));
        }
    }
}
