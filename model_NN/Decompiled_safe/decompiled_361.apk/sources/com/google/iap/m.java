package com.google.iap;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import com.urbanairship.b.p;
import com.urbanairship.e;

public final class m {

    /* renamed from: a  reason: collision with root package name */
    private static p f1136a;

    public static synchronized void a() {
        synchronized (m.class) {
            f1136a = null;
        }
    }

    public static void a(Activity activity, PendingIntent pendingIntent, Intent intent) {
        if (f1136a != null) {
            f1136a.a(activity, pendingIntent, intent);
        }
    }

    public static void a(e eVar) {
        if (f1136a != null) {
            p.a(eVar);
        }
    }

    public static void a(f fVar, String str, String str2, long j, String str3, String str4) {
        e.b("purchaseResponse");
        if (f1136a != null) {
            p.a(fVar, str, str2, j, str3, str4);
        }
    }

    public static void a(h hVar, e eVar) {
        if (f1136a != null) {
            p.a(hVar, eVar);
        }
    }

    public static synchronized void a(p pVar) {
        synchronized (m.class) {
            f1136a = pVar;
        }
    }

    public static void a(boolean z) {
        if (f1136a != null) {
            p.a(z);
        }
    }
}
