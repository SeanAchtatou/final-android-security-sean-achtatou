package com.google.iap;

import android.os.Bundle;
import android.os.RemoteException;
import com.android.vending.a.b;
import com.urbanairship.e;

abstract class c {

    /* renamed from: a  reason: collision with root package name */
    private final int f1118a;

    /* renamed from: b  reason: collision with root package name */
    private long f1119b;
    private /* synthetic */ BillingService c;

    public c(BillingService billingService, int i) {
        this.c = billingService;
        this.f1118a = i;
    }

    protected static void a(Bundle bundle) {
        e.a(bundle.getInt("RESPONSE_CODE"));
    }

    /* access modifiers changed from: protected */
    public abstract long a();

    /* access modifiers changed from: protected */
    public final Bundle a(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("BILLING_REQUEST", str);
        bundle.putInt("API_VERSION", 1);
        bundle.putString("PACKAGE_NAME", this.c.getPackageName());
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void a(RemoteException remoteException) {
        e.a("remote billing service crashed");
        b unused = BillingService.f1112a = null;
    }

    /* access modifiers changed from: protected */
    public void a(e eVar) {
    }

    public boolean b() {
        if (BillingService.f1112a != null) {
            try {
                this.f1119b = a();
                if (this.f1119b >= 0) {
                    BillingService.c.put(Long.valueOf(this.f1119b), this);
                }
                return true;
            } catch (RemoteException e) {
                a(e);
            }
        }
        return false;
    }

    public boolean c() {
        if (b()) {
            return true;
        }
        if (!this.c.g()) {
            return false;
        }
        BillingService.f1113b.add(this);
        return true;
    }

    public int d() {
        return this.f1118a;
    }
}
