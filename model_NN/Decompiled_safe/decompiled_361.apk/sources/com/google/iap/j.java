package com.google.iap;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Handler;
import com.urbanairship.e;
import java.lang.reflect.Method;

public abstract class j {
    private static final Class[] d = {IntentSender.class, Intent.class, Integer.TYPE, Integer.TYPE, Integer.TYPE};

    /* renamed from: a  reason: collision with root package name */
    private final Handler f1131a;

    /* renamed from: b  reason: collision with root package name */
    private Method f1132b;
    private Object[] c = new Object[5];

    public j(Handler handler) {
        this.f1131a = handler;
    }

    public final void a(Activity activity, PendingIntent pendingIntent, Intent intent) {
        try {
            this.f1132b = activity.getClass().getMethod("startIntentSender", d);
        } catch (SecurityException e) {
            this.f1132b = null;
        } catch (NoSuchMethodException e2) {
            this.f1132b = null;
        }
        if (this.f1132b != null) {
            try {
                this.c[0] = pendingIntent.getIntentSender();
                this.c[1] = intent;
                this.c[2] = 0;
                this.c[3] = 0;
                this.c[4] = 0;
                this.f1132b.invoke(activity, this.c);
            } catch (Exception e3) {
                e.a("error starting buy page activity", e3);
            }
        } else {
            try {
                pendingIntent.send(activity, 0, intent);
            } catch (PendingIntent.CanceledException e4) {
                e.a("error starting buy page activity", e4);
            }
        }
    }
}
