package com.google.iap;

import android.os.Bundle;
import android.os.RemoteException;

final class k extends c {

    /* renamed from: a  reason: collision with root package name */
    private long f1133a;

    /* renamed from: b  reason: collision with root package name */
    private String[] f1134b;
    private /* synthetic */ BillingService c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(BillingService billingService, int i, String[] strArr) {
        super(billingService, i);
        this.c = billingService;
        this.f1134b = strArr;
    }

    /* access modifiers changed from: protected */
    public final long a() {
        this.f1133a = d.a();
        Bundle a2 = a("GET_PURCHASE_INFORMATION");
        a2.putLong("NONCE", this.f1133a);
        a2.putStringArray("NOTIFY_IDS", this.f1134b);
        Bundle a3 = BillingService.f1112a.a(a2);
        a(a3);
        return a3.getLong("REQUEST_ID", g.f1126a);
    }

    /* access modifiers changed from: protected */
    public final void a(RemoteException remoteException) {
        super.a(remoteException);
        d.a(this.f1133a);
    }
}
