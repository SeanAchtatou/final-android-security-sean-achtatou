package com.google.iap;

import java.security.SecureRandom;
import java.util.HashSet;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static final SecureRandom f1120a = new SecureRandom();

    /* renamed from: b  reason: collision with root package name */
    private static HashSet f1121b = new HashSet();

    public static long a() {
        long nextLong = f1120a.nextLong();
        f1121b.add(Long.valueOf(nextLong));
        return nextLong;
    }

    public static void a(long j) {
        f1121b.remove(Long.valueOf(j));
    }
}
