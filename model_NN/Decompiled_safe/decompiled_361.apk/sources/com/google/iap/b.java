package com.google.iap;

import android.os.Bundle;
import android.os.RemoteException;

public final class b extends c {

    /* renamed from: a  reason: collision with root package name */
    private long f1116a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ BillingService f1117b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(BillingService billingService) {
        super(billingService, -1);
        this.f1117b = billingService;
    }

    /* access modifiers changed from: protected */
    public final long a() {
        this.f1116a = d.a();
        Bundle a2 = a("RESTORE_TRANSACTIONS");
        a2.putLong("NONCE", this.f1116a);
        Bundle a3 = BillingService.f1112a.a(a2);
        a(a3);
        return a3.getLong("REQUEST_ID", g.f1126a);
    }

    /* access modifiers changed from: protected */
    public final void a(RemoteException remoteException) {
        super.a(remoteException);
        d.a(this.f1116a);
    }

    /* access modifiers changed from: protected */
    public final void a(e eVar) {
        m.a(eVar);
    }

    public final /* bridge */ /* synthetic */ boolean b() {
        return super.b();
    }

    public final /* bridge */ /* synthetic */ boolean c() {
        return super.c();
    }

    public final /* bridge */ /* synthetic */ int d() {
        return super.d();
    }
}
