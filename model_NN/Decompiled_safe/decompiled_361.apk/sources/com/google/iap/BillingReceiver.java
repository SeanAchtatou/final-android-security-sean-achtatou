package com.google.iap;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.urbanairship.e;

public class BillingReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        e.c("Recieved: " + action);
        if ("com.android.vending.billing.PURCHASE_STATE_CHANGED".equals(action)) {
            String stringExtra = intent.getStringExtra("inapp_signed_data");
            String stringExtra2 = intent.getStringExtra("inapp_signature");
            if (stringExtra != null && stringExtra2 != null) {
                Intent intent2 = new Intent("com.android.vending.billing.PURCHASE_STATE_CHANGED");
                intent2.setClass(context, BillingService.class);
                intent2.putExtra("inapp_signed_data", stringExtra);
                intent2.putExtra("inapp_signature", stringExtra2);
                context.startService(intent2);
            }
        } else if ("com.android.vending.billing.IN_APP_NOTIFY".equals(action)) {
            String stringExtra3 = intent.getStringExtra("notification_id");
            Intent intent3 = new Intent("com.example.dungeons.GET_PURCHASE_INFORMATION");
            intent3.setClass(context, BillingService.class);
            intent3.putExtra("notification_id", stringExtra3);
            context.startService(intent3);
        } else if ("com.android.vending.billing.RESPONSE_CODE".equals(action)) {
            long longExtra = intent.getLongExtra("request_id", -1);
            int intExtra = intent.getIntExtra("response_code", e.RESULT_ERROR.ordinal());
            Intent intent4 = new Intent("com.android.vending.billing.RESPONSE_CODE");
            intent4.setClass(context, BillingService.class);
            intent4.putExtra("request_id", longExtra);
            intent4.putExtra("response_code", intExtra);
            context.startService(intent4);
        } else {
            e.a("unexpected action: " + action);
        }
    }
}
