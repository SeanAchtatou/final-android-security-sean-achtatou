package com.google.iap;

public enum e {
    RESULT_OK,
    RESULT_USER_CANCELED,
    RESULT_SERVICE_UNAVAILABLE,
    RESULT_BILLING_UNAVAILABLE,
    RESULT_ITEM_UNAVAILABLE,
    RESULT_DEVELOPER_ERROR,
    RESULT_ERROR;

    public static e a(int i) {
        e[] values = values();
        return (i < 0 || i >= values.length) ? RESULT_ERROR : values[i];
    }
}
