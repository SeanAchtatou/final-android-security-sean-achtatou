package com.google.iap;

public enum f {
    PURCHASED,
    CANCELED,
    REFUNDED;

    public static f a(int i) {
        f[] values = values();
        return (i < 0 || i >= values.length) ? CANCELED : values[i];
    }
}
