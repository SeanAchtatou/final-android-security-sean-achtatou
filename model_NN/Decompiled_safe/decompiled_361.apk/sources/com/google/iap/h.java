package com.google.iap;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import com.urbanairship.e;

public final class h extends c {

    /* renamed from: a  reason: collision with root package name */
    public final String f1127a;

    /* renamed from: b  reason: collision with root package name */
    private String f1128b;
    private Activity c;
    private /* synthetic */ BillingService d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private h(BillingService billingService, Activity activity, String str) {
        super(billingService, -1);
        this.d = billingService;
        this.f1127a = str;
        this.f1128b = null;
        this.c = activity;
        e.c("Request Purchase for: " + str);
    }

    public h(BillingService billingService, Activity activity, String str, byte b2) {
        this(billingService, activity, str);
    }

    /* access modifiers changed from: protected */
    public final long a() {
        Bundle a2 = a("REQUEST_PURCHASE");
        a2.putString("ITEM_ID", this.f1127a);
        if (this.f1128b != null) {
            a2.putString("DEVELOPER_PAYLOAD", this.f1128b);
        }
        Bundle a3 = BillingService.f1112a.a(a2);
        PendingIntent pendingIntent = (PendingIntent) a3.getParcelable("PURCHASE_INTENT");
        if (pendingIntent == null) {
            e.e("Error with requestPurchase");
            return g.f1126a;
        }
        m.a(this.c, pendingIntent, new Intent());
        return a3.getLong("REQUEST_ID", g.f1126a);
    }

    /* access modifiers changed from: protected */
    public final void a(e eVar) {
        m.a(this, eVar);
    }

    public final /* bridge */ /* synthetic */ boolean b() {
        return super.b();
    }

    public final /* bridge */ /* synthetic */ boolean c() {
        return super.c();
    }

    public final /* bridge */ /* synthetic */ int d() {
        return super.d();
    }
}
