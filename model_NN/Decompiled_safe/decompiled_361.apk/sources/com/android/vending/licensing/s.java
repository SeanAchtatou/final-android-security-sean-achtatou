package com.android.vending.licensing;

import android.util.Log;

final class s implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ int f150a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ String f151b;
    private /* synthetic */ String c;
    private /* synthetic */ c d;

    s(c cVar, int i, String str, String str2) {
        this.d = cVar;
        this.f150a = i;
        this.f151b = str;
        this.c = str2;
    }

    public final void run() {
        Log.i("LicenseChecker", "Received response.");
        if (this.d.f134a.i.contains(this.d.f135b)) {
            c.b(this.d);
            this.d.f135b.a(this.d.f134a.c, this.f150a, this.f151b, this.c);
            this.d.f134a.a(this.d.f135b);
        }
    }
}
