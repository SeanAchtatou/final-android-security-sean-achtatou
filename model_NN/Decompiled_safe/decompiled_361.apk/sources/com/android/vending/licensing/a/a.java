package com.android.vending.licensing.a;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f132a = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};

    /* renamed from: b  reason: collision with root package name */
    private static final byte[] f133b = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] c = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, -9, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, -9, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9, -9};
    private static final byte[] d = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, 63, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9, -9};
    private static /* synthetic */ boolean e;

    static {
        boolean z;
        if (!a.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        e = z;
    }

    private a() {
    }

    public static String a(byte[] bArr) {
        int i;
        int i2;
        int length = bArr.length;
        byte[] bArr2 = f132a;
        int i3 = ((length + 2) / 3) * 4;
        byte[] bArr3 = new byte[(i3 + (i3 / Integer.MAX_VALUE))];
        int i4 = length - 2;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        while (i7 < i4) {
            int i8 = ((bArr[i7 + 0] << 24) >>> 8) | ((bArr[(i7 + 1) + 0] << 24) >>> 16) | ((bArr[(i7 + 2) + 0] << 24) >>> 24);
            bArr3[i6] = bArr2[i8 >>> 18];
            bArr3[i6 + 1] = bArr2[(i8 >>> 12) & 63];
            bArr3[i6 + 2] = bArr2[(i8 >>> 6) & 63];
            bArr3[i6 + 3] = bArr2[i8 & 63];
            i5 += 4;
            if (i5 == Integer.MAX_VALUE) {
                bArr3[i6 + 4] = 10;
                i6++;
                i5 = 0;
            }
            i7 += 3;
            i6 += 4;
        }
        if (i7 < length) {
            int i9 = i7 + 0;
            int i10 = length - i7;
            int i11 = (i10 > 2 ? (bArr[i9 + 2] << 24) >>> 24 : 0) | (i10 > 0 ? (bArr[i9] << 24) >>> 8 : 0) | (i10 > 1 ? (bArr[i9 + 1] << 24) >>> 16 : 0);
            switch (i10) {
                case 1:
                    bArr3[i6] = bArr2[i11 >>> 18];
                    bArr3[i6 + 1] = bArr2[(i11 >>> 12) & 63];
                    bArr3[i6 + 2] = 61;
                    bArr3[i6 + 3] = 61;
                    break;
                case 2:
                    bArr3[i6] = bArr2[i11 >>> 18];
                    bArr3[i6 + 1] = bArr2[(i11 >>> 12) & 63];
                    bArr3[i6 + 2] = bArr2[(i11 >>> 6) & 63];
                    bArr3[i6 + 3] = 61;
                    break;
                case 3:
                    bArr3[i6] = bArr2[i11 >>> 18];
                    bArr3[i6 + 1] = bArr2[(i11 >>> 12) & 63];
                    bArr3[i6 + 2] = bArr2[(i11 >>> 6) & 63];
                    bArr3[i6 + 3] = bArr2[i11 & 63];
                    break;
            }
            if (i5 + 4 == Integer.MAX_VALUE) {
                bArr3[i6 + 4] = 10;
                i2 = i6 + 1;
            } else {
                i2 = i6;
            }
            i = i2 + 4;
        } else {
            i = i6;
        }
        if (e || i == bArr3.length) {
            return new String(bArr3, 0, bArr3.length);
        }
        throw new AssertionError();
    }

    private static int a(byte[] bArr, byte[] bArr2, int i, byte[] bArr3) {
        if (bArr[2] == 61) {
            bArr2[i] = (byte) ((((bArr3[bArr[0]] << 24) >>> 6) | ((bArr3[bArr[1]] << 24) >>> 12)) >>> 16);
            return 1;
        } else if (bArr[3] == 61) {
            int i2 = ((bArr3[bArr[0]] << 24) >>> 6) | ((bArr3[bArr[1]] << 24) >>> 12) | ((bArr3[bArr[2]] << 24) >>> 18);
            bArr2[i] = (byte) (i2 >>> 16);
            bArr2[i + 1] = (byte) (i2 >>> 8);
            return 2;
        } else {
            int i3 = ((bArr3[bArr[0]] << 24) >>> 6) | ((bArr3[bArr[1]] << 24) >>> 12) | ((bArr3[bArr[2]] << 24) >>> 18) | ((bArr3[bArr[3]] << 24) >>> 24);
            bArr2[i] = (byte) (i3 >> 16);
            bArr2[i + 1] = (byte) (i3 >> 8);
            bArr2[i + 2] = (byte) i3;
            return 3;
        }
    }

    public static byte[] a(String str) {
        int i;
        byte[] bytes = str.getBytes();
        int length = bytes.length;
        byte[] bArr = c;
        byte[] bArr2 = new byte[(((length * 3) / 4) + 2)];
        byte[] bArr3 = new byte[4];
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if (i2 >= length) {
                break;
            }
            byte b2 = (byte) (bytes[i2 + 0] & Byte.MAX_VALUE);
            byte b3 = bArr[b2];
            if (b3 >= -5) {
                if (b3 >= -1) {
                    if (b2 == 61) {
                        int i5 = length - i2;
                        byte b4 = (byte) (bytes[(length - 1) + 0] & Byte.MAX_VALUE);
                        if (i3 == 0 || i3 == 1) {
                            throw new b("invalid padding byte '=' at byte offset " + i2);
                        } else if ((i3 == 3 && i5 > 2) || (i3 == 4 && i5 > 1)) {
                            throw new b("padding byte '=' falsely signals end of encoded value at offset " + i2);
                        } else if (b4 != 61 && b4 != 10) {
                            throw new b("encoded value has invalid trailing byte");
                        }
                    } else {
                        int i6 = i3 + 1;
                        bArr3[i3] = b2;
                        if (i6 == 4) {
                            i4 = a(bArr3, bArr2, i4, bArr) + i4;
                            i3 = 0;
                        } else {
                            i3 = i6;
                        }
                    }
                }
                i2++;
            } else {
                throw new b("Bad Base64 input character at " + i2 + ": " + ((int) bytes[i2 + 0]) + "(decimal)");
            }
        }
        if (i3 == 0) {
            i = i4;
        } else if (i3 == 1) {
            throw new b("single trailing character at offset " + (length - 1));
        } else {
            bArr3[i3] = 61;
            i = a(bArr3, bArr2, i4, bArr) + i4;
        }
        byte[] bArr4 = new byte[i];
        System.arraycopy(bArr2, 0, bArr4, 0, i);
        return bArr4;
    }
}
