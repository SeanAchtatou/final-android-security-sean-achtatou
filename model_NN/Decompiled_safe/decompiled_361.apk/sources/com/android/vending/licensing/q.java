package com.android.vending.licensing;

import android.os.IBinder;
import android.os.Parcel;

final class q implements o {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f147a;

    q(IBinder iBinder) {
        this.f147a = iBinder;
    }

    public final IBinder asBinder() {
        return this.f147a;
    }

    public final void a(int i, String str, String str2) {
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.android.vending.licensing.ILicenseResultListener");
            obtain.writeInt(i);
            obtain.writeString(str);
            obtain.writeString(str2);
            this.f147a.transact(1, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }
}
