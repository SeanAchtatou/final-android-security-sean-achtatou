package com.android.vending.licensing;

import android.content.SharedPreferences;
import android.util.Log;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private final SharedPreferences f140a;

    /* renamed from: b  reason: collision with root package name */
    private final d f141b;
    private SharedPreferences.Editor c = null;

    public h(SharedPreferences sharedPreferences, d dVar) {
        this.f140a = sharedPreferences;
        this.f141b = dVar;
    }

    public final void a(String str, String str2) {
        if (this.c == null) {
            this.c = this.f140a.edit();
        }
        this.c.putString(str, this.f141b.a(str2));
    }

    public final String b(String str, String str2) {
        String string = this.f140a.getString(str, null);
        if (string != null) {
            try {
                return this.f141b.b(string);
            } catch (b e) {
                Log.w("PreferenceObfuscator", "Validation error while reading preference: " + str);
            }
        }
        return str2;
    }

    public final void a() {
        if (this.c != null) {
            this.c.commit();
            this.c = null;
        }
    }
}
