package com.android.vending.licensing;

import android.content.Context;
import android.util.Log;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public final class i implements k {

    /* renamed from: a  reason: collision with root package name */
    private long f142a = Long.parseLong(this.g.b("validityTimestamp", "0"));

    /* renamed from: b  reason: collision with root package name */
    private long f143b = Long.parseLong(this.g.b("retryUntil", "0"));
    private long c = Long.parseLong(this.g.b("maxRetries", "0"));
    private long d = Long.parseLong(this.g.b("retryCount", "0"));
    private long e;
    private f f = f.valueOf(this.g.b("lastResponse", f.RETRY.toString()));
    private h g;

    public i(Context context, d dVar) {
        this.g = new h(context.getSharedPreferences("com.android.vending.licensing.ServerManagedPolicy", 0), dVar);
    }

    public final void a(f fVar, t tVar) {
        if (fVar != f.RETRY) {
            a(0);
        } else {
            a(this.d + 1);
        }
        if (fVar == f.LICENSED) {
            Map d2 = d(tVar.g);
            this.f = fVar;
            a((String) d2.get("VT"));
            b((String) d2.get("GT"));
            c((String) d2.get("GR"));
        } else if (fVar == f.NOT_LICENSED) {
            a("0");
            b("0");
            c("0");
        }
        this.e = System.currentTimeMillis();
        this.f = fVar;
        this.g.a("lastResponse", fVar.toString());
        this.g.a();
    }

    private void a(long j) {
        this.d = j;
        this.g.a("retryCount", Long.toString(j));
    }

    private void a(String str) {
        Long valueOf;
        String l;
        try {
            valueOf = Long.valueOf(Long.parseLong(str));
            l = str;
        } catch (NumberFormatException e2) {
            Log.w("ServerManagedPolicy", "License validity timestamp (VT) missing, caching for a minute");
            valueOf = Long.valueOf(System.currentTimeMillis() + 60000);
            l = Long.toString(valueOf.longValue());
        }
        this.f142a = valueOf.longValue();
        this.g.a("validityTimestamp", l);
    }

    private void b(String str) {
        String str2;
        Long l;
        try {
            l = Long.valueOf(Long.parseLong(str));
            str2 = str;
        } catch (NumberFormatException e2) {
            Log.w("ServerManagedPolicy", "License retry timestamp (GT) missing, grace period disabled");
            str2 = "0";
            l = null;
        }
        this.f143b = l.longValue();
        this.g.a("retryUntil", str2);
    }

    private void c(String str) {
        String str2;
        Long l;
        try {
            l = Long.valueOf(Long.parseLong(str));
            str2 = str;
        } catch (NumberFormatException e2) {
            Log.w("ServerManagedPolicy", "Licence retry count (GR) missing, grace period disabled");
            str2 = "0";
            l = null;
        }
        this.c = l.longValue();
        this.g.a("maxRetries", str2);
    }

    public final boolean a() {
        long currentTimeMillis = System.currentTimeMillis();
        if (this.f == f.LICENSED) {
            if (currentTimeMillis <= this.f142a) {
                return true;
            }
        } else if (this.f == f.RETRY && currentTimeMillis < this.e + 60000) {
            return currentTimeMillis <= this.f143b || this.d <= this.c;
        }
        return false;
    }

    private static Map d(String str) {
        HashMap hashMap = new HashMap();
        try {
            for (NameValuePair nameValuePair : URLEncodedUtils.parse(new URI('?' + str), "UTF-8")) {
                hashMap.put(nameValuePair.getName(), nameValuePair.getValue());
            }
        } catch (URISyntaxException e2) {
            Log.w("ServerManagedPolicy", "Invalid syntax error while decoding extras data from server.");
        }
        return hashMap;
    }
}
