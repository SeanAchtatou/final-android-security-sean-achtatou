package com.android.vending.licensing;

import android.util.Log;

final class c extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ u f134a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final m f135b;
    private Runnable c;

    static /* synthetic */ void b(c cVar) {
        Log.i("LicenseChecker", "Clearing timeout.");
        cVar.f134a.f.removeCallbacks(cVar.c);
    }

    public c(u uVar, m mVar) {
        this.f134a = uVar;
        this.f135b = mVar;
        this.c = new r(this, uVar);
        Log.i("LicenseChecker", "Start monitoring timeout.");
        this.f134a.f.postDelayed(this.c, 10000);
    }

    public final void a(int i, String str, String str2) {
        this.f134a.f.post(new s(this, i, str, str2));
    }
}
