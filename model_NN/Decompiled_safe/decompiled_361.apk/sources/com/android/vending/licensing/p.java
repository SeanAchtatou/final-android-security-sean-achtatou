package com.android.vending.licensing;

import android.os.IBinder;
import android.os.Parcel;

final class p implements ILicensingService {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f146a;

    p(IBinder iBinder) {
        this.f146a = iBinder;
    }

    public final IBinder asBinder() {
        return this.f146a;
    }

    public final void a(long j, String str, o oVar) {
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.android.vending.licensing.ILicensingService");
            obtain.writeLong(j);
            obtain.writeString(str);
            obtain.writeStrongBinder(oVar != null ? oVar.asBinder() : null);
            this.f146a.transact(1, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }
}
