package com.android.vending.licensing;

import android.text.TextUtils;

final class t {

    /* renamed from: a  reason: collision with root package name */
    public int f152a;

    /* renamed from: b  reason: collision with root package name */
    public int f153b;
    public String c;
    public String d;
    public String e;
    public long f;
    public String g;

    t() {
    }

    public final String toString() {
        return TextUtils.join("|", new Object[]{Integer.valueOf(this.f152a), Integer.valueOf(this.f153b), this.c, this.d, this.e, Long.valueOf(this.f)});
    }
}
