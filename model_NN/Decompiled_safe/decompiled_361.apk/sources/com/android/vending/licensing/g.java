package com.android.vending.licensing;

import com.android.vending.licensing.a.a;
import com.android.vending.licensing.a.b;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public final class g implements d {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f138a = {16, 74, 71, -80, 32, 101, -47, 72, 117, -14, 0, -29, 70, 65, -12, 74};

    /* renamed from: b  reason: collision with root package name */
    private Cipher f139b;
    private Cipher c;

    public g(byte[] bArr, String str, String str2) {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(SecretKeyFactory.getInstance("PBEWITHSHAAND256BITAES-CBC-BC").generateSecret(new PBEKeySpec((str + str2).toCharArray(), bArr, 1024, 256)).getEncoded(), "AES");
            this.f139b = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.f139b.init(1, secretKeySpec, new IvParameterSpec(f138a));
            this.c = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.c.init(2, secretKeySpec, new IvParameterSpec(f138a));
        } catch (GeneralSecurityException e) {
            throw new RuntimeException("Invalid environment", e);
        }
    }

    public final String a(String str) {
        if (str == null) {
            return null;
        }
        try {
            return a.a(this.f139b.doFinal(("com.android.vending.licensing.AESObfuscator-1|" + str).getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Invalid environment", e);
        } catch (GeneralSecurityException e2) {
            throw new RuntimeException("Invalid environment", e2);
        }
    }

    public final String b(String str) {
        if (str == null) {
            return null;
        }
        try {
            String str2 = new String(this.c.doFinal(a.a(str)), "UTF-8");
            if (str2.indexOf("com.android.vending.licensing.AESObfuscator-1|") == 0) {
                return str2.substring("com.android.vending.licensing.AESObfuscator-1|".length(), str2.length());
            }
            throw new b("Header not found (invalid data or key):" + str);
        } catch (b e) {
            throw new b(e.getMessage() + ':' + str);
        } catch (IllegalBlockSizeException e2) {
            throw new b(e2.getMessage() + ':' + str);
        } catch (BadPaddingException e3) {
            throw new b(e3.getMessage() + ':' + str);
        } catch (UnsupportedEncodingException e4) {
            throw new RuntimeException("Invalid environment", e4);
        }
    }
}
