package com.android.vending.licensing;

import android.text.TextUtils;
import android.util.Log;
import com.android.vending.licensing.a.a;
import com.android.vending.licensing.a.b;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Iterator;
import java.util.regex.Pattern;

final class m {

    /* renamed from: a  reason: collision with root package name */
    private final k f144a;

    /* renamed from: b  reason: collision with root package name */
    private final e f145b;
    private final int c;
    private final String d;
    private final String e;
    private final a f;

    m(k kVar, a aVar, e eVar, int i, String str, String str2) {
        this.f144a = kVar;
        this.f = aVar;
        this.f145b = eVar;
        this.c = i;
        this.d = str;
        this.e = str2;
    }

    public final e a() {
        return this.f145b;
    }

    public final int b() {
        return this.c;
    }

    public final String c() {
        return this.d;
    }

    public final void a(PublicKey publicKey, int i, String str, String str2) {
        t tVar = null;
        if (i == 0 || i == 1 || i == 2) {
            try {
                Signature instance = Signature.getInstance("SHA1withRSA");
                instance.initVerify(publicKey);
                instance.update(str.getBytes());
                if (!instance.verify(a.a(str2))) {
                    Log.e("LicenseValidator", "Signature verification failed.");
                    d();
                    return;
                }
                try {
                    TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(':');
                    simpleStringSplitter.setString(str);
                    Iterator it = simpleStringSplitter.iterator();
                    if (!it.hasNext()) {
                        throw new IllegalArgumentException("Blank response.");
                    }
                    String str3 = (String) it.next();
                    String str4 = it.hasNext() ? (String) it.next() : "";
                    String[] split = TextUtils.split(str3, Pattern.quote("|"));
                    if (split.length < 6) {
                        throw new IllegalArgumentException("Wrong number of fields.");
                    }
                    t tVar2 = new t();
                    tVar2.g = str4;
                    tVar2.f152a = Integer.parseInt(split[0]);
                    tVar2.f153b = Integer.parseInt(split[1]);
                    tVar2.c = split[2];
                    tVar2.d = split[3];
                    tVar2.e = split[4];
                    tVar2.f = Long.parseLong(split[5]);
                    if (tVar2.f152a != i) {
                        Log.e("LicenseValidator", "Response codes don't match.");
                        d();
                        return;
                    } else if (tVar2.f153b != this.c) {
                        Log.e("LicenseValidator", "Nonce doesn't match.");
                        d();
                        return;
                    } else if (!tVar2.c.equals(this.d)) {
                        Log.e("LicenseValidator", "Package name doesn't match.");
                        d();
                        return;
                    } else if (!tVar2.d.equals(this.e)) {
                        Log.e("LicenseValidator", "Version codes don't match.");
                        d();
                        return;
                    } else if (TextUtils.isEmpty(tVar2.e)) {
                        Log.e("LicenseValidator", "User identifier is empty.");
                        d();
                        return;
                    } else {
                        tVar = tVar2;
                    }
                } catch (IllegalArgumentException e2) {
                    Log.e("LicenseValidator", "Could not parse response.");
                    d();
                    return;
                }
            } catch (NoSuchAlgorithmException e3) {
                throw new RuntimeException(e3);
            } catch (InvalidKeyException e4) {
                a(v.INVALID_PUBLIC_KEY);
                return;
            } catch (SignatureException e5) {
                throw new RuntimeException(e5);
            } catch (b e6) {
                Log.e("LicenseValidator", "Could not Base64-decode signature.");
                d();
                return;
            }
        }
        switch (i) {
            case 0:
            case 2:
                a(this.f.a(), tVar);
                return;
            case 1:
                a(f.NOT_LICENSED, tVar);
                return;
            case 3:
                a(v.NOT_MARKET_MANAGED);
                return;
            case 4:
                Log.w("LicenseValidator", "An error has occurred on the licensing server.");
                a(f.RETRY, tVar);
                return;
            case 5:
                Log.w("LicenseValidator", "Licensing server is refusing to talk to this device, over quota.");
                a(f.RETRY, tVar);
                return;
            case 257:
                Log.w("LicenseValidator", "Error contacting licensing server.");
                a(f.RETRY, tVar);
                return;
            case 258:
                a(v.INVALID_PACKAGE_NAME);
                return;
            case 259:
                a(v.NON_MATCHING_UID);
                return;
            default:
                Log.e("LicenseValidator", "Unknown response code for license check.");
                d();
                return;
        }
    }

    private void a(f fVar, t tVar) {
        this.f144a.a(fVar, tVar);
        if (this.f144a.a()) {
            this.f145b.a();
        } else {
            this.f145b.b();
        }
    }

    private void a(v vVar) {
        this.f145b.a(vVar);
    }

    private void d() {
        this.f145b.b();
    }
}
