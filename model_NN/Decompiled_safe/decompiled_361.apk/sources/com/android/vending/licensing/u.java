package com.android.vending.licensing;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.util.Log;
import com.android.vending.licensing.a.a;
import com.android.vending.licensing.a.b;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public final class u implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    private static final SecureRandom f154a = new SecureRandom();

    /* renamed from: b  reason: collision with root package name */
    private ILicensingService f155b;
    /* access modifiers changed from: private */
    public PublicKey c;
    private final Context d;
    private final k e;
    /* access modifiers changed from: private */
    public Handler f;
    private final String g;
    private final String h;
    /* access modifiers changed from: private */
    public final Set i = new HashSet();
    private final Queue j = new LinkedList();

    public u(Context context, k kVar, String str) {
        this.d = context;
        this.e = kVar;
        this.c = a(str);
        this.g = this.d.getPackageName();
        this.h = a(context, this.g);
        HandlerThread handlerThread = new HandlerThread("background thread");
        handlerThread.start();
        this.f = new Handler(handlerThread.getLooper());
    }

    private static PublicKey a(String str) {
        try {
            return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(a.a(str)));
        } catch (NoSuchAlgorithmException e2) {
            throw new RuntimeException(e2);
        } catch (b e3) {
            Log.e("LicenseChecker", "Could not decode from Base64.");
            throw new IllegalArgumentException(e3);
        } catch (InvalidKeySpecException e4) {
            Log.e("LicenseChecker", "Invalid key specification.");
            throw new IllegalArgumentException(e4);
        }
    }

    public final synchronized void a(e eVar) {
        if (this.e.a()) {
            Log.i("LicenseChecker", "Using cached license response");
            eVar.a();
        } else {
            m mVar = new m(this.e, new j(), eVar, f154a.nextInt(), this.g, this.h);
            if (this.f155b == null) {
                Log.i("LicenseChecker", "Binding to licensing service.");
                try {
                    if (this.d.bindService(new Intent(ILicensingService.class.getName()), this, 1)) {
                        this.j.offer(mVar);
                    } else {
                        Log.e("LicenseChecker", "Could not bind to service.");
                        b(mVar);
                    }
                } catch (SecurityException e2) {
                    eVar.a(v.MISSING_PERMISSION);
                }
            } else {
                this.j.offer(mVar);
                b();
            }
        }
        return;
    }

    private void b() {
        while (true) {
            m mVar = (m) this.j.poll();
            if (mVar != null) {
                try {
                    Log.i("LicenseChecker", "Calling checkLicense on service for " + mVar.c());
                    this.f155b.a((long) mVar.b(), mVar.c(), new c(this, mVar));
                    this.i.add(mVar);
                } catch (RemoteException e2) {
                    Log.w("LicenseChecker", "RemoteException in checkLicense call.", e2);
                    b(mVar);
                }
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(m mVar) {
        this.i.remove(mVar);
        if (this.i.isEmpty()) {
            c();
        }
    }

    public final synchronized void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        ILicensingService pVar;
        if (iBinder == null) {
            pVar = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.android.vending.licensing.ILicensingService");
            pVar = (queryLocalInterface == null || !(queryLocalInterface instanceof ILicensingService)) ? new p(iBinder) : (ILicensingService) queryLocalInterface;
        }
        this.f155b = pVar;
        b();
    }

    public final synchronized void onServiceDisconnected(ComponentName componentName) {
        Log.w("LicenseChecker", "Service unexpectedly disconnected.");
        this.f155b = null;
    }

    /* access modifiers changed from: private */
    public synchronized void b(m mVar) {
        this.e.a(f.RETRY, null);
        if (this.e.a()) {
            mVar.a().a();
        } else {
            mVar.a().b();
        }
    }

    private void c() {
        if (this.f155b != null) {
            try {
                this.d.unbindService(this);
            } catch (IllegalArgumentException e2) {
                Log.e("LicenseChecker", "Unable to unbind from licensing service (already unbound)");
            }
            this.f155b = null;
        }
    }

    public final synchronized void a() {
        c();
        this.f.getLooper().quit();
    }

    private static String a(Context context, String str) {
        try {
            return String.valueOf(context.getPackageManager().getPackageInfo(str, 0).versionCode);
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e("LicenseChecker", "Package not found. could not get version code.");
            return "";
        }
    }
}
