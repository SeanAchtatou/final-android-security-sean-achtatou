package com.urbanairship;

import android.util.Log;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    public static int f1224a = 6;

    /* renamed from: b  reason: collision with root package name */
    public static String f1225b = "UrbanAirship";

    private e() {
    }

    public static void a(String str) {
        if (f1224a <= 5) {
            Log.w(f1225b, str);
        }
    }

    public static void a(String str, Throwable th) {
        if (f1224a <= 6) {
            Log.e(f1225b, str, th);
        }
    }

    public static void a(Throwable th) {
        if (f1224a <= 6) {
            Log.e(f1225b, null, th);
        }
    }

    public static void b(String str) {
        if (f1224a <= 2) {
            Log.v(f1225b, str);
        }
    }

    public static void c(String str) {
        if (f1224a <= 3) {
            Log.d(f1225b, str);
        }
    }

    public static void d(String str) {
        if (f1224a <= 4) {
            Log.i(f1225b, str);
        }
    }

    public static void e(String str) {
        if (f1224a <= 6) {
            Log.e(f1225b, str);
        }
    }
}
