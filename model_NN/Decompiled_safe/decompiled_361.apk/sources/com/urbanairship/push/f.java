package com.urbanairship.push;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.urbanairship.CoreReceiver;
import com.urbanairship.analytics.l;
import com.urbanairship.c;
import com.urbanairship.d;
import com.urbanairship.e;
import com.urbanairship.push.b.n;
import com.urbanairship.push.c2dm.a;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private static final f f1279a = new f();

    /* renamed from: b  reason: collision with root package name */
    private b f1280b;
    private Class c;
    private a d = new a(this);
    /* access modifiers changed from: private */
    public d e = new d();

    private f() {
        c.a().g().registerReceiver(this.d, new IntentFilter("com.urbanairship.push.UPDATE_APID"));
        this.f1280b = new g();
    }

    public static void a() {
        e.b("PushManager init");
        c();
    }

    public static void a(String str, String str2, Map map) {
        Notification a2;
        String uuid = str2 == null ? UUID.randomUUID().toString() : str2;
        c.a().k().a(new l(uuid));
        if (map.containsKey("com.urbanairship.push.PING")) {
            e.b("Received UA Ping.");
            return;
        }
        Context g = c.a().g();
        Intent intent = new Intent("com.urbanairship.push.PUSH_RECEIVED");
        b bVar = f1279a.f1280b;
        if (!(bVar == null || (a2 = bVar.a(str)) == null)) {
            int a3 = bVar.a();
            intent.putExtra("com.urbanairship.push.NOTIFICATION_ID", a3);
            if (a2.contentIntent == null) {
                Intent intent2 = new Intent("com.urbanairship.push.NOTIFICATION_OPENED_PROXY." + UUID.randomUUID().toString());
                intent2.setClass(c.a().g(), CoreReceiver.class);
                for (String str3 : map.keySet()) {
                    intent2.putExtra(str3, (String) map.get(str3));
                }
                intent2.putExtra("com.urbanairship.push.ALERT", str);
                intent2.putExtra("com.urbanairship.push.PUSH_ID", uuid);
                a2.contentIntent = PendingIntent.getBroadcast(c.a().g(), 0, intent2, 0);
            }
            ((NotificationManager) g.getSystemService("notification")).notify(a3, a2);
        }
        Class cls = f1279a.c;
        if (cls != null) {
            intent.setClass(c.a().g(), cls);
            for (String str4 : map.keySet()) {
                intent.putExtra(str4, (String) map.get(str4));
            }
            intent.putExtra("com.urbanairship.push.ALERT", str);
            intent.putExtra("com.urbanairship.push.PUSH_ID", uuid);
            g.sendBroadcast(intent);
        }
    }

    public static f b() {
        return f1279a;
    }

    public static void c() {
        e.b("PushManager startService");
        Context g = c.a().g();
        Intent intent = new Intent(g, PushService.class);
        intent.setAction("com.urbanairship.push.START");
        g.startService(intent);
    }

    public static void d() {
        e.b("PushManager stopService");
        Context g = c.a().g();
        Intent intent = new Intent(g, PushService.class);
        intent.setAction("com.urbanairship.push.STOP");
        g.startService(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    public static void e() {
        if (!f1279a.e.a("com.urbanairship.push.PUSH_ENABLED", false)) {
            f1279a.e.a(true);
            c();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    public static void f() {
        if (f1279a.e.a("com.urbanairship.push.PUSH_ENABLED", false)) {
            f1279a.e.a(false);
            if (f1279a.e.a("com.urbanairship.push.C2DM_KEY") != null) {
                f1279a.c(null);
                a.c();
            }
            d();
        }
    }

    static /* synthetic */ void k() {
        Intent intent = new Intent();
        intent.setClass(c.a().g(), f.class);
        intent.setAction("com.urbanairship.push.UPDATE_APID");
        PendingIntent service = PendingIntent.getService(c.a().g(), 0, intent, 0);
        e.d("Scheduling APID update in 10 minutes");
        ((AlarmManager) c.a().g().getSystemService("alarm")).set(1, System.currentTimeMillis() + 600000, service);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.a.b(java.lang.String, int):boolean
      com.urbanairship.a.b(java.lang.String, long):boolean
      com.urbanairship.a.b(java.lang.String, boolean):boolean */
    /* access modifiers changed from: private */
    public void l() {
        this.e.b("com.urbanairship.push.APID_UPDATE_NEEDED", true);
        String a2 = this.e.a("com.urbanairship.push.APID");
        e.c("Updating APID: " + a2);
        if (a2 == null || a2.length() == 0) {
            e.e("No APID. Cannot update.");
            return;
        }
        String str = c.a().h().f1170a + "api/apids/" + a2;
        String a3 = this.e.a("com.urbanairship.push.C2DM_KEY");
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("alias", this.e.a("com.urbanairship.push.ALIAS"));
            jSONObject.put("tags", new JSONArray((Collection) this.e.h()));
            jSONObject.put("c2dm_registration_id", a3);
            e.b("URL: " + str);
            com.urbanairship.c.c cVar = new com.urbanairship.c.c("PUT", str);
            try {
                StringEntity stringEntity = new StringEntity(jSONObject.toString());
                stringEntity.setContentType("application/json");
                cVar.setEntity(stringEntity);
                e.b("Body: " + jSONObject.toString());
            } catch (UnsupportedEncodingException e2) {
                e.e("Error setting registrationRequest entity.");
            }
            cVar.a(new c(this));
        } catch (JSONException e3) {
            e.e("Error creating JSON Registration body.");
        }
    }

    public final void a(b bVar) {
        this.f1280b = bVar;
    }

    public final void a(Class cls) {
        this.c = cls;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    public final void a(String str) {
        c(str);
        Context g = c.a().g();
        Class cls = f1279a.c;
        if (cls != null) {
            Intent intent = new Intent("com.urbanairship.push.REGISTRATION_FINISHED");
            intent.setClass(c.a().g(), cls);
            intent.putExtra("com.urbanairship.push.APID", this.e.a("com.urbanairship.push.APID"));
            intent.putExtra("com.urbanairship.push.REGISTRATION_VALID", this.e.a("com.urbanairship.push.PUSH_ENABLED", false));
            intent.putExtra("com.urbanairship.push.C2DM_REGISTRATION_ID", str);
            g.sendBroadcast(intent);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void b(String str) {
        boolean z;
        e.e("C2DM Failure: " + str);
        if ("SERVICE_NOT_AVAILABLE".equals(str)) {
            a.b();
            z = false;
        } else if ("ACCOUNT_MISSING".equals(str)) {
            z = true;
        } else if ("AUTHENTICATION_FAILED".equals(str)) {
            z = true;
        } else if ("TOO_MANY_REGISTRATIONS".equals(str)) {
            z = true;
        } else if ("INVALID_SENDER".equals(str)) {
            e.e("Your C2DM sender ID is invalid. Please check your AirshipConfig.");
            z = true;
        } else {
            z = "PHONE_REGISTRATION_ERROR".equals(str);
        }
        if (z) {
            c(null);
            if (c.a().h().a() == d.HYBRID) {
                n.a(c.a().g());
            }
        }
        Context g = c.a().g();
        Class cls = f1279a.c;
        if (cls != null) {
            Intent intent = new Intent("com.urbanairship.push.REGISTRATION_FINISHED");
            intent.setClass(c.a().g(), cls);
            intent.putExtra("com.urbanairship.push.APID", this.e.a("com.urbanairship.push.APID"));
            intent.putExtra("com.urbanairship.push.REGISTRATION_VALID", false);
            intent.putExtra("com.urbanairship.push.REGISTRATION_ERROR", str);
            g.sendBroadcast(intent);
        }
    }

    public final void c(String str) {
        d dVar = this.e;
        String a2 = dVar.a("com.urbanairship.push.C2DM_KEY");
        if (a2 != null) {
            if (a2.equals(str)) {
                return;
            }
        } else if (str == null) {
            return;
        }
        dVar.a("com.urbanairship.push.C2DM_KEY", str);
        l();
    }

    public final Class g() {
        return this.c;
    }

    public final d h() {
        return this.e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    public final void i() {
        if (this.e.a("com.urbanairship.push.APID_UPDATE_NEEDED", true)) {
            l();
        }
        Context g = c.a().g();
        Class cls = f1279a.c;
        if (cls != null) {
            Intent intent = new Intent("com.urbanairship.push.REGISTRATION_FINISHED");
            intent.setClass(c.a().g(), cls);
            intent.putExtra("com.urbanairship.push.APID", this.e.a("com.urbanairship.push.APID"));
            intent.putExtra("com.urbanairship.push.REGISTRATION_VALID", this.e.a("com.urbanairship.push.PUSH_ENABLED", false));
            g.sendBroadcast(intent);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    public final void j() {
        if (this.e.a("com.urbanairship.push.APID_UPDATE_NEEDED", true)) {
            l();
        }
    }
}
