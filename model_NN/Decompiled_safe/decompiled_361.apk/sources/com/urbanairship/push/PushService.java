package com.urbanairship.push;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.urbanairship.analytics.q;
import com.urbanairship.b;
import com.urbanairship.c;
import com.urbanairship.d;
import com.urbanairship.e;
import com.urbanairship.push.b.n;
import com.urbanairship.push.c2dm.a;

public class PushService extends Service {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1228a = false;

    /* renamed from: b  reason: collision with root package name */
    private d f1229b = f.b().h();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.a.b(java.lang.String, int):boolean
      com.urbanairship.a.b(java.lang.String, long):boolean
      com.urbanairship.a.b(java.lang.String, boolean):boolean */
    private synchronized void a() {
        if (!this.f1228a) {
            if (this.f1229b.a("com.urbanairship.push.APID") != null) {
                c.a().k().a(new q());
            }
            if (!this.f1229b.a("com.urbanairship.push.PUSH_ENABLED", false)) {
                e.b("Push is disabled.  Not starting Push Service.");
            } else {
                this.f1228a = true;
                Context g = c.a().g();
                b h = c.a().h();
                f.b().h().b("com.urbanairship.push.APID_UPDATE_NEEDED", true);
                if (h.a() == d.HELIUM) {
                    e.c("Starting Helium");
                    if (this.f1229b.a("com.urbanairship.push.C2DM_KEY") != null) {
                        f.b().c(null);
                        a.c();
                    }
                    n.a(g);
                } else {
                    e.c("Starting C2DM");
                    a.a();
                }
            }
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        e.b("Push Service destroyed");
        d a2 = c.a().h().a();
        if (a2 == d.HELIUM || a2 == d.HYBRID) {
            n.a().b();
        }
        this.f1228a = false;
    }

    public void onStart(Intent intent, int i) {
        e.c("Service started with intent=" + intent);
        super.onStart(intent, i);
        if (intent == null || intent.getAction() == null) {
            e.a("Attempted to start service with null intent or action.");
            return;
        }
        String action = intent.getAction();
        if (action.equals("com.urbanairship.push.STOP")) {
            if (this.f1228a) {
                stopSelf();
            }
        } else if (action.equals("com.urbanairship.push.START")) {
            a();
        } else if (action.equals("com.urbanairship.push.HEARTBEAT")) {
            e.c("** Heartbeat - PushService started");
            if (this.f1228a) {
                n.a().e();
            } else {
                a();
            }
        } else {
            e.a("Unknown action: " + intent.getAction());
        }
    }
}
