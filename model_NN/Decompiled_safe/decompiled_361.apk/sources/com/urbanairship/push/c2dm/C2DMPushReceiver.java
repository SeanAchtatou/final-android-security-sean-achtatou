package com.urbanairship.push.c2dm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.urbanairship.c;
import com.urbanairship.d;
import com.urbanairship.e;
import com.urbanairship.push.f;
import java.util.HashMap;

public class C2DMPushReceiver extends BroadcastReceiver {
    private static void a(Intent intent) {
        HashMap hashMap = new HashMap();
        for (String next : intent.getExtras().keySet()) {
            hashMap.put(next, intent.getStringExtra(next));
        }
        String str = (String) hashMap.remove("com.urbanairship.push.PUSH_ID");
        e.d("Got C2DM push: " + str);
        f.a((String) hashMap.remove("com.urbanairship.push.ALERT"), str, hashMap);
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals("com.google.android.c2dm.intent.REGISTRATION")) {
            if (c.a().h().a() != d.HELIUM) {
                String stringExtra = intent.getStringExtra("registration_id");
                String stringExtra2 = intent.getStringExtra("error");
                String stringExtra3 = intent.getStringExtra("unregistered");
                if (stringExtra2 != null) {
                    e.e("Got error:" + stringExtra2);
                    f.b().b(stringExtra2);
                } else if (stringExtra3 != null) {
                    e.d("Unregistered from C2DM: " + stringExtra3);
                } else if (stringExtra != null) {
                    e.d("Got C2DM registration id:" + stringExtra);
                    f.b().a(stringExtra);
                }
            }
        } else if (action.equals("com.google.android.c2dm.intent.RECEIVE")) {
            a(intent);
        } else {
            e.a("Received unknown action: " + action);
        }
    }
}
