package com.urbanairship.push.c2dm;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.urbanairship.c;
import com.urbanairship.e;
import com.urbanairship.push.b.k;
import com.urbanairship.push.f;

final class b extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ long f1276a;

    b(long j) {
        this.f1276a = j;
    }

    public final void run() {
        try {
            Thread.sleep(this.f1276a);
        } catch (InterruptedException e) {
            e.a(e);
        }
        Context g = c.a().g();
        String str = c.a().h().c;
        if (str == null) {
            e.e("The C2DM sender email is not set. Unable to register.");
            return;
        }
        String a2 = f.b().h().a("com.urbanairship.push.BOX_OFFICE_SECRET");
        if (a2 == null) {
            try {
                a2 = new com.urbanairship.push.b.c().a();
            } catch (k e2) {
                e.e("BoxOffice failure.");
            }
        }
        if (a2 == null) {
            e.e("BoxOffice firstRun failed. Unable to continue C2DM registration.");
            return;
        }
        Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
        intent.putExtra("app", PendingIntent.getBroadcast(g, 0, new Intent(), 0));
        intent.putExtra("sender", str);
        g.startService(intent);
        e.d("Sent C2DM registration, sender: " + str);
    }
}
