package com.urbanairship.push.c2dm;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import com.urbanairship.c;
import com.urbanairship.e;
import com.urbanairship.push.d;
import com.urbanairship.push.f;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f1274a = new a();

    /* renamed from: b  reason: collision with root package name */
    private static long f1275b = 10000;

    private a() {
    }

    public static void a() {
        e.d("Initializing C2DM Push ...");
        if (Build.VERSION.SDK_INT < 8) {
            e.c("C2DM not supported in API level " + Build.VERSION.SDK_INT);
            f.b().b("PHONE_REGISTRATION_ERROR");
            return;
        }
        d h = f.b().h();
        if (h.a("com.urbanairship.push.C2DM_KEY") == null) {
            a(0);
            return;
        }
        e.d("Using C2DM ID: " + h.a("com.urbanairship.push.C2DM_KEY"));
        f.b().j();
    }

    private static void a(long j) {
        new b(j).start();
    }

    public static void b() {
        a(f1275b);
        f1275b *= 2;
    }

    public static void c() {
        Context g = c.a().g();
        Intent intent = new Intent("com.google.android.c2dm.intent.UNREGISTER");
        intent.putExtra("app", PendingIntent.getBroadcast(g, 0, new Intent(), 0));
        c.a().g().startService(intent);
    }
}
