package com.urbanairship.push;

import com.urbanairship.a;
import com.urbanairship.analytics.f;
import com.urbanairship.c;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;

public final class d extends a {
    public d() {
        super("com.urbanairship.push");
    }

    private static void i() {
        c.a().k().a(new f());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    public final boolean a() {
        return a("com.urbanairship.push.PUSH_ENABLED", false);
    }

    public final boolean a(Date date, Date date2) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        int i = instance.get(11);
        int i2 = instance.get(12);
        Calendar instance2 = Calendar.getInstance();
        instance2.setTime(date2);
        int i3 = instance2.get(11);
        int i4 = instance2.get(12);
        i();
        return b("com.urbanairship.push.QuietTime.START_HOUR", i) && b("com.urbanairship.push.QuietTime.START_MINUTE", i2) && b("com.urbanairship.push.QuietTime.END_HOUR", i3) && b("com.urbanairship.push.QuietTime.END_MINUTE", i4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public final boolean a(boolean z) {
        if (a("com.urbanairship.push.PUSH_ENABLED", false) == z) {
            return true;
        }
        i();
        return b("com.urbanairship.push.PUSH_ENABLED", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    public final boolean b() {
        return a("com.urbanairship.push.SOUND_ENABLED", true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    public final boolean b(boolean z) {
        if (a("com.urbanairship.push.SOUND_ENABLED", true) == z) {
            return true;
        }
        i();
        return b("com.urbanairship.push.SOUND_ENABLED", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    public final boolean c() {
        return a("com.urbanairship.push.VIBRATE_ENABLED", true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    public final boolean c(boolean z) {
        if (a("com.urbanairship.push.VIBRATE_ENABLED", true) == z) {
            return true;
        }
        i();
        return b("com.urbanairship.push.VIBRATE_ENABLED", z);
    }

    public final String d() {
        return a("com.urbanairship.push.APID");
    }

    public final boolean d(boolean z) {
        return b("com.urbanairship.push.QuietTime.ENABLED", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    public final boolean e() {
        return a("com.urbanairship.push.QuietTime.ENABLED", false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    public final boolean f() {
        if (!a("com.urbanairship.push.QuietTime.ENABLED", false)) {
            return false;
        }
        Date date = new Date();
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        int a2 = a("com.urbanairship.push.QuietTime.START_HOUR", -1);
        int a3 = a("com.urbanairship.push.QuietTime.START_MINUTE", -1);
        int a4 = a("com.urbanairship.push.QuietTime.END_HOUR", -1);
        int a5 = a("com.urbanairship.push.QuietTime.END_MINUTE", -1);
        if (-1 == a2 || -1 == a3 || -1 == a4 || -1 == a5) {
            return false;
        }
        Calendar instance2 = Calendar.getInstance();
        instance2.setTime(date);
        instance2.set(11, a2);
        instance2.set(12, a3);
        Calendar instance3 = Calendar.getInstance();
        instance3.setTime(date);
        instance3.set(11, a4);
        instance3.set(12, a5);
        if (instance2.after(instance) && instance3.before(instance2)) {
            instance2.add(6, -1);
        }
        if (instance3.before(instance2)) {
            instance3.add(6, 1);
        }
        return instance.after(instance2) && instance.before(instance3);
    }

    public final Date[] g() {
        int a2 = a("com.urbanairship.push.QuietTime.START_HOUR", -1);
        int a3 = a("com.urbanairship.push.QuietTime.START_MINUTE", -1);
        int a4 = a("com.urbanairship.push.QuietTime.END_HOUR", -1);
        int a5 = a("com.urbanairship.push.QuietTime.END_MINUTE", -1);
        if (a2 == -1 || a3 == -1 || a4 == -1 || a5 == -1) {
            return null;
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(new Date());
        instance.set(11, a2);
        instance.set(12, a3);
        Date time = instance.getTime();
        Calendar instance2 = Calendar.getInstance();
        instance2.setTime(new Date());
        instance2.set(11, a4);
        instance2.set(12, a5);
        if (a4 < a2) {
            instance2.add(5, 1);
        }
        return new Date[]{time, instance2.getTime()};
    }

    public final Set h() {
        HashSet hashSet = new HashSet();
        String a2 = a("com.urbanairship.push.TAGS");
        if (a2 != null) {
            try {
                JSONArray jSONArray = new JSONArray(a2);
                int length = jSONArray.length();
                for (int i = 0; i < length; i++) {
                    hashSet.add(jSONArray.getString(i));
                }
            } catch (JSONException e) {
            }
        }
        return hashSet;
    }
}
