package com.urbanairship.push.b;

public final class g {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f1263a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1264b = 0;
    private long c = System.currentTimeMillis();
    private /* synthetic */ c d;

    public g(c cVar, String str) {
        this.d = cVar;
        this.f1263a = str;
    }

    static /* synthetic */ int b(g gVar) {
        int i = gVar.f1264b;
        gVar.f1264b = i + 1;
        return i;
    }

    public final boolean a() {
        long currentTimeMillis = System.currentTimeMillis() - this.c;
        if (currentTimeMillis < 0) {
            return false;
        }
        return currentTimeMillis < 86400000 && this.f1264b < 3;
    }
}
