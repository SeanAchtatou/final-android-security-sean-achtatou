package com.urbanairship.push.b;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import com.urbanairship.c.b;
import com.urbanairship.e;
import com.urbanairship.push.d;
import com.urbanairship.push.f;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.UUID;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    protected static UUID f1259a = null;
    private static String f;

    /* renamed from: b  reason: collision with root package name */
    private d f1260b = f.b().h();
    private String c = null;
    private final DecimalFormat d = new DecimalFormat("0.000");
    private final LinkedList e = new LinkedList();

    public c() {
        f = a(com.urbanairship.c.a().g());
    }

    private static com.urbanairship.c.d a(com.urbanairship.c.d dVar) {
        switch (dVar.a()) {
            case 200:
                break;
            case 404:
                throw new k("404 - Not Found");
            case 500:
                throw new k("Internal Server Error");
            default:
                throw new k("Bad BoxOffice Response: " + dVar.a());
        }
        return dVar;
    }

    private static String a(Context context) {
        String string;
        if (context == null || (string = Settings.Secure.getString(context.getContentResolver(), "android_id")) == null) {
            return "";
        }
        byte[] bytes = string.getBytes();
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(bytes, 0, bytes.length);
            byte[] digest = instance.digest();
            StringBuilder sb = new StringBuilder();
            int length = digest.length;
            for (int i = 0; i < length; i++) {
                sb.append(String.format("%02x", Byte.valueOf(digest[i])));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e2) {
            e.e("Unable to hash the device ID: SHA1 digester not present");
            return "";
        }
    }

    private void a(b bVar) {
        bVar.setHeader("X-UA-Device-Family", "Android");
        bVar.setHeader("X-UA-Device-Model", Build.MODEL);
        bVar.setHeader("X-UA-OS-Version", Build.VERSION.RELEASE);
        bVar.setHeader("X-UA-Lib-Version", "3.0.0");
        bVar.setHeader("X-UA-Package-Name", com.urbanairship.c.b());
        bVar.setHeader("X-UA-Sent-At", this.d.format(((double) System.currentTimeMillis()) / 1000.0d));
        if (com.urbanairship.c.a().h().g) {
            bVar.setHeader("X-UA-Device-ID", f);
        }
        bVar.setHeader("Authorization", "Basic cU9ES09HVEFTMGVqa1pvTUZRTkt3ZzptMVF4bmFBcFN0cU1BekN5Y2ppQkJn");
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(java.lang.String r11) {
        /*
            r10 = this;
            r9 = 1
            r8 = 0
            java.lang.String r0 = "Received lookup response from BoxOffice."
            com.urbanairship.e.c(r0)
            org.json.JSONTokener r0 = new org.json.JSONTokener     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
            r0.<init>(r11)     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
            java.lang.Object r0 = r0.nextValue()     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
            java.lang.String r1 = "retry_after"
            boolean r1 = r0.has(r1)     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
            if (r1 == 0) goto L_0x0027
            java.lang.String r1 = "retry_after"
            long r0 = r0.getLong(r1)     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
            com.urbanairship.push.b.n.a(r0)     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
            com.urbanairship.push.f.d()     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
        L_0x0026:
            return
        L_0x0027:
            java.lang.String r1 = "tut"
            java.lang.String r1 = r0.getString(r1)     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
            java.util.UUID r1 = java.util.UUID.fromString(r1)     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
            com.urbanairship.push.b.c.f1259a = r1     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
            java.util.LinkedList r1 = r10.e     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
            monitor-enter(r1)     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
            java.util.LinkedList r2 = r10.e     // Catch:{ all -> 0x007d }
            r2.clear()     // Catch:{ all -> 0x007d }
            java.lang.String r2 = "servers"
            org.json.JSONArray r2 = r0.getJSONArray(r2)     // Catch:{ all -> 0x007d }
            int r3 = r2.length()     // Catch:{ all -> 0x007d }
            r4 = r8
        L_0x0046:
            if (r4 >= r3) goto L_0x0059
            java.util.LinkedList r5 = r10.e     // Catch:{ all -> 0x007d }
            com.urbanairship.push.b.g r6 = new com.urbanairship.push.b.g     // Catch:{ all -> 0x007d }
            java.lang.String r7 = r2.getString(r4)     // Catch:{ all -> 0x007d }
            r6.<init>(r10, r7)     // Catch:{ all -> 0x007d }
            r5.add(r6)     // Catch:{ all -> 0x007d }
            int r4 = r4 + 1
            goto L_0x0046
        L_0x0059:
            monitor-exit(r1)     // Catch:{ all -> 0x007d }
            java.lang.String r1 = "max_keepalive_interval"
            boolean r1 = r0.has(r1)     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
            if (r1 == 0) goto L_0x0026
            java.lang.String r1 = "max_keepalive_interval"
            int r0 = r0.getInt(r1)     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
            long r0 = (long) r0     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
            com.urbanairship.push.b.h.d = r0     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
            goto L_0x0026
        L_0x006c:
            r0 = move-exception
            com.urbanairship.push.b.k r0 = new com.urbanairship.push.b.k
            java.lang.String r1 = "Unparseable JSON: '%s'"
            java.lang.Object[] r2 = new java.lang.Object[r9]
            r2[r8] = r11
            java.lang.String r1 = java.lang.String.format(r1, r2)
            r0.<init>(r1)
            throw r0
        L_0x007d:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x007d }
            throw r0     // Catch:{ JSONException -> 0x006c, NullPointerException -> 0x0080 }
        L_0x0080:
            r0 = move-exception
            com.urbanairship.push.b.k r0 = new com.urbanairship.push.b.k
            java.lang.String r1 = "Invalid Response: '%s'"
            java.lang.Object[] r2 = new java.lang.Object[r9]
            r2[r8] = r11
            java.lang.String r1 = java.lang.String.format(r1, r2)
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.urbanairship.push.b.c.b(java.lang.String):void");
    }

    public final String a() {
        String a2 = this.f1260b.a("com.urbanairship.push.APID");
        if (a2 == null) {
            a2 = UUID.randomUUID().toString();
            e.c("Generating APID: " + a2);
            this.f1260b.a("com.urbanairship.push.APID", a2);
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("package", com.urbanairship.c.b()));
        arrayList.add(new BasicNameValuePair("apid", a2));
        try {
            b bVar = new b("POST", m.f1270a + "/firstrun");
            bVar.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            a(bVar);
            String trim = a(bVar.a()).c().trim();
            this.f1260b.a("com.urbanairship.push.BOX_OFFICE_SECRET", trim);
            return trim;
        } catch (UnsupportedEncodingException e2) {
            throw new k("Failed to post to /firstrun; UTF-8 unsupported!");
        } catch (Exception e3) {
            e.e("Error posting to /firstrun");
            throw new k("Failed to post to /furstrun");
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        synchronized (this.e) {
            Iterator it = this.e.iterator();
            while (it.hasNext()) {
                g gVar = (g) it.next();
                if (gVar.f1263a.equals(str)) {
                    e.b("Resetting failure count for " + str + " to 0.");
                    int unused = gVar.f1264b = 0;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0128, code lost:
        if (com.urbanairship.push.b.g.a(r0).split(":").length == 2) goto L_0x013a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0139, code lost:
        throw new com.urbanairship.push.b.k(java.lang.String.format("Got invalid server: '%s'", r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x013a, code lost:
        r1 = r10.e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x013c, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0141, code lost:
        if (r0.a() == false) goto L_0x0148;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0143, code lost:
        r10.e.add(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0148, code lost:
        r10.c = com.urbanairship.push.b.g.a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x014e, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        return com.urbanairship.push.b.g.a(r0);
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String b() {
        /*
            r10 = this;
            r9 = 2
            r8 = 1
            r7 = 0
            com.urbanairship.push.d r0 = r10.f1260b
            java.lang.String r1 = "com.urbanairship.push.BOX_OFFICE_SECRET"
            java.lang.String r0 = r0.a(r1)
            if (r0 != 0) goto L_0x0010
            r10.a()
        L_0x0010:
            java.util.LinkedList r1 = r10.e
            monitor-enter(r1)
            java.util.LinkedList r0 = r10.e     // Catch:{ all -> 0x00c2 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x00c2 }
            if (r0 == 0) goto L_0x00fc
            org.apache.http.message.BasicNameValuePair r0 = new org.apache.http.message.BasicNameValuePair     // Catch:{ all -> 0x00c2 }
            java.lang.String r2 = "apid"
            com.urbanairship.push.d r3 = r10.f1260b     // Catch:{ all -> 0x00c2 }
            java.lang.String r4 = "com.urbanairship.push.APID"
            java.lang.String r3 = r3.a(r4)     // Catch:{ all -> 0x00c2 }
            r0.<init>(r2, r3)     // Catch:{ all -> 0x00c2 }
            org.apache.http.message.BasicNameValuePair r2 = new org.apache.http.message.BasicNameValuePair     // Catch:{ all -> 0x00c2 }
            java.lang.String r3 = "secret"
            com.urbanairship.push.d r4 = r10.f1260b     // Catch:{ all -> 0x00c2 }
            java.lang.String r5 = "com.urbanairship.push.BOX_OFFICE_SECRET"
            java.lang.String r4 = r4.a(r5)     // Catch:{ all -> 0x00c2 }
            r2.<init>(r3, r4)     // Catch:{ all -> 0x00c2 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ all -> 0x00c2 }
            r3.<init>()     // Catch:{ all -> 0x00c2 }
            r3.add(r0)     // Catch:{ all -> 0x00c2 }
            r3.add(r2)     // Catch:{ all -> 0x00c2 }
            com.urbanairship.c.b r0 = new com.urbanairship.c.b     // Catch:{ all -> 0x00c2 }
            java.lang.String r2 = "POST"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c2 }
            r4.<init>()     // Catch:{ all -> 0x00c2 }
            java.lang.String r5 = com.urbanairship.push.b.m.f1270a     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00c2 }
            java.lang.String r5 = "/lookup"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00c2 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00c2 }
            r0.<init>(r2, r4)     // Catch:{ all -> 0x00c2 }
            org.apache.http.client.entity.UrlEncodedFormEntity r2 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ UnsupportedEncodingException -> 0x00c5 }
            java.lang.String r4 = "UTF-8"
            r2.<init>(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x00c5 }
            r0.setEntity(r2)     // Catch:{ UnsupportedEncodingException -> 0x00c5 }
            r10.a(r0)     // Catch:{ all -> 0x00c2 }
            android.content.pm.PackageInfo r2 = com.urbanairship.c.c()     // Catch:{ all -> 0x00c2 }
            if (r2 == 0) goto L_0x00ce
            java.lang.String r2 = r2.versionName     // Catch:{ all -> 0x00c2 }
        L_0x0075:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c2 }
            r3.<init>()     // Catch:{ all -> 0x00c2 }
            java.lang.String r4 = "Set version header :"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ all -> 0x00c2 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00c2 }
            com.urbanairship.e.c(r3)     // Catch:{ all -> 0x00c2 }
            java.lang.String r3 = "X-UA-Package-Version"
            r0.setHeader(r3, r2)     // Catch:{ all -> 0x00c2 }
            java.lang.String r2 = "User-Agent"
            java.lang.String r3 = "Embedded Push/%s Android/%s [%s]"
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x00c2 }
            r5 = 0
            java.lang.String r6 = "3.0.0"
            r4[r5] = r6     // Catch:{ all -> 0x00c2 }
            r5 = 1
            java.lang.String r6 = android.os.Build.VERSION.RELEASE     // Catch:{ all -> 0x00c2 }
            r4[r5] = r6     // Catch:{ all -> 0x00c2 }
            r5 = 2
            java.lang.String r6 = com.urbanairship.c.b()     // Catch:{ all -> 0x00c2 }
            r4[r5] = r6     // Catch:{ all -> 0x00c2 }
            java.lang.String r3 = java.lang.String.format(r3, r4)     // Catch:{ all -> 0x00c2 }
            r0.setHeader(r2, r3)     // Catch:{ all -> 0x00c2 }
            com.urbanairship.c.d r0 = r0.a()     // Catch:{ Exception -> 0x00d1 }
            if (r0 != 0) goto L_0x00df
            java.lang.String r0 = "Error posting to /lookup"
            com.urbanairship.e.e(r0)     // Catch:{ all -> 0x00c2 }
            com.urbanairship.push.b.k r0 = new com.urbanairship.push.b.k     // Catch:{ all -> 0x00c2 }
            java.lang.String r2 = "Error posting to /lookup"
            r0.<init>(r2)     // Catch:{ all -> 0x00c2 }
            throw r0     // Catch:{ all -> 0x00c2 }
        L_0x00c2:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00c2 }
            throw r0
        L_0x00c5:
            r0 = move-exception
            com.urbanairship.push.b.k r0 = new com.urbanairship.push.b.k     // Catch:{ all -> 0x00c2 }
            java.lang.String r2 = "Failed to post to /furstrun; UTF-8 unsupported!"
            r0.<init>(r2)     // Catch:{ all -> 0x00c2 }
            throw r0     // Catch:{ all -> 0x00c2 }
        L_0x00ce:
            java.lang.String r2 = "Unavailable"
            goto L_0x0075
        L_0x00d1:
            r0 = move-exception
            java.lang.String r0 = "Error posting to /lookup"
            com.urbanairship.e.e(r0)     // Catch:{ all -> 0x00c2 }
            com.urbanairship.push.b.k r0 = new com.urbanairship.push.b.k     // Catch:{ all -> 0x00c2 }
            java.lang.String r2 = "Error posting to /lookup"
            r0.<init>(r2)     // Catch:{ all -> 0x00c2 }
            throw r0     // Catch:{ all -> 0x00c2 }
        L_0x00df:
            java.lang.String r2 = r0.b()     // Catch:{ all -> 0x00c2 }
            com.urbanairship.c.d r0 = a(r0)     // Catch:{ all -> 0x00c2 }
            java.lang.String r0 = r0.c()     // Catch:{ all -> 0x00c2 }
            java.lang.String r0 = r0.trim()     // Catch:{ all -> 0x00c2 }
            if (r2 == 0) goto L_0x010c
            java.lang.String r3 = "application/json"
            boolean r2 = r2.equals(r3)     // Catch:{ all -> 0x00c2 }
            if (r2 == 0) goto L_0x010c
            r10.b(r0)     // Catch:{ all -> 0x00c2 }
        L_0x00fc:
            java.util.LinkedList r0 = r10.e     // Catch:{ all -> 0x00c2 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x00c2 }
            if (r0 == 0) goto L_0x0114
            java.lang.String r0 = "No Helium servers returned from BoxOffice lookup."
            com.urbanairship.e.d(r0)     // Catch:{ all -> 0x00c2 }
            r0 = 0
            monitor-exit(r1)     // Catch:{ all -> 0x00c2 }
        L_0x010b:
            return r0
        L_0x010c:
            com.urbanairship.push.b.k r0 = new com.urbanairship.push.b.k     // Catch:{ all -> 0x00c2 }
            java.lang.String r2 = "Received invalid BoxOffice response; content type is not application/json"
            r0.<init>(r2)     // Catch:{ all -> 0x00c2 }
            throw r0     // Catch:{ all -> 0x00c2 }
        L_0x0114:
            java.util.LinkedList r0 = r10.e     // Catch:{ all -> 0x00c2 }
            java.lang.Object r0 = r0.remove()     // Catch:{ all -> 0x00c2 }
            com.urbanairship.push.b.g r0 = (com.urbanairship.push.b.g) r0     // Catch:{ all -> 0x00c2 }
            monitor-exit(r1)     // Catch:{ all -> 0x00c2 }
            java.lang.String r1 = r0.f1263a
            java.lang.String r2 = ":"
            java.lang.String[] r1 = r1.split(r2)
            int r1 = r1.length
            if (r1 == r9) goto L_0x013a
            com.urbanairship.push.b.k r1 = new com.urbanairship.push.b.k
            java.lang.String r2 = "Got invalid server: '%s'"
            java.lang.Object[] r3 = new java.lang.Object[r8]
            r3[r7] = r0
            java.lang.String r0 = java.lang.String.format(r2, r3)
            r1.<init>(r0)
            throw r1
        L_0x013a:
            java.util.LinkedList r1 = r10.e
            monitor-enter(r1)
            boolean r2 = r0.a()     // Catch:{ all -> 0x0154 }
            if (r2 == 0) goto L_0x0148
            java.util.LinkedList r2 = r10.e     // Catch:{ all -> 0x0154 }
            r2.add(r0)     // Catch:{ all -> 0x0154 }
        L_0x0148:
            java.lang.String r2 = r0.f1263a     // Catch:{ all -> 0x0154 }
            r10.c = r2     // Catch:{ all -> 0x0154 }
            monitor-exit(r1)     // Catch:{ all -> 0x0154 }
            java.lang.String r0 = r0.f1263a
            goto L_0x010b
        L_0x0154:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0154 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.urbanairship.push.b.c.b():java.lang.String");
    }

    public final void c() {
        String str = this.c;
        synchronized (this.e) {
            Iterator it = this.e.iterator();
            while (it.hasNext()) {
                g gVar = (g) it.next();
                if (gVar.f1263a.equals(str)) {
                    g.b(gVar);
                    e.d("Setting failure count for " + str + " to " + gVar.f1264b);
                }
            }
        }
    }

    public final String d() {
        return this.c;
    }
}
