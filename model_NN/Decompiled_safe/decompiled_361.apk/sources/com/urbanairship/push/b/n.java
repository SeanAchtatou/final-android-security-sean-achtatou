package com.urbanairship.push.b;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.urbanairship.c;
import com.urbanairship.e;
import com.urbanairship.push.PushService;
import com.urbanairship.push.a.i;
import com.urbanairship.push.a.j;
import com.urbanairship.push.d;
import com.urbanairship.push.f;
import java.util.HashMap;

public final class n {

    /* renamed from: a  reason: collision with root package name */
    private static n f1271a = new n();

    /* renamed from: b  reason: collision with root package name */
    private static BroadcastReceiver f1272b;
    private String c = null;
    private a d = null;

    private n() {
    }

    public static n a() {
        return f1271a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean
      com.urbanairship.a.a(java.lang.String, long):long */
    public static void a(Context context) {
        e.b("Embedded Push Initializing...");
        Intent intent = new Intent(context, PushService.class);
        intent.setAction("com.urbanairship.push.HEARTBEAT");
        ((AlarmManager) context.getSystemService("alarm")).setInexactRepeating(1, System.currentTimeMillis() + 900000, 900000, PendingIntent.getService(context, 0, intent, 0));
        if (c()) {
            e.c("In holding pattern. Will retry after " + f.b().h().a("com.urbanairship.push.RETRY_AFTER", 0L));
            f.d();
            return;
        }
        if (f1272b == null) {
            f1272b = new f();
            context.registerReceiver(f1272b, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
        e.b("Embedded Push initialization complete.");
    }

    public static void a(j jVar) {
        e.c("Received Helium Push.");
        String e = jVar.e();
        String j = jVar.j();
        HashMap hashMap = new HashMap();
        if (jVar.l() > 0) {
            for (i iVar : jVar.k()) {
                hashMap.put(iVar.c(), iVar.e());
            }
        } else if (j != null && j.length() > 0) {
            hashMap.put("com.urbanairship.push.STRING_EXTRA", j);
        }
        f.a(e, jVar.c(), hashMap);
    }

    static /* synthetic */ void a(n nVar) {
        String c2 = l.c();
        e.b("Current IP: " + c2 + ". Previous IP: " + nVar.c);
        e.b("IP Changed: " + ((nVar.c != null || c2 == null) ? (nVar.c == null || c2 == null || nVar.c.equals(c2)) ? false : true : true));
        if (!c()) {
            e.c("Starting new connection to Helium");
            if (nVar.d != null) {
                nVar.d.a();
            }
            nVar.d = new a(nVar);
            nVar.d.start();
        }
    }

    public static boolean a(long j) {
        long j2;
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        if (currentTimeMillis >= j) {
            e.c("BoxOffice retry_after response is in the past. Ignoring.");
            return false;
        }
        if (j - currentTimeMillis > 604800) {
            e.c("BoxOffice retry_after response of " + j + " exceeds our maximum retry delay. Setting to max delay.");
            j2 = currentTimeMillis + 604800;
        } else {
            j2 = j;
        }
        e.c("Received BoxOffice response to reconnect after: " + j2 + ". Currently: " + currentTimeMillis + ". Shutting down" + "for " + (j2 - currentTimeMillis) + " seconds.");
        return f.b().h().b("com.urbanairship.push.RETRY_AFTER", j2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean
      com.urbanairship.a.a(java.lang.String, long):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.b(java.lang.String, long):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.a.b(java.lang.String, int):boolean
      com.urbanairship.a.b(java.lang.String, boolean):boolean
      com.urbanairship.a.b(java.lang.String, long):boolean */
    public static boolean c() {
        d h = f.b().h();
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        long a2 = h.a("com.urbanairship.push.RETRY_AFTER", 0L);
        if (a2 - currentTimeMillis > 604800) {
            h.b("com.urbanairship.push.RETRY_AFTER", 0L);
            a2 = 0;
        }
        return a2 > currentTimeMillis;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    protected static void d() {
        e.c("sending valid: " + c.b() + ", " + f.b().h().a("com.urbanairship.push.PUSH_ENABLED", false));
        f.b().i();
    }

    public final synchronized void a(String str) {
        this.c = str;
    }

    public final void b() {
        e.b("Embedded Push teardown!");
        Context g = c.a().g();
        if (f1272b != null) {
            g.unregisterReceiver(f1272b);
            f1272b = null;
        }
        if (this.d != null) {
            this.d.a();
            this.d = null;
        }
    }

    public final void e() {
        if (this.d != null) {
            this.d.b();
        }
    }
}
