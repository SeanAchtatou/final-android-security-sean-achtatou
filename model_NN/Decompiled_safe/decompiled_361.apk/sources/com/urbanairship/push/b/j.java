package com.urbanairship.push.b;

import android.os.Build;
import com.google.a.b;
import com.google.a.g;
import com.google.a.l;
import com.urbanairship.e;
import com.urbanairship.push.a.c;
import com.urbanairship.push.a.h;
import com.urbanairship.push.a.k;
import com.urbanairship.push.a.m;
import com.urbanairship.push.a.p;
import com.urbanairship.push.a.r;
import com.urbanairship.push.a.t;
import com.urbanairship.push.d;
import com.urbanairship.push.f;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class j {

    /* renamed from: a  reason: collision with root package name */
    public long f1268a = System.currentTimeMillis();

    /* renamed from: b  reason: collision with root package name */
    private c f1269b;
    private b c;
    private OutputStream d;
    private d e = f.b().h();
    private HashMap f;

    public j(Socket socket, c cVar) {
        this.f1269b = cVar;
        this.c = b.a(socket.getInputStream());
        this.d = socket.getOutputStream();
        this.f = new HashMap();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    public final void a() {
        String str;
        String str2;
        short s = ByteBuffer.wrap(this.c.c(2)).getShort();
        if (s == 0) {
            e.b("keepalive read");
            this.f1268a = System.currentTimeMillis();
        } else if (c.f1259a == null) {
            throw new i(this, "key unknown!");
        } else {
            k a2 = k.a(new e(c.f1259a).b(com.urbanairship.a.e.b(this.c.c(s))));
            e.b("response read");
            if (a2.c() == p.REGISTER) {
                r a3 = r.a(a2.e());
                e.c("Registration response received!");
                List b2 = a3.b();
                List c2 = a3.c();
                String str3 = "";
                Iterator it = b2.iterator();
                while (true) {
                    str = str3;
                    if (!it.hasNext()) {
                        break;
                    }
                    str3 = str + ((c) it.next()).c() + " ";
                }
                String str4 = "";
                Iterator it2 = c2.iterator();
                while (true) {
                    str2 = str4;
                    if (!it2.hasNext()) {
                        break;
                    }
                    str4 = str2 + ((c) it2.next()).c() + " ";
                }
                e.b("Valid: " + str);
                e.b("Invalid: " + str2);
                if (b2.size() > 0) {
                    n.d();
                } else if (c2.size() > 0) {
                    n.d();
                    throw new RuntimeException("Package name on server does not match the application package name.");
                }
                this.f1269b.a(this.f1269b.d());
            } else if (a2.c() == p.PUSH_NOTIFICATION) {
                com.urbanairship.push.a.j a4 = com.urbanairship.push.a.j.a(a2.e());
                String h = a4.h();
                String c3 = a4.c();
                e.b("Got app id: " + h);
                if (this.f.containsKey(h) && ((String) this.f.get(h)).equals(c3)) {
                    e.d("Message " + c3 + " already sent. Discarding.");
                    return;
                }
                e.d("Message " + c3 + " received.");
                this.f.put(h, c3);
                if (!this.e.a("com.urbanairship.push.PUSH_ENABLED", false)) {
                    e.a(String.format("Got push notification, but Push is disabled", new Object[0]));
                } else {
                    n.a(a4);
                }
            } else {
                e.e("Got a bad request!");
            }
        }
    }

    public final void b() {
        String b2 = com.urbanairship.c.a().h().b();
        com.urbanairship.push.a.b d2 = h.n().a(this.e.a("com.urbanairship.push.APID")).a(m.ANDROID).b(Build.VERSION.RELEASE).c("3.0.0").d(this.e.a("com.urbanairship.push.BOX_OFFICE_SECRET"));
        String b3 = com.urbanairship.c.b();
        ArrayList arrayList = new ArrayList();
        arrayList.add(c.h().a(b3).b(b2).a());
        k a2 = k.j().a(t.h().a(this.e.a("com.urbanairship.push.APID")).a(g.a(com.urbanairship.a.e.a(new e(c.f1259a).a(d2.a(arrayList).a().p())))).a().o()).a(p.REGISTER).a();
        try {
            short g = (short) a2.g();
            e.b("Size: " + ((int) g));
            this.d.write(ByteBuffer.allocate(2).putShort(g).array());
            OutputStream outputStream = this.d;
            int g2 = a2.g();
            if (g2 > 4096) {
                g2 = 4096;
            }
            l a3 = l.a(outputStream, g2);
            a2.a(a3);
            a3.a();
        } catch (Exception e2) {
            e.a(e2);
        }
    }
}
