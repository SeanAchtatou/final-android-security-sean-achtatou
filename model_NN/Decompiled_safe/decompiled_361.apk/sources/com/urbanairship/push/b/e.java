package com.urbanairship.push.b;

import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f1261a = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    /* renamed from: b  reason: collision with root package name */
    private static final IvParameterSpec f1262b = new IvParameterSpec(f1261a);
    private Cipher c;
    private SecretKeySpec d;

    public e(UUID uuid) {
        if (uuid == null) {
            throw new b(this);
        }
        try {
            this.c = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.d = new SecretKeySpec(uuid.toString().replace("-", "").getBytes(), "AES");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e2) {
            e2.printStackTrace();
        }
    }

    public final byte[] a(byte[] bArr) {
        try {
            this.c.init(1, this.d, f1262b);
            return this.c.doFinal(bArr);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final byte[] b(byte[] bArr) {
        try {
            this.c.init(2, this.d, f1262b);
            return this.c.doFinal(bArr);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
