package com.urbanairship.push.b;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.urbanairship.c;
import com.urbanairship.e;

public final class l {
    public static boolean a() {
        NetworkInfo d = d();
        if (d == null) {
            return false;
        }
        return d.isConnected();
    }

    public static String b() {
        NetworkInfo d = d();
        return d == null ? "none" : d.getTypeName();
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x000c A[Catch:{ SocketException -> 0x004f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String c() {
        /*
            r0 = 0
            java.util.Enumeration r1 = java.net.NetworkInterface.getNetworkInterfaces()     // Catch:{ SocketException -> 0x0048 }
            r2 = r0
        L_0x0006:
            boolean r0 = r1.hasMoreElements()     // Catch:{ SocketException -> 0x004f }
            if (r0 == 0) goto L_0x0030
            java.lang.Object r0 = r1.nextElement()     // Catch:{ SocketException -> 0x004f }
            java.net.NetworkInterface r0 = (java.net.NetworkInterface) r0     // Catch:{ SocketException -> 0x004f }
            java.util.Enumeration r3 = r0.getInetAddresses()     // Catch:{ SocketException -> 0x004f }
        L_0x0016:
            boolean r0 = r3.hasMoreElements()     // Catch:{ SocketException -> 0x004f }
            if (r0 == 0) goto L_0x0006
            java.lang.Object r0 = r3.nextElement()     // Catch:{ SocketException -> 0x004f }
            java.net.InetAddress r0 = (java.net.InetAddress) r0     // Catch:{ SocketException -> 0x004f }
            boolean r4 = r0.isLoopbackAddress()     // Catch:{ SocketException -> 0x004f }
            if (r4 != 0) goto L_0x0052
            if (r2 != 0) goto L_0x0052
            java.lang.String r0 = r0.getHostAddress()     // Catch:{ SocketException -> 0x004f }
        L_0x002e:
            r2 = r0
            goto L_0x0016
        L_0x0030:
            r0 = r2
        L_0x0031:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Detected active IP address as: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            java.lang.String r1 = r1.toString()
            com.urbanairship.e.b(r1)
            return r0
        L_0x0048:
            r1 = move-exception
        L_0x0049:
            java.lang.String r1 = "Error fetching IP address information"
            com.urbanairship.e.e(r1)
            goto L_0x0031
        L_0x004f:
            r0 = move-exception
            r0 = r2
            goto L_0x0049
        L_0x0052:
            r0 = r2
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.urbanairship.push.b.l.c():java.lang.String");
    }

    private static NetworkInfo d() {
        ConnectivityManager connectivityManager = (ConnectivityManager) c.a().g().getSystemService("connectivity");
        if (connectivityManager != null) {
            return connectivityManager.getActiveNetworkInfo();
        }
        e.e("Error fetching network info.");
        return null;
    }
}
