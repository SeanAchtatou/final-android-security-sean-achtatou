package com.urbanairship.push.b;

import com.google.a.o;
import com.urbanairship.e;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public final class a extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f1256a = false;

    /* renamed from: b  reason: collision with root package name */
    private Socket f1257b;
    private j c;
    private n d;
    private c e;
    private volatile boolean f = false;
    private long g = ((long) h.f1265a);

    public a(n nVar) {
        this.d = nVar;
        this.e = new c();
        setName("HeliumConnectionThread");
    }

    private static void a(Socket socket) {
        f1256a = false;
        if (socket != null) {
            try {
                if (socket.isConnected() && !socket.isClosed()) {
                    socket.close();
                }
            } catch (IOException e2) {
                e.a("Error closing socket.");
            }
        }
    }

    private boolean a(long j) {
        long j2 = this.g;
        long min = System.currentTimeMillis() - j < j2 ? Math.min(j2 * 4, h.f1266b) : (long) h.f1265a;
        e.c("Rescheduling connection in " + min + "ms.");
        this.g = min;
        try {
            Thread.sleep(min);
            return true;
        } catch (InterruptedException e2) {
            return false;
        }
    }

    public final void a() {
        e.c("Connection aborting.");
        this.f = false;
        e.c("Closing socket.");
        if (this.f1257b != null) {
            a(this.f1257b);
        }
        e.c("Service stopped, socket closed successfully.");
    }

    public final void b() {
        if (this.c != null && System.currentTimeMillis() - this.c.f1268a > h.d * 1000) {
            a(this.f1257b);
        }
    }

    public final void run() {
        e.b("HeliumConnection - run");
        this.f = true;
        while (this.f) {
            String b2 = l.b();
            this.d.a(l.c());
            long currentTimeMillis = System.currentTimeMillis();
            try {
                String b3 = this.e.b();
                if (b3 == null) {
                    throw new k("No Helium servers available for connection.");
                }
                String[] split = b3.split(":");
                String str = split[0];
                Integer num = new Integer(split[1]);
                if (!this.f) {
                    e.c("Connection sequence aborted. Ending prior to opening Helium connection.");
                    return;
                }
                e.c("Connecting to " + str + ":" + num);
                try {
                    this.f1257b = new Socket();
                    this.f1257b.connect(new InetSocketAddress(str, num.intValue()), ((int) h.d) * 1000);
                    e.d("Connection established to " + this.f1257b.getInetAddress() + ":" + num + " on network type " + b2);
                    f1256a = true;
                    this.c = new j(this.f1257b, this.e);
                    this.c.b();
                    while (this.f) {
                        this.c.a();
                        Thread.sleep((long) h.c);
                    }
                    if (!this.f) {
                        e.c("Connection aborted, shutting down. Network type=" + b2);
                    } else {
                        a(this.f1257b);
                        this.e.c();
                        if (!l.a() || n.c()) {
                            this.f = false;
                        } else if (!a(currentTimeMillis)) {
                            this.f = false;
                            return;
                        }
                    }
                } catch (InterruptedException e2) {
                    e.c("Connection thread interrupted.");
                    this.f = false;
                    if (!this.f) {
                        e.c("Connection aborted, shutting down. Network type=" + b2);
                        return;
                    }
                    a(this.f1257b);
                    this.e.c();
                    if (!l.a() || n.c()) {
                        this.f = false;
                        return;
                    } else if (!a(currentTimeMillis)) {
                        this.f = false;
                        return;
                    }
                } catch (o e3) {
                    e.c("Invalid protobuf (expected, likely due to interruption).");
                    if (!this.f) {
                        e.c("Connection aborted, shutting down. Network type=" + b2);
                    } else {
                        a(this.f1257b);
                        this.e.c();
                        if (!l.a() || n.c()) {
                            this.f = false;
                        } else if (!a(currentTimeMillis)) {
                            this.f = false;
                            return;
                        }
                    }
                } catch (InterruptedIOException e4) {
                    e.c("Socket timed out.");
                    if (!this.f) {
                        e.c("Connection aborted, shutting down. Network type=" + b2);
                    } else {
                        a(this.f1257b);
                        this.e.c();
                        if (!l.a() || n.c()) {
                            this.f = false;
                        } else if (!a(currentTimeMillis)) {
                            this.f = false;
                            return;
                        }
                    }
                } catch (IOException e5) {
                    e.c("IOException (Expected following restart or connectivity change).");
                    if (!this.f) {
                        e.c("Connection aborted, shutting down. Network type=" + b2);
                    } else {
                        a(this.f1257b);
                        this.e.c();
                        if (!l.a() || n.c()) {
                            this.f = false;
                        } else if (!a(currentTimeMillis)) {
                            this.f = false;
                            return;
                        }
                    }
                } catch (i e6) {
                    e.c("Helium exception - secret not set.");
                    if (!this.f) {
                        e.c("Connection aborted, shutting down. Network type=" + b2);
                    } else {
                        a(this.f1257b);
                        this.e.c();
                        if (!l.a() || n.c()) {
                            this.f = false;
                        } else if (!a(currentTimeMillis)) {
                            this.f = false;
                            return;
                        }
                    }
                } catch (Exception e7) {
                    e.c("Exception in Helium connection. Network type=" + b2 + " " + e7.getMessage());
                    if (!this.f) {
                        e.c("Connection aborted, shutting down. Network type=" + b2);
                    } else {
                        a(this.f1257b);
                        this.e.c();
                        if (!l.a() || n.c()) {
                            this.f = false;
                        } else if (!a(currentTimeMillis)) {
                            this.f = false;
                            return;
                        }
                    }
                } catch (Throwable th) {
                    if (!this.f) {
                        e.c("Connection aborted, shutting down. Network type=" + b2);
                    } else {
                        a(this.f1257b);
                        this.e.c();
                        if (!l.a() || n.c()) {
                            this.f = false;
                        } else if (!a(currentTimeMillis)) {
                            this.f = false;
                            return;
                        }
                    }
                    throw th;
                }
            } catch (k e8) {
                if (!a(currentTimeMillis)) {
                    this.f = false;
                    return;
                }
            }
        }
    }
}
