package com.urbanairship.push;

import android.app.Notification;
import android.net.Uri;
import android.widget.RemoteViews;
import com.urbanairship.a.b;
import com.urbanairship.c;

public final class e implements b {

    /* renamed from: a  reason: collision with root package name */
    public int f1277a;

    /* renamed from: b  reason: collision with root package name */
    public int f1278b;
    public int c;
    public int d;
    public int e = c.d().icon;
    public int f = c.d().icon;
    private int g = -1;
    private Uri h;

    public final int a() {
        return this.g > 0 ? this.g : b.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    public final Notification a(String str) {
        Notification notification = new Notification(this.f, str, System.currentTimeMillis());
        notification.flags = 16;
        notification.defaults = 0;
        RemoteViews remoteViews = new RemoteViews(c.a().g().getPackageName(), this.f1277a);
        remoteViews.setTextViewText(this.c, c.e());
        remoteViews.setTextViewText(this.d, str);
        remoteViews.setImageViewResource(this.f1278b, this.e);
        notification.contentView = remoteViews;
        d h2 = f.b().h();
        if (!h2.f()) {
            if (h2.a("com.urbanairship.push.VIBRATE_ENABLED", true)) {
                notification.defaults |= 2;
            }
            if (h2.a("com.urbanairship.push.SOUND_ENABLED", true)) {
                if (this.h != null) {
                    notification.sound = this.h;
                } else {
                    notification.defaults |= 1;
                }
            }
        }
        return notification;
    }
}
