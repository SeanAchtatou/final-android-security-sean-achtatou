package com.urbanairship.push.a;

import com.google.a.a;
import com.google.a.c;
import com.google.a.p;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class b extends c {

    /* renamed from: a  reason: collision with root package name */
    private h f1232a;

    private b() {
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public b b(com.google.a.b bVar, p pVar) {
        while (true) {
            int a2 = bVar.a();
            switch (a2) {
                case 0:
                    return this;
                case 10:
                    a(bVar.b());
                    break;
                case 16:
                    m a3 = m.a(bVar.d());
                    if (a3 == null) {
                        break;
                    } else {
                        a(a3);
                        break;
                    }
                case 26:
                    b(bVar.b());
                    break;
                case 34:
                    c(bVar.b());
                    break;
                case 42:
                    d(bVar.b());
                    break;
                case 50:
                    a h = c.h();
                    bVar.a(h, pVar);
                    c b2 = h.b();
                    if (b2 != null) {
                        if (this.f1232a.l.isEmpty()) {
                            List unused = this.f1232a.l = new ArrayList();
                        }
                        this.f1232a.l.add(b2);
                        break;
                    } else {
                        throw new NullPointerException();
                    }
                default:
                    if (bVar.b(a2)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }

    /* access modifiers changed from: private */
    public static b e() {
        b bVar = new b();
        bVar.f1232a = new h();
        return bVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public b d() {
        b e = e();
        h hVar = this.f1232a;
        if (hVar != h.a()) {
            if (hVar.b()) {
                e.a(hVar.c());
            }
            if (hVar.d()) {
                e.a(hVar.e());
            }
            if (hVar.f()) {
                e.b(hVar.h());
            }
            if (hVar.i()) {
                e.c(hVar.j());
            }
            if (hVar.k()) {
                e.d(hVar.l());
            }
            if (!hVar.l.isEmpty()) {
                if (e.f1232a.l.isEmpty()) {
                    List unused = e.f1232a.l = new ArrayList();
                }
                e.f1232a.l.addAll(hVar.l);
            }
        }
        return e;
    }

    public final b a(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1232a.d = true;
        m unused2 = this.f1232a.e = mVar;
        return this;
    }

    public final b a(Iterable iterable) {
        if (this.f1232a.l.isEmpty()) {
            List unused = this.f1232a.l = new ArrayList();
        }
        c.a(iterable, this.f1232a.l);
        return this;
    }

    public final b a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1232a.f1238b = true;
        String unused2 = this.f1232a.c = str;
        return this;
    }

    public final h a() {
        if (this.f1232a != null && !this.f1232a.m()) {
            throw new a();
        } else if (this.f1232a == null) {
            throw new IllegalStateException("build() has already been called on this Builder.");
        } else {
            if (this.f1232a.l != Collections.EMPTY_LIST) {
                List unused = this.f1232a.l = Collections.unmodifiableList(this.f1232a.l);
            }
            h hVar = this.f1232a;
            this.f1232a = null;
            return hVar;
        }
    }

    public final b b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1232a.f = true;
        String unused2 = this.f1232a.g = str;
        return this;
    }

    public final b c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1232a.h = true;
        String unused2 = this.f1232a.i = str;
        return this;
    }

    public final b d(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1232a.j = true;
        String unused2 = this.f1232a.k = str;
        return this;
    }
}
