package com.urbanairship.push.a;

import com.google.a.b;
import com.google.a.c;
import com.google.a.d;
import com.google.a.e;
import com.google.a.p;

public final class g extends c {

    /* renamed from: a  reason: collision with root package name */
    private i f1236a;

    private g() {
    }

    private g a(b bVar) {
        while (true) {
            int a2 = bVar.a();
            switch (a2) {
                case 0:
                    return this;
                case 10:
                    a(bVar.b());
                    break;
                case 18:
                    b(bVar.b());
                    break;
                default:
                    if (bVar.b(a2)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }

    private g a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1236a.f1240b = true;
        String unused2 = this.f1236a.c = str;
        return this;
    }

    private g b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1236a.d = true;
        String unused2 = this.f1236a.e = str;
        return this;
    }

    /* access modifiers changed from: private */
    public static g e() {
        g gVar = new g();
        gVar.f1236a = new i();
        return gVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public g d() {
        g e = e();
        i iVar = this.f1236a;
        if (iVar != i.a()) {
            if (iVar.b()) {
                e.a(iVar.c());
            }
            if (iVar.d()) {
                e.b(iVar.e());
            }
        }
        return e;
    }

    public final /* bridge */ /* synthetic */ e a(b bVar, p pVar) {
        return a(bVar);
    }

    public final i a() {
        if (this.f1236a == null) {
            throw new IllegalStateException("build() has already been called on this Builder.");
        }
        i iVar = this.f1236a;
        this.f1236a = null;
        return iVar;
    }

    public final /* bridge */ /* synthetic */ d b(b bVar, p pVar) {
        return a(bVar);
    }
}
