package com.urbanairship.push.a;

import com.google.a.a;
import com.google.a.b;
import com.google.a.c;
import com.google.a.e;
import com.google.a.g;
import com.google.a.p;

public final class d extends c {

    /* renamed from: a  reason: collision with root package name */
    private t f1235a;

    private d() {
    }

    private d a(b bVar) {
        while (true) {
            int a2 = bVar.a();
            switch (a2) {
                case 0:
                    return this;
                case 10:
                    a(bVar.b());
                    break;
                case 18:
                    a(bVar.c());
                    break;
                default:
                    if (bVar.b(a2)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }

    /* access modifiers changed from: private */
    public static d e() {
        d dVar = new d();
        dVar.f1235a = new t();
        return dVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public d d() {
        d e = e();
        t tVar = this.f1235a;
        if (tVar != t.a()) {
            if (tVar.b()) {
                e.a(tVar.c());
            }
            if (tVar.d()) {
                e.a(tVar.e());
            }
        }
        return e;
    }

    public final /* bridge */ /* synthetic */ e a(b bVar, p pVar) {
        return a(bVar);
    }

    public final d a(g gVar) {
        if (gVar == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1235a.d = true;
        g unused2 = this.f1235a.e = gVar;
        return this;
    }

    public final d a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1235a.f1255b = true;
        String unused2 = this.f1235a.c = str;
        return this;
    }

    public final t a() {
        if (this.f1235a != null && !this.f1235a.f()) {
            throw new a();
        } else if (this.f1235a == null) {
            throw new IllegalStateException("build() has already been called on this Builder.");
        } else {
            t tVar = this.f1235a;
            this.f1235a = null;
            return tVar;
        }
    }

    public final /* bridge */ /* synthetic */ com.google.a.d b(b bVar, p pVar) {
        return a(bVar);
    }
}
