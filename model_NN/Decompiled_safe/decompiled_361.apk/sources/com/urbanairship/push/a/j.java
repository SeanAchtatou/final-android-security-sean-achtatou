package com.urbanairship.push.a;

import com.google.a.g;
import com.google.a.l;
import com.google.a.q;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class j extends q {

    /* renamed from: a  reason: collision with root package name */
    private static final j f1241a = new j(0);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f1242b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public List j;
    private int k;

    /* synthetic */ j() {
        this((byte) 0);
    }

    private j(byte b2) {
        this.c = "";
        this.e = "";
        this.g = "";
        this.i = "";
        this.j = Collections.emptyList();
        this.k = -1;
    }

    private j(char c2) {
        this.c = "";
        this.e = "";
        this.g = "";
        this.i = "";
        this.j = Collections.emptyList();
        this.k = -1;
    }

    public static j a() {
        return f1241a;
    }

    public static j a(g gVar) {
        return o.a((o) o.b().b(gVar));
    }

    public final void a(l lVar) {
        g();
        if (this.f1242b) {
            lVar.a(1, this.c);
        }
        if (this.d) {
            lVar.a(2, this.e);
        }
        if (this.f) {
            lVar.a(3, this.g);
        }
        if (this.h) {
            lVar.a(4, this.i);
        }
        for (i a2 : this.j) {
            lVar.a(5, a2);
        }
    }

    public final boolean b() {
        return this.f1242b;
    }

    public final String c() {
        return this.c;
    }

    public final boolean d() {
        return this.d;
    }

    public final String e() {
        return this.e;
    }

    public final boolean f() {
        return this.f;
    }

    public final int g() {
        int i2 = this.k;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        if (this.f1242b) {
            i3 = l.b(1, this.c) + 0;
        }
        if (this.d) {
            i3 += l.b(2, this.e);
        }
        if (this.f) {
            i3 += l.b(3, this.g);
        }
        if (this.h) {
            i3 += l.b(4, this.i);
        }
        Iterator it = this.j.iterator();
        while (true) {
            int i4 = i3;
            if (it.hasNext()) {
                i3 = l.b(5, (i) it.next()) + i4;
            } else {
                this.k = i4;
                return i4;
            }
        }
    }

    public final String h() {
        return this.g;
    }

    public final boolean i() {
        return this.h;
    }

    public final String j() {
        return this.i;
    }

    public final List k() {
        return this.j;
    }

    public final int l() {
        return this.j.size();
    }

    public final boolean m() {
        if (!this.f1242b) {
            return false;
        }
        if (!this.d) {
            return false;
        }
        if (!this.f) {
            return false;
        }
        for (i f2 : this.j) {
            if (!f2.f()) {
                return false;
            }
        }
        return true;
    }
}
