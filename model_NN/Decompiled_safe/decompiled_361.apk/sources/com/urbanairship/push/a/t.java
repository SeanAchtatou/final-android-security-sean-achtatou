package com.urbanairship.push.a;

import com.google.a.g;
import com.google.a.l;
import com.google.a.q;

public final class t extends q {

    /* renamed from: a  reason: collision with root package name */
    private static final t f1254a = new t(0);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f1255b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public g e;
    private int f;

    /* synthetic */ t() {
        this((byte) 0);
    }

    private t(byte b2) {
        this.c = "";
        this.e = g.f1102a;
        this.f = -1;
    }

    private t(char c2) {
        this.c = "";
        this.e = g.f1102a;
        this.f = -1;
    }

    public static t a() {
        return f1254a;
    }

    public static d h() {
        return d.e();
    }

    public final void a(l lVar) {
        g();
        if (this.f1255b) {
            lVar.a(1, this.c);
        }
        if (this.d) {
            lVar.a(this.e);
        }
    }

    public final boolean b() {
        return this.f1255b;
    }

    public final String c() {
        return this.c;
    }

    public final boolean d() {
        return this.d;
    }

    public final g e() {
        return this.e;
    }

    public final boolean f() {
        if (!this.f1255b) {
            return false;
        }
        return this.d;
    }

    public final int g() {
        int i = this.f;
        if (i == -1) {
            i = 0;
            if (this.f1255b) {
                i = l.b(1, this.c) + 0;
            }
            if (this.d) {
                i += l.b(this.e);
            }
            this.f = i;
        }
        return i;
    }
}
