package com.urbanairship.push.a;

import com.google.a.a;
import com.google.a.b;
import com.google.a.c;
import com.google.a.p;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class s extends c {

    /* renamed from: a  reason: collision with root package name */
    private r f1253a;

    private s() {
    }

    static /* synthetic */ r a(s sVar) {
        if (!sVar.f1253a.d()) {
            throw new a().a();
        } else if (sVar.f1253a == null) {
            throw new IllegalStateException("build() has already been called on this Builder.");
        } else {
            if (sVar.f1253a.f1252b != Collections.EMPTY_LIST) {
                List unused = sVar.f1253a.f1252b = Collections.unmodifiableList(sVar.f1253a.f1252b);
            }
            if (sVar.f1253a.c != Collections.EMPTY_LIST) {
                List unused2 = sVar.f1253a.c = Collections.unmodifiableList(sVar.f1253a.c);
            }
            r rVar = sVar.f1253a;
            sVar.f1253a = null;
            return rVar;
        }
    }

    /* access modifiers changed from: private */
    public static s b() {
        s sVar = new s();
        sVar.f1253a = new r();
        return sVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public s b(b bVar, p pVar) {
        while (true) {
            int a2 = bVar.a();
            switch (a2) {
                case 0:
                    return this;
                case 10:
                    a h = c.h();
                    bVar.a(h, pVar);
                    c b2 = h.b();
                    if (b2 != null) {
                        if (this.f1253a.f1252b.isEmpty()) {
                            List unused = this.f1253a.f1252b = new ArrayList();
                        }
                        this.f1253a.f1252b.add(b2);
                        break;
                    } else {
                        throw new NullPointerException();
                    }
                case 18:
                    a h2 = c.h();
                    bVar.a(h2, pVar);
                    c b3 = h2.b();
                    if (b3 != null) {
                        if (this.f1253a.c.isEmpty()) {
                            List unused2 = this.f1253a.c = new ArrayList();
                        }
                        this.f1253a.c.add(b3);
                        break;
                    } else {
                        throw new NullPointerException();
                    }
                default:
                    if (bVar.b(a2)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public s d() {
        s b2 = b();
        r rVar = this.f1253a;
        if (rVar != r.a()) {
            if (!rVar.f1252b.isEmpty()) {
                if (b2.f1253a.f1252b.isEmpty()) {
                    List unused = b2.f1253a.f1252b = new ArrayList();
                }
                b2.f1253a.f1252b.addAll(rVar.f1252b);
            }
            if (!rVar.c.isEmpty()) {
                if (b2.f1253a.c.isEmpty()) {
                    List unused2 = b2.f1253a.c = new ArrayList();
                }
                b2.f1253a.c.addAll(rVar.c);
            }
        }
        return b2;
    }
}
