package com.urbanairship.push.a;

import com.google.a.a;
import com.google.a.b;
import com.google.a.c;
import com.google.a.d;
import com.google.a.e;
import com.google.a.g;
import com.google.a.p;

public final class l extends c {

    /* renamed from: a  reason: collision with root package name */
    private k f1245a;

    private l() {
    }

    static /* synthetic */ k a(l lVar) {
        if (lVar.f1245a.i()) {
            return lVar.g();
        }
        throw new a().a();
    }

    private l a(b bVar) {
        while (true) {
            int a2 = bVar.a();
            switch (a2) {
                case 0:
                    return this;
                case 8:
                    p a3 = p.a(bVar.d());
                    if (a3 == null) {
                        break;
                    } else {
                        a(a3);
                        break;
                    }
                case 18:
                    a(bVar.c());
                    break;
                case 26:
                    a(bVar.b());
                    break;
                default:
                    if (bVar.b(a2)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }

    private l a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1245a.f = true;
        String unused2 = this.f1245a.g = str;
        return this;
    }

    /* access modifiers changed from: private */
    public static l e() {
        l lVar = new l();
        lVar.f1245a = new k();
        return lVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public l d() {
        l e = e();
        k kVar = this.f1245a;
        if (kVar != k.a()) {
            if (kVar.b()) {
                e.a(kVar.c());
            }
            if (kVar.d()) {
                e.a(kVar.e());
            }
            if (kVar.f()) {
                e.a(kVar.h());
            }
        }
        return e;
    }

    private k g() {
        if (this.f1245a == null) {
            throw new IllegalStateException("build() has already been called on this Builder.");
        }
        k kVar = this.f1245a;
        this.f1245a = null;
        return kVar;
    }

    public final /* bridge */ /* synthetic */ e a(b bVar, p pVar) {
        return a(bVar);
    }

    public final k a() {
        if (this.f1245a == null || this.f1245a.i()) {
            return g();
        }
        throw new a();
    }

    public final l a(g gVar) {
        if (gVar == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1245a.d = true;
        g unused2 = this.f1245a.e = gVar;
        return this;
    }

    public final l a(p pVar) {
        if (pVar == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1245a.f1244b = true;
        p unused2 = this.f1245a.c = pVar;
        return this;
    }

    public final /* bridge */ /* synthetic */ d b(b bVar, p pVar) {
        return a(bVar);
    }
}
