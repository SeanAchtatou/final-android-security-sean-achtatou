package com.urbanairship.push.a;

import com.google.a.l;
import com.google.a.q;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class h extends q {

    /* renamed from: a  reason: collision with root package name */
    private static final h f1237a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f1238b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public m e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public boolean j;
    /* access modifiers changed from: private */
    public String k;
    /* access modifiers changed from: private */
    public List l;
    private int m;

    static {
        h hVar = new h(0);
        f1237a = hVar;
        hVar.e = m.ANDROID;
    }

    /* synthetic */ h() {
        this((byte) 0);
    }

    private h(byte b2) {
        this.c = "";
        this.g = "";
        this.i = "";
        this.k = "";
        this.l = Collections.emptyList();
        this.m = -1;
        this.e = m.ANDROID;
    }

    private h(char c2) {
        this.c = "";
        this.g = "";
        this.i = "";
        this.k = "";
        this.l = Collections.emptyList();
        this.m = -1;
    }

    public static h a() {
        return f1237a;
    }

    public static b n() {
        return b.e();
    }

    public final void a(l lVar) {
        g();
        if (this.f1238b) {
            lVar.a(1, this.c);
        }
        if (this.d) {
            lVar.a(2, this.e.a());
        }
        if (this.f) {
            lVar.a(3, this.g);
        }
        if (this.h) {
            lVar.a(4, this.i);
        }
        if (this.j) {
            lVar.a(5, this.k);
        }
        for (c a2 : this.l) {
            lVar.a(6, a2);
        }
    }

    public final boolean b() {
        return this.f1238b;
    }

    public final String c() {
        return this.c;
    }

    public final boolean d() {
        return this.d;
    }

    public final m e() {
        return this.e;
    }

    public final boolean f() {
        return this.f;
    }

    public final int g() {
        int i2 = this.m;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        if (this.f1238b) {
            i3 = l.b(1, this.c) + 0;
        }
        if (this.d) {
            i3 += l.b(2, this.e.a());
        }
        if (this.f) {
            i3 += l.b(3, this.g);
        }
        if (this.h) {
            i3 += l.b(4, this.i);
        }
        if (this.j) {
            i3 += l.b(5, this.k);
        }
        Iterator it = this.l.iterator();
        while (true) {
            int i4 = i3;
            if (it.hasNext()) {
                i3 = l.b(6, (c) it.next()) + i4;
            } else {
                this.m = i4;
                return i4;
            }
        }
    }

    public final String h() {
        return this.g;
    }

    public final boolean i() {
        return this.h;
    }

    public final String j() {
        return this.i;
    }

    public final boolean k() {
        return this.j;
    }

    public final String l() {
        return this.k;
    }

    public final boolean m() {
        if (!this.f1238b) {
            return false;
        }
        if (!this.d) {
            return false;
        }
        if (!this.f) {
            return false;
        }
        if (!this.h) {
            return false;
        }
        if (!this.j) {
            return false;
        }
        for (c f2 : this.l) {
            if (!f2.f()) {
                return false;
            }
        }
        return true;
    }
}
