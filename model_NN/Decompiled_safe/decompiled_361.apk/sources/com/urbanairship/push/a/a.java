package com.urbanairship.push.a;

import com.google.a.b;
import com.google.a.c;
import com.google.a.d;
import com.google.a.e;
import com.google.a.p;

public final class a extends c {

    /* renamed from: a  reason: collision with root package name */
    private c f1231a;

    private a() {
    }

    private a a(b bVar) {
        while (true) {
            int a2 = bVar.a();
            switch (a2) {
                case 0:
                    return this;
                case 10:
                    a(bVar.b());
                    break;
                case 18:
                    b(bVar.b());
                    break;
                default:
                    if (bVar.b(a2)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }

    /* access modifiers changed from: private */
    public static a f() {
        a aVar = new a();
        aVar.f1231a = new c();
        return aVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public a d() {
        a f = f();
        c cVar = this.f1231a;
        if (cVar != c.a()) {
            if (cVar.b()) {
                f.a(cVar.c());
            }
            if (cVar.d()) {
                f.b(cVar.e());
            }
        }
        return f;
    }

    public final /* bridge */ /* synthetic */ e a(b bVar, p pVar) {
        return a(bVar);
    }

    public final a a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1231a.f1234b = true;
        String unused2 = this.f1231a.c = str;
        return this;
    }

    public final c a() {
        if (this.f1231a == null || this.f1231a.f()) {
            return b();
        }
        throw new com.google.a.a();
    }

    public final /* bridge */ /* synthetic */ d b(b bVar, p pVar) {
        return a(bVar);
    }

    public final a b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1231a.d = true;
        String unused2 = this.f1231a.e = str;
        return this;
    }

    public final c b() {
        if (this.f1231a == null) {
            throw new IllegalStateException("build() has already been called on this Builder.");
        }
        c cVar = this.f1231a;
        this.f1231a = null;
        return cVar;
    }
}
