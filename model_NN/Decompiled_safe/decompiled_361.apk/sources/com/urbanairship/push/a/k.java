package com.urbanairship.push.a;

import com.google.a.g;
import com.google.a.l;
import com.google.a.q;

public final class k extends q {

    /* renamed from: a  reason: collision with root package name */
    private static final k f1243a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f1244b;
    /* access modifiers changed from: private */
    public p c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public g e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public String g;
    private int h;

    static {
        k kVar = new k(0);
        f1243a = kVar;
        kVar.c = p.REGISTER;
    }

    /* synthetic */ k() {
        this((byte) 0);
    }

    private k(byte b2) {
        this.e = g.f1102a;
        this.g = "";
        this.h = -1;
        this.c = p.REGISTER;
    }

    private k(char c2) {
        this.e = g.f1102a;
        this.g = "";
        this.h = -1;
    }

    public static k a() {
        return f1243a;
    }

    public static k a(byte[] bArr) {
        return l.a((l) l.e().a(bArr, bArr.length));
    }

    public static l j() {
        return l.e();
    }

    public final void a(l lVar) {
        g();
        if (this.f1244b) {
            lVar.a(1, this.c.a());
        }
        if (this.d) {
            lVar.a(this.e);
        }
        if (this.f) {
            lVar.a(3, this.g);
        }
    }

    public final boolean b() {
        return this.f1244b;
    }

    public final p c() {
        return this.c;
    }

    public final boolean d() {
        return this.d;
    }

    public final g e() {
        return this.e;
    }

    public final boolean f() {
        return this.f;
    }

    public final int g() {
        int i = this.h;
        if (i == -1) {
            i = 0;
            if (this.f1244b) {
                i = l.b(1, this.c.a()) + 0;
            }
            if (this.d) {
                i += l.b(this.e);
            }
            if (this.f) {
                i += l.b(3, this.g);
            }
            this.h = i;
        }
        return i;
    }

    public final String h() {
        return this.g;
    }

    public final boolean i() {
        if (!this.f1244b) {
            return false;
        }
        return this.d;
    }
}
