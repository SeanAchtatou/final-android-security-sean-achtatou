package com.urbanairship.push.a;

import com.google.a.l;
import com.google.a.q;

public final class i extends q {

    /* renamed from: a  reason: collision with root package name */
    private static final i f1239a = new i(0);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f1240b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public String e;
    private int f;

    /* synthetic */ i() {
        this((byte) 0);
    }

    private i(byte b2) {
        this.c = "";
        this.e = "";
        this.f = -1;
    }

    private i(char c2) {
        this.c = "";
        this.e = "";
        this.f = -1;
    }

    public static i a() {
        return f1239a;
    }

    public static g h() {
        return g.e();
    }

    public final void a(l lVar) {
        g();
        if (this.f1240b) {
            lVar.a(1, this.c);
        }
        if (this.d) {
            lVar.a(2, this.e);
        }
    }

    public final boolean b() {
        return this.f1240b;
    }

    public final String c() {
        return this.c;
    }

    public final boolean d() {
        return this.d;
    }

    public final String e() {
        return this.e;
    }

    public final boolean f() {
        if (!this.f1240b) {
            return false;
        }
        return this.d;
    }

    public final int g() {
        int i = this.f;
        if (i == -1) {
            i = 0;
            if (this.f1240b) {
                i = l.b(1, this.c) + 0;
            }
            if (this.d) {
                i += l.b(2, this.e);
            }
            this.f = i;
        }
        return i;
    }
}
