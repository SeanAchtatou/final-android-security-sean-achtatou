package com.urbanairship.push.a;

import com.google.a.a;
import com.google.a.b;
import com.google.a.c;
import com.google.a.p;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class o extends c {

    /* renamed from: a  reason: collision with root package name */
    private j f1248a;

    private o() {
    }

    static /* synthetic */ j a(o oVar) {
        if (!oVar.f1248a.m()) {
            throw new a().a();
        } else if (oVar.f1248a == null) {
            throw new IllegalStateException("build() has already been called on this Builder.");
        } else {
            if (oVar.f1248a.j != Collections.EMPTY_LIST) {
                List unused = oVar.f1248a.j = Collections.unmodifiableList(oVar.f1248a.j);
            }
            j jVar = oVar.f1248a;
            oVar.f1248a = null;
            return jVar;
        }
    }

    private o a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1248a.f1242b = true;
        String unused2 = this.f1248a.c = str;
        return this;
    }

    /* access modifiers changed from: private */
    public static o b() {
        o oVar = new o();
        oVar.f1248a = new j();
        return oVar;
    }

    private o b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1248a.d = true;
        String unused2 = this.f1248a.e = str;
        return this;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public o b(b bVar, p pVar) {
        while (true) {
            int a2 = bVar.a();
            switch (a2) {
                case 0:
                    return this;
                case 10:
                    a(bVar.b());
                    break;
                case 18:
                    b(bVar.b());
                    break;
                case 26:
                    c(bVar.b());
                    break;
                case 34:
                    d(bVar.b());
                    break;
                case 42:
                    g h = i.h();
                    bVar.a(h, pVar);
                    i a3 = h.a();
                    if (a3 != null) {
                        if (this.f1248a.j.isEmpty()) {
                            List unused = this.f1248a.j = new ArrayList();
                        }
                        this.f1248a.j.add(a3);
                        break;
                    } else {
                        throw new NullPointerException();
                    }
                default:
                    if (bVar.b(a2)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }

    private o c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1248a.f = true;
        String unused2 = this.f1248a.g = str;
        return this;
    }

    private o d(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        boolean unused = this.f1248a.h = true;
        String unused2 = this.f1248a.i = str;
        return this;
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public o d() {
        o b2 = b();
        j jVar = this.f1248a;
        if (jVar != j.a()) {
            if (jVar.b()) {
                b2.a(jVar.c());
            }
            if (jVar.d()) {
                b2.b(jVar.e());
            }
            if (jVar.f()) {
                b2.c(jVar.h());
            }
            if (jVar.i()) {
                b2.d(jVar.j());
            }
            if (!jVar.j.isEmpty()) {
                if (b2.f1248a.j.isEmpty()) {
                    List unused = b2.f1248a.j = new ArrayList();
                }
                b2.f1248a.j.addAll(jVar.j);
            }
        }
        return b2;
    }
}
