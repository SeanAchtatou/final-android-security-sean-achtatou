package com.urbanairship.push.a;

import com.google.a.l;
import com.google.a.q;

public final class c extends q {

    /* renamed from: a  reason: collision with root package name */
    private static final c f1233a = new c(0);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f1234b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public String e;
    private int f;

    /* synthetic */ c() {
        this((byte) 0);
    }

    private c(byte b2) {
        this.c = "";
        this.e = "";
        this.f = -1;
    }

    private c(char c2) {
        this.c = "";
        this.e = "";
        this.f = -1;
    }

    public static c a() {
        return f1233a;
    }

    public static a h() {
        return a.f();
    }

    public final void a(l lVar) {
        g();
        if (this.f1234b) {
            lVar.a(1, this.c);
        }
        if (this.d) {
            lVar.a(2, this.e);
        }
    }

    public final boolean b() {
        return this.f1234b;
    }

    public final String c() {
        return this.c;
    }

    public final boolean d() {
        return this.d;
    }

    public final String e() {
        return this.e;
    }

    public final boolean f() {
        if (!this.f1234b) {
            return false;
        }
        return this.d;
    }

    public final int g() {
        int i = this.f;
        if (i == -1) {
            i = 0;
            if (this.f1234b) {
                i = l.b(1, this.c) + 0;
            }
            if (this.d) {
                i += l.b(2, this.e);
            }
            this.f = i;
        }
        return i;
    }
}
