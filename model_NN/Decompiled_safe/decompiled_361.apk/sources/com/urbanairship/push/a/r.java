package com.urbanairship.push.a;

import com.google.a.g;
import com.google.a.l;
import com.google.a.q;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class r extends q {

    /* renamed from: a  reason: collision with root package name */
    private static final r f1251a = new r(0);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public List f1252b;
    /* access modifiers changed from: private */
    public List c;
    private int d;

    /* synthetic */ r() {
        this((byte) 0);
    }

    private r(byte b2) {
        this.f1252b = Collections.emptyList();
        this.c = Collections.emptyList();
        this.d = -1;
    }

    private r(char c2) {
        this.f1252b = Collections.emptyList();
        this.c = Collections.emptyList();
        this.d = -1;
    }

    public static r a() {
        return f1251a;
    }

    public static r a(g gVar) {
        return s.a((s) s.b().b(gVar));
    }

    public final void a(l lVar) {
        g();
        for (c a2 : this.f1252b) {
            lVar.a(1, a2);
        }
        for (c a3 : this.c) {
            lVar.a(2, a3);
        }
    }

    public final List b() {
        return this.f1252b;
    }

    public final List c() {
        return this.c;
    }

    public final boolean d() {
        for (c f : this.f1252b) {
            if (!f.f()) {
                return false;
            }
        }
        for (c f2 : this.c) {
            if (!f2.f()) {
                return false;
            }
        }
        return true;
    }

    public final int g() {
        int i;
        int i2 = this.d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        Iterator it = this.f1252b.iterator();
        while (true) {
            i = i3;
            if (!it.hasNext()) {
                break;
            }
            i3 = l.b(1, (c) it.next()) + i;
        }
        for (c b2 : this.c) {
            i = l.b(2, b2) + i;
        }
        this.d = i;
        return i;
    }
}
