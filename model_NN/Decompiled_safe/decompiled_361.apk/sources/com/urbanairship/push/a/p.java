package com.urbanairship.push.a;

import com.google.a.j;

public enum p {
    REGISTER(0, 0),
    DEVICE_CONNECT(1, 1),
    PUSH_NOTIFICATION(2, 2),
    ECHO(3, 3),
    HELLO(4, 4);
    
    private static j f = new q();
    private final int g;
    private final int h;

    private p(int i2, int i3) {
        this.g = i2;
        this.h = i3;
    }

    public static p a(int i2) {
        switch (i2) {
            case 0:
                return REGISTER;
            case 1:
                return DEVICE_CONNECT;
            case 2:
                return PUSH_NOTIFICATION;
            case 3:
                return ECHO;
            case 4:
                return HELLO;
            default:
                return null;
        }
    }

    public final int a() {
        return this.h;
    }
}
