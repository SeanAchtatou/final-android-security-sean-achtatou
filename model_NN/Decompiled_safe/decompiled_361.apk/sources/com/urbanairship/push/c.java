package com.urbanairship.push;

import com.urbanairship.c.d;
import com.urbanairship.c.h;
import com.urbanairship.e;

final class c extends h {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ f f1273a;

    c(f fVar) {
        this.f1273a = fVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.a.b(java.lang.String, int):boolean
      com.urbanairship.a.b(java.lang.String, long):boolean
      com.urbanairship.a.b(java.lang.String, boolean):boolean */
    public final void a(d dVar) {
        String c = dVar.c();
        e.d("Registration status code: " + dVar.a());
        e.b("Registration result " + c);
        if (dVar.a() == 200) {
            e.d("Registration request succeeded.");
            this.f1273a.e.b("com.urbanairship.push.APID_UPDATE_NEEDED", false);
            return;
        }
        e.d("Registration request response status: " + dVar.a());
    }

    public final void a(Exception exc) {
        e.e("Error registering APID");
        f.k();
    }
}
