package com.urbanairship.push;

import android.app.Notification;
import com.urbanairship.a.b;
import com.urbanairship.c;

public final class g implements b {

    /* renamed from: a  reason: collision with root package name */
    private int f1281a = c.d().icon;

    /* renamed from: b  reason: collision with root package name */
    private String f1282b = c.e();
    private int c = -1;

    public final int a() {
        return this.c > 0 ? this.c : b.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    public final Notification a(String str) {
        Notification notification = new Notification(this.f1281a, str, System.currentTimeMillis());
        notification.flags = 16;
        notification.setLatestEventInfo(c.a().g(), this.f1282b, str, null);
        notification.defaults = 0;
        d h = f.b().h();
        if (!h.f()) {
            if (h.a("com.urbanairship.push.VIBRATE_ENABLED", true)) {
                notification.defaults |= 2;
            }
            if (h.a("com.urbanairship.push.SOUND_ENABLED", true)) {
                notification.defaults |= 1;
            }
        }
        return notification;
    }
}
