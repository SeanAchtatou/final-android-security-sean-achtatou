package com.urbanairship.analytics;

import android.app.Activity;
import android.content.Intent;
import android.provider.Settings;
import com.urbanairship.CoreReceiver;
import com.urbanairship.c;
import com.urbanairship.e;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private final String f1157a = c.a().h().f1171b;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public String f1158b;
    /* access modifiers changed from: private */
    public d c = new d(this);
    /* access modifiers changed from: private */
    public boolean d = false;
    private j e = new j(new o(this));
    private h f = new h();
    private i g = new i(this.f);

    static String c() {
        String string = Settings.Secure.getString(c.a().g().getContentResolver(), "android_id");
        if (string == null) {
            return "unavailable";
        }
        byte[] bytes = string.getBytes();
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(bytes, 0, bytes.length);
            byte[] digest = instance.digest();
            StringBuilder sb = new StringBuilder();
            int length = digest.length;
            for (int i = 0; i < length; i++) {
                sb.append(String.format("%02x", Byte.valueOf(digest[i])));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e2) {
            e.e("Unable to hash the device ID: SHA1 digester not present");
            return "unavailable";
        }
    }

    public final String a() {
        return this.f1158b;
    }

    public final void a(Activity activity) {
        this.e.a(activity);
        a(new s(activity));
    }

    public final void a(c cVar) {
        String str = c.a().h().f1171b;
        if (str != null && str.length() != 0 && c.a().h().g) {
            e.d(cVar.getClass().getName() + " - " + cVar.d().toString());
            int c2 = this.f.c();
            if (c2 > this.g.b()) {
                e.d("DB size exceeded. Deleting non-critical events.");
                if ("activity_started".equals(cVar.f()) || "activity_stopped".equals(cVar.f())) {
                    e.d("Database full. Not logging activity start/stop events");
                    return;
                }
                e.d("Deleting activity start/stop events.");
                this.f.a("activity_started");
                this.f.a("activity_stopped");
                c2 = this.f.c();
            }
            if (c2 > this.g.b()) {
                e.d("Deleting oldest session.");
                String a2 = this.f.a();
                if (a2 != null && a2.length() > 0) {
                    this.f.b(a2);
                }
            }
            this.f.a(cVar);
            Intent intent = new Intent("com.urbanairship.analytics.START");
            intent.setClass(c.a().g(), CoreReceiver.class);
            c.a().g().sendBroadcast(intent);
        }
    }

    public final void a(String str) {
        this.f1158b = str;
    }

    /* access modifiers changed from: package-private */
    public final d b() {
        return this.c;
    }

    public final void b(Activity activity) {
        a(new k(activity));
        j jVar = this.e;
        new b(jVar, new a(jVar, activity)).start();
    }

    public final void d() {
        this.g.a();
    }
}
