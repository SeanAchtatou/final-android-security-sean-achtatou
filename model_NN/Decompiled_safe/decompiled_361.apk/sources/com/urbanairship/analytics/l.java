package com.urbanairship.analytics;

import com.urbanairship.e;
import org.json.JSONException;
import org.json.JSONObject;

public final class l extends c {

    /* renamed from: a  reason: collision with root package name */
    private String f1165a;

    public l(String str) {
        this.f1165a = str;
    }

    /* access modifiers changed from: package-private */
    public final String f() {
        return "push_arrived";
    }

    /* access modifiers changed from: package-private */
    public final JSONObject g() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("session_id", c().f1167b);
            jSONObject.put("push_id", this.f1165a);
        } catch (JSONException e) {
            e.e("Error constructing JSON data for " + "push_arrived");
        }
        return jSONObject;
    }
}
