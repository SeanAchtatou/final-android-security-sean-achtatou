package com.urbanairship.analytics;

import android.app.Activity;
import com.urbanairship.e;
import org.json.JSONException;
import org.json.JSONObject;

final class s extends c {

    /* renamed from: a  reason: collision with root package name */
    private String f1169a;

    s(Activity activity) {
        this.f1169a = activity.getComponentName().getClassName();
    }

    /* access modifiers changed from: package-private */
    public final String f() {
        return "activity_started";
    }

    /* access modifiers changed from: package-private */
    public final JSONObject g() {
        JSONObject jSONObject = new JSONObject();
        m c = c();
        try {
            jSONObject.put("class_name", this.f1169a);
            jSONObject.put("session_id", c.f1167b);
        } catch (JSONException e) {
            e.e("Error constructing JSON data for " + "activity_started");
        }
        return jSONObject;
    }
}
