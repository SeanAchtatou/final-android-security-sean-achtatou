package com.urbanairship.analytics;

import com.urbanairship.e;
import org.json.JSONException;
import org.json.JSONObject;

final class p extends c {
    p() {
    }

    /* access modifiers changed from: package-private */
    public final String f() {
        return "app_background";
    }

    /* access modifiers changed from: package-private */
    public final JSONObject g() {
        JSONObject jSONObject = new JSONObject();
        m c = c();
        try {
            jSONObject.put("session_id", c().f1167b);
            jSONObject.put("connection_type", m.a());
            String b2 = m.b();
            if (b2.length() > 0) {
                jSONObject.put("connection_subtype", b2);
            }
            jSONObject.put("session_id", c.f1167b);
            jSONObject.put("push_id", c.f1166a);
        } catch (JSONException e) {
            e.e("Error constructing JSON data for " + "app_background");
        }
        return jSONObject;
    }
}
