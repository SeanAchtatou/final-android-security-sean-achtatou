package com.urbanairship.analytics;

import com.urbanairship.a;
import com.urbanairship.e;
import java.util.Collections;
import java.util.Map;

final class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1160a;

    /* renamed from: b  reason: collision with root package name */
    private int f1161b = 102400;
    private int c = this.i.a("MAX_TOTAL_DB_SIZE", 5120000);
    private int d = this.i.a("MAX_BATCH_SIZE", 512000);
    private int e = this.i.a("MAX_WAIT", 604800000);
    private int f = this.i.a("MIN_BATCH_INTERVAL", 60000);
    private long g = this.i.a("LAST_SEND", System.currentTimeMillis());
    private volatile boolean h;
    private a i = new a("com.urbanairship.analytics");
    private h j;
    private Thread k;

    public i(h hVar) {
        this.j = hVar;
        this.f1160a = true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0149  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x01b7  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x027c  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0282  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(java.util.Collection r13) {
        /*
            r12 = this;
            r11 = 604800000(0x240c8400, float:3.046947E-17)
            r10 = 5120000(0x4e2000, float:7.174648E-39)
            r9 = 512000(0x7d000, float:7.17465E-40)
            r8 = 60000(0xea60, float:8.4078E-41)
            r7 = 0
            if (r13 != 0) goto L_0x0016
            java.lang.String r0 = "Send failed. No events."
            com.urbanairship.e.e(r0)
            r0 = r7
        L_0x0015:
            return r0
        L_0x0016:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Sending "
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = r13.size()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = " events."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.urbanairship.e.d(r0)
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            java.lang.String r0 = "["
            byte[] r0 = r0.getBytes()     // Catch:{ IOException -> 0x006b }
            r1.write(r0)     // Catch:{ IOException -> 0x006b }
            java.util.Iterator r2 = r13.iterator()     // Catch:{ IOException -> 0x006b }
        L_0x0048:
            boolean r0 = r2.hasNext()     // Catch:{ IOException -> 0x006b }
            if (r0 == 0) goto L_0x0073
            java.lang.Object r0 = r2.next()     // Catch:{ IOException -> 0x006b }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x006b }
            byte[] r0 = r0.getBytes()     // Catch:{ IOException -> 0x006b }
            r1.write(r0)     // Catch:{ IOException -> 0x006b }
            boolean r0 = r2.hasNext()     // Catch:{ IOException -> 0x006b }
            if (r0 == 0) goto L_0x0048
            java.lang.String r0 = ","
            byte[] r0 = r0.getBytes()     // Catch:{ IOException -> 0x006b }
            r1.write(r0)     // Catch:{ IOException -> 0x006b }
            goto L_0x0048
        L_0x006b:
            r0 = move-exception
            java.lang.String r0 = "Unable to create raw JSON payload."
            com.urbanairship.e.e(r0)
            r0 = r7
            goto L_0x0015
        L_0x0073:
            java.lang.String r0 = "]"
            byte[] r0 = r0.getBytes()     // Catch:{ IOException -> 0x006b }
            r1.write(r0)     // Catch:{ IOException -> 0x006b }
            byte[] r0 = r1.toByteArray()
            com.urbanairship.c.b r1 = new com.urbanairship.c.b
            java.lang.String r2 = "POST"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            com.urbanairship.c r4 = com.urbanairship.c.a()
            com.urbanairship.b r4 = r4.h()
            java.lang.String r4 = r4.f1171b
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "warp9/"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.<init>(r2, r3)
            r2 = 0
            boolean r3 = r12.f1160a
            if (r3 == 0) goto L_0x00fd
            int r3 = r0.length     // Catch:{ IOException -> 0x0271 }
            int r3 = r3 / 4
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0271 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x0271 }
            java.util.zip.GZIPOutputStream r5 = new java.util.zip.GZIPOutputStream     // Catch:{ IOException -> 0x0271 }
            r5.<init>(r4)     // Catch:{ IOException -> 0x0271 }
            r5.write(r0)     // Catch:{ IOException -> 0x0271 }
            r5.close()     // Catch:{ IOException -> 0x0271 }
            byte[] r4 = r4.toByteArray()     // Catch:{ IOException -> 0x0271 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0271 }
            r5.<init>()     // Catch:{ IOException -> 0x0271 }
            java.lang.String r6 = "GZIP'd: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0271 }
            int r6 = r0.length     // Catch:{ IOException -> 0x0271 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0271 }
            java.lang.String r6 = " into "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0271 }
            int r6 = r4.length     // Catch:{ IOException -> 0x0271 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0271 }
            java.lang.String r6 = " (expected "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0271 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ IOException -> 0x0271 }
            java.lang.String r5 = ")"
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ IOException -> 0x0271 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0271 }
            com.urbanairship.e.b(r3)     // Catch:{ IOException -> 0x0271 }
            org.apache.http.entity.ByteArrayEntity r3 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ IOException -> 0x0271 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x0271 }
            java.lang.String r2 = "gzip"
            r3.setContentEncoding(r2)     // Catch:{ IOException -> 0x027f }
            r2 = r3
        L_0x00fd:
            if (r2 != 0) goto L_0x0282
            org.apache.http.entity.ByteArrayEntity r2 = new org.apache.http.entity.ByteArrayEntity
            r2.<init>(r0)
            r1.setEntity(r2)
            r0 = r2
        L_0x0108:
            java.lang.String r2 = "application/json"
            r0.setContentType(r2)
            r1.setEntity(r0)
            java.lang.String r0 = "X-UA-Device-Family"
            java.lang.String r2 = "android"
            r1.setHeader(r0, r2)
            long r2 = java.lang.System.currentTimeMillis()
            double r2 = (double) r2
            r4 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r2 = r2 / r4
            java.lang.String r0 = "X-UA-Sent-At"
            java.lang.String r2 = java.lang.Double.toString(r2)
            r1.setHeader(r0, r2)
            java.lang.String r0 = "X-UA-Package-Name"
            java.lang.String r2 = com.urbanairship.c.b()
            r1.setHeader(r0, r2)
            java.lang.String r0 = "X-UA-Package-Version"
            android.content.pm.PackageInfo r2 = com.urbanairship.c.c()
            java.lang.String r2 = r2.versionName
            r1.setHeader(r0, r2)
            java.lang.String r0 = com.urbanairship.analytics.g.c()
            int r2 = r0.length()
            if (r2 <= 0) goto L_0x014e
            java.lang.String r2 = "X-UA-Device-ID"
            r1.setHeader(r2, r0)
        L_0x014e:
            com.urbanairship.c r0 = com.urbanairship.c.a()
            com.urbanairship.b r0 = r0.h()
            java.lang.String r2 = "X-UA-App-Key"
            java.lang.String r3 = r0.b()
            r1.setHeader(r2, r3)
            java.lang.String r2 = "X-UA-In-Production"
            boolean r0 = r0.d
            java.lang.String r0 = java.lang.Boolean.toString(r0)
            r1.setHeader(r2, r0)
            java.lang.String r0 = "X-UA-Device-Model"
            java.lang.String r2 = android.os.Build.MODEL
            r1.setHeader(r0, r2)
            java.lang.String r0 = "X-UA-OS-Version"
            java.lang.String r2 = android.os.Build.VERSION.RELEASE
            r1.setHeader(r0, r2)
            com.urbanairship.push.f r0 = com.urbanairship.push.f.b()
            com.urbanairship.push.d r0 = r0.h()
            java.lang.String r2 = "com.urbanairship.push.APID"
            java.lang.String r0 = r0.a(r2)
            if (r0 == 0) goto L_0x0193
            int r2 = r0.length()
            if (r2 <= 0) goto L_0x0193
            java.lang.String r2 = "X-UA-Apid"
            r1.setHeader(r2, r0)
        L_0x0193:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "Sending Analytics to: "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.net.URI r2 = r1.getURI()
            java.lang.String r2 = r2.toASCIIString()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.urbanairship.e.c(r0)
            com.urbanairship.c.d r0 = r1.a()
            if (r0 == 0) goto L_0x027c
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Warp 9 response: "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r0.a()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.urbanairship.e.c(r1)
            java.lang.String r1 = "X-UA-Max-Total"
            org.apache.http.Header r1 = r0.a(r1)
            java.lang.String r2 = "X-UA-Max-Batch"
            org.apache.http.Header r2 = r0.a(r2)
            java.lang.String r3 = "X-UA-Max-Wait"
            org.apache.http.Header r3 = r0.a(r3)
            java.lang.String r4 = "X-UA-Min-Batch-Interval"
            org.apache.http.Header r4 = r0.a(r4)
            if (r1 == 0) goto L_0x01fe
            java.lang.String r1 = r1.getValue()
            int r1 = java.lang.Integer.parseInt(r1)
            int r1 = r1 * 1024
            int r1 = java.lang.Math.min(r1, r10)
            if (r1 > 0) goto L_0x01fc
            r1 = r10
        L_0x01fc:
            r12.c = r1
        L_0x01fe:
            if (r2 == 0) goto L_0x0213
            java.lang.String r1 = r2.getValue()
            int r1 = java.lang.Integer.parseInt(r1)
            int r1 = r1 * 1024
            int r1 = java.lang.Math.min(r1, r9)
            if (r1 > 0) goto L_0x0211
            r1 = r9
        L_0x0211:
            r12.d = r1
        L_0x0213:
            if (r3 == 0) goto L_0x0226
            java.lang.String r1 = r3.getValue()
            int r1 = java.lang.Integer.parseInt(r1)
            int r1 = java.lang.Math.min(r1, r11)
            if (r1 > 0) goto L_0x0224
            r1 = r11
        L_0x0224:
            r12.e = r1
        L_0x0226:
            if (r4 == 0) goto L_0x0239
            java.lang.String r1 = r4.getValue()
            int r1 = java.lang.Integer.parseInt(r1)
            int r1 = java.lang.Math.max(r1, r8)
            if (r1 > 0) goto L_0x0237
            r1 = r8
        L_0x0237:
            r12.f = r1
        L_0x0239:
            com.urbanairship.a r1 = r12.i
            java.lang.String r2 = "MAX_TOTAL_DB_SIZE"
            int r3 = r12.c
            r1.b(r2, r3)
            com.urbanairship.a r1 = r12.i
            java.lang.String r2 = "MAX_BATCH_SIZE"
            int r3 = r12.d
            r1.b(r2, r3)
            com.urbanairship.a r1 = r12.i
            java.lang.String r2 = "MAX_WAIT"
            int r3 = r12.e
            r1.b(r2, r3)
            com.urbanairship.a r1 = r12.i
            java.lang.String r2 = "MIN_BATCH_INTERVAL"
            int r3 = r12.f
            r1.b(r2, r3)
            com.urbanairship.a r1 = r12.i
            java.lang.String r2 = "LAST_SEND"
            long r3 = r12.g
            r1.b(r2, r3)
            int r0 = r0.a()
            r1 = 200(0xc8, float:2.8E-43)
            if (r0 != r1) goto L_0x0279
            r0 = 1
            goto L_0x0015
        L_0x0271:
            r3 = move-exception
        L_0x0272:
            java.lang.String r3 = "GZIP of analytics payload failed."
            com.urbanairship.e.e(r3)
            goto L_0x00fd
        L_0x0279:
            r0 = r7
            goto L_0x0015
        L_0x027c:
            r0 = r7
            goto L_0x0015
        L_0x027f:
            r2 = move-exception
            r2 = r3
            goto L_0x0272
        L_0x0282:
            r0 = r2
            goto L_0x0108
        */
        throw new UnsupportedOperationException("Method not decompiled: com.urbanairship.analytics.i.a(java.util.Collection):boolean");
    }

    public final void a() {
        if (!this.h) {
            e.b("EventUploadManager - starting upload thread");
            this.k = new Thread(this);
            this.k.start();
        }
    }

    public final int b() {
        return this.c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public final void run() {
        this.h = true;
        while (this.h) {
            long max = Math.max(0L, (this.g + ((long) this.f)) - System.currentTimeMillis());
            if (max > 0) {
                try {
                    Thread.sleep(max);
                } catch (InterruptedException e2) {
                    e.d("InterruptedException in EventUploadManager. Bailing!");
                    this.h = false;
                    return;
                }
            }
            if (this.h) {
                this.g = System.currentTimeMillis();
                this.i.b("LAST_SEND", this.g);
                int b2 = this.j.b();
                if (b2 <= 0) {
                    this.h = false;
                    e.d("Exiting analytics upload thread.");
                    return;
                }
                Map a2 = this.j.a(this.f1161b / (this.j.c() / b2));
                if (a(a2.values())) {
                    this.j.a(((Long) Collections.max(a2.keySet())).longValue());
                }
            } else {
                return;
            }
        }
    }
}
