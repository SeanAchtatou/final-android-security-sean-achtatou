package com.urbanairship.analytics;

import com.urbanairship.c;
import com.urbanairship.e;
import com.urbanairship.push.f;
import org.json.JSONException;
import org.json.JSONObject;

public final class q extends c {
    /* access modifiers changed from: package-private */
    public final String f() {
        return "push_service_started";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public final JSONObject g() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("session_id", c().f1167b);
            jSONObject.put("apid", f.b().h().a("com.urbanairship.push.APID"));
            jSONObject.put("transport", c.a().h().a().toString());
            jSONObject.put("push_enabled", f.b().h().a("com.urbanairship.push.PUSH_ENABLED", false));
        } catch (JSONException e) {
            e.e("Error constructing JSON data for " + "push_service_started");
        }
        return jSONObject;
    }
}
