package com.urbanairship.analytics;

import android.app.Activity;
import com.urbanairship.c;

public class InstrumentedActivity extends Activity {
    public void onStart() {
        super.onStart();
        c.a().k().a(this);
    }

    public void onStop() {
        super.onStop();
        c.a().k().b(this);
    }
}
