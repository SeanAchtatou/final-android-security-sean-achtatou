package com.urbanairship.analytics;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.urbanairship.c;
import com.urbanairship.e;
import java.util.HashMap;
import java.util.Map;

final class h {

    /* renamed from: a  reason: collision with root package name */
    private SQLiteDatabase f1159a;

    public h() {
        try {
            this.f1159a = new e(c.a().g()).getWritableDatabase();
            if (this.f1159a == null) {
                e.e("Unable to create or open the analytics database.");
            }
        } catch (SQLiteException e) {
            e.e("Unable to open Analytics Event DB");
        }
    }

    public final int a(c cVar) {
        if (this.f1159a == null) {
            e.e("Unable to insert event. Database not open.");
            return -1;
        }
        ContentValues contentValues = new ContentValues();
        int length = cVar.d().toString().length();
        contentValues.put("type", cVar.f());
        contentValues.put("event_id", cVar.a());
        contentValues.put("data", cVar.d().toString());
        contentValues.put("time", cVar.b());
        contentValues.put("session_id", cVar.e());
        contentValues.put("event_size", Integer.valueOf(length));
        if (this.f1159a.insert("events", null, contentValues) >= 0) {
            return length;
        }
        e.e("Error inserting event into Analytics DB.");
        return -1;
    }

    public final String a() {
        if (this.f1159a == null) {
            e.e("Unable to get session ID. Database not open.");
            return null;
        }
        Cursor query = this.f1159a.query("events", new String[]{"session_id"}, null, null, null, null, "_id ASC", "0, 1");
        String string = query.moveToFirst() ? query.getString(0) : null;
        query.close();
        return string;
    }

    public final Map a(int i) {
        if (this.f1159a == null) {
            e.e("Unable to get events. Database not open.");
            return null;
        }
        Cursor query = this.f1159a.query("events", new String[]{"_id", "data"}, null, null, null, null, "_id ASC", "0, " + i);
        HashMap hashMap = new HashMap(i);
        query.moveToFirst();
        while (!query.isAfterLast()) {
            hashMap.put(Long.valueOf(query.getLong(0)), query.getString(1));
            query.moveToNext();
        }
        query.close();
        return hashMap;
    }

    public final void a(long j) {
        e.d("Deleting old events");
        if (this.f1159a == null) {
            e.e("Unable to delete events. Database not open.");
            return;
        }
        this.f1159a.delete("events", "_id <= ?", new String[]{Long.toString(j)});
    }

    public final void a(String str) {
        if (this.f1159a == null) {
            e.e("Unable to delete events. Database not open.");
            return;
        }
        e.d("Deleted " + this.f1159a.delete("events", "type = ?", new String[]{str}) + " rows with event type = " + str);
    }

    public final int b() {
        if (this.f1159a == null) {
            e.e("Unable to get event count. Database not open.");
            return -1;
        }
        Cursor query = this.f1159a.query("events", new String[]{"COUNT(*) as _cnt"}, null, null, null, null, null);
        Integer valueOf = query.moveToFirst() ? Integer.valueOf(query.getInt(0)) : null;
        query.close();
        if (valueOf != null) {
            return valueOf.intValue();
        }
        return -1;
    }

    public final void b(String str) {
        if (this.f1159a == null) {
            e.e("Unable to delete session. Database not open.");
            return;
        }
        e.d("Deleted " + this.f1159a.delete("events", "session_id = ?", new String[]{str}) + " rows with session id " + str);
    }

    public final int c() {
        if (this.f1159a == null) {
            e.e("Unable to get DB size. Database not open.");
            return -1;
        }
        Cursor query = this.f1159a.query("events", new String[]{"SUM(event_size) as _size"}, null, null, null, null, null);
        Integer valueOf = query.moveToFirst() ? Integer.valueOf(query.getInt(0)) : null;
        query.close();
        if (valueOf != null) {
            return valueOf.intValue();
        }
        return -1;
    }
}
