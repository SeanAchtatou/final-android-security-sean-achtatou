package com.urbanairship.analytics;

import com.urbanairship.e;
import com.urbanairship.push.d;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class f extends c {
    /* access modifiers changed from: package-private */
    public final String f() {
        return "push_preferences_changed";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public final JSONObject g() {
        JSONObject jSONObject = new JSONObject();
        d h = com.urbanairship.push.f.b().h();
        try {
            jSONObject.put("session_id", c().f1167b);
            jSONObject.put("notification_types", new JSONArray((Collection) m.c()));
            Date[] g = h.g();
            if (h.a("com.urbanairship.push.QuietTime.ENABLED", false) && g != null) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
                ArrayList arrayList = new ArrayList(2);
                arrayList.add(simpleDateFormat.format(g[0]));
                arrayList.add(simpleDateFormat.format(g[1]));
                jSONObject.put("quiet_time", new JSONArray((Collection) arrayList));
            }
            jSONObject.put("push_enabled", h.a("com.urbanairship.push.PUSH_ENABLED", false));
        } catch (JSONException e) {
            e.e("Error constructing JSON data for " + "push_preferences_changed");
        }
        return jSONObject;
    }
}
