package com.urbanairship.analytics;

import android.os.Build;
import android.telephony.TelephonyManager;
import com.urbanairship.c;
import com.urbanairship.e;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class n extends c {
    n() {
    }

    /* access modifiers changed from: package-private */
    public final String f() {
        return "app_foreground";
    }

    /* access modifiers changed from: package-private */
    public final JSONObject g() {
        JSONObject jSONObject = new JSONObject();
        m c = c();
        try {
            jSONObject.put("connection_type", m.a());
            String b2 = m.b();
            if (b2.length() > 0) {
                jSONObject.put("connection_subtype", b2);
            }
            jSONObject.put("carrier", ((TelephonyManager) c.a().g().getSystemService("phone")).getNetworkOperatorName());
            jSONObject.put("time_zone", (long) (Calendar.getInstance().getTimeZone().getOffset(System.currentTimeMillis()) / 1000));
            jSONObject.put("daylight_savings", Calendar.getInstance().getTimeZone().inDaylightTime(new Date()));
            jSONObject.put("notification_types", new JSONArray((Collection) m.c()));
            jSONObject.put("os_version", Build.VERSION.RELEASE);
            jSONObject.put("lib_version", c.j());
            jSONObject.put("package_version", c.c().versionName);
            jSONObject.put("session_id", c.f1167b);
            jSONObject.put("push_id", c.f1166a);
        } catch (JSONException e) {
            e.e("Error constructing JSON data for " + "app_foreground");
        }
        return jSONObject;
    }
}
