package com.urbanairship.analytics;

import com.urbanairship.e;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class c {

    /* renamed from: a  reason: collision with root package name */
    private String f1153a = UUID.randomUUID().toString();

    /* renamed from: b  reason: collision with root package name */
    private String f1154b = this.d.f1167b;
    private String c = Long.toString(System.currentTimeMillis() / 1000);
    private m d = new m(this);

    c() {
    }

    /* access modifiers changed from: package-private */
    public final String a() {
        return this.f1153a;
    }

    /* access modifiers changed from: package-private */
    public final String b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final m c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        JSONObject g = g();
        try {
            jSONObject.put("type", f());
            jSONObject.put("event_id", this.f1153a);
            jSONObject.put("time", this.c);
            jSONObject.put("data", g);
        } catch (JSONException e) {
            e.e("Error constructing JSON " + f() + " representation");
        }
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    public final String e() {
        return this.f1154b;
    }

    /* access modifiers changed from: package-private */
    public abstract String f();

    /* access modifiers changed from: package-private */
    public abstract JSONObject g();
}
