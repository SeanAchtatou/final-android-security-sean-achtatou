package com.urbanairship.analytics;

import android.app.ListActivity;
import com.urbanairship.c;

public class InstrumentedListActivity extends ListActivity {
    public void onStart() {
        super.onStart();
        c.a().k().a(this);
    }

    public void onStop() {
        super.onStop();
        c.a().k().b(this);
    }
}
