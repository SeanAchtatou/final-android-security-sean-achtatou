package com.urbanairship.analytics;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.urbanairship.c;
import com.urbanairship.push.d;
import com.urbanairship.push.f;
import java.util.ArrayList;

final class m {

    /* renamed from: a  reason: collision with root package name */
    String f1166a = c.a().k().a();

    /* renamed from: b  reason: collision with root package name */
    String f1167b = c.a().k().b().a();
    private /* synthetic */ c c;

    public m(c cVar) {
        this.c = cVar;
    }

    public static String a() {
        NetworkInfo activeNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) c.a().g().getSystemService("connectivity");
        switch ((connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null) ? -1 : activeNetworkInfo.getType()) {
            case 0:
                return "cell";
            case 1:
                return "wifi";
            case 6:
                return "wimax";
            default:
                return "none";
        }
    }

    public static String b() {
        NetworkInfo activeNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) c.a().g().getSystemService("connectivity");
        return (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null) ? "" : activeNetworkInfo.getSubtypeName();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.urbanairship.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.urbanairship.push.d.a(java.util.Date, java.util.Date):boolean
      com.urbanairship.a.a(java.lang.String, int):int
      com.urbanairship.a.a(java.lang.String, long):long
      com.urbanairship.a.a(java.lang.String, java.lang.String):boolean
      com.urbanairship.a.a(java.lang.String, boolean):boolean */
    public static ArrayList c() {
        d h = f.b().h();
        ArrayList arrayList = new ArrayList();
        if (h.a("com.urbanairship.push.SOUND_ENABLED", true)) {
            arrayList.add("sound");
        }
        if (h.a("com.urbanairship.push.VIBRATE_ENABLED", true)) {
            arrayList.add("vibrate");
        }
        return arrayList;
    }
}
