package com.urbanairship.analytics;

import android.app.Activity;
import java.util.HashSet;

final class j {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public HashSet f1162a = new HashSet();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public r f1163b;

    public j(r rVar) {
        this.f1163b = rVar;
    }

    public final void a(Activity activity) {
        if (this.f1162a.isEmpty()) {
            this.f1163b.a();
        }
        this.f1162a.add(Integer.valueOf(activity.hashCode()));
    }
}
