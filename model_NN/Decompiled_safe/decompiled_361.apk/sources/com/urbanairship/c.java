package com.urbanairship;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.urbanairship.analytics.g;
import com.urbanairship.b.h;
import com.urbanairship.push.f;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static final c f1207a = new c();

    /* renamed from: b  reason: collision with root package name */
    private Context f1208b;
    private b c;
    private g d;
    private boolean e = false;

    private c() {
    }

    public static c a() {
        return f1207a;
    }

    public static void a(Application application, b bVar) {
        if (application == null) {
            throw new IllegalArgumentException("Application argument must not be null");
        } else if (f1207a.e) {
            e.e("You can only call UAirship.takeOff once.");
        } else {
            c cVar = f1207a;
            Context applicationContext = application.getApplicationContext();
            cVar.f1208b = applicationContext;
            b a2 = bVar == null ? b.a(applicationContext) : bVar;
            f1207a.c = a2;
            if (!a2.d) {
                e.f1224a = 3;
            } else {
                e.f1224a = 6;
            }
            e.f1225b = e() + " - UALib";
            e.c("Airship Take Off! Lib Version: 1.0.0 / App key = " + a2.b() + " / secret = " + a2.c());
            e.c("In Production? " + a2.d);
            if (!(a2.b() != null && a2.b().length() > 0 && a2.b().indexOf(32) < 0)) {
                throw new RuntimeException("Application configuration is invalid.");
            }
            new f().a_(applicationContext);
            f1207a.e = true;
            if (a2.e) {
                e.c("Initializing Push.");
                f.a();
            }
            if (a2.f) {
                e.c("Initializing IAP.");
                h.b();
            }
            e.c("Initializing Analytics.");
            f1207a.d = new g();
        }
    }

    public static String b() {
        return f1207a.f1208b.getPackageName();
    }

    public static PackageInfo c() {
        try {
            return l().getPackageInfo(b(), 0);
        } catch (PackageManager.NameNotFoundException e2) {
            e.d("NameNotFound for: " + b() + ". Disabling.");
            return null;
        }
    }

    public static ApplicationInfo d() {
        try {
            return l().getApplicationInfo(b(), 0);
        } catch (PackageManager.NameNotFoundException e2) {
            e.d("NameNotFound for: " + b() + ". Disabling.");
            return null;
        }
    }

    public static String e() {
        if (d() != null) {
            return l().getApplicationLabel(d()).toString();
        }
        return null;
    }

    public static int f() {
        return d().icon;
    }

    public static String j() {
        return "1.0.0";
    }

    private static PackageManager l() {
        return f1207a.f1208b.getPackageManager();
    }

    public final Context g() {
        return this.f1208b;
    }

    public final b h() {
        return this.c;
    }

    public final boolean i() {
        return this.e;
    }

    public final g k() {
        return this.d;
    }
}
