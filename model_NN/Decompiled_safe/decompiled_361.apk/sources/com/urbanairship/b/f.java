package com.urbanairship.b;

import com.urbanairship.e;
import java.io.File;
import java.text.DecimalFormat;
import java.util.Observer;
import org.json.JSONObject;

public final class f implements Comparable {

    /* renamed from: a  reason: collision with root package name */
    String f1179a;

    /* renamed from: b  reason: collision with root package name */
    String f1180b;
    private String c;
    private String d;
    private boolean e;
    private int f;
    private double g;
    private String h;
    private String i;
    private String j;
    private File k;
    private j l;
    private u m;

    f() {
        a(j.PURCHASED);
    }

    f(JSONObject jSONObject) {
        this.f1179a = jSONObject.optString("product_id");
        this.f1180b = jSONObject.optString("name");
        this.d = jSONObject.optString("description");
        this.c = jSONObject.optString("price", "");
        this.e = jSONObject.optBoolean("free");
        if (this.e) {
            this.c = "FREE";
        }
        this.f = jSONObject.optInt("current_revision", 1);
        this.g = jSONObject.optDouble("file_size", 0.0d);
        this.h = jSONObject.optString("preview_url");
        this.i = jSONObject.optString("icon_url");
        this.j = jSONObject.optString("download_url");
        if (y.c(this.f1179a)) {
            e.d("found purchase receipt for " + this.f1179a);
            y b2 = y.b(this.f1179a);
            if (b2.c().intValue() < this.f) {
                e.d("setting status to UPDATE for " + this.f1179a);
                this.l = j.UPDATE;
            } else if (g.a(this.f1179a)) {
                e.d(this.f1179a + " is pending");
                f a2 = h.a().f().a(this.f1179a);
                if (a2 != null) {
                    this.l = a2.l;
                    e.d(this.f1179a + " has existing status, copying");
                } else {
                    e.d("setting status to PURCHASED for " + this.f1179a);
                    this.l = j.PURCHASED;
                }
            } else {
                this.l = j.INSTALLED;
                e.d("setting status to INSTALLED for " + this.f1179a);
            }
            String d2 = b2.d();
            if (d2 != null) {
                this.k = new File(d2);
                e.d("Download path for " + this.f1180b + ": " + this.k);
            } else {
                e.d("No download path found for " + this.f1180b);
            }
        } else {
            this.l = j.UNPURCHASED;
            e.d("no receipt found for " + this.f1179a + ", setting status to UNPURCHASED");
        }
        this.m = new u(this);
    }

    public final String a() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public final void a(j jVar) {
        this.l = jVar;
        this.m.notifyObservers(this);
    }

    /* access modifiers changed from: package-private */
    public final void a(File file) {
        this.k = file;
    }

    public final void a(Observer observer) {
        this.m.addObserver(observer);
    }

    public final String b() {
        return this.f1180b;
    }

    public final void b(Observer observer) {
        this.m.deleteObserver(observer);
    }

    public final String c() {
        return this.d;
    }

    public final /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return this.f1180b.compareTo(((f) obj).f1180b);
    }

    public final int d() {
        return this.f;
    }

    public final String e() {
        String str;
        double d2 = this.g;
        double d3 = d2 / 1000.0d;
        double d4 = d2 / 1000000.0d;
        if (d2 < 1000.0d) {
            str = "Bytes";
        } else if (d3 < 1.0d || d3 >= 1000.0d) {
            str = "MB";
            d2 = d4;
        } else {
            double d5 = d3;
            str = "KB";
            d2 = d5;
        }
        return new DecimalFormat("#0.0").format(d2) + str;
    }

    public final String f() {
        return this.h;
    }

    public final boolean g() {
        return this.e;
    }

    public final String h() {
        return this.c;
    }

    public final String i() {
        return this.f1179a;
    }

    public final j j() {
        return this.l;
    }

    public final String k() {
        return this.j;
    }
}
