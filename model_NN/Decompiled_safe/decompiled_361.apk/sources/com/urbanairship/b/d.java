package com.urbanairship.b;

import com.urbanairship.a.i;
import com.urbanairship.c.h;
import com.urbanairship.e;
import java.io.File;

final class d extends h {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ File f1175a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ String f1176b;
    private /* synthetic */ f c;
    private /* synthetic */ File d;
    private /* synthetic */ g e;

    d(g gVar, File file, String str, f fVar, File file2) {
        this.e = gVar;
        this.f1175a = file;
        this.f1176b = str;
        this.c = fVar;
        this.d = file2;
    }

    public final void a(int i) {
        e.b("Download " + this.c + " progress " + i);
        l b2 = this.e.d.b(this.f1176b);
        b2.a(i);
        b.a(b2);
        i l = h.a().l();
        if (l != null) {
            l.b(this.c, i);
        }
    }

    public final void a(com.urbanairship.c.d dVar) {
        e.d("Downloaded product to " + this.f1175a + ", extracting...");
        l b2 = this.e.d.b(this.f1176b);
        b2.a(100);
        i l = h.a().l();
        if (l != null) {
            l.b(this.c, 100);
        }
        b2.a(n.DECOMPRESSING);
        b.a(b2);
        i iVar = new i();
        iVar.a(new s(this.e, this.c, this.d));
        iVar.execute(this.f1175a, this.d);
    }

    public final void a(Exception exc) {
        e.e("Download " + this.c + " failed");
        this.e.b(this.c);
    }
}
