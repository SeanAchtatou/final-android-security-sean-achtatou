package com.urbanairship.b;

import android.content.SharedPreferences;
import com.urbanairship.c;
import com.urbanairship.e;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

final class y {

    /* renamed from: a  reason: collision with root package name */
    private String f1205a;

    /* renamed from: b  reason: collision with root package name */
    private String f1206b;
    private String c;
    private String d;
    private Integer e;
    private Long f;
    private String g;

    y(Integer num, String str) {
        this(num, str, null, null, null, null);
    }

    private y(Integer num, String str, String str2, Long l, String str3, String str4) {
        this.f1205a = str;
        this.e = num;
        this.f1206b = str2;
        this.f = l;
        this.c = str3;
        this.d = str4;
        this.g = null;
    }

    y(Integer num, String str, String str2, Long l, String str3, String str4, byte b2) {
        this(num, str, str2, l, str3, str4);
    }

    private y(String str) {
        try {
            JSONObject jSONObject = (JSONObject) new JSONTokener(str).nextValue();
            this.f1205a = jSONObject.getString("productId");
            this.e = Integer.valueOf(jSONObject.getInt("productRevision"));
            this.f1206b = jSONObject.optString("orderId", null);
            this.f = Long.valueOf(jSONObject.optLong("purchaseTime"));
            this.c = jSONObject.optString("data", null);
            this.d = jSONObject.optString("signature", null);
            this.g = jSONObject.optString("downloadPathString", null);
        } catch (JSONException e2) {
            e.e("Error parsing receipt");
        }
    }

    static y b(String str) {
        String str2;
        try {
            str2 = f().getString(str, null);
        } catch (ClassCastException e2) {
            e.e("receipt not found for " + str);
            str2 = null;
        }
        if (str2 != null) {
            return new y(str2);
        }
        return null;
    }

    static boolean c(String str) {
        return f().contains(str);
    }

    private static SharedPreferences f() {
        return c.a().g().getSharedPreferences("com.urbanairship.iap.receipts", 0);
    }

    /* access modifiers changed from: package-private */
    public final String a() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final void a(Integer num) {
        this.e = num;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.g = str;
    }

    /* access modifiers changed from: package-private */
    public final String b() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final Integer c() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final String d() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public final boolean e() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("productRevision", this.e);
            jSONObject.put("productId", this.f1205a);
            if (this.f1206b != null) {
                jSONObject.put("orderId", this.f1206b);
            }
            if (this.f != null) {
                jSONObject.put("purchaseTime", this.f);
            }
            if (this.c != null) {
                jSONObject.put("data", this.c);
            }
            if (this.d != null) {
                jSONObject.put("signature", this.d);
            }
            if (this.g != null) {
                jSONObject.put("downloadPathString", this.g);
            }
            try {
                SharedPreferences.Editor edit = f().edit();
                edit.putString(this.f1205a, jSONObject.toString());
                edit.commit();
                return true;
            } catch (Exception e2) {
                e.e("error writing receipt");
                return false;
            }
        } catch (JSONException e3) {
            e.e("error constructing JSON object out of receipt data");
            return false;
        }
    }
}
