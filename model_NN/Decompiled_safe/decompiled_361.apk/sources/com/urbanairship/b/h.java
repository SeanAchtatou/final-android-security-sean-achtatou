package com.urbanairship.b;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import com.google.iap.BillingService;
import com.google.iap.m;
import com.urbanairship.c;
import com.urbanairship.e;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private static final h f1183a = new h();

    /* renamed from: b  reason: collision with root package name */
    private e f1184b;
    private String c = null;
    private int d;
    private g e;
    private BillingService f;
    private p g;
    private k h;
    private boolean i = false;
    private String j;
    private String k;
    private i l;

    private h() {
    }

    public static h a() {
        return f1183a;
    }

    static void a(boolean z) {
        f1183a.i = z;
        if (f1183a.l != null) {
            f1183a.l.a(z);
        }
    }

    public static void b() {
        f1183a.f1184b = new e();
        f1183a.e = new g();
        f1183a.h = new q();
        f1183a.f = new BillingService();
        f1183a.f.a(c.a().g());
        f1183a.c = c.e();
        f1183a.d = c.f();
        f1183a.f.a();
        f1183a.l = new a();
        if (f1183a.g != null) {
            m.a();
        }
        f1183a.g = new p(new Handler());
        m.a(f1183a.g);
    }

    public static boolean c() {
        return f1183a.i;
    }

    static void d() {
        try {
            SharedPreferences.Editor edit = c.a().g().getSharedPreferences("com.urbanairship.iap.first_run", 0).edit();
            edit.putBoolean("initialized", true);
            edit.commit();
        } catch (Exception e2) {
            e.e("Error writing to shared preferences");
        }
    }

    static boolean e() {
        return !c.a().g().getSharedPreferences("com.urbanairship.iap.first_run", 0).getBoolean("initialized", false);
    }

    public final void a(k kVar) {
        this.h = kVar;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(Activity activity, f fVar) {
        if (!this.i) {
            e.e("Billing is not supported on this version of Android Market");
            return false;
        } else if (this.f.a(activity, fVar.f1179a)) {
            return true;
        } else {
            if (this.l != null) {
                this.l.b();
            }
            return false;
        }
    }

    public final e f() {
        return this.f1184b;
    }

    /* access modifiers changed from: package-private */
    public final g g() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final BillingService h() {
        return this.f;
    }

    public final k i() {
        return this.h;
    }

    public final String j() {
        return this.j != null ? this.j : "iap/temp/" + c.e() + "/";
    }

    public final String k() {
        return this.k != null ? this.k : "iap/downloads/" + c.e() + "/";
    }

    public final i l() {
        return this.l;
    }
}
