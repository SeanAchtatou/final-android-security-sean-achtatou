package com.urbanairship.b;

import com.urbanairship.c.d;
import com.urbanairship.c.h;
import com.urbanairship.e;
import org.json.JSONObject;
import org.json.JSONTokener;

final class c extends h {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ f f1173a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ String f1174b;
    private /* synthetic */ g c;

    c(g gVar, f fVar, String str) {
        this.c = gVar;
        this.f1173a = fVar;
        this.f1174b = str;
    }

    public final void a(d dVar) {
        String c2 = dVar.c();
        e.b("verifyRequest result " + c2);
        if (dVar.a() == 200) {
            try {
                g.a(this.c, this.f1173a, ((JSONObject) new JSONTokener(c2).nextValue()).getString("content_url"));
            } catch (Exception e) {
                e.e("Error parsing verification response from server, for product " + this.f1174b);
                this.c.b(this.f1173a);
            }
        } else {
            e.c("verifyRequest response status: " + dVar.a());
            this.c.b(this.f1173a);
        }
    }

    public final void a(Exception exc) {
        e.e("Error fetching content url from server, for product: " + this.f1173a);
        this.c.b(this.f1173a);
    }
}
