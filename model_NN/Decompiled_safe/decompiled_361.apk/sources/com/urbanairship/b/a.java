package com.urbanairship.b;

import com.urbanairship.e;

public final class a extends i {
    public final void a() {
        com.urbanairship.a.a.a("Checking for existing purchases", 0);
    }

    public final void a(f fVar) {
        com.urbanairship.a.a.a(String.format("Download of %s failed", fVar.f1180b), 0);
    }

    public final void a(f fVar, int i) {
        if (i == 1) {
            com.urbanairship.a.a.a(String.format("Downloading %s...", fVar.f1180b), 0);
        }
    }

    public final void a(boolean z) {
        if (!z) {
            com.urbanairship.a.a.a("Billing is not supported on this version of Android Market", 1);
        }
    }

    public final void b() {
        com.urbanairship.a.a.a("Error connecting to billing service, please try again later", 1);
    }

    public final void b(f fVar) {
        com.urbanairship.a.a.a(fVar.f1180b + " was sucessfully installed", 0);
    }

    public final void b(f fVar, int i) {
        e.b("Download progress for " + fVar.f1180b + ": " + i + "%");
    }
}
