package com.urbanairship.b;

import com.urbanairship.a.c;
import com.urbanairship.e;
import java.io.File;

final class s extends c {

    /* renamed from: a  reason: collision with root package name */
    private f f1197a;

    /* renamed from: b  reason: collision with root package name */
    private File f1198b;
    private /* synthetic */ g c;

    public s(g gVar, f fVar, File file) {
        this.c = gVar;
        this.f1197a = fVar;
        this.f1198b = file;
    }

    public final void a() {
        String str = this.f1197a.f1179a;
        l b2 = this.c.d.b(str);
        b2.a(n.INSTALL_SUCCESSFUL);
        this.f1197a.a(j.INSTALLED);
        this.f1197a.a(this.f1198b);
        e.b("Setting download path for " + b2.b() + ": " + this.f1198b);
        y b3 = y.b(this.f1197a.f1179a);
        b3.a(this.f1198b.toString());
        b3.e();
        this.c.d.a(str);
        g.b(str);
        this.c.f1182b.remove(str);
        i l = h.a().l();
        if (l != null) {
            l.b(this.f1197a);
        }
    }

    public final void b() {
        String str = this.f1197a.f1179a;
        e.e(String.format("Extraction of %s failed", this.f1197a.f1180b));
        l b2 = this.c.d.b(str);
        b2.a(n.DECOMPRESS_FAILED);
        b.a(b2);
        this.c.b(this.f1197a);
    }
}
