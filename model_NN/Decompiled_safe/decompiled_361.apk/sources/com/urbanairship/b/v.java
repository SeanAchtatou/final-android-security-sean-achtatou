package com.urbanairship.b;

import java.util.Observable;
import java.util.Observer;

final class v implements Observer {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ g f1202a;

    /* synthetic */ v(g gVar) {
        this(gVar, (byte) 0);
    }

    private v(g gVar, byte b2) {
        this.f1202a = gVar;
    }

    public final void update(Observable observable, Object obj) {
        if (((e) obj).a() != t.LOADED) {
            return;
        }
        if (!h.e() || !h.c()) {
            g.c(this.f1202a);
            return;
        }
        i l = h.a().l();
        if (l != null) {
            l.a();
        }
        h.a().h().b();
    }
}
