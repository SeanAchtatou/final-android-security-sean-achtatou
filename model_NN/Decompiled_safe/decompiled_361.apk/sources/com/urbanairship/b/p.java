package com.urbanairship.b;

import android.os.Handler;
import com.google.iap.e;
import com.google.iap.f;
import com.google.iap.h;
import com.google.iap.j;

public final class p extends j {
    public p(Handler handler) {
        super(handler);
    }

    public static void a(e eVar) {
        if (eVar == e.RESULT_OK) {
            com.urbanairship.e.d("OK response from onRestoreTransactionsRequest, calling firstRun()");
            h.d();
        }
    }

    public static void a(f fVar, String str, String str2, long j, String str3, String str4) {
        com.urbanairship.e.d("purchase state changed for " + str + ": " + fVar);
        if (fVar == f.PURCHASED) {
            f a2 = h.a().f().a(str);
            com.urbanairship.e.d("storing purchase receipt for " + str);
            new y(Integer.valueOf(a2.d()), str, str2, Long.valueOf(j), str3, str4, (byte) 0).e();
            com.urbanairship.e.d("starting download for " + str);
            h.a().g().a(a2);
        }
    }

    public static void a(h hVar, e eVar) {
        if (eVar != e.RESULT_OK) {
            h.a().f().a(hVar.f1127a).a(j.UNPURCHASED);
        }
    }

    public static void a(boolean z) {
        h.a(z);
    }
}
