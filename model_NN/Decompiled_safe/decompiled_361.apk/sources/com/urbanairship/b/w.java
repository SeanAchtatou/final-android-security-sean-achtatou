package com.urbanairship.b;

import com.urbanairship.c.d;
import com.urbanairship.c.h;
import com.urbanairship.e;
import org.json.JSONArray;
import org.json.JSONTokener;

final class w extends h {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ e f1203a;

    w(e eVar) {
        this.f1203a = eVar;
    }

    public final void a(d dVar) {
        if (dVar.a() == 200) {
            String c = dVar.c();
            try {
                e.d("Inventory response string: " + c);
                e.a(this.f1203a, (JSONArray) new JSONTokener(c).nextValue());
            } catch (Exception e) {
                e.e("Error parsing JSON product list");
                this.f1203a.a(t.FAILED);
            }
            this.f1203a.b();
            if (this.f1203a.a() == t.LOADED) {
                e.d("Inventory loaded " + this.f1203a.f1178b);
                return;
            }
            return;
        }
        if (dVar.a() == 401) {
            e.e("Authorization failed, make sure the application key and secret are propertly set");
        }
        e.d("inventoryRequest response status: " + dVar.a());
        e.d("inventoryRequest response string: " + dVar.c());
        this.f1203a.a(t.FAILED);
    }

    public final void a(Exception exc) {
        e.e("Error loading product inventory from server");
        this.f1203a.a(t.FAILED);
    }
}
