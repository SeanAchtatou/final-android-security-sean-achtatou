package com.urbanairship.b;

import android.app.NotificationManager;
import com.urbanairship.c;
import java.util.HashMap;

final class b {

    /* renamed from: a  reason: collision with root package name */
    private HashMap f1172a = new HashMap();

    b() {
    }

    static void a(l lVar) {
        ((NotificationManager) c.a().g().getSystemService("notification")).notify(lVar.c(), h.a().i().a(lVar));
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        l lVar = (l) this.f1172a.get(str);
        lVar.g();
        a(lVar);
        this.f1172a.remove(str);
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, String str2, n nVar) {
        this.f1172a.put(str, new l(nVar, str2, str));
    }

    /* access modifiers changed from: package-private */
    public final l b(String str) {
        return (l) this.f1172a.get(str);
    }

    /* access modifiers changed from: package-private */
    public final boolean c(String str) {
        return this.f1172a.containsKey(str);
    }
}
