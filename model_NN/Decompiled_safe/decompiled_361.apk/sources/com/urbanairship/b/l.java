package com.urbanairship.b;

import com.urbanairship.a.b;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private n f1187a;

    /* renamed from: b  reason: collision with root package name */
    private String f1188b;
    private String c;
    private int d;
    private int e;
    private int f = 0;
    private long g;

    public l(n nVar, String str, String str2) {
        this.f1187a = nVar;
        this.f1188b = str;
        this.c = str2;
        this.d = b.a();
        this.e = 10;
        this.g = System.currentTimeMillis();
    }

    public final n a() {
        return this.f1187a;
    }

    public final void a(int i) {
        this.f = i;
    }

    public final void a(n nVar) {
        this.f1187a = nVar;
    }

    public final String b() {
        return this.f1188b;
    }

    public final int c() {
        return this.d;
    }

    public final int d() {
        return this.e;
    }

    public final int e() {
        return this.f;
    }

    public final long f() {
        return this.g;
    }

    public final void g() {
        this.e = 16;
    }
}
