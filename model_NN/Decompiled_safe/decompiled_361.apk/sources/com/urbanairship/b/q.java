package com.urbanairship.b;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.urbanairship.c;

public class q implements k {

    /* renamed from: a  reason: collision with root package name */
    private int f1194a = c.d().icon;

    /* renamed from: b  reason: collision with root package name */
    private Context f1195b = c.a().g();

    protected static String b(l lVar) {
        switch (m.f1189a[lVar.a().ordinal()]) {
            case 1:
                return String.format("Downloading %s...", lVar.b());
            case 2:
                return String.format("Decompressing %s...", lVar.b());
            case 3:
                return String.format("Extraction of %s failed", lVar.b());
            case 4:
                return String.format("Download of %s failed", lVar.b());
            case 5:
                return lVar.b() + " was sucessfully installed";
            case 6:
                return "Verifying " + lVar.b();
            default:
                return "";
        }
    }

    public Notification a(l lVar) {
        PendingIntent activity = PendingIntent.getActivity(this.f1195b, 0, new Intent(), 0);
        Notification notification = new Notification(this.f1194a, lVar.b(), lVar.f());
        notification.flags = lVar.d();
        notification.setLatestEventInfo(this.f1195b, lVar.b(), b(lVar), activity);
        return notification;
    }
}
