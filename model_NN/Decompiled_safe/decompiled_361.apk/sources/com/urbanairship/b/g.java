package com.urbanairship.b;

import android.content.SharedPreferences;
import android.os.Environment;
import com.urbanairship.c;
import com.urbanairship.c.b;
import com.urbanairship.e;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private v f1181a = new v(this);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public HashMap f1182b = new HashMap();
    private com.urbanairship.c.g c = new com.urbanairship.c.g();
    /* access modifiers changed from: private */
    public b d = new b();

    g() {
        h.a().f().a(this.f1181a);
    }

    private static SharedPreferences a() {
        return c.a().g().getSharedPreferences("com.urbanairship.iap.pending_products", 0);
    }

    static /* synthetic */ void a(g gVar, f fVar, String str) {
        File filesDir;
        String str2 = fVar.f1179a;
        String str3 = fVar.f1180b;
        b bVar = new b("GET", str);
        fVar.a(j.DOWNLOADING);
        l b2 = gVar.d.b(str2);
        b2.a(n.DOWNLOADING);
        b.a(b2);
        try {
            if (Environment.getExternalStorageState().equals("mounted")) {
                filesDir = Environment.getExternalStorageDirectory();
                e.c("Writing to SD card");
            } else {
                filesDir = c.a().g().getFilesDir();
                e.d("SD card not available, writing to internal storage");
            }
            File canonicalFile = new File(filesDir, h.a().j() + str2 + ".zip").getCanonicalFile();
            File canonicalFile2 = new File(filesDir, h.a().k() + str3).getCanonicalFile();
            bVar.a(canonicalFile);
            gVar.c.a(bVar, new d(gVar, canonicalFile, str2, fVar, canonicalFile2));
        } catch (Exception e) {
            e.e("Error downloading product " + str3);
            gVar.b(fVar);
        }
    }

    public static boolean a(String str) {
        return a().contains(str);
    }

    /* access modifiers changed from: private */
    public void b(f fVar) {
        String str = fVar.f1179a;
        String str2 = fVar.f1180b;
        int intValue = ((Integer) this.f1182b.get(str)).intValue();
        fVar.a(j.PURCHASED);
        if (((Integer) this.f1182b.get(str)).intValue() < 3) {
            this.f1182b.put(str, Integer.valueOf(intValue + 1));
            e.d("Retrying download of " + str2);
            c(fVar);
            return;
        }
        e.d(String.format("Already tried downloading %s %d times, giving up for now", str2, 3));
        this.d.b(str).a(n.DOWNLOAD_FAILED);
        this.d.a(str);
        this.f1182b.remove(str);
        i l = h.a().l();
        if (l != null) {
            l.a(fVar);
        }
    }

    static /* synthetic */ void b(String str) {
        SharedPreferences.Editor edit = a().edit();
        edit.remove(str);
        edit.commit();
    }

    private void c(f fVar) {
        String str = fVar.f1179a;
        String str2 = fVar.f1180b;
        fVar.a(j.WAITING);
        if (!this.d.c(str)) {
            this.d.a(str, str2, n.VERIFYING_RECEIPT);
        }
        if (!this.f1182b.containsKey(str)) {
            this.f1182b.put(str, 1);
        }
        com.urbanairship.c.c cVar = new com.urbanairship.c.c("POST", fVar.k() + "?platform=android");
        y b2 = y.b(str);
        String a2 = b2.a();
        String b3 = b2.b();
        if (a2 == null || b3 == null) {
            e.c("free product, sending bare request");
        } else {
            e.c("paid product, verifying receipt");
            e.c("receipt: " + a2 + " signature: " + b3);
            try {
                StringEntity stringEntity = new StringEntity(a2);
                stringEntity.setContentType("application/json");
                cVar.setEntity(stringEntity);
            } catch (UnsupportedEncodingException e) {
                e.e("Error setting verifyRequest entity");
            }
            cVar.addHeader(new BasicHeader("x-google-iap-signature", b3));
        }
        this.c.a(cVar, new c(this, fVar, str2));
        b.a(this.d.b(str));
        i l = h.a().l();
        if (l != null) {
            l.a(fVar, ((Integer) this.f1182b.get(str)).intValue());
        }
    }

    static /* synthetic */ void c(g gVar) {
        e.b("resumePendingProducts");
        for (Map.Entry<String, ?> key : a().getAll().entrySet()) {
            f a2 = h.a().f().a((String) key.getKey());
            String str = a2.f1180b;
            j j = a2.j();
            if (j == j.DOWNLOADING || j == j.WAITING) {
                e.d(str + " is already downloading");
            } else {
                e.d("resuming download of " + str);
                gVar.c(a2);
            }
        }
    }

    public final void a(f fVar) {
        String str = fVar.f1179a;
        if (!a(str)) {
            SharedPreferences.Editor edit = a().edit();
            edit.putBoolean(str, true);
            edit.commit();
        }
        c(fVar);
    }
}
