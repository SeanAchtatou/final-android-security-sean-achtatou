package com.urbanairship.b;

import android.app.Activity;
import com.urbanairship.c;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observer;
import java.util.TreeMap;
import org.json.JSONArray;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private Map f1177a = new TreeMap();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ArrayList f1178b = new ArrayList();
    private ArrayList c = new ArrayList();
    private ArrayList d = new ArrayList();
    private t e = t.INITIALIZED;
    private r f = new r(this);

    e() {
    }

    static /* synthetic */ void a(e eVar, JSONArray jSONArray) {
        for (int i = 0; i < jSONArray.length(); i++) {
            f fVar = new f(jSONArray.getJSONObject(i));
            eVar.f1177a.put(fVar.f1179a, fVar);
        }
    }

    public final f a(String str) {
        return (f) this.f1177a.get(str);
    }

    public final t a() {
        return this.e;
    }

    public final List a(o oVar) {
        ArrayList arrayList = null;
        switch (x.f1204a[oVar.ordinal()]) {
            case 1:
                arrayList = this.f1178b;
                break;
            case 2:
                arrayList = this.d;
                break;
            case 3:
                arrayList = this.c;
                break;
            default:
                com.urbanairship.e.e("No product list for " + oVar);
                break;
        }
        return Collections.unmodifiableList(arrayList);
    }

    public final void a(Activity activity, f fVar) {
        String str = fVar.f1179a;
        String str2 = fVar.f1180b;
        if (this.e == t.LOADED) {
            com.urbanairship.e.d("Retrieving product: " + str2);
            if (fVar.g() || fVar.j() == j.UPDATE) {
                com.urbanairship.e.d("Free or updated product, will download if valid...");
                if (y.c(fVar.f1179a)) {
                    y b2 = y.b(str);
                    b2.a(Integer.valueOf(fVar.d()));
                    b2.e();
                } else {
                    new y(Integer.valueOf(fVar.d()), fVar.f1179a).e();
                }
                h.a().g().a(fVar);
            } else if (activity != null) {
                com.urbanairship.e.d("Paid product, attempting to purchase: [" + fVar.f1179a + "] " + fVar.f1180b);
                fVar.a(j.WAITING);
                if (!h.a().a(activity, fVar)) {
                    fVar.a(j.UNPURCHASED);
                }
            } else {
                com.urbanairship.e.a("Attempting to purchase product with null activity parameter, bailing");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(t tVar) {
        this.e = tVar;
        this.f.notifyObservers(this);
    }

    public final void a(Observer observer) {
        this.f.addObserver(observer);
    }

    public final int b(o oVar) {
        switch (x.f1204a[oVar.ordinal()]) {
            case 1:
                return this.f1178b.size();
            case 2:
                return this.d.size();
            case 3:
                return this.c.size();
            default:
                com.urbanairship.e.e("No product list for " + oVar);
                return 0;
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        com.urbanairship.e.d("Updating inventory");
        this.f1178b.clear();
        this.f1178b.addAll(this.f1177a.values());
        Collections.sort(this.f1178b);
        this.c.clear();
        this.d.clear();
        Iterator it = this.f1178b.iterator();
        while (it.hasNext()) {
            f fVar = (f) it.next();
            if (fVar.j() == j.UPDATE) {
                this.c.add(fVar);
            }
            if (fVar.j() == j.PURCHASED || fVar.j() == j.DOWNLOADING || fVar.j() == j.INSTALLED) {
                this.d.add(fVar);
            }
        }
        if (this.f1178b.size() > 0) {
            a(t.LOADED);
        } else {
            a(t.EMPTY);
        }
    }

    public final void b(Observer observer) {
        this.f.deleteObserver(observer);
    }

    public final void c() {
        if (!c.a().i()) {
            throw new UnsupportedOperationException("Inventory cannot be loaded unless UAirship.takeOff() has been called");
        }
        com.urbanairship.e.d("Loading inventory");
        String str = c.a().h().f1170a + "api/app/content/?platform=android";
        com.urbanairship.e.d("Fetching inventory from: " + str);
        new com.urbanairship.c.c("GET", str).a(new w(this));
        a(t.DOWNLOADING);
    }
}
