package com.urbanairship;

import android.content.Context;

public final class b extends g {

    /* renamed from: a  reason: collision with root package name */
    public String f1170a = "https://go.urbanairship.com/";

    /* renamed from: b  reason: collision with root package name */
    public String f1171b = "https://combine.urbanairship.com/";
    public String c;
    public boolean d = false;
    public boolean e = true;
    public boolean f = true;
    public boolean g = true;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;

    public static b a(Context context) {
        b bVar = new b();
        bVar.a_(context);
        return bVar;
    }

    public final d a() {
        return d.C2DM.toString().equalsIgnoreCase(this.l) ? d.C2DM : d.HELIUM.toString().equalsIgnoreCase(this.l) ? d.HELIUM : d.HYBRID.toString().equalsIgnoreCase(this.l) ? d.HYBRID : (this.c == null || this.c.length() <= 0) ? d.HELIUM : d.C2DM;
    }

    public final String b() {
        return this.d ? this.h : this.j;
    }

    public final String c() {
        return this.d ? this.i : this.k;
    }

    public final String d() {
        return "airshipconfig.properties";
    }
}
