package com.urbanairship.c;

import android.os.AsyncTask;
import java.io.FileOutputStream;
import java.io.InputStream;

final class a extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private h f1209a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ b f1210b;

    public a(b bVar, h hVar) {
        this.f1210b = bVar;
        this.f1209a = hVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public d doInBackground(b... bVarArr) {
        int i = 0;
        if (bVarArr.length > 1) {
            throw new RuntimeException("Background Request only handles executing one Request at a time ");
        } else if (bVarArr.length <= 0) {
            return null;
        } else {
            try {
                d a2 = bVarArr[0].a();
                if (this.f1210b.f1214b == null || a2 == null) {
                    return a2;
                }
                this.f1210b.f1214b.getParentFile().mkdirs();
                long contentLength = a2.f1215a.getEntity().getContentLength();
                InputStream content = a2.f1215a.getEntity().getContent();
                FileOutputStream fileOutputStream = new FileOutputStream(this.f1210b.f1214b);
                byte[] bArr = new byte[8192];
                while (true) {
                    int read = content.read(bArr);
                    if (read != -1) {
                        i += read;
                        fileOutputStream.write(bArr, 0, read);
                        publishProgress(Integer.valueOf((int) ((((float) i) / ((float) contentLength)) * 100.0f)));
                    } else {
                        fileOutputStream.flush();
                        content.close();
                        fileOutputStream.close();
                        return a2;
                    }
                }
            } catch (Exception e) {
                return null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        d dVar = (d) obj;
        if (dVar != null) {
            this.f1209a.a(dVar);
        } else {
            this.f1209a.a(new Exception("Error when executing request."));
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onProgressUpdate(Object[] objArr) {
        this.f1209a.a(((Integer[]) objArr)[0].intValue());
    }
}
