package com.urbanairship.c;

import android.os.Handler;
import android.os.Looper;
import com.urbanairship.c.a.a;
import com.urbanairship.e;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class b extends HttpEntityEnclosingRequestBase {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f1213a = true;

    /* renamed from: b  reason: collision with root package name */
    File f1214b;
    private String c;
    private DefaultHttpClient d;

    public b(String str, String str2) {
        this.c = str;
        setURI(URI.create(str2));
        this.params = new BasicHttpParams();
        if (f1213a) {
            this.d = new DefaultHttpClient(this.params);
        } else {
            e.e("Verify SSL Cert: false");
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", new a(), 443));
            this.d = new DefaultHttpClient(new SingleClientConnManager(this.params, schemeRegistry), this.params);
        }
        HttpConnectionParams.setSocketBufferSize(this.params, 16384);
        a(30000);
        e.b("Set Timeout: " + HttpConnectionParams.getConnectionTimeout(this.d.getParams()));
        e.b("Set Socket Buffer Size: " + HttpConnectionParams.getSocketBufferSize(this.d.getParams()));
    }

    public final d a() {
        try {
            return new d(this.d.execute(this));
        } catch (IOException e) {
            e.e("Error when executing request: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public final void a(int i) {
        HttpConnectionParams.setConnectionTimeout(this.params, i);
    }

    public final void a(h hVar) {
        new Handler(Looper.getMainLooper()).post(new f(this, hVar, this));
    }

    public final void a(File file) {
        this.f1214b = file;
    }

    public final void a(String str, String str2) {
        UsernamePasswordCredentials usernamePasswordCredentials = new UsernamePasswordCredentials(str, str2);
        BasicCredentialsProvider basicCredentialsProvider = new BasicCredentialsProvider();
        basicCredentialsProvider.setCredentials(AuthScope.ANY, usernamePasswordCredentials);
        this.d.setCredentialsProvider(basicCredentialsProvider);
    }

    public String getMethod() {
        return this.c;
    }
}
