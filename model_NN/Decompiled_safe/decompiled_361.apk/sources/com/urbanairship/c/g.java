package com.urbanairship.c;

import com.urbanairship.e;
import java.util.HashMap;
import java.util.LinkedList;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private LinkedList f1220a = new LinkedList();

    /* renamed from: b  reason: collision with root package name */
    private HashMap f1221b = new HashMap();
    private int c = 3;
    private int d = 0;

    private void a() {
        while (this.d < this.c && !this.f1220a.isEmpty()) {
            b bVar = (b) this.f1220a.poll();
            e.b("running request " + bVar.getURI());
            this.d++;
            h hVar = (h) this.f1221b.get(bVar);
            try {
                bVar.a(new e(this, bVar, hVar));
            } catch (Exception e) {
                e.e("Error running request");
                hVar.a(e);
                a(bVar);
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(b bVar) {
        e.b("removing request " + bVar.getURI());
        this.f1221b.remove(bVar);
        this.d--;
        a();
    }

    public final synchronized void a(b bVar, h hVar) {
        e.b("adding request " + bVar.getURI());
        this.f1220a.add(bVar);
        this.f1221b.put(bVar, hVar);
        a();
    }
}
