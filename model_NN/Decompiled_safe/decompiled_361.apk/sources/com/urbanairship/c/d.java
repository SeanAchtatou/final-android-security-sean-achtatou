package com.urbanairship.c;

import com.urbanairship.e;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    HttpResponse f1215a;

    public d(HttpResponse httpResponse) {
        this.f1215a = httpResponse;
    }

    public final int a() {
        return this.f1215a.getStatusLine().getStatusCode();
    }

    public final Header a(String str) {
        return this.f1215a.getFirstHeader(str);
    }

    public final String b() {
        Header firstHeader = this.f1215a.getFirstHeader("Content-Type");
        if (firstHeader != null) {
            return firstHeader.getValue();
        }
        return null;
    }

    public final String c() {
        if (this.f1215a.getEntity() == null) {
            return "";
        }
        try {
            return EntityUtils.toString(this.f1215a.getEntity());
        } catch (Exception e) {
            e.e("Error fetching http entity");
            return "";
        }
    }
}
