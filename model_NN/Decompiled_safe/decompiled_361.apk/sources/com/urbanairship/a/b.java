package com.urbanairship.a;

import android.content.SharedPreferences;
import com.urbanairship.c;
import com.urbanairship.e;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static int f1138a = 1000;

    /* renamed from: b  reason: collision with root package name */
    private static int f1139b = 100;

    public static int a() {
        int i;
        synchronized (b.class) {
            int c = c();
            int d = d();
            i = b().getInt("count", c);
            if (i < d + c) {
                e.b("incrementing notification id count");
                a("count", i + 1);
            } else {
                e.b("resetting notification id count");
                a("count", c);
            }
            e.b("notification id: " + i);
        }
        return i;
    }

    private static void a(String str, int i) {
        SharedPreferences.Editor edit = b().edit();
        edit.putInt(str, i);
        edit.commit();
    }

    private static SharedPreferences b() {
        return c.a().g().getSharedPreferences("com.urbanairship.notificationidgenerator", 0);
    }

    private static int c() {
        int i;
        synchronized (b.class) {
            i = f1138a;
        }
        return i;
    }

    private static int d() {
        int i;
        synchronized (b.class) {
            i = f1139b;
        }
        return i;
    }
}
