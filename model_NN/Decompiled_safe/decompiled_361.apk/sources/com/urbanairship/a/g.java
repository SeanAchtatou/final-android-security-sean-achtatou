package com.urbanairship.a;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;

final class g extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ String f1144a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ d f1145b;

    g(d dVar, String str) {
        this.f1145b = dVar;
        this.f1144a = str;
    }

    public final void handleMessage(Message message) {
        if (((Drawable) message.obj) != null) {
            this.f1145b.f1141b.a((Drawable) message.obj);
        }
    }
}
