package com.urbanairship.a;

import android.os.AsyncTask;
import com.urbanairship.e;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public final class i extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private c f1148a;

    private static Exception a(File... fileArr) {
        if (fileArr.length != 2) {
            throw new IllegalArgumentException();
        }
        File file = fileArr[0];
        File file2 = fileArr[1];
        try {
            ZipFile zipFile = new ZipFile(file.getCanonicalPath());
            ArrayList list = Collections.list(zipFile.entries());
            e.b("Zip file: " + zipFile.getName() + " contains " + list.size() + " files");
            Iterator it = list.iterator();
            while (it.hasNext()) {
                ZipEntry zipEntry = (ZipEntry) it.next();
                if (zipEntry.getSize() != 0) {
                    String name = zipEntry.getName();
                    e.d("Unzipping file entry: " + name);
                    File canonicalFile = new File(file2, name).getCanonicalFile();
                    e.c("Creating file " + canonicalFile);
                    canonicalFile.getParentFile().mkdirs();
                    canonicalFile.createNewFile();
                    InputStream inputStream = zipFile.getInputStream(zipEntry);
                    FileOutputStream fileOutputStream = new FileOutputStream(canonicalFile);
                    byte[] bArr = new byte[4096];
                    while (true) {
                        int read = inputStream.read(bArr);
                        if (read <= 0) {
                            break;
                        }
                        fileOutputStream.write(bArr, 0, read);
                    }
                    fileOutputStream.flush();
                    inputStream.close();
                    fileOutputStream.close();
                }
            }
            file.delete();
            return null;
        } catch (Exception e) {
            file.delete();
            return e;
        }
    }

    public final void a(c cVar) {
        this.f1148a = cVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return a((File[]) objArr);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        if (((Exception) obj) != null) {
            this.f1148a.b();
        } else {
            this.f1148a.a();
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onProgressUpdate(Object[] objArr) {
        ((Integer[]) objArr)[0].intValue();
    }
}
