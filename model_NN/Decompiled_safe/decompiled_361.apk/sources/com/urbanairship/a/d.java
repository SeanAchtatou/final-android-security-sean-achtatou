package com.urbanairship.a;

import android.graphics.drawable.Drawable;
import java.util.HashMap;
import java.util.Map;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private static Map f1140a = new HashMap();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public j f1141b;

    public d(String str, j jVar) {
        this.f1141b = jVar;
        new h(this, str, new g(this, str)).start();
    }

    public static boolean a(String str) {
        boolean containsKey;
        synchronized (d.class) {
            containsKey = f1140a.containsKey(str);
        }
        return containsKey;
    }

    public static Drawable b(String str) {
        Drawable drawable;
        synchronized (d.class) {
            drawable = (Drawable) f1140a.get(str);
        }
        return drawable;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.drawable.Drawable d(java.lang.String r4) {
        /*
            r3 = 0
            java.lang.Class<com.urbanairship.a.d> r0 = com.urbanairship.a.d.class
            monitor-enter(r0)
            java.util.Map r1 = com.urbanairship.a.d.f1140a     // Catch:{ all -> 0x0049 }
            boolean r1 = r1.containsKey(r4)     // Catch:{ all -> 0x0049 }
            if (r1 == 0) goto L_0x0017
            java.util.Map r1 = com.urbanairship.a.d.f1140a     // Catch:{ all -> 0x0049 }
            java.lang.Object r4 = r1.get(r4)     // Catch:{ all -> 0x0049 }
            android.graphics.drawable.Drawable r4 = (android.graphics.drawable.Drawable) r4     // Catch:{ all -> 0x0049 }
            monitor-exit(r0)     // Catch:{ all -> 0x0049 }
            r0 = r4
        L_0x0016:
            return r0
        L_0x0017:
            monitor-exit(r0)     // Catch:{ all -> 0x0049 }
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ MalformedURLException -> 0x0041, IOException -> 0x004c, IllegalStateException -> 0x0054 }
            r0.<init>()     // Catch:{ MalformedURLException -> 0x0041, IOException -> 0x004c, IllegalStateException -> 0x0054 }
            org.apache.http.client.methods.HttpGet r1 = new org.apache.http.client.methods.HttpGet     // Catch:{ MalformedURLException -> 0x0041, IOException -> 0x004c, IllegalStateException -> 0x0054 }
            r1.<init>(r4)     // Catch:{ MalformedURLException -> 0x0041, IOException -> 0x004c, IllegalStateException -> 0x0054 }
            org.apache.http.HttpResponse r0 = r0.execute(r1)     // Catch:{ MalformedURLException -> 0x0041, IOException -> 0x004c, IllegalStateException -> 0x0054 }
            org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ MalformedURLException -> 0x0041, IOException -> 0x004c, IllegalStateException -> 0x0054 }
            java.io.InputStream r0 = r0.getContent()     // Catch:{ MalformedURLException -> 0x0041, IOException -> 0x004c, IllegalStateException -> 0x0054 }
            java.lang.String r1 = "Async Image"
            android.graphics.drawable.Drawable r0 = android.graphics.drawable.Drawable.createFromStream(r0, r1)     // Catch:{ MalformedURLException -> 0x0041, IOException -> 0x004c, IllegalStateException -> 0x0054 }
            java.lang.Class<com.urbanairship.a.d> r1 = com.urbanairship.a.d.class
            monitor-enter(r1)     // Catch:{ MalformedURLException -> 0x0041, IOException -> 0x004c, IllegalStateException -> 0x0054 }
            java.util.Map r2 = com.urbanairship.a.d.f1140a     // Catch:{ all -> 0x003e }
            r2.put(r4, r0)     // Catch:{ all -> 0x003e }
            monitor-exit(r1)     // Catch:{ all -> 0x003e }
            goto L_0x0016
        L_0x003e:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x003e }
            throw r0     // Catch:{ MalformedURLException -> 0x0041, IOException -> 0x004c, IllegalStateException -> 0x0054 }
        L_0x0041:
            r0 = move-exception
            java.lang.String r1 = "fetchDrawable failed"
            com.urbanairship.e.a(r1, r0)
            r0 = r3
            goto L_0x0016
        L_0x0049:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0049 }
            throw r1
        L_0x004c:
            r0 = move-exception
            java.lang.String r1 = "fetchDrawable failed"
            com.urbanairship.e.a(r1, r0)
            r0 = r3
            goto L_0x0016
        L_0x0054:
            r0 = move-exception
            java.lang.String r1 = "fetchDrawable failed"
            com.urbanairship.e.a(r1, r0)
            r0 = r3
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.urbanairship.a.d.d(java.lang.String):android.graphics.drawable.Drawable");
    }
}
