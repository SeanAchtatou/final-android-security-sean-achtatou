package com.urbanairship.a;

import android.content.Context;
import android.widget.Toast;
import com.urbanairship.c;
import com.urbanairship.e;

public final class a {
    public static void a(String str, int i) {
        Context g = c.a().g();
        if (g == null) {
            e.d("Toaster - applicationContext is null, bailing out");
        } else {
            Toast.makeText(g, str, i).show();
        }
    }
}
