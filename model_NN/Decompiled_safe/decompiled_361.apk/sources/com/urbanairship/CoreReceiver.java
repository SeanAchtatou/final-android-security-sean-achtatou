package com.urbanairship;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.urbanairship.push.f;

public class CoreReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals("android.intent.action.ACTION_SHUTDOWN")) {
            f.d();
        } else if (action.equals("android.intent.action.BOOT_COMPLETED")) {
            f.c();
        } else if ("com.urbanairship.analytics.START".equals(action)) {
            c.a().k().d();
        } else if (action.startsWith("com.urbanairship.push.NOTIFICATION_OPENED_PROXY")) {
            e.c("Received push conversion: " + intent.getStringExtra("com.urbanairship.push.PUSH_ID"));
            c.a().k().a(intent.getStringExtra("com.urbanairship.push.PUSH_ID"));
            Intent intent2 = new Intent("com.urbanairship.push.NOTIFICATION_OPENED");
            intent2.setClass(c.a().g(), f.b().g());
            intent2.putExtras(intent.getExtras());
            c.a().g().sendBroadcast(intent2);
        }
    }
}
