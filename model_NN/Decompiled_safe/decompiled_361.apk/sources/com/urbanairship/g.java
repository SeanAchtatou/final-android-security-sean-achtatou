package com.urbanairship;

import android.content.Context;
import android.content.res.AssetManager;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.ListIterator;
import java.util.Properties;

public abstract class g {
    public void a_(Context context) {
        String d = d();
        AssetManager assets = context.getResources().getAssets();
        try {
            if (!Arrays.asList(assets.list("")).contains(d)) {
                e.b("Options - Couldn't find " + d);
                return;
            }
            Properties properties = new Properties();
            try {
                properties.load(assets.open(d));
                ListIterator listIterator = Arrays.asList(getClass().getDeclaredFields()).listIterator();
                while (listIterator.hasNext()) {
                    Field field = (Field) listIterator.next();
                    String property = properties.getProperty(field.getName());
                    if (property != null) {
                        try {
                            if (field.getType() == Boolean.TYPE || field.getType() == Boolean.class) {
                                field.set(this, Boolean.valueOf(property));
                            } else {
                                try {
                                    field.set(this, property.trim());
                                } catch (IllegalArgumentException e) {
                                    e.e("Unable to set field '" + field.getName() + "' due to type mismatch.");
                                }
                            }
                        } catch (IllegalAccessException e2) {
                            e.e("Unable to set field '" + field.getName() + "' because the field is not visible.");
                        }
                    }
                }
            } catch (IOException e3) {
                e.a("Error loading properties file " + d, e3);
            }
        } catch (IOException e4) {
            e.a(e4);
        }
    }

    public abstract String d();
}
