package com.urbanairship;

import android.content.Context;
import com.urbanairship.c.b;
import com.urbanairship.push.b.m;

public final class f extends g {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1226a = false;

    /* renamed from: b  reason: collision with root package name */
    private boolean f1227b = false;

    public final void a_(Context context) {
        super.a_(context);
        if (this.f1226a) {
            e.d("InternalOptions - ignoring SSL Hostnames");
            b.f1213a = false;
        }
        if (this.f1227b) {
            e.d("InternalOptions - using test cluster");
            c.a().h().f1170a = "http://test.urbanairship.com";
            m.f1270a = "http://75.101.249.15:8090";
        }
    }

    public final String d() {
        return "internal.properties";
    }
}
