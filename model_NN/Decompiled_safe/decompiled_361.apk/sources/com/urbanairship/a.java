package com.urbanairship;

import android.content.SharedPreferences;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private String f1137a;

    public a(String str) {
        this.f1137a = str;
    }

    private synchronized SharedPreferences a() {
        return c.a().g().getSharedPreferences(this.f1137a, 4);
    }

    public final int a(String str, int i) {
        return a().getInt(str, i);
    }

    public final long a(String str, long j) {
        return a().getLong(str, j);
    }

    public final String a(String str) {
        return a().getString(str, null);
    }

    public final boolean a(String str, String str2) {
        return a().edit().putString(str, str2).commit();
    }

    public final boolean a(String str, boolean z) {
        return a().getBoolean(str, z);
    }

    public final boolean b(String str, int i) {
        return a().edit().putInt(str, i).commit();
    }

    public final boolean b(String str, long j) {
        return a().edit().putLong(str, j).commit();
    }

    public final boolean b(String str, boolean z) {
        return a().edit().putBoolean(str, z).commit();
    }
}
