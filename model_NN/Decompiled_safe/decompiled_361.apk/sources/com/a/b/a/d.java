package com.a.b.a;

import java.util.logging.Level;
import java.util.logging.Logger;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f129a;

    /* renamed from: b  reason: collision with root package name */
    private static Logger f130b = Logger.getLogger("javax.activation");
    private static final Level c = Level.FINE;

    static {
        f129a = false;
        try {
            f129a = Boolean.getBoolean("javax.activation.debug");
        } catch (Throwable th) {
        }
    }

    private d() {
    }

    public static void a(String str) {
        if (f129a) {
            System.out.println(str);
        }
        f130b.log(c, str);
    }

    public static void a(String str, Throwable th) {
        if (f129a) {
            System.out.println(String.valueOf(str) + "; Exception: " + th);
        }
        f130b.log(c, str, th);
    }

    public static boolean a() {
        return f129a || f130b.isLoggable(c);
    }
}
