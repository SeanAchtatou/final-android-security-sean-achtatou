package com.a.b.a;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private String f125a;

    /* renamed from: b  reason: collision with root package name */
    private int f126b = 0;
    private int c;
    private int d;
    private String e;
    private boolean f;
    private char g;

    public a(String str) {
        this.f125a = str;
        this.c = str.length();
        this.d = 1;
        this.e = "";
        this.f = false;
        this.g = ';';
    }

    public final void a(boolean z) {
        this.f = z;
    }

    public static String a(int i) {
        switch (i) {
            case 0:
                return "unknown";
            case 1:
                return "start";
            case 2:
                return "string";
            case 5:
                return "EOI";
            case 47:
                return "'/'";
            case 59:
                return "';'";
            case 61:
                return "'='";
            default:
                return "really unknown";
        }
    }

    public final String a() {
        return this.e;
    }

    public final int b() {
        if (this.f126b < this.c) {
            while (this.f126b < this.c && Character.isWhitespace(this.f125a.charAt(this.f126b))) {
                this.f126b++;
            }
            if (this.f126b < this.c) {
                char charAt = this.f125a.charAt(this.f126b);
                if (this.f) {
                    if (charAt == ';' || charAt == '=') {
                        this.d = charAt;
                        this.e = new Character(charAt).toString();
                        this.f126b++;
                    } else {
                        d();
                    }
                } else if (a(charAt)) {
                    c();
                } else if (charAt == '/' || charAt == ';' || charAt == '=') {
                    this.d = charAt;
                    this.e = new Character(charAt).toString();
                    this.f126b++;
                } else {
                    this.d = 0;
                    this.e = new Character(charAt).toString();
                    this.f126b++;
                }
            } else {
                this.d = 5;
                this.e = null;
            }
        } else {
            this.d = 5;
            this.e = null;
        }
        return this.d;
    }

    private void c() {
        int i = this.f126b;
        while (this.f126b < this.c && a(this.f125a.charAt(this.f126b))) {
            this.f126b++;
        }
        this.d = 2;
        this.e = this.f125a.substring(i, this.f126b);
    }

    private void d() {
        int i = this.f126b;
        boolean z = false;
        while (this.f126b < this.c && !z) {
            if (this.f125a.charAt(this.f126b) != this.g) {
                this.f126b++;
            } else {
                z = true;
            }
        }
        this.d = 2;
        String substring = this.f125a.substring(i, this.f126b);
        int length = substring.length();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.ensureCapacity(length);
        int i2 = 0;
        while (i2 < length) {
            char charAt = substring.charAt(i2);
            if (charAt != '\\') {
                stringBuffer.append(charAt);
            } else if (i2 < length - 1) {
                stringBuffer.append(substring.charAt(i2 + 1));
                i2++;
            } else {
                stringBuffer.append(charAt);
            }
            i2++;
        }
        this.e = stringBuffer.toString();
    }

    private static boolean a(char c2) {
        boolean z;
        switch (c2) {
            case '\"':
            case '(':
            case ')':
            case ',':
            case '/':
            case ':':
            case ';':
            case '<':
            case '=':
            case '>':
            case '?':
            case '@':
            case '[':
            case '\\':
            case ']':
                z = true;
                break;
            default:
                z = false;
                break;
        }
        return !z && !Character.isISOControl(c2) && !Character.isWhitespace(c2);
    }
}
