package com.a.b.a;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public final class b {
    private static boolean d;

    /* renamed from: a  reason: collision with root package name */
    private Map f127a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    private Map f128b = new HashMap();
    private Map c = new HashMap();

    static {
        d = false;
        try {
            d = Boolean.getBoolean("javax.activation.addreverse");
        } catch (Throwable th) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048 A[SYNTHETIC, Splitter:B:13:0x0048] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public b(java.lang.String r4) {
        /*
            r3 = this;
            r3.<init>()
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r3.f127a = r0
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r3.f128b = r0
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r3.c = r0
            boolean r0 = com.a.b.a.d.a()
            if (r0 == 0) goto L_0x0030
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "new MailcapFile: file "
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            com.a.b.a.d.a(r0)
        L_0x0030:
            r0 = 0
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ all -> 0x0042 }
            r1.<init>(r4)     // Catch:{ all -> 0x0042 }
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ all -> 0x0050 }
            r0.<init>(r1)     // Catch:{ all -> 0x0050 }
            r3.a(r0)     // Catch:{ all -> 0x0050 }
            r1.close()     // Catch:{ IOException -> 0x004e }
        L_0x0041:
            return
        L_0x0042:
            r1 = move-exception
            r2 = r1
            r1 = r0
            r0 = r2
        L_0x0046:
            if (r1 == 0) goto L_0x004b
            r1.close()     // Catch:{ IOException -> 0x004c }
        L_0x004b:
            throw r0
        L_0x004c:
            r1 = move-exception
            goto L_0x004b
        L_0x004e:
            r0 = move-exception
            goto L_0x0041
        L_0x0050:
            r0 = move-exception
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.b.a.b.<init>(java.lang.String):void");
    }

    public b(InputStream inputStream) {
        if (d.a()) {
            d.a("new MailcapFile: InputStream");
        }
        a(new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1")));
    }

    public b() {
        if (d.a()) {
            d.a("new MailcapFile: default");
        }
    }

    public final Map a(String str) {
        Map map = (Map) this.f127a.get(str);
        int indexOf = str.indexOf(47);
        if (str.substring(indexOf + 1).equals("*")) {
            return map;
        }
        Map map2 = (Map) this.f127a.get(String.valueOf(str.substring(0, indexOf + 1)) + "*");
        if (map2 == null) {
            return map;
        }
        if (map != null) {
            return a(map, map2);
        }
        return map2;
    }

    public final Map b(String str) {
        Map map = (Map) this.f128b.get(str);
        int indexOf = str.indexOf(47);
        if (str.substring(indexOf + 1).equals("*")) {
            return map;
        }
        Map map2 = (Map) this.f128b.get(String.valueOf(str.substring(0, indexOf + 1)) + "*");
        if (map2 == null) {
            return map;
        }
        if (map != null) {
            return a(map, map2);
        }
        return map2;
    }

    private static Map a(Map map, Map map2) {
        HashMap hashMap = new HashMap(map);
        for (String str : map2.keySet()) {
            List list = (List) hashMap.get(str);
            if (list == null) {
                hashMap.put(str, map2.get(str));
            } else {
                ArrayList arrayList = new ArrayList(list);
                arrayList.addAll((List) map2.get(str));
                hashMap.put(str, arrayList);
            }
        }
        return hashMap;
    }

    public final void c(String str) {
        if (d.a()) {
            d.a("appendToMailcap: " + str);
        }
        try {
            a(new StringReader(str));
        } catch (IOException e) {
        }
    }

    private void a(Reader reader) {
        BufferedReader bufferedReader = new BufferedReader(reader);
        String str = null;
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                String trim = readLine.trim();
                try {
                    if (trim.charAt(0) != '#') {
                        if (trim.charAt(trim.length() - 1) == '\\') {
                            if (str != null) {
                                str = String.valueOf(str) + trim.substring(0, trim.length() - 1);
                            } else {
                                str = trim.substring(0, trim.length() - 1);
                            }
                        } else if (str != null) {
                            try {
                                d(String.valueOf(str) + trim);
                            } catch (c e) {
                            }
                            str = null;
                        } else {
                            try {
                                d(trim);
                            } catch (c e2) {
                            }
                        }
                    }
                } catch (StringIndexOutOfBoundsException e3) {
                }
            } else {
                return;
            }
        }
    }

    private void d(String str) {
        int i;
        boolean z;
        int i2;
        a aVar = new a(str);
        aVar.a(false);
        if (d.a()) {
            d.a("parse: " + str);
        }
        int b2 = aVar.b();
        if (b2 != 2) {
            a(2, b2, aVar.a());
        }
        String lowerCase = aVar.a().toLowerCase(Locale.ENGLISH);
        String str2 = "*";
        int b3 = aVar.b();
        if (!(b3 == 47 || b3 == 59)) {
            b(47, b3, aVar.a());
        }
        if (b3 == 47) {
            int b4 = aVar.b();
            if (b4 != 2) {
                a(2, b4, aVar.a());
            }
            str2 = aVar.a().toLowerCase(Locale.ENGLISH);
            b3 = aVar.b();
        }
        String str3 = String.valueOf(lowerCase) + "/" + str2;
        if (d.a()) {
            d.a("  Type: " + str3);
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (b3 != 59) {
            a(59, b3, aVar.a());
        }
        aVar.a(true);
        int b5 = aVar.b();
        aVar.a(false);
        if (!(b5 == 2 || b5 == 59)) {
            b(2, b5, aVar.a());
        }
        if (b5 == 2) {
            List list = (List) this.c.get(str3);
            if (list == null) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(str);
                this.c.put(str3, arrayList);
            } else {
                list.add(str);
            }
        }
        if (b5 != 59) {
            i = aVar.b();
        } else {
            i = b5;
        }
        if (i == 59) {
            boolean z2 = false;
            while (true) {
                int b6 = aVar.b();
                if (b6 != 2) {
                    a(2, b6, aVar.a());
                }
                String lowerCase2 = aVar.a().toLowerCase(Locale.ENGLISH);
                int b7 = aVar.b();
                if (b7 == 61 || b7 == 59 || b7 == 5) {
                    if (b7 == 61) {
                        aVar.a(true);
                        int b8 = aVar.b();
                        aVar.a(false);
                        if (b8 != 2) {
                            a(2, b8, aVar.a());
                        }
                        String a2 = aVar.a();
                        if (lowerCase2.startsWith("x-java-")) {
                            String substring = lowerCase2.substring(7);
                            if (!substring.equals("fallback-entry") || !a2.equalsIgnoreCase("true")) {
                                if (d.a()) {
                                    d.a("    Command: " + substring + ", Class: " + a2);
                                }
                                Object obj = (List) linkedHashMap.get(substring);
                                if (obj == null) {
                                    obj = new ArrayList();
                                    linkedHashMap.put(substring, obj);
                                }
                                if (d) {
                                    obj.add(0, a2);
                                    z = z2;
                                    i2 = aVar.b();
                                } else {
                                    obj.add(a2);
                                }
                            } else {
                                z = true;
                                i2 = aVar.b();
                            }
                        }
                        z = z2;
                        i2 = aVar.b();
                    } else {
                        z = z2;
                        i2 = b7;
                    }
                    if (i2 != 59) {
                        Map map = z ? this.f128b : this.f127a;
                        Map map2 = (Map) map.get(str3);
                        if (map2 == null) {
                            map.put(str3, linkedHashMap);
                            return;
                        }
                        if (d.a()) {
                            d.a("Merging commands for type " + str3);
                        }
                        for (String str4 : map2.keySet()) {
                            List list2 = (List) map2.get(str4);
                            List<String> list3 = (List) linkedHashMap.get(str4);
                            if (list3 != null) {
                                for (String str5 : list3) {
                                    if (!list2.contains(str5)) {
                                        if (d) {
                                            list2.add(0, str5);
                                        } else {
                                            list2.add(str5);
                                        }
                                    }
                                }
                            }
                        }
                        for (String str6 : linkedHashMap.keySet()) {
                            if (!map2.containsKey(str6)) {
                                map2.put(str6, (List) linkedHashMap.get(str6));
                            }
                        }
                        return;
                    }
                    z2 = z;
                } else {
                    String a3 = aVar.a();
                    if (d.a()) {
                        d.a("PARSE ERROR: Encountered a " + a.a(b7) + " token (" + a3 + ") while expecting a " + a.a(61) + ", a " + a.a(59) + ", or a " + a.a(5) + " token.");
                    }
                    throw new c("Encountered a " + a.a(b7) + " token (" + a3 + ") while expecting a " + a.a(61) + ", a " + a.a(59) + ", or a " + a.a(5) + " token.");
                }
            }
        } else if (i != 5) {
            b(5, i, aVar.a());
        }
    }

    private static void a(int i, int i2, String str) {
        throw new c("Encountered a " + a.a(i2) + " token (" + str + ") while expecting a " + a.a(i) + " token.");
    }

    private static void b(int i, int i2, String str) {
        throw new c("Encountered a " + a.a(i2) + " token (" + str + ") while expecting a " + a.a(i) + " or a " + a.a(59) + " token.");
    }
}
