package com.a.a.a;

import java.io.OutputStream;

public final class c extends i {

    /* renamed from: b  reason: collision with root package name */
    private static String f112b = "=_?\"#$%&'(),.:;<>@[\\]^`{|}~";
    private static String c = "=_?";

    /* renamed from: a  reason: collision with root package name */
    private String f113a;

    public c(OutputStream outputStream, boolean z) {
        super(outputStream);
        this.f113a = z ? f112b : c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.a.i.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.a.a.a.c.a(byte[], boolean):int
      com.a.a.a.i.a(int, boolean):void */
    public final void write(int i) {
        int i2 = i & 255;
        if (i2 == 32) {
            a(95, false);
        } else if (i2 < 32 || i2 >= 127 || this.f113a.indexOf(i2) >= 0) {
            a(i2, true);
        } else {
            a(i2, false);
        }
    }

    public static int a(byte[] bArr, boolean z) {
        String str = z ? f112b : c;
        int i = 0;
        for (byte b2 : bArr) {
            byte b3 = b2 & 255;
            if (b3 < 32 || b3 >= Byte.MAX_VALUE || str.indexOf(b3) >= 0) {
                i += 3;
            } else {
                i++;
            }
        }
        return i;
    }
}
