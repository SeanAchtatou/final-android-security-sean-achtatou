package com.a.a.a;

import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class e extends FilterInputStream {
    private static final char[] h = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
    private static final byte[] i = new byte[256];

    /* renamed from: a  reason: collision with root package name */
    private byte[] f114a = new byte[3];

    /* renamed from: b  reason: collision with root package name */
    private int f115b = 0;
    private int c = 0;
    private byte[] d = new byte[8190];
    private int e = 0;
    private int f = 0;
    private boolean g = false;

    public e(InputStream inputStream) {
        super(inputStream);
        boolean z;
        try {
            String property = System.getProperty("mail.mime.base64.ignoreerrors");
            if (property == null || property.equalsIgnoreCase("false")) {
                z = false;
            } else {
                z = true;
            }
            this.g = z;
        } catch (SecurityException e2) {
        }
    }

    public final int read() {
        if (this.c >= this.f115b) {
            this.f115b = a(this.f114a, 0, this.f114a.length);
            if (this.f115b <= 0) {
                return -1;
            }
            this.c = 0;
        }
        byte[] bArr = this.f114a;
        int i2 = this.c;
        this.c = i2 + 1;
        return bArr[i2] & 255;
    }

    public final int read(byte[] bArr, int i2, int i3) {
        int i4 = i3;
        int i5 = i2;
        while (this.c < this.f115b && i4 > 0) {
            byte[] bArr2 = this.f114a;
            int i6 = this.c;
            this.c = i6 + 1;
            bArr[i5] = bArr2[i6];
            i4--;
            i5++;
        }
        if (this.c >= this.f115b) {
            this.c = 0;
            this.f115b = 0;
        }
        int i7 = (i4 / 3) * 3;
        if (i7 > 0) {
            int a2 = a(bArr, i5, i7);
            i5 += a2;
            i4 -= a2;
            if (a2 != i7) {
                if (i5 == i2) {
                    return -1;
                }
                return i5 - i2;
            }
        }
        while (i4 > 0) {
            int read = read();
            if (read == -1) {
                break;
            }
            bArr[i5] = (byte) read;
            i4--;
            i5++;
        }
        if (i5 == i2) {
            return -1;
        }
        return i5 - i2;
    }

    public final boolean markSupported() {
        return false;
    }

    public final int available() {
        return ((this.in.available() * 3) / 4) + (this.f115b - this.c);
    }

    static {
        for (int i2 = 0; i2 < 255; i2++) {
            i[i2] = -1;
        }
        for (int i3 = 0; i3 < h.length; i3++) {
            i[h[i3]] = (byte) i3;
        }
    }

    private int a(byte[] bArr, int i2, int i3) {
        boolean z;
        int i4 = i3;
        int i5 = i2;
        while (i4 >= 3) {
            int i6 = 0;
            int i7 = 0;
            while (i7 < 4) {
                int a2 = a();
                if (a2 == -1 || a2 == -2) {
                    if (a2 == -1) {
                        if (i7 == 0) {
                            return i5 - i2;
                        }
                        if (!this.g) {
                            throw new IOException("Error in encoded stream: needed 4 valid base64 characters but only got " + i7 + " before EOF" + b());
                        }
                        z = true;
                    } else if (i7 < 2 && !this.g) {
                        throw new IOException("Error in encoded stream: needed at least 2 valid base64 characters, but only got " + i7 + " before padding character (=)" + b());
                    } else if (i7 == 0) {
                        return i5 - i2;
                    } else {
                        z = false;
                    }
                    int i8 = i7 - 1;
                    if (i8 == 0) {
                        i8 = 1;
                    }
                    int i9 = i6 << 6;
                    for (int i10 = i7 + 1; i10 < 4; i10++) {
                        if (!z) {
                            int a3 = a();
                            if (a3 == -1) {
                                if (!this.g) {
                                    throw new IOException("Error in encoded stream: hit EOF while looking for padding characters (=)" + b());
                                }
                            } else if (a3 != -2 && !this.g) {
                                throw new IOException("Error in encoded stream: found valid base64 character after a padding character (=)" + b());
                            }
                        }
                        i9 <<= 6;
                    }
                    int i11 = i9 >> 8;
                    if (i8 == 2) {
                        bArr[i5 + 1] = (byte) (i11 & 255);
                    }
                    bArr[i5] = (byte) ((i11 >> 8) & 255);
                    return (i5 + i8) - i2;
                }
                i7++;
                i6 = (i6 << 6) | a2;
            }
            bArr[i5 + 2] = (byte) (i6 & 255);
            int i12 = i6 >> 8;
            bArr[i5 + 1] = (byte) (i12 & 255);
            bArr[i5] = (byte) ((i12 >> 8) & 255);
            i4 -= 3;
            i5 += 3;
        }
        return i5 - i2;
    }

    private int a() {
        byte b2;
        do {
            if (this.e >= this.f) {
                try {
                    this.f = this.in.read(this.d);
                    if (this.f <= 0) {
                        return -1;
                    }
                    this.e = 0;
                } catch (EOFException e2) {
                    return -1;
                }
            }
            byte[] bArr = this.d;
            int i2 = this.e;
            this.e = i2 + 1;
            byte b3 = bArr[i2] & 255;
            if (b3 == 61) {
                return -2;
            }
            b2 = i[b3];
        } while (b2 == -1);
        return b2;
    }

    private String b() {
        int i2 = this.e > 10 ? 10 : this.e;
        if (i2 <= 0) {
            return "";
        }
        int i3 = this.e - i2;
        String str = String.valueOf("") + ", the " + i2 + " most recent characters were: \"";
        for (int i4 = i3; i4 < this.e; i4++) {
            char c2 = (char) (this.d[i4] & 255);
            switch (c2) {
                case 9:
                    str = String.valueOf(str) + "\\t";
                    break;
                case 10:
                    str = String.valueOf(str) + "\\n";
                    break;
                case 11:
                case 12:
                default:
                    if (c2 >= ' ' && c2 < 127) {
                        str = String.valueOf(str) + c2;
                        break;
                    } else {
                        str = String.valueOf(str) + "\\" + ((int) c2);
                        break;
                    }
                    break;
                case 13:
                    str = String.valueOf(str) + "\\r";
                    break;
            }
        }
        return String.valueOf(str) + "\"";
    }
}
