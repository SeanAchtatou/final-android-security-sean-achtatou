package com.a.a.a;

import java.io.FilterInputStream;
import java.io.InputStream;
import java.io.PushbackInputStream;

public class j extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    protected byte[] f123a = new byte[2];

    /* renamed from: b  reason: collision with root package name */
    private int f124b = 0;

    public j(InputStream inputStream) {
        super(new PushbackInputStream(inputStream, 2));
    }

    public int read() {
        int read;
        if (this.f124b > 0) {
            this.f124b--;
            return 32;
        }
        int read2 = this.in.read();
        if (read2 == 32) {
            while (true) {
                read = this.in.read();
                if (read != 32) {
                    break;
                }
                this.f124b++;
            }
            if (read == 13 || read == 10 || read == -1) {
                this.f124b = 0;
                return read;
            }
            ((PushbackInputStream) this.in).unread(read);
            return 32;
        } else if (read2 != 61) {
            return read2;
        } else {
            int read3 = this.in.read();
            if (read3 == 10) {
                return read();
            }
            if (read3 == 13) {
                int read4 = this.in.read();
                if (read4 != 10) {
                    ((PushbackInputStream) this.in).unread(read4);
                }
                return read();
            } else if (read3 == -1) {
                return -1;
            } else {
                this.f123a[0] = (byte) read3;
                this.f123a[1] = (byte) this.in.read();
                try {
                    return a.a(this.f123a);
                } catch (NumberFormatException e) {
                    ((PushbackInputStream) this.in).unread(this.f123a);
                    return read2;
                }
            }
        }
    }

    public int read(byte[] bArr, int i, int i2) {
        int i3 = 0;
        while (i3 < i2) {
            int read = read();
            if (read != -1) {
                bArr[i + i3] = (byte) read;
                i3++;
            } else if (i3 == 0) {
                return -1;
            } else {
                return i3;
            }
        }
        return i3;
    }

    public boolean markSupported() {
        return false;
    }

    public int available() {
        return this.in.available();
    }
}
