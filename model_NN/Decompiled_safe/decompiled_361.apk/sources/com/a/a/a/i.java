package com.a.a.a;

import java.io.FilterOutputStream;
import java.io.OutputStream;

public class i extends FilterOutputStream {
    private static final char[] e = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /* renamed from: a  reason: collision with root package name */
    private int f121a = 0;

    /* renamed from: b  reason: collision with root package name */
    private int f122b = 2147483646;
    private boolean c = false;
    private boolean d = false;

    public i(OutputStream outputStream) {
        super(outputStream);
    }

    public void write(byte[] bArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            write(bArr[i + i3]);
        }
    }

    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public void write(int i) {
        int i2 = i & 255;
        if (this.c) {
            if (i2 == 13 || i2 == 10) {
                a(32, true);
            } else {
                a(32, false);
            }
            this.c = false;
        }
        if (i2 == 13) {
            this.d = true;
            a();
            return;
        }
        if (i2 == 10) {
            if (!this.d) {
                a();
            }
        } else if (i2 == 32) {
            this.c = true;
        } else if (i2 < 32 || i2 >= 127 || i2 == 61) {
            a(i2, true);
        } else {
            a(i2, false);
        }
        this.d = false;
    }

    public void flush() {
        this.out.flush();
    }

    public void close() {
        this.out.close();
    }

    private void a() {
        this.out.write(13);
        this.out.write(10);
        this.f121a = 0;
    }

    /* access modifiers changed from: protected */
    public final void a(int i, boolean z) {
        if (z) {
            int i2 = this.f121a + 3;
            this.f121a = i2;
            if (i2 > this.f122b) {
                this.out.write(61);
                this.out.write(13);
                this.out.write(10);
                this.f121a = 3;
            }
            this.out.write(61);
            this.out.write(e[i >> 4]);
            this.out.write(e[i & 15]);
            return;
        }
        int i3 = this.f121a + 1;
        this.f121a = i3;
        if (i3 > this.f122b) {
            this.out.write(61);
            this.out.write(13);
            this.out.write(10);
            this.f121a = 1;
        }
        this.out.write(i);
    }
}
