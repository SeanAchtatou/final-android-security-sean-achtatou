package com.a.a.a;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class f extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    private String f116a;

    /* renamed from: b  reason: collision with root package name */
    private int f117b;
    private byte[] c;
    private int d = 0;
    private int e = 0;
    private boolean f = false;
    private boolean g = false;
    private g h;

    public f(InputStream inputStream) {
        super(inputStream);
        this.h = new g(inputStream);
        this.c = new byte[45];
    }

    public final int read() {
        String a2;
        if (this.e >= this.d) {
            if (!this.f) {
                do {
                    a2 = this.h.a();
                    if (a2 == null) {
                        throw new IOException("UUDecoder error: No Begin");
                    }
                } while (!a2.regionMatches(true, 0, "begin", 0, 5));
                try {
                    this.f117b = Integer.parseInt(a2.substring(6, 9));
                    this.f116a = a2.substring(10);
                    this.f = true;
                } catch (NumberFormatException e2) {
                    throw new IOException("UUDecoder error: " + e2.toString());
                }
            }
            if (!a()) {
                return -1;
            }
            this.e = 0;
        }
        byte[] bArr = this.c;
        int i = this.e;
        this.e = i + 1;
        return bArr[i] & 255;
    }

    public final int read(byte[] bArr, int i, int i2) {
        int i3 = 0;
        while (i3 < i2) {
            int read = read();
            if (read != -1) {
                bArr[i + i3] = (byte) read;
                i3++;
            } else if (i3 == 0) {
                return -1;
            } else {
                return i3;
            }
        }
        return i3;
    }

    public final boolean markSupported() {
        return false;
    }

    public final int available() {
        return ((this.in.available() * 3) / 4) + (this.d - this.e);
    }

    private boolean a() {
        String a2;
        byte b2;
        int i;
        if (this.g) {
            return false;
        }
        this.d = 0;
        do {
            a2 = this.h.a();
            if (a2 == null) {
                throw new IOException("Missing End");
            } else if (a2.regionMatches(true, 0, "end", 0, 3)) {
                this.g = true;
                return false;
            }
        } while (a2.length() == 0);
        char charAt = a2.charAt(0);
        if (charAt < ' ') {
            throw new IOException("Buffer format error");
        }
        int i2 = (charAt - ' ') & 63;
        if (i2 == 0) {
            String a3 = this.h.a();
            if (a3 == null || !a3.regionMatches(true, 0, "end", 0, 3)) {
                throw new IOException("Missing End");
            }
            this.g = true;
            return false;
        } else if (a2.length() < (((i2 * 8) + 5) / 6) + 1) {
            throw new IOException("Short buffer error");
        } else {
            int i3 = 1;
            while (this.d < i2) {
                int i4 = i3 + 1;
                int i5 = i4 + 1;
                byte charAt2 = (byte) ((a2.charAt(i4) - ' ') & 63);
                byte[] bArr = this.c;
                int i6 = this.d;
                this.d = i6 + 1;
                bArr[i6] = (byte) (((((byte) ((a2.charAt(i3) - ' ') & 63)) << 2) & 252) | ((charAt2 >>> 4) & 3));
                if (this.d < i2) {
                    int i7 = i5 + 1;
                    byte charAt3 = (byte) ((a2.charAt(i5) - ' ') & 63);
                    byte[] bArr2 = this.c;
                    int i8 = this.d;
                    this.d = i8 + 1;
                    bArr2[i8] = (byte) (((charAt2 << 4) & 240) | ((charAt3 >>> 2) & 15));
                    i = i7;
                    b2 = charAt3;
                } else {
                    b2 = charAt2;
                    i = i5;
                }
                if (this.d < i2) {
                    byte[] bArr3 = this.c;
                    int i9 = this.d;
                    this.d = i9 + 1;
                    bArr3[i9] = (byte) (((b2 << 6) & 192) | (((byte) ((a2.charAt(i) - ' ') & 63)) & 63));
                    i3 = i + 1;
                } else {
                    i3 = i;
                }
            }
            return true;
        }
    }
}
