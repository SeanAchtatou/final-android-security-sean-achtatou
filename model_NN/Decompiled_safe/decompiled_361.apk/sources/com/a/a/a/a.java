package com.a.a.a;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public final class a {
    private a() {
    }

    public static int a(byte[] bArr) {
        int i;
        boolean z;
        if (bArr == null) {
            throw new NumberFormatException("null");
        }
        if (bArr[0] == 45) {
            i = 0 + 1;
            z = true;
        } else {
            i = 0;
            z = false;
        }
        int i2 = i + 1;
        int digit = Character.digit((char) bArr[i], 16);
        if (digit < 0) {
            throw new NumberFormatException("illegal number: " + b(bArr));
        }
        int i3 = i2;
        int i4 = -digit;
        int i5 = i3;
        while (i5 < 2) {
            i5++;
            int digit2 = Character.digit((char) bArr[1], 16);
            if (digit2 < 0) {
                throw new NumberFormatException("illegal number");
            } else if (i4 < -134217727) {
                throw new NumberFormatException("illegal number");
            } else {
                int i6 = i4 * 16;
                if (i6 < -2147483647 + digit2) {
                    throw new NumberFormatException("illegal number");
                }
                i4 = i6 - digit2;
            }
        }
        if (z) {
            return i4;
        }
        return -i4;
    }

    private static String b(byte[] bArr) {
        int i = 0;
        char[] cArr = new char[2];
        int i2 = 0;
        while (i < 2) {
            cArr[i] = (char) (bArr[i2] & 255);
            i++;
            i2++;
        }
        return new String(cArr);
    }

    public static byte[] a(String str) {
        char[] charArray = str.toCharArray();
        int length = charArray.length;
        byte[] bArr = new byte[length];
        for (int i = 0; i < length; i++) {
            bArr[i] = (byte) charArray[i];
        }
        return bArr;
    }

    public static byte[] a(InputStream inputStream) {
        if (inputStream instanceof ByteArrayInputStream) {
            int available = inputStream.available();
            byte[] bArr = new byte[available];
            inputStream.read(bArr, 0, available);
            return bArr;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr2 = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr2, 0, 1024);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr2, 0, read);
        }
    }
}
