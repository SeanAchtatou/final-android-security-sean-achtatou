package com.a.a.a;

import java.io.FilterOutputStream;
import java.io.OutputStream;

public class h extends FilterOutputStream {
    private static byte[] h = {13, 10};
    private static final char[] i = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

    /* renamed from: a  reason: collision with root package name */
    private byte[] f119a;

    /* renamed from: b  reason: collision with root package name */
    private int f120b;
    private byte[] c;
    private int d;
    private int e;
    private int f;
    private boolean g;

    public h(OutputStream outputStream) {
        super(outputStream);
        this.f120b = 0;
        this.d = 0;
        this.g = false;
        this.f119a = new byte[3];
        this.g = true;
        this.e = 76;
        this.f = 57;
        if (this.g) {
            this.c = new byte[76];
            return;
        }
        this.c = new byte[78];
        this.c[76] = 13;
        this.c[77] = 10;
    }

    public synchronized void write(byte[] bArr, int i2, int i3) {
        int i4 = i2 + i3;
        int i5 = i2;
        while (this.f120b != 0 && i5 < i4) {
            int i6 = i5 + 1;
            write(bArr[i5]);
            i5 = i6;
        }
        int i7 = ((this.e - this.d) / 4) * 3;
        if (i5 + i7 < i4) {
            int i8 = ((i7 + 2) / 3) * 4;
            if (!this.g) {
                int i9 = i8 + 1;
                this.c[i8] = 13;
                this.c[i9] = 10;
                i8 = i9 + 1;
            }
            this.out.write(a(bArr, i5, i7, this.c), 0, i8);
            i5 += i7;
            this.d = 0;
        }
        while (this.f + i5 < i4) {
            this.out.write(a(bArr, i5, this.f, this.c));
            i5 += this.f;
        }
        if (i5 + 3 < i4) {
            int i10 = ((i4 - i5) / 3) * 3;
            int i11 = ((i10 + 2) / 3) * 4;
            this.out.write(a(bArr, i5, i10, this.c), 0, i11);
            i5 += i10;
            this.d += i11;
        }
        while (i5 < i4) {
            write(bArr[i5]);
            i5++;
        }
    }

    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public synchronized void write(int i2) {
        byte[] bArr = this.f119a;
        int i3 = this.f120b;
        this.f120b = i3 + 1;
        bArr[i3] = (byte) i2;
        if (this.f120b == 3) {
            a();
            this.f120b = 0;
        }
    }

    public synchronized void flush() {
        if (this.f120b > 0) {
            a();
            this.f120b = 0;
        }
        this.out.flush();
    }

    public synchronized void close() {
        flush();
        if (this.d > 0 && !this.g) {
            this.out.write(h);
            this.out.flush();
        }
        this.out.close();
    }

    private void a() {
        int i2 = ((this.f120b + 2) / 3) * 4;
        this.out.write(a(this.f119a, 0, this.f120b, this.c), 0, i2);
        this.d = i2 + this.d;
        if (this.d >= this.e) {
            if (!this.g) {
                this.out.write(h);
            }
            this.d = 0;
        }
    }

    private static byte[] a(byte[] bArr, int i2, int i3, byte[] bArr2) {
        byte[] bArr3;
        if (bArr2 == null) {
            bArr3 = new byte[(((i3 + 2) / 3) * 4)];
        } else {
            bArr3 = bArr2;
        }
        int i4 = 0;
        int i5 = i3;
        int i6 = i2;
        while (i5 >= 3) {
            int i7 = i6 + 1;
            int i8 = i7 + 1;
            byte b2 = ((((bArr[i6] & 255) << 8) | (bArr[i7] & 255)) << 8) | (bArr[i8] & 255);
            bArr3[i4 + 3] = (byte) i[b2 & 63];
            int i9 = b2 >> 6;
            bArr3[i4 + 2] = (byte) i[i9 & 63];
            int i10 = i9 >> 6;
            bArr3[i4 + 1] = (byte) i[i10 & 63];
            bArr3[i4 + 0] = (byte) i[(i10 >> 6) & 63];
            i5 -= 3;
            i4 += 4;
            i6 = i8 + 1;
        }
        if (i5 == 1) {
            int i11 = (bArr[i6] & 255) << 4;
            bArr3[i4 + 3] = 61;
            bArr3[i4 + 2] = 61;
            bArr3[i4 + 1] = (byte) i[i11 & 63];
            bArr3[i4 + 0] = (byte) i[(i11 >> 6) & 63];
        } else if (i5 == 2) {
            int i12 = ((bArr[i6 + 1] & 255) | ((bArr[i6] & 255) << 8)) << 2;
            bArr3[i4 + 3] = 61;
            bArr3[i4 + 2] = (byte) i[i12 & 63];
            int i13 = i12 >> 6;
            bArr3[i4 + 1] = (byte) i[i13 & 63];
            bArr3[i4 + 0] = (byte) i[(i13 >> 6) & 63];
        }
        return bArr3;
    }
}
