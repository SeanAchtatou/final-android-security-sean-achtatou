package com.a.a.a;

import java.io.FilterInputStream;
import java.io.InputStream;
import java.io.PushbackInputStream;

public final class g extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    private char[] f118a = null;

    public g(InputStream inputStream) {
        super(inputStream);
    }

    public final String a() {
        int read;
        PushbackInputStream pushbackInputStream;
        InputStream inputStream = this.in;
        char[] cArr = this.f118a;
        if (cArr == null) {
            cArr = new char[128];
            this.f118a = cArr;
        }
        int length = cArr.length;
        char[] cArr2 = cArr;
        int i = 0;
        while (true) {
            read = inputStream.read();
            if (read == -1 || read == 10) {
                break;
            } else if (read == 13) {
                int read2 = inputStream.read();
                if (read2 == 13) {
                    read2 = inputStream.read();
                }
                if (read2 != 10) {
                    if (!(inputStream instanceof PushbackInputStream)) {
                        PushbackInputStream pushbackInputStream2 = new PushbackInputStream(inputStream);
                        this.in = pushbackInputStream2;
                        pushbackInputStream = pushbackInputStream2;
                    } else {
                        pushbackInputStream = inputStream;
                    }
                    ((PushbackInputStream) pushbackInputStream).unread(read2);
                }
            } else {
                length--;
                if (length < 0) {
                    char[] cArr3 = new char[(i + 128)];
                    System.arraycopy(this.f118a, 0, cArr3, 0, i);
                    this.f118a = cArr3;
                    cArr2 = cArr3;
                    length = (cArr3.length - i) - 1;
                }
                cArr2[i] = (char) read;
                i++;
            }
        }
        if (read == -1 && i == 0) {
            return null;
        }
        return String.copyValueOf(cArr2, 0, i);
    }
}
