package com.a.a.a;

import java.io.IOException;
import java.io.InputStream;

public final class d extends j {
    public d(InputStream inputStream) {
        super(inputStream);
    }

    public final int read() {
        int read = this.in.read();
        if (read == 95) {
            return 32;
        }
        if (read != 61) {
            return read;
        }
        this.f123a[0] = (byte) this.in.read();
        this.f123a[1] = (byte) this.in.read();
        try {
            return a.a(this.f123a);
        } catch (NumberFormatException e) {
            throw new IOException("Error in QP stream " + e.getMessage());
        }
    }
}
