package com.cyberandsons.tcmaidtrial.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.urbanairship.c;

public class IntentReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Log.i("PushSample", "Received intent: " + intent.toString());
        String action = intent.getAction();
        if (action.equals("com.urbanairship.push.PUSH_RECEIVED")) {
            Log.i("PushSample", "Received push notification. Alert: " + intent.getStringExtra("com.urbanairship.push.ALERT") + ". Payload: " + intent.getStringExtra("com.urbanairship.push.STRING_EXTRA") + ". NotificationID=" + intent.getIntExtra("com.urbanairship.push.NOTIFICATION_ID", 0));
        } else if (action.equals("com.urbanairship.push.NOTIFICATION_OPENED")) {
            Log.i("PushSample", "User clicked notification. Message: " + intent.getStringExtra("com.urbanairship.push.ALERT") + ". Payload: " + intent.getStringExtra("com.urbanairship.push.STRING_EXTRA"));
            Intent intent2 = new Intent("android.intent.action.MAIN");
            intent2.setFlags(268435456);
            c.a().g().startActivity(intent2);
        } else if (action.equals("com.urbanairship.push.REGISTRATION_FINISHED")) {
            Log.i("PushSample", "Registration complete. APID:" + intent.getStringExtra("com.urbanairship.push.APID") + ". Valid: " + intent.getBooleanExtra("com.urbanairship.push.REGISTRATION_VALID", false));
        }
    }
}
