package com.cyberandsons.tcmaidtrial.push;

import android.view.View;
import android.widget.CheckBox;

final class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PushPreferencesActivity f1032a;

    b(PushPreferencesActivity pushPreferencesActivity) {
        this.f1032a = pushPreferencesActivity;
    }

    public final void onClick(View view) {
        this.f1032a.b(((CheckBox) view).isChecked());
    }
}
