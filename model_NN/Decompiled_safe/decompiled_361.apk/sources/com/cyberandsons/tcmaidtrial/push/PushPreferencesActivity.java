package com.cyberandsons.tcmaidtrial.push;

import android.content.res.Configuration;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.CheckBox;
import android.widget.TimePicker;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.urbanairship.analytics.InstrumentedActivity;
import com.urbanairship.push.d;
import com.urbanairship.push.f;
import java.util.Calendar;
import java.util.Date;

public class PushPreferencesActivity extends InstrumentedActivity {

    /* renamed from: a  reason: collision with root package name */
    private CheckBox f1029a;

    /* renamed from: b  reason: collision with root package name */
    private CheckBox f1030b;
    private CheckBox c;
    private CheckBox d;
    private TimePicker e;
    private TimePicker f;
    private d g = f.b().h();

    /* access modifiers changed from: private */
    public void a(boolean z) {
        this.f1030b.setEnabled(z);
        this.c.setEnabled(z);
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        this.e.setEnabled(z);
        this.f.setEnabled(z);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().requestFeature(3);
        setContentView((int) C0000R.layout.push_preferences_dialog);
        this.f1029a = (CheckBox) findViewById(C0000R.id.push_enabled);
        this.f1030b = (CheckBox) findViewById(C0000R.id.sound_enabled);
        this.c = (CheckBox) findViewById(C0000R.id.vibrate_enabled);
        this.d = (CheckBox) findViewById(C0000R.id.quiet_time_enabled);
        this.e = (TimePicker) findViewById(C0000R.id.start_time);
        this.f = (TimePicker) findViewById(C0000R.id.end_time);
        this.e.setIs24HourView(Boolean.valueOf(DateFormat.is24HourFormat(this)));
        this.f.setIs24HourView(Boolean.valueOf(DateFormat.is24HourFormat(this)));
        this.f1029a.setOnClickListener(new a(this));
        this.d.setOnClickListener(new b(this));
    }

    public void onStart() {
        super.onStart();
        boolean a2 = this.g.a();
        this.f1029a.setChecked(a2);
        this.f1030b.setChecked(this.g.b());
        this.c.setChecked(this.g.c());
        a(a2);
        boolean e2 = this.g.e();
        this.d.setChecked(e2);
        b(e2);
        Date[] g2 = this.g.g();
        if (g2 != null) {
            this.e.setCurrentHour(Integer.valueOf(g2[0].getHours()));
            this.e.setCurrentMinute(Integer.valueOf(g2[0].getMinutes()));
            this.f.setCurrentHour(Integer.valueOf(g2[1].getHours()));
            this.f.setCurrentMinute(Integer.valueOf(g2[1].getMinutes()));
        }
    }

    public void onStop() {
        super.onStop();
        boolean isChecked = this.f1029a.isChecked();
        boolean isChecked2 = this.d.isChecked();
        if (isChecked) {
            f.e();
        } else {
            f.f();
        }
        this.g.b(this.f1030b.isChecked());
        this.g.c(this.c.isChecked());
        this.g.d(isChecked2);
        if (isChecked2) {
            Calendar instance = Calendar.getInstance();
            instance.setTime(new Date());
            instance.set(11, this.e.getCurrentHour().intValue());
            instance.set(12, this.e.getCurrentMinute().intValue());
            Date time = instance.getTime();
            Calendar instance2 = Calendar.getInstance();
            instance2.setTime(new Date());
            instance2.set(11, this.f.getCurrentHour().intValue());
            instance2.set(12, this.f.getCurrentMinute().intValue());
            this.g.a(time, instance2.getTime());
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }
}
