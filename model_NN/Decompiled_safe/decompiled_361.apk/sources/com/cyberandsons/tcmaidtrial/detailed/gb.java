package com.cyberandsons.tcmaidtrial.detailed;

import android.view.MotionEvent;
import android.view.View;

final class gb implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SixStagesDetail f609a;

    gb(SixStagesDetail sixStagesDetail) {
        this.f609a = sixStagesDetail;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f609a.E.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f609a.c;
    }
}
