package com.cyberandsons.tcmaidtrial.detailed;

import android.view.MotionEvent;
import android.view.View;

final class fz implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SixStagesDetail f606a;

    fz(SixStagesDetail sixStagesDetail) {
        this.f606a = sixStagesDetail;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f606a.C.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f606a.c;
    }
}
