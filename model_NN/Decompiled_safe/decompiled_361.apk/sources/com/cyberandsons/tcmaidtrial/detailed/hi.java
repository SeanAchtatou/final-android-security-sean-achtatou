package com.cyberandsons.tcmaidtrial.detailed;

import android.view.MotionEvent;
import android.view.View;

final class hi implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiagnosisDetail f643a;

    hi(DiagnosisDetail diagnosisDetail) {
        this.f643a = diagnosisDetail;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f643a.G.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f643a.c;
    }
}
