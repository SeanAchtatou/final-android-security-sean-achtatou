package com.cyberandsons.tcmaidtrial.detailed;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.FloatMath;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.draw.a;
import com.cyberandsons.tcmaidtrial.draw.e;
import com.cyberandsons.tcmaidtrial.misc.ad;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class WristAnkleDetail extends Activity implements View.OnClickListener, View.OnTouchListener {
    private String A;
    private boolean B;
    private SQLiteDatabase C;
    private n D;
    private boolean E;
    /* access modifiers changed from: private */
    public GestureDetector F;
    private View.OnTouchListener G;
    private Matrix H;
    private Matrix I;
    private int J;
    private PointF K;
    private PointF L;
    private float M;

    /* renamed from: a  reason: collision with root package name */
    ScrollView f441a;

    /* renamed from: b  reason: collision with root package name */
    EditText f442b;
    boolean c;
    private TextView d;
    private ImageView e;
    private EditText f;
    private EditText g;
    private ImageButton h;
    private ImageButton i;
    private ImageButton j;
    private ImageButton k;
    private ImageButton l;
    private ImageButton m;
    private ImageButton n;
    private ImageButton o;
    private Button p;
    private boolean q = true;
    private boolean r;
    private boolean s;
    private boolean t;
    private int u;
    private int v;
    private boolean w;
    private boolean x;
    private boolean y;
    private int z;

    public WristAnkleDetail() {
        this.r = !this.q;
        this.s = this.r;
        this.t = this.r;
        this.w = this.r;
        this.x = this.r;
        this.y = this.r;
        this.z = 0;
        this.c = this.q;
        this.B = this.r;
        this.E = this.r;
        this.H = new Matrix();
        this.I = new Matrix();
        this.J = 0;
        this.K = new PointF();
        this.L = new PointF();
        this.M = 1.0f;
    }

    static /* synthetic */ void a(WristAnkleDetail wristAnkleDetail) {
        int i2;
        if (wristAnkleDetail.x) {
            wristAnkleDetail.z++;
            if (wristAnkleDetail.z == 4) {
                wristAnkleDetail.z = 0;
            }
            switch (wristAnkleDetail.z) {
                case 1:
                    i2 = C0000R.drawable.wristanklefront;
                    break;
                case 2:
                    i2 = C0000R.drawable.wristankleback;
                    break;
                case 3:
                    i2 = C0000R.drawable.wristankleside;
                    break;
                default:
                    i2 = C0000R.drawable.wristanklefront;
                    break;
            }
            if (wristAnkleDetail.z > 0) {
                Bitmap decodeResource = BitmapFactory.decodeResource(wristAnkleDetail.getResources(), i2);
                wristAnkleDetail.e.setImageBitmap(new a(wristAnkleDetail, decodeResource, decodeResource.getHeight(), decodeResource.getWidth()).a());
                return;
            }
            wristAnkleDetail.a(wristAnkleDetail.r);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.C == null || !this.C.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("WAD:openDataBase()", str + " opened.");
                this.C = SQLiteDatabase.openDatabase(str, null, 16);
                this.C.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.C.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("WAD:openDataBase()", str2 + " ATTACHed.");
                this.D = new n();
                this.D.a(this.C);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new gc(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        this.x = this.D.A();
        this.E = this.D.ad();
        setContentView((int) C0000R.layout.wristankle_detailed_edit);
        this.d = (TextView) findViewById(C0000R.id.tce_label);
        this.f441a = (ScrollView) findViewById(C0000R.id.svDetail);
        getWindow().setSoftInputMode(3);
        this.F = new GestureDetector(new dy(this));
        this.G = new ga(this);
        this.d.setOnClickListener(this);
        this.d.setOnTouchListener(this.G);
        a(true);
        this.f441a.smoothScrollBy(0, 0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 2, 0, "Add").setIcon(17301559);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 2:
                b();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void onDestroy() {
        try {
            if (this.C != null && this.C.isOpen()) {
                this.C.close();
                this.C = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
        if (i3 == -1) {
            Uri data = intent.getData();
            Log.i("onActivityResult", data.toString());
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(getContentResolver().openInputStream(data));
                if (decodeStream != null) {
                    BitmapDrawable a2 = dh.a(decodeStream, TcmAid.cS - 0, TcmAid.cS - 0, getWindow());
                    this.e.setImageDrawable(a2);
                    this.e.setVisibility(0);
                    this.y = this.q;
                    this.i.setImageResource(17301560);
                    this.i.setVisibility(0);
                    String format = String.format("%s/%s_alt.png", getString(C0000R.string.image_path), this.A);
                    a(format, this.q);
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(format);
                        a2.getBitmap().compress(Bitmap.CompressFormat.PNG, 90, fileOutputStream);
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    } catch (Exception e2) {
                        Log.e("WAD:onActivityResult", "Failed to write alternate image: " + e2.getLocalizedMessage());
                    }
                }
            } catch (FileNotFoundException e3) {
                Log.e("WAD:onActivityResult", "Failed to process alternate image: " + e3.getLocalizedMessage());
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            this.B = this.q;
            TcmAid.bl = this.q;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        int i2;
        if (!this.s) {
            int i3 = 131073;
            if (this.D.x()) {
                i3 = 131073 + 16384;
            }
            if (this.D.y()) {
                i2 = i3 + 32768;
            } else {
                i2 = i3;
            }
            this.s = this.q;
            this.c = this.r;
            this.h.setVisibility(4);
            this.i.setVisibility(4);
            this.j.setVisibility(4);
            this.k.setVisibility(4);
            this.m.setVisibility(4);
            this.n.setVisibility(4);
            this.l.setVisibility(4);
            this.o.setImageResource(17301582);
            this.p.setVisibility(0);
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            inputMethodManager.getInputMethodList();
            inputMethodManager.toggleSoftInput(2, 0);
            ad.a(this.f, i2);
            ad.a(this.g, i2);
            ad.a(this.f442b, i2);
            return;
        }
        a((String) null, this.r);
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.f442b.getWindowToken(), 0);
        this.s = this.r;
        this.c = this.q;
        this.h.setVisibility(0);
        if (this.x) {
            this.j.setVisibility(0);
            this.i.setVisibility(0);
        }
        this.k.setVisibility(0);
        this.m.setVisibility(0);
        this.n.setVisibility(0);
        this.l.setVisibility(0);
        this.o.setImageResource(17301566);
        this.p.setVisibility(4);
        a(false);
    }

    private void a(String str, boolean z2) {
        int i2;
        boolean a2;
        ContentValues contentValues = new ContentValues();
        boolean z3 = this.r;
        if (TcmAid.bi == dh.f946a.intValue()) {
            a2 = z3;
            i2 = TcmAid.bi + 1000;
        } else {
            i2 = TcmAid.bi;
            a2 = q.a("SELECT COUNT(userDB.wristankle._id) FROM userDB.wristankle ", i2, this.C);
            if (!a2) {
                contentValues.put("_id", Integer.toString(i2));
            }
        }
        contentValues.put("location", this.f.getText().toString());
        contentValues.put("zone", this.d.getText().toString());
        contentValues.put("indications", this.g.getText().toString());
        contentValues.put("notes", this.f442b.getText().toString());
        if (z2) {
            contentValues.put("alt_image", str);
        }
        if (!a2) {
            try {
                this.C.insert("userDB.wristankle", null, contentValues);
            } catch (SQLException e2) {
                q.a(e2);
            }
        } else {
            this.C.update("userDB.wristankle", contentValues, "_id=" + Integer.toString(i2), null);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!this.y) {
            startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 0);
            return;
        }
        a((String) null, this.q);
        this.y = this.r;
        a(false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:116:0x04e0  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x04e9  */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x0568  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x056d A[SYNTHETIC, Splitter:B:139:0x056d] */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x057c  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x0587  */
    /* JADX WARNING: Removed duplicated region for block: B:163:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x034c  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0362 A[Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x03d0 A[Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x03de A[Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0402  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0417  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r14) {
        /*
            r13 = this;
            r11 = 4
            r10 = 1
            r9 = 0
            r8 = 0
            if (r14 == 0) goto L_0x010b
            r0 = 2131362330(0x7f0a021a, float:1.8344438E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.o = r0
            android.widget.ImageButton r0 = r13.o
            com.cyberandsons.tcmaidtrial.detailed.gn r1 = new com.cyberandsons.tcmaidtrial.detailed.gn
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            r0 = 2131362331(0x7f0a021b, float:1.834444E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r13.p = r0
            android.widget.Button r0 = r13.p
            com.cyberandsons.tcmaidtrial.detailed.gm r1 = new com.cyberandsons.tcmaidtrial.detailed.gm
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            r0 = 2131361811(0x7f0a0013, float:1.8343385E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.h = r0
            android.widget.ImageButton r0 = r13.h
            com.cyberandsons.tcmaidtrial.detailed.gp r1 = new com.cyberandsons.tcmaidtrial.detailed.gp
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            r0 = 2131361846(0x7f0a0036, float:1.8343456E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.j = r0
            android.widget.ImageButton r0 = r13.j
            com.cyberandsons.tcmaidtrial.detailed.go r1 = new com.cyberandsons.tcmaidtrial.detailed.go
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            boolean r0 = r13.x
            if (r0 != 0) goto L_0x0063
            android.widget.ImageButton r0 = r13.j
            r0.setVisibility(r11)
        L_0x0063:
            r0 = 2131361809(0x7f0a0011, float:1.834338E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.i = r0
            android.widget.ImageButton r0 = r13.i
            com.cyberandsons.tcmaidtrial.detailed.gh r1 = new com.cyberandsons.tcmaidtrial.detailed.gh
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            boolean r0 = r13.x
            if (r0 != 0) goto L_0x0081
            android.widget.ImageButton r0 = r13.i
            r0.setVisibility(r11)
        L_0x0081:
            r0 = 2131361815(0x7f0a0017, float:1.8343393E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.k = r0
            android.widget.ImageButton r0 = r13.k
            com.cyberandsons.tcmaidtrial.detailed.gi r1 = new com.cyberandsons.tcmaidtrial.detailed.gi
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            r0 = 2131361814(0x7f0a0016, float:1.834339E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.m = r0
            android.widget.ImageButton r0 = r13.m
            com.cyberandsons.tcmaidtrial.detailed.gj r1 = new com.cyberandsons.tcmaidtrial.detailed.gj
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            r0 = 2131361813(0x7f0a0015, float:1.8343389E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.n = r0
            android.widget.ImageButton r0 = r13.n
            com.cyberandsons.tcmaidtrial.detailed.gl r1 = new com.cyberandsons.tcmaidtrial.detailed.gl
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            r0 = 2131361812(0x7f0a0014, float:1.8343387E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.l = r0
            android.widget.ImageButton r0 = r13.l
            com.cyberandsons.tcmaidtrial.detailed.gg r1 = new com.cyberandsons.tcmaidtrial.detailed.gg
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            boolean r0 = r13.E
            if (r0 == 0) goto L_0x00e3
            android.widget.ImageButton r0 = r13.k
            r0.setVisibility(r11)
            android.widget.ImageButton r0 = r13.l
            r0.setVisibility(r11)
        L_0x00e3:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.bd
            if (r0 != 0) goto L_0x0421
            boolean r0 = r13.E
            if (r0 != 0) goto L_0x00f2
            android.widget.ImageButton r0 = r13.k
            boolean r1 = r13.r
            r13.a(r0, r1)
        L_0x00f2:
            android.widget.ImageButton r0 = r13.m
            boolean r1 = r13.r
            r13.a(r0, r1)
            android.widget.ImageButton r0 = r13.n
            boolean r1 = r13.q
            r13.a(r0, r1)
            boolean r0 = r13.E
            if (r0 != 0) goto L_0x010b
            android.widget.ImageButton r0 = r13.l
            boolean r1 = r13.q
            r13.a(r0, r1)
        L_0x010b:
            r0 = 2131362354(0x7f0a0232, float:1.8344486E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r13.e = r0
            android.widget.ImageView r0 = r13.e
            r0.setOnTouchListener(r13)
            android.graphics.Matrix r0 = r13.H
            r1 = 1065353216(0x3f800000, float:1.0)
            r2 = 1065353216(0x3f800000, float:1.0)
            r0.setTranslate(r1, r2)
            android.widget.ImageView r0 = r13.e
            android.graphics.Matrix r1 = r13.H
            r0.setImageMatrix(r1)
            android.widget.EditText r0 = r13.f
            if (r0 == 0) goto L_0x0131
            r13.f = r9
        L_0x0131:
            r0 = 2131362356(0x7f0a0234, float:1.834449E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.f = r0
            android.widget.EditText r0 = r13.f
            com.cyberandsons.tcmaidtrial.detailed.fu r1 = new com.cyberandsons.tcmaidtrial.detailed.fu
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.g
            if (r0 == 0) goto L_0x014c
            r13.g = r9
        L_0x014c:
            r0 = 2131362358(0x7f0a0236, float:1.8344494E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.g = r0
            android.widget.EditText r0 = r13.g
            com.cyberandsons.tcmaidtrial.detailed.fs r1 = new com.cyberandsons.tcmaidtrial.detailed.fs
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.f442b
            if (r0 == 0) goto L_0x0167
            r13.f442b = r9
        L_0x0167:
            r0 = 2131362360(0x7f0a0238, float:1.8344498E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.f442b = r0
            android.widget.EditText r0 = r13.f442b
            com.cyberandsons.tcmaidtrial.detailed.fq r1 = new com.cyberandsons.tcmaidtrial.detailed.fq
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            com.cyberandsons.tcmaidtrial.a.b r1 = new com.cyberandsons.tcmaidtrial.a.b
            r1.<init>()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "SELECT main.wristankle._id, main.wristankle.zone, main.wristankle.indications, main.wristankle.side, main.wristankle.direction, main.wristankle.location, main.wristankle.depth, main.wristankle.row1, main.wristankle.col1, main.wristankle.row2, main.wristankle.col2, main.wristankle.row3, main.wristankle.col3, main.wristankle.row4, main.wristankle.col4, main.wristankle.row5, main.wristankle.col5, main.wristankle.blocksz, main.wristankle.blockdir, main.wristankle.label, main.wristankle.red, main.wristankle.green, main.wristankle.blue, main.wristankle.image, main.wristankle.notes FROM main.wristankle WHERE "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.TcmAid.bf
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = r0.toString()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.I
            if (r0 == 0) goto L_0x019f
            java.lang.String r0 = "WAD:loadSystemSuppliedData()"
            android.util.Log.d(r0, r2)
        L_0x019f:
            android.database.sqlite.SQLiteDatabase r0 = r13.C     // Catch:{ SQLException -> 0x05a7, NullPointerException -> 0x05a3, all -> 0x04e5 }
            r3 = 0
            android.database.Cursor r0 = r0.rawQuery(r2, r3)     // Catch:{ SQLException -> 0x05a7, NullPointerException -> 0x05a3, all -> 0x04e5 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x05a7, NullPointerException -> 0x05a3, all -> 0x04e5 }
            int r3 = r0.getCount()     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            boolean r4 = com.cyberandsons.tcmaidtrial.misc.dh.I     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            if (r4 == 0) goto L_0x01f2
            java.lang.String r4 = "WAD:loadSystemSuppliedData()"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r5.<init>()     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            java.lang.String r6 = "query count = "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            java.lang.String r6 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            java.lang.String r5 = r5.toString()     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            android.util.Log.d(r4, r5)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            java.lang.String r4 = "WAD:loadSystemSuppliedData()"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r5.<init>()     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            java.lang.String r6 = "column count = '"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            int r6 = r0.getColumnCount()     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            java.lang.String r6 = "' "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            java.lang.String r5 = r5.toString()     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            android.util.Log.d(r4, r5)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
        L_0x01f2:
            boolean r4 = r0.moveToFirst()     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            if (r4 == 0) goto L_0x0314
            if (r3 != r10) goto L_0x0314
            r3 = 0
            int r3 = r0.getInt(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.f163a = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 0
            r1.f164b = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.c = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 23
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.d = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 6
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.x = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 7
            int r3 = r0.getInt(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.f = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 8
            int r3 = r0.getInt(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.g = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 9
            int r3 = r0.getInt(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.h = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 10
            int r3 = r0.getInt(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.i = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 11
            int r3 = r0.getInt(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.j = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 12
            int r3 = r0.getInt(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.k = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 13
            int r3 = r0.getInt(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.l = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 14
            int r3 = r0.getInt(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.m = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 15
            int r3 = r0.getInt(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.n = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 16
            int r3 = r0.getInt(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.o = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 17
            int r3 = r0.getInt(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.y = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 18
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.z = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 19
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.e = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 20
            float r3 = r0.getFloat(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r4 = 1065353216(0x3f800000, float:1.0)
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 != 0) goto L_0x0452
            r3 = 255(0xff, float:3.57E-43)
        L_0x028f:
            r1.A = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 21
            float r3 = r0.getFloat(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r4 = 1065353216(0x3f800000, float:1.0)
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 != 0) goto L_0x0455
            r3 = 128(0x80, float:1.794E-43)
        L_0x029f:
            r1.B = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 22
            float r3 = r0.getFloat(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r4 = 1065353216(0x3f800000, float:1.0)
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 != 0) goto L_0x0458
            r3 = 255(0xff, float:3.57E-43)
        L_0x02af:
            r1.C = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = -1
            r1.p = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = -1
            r1.q = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = -1
            r1.s = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = -1
            r1.r = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = -1
            r1.u = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = -1
            r1.t = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = -1
            r1.v = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = -1
            r1.w = r3     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            android.widget.TextView r3 = r13.d     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r4 = 1
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3.setText(r4)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 23
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r4 = 23
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r13.A = r4     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            boolean r4 = r13.x     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            if (r4 == 0) goto L_0x0498
            java.lang.String r4 = "points"
            android.graphics.Bitmap r4 = com.cyberandsons.tcmaidtrial.misc.dh.b(r3, r4)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            if (r4 != 0) goto L_0x045b
            android.widget.ImageView r1 = r13.e     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 2130837543(0x7f020027, float:1.7280043E38)
            r1.setImageResource(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
        L_0x02f5:
            android.widget.EditText r1 = r13.f     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 5
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.setText(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            android.widget.EditText r1 = r13.g     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 2
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.setText(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            android.widget.EditText r1 = r13.f442b     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 24
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r1.setText(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
        L_0x0314:
            if (r0 == 0) goto L_0x0319
            r0.close()
        L_0x0319:
            boolean r0 = r13.r
            r13.y = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT userDB.wristankle.indications, userDB.wristankle.location, userDB.wristankle.notes, userDB.wristankle.zone, userDB.wristankle.alt_image FROM userDB.wristankle WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "userDB.wristankle."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "_id"
            java.lang.StringBuilder r0 = r0.append(r1)
            r1 = 61
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.bi
            java.lang.String r1 = java.lang.Integer.toString(r1)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.I
            if (r0 == 0) goto L_0x0351
            java.lang.String r0 = "WA:loadUserModifiedData()"
            android.util.Log.d(r0, r1)
        L_0x0351:
            android.database.sqlite.SQLiteDatabase r0 = r13.C     // Catch:{ SQLException -> 0x0596, NullPointerException -> 0x0593, all -> 0x058e }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x0596, NullPointerException -> 0x0593, all -> 0x058e }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0596, NullPointerException -> 0x0593, all -> 0x058e }
            int r2 = r0.getCount()     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.I     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            if (r3 == 0) goto L_0x03a4
            java.lang.String r3 = "WAD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r4.<init>()     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            java.lang.String r5 = "query count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            java.lang.String r5 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            java.lang.String r3 = "WAD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r4.<init>()     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            java.lang.String r5 = "column count = '"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            int r5 = r0.getColumnCount()     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            java.lang.String r5 = "' "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
        L_0x03a4:
            boolean r3 = r0.moveToFirst()     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            if (r3 == 0) goto L_0x03f9
            if (r2 != r10) goto L_0x03f9
            android.widget.EditText r2 = r13.g     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r3 = 0
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            android.widget.EditText r2 = r13.f     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            android.widget.EditText r2 = r13.f442b     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r3 = 2
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.bi     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r2 <= r3) goto L_0x03da
            android.widget.TextView r2 = r13.d     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r3 = 3
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
        L_0x03da:
            boolean r2 = r13.x     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            if (r2 == 0) goto L_0x056d
            r2 = 4
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            if (r2 == 0) goto L_0x051a
            int r3 = r2.length()     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            if (r3 <= 0) goto L_0x051a
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeFile(r2)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            if (r2 != 0) goto L_0x04ed
            android.widget.ImageView r2 = r13.e     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r3 = 2130837543(0x7f020027, float:1.7280043E38)
            r2.setImageResource(r3)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
        L_0x03f9:
            if (r0 == 0) goto L_0x03fe
            r0.close()
        L_0x03fe:
            boolean r0 = r13.x
            if (r0 == 0) goto L_0x0587
            boolean r0 = r13.y
            if (r0 == 0) goto L_0x0580
            android.widget.ImageButton r0 = r13.i
            r1 = 17301560(0x1080038, float:2.4979412E-38)
            r0.setImageResource(r1)
            android.widget.ImageButton r0 = r13.i
            r0.setVisibility(r8)
        L_0x0413:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.cR
            if (r0 != r10) goto L_0x0420
            com.cyberandsons.tcmaidtrial.a.n r0 = r13.D
            java.lang.String r0 = r0.M()
            r13.a(r0)
        L_0x0420:
            return
        L_0x0421:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.bd
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.be
            int r1 = r1.size()
            int r1 = r1 - r10
            if (r0 != r1) goto L_0x010b
            boolean r0 = r13.E
            if (r0 != 0) goto L_0x0437
            android.widget.ImageButton r0 = r13.k
            boolean r1 = r13.q
            r13.a(r0, r1)
        L_0x0437:
            android.widget.ImageButton r0 = r13.m
            boolean r1 = r13.q
            r13.a(r0, r1)
            android.widget.ImageButton r0 = r13.n
            boolean r1 = r13.r
            r13.a(r0, r1)
            boolean r0 = r13.E
            if (r0 != 0) goto L_0x010b
            android.widget.ImageButton r0 = r13.l
            boolean r1 = r13.r
            r13.a(r0, r1)
            goto L_0x010b
        L_0x0452:
            r3 = r8
            goto L_0x028f
        L_0x0455:
            r3 = r8
            goto L_0x029f
        L_0x0458:
            r3 = r8
            goto L_0x02af
        L_0x045b:
            com.cyberandsons.tcmaidtrial.draw.a r5 = new com.cyberandsons.tcmaidtrial.draw.a     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            int r6 = r4.getHeight()     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            int r7 = r4.getWidth()     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r5.<init>(r13, r4, r6, r7)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r5.a(r3, r1)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            android.graphics.Bitmap r1 = r5.a()     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            int r3 = r3 - r8
            int r4 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            int r4 = r4 - r8
            android.view.Window r5 = r13.getWindow()     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            android.graphics.drawable.BitmapDrawable r1 = com.cyberandsons.tcmaidtrial.misc.dh.a(r1, r3, r4, r5)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            android.widget.ImageView r3 = r13.e     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3.setImageDrawable(r1)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            android.widget.ImageView r1 = r13.e     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 0
            r1.setVisibility(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            goto L_0x02f5
        L_0x048a:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x048e:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x05a0 }
            if (r1 == 0) goto L_0x0319
            r1.close()
            goto L_0x0319
        L_0x0498:
            android.widget.ImageView r1 = r13.e     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            r3 = 8
            r1.setVisibility(r3)     // Catch:{ SQLException -> 0x048a, NullPointerException -> 0x04a1 }
            goto L_0x02f5
        L_0x04a1:
            r1 = move-exception
        L_0x04a2:
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x059a }
            java.lang.String r3 = "myVariable"
            r1.a(r3, r2)     // Catch:{ all -> 0x059a }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x059a }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x059a }
            java.lang.String r3 = "WAD:loadSystemSuppliedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x059a }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x059a }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x059a }
            r1.<init>(r13)     // Catch:{ all -> 0x059a }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x059a }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x059a }
            r2 = 2131099673(0x7f060019, float:1.7811706E38)
            java.lang.String r2 = r13.getString(r2)     // Catch:{ all -> 0x059a }
            r1.setMessage(r2)     // Catch:{ all -> 0x059a }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.fo r3 = new com.cyberandsons.tcmaidtrial.detailed.fo     // Catch:{ all -> 0x059a }
            r3.<init>(r13)     // Catch:{ all -> 0x059a }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x059a }
            r1.show()     // Catch:{ all -> 0x059a }
            if (r0 == 0) goto L_0x0319
            r0.close()
            goto L_0x0319
        L_0x04e5:
            r0 = move-exception
            r1 = r9
        L_0x04e7:
            if (r1 == 0) goto L_0x04ec
            r1.close()
        L_0x04ec:
            throw r0
        L_0x04ed:
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            int r3 = r3 - r8
            int r4 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            int r4 = r4 - r8
            android.view.Window r5 = r13.getWindow()     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            android.graphics.drawable.BitmapDrawable r2 = com.cyberandsons.tcmaidtrial.misc.dh.a(r2, r3, r4, r5)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            android.widget.ImageView r3 = r13.e     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r3.setImageDrawable(r2)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            android.widget.ImageView r2 = r13.e     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r3 = 0
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            boolean r2 = r13.q     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r13.y = r2     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            goto L_0x03f9
        L_0x050c:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x0510:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0591 }
            if (r1 == 0) goto L_0x03fe
            r1.close()
            goto L_0x03fe
        L_0x051a:
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.bi     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r2 <= r3) goto L_0x03f9
            android.widget.ImageView r2 = r13.e     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r3 = 8
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            goto L_0x03f9
        L_0x0529:
            r2 = move-exception
        L_0x052a:
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0576 }
            java.lang.String r3 = "myVariable"
            r2.a(r3, r1)     // Catch:{ all -> 0x0576 }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0576 }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x0576 }
            java.lang.String r3 = "WAD:loadUserModifiedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x0576 }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x0576 }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x0576 }
            r1.<init>(r13)     // Catch:{ all -> 0x0576 }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x0576 }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x0576 }
            r2 = 2131099674(0x7f06001a, float:1.7811708E38)
            java.lang.String r2 = r13.getString(r2)     // Catch:{ all -> 0x0576 }
            r1.setMessage(r2)     // Catch:{ all -> 0x0576 }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.gf r3 = new com.cyberandsons.tcmaidtrial.detailed.gf     // Catch:{ all -> 0x0576 }
            r3.<init>(r13)     // Catch:{ all -> 0x0576 }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x0576 }
            r1.show()     // Catch:{ all -> 0x0576 }
            if (r0 == 0) goto L_0x03fe
            r0.close()
            goto L_0x03fe
        L_0x056d:
            android.widget.ImageView r2 = r13.e     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            r3 = 8
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x050c, NullPointerException -> 0x0529 }
            goto L_0x03f9
        L_0x0576:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x057a:
            if (r1 == 0) goto L_0x057f
            r1.close()
        L_0x057f:
            throw r0
        L_0x0580:
            android.widget.ImageButton r0 = r13.i
            r0.setVisibility(r11)
            goto L_0x0413
        L_0x0587:
            android.widget.ImageButton r0 = r13.i
            r0.setVisibility(r11)
            goto L_0x0413
        L_0x058e:
            r0 = move-exception
            r1 = r9
            goto L_0x057a
        L_0x0591:
            r0 = move-exception
            goto L_0x057a
        L_0x0593:
            r0 = move-exception
            r0 = r9
            goto L_0x052a
        L_0x0596:
            r0 = move-exception
            r1 = r9
            goto L_0x0510
        L_0x059a:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x04e7
        L_0x05a0:
            r0 = move-exception
            goto L_0x04e7
        L_0x05a3:
            r0 = move-exception
            r0 = r9
            goto L_0x04a2
        L_0x05a7:
            r0 = move-exception
            r1 = r9
            goto L_0x048e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.detailed.WristAnkleDetail.a(boolean):void");
    }

    private void a(String str) {
        if (str != null && str.length() != 0) {
            String[] split = str.split(",");
            for (String parseInt : split) {
                switch (Integer.parseInt(parseInt)) {
                    case 1:
                        dh.a(this.f.getText());
                        break;
                    case 2:
                        dh.a(this.g.getText());
                        break;
                    case 3:
                        dh.a(this.f442b.getText());
                        break;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        boolean z2 = this.r;
        switch (i2) {
            case 0:
                if (this.w) {
                    this.u -= 2;
                } else {
                    TcmAid.bd = 0;
                    if (!this.E) {
                        a(this.k, this.r);
                    }
                    a(this.m, this.r);
                    a(this.n, this.q);
                    if (!this.E) {
                        a(this.l, this.q);
                    }
                }
                z2 = this.q;
                break;
            case 1:
                if (this.w) {
                    this.v -= 2;
                } else if (TcmAid.bd - 1 >= 0) {
                    if (TcmAid.bd - 1 == 0) {
                        if (!this.E) {
                            a(this.k, this.r);
                        }
                        a(this.m, this.r);
                    }
                    if (this.n.isEnabled() == this.r) {
                        a(this.n, this.q);
                        if (!this.E) {
                            a(this.l, this.q);
                        }
                    }
                    TcmAid.bd--;
                }
                z2 = this.q;
                break;
            case 2:
                if (!this.w) {
                    int size = TcmAid.be.size();
                    if (TcmAid.bd + 1 < size) {
                        if (TcmAid.bd + 1 == size - 1) {
                            a(this.n, this.r);
                            if (!this.E) {
                                a(this.l, this.r);
                            }
                        }
                        if (this.m.isEnabled() == this.r) {
                            if (!this.E) {
                                a(this.k, this.q);
                            }
                            a(this.m, this.q);
                        }
                        TcmAid.bd++;
                        z2 = this.q;
                        break;
                    }
                } else {
                    this.v += 2;
                    z2 = this.q;
                    break;
                }
                break;
            case 3:
                if (this.w) {
                    this.u += 2;
                } else {
                    TcmAid.bd = TcmAid.be.size() - 1;
                    if (!this.E) {
                        a(this.k, this.q);
                    }
                    a(this.m, this.q);
                    a(this.n, this.r);
                    if (!this.E) {
                        a(this.l, this.r);
                    }
                }
                z2 = this.q;
                break;
        }
        if (z2) {
            if (TcmAid.bd >= TcmAid.be.size()) {
                TcmAid.bd = TcmAid.be.size() - 1;
            }
            int intValue = ((Integer) TcmAid.be.get(TcmAid.bd)).intValue();
            TcmAid.bf = "main.wristankle._id=" + Integer.toString(intValue);
            TcmAid.bi = intValue;
            a(false);
            this.f441a.post(new ge(this));
        }
    }

    private void a(ImageButton imageButton, boolean z2) {
        if (z2) {
            imageButton.setEnabled(this.q);
            imageButton.setVisibility(0);
            return;
        }
        imageButton.setEnabled(this.r);
        imageButton.setVisibility(4);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        e a2 = e.a(motionEvent);
        ImageView imageView = (ImageView) view;
        StringBuilder sb = new StringBuilder();
        int b2 = a2.b();
        int i2 = b2 & 255;
        sb.append("event ACTION_").append(new String[]{"DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE", "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?"}[i2]);
        if (i2 == 5 || i2 == 6) {
            sb.append("(pid ").append(b2 >> 8);
            sb.append(')');
        }
        sb.append('[');
        for (int i3 = 0; i3 < a2.a(); i3++) {
            sb.append('#').append(i3);
            sb.append("(pid ").append(a2.c(i3));
            sb.append(")=").append((int) a2.a(i3));
            sb.append(',').append((int) a2.b(i3));
            if (i3 + 1 < a2.a()) {
                sb.append(';');
            }
        }
        sb.append(']');
        Log.d("WAD:dumpEvent()", sb.toString());
        switch (a2.b() & 255) {
            case 0:
                this.I.set(this.H);
                this.K.set(a2.c(), a2.d());
                Log.d("WAD:onTouch()", "mode=DRAG");
                this.J = 1;
                break;
            case 1:
            case 6:
                this.J = 0;
                Log.d("WAD:onTouch()", "mode=NONE");
                break;
            case 2:
                if (this.J != 1) {
                    if (this.J == 2) {
                        float a3 = a(a2);
                        Log.d("WAD:onTouch()", "newDist=" + a3);
                        if (a3 > 10.0f) {
                            this.H.set(this.I);
                            float f2 = a3 / this.M;
                            this.H.postScale(f2, f2, this.L.x, this.L.y);
                            break;
                        }
                    }
                } else {
                    this.H.set(this.I);
                    this.H.postTranslate(a2.c() - this.K.x, a2.d() - this.K.y);
                    break;
                }
                break;
            case 5:
                this.M = a(a2);
                Log.d("WAD:onTouch()", "oldDist=" + this.M);
                if (this.M > 10.0f) {
                    this.I.set(this.H);
                    this.L.set((a2.a(0) + a2.a(1)) / 2.0f, (a2.b(1) + a2.b(0)) / 2.0f);
                    this.J = 2;
                    Log.d("WAD:onTouch()", "mode=ZOOM");
                    break;
                }
                break;
        }
        imageView.setImageMatrix(this.H);
        return true;
    }

    private static float a(e eVar) {
        float a2 = eVar.a(0) - eVar.a(1);
        float b2 = eVar.b(0) - eVar.b(1);
        return FloatMath.sqrt((a2 * a2) + (b2 * b2));
    }

    public void onClick(View view) {
    }
}
