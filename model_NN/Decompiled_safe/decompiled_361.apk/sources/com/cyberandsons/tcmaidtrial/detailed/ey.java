package com.cyberandsons.tcmaidtrial.detailed;

import android.view.MotionEvent;
import android.view.View;

final class ey implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointsDetail f576a;

    ey(PointsDetail pointsDetail) {
        this.f576a = pointsDetail;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f576a.X.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f576a.c;
    }
}
