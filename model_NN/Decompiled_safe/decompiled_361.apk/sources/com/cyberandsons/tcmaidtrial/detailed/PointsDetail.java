package com.cyberandsons.tcmaidtrial.detailed;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.FloatMath;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.a;
import com.cyberandsons.tcmaidtrial.a.b;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.draw.e;
import com.cyberandsons.tcmaidtrial.lists.PointsList;
import com.cyberandsons.tcmaidtrial.misc.ad;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class PointsDetail extends Activity implements View.OnClickListener, View.OnTouchListener {
    private boolean A;
    private boolean B;
    private boolean C;
    private int D;
    private int E;
    private boolean F;
    private boolean G;
    private boolean H;
    private boolean I;
    private boolean J;
    private String K;
    private boolean L;
    private boolean M;
    private boolean N;
    private boolean O;
    private boolean P;
    private SQLiteDatabase Q;
    private n R;
    private int S;
    private boolean T;
    private boolean U;
    /* access modifiers changed from: private */
    public GestureDetector V;
    private View.OnTouchListener W;
    /* access modifiers changed from: private */
    public GestureDetector X;
    private View.OnTouchListener Y;
    private Matrix Z;

    /* renamed from: a  reason: collision with root package name */
    ScrollView f431a;
    private Matrix aa;
    private int ab;
    private PointF ac;
    private PointF ad;
    private float ae;

    /* renamed from: b  reason: collision with root package name */
    EditText f432b;
    boolean c;
    private TextView d;
    private ImageView e;
    private EditText f;
    private TextView g;
    private EditText h;
    private EditText i;
    private EditText j;
    private EditText k;
    private EditText l;
    private EditText m;
    private EditText n;
    private EditText o;
    private EditText p;
    private ImageButton q;
    private ImageButton r;
    private ImageButton s;
    private ImageButton t;
    private ImageButton u;
    private ImageButton v;
    private ImageButton w;
    private ImageButton x;
    private Button y;
    private boolean z = true;

    public PointsDetail() {
        this.A = !this.z;
        this.B = this.A;
        this.C = this.A;
        this.F = this.A;
        this.G = this.A;
        this.H = this.A;
        this.c = this.z;
        this.I = this.A;
        this.J = this.A;
        this.L = this.z;
        this.M = this.z;
        this.N = this.A;
        this.O = this.A;
        this.P = this.A;
        this.T = this.A;
        this.U = this.A;
        this.Z = new Matrix();
        this.aa = new Matrix();
        this.ab = 0;
        this.ac = new PointF();
        this.ad = new PointF();
        this.ae = 1.0f;
    }

    static /* synthetic */ void a(PointsDetail pointsDetail) {
        String format;
        if (pointsDetail.T) {
            String r2 = pointsDetail.R.r();
            if (n.a(r2, pointsDetail.S)) {
                String[] split = r2.split(",");
                boolean z2 = pointsDetail.z;
                String str = "";
                for (String parseInt : split) {
                    int parseInt2 = Integer.parseInt(parseInt);
                    if (parseInt2 != pointsDetail.S) {
                        if (z2) {
                            str = String.format("%d", Integer.valueOf(parseInt2));
                            z2 = pointsDetail.A;
                        } else {
                            str = str + String.format(",%d", Integer.valueOf(parseInt2));
                        }
                    }
                }
                pointsDetail.q.setImageDrawable(pointsDetail.getResources().getDrawable(17301619));
                format = str;
            } else {
                if (r2 == null || r2.length() <= 0) {
                    format = String.format("%d", Integer.valueOf(pointsDetail.S));
                } else {
                    format = String.format("%s,%d", r2, Integer.valueOf(pointsDetail.S));
                }
                pointsDetail.q.setImageDrawable(pointsDetail.getResources().getDrawable(17301618));
            }
            pointsDetail.R.f(format, pointsDetail.Q);
            return;
        }
        pointsDetail.setResult(2);
        pointsDetail.finish();
    }

    static /* synthetic */ void b(PointsDetail pointsDetail) {
        if (pointsDetail.H) {
            pointsDetail.I = !pointsDetail.I;
            if (!pointsDetail.I) {
                pointsDetail.s.setImageResource(C0000R.drawable.tags);
            } else {
                pointsDetail.s.setImageResource(C0000R.drawable.tag);
            }
        }
        pointsDetail.d();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.Q == null || !this.Q.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("PD:openDataBase()", str + " opened.");
                this.Q = SQLiteDatabase.openDatabase(str, null, 16);
                this.Q.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.Q.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("PD:openDataBase()", str2 + " ATTACHed.");
                this.R = new n();
                this.R.a(this.Q);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new ev(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        this.H = this.R.b();
        this.M = this.R.C();
        this.G = this.R.m();
        this.O = this.R.X();
        this.P = this.R.Y();
        this.U = this.R.ad();
        setContentView((int) C0000R.layout.points_detailed_edit);
        this.d = (TextView) findViewById(C0000R.id.tce_label);
        this.f431a = (ScrollView) findViewById(C0000R.id.svDetail);
        getWindow().setSoftInputMode(3);
        this.V = new GestureDetector(new bg(this));
        this.W = new ez(this);
        this.X = new GestureDetector(new cj(this));
        this.Y = new ey(this);
        this.d.setOnClickListener(this);
        this.d.setOnTouchListener(this.Y);
        a(true);
        this.f431a.smoothScrollBy(0, 0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 2, 0, "Add").setIcon(17301559);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 2:
                c();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void onDestroy() {
        TcmAid.ap = null;
        try {
            if (this.Q != null && this.Q.isOpen()) {
                this.Q.close();
                this.Q = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
        if (i3 == -1) {
            Uri data = intent.getData();
            Log.i("onActivityResult", data.toString());
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(getContentResolver().openInputStream(data));
                if (decodeStream != null) {
                    BitmapDrawable a2 = dh.a(decodeStream, TcmAid.cS - 0, TcmAid.cS - 0, getWindow());
                    this.e.setImageDrawable(a2);
                    this.e.setVisibility(0);
                    this.J = this.z;
                    this.r.setImageResource(17301560);
                    this.r.setVisibility(0);
                    String format = String.format("%s/%s_alt.png", getString(C0000R.string.image_path), this.K);
                    a(format, this.z);
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(format);
                        a2.getBitmap().compress(Bitmap.CompressFormat.PNG, 90, fileOutputStream);
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    } catch (Exception e2) {
                        Log.e("PD:onActivityResult", "Failed to write alternate image: " + e2.getLocalizedMessage());
                    }
                }
            } catch (FileNotFoundException e3) {
                Log.e("PD:onActivityResult", "Failed to process alternate image: " + e3.getLocalizedMessage());
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            this.N = this.z;
            TcmAid.bl = this.z;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        int i2;
        int i3;
        int i4;
        int i5;
        if (!this.B) {
            if (this.R.x()) {
                i2 = 1 + 16384;
                i3 = 131073 + 16384;
            } else {
                i2 = 1;
                i3 = 131073;
            }
            if (this.R.y()) {
                i4 = i2 + 32768;
                i5 = i3 + 32768;
            } else {
                i4 = i2;
                i5 = i3;
            }
            this.B = this.z;
            this.c = this.A;
            this.q.setVisibility(4);
            this.s.setVisibility(4);
            this.r.setVisibility(4);
            this.t.setVisibility(4);
            this.v.setVisibility(4);
            this.w.setVisibility(4);
            this.u.setVisibility(4);
            this.x.setImageResource(17301582);
            this.y.setVisibility(0);
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            inputMethodManager.getInputMethodList();
            inputMethodManager.toggleSoftInput(2, 0);
            ad.a(this.f, i5);
            ad.a(this.h, i4);
            ad.a(this.j, i5);
            ad.a(this.k, i5);
            ad.a(this.l, i5);
            ad.a(this.m, i5);
            ad.a(this.n, i5);
            ad.a(this.o, i5);
            ad.a(this.p, i5);
            ad.a(this.f432b, i5);
            return;
        }
        a((String) null, this.A);
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.f432b.getWindowToken(), 0);
        this.B = this.A;
        this.c = this.z;
        this.q.setVisibility(0);
        if (this.H) {
            this.s.setVisibility(0);
            this.r.setVisibility(0);
        }
        this.t.setVisibility(0);
        this.v.setVisibility(0);
        this.w.setVisibility(0);
        this.u.setVisibility(0);
        this.x.setImageResource(17301566);
        this.y.setVisibility(4);
        a(false);
    }

    private void a(String str, boolean z2) {
        int i2;
        boolean a2;
        ContentValues contentValues = new ContentValues();
        boolean z3 = this.A;
        if (TcmAid.at == dh.f946a.intValue()) {
            a2 = z3;
            i2 = TcmAid.at + 1000;
        } else {
            i2 = TcmAid.at;
            a2 = q.a("SELECT COUNT(userDB.pointslocation._id) FROM userDB.pointslocation ", i2, this.Q);
            if (!a2) {
                contentValues.put("_id", Integer.toString(i2));
            }
        }
        contentValues.put("location", this.f.getText().toString());
        contentValues.put("englishname", this.h.getText().toString());
        contentValues.put("indication", this.j.getText().toString());
        contentValues.put("func", this.k.getText().toString());
        contentValues.put("needling_method", this.l.getText().toString());
        contentValues.put("contraindication", this.m.getText().toString());
        contentValues.put("localapp", this.n.getText().toString());
        contentValues.put("misc", this.o.getText().toString());
        contentValues.put("needlingcombo", this.p.getText().toString());
        contentValues.put("commentary", this.f432b.getText().toString());
        int indexOf = this.d.getText().toString().indexOf(" - ");
        if (indexOf > 0) {
            contentValues.put("common_name", this.d.getText().toString().substring(0, indexOf));
        } else {
            contentValues.put("common_name", this.d.getText().toString());
        }
        if (z2) {
            contentValues.put("alt_image", str);
        }
        if (!a2) {
            try {
                this.Q.insert("userDB.pointslocation", null, contentValues);
            } catch (SQLException e2) {
                q.a(e2);
            }
        } else {
            this.Q.update("userDB.pointslocation", contentValues, "_id=" + Integer.toString(i2), null);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (!this.J) {
            startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 0);
            return;
        }
        a((String) null, this.z);
        this.J = this.A;
        a(false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:113:0x0479  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x048d  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0498  */
    /* JADX WARNING: Removed duplicated region for block: B:131:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x03b0  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x03c5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r11) {
        /*
            r10 = this;
            r8 = 1
            r7 = 0
            r6 = 4
            r3 = 0
            if (r11 == 0) goto L_0x0113
            r0 = 2131362330(0x7f0a021a, float:1.8344438E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r10.x = r0
            android.widget.ImageButton r0 = r10.x
            com.cyberandsons.tcmaidtrial.detailed.bh r1 = new com.cyberandsons.tcmaidtrial.detailed.bh
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362331(0x7f0a021b, float:1.834444E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r10.y = r0
            android.widget.Button r0 = r10.y
            com.cyberandsons.tcmaidtrial.detailed.bi r1 = new com.cyberandsons.tcmaidtrial.detailed.bi
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            com.cyberandsons.tcmaidtrial.a.n r0 = r10.R
            boolean r0 = r0.ab()
            r10.T = r0
            r0 = 2131361811(0x7f0a0013, float:1.8343385E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r10.q = r0
            android.widget.ImageButton r0 = r10.q
            com.cyberandsons.tcmaidtrial.detailed.bj r1 = new com.cyberandsons.tcmaidtrial.detailed.bj
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131361844(0x7f0a0034, float:1.8343452E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r10.s = r0
            android.widget.ImageButton r0 = r10.s
            com.cyberandsons.tcmaidtrial.detailed.bk r1 = new com.cyberandsons.tcmaidtrial.detailed.bk
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            boolean r0 = r10.H
            if (r0 != 0) goto L_0x006b
            android.widget.ImageButton r0 = r10.s
            r0.setVisibility(r6)
        L_0x006b:
            r0 = 2131361809(0x7f0a0011, float:1.834338E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r10.r = r0
            android.widget.ImageButton r0 = r10.r
            com.cyberandsons.tcmaidtrial.detailed.bl r1 = new com.cyberandsons.tcmaidtrial.detailed.bl
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            boolean r0 = r10.H
            if (r0 != 0) goto L_0x0089
            android.widget.ImageButton r0 = r10.r
            r0.setVisibility(r6)
        L_0x0089:
            r0 = 2131361815(0x7f0a0017, float:1.8343393E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r10.t = r0
            android.widget.ImageButton r0 = r10.t
            com.cyberandsons.tcmaidtrial.detailed.bm r1 = new com.cyberandsons.tcmaidtrial.detailed.bm
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131361814(0x7f0a0016, float:1.834339E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r10.v = r0
            android.widget.ImageButton r0 = r10.v
            com.cyberandsons.tcmaidtrial.detailed.bn r1 = new com.cyberandsons.tcmaidtrial.detailed.bn
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131361813(0x7f0a0015, float:1.8343389E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r10.w = r0
            android.widget.ImageButton r0 = r10.w
            com.cyberandsons.tcmaidtrial.detailed.bo r1 = new com.cyberandsons.tcmaidtrial.detailed.bo
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131361812(0x7f0a0014, float:1.8343387E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r10.u = r0
            android.widget.ImageButton r0 = r10.u
            com.cyberandsons.tcmaidtrial.detailed.bp r1 = new com.cyberandsons.tcmaidtrial.detailed.bp
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            boolean r0 = r10.U
            if (r0 == 0) goto L_0x00eb
            android.widget.ImageButton r0 = r10.t
            r0.setVisibility(r6)
            android.widget.ImageButton r0 = r10.u
            r0.setVisibility(r6)
        L_0x00eb:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.ak
            if (r0 != 0) goto L_0x03cf
            boolean r0 = r10.U
            if (r0 != 0) goto L_0x00fa
            android.widget.ImageButton r0 = r10.t
            boolean r1 = r10.A
            r10.a(r0, r1)
        L_0x00fa:
            android.widget.ImageButton r0 = r10.v
            boolean r1 = r10.A
            r10.a(r0, r1)
            android.widget.ImageButton r0 = r10.w
            boolean r1 = r10.z
            r10.a(r0, r1)
            boolean r0 = r10.U
            if (r0 != 0) goto L_0x0113
            android.widget.ImageButton r0 = r10.u
            boolean r1 = r10.z
            r10.a(r0, r1)
        L_0x0113:
            r0 = 2131362100(0x7f0a0134, float:1.8343971E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r10.e = r0
            android.widget.ImageView r0 = r10.e
            r0.setOnTouchListener(r10)
            android.graphics.Matrix r0 = r10.Z
            r1 = 1065353216(0x3f800000, float:1.0)
            r2 = 1065353216(0x3f800000, float:1.0)
            r0.setTranslate(r1, r2)
            android.widget.ImageView r0 = r10.e
            android.graphics.Matrix r1 = r10.Z
            r0.setImageMatrix(r1)
            android.widget.EditText r0 = r10.f
            if (r0 == 0) goto L_0x0139
            r10.f = r3
        L_0x0139:
            r0 = 2131362108(0x7f0a013c, float:1.8343987E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.f = r0
            android.widget.EditText r0 = r10.f
            com.cyberandsons.tcmaidtrial.detailed.eg r1 = new com.cyberandsons.tcmaidtrial.detailed.eg
            r1.<init>(r10)
            r0.setOnTouchListener(r1)
            android.widget.TextView r0 = r10.g
            if (r0 == 0) goto L_0x0154
            r10.g = r3
        L_0x0154:
            r0 = 2131362102(0x7f0a0136, float:1.8343975E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.g = r0
            android.widget.EditText r0 = r10.h
            if (r0 == 0) goto L_0x0165
            r10.h = r3
        L_0x0165:
            r0 = 2131362110(0x7f0a013e, float:1.8343991E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.h = r0
            android.widget.EditText r0 = r10.h
            com.cyberandsons.tcmaidtrial.detailed.ef r1 = new com.cyberandsons.tcmaidtrial.detailed.ef
            r1.<init>(r10)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r10.i
            if (r0 == 0) goto L_0x0180
            r10.i = r3
        L_0x0180:
            r0 = 2131362112(0x7f0a0140, float:1.8343995E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.i = r0
            android.widget.EditText r0 = r10.i
            com.cyberandsons.tcmaidtrial.detailed.ee r1 = new com.cyberandsons.tcmaidtrial.detailed.ee
            r1.<init>(r10)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r10.j
            if (r0 == 0) goto L_0x019b
            r10.j = r3
        L_0x019b:
            r0 = 2131362114(0x7f0a0142, float:1.8344E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.j = r0
            android.widget.EditText r0 = r10.j
            com.cyberandsons.tcmaidtrial.detailed.ed r1 = new com.cyberandsons.tcmaidtrial.detailed.ed
            r1.<init>(r10)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r10.k
            if (r0 == 0) goto L_0x01b6
            r10.k = r3
        L_0x01b6:
            r0 = 2131362116(0x7f0a0144, float:1.8344003E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.k = r0
            android.widget.EditText r0 = r10.k
            com.cyberandsons.tcmaidtrial.detailed.ec r1 = new com.cyberandsons.tcmaidtrial.detailed.ec
            r1.<init>(r10)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r10.l
            if (r0 == 0) goto L_0x01d1
            r10.l = r3
        L_0x01d1:
            r0 = 2131362118(0x7f0a0146, float:1.8344008E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.l = r0
            android.widget.EditText r0 = r10.l
            com.cyberandsons.tcmaidtrial.detailed.eb r1 = new com.cyberandsons.tcmaidtrial.detailed.eb
            r1.<init>(r10)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r10.m
            if (r0 == 0) goto L_0x01ec
            r10.m = r3
        L_0x01ec:
            r0 = 2131362120(0x7f0a0148, float:1.8344012E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.m = r0
            android.widget.EditText r0 = r10.m
            com.cyberandsons.tcmaidtrial.detailed.ea r1 = new com.cyberandsons.tcmaidtrial.detailed.ea
            r1.<init>(r10)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r10.n
            if (r0 == 0) goto L_0x0207
            r10.n = r3
        L_0x0207:
            r0 = 2131362122(0x7f0a014a, float:1.8344016E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.n = r0
            android.widget.EditText r0 = r10.n
            com.cyberandsons.tcmaidtrial.detailed.ei r1 = new com.cyberandsons.tcmaidtrial.detailed.ei
            r1.<init>(r10)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r10.o
            if (r0 == 0) goto L_0x0222
            r10.o = r3
        L_0x0222:
            r0 = 2131362124(0x7f0a014c, float:1.834402E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.o = r0
            android.widget.EditText r0 = r10.o
            com.cyberandsons.tcmaidtrial.detailed.ej r1 = new com.cyberandsons.tcmaidtrial.detailed.ej
            r1.<init>(r10)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r10.p
            if (r0 == 0) goto L_0x023d
            r10.p = r3
        L_0x023d:
            r0 = 2131362126(0x7f0a014e, float:1.8344024E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.p = r0
            android.widget.EditText r0 = r10.p
            r0.setOnClickListener(r10)
            android.widget.EditText r0 = r10.p
            android.view.View$OnTouchListener r1 = r10.W
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r10.f432b
            if (r0 == 0) goto L_0x025a
            r10.f432b = r3
        L_0x025a:
            r0 = 2131362128(0x7f0a0150, float:1.8344028E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.f432b = r0
            android.widget.EditText r0 = r10.f432b
            com.cyberandsons.tcmaidtrial.detailed.eh r1 = new com.cyberandsons.tcmaidtrial.detailed.eh
            r1.<init>(r10)
            r0.setOnTouchListener(r1)
            r10.d()
            boolean r0 = r10.A
            r10.J = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT userDB.pointslocation.indication, userDB.pointslocation.func, userDB.pointslocation.needling_method, userDB.pointslocation.contraindication, userDB.pointslocation.localapp, userDB.pointslocation.misc, userDB.pointslocation.needlingcombo, userDB.pointslocation.commentary, userDB.pointslocation.location, userDB.pointslocation.englishname, main.meridian.abbr_name, userDB.pointslocation.name, userDB.pointslocation.common_name, userDB.pointslocation.alt_image FROM userDB.pointslocation LEFT JOIN main.meridian ON main.meridian._id = userDB.pointslocation.meridian WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "userDB.pointslocation."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "_id"
            java.lang.StringBuilder r0 = r0.append(r1)
            r1 = 61
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.at
            java.lang.String r1 = java.lang.Integer.toString(r1)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.m
            if (r0 == 0) goto L_0x02aa
            java.lang.String r0 = "PD:loadUserModifiedData()"
            android.util.Log.d(r0, r1)
        L_0x02aa:
            android.database.sqlite.SQLiteDatabase r0 = r10.Q     // Catch:{ SQLException -> 0x04a7, NullPointerException -> 0x04a4, all -> 0x049f }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x04a7, NullPointerException -> 0x04a4, all -> 0x049f }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x04a7, NullPointerException -> 0x04a4, all -> 0x049f }
            int r2 = r0.getCount()     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.m     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            if (r3 == 0) goto L_0x02fd
            java.lang.String r3 = "PD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r4.<init>()     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            java.lang.String r5 = "query count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            java.lang.String r5 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            java.lang.String r3 = "PD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r4.<init>()     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            java.lang.String r5 = "column count = '"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            int r5 = r0.getColumnCount()     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            java.lang.String r5 = "' "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
        L_0x02fd:
            boolean r3 = r0.moveToFirst()     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            if (r3 == 0) goto L_0x03a7
            if (r2 != r8) goto L_0x03a7
            android.widget.EditText r2 = r10.j     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 0
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r2.setText(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            android.widget.EditText r2 = r10.k     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r2.setText(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            android.widget.EditText r2 = r10.l     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 2
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r2.setText(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            android.widget.EditText r2 = r10.m     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 3
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r2.setText(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            android.widget.EditText r2 = r10.n     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 4
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r2.setText(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            android.widget.EditText r2 = r10.o     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 5
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r2.setText(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            android.widget.EditText r2 = r10.p     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 6
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r2.setText(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            android.widget.EditText r2 = r10.f432b     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 7
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r2.setText(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            android.widget.EditText r2 = r10.f     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 8
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r2.setText(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            android.widget.EditText r2 = r10.h     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 9
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r2.setText(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            android.widget.TextView r2 = r10.g     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 11
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r2.setText(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.at     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r2 <= r3) goto L_0x0387
            android.widget.TextView r2 = r10.d     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 12
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r2.setText(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
        L_0x0387:
            boolean r2 = r10.H     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            if (r2 == 0) goto L_0x047e
            r2 = 13
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            if (r2 == 0) goto L_0x042b
            int r3 = r2.length()     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            if (r3 <= 0) goto L_0x042b
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeFile(r2)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            if (r2 != 0) goto L_0x0400
            android.widget.ImageView r2 = r10.e     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 2130837543(0x7f020027, float:1.7280043E38)
            r2.setImageResource(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
        L_0x03a7:
            if (r0 == 0) goto L_0x03ac
            r0.close()
        L_0x03ac:
            boolean r0 = r10.H
            if (r0 == 0) goto L_0x0498
            boolean r0 = r10.J
            if (r0 == 0) goto L_0x0491
            android.widget.ImageButton r0 = r10.r
            r1 = 17301560(0x1080038, float:2.4979412E-38)
            r0.setImageResource(r1)
            android.widget.ImageButton r0 = r10.r
            r0.setVisibility(r7)
        L_0x03c1:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.cR
            if (r0 != r8) goto L_0x03ce
            com.cyberandsons.tcmaidtrial.a.n r0 = r10.R
            java.lang.String r0 = r0.H()
            r10.a(r0)
        L_0x03ce:
            return
        L_0x03cf:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.ak
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.al
            int r1 = r1.size()
            int r1 = r1 - r8
            if (r0 != r1) goto L_0x0113
            boolean r0 = r10.U
            if (r0 != 0) goto L_0x03e5
            android.widget.ImageButton r0 = r10.t
            boolean r1 = r10.z
            r10.a(r0, r1)
        L_0x03e5:
            android.widget.ImageButton r0 = r10.v
            boolean r1 = r10.z
            r10.a(r0, r1)
            android.widget.ImageButton r0 = r10.w
            boolean r1 = r10.A
            r10.a(r0, r1)
            boolean r0 = r10.U
            if (r0 != 0) goto L_0x0113
            android.widget.ImageButton r0 = r10.u
            boolean r1 = r10.A
            r10.a(r0, r1)
            goto L_0x0113
        L_0x0400:
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            int r3 = r3 - r7
            int r4 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            int r4 = r4 - r7
            android.view.Window r5 = r10.getWindow()     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            android.graphics.drawable.BitmapDrawable r2 = com.cyberandsons.tcmaidtrial.misc.dh.a(r2, r3, r4, r5)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            android.widget.ImageView r3 = r10.e     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3.setImageDrawable(r2)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            android.widget.ImageView r2 = r10.e     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 0
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            boolean r2 = r10.z     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r10.J = r2     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            goto L_0x03a7
        L_0x041e:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x0422:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x04a2 }
            if (r1 == 0) goto L_0x03ac
            r1.close()
            goto L_0x03ac
        L_0x042b:
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.at     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r2 <= r3) goto L_0x03a7
            android.widget.ImageView r2 = r10.e     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 8
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            goto L_0x03a7
        L_0x043a:
            r2 = move-exception
        L_0x043b:
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0487 }
            java.lang.String r3 = "myVariable"
            r2.a(r3, r1)     // Catch:{ all -> 0x0487 }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0487 }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x0487 }
            java.lang.String r3 = "PD:loadUserModifiedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x0487 }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x0487 }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x0487 }
            r1.<init>(r10)     // Catch:{ all -> 0x0487 }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x0487 }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x0487 }
            r2 = 2131099674(0x7f06001a, float:1.7811708E38)
            java.lang.String r2 = r10.getString(r2)     // Catch:{ all -> 0x0487 }
            r1.setMessage(r2)     // Catch:{ all -> 0x0487 }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.er r3 = new com.cyberandsons.tcmaidtrial.detailed.er     // Catch:{ all -> 0x0487 }
            r3.<init>(r10)     // Catch:{ all -> 0x0487 }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x0487 }
            r1.show()     // Catch:{ all -> 0x0487 }
            if (r0 == 0) goto L_0x03ac
            r0.close()
            goto L_0x03ac
        L_0x047e:
            android.widget.ImageView r2 = r10.e     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            r3 = 8
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x041e, NullPointerException -> 0x043a }
            goto L_0x03a7
        L_0x0487:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x048b:
            if (r1 == 0) goto L_0x0490
            r1.close()
        L_0x0490:
            throw r0
        L_0x0491:
            android.widget.ImageButton r0 = r10.r
            r0.setVisibility(r6)
            goto L_0x03c1
        L_0x0498:
            android.widget.ImageButton r0 = r10.r
            r0.setVisibility(r6)
            goto L_0x03c1
        L_0x049f:
            r0 = move-exception
            r1 = r3
            goto L_0x048b
        L_0x04a2:
            r0 = move-exception
            goto L_0x048b
        L_0x04a4:
            r0 = move-exception
            r0 = r3
            goto L_0x043b
        L_0x04a7:
            r0 = move-exception
            r1 = r3
            goto L_0x0422
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.detailed.PointsDetail.a(boolean):void");
    }

    private void a(String str) {
        if (str != null && str.length() != 0) {
            String[] split = str.split(",");
            for (String parseInt : split) {
                switch (Integer.parseInt(parseInt)) {
                    case 1:
                        dh.a(this.f.getText());
                        break;
                    case 2:
                        dh.a(this.h.getText());
                        break;
                    case 3:
                        dh.a(this.j.getText());
                        break;
                    case 4:
                        dh.a(this.k.getText());
                        break;
                    case 5:
                        dh.a(this.l.getText());
                        break;
                    case 6:
                        dh.a(this.m.getText());
                        break;
                    case 7:
                        dh.a(this.n.getText());
                        break;
                    case 8:
                        dh.a(this.o.getText());
                        break;
                    case 9:
                        dh.a(this.p.getText());
                        break;
                    case 10:
                        dh.a(this.f432b.getText());
                        break;
                }
            }
        }
    }

    /* JADX WARN: Type inference failed for: r1v16, types: [android.database.Cursor] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:118:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x04ca  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x04de  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d() {
        /*
            r14 = this;
            r3 = 0
            r12 = 0
            r5 = 1
            com.cyberandsons.tcmaidtrial.a.n r1 = r14.R
            boolean r9 = r1.v()
            com.cyberandsons.tcmaidtrial.a.n r1 = r14.R
            int r10 = r1.ac()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.indication, main.fiveelement.item_name, main.typeofpoint.item_name, main.specificpoints1.item_name, main.specificpoints2.item_name, main.specificpoints3.item_name, main.specificpoints4.item_name, main.pointslocation.englishname, main.pointslocation.location, main.pointslocation.func, main.pointslocation.needling_method, main.pointslocation.contraindication, main.pointslocation.localapp, main.pointslocation.misc, main.pointslocation.needlingcombo, main.pointslocation.notes, main.pointslocation.image, main.pointslocation.label, main.pointslocation.row1, main.pointslocation.col1, main.pointslocation.row2, main.pointslocation.col2, main.pointslocation.row3, main.pointslocation.col3, main.pointslocation.row4, main.pointslocation.col4, main.pointslocation.row5, main.pointslocation.col5, main.pointslocation.red, main.pointslocation.green, main.pointslocation.blue, main.pointslocation.depth, main.pointslocation.scaleRow1, main.pointslocation.scaleCol1, main.pointslocation.scaleRow2, main.pointslocation.scaleCol2, main.pointslocation.scaleSize, main.pointslocation.scaleMarkers, main.pointslocation.lblRow, main.pointslocation.lblCol, main.pointslocation.common_name, main.tonemarks.ptonemarks, main.tonemarks.pjapanese, main.tonemarks.pscc, main.tonemarks.ptcc, main.pointslocation.meridian_points FROM main.pointslocation JOIN main.meridian ON main.meridian._id=main.pointslocation.meridian JOIN main.fiveelement ON main.fiveelement._id=main.pointslocation.fiveelement JOIN main.typeofpoint ON main.typeofpoint._id=main.pointslocation.typeofpoint JOIN main.specificpoints1 ON main.specificpoints1._id=main.pointslocation.specificpoint1 JOIN main.specificpoints2 ON main.specificpoints2._id=main.pointslocation.specificpoint2 JOIN main.specificpoints3 ON main.specificpoints3._id=main.pointslocation.specificpoint3 JOIN main.specificpoints4 ON main.specificpoints4._id=main.pointslocation.specificpoint4 JOIN main.tonemarks ON main.tonemarks.pid = main.pointslocation._id WHERE "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.TcmAid.ap
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r11 = r1.toString()
            boolean r1 = com.cyberandsons.tcmaidtrial.misc.dh.m     // Catch:{ SQLException -> 0x0513, NullPointerException -> 0x050f, all -> 0x0505 }
            if (r1 == 0) goto L_0x002d
            java.lang.String r1 = "PD:loadSystemSuppliedData()"
            android.util.Log.d(r1, r11)     // Catch:{ SQLException -> 0x0513, NullPointerException -> 0x050f, all -> 0x0505 }
        L_0x002d:
            android.database.sqlite.SQLiteDatabase r1 = r14.Q     // Catch:{ SQLException -> 0x0513, NullPointerException -> 0x050f, all -> 0x0505 }
            r2 = 0
            android.database.Cursor r1 = r1.rawQuery(r11, r2)     // Catch:{ SQLException -> 0x0513, NullPointerException -> 0x050f, all -> 0x0505 }
            r0 = r1
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0513, NullPointerException -> 0x050f, all -> 0x0505 }
            r8 = r0
            int r3 = r8.getCount()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r1 = com.cyberandsons.tcmaidtrial.misc.dh.m     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r1 == 0) goto L_0x0082
            java.lang.String r1 = "PD:loadSystemSuppliedData()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r4 = "query count = "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r4 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.util.Log.d(r1, r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r1 = "PD:loadSystemSuppliedData()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r4 = "column count = '"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            int r4 = r8.getColumnCount()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r4 = "' "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.util.Log.d(r1, r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
        L_0x0082:
            boolean r1 = r8.moveToFirst()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r1 == 0) goto L_0x0318
            if (r3 != r5) goto L_0x0318
            r1 = 0
            int r1 = r8.getInt(r1)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r14.S = r1     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r1 = r14.I     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r1 != 0) goto L_0x031e
            boolean r1 = r14.P     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r1 == 0) goto L_0x031e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = "SELECT main.pointslocation.common_name, main.pointslocation.image, main.pointslocation.label, main.pointslocation.row1, main.pointslocation.col1, main.pointslocation.row2, main.pointslocation.col2, main.pointslocation.row3, main.pointslocation.col3, main.pointslocation.row4, main.pointslocation.col4, main.pointslocation.row5, main.pointslocation.col5, main.pointslocation.lblRow, main.pointslocation.lblCol, main.pointslocation.red, main.pointslocation.green, main.pointslocation.blue, main.pointslocation.depth, main.pointslocation.scaleRow1, main.pointslocation.scaleCol1, main.pointslocation.scaleRow2, main.pointslocation.scaleCol2, main.pointslocation.scaleSize, main.pointslocation.scaleMarkers, main.pointslocation.meridian_points, main.pointslocation._id FROM main.pointslocation WHERE "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = " image='"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 20
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = "' "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r2 = r14.G     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r2 != 0) goto L_0x00f4
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = " AND common_name NOT LIKE '%"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 1
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = "%' OR _id="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            int r2 = r14.S     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = " GROUP BY meridian_points"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
        L_0x00f4:
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.m     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r2 == 0) goto L_0x00fd
            java.lang.String r2 = "PD:loadSystemSuppliedData_2()"
            android.util.Log.d(r2, r1)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
        L_0x00fd:
            android.database.sqlite.SQLiteDatabase r2 = r14.Q     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r3 = 0
            android.database.Cursor r2 = r2.rawQuery(r1, r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.database.sqlite.SQLiteCursor r2 = (android.database.sqlite.SQLiteCursor) r2     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            int r3 = r2.getCount()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r1 = com.cyberandsons.tcmaidtrial.misc.dh.m     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r1 == 0) goto L_0x0150
            java.lang.String r1 = "PD:loadSystemSuppliedData_2()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r4.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r5 = "query count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r5 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.util.Log.d(r1, r4)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r1 = "PD:loadSystemSuppliedData_2()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r4.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r5 = "column count = '"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            int r5 = r2.getColumnCount()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r5 = "' "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.util.Log.d(r1, r4)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
        L_0x0150:
            int r4 = r14.S     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r5 = r14.z     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r6 = r14.A     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1 = 1
            java.lang.String r7 = r8.getString(r1)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1 = r14
            com.cyberandsons.tcmaidtrial.a.b[] r1 = r1.a(r2, r3, r4, r5, r6, r7)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2.close()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
        L_0x0163:
            java.lang.String r2 = ""
            if (r9 != 0) goto L_0x0180
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r3 = 44
            java.lang.String r3 = r8.getString(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r3 = " - "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
        L_0x0180:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r3.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r3 = 45
            java.lang.String r3 = r8.getString(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.misc.ad.a(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r3 = "  "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            int r3 = r10 + 47
            java.lang.String r3 = r8.getString(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.widget.TextView r3 = r14.d     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r3.setText(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r2 = r14.H     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r2 == 0) goto L_0x04e2
            r2 = 20
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r3 = 44
            java.lang.String r3 = r8.getString(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r14.K = r3     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r3 = "points"
            android.graphics.Bitmap r3 = com.cyberandsons.tcmaidtrial.misc.dh.b(r2, r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r3 != 0) goto L_0x044b
            android.widget.ImageView r1 = r14.e     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 2130837543(0x7f020027, float:1.7280043E38)
            r1.setImageResource(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
        L_0x01d2:
            android.widget.EditText r1 = r14.f     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 12
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1.setText(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 45
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.misc.ad.a(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = "\n"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            int r2 = r10 + 47
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = "\n"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 46
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r2 != 0) goto L_0x04eb
            java.lang.String r2 = ""
        L_0x0210:
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.misc.ad.a(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.widget.TextView r2 = r14.g     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2.setText(r1)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.widget.EditText r1 = r14.h     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 11
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1.setText(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1 = 7
            java.lang.String r1 = r8.getString(r1)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 8
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r3 = 9
            java.lang.String r3 = r8.getString(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r4 = 10
            java.lang.String r4 = r8.getString(r4)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            int r5 = r2.length()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r5 <= 0) goto L_0x0260
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r5.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r5.append(r1)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r5 = "\n"
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
        L_0x0260:
            int r2 = r3.length()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r2 <= 0) goto L_0x027d
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = "\n"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
        L_0x027d:
            int r2 = r4.length()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r2 <= 0) goto L_0x029a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = "\n"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
        L_0x029a:
            android.widget.EditText r2 = r14.i     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2.setText(r1)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.widget.EditText r1 = r14.j     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 4
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1.setText(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.widget.EditText r1 = r14.k     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 13
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1.setText(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.widget.EditText r1 = r14.l     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 14
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1.setText(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.widget.EditText r1 = r14.m     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 15
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1.setText(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.widget.EditText r1 = r14.n     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 16
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1.setText(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.widget.EditText r1 = r14.o     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 17
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1.setText(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.widget.EditText r1 = r14.p     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 18
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1.setText(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.widget.EditText r1 = r14.f432b     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 19
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1.setText(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r1 = r14.T     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r1 == 0) goto L_0x0318
            com.cyberandsons.tcmaidtrial.a.n r1 = r14.R     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r1 = r1.r()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            int r2 = r14.S     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r1 = com.cyberandsons.tcmaidtrial.a.n.a(r1, r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r1 == 0) goto L_0x04f3
            android.widget.ImageButton r1 = r14.q     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.content.res.Resources r2 = r14.getResources()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r3 = 17301618(0x1080072, float:2.4979574E-38)
            android.graphics.drawable.Drawable r2 = r2.getDrawable(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1.setImageDrawable(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
        L_0x0318:
            if (r8 == 0) goto L_0x031d
            r8.close()
        L_0x031d:
            return
        L_0x031e:
            boolean r1 = r14.I     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r1 == 0) goto L_0x043b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = "SELECT main.pointslocation.common_name, main.pointslocation.image, main.pointslocation.label, main.pointslocation.row1, main.pointslocation.col1, main.pointslocation.row2, main.pointslocation.col2, main.pointslocation.row3, main.pointslocation.col3, main.pointslocation.row4, main.pointslocation.col4, main.pointslocation.row5, main.pointslocation.col5, main.pointslocation.lblRow, main.pointslocation.lblCol, main.pointslocation.red, main.pointslocation.green, main.pointslocation.blue, main.pointslocation.depth, main.pointslocation.scaleRow1, main.pointslocation.scaleCol1, main.pointslocation.scaleRow2, main.pointslocation.scaleCol2, main.pointslocation.scaleSize, main.pointslocation.scaleMarkers, main.pointslocation.meridian_points, main.pointslocation._id FROM main.pointslocation WHERE "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = " image='"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 20
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = "' "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r2 = ""
            boolean r3 = r14.G     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r3 != 0) goto L_0x0517
            boolean r3 = r14.P     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r3 == 0) goto L_0x0416
            r2 = 1
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r3.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r3 = " AND common_name LIKE '%"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r3 = 1
            java.lang.String r3 = r8.getString(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r3 = "%'"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r3 = " OR (image='"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r3 = 20
            java.lang.String r3 = r8.getString(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r3 = "' AND common_name NOT LIKE '%"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r3 = 1
            java.lang.String r3 = r8.getString(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r3 = "%')"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r7 = r2
        L_0x039e:
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.m     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r2 == 0) goto L_0x03a7
            java.lang.String r2 = "PD:loadSystemSuppliedData_2()"
            android.util.Log.d(r2, r1)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
        L_0x03a7:
            android.database.sqlite.SQLiteDatabase r2 = r14.Q     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r3 = 0
            android.database.Cursor r2 = r2.rawQuery(r1, r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.database.sqlite.SQLiteCursor r2 = (android.database.sqlite.SQLiteCursor) r2     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            int r3 = r2.getCount()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r1 = com.cyberandsons.tcmaidtrial.misc.dh.m     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r1 == 0) goto L_0x03fa
            java.lang.String r1 = "PD:loadSystemSuppliedData_2()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r4.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r5 = "query count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r5 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.util.Log.d(r1, r4)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r1 = "PD:loadSystemSuppliedData_2()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r4.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r5 = "column count = '"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            int r5 = r2.getColumnCount()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r5 = "' "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.util.Log.d(r1, r4)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
        L_0x03fa:
            int r4 = r14.S     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r5 = r14.z     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r6 = r14.z     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1 = r14
            com.cyberandsons.tcmaidtrial.a.b[] r1 = r1.a(r2, r3, r4, r5, r6, r7)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2.close()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            goto L_0x0163
        L_0x040a:
            r1 = move-exception
            r2 = r8
        L_0x040c:
            com.cyberandsons.tcmaidtrial.a.q.a(r1)     // Catch:{ all -> 0x0508 }
            if (r2 == 0) goto L_0x031d
            r2.close()
            goto L_0x031d
        L_0x0416:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r3.<init>()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r3 = " AND common_name LIKE '%"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r3 = 1
            java.lang.String r3 = r8.getString(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r3 = "%'"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r7 = r2
            goto L_0x039e
        L_0x043b:
            int r4 = r14.S     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r5 = r14.A     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r6 = r14.A     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            java.lang.String r7 = ""
            r1 = r14
            r2 = r8
            com.cyberandsons.tcmaidtrial.a.b[] r1 = r1.a(r2, r3, r4, r5, r6, r7)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            goto L_0x0163
        L_0x044b:
            com.cyberandsons.tcmaidtrial.draw.a r4 = new com.cyberandsons.tcmaidtrial.draw.a     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            int r5 = r3.getHeight()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            int r6 = r3.getWidth()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r4.<init>(r14, r3, r5, r6)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r3 = r14.I     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r3 != 0) goto L_0x0464
            boolean r3 = r14.I     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r3 != 0) goto L_0x04cf
            boolean r3 = r14.P     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            if (r3 == 0) goto L_0x04cf
        L_0x0464:
            int r3 = r14.S     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r5 = r14.M     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r4.a(r3, r2, r1, r5)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
        L_0x046b:
            android.graphics.Bitmap r1 = r4.a()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            int r2 = r2 - r12
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            int r3 = r3 - r12
            android.view.Window r4 = r14.getWindow()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.graphics.drawable.BitmapDrawable r1 = com.cyberandsons.tcmaidtrial.misc.dh.a(r1, r2, r3, r4)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.widget.ImageView r2 = r14.e     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2.setImageDrawable(r1)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.widget.ImageView r1 = r14.e     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 0
            r1.setVisibility(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            goto L_0x01d2
        L_0x048a:
            r1 = move-exception
            r1 = r8
        L_0x048c:
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x050a }
            java.lang.String r3 = "myVariable"
            r2.a(r3, r11)     // Catch:{ all -> 0x050a }
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x050a }
            java.lang.NullPointerException r3 = new java.lang.NullPointerException     // Catch:{ all -> 0x050a }
            java.lang.String r4 = "PD:loadSystemSuppliedData()"
            r3.<init>(r4)     // Catch:{ all -> 0x050a }
            r2.handleSilentException(r3)     // Catch:{ all -> 0x050a }
            android.app.AlertDialog$Builder r2 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x050a }
            r2.<init>(r14)     // Catch:{ all -> 0x050a }
            android.app.AlertDialog r2 = r2.create()     // Catch:{ all -> 0x050a }
            java.lang.String r3 = "Attention:"
            r2.setTitle(r3)     // Catch:{ all -> 0x050a }
            r3 = 2131099673(0x7f060019, float:1.7811706E38)
            java.lang.String r3 = r14.getString(r3)     // Catch:{ all -> 0x050a }
            r2.setMessage(r3)     // Catch:{ all -> 0x050a }
            java.lang.String r3 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.et r4 = new com.cyberandsons.tcmaidtrial.detailed.et     // Catch:{ all -> 0x050a }
            r4.<init>(r14)     // Catch:{ all -> 0x050a }
            r2.setButton(r3, r4)     // Catch:{ all -> 0x050a }
            r2.show()     // Catch:{ all -> 0x050a }
            if (r1 == 0) goto L_0x031d
            r1.close()
            goto L_0x031d
        L_0x04cf:
            r3 = 0
            r1 = r1[r3]     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r3 = r14.L     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            boolean r5 = r14.M     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r4.a(r2, r1, r3, r5)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            goto L_0x046b
        L_0x04da:
            r1 = move-exception
            r2 = r8
        L_0x04dc:
            if (r2 == 0) goto L_0x04e1
            r2.close()
        L_0x04e1:
            throw r1
        L_0x04e2:
            android.widget.ImageView r1 = r14.e     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r2 = 8
            r1.setVisibility(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            goto L_0x01d2
        L_0x04eb:
            r2 = 46
            java.lang.String r2 = r8.getString(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            goto L_0x0210
        L_0x04f3:
            android.widget.ImageButton r1 = r14.q     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            android.content.res.Resources r2 = r14.getResources()     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r3 = 17301619(0x1080073, float:2.4979577E-38)
            android.graphics.drawable.Drawable r2 = r2.getDrawable(r3)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            r1.setImageDrawable(r2)     // Catch:{ SQLException -> 0x040a, NullPointerException -> 0x048a, all -> 0x04da }
            goto L_0x0318
        L_0x0505:
            r1 = move-exception
            r2 = r3
            goto L_0x04dc
        L_0x0508:
            r1 = move-exception
            goto L_0x04dc
        L_0x050a:
            r2 = move-exception
            r13 = r2
            r2 = r1
            r1 = r13
            goto L_0x04dc
        L_0x050f:
            r1 = move-exception
            r1 = r3
            goto L_0x048c
        L_0x0513:
            r1 = move-exception
            r2 = r3
            goto L_0x040c
        L_0x0517:
            r7 = r2
            goto L_0x039e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.detailed.PointsDetail.d():void");
    }

    private b[] a(SQLiteCursor sQLiteCursor, int i2, int i3, boolean z2, boolean z3, String str) {
        boolean t2 = this.R.t();
        b[] bVarArr = new b[i2];
        sQLiteCursor.moveToFirst();
        if (dh.m) {
            Log.d("PD:buildPointCorrdinates()", "query count = " + Integer.toString(i2));
            Log.d("PD:buildPointCorrdinates()", "column count = '" + Integer.toString(sQLiteCursor.getColumnCount()) + "' ");
        }
        for (int i4 = 0; i4 < i2; i4++) {
            bVarArr[i4] = new b();
            if (i2 != 1 || z2) {
                bVarArr[i4].f163a = sQLiteCursor.getInt(26);
                bVarArr[i4].f164b = null;
                bVarArr[i4].c = sQLiteCursor.getString(0);
                bVarArr[i4].d = sQLiteCursor.getString(1);
                bVarArr[i4].e = sQLiteCursor.getString(2);
                bVarArr[i4].f = sQLiteCursor.getInt(3);
                bVarArr[i4].g = sQLiteCursor.getInt(4);
                bVarArr[i4].h = sQLiteCursor.getInt(5);
                bVarArr[i4].i = sQLiteCursor.getInt(6);
                bVarArr[i4].j = sQLiteCursor.getInt(7);
                bVarArr[i4].k = sQLiteCursor.getInt(8);
                bVarArr[i4].l = sQLiteCursor.getInt(9);
                bVarArr[i4].m = sQLiteCursor.getInt(10);
                bVarArr[i4].n = sQLiteCursor.getInt(11);
                bVarArr[i4].o = sQLiteCursor.getInt(12);
                bVarArr[i4].A = sQLiteCursor.getFloat(15) == 1.0f ? 255 : 0;
                bVarArr[i4].B = sQLiteCursor.getFloat(16) == 1.0f ? 128 : 0;
                bVarArr[i4].C = sQLiteCursor.getFloat(7) == 1.0f ? 255 : 0;
                bVarArr[i4].x = sQLiteCursor.getString(18);
                bVarArr[i4].s = sQLiteCursor.getInt(19);
                bVarArr[i4].r = sQLiteCursor.getInt(20);
                bVarArr[i4].u = sQLiteCursor.getInt(21);
                bVarArr[i4].t = sQLiteCursor.getInt(22);
                bVarArr[i4].v = sQLiteCursor.getInt(23);
                bVarArr[i4].w = sQLiteCursor.getInt(24);
                bVarArr[i4].p = sQLiteCursor.getInt(13);
                bVarArr[i4].q = sQLiteCursor.getInt(14);
                bVarArr[i4].y = -1;
                bVarArr[i4].z = null;
                bVarArr[i4].D.clear();
                if (this.O && bVarArr[i4].f163a == i3 && sQLiteCursor.getString(25).length() > 0) {
                    String[] split = sQLiteCursor.getString(25).split(",");
                    int i5 = 0;
                    while (i5 < split.length) {
                        int i6 = i5 + 1;
                        bVarArr[i4].D.add(new Point(Integer.parseInt(split[i5]), Integer.parseInt(split[i6])));
                        i5 = i6 + 1;
                    }
                }
                if (this.P && bVarArr[i4].f163a != i3) {
                    if (!z3) {
                        bVarArr[i4].f163a = -2;
                    } else if (!z3 || bVarArr[i4].c.indexOf(str) != -1) {
                        bVarArr[i4].f163a = sQLiteCursor.getInt(26);
                    } else {
                        bVarArr[i4].f163a = -2;
                    }
                    if (sQLiteCursor.getString(25).length() > 0) {
                        String[] split2 = sQLiteCursor.getString(25).split(",");
                        int i7 = 0;
                        while (i7 < split2.length) {
                            int i8 = i7 + 1;
                            bVarArr[i4].D.add(new Point(Integer.parseInt(split2[i7]), Integer.parseInt(split2[i8])));
                            i7 = i8 + 1;
                        }
                    }
                }
            } else {
                bVarArr[i4].f163a = sQLiteCursor.getInt(0);
                bVarArr[i4].f164b = null;
                bVarArr[i4].c = sQLiteCursor.getString(44);
                bVarArr[i4].d = sQLiteCursor.getString(20);
                bVarArr[i4].e = sQLiteCursor.getString(21);
                bVarArr[i4].f = sQLiteCursor.getInt(22);
                bVarArr[i4].g = sQLiteCursor.getInt(23);
                bVarArr[i4].h = sQLiteCursor.getInt(24);
                bVarArr[i4].i = sQLiteCursor.getInt(25);
                bVarArr[i4].j = sQLiteCursor.getInt(26);
                bVarArr[i4].k = sQLiteCursor.getInt(27);
                bVarArr[i4].l = sQLiteCursor.getInt(28);
                bVarArr[i4].m = sQLiteCursor.getInt(29);
                bVarArr[i4].n = sQLiteCursor.getInt(30);
                bVarArr[i4].o = sQLiteCursor.getInt(31);
                bVarArr[i4].A = sQLiteCursor.getFloat(32) == 1.0f ? 255 : 0;
                bVarArr[i4].B = sQLiteCursor.getFloat(33) == 1.0f ? 128 : 0;
                bVarArr[i4].C = sQLiteCursor.getFloat(34) == 1.0f ? 255 : 0;
                bVarArr[i4].x = sQLiteCursor.getString(35);
                bVarArr[i4].s = sQLiteCursor.getInt(36);
                bVarArr[i4].r = sQLiteCursor.getInt(37);
                bVarArr[i4].u = sQLiteCursor.getInt(38);
                bVarArr[i4].t = sQLiteCursor.getInt(39);
                bVarArr[i4].v = sQLiteCursor.getInt(40);
                bVarArr[i4].w = sQLiteCursor.getInt(41);
                bVarArr[i4].p = sQLiteCursor.getInt(42);
                bVarArr[i4].q = sQLiteCursor.getInt(43);
                bVarArr[i4].y = -1;
                bVarArr[i4].z = null;
                bVarArr[i4].D.clear();
                if (this.O && sQLiteCursor.getString(49).length() > 0) {
                    String[] split3 = sQLiteCursor.getString(49).split(",");
                    int i9 = 0;
                    while (i9 < split3.length) {
                        int i10 = i9 + 1;
                        bVarArr[i4].D.add(new Point(Integer.parseInt(split3[i9]), Integer.parseInt(split3[i10])));
                        i9 = i10 + 1;
                    }
                }
            }
            sQLiteCursor.moveToNext();
            if (!t2) {
                bVarArr[i4].A = 0;
                bVarArr[i4].B = 0;
                bVarArr[i4].C = 0;
            }
        }
        sQLiteCursor.moveToFirst();
        return bVarArr;
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        boolean z2 = this.A;
        switch (i2) {
            case 0:
                if (this.F) {
                    this.D -= 2;
                } else {
                    TcmAid.ak = 0;
                    if (!this.U) {
                        a(this.t, this.A);
                    }
                    a(this.v, this.A);
                    a(this.w, this.z);
                    if (!this.U) {
                        a(this.u, this.z);
                    }
                }
                z2 = this.z;
                break;
            case 1:
                if (this.F) {
                    this.E -= 2;
                } else if (TcmAid.ak - 1 >= 0) {
                    if (TcmAid.ak - 1 == 0) {
                        if (!this.U) {
                            a(this.t, this.A);
                        }
                        a(this.v, this.A);
                    }
                    if (this.w.isEnabled() == this.A) {
                        a(this.w, this.z);
                        if (!this.U) {
                            a(this.u, this.z);
                        }
                    }
                    TcmAid.ak--;
                }
                z2 = this.z;
                break;
            case 2:
                if (!this.F) {
                    int size = TcmAid.al.size();
                    if (TcmAid.ak + 1 < size) {
                        if (TcmAid.ak + 1 == size - 1) {
                            a(this.w, this.A);
                            if (!this.U) {
                                a(this.u, this.A);
                            }
                        }
                        if (this.v.isEnabled() == this.A) {
                            if (!this.U) {
                                a(this.t, this.z);
                            }
                            a(this.v, this.z);
                        }
                        TcmAid.ak++;
                        z2 = this.z;
                        break;
                    }
                } else {
                    this.E += 2;
                    z2 = this.z;
                    break;
                }
                break;
            case 3:
                if (this.F) {
                    this.D += 2;
                } else {
                    TcmAid.ak = TcmAid.al.size() - 1;
                    if (!this.U) {
                        a(this.t, this.z);
                    }
                    a(this.v, this.z);
                    a(this.w, this.A);
                    if (!this.U) {
                        a(this.u, this.A);
                    }
                }
                z2 = this.z;
                break;
        }
        if (z2) {
            if (TcmAid.ak >= TcmAid.al.size()) {
                TcmAid.ak = TcmAid.al.size() - 1;
            }
            int intValue = ((Integer) TcmAid.al.get(TcmAid.ak)).intValue();
            TcmAid.ap = "main.pointslocation._id=" + Integer.toString(intValue);
            TcmAid.at = intValue;
            a(false);
            this.f431a.post(new ex(this));
        }
    }

    private void a(ImageButton imageButton, boolean z2) {
        if (z2) {
            imageButton.setEnabled(this.z);
            imageButton.setVisibility(0);
            return;
        }
        imageButton.setEnabled(this.A);
        imageButton.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        boolean z2;
        String str;
        if (this.N) {
            this.N = this.A;
            return;
        }
        String[] split = this.p.getText().toString().split(" ");
        String[] strArr = {"BL", "DU", "GB", "HT", "KI", "LI", "LV", "LU", "PC", "REN", "SI", "SJ", "SP", "ST", "TB", "TW", "M-HN", "N-HN", "M-BW", "N-BW", "M-CA", "N-CA", "M-UE", "N-UE", "M-LE", "N-LE"};
        StringBuffer stringBuffer = new StringBuffer();
        boolean z3 = true;
        for (String trim : split) {
            String trim2 = trim.trim();
            int i2 = 0;
            while (true) {
                if (i2 >= strArr.length) {
                    z2 = false;
                    break;
                } else if (trim2.indexOf(strArr[i2]) != -1) {
                    if (trim2.indexOf(",", 0) > 0) {
                        trim2 = trim2.replace(",", "");
                    }
                    int indexOf = trim2.indexOf("\n");
                    if (indexOf > 0) {
                        trim2 = trim2.substring(0, indexOf);
                        z2 = true;
                    } else {
                        z2 = true;
                    }
                } else {
                    i2++;
                }
            }
            if (z2) {
                if (dh.M) {
                    Log.d("PD:tok", trim2);
                }
                if (z3) {
                    str = " WHERE ";
                } else {
                    str = " OR ";
                }
                stringBuffer.append(str + " xxxx.pointslocation.common_name LIKE '" + trim2 + "' OR xxxx.pointslocation.name LIKE '" + trim2 + "' ");
                if (dh.M) {
                    Log.d("PD:pointItems", stringBuffer.toString());
                }
                z3 = false;
            }
        }
        TcmAid.ar = a.a(stringBuffer.toString().replace("xxxx", "main"), stringBuffer.toString().replace("xxxx", "userDB").replace("WHERE  userDB", "WHERE (userDB") + ')', this.R);
        TcmAid.ap = null;
        startActivityForResult(new Intent(this, PointsList.class), 1350);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        e a2 = e.a(motionEvent);
        ImageView imageView = (ImageView) view;
        StringBuilder sb = new StringBuilder();
        int b2 = a2.b();
        int i2 = b2 & 255;
        sb.append("event ACTION_").append(new String[]{"DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE", "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?"}[i2]);
        if (i2 == 5 || i2 == 6) {
            sb.append("(pid ").append(b2 >> 8);
            sb.append(')');
        }
        sb.append('[');
        for (int i3 = 0; i3 < a2.a(); i3++) {
            sb.append('#').append(i3);
            sb.append("(pid ").append(a2.c(i3));
            sb.append(")=").append((int) a2.a(i3));
            sb.append(',').append((int) a2.b(i3));
            if (i3 + 1 < a2.a()) {
                sb.append(';');
            }
        }
        sb.append(']');
        Log.d("PD:dumpEvent()", sb.toString());
        switch (a2.b() & 255) {
            case 0:
                this.aa.set(this.Z);
                this.ac.set(a2.c(), a2.d());
                Log.d("PD:onTouch()", "mode=DRAG");
                this.ab = 1;
                break;
            case 1:
            case 6:
                this.ab = 0;
                Log.d("PD:onTouch()", "mode=NONE");
                break;
            case 2:
                if (this.ab != 1) {
                    if (this.ab == 2) {
                        float a3 = a(a2);
                        Log.d("PD:onTouch()", "newDist=" + a3);
                        if (a3 > 10.0f) {
                            this.Z.set(this.aa);
                            float f2 = a3 / this.ae;
                            this.Z.postScale(f2, f2, this.ad.x, this.ad.y);
                            break;
                        }
                    }
                } else {
                    this.Z.set(this.aa);
                    this.Z.postTranslate(a2.c() - this.ac.x, a2.d() - this.ac.y);
                    break;
                }
                break;
            case 5:
                this.ae = a(a2);
                Log.d("PD:onTouch()", "oldDist=" + this.ae);
                if (this.ae > 10.0f) {
                    this.aa.set(this.Z);
                    this.ad.set((a2.a(0) + a2.a(1)) / 2.0f, (a2.b(1) + a2.b(0)) / 2.0f);
                    this.ab = 2;
                    Log.d("PD:onTouch()", "mode=ZOOM");
                    break;
                }
                break;
        }
        imageView.setImageMatrix(this.Z);
        return true;
    }

    private static float a(e eVar) {
        float a2 = eVar.a(0) - eVar.a(1);
        float b2 = eVar.b(0) - eVar.b(1);
        return FloatMath.sqrt((a2 * a2) + (b2 * b2));
    }

    public void onClick(View view) {
    }
}
