package com.cyberandsons.tcmaidtrial.detailed;

import android.app.Activity;
import android.app.AlertDialog;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.b;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.draw.a;
import com.cyberandsons.tcmaidtrial.misc.ad;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.util.ArrayList;

public class PointsDetailMB extends Activity {
    public static ArrayList g = new ArrayList();
    private ImageButton A;
    private boolean B;
    private boolean C;
    private boolean D;
    private boolean E;
    private boolean F;
    private boolean G;
    private boolean H;
    /* access modifiers changed from: private */
    public SQLiteDatabase I;
    private n J;
    private b[] K;
    private int L;
    private boolean M;
    /* access modifiers changed from: private */
    public boolean N;
    /* access modifiers changed from: private */
    public int O;
    private int P;
    /* access modifiers changed from: private */
    public int Q;
    /* access modifiers changed from: private */
    public int R;
    /* access modifiers changed from: private */
    public int S;

    /* renamed from: a  reason: collision with root package name */
    String f433a;

    /* renamed from: b  reason: collision with root package name */
    String f434b;
    String c;
    String d;
    boolean e = true;
    boolean f;
    private ScrollView h;
    private TextView i;
    private ImageView j;
    private TextView k;
    private String l;
    private ImageButton m;
    private ImageButton n;
    private ImageButton o;
    private ImageButton p;
    private ImageButton q;
    private ImageButton r;
    private ImageButton s;
    private ImageButton t;
    private ImageButton u;
    private ImageButton v;
    private ImageButton w;
    private ImageButton x;
    private ImageButton y;
    private ImageButton z;

    public PointsDetailMB() {
        this.f = !this.e;
        this.B = this.f;
        this.C = this.f;
        this.D = this.e;
        this.E = this.e;
        this.F = this.e;
        this.G = this.f;
        this.H = this.f;
        this.M = true;
        this.N = false;
        this.O = 1;
        this.P = 1;
        this.Q = 10;
        this.R = 120;
        this.S = 120;
    }

    static /* synthetic */ int a(PointsDetailMB pointsDetailMB, int i2) {
        int i3 = pointsDetailMB.S - i2;
        pointsDetailMB.S = i3;
        return i3;
    }

    static /* synthetic */ int b(PointsDetailMB pointsDetailMB, int i2) {
        int i3 = pointsDetailMB.R - i2;
        pointsDetailMB.R = i3;
        return i3;
    }

    static /* synthetic */ void b(PointsDetailMB pointsDetailMB) {
        String str;
        pointsDetailMB.i.setText(String.format("%s\nrow=%d, col=%d, inc=%d", pointsDetailMB.l, Integer.valueOf(pointsDetailMB.R), Integer.valueOf(pointsDetailMB.S), Integer.valueOf(pointsDetailMB.Q)));
        int size = g.size();
        if (pointsDetailMB.O == 0) {
            str = "S";
            g.clear();
            ArrayList arrayList = g;
            int i2 = pointsDetailMB.O;
            pointsDetailMB.O = i2 + 1;
            arrayList.add(i2, new Point(pointsDetailMB.R, pointsDetailMB.S));
            g.add(pointsDetailMB.O, new Point(pointsDetailMB.R, pointsDetailMB.S));
            pointsDetailMB.P = pointsDetailMB.O;
        } else if (pointsDetailMB.O != pointsDetailMB.P) {
            if (pointsDetailMB.N) {
                str = "E";
            } else {
                ArrayList arrayList2 = g;
                int i3 = pointsDetailMB.O;
                pointsDetailMB.O = i3 + 1;
                arrayList2.add(i3, (Point) g.get(pointsDetailMB.P));
                g.add(pointsDetailMB.O, new Point(pointsDetailMB.R, pointsDetailMB.S));
                str = "P";
            }
            pointsDetailMB.P = pointsDetailMB.O;
        } else {
            str = "N";
            g.remove(pointsDetailMB.O);
            g.add(pointsDetailMB.O, new Point(pointsDetailMB.R, pointsDetailMB.S));
        }
        Log.i("points", String.format("%s sz = %d, currPoint = %d, row = %d, col = %d", str, Integer.valueOf(size), Integer.valueOf(pointsDetailMB.O), Integer.valueOf(pointsDetailMB.R), Integer.valueOf(pointsDetailMB.S)));
        pointsDetailMB.K[0].D = g;
        Bitmap b2 = dh.b(pointsDetailMB.f433a, "points");
        if (b2 == null) {
            pointsDetailMB.j.setImageResource(C0000R.drawable.nopicture);
            return;
        }
        a aVar = new a(pointsDetailMB, b2, b2.getHeight(), b2.getWidth());
        aVar.a(pointsDetailMB.L, pointsDetailMB.f433a, pointsDetailMB.K, pointsDetailMB.F);
        pointsDetailMB.j.setImageDrawable(dh.a(aVar.a(), TcmAid.cS - 0, TcmAid.cS - 0, pointsDetailMB.getWindow()));
    }

    static /* synthetic */ int c(PointsDetailMB pointsDetailMB, int i2) {
        int i3 = pointsDetailMB.R + i2;
        pointsDetailMB.R = i3;
        return i3;
    }

    static /* synthetic */ int d(PointsDetailMB pointsDetailMB) {
        int i2 = pointsDetailMB.O;
        pointsDetailMB.O = i2 + 1;
        return i2;
    }

    static /* synthetic */ int d(PointsDetailMB pointsDetailMB, int i2) {
        int i3 = pointsDetailMB.S + i2;
        pointsDetailMB.S = i3;
        return i3;
    }

    static /* synthetic */ int e(PointsDetailMB pointsDetailMB) {
        int i2 = pointsDetailMB.Q;
        pointsDetailMB.Q = i2 + 1;
        return i2;
    }

    static /* synthetic */ int f(PointsDetailMB pointsDetailMB) {
        int i2 = pointsDetailMB.Q;
        pointsDetailMB.Q = i2 - 1;
        return i2;
    }

    static /* synthetic */ int h(PointsDetailMB pointsDetailMB) {
        int i2 = pointsDetailMB.O - 1;
        pointsDetailMB.O = i2;
        return i2;
    }

    public void onCreate(Bundle bundle) {
        SQLiteCursor sQLiteCursor;
        super.onCreate(bundle);
        if (this.I == null || !this.I.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("PD:openDataBase()", str + " opened.");
                this.I = SQLiteDatabase.openDatabase(str, null, 16);
                this.I.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.I.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("PD:openDataBase()", str2 + " ATTACHed.");
                this.J = new n();
                this.J.a(this.I);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new hz(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        this.C = this.J.b();
        this.F = this.J.C();
        this.B = this.J.m();
        this.H = this.J.ad();
        setContentView((int) C0000R.layout.points_detailed_mb);
        this.i = (TextView) findViewById(C0000R.id.tce_label);
        this.h = (ScrollView) findViewById(C0000R.id.svDetail);
        getWindow().setSoftInputMode(3);
        if (this.M) {
            this.O = 1;
            g.add(0, new Point(this.R, this.S));
            g.add(1, new Point(this.R, this.S));
        }
        this.m = (ImageButton) findViewById(C0000R.id.left);
        this.m.setOnClickListener(new cc(this));
        this.n = (ImageButton) findViewById(C0000R.id.up);
        this.n.setOnClickListener(new cb(this));
        this.q = (ImageButton) findViewById(C0000R.id.upleft);
        this.q.setOnClickListener(new ca(this));
        this.r = (ImageButton) findViewById(C0000R.id.downleft);
        this.r.setOnClickListener(new bz(this));
        this.o = (ImageButton) findViewById(C0000R.id.down);
        this.o.setOnClickListener(new ce(this));
        this.p = (ImageButton) findViewById(C0000R.id.right);
        this.p.setOnClickListener(new cf(this));
        this.s = (ImageButton) findViewById(C0000R.id.upright);
        this.s.setOnClickListener(new cg(this));
        this.t = (ImageButton) findViewById(C0000R.id.downright);
        this.t.setOnClickListener(new ch(this));
        this.x = (ImageButton) findViewById(C0000R.id.start);
        this.x.setOnClickListener(new ci(this));
        this.y = (ImageButton) findViewById(C0000R.id.point);
        this.y.setOnClickListener(new hx(this));
        this.u = (ImageButton) findViewById(C0000R.id.increment);
        this.u.setOnClickListener(new hy(this));
        this.v = (ImageButton) findViewById(C0000R.id.decrement);
        this.v.setOnClickListener(new hv(this));
        this.w = (ImageButton) findViewById(C0000R.id.newline);
        this.w.setOnClickListener(new hw(this));
        this.z = (ImageButton) findViewById(C0000R.id.end);
        this.z.setOnClickListener(new ia(this));
        this.A = (ImageButton) findViewById(C0000R.id.save);
        this.A.setOnClickListener(new ib(this));
        this.j = (ImageView) findViewById(C0000R.id.p_image);
        if (this.k != null) {
            this.k = null;
        }
        this.k = (TextView) findViewById(C0000R.id.p_name);
        boolean v2 = this.J.v();
        String str3 = "SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.indication, main.fiveelement.item_name, main.typeofpoint.item_name, main.specificpoints1.item_name, main.specificpoints2.item_name, main.specificpoints3.item_name, main.specificpoints4.item_name, main.pointslocation.englishname, main.pointslocation.location, main.pointslocation.func, main.pointslocation.needling_method, main.pointslocation.contraindication, main.pointslocation.localapp, main.pointslocation.misc, main.pointslocation.needlingcombo, main.pointslocation.notes, main.pointslocation.image, main.pointslocation.label, main.pointslocation.row1, main.pointslocation.col1, main.pointslocation.row2, main.pointslocation.col2, main.pointslocation.row3, main.pointslocation.col3, main.pointslocation.row4, main.pointslocation.col4, main.pointslocation.row5, main.pointslocation.col5, main.pointslocation.red, main.pointslocation.green, main.pointslocation.blue, main.pointslocation.depth, main.pointslocation.scaleRow1, main.pointslocation.scaleCol1, main.pointslocation.scaleRow2, main.pointslocation.scaleCol2, main.pointslocation.scaleSize, main.pointslocation.scaleMarkers, main.pointslocation.lblRow, main.pointslocation.lblCol, main.pointslocation.common_name, main.tonemarks.ptonemarks, main.tonemarks.pjapanese, main.tonemarks.pscc, main.tonemarks.ptcc, main.pointslocation.meridian_points FROM main.pointslocation JOIN main.meridian ON main.meridian._id=main.pointslocation.meridian JOIN main.fiveelement ON main.fiveelement._id=main.pointslocation.fiveelement JOIN main.typeofpoint ON main.typeofpoint._id=main.pointslocation.typeofpoint JOIN main.specificpoints1 ON main.specificpoints1._id=main.pointslocation.specificpoint1 JOIN main.specificpoints2 ON main.specificpoints2._id=main.pointslocation.specificpoint2 JOIN main.specificpoints3 ON main.specificpoints3._id=main.pointslocation.specificpoint3 JOIN main.specificpoints4 ON main.specificpoints4._id=main.pointslocation.specificpoint4 JOIN main.tonemarks ON main.tonemarks.pid = main.pointslocation._id WHERE " + TcmAid.ap;
        try {
            if (dh.m) {
                Log.d("PD:loadSystemSuppliedData()", str3);
            }
            SQLiteCursor sQLiteCursor2 = (SQLiteCursor) this.I.rawQuery(str3, null);
            try {
                int count = sQLiteCursor2.getCount();
                if (dh.m) {
                    Log.d("PD:loadSystemSuppliedData()", "query count = " + Integer.toString(count));
                    Log.d("PD:loadSystemSuppliedData()", "column count = '" + Integer.toString(sQLiteCursor2.getColumnCount()) + "' ");
                }
                if (sQLiteCursor2.moveToFirst() && count == 1) {
                    this.c = sQLiteCursor2.getString(1);
                    SQLiteCursor sQLiteCursor3 = (SQLiteCursor) this.I.rawQuery(("SELECT main.pointslocation.common_name, main.pointslocation.image, main.pointslocation.label, main.pointslocation.row1, main.pointslocation.col1, main.pointslocation.row2, main.pointslocation.col2, main.pointslocation.row3, main.pointslocation.col3, main.pointslocation.row4, main.pointslocation.col4, main.pointslocation.row5, main.pointslocation.col5, main.pointslocation.lblRow, main.pointslocation.lblCol, main.pointslocation.red, main.pointslocation.green, main.pointslocation.blue, main.pointslocation.depth, main.pointslocation.scaleRow1, main.pointslocation.scaleCol1, main.pointslocation.scaleRow2, main.pointslocation.scaleCol2, main.pointslocation.scaleSize, main.pointslocation.scaleMarkers, main.pointslocation.meridian_points, main.pointslocation._id FROM main.pointslocation WHERE " + " image='" + sQLiteCursor2.getString(20) + "' ") + " AND common_name like '%" + this.c + "%'", null);
                    this.K = a(sQLiteCursor3, sQLiteCursor3.getCount(), this.e);
                    sQLiteCursor3.close();
                    this.L = sQLiteCursor2.getInt(0);
                    this.l = "";
                    if (!v2) {
                        this.l = sQLiteCursor2.getString(44) + " - ";
                    }
                    this.l += ad.a(sQLiteCursor2.getString(45)) + "  " + sQLiteCursor2.getString(47);
                    this.i.setText(this.l);
                    this.f433a = sQLiteCursor2.getString(20);
                    Bitmap b2 = dh.b(this.f433a, "points");
                    if (b2 == null) {
                        this.j.setImageResource(C0000R.drawable.nopicture);
                    } else {
                        a aVar = new a(this, b2, b2.getHeight(), b2.getWidth());
                        aVar.a(this.L, this.f433a, this.K, this.F);
                        this.j.setImageDrawable(dh.a(aVar.a(), TcmAid.cS - 0, TcmAid.cS - 0, getWindow()));
                    }
                    this.f434b = sQLiteCursor2.getString(1);
                }
                sQLiteCursor2.close();
            } catch (SQLException e3) {
                SQLException sQLException = e3;
                sQLiteCursor = sQLiteCursor2;
                e = sQLException;
            } catch (Throwable th) {
                Throwable th2 = th;
                sQLiteCursor = sQLiteCursor2;
                th = th2;
                sQLiteCursor.close();
                throw th;
            }
        } catch (SQLException e4) {
            e = e4;
            sQLiteCursor = null;
            try {
                q.a(e);
                sQLiteCursor.close();
                this.h.smoothScrollBy(0, 0);
            } catch (Throwable th3) {
                th = th3;
                sQLiteCursor.close();
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            sQLiteCursor = null;
            sQLiteCursor.close();
            throw th;
        }
        this.h.smoothScrollBy(0, 0);
    }

    public void onDestroy() {
        try {
            if (this.I != null && this.I.isOpen()) {
                this.I.close();
                this.I = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            this.G = this.e;
            TcmAid.bl = this.e;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    private b[] a(SQLiteCursor sQLiteCursor, int i2, boolean z2) {
        boolean t2 = this.J.t();
        b[] bVarArr = new b[i2];
        sQLiteCursor.moveToFirst();
        if (dh.m) {
            Log.d("PD:buildPointCorrdinates()", "query count = " + Integer.toString(i2));
            Log.d("PD:buildPointCorrdinates()", "column count = '" + Integer.toString(sQLiteCursor.getColumnCount()) + "' ");
        }
        for (int i3 = 0; i3 < i2; i3++) {
            bVarArr[i3] = new b();
            if (i2 != 1 || z2) {
                bVarArr[i3].f163a = sQLiteCursor.getInt(26);
                bVarArr[i3].f164b = null;
                bVarArr[i3].c = sQLiteCursor.getString(0);
                bVarArr[i3].d = sQLiteCursor.getString(1);
                bVarArr[i3].e = sQLiteCursor.getString(2);
                bVarArr[i3].f = sQLiteCursor.getInt(3);
                bVarArr[i3].g = sQLiteCursor.getInt(4);
                bVarArr[i3].h = sQLiteCursor.getInt(5);
                bVarArr[i3].i = sQLiteCursor.getInt(6);
                bVarArr[i3].j = sQLiteCursor.getInt(7);
                bVarArr[i3].k = sQLiteCursor.getInt(8);
                bVarArr[i3].l = sQLiteCursor.getInt(9);
                bVarArr[i3].m = sQLiteCursor.getInt(10);
                bVarArr[i3].n = sQLiteCursor.getInt(11);
                bVarArr[i3].o = sQLiteCursor.getInt(12);
                bVarArr[i3].A = sQLiteCursor.getFloat(15) == 1.0f ? 255 : 0;
                bVarArr[i3].B = sQLiteCursor.getFloat(16) == 1.0f ? 128 : 0;
                bVarArr[i3].C = sQLiteCursor.getFloat(7) == 1.0f ? 255 : 0;
                bVarArr[i3].x = sQLiteCursor.getString(18);
                bVarArr[i3].s = sQLiteCursor.getInt(19);
                bVarArr[i3].r = sQLiteCursor.getInt(20);
                bVarArr[i3].u = sQLiteCursor.getInt(21);
                bVarArr[i3].t = sQLiteCursor.getInt(22);
                bVarArr[i3].v = sQLiteCursor.getInt(23);
                bVarArr[i3].w = sQLiteCursor.getInt(24);
                bVarArr[i3].p = sQLiteCursor.getInt(13);
                bVarArr[i3].q = sQLiteCursor.getInt(14);
                bVarArr[i3].y = -1;
                bVarArr[i3].z = null;
                this.O = -1;
                g.clear();
                String[] split = sQLiteCursor.getString(25).split(",");
                int i4 = 0;
                while (i4 < split.length) {
                    int i5 = i4 + 1;
                    this.R = Integer.parseInt(split[i4]);
                    this.S = Integer.parseInt(split[i5]);
                    bVarArr[i3].D.add(new Point(this.R, this.S));
                    g.add(new Point(this.R, this.S));
                    this.O++;
                    i4 = i5 + 1;
                }
                this.P = this.O;
            } else {
                bVarArr[i3].f163a = sQLiteCursor.getInt(0);
                bVarArr[i3].f164b = null;
                bVarArr[i3].c = sQLiteCursor.getString(44);
                bVarArr[i3].d = sQLiteCursor.getString(20);
                bVarArr[i3].e = sQLiteCursor.getString(21);
                bVarArr[i3].f = sQLiteCursor.getInt(22);
                bVarArr[i3].g = sQLiteCursor.getInt(23);
                bVarArr[i3].h = sQLiteCursor.getInt(24);
                bVarArr[i3].i = sQLiteCursor.getInt(25);
                bVarArr[i3].j = sQLiteCursor.getInt(26);
                bVarArr[i3].k = sQLiteCursor.getInt(27);
                bVarArr[i3].l = sQLiteCursor.getInt(28);
                bVarArr[i3].m = sQLiteCursor.getInt(29);
                bVarArr[i3].n = sQLiteCursor.getInt(30);
                bVarArr[i3].o = sQLiteCursor.getInt(31);
                bVarArr[i3].A = sQLiteCursor.getFloat(32) == 1.0f ? 255 : 0;
                bVarArr[i3].B = sQLiteCursor.getFloat(33) == 1.0f ? 128 : 0;
                bVarArr[i3].C = sQLiteCursor.getFloat(34) == 1.0f ? 255 : 0;
                bVarArr[i3].x = sQLiteCursor.getString(35);
                bVarArr[i3].s = sQLiteCursor.getInt(36);
                bVarArr[i3].r = sQLiteCursor.getInt(37);
                bVarArr[i3].u = sQLiteCursor.getInt(38);
                bVarArr[i3].t = sQLiteCursor.getInt(39);
                bVarArr[i3].v = sQLiteCursor.getInt(40);
                bVarArr[i3].w = sQLiteCursor.getInt(41);
                bVarArr[i3].p = sQLiteCursor.getInt(42);
                bVarArr[i3].q = sQLiteCursor.getInt(43);
                bVarArr[i3].y = -1;
                bVarArr[i3].z = null;
                this.O = -1;
                g.clear();
                String[] split2 = sQLiteCursor.getString(48).split(",");
                int i6 = 0;
                while (i6 < split2.length) {
                    int i7 = i6 + 1;
                    this.R = Integer.parseInt(split2[i6]);
                    this.S = Integer.parseInt(split2[i7]);
                    bVarArr[i3].D.add(new Point(this.R, this.S));
                    g.add(new Point(this.R, this.S));
                    this.O++;
                    i6 = i7 + 1;
                }
                this.P = this.O;
            }
            sQLiteCursor.moveToNext();
            if (!t2) {
                bVarArr[i3].A = 0;
                bVarArr[i3].B = 0;
                bVarArr[i3].C = 0;
            }
        }
        sQLiteCursor.moveToFirst();
        return bVarArr;
    }
}
