package com.cyberandsons.tcmaidtrial.detailed;

import android.view.MotionEvent;
import android.view.View;

final class ez implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointsDetail f577a;

    ez(PointsDetail pointsDetail) {
        this.f577a = pointsDetail;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f577a.V.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f577a.c;
    }
}
