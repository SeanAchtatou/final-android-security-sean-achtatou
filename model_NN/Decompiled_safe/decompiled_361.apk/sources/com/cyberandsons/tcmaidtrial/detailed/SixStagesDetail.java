package com.cyberandsons.tcmaidtrial.detailed;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.a;
import com.cyberandsons.tcmaidtrial.a.c;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.lists.FormulasList;
import com.cyberandsons.tcmaidtrial.lists.PointsList;
import com.cyberandsons.tcmaidtrial.misc.ad;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class SixStagesDetail extends Activity implements View.OnClickListener, View.OnTouchListener {
    private n A;
    private boolean B;
    /* access modifiers changed from: private */
    public GestureDetector C;
    private View.OnTouchListener D;
    /* access modifiers changed from: private */
    public GestureDetector E;
    private View.OnTouchListener F;
    /* access modifiers changed from: private */
    public GestureDetector G;
    private View.OnTouchListener H;

    /* renamed from: a  reason: collision with root package name */
    ScrollView f437a;

    /* renamed from: b  reason: collision with root package name */
    EditText f438b;
    boolean c;
    private TextView d;
    private EditText e;
    private EditText f;
    private EditText g;
    private EditText h;
    private EditText i;
    private EditText j;
    private ImageButton k;
    private ImageButton l;
    private ImageButton m;
    private ImageButton n;
    private ImageButton o;
    private ImageButton p;
    private Button q;
    private boolean r = true;
    private boolean s;
    private boolean t;
    private boolean u;
    private int v;
    private int w;
    private boolean x;
    private boolean y;
    private SQLiteDatabase z;

    public SixStagesDetail() {
        this.s = !this.r;
        this.t = this.s;
        this.u = this.s;
        this.x = this.s;
        this.c = this.r;
        this.y = this.s;
        this.B = this.s;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.z == null || !this.z.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("SSD:openDataBase()", str + " opened.");
                this.z = SQLiteDatabase.openDatabase(str, null, 16);
                this.z.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.z.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("SSD:openDataBase()", str2 + " ATTACHed.");
                this.A = new n();
                this.A.a(this.z);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new ft(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        this.B = this.A.ad();
        setContentView((int) C0000R.layout.sixstages_detailed_edit);
        this.d = (TextView) findViewById(C0000R.id.tce_label);
        this.f437a = (ScrollView) findViewById(C0000R.id.svDetail);
        getWindow().setSoftInputMode(3);
        this.C = new GestureDetector(new l(this));
        this.D = new fz(this);
        this.E = new GestureDetector(new cm(this));
        this.F = new gb(this);
        this.G = new GestureDetector(new ck(this));
        this.H = new gd(this);
        this.d.setOnClickListener(this);
        this.d.setOnTouchListener(this.H);
        a(true);
        this.f437a.smoothScrollBy(0, 0);
    }

    public void onDestroy() {
        TcmAid.S = null;
        TcmAid.ar = null;
        try {
            if (this.z != null && this.z.isOpen()) {
                this.z.close();
                this.z = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            this.y = this.r;
            TcmAid.bl = this.r;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        int i2;
        boolean a2;
        int i3;
        if (!this.t) {
            int i4 = 131073;
            if (this.A.x()) {
                i4 = 131073 + 16384;
            }
            if (this.A.y()) {
                i3 = i4 + 32768;
            } else {
                i3 = i4;
            }
            this.t = this.r;
            this.c = this.s;
            this.k.setVisibility(4);
            this.l.setVisibility(4);
            this.n.setVisibility(4);
            this.o.setVisibility(4);
            this.m.setVisibility(4);
            this.p.setImageResource(17301582);
            this.q.setVisibility(0);
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            inputMethodManager.getInputMethodList();
            inputMethodManager.toggleSoftInput(2, 0);
            ad.a(this.e, i3);
            ad.a(this.f, i3);
            ad.a(this.g, i3);
            ad.a(this.h, i3);
            ad.a(this.i, i3);
            ad.a(this.j, i3);
            ad.a(this.f438b, i3);
            return;
        }
        ContentValues contentValues = new ContentValues();
        boolean z2 = this.s;
        if (TcmAid.aQ == dh.f946a.intValue()) {
            a2 = z2;
            i2 = TcmAid.aQ + 1000;
        } else {
            i2 = TcmAid.aQ;
            a2 = q.a("SELECT COUNT(userDB.sixchannels._id) FROM userDB.sixchannels ", i2, this.z);
            if (!a2) {
                contentValues.put("_id", Integer.toString(i2));
            }
        }
        contentValues.put("symptoms", this.e.getText().toString());
        contentValues.put("english", this.f.getText().toString());
        contentValues.put("tongue", this.g.getText().toString());
        contentValues.put("pulse", this.h.getText().toString());
        contentValues.put("points", this.i.getText().toString());
        contentValues.put("formula", this.j.getText().toString());
        contentValues.put("notes", this.f438b.getText().toString());
        if (!a2) {
            try {
                this.z.insert("userDB.sixchannels", null, contentValues);
            } catch (SQLException e2) {
                q.a(e2);
            }
        } else {
            this.z.update("userDB.sixchannels", contentValues, "_id=" + Integer.toString(i2), null);
        }
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.f438b.getWindowToken(), 0);
        this.t = this.s;
        this.c = this.r;
        this.k.setVisibility(0);
        this.l.setVisibility(0);
        this.n.setVisibility(0);
        this.o.setVisibility(0);
        this.m.setVisibility(0);
        this.p.setImageResource(17301566);
        this.q.setVisibility(4);
        a(false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:105:0x0437  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0440  */
    /* JADX WARNING: Removed duplicated region for block: B:124:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0298  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x02ae A[Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0344 A[Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0350  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0357  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0399  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x03de  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x03e7  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x03f2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r10) {
        /*
            r9 = this;
            r2 = 4
            r7 = 1
            r6 = 0
            if (r10 == 0) goto L_0x00ce
            r0 = 2131362330(0x7f0a021a, float:1.8344438E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r9.p = r0
            android.widget.ImageButton r0 = r9.p
            com.cyberandsons.tcmaidtrial.detailed.b r1 = new com.cyberandsons.tcmaidtrial.detailed.b
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            r0 = 2131362331(0x7f0a021b, float:1.834444E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r9.q = r0
            android.widget.Button r0 = r9.q
            com.cyberandsons.tcmaidtrial.detailed.e r1 = new com.cyberandsons.tcmaidtrial.detailed.e
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            r0 = 2131361811(0x7f0a0013, float:1.8343385E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r9.k = r0
            android.widget.ImageButton r0 = r9.k
            com.cyberandsons.tcmaidtrial.detailed.f r1 = new com.cyberandsons.tcmaidtrial.detailed.f
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            r0 = 2131361815(0x7f0a0017, float:1.8343393E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r9.l = r0
            android.widget.ImageButton r0 = r9.l
            com.cyberandsons.tcmaidtrial.detailed.c r1 = new com.cyberandsons.tcmaidtrial.detailed.c
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            r0 = 2131361814(0x7f0a0016, float:1.834339E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r9.n = r0
            android.widget.ImageButton r0 = r9.n
            com.cyberandsons.tcmaidtrial.detailed.d r1 = new com.cyberandsons.tcmaidtrial.detailed.d
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            r0 = 2131361813(0x7f0a0015, float:1.8343389E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r9.o = r0
            android.widget.ImageButton r0 = r9.o
            com.cyberandsons.tcmaidtrial.detailed.i r1 = new com.cyberandsons.tcmaidtrial.detailed.i
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            r0 = 2131361812(0x7f0a0014, float:1.8343387E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r9.m = r0
            android.widget.ImageButton r0 = r9.m
            com.cyberandsons.tcmaidtrial.detailed.j r1 = new com.cyberandsons.tcmaidtrial.detailed.j
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            boolean r0 = r9.B
            if (r0 == 0) goto L_0x00a6
            android.widget.ImageButton r0 = r9.l
            r0.setVisibility(r2)
            android.widget.ImageButton r0 = r9.m
            r0.setVisibility(r2)
        L_0x00a6:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.aK
            if (r0 != 0) goto L_0x0361
            boolean r0 = r9.B
            if (r0 != 0) goto L_0x00b5
            android.widget.ImageButton r0 = r9.l
            boolean r1 = r9.s
            r9.a(r0, r1)
        L_0x00b5:
            android.widget.ImageButton r0 = r9.n
            boolean r1 = r9.s
            r9.a(r0, r1)
            android.widget.ImageButton r0 = r9.o
            boolean r1 = r9.r
            r9.a(r0, r1)
            boolean r0 = r9.B
            if (r0 != 0) goto L_0x00ce
            android.widget.ImageButton r0 = r9.m
            boolean r1 = r9.r
            r9.a(r0, r1)
        L_0x00ce:
            android.widget.EditText r0 = r9.f
            if (r0 == 0) goto L_0x00d4
            r9.f = r6
        L_0x00d4:
            r0 = 2131362312(0x7f0a0208, float:1.8344401E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r9.f = r0
            android.widget.EditText r0 = r9.f
            com.cyberandsons.tcmaidtrial.detailed.g r1 = new com.cyberandsons.tcmaidtrial.detailed.g
            r1.<init>(r9)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r9.e
            if (r0 == 0) goto L_0x00ef
            r9.e = r6
        L_0x00ef:
            r0 = 2131362314(0x7f0a020a, float:1.8344405E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r9.e = r0
            android.widget.EditText r0 = r9.e
            com.cyberandsons.tcmaidtrial.detailed.h r1 = new com.cyberandsons.tcmaidtrial.detailed.h
            r1.<init>(r9)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r9.g
            if (r0 == 0) goto L_0x010a
            r9.g = r6
        L_0x010a:
            r0 = 2131362316(0x7f0a020c, float:1.834441E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r9.g = r0
            android.widget.EditText r0 = r9.g
            com.cyberandsons.tcmaidtrial.detailed.fv r1 = new com.cyberandsons.tcmaidtrial.detailed.fv
            r1.<init>(r9)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r9.h
            if (r0 == 0) goto L_0x0125
            r9.h = r6
        L_0x0125:
            r0 = 2131362318(0x7f0a020e, float:1.8344413E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r9.h = r0
            android.widget.EditText r0 = r9.h
            com.cyberandsons.tcmaidtrial.detailed.fw r1 = new com.cyberandsons.tcmaidtrial.detailed.fw
            r1.<init>(r9)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r9.i
            if (r0 == 0) goto L_0x0140
            r9.i = r6
        L_0x0140:
            r0 = 2131362320(0x7f0a0210, float:1.8344417E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r9.i = r0
            android.widget.EditText r0 = r9.i
            r0.setOnClickListener(r9)
            android.widget.EditText r0 = r9.i
            android.view.View$OnTouchListener r1 = r9.D
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r9.j
            if (r0 == 0) goto L_0x015d
            r9.j = r6
        L_0x015d:
            r0 = 2131362325(0x7f0a0215, float:1.8344427E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r9.j = r0
            android.widget.EditText r0 = r9.j
            r0.setOnClickListener(r9)
            android.widget.EditText r0 = r9.j
            android.view.View$OnTouchListener r1 = r9.F
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r9.f438b
            if (r0 == 0) goto L_0x017a
            r9.f438b = r6
        L_0x017a:
            r0 = 2131362324(0x7f0a0214, float:1.8344425E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r9.f438b = r0
            android.widget.EditText r0 = r9.f438b
            com.cyberandsons.tcmaidtrial.detailed.fx r1 = new com.cyberandsons.tcmaidtrial.detailed.fx
            r1.<init>(r9)
            r0.setOnTouchListener(r1)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT main.sixchannels._id, main.sixchannels.pathology, main.sixchannels.english, main.sixchannels.symptoms, main.sixchannels.tongue, main.sixchannels.pulse, main.sixchannels.points, main.sixchannels.formula, main.sixchannels.notes FROM main.sixchannels WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.aN
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r2 = r0.toString()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.E
            if (r0 == 0) goto L_0x01ad
            java.lang.String r0 = "SSD:loadSystemSuppliedData()"
            android.util.Log.d(r0, r2)
        L_0x01ad:
            android.database.sqlite.SQLiteDatabase r0 = r9.z     // Catch:{ SQLException -> 0x0392, NullPointerException -> 0x039e, all -> 0x03e3 }
            r1 = 0
            android.database.Cursor r0 = r0.rawQuery(r2, r1)     // Catch:{ SQLException -> 0x0392, NullPointerException -> 0x039e, all -> 0x03e3 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0392, NullPointerException -> 0x039e, all -> 0x03e3 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.E     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            if (r3 == 0) goto L_0x0200
            java.lang.String r3 = "SSD:loadSystemSuppliedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r4.<init>()     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            java.lang.String r5 = "query count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            java.lang.String r5 = java.lang.Integer.toString(r1)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            java.lang.String r3 = "SSD:loadSystemSuppliedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r4.<init>()     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            java.lang.String r5 = "column count = '"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            int r5 = r0.getColumnCount()     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            java.lang.String r5 = "' "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
        L_0x0200:
            boolean r3 = r0.moveToFirst()     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            if (r3 == 0) goto L_0x0264
            if (r1 != r7) goto L_0x0264
            android.widget.TextView r1 = r9.d     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r1.setText(r3)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            android.widget.EditText r1 = r9.f     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r3 = 2
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r1.setText(r3)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            android.widget.EditText r1 = r9.e     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r3 = 3
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r1.setText(r3)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            android.widget.EditText r1 = r9.g     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r3 = 4
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r1.setText(r3)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            android.widget.EditText r1 = r9.h     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r3 = 5
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r1.setText(r3)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r1 = 2131362320(0x7f0a0210, float:1.8344417E38)
            android.view.View r1 = r9.findViewById(r1)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            android.widget.EditText r1 = (android.widget.EditText) r1     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r9.i = r1     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            android.widget.EditText r1 = r9.i     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r3 = 6
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r1.setText(r3)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            android.widget.EditText r1 = r9.j     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r3 = 7
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r1.setText(r3)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            android.widget.EditText r1 = r9.f438b     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r3 = 8
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
            r1.setText(r3)     // Catch:{ SQLException -> 0x045c, NullPointerException -> 0x0459 }
        L_0x0264:
            if (r0 == 0) goto L_0x0269
            r0.close()
        L_0x0269:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT userDB.sixchannels.english, userDB.sixchannels.symptoms, userDB.sixchannels.tongue, userDB.sixchannels.pulse, userDB.sixchannels.points, userDB.sixchannels.formula, userDB.sixchannels.notes, userDB.sixchannels.pathology FROM userDB.sixchannels WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "userDB.sixchannels."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "_id"
            java.lang.StringBuilder r0 = r0.append(r1)
            r1 = 61
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.aQ
            java.lang.String r1 = java.lang.Integer.toString(r1)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.E
            if (r0 == 0) goto L_0x029d
            java.lang.String r0 = "SSD:loadUserModifiedData()"
            android.util.Log.d(r0, r1)
        L_0x029d:
            android.database.sqlite.SQLiteDatabase r0 = r9.z     // Catch:{ SQLException -> 0x03eb, NullPointerException -> 0x03f7, all -> 0x043c }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x03eb, NullPointerException -> 0x03f7, all -> 0x043c }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x03eb, NullPointerException -> 0x03f7, all -> 0x043c }
            int r2 = r0.getCount()     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.E     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            if (r3 == 0) goto L_0x02f0
            java.lang.String r3 = "SSD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r4.<init>()     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            java.lang.String r5 = "query count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            java.lang.String r5 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            java.lang.String r3 = "SSD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r4.<init>()     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            java.lang.String r5 = "column count = '"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            int r5 = r0.getColumnCount()     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            java.lang.String r5 = "' "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
        L_0x02f0:
            boolean r3 = r0.moveToFirst()     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            if (r3 == 0) goto L_0x034e
            if (r2 != r7) goto L_0x034e
            android.widget.EditText r2 = r9.f     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r3 = 0
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r2.setText(r3)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            android.widget.EditText r2 = r9.e     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r2.setText(r3)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            android.widget.EditText r2 = r9.g     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r3 = 2
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r2.setText(r3)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            android.widget.EditText r2 = r9.h     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r3 = 3
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r2.setText(r3)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            android.widget.EditText r2 = r9.i     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r3 = 4
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r2.setText(r3)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            android.widget.EditText r2 = r9.j     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r3 = 5
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r2.setText(r3)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            android.widget.EditText r2 = r9.f438b     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r3 = 6
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r2.setText(r3)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.aQ     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r2 <= r3) goto L_0x034e
            android.widget.TextView r2 = r9.d     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r3 = 7
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
            r2.setText(r3)     // Catch:{ SQLException -> 0x044d, NullPointerException -> 0x044b }
        L_0x034e:
            if (r0 == 0) goto L_0x0353
            r0.close()
        L_0x0353:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.cR
            if (r0 != r7) goto L_0x0360
            com.cyberandsons.tcmaidtrial.a.n r0 = r9.A
            java.lang.String r0 = r0.K()
            r9.a(r0)
        L_0x0360:
            return
        L_0x0361:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.aK
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.aL
            int r1 = r1.size()
            int r1 = r1 - r7
            if (r0 != r1) goto L_0x00ce
            boolean r0 = r9.B
            if (r0 != 0) goto L_0x0377
            android.widget.ImageButton r0 = r9.l
            boolean r1 = r9.r
            r9.a(r0, r1)
        L_0x0377:
            android.widget.ImageButton r0 = r9.n
            boolean r1 = r9.r
            r9.a(r0, r1)
            android.widget.ImageButton r0 = r9.o
            boolean r1 = r9.s
            r9.a(r0, r1)
            boolean r0 = r9.B
            if (r0 != 0) goto L_0x00ce
            android.widget.ImageButton r0 = r9.m
            boolean r1 = r9.s
            r9.a(r0, r1)
            goto L_0x00ce
        L_0x0392:
            r0 = move-exception
            r1 = r6
        L_0x0394:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0457 }
            if (r1 == 0) goto L_0x0269
            r1.close()
            goto L_0x0269
        L_0x039e:
            r0 = move-exception
            r0 = r6
        L_0x03a0:
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0452 }
            java.lang.String r3 = "myVariable"
            r1.a(r3, r2)     // Catch:{ all -> 0x0452 }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0452 }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x0452 }
            java.lang.String r3 = "SSD:loadSystemSuppliedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x0452 }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x0452 }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x0452 }
            r1.<init>(r9)     // Catch:{ all -> 0x0452 }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x0452 }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x0452 }
            r2 = 2131099673(0x7f060019, float:1.7811706E38)
            java.lang.String r2 = r9.getString(r2)     // Catch:{ all -> 0x0452 }
            r1.setMessage(r2)     // Catch:{ all -> 0x0452 }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.fn r3 = new com.cyberandsons.tcmaidtrial.detailed.fn     // Catch:{ all -> 0x0452 }
            r3.<init>(r9)     // Catch:{ all -> 0x0452 }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x0452 }
            r1.show()     // Catch:{ all -> 0x0452 }
            if (r0 == 0) goto L_0x0269
            r0.close()
            goto L_0x0269
        L_0x03e3:
            r0 = move-exception
            r1 = r6
        L_0x03e5:
            if (r1 == 0) goto L_0x03ea
            r1.close()
        L_0x03ea:
            throw r0
        L_0x03eb:
            r0 = move-exception
            r1 = r6
        L_0x03ed:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0449 }
            if (r1 == 0) goto L_0x0353
            r1.close()
            goto L_0x0353
        L_0x03f7:
            r0 = move-exception
            r0 = r6
        L_0x03f9:
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0444 }
            java.lang.String r3 = "myVariable"
            r2.a(r3, r1)     // Catch:{ all -> 0x0444 }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0444 }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x0444 }
            java.lang.String r3 = "SSD:loadUserModifiedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x0444 }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x0444 }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x0444 }
            r1.<init>(r9)     // Catch:{ all -> 0x0444 }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x0444 }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x0444 }
            r2 = 2131099674(0x7f06001a, float:1.7811708E38)
            java.lang.String r2 = r9.getString(r2)     // Catch:{ all -> 0x0444 }
            r1.setMessage(r2)     // Catch:{ all -> 0x0444 }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.fp r3 = new com.cyberandsons.tcmaidtrial.detailed.fp     // Catch:{ all -> 0x0444 }
            r3.<init>(r9)     // Catch:{ all -> 0x0444 }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x0444 }
            r1.show()     // Catch:{ all -> 0x0444 }
            if (r0 == 0) goto L_0x0353
            r0.close()
            goto L_0x0353
        L_0x043c:
            r0 = move-exception
            r1 = r6
        L_0x043e:
            if (r1 == 0) goto L_0x0443
            r1.close()
        L_0x0443:
            throw r0
        L_0x0444:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x043e
        L_0x0449:
            r0 = move-exception
            goto L_0x043e
        L_0x044b:
            r2 = move-exception
            goto L_0x03f9
        L_0x044d:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x03ed
        L_0x0452:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x03e5
        L_0x0457:
            r0 = move-exception
            goto L_0x03e5
        L_0x0459:
            r1 = move-exception
            goto L_0x03a0
        L_0x045c:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0394
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.detailed.SixStagesDetail.a(boolean):void");
    }

    private void a(String str) {
        if (str != null && str.length() != 0) {
            String[] split = str.split(",");
            for (String parseInt : split) {
                switch (Integer.parseInt(parseInt)) {
                    case 1:
                        dh.a(this.f.getText());
                        break;
                    case 2:
                        dh.a(this.e.getText());
                        break;
                    case 3:
                        dh.a(this.g.getText());
                        break;
                    case 4:
                        dh.a(this.h.getText());
                        break;
                    case 5:
                        dh.a(this.i.getText());
                        break;
                    case 6:
                        dh.a(this.j.getText());
                        break;
                    case 7:
                        dh.a(this.f438b.getText());
                        break;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        boolean z2;
        String str;
        if (this.y) {
            this.y = this.s;
            return;
        }
        String[] split = this.i.getText().toString().split(",");
        String[] strArr = {"BL", "DU", "GB", "HT", "KI", "LI", "LV", "LU", "PC", "REN", "SI", "SJ", "SP", "ST", "TB", "TW", "M-HN", "N-HN", "M-BW", "N-BW", "M-CA", "N-CA", "M-UE", "N-UE", "M-LE", "N-LE"};
        StringBuffer stringBuffer = new StringBuffer();
        boolean z3 = true;
        for (String trim : split) {
            String trim2 = trim.trim();
            int i2 = 0;
            while (true) {
                if (i2 >= strArr.length) {
                    z2 = false;
                    break;
                }
                if (dh.N) {
                    Log.d("SSD:meridianArray", " meridianArray[" + Integer.toString(i2) + "] = (" + strArr[i2] + ')');
                }
                if (trim2.indexOf(strArr[i2]) != -1) {
                    z2 = true;
                    break;
                }
                i2++;
            }
            if (z2) {
                if (dh.N) {
                    Log.d("SSD:tok", trim2);
                }
                if (z3) {
                    str = " WHERE ";
                } else {
                    str = " OR ";
                }
                stringBuffer.append(str + " xxxx.pointslocation.common_name LIKE '" + trim2 + "' OR xxxx.pointslocation.name LIKE '" + trim2 + "' ");
                if (dh.N) {
                    Log.d("SSD:pointItems", stringBuffer.toString());
                }
                z3 = false;
            }
        }
        if (dh.N) {
            Log.d("SSD:pointItems", stringBuffer.toString());
        }
        TcmAid.ar = a.a(stringBuffer.toString().replace("xxxx", "main"), stringBuffer.toString().replace("xxxx", "userDB").replace("WHERE  userDB", "WHERE (userDB") + ')', this.A);
        TcmAid.ap = null;
        startActivityForResult(new Intent(this, PointsList.class), 1350);
    }

    /* access modifiers changed from: protected */
    public final void c() {
        String str;
        String str2;
        if (this.y) {
            this.y = this.s;
            return;
        }
        int N = this.A.N();
        if (N == 0 || N == 1) {
            str = "pinyin";
        } else {
            str = "english";
        }
        String[] split = this.j.getText().toString().replaceAll(" and ", ", ").replaceAll(" or ", ", ").split(",");
        StringBuffer stringBuffer = new StringBuffer();
        boolean z2 = this.r;
        for (String trim : split) {
            String trim2 = trim.trim();
            if (z2) {
                str2 = " AND xxxx.formulas.pinyin LIKE '" + trim2.trim() + "' ";
                z2 = this.s;
            } else {
                str2 = " OR xxxx.formulas.pinyin LIKE '" + trim2.trim() + "' ";
            }
            stringBuffer.append(str2);
            if (dh.N) {
                Log.d("SSD:formulaItems", stringBuffer.toString());
            }
        }
        TcmAid.S = c.a(stringBuffer.toString().replace("xxxx", "main"), stringBuffer.toString().replace("xxxx", "userDB").replace("AND userDB", "AND (userDB") + ')', str);
        TcmAid.Q = null;
        startActivityForResult(new Intent(this, FormulasList.class), 1350);
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        boolean z2 = this.s;
        switch (i2) {
            case 0:
                if (this.x) {
                    this.v -= 2;
                } else {
                    TcmAid.aK = 0;
                    if (!this.B) {
                        a(this.l, this.s);
                    }
                    a(this.n, this.s);
                    a(this.o, this.r);
                    if (!this.B) {
                        a(this.m, this.r);
                    }
                }
                z2 = this.r;
                break;
            case 1:
                if (this.x) {
                    this.w -= 2;
                } else if (TcmAid.aK - 1 >= 0) {
                    if (TcmAid.aK - 1 == 0) {
                        if (!this.B) {
                            a(this.l, this.s);
                        }
                        a(this.n, this.s);
                    }
                    if (this.o.isEnabled() == this.s) {
                        a(this.o, this.r);
                        if (!this.B) {
                            a(this.m, this.r);
                        }
                    }
                    TcmAid.aK--;
                }
                z2 = this.r;
                break;
            case 2:
                if (!this.x) {
                    int size = TcmAid.aL.size();
                    if (TcmAid.aK + 1 < size) {
                        if (TcmAid.aK + 1 == size - 1) {
                            a(this.o, this.s);
                            if (!this.B) {
                                a(this.m, this.s);
                            }
                        }
                        if (this.n.isEnabled() == this.s) {
                            if (!this.B) {
                                a(this.l, this.r);
                            }
                            a(this.n, this.r);
                        }
                        TcmAid.aK++;
                        z2 = this.r;
                        break;
                    }
                } else {
                    this.w += 2;
                    z2 = this.r;
                    break;
                }
                break;
            case 3:
                if (this.x) {
                    this.v += 2;
                } else {
                    TcmAid.aK = TcmAid.aL.size() - 1;
                    if (!this.B) {
                        a(this.l, this.r);
                    }
                    a(this.n, this.r);
                    a(this.o, this.s);
                    if (!this.B) {
                        a(this.m, this.s);
                    }
                }
                z2 = this.r;
                break;
        }
        if (z2) {
            if (TcmAid.aK >= TcmAid.aL.size()) {
                TcmAid.aK = TcmAid.aL.size() - 1;
            }
            int intValue = ((Integer) TcmAid.aL.get(TcmAid.aK)).intValue();
            TcmAid.aN = "main.sixchannels._id=" + Integer.toString(intValue);
            TcmAid.aQ = intValue;
            a(false);
            this.f437a.post(new fr(this));
        }
    }

    private void a(ImageButton imageButton, boolean z2) {
        if (z2) {
            imageButton.setEnabled(this.r);
            imageButton.setVisibility(0);
            return;
        }
        imageButton.setEnabled(this.s);
        imageButton.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }

    public void onClick(View view) {
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }
}
