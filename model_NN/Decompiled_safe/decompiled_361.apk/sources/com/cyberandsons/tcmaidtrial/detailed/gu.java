package com.cyberandsons.tcmaidtrial.detailed;

import android.content.Intent;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.draw.ImageGallery;

final class gu implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FormulasDetail f628a;

    gu(FormulasDetail formulasDetail) {
        this.f628a = formulasDetail;
    }

    public final void onClick(View view) {
        String str;
        FormulasDetail formulasDetail = this.f628a;
        if (formulasDetail.g) {
            formulasDetail.g = formulasDetail.e;
            return;
        }
        String[] split = formulasDetail.f428b.getText().toString().split(" ");
        StringBuffer stringBuffer = new StringBuffer();
        String str2 = "";
        boolean z = formulasDetail.e;
        boolean z2 = formulasDetail.d;
        boolean z3 = z;
        for (String trim : split) {
            String trim2 = trim.trim();
            if (trim2.indexOf(40) != -1) {
                z3 = formulasDetail.d;
            } else if (trim2.indexOf(58) < 0) {
                str2 = str2 + trim2 + ' ';
            }
            if (z3) {
                if (z2) {
                    str = str2.trim();
                    z2 = formulasDetail.e;
                } else {
                    str = ',' + str2.trim();
                }
                boolean z4 = formulasDetail.e;
                stringBuffer.append(str);
                boolean z5 = z4;
                str2 = "";
                z3 = z5;
            }
        }
        TcmAid.U = stringBuffer.toString();
        formulasDetail.startActivityForResult(new Intent(formulasDetail, ImageGallery.class), 1350);
    }
}
