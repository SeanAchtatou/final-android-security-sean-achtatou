package com.cyberandsons.tcmaidtrial.detailed;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.FloatMath;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.draw.a;
import com.cyberandsons.tcmaidtrial.draw.e;
import com.cyberandsons.tcmaidtrial.misc.ad;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class AuricularDetail extends Activity implements View.OnClickListener, View.OnTouchListener {
    private boolean A;
    private String B;
    private boolean C;
    private boolean D;
    private SQLiteDatabase E;
    private n F;
    private int G;
    private boolean H;
    private boolean I;
    /* access modifiers changed from: private */
    public GestureDetector J;
    private View.OnTouchListener K;
    private Matrix L;
    private Matrix M;
    private int N;
    private PointF O;
    private PointF P;
    private float Q;

    /* renamed from: a  reason: collision with root package name */
    ScrollView f423a;

    /* renamed from: b  reason: collision with root package name */
    EditText f424b;
    boolean c;
    private TextView d;
    private ImageView e;
    private EditText f;
    private EditText g;
    private EditText h;
    private ImageButton i;
    private ImageButton j;
    private ImageButton k;
    private ImageButton l;
    private ImageButton m;
    private ImageButton n;
    private ImageButton o;
    private ImageButton p;
    private Button q;
    private boolean r = true;
    private boolean s;
    private boolean t;
    private boolean u;
    private int v;
    private int w;
    private boolean x;
    private boolean y;
    private boolean z;

    public AuricularDetail() {
        this.s = !this.r;
        this.t = this.s;
        this.u = this.s;
        this.x = this.s;
        this.y = this.s;
        this.z = this.s;
        this.A = this.s;
        this.c = this.r;
        this.C = this.s;
        this.D = this.s;
        this.H = this.s;
        this.I = this.s;
        this.L = new Matrix();
        this.M = new Matrix();
        this.N = 0;
        this.O = new PointF();
        this.P = new PointF();
        this.Q = 1.0f;
    }

    static /* synthetic */ void a(AuricularDetail auricularDetail) {
        String format;
        if (auricularDetail.H) {
            String n2 = auricularDetail.F.n();
            if (n.a(n2, auricularDetail.G)) {
                String[] split = n2.split(",");
                boolean z2 = auricularDetail.r;
                String str = "";
                for (String parseInt : split) {
                    int parseInt2 = Integer.parseInt(parseInt);
                    if (parseInt2 != auricularDetail.G) {
                        if (z2) {
                            str = String.format("%d", Integer.valueOf(parseInt2));
                            z2 = auricularDetail.s;
                        } else {
                            str = str + String.format(",%d", Integer.valueOf(parseInt2));
                        }
                    }
                }
                auricularDetail.i.setImageDrawable(auricularDetail.getResources().getDrawable(17301618));
                format = str;
            } else {
                if (n2 == null || n2.length() <= 0) {
                    format = String.format("%d", Integer.valueOf(auricularDetail.G));
                } else {
                    format = String.format("%s,%d", n2, Integer.valueOf(auricularDetail.G));
                }
                auricularDetail.i.setImageDrawable(auricularDetail.getResources().getDrawable(17301619));
            }
            auricularDetail.F.b(format, auricularDetail.E);
            return;
        }
        auricularDetail.setResult(2);
        auricularDetail.finish();
    }

    static /* synthetic */ void b(AuricularDetail auricularDetail) {
        if (auricularDetail.z) {
            auricularDetail.C = !auricularDetail.C;
            if (auricularDetail.C) {
                Bitmap decodeResource = BitmapFactory.decodeResource(auricularDetail.getResources(), C0000R.drawable.homunculus);
                auricularDetail.e.setImageBitmap(new a(auricularDetail, decodeResource, decodeResource.getHeight(), decodeResource.getWidth()).a());
                return;
            }
            auricularDetail.a(auricularDetail.s);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.E == null || !this.E.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("AD:openDataBase()", str + " opened.");
                this.E = SQLiteDatabase.openDatabase(str, null, 16);
                this.E.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.E.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("AD:openDataBase()", str2 + " ATTACHed.");
                this.F = new n();
                this.F.a(this.E);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new cx(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        this.z = this.F.w();
        this.I = this.F.ad();
        setContentView((int) C0000R.layout.auricular_detailed_edit);
        this.d = (TextView) findViewById(C0000R.id.tce_label);
        this.f423a = (ScrollView) findViewById(C0000R.id.svDetail);
        getWindow().setSoftInputMode(3);
        this.J = new GestureDetector(new dx(this));
        this.K = new cw(this);
        this.d.setOnClickListener(this);
        this.d.setOnTouchListener(this.K);
        a(true);
        this.f423a.smoothScrollBy(0, 0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 2, 0, "Add").setIcon(17301559);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 2:
                b();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void onDestroy() {
        try {
            if (this.E != null && this.E.isOpen()) {
                this.E.close();
                this.E = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
        if (i3 == -1) {
            Uri data = intent.getData();
            Log.i("onActivityResult", data.toString());
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(getContentResolver().openInputStream(data));
                if (decodeStream != null) {
                    BitmapDrawable a2 = dh.a(decodeStream, TcmAid.cS - 0, TcmAid.cS - 0, getWindow());
                    this.e.setImageDrawable(a2);
                    this.e.setVisibility(0);
                    this.A = this.r;
                    this.k.setImageResource(17301560);
                    this.k.setVisibility(0);
                    String format = String.format("%s/%s_alt.png", getString(C0000R.string.image_path), this.B);
                    a(format, this.r);
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(format);
                        a2.getBitmap().compress(Bitmap.CompressFormat.PNG, 90, fileOutputStream);
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    } catch (Exception e2) {
                        Log.e("AD:onActivityResult", "Failed to write alternate image: " + e2.getLocalizedMessage());
                    }
                }
            } catch (FileNotFoundException e3) {
                Log.e("AD:onActivityResult", "Failed to process alternate image: " + e3.getLocalizedMessage());
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            this.D = this.r;
            TcmAid.bl = this.r;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        int i2;
        int i3;
        int i4;
        int i5;
        if (!this.t) {
            if (this.F.x()) {
                i2 = 1 + 16384;
                i3 = 131073 + 16384;
            } else {
                i2 = 1;
                i3 = 131073;
            }
            if (this.F.y()) {
                i4 = i2 + 32768;
                i5 = i3 + 32768;
            } else {
                i4 = i2;
                i5 = i3;
            }
            this.t = this.r;
            this.c = this.s;
            this.i.setVisibility(4);
            this.j.setVisibility(4);
            this.k.setVisibility(4);
            this.l.setVisibility(4);
            this.n.setVisibility(4);
            this.o.setVisibility(4);
            this.m.setVisibility(4);
            this.p.setImageResource(17301582);
            this.q.setVisibility(0);
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            inputMethodManager.getInputMethodList();
            inputMethodManager.toggleSoftInput(2, 0);
            ad.a(this.f, i5);
            ad.a(this.g, i5);
            ad.a(this.h, i4);
            ad.a(this.f424b, i5);
            return;
        }
        a((String) null, this.s);
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.f424b.getWindowToken(), 0);
        this.t = this.s;
        this.c = this.r;
        this.i.setVisibility(0);
        if (this.z) {
            this.j.setVisibility(0);
            this.k.setVisibility(0);
        }
        this.l.setVisibility(0);
        this.n.setVisibility(0);
        this.o.setVisibility(0);
        this.m.setVisibility(0);
        this.p.setImageResource(17301566);
        this.q.setVisibility(4);
        a(false);
    }

    private void a(String str, boolean z2) {
        int i2;
        boolean a2;
        ContentValues contentValues = new ContentValues();
        boolean z3 = this.s;
        if (TcmAid.r == dh.f946a.intValue()) {
            a2 = z3;
            i2 = TcmAid.r + 1000;
        } else {
            i2 = TcmAid.r;
            a2 = q.a("SELECT COUNT(userDB.auricular._id) FROM userDB.auricular ", i2, this.E);
            if (!a2) {
                contentValues.put("_id", Integer.toString(i2));
            }
        }
        contentValues.put("location", this.f.getText().toString());
        contentValues.put("indication", this.g.getText().toString());
        contentValues.put("pointcat", this.h.getText().toString());
        contentValues.put("notes", this.f424b.getText().toString());
        if (z2) {
            contentValues.put("alt_image", str);
        }
        if (!a2) {
            try {
                this.E.insert("userDB.auricular", null, contentValues);
            } catch (SQLException e2) {
                q.a(e2);
            }
        } else {
            this.E.update("userDB.auricular", contentValues, "_id=" + Integer.toString(i2), null);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!this.A) {
            startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 0);
            return;
        }
        a((String) null, this.r);
        this.A = this.s;
        a(false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:0x0502  */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x05ae  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x05b7  */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x0636  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x063b A[SYNTHETIC, Splitter:B:150:0x063b] */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x064a  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x0655  */
    /* JADX WARNING: Removed duplicated region for block: B:174:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x042d  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0443 A[Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x04bb A[Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x04c9 A[Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x04ed  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r14) {
        /*
            r13 = this;
            r11 = 4
            r10 = 1
            r9 = 0
            r8 = 0
            if (r14 == 0) goto L_0x0113
            r0 = 2131362330(0x7f0a021a, float:1.8344438E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.p = r0
            android.widget.ImageButton r0 = r13.p
            com.cyberandsons.tcmaidtrial.detailed.dt r1 = new com.cyberandsons.tcmaidtrial.detailed.dt
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            r0 = 2131362331(0x7f0a021b, float:1.834444E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r13.q = r0
            android.widget.Button r0 = r13.q
            com.cyberandsons.tcmaidtrial.detailed.du r1 = new com.cyberandsons.tcmaidtrial.detailed.du
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            com.cyberandsons.tcmaidtrial.a.n r0 = r13.F
            boolean r0 = r0.ab()
            r13.H = r0
            r0 = 2131361811(0x7f0a0013, float:1.8343385E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.i = r0
            android.widget.ImageButton r0 = r13.i
            com.cyberandsons.tcmaidtrial.detailed.dv r1 = new com.cyberandsons.tcmaidtrial.detailed.dv
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            r0 = 2131361846(0x7f0a0036, float:1.8343456E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.j = r0
            android.widget.ImageButton r0 = r13.j
            com.cyberandsons.tcmaidtrial.detailed.dw r1 = new com.cyberandsons.tcmaidtrial.detailed.dw
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            boolean r0 = r13.z
            if (r0 != 0) goto L_0x006b
            android.widget.ImageButton r0 = r13.j
            r0.setVisibility(r11)
        L_0x006b:
            r0 = 2131361809(0x7f0a0011, float:1.834338E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.k = r0
            android.widget.ImageButton r0 = r13.k
            com.cyberandsons.tcmaidtrial.detailed.dp r1 = new com.cyberandsons.tcmaidtrial.detailed.dp
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            boolean r0 = r13.z
            if (r0 != 0) goto L_0x0089
            android.widget.ImageButton r0 = r13.k
            r0.setVisibility(r11)
        L_0x0089:
            r0 = 2131361815(0x7f0a0017, float:1.8343393E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.l = r0
            android.widget.ImageButton r0 = r13.l
            com.cyberandsons.tcmaidtrial.detailed.dq r1 = new com.cyberandsons.tcmaidtrial.detailed.dq
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            r0 = 2131361814(0x7f0a0016, float:1.834339E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.n = r0
            android.widget.ImageButton r0 = r13.n
            com.cyberandsons.tcmaidtrial.detailed.dr r1 = new com.cyberandsons.tcmaidtrial.detailed.dr
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            r0 = 2131361813(0x7f0a0015, float:1.8343389E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.o = r0
            android.widget.ImageButton r0 = r13.o
            com.cyberandsons.tcmaidtrial.detailed.ds r1 = new com.cyberandsons.tcmaidtrial.detailed.ds
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            r0 = 2131361812(0x7f0a0014, float:1.8343387E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.m = r0
            android.widget.ImageButton r0 = r13.m
            com.cyberandsons.tcmaidtrial.detailed.do r1 = new com.cyberandsons.tcmaidtrial.detailed.do
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            boolean r0 = r13.I
            if (r0 == 0) goto L_0x00eb
            android.widget.ImageButton r0 = r13.l
            r0.setVisibility(r11)
            android.widget.ImageButton r0 = r13.m
            r0.setVisibility(r11)
        L_0x00eb:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.h
            if (r0 != 0) goto L_0x050c
            boolean r0 = r13.I
            if (r0 != 0) goto L_0x00fa
            android.widget.ImageButton r0 = r13.l
            boolean r1 = r13.s
            r13.a(r0, r1)
        L_0x00fa:
            android.widget.ImageButton r0 = r13.n
            boolean r1 = r13.s
            r13.a(r0, r1)
            android.widget.ImageButton r0 = r13.o
            boolean r1 = r13.r
            r13.a(r0, r1)
            boolean r0 = r13.I
            if (r0 != 0) goto L_0x0113
            android.widget.ImageButton r0 = r13.m
            boolean r1 = r13.r
            r13.a(r0, r1)
        L_0x0113:
            r0 = 2131361793(0x7f0a0001, float:1.8343348E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ScrollView r0 = (android.widget.ScrollView) r0
            r13.f423a = r0
            r0 = 2131361794(0x7f0a0002, float:1.834335E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r13.e = r0
            android.widget.ImageView r0 = r13.e
            r0.setOnTouchListener(r13)
            android.graphics.Matrix r0 = r13.L
            r1 = 1065353216(0x3f800000, float:1.0)
            r2 = 1065353216(0x3f800000, float:1.0)
            r0.setTranslate(r1, r2)
            android.widget.ImageView r0 = r13.e
            android.graphics.Matrix r1 = r13.L
            r0.setImageMatrix(r1)
            android.widget.EditText r0 = r13.f
            if (r0 == 0) goto L_0x0144
            r13.f = r9
        L_0x0144:
            r0 = 2131361798(0x7f0a0006, float:1.8343359E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.f = r0
            android.widget.EditText r0 = r13.f
            com.cyberandsons.tcmaidtrial.detailed.de r1 = new com.cyberandsons.tcmaidtrial.detailed.de
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.g
            if (r0 == 0) goto L_0x015f
            r13.g = r9
        L_0x015f:
            r0 = 2131361800(0x7f0a0008, float:1.8343363E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.g = r0
            android.widget.EditText r0 = r13.g
            com.cyberandsons.tcmaidtrial.detailed.dd r1 = new com.cyberandsons.tcmaidtrial.detailed.dd
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.h
            if (r0 == 0) goto L_0x017a
            r13.h = r9
        L_0x017a:
            r0 = 2131361802(0x7f0a000a, float:1.8343367E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.h = r0
            android.widget.EditText r0 = r13.h
            com.cyberandsons.tcmaidtrial.detailed.dc r1 = new com.cyberandsons.tcmaidtrial.detailed.dc
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.f424b
            if (r0 == 0) goto L_0x0195
            r13.f424b = r9
        L_0x0195:
            r0 = 2131361804(0x7f0a000c, float:1.834337E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.f424b = r0
            android.widget.EditText r0 = r13.f424b
            com.cyberandsons.tcmaidtrial.detailed.db r1 = new com.cyberandsons.tcmaidtrial.detailed.db
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            com.cyberandsons.tcmaidtrial.a.b r1 = new com.cyberandsons.tcmaidtrial.a.b
            r1.<init>()
            com.cyberandsons.tcmaidtrial.a.n r0 = r13.F
            boolean r2 = r0.t()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = "SELECT main.auricular._id, main.auricular.point, main.auricular.location, main.auricular.indication, main.auricular.pointcat, main.auricular.depth, main.auricular.row1, main.auricular.col1, main.auricular.row2, main.auricular.col2, main.auricular.row3, main.auricular.col3, main.auricular.row4, main.auricular.col4, main.auricular.row5, main.auricular.col5, main.auricular.blocksz, main.auricular.blockdir,main.auricular.label, main.auricular.red, main.auricular.green, main.auricular.blue, main.auricular.notes, main.auricular.nada, main.auricular.battlefield, main.auricular.acacd, main.auricular.stopsmoking, main.auricular.weightloss, main.auricular.lblRow, main.auricular.lblCol FROM main.auricular WHERE "
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.TcmAid.m
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = r0.toString()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.p
            if (r0 == 0) goto L_0x01d3
            java.lang.String r0 = "AD:loadSystemSuppliedData()"
            android.util.Log.d(r0, r3)
        L_0x01d3:
            android.database.sqlite.SQLiteDatabase r0 = r13.E     // Catch:{ SQLException -> 0x0675, NullPointerException -> 0x0671, all -> 0x05b3 }
            r4 = 0
            android.database.Cursor r0 = r0.rawQuery(r3, r4)     // Catch:{ SQLException -> 0x0675, NullPointerException -> 0x0671, all -> 0x05b3 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0675, NullPointerException -> 0x0671, all -> 0x05b3 }
            int r4 = r0.getCount()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            boolean r5 = com.cyberandsons.tcmaidtrial.misc.dh.p     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            if (r5 == 0) goto L_0x0226
            java.lang.String r5 = "AD:loadSystemSuppliedData()"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r6.<init>()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.String r7 = "query count = "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.String r7 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.String r6 = r6.toString()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            android.util.Log.d(r5, r6)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.String r5 = "AD:loadSystemSuppliedData()"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r6.<init>()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.String r7 = "column count = '"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            int r7 = r0.getColumnCount()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.String r7 = "' "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.String r6 = r6.toString()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            android.util.Log.d(r5, r6)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
        L_0x0226:
            boolean r5 = r0.moveToFirst()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            if (r5 == 0) goto L_0x03f5
            if (r4 != r10) goto L_0x03f5
            r4 = 0
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.f163a = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 0
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r13.G = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 0
            r1.f164b = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 1
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.c = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.String r4 = "auricular"
            r1.d = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 5
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.x = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 6
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.f = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 7
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.g = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 8
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.h = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 9
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.i = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 10
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.j = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 11
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.k = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 12
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.l = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 13
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.m = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 14
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.n = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 15
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.o = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 16
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.y = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 17
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.z = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 18
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.e = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 19
            float r4 = r0.getFloat(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r5 = 1065353216(0x3f800000, float:1.0)
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 != 0) goto L_0x053d
            r4 = 255(0xff, float:3.57E-43)
        L_0x02c5:
            r1.A = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 20
            float r4 = r0.getFloat(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r5 = 1065353216(0x3f800000, float:1.0)
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 != 0) goto L_0x0540
            r4 = 128(0x80, float:1.794E-43)
        L_0x02d5:
            r1.B = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 21
            float r4 = r0.getFloat(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r5 = 1065353216(0x3f800000, float:1.0)
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 != 0) goto L_0x0543
            r4 = 255(0xff, float:3.57E-43)
        L_0x02e5:
            r1.C = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 24
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.p = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 25
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.q = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = -1
            r1.s = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = -1
            r1.r = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = -1
            r1.u = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = -1
            r1.t = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = -1
            r1.v = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = -1
            r1.w = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            if (r2 != 0) goto L_0x0314
            r2 = 0
            r1.A = r2     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r2 = 0
            r1.B = r2     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r2 = 0
            r1.C = r2     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
        L_0x0314:
            android.widget.TextView r2 = r13.d     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 1
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r2.setText(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            android.content.res.Resources r2 = r13.getResources()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 2130837506(0x7f020002, float:1.7279968E38)
            java.lang.String r2 = r2.getString(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 1
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r13.B = r4     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            boolean r4 = r13.z     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            if (r4 == 0) goto L_0x0546
            android.content.res.Resources r4 = r13.getResources()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r5 = 2130837506(0x7f020002, float:1.7279968E38)
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeResource(r4, r5)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r5 = 280(0x118, float:3.92E-43)
            r6 = 280(0x118, float:3.92E-43)
            android.graphics.drawable.BitmapDrawable r4 = com.cyberandsons.tcmaidtrial.misc.dh.a(r4, r5, r6)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            android.graphics.Bitmap r4 = r4.getBitmap()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            boolean r5 = com.cyberandsons.tcmaidtrial.misc.dh.p     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            if (r5 == 0) goto L_0x037d
            java.lang.String r5 = "AD:auricular row/col"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r6.<init>()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.String r7 = "Row = "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            int r7 = r1.f     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.String r7 = ", Col = "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            int r7 = r1.g     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.String r6 = r6.toString()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            android.util.Log.d(r5, r6)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
        L_0x037d:
            com.cyberandsons.tcmaidtrial.draw.a r5 = new com.cyberandsons.tcmaidtrial.draw.a     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            int r6 = r4.getHeight()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            int r7 = r4.getWidth()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r5.<init>(r13, r4, r6, r7)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r5.a(r2, r1)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            android.graphics.Bitmap r1 = r5.a()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            int r2 = r2 - r8
            int r4 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            int r4 = r4 - r8
            android.view.Window r5 = r13.getWindow()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            android.graphics.drawable.BitmapDrawable r1 = com.cyberandsons.tcmaidtrial.misc.dh.a(r1, r2, r4, r5)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            android.widget.ImageView r2 = r13.e     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r2.setImageDrawable(r1)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            android.widget.ImageView r1 = r13.e     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r2 = 0
            r1.setVisibility(r2)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
        L_0x03aa:
            android.widget.EditText r1 = r13.f     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r2 = 2
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.setText(r2)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            android.widget.EditText r1 = r13.g     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r2 = 3
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.setText(r2)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            android.widget.EditText r1 = r13.h     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r2 = 4
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.setText(r2)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            android.widget.EditText r1 = r13.f424b     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r2 = 22
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.setText(r2)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            boolean r1 = r13.H     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            if (r1 == 0) goto L_0x03f5
            com.cyberandsons.tcmaidtrial.a.n r1 = r13.F     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            java.lang.String r1 = r1.n()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            int r2 = r13.G     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            boolean r1 = com.cyberandsons.tcmaidtrial.a.n.a(r1, r2)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            if (r1 == 0) goto L_0x055d
            android.widget.ImageButton r1 = r13.i     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            android.content.res.Resources r2 = r13.getResources()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 17301618(0x1080072, float:2.4979574E-38)
            android.graphics.drawable.Drawable r2 = r2.getDrawable(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.setImageDrawable(r2)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
        L_0x03f5:
            if (r0 == 0) goto L_0x03fa
            r0.close()
        L_0x03fa:
            boolean r0 = r13.s
            r13.A = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT userDB.auricular.location, userDB.auricular.indication, userDB.auricular.pointcat, userDB.auricular.notes, userDB.auricular.point, userDB.auricular.alt_image FROM userDB.auricular WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "userDB.auricular."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "_id"
            java.lang.StringBuilder r0 = r0.append(r1)
            r1 = 61
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.r
            java.lang.String r1 = java.lang.Integer.toString(r1)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.p
            if (r0 == 0) goto L_0x0432
            java.lang.String r0 = "AD:loadUserModifiedData()"
            android.util.Log.d(r0, r1)
        L_0x0432:
            android.database.sqlite.SQLiteDatabase r0 = r13.E     // Catch:{ SQLException -> 0x0664, NullPointerException -> 0x0661, all -> 0x065c }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x0664, NullPointerException -> 0x0661, all -> 0x065c }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0664, NullPointerException -> 0x0661, all -> 0x065c }
            int r2 = r0.getCount()     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.p     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            if (r3 == 0) goto L_0x0485
            java.lang.String r3 = "DD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r4.<init>()     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            java.lang.String r5 = "query count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            java.lang.String r5 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            java.lang.String r3 = "DD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r4.<init>()     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            java.lang.String r5 = "column count = '"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            int r5 = r0.getColumnCount()     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            java.lang.String r5 = "' "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
        L_0x0485:
            boolean r3 = r0.moveToFirst()     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            if (r3 == 0) goto L_0x04e4
            if (r2 != r10) goto L_0x04e4
            android.widget.EditText r2 = r13.f     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r3 = 0
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            android.widget.EditText r2 = r13.g     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            android.widget.EditText r2 = r13.h     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r3 = 2
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            android.widget.EditText r2 = r13.f424b     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r3 = 3
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.r     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r2 <= r3) goto L_0x04c5
            android.widget.TextView r2 = r13.d     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r3 = 4
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
        L_0x04c5:
            boolean r2 = r13.z     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            if (r2 == 0) goto L_0x063b
            r2 = 5
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            if (r2 == 0) goto L_0x05e8
            int r3 = r2.length()     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            if (r3 <= 0) goto L_0x05e8
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeFile(r2)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            if (r2 != 0) goto L_0x05bb
            android.widget.ImageView r2 = r13.e     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r3 = 2130837543(0x7f020027, float:1.7280043E38)
            r2.setImageResource(r3)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
        L_0x04e4:
            if (r0 == 0) goto L_0x04e9
            r0.close()
        L_0x04e9:
            boolean r0 = r13.z
            if (r0 == 0) goto L_0x0655
            boolean r0 = r13.A
            if (r0 == 0) goto L_0x064e
            android.widget.ImageButton r0 = r13.k
            r1 = 17301560(0x1080038, float:2.4979412E-38)
            r0.setImageResource(r1)
            android.widget.ImageButton r0 = r13.k
            r0.setVisibility(r8)
        L_0x04fe:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.cR
            if (r0 != r10) goto L_0x050b
            com.cyberandsons.tcmaidtrial.a.n r0 = r13.F
            java.lang.String r0 = r0.D()
            r13.a(r0)
        L_0x050b:
            return
        L_0x050c:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.h
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.i
            int r1 = r1.size()
            int r1 = r1 - r10
            if (r0 != r1) goto L_0x0113
            boolean r0 = r13.I
            if (r0 != 0) goto L_0x0522
            android.widget.ImageButton r0 = r13.l
            boolean r1 = r13.r
            r13.a(r0, r1)
        L_0x0522:
            android.widget.ImageButton r0 = r13.n
            boolean r1 = r13.r
            r13.a(r0, r1)
            android.widget.ImageButton r0 = r13.o
            boolean r1 = r13.s
            r13.a(r0, r1)
            boolean r0 = r13.I
            if (r0 != 0) goto L_0x0113
            android.widget.ImageButton r0 = r13.m
            boolean r1 = r13.s
            r13.a(r0, r1)
            goto L_0x0113
        L_0x053d:
            r4 = r8
            goto L_0x02c5
        L_0x0540:
            r4 = r8
            goto L_0x02d5
        L_0x0543:
            r4 = r8
            goto L_0x02e5
        L_0x0546:
            android.widget.ImageView r1 = r13.e     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r2 = 8
            r1.setVisibility(r2)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            goto L_0x03aa
        L_0x054f:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x0553:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x066e }
            if (r1 == 0) goto L_0x03fa
            r1.close()
            goto L_0x03fa
        L_0x055d:
            android.widget.ImageButton r1 = r13.i     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            android.content.res.Resources r2 = r13.getResources()     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r4 = 17301619(0x1080073, float:2.4979577E-38)
            android.graphics.drawable.Drawable r2 = r2.getDrawable(r4)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            r1.setImageDrawable(r2)     // Catch:{ SQLException -> 0x054f, NullPointerException -> 0x056f }
            goto L_0x03f5
        L_0x056f:
            r1 = move-exception
        L_0x0570:
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0668 }
            java.lang.String r2 = "myVariable"
            r1.a(r2, r3)     // Catch:{ all -> 0x0668 }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0668 }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x0668 }
            java.lang.String r3 = "AD:loadSystemSuppliedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x0668 }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x0668 }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x0668 }
            r1.<init>(r13)     // Catch:{ all -> 0x0668 }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x0668 }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x0668 }
            r2 = 2131099673(0x7f060019, float:1.7811706E38)
            java.lang.String r2 = r13.getString(r2)     // Catch:{ all -> 0x0668 }
            r1.setMessage(r2)     // Catch:{ all -> 0x0668 }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.da r3 = new com.cyberandsons.tcmaidtrial.detailed.da     // Catch:{ all -> 0x0668 }
            r3.<init>(r13)     // Catch:{ all -> 0x0668 }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x0668 }
            r1.show()     // Catch:{ all -> 0x0668 }
            if (r0 == 0) goto L_0x03fa
            r0.close()
            goto L_0x03fa
        L_0x05b3:
            r0 = move-exception
            r1 = r9
        L_0x05b5:
            if (r1 == 0) goto L_0x05ba
            r1.close()
        L_0x05ba:
            throw r0
        L_0x05bb:
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            int r3 = r3 - r8
            int r4 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            int r4 = r4 - r8
            android.view.Window r5 = r13.getWindow()     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            android.graphics.drawable.BitmapDrawable r2 = com.cyberandsons.tcmaidtrial.misc.dh.a(r2, r3, r4, r5)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            android.widget.ImageView r3 = r13.e     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r3.setImageDrawable(r2)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            android.widget.ImageView r2 = r13.e     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r3 = 0
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            boolean r2 = r13.r     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r13.A = r2     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            goto L_0x04e4
        L_0x05da:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x05de:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x065f }
            if (r1 == 0) goto L_0x04e9
            r1.close()
            goto L_0x04e9
        L_0x05e8:
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.r     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r2 <= r3) goto L_0x04e4
            android.widget.ImageView r2 = r13.e     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r3 = 8
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            goto L_0x04e4
        L_0x05f7:
            r2 = move-exception
        L_0x05f8:
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0644 }
            java.lang.String r3 = "myVariable"
            r2.a(r3, r1)     // Catch:{ all -> 0x0644 }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0644 }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x0644 }
            java.lang.String r3 = "AD:loadUserModifiedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x0644 }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x0644 }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x0644 }
            r1.<init>(r13)     // Catch:{ all -> 0x0644 }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x0644 }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x0644 }
            r2 = 2131099674(0x7f06001a, float:1.7811708E38)
            java.lang.String r2 = r13.getString(r2)     // Catch:{ all -> 0x0644 }
            r1.setMessage(r2)     // Catch:{ all -> 0x0644 }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.cz r3 = new com.cyberandsons.tcmaidtrial.detailed.cz     // Catch:{ all -> 0x0644 }
            r3.<init>(r13)     // Catch:{ all -> 0x0644 }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x0644 }
            r1.show()     // Catch:{ all -> 0x0644 }
            if (r0 == 0) goto L_0x04e9
            r0.close()
            goto L_0x04e9
        L_0x063b:
            android.widget.ImageView r2 = r13.e     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            r3 = 8
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x05da, NullPointerException -> 0x05f7 }
            goto L_0x04e4
        L_0x0644:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x0648:
            if (r1 == 0) goto L_0x064d
            r1.close()
        L_0x064d:
            throw r0
        L_0x064e:
            android.widget.ImageButton r0 = r13.k
            r0.setVisibility(r11)
            goto L_0x04fe
        L_0x0655:
            android.widget.ImageButton r0 = r13.k
            r0.setVisibility(r11)
            goto L_0x04fe
        L_0x065c:
            r0 = move-exception
            r1 = r9
            goto L_0x0648
        L_0x065f:
            r0 = move-exception
            goto L_0x0648
        L_0x0661:
            r0 = move-exception
            r0 = r9
            goto L_0x05f8
        L_0x0664:
            r0 = move-exception
            r1 = r9
            goto L_0x05de
        L_0x0668:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x05b5
        L_0x066e:
            r0 = move-exception
            goto L_0x05b5
        L_0x0671:
            r0 = move-exception
            r0 = r9
            goto L_0x0570
        L_0x0675:
            r0 = move-exception
            r1 = r9
            goto L_0x0553
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.detailed.AuricularDetail.a(boolean):void");
    }

    private void a(String str) {
        if (str != null && str.length() != 0) {
            String[] split = str.split(",");
            for (String parseInt : split) {
                switch (Integer.parseInt(parseInt)) {
                    case 1:
                        dh.a(this.g.getText());
                        break;
                    case 2:
                        dh.a(this.h.getText());
                        break;
                    case 3:
                        dh.a(this.f424b.getText());
                        break;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        int size;
        int size2;
        Integer num;
        boolean z2 = this.s;
        switch (i2) {
            case 0:
                if (this.x) {
                    this.v -= 2;
                } else {
                    TcmAid.h = 0;
                    if (!this.I) {
                        a(this.l, this.s);
                    }
                    a(this.n, this.s);
                    a(this.o, this.r);
                    if (!this.I) {
                        a(this.m, this.r);
                    }
                }
                z2 = this.r;
                break;
            case 1:
                if (this.x) {
                    this.w -= 2;
                } else if (TcmAid.h - 1 >= 0) {
                    if (TcmAid.h - 1 == 0) {
                        if (!this.I) {
                            a(this.l, this.s);
                        }
                        a(this.n, this.s);
                    }
                    if (this.o.isEnabled() == this.s) {
                        a(this.o, this.r);
                        if (!this.I) {
                            a(this.m, this.r);
                        }
                    }
                    TcmAid.h--;
                }
                z2 = this.r;
                break;
            case 2:
                if (!this.x) {
                    if (this.y) {
                        size2 = TcmAid.k.size();
                    } else {
                        size2 = TcmAid.i.size();
                    }
                    if (TcmAid.h + 1 < size2) {
                        if (TcmAid.h + 1 == size2 - 1) {
                            a(this.o, this.s);
                            if (!this.I) {
                                a(this.m, this.s);
                            }
                        }
                        if (this.n.isEnabled() == this.s) {
                            if (!this.I) {
                                a(this.l, this.r);
                            }
                            a(this.n, this.r);
                        }
                        TcmAid.h++;
                        z2 = this.r;
                        break;
                    }
                } else {
                    this.w += 2;
                    z2 = this.r;
                    break;
                }
                break;
            case 3:
                if (this.x) {
                    this.v += 2;
                } else {
                    if (this.y) {
                        size = TcmAid.k.size();
                    } else {
                        size = TcmAid.i.size();
                    }
                    TcmAid.h = size - 1;
                    if (!this.I) {
                        a(this.l, this.r);
                    }
                    a(this.n, this.r);
                    a(this.o, this.s);
                    if (!this.I) {
                        a(this.m, this.s);
                    }
                }
                z2 = this.r;
                break;
        }
        if (z2) {
            if (this.y) {
                if (TcmAid.h >= TcmAid.k.size()) {
                    TcmAid.h = TcmAid.k.size() - 1;
                }
            } else if (TcmAid.h >= TcmAid.i.size()) {
                TcmAid.h = TcmAid.i.size() - 1;
            }
            if (this.y) {
                num = (Integer) TcmAid.k.get(TcmAid.h);
            } else {
                num = (Integer) TcmAid.i.get(TcmAid.h);
            }
            int intValue = num.intValue();
            TcmAid.m = "main.auricular._id=" + Integer.toString(intValue);
            TcmAid.r = intValue;
            a(false);
            this.f423a.post(new cy(this));
        }
    }

    private void a(ImageButton imageButton, boolean z2) {
        if (z2) {
            imageButton.setEnabled(this.r);
            imageButton.setVisibility(0);
            return;
        }
        imageButton.setEnabled(this.s);
        imageButton.setVisibility(4);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        e a2 = e.a(motionEvent);
        ImageView imageView = (ImageView) view;
        StringBuilder sb = new StringBuilder();
        int b2 = a2.b();
        int i2 = b2 & 255;
        sb.append("event ACTION_").append(new String[]{"DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE", "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?"}[i2]);
        if (i2 == 5 || i2 == 6) {
            sb.append("(pid ").append(b2 >> 8);
            sb.append(')');
        }
        sb.append('[');
        for (int i3 = 0; i3 < a2.a(); i3++) {
            sb.append('#').append(i3);
            sb.append("(pid ").append(a2.c(i3));
            sb.append(")=").append((int) a2.a(i3));
            sb.append(',').append((int) a2.b(i3));
            if (i3 + 1 < a2.a()) {
                sb.append(';');
            }
        }
        sb.append(']');
        Log.d("AD:dumpEvent()", sb.toString());
        switch (a2.b() & 255) {
            case 0:
                this.M.set(this.L);
                this.O.set(a2.c(), a2.d());
                Log.d("AD:onTouch()", "mode=DRAG");
                this.N = 1;
                break;
            case 1:
            case 6:
                this.N = 0;
                Log.d("AD:onTouch()", "mode=NONE");
                break;
            case 2:
                if (this.N != 1) {
                    if (this.N == 2) {
                        float a3 = a(a2);
                        Log.d("AD:onTouch()", "newDist=" + a3);
                        if (a3 > 10.0f) {
                            this.L.set(this.M);
                            float f2 = a3 / this.Q;
                            this.L.postScale(f2, f2, this.P.x, this.P.y);
                            break;
                        }
                    }
                } else {
                    this.L.set(this.M);
                    this.L.postTranslate(a2.c() - this.O.x, a2.d() - this.O.y);
                    break;
                }
                break;
            case 5:
                this.Q = a(a2);
                Log.d("AD:onTouch()", "oldDist=" + this.Q);
                if (this.Q > 10.0f) {
                    this.M.set(this.L);
                    this.P.set((a2.a(0) + a2.a(1)) / 2.0f, (a2.b(1) + a2.b(0)) / 2.0f);
                    this.N = 2;
                    Log.d("AD:onTouch()", "mode=ZOOM");
                    break;
                }
                break;
        }
        imageView.setImageMatrix(this.L);
        return true;
    }

    private static float a(e eVar) {
        float a2 = eVar.a(0) - eVar.a(1);
        float b2 = eVar.b(0) - eVar.b(1);
        return FloatMath.sqrt((a2 * a2) + (b2 * b2));
    }

    public void onClick(View view) {
    }
}
