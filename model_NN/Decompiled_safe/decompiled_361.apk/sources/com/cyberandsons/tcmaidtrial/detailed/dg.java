package com.cyberandsons.tcmaidtrial.detailed;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class dg implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TungDetail f531a;

    dg(TungDetail tungDetail) {
        this.f531a = tungDetail;
    }

    public final void onClick(View view) {
        TungDetail tungDetail = this.f531a;
        ((InputMethodManager) tungDetail.getSystemService("input_method")).hideSoftInputFromWindow(tungDetail.f440b.getWindowToken(), 0);
    }
}
