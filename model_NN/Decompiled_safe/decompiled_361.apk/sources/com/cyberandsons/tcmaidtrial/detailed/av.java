package com.cyberandsons.tcmaidtrial.detailed;

import android.view.MotionEvent;
import android.view.View;

final class av implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PulseDiagDetail f465a;

    av(PulseDiagDetail pulseDiagDetail) {
        this.f465a = pulseDiagDetail;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f465a.P.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f465a.c;
    }
}
