package com.cyberandsons.tcmaidtrial.detailed;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class bi implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointsDetail f479a;

    bi(PointsDetail pointsDetail) {
        this.f479a = pointsDetail;
    }

    public final void onClick(View view) {
        PointsDetail pointsDetail = this.f479a;
        ((InputMethodManager) pointsDetail.getSystemService("input_method")).hideSoftInputFromWindow(pointsDetail.f432b.getWindowToken(), 0);
    }
}
