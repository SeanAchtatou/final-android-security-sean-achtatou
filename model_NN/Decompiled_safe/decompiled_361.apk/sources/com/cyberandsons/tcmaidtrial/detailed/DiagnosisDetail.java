package com.cyberandsons.tcmaidtrial.detailed;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.a;
import com.cyberandsons.tcmaidtrial.a.c;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.lists.FormulasList;
import com.cyberandsons.tcmaidtrial.lists.PointsList;
import com.cyberandsons.tcmaidtrial.misc.ad;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class DiagnosisDetail extends Activity implements View.OnClickListener, View.OnTouchListener {
    private n A;
    private int B;
    private boolean C;
    private boolean D;
    /* access modifiers changed from: private */
    public GestureDetector E;
    private View.OnTouchListener F;
    /* access modifiers changed from: private */
    public GestureDetector G;
    private View.OnTouchListener H;
    /* access modifiers changed from: private */
    public GestureDetector I;
    private View.OnTouchListener J;

    /* renamed from: a  reason: collision with root package name */
    ScrollView f425a;

    /* renamed from: b  reason: collision with root package name */
    EditText f426b;
    boolean c;
    private TextView d;
    private EditText e;
    private EditText f;
    private EditText g;
    private EditText h;
    private EditText i;
    private EditText j;
    private ImageButton k;
    private ImageButton l;
    private ImageButton m;
    private ImageButton n;
    private ImageButton o;
    private ImageButton p;
    private Button q;
    private boolean r = true;
    private boolean s;
    private boolean t;
    private boolean u;
    private int v;
    private int w;
    private boolean x;
    private boolean y;
    private SQLiteDatabase z;

    public DiagnosisDetail() {
        this.s = !this.r;
        this.t = this.s;
        this.u = this.s;
        this.x = this.s;
        this.c = this.r;
        this.y = this.s;
        this.C = this.s;
        this.D = this.s;
    }

    static /* synthetic */ void a(DiagnosisDetail diagnosisDetail) {
        String format;
        if (diagnosisDetail.C) {
            String o2 = diagnosisDetail.A.o();
            if (n.a(o2, diagnosisDetail.B)) {
                String[] split = o2.split(",");
                boolean z2 = diagnosisDetail.r;
                String str = "";
                for (String parseInt : split) {
                    int parseInt2 = Integer.parseInt(parseInt);
                    if (parseInt2 != diagnosisDetail.B) {
                        if (z2) {
                            str = String.format("%d", Integer.valueOf(parseInt2));
                            z2 = diagnosisDetail.s;
                        } else {
                            str = str + String.format(",%d", Integer.valueOf(parseInt2));
                        }
                    }
                }
                diagnosisDetail.k.setImageDrawable(diagnosisDetail.getResources().getDrawable(17301619));
                format = str;
            } else {
                if (o2 == null || o2.length() <= 0) {
                    format = String.format("%d", Integer.valueOf(diagnosisDetail.B));
                } else {
                    format = String.format("%s,%d", o2, Integer.valueOf(diagnosisDetail.B));
                }
                diagnosisDetail.k.setImageDrawable(diagnosisDetail.getResources().getDrawable(17301618));
            }
            diagnosisDetail.A.c(format, diagnosisDetail.z);
            return;
        }
        diagnosisDetail.setResult(2);
        diagnosisDetail.finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.z == null || !this.z.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("DD:openDataBase()", str + " opened.");
                this.z = SQLiteDatabase.openDatabase(str, null, 16);
                this.z.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.z.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("DD:openDataBase()", str2 + " ATTACHed.");
                this.A = new n();
                this.A.a(this.z);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new hg(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        this.D = this.A.ad();
        setContentView((int) C0000R.layout.diagnosis_detailed_edit);
        this.d = (TextView) findViewById(C0000R.id.tce_label);
        this.f425a = (ScrollView) findViewById(C0000R.id.svDetail);
        getWindow().setSoftInputMode(3);
        this.E = new GestureDetector(new a(this));
        this.F = new hh(this);
        this.G = new GestureDetector(new k(this));
        this.H = new hi(this);
        this.I = new GestureDetector(new cl(this));
        this.J = new hb(this);
        this.d.setOnClickListener(this);
        this.d.setOnTouchListener(this.J);
        a(true);
        this.f425a.smoothScrollBy(0, 0);
    }

    public void onDestroy() {
        TcmAid.S = null;
        TcmAid.ar = null;
        try {
            if (this.z != null && this.z.isOpen()) {
                this.z.close();
                this.z = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            this.y = this.r;
            TcmAid.bl = this.r;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        int i2;
        boolean a2;
        int i3;
        if (!this.t) {
            int i4 = 131073;
            if (this.A.x()) {
                i4 = 131073 + 16384;
            }
            if (this.A.y()) {
                i3 = i4 + 32768;
            } else {
                i3 = i4;
            }
            this.t = this.r;
            this.c = this.s;
            this.k.setVisibility(4);
            this.l.setVisibility(4);
            this.n.setVisibility(4);
            this.o.setVisibility(4);
            this.m.setVisibility(4);
            this.p.setImageResource(17301582);
            this.q.setVisibility(0);
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            inputMethodManager.getInputMethodList();
            inputMethodManager.toggleSoftInput(2, 0);
            ad.a(this.e, i3);
            ad.a(this.f, i3);
            ad.a(this.g, i3);
            ad.a(this.h, i3);
            ad.a(this.i, i3);
            ad.a(this.j, i3);
            ad.a(this.f426b, i3);
            return;
        }
        ContentValues contentValues = new ContentValues();
        boolean z2 = this.s;
        if (TcmAid.I == dh.f946a.intValue()) {
            a2 = z2;
            i2 = TcmAid.I + 1000;
        } else {
            i2 = TcmAid.I;
            a2 = q.a("SELECT COUNT(userDB.zangfupathology._id) FROM userDB.zangfupathology ", i2, this.z);
            if (!a2) {
                contentValues.put("_id", Integer.toString(i2));
            }
        }
        contentValues.put("symptoms", this.e.getText().toString());
        contentValues.put("txPrinciples", this.f.getText().toString());
        contentValues.put("tongue", this.g.getText().toString());
        contentValues.put("pulse", this.h.getText().toString());
        contentValues.put("points", this.i.getText().toString());
        contentValues.put("formula", this.j.getText().toString());
        contentValues.put("notes", this.f426b.getText().toString());
        contentValues.put("pathtype", Integer.toString(TcmAid.A));
        if (!a2) {
            try {
                this.z.insert("userDB.zangfupathology", null, contentValues);
            } catch (SQLException e2) {
                q.a(e2);
            }
        } else {
            this.z.update("userDB.zangfupathology", contentValues, "_id=" + Integer.toString(i2), null);
        }
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.f426b.getWindowToken(), 2);
        this.t = this.s;
        this.c = this.r;
        this.k.setVisibility(0);
        this.l.setVisibility(0);
        this.n.setVisibility(0);
        this.o.setVisibility(0);
        this.m.setVisibility(0);
        this.p.setImageResource(17301566);
        this.q.setVisibility(4);
        a(false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:105:0x042e  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0473  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x047c  */
    /* JADX WARNING: Removed duplicated region for block: B:130:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x02be  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x02d4 A[Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x036b A[Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0378  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x037f  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x041a  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0423  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r10) {
        /*
            r9 = this;
            r2 = 4
            r7 = 1
            r6 = 0
            if (r10 == 0) goto L_0x00d6
            r0 = 2131362330(0x7f0a021a, float:1.8344438E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r9.p = r0
            android.widget.ImageButton r0 = r9.p
            com.cyberandsons.tcmaidtrial.detailed.bw r1 = new com.cyberandsons.tcmaidtrial.detailed.bw
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            r0 = 2131362331(0x7f0a021b, float:1.834444E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r9.q = r0
            android.widget.Button r0 = r9.q
            com.cyberandsons.tcmaidtrial.detailed.bv r1 = new com.cyberandsons.tcmaidtrial.detailed.bv
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            com.cyberandsons.tcmaidtrial.a.n r0 = r9.A
            boolean r0 = r0.ab()
            r9.C = r0
            r0 = 2131361811(0x7f0a0013, float:1.8343385E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r9.k = r0
            android.widget.ImageButton r0 = r9.k
            com.cyberandsons.tcmaidtrial.detailed.bu r1 = new com.cyberandsons.tcmaidtrial.detailed.bu
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            r0 = 2131361815(0x7f0a0017, float:1.8343393E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r9.l = r0
            android.widget.ImageButton r0 = r9.l
            com.cyberandsons.tcmaidtrial.detailed.bt r1 = new com.cyberandsons.tcmaidtrial.detailed.bt
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            r0 = 2131361814(0x7f0a0016, float:1.834339E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r9.n = r0
            android.widget.ImageButton r0 = r9.n
            com.cyberandsons.tcmaidtrial.detailed.bs r1 = new com.cyberandsons.tcmaidtrial.detailed.bs
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            r0 = 2131361813(0x7f0a0015, float:1.8343389E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r9.o = r0
            android.widget.ImageButton r0 = r9.o
            com.cyberandsons.tcmaidtrial.detailed.br r1 = new com.cyberandsons.tcmaidtrial.detailed.br
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            r0 = 2131361812(0x7f0a0014, float:1.8343387E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r9.m = r0
            android.widget.ImageButton r0 = r9.m
            com.cyberandsons.tcmaidtrial.detailed.bq r1 = new com.cyberandsons.tcmaidtrial.detailed.bq
            r1.<init>(r9)
            r0.setOnClickListener(r1)
            boolean r0 = r9.D
            if (r0 == 0) goto L_0x00ae
            android.widget.ImageButton r0 = r9.l
            r0.setVisibility(r2)
            android.widget.ImageButton r0 = r9.m
            r0.setVisibility(r2)
        L_0x00ae:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.u
            if (r0 != 0) goto L_0x0389
            boolean r0 = r9.D
            if (r0 != 0) goto L_0x00bd
            android.widget.ImageButton r0 = r9.l
            boolean r1 = r9.s
            r9.a(r0, r1)
        L_0x00bd:
            android.widget.ImageButton r0 = r9.n
            boolean r1 = r9.s
            r9.a(r0, r1)
            android.widget.ImageButton r0 = r9.o
            boolean r1 = r9.r
            r9.a(r0, r1)
            boolean r0 = r9.D
            if (r0 != 0) goto L_0x00d6
            android.widget.ImageButton r0 = r9.m
            boolean r1 = r9.r
            r9.a(r0, r1)
        L_0x00d6:
            android.widget.EditText r0 = r9.e
            if (r0 == 0) goto L_0x00dc
            r9.e = r6
        L_0x00dc:
            r0 = 2131361867(0x7f0a004b, float:1.8343498E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r9.e = r0
            android.widget.EditText r0 = r9.e
            com.cyberandsons.tcmaidtrial.detailed.bx r1 = new com.cyberandsons.tcmaidtrial.detailed.bx
            r1.<init>(r9)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r9.f
            if (r0 == 0) goto L_0x00f7
            r9.f = r6
        L_0x00f7:
            r0 = 2131361869(0x7f0a004d, float:1.8343503E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r9.f = r0
            android.widget.EditText r0 = r9.f
            com.cyberandsons.tcmaidtrial.detailed.by r1 = new com.cyberandsons.tcmaidtrial.detailed.by
            r1.<init>(r9)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r9.g
            if (r0 == 0) goto L_0x0112
            r9.g = r6
        L_0x0112:
            r0 = 2131361871(0x7f0a004f, float:1.8343507E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r9.g = r0
            android.widget.EditText r0 = r9.g
            com.cyberandsons.tcmaidtrial.detailed.he r1 = new com.cyberandsons.tcmaidtrial.detailed.he
            r1.<init>(r9)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r9.h
            if (r0 == 0) goto L_0x012d
            r9.h = r6
        L_0x012d:
            r0 = 2131361873(0x7f0a0051, float:1.834351E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r9.h = r0
            android.widget.EditText r0 = r9.h
            com.cyberandsons.tcmaidtrial.detailed.hj r1 = new com.cyberandsons.tcmaidtrial.detailed.hj
            r1.<init>(r9)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r9.i
            if (r0 == 0) goto L_0x0148
            r9.i = r6
        L_0x0148:
            r0 = 2131361875(0x7f0a0053, float:1.8343515E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r9.i = r0
            android.widget.EditText r0 = r9.i
            r0.setOnClickListener(r9)
            android.widget.EditText r0 = r9.i
            android.view.View$OnTouchListener r1 = r9.F
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r9.j
            if (r0 == 0) goto L_0x0165
            r9.j = r6
        L_0x0165:
            r0 = 2131361881(0x7f0a0059, float:1.8343527E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r9.j = r0
            android.widget.EditText r0 = r9.j
            r0.setOnClickListener(r9)
            android.widget.EditText r0 = r9.j
            android.view.View$OnTouchListener r1 = r9.H
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r9.f426b
            if (r0 == 0) goto L_0x0182
            r9.f426b = r6
        L_0x0182:
            r0 = 2131361879(0x7f0a0057, float:1.8343523E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r9.f426b = r0
            android.widget.EditText r0 = r9.f426b
            com.cyberandsons.tcmaidtrial.detailed.hk r1 = new com.cyberandsons.tcmaidtrial.detailed.hk
            r1.<init>(r9)
            r0.setOnTouchListener(r1)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT main.zangfupathology._id, main.zangfupathology.pathology, main.zangfupathology.symptoms, main.zangfupathology.tongue, main.zangfupathology.pulse, main.zangfupathology.txPrinciples, main.zangfupathology.points, main.zangfupathology.formula, main.zangfupathology.interior_exterior, main.zangfupathology.hot_cold, main.zangfupathology.xu_shi, main.zangfupathology.notes FROM main.zangfupathology WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.C
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.y
            if (r0 == 0) goto L_0x01b5
            java.lang.String r0 = "DD:loadSystemSuppliedData()"
            android.util.Log.d(r0, r1)
        L_0x01b5:
            android.database.sqlite.SQLiteDatabase r0 = r9.z     // Catch:{ SQLException -> 0x0498, NullPointerException -> 0x03da, all -> 0x041f }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x0498, NullPointerException -> 0x03da, all -> 0x041f }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0498, NullPointerException -> 0x03da, all -> 0x041f }
            int r2 = r0.getCount()     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.y     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            if (r3 == 0) goto L_0x0208
            java.lang.String r3 = "DD:loadSystemSuppliedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r4.<init>()     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            java.lang.String r5 = "query count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            java.lang.String r5 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            java.lang.String r3 = "DD:loadSystemSuppliedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r4.<init>()     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            java.lang.String r5 = "column count = '"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            int r5 = r0.getColumnCount()     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            java.lang.String r5 = "' "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
        L_0x0208:
            boolean r3 = r0.moveToFirst()     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            if (r3 == 0) goto L_0x028a
            if (r2 != r7) goto L_0x028a
            r2 = 0
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r9.B = r2     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            android.widget.TextView r2 = r9.d     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            android.widget.EditText r2 = r9.e     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r3 = 2
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            android.widget.EditText r2 = r9.f     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r3 = 5
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            android.widget.EditText r2 = r9.g     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r3 = 3
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            android.widget.EditText r2 = r9.h     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r3 = 4
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            android.widget.EditText r2 = r9.i     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r3 = 6
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            android.widget.EditText r2 = r9.j     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r3 = 7
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            android.widget.EditText r2 = r9.f426b     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r3 = 11
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            boolean r2 = r9.C     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            if (r2 == 0) goto L_0x028a
            com.cyberandsons.tcmaidtrial.a.n r2 = r9.A     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            java.lang.String r2 = r2.o()     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            int r3 = r9.B     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            boolean r2 = com.cyberandsons.tcmaidtrial.a.n.a(r2, r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            if (r2 == 0) goto L_0x03ba
            android.widget.ImageButton r2 = r9.k     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            android.content.res.Resources r3 = r9.getResources()     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r4 = 17301618(0x1080072, float:2.4979574E-38)
            android.graphics.drawable.Drawable r3 = r3.getDrawable(r4)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r2.setImageDrawable(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
        L_0x028a:
            if (r0 == 0) goto L_0x028f
            r0.close()
        L_0x028f:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT userDB.zangfupathology.symptoms, userDB.zangfupathology.tongue, userDB.zangfupathology.pulse, userDB.zangfupathology.txPrinciples, userDB.zangfupathology.points, userDB.zangfupathology.formula, userDB.zangfupathology.interior_exterior, userDB.zangfupathology.hot_cold, userDB.zangfupathology.xu_shi, userDB.zangfupathology.notes, userDB.zangfupathology.pathology FROM userDB.zangfupathology WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "userDB.zangfupathology."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "_id"
            java.lang.StringBuilder r0 = r0.append(r1)
            r1 = 61
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.I
            java.lang.String r1 = java.lang.Integer.toString(r1)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.y
            if (r0 == 0) goto L_0x02c3
            java.lang.String r0 = "DD:loadUserModifiedData()"
            android.util.Log.d(r0, r1)
        L_0x02c3:
            android.database.sqlite.SQLiteDatabase r0 = r9.z     // Catch:{ SQLException -> 0x0427, NullPointerException -> 0x0433, all -> 0x0478 }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x0427, NullPointerException -> 0x0433, all -> 0x0478 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0427, NullPointerException -> 0x0433, all -> 0x0478 }
            int r2 = r0.getCount()     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.y     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            if (r3 == 0) goto L_0x0316
            java.lang.String r3 = "DD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r4.<init>()     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            java.lang.String r5 = "query count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            java.lang.String r5 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            java.lang.String r3 = "DD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r4.<init>()     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            java.lang.String r5 = "column count = '"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            int r5 = r0.getColumnCount()     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            java.lang.String r5 = "' "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
        L_0x0316:
            boolean r3 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            if (r3 == 0) goto L_0x0376
            if (r2 != r7) goto L_0x0376
            android.widget.EditText r2 = r9.e     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r3 = 0
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            android.widget.EditText r2 = r9.g     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            android.widget.EditText r2 = r9.h     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r3 = 2
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            android.widget.EditText r2 = r9.f     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r3 = 3
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            android.widget.EditText r2 = r9.i     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r3 = 4
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            android.widget.EditText r2 = r9.j     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r3 = 5
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            android.widget.EditText r2 = r9.f426b     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r3 = 9
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.I     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r2 <= r3) goto L_0x0376
            android.widget.TextView r2 = r9.d     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r3 = 10
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0489, NullPointerException -> 0x0487 }
        L_0x0376:
            if (r0 == 0) goto L_0x037b
            r0.close()
        L_0x037b:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.cR
            if (r0 != r7) goto L_0x0388
            com.cyberandsons.tcmaidtrial.a.n r0 = r9.A
            java.lang.String r0 = r0.E()
            r9.a(r0)
        L_0x0388:
            return
        L_0x0389:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.u
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.v
            int r1 = r1.size()
            int r1 = r1 - r7
            if (r0 != r1) goto L_0x00d6
            boolean r0 = r9.D
            if (r0 != 0) goto L_0x039f
            android.widget.ImageButton r0 = r9.l
            boolean r1 = r9.r
            r9.a(r0, r1)
        L_0x039f:
            android.widget.ImageButton r0 = r9.n
            boolean r1 = r9.r
            r9.a(r0, r1)
            android.widget.ImageButton r0 = r9.o
            boolean r1 = r9.s
            r9.a(r0, r1)
            boolean r0 = r9.D
            if (r0 != 0) goto L_0x00d6
            android.widget.ImageButton r0 = r9.m
            boolean r1 = r9.s
            r9.a(r0, r1)
            goto L_0x00d6
        L_0x03ba:
            android.widget.ImageButton r2 = r9.k     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            android.content.res.Resources r3 = r9.getResources()     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r4 = 17301619(0x1080073, float:2.4979577E-38)
            android.graphics.drawable.Drawable r3 = r3.getDrawable(r4)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            r2.setImageDrawable(r3)     // Catch:{ SQLException -> 0x03cc, NullPointerException -> 0x0495 }
            goto L_0x028a
        L_0x03cc:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x03d0:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0493 }
            if (r1 == 0) goto L_0x028f
            r1.close()
            goto L_0x028f
        L_0x03da:
            r0 = move-exception
            r0 = r6
        L_0x03dc:
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x048e }
            java.lang.String r3 = "myVariable"
            r2.a(r3, r1)     // Catch:{ all -> 0x048e }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x048e }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x048e }
            java.lang.String r3 = "DD:loadSystemSuppliedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x048e }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x048e }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x048e }
            r1.<init>(r9)     // Catch:{ all -> 0x048e }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x048e }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x048e }
            r2 = 2131099673(0x7f060019, float:1.7811706E38)
            java.lang.String r2 = r9.getString(r2)     // Catch:{ all -> 0x048e }
            r1.setMessage(r2)     // Catch:{ all -> 0x048e }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.hl r3 = new com.cyberandsons.tcmaidtrial.detailed.hl     // Catch:{ all -> 0x048e }
            r3.<init>(r9)     // Catch:{ all -> 0x048e }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x048e }
            r1.show()     // Catch:{ all -> 0x048e }
            if (r0 == 0) goto L_0x028f
            r0.close()
            goto L_0x028f
        L_0x041f:
            r0 = move-exception
            r1 = r6
        L_0x0421:
            if (r1 == 0) goto L_0x0426
            r1.close()
        L_0x0426:
            throw r0
        L_0x0427:
            r0 = move-exception
            r1 = r6
        L_0x0429:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0485 }
            if (r1 == 0) goto L_0x037b
            r1.close()
            goto L_0x037b
        L_0x0433:
            r0 = move-exception
            r0 = r6
        L_0x0435:
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0480 }
            java.lang.String r3 = "myVariable"
            r2.a(r3, r1)     // Catch:{ all -> 0x0480 }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0480 }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x0480 }
            java.lang.String r3 = "DD:loadUserModifiedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x0480 }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x0480 }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x0480 }
            r1.<init>(r9)     // Catch:{ all -> 0x0480 }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x0480 }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x0480 }
            r2 = 2131099674(0x7f06001a, float:1.7811708E38)
            java.lang.String r2 = r9.getString(r2)     // Catch:{ all -> 0x0480 }
            r1.setMessage(r2)     // Catch:{ all -> 0x0480 }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.hm r3 = new com.cyberandsons.tcmaidtrial.detailed.hm     // Catch:{ all -> 0x0480 }
            r3.<init>(r9)     // Catch:{ all -> 0x0480 }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x0480 }
            r1.show()     // Catch:{ all -> 0x0480 }
            if (r0 == 0) goto L_0x037b
            r0.close()
            goto L_0x037b
        L_0x0478:
            r0 = move-exception
            r1 = r6
        L_0x047a:
            if (r1 == 0) goto L_0x047f
            r1.close()
        L_0x047f:
            throw r0
        L_0x0480:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x047a
        L_0x0485:
            r0 = move-exception
            goto L_0x047a
        L_0x0487:
            r2 = move-exception
            goto L_0x0435
        L_0x0489:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0429
        L_0x048e:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0421
        L_0x0493:
            r0 = move-exception
            goto L_0x0421
        L_0x0495:
            r2 = move-exception
            goto L_0x03dc
        L_0x0498:
            r0 = move-exception
            r1 = r6
            goto L_0x03d0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.detailed.DiagnosisDetail.a(boolean):void");
    }

    private void a(String str) {
        if (str != null && str.length() != 0) {
            String[] split = str.split(",");
            for (String parseInt : split) {
                switch (Integer.parseInt(parseInt)) {
                    case 1:
                        dh.a(this.e.getText());
                        break;
                    case 2:
                        dh.a(this.f.getText());
                        break;
                    case 3:
                        dh.a(this.g.getText());
                        break;
                    case 4:
                        dh.a(this.h.getText());
                        break;
                    case 5:
                        dh.a(this.i.getText());
                        break;
                    case 6:
                        dh.a(this.j.getText());
                        break;
                    case 7:
                        dh.a(this.f426b.getText());
                        break;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        boolean z2;
        String str;
        if (this.y) {
            this.y = false;
            return;
        }
        String[] split = this.i.getText().toString().split(",");
        String[] strArr = {"BL", "DU", "GB", "HT", "KI", "LI", "LV", "LU", "PC", "REN", "SI", "SJ", "SP", "ST", "TB", "TW", "M-HN", "N-HN", "M-BW", "N-BW", "M-CA", "N-CA", "M-UE", "N-UE", "M-LE", "N-LE"};
        StringBuffer stringBuffer = new StringBuffer();
        boolean z3 = true;
        for (String trim : split) {
            String trim2 = trim.trim();
            int i2 = 0;
            while (true) {
                if (i2 >= strArr.length) {
                    z2 = false;
                    break;
                }
                if (dh.K) {
                    Log.d("DD:meridianArray", " meridianArray[" + Integer.toString(i2) + "] = (" + strArr[i2] + ')');
                }
                if (trim2.indexOf(strArr[i2]) != -1) {
                    z2 = true;
                    break;
                }
                i2++;
            }
            if (z2) {
                if (dh.K) {
                    Log.d("DD:tok", trim2);
                }
                if (z3) {
                    str = " WHERE ";
                } else {
                    str = " OR ";
                }
                stringBuffer.append(str + " xxxx.pointslocation.common_name LIKE '" + trim2 + "' OR xxxx.pointslocation.name LIKE '" + trim2 + "' ");
                if (dh.K) {
                    Log.d("DD:pointItems", stringBuffer.toString());
                }
                z3 = false;
            }
        }
        if (dh.K) {
            Log.d("DD:pointItems", stringBuffer.toString());
        }
        TcmAid.ar = a.a(stringBuffer.toString().replace("xxxx", "main"), stringBuffer.toString().replace("xxxx", "userDB").replace("WHERE  userDB", "WHERE (userDB") + ')', this.A);
        TcmAid.ap = null;
        startActivityForResult(new Intent(this, PointsList.class), 1350);
    }

    /* access modifiers changed from: protected */
    public final void c() {
        String str;
        String str2;
        if (this.y) {
            this.y = this.s;
            return;
        }
        int N = this.A.N();
        if (N == 0 || N == 1) {
            str = "pinyin";
        } else {
            str = "english";
        }
        String[] split = this.j.getText().toString().replaceAll(" and ", ", ").replaceAll(" or ", ", ").split(",");
        StringBuffer stringBuffer = new StringBuffer();
        boolean z2 = this.r;
        for (String trim : split) {
            String trim2 = trim.trim();
            if (z2) {
                str2 = " AND xxxx.formulas.pinyin LIKE '" + trim2.trim() + "' ";
                z2 = this.s;
            } else {
                str2 = " OR xxxx.formulas.pinyin LIKE '" + trim2.trim() + "' ";
            }
            stringBuffer.append(str2);
            if (dh.K) {
                Log.d("DD:formulaStr", trim2);
            }
        }
        TcmAid.S = c.a(stringBuffer.toString().replace("xxxx", "main"), stringBuffer.toString().replace("xxxx", "userDB").replace("AND userDB", "AND (userDB") + ')', str);
        TcmAid.Q = null;
        startActivityForResult(new Intent(this, FormulasList.class), 1350);
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        boolean z2 = this.s;
        switch (i2) {
            case 0:
                if (this.x) {
                    this.v -= 2;
                } else {
                    TcmAid.u = 0;
                    if (!this.D) {
                        a(this.l, this.s);
                    }
                    a(this.n, this.s);
                    a(this.o, this.r);
                    if (!this.D) {
                        a(this.m, this.r);
                    }
                }
                z2 = this.r;
                break;
            case 1:
                if (this.x) {
                    this.w -= 2;
                } else if (TcmAid.u - 1 >= 0) {
                    if (TcmAid.u - 1 == 0) {
                        if (!this.D) {
                            a(this.l, this.s);
                        }
                        a(this.n, this.s);
                    }
                    if (this.o.isEnabled() == this.s) {
                        a(this.o, this.r);
                        if (!this.D) {
                            a(this.m, this.r);
                        }
                    }
                    TcmAid.u--;
                }
                z2 = this.r;
                break;
            case 2:
                if (!this.x) {
                    int size = TcmAid.v.size();
                    if (TcmAid.u + 1 < size) {
                        if (TcmAid.u + 1 == size - 1) {
                            a(this.o, this.s);
                            if (!this.D) {
                                a(this.m, this.s);
                            }
                        }
                        if (this.n.isEnabled() == this.s) {
                            if (!this.D) {
                                a(this.l, this.r);
                            }
                            a(this.n, this.r);
                        }
                        TcmAid.u++;
                        z2 = this.r;
                        break;
                    }
                } else {
                    this.w += 2;
                    z2 = this.r;
                    break;
                }
                break;
            case 3:
                if (this.x) {
                    this.v += 2;
                } else {
                    TcmAid.u = TcmAid.v.size() - 1;
                    if (!this.D) {
                        a(this.l, this.r);
                    }
                    a(this.n, this.r);
                    a(this.o, this.s);
                    if (!this.D) {
                        a(this.m, this.s);
                    }
                }
                z2 = this.r;
                break;
        }
        if (z2) {
            if (TcmAid.u >= TcmAid.v.size()) {
                TcmAid.u = TcmAid.v.size() - 1;
            }
            int intValue = ((Integer) TcmAid.v.get(TcmAid.u)).intValue();
            TcmAid.C = "main.zangfupathology._id=" + Integer.toString(intValue);
            TcmAid.I = intValue;
            a(false);
            this.f425a.post(new hf(this));
        }
    }

    private void a(ImageButton imageButton, boolean z2) {
        if (z2) {
            imageButton.setEnabled(this.r);
            imageButton.setVisibility(0);
            return;
        }
        imageButton.setEnabled(this.s);
        imageButton.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }

    public void onClick(View view) {
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }
}
