package com.cyberandsons.tcmaidtrial.detailed;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.c;
import com.cyberandsons.tcmaidtrial.a.m;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.lists.HerbList;
import com.cyberandsons.tcmaidtrial.misc.ad;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class FormulasDetail extends Activity implements View.OnClickListener, View.OnTouchListener {
    private ImageButton A;
    private Button B;
    private boolean C;
    private boolean D;
    private int E;
    private int F;
    private boolean G;
    private boolean H;
    private SQLiteDatabase I;
    private n J;
    private int K;
    private boolean L;
    private boolean M;
    private int N;
    private int O;
    private int P;
    private int Q;
    private int R;
    private boolean S;
    /* access modifiers changed from: private */
    public GestureDetector T;
    private View.OnTouchListener U;
    /* access modifiers changed from: private */
    public GestureDetector V;
    private View.OnTouchListener W;

    /* renamed from: a  reason: collision with root package name */
    ScrollView f427a;

    /* renamed from: b  reason: collision with root package name */
    EditText f428b;
    EditText c;
    boolean d = true;
    boolean e;
    boolean f;
    boolean g;
    private TextView h;
    private TextView i;
    private TextView j;
    private TextView k;
    private EditText l;
    private EditText m;
    private EditText n;
    private EditText o;
    private EditText p;
    private EditText q;
    private EditText r;
    private EditText s;
    private EditText t;
    private ImageButton u;
    private ImageButton v;
    private ImageButton w;
    private ImageButton x;
    private ImageButton y;
    private ImageButton z;

    public FormulasDetail() {
        this.e = !this.d;
        this.C = this.e;
        this.D = this.e;
        this.G = this.e;
        this.f = this.d;
        this.H = this.e;
        this.g = this.e;
        this.L = this.e;
        this.M = this.e;
    }

    static /* synthetic */ void a(FormulasDetail formulasDetail) {
        String format;
        if (formulasDetail.L) {
            String p2 = formulasDetail.J.p();
            if (n.a(p2, formulasDetail.K)) {
                String[] split = p2.split(",");
                boolean z2 = formulasDetail.d;
                String str = "";
                for (String parseInt : split) {
                    int parseInt2 = Integer.parseInt(parseInt);
                    if (parseInt2 != formulasDetail.K) {
                        if (z2) {
                            str = String.format("%d", Integer.valueOf(parseInt2));
                            z2 = formulasDetail.e;
                        } else {
                            str = str + String.format(",%d", Integer.valueOf(parseInt2));
                        }
                    }
                }
                formulasDetail.u.setImageDrawable(formulasDetail.getResources().getDrawable(17301619));
                format = str;
            } else {
                if (p2 == null || p2.length() <= 0) {
                    format = String.format("%d", Integer.valueOf(formulasDetail.K));
                } else {
                    format = String.format("%s,%d", p2, Integer.valueOf(formulasDetail.K));
                }
                formulasDetail.u.setImageDrawable(formulasDetail.getResources().getDrawable(17301618));
            }
            formulasDetail.J.d(format, formulasDetail.I);
            return;
        }
        formulasDetail.setResult(2);
        formulasDetail.finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.I == null || !this.I.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("FD:openDataBase()", str + " opened.");
                this.I = SQLiteDatabase.openDatabase(str, null, 16);
                this.I.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.I.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("FD:openDataBase()", str2 + " ATTACHed.");
                this.J = new n();
                this.J.a(this.I);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new aq(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        this.M = this.J.ad();
        this.S = this.J.an();
        this.N = this.J.ai();
        this.O = this.J.aj();
        this.P = this.J.ak();
        this.Q = this.J.al();
        this.R = this.J.am();
        setContentView((int) C0000R.layout.formulas_detailed_edit);
        this.h = (TextView) findViewById(C0000R.id.tce_label);
        this.f427a = (ScrollView) findViewById(C0000R.id.svDetail);
        getWindow().setSoftInputMode(3);
        this.T = new GestureDetector(new ha(this));
        this.U = new ao(this);
        this.V = new GestureDetector(new gk(this));
        this.W = new at(this);
        this.h.setOnClickListener(this);
        this.h.setOnTouchListener(this.W);
        a(true);
        this.f427a.smoothScrollBy(0, 0);
    }

    public void onDestroy() {
        TcmAid.Q = null;
        TcmAid.af = null;
        try {
            if (this.I != null && this.I.isOpen()) {
                this.I.close();
                this.I = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            this.g = this.d;
            TcmAid.bl = this.d;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        int i2;
        boolean a2;
        int i3;
        if (!this.C) {
            int i4 = 131073;
            if (this.J.x()) {
                i4 = 131073 + 16384;
            }
            if (this.J.y()) {
                i3 = i4 + 32768;
            } else {
                i3 = i4;
            }
            this.C = this.d;
            this.f = this.e;
            this.u.setVisibility(4);
            this.v.setVisibility(4);
            this.w.setVisibility(4);
            this.y.setVisibility(4);
            this.z.setVisibility(4);
            this.x.setVisibility(4);
            this.A.setImageResource(17301582);
            this.B.setVisibility(0);
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            inputMethodManager.getInputMethodList();
            inputMethodManager.toggleSoftInput(2, 0);
            ad.a(this.l, i3);
            ad.a(this.m, i3);
            ad.a(this.n, i3);
            ad.a(this.o, i3);
            ad.a(this.p, i3);
            ad.a(this.f428b, i3);
            ad.a(this.q, i3);
            ad.a(this.r, i3);
            ad.a(this.s, i3);
            ad.a(this.t, i3);
            ad.a(this.c, i3);
            return;
        }
        ContentValues contentValues = new ContentValues();
        boolean z2 = this.e;
        if (TcmAid.V == dh.f946a.intValue()) {
            a2 = z2;
            i2 = TcmAid.V + 1000;
        } else {
            i2 = TcmAid.V;
            a2 = q.a("SELECT COUNT(userDB.formulas._id) FROM userDB.formulas ", i2, this.I);
            if (!a2) {
                contentValues.put("_id", Integer.toString(i2));
            }
        }
        contentValues.put("fcategory", Integer.valueOf(c.b(this.i.getText().toString(), this.I)));
        contentValues.put("diagnosis", this.l.getText().toString());
        contentValues.put("symptoms", this.m.getText().toString());
        contentValues.put("functions", this.n.getText().toString());
        contentValues.put("tongue", this.o.getText().toString());
        contentValues.put("pulse", this.p.getText().toString());
        contentValues.put("ingredients", this.f428b.getText().toString());
        contentValues.put("herb_breakdown", this.q.getText().toString());
        contentValues.put("modification", this.r.getText().toString());
        contentValues.put("contraindication", this.s.getText().toString());
        contentValues.put("western_USE", this.t.getText().toString());
        contentValues.put("notes", this.c.getText().toString());
        if (!a2) {
            try {
                this.I.insert("userDB.formulas", null, contentValues);
            } catch (SQLException e2) {
                q.a(e2);
            }
        } else {
            this.I.update("userDB.formulas", contentValues, "_id=" + Integer.toString(i2), null);
        }
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.c.getWindowToken(), 0);
        this.C = this.e;
        this.f = this.d;
        this.u.setVisibility(0);
        this.v.setVisibility(0);
        this.w.setVisibility(0);
        this.y.setVisibility(0);
        this.z.setVisibility(0);
        this.x.setVisibility(0);
        this.A.setImageResource(17301566);
        this.B.setVisibility(4);
        a(false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x04fa  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0504  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x050e  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x05c0  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x05c9  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x05d4  */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x0619  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x0622  */
    /* JADX WARNING: Removed duplicated region for block: B:167:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0400  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0416 A[Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x04ed A[Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r12) {
        /*
            r11 = this;
            r2 = 4
            r9 = 1
            r8 = 0
            if (r12 == 0) goto L_0x00eb
            r0 = 2131362330(0x7f0a021a, float:1.8344438E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r11.A = r0
            android.widget.ImageButton r0 = r11.A
            com.cyberandsons.tcmaidtrial.detailed.gy r1 = new com.cyberandsons.tcmaidtrial.detailed.gy
            r1.<init>(r11)
            r0.setOnClickListener(r1)
            r0 = 2131362331(0x7f0a021b, float:1.834444E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r11.B = r0
            android.widget.Button r0 = r11.B
            com.cyberandsons.tcmaidtrial.detailed.gw r1 = new com.cyberandsons.tcmaidtrial.detailed.gw
            r1.<init>(r11)
            r0.setOnClickListener(r1)
            com.cyberandsons.tcmaidtrial.a.n r0 = r11.J
            boolean r0 = r0.ab()
            r11.L = r0
            r0 = 2131361811(0x7f0a0013, float:1.8343385E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r11.u = r0
            android.widget.ImageButton r0 = r11.u
            com.cyberandsons.tcmaidtrial.detailed.gx r1 = new com.cyberandsons.tcmaidtrial.detailed.gx
            r1.<init>(r11)
            r0.setOnClickListener(r1)
            r0 = 2131361845(0x7f0a0035, float:1.8343454E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r11.v = r0
            android.widget.ImageButton r0 = r11.v
            com.cyberandsons.tcmaidtrial.detailed.gu r1 = new com.cyberandsons.tcmaidtrial.detailed.gu
            r1.<init>(r11)
            r0.setOnClickListener(r1)
            r0 = 2131361815(0x7f0a0017, float:1.8343393E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r11.w = r0
            android.widget.ImageButton r0 = r11.w
            com.cyberandsons.tcmaidtrial.detailed.gv r1 = new com.cyberandsons.tcmaidtrial.detailed.gv
            r1.<init>(r11)
            r0.setOnClickListener(r1)
            r0 = 2131361814(0x7f0a0016, float:1.834339E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r11.y = r0
            android.widget.ImageButton r0 = r11.y
            com.cyberandsons.tcmaidtrial.detailed.gs r1 = new com.cyberandsons.tcmaidtrial.detailed.gs
            r1.<init>(r11)
            r0.setOnClickListener(r1)
            r0 = 2131361813(0x7f0a0015, float:1.8343389E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r11.z = r0
            android.widget.ImageButton r0 = r11.z
            com.cyberandsons.tcmaidtrial.detailed.gt r1 = new com.cyberandsons.tcmaidtrial.detailed.gt
            r1.<init>(r11)
            r0.setOnClickListener(r1)
            r0 = 2131361812(0x7f0a0014, float:1.8343387E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r11.x = r0
            android.widget.ImageButton r0 = r11.x
            com.cyberandsons.tcmaidtrial.detailed.gq r1 = new com.cyberandsons.tcmaidtrial.detailed.gq
            r1.<init>(r11)
            r0.setOnClickListener(r1)
            boolean r0 = r11.M
            if (r0 == 0) goto L_0x00c3
            android.widget.ImageButton r0 = r11.w
            r0.setVisibility(r2)
            android.widget.ImageButton r0 = r11.x
            r0.setVisibility(r2)
        L_0x00c3:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.L
            if (r0 != 0) goto L_0x0518
            boolean r0 = r11.M
            if (r0 != 0) goto L_0x00d2
            android.widget.ImageButton r0 = r11.w
            boolean r1 = r11.e
            r11.a(r0, r1)
        L_0x00d2:
            android.widget.ImageButton r0 = r11.y
            boolean r1 = r11.e
            r11.a(r0, r1)
            android.widget.ImageButton r0 = r11.z
            boolean r1 = r11.d
            r11.a(r0, r1)
            boolean r0 = r11.M
            if (r0 != 0) goto L_0x00eb
            android.widget.ImageButton r0 = r11.x
            boolean r1 = r11.d
            r11.a(r0, r1)
        L_0x00eb:
            android.widget.TextView r0 = r11.i
            if (r0 == 0) goto L_0x00f1
            r11.i = r8
        L_0x00f1:
            r0 = 2131361914(0x7f0a007a, float:1.8343594E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r11.i = r0
            android.widget.TextView r0 = r11.j
            if (r0 == 0) goto L_0x0102
            r11.j = r8
        L_0x0102:
            r0 = 2131361915(0x7f0a007b, float:1.8343596E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r11.j = r0
            android.widget.TextView r0 = r11.k
            if (r0 == 0) goto L_0x0113
            r11.k = r8
        L_0x0113:
            r0 = 2131361916(0x7f0a007c, float:1.8343598E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r11.k = r0
            android.widget.EditText r0 = r11.l
            if (r0 == 0) goto L_0x0124
            r11.l = r8
        L_0x0124:
            r0 = 2131361918(0x7f0a007e, float:1.8343602E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r11.l = r0
            android.widget.EditText r0 = r11.l
            com.cyberandsons.tcmaidtrial.detailed.gr r1 = new com.cyberandsons.tcmaidtrial.detailed.gr
            r1.<init>(r11)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r11.m
            if (r0 == 0) goto L_0x013f
            r11.m = r8
        L_0x013f:
            r0 = 2131361920(0x7f0a0080, float:1.8343606E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r11.m = r0
            android.widget.EditText r0 = r11.m
            com.cyberandsons.tcmaidtrial.detailed.m r1 = new com.cyberandsons.tcmaidtrial.detailed.m
            r1.<init>(r11)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r11.n
            if (r0 == 0) goto L_0x015a
            r11.n = r8
        L_0x015a:
            r0 = 2131361922(0x7f0a0082, float:1.834361E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r11.n = r0
            android.widget.EditText r0 = r11.n
            com.cyberandsons.tcmaidtrial.detailed.ae r1 = new com.cyberandsons.tcmaidtrial.detailed.ae
            r1.<init>(r11)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r11.o
            if (r0 == 0) goto L_0x0175
            r11.o = r8
        L_0x0175:
            r0 = 2131361924(0x7f0a0084, float:1.8343614E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r11.o = r0
            android.widget.EditText r0 = r11.o
            com.cyberandsons.tcmaidtrial.detailed.ac r1 = new com.cyberandsons.tcmaidtrial.detailed.ac
            r1.<init>(r11)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r11.p
            if (r0 == 0) goto L_0x0190
            r11.p = r8
        L_0x0190:
            r0 = 2131361926(0x7f0a0086, float:1.8343618E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r11.p = r0
            android.widget.EditText r0 = r11.p
            com.cyberandsons.tcmaidtrial.detailed.aa r1 = new com.cyberandsons.tcmaidtrial.detailed.aa
            r1.<init>(r11)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r11.f428b
            if (r0 == 0) goto L_0x01ab
            r11.f428b = r8
        L_0x01ab:
            r0 = 2131361928(0x7f0a0088, float:1.8343622E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r11.f428b = r0
            boolean r0 = r11.S
            if (r0 == 0) goto L_0x01c2
            android.widget.EditText r0 = r11.f428b
            r1 = 2130903057(0x7f030011, float:1.7412921E38)
            r0.setBackgroundResource(r1)
        L_0x01c2:
            android.widget.EditText r0 = r11.f428b
            r0.setOnClickListener(r11)
            android.widget.EditText r0 = r11.f428b
            android.view.View$OnTouchListener r1 = r11.U
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r11.q
            if (r0 == 0) goto L_0x01d4
            r11.q = r8
        L_0x01d4:
            r0 = 2131361930(0x7f0a008a, float:1.8343626E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r11.q = r0
            android.widget.EditText r0 = r11.q
            com.cyberandsons.tcmaidtrial.detailed.y r1 = new com.cyberandsons.tcmaidtrial.detailed.y
            r1.<init>(r11)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r11.r
            if (r0 == 0) goto L_0x01ef
            r11.r = r8
        L_0x01ef:
            r0 = 2131361932(0x7f0a008c, float:1.834363E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r11.r = r0
            android.widget.EditText r0 = r11.r
            com.cyberandsons.tcmaidtrial.detailed.x r1 = new com.cyberandsons.tcmaidtrial.detailed.x
            r1.<init>(r11)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r11.s
            if (r0 == 0) goto L_0x020a
            r11.s = r8
        L_0x020a:
            r0 = 2131361934(0x7f0a008e, float:1.8343634E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r11.s = r0
            android.widget.EditText r0 = r11.s
            com.cyberandsons.tcmaidtrial.detailed.w r1 = new com.cyberandsons.tcmaidtrial.detailed.w
            r1.<init>(r11)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r11.t
            if (r0 == 0) goto L_0x0225
            r11.t = r8
        L_0x0225:
            r0 = 2131361936(0x7f0a0090, float:1.8343638E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r11.t = r0
            android.widget.EditText r0 = r11.t
            com.cyberandsons.tcmaidtrial.detailed.u r1 = new com.cyberandsons.tcmaidtrial.detailed.u
            r1.<init>(r11)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r11.c
            if (r0 == 0) goto L_0x0240
            r11.c = r8
        L_0x0240:
            r0 = 2131361938(0x7f0a0092, float:1.8343642E38)
            android.view.View r0 = r11.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r11.c = r0
            android.widget.EditText r0 = r11.c
            com.cyberandsons.tcmaidtrial.detailed.t r1 = new com.cyberandsons.tcmaidtrial.detailed.t
            r1.<init>(r11)
            r0.setOnTouchListener(r1)
            com.cyberandsons.tcmaidtrial.a.n r0 = r11.J
            int r0 = r0.N()
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.cV
            r2 = -1
            if (r1 == r2) goto L_0x0643
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.cV
            r1 = r0
        L_0x0263:
            com.cyberandsons.tcmaidtrial.a.n r0 = r11.J
            int r2 = r0.ac()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = "SELECT main.formulas._id, main.formulas.pinyin, main.formulas.english, main.fcategory.item_name, main.formulas.syndromedx, main.formulas.symptoms, main.formulas.tongue, main.formulas.pulse, main.formulas.functions, main.formulas.ingredients, main.formulas.herb_breakdown, main.formulas.notes, main.formulas.contraindication, main.formulas.chiefcomplaint, main.formulas.txprin, main.formulas.modification, main.formulas.diagnosis, main.formulas.western_use, main.formulas.fcategory, main.formulas.majorcategory, main.tonemarks.ftonemarks, main.tonemarks.fscc, main.tonemarks.ftcc FROM main.formulas JOIN main.fcategory ON main.fcategory._id = main.formulas.fcategory JOIN main.tonemarks ON main.tonemarks.fid = main.formulas._id WHERE main.formulas.useiPhone=1 AND ("
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.TcmAid.Q
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = r0.toString()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.v
            if (r0 == 0) goto L_0x0287
            java.lang.String r0 = "FD:loadSystemSuppliedData()"
            android.util.Log.d(r0, r3)
        L_0x0287:
            android.database.sqlite.SQLiteDatabase r0 = r11.I     // Catch:{ SQLException -> 0x063f, NullPointerException -> 0x063b, all -> 0x05c5 }
            r4 = 0
            android.database.Cursor r0 = r0.rawQuery(r3, r4)     // Catch:{ SQLException -> 0x063f, NullPointerException -> 0x063b, all -> 0x05c5 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x063f, NullPointerException -> 0x063b, all -> 0x05c5 }
            int r4 = r0.getCount()     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            boolean r5 = com.cyberandsons.tcmaidtrial.misc.dh.v     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            if (r5 == 0) goto L_0x02da
            java.lang.String r5 = "FD:loadSystemSuppliedData()"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r6.<init>()     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.String r7 = "query count = "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.String r7 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.String r6 = r6.toString()     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.util.Log.d(r5, r6)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.String r5 = "FD:loadSystemSuppliedData()"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r6.<init>()     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.String r7 = "column count = '"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            int r7 = r0.getColumnCount()     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.String r7 = "' "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.String r6 = r6.toString()     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.util.Log.d(r5, r6)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
        L_0x02da:
            boolean r5 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            if (r5 == 0) goto L_0x03cc
            if (r4 != r9) goto L_0x03cc
            r4 = 0
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r11.K = r4     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r4.<init>()     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r5 = 20
            java.lang.String r5 = r0.getString(r5)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.String r5 = com.cyberandsons.tcmaidtrial.misc.ad.a(r5)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.String r5 = "\n"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            int r2 = r2 + 21
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            if (r1 == 0) goto L_0x0314
            if (r1 != r9) goto L_0x0549
        L_0x0314:
            android.widget.TextView r1 = r11.h     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.widget.TextView r1 = r11.k     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r2 = 2
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.widget.TextView r1 = r11.j     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.String r2 = "English"
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
        L_0x032a:
            android.widget.TextView r1 = r11.i     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r2 = 3
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.widget.EditText r1 = r11.l     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r2 = 16
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.widget.EditText r1 = r11.m     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r2 = 5
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.widget.EditText r1 = r11.n     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r2 = 8
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.widget.EditText r1 = r11.o     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r2 = 6
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.widget.EditText r1 = r11.p     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r2 = 7
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.widget.EditText r1 = r11.f428b     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r2 = 9
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.widget.EditText r1 = r11.q     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r2 = 10
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.widget.EditText r1 = r11.r     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r2 = 15
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.widget.EditText r1 = r11.s     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r2 = 12
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.widget.EditText r1 = r11.t     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r2 = 17
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.widget.EditText r1 = r11.c     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r2 = 11
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            boolean r1 = r11.L     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            if (r1 == 0) goto L_0x03cc
            com.cyberandsons.tcmaidtrial.a.n r1 = r11.J     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.String r1 = r1.p()     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            int r2 = r11.K     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            boolean r1 = com.cyberandsons.tcmaidtrial.a.n.a(r1, r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            if (r1 == 0) goto L_0x056f
            android.widget.ImageButton r1 = r11.u     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.content.res.Resources r2 = r11.getResources()     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r4 = 17301618(0x1080072, float:2.4979574E-38)
            android.graphics.drawable.Drawable r2 = r2.getDrawable(r4)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setImageDrawable(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
        L_0x03cc:
            if (r0 == 0) goto L_0x03d1
            r0.close()
        L_0x03d1:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT userDB.formulas.symptoms, userDB.formulas.functions, userDB.formulas.tongue, userDB.formulas.pulse, userDB.formulas.ingredients, userDB.formulas.herb_breakdown, userDB.formulas.modification, userDB.formulas.contraindication, userDB.formulas.western_use, userDB.formulas.notes, userDB.formulas.fcategory, userDB.formulas.diagnosis, userDB.formulas.english, userDB.formulas.pinyin, main.fcategory.item_name FROM userDB.formulas LEFT JOIN main.fcategory ON main.fcategory._id=userDB.formulas.fcategory WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "userDB.formulas."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "_id"
            java.lang.StringBuilder r0 = r0.append(r1)
            r1 = 61
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.V
            java.lang.String r1 = java.lang.Integer.toString(r1)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.v
            if (r0 == 0) goto L_0x0405
            java.lang.String r0 = "FD:loadUserModifiedData()"
            android.util.Log.d(r0, r1)
        L_0x0405:
            android.database.sqlite.SQLiteDatabase r0 = r11.I     // Catch:{ SQLException -> 0x05cd, NullPointerException -> 0x05d9, all -> 0x061e }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x05cd, NullPointerException -> 0x05d9, all -> 0x061e }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x05cd, NullPointerException -> 0x05d9, all -> 0x061e }
            int r2 = r0.getCount()     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.v     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            if (r3 == 0) goto L_0x0458
            java.lang.String r3 = "FD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r4.<init>()     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            java.lang.String r5 = "query count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            java.lang.String r5 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            java.lang.String r3 = "FD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r4.<init>()     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            java.lang.String r5 = "column count = '"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            int r5 = r0.getColumnCount()     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            java.lang.String r5 = "' "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
        L_0x0458:
            boolean r3 = r0.moveToFirst()     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            if (r3 == 0) goto L_0x04f8
            if (r2 != r9) goto L_0x04f8
            android.widget.EditText r2 = r11.m     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r3 = 0
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r2.setText(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            android.widget.EditText r2 = r11.n     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r2.setText(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            android.widget.EditText r2 = r11.o     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r3 = 2
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r2.setText(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            android.widget.EditText r2 = r11.p     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r3 = 3
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r2.setText(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            android.widget.EditText r2 = r11.f428b     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r3 = 4
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r2.setText(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            android.widget.EditText r2 = r11.q     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r3 = 5
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r2.setText(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            android.widget.EditText r2 = r11.r     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r3 = 6
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r2.setText(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            android.widget.EditText r2 = r11.s     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r3 = 7
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r2.setText(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            android.widget.EditText r2 = r11.t     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r3 = 8
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r2.setText(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            android.widget.EditText r2 = r11.c     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r3 = 9
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r2.setText(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            android.widget.EditText r2 = r11.l     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r3 = 11
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r2.setText(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            android.widget.TextView r2 = r11.k     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r3 = 12
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r2.setText(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            android.widget.TextView r2 = r11.i     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r3 = 14
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r2.setText(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.V     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r2 <= r3) goto L_0x04f8
            android.widget.TextView r2 = r11.h     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r3 = 13
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
            r2.setText(r3)     // Catch:{ SQLException -> 0x062f, NullPointerException -> 0x062d }
        L_0x04f8:
            if (r0 == 0) goto L_0x04fd
            r0.close()
        L_0x04fd:
            r11.c()
            boolean r0 = r11.S
            if (r0 == 0) goto L_0x050a
            r11.c()
            r11.d()
        L_0x050a:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.cR
            if (r0 != r9) goto L_0x0517
            com.cyberandsons.tcmaidtrial.a.n r0 = r11.J
            java.lang.String r0 = r0.F()
            r11.a(r0)
        L_0x0517:
            return
        L_0x0518:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.L
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.M
            int r1 = r1.size()
            int r1 = r1 - r9
            if (r0 != r1) goto L_0x00eb
            boolean r0 = r11.M
            if (r0 != 0) goto L_0x052e
            android.widget.ImageButton r0 = r11.w
            boolean r1 = r11.d
            r11.a(r0, r1)
        L_0x052e:
            android.widget.ImageButton r0 = r11.y
            boolean r1 = r11.d
            r11.a(r0, r1)
            android.widget.ImageButton r0 = r11.z
            boolean r1 = r11.e
            r11.a(r0, r1)
            boolean r0 = r11.M
            if (r0 != 0) goto L_0x00eb
            android.widget.ImageButton r0 = r11.x
            boolean r1 = r11.e
            r11.a(r0, r1)
            goto L_0x00eb
        L_0x0549:
            android.widget.TextView r1 = r11.h     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r4 = 2
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setText(r4)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.widget.TextView r1 = r11.k     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.widget.TextView r1 = r11.j     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            java.lang.String r2 = "Pinyin"
            r1.setText(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            goto L_0x032a
        L_0x0561:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x0565:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0639 }
            if (r1 == 0) goto L_0x03d1
            r1.close()
            goto L_0x03d1
        L_0x056f:
            android.widget.ImageButton r1 = r11.u     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            android.content.res.Resources r2 = r11.getResources()     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r4 = 17301619(0x1080073, float:2.4979577E-38)
            android.graphics.drawable.Drawable r2 = r2.getDrawable(r4)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            r1.setImageDrawable(r2)     // Catch:{ SQLException -> 0x0561, NullPointerException -> 0x0581 }
            goto L_0x03cc
        L_0x0581:
            r1 = move-exception
        L_0x0582:
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0634 }
            java.lang.String r2 = "myVariable"
            r1.a(r2, r3)     // Catch:{ all -> 0x0634 }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0634 }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x0634 }
            java.lang.String r3 = "FD:loadSystemSuppliedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x0634 }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x0634 }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x0634 }
            r1.<init>(r11)     // Catch:{ all -> 0x0634 }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x0634 }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x0634 }
            r2 = 2131099673(0x7f060019, float:1.7811706E38)
            java.lang.String r2 = r11.getString(r2)     // Catch:{ all -> 0x0634 }
            r1.setMessage(r2)     // Catch:{ all -> 0x0634 }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.s r3 = new com.cyberandsons.tcmaidtrial.detailed.s     // Catch:{ all -> 0x0634 }
            r3.<init>(r11)     // Catch:{ all -> 0x0634 }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x0634 }
            r1.show()     // Catch:{ all -> 0x0634 }
            if (r0 == 0) goto L_0x03d1
            r0.close()
            goto L_0x03d1
        L_0x05c5:
            r0 = move-exception
            r1 = r8
        L_0x05c7:
            if (r1 == 0) goto L_0x05cc
            r1.close()
        L_0x05cc:
            throw r0
        L_0x05cd:
            r0 = move-exception
            r1 = r8
        L_0x05cf:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x062b }
            if (r1 == 0) goto L_0x04fd
            r1.close()
            goto L_0x04fd
        L_0x05d9:
            r0 = move-exception
            r0 = r8
        L_0x05db:
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0626 }
            java.lang.String r3 = "myVariable"
            r2.a(r3, r1)     // Catch:{ all -> 0x0626 }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0626 }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x0626 }
            java.lang.String r3 = "FD:loadUserModifiedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x0626 }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x0626 }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x0626 }
            r1.<init>(r11)     // Catch:{ all -> 0x0626 }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x0626 }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x0626 }
            r2 = 2131099674(0x7f06001a, float:1.7811708E38)
            java.lang.String r2 = r11.getString(r2)     // Catch:{ all -> 0x0626 }
            r1.setMessage(r2)     // Catch:{ all -> 0x0626 }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.ag r3 = new com.cyberandsons.tcmaidtrial.detailed.ag     // Catch:{ all -> 0x0626 }
            r3.<init>(r11)     // Catch:{ all -> 0x0626 }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x0626 }
            r1.show()     // Catch:{ all -> 0x0626 }
            if (r0 == 0) goto L_0x04fd
            r0.close()
            goto L_0x04fd
        L_0x061e:
            r0 = move-exception
            r1 = r8
        L_0x0620:
            if (r1 == 0) goto L_0x0625
            r1.close()
        L_0x0625:
            throw r0
        L_0x0626:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0620
        L_0x062b:
            r0 = move-exception
            goto L_0x0620
        L_0x062d:
            r2 = move-exception
            goto L_0x05db
        L_0x062f:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x05cf
        L_0x0634:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x05c7
        L_0x0639:
            r0 = move-exception
            goto L_0x05c7
        L_0x063b:
            r0 = move-exception
            r0 = r8
            goto L_0x0582
        L_0x063f:
            r0 = move-exception
            r1 = r8
            goto L_0x0565
        L_0x0643:
            r1 = r0
            goto L_0x0263
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.detailed.FormulasDetail.a(boolean):void");
    }

    private void a(String str) {
        if (str != null && str.length() != 0) {
            String[] split = str.split(",");
            for (String parseInt : split) {
                switch (Integer.parseInt(parseInt)) {
                    case 2:
                        dh.a(this.l.getText());
                        break;
                    case 3:
                        dh.a(this.m.getText());
                        break;
                    case 4:
                        dh.a(this.n.getText());
                        break;
                    case 5:
                        dh.a(this.o.getText());
                        break;
                    case 6:
                        dh.a(this.p.getText());
                        break;
                    case 7:
                        dh.a(this.f428b.getText());
                        break;
                    case 8:
                        dh.a(this.q.getText());
                        break;
                    case 9:
                        dh.a(this.r.getText());
                        break;
                    case 10:
                        dh.a(this.s.getText());
                        break;
                    case 11:
                        dh.a(this.t.getText());
                        break;
                    case 12:
                        dh.a(this.c.getText());
                        break;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        int size;
        int size2;
        Integer num;
        boolean z2 = this.e;
        switch (i2) {
            case 0:
                if (this.G) {
                    this.E -= 2;
                } else {
                    TcmAid.L = 0;
                    if (!this.M) {
                        a(this.w, this.e);
                    }
                    a(this.y, this.e);
                    a(this.z, this.d);
                    if (!this.M) {
                        a(this.x, this.d);
                    }
                }
                z2 = this.d;
                break;
            case 1:
                if (this.G) {
                    this.F -= 2;
                } else if (TcmAid.L - 1 >= 0) {
                    if (TcmAid.L - 1 == 0) {
                        if (!this.M) {
                            a(this.w, this.e);
                        }
                        a(this.y, this.e);
                    }
                    if (this.z.isEnabled() == this.e) {
                        a(this.z, this.d);
                        if (!this.M) {
                            a(this.x, this.d);
                        }
                    }
                    TcmAid.L--;
                }
                z2 = this.d;
                break;
            case 2:
                if (!this.G) {
                    if (this.H) {
                        size2 = TcmAid.N.size();
                    } else {
                        size2 = TcmAid.M.size();
                    }
                    if (TcmAid.L + 1 < size2) {
                        if (TcmAid.L + 1 == size2 - 1) {
                            a(this.z, this.e);
                            if (!this.M) {
                                a(this.x, this.e);
                            }
                        }
                        if (this.y.isEnabled() == this.e) {
                            if (!this.M) {
                                a(this.w, this.d);
                            }
                            a(this.y, this.d);
                        }
                        TcmAid.L++;
                        z2 = this.d;
                        break;
                    }
                } else {
                    this.F += 2;
                    z2 = this.d;
                    break;
                }
                break;
            case 3:
                if (this.G) {
                    this.E += 2;
                } else {
                    if (this.H) {
                        size = TcmAid.N.size();
                    } else {
                        size = TcmAid.M.size();
                    }
                    TcmAid.L = size - 1;
                    if (!this.M) {
                        a(this.w, this.d);
                    }
                    a(this.y, this.d);
                    a(this.z, this.e);
                    if (!this.M) {
                        a(this.x, this.e);
                    }
                }
                z2 = this.d;
                break;
        }
        if (z2) {
            if (this.H) {
                if (TcmAid.L >= TcmAid.N.size()) {
                    TcmAid.L = TcmAid.N.size() - 1;
                }
            } else if (TcmAid.L >= TcmAid.M.size()) {
                TcmAid.L = TcmAid.M.size() - 1;
            }
            if (this.H) {
                num = (Integer) TcmAid.N.get(TcmAid.L);
            } else {
                num = (Integer) TcmAid.M.get(TcmAid.L);
            }
            int intValue = num.intValue();
            TcmAid.Q = "main.formulas._id=" + Integer.toString(intValue) + ')';
            TcmAid.V = intValue;
            a(false);
            this.f427a.post(new ah(this));
        }
    }

    private void a(ImageButton imageButton, boolean z2) {
        if (z2) {
            imageButton.setEnabled(this.d);
            imageButton.setVisibility(0);
            return;
        }
        imageButton.setEnabled(this.e);
        imageButton.setVisibility(4);
    }

    private void c() {
        String[] split = this.f428b.getText().toString().split("\n");
        boolean z2 = this.e;
        if (this.f428b.getText().toString().indexOf("1) ") >= 0) {
            z2 = this.d;
        }
        String str = "";
        int i2 = 1;
        for (String str2 : split) {
            if (z2) {
                str = String.format("%s%s\n", str, str2);
            } else {
                i2++;
                str = String.format("%s%d) %s\n", str, Integer.valueOf(i2), str2);
            }
        }
        this.f428b.setText(str);
    }

    private void d() {
        int i2;
        String[] split = this.f428b.getText().toString().split(" ");
        Editable text = this.f428b.getText();
        String str = "";
        int i3 = 0;
        boolean z2 = this.e;
        for (String trim : split) {
            String trim2 = trim.trim();
            if (trim2.indexOf(40) != -1) {
                z2 = this.d;
            } else if (trim2.indexOf(58) < 0) {
                str = str + trim2 + ' ';
            }
            if (z2) {
                switch (m.a(str.trim(), this.I)) {
                    case 0:
                        i2 = this.N;
                        break;
                    case 1:
                        i2 = this.O;
                        break;
                    case 2:
                    default:
                        i2 = this.P;
                        break;
                    case 3:
                        i2 = this.Q;
                        break;
                    case 4:
                        i2 = this.R;
                        break;
                }
                boolean z3 = this.e;
                int indexOf = this.f428b.getText().toString().indexOf(str);
                text.setSpan(new ForegroundColorSpan(-3355444), i3, indexOf, 33);
                i3 = str.length() + indexOf;
                text.setSpan(new ForegroundColorSpan(i2), indexOf, i3, 33);
                str = "";
                z2 = z3;
            }
            text.setSpan(new ForegroundColorSpan(-3355444), i3, this.f428b.getText().toString().length(), 33);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        String str;
        String str2;
        if (this.g) {
            this.g = this.e;
            return;
        }
        int O2 = this.J.O();
        if (TcmAid.cU != -1) {
            O2 = TcmAid.cU;
        }
        if (O2 == 0 || O2 == 1) {
            str = "pinyin";
        } else if (O2 == 2) {
            str = "english";
        } else {
            str = "botanical";
        }
        String[] split = this.f428b.getText().toString().split(" ");
        StringBuffer stringBuffer = new StringBuffer();
        String str3 = "";
        boolean z2 = this.e;
        boolean z3 = this.d;
        boolean z4 = z2;
        for (String trim : split) {
            String trim2 = trim.trim();
            if (trim2.indexOf(40) != -1) {
                z4 = this.d;
            } else if (trim2.indexOf(58) < 0) {
                str3 = str3 + trim2 + ' ';
            }
            if (z4) {
                if (z3) {
                    str2 = " WHERE xxxx.materiamedica.pinyin LIKE '" + str3.trim() + "' ";
                    z3 = this.e;
                } else {
                    str2 = " OR xxxx.materiamedica.pinyin LIKE '" + str3.trim() + "' ";
                }
                boolean z5 = this.e;
                stringBuffer.append(str2);
                boolean z6 = z5;
                str3 = "";
                z4 = z6;
            }
        }
        TcmAid.af = m.d(str) + stringBuffer.toString().replace("xxxx", "main") + " UNION " + m.e(str) + (stringBuffer.toString().replace("xxxx", "userDB").replace("WHERE userDB", "WHERE (userDB") + ')') + " AND userDB.materiamedica._id>1000 ORDER BY 2";
        TcmAid.ad = null;
        startActivityForResult(new Intent(this, HerbList.class), 1350);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }

    public void onClick(View view) {
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }
}
