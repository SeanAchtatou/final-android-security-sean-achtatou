package com.cyberandsons.tcmaidtrial.detailed;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class co implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PulseDiagDetail f512a;

    co(PulseDiagDetail pulseDiagDetail) {
        this.f512a = pulseDiagDetail;
    }

    public final void onClick(View view) {
        PulseDiagDetail pulseDiagDetail = this.f512a;
        ((InputMethodManager) pulseDiagDetail.getSystemService("input_method")).hideSoftInputFromWindow(pulseDiagDetail.f436b.getWindowToken(), 0);
    }
}
