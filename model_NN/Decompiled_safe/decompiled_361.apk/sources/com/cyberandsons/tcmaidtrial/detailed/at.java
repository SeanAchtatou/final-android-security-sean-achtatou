package com.cyberandsons.tcmaidtrial.detailed;

import android.view.MotionEvent;
import android.view.View;

final class at implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FormulasDetail f463a;

    at(FormulasDetail formulasDetail) {
        this.f463a = formulasDetail;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f463a.V.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f463a.f;
    }
}
