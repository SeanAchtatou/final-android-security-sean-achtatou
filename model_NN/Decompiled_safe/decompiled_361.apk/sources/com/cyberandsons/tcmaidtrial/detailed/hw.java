package com.cyberandsons.tcmaidtrial.detailed;

import android.graphics.Point;
import android.view.View;

final class hw implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointsDetailMB f657a;

    hw(PointsDetailMB pointsDetailMB) {
        this.f657a = pointsDetailMB;
    }

    public final void onClick(View view) {
        PointsDetailMB.g.remove(this.f657a.O);
        PointsDetailMB.g.remove(PointsDetailMB.h(this.f657a));
        PointsDetailMB.g.add(PointsDetailMB.d(this.f657a), new Point(this.f657a.R, this.f657a.S));
        PointsDetailMB.g.add(this.f657a.O, new Point(this.f657a.R, this.f657a.S));
        PointsDetailMB.b(this.f657a);
    }
}
