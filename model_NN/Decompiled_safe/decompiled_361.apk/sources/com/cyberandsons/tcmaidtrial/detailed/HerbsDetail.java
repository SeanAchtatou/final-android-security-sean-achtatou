package com.cyberandsons.tcmaidtrial.detailed;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.FloatMath;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.c;
import com.cyberandsons.tcmaidtrial.a.m;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.draw.e;
import com.cyberandsons.tcmaidtrial.lists.FormulasList;
import com.cyberandsons.tcmaidtrial.misc.ad;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class HerbsDetail extends Activity implements View.OnClickListener, View.OnTouchListener {
    private ImageButton A;
    private ImageButton B;
    private ImageButton C;
    private ImageButton D;
    private ImageButton E;
    private ImageButton F;
    private ImageButton G;
    private Button H;
    private boolean I = true;
    private boolean J;
    private boolean K;
    private boolean L;
    private int M;
    private int N;
    private boolean O;
    private boolean P;
    private boolean Q;
    private String R;
    private boolean S;
    private boolean T;
    private SQLiteDatabase U;
    private n V;
    private int W;
    private boolean X;
    private boolean Y;
    private int Z;

    /* renamed from: a  reason: collision with root package name */
    ScrollView f429a;
    private int aa;
    private int ab;
    private int ac;
    private int ad;
    private boolean ae;
    /* access modifiers changed from: private */
    public GestureDetector af;
    private View.OnTouchListener ag;
    /* access modifiers changed from: private */
    public GestureDetector ah;
    private View.OnTouchListener ai;
    private Matrix aj;
    private Matrix ak;
    private int al;
    private PointF am;
    private PointF an;
    private float ao;

    /* renamed from: b  reason: collision with root package name */
    EditText f430b;
    boolean c;
    private TextView d;
    private ImageView e;
    private TextView f;
    private EditText g;
    private TextView h;
    private TextView i;
    private TextView j;
    private TextView k;
    private EditText l;
    private EditText m;
    private EditText n;
    private EditText o;
    private EditText p;
    private EditText q;
    private EditText r;
    private EditText s;
    private EditText t;
    private EditText u;
    private EditText v;
    private EditText w;
    private EditText x;
    private EditText y;
    private TextView z;

    public HerbsDetail() {
        this.J = !this.I;
        this.K = this.J;
        this.L = this.J;
        this.O = this.J;
        this.P = this.J;
        this.Q = this.J;
        this.c = this.I;
        this.S = this.J;
        this.T = this.J;
        this.X = this.J;
        this.Y = this.J;
        this.aj = new Matrix();
        this.ak = new Matrix();
        this.al = 0;
        this.am = new PointF();
        this.an = new PointF();
        this.ao = 1.0f;
    }

    static /* synthetic */ void a(HerbsDetail herbsDetail) {
        String format;
        if (herbsDetail.X) {
            String q2 = herbsDetail.V.q();
            if (n.a(q2, herbsDetail.W)) {
                String[] split = q2.split(",");
                boolean z2 = herbsDetail.I;
                String str = "";
                for (String parseInt : split) {
                    int parseInt2 = Integer.parseInt(parseInt);
                    if (parseInt2 != herbsDetail.W) {
                        if (z2) {
                            str = String.format("%d", Integer.valueOf(parseInt2));
                            z2 = herbsDetail.J;
                        } else {
                            str = str + String.format(",%d", Integer.valueOf(parseInt2));
                        }
                    }
                }
                herbsDetail.A.setImageDrawable(herbsDetail.getResources().getDrawable(17301619));
                format = str;
            } else {
                if (q2 == null || q2.length() <= 0) {
                    format = String.format("%d", Integer.valueOf(herbsDetail.W));
                } else {
                    format = String.format("%s,%d", q2, Integer.valueOf(herbsDetail.W));
                }
                herbsDetail.A.setImageDrawable(herbsDetail.getResources().getDrawable(17301618));
            }
            herbsDetail.V.e(format, herbsDetail.U);
            return;
        }
        herbsDetail.setResult(2);
        herbsDetail.finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.U == null || !this.U.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("HD:openDataBase()", str + " opened.");
                this.U = SQLiteDatabase.openDatabase(str, null, 16);
                this.U.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.U.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("HD:openDataBase()", str2 + " ATTACHed.");
                this.V = new n();
                this.V.a(this.U);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new fk(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        setContentView((int) C0000R.layout.herbs_detailed_edit);
        this.P = this.V.a();
        this.Y = this.V.ad();
        this.ae = this.V.an();
        this.Z = this.V.ai();
        this.aa = this.V.aj();
        this.ab = this.V.ak();
        this.ac = this.V.al();
        this.ad = this.V.am();
        this.d = (TextView) findViewById(C0000R.id.tce_label);
        this.f429a = (ScrollView) findViewById(C0000R.id.svDetail);
        getWindow().setSoftInputMode(3);
        this.af = new GestureDetector(new fy(this));
        this.ag = new fj(this);
        this.ah = new GestureDetector(new hn(this));
        this.ai = new dz(this);
        this.d.setOnClickListener(this);
        this.d.setOnTouchListener(this.ai);
        a(true);
        this.f429a.smoothScrollTo(0, 0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 2, 0, "Add").setIcon(17301559);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 2:
                c();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void onDestroy() {
        TcmAid.S = null;
        TcmAid.ad = null;
        try {
            if (this.U != null && this.U.isOpen()) {
                this.U.close();
                this.U = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
        if (i3 == -1) {
            Uri data = intent.getData();
            Log.i("onActivityResult", data.toString());
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(getContentResolver().openInputStream(data));
                if (decodeStream != null) {
                    BitmapDrawable a2 = dh.a(decodeStream, TcmAid.cS - 0, TcmAid.cS - 0, getWindow());
                    this.e.setImageDrawable(a2);
                    this.e.setVisibility(0);
                    this.Q = this.I;
                    this.B.setImageResource(17301560);
                    this.B.setVisibility(0);
                    String format = String.format("%s/%s_alt.png", getString(C0000R.string.image_path), this.R);
                    a(format, this.I);
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(format);
                        a2.getBitmap().compress(Bitmap.CompressFormat.PNG, 90, fileOutputStream);
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    } catch (Exception e2) {
                        Log.e("HD:onActivityResult", "Failed to write alternate image: " + e2.getLocalizedMessage());
                    }
                }
            } catch (FileNotFoundException e3) {
                Log.e("HD:onActivityResult", "Failed to process alternate image: " + e3.getLocalizedMessage());
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            this.T = this.I;
            TcmAid.bl = this.I;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        int i2;
        int i3;
        int i4;
        int i5;
        if (!this.K) {
            if (this.V.x()) {
                i2 = 1 + 16384;
                i3 = 131073 + 16384;
            } else {
                i2 = 1;
                i3 = 131073;
            }
            if (this.V.y()) {
                i4 = i2 + 32768;
                i5 = i3 + 32768;
            } else {
                i4 = i2;
                i5 = i3;
            }
            this.K = this.I;
            this.c = this.J;
            this.A.setVisibility(4);
            this.B.setVisibility(4);
            this.C.setVisibility(4);
            this.E.setVisibility(4);
            this.F.setVisibility(4);
            this.D.setVisibility(4);
            this.G.setImageResource(17301582);
            this.H.setVisibility(0);
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            inputMethodManager.getInputMethodList();
            inputMethodManager.toggleSoftInput(2, 0);
            ad.a(this.g, i4);
            ad.a(this.l, i4);
            ad.a(this.m, i5);
            ad.a(this.n, i5);
            ad.a(this.o, i5);
            ad.a(this.p, i5);
            ad.a(this.q, i5);
            ad.a(this.r, i4);
            ad.a(this.s, i5);
            ad.a(this.t, i5);
            ad.a(this.u, i5);
            ad.a(this.v, i5);
            ad.a(this.w, i5);
            ad.a(this.f430b, i5);
            ad.a(this.x, i5);
            ad.a(this.y, i5);
            return;
        }
        a((String) null, this.J);
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.f430b.getWindowToken(), 0);
        this.K = this.J;
        this.c = this.I;
        this.A.setVisibility(0);
        if (this.P) {
            this.B.setVisibility(0);
        }
        this.C.setVisibility(0);
        this.E.setVisibility(0);
        this.F.setVisibility(0);
        this.D.setVisibility(0);
        this.G.setImageResource(17301566);
        this.H.setVisibility(4);
        a(false);
    }

    private void a(String str, boolean z2) {
        int i2;
        boolean a2;
        ContentValues contentValues = new ContentValues();
        boolean z3 = this.J;
        if (TcmAid.ah == dh.f946a.intValue()) {
            a2 = z3;
            i2 = TcmAid.ah + 1000;
        } else {
            i2 = TcmAid.ah;
            a2 = q.a("SELECT COUNT(userDB.materiamedica._id) FROM userDB.materiamedica", i2, this.U);
            if (!a2) {
                contentValues.put("_id", Integer.toString(i2));
            }
        }
        contentValues.put("mmcategory", Integer.valueOf(m.d(this.f.getText().toString(), this.U)));
        contentValues.put("mmcategory_item_name", this.f.getText().toString());
        contentValues.put("subcategory", this.g.getText().toString());
        contentValues.put("english", this.h.getText().toString());
        contentValues.put("botanical", this.i.getText().toString());
        contentValues.put("meridians", this.l.getText().toString());
        contentValues.put("taste", this.m.getText().toString());
        contentValues.put("temp", this.n.getText().toString());
        contentValues.put("functions", this.o.getText().toString());
        contentValues.put("indications", this.p.getText().toString());
        contentValues.put("contraindication", this.q.getText().toString());
        contentValues.put("dosage", this.r.getText().toString());
        contentValues.put("cautions", this.s.getText().toString());
        contentValues.put("combinations", this.t.getText().toString());
        contentValues.put("general_functions", this.u.getText().toString());
        contentValues.put("toxicology", this.v.getText().toString());
        contentValues.put("pharmacology", this.w.getText().toString());
        contentValues.put("remarks", this.f430b.getText().toString());
        contentValues.put("herb_herb", this.x.getText().toString());
        contentValues.put("herb_drug", this.y.getText().toString());
        contentValues.put("preparation", "");
        if (z2) {
            contentValues.put("alt_image", str);
        }
        if (!a2) {
            try {
                this.U.insert("userDB.materiamedica", null, contentValues);
            } catch (SQLException e2) {
                q.a(e2);
            }
        } else {
            this.U.update("userDB.materiamedica", contentValues, "_id=" + Integer.toString(i2), null);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (!this.Q) {
            startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 0);
            return;
        }
        a((String) null, this.I);
        this.Q = this.J;
        a(false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:123:0x05a6  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x05bc A[Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x06e1 A[Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x06f0 A[Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x0715  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x072a  */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x080f  */
    /* JADX WARNING: Removed duplicated region for block: B:185:0x0835  */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x08df  */
    /* JADX WARNING: Removed duplicated region for block: B:211:0x08e4 A[SYNTHETIC, Splitter:B:211:0x08e4] */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x08f3  */
    /* JADX WARNING: Removed duplicated region for block: B:219:0x08fe  */
    /* JADX WARNING: Removed duplicated region for block: B:236:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r14) {
        /*
            r13 = this;
            r11 = 4
            r10 = 0
            r9 = 1
            r8 = 0
            if (r14 == 0) goto L_0x00f5
            r0 = 2131362330(0x7f0a021a, float:1.8344438E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.G = r0
            android.widget.ImageButton r0 = r13.G
            com.cyberandsons.tcmaidtrial.detailed.hd r1 = new com.cyberandsons.tcmaidtrial.detailed.hd
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            r0 = 2131362331(0x7f0a021b, float:1.834444E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r13.H = r0
            android.widget.Button r0 = r13.H
            com.cyberandsons.tcmaidtrial.detailed.hc r1 = new com.cyberandsons.tcmaidtrial.detailed.hc
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            com.cyberandsons.tcmaidtrial.a.n r0 = r13.V
            boolean r0 = r0.ab()
            r13.X = r0
            r0 = 2131361811(0x7f0a0013, float:1.8343385E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.A = r0
            android.widget.ImageButton r0 = r13.A
            com.cyberandsons.tcmaidtrial.detailed.hq r1 = new com.cyberandsons.tcmaidtrial.detailed.hq
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            r0 = 2131361809(0x7f0a0011, float:1.834338E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.B = r0
            android.widget.ImageButton r0 = r13.B
            com.cyberandsons.tcmaidtrial.detailed.hr r1 = new com.cyberandsons.tcmaidtrial.detailed.hr
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            boolean r0 = r13.P
            if (r0 != 0) goto L_0x006b
            android.widget.ImageButton r0 = r13.B
            r0.setVisibility(r11)
        L_0x006b:
            r0 = 2131361815(0x7f0a0017, float:1.8343393E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.C = r0
            android.widget.ImageButton r0 = r13.C
            com.cyberandsons.tcmaidtrial.detailed.ho r1 = new com.cyberandsons.tcmaidtrial.detailed.ho
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            r0 = 2131361814(0x7f0a0016, float:1.834339E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.E = r0
            android.widget.ImageButton r0 = r13.E
            com.cyberandsons.tcmaidtrial.detailed.hp r1 = new com.cyberandsons.tcmaidtrial.detailed.hp
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            r0 = 2131361813(0x7f0a0015, float:1.8343389E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.F = r0
            android.widget.ImageButton r0 = r13.F
            com.cyberandsons.tcmaidtrial.detailed.ht r1 = new com.cyberandsons.tcmaidtrial.detailed.ht
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            r0 = 2131361812(0x7f0a0014, float:1.8343387E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r13.D = r0
            android.widget.ImageButton r0 = r13.D
            com.cyberandsons.tcmaidtrial.detailed.hu r1 = new com.cyberandsons.tcmaidtrial.detailed.hu
            r1.<init>(r13)
            r0.setOnClickListener(r1)
            boolean r0 = r13.Y
            if (r0 == 0) goto L_0x00cd
            android.widget.ImageButton r0 = r13.C
            r0.setVisibility(r11)
            android.widget.ImageButton r0 = r13.D
            r0.setVisibility(r11)
        L_0x00cd:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.Y
            if (r0 != 0) goto L_0x0734
            boolean r0 = r13.Y
            if (r0 != 0) goto L_0x00dc
            android.widget.ImageButton r0 = r13.C
            boolean r1 = r13.J
            r13.a(r0, r1)
        L_0x00dc:
            android.widget.ImageButton r0 = r13.E
            boolean r1 = r13.J
            r13.a(r0, r1)
            android.widget.ImageButton r0 = r13.F
            boolean r1 = r13.I
            r13.a(r0, r1)
            boolean r0 = r13.Y
            if (r0 != 0) goto L_0x00f5
            android.widget.ImageButton r0 = r13.D
            boolean r1 = r13.I
            r13.a(r0, r1)
        L_0x00f5:
            r0 = 2131361941(0x7f0a0095, float:1.8343649E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r13.e = r0
            android.widget.ImageView r0 = r13.e
            r0.setOnTouchListener(r13)
            android.graphics.Matrix r0 = r13.aj
            r1 = 1065353216(0x3f800000, float:1.0)
            r2 = 1065353216(0x3f800000, float:1.0)
            r0.setTranslate(r1, r2)
            android.widget.ImageView r0 = r13.e
            android.graphics.Matrix r1 = r13.aj
            r0.setImageMatrix(r1)
            android.widget.TextView r0 = r13.f
            if (r0 == 0) goto L_0x011b
            r13.f = r8
        L_0x011b:
            r0 = 2131361945(0x7f0a0099, float:1.8343657E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r13.f = r0
            android.widget.EditText r0 = r13.g
            if (r0 == 0) goto L_0x012c
            r13.g = r8
        L_0x012c:
            r0 = 2131361947(0x7f0a009b, float:1.834366E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.g = r0
            android.widget.EditText r0 = r13.g
            com.cyberandsons.tcmaidtrial.detailed.hs r1 = new com.cyberandsons.tcmaidtrial.detailed.hs
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.TextView r0 = r13.j
            if (r0 == 0) goto L_0x0147
            r13.j = r8
        L_0x0147:
            r0 = 2131361948(0x7f0a009c, float:1.8343663E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r13.j = r0
            android.widget.TextView r0 = r13.h
            if (r0 == 0) goto L_0x0158
            r13.h = r8
        L_0x0158:
            r0 = 2131361949(0x7f0a009d, float:1.8343665E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r13.h = r0
            android.widget.TextView r0 = r13.k
            if (r0 == 0) goto L_0x0169
            r13.k = r8
        L_0x0169:
            r0 = 2131361950(0x7f0a009e, float:1.8343667E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r13.k = r0
            android.widget.TextView r0 = r13.i
            if (r0 == 0) goto L_0x017a
            r13.i = r8
        L_0x017a:
            r0 = 2131361951(0x7f0a009f, float:1.8343669E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r13.i = r0
            android.widget.EditText r0 = r13.l
            if (r0 == 0) goto L_0x018b
            r13.l = r8
        L_0x018b:
            r0 = 2131361953(0x7f0a00a1, float:1.8343673E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.l = r0
            android.widget.EditText r0 = r13.l
            com.cyberandsons.tcmaidtrial.detailed.el r1 = new com.cyberandsons.tcmaidtrial.detailed.el
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.m
            if (r0 == 0) goto L_0x01a6
            r13.m = r8
        L_0x01a6:
            r0 = 2131361955(0x7f0a00a3, float:1.8343677E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.m = r0
            android.widget.EditText r0 = r13.m
            com.cyberandsons.tcmaidtrial.detailed.ek r1 = new com.cyberandsons.tcmaidtrial.detailed.ek
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.n
            if (r0 == 0) goto L_0x01c1
            r13.n = r8
        L_0x01c1:
            r0 = 2131361957(0x7f0a00a5, float:1.834368E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.n = r0
            android.widget.EditText r0 = r13.n
            com.cyberandsons.tcmaidtrial.detailed.en r1 = new com.cyberandsons.tcmaidtrial.detailed.en
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.o
            if (r0 == 0) goto L_0x01dc
            r13.o = r8
        L_0x01dc:
            r0 = 2131361959(0x7f0a00a7, float:1.8343685E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.o = r0
            android.widget.EditText r0 = r13.o
            com.cyberandsons.tcmaidtrial.detailed.em r1 = new com.cyberandsons.tcmaidtrial.detailed.em
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.p
            if (r0 == 0) goto L_0x01f7
            r13.p = r8
        L_0x01f7:
            r0 = 2131361961(0x7f0a00a9, float:1.834369E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.p = r0
            android.widget.EditText r0 = r13.p
            com.cyberandsons.tcmaidtrial.detailed.ep r1 = new com.cyberandsons.tcmaidtrial.detailed.ep
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.q
            if (r0 == 0) goto L_0x0212
            r13.q = r8
        L_0x0212:
            r0 = 2131361963(0x7f0a00ab, float:1.8343693E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.q = r0
            android.widget.EditText r0 = r13.q
            com.cyberandsons.tcmaidtrial.detailed.eo r1 = new com.cyberandsons.tcmaidtrial.detailed.eo
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.r
            if (r0 == 0) goto L_0x022d
            r13.r = r8
        L_0x022d:
            r0 = 2131361965(0x7f0a00ad, float:1.8343697E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.r = r0
            android.widget.EditText r0 = r13.r
            com.cyberandsons.tcmaidtrial.detailed.es r1 = new com.cyberandsons.tcmaidtrial.detailed.es
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.s
            if (r0 == 0) goto L_0x0248
            r13.s = r8
        L_0x0248:
            r0 = 2131361967(0x7f0a00af, float:1.8343701E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.s = r0
            android.widget.EditText r0 = r13.s
            com.cyberandsons.tcmaidtrial.detailed.eq r1 = new com.cyberandsons.tcmaidtrial.detailed.eq
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.t
            if (r0 == 0) goto L_0x0263
            r13.t = r8
        L_0x0263:
            r0 = 2131361969(0x7f0a00b1, float:1.8343705E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.t = r0
            android.widget.EditText r0 = r13.t
            com.cyberandsons.tcmaidtrial.detailed.ew r1 = new com.cyberandsons.tcmaidtrial.detailed.ew
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.u
            if (r0 == 0) goto L_0x027e
            r13.u = r8
        L_0x027e:
            r0 = 2131361971(0x7f0a00b3, float:1.834371E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.u = r0
            android.widget.EditText r0 = r13.u
            com.cyberandsons.tcmaidtrial.detailed.eu r1 = new com.cyberandsons.tcmaidtrial.detailed.eu
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.v
            if (r0 == 0) goto L_0x0299
            r13.v = r8
        L_0x0299:
            r0 = 2131361973(0x7f0a00b5, float:1.8343713E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.v = r0
            android.widget.EditText r0 = r13.v
            com.cyberandsons.tcmaidtrial.detailed.fe r1 = new com.cyberandsons.tcmaidtrial.detailed.fe
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.w
            if (r0 == 0) goto L_0x02b4
            r13.f430b = r8
        L_0x02b4:
            r0 = 2131361975(0x7f0a00b7, float:1.8343718E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.w = r0
            android.widget.EditText r0 = r13.w
            com.cyberandsons.tcmaidtrial.detailed.fd r1 = new com.cyberandsons.tcmaidtrial.detailed.fd
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.f430b
            if (r0 == 0) goto L_0x02cf
            r13.f430b = r8
        L_0x02cf:
            r0 = 2131361977(0x7f0a00b9, float:1.8343722E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.f430b = r0
            android.widget.EditText r0 = r13.f430b
            com.cyberandsons.tcmaidtrial.detailed.fc r1 = new com.cyberandsons.tcmaidtrial.detailed.fc
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.x
            if (r0 == 0) goto L_0x02ea
            r13.x = r8
        L_0x02ea:
            r0 = 2131361979(0x7f0a00bb, float:1.8343726E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.x = r0
            android.widget.EditText r0 = r13.x
            com.cyberandsons.tcmaidtrial.detailed.fi r1 = new com.cyberandsons.tcmaidtrial.detailed.fi
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r13.y
            if (r0 == 0) goto L_0x0305
            r13.y = r8
        L_0x0305:
            r0 = 2131361981(0x7f0a00bd, float:1.834373E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r13.y = r0
            android.widget.EditText r0 = r13.y
            com.cyberandsons.tcmaidtrial.detailed.fh r1 = new com.cyberandsons.tcmaidtrial.detailed.fh
            r1.<init>(r13)
            r0.setOnTouchListener(r1)
            r0 = 2131361984(0x7f0a00c0, float:1.8343736E38)
            android.view.View r0 = r13.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r13.z = r0
            com.cyberandsons.tcmaidtrial.a.n r0 = r13.V
            int r0 = r0.O()
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.cU
            r2 = -1
            if (r1 == r2) goto L_0x0920
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.cU
            r1 = r0
        L_0x0333:
            com.cyberandsons.tcmaidtrial.a.n r0 = r13.V
            int r2 = r0.ac()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = "SELECT main.materiamedica._id, main.materiamedica.pinyin, mmcategory.item_name, main.materiamedica.english, main.materiamedica.botanical, main.materiamedica.meridians, main.materiamedica.taste, main.materiamedica.functions, main.materiamedica.indications, main.materiamedica.contraindication, main.materiamedica.dosage, main.materiamedica.remarks, main.materiamedica.combinations, main.materiamedica.temp, main.materiamedica.subcategory, main.materiamedica.general_functions, main.materiamedica.cautions, main.materiamedica.mmcategory, main.materiamedica.toxicology, main.materiamedica.pharmacology, main.tonemarks.mtonemarks, main.tonemarks.mscc, main.tonemarks.mtcc FROM main.materiamedica JOIN main.mmcategory ON main.mmcategory._id=main.materiamedica.mmcategory JOIN main.tonemarks ON main.tonemarks.mid = main.materiamedica._id WHERE "
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.TcmAid.ad
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = r0.toString()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.s
            if (r0 == 0) goto L_0x0357
            java.lang.String r0 = "HD:loadSystemSuppliedData()"
            android.util.Log.d(r0, r3)
        L_0x0357:
            android.database.sqlite.SQLiteDatabase r0 = r13.U     // Catch:{ SQLException -> 0x091c, NullPointerException -> 0x0918, all -> 0x0911 }
            r4 = 0
            android.database.Cursor r0 = r0.rawQuery(r3, r4)     // Catch:{ SQLException -> 0x091c, NullPointerException -> 0x0918, all -> 0x0911 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x091c, NullPointerException -> 0x0918, all -> 0x0911 }
            int r4 = r0.getCount()     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            boolean r5 = com.cyberandsons.tcmaidtrial.misc.dh.s     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            if (r5 == 0) goto L_0x03aa
            java.lang.String r5 = "HD:loadSystemSuppliedData()"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r6.<init>()     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r7 = "query count = "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r7 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r6 = r6.toString()     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.util.Log.d(r5, r6)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r5 = "HD:loadSystemSuppliedData()"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r6.<init>()     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r7 = "column count = '"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            int r7 = r0.getColumnCount()     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r7 = "' "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r6 = r6.toString()     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.util.Log.d(r5, r6)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
        L_0x03aa:
            boolean r5 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            if (r5 == 0) goto L_0x056e
            if (r4 != r9) goto L_0x056e
            r4 = 0
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r13.W = r4     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r4 = 17
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r5.<init>()     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r6 = 20
            java.lang.String r6 = r0.getString(r6)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r6 = com.cyberandsons.tcmaidtrial.misc.ad.a(r6)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r6 = "\n"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            int r2 = r2 + 21
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            if (r1 == 0) goto L_0x03ea
            if (r1 != r9) goto L_0x0765
        L_0x03ea:
            android.widget.TextView r1 = r13.d     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.TextView r1 = r13.h     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 3
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.misc.ad.a(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.TextView r1 = r13.j     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r2 = "English"
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.TextView r1 = r13.i     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 4
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.TextView r1 = r13.k     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r2 = "Phamaceutical"
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
        L_0x0415:
            r1 = 1
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r13.R = r1     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r1 = r13.R     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r2 = "Da Ji"
            r1.equalsIgnoreCase(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r1 = r13.R     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r2 = "Da Ji"
            boolean r1 = r1.equalsIgnoreCase(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            if (r1 == 0) goto L_0x0435
            r1 = 10
            if (r4 != r1) goto L_0x0435
            java.lang.String r1 = "Da Ji2"
            r13.R = r1     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
        L_0x0435:
            boolean r1 = r13.P     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            if (r1 == 0) goto L_0x0839
            java.lang.String r1 = r13.R     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.misc.dh.a(r1)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r2 = "herbs"
            android.graphics.Bitmap r1 = com.cyberandsons.tcmaidtrial.misc.dh.b(r1, r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            if (r1 != 0) goto L_0x0814
            android.widget.ImageView r1 = r13.e     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 2130837543(0x7f020027, float:1.7280043E38)
            r1.setImageResource(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
        L_0x044f:
            android.widget.TextView r1 = r13.f     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 2
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.EditText r1 = r13.g     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 14
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.EditText r1 = r13.l     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 5
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.EditText r1 = r13.m     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 6
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.misc.ad.a(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.EditText r1 = r13.n     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 13
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.misc.ad.a(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.EditText r1 = r13.o     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 7
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.EditText r1 = r13.p     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 8
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.EditText r1 = r13.q     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 9
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.EditText r1 = r13.r     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 10
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.EditText r1 = r13.s     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 16
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.EditText r1 = r13.t     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 12
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.EditText r1 = r13.u     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 15
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.EditText r1 = r13.v     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 18
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.EditText r1 = r13.w     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 19
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.EditText r1 = r13.f430b     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 11
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.EditText r1 = r13.x     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 1
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r4 = 0
            android.database.sqlite.SQLiteDatabase r5 = r13.U     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.CharSequence r2 = com.cyberandsons.tcmaidtrial.a.m.a(r2, r4, r5)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.EditText r1 = r13.y     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 1
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r4 = 1
            android.database.sqlite.SQLiteDatabase r5 = r13.U     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.CharSequence r2 = com.cyberandsons.tcmaidtrial.a.m.a(r2, r4, r5)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.TextView r1 = r13.z     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 1
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.database.sqlite.SQLiteDatabase r4 = r13.U     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.CharSequence r2 = com.cyberandsons.tcmaidtrial.a.m.b(r2, r4)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.TextView r1 = r13.z     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setOnClickListener(r13)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.TextView r1 = r13.z     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.view.View$OnTouchListener r2 = r13.ag     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setOnTouchListener(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            boolean r1 = r13.ae     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            if (r1 == 0) goto L_0x054c
            int r1 = r13.W     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.database.sqlite.SQLiteDatabase r2 = r13.U     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            int r1 = com.cyberandsons.tcmaidtrial.a.m.a(r1, r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            switch(r1) {
                case 0: goto L_0x0842;
                case 1: goto L_0x0846;
                case 2: goto L_0x0545;
                case 3: goto L_0x084a;
                case 4: goto L_0x084e;
                default: goto L_0x0545;
            }     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
        L_0x0545:
            int r1 = r13.ab     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
        L_0x0547:
            android.widget.TextView r2 = r13.d     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2.setTextColor(r1)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
        L_0x054c:
            boolean r1 = r13.X     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            if (r1 == 0) goto L_0x056e
            com.cyberandsons.tcmaidtrial.a.n r1 = r13.V     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r1 = r1.q()     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            int r2 = r13.W     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            boolean r1 = com.cyberandsons.tcmaidtrial.a.n.a(r1, r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            if (r1 == 0) goto L_0x0852
            android.widget.ImageButton r1 = r13.A     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.content.res.Resources r2 = r13.getResources()     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r4 = 17301618(0x1080072, float:2.4979574E-38)
            android.graphics.drawable.Drawable r2 = r2.getDrawable(r4)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setImageDrawable(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
        L_0x056e:
            if (r0 == 0) goto L_0x0573
            r0.close()
        L_0x0573:
            boolean r0 = r13.J
            r13.Q = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT userDB.materiamedica.meridians, userDB.materiamedica.taste, userDB.materiamedica.temp, userDB.materiamedica.functions, userDB.materiamedica.indications, userDB.materiamedica.contraindication, userDB.materiamedica.dosage, userDB.materiamedica.remarks, userDB.materiamedica.combinations, userDB.materiamedica.general_functions, userDB.materiamedica.cautions, userDB.materiamedica.mmcategory, userDB.materiamedica.herb_herb, userDB.materiamedica.herb_drug, userDB.materiamedica.botanical, userDB.materiamedica.english, userDB.materiamedica.subcategory, userDB.materiamedica.pinyin, userDB.materiamedica.toxicology, userDB.materiamedica.pharmacology, main.mmcategory.item_name, userDB.materiamedica.alt_image FROM userDB.materiamedica LEFT JOIN main.mmcategory ON main.mmcategory._id=userDB.materiamedica.mmcategory WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "userDB.materiamedica."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "_id"
            java.lang.StringBuilder r0 = r0.append(r1)
            r1 = 61
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.ah
            java.lang.String r1 = java.lang.Integer.toString(r1)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.s
            if (r0 == 0) goto L_0x05ab
            java.lang.String r0 = "HD:loadUserModifiedData()"
            android.util.Log.d(r0, r1)
        L_0x05ab:
            android.database.sqlite.SQLiteDatabase r0 = r13.U     // Catch:{ SQLException -> 0x090d, NullPointerException -> 0x090a, all -> 0x0905 }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x090d, NullPointerException -> 0x090a, all -> 0x0905 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x090d, NullPointerException -> 0x090a, all -> 0x0905 }
            int r2 = r0.getCount()     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.s     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            if (r3 == 0) goto L_0x05fe
            java.lang.String r3 = "HD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r4.<init>()     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            java.lang.String r5 = "query count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            java.lang.String r5 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            java.lang.String r3 = "HD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r4.<init>()     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            java.lang.String r5 = "column count = '"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            int r5 = r0.getColumnCount()     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            java.lang.String r5 = "' "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
        L_0x05fe:
            boolean r3 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            if (r3 == 0) goto L_0x070c
            if (r2 != r9) goto L_0x070c
            android.widget.EditText r2 = r13.l     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 0
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.EditText r2 = r13.m     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.misc.ad.a(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.EditText r2 = r13.n     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 2
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.misc.ad.a(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.EditText r2 = r13.o     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 3
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.EditText r2 = r13.p     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 4
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.EditText r2 = r13.q     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 5
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.EditText r2 = r13.r     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 6
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.EditText r2 = r13.f430b     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 7
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.EditText r2 = r13.t     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 8
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.EditText r2 = r13.u     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 9
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.EditText r2 = r13.s     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 10
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.EditText r2 = r13.x     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 12
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.EditText r2 = r13.y     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 13
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.TextView r2 = r13.i     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 14
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.TextView r2 = r13.h     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 15
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.misc.ad.a(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.EditText r2 = r13.g     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 16
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.EditText r2 = r13.v     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 18
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.EditText r2 = r13.w     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 19
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.TextView r2 = r13.f     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 20
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.ah     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r2 <= r3) goto L_0x06ec
            android.widget.TextView r2 = r13.d     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 17
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
        L_0x06ec:
            boolean r2 = r13.P     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            if (r2 == 0) goto L_0x08e4
            r2 = 21
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            if (r2 == 0) goto L_0x0891
            int r3 = r2.length()     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            if (r3 <= 0) goto L_0x0891
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeFile(r2)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            if (r2 != 0) goto L_0x0864
            android.widget.ImageView r2 = r13.e     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 2130837543(0x7f020027, float:1.7280043E38)
            r2.setImageResource(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
        L_0x070c:
            if (r0 == 0) goto L_0x0711
            r0.close()
        L_0x0711:
            boolean r0 = r13.P
            if (r0 == 0) goto L_0x08fe
            boolean r0 = r13.Q
            if (r0 == 0) goto L_0x08f7
            android.widget.ImageButton r0 = r13.B
            r1 = 17301560(0x1080038, float:2.4979412E-38)
            r0.setImageResource(r1)
            android.widget.ImageButton r0 = r13.B
            r0.setVisibility(r10)
        L_0x0726:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.cR
            if (r0 != r9) goto L_0x0733
            com.cyberandsons.tcmaidtrial.a.n r0 = r13.V
            java.lang.String r0 = r0.G()
            r13.a(r0)
        L_0x0733:
            return
        L_0x0734:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.Y
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.Z
            int r1 = r1.size()
            int r1 = r1 - r9
            if (r0 != r1) goto L_0x00f5
            boolean r0 = r13.Y
            if (r0 != 0) goto L_0x074a
            android.widget.ImageButton r0 = r13.C
            boolean r1 = r13.I
            r13.a(r0, r1)
        L_0x074a:
            android.widget.ImageButton r0 = r13.E
            boolean r1 = r13.I
            r13.a(r0, r1)
            android.widget.ImageButton r0 = r13.F
            boolean r1 = r13.J
            r13.a(r0, r1)
            boolean r0 = r13.Y
            if (r0 != 0) goto L_0x00f5
            android.widget.ImageButton r0 = r13.D
            boolean r1 = r13.J
            r13.a(r0, r1)
            goto L_0x00f5
        L_0x0765:
            r5 = 2
            if (r1 != r5) goto L_0x07a3
            android.widget.TextView r1 = r13.d     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r5 = 3
            java.lang.String r5 = r0.getString(r5)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r5 = com.cyberandsons.tcmaidtrial.misc.ad.a(r5)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r5)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.TextView r1 = r13.h     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.TextView r1 = r13.j     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r2 = "Pinyin"
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.TextView r1 = r13.i     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 4
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.TextView r1 = r13.k     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r2 = "Phamaceutical"
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            goto L_0x0415
        L_0x0795:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x0799:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0915 }
            if (r1 == 0) goto L_0x0573
            r1.close()
            goto L_0x0573
        L_0x07a3:
            android.widget.TextView r1 = r13.d     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r5 = 4
            java.lang.String r5 = r0.getString(r5)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r5)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.TextView r1 = r13.h     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.TextView r1 = r13.j     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r2 = "Pinyin"
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.TextView r1 = r13.i     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 3
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.misc.ad.a(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.TextView r1 = r13.k     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            java.lang.String r2 = "English"
            r1.setText(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            goto L_0x0415
        L_0x07d0:
            r1 = move-exception
        L_0x07d1:
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x082f }
            java.lang.String r2 = "myVariable"
            r1.a(r2, r3)     // Catch:{ all -> 0x082f }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x082f }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x082f }
            java.lang.String r3 = "HD:loadSystemSuppliedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x082f }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x082f }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x082f }
            r1.<init>(r13)     // Catch:{ all -> 0x082f }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x082f }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x082f }
            r2 = 2131099673(0x7f060019, float:1.7811706E38)
            java.lang.String r2 = r13.getString(r2)     // Catch:{ all -> 0x082f }
            r1.setMessage(r2)     // Catch:{ all -> 0x082f }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.fg r3 = new com.cyberandsons.tcmaidtrial.detailed.fg     // Catch:{ all -> 0x082f }
            r3.<init>(r13)     // Catch:{ all -> 0x082f }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x082f }
            r1.show()     // Catch:{ all -> 0x082f }
            if (r0 == 0) goto L_0x0573
            r0.close()
            goto L_0x0573
        L_0x0814:
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            int r2 = r2 - r10
            int r4 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            int r4 = r4 - r10
            android.view.Window r5 = r13.getWindow()     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.graphics.drawable.BitmapDrawable r1 = com.cyberandsons.tcmaidtrial.misc.dh.a(r1, r2, r4, r5)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.ImageView r2 = r13.e     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2.setImageDrawable(r1)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.widget.ImageView r1 = r13.e     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 0
            r1.setVisibility(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            goto L_0x044f
        L_0x082f:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x0833:
            if (r1 == 0) goto L_0x0838
            r1.close()
        L_0x0838:
            throw r0
        L_0x0839:
            android.widget.ImageView r1 = r13.e     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r2 = 8
            r1.setVisibility(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            goto L_0x044f
        L_0x0842:
            int r1 = r13.Z     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            goto L_0x0547
        L_0x0846:
            int r1 = r13.aa     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            goto L_0x0547
        L_0x084a:
            int r1 = r13.ac     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            goto L_0x0547
        L_0x084e:
            int r1 = r13.ad     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            goto L_0x0547
        L_0x0852:
            android.widget.ImageButton r1 = r13.A     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            android.content.res.Resources r2 = r13.getResources()     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r4 = 17301619(0x1080073, float:2.4979577E-38)
            android.graphics.drawable.Drawable r2 = r2.getDrawable(r4)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            r1.setImageDrawable(r2)     // Catch:{ SQLException -> 0x0795, NullPointerException -> 0x07d0 }
            goto L_0x056e
        L_0x0864:
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            int r3 = r3 - r10
            int r4 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            int r4 = r4 - r10
            android.view.Window r5 = r13.getWindow()     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.graphics.drawable.BitmapDrawable r2 = com.cyberandsons.tcmaidtrial.misc.dh.a(r2, r3, r4, r5)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.ImageView r3 = r13.e     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3.setImageDrawable(r2)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            android.widget.ImageView r2 = r13.e     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 0
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            boolean r2 = r13.I     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r13.Q = r2     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            goto L_0x070c
        L_0x0883:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x0887:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0908 }
            if (r1 == 0) goto L_0x0711
            r1.close()
            goto L_0x0711
        L_0x0891:
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.ah     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r2 <= r3) goto L_0x070c
            android.widget.ImageView r2 = r13.e     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 8
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            goto L_0x070c
        L_0x08a0:
            r2 = move-exception
        L_0x08a1:
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x08ed }
            java.lang.String r3 = "myVariable"
            r2.a(r3, r1)     // Catch:{ all -> 0x08ed }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x08ed }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x08ed }
            java.lang.String r3 = "HD:loadUserModifiedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x08ed }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x08ed }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x08ed }
            r1.<init>(r13)     // Catch:{ all -> 0x08ed }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x08ed }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x08ed }
            r2 = 2131099674(0x7f06001a, float:1.7811708E38)
            java.lang.String r2 = r13.getString(r2)     // Catch:{ all -> 0x08ed }
            r1.setMessage(r2)     // Catch:{ all -> 0x08ed }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.ff r3 = new com.cyberandsons.tcmaidtrial.detailed.ff     // Catch:{ all -> 0x08ed }
            r3.<init>(r13)     // Catch:{ all -> 0x08ed }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x08ed }
            r1.show()     // Catch:{ all -> 0x08ed }
            if (r0 == 0) goto L_0x0711
            r0.close()
            goto L_0x0711
        L_0x08e4:
            android.widget.ImageView r2 = r13.e     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            r3 = 8
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x0883, NullPointerException -> 0x08a0 }
            goto L_0x070c
        L_0x08ed:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x08f1:
            if (r1 == 0) goto L_0x08f6
            r1.close()
        L_0x08f6:
            throw r0
        L_0x08f7:
            android.widget.ImageButton r0 = r13.B
            r0.setVisibility(r11)
            goto L_0x0726
        L_0x08fe:
            android.widget.ImageButton r0 = r13.B
            r0.setVisibility(r11)
            goto L_0x0726
        L_0x0905:
            r0 = move-exception
            r1 = r8
            goto L_0x08f1
        L_0x0908:
            r0 = move-exception
            goto L_0x08f1
        L_0x090a:
            r0 = move-exception
            r0 = r8
            goto L_0x08a1
        L_0x090d:
            r0 = move-exception
            r1 = r8
            goto L_0x0887
        L_0x0911:
            r0 = move-exception
            r1 = r8
            goto L_0x0833
        L_0x0915:
            r0 = move-exception
            goto L_0x0833
        L_0x0918:
            r0 = move-exception
            r0 = r8
            goto L_0x07d1
        L_0x091c:
            r0 = move-exception
            r1 = r8
            goto L_0x0799
        L_0x0920:
            r1 = r0
            goto L_0x0333
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.detailed.HerbsDetail.a(boolean):void");
    }

    private void a(String str) {
        if (str != null && str.length() != 0) {
            String[] split = str.split(",");
            for (String parseInt : split) {
                switch (Integer.parseInt(parseInt)) {
                    case 3:
                        dh.a(this.l.getText());
                        break;
                    case 4:
                        dh.a(this.m.getText());
                        break;
                    case 5:
                        dh.a(this.n.getText());
                        break;
                    case 6:
                        dh.a(this.o.getText());
                        break;
                    case 7:
                        dh.a(this.p.getText());
                        break;
                    case 8:
                        dh.a(this.q.getText());
                        break;
                    case 9:
                        dh.a(this.r.getText());
                        break;
                    case 10:
                        dh.a(this.s.getText());
                        break;
                    case 11:
                        dh.a(this.t.getText());
                        break;
                    case 12:
                        dh.a(this.u.getText());
                        break;
                    case 13:
                        dh.a(this.v.getText());
                        break;
                    case 14:
                        dh.a(this.w.getText());
                        break;
                    case 15:
                        dh.a(this.f430b.getText());
                        break;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        int size;
        int size2;
        Integer num;
        boolean z2 = this.J;
        switch (i2) {
            case 0:
                if (this.O) {
                    this.M -= 2;
                } else {
                    TcmAid.Y = 0;
                    if (!this.Y) {
                        a(this.C, this.J);
                    }
                    a(this.E, this.J);
                    a(this.F, this.I);
                    if (!this.Y) {
                        a(this.D, this.I);
                    }
                }
                z2 = this.I;
                break;
            case 1:
                if (this.O) {
                    this.N -= 2;
                } else if (TcmAid.Y - 1 >= 0) {
                    if (TcmAid.Y - 1 == 0) {
                        if (!this.Y) {
                            a(this.C, this.J);
                        }
                        a(this.E, this.J);
                    }
                    if (this.F.isEnabled() == this.J) {
                        a(this.F, this.I);
                        if (!this.Y) {
                            a(this.D, this.I);
                        }
                    }
                    TcmAid.Y--;
                }
                z2 = this.I;
                break;
            case 2:
                if (!this.O) {
                    if (this.S) {
                        size2 = TcmAid.aa.size();
                    } else {
                        size2 = TcmAid.Z.size();
                    }
                    if (TcmAid.Y + 1 < size2) {
                        if (TcmAid.Y + 1 == size2 - 1) {
                            a(this.F, this.J);
                            if (!this.Y) {
                                a(this.D, this.J);
                            }
                        }
                        if (this.E.isEnabled() == this.J) {
                            if (!this.Y) {
                                a(this.C, this.I);
                            }
                            a(this.E, this.I);
                        }
                        TcmAid.Y++;
                        z2 = this.I;
                        break;
                    }
                } else {
                    this.N += 2;
                    z2 = this.I;
                    break;
                }
                break;
            case 3:
                if (this.O) {
                    this.M += 2;
                } else {
                    if (this.S) {
                        size = TcmAid.aa.size();
                    } else {
                        size = TcmAid.Z.size();
                    }
                    TcmAid.Y = size - 1;
                    if (!this.Y) {
                        a(this.C, this.I);
                    }
                    a(this.E, this.I);
                    a(this.F, this.J);
                    if (!this.Y) {
                        a(this.D, this.J);
                    }
                }
                z2 = this.I;
                break;
        }
        if (z2) {
            if (this.S) {
                if (TcmAid.Y >= TcmAid.aa.size()) {
                    TcmAid.Y = TcmAid.aa.size() - 1;
                }
            } else if (TcmAid.Y >= TcmAid.Z.size()) {
                TcmAid.Y = TcmAid.Z.size() - 1;
            }
            if (this.S) {
                num = (Integer) TcmAid.aa.get(TcmAid.Y);
            } else {
                num = (Integer) TcmAid.Z.get(TcmAid.Y);
            }
            int intValue = num.intValue();
            TcmAid.ad = "main.materiamedica._id=" + Integer.toString(intValue);
            TcmAid.ah = intValue;
            a(false);
            this.f429a.post(new fl(this));
        }
    }

    private void a(ImageButton imageButton, boolean z2) {
        if (z2) {
            imageButton.setEnabled(this.I);
            imageButton.setVisibility(0);
            return;
        }
        imageButton.setEnabled(this.J);
        imageButton.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        String str;
        String str2;
        if (this.T) {
            this.T = this.J;
            return;
        }
        int N2 = this.V.N();
        if (TcmAid.cV != -1) {
            N2 = TcmAid.cV;
        }
        if (N2 == 0 || N2 == 1) {
            str = "pinyin";
        } else {
            str = "english";
        }
        String[] split = this.z.getText().toString().split("- ");
        StringBuffer stringBuffer = new StringBuffer();
        boolean z2 = this.I;
        for (String trim : split) {
            String trim2 = trim.trim();
            if (trim2.length() != 0) {
                if (z2) {
                    str2 = " AND xxxx.formulas.pinyin LIKE '" + trim2 + "' ";
                    z2 = this.J;
                } else {
                    str2 = " OR xxxx.formulas.pinyin LIKE '" + trim2 + "' ";
                }
                stringBuffer.append(str2);
            }
        }
        if (dh.L) {
            Log.d("HD:formulaItems", stringBuffer.toString());
        }
        TcmAid.S = c.a(stringBuffer.toString().replace("xxxx", "main"), stringBuffer.toString().replace("xxxx", "userDB").replace("AND userDB", "AND (userDB") + ')', str);
        TcmAid.Q = null;
        startActivityForResult(new Intent(this, FormulasList.class), 1350);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        e a2 = e.a(motionEvent);
        ImageView imageView = (ImageView) view;
        StringBuilder sb = new StringBuilder();
        int b2 = a2.b();
        int i2 = b2 & 255;
        sb.append("event ACTION_").append(new String[]{"DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE", "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?"}[i2]);
        if (i2 == 5 || i2 == 6) {
            sb.append("(pid ").append(b2 >> 8);
            sb.append(')');
        }
        sb.append('[');
        for (int i3 = 0; i3 < a2.a(); i3++) {
            sb.append('#').append(i3);
            sb.append("(pid ").append(a2.c(i3));
            sb.append(")=").append((int) a2.a(i3));
            sb.append(',').append((int) a2.b(i3));
            if (i3 + 1 < a2.a()) {
                sb.append(';');
            }
        }
        sb.append(']');
        Log.d("HD:dumpEvent()", sb.toString());
        switch (a2.b() & 255) {
            case 0:
                this.ak.set(this.aj);
                this.am.set(a2.c(), a2.d());
                Log.d("HD:onTouch()", "mode=DRAG");
                this.al = 1;
                break;
            case 1:
            case 6:
                this.al = 0;
                Log.d("HD:onTouch()", "mode=NONE");
                break;
            case 2:
                if (this.al != 1) {
                    if (this.al == 2) {
                        float a3 = a(a2);
                        Log.d("HD:onTouch()", "newDist=" + a3);
                        if (a3 > 10.0f) {
                            this.aj.set(this.ak);
                            float f2 = a3 / this.ao;
                            this.aj.postScale(f2, f2, this.an.x, this.an.y);
                            break;
                        }
                    }
                } else {
                    this.aj.set(this.ak);
                    this.aj.postTranslate(a2.c() - this.am.x, a2.d() - this.am.y);
                    break;
                }
                break;
            case 5:
                this.ao = a(a2);
                Log.d("HD:onTouch()", "oldDist=" + this.ao);
                if (this.ao > 10.0f) {
                    this.ak.set(this.aj);
                    this.an.set((a2.a(0) + a2.a(1)) / 2.0f, (a2.b(1) + a2.b(0)) / 2.0f);
                    this.al = 2;
                    Log.d("HD:onTouch()", "mode=ZOOM");
                    break;
                }
                break;
        }
        imageView.setImageMatrix(this.aj);
        return true;
    }

    private static float a(e eVar) {
        float a2 = eVar.a(0) - eVar.a(1);
        float b2 = eVar.b(0) - eVar.b(1);
        return FloatMath.sqrt((a2 * a2) + (b2 * b2));
    }

    public void onClick(View view) {
    }
}
