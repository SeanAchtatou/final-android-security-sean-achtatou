package com.cyberandsons.tcmaidtrial.detailed;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class hc implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HerbsDetail f637a;

    hc(HerbsDetail herbsDetail) {
        this.f637a = herbsDetail;
    }

    public final void onClick(View view) {
        HerbsDetail herbsDetail = this.f637a;
        ((InputMethodManager) herbsDetail.getSystemService("input_method")).hideSoftInputFromWindow(herbsDetail.f430b.getWindowToken(), 0);
    }
}
