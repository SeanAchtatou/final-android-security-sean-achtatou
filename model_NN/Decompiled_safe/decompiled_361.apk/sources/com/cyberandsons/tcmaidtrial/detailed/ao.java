package com.cyberandsons.tcmaidtrial.detailed;

import android.view.MotionEvent;
import android.view.View;

final class ao implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FormulasDetail f458a;

    ao(FormulasDetail formulasDetail) {
        this.f458a = formulasDetail;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f458a.T.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f458a.f;
    }
}
