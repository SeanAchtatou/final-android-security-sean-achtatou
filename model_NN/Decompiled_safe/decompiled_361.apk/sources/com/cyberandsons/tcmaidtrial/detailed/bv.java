package com.cyberandsons.tcmaidtrial.detailed;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class bv implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiagnosisDetail f492a;

    bv(DiagnosisDetail diagnosisDetail) {
        this.f492a = diagnosisDetail;
    }

    public final void onClick(View view) {
        DiagnosisDetail diagnosisDetail = this.f492a;
        ((InputMethodManager) diagnosisDetail.getSystemService("input_method")).hideSoftInputFromWindow(diagnosisDetail.f426b.getWindowToken(), 2);
    }
}
