package com.cyberandsons.tcmaidtrial.detailed;

import android.view.MotionEvent;
import android.view.View;

final class ga implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ WristAnkleDetail f608a;

    ga(WristAnkleDetail wristAnkleDetail) {
        this.f608a = wristAnkleDetail;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f608a.F.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f608a.c;
    }
}
