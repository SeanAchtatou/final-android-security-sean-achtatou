package com.cyberandsons.tcmaidtrial.detailed;

import android.app.ProgressDialog;
import android.database.SQLException;
import android.graphics.Point;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.a.q;
import java.util.Iterator;

final class ib implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PointsDetailMB f663a;

    ib(PointsDetailMB pointsDetailMB) {
        this.f663a = pointsDetailMB;
    }

    public final void onClick(View view) {
        String str;
        if (this.f663a.N) {
            String str2 = "";
            boolean z = this.f663a.e;
            Iterator it = PointsDetailMB.g.iterator();
            while (true) {
                str = str2;
                if (!it.hasNext()) {
                    break;
                }
                Point point = (Point) it.next();
                if (z) {
                    str2 = String.format("%d,%d", Integer.valueOf(point.x), Integer.valueOf(point.y));
                    z = this.f663a.f;
                } else {
                    str2 = str + String.format(",%d,%d", Integer.valueOf(point.x), Integer.valueOf(point.y));
                }
            }
            this.f663a.d = "UPDATE main.pointslocation SET meridian_points='" + str + "' WHERE image='" + this.f663a.f433a + "' AND common_name LIKE '" + this.f663a.f434b + "%'";
            try {
                this.f663a.I.execSQL(this.f663a.d);
                Toast makeText = Toast.makeText(this.f663a, this.f663a.c + " meridian in '" + this.f663a.f433a + "' image are saved!", 1);
                makeText.setGravity(17, makeText.getXOffset() / 2, makeText.getYOffset() / 2);
                makeText.show();
            } catch (SQLException e) {
                q.a(e);
            }
            try {
                ProgressDialog show = ProgressDialog.show(this.f663a, "Submit Email", "Sending....", true);
                show.setIcon((int) C0000R.drawable.email);
                new fa(this, new fb(this, show)).start();
            } catch (Exception e2) {
                Log.e("SendMail", e2.getMessage(), e2);
            }
        }
    }
}
