package com.cyberandsons.tcmaidtrial.detailed;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SixStagesDetail f551a;

    e(SixStagesDetail sixStagesDetail) {
        this.f551a = sixStagesDetail;
    }

    public final void onClick(View view) {
        SixStagesDetail sixStagesDetail = this.f551a;
        ((InputMethodManager) sixStagesDetail.getSystemService("input_method")).hideSoftInputFromWindow(sixStagesDetail.f438b.getWindowToken(), 0);
    }
}
