package com.cyberandsons.tcmaidtrial.detailed;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class gw implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FormulasDetail f630a;

    gw(FormulasDetail formulasDetail) {
        this.f630a = formulasDetail;
    }

    public final void onClick(View view) {
        FormulasDetail formulasDetail = this.f630a;
        ((InputMethodManager) formulasDetail.getSystemService("input_method")).hideSoftInputFromWindow(formulasDetail.c.getWindowToken(), 0);
    }
}
