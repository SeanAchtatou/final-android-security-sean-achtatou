package com.cyberandsons.tcmaidtrial.detailed;

import android.view.View;
import android.widget.Toast;

final class ia implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointsDetailMB f662a;

    ia(PointsDetailMB pointsDetailMB) {
        this.f662a = pointsDetailMB;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.cyberandsons.tcmaidtrial.detailed.PointsDetailMB.a(com.cyberandsons.tcmaidtrial.detailed.PointsDetailMB, boolean):boolean
     arg types: [com.cyberandsons.tcmaidtrial.detailed.PointsDetailMB, int]
     candidates:
      com.cyberandsons.tcmaidtrial.detailed.PointsDetailMB.a(com.cyberandsons.tcmaidtrial.detailed.PointsDetailMB, int):int
      com.cyberandsons.tcmaidtrial.detailed.PointsDetailMB.a(com.cyberandsons.tcmaidtrial.detailed.PointsDetailMB, boolean):boolean */
    public final void onClick(View view) {
        PointsDetailMB.d(this.f662a);
        boolean unused = this.f662a.N = true;
        PointsDetailMB.b(this.f662a);
        Toast makeText = Toast.makeText(this.f662a, "You've stopped, now it's time to save!", 1);
        makeText.setGravity(17, makeText.getXOffset() / 2, makeText.getYOffset() / 2);
        makeText.show();
    }
}
