package com.cyberandsons.tcmaidtrial.detailed;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.Toast;

final class bg extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointsDetail f477a;

    bg(PointsDetail pointsDetail) {
        this.f477a = pointsDetail;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        try {
            if (Math.abs(motionEvent.getY() - motionEvent2.getY()) > 250.0f) {
                return false;
            }
            if (motionEvent.getX() - motionEvent2.getX() <= 120.0f || Math.abs(f) <= 200.0f) {
                if (motionEvent2.getX() - motionEvent.getX() > 120.0f && Math.abs(f) > 200.0f) {
                    this.f477a.b();
                }
                return false;
            }
            Toast.makeText(this.f477a, "Please Swipe to the Right or Double-Tap", 0).show();
            return false;
        } catch (Exception e) {
        }
    }

    public final boolean onDoubleTap(MotionEvent motionEvent) {
        this.f477a.b();
        return true;
    }
}
