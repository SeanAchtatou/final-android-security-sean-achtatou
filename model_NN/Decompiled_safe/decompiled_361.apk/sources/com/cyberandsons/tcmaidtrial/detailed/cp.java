package com.cyberandsons.tcmaidtrial.detailed;

import android.view.View;

final class cp implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PulseDiagDetail f513a;

    cp(PulseDiagDetail pulseDiagDetail) {
        this.f513a = pulseDiagDetail;
    }

    public final void onClick(View view) {
        PulseDiagDetail pulseDiagDetail = this.f513a;
        pulseDiagDetail.setResult(2);
        pulseDiagDetail.finish();
    }
}
