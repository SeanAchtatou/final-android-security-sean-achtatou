package com.cyberandsons.tcmaidtrial.detailed;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class fm extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TungDetail f593a;

    fm(TungDetail tungDetail) {
        this.f593a = tungDetail;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        try {
            if (Math.abs(motionEvent.getY() - motionEvent2.getY()) > 250.0f) {
                return false;
            }
            if (motionEvent.getX() - motionEvent2.getX() <= 120.0f || Math.abs(f) <= 200.0f) {
                if (motionEvent2.getX() - motionEvent.getX() > 120.0f && Math.abs(f) > 200.0f) {
                    this.f593a.a(1);
                }
                return false;
            }
            this.f593a.a(2);
            return false;
        } catch (Exception e) {
        }
    }
}
