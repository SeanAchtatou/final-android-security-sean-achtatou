package com.cyberandsons.tcmaidtrial.detailed;

import android.view.MotionEvent;
import android.view.View;

final class dz implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HerbsDetail f550a;

    dz(HerbsDetail herbsDetail) {
        this.f550a = herbsDetail;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f550a.ah.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f550a.c;
    }
}
