package com.cyberandsons.tcmaidtrial.detailed;

import android.view.MotionEvent;
import android.view.View;

final class gd implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SixStagesDetail f611a;

    gd(SixStagesDetail sixStagesDetail) {
        this.f611a = sixStagesDetail;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f611a.G.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f611a.c;
    }
}
