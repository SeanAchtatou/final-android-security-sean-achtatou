package com.cyberandsons.tcmaidtrial.detailed;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.FloatMath;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.draw.e;
import com.cyberandsons.tcmaidtrial.misc.ad;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class PulseDiagDetail extends Activity implements View.OnClickListener, View.OnTouchListener {
    private ImageButton A;
    private ImageButton B;
    private Button C;
    private boolean D = true;
    private boolean E;
    private boolean F;
    private boolean G;
    private boolean H;
    private boolean I;
    private boolean J;
    private String K;
    private boolean L;
    private SQLiteDatabase M;
    private n N;
    private boolean O;
    /* access modifiers changed from: private */
    public GestureDetector P;
    private View.OnTouchListener Q;
    private Matrix R;
    private Matrix S;
    private int T;
    private PointF U;
    private PointF V;
    private float W;

    /* renamed from: a  reason: collision with root package name */
    ScrollView f435a;

    /* renamed from: b  reason: collision with root package name */
    EditText f436b;
    boolean c;
    private TextView d;
    private ImageView e;
    private TextView f;
    private EditText g;
    private EditText h;
    private EditText i;
    private EditText j;
    private EditText k;
    private EditText l;
    private EditText m;
    private EditText n;
    private EditText o;
    private EditText p;
    private EditText q;
    private EditText r;
    private EditText s;
    private EditText t;
    private EditText u;
    private ImageButton v;
    private ImageButton w;
    private ImageButton x;
    private ImageButton y;
    private ImageButton z;

    public PulseDiagDetail() {
        this.E = !this.D;
        this.F = this.E;
        this.G = this.E;
        this.H = this.E;
        this.I = this.E;
        this.J = this.E;
        this.c = this.D;
        this.L = this.E;
        this.O = this.E;
        this.R = new Matrix();
        this.S = new Matrix();
        this.T = 0;
        this.U = new PointF();
        this.V = new PointF();
        this.W = 1.0f;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.M == null || !this.M.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("PDD:openDataBase()", str + " opened.");
                this.M = SQLiteDatabase.openDatabase(str, null, 16);
                this.M.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.M.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("PDD:openDataBase()", str2 + " ATTACHed.");
                this.N = new n();
                this.N.a(this.M);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new r(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        if (dh.c) {
            this.I = this.N.I();
        }
        this.O = this.N.ad();
        setContentView((int) C0000R.layout.pulsediag_detailed_edit);
        this.d = (TextView) findViewById(C0000R.id.tce_label);
        this.f435a = (ScrollView) findViewById(C0000R.id.svDetail);
        getWindow().setSoftInputMode(3);
        this.P = new GestureDetector(new gz(this));
        this.Q = new av(this);
        this.d.setOnClickListener(this);
        this.d.setOnTouchListener(this.Q);
        a(true);
        this.f435a.smoothScrollBy(0, 0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 2, 0, "Add").setIcon(17301559);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 2:
                b();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void onDestroy() {
        TcmAid.aD = null;
        try {
            if (this.M != null && this.M.isOpen()) {
                this.M.close();
                this.M = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
        if (i3 == -1) {
            Uri data = intent.getData();
            Log.i("onActivityResult", data.toString());
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(getContentResolver().openInputStream(data));
                if (decodeStream != null) {
                    BitmapDrawable a2 = dh.a(decodeStream, TcmAid.cS - 0, TcmAid.cS - 0, getWindow());
                    this.e.setImageDrawable(a2);
                    this.e.setVisibility(0);
                    this.J = this.D;
                    this.w.setImageResource(17301560);
                    this.w.setVisibility(0);
                    String format = String.format("%s/%s_alt.png", getString(C0000R.string.image_path), this.K);
                    a(format, this.D);
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(format);
                        a2.getBitmap().compress(Bitmap.CompressFormat.PNG, 90, fileOutputStream);
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    } catch (Exception e2) {
                        Log.e("PDD:onActivityResult", "Failed to write alternate image: " + e2.getLocalizedMessage());
                    }
                }
            } catch (FileNotFoundException e3) {
                Log.e("PDD:onActivityResult", "Failed to process alternate image: " + e3.getLocalizedMessage());
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            this.L = this.D;
            TcmAid.bl = this.D;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        int i2;
        if (!this.F) {
            int i3 = 131073;
            if (this.N.x()) {
                i3 = 131073 + 16384;
            }
            if (this.N.y()) {
                i2 = i3 + 32768;
            } else {
                i2 = i3;
            }
            this.F = this.D;
            this.c = this.E;
            this.v.setVisibility(4);
            this.w.setVisibility(4);
            this.x.setVisibility(4);
            this.z.setVisibility(4);
            this.A.setVisibility(4);
            this.y.setVisibility(4);
            this.B.setImageResource(17301582);
            this.C.setVisibility(0);
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            inputMethodManager.getInputMethodList();
            inputMethodManager.toggleSoftInput(2, 0);
            ad.a(this.g, i2);
            ad.a(this.h, i2);
            ad.a(this.i, i2);
            ad.a(this.j, i2);
            ad.a(this.k, i2);
            ad.a(this.l, i2);
            ad.a(this.m, i2);
            ad.a(this.n, i2);
            ad.a(this.o, i2);
            ad.a(this.p, i2);
            ad.a(this.q, i2);
            ad.a(this.r, i2);
            ad.a(this.s, i2);
            ad.a(this.t, i2);
            ad.a(this.u, i2);
            ad.a(this.f436b, i2);
            return;
        }
        a((String) null, this.E);
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.f436b.getWindowToken(), 0);
        this.F = this.E;
        this.c = this.D;
        this.v.setVisibility(0);
        if (this.I) {
            this.w.setVisibility(0);
        }
        this.x.setVisibility(0);
        this.z.setVisibility(0);
        this.A.setVisibility(0);
        this.y.setVisibility(0);
        this.B.setImageResource(17301566);
        this.C.setVisibility(4);
        a(false);
    }

    private void a(String str, boolean z2) {
        int i2;
        boolean a2;
        ContentValues contentValues = new ContentValues();
        boolean z3 = this.E;
        if (TcmAid.aH == dh.f946a.intValue()) {
            a2 = z3;
            i2 = TcmAid.aH + 1000;
        } else {
            i2 = TcmAid.aH;
            a2 = q.a("SELECT COUNT(userDB.pulsediag._id) FROM userDB.pulsediag ", i2, this.M);
            if (!a2) {
                contentValues.put("_id", Integer.toString(i2));
            }
        }
        String obj = this.f.getText().toString();
        contentValues.put("name", obj);
        contentValues.put("category", this.g.getText().toString());
        contentValues.put("description", this.h.getText().toString().replaceAll(obj.toLowerCase(), "____"));
        contentValues.put("sensation", this.i.getText().toString());
        contentValues.put("indications", this.j.getText().toString());
        contentValues.put("left_cun", this.k.getText().toString());
        contentValues.put("left_guan", this.l.getText().toString());
        contentValues.put("left_chi", this.m.getText().toString());
        contentValues.put("right_cun", this.n.getText().toString());
        contentValues.put("right_guan", this.o.getText().toString());
        contentValues.put("right_cun", this.p.getText().toString());
        contentValues.put("bilateral_cun", this.q.getText().toString());
        contentValues.put("bilateral_guan", this.r.getText().toString());
        contentValues.put("bilateral_chi", this.s.getText().toString());
        contentValues.put("combinations", this.t.getText().toString());
        contentValues.put("alternate_names", this.u.getText().toString());
        contentValues.put("notes", this.f436b.getText().toString());
        if (z2) {
            contentValues.put("alt_image", str);
        }
        if (!a2) {
            try {
                this.M.insert("userDB.pulsediag", null, contentValues);
            } catch (SQLException e2) {
                q.a(e2);
            }
        } else {
            this.M.update("userDB.pulsediag", contentValues, "_id=" + Integer.toString(i2), null);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!this.J) {
            startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 0);
            return;
        }
        a((String) null, this.D);
        this.J = this.E;
        a(false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x05d0 A[Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }] */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x05de A[Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0603  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x0618  */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x06c4  */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x06cd  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x06d6  */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x0755  */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x075a A[SYNTHETIC, Splitter:B:169:0x075a] */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x0769  */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x0774  */
    /* JADX WARNING: Removed duplicated region for block: B:194:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x04ab  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x04c1 A[Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r13) {
        /*
            r12 = this;
            r10 = 4
            r9 = 0
            r8 = 1
            r7 = 0
            if (r13 == 0) goto L_0x00ed
            r0 = 2131362330(0x7f0a021a, float:1.8344438E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r12.B = r0
            android.widget.ImageButton r0 = r12.B
            com.cyberandsons.tcmaidtrial.detailed.cn r1 = new com.cyberandsons.tcmaidtrial.detailed.cn
            r1.<init>(r12)
            r0.setOnClickListener(r1)
            r0 = 2131362331(0x7f0a021b, float:1.834444E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r12.C = r0
            android.widget.Button r0 = r12.C
            com.cyberandsons.tcmaidtrial.detailed.co r1 = new com.cyberandsons.tcmaidtrial.detailed.co
            r1.<init>(r12)
            r0.setOnClickListener(r1)
            r0 = 2131361811(0x7f0a0013, float:1.8343385E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r12.v = r0
            android.widget.ImageButton r0 = r12.v
            com.cyberandsons.tcmaidtrial.detailed.cp r1 = new com.cyberandsons.tcmaidtrial.detailed.cp
            r1.<init>(r12)
            r0.setOnClickListener(r1)
            r0 = 2131361809(0x7f0a0011, float:1.834338E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r12.w = r0
            android.widget.ImageButton r0 = r12.w
            com.cyberandsons.tcmaidtrial.detailed.cq r1 = new com.cyberandsons.tcmaidtrial.detailed.cq
            r1.<init>(r12)
            r0.setOnClickListener(r1)
            boolean r0 = r12.I
            if (r0 != 0) goto L_0x0063
            android.widget.ImageButton r0 = r12.w
            r0.setVisibility(r10)
        L_0x0063:
            r0 = 2131361815(0x7f0a0017, float:1.8343393E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r12.x = r0
            android.widget.ImageButton r0 = r12.x
            com.cyberandsons.tcmaidtrial.detailed.cv r1 = new com.cyberandsons.tcmaidtrial.detailed.cv
            r1.<init>(r12)
            r0.setOnClickListener(r1)
            r0 = 2131361814(0x7f0a0016, float:1.834339E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r12.z = r0
            android.widget.ImageButton r0 = r12.z
            com.cyberandsons.tcmaidtrial.detailed.cu r1 = new com.cyberandsons.tcmaidtrial.detailed.cu
            r1.<init>(r12)
            r0.setOnClickListener(r1)
            r0 = 2131361813(0x7f0a0015, float:1.8343389E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r12.A = r0
            android.widget.ImageButton r0 = r12.A
            com.cyberandsons.tcmaidtrial.detailed.ct r1 = new com.cyberandsons.tcmaidtrial.detailed.ct
            r1.<init>(r12)
            r0.setOnClickListener(r1)
            r0 = 2131361812(0x7f0a0014, float:1.8343387E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r12.y = r0
            android.widget.ImageButton r0 = r12.y
            com.cyberandsons.tcmaidtrial.detailed.cs r1 = new com.cyberandsons.tcmaidtrial.detailed.cs
            r1.<init>(r12)
            r0.setOnClickListener(r1)
            boolean r0 = r12.O
            if (r0 == 0) goto L_0x00c5
            android.widget.ImageButton r0 = r12.x
            r0.setVisibility(r10)
            android.widget.ImageButton r0 = r12.y
            r0.setVisibility(r10)
        L_0x00c5:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.ay
            if (r0 != 0) goto L_0x0622
            boolean r0 = r12.O
            if (r0 != 0) goto L_0x00d4
            android.widget.ImageButton r0 = r12.x
            boolean r1 = r12.E
            r12.a(r0, r1)
        L_0x00d4:
            android.widget.ImageButton r0 = r12.z
            boolean r1 = r12.E
            r12.a(r0, r1)
            android.widget.ImageButton r0 = r12.A
            boolean r1 = r12.D
            r12.a(r0, r1)
            boolean r0 = r12.O
            if (r0 != 0) goto L_0x00ed
            android.widget.ImageButton r0 = r12.y
            boolean r1 = r12.D
            r12.a(r0, r1)
        L_0x00ed:
            r0 = 2131362142(0x7f0a015e, float:1.8344056E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r12.e = r0
            android.widget.ImageView r0 = r12.e
            r0.setOnTouchListener(r12)
            android.graphics.Matrix r0 = r12.R
            r1 = 1065353216(0x3f800000, float:1.0)
            r2 = 1065353216(0x3f800000, float:1.0)
            r0.setTranslate(r1, r2)
            android.widget.ImageView r0 = r12.e
            android.graphics.Matrix r1 = r12.R
            r0.setImageMatrix(r1)
            android.widget.TextView r0 = r12.f
            if (r0 == 0) goto L_0x0113
            r12.f = r7
        L_0x0113:
            r0 = 2131362144(0x7f0a0160, float:1.834406E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r12.f = r0
            android.widget.TextView r0 = r12.f
            com.cyberandsons.tcmaidtrial.detailed.cr r1 = new com.cyberandsons.tcmaidtrial.detailed.cr
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r12.g
            if (r0 == 0) goto L_0x012e
            r12.g = r7
        L_0x012e:
            r0 = 2131362148(0x7f0a0164, float:1.8344068E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r12.g = r0
            android.widget.EditText r0 = r12.g
            com.cyberandsons.tcmaidtrial.detailed.ap r1 = new com.cyberandsons.tcmaidtrial.detailed.ap
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r12.h
            if (r0 == 0) goto L_0x0149
            r12.h = r7
        L_0x0149:
            r0 = 2131362150(0x7f0a0166, float:1.8344072E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r12.h = r0
            android.widget.EditText r0 = r12.h
            com.cyberandsons.tcmaidtrial.detailed.ar r1 = new com.cyberandsons.tcmaidtrial.detailed.ar
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r12.i
            if (r0 == 0) goto L_0x0164
            r12.i = r7
        L_0x0164:
            r0 = 2131362152(0x7f0a0168, float:1.8344077E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r12.i = r0
            android.widget.EditText r0 = r12.i
            com.cyberandsons.tcmaidtrial.detailed.as r1 = new com.cyberandsons.tcmaidtrial.detailed.as
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r12.j
            if (r0 == 0) goto L_0x017f
            r12.j = r7
        L_0x017f:
            r0 = 2131362154(0x7f0a016a, float:1.834408E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r12.j = r0
            android.widget.EditText r0 = r12.j
            com.cyberandsons.tcmaidtrial.detailed.au r1 = new com.cyberandsons.tcmaidtrial.detailed.au
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r12.k
            if (r0 == 0) goto L_0x019a
            r12.k = r7
        L_0x019a:
            r0 = 2131362156(0x7f0a016c, float:1.8344085E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r12.k = r0
            android.widget.EditText r0 = r12.k
            com.cyberandsons.tcmaidtrial.detailed.ai r1 = new com.cyberandsons.tcmaidtrial.detailed.ai
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r12.l
            if (r0 == 0) goto L_0x01b5
            r12.l = r7
        L_0x01b5:
            r0 = 2131362158(0x7f0a016e, float:1.8344089E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r12.l = r0
            android.widget.EditText r0 = r12.l
            com.cyberandsons.tcmaidtrial.detailed.aj r1 = new com.cyberandsons.tcmaidtrial.detailed.aj
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r12.m
            if (r0 == 0) goto L_0x01d0
            r12.m = r7
        L_0x01d0:
            r0 = 2131362160(0x7f0a0170, float:1.8344093E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r12.m = r0
            android.widget.EditText r0 = r12.m
            com.cyberandsons.tcmaidtrial.detailed.ak r1 = new com.cyberandsons.tcmaidtrial.detailed.ak
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r12.n
            if (r0 == 0) goto L_0x01eb
            r12.n = r7
        L_0x01eb:
            r0 = 2131362162(0x7f0a0172, float:1.8344097E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r12.n = r0
            android.widget.EditText r0 = r12.n
            com.cyberandsons.tcmaidtrial.detailed.al r1 = new com.cyberandsons.tcmaidtrial.detailed.al
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r12.o
            if (r0 == 0) goto L_0x0206
            r12.o = r7
        L_0x0206:
            r0 = 2131362164(0x7f0a0174, float:1.83441E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r12.o = r0
            android.widget.EditText r0 = r12.o
            com.cyberandsons.tcmaidtrial.detailed.am r1 = new com.cyberandsons.tcmaidtrial.detailed.am
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r12.p
            if (r0 == 0) goto L_0x0221
            r12.p = r7
        L_0x0221:
            r0 = 2131362166(0x7f0a0176, float:1.8344105E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r12.p = r0
            android.widget.EditText r0 = r12.p
            com.cyberandsons.tcmaidtrial.detailed.an r1 = new com.cyberandsons.tcmaidtrial.detailed.an
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r12.q
            if (r0 == 0) goto L_0x023c
            r12.q = r7
        L_0x023c:
            r0 = 2131362168(0x7f0a0178, float:1.8344109E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r12.q = r0
            android.widget.EditText r0 = r12.q
            com.cyberandsons.tcmaidtrial.detailed.v r1 = new com.cyberandsons.tcmaidtrial.detailed.v
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r12.r
            if (r0 == 0) goto L_0x0257
            r12.r = r7
        L_0x0257:
            r0 = 2131362170(0x7f0a017a, float:1.8344113E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r12.r = r0
            android.widget.EditText r0 = r12.r
            com.cyberandsons.tcmaidtrial.detailed.ad r1 = new com.cyberandsons.tcmaidtrial.detailed.ad
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r12.s
            if (r0 == 0) goto L_0x0272
            r12.s = r7
        L_0x0272:
            r0 = 2131362172(0x7f0a017c, float:1.8344117E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r12.s = r0
            android.widget.EditText r0 = r12.s
            com.cyberandsons.tcmaidtrial.detailed.af r1 = new com.cyberandsons.tcmaidtrial.detailed.af
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r12.t
            if (r0 == 0) goto L_0x028d
            r12.t = r7
        L_0x028d:
            r0 = 2131362174(0x7f0a017e, float:1.8344121E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r12.t = r0
            android.widget.EditText r0 = r12.t
            com.cyberandsons.tcmaidtrial.detailed.z r1 = new com.cyberandsons.tcmaidtrial.detailed.z
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r12.u
            if (r0 == 0) goto L_0x02a8
            r12.u = r7
        L_0x02a8:
            r0 = 2131362146(0x7f0a0162, float:1.8344064E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r12.u = r0
            android.widget.EditText r0 = r12.u
            com.cyberandsons.tcmaidtrial.detailed.ab r1 = new com.cyberandsons.tcmaidtrial.detailed.ab
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r12.f436b
            if (r0 == 0) goto L_0x02c3
            r12.f436b = r7
        L_0x02c3:
            r0 = 2131362176(0x7f0a0180, float:1.8344125E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r12.f436b = r0
            android.widget.EditText r0 = r12.f436b
            com.cyberandsons.tcmaidtrial.detailed.p r1 = new com.cyberandsons.tcmaidtrial.detailed.p
            r1.<init>(r12)
            r0.setOnTouchListener(r1)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT main.pulsediag._id, main.pulsediag.name, main.pulsediag.pinyin, main.pulsediag.category, main.pulsediag.description, main.pulsediag.sensation, main.pulsediag.indications, main.pulsediag.left_cun, main.pulsediag.left_guan, main.pulsediag.left_chi, main.pulsediag.right_cun, main.pulsediag.right_guan, main.pulsediag.right_chi, main.pulsediag.bilateral_cun, main.pulsediag.bilateral_guan, main.pulsediag.bilateral_chi, main.pulsediag.combinations, main.pulsediag.alternate_names, main.pulsediag.notes, main.tonemarks.pdtonemarks, main.tonemarks.pdscc, main.tonemarks.pdtcc FROM main.pulsediag JOIN main.tonemarks ON main.tonemarks.pdid = main.pulsediag._id WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.aD
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            com.cyberandsons.tcmaidtrial.a.n r0 = r12.N
            int r2 = r0.ac()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.C
            if (r0 == 0) goto L_0x02fc
            java.lang.String r0 = "PDD:loadSystemSuppliedData()"
            android.util.Log.d(r0, r1)
        L_0x02fc:
            android.database.sqlite.SQLiteDatabase r0 = r12.M     // Catch:{ SQLException -> 0x0797, NullPointerException -> 0x0793, IOException -> 0x06c9, all -> 0x06d2 }
            r3 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r3)     // Catch:{ SQLException -> 0x0797, NullPointerException -> 0x0793, IOException -> 0x06c9, all -> 0x06d2 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0797, NullPointerException -> 0x0793, IOException -> 0x06c9, all -> 0x06d2 }
            int r3 = r0.getCount()     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            boolean r4 = com.cyberandsons.tcmaidtrial.misc.dh.C     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            if (r4 == 0) goto L_0x034f
            java.lang.String r4 = "PDD:loadSystemSuppliedData()"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r5.<init>()     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.String r6 = "query count = "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.String r6 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.String r5 = r5.toString()     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.util.Log.d(r4, r5)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.String r4 = "PDD:loadSystemSuppliedData()"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r5.<init>()     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.String r6 = "column count = '"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            int r6 = r0.getColumnCount()     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.String r6 = "' "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.String r5 = r5.toString()     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.util.Log.d(r4, r5)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
        L_0x034f:
            boolean r4 = r0.moveToFirst()     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            if (r4 == 0) goto L_0x0473
            if (r3 != r8) goto L_0x0473
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.TextView r4 = r12.f     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4.setText(r3)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.EditText r4 = r12.g     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r5 = 3
            java.lang.String r5 = r0.getString(r5)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4.setText(r5)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 4
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.String r5 = "____"
            java.lang.String r3 = r3.toLowerCase()     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.String r3 = r4.replaceAll(r5, r3)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.EditText r4 = r12.h     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4.setText(r3)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.EditText r3 = r12.i     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 5
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.setText(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.EditText r3 = r12.j     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 6
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.setText(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.EditText r3 = r12.k     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 7
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.setText(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.EditText r3 = r12.l     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 8
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.setText(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.EditText r3 = r12.m     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 9
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.setText(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.EditText r3 = r12.n     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 10
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.setText(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.EditText r3 = r12.o     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 11
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.setText(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.EditText r3 = r12.p     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 12
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.setText(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.EditText r3 = r12.q     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 13
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.setText(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.EditText r3 = r12.r     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 14
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.setText(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.EditText r3 = r12.s     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 15
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.setText(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.EditText r3 = r12.t     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 16
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.setText(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.EditText r3 = r12.u     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 17
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.setText(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.EditText r3 = r12.f436b     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 18
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.setText(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.<init>()     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 1
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.misc.ad.a(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 32
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 19
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r4 = 32
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            int r2 = r2 + 20
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.TextView r3 = r12.d     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.setText(r2)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r2 = 1
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r12.K = r2     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            boolean r2 = r12.I     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            if (r2 == 0) goto L_0x067c
            android.content.Context r2 = r12.getApplicationContext()     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.graphics.Bitmap r2 = com.cyberandsons.tcmaidtrial.misc.dh.a(r2, r3)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            if (r2 != 0) goto L_0x0653
            android.widget.ImageView r2 = r12.e     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3 = 2130837543(0x7f020027, float:1.7280043E38)
            r2.setImageResource(r3)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
        L_0x0473:
            if (r0 == 0) goto L_0x0478
            r0.close()
        L_0x0478:
            boolean r0 = r12.E
            r12.J = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT userDB.pulsediag._id, userDB.pulsediag.name, userDB.pulsediag.pinyin, userDB.pulsediag.category, userDB.pulsediag.description, userDB.pulsediag.sensation, userDB.pulsediag.indications, userDB.pulsediag.left_cun, userDB.pulsediag.left_guan, userDB.pulsediag.left_chi, userDB.pulsediag.right_cun, userDB.pulsediag.right_guan, userDB.pulsediag.right_chi, userDB.pulsediag.bilateral_cun, userDB.pulsediag.bilateral_guan, userDB.pulsediag.bilateral_chi, userDB.pulsediag.combinations, userDB.pulsediag.alternate_names, userDB.pulsediag.notes, userDB.pulsediag.alt_image FROM userDB.pulsediag WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "userDB.pulsediag."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "_id"
            java.lang.StringBuilder r0 = r0.append(r1)
            r1 = 61
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.aH
            java.lang.String r1 = java.lang.Integer.toString(r1)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.C
            if (r0 == 0) goto L_0x04b0
            java.lang.String r0 = "PDD:loadUserModifiedData()"
            android.util.Log.d(r0, r1)
        L_0x04b0:
            android.database.sqlite.SQLiteDatabase r0 = r12.M     // Catch:{ SQLException -> 0x0783, NullPointerException -> 0x0780, all -> 0x077b }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x0783, NullPointerException -> 0x0780, all -> 0x077b }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0783, NullPointerException -> 0x0780, all -> 0x077b }
            int r2 = r0.getCount()     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.C     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            if (r3 == 0) goto L_0x0503
            java.lang.String r3 = "PDD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r4.<init>()     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            java.lang.String r5 = "query count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            java.lang.String r5 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            java.lang.String r3 = "PDD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r4.<init>()     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            java.lang.String r5 = "column count = '"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            int r5 = r0.getColumnCount()     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            java.lang.String r5 = "' "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
        L_0x0503:
            boolean r3 = r0.moveToFirst()     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            if (r3 == 0) goto L_0x05fa
            if (r2 != r8) goto L_0x05fa
            r2 = 1
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.TextView r3 = r12.f     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3.setText(r2)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.EditText r3 = r12.g     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r4 = 3
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3.setText(r4)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 4
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            java.lang.String r4 = "____"
            java.lang.String r2 = r2.toLowerCase()     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            java.lang.String r2 = r3.replaceAll(r4, r2)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.EditText r3 = r12.h     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3.setText(r2)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.EditText r2 = r12.i     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 5
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.EditText r2 = r12.j     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 6
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.EditText r2 = r12.k     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 7
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.EditText r2 = r12.l     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 8
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.EditText r2 = r12.m     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 9
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.EditText r2 = r12.n     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 10
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.EditText r2 = r12.o     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 11
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.EditText r2 = r12.p     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 12
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.EditText r2 = r12.q     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 13
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.EditText r2 = r12.r     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 14
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.EditText r2 = r12.s     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 15
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.EditText r2 = r12.t     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 16
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.EditText r2 = r12.u     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 17
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.EditText r2 = r12.f436b     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 18
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.aH     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r2 <= r3) goto L_0x05da
            android.widget.TextView r2 = r12.d     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r2.setText(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
        L_0x05da:
            boolean r2 = r12.I     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            if (r2 == 0) goto L_0x075a
            r2 = 19
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            if (r2 == 0) goto L_0x0707
            int r3 = r2.length()     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            if (r3 <= 0) goto L_0x0707
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeFile(r2)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            if (r2 != 0) goto L_0x06da
            android.widget.ImageView r2 = r12.e     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 2130837543(0x7f020027, float:1.7280043E38)
            r2.setImageResource(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
        L_0x05fa:
            if (r0 == 0) goto L_0x05ff
            r0.close()
        L_0x05ff:
            boolean r0 = r12.I
            if (r0 == 0) goto L_0x0774
            boolean r0 = r12.J
            if (r0 == 0) goto L_0x076d
            android.widget.ImageButton r0 = r12.w
            r1 = 17301560(0x1080038, float:2.4979412E-38)
            r0.setImageResource(r1)
            android.widget.ImageButton r0 = r12.w
            r0.setVisibility(r9)
        L_0x0614:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.cR
            if (r0 != r8) goto L_0x0621
            com.cyberandsons.tcmaidtrial.a.n r0 = r12.N
            java.lang.String r0 = r0.J()
            r12.a(r0)
        L_0x0621:
            return
        L_0x0622:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.ay
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.az
            int r1 = r1.size()
            int r1 = r1 - r8
            if (r0 != r1) goto L_0x00ed
            boolean r0 = r12.O
            if (r0 != 0) goto L_0x0638
            android.widget.ImageButton r0 = r12.x
            boolean r1 = r12.D
            r12.a(r0, r1)
        L_0x0638:
            android.widget.ImageButton r0 = r12.z
            boolean r1 = r12.D
            r12.a(r0, r1)
            android.widget.ImageButton r0 = r12.A
            boolean r1 = r12.E
            r12.a(r0, r1)
            boolean r0 = r12.O
            if (r0 != 0) goto L_0x00ed
            android.widget.ImageButton r0 = r12.y
            boolean r1 = r12.E
            r12.a(r0, r1)
            goto L_0x00ed
        L_0x0653:
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            int r3 = r3 - r9
            int r4 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            int r4 = r4 - r9
            android.view.Window r5 = r12.getWindow()     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.graphics.drawable.BitmapDrawable r2 = com.cyberandsons.tcmaidtrial.misc.dh.a(r2, r3, r4, r5)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.ImageView r3 = r12.e     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3.setImageDrawable(r2)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            android.widget.ImageView r2 = r12.e     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3 = 0
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            goto L_0x0473
        L_0x066e:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
        L_0x0672:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x078d }
            if (r1 == 0) goto L_0x0478
            r1.close()
            goto L_0x0478
        L_0x067c:
            android.widget.ImageView r2 = r12.e     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            r3 = 8
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x066e, NullPointerException -> 0x0685, IOException -> 0x0790 }
            goto L_0x0473
        L_0x0685:
            r2 = move-exception
        L_0x0686:
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0787 }
            java.lang.String r3 = "myVariable"
            r2.a(r3, r1)     // Catch:{ all -> 0x0787 }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0787 }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x0787 }
            java.lang.String r3 = "PDD:loadSystemSuppliedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x0787 }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x0787 }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x0787 }
            r1.<init>(r12)     // Catch:{ all -> 0x0787 }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x0787 }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x0787 }
            r2 = 2131099673(0x7f060019, float:1.7811706E38)
            java.lang.String r2 = r12.getString(r2)     // Catch:{ all -> 0x0787 }
            r1.setMessage(r2)     // Catch:{ all -> 0x0787 }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.q r3 = new com.cyberandsons.tcmaidtrial.detailed.q     // Catch:{ all -> 0x0787 }
            r3.<init>(r12)     // Catch:{ all -> 0x0787 }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x0787 }
            r1.show()     // Catch:{ all -> 0x0787 }
            if (r0 == 0) goto L_0x0478
            r0.close()
            goto L_0x0478
        L_0x06c9:
            r0 = move-exception
            r0 = r7
        L_0x06cb:
            if (r0 == 0) goto L_0x0478
            r0.close()
            goto L_0x0478
        L_0x06d2:
            r0 = move-exception
            r1 = r7
        L_0x06d4:
            if (r1 == 0) goto L_0x06d9
            r1.close()
        L_0x06d9:
            throw r0
        L_0x06da:
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            int r3 = r3 - r9
            int r4 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            int r4 = r4 - r9
            android.view.Window r5 = r12.getWindow()     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.graphics.drawable.BitmapDrawable r2 = com.cyberandsons.tcmaidtrial.misc.dh.a(r2, r3, r4, r5)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.ImageView r3 = r12.e     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3.setImageDrawable(r2)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            android.widget.ImageView r2 = r12.e     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 0
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            boolean r2 = r12.D     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r12.J = r2     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            goto L_0x05fa
        L_0x06f9:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
        L_0x06fd:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x077e }
            if (r1 == 0) goto L_0x05ff
            r1.close()
            goto L_0x05ff
        L_0x0707:
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.aH     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r2 <= r3) goto L_0x05fa
            android.widget.ImageView r2 = r12.e     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 8
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            goto L_0x05fa
        L_0x0716:
            r2 = move-exception
        L_0x0717:
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0763 }
            java.lang.String r3 = "myVariable"
            r2.a(r3, r1)     // Catch:{ all -> 0x0763 }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0763 }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x0763 }
            java.lang.String r3 = "PDD:loadUserModifiedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x0763 }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x0763 }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x0763 }
            r1.<init>(r12)     // Catch:{ all -> 0x0763 }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x0763 }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x0763 }
            r2 = 2131099674(0x7f06001a, float:1.7811708E38)
            java.lang.String r2 = r12.getString(r2)     // Catch:{ all -> 0x0763 }
            r1.setMessage(r2)     // Catch:{ all -> 0x0763 }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.n r3 = new com.cyberandsons.tcmaidtrial.detailed.n     // Catch:{ all -> 0x0763 }
            r3.<init>(r12)     // Catch:{ all -> 0x0763 }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x0763 }
            r1.show()     // Catch:{ all -> 0x0763 }
            if (r0 == 0) goto L_0x05ff
            r0.close()
            goto L_0x05ff
        L_0x075a:
            android.widget.ImageView r2 = r12.e     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            r3 = 8
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x06f9, NullPointerException -> 0x0716 }
            goto L_0x05fa
        L_0x0763:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
        L_0x0767:
            if (r1 == 0) goto L_0x076c
            r1.close()
        L_0x076c:
            throw r0
        L_0x076d:
            android.widget.ImageButton r0 = r12.w
            r0.setVisibility(r10)
            goto L_0x0614
        L_0x0774:
            android.widget.ImageButton r0 = r12.w
            r0.setVisibility(r10)
            goto L_0x0614
        L_0x077b:
            r0 = move-exception
            r1 = r7
            goto L_0x0767
        L_0x077e:
            r0 = move-exception
            goto L_0x0767
        L_0x0780:
            r0 = move-exception
            r0 = r7
            goto L_0x0717
        L_0x0783:
            r0 = move-exception
            r1 = r7
            goto L_0x06fd
        L_0x0787:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x06d4
        L_0x078d:
            r0 = move-exception
            goto L_0x06d4
        L_0x0790:
            r1 = move-exception
            goto L_0x06cb
        L_0x0793:
            r0 = move-exception
            r0 = r7
            goto L_0x0686
        L_0x0797:
            r0 = move-exception
            r1 = r7
            goto L_0x0672
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.detailed.PulseDiagDetail.a(boolean):void");
    }

    private void a(String str) {
        if (str != null && str.length() != 0) {
            String[] split = str.split(",");
            for (String parseInt : split) {
                switch (Integer.parseInt(parseInt)) {
                    case 2:
                        dh.a(this.g.getText());
                        break;
                    case 3:
                        dh.a(this.h.getText());
                        break;
                    case 4:
                        dh.a(this.i.getText());
                        break;
                    case 5:
                        dh.a(this.j.getText());
                        break;
                    case 6:
                        dh.a(this.k.getText());
                        break;
                    case 7:
                        dh.a(this.l.getText());
                        break;
                    case 8:
                        dh.a(this.m.getText());
                        break;
                    case 9:
                        dh.a(this.n.getText());
                        break;
                    case 10:
                        dh.a(this.o.getText());
                        break;
                    case 11:
                        dh.a(this.p.getText());
                        break;
                    case 12:
                        dh.a(this.q.getText());
                        break;
                    case 13:
                        dh.a(this.r.getText());
                        break;
                    case 14:
                        dh.a(this.s.getText());
                        break;
                    case 15:
                        dh.a(this.t.getText());
                        break;
                    case 16:
                        dh.a(this.u.getText());
                        break;
                    case 17:
                        dh.a(this.f436b.getText());
                        break;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        int size;
        int size2;
        Integer num;
        boolean z2 = this.E;
        switch (i2) {
            case 0:
                TcmAid.ay = 0;
                if (!this.O) {
                    a(this.x, this.E);
                }
                a(this.z, this.E);
                a(this.A, this.D);
                if (!this.O) {
                    a(this.y, this.D);
                }
                z2 = this.D;
                break;
            case 1:
                if (TcmAid.ay - 1 >= 0) {
                    if (TcmAid.ay - 1 == 0) {
                        if (!this.O) {
                            a(this.x, this.E);
                        }
                        a(this.z, this.E);
                    }
                    if (this.A.isEnabled() == this.E) {
                        a(this.A, this.D);
                        if (!this.O) {
                            a(this.y, this.D);
                        }
                    }
                    TcmAid.ay--;
                    z2 = this.D;
                    break;
                }
                break;
            case 2:
                if (this.H) {
                    size2 = TcmAid.aB.size();
                } else {
                    size2 = TcmAid.az.size();
                }
                if (TcmAid.ay + 1 < size2) {
                    if (TcmAid.ay + 1 == size2 - 1) {
                        a(this.A, this.E);
                        if (!this.O) {
                            a(this.y, this.E);
                        }
                    }
                    if (this.z.isEnabled() == this.E) {
                        if (!this.O) {
                            a(this.x, this.D);
                        }
                        a(this.z, this.D);
                    }
                    TcmAid.ay++;
                    z2 = this.D;
                    break;
                }
                break;
            case 3:
                if (this.H) {
                    size = TcmAid.aB.size();
                } else {
                    size = TcmAid.az.size();
                }
                TcmAid.ay = size - 1;
                if (!this.O) {
                    a(this.x, this.D);
                }
                a(this.z, this.D);
                a(this.A, this.E);
                if (!this.O) {
                    a(this.y, this.E);
                }
                z2 = this.D;
                break;
        }
        if (z2) {
            if (this.H) {
                if (TcmAid.ay >= TcmAid.aB.size()) {
                    TcmAid.ay = TcmAid.aB.size() - 1;
                }
            } else if (TcmAid.ay >= TcmAid.az.size()) {
                TcmAid.ay = TcmAid.az.size() - 1;
            }
            if (this.H) {
                num = (Integer) TcmAid.aB.get(TcmAid.ay);
            } else {
                num = (Integer) TcmAid.az.get(TcmAid.ay);
            }
            int intValue = num.intValue();
            TcmAid.aD = "main.pulsediag._id=" + Integer.toString(intValue);
            TcmAid.aH = intValue;
            a(false);
            this.f435a.post(new o(this));
        }
    }

    private void a(ImageButton imageButton, boolean z2) {
        if (z2) {
            imageButton.setEnabled(this.D);
            imageButton.setVisibility(0);
            return;
        }
        imageButton.setEnabled(this.E);
        imageButton.setVisibility(4);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        e a2 = e.a(motionEvent);
        ImageView imageView = (ImageView) view;
        StringBuilder sb = new StringBuilder();
        int b2 = a2.b();
        int i2 = b2 & 255;
        sb.append("event ACTION_").append(new String[]{"DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE", "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?"}[i2]);
        if (i2 == 5 || i2 == 6) {
            sb.append("(pid ").append(b2 >> 8);
            sb.append(')');
        }
        sb.append('[');
        for (int i3 = 0; i3 < a2.a(); i3++) {
            sb.append('#').append(i3);
            sb.append("(pid ").append(a2.c(i3));
            sb.append(")=").append((int) a2.a(i3));
            sb.append(',').append((int) a2.b(i3));
            if (i3 + 1 < a2.a()) {
                sb.append(';');
            }
        }
        sb.append(']');
        Log.d("PDD:dumpEvent()", sb.toString());
        switch (a2.b() & 255) {
            case 0:
                this.S.set(this.R);
                this.U.set(a2.c(), a2.d());
                Log.d("PDD:onTouch()", "mode=DRAG");
                this.T = 1;
                break;
            case 1:
            case 6:
                this.T = 0;
                Log.d("PDD:onTouch()", "mode=NONE");
                break;
            case 2:
                if (this.T != 1) {
                    if (this.T == 2) {
                        float a3 = a(a2);
                        Log.d("PDD:onTouch()", "newDist=" + a3);
                        if (a3 > 10.0f) {
                            this.R.set(this.S);
                            float f2 = a3 / this.W;
                            this.R.postScale(f2, f2, this.V.x, this.V.y);
                            break;
                        }
                    }
                } else {
                    this.R.set(this.S);
                    this.R.postTranslate(a2.c() - this.U.x, a2.d() - this.U.y);
                    break;
                }
                break;
            case 5:
                this.W = a(a2);
                Log.d("PDD:onTouch()", "oldDist=" + this.W);
                if (this.W > 10.0f) {
                    this.S.set(this.R);
                    this.V.set((a2.a(0) + a2.a(1)) / 2.0f, (a2.b(1) + a2.b(0)) / 2.0f);
                    this.T = 2;
                    Log.d("PDD:onTouch()", "mode=ZOOM");
                    break;
                }
                break;
        }
        imageView.setImageMatrix(this.R);
        return true;
    }

    private static float a(e eVar) {
        float a2 = eVar.a(0) - eVar.a(1);
        float b2 = eVar.b(0) - eVar.b(1);
        return FloatMath.sqrt((a2 * a2) + (b2 * b2));
    }

    public void onClick(View view) {
    }
}
