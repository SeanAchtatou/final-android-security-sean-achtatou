package com.cyberandsons.tcmaidtrial.detailed;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.FloatMath;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.b;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.draw.e;
import com.cyberandsons.tcmaidtrial.misc.ad;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class TungDetail extends Activity implements View.OnClickListener, View.OnTouchListener {
    private boolean A;
    private boolean B;
    private boolean C;
    private boolean D;
    private String E;
    private boolean F;
    private boolean G;
    private boolean H;
    private boolean I;
    private SQLiteDatabase J;
    private n K;
    private boolean L;
    /* access modifiers changed from: private */
    public GestureDetector M;
    private View.OnTouchListener N;
    private Matrix O;
    private Matrix P;
    private int Q;
    private PointF R;
    private PointF S;
    private float T;

    /* renamed from: a  reason: collision with root package name */
    ScrollView f439a;

    /* renamed from: b  reason: collision with root package name */
    EditText f440b;
    boolean c;
    private TextView d;
    private ImageView e;
    private TextView f;
    private EditText g;
    private EditText h;
    private EditText i;
    private EditText j;
    private EditText k;
    private ImageButton l;
    private ImageButton m;
    private ImageButton n;
    private ImageButton o;
    private ImageButton p;
    private ImageButton q;
    private ImageButton r;
    private ImageButton s;
    private Button t;
    private boolean u = true;
    private boolean v;
    private boolean w;
    private boolean x;
    private int y;
    private int z;

    public TungDetail() {
        this.v = !this.u;
        this.w = this.v;
        this.x = this.v;
        this.A = this.v;
        this.B = this.v;
        this.C = this.v;
        this.D = this.v;
        this.c = this.u;
        this.F = this.v;
        this.G = this.u;
        this.H = this.u;
        this.I = this.v;
        this.L = this.v;
        this.O = new Matrix();
        this.P = new Matrix();
        this.Q = 0;
        this.R = new PointF();
        this.S = new PointF();
        this.T = 1.0f;
    }

    static /* synthetic */ void a(TungDetail tungDetail) {
        if (tungDetail.C) {
            tungDetail.F = !tungDetail.F;
            if (!tungDetail.F) {
                tungDetail.m.setImageResource(C0000R.drawable.tags);
            } else {
                tungDetail.m.setImageResource(C0000R.drawable.tag);
            }
        }
        tungDetail.c();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.J == null || !this.J.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("TD:openDataBase()", str + " opened.");
                this.J = SQLiteDatabase.openDatabase(str, null, 16);
                this.J.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.J.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("TD:openDataBase()", str2 + " ATTACHed.");
                this.K = new n();
                this.K.a(this.J);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new be(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        this.C = this.K.c();
        this.H = this.K.C();
        this.B = this.K.m();
        this.L = this.K.ad();
        setContentView((int) C0000R.layout.tung_detailed_edit);
        this.d = (TextView) findViewById(C0000R.id.tce_label);
        this.f439a = (ScrollView) findViewById(C0000R.id.svDetail);
        getWindow().setSoftInputMode(3);
        this.M = new GestureDetector(new fm(this));
        this.N = new cd(this);
        this.d.setOnClickListener(this);
        this.d.setOnTouchListener(this.N);
        a(true);
        this.f439a.smoothScrollBy(0, 0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 2, 0, "Add").setIcon(17301559);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 2:
                b();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void onDestroy() {
        TcmAid.aW = null;
        try {
            if (this.J != null && this.J.isOpen()) {
                this.J.close();
                this.J = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
        if (i3 == -1) {
            Uri data = intent.getData();
            Log.i("onActivityResult", data.toString());
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(getContentResolver().openInputStream(data));
                if (decodeStream != null) {
                    BitmapDrawable a2 = dh.a(decodeStream, TcmAid.cS - 0, TcmAid.cS - 0, getWindow());
                    this.e.setImageDrawable(a2);
                    this.e.setVisibility(0);
                    this.D = this.u;
                    this.n.setImageResource(17301560);
                    this.n.setVisibility(0);
                    String format = String.format("%s/%s_alt.png", getString(C0000R.string.image_path), this.E);
                    a(format, this.u);
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(format);
                        a2.getBitmap().compress(Bitmap.CompressFormat.PNG, 90, fileOutputStream);
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    } catch (Exception e2) {
                        Log.e("TD:onActivityResult", "Failed to write alternate image: " + e2.getLocalizedMessage());
                    }
                }
            } catch (FileNotFoundException e3) {
                Log.e("TD:onActivityResult", "Failed to process alternate image: " + e3.getLocalizedMessage());
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            this.I = this.u;
            TcmAid.bl = this.u;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        int i2;
        int i3;
        int i4;
        int i5;
        if (!this.w) {
            if (this.K.x()) {
                i2 = 1 + 16384;
                i3 = 131073 + 16384;
            } else {
                i2 = 1;
                i3 = 131073;
            }
            if (this.K.y()) {
                i4 = i2 + 32768;
                i5 = i3 + 32768;
            } else {
                i4 = i2;
                i5 = i3;
            }
            this.w = this.u;
            this.c = this.v;
            this.l.setVisibility(4);
            this.m.setVisibility(4);
            this.n.setVisibility(4);
            this.o.setVisibility(4);
            this.q.setVisibility(4);
            this.r.setVisibility(4);
            this.p.setVisibility(4);
            this.s.setImageResource(17301582);
            this.t.setVisibility(0);
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            inputMethodManager.getInputMethodList();
            inputMethodManager.toggleSoftInput(2, 0);
            ad.a(this.g, i5);
            ad.a(this.h, i4);
            ad.a(this.i, i5);
            ad.a(this.j, i5);
            ad.a(this.k, i5);
            ad.a(this.f440b, i5);
            return;
        }
        a((String) null, this.v);
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.f440b.getWindowToken(), 0);
        this.w = this.v;
        this.c = this.u;
        this.l.setVisibility(0);
        if (this.C) {
            this.m.setVisibility(0);
            this.n.setVisibility(0);
        }
        this.o.setVisibility(0);
        this.q.setVisibility(0);
        this.r.setVisibility(0);
        this.p.setVisibility(0);
        this.s.setImageResource(17301566);
        this.t.setVisibility(4);
        a(false);
    }

    private void a(String str, boolean z2) {
        int i2;
        boolean a2;
        ContentValues contentValues = new ContentValues();
        boolean z3 = this.v;
        if (TcmAid.ba == dh.f946a.intValue()) {
            a2 = z3;
            i2 = TcmAid.ba + 1000;
        } else {
            i2 = TcmAid.ba;
            a2 = q.a("SELECT COUNT(userDB.mastertung._id) FROM userDB.mastertung", i2, this.J);
            if (!a2) {
                contentValues.put("_id", Integer.toString(i2));
            }
        }
        contentValues.put("points", this.f.getText().toString());
        contentValues.put("location", this.g.getText().toString());
        contentValues.put("english_name", this.h.getText().toString());
        contentValues.put("description", this.i.getText().toString());
        contentValues.put("anatomy", this.j.getText().toString());
        contentValues.put("preparation", this.k.getText().toString());
        contentValues.put("note", this.f440b.getText().toString());
        contentValues.put("proc", "");
        contentValues.put("number", "");
        contentValues.put("importance", "");
        if (z2) {
            contentValues.put("alt_image", str);
        }
        if (!a2) {
            try {
                this.J.insert("userDB.mastertung", null, contentValues);
            } catch (SQLException e2) {
                q.a(e2);
            }
        } else {
            this.J.update("userDB.mastertung", contentValues, "_id=" + Integer.toString(i2), null);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!this.D) {
            startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 0);
            return;
        }
        a((String) null, this.u);
        this.D = this.v;
        a(false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:104:0x03f1  */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x03fc  */
    /* JADX WARNING: Removed duplicated region for block: B:116:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0314  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0329  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x03dd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r11) {
        /*
            r10 = this;
            r8 = 1
            r7 = 0
            r6 = 4
            r3 = 0
            if (r11 == 0) goto L_0x010b
            r0 = 2131362330(0x7f0a021a, float:1.8344438E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r10.s = r0
            android.widget.ImageButton r0 = r10.s
            com.cyberandsons.tcmaidtrial.detailed.df r1 = new com.cyberandsons.tcmaidtrial.detailed.df
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362331(0x7f0a021b, float:1.834444E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r10.t = r0
            android.widget.Button r0 = r10.t
            com.cyberandsons.tcmaidtrial.detailed.dg r1 = new com.cyberandsons.tcmaidtrial.detailed.dg
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131361811(0x7f0a0013, float:1.8343385E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r10.l = r0
            android.widget.ImageButton r0 = r10.l
            com.cyberandsons.tcmaidtrial.detailed.dk r1 = new com.cyberandsons.tcmaidtrial.detailed.dk
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131361844(0x7f0a0034, float:1.8343452E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r10.m = r0
            android.widget.ImageButton r0 = r10.m
            com.cyberandsons.tcmaidtrial.detailed.dl r1 = new com.cyberandsons.tcmaidtrial.detailed.dl
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            boolean r0 = r10.C
            if (r0 != 0) goto L_0x0063
            android.widget.ImageButton r0 = r10.m
            r0.setVisibility(r6)
        L_0x0063:
            r0 = 2131361809(0x7f0a0011, float:1.834338E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r10.n = r0
            android.widget.ImageButton r0 = r10.n
            com.cyberandsons.tcmaidtrial.detailed.dm r1 = new com.cyberandsons.tcmaidtrial.detailed.dm
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            boolean r0 = r10.C
            if (r0 != 0) goto L_0x0081
            android.widget.ImageButton r0 = r10.n
            r0.setVisibility(r6)
        L_0x0081:
            r0 = 2131361815(0x7f0a0017, float:1.8343393E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r10.o = r0
            android.widget.ImageButton r0 = r10.o
            com.cyberandsons.tcmaidtrial.detailed.dn r1 = new com.cyberandsons.tcmaidtrial.detailed.dn
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131361814(0x7f0a0016, float:1.834339E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r10.q = r0
            android.widget.ImageButton r0 = r10.q
            com.cyberandsons.tcmaidtrial.detailed.dh r1 = new com.cyberandsons.tcmaidtrial.detailed.dh
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131361813(0x7f0a0015, float:1.8343389E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r10.r = r0
            android.widget.ImageButton r0 = r10.r
            com.cyberandsons.tcmaidtrial.detailed.di r1 = new com.cyberandsons.tcmaidtrial.detailed.di
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131361812(0x7f0a0014, float:1.8343387E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r10.p = r0
            android.widget.ImageButton r0 = r10.p
            com.cyberandsons.tcmaidtrial.detailed.dj r1 = new com.cyberandsons.tcmaidtrial.detailed.dj
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            boolean r0 = r10.L
            if (r0 == 0) goto L_0x00e3
            android.widget.ImageButton r0 = r10.o
            r0.setVisibility(r6)
            android.widget.ImageButton r0 = r10.p
            r0.setVisibility(r6)
        L_0x00e3:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.aT
            if (r0 != 0) goto L_0x0333
            boolean r0 = r10.L
            if (r0 != 0) goto L_0x00f2
            android.widget.ImageButton r0 = r10.o
            boolean r1 = r10.v
            r10.a(r0, r1)
        L_0x00f2:
            android.widget.ImageButton r0 = r10.q
            boolean r1 = r10.v
            r10.a(r0, r1)
            android.widget.ImageButton r0 = r10.r
            boolean r1 = r10.u
            r10.a(r0, r1)
            boolean r0 = r10.L
            if (r0 != 0) goto L_0x010b
            android.widget.ImageButton r0 = r10.p
            boolean r1 = r10.u
            r10.a(r0, r1)
        L_0x010b:
            r0 = 2131362334(0x7f0a021e, float:1.8344446E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r10.e = r0
            android.widget.ImageView r0 = r10.e
            r0.setOnTouchListener(r10)
            android.graphics.Matrix r0 = r10.O
            r1 = 1065353216(0x3f800000, float:1.0)
            r2 = 1065353216(0x3f800000, float:1.0)
            r0.setTranslate(r1, r2)
            android.widget.ImageView r0 = r10.e
            android.graphics.Matrix r1 = r10.O
            r0.setImageMatrix(r1)
            android.widget.EditText r0 = r10.g
            if (r0 == 0) goto L_0x0131
            r10.g = r3
        L_0x0131:
            r0 = 2131362342(0x7f0a0226, float:1.8344462E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.g = r0
            android.widget.EditText r0 = r10.g
            com.cyberandsons.tcmaidtrial.detailed.ax r1 = new com.cyberandsons.tcmaidtrial.detailed.ax
            r1.<init>(r10)
            r0.setOnTouchListener(r1)
            android.widget.TextView r0 = r10.f
            if (r0 == 0) goto L_0x014c
            r10.f = r3
        L_0x014c:
            r0 = 2131362336(0x7f0a0220, float:1.834445E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.f = r0
            android.widget.EditText r0 = r10.h
            if (r0 == 0) goto L_0x015d
            r10.h = r3
        L_0x015d:
            r0 = 2131362344(0x7f0a0228, float:1.8344466E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.h = r0
            android.widget.EditText r0 = r10.h
            com.cyberandsons.tcmaidtrial.detailed.aw r1 = new com.cyberandsons.tcmaidtrial.detailed.aw
            r1.<init>(r10)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r10.i
            if (r0 == 0) goto L_0x0178
            r10.i = r3
        L_0x0178:
            r0 = 2131362346(0x7f0a022a, float:1.834447E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.i = r0
            android.widget.EditText r0 = r10.i
            com.cyberandsons.tcmaidtrial.detailed.az r1 = new com.cyberandsons.tcmaidtrial.detailed.az
            r1.<init>(r10)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r10.j
            if (r0 == 0) goto L_0x0193
            r10.j = r3
        L_0x0193:
            r0 = 2131362348(0x7f0a022c, float:1.8344474E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.j = r0
            android.widget.EditText r0 = r10.j
            com.cyberandsons.tcmaidtrial.detailed.ay r1 = new com.cyberandsons.tcmaidtrial.detailed.ay
            r1.<init>(r10)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r10.k
            if (r0 == 0) goto L_0x01ae
            r10.k = r3
        L_0x01ae:
            r0 = 2131362350(0x7f0a022e, float:1.8344478E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.k = r0
            android.widget.EditText r0 = r10.k
            com.cyberandsons.tcmaidtrial.detailed.bb r1 = new com.cyberandsons.tcmaidtrial.detailed.bb
            r1.<init>(r10)
            r0.setOnTouchListener(r1)
            android.widget.EditText r0 = r10.f440b
            if (r0 == 0) goto L_0x01c9
            r10.f440b = r3
        L_0x01c9:
            r0 = 2131362352(0x7f0a0230, float:1.8344482E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r10.f440b = r0
            android.widget.EditText r0 = r10.f440b
            com.cyberandsons.tcmaidtrial.detailed.ba r1 = new com.cyberandsons.tcmaidtrial.detailed.ba
            r1.<init>(r10)
            r0.setOnTouchListener(r1)
            r10.c()
            boolean r0 = r10.v
            r10.D = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT userDB.mastertung.description, userDB.mastertung.location, userDB.mastertung.note, userDB.mastertung.anatomy, userDB.mastertung.preparation, userDB.mastertung.proc, userDB.mastertung.points, userDB.mastertung.english_name, userDB.mastertung.number, userDB.mastertung.alt_image FROM userDB.mastertung WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "userDB.mastertung."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "_id"
            java.lang.StringBuilder r0 = r0.append(r1)
            r1 = 61
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.ba
            java.lang.String r1 = java.lang.Integer.toString(r1)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.G
            if (r0 == 0) goto L_0x0219
            java.lang.String r0 = "TD:loadUserModifiedData()"
            android.util.Log.d(r0, r1)
        L_0x0219:
            android.database.sqlite.SQLiteDatabase r0 = r10.J     // Catch:{ SQLException -> 0x040b, NullPointerException -> 0x0408, all -> 0x0403 }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x040b, NullPointerException -> 0x0408, all -> 0x0403 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x040b, NullPointerException -> 0x0408, all -> 0x0403 }
            int r2 = r0.getCount()     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.G     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            if (r3 == 0) goto L_0x026c
            java.lang.String r3 = "TD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r4.<init>()     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            java.lang.String r5 = "query count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            java.lang.String r5 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            java.lang.String r3 = "TD:loadUserModifiedData()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r4.<init>()     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            java.lang.String r5 = "column count = '"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            int r5 = r0.getColumnCount()     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            java.lang.String r5 = "' "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
        L_0x026c:
            boolean r3 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            if (r3 == 0) goto L_0x030b
            if (r2 != r8) goto L_0x030b
            android.widget.EditText r2 = r10.i     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3 = 0
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            android.widget.EditText r2 = r10.g     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            android.widget.EditText r2 = r10.f440b     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3 = 2
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            android.widget.EditText r2 = r10.j     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3 = 3
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            android.widget.EditText r2 = r10.k     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3 = 4
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            android.widget.EditText r2 = r10.h     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3 = 7
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.ba     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r2 <= r3) goto L_0x02eb
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r2.<init>()     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3 = 8
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            java.lang.String r3 = " - "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3 = 6
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.misc.ad.a(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            android.widget.TextView r3 = r10.d     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3.setText(r2)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            android.widget.TextView r2 = r10.f     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3 = 6
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r2.setText(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
        L_0x02eb:
            boolean r2 = r10.C     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            if (r2 == 0) goto L_0x03e2
            r2 = 9
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            if (r2 == 0) goto L_0x038f
            int r3 = r2.length()     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            if (r3 <= 0) goto L_0x038f
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeFile(r2)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            if (r2 != 0) goto L_0x0364
            android.widget.ImageView r2 = r10.e     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3 = 2130837543(0x7f020027, float:1.7280043E38)
            r2.setImageResource(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
        L_0x030b:
            if (r0 == 0) goto L_0x0310
            r0.close()
        L_0x0310:
            boolean r0 = r10.C
            if (r0 == 0) goto L_0x03fc
            boolean r0 = r10.D
            if (r0 == 0) goto L_0x03f5
            android.widget.ImageButton r0 = r10.n
            r1 = 17301560(0x1080038, float:2.4979412E-38)
            r0.setImageResource(r1)
            android.widget.ImageButton r0 = r10.n
            r0.setVisibility(r7)
        L_0x0325:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.cR
            if (r0 != r8) goto L_0x0332
            com.cyberandsons.tcmaidtrial.a.n r0 = r10.K
            java.lang.String r0 = r0.L()
            r10.a(r0)
        L_0x0332:
            return
        L_0x0333:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.aT
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.aU
            int r1 = r1.size()
            int r1 = r1 - r8
            if (r0 != r1) goto L_0x010b
            boolean r0 = r10.L
            if (r0 != 0) goto L_0x0349
            android.widget.ImageButton r0 = r10.o
            boolean r1 = r10.u
            r10.a(r0, r1)
        L_0x0349:
            android.widget.ImageButton r0 = r10.q
            boolean r1 = r10.u
            r10.a(r0, r1)
            android.widget.ImageButton r0 = r10.r
            boolean r1 = r10.v
            r10.a(r0, r1)
            boolean r0 = r10.L
            if (r0 != 0) goto L_0x010b
            android.widget.ImageButton r0 = r10.p
            boolean r1 = r10.v
            r10.a(r0, r1)
            goto L_0x010b
        L_0x0364:
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            int r3 = r3 - r7
            int r4 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            int r4 = r4 - r7
            android.view.Window r5 = r10.getWindow()     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            android.graphics.drawable.BitmapDrawable r2 = com.cyberandsons.tcmaidtrial.misc.dh.a(r2, r3, r4, r5)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            android.widget.ImageView r3 = r10.e     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3.setImageDrawable(r2)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            android.widget.ImageView r2 = r10.e     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3 = 0
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            boolean r2 = r10.u     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r10.D = r2     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            goto L_0x030b
        L_0x0382:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x0386:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0406 }
            if (r1 == 0) goto L_0x0310
            r1.close()
            goto L_0x0310
        L_0x038f:
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.ba     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3 = 1000(0x3e8, float:1.401E-42)
            if (r2 <= r3) goto L_0x030b
            android.widget.ImageView r2 = r10.e     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3 = 8
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            goto L_0x030b
        L_0x039e:
            r2 = move-exception
        L_0x039f:
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x03eb }
            java.lang.String r3 = "myVariable"
            r2.a(r3, r1)     // Catch:{ all -> 0x03eb }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x03eb }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x03eb }
            java.lang.String r3 = "TD:loadUserModifiedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x03eb }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x03eb }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x03eb }
            r1.<init>(r10)     // Catch:{ all -> 0x03eb }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x03eb }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x03eb }
            r2 = 2131099674(0x7f06001a, float:1.7811708E38)
            java.lang.String r2 = r10.getString(r2)     // Catch:{ all -> 0x03eb }
            r1.setMessage(r2)     // Catch:{ all -> 0x03eb }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.bc r3 = new com.cyberandsons.tcmaidtrial.detailed.bc     // Catch:{ all -> 0x03eb }
            r3.<init>(r10)     // Catch:{ all -> 0x03eb }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x03eb }
            r1.show()     // Catch:{ all -> 0x03eb }
            if (r0 == 0) goto L_0x0310
            r0.close()
            goto L_0x0310
        L_0x03e2:
            android.widget.ImageView r2 = r10.e     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            r3 = 8
            r2.setVisibility(r3)     // Catch:{ SQLException -> 0x0382, NullPointerException -> 0x039e }
            goto L_0x030b
        L_0x03eb:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x03ef:
            if (r1 == 0) goto L_0x03f4
            r1.close()
        L_0x03f4:
            throw r0
        L_0x03f5:
            android.widget.ImageButton r0 = r10.n
            r0.setVisibility(r6)
            goto L_0x0325
        L_0x03fc:
            android.widget.ImageButton r0 = r10.n
            r0.setVisibility(r6)
            goto L_0x0325
        L_0x0403:
            r0 = move-exception
            r1 = r3
            goto L_0x03ef
        L_0x0406:
            r0 = move-exception
            goto L_0x03ef
        L_0x0408:
            r0 = move-exception
            r0 = r3
            goto L_0x039f
        L_0x040b:
            r0 = move-exception
            r1 = r3
            goto L_0x0386
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.detailed.TungDetail.a(boolean):void");
    }

    private void a(String str) {
        if (str != null && str.length() != 0) {
            String[] split = str.split(",");
            for (String parseInt : split) {
                switch (Integer.parseInt(parseInt)) {
                    case 2:
                        dh.a(this.h.getText());
                        break;
                    case 3:
                        dh.a(this.g.getText());
                        break;
                    case 4:
                        dh.a(this.i.getText());
                        break;
                    case 5:
                        dh.a(this.j.getText());
                        break;
                    case 6:
                        dh.a(this.k.getText());
                        break;
                    case 7:
                        dh.a(this.f440b.getText());
                        break;
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:55:0x02be  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x02d2  */
    /* JADX WARNING: Removed duplicated region for block: B:74:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c() {
        /*
            r12 = this;
            r7 = 1
            r4 = 0
            r10 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT main.mastertung._id, main.mastertung.group_number, main.mastertung.point_number, main.mastertung.name, main.mastertung.indications, main.mastertung.location, main.mastertung.note, main.mastertung.anatomy, main.mastertung.preparation, main.mastertung.proc, main.mastertung.english_name, main.mastertung.image, main.mastertung.label, main.mastertung.row1, main.mastertung.col1, main.mastertung.row2, main.mastertung.col2, main.mastertung.row3, main.mastertung.col3, main.mastertung.row4, main.mastertung.col4, main.mastertung.row5, main.mastertung.col5, main.mastertung.red, main.mastertung.green, main.mastertung.blue, main.mastertung.depth, main.mastertung.blocksz, main.mastertung.blockdir, main.mastertung.scaleRow1, main.mastertung.scaleCol1, main.mastertung.scaleRow2, main.mastertung.scaleCol2, main.mastertung.scaleSize, main.mastertung.scaleMarkers, main.mastertung.lblRow, main.mastertung.lblCol, main.tonemarks.ttonemarks, main.tonemarks.tscc, main.tonemarks.ttcc FROM main.mastertung JOIN main.tonemarks ON main.tonemarks.tid = main.mastertung._id WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.aW
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r2 = r0.toString()
            com.cyberandsons.tcmaidtrial.a.n r0 = r12.K
            int r3 = r0.ac()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.G     // Catch:{ SQLException -> 0x02de, NullPointerException -> 0x02db, all -> 0x02d6 }
            if (r0 == 0) goto L_0x0027
            java.lang.String r0 = "TD:loadSystemSuppliedData()"
            android.util.Log.d(r0, r2)     // Catch:{ SQLException -> 0x02de, NullPointerException -> 0x02db, all -> 0x02d6 }
        L_0x0027:
            android.database.sqlite.SQLiteDatabase r0 = r12.J     // Catch:{ SQLException -> 0x02de, NullPointerException -> 0x02db, all -> 0x02d6 }
            r1 = 0
            android.database.Cursor r0 = r0.rawQuery(r2, r1)     // Catch:{ SQLException -> 0x02de, NullPointerException -> 0x02db, all -> 0x02d6 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x02de, NullPointerException -> 0x02db, all -> 0x02d6 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            boolean r4 = com.cyberandsons.tcmaidtrial.misc.dh.G     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            if (r4 == 0) goto L_0x007a
            java.lang.String r4 = "TD:loadSystemSuppliedData()"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r5.<init>()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r6 = "query count = "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r6 = java.lang.Integer.toString(r1)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r5 = r5.toString()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            android.util.Log.d(r4, r5)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r4 = "TD:loadSystemSuppliedData()"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r5.<init>()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r6 = "column count = '"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            int r6 = r0.getColumnCount()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r6 = "' "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r5 = r5.toString()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            android.util.Log.d(r4, r5)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
        L_0x007a:
            boolean r4 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            if (r4 == 0) goto L_0x021c
            if (r1 != r7) goto L_0x021c
            boolean r4 = r12.F     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            if (r4 == 0) goto L_0x0222
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r1.<init>()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r4 = "SELECT main.mastertung.point_number, main.mastertung.image, main.mastertung.label, main.mastertung.row1, main.mastertung.col1, main.mastertung.row2, main.mastertung.col2, main.mastertung.row3, main.mastertung.col3, main.mastertung.row4, main.mastertung.col4, main.mastertung.row5, main.mastertung.col5, main.mastertung.lblRow, main.mastertung.lblCol, main.mastertung.red, main.mastertung.green, main.mastertung.blue, main.mastertung.depth, main.mastertung.scaleRow1, main.mastertung.scaleCol1, main.mastertung.scaleRow2, main.mastertung.scaleCol2, main.mastertung.scaleSize, main.mastertung.scaleMarkers, main.mastertung._id FROM main.mastertung WHERE "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r4 = " image='"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r4 = 11
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r4 = "' "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            boolean r4 = r12.B     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            if (r4 != 0) goto L_0x00d1
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r4.<init>()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r4 = " AND group_number like '%"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r4 = 1
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r4 = "%'"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
        L_0x00d1:
            boolean r4 = com.cyberandsons.tcmaidtrial.misc.dh.G     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            if (r4 == 0) goto L_0x00da
            java.lang.String r4 = "TD:loadSystemSuppliedData()"
            android.util.Log.d(r4, r1)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
        L_0x00da:
            android.database.sqlite.SQLiteDatabase r4 = r12.J     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r5 = 0
            android.database.Cursor r1 = r4.rawQuery(r1, r5)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            android.database.sqlite.SQLiteCursor r1 = (android.database.sqlite.SQLiteCursor) r1     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            int r4 = r1.getCount()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            boolean r5 = com.cyberandsons.tcmaidtrial.misc.dh.G     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            if (r5 == 0) goto L_0x012d
            java.lang.String r5 = "TD:loadSystemSuppliedData_2()"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r6.<init>()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r7 = "query count = "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r7 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r6 = r6.toString()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            android.util.Log.d(r5, r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r5 = "TD:loadSystemSuppliedData_2()"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r6.<init>()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r7 = "column count = '"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            int r7 = r1.getColumnCount()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r7 = "' "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r6 = r6.toString()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            android.util.Log.d(r5, r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
        L_0x012d:
            boolean r5 = r12.u     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            com.cyberandsons.tcmaidtrial.a.b[] r4 = r12.a(r1, r4, r5)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r1.close()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r1 = r4
        L_0x0137:
            r4 = 0
            int r4 = r0.getInt(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r5.<init>()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r6 = 2
            java.lang.String r6 = r0.getString(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r6 = " - "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r6 = 37
            java.lang.String r6 = r0.getString(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r6 = com.cyberandsons.tcmaidtrial.misc.ad.a(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r6 = "  "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            int r6 = r3 + 38
            java.lang.String r6 = r0.getString(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r5 = r5.toString()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            android.widget.TextView r6 = r12.d     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r6.setText(r5)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r5 = 11
            java.lang.String r5 = r0.getString(r5)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r6 = 11
            java.lang.String r6 = r0.getString(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r12.E = r6     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            boolean r6 = r12.C     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            if (r6 == 0) goto L_0x02c3
            java.lang.String r6 = "auricular"
            boolean r6 = r5.equalsIgnoreCase(r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            if (r6 == 0) goto L_0x022a
            android.content.res.Resources r6 = r12.getResources()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r7 = 2130837506(0x7f020002, float:1.7279968E38)
            android.graphics.Bitmap r6 = android.graphics.BitmapFactory.decodeResource(r6, r7)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r7 = 280(0x118, float:3.92E-43)
            r8 = 280(0x118, float:3.92E-43)
            android.graphics.drawable.BitmapDrawable r6 = com.cyberandsons.tcmaidtrial.misc.dh.a(r6, r7, r8)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            android.graphics.Bitmap r6 = r6.getBitmap()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
        L_0x01a8:
            if (r6 != 0) goto L_0x0232
            android.widget.ImageView r1 = r12.e     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r4 = 2130837543(0x7f020027, float:1.7280043E38)
            r1.setImageResource(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
        L_0x01b2:
            android.widget.EditText r1 = r12.g     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r4 = 5
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r1.setText(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r1.<init>()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r4 = 37
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.misc.ad.a(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r4 = "\n"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            int r3 = r3 + 38
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            android.widget.TextView r3 = r12.f     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r3.setText(r1)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            android.widget.EditText r1 = r12.h     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r3 = 10
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r1.setText(r3)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            android.widget.EditText r1 = r12.i     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r3 = 4
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r1.setText(r3)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            android.widget.EditText r1 = r12.j     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r3 = 7
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r1.setText(r3)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            android.widget.EditText r1 = r12.k     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r3 = 8
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r1.setText(r3)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            android.widget.EditText r1 = r12.f440b     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r3 = 6
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r1.setText(r3)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
        L_0x021c:
            if (r0 == 0) goto L_0x0221
            r0.close()
        L_0x0221:
            return
        L_0x0222:
            boolean r4 = r12.v     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            com.cyberandsons.tcmaidtrial.a.b[] r1 = r12.a(r0, r1, r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            goto L_0x0137
        L_0x022a:
            java.lang.String r6 = "points"
            android.graphics.Bitmap r6 = com.cyberandsons.tcmaidtrial.misc.dh.b(r5, r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            goto L_0x01a8
        L_0x0232:
            com.cyberandsons.tcmaidtrial.draw.a r7 = new com.cyberandsons.tcmaidtrial.draw.a     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            int r8 = r6.getHeight()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            int r9 = r6.getWidth()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r7.<init>(r12, r6, r8, r9)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            boolean r6 = r12.F     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            if (r6 == 0) goto L_0x0274
            boolean r6 = r12.H     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r7.a(r4, r5, r1, r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
        L_0x0248:
            android.graphics.Bitmap r1 = r7.a()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            int r4 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            int r4 = r4 - r10
            int r5 = com.cyberandsons.tcmaidtrial.TcmAid.cS     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            int r5 = r5 - r10
            android.view.Window r6 = r12.getWindow()     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            android.graphics.drawable.BitmapDrawable r1 = com.cyberandsons.tcmaidtrial.misc.dh.a(r1, r4, r5, r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            android.widget.ImageView r4 = r12.e     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r4.setImageDrawable(r1)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            android.widget.ImageView r1 = r12.e     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r4 = 0
            r1.setVisibility(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            goto L_0x01b2
        L_0x0267:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
        L_0x026b:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x02d9 }
            if (r1 == 0) goto L_0x0221
            r1.close()
            goto L_0x0221
        L_0x0274:
            r4 = 0
            r1 = r1[r4]     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            boolean r4 = r12.G     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            boolean r6 = r12.H     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r7.a(r5, r1, r4, r6)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            goto L_0x0248
        L_0x027f:
            r1 = move-exception
        L_0x0280:
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x02cc }
            java.lang.String r3 = "myVariable"
            r1.a(r3, r2)     // Catch:{ all -> 0x02cc }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x02cc }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x02cc }
            java.lang.String r3 = "TD:loadSystemSuppliedData()"
            r2.<init>(r3)     // Catch:{ all -> 0x02cc }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x02cc }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x02cc }
            r1.<init>(r12)     // Catch:{ all -> 0x02cc }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x02cc }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x02cc }
            r2 = 2131099673(0x7f060019, float:1.7811706E38)
            java.lang.String r2 = r12.getString(r2)     // Catch:{ all -> 0x02cc }
            r1.setMessage(r2)     // Catch:{ all -> 0x02cc }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.detailed.bd r3 = new com.cyberandsons.tcmaidtrial.detailed.bd     // Catch:{ all -> 0x02cc }
            r3.<init>(r12)     // Catch:{ all -> 0x02cc }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x02cc }
            r1.show()     // Catch:{ all -> 0x02cc }
            if (r0 == 0) goto L_0x0221
            r0.close()
            goto L_0x0221
        L_0x02c3:
            android.widget.ImageView r1 = r12.e     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            r4 = 8
            r1.setVisibility(r4)     // Catch:{ SQLException -> 0x0267, NullPointerException -> 0x027f }
            goto L_0x01b2
        L_0x02cc:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
        L_0x02d0:
            if (r1 == 0) goto L_0x02d5
            r1.close()
        L_0x02d5:
            throw r0
        L_0x02d6:
            r0 = move-exception
            r1 = r4
            goto L_0x02d0
        L_0x02d9:
            r0 = move-exception
            goto L_0x02d0
        L_0x02db:
            r0 = move-exception
            r0 = r4
            goto L_0x0280
        L_0x02de:
            r0 = move-exception
            r1 = r4
            goto L_0x026b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.detailed.TungDetail.c():void");
    }

    private b[] a(SQLiteCursor sQLiteCursor, int i2, boolean z2) {
        int i3;
        int i4;
        int i5;
        int i6;
        boolean t2 = this.K.t();
        b[] bVarArr = new b[i2];
        sQLiteCursor.moveToFirst();
        if (dh.G) {
            Log.d("PD:buildPointCorrdinates()", "query count = " + Integer.toString(i2));
            Log.d("PD:buildPointCorrdinates()", "column count = '" + Integer.toString(sQLiteCursor.getColumnCount()) + "' ");
        }
        for (int i7 = 0; i7 < i2; i7++) {
            bVarArr[i7] = new b();
            if (i2 != 1 || z2) {
                bVarArr[i7].f163a = sQLiteCursor.getInt(25);
                bVarArr[i7].f164b = null;
                bVarArr[i7].c = sQLiteCursor.getString(0);
                bVarArr[i7].d = sQLiteCursor.getString(1);
                bVarArr[i7].e = sQLiteCursor.getString(2);
                bVarArr[i7].f = sQLiteCursor.getInt(3);
                bVarArr[i7].g = sQLiteCursor.getInt(4);
                bVarArr[i7].h = sQLiteCursor.getInt(5);
                bVarArr[i7].i = sQLiteCursor.getInt(6);
                bVarArr[i7].j = sQLiteCursor.getInt(7);
                bVarArr[i7].k = sQLiteCursor.getInt(8);
                bVarArr[i7].l = sQLiteCursor.getInt(9);
                bVarArr[i7].m = sQLiteCursor.getInt(10);
                bVarArr[i7].n = sQLiteCursor.getInt(11);
                bVarArr[i7].o = sQLiteCursor.getInt(12);
                bVarArr[i7].A = sQLiteCursor.getFloat(15) == 1.0f ? 255 : 0;
                b bVar = bVarArr[i7];
                if (sQLiteCursor.getFloat(16) == 1.0f) {
                    i3 = 128;
                } else {
                    i3 = 0;
                }
                bVar.B = i3;
                b bVar2 = bVarArr[i7];
                if (sQLiteCursor.getFloat(17) == 1.0f) {
                    i4 = 255;
                } else {
                    i4 = 0;
                }
                bVar2.C = i4;
                bVarArr[i7].x = sQLiteCursor.getString(18);
                bVarArr[i7].s = sQLiteCursor.getInt(19);
                bVarArr[i7].r = sQLiteCursor.getInt(20);
                bVarArr[i7].u = sQLiteCursor.getInt(21);
                bVarArr[i7].t = sQLiteCursor.getInt(22);
                bVarArr[i7].v = sQLiteCursor.getInt(23);
                bVarArr[i7].w = sQLiteCursor.getInt(24);
                bVarArr[i7].p = sQLiteCursor.getInt(13);
                bVarArr[i7].q = sQLiteCursor.getInt(14);
                bVarArr[i7].y = -1;
                bVarArr[i7].z = null;
            } else {
                bVarArr[i7].f163a = sQLiteCursor.getInt(0);
                bVarArr[i7].f164b = null;
                bVarArr[i7].c = sQLiteCursor.getString(2);
                bVarArr[i7].d = sQLiteCursor.getString(11);
                bVarArr[i7].e = sQLiteCursor.getString(12);
                bVarArr[i7].f = sQLiteCursor.getInt(13);
                bVarArr[i7].g = sQLiteCursor.getInt(14);
                bVarArr[i7].h = sQLiteCursor.getInt(15);
                bVarArr[i7].i = sQLiteCursor.getInt(16);
                bVarArr[i7].j = sQLiteCursor.getInt(17);
                bVarArr[i7].k = sQLiteCursor.getInt(18);
                bVarArr[i7].l = sQLiteCursor.getInt(19);
                bVarArr[i7].m = sQLiteCursor.getInt(20);
                bVarArr[i7].n = sQLiteCursor.getInt(21);
                bVarArr[i7].o = sQLiteCursor.getInt(22);
                bVarArr[i7].A = sQLiteCursor.getFloat(23) == 1.0f ? 255 : 0;
                b bVar3 = bVarArr[i7];
                if (sQLiteCursor.getFloat(24) == 1.0f) {
                    i5 = 128;
                } else {
                    i5 = 0;
                }
                bVar3.B = i5;
                b bVar4 = bVarArr[i7];
                if (sQLiteCursor.getFloat(25) == 1.0f) {
                    i6 = 255;
                } else {
                    i6 = 0;
                }
                bVar4.C = i6;
                bVarArr[i7].x = sQLiteCursor.getString(26);
                bVarArr[i7].y = sQLiteCursor.getInt(27);
                bVarArr[i7].z = sQLiteCursor.getString(28);
                bVarArr[i7].s = sQLiteCursor.getInt(29);
                bVarArr[i7].r = sQLiteCursor.getInt(30);
                bVarArr[i7].u = sQLiteCursor.getInt(31);
                bVarArr[i7].t = sQLiteCursor.getInt(32);
                bVarArr[i7].v = sQLiteCursor.getInt(33);
                bVarArr[i7].w = sQLiteCursor.getInt(34);
                bVarArr[i7].p = sQLiteCursor.getInt(35);
                bVarArr[i7].q = sQLiteCursor.getInt(36);
            }
            sQLiteCursor.moveToNext();
            if (!t2) {
                bVarArr[i7].A = 0;
                bVarArr[i7].B = 0;
                bVarArr[i7].C = 0;
            }
        }
        sQLiteCursor.moveToFirst();
        return bVarArr;
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        boolean z2 = this.v;
        switch (i2) {
            case 0:
                if (this.A) {
                    this.y -= 2;
                } else {
                    TcmAid.aT = 0;
                    if (!this.L) {
                        a(this.o, this.v);
                    }
                    a(this.q, this.v);
                    a(this.r, this.u);
                    if (!this.L) {
                        a(this.p, this.u);
                    }
                }
                z2 = this.u;
                break;
            case 1:
                if (this.A) {
                    this.z -= 2;
                } else if (TcmAid.aT - 1 >= 0) {
                    if (TcmAid.aT - 1 == 0) {
                        if (!this.L) {
                            a(this.o, this.v);
                        }
                        a(this.q, this.v);
                    }
                    if (this.r.isEnabled() == this.v) {
                        a(this.r, this.u);
                        if (!this.L) {
                            a(this.p, this.u);
                        }
                    }
                    TcmAid.aT--;
                }
                z2 = this.u;
                break;
            case 2:
                if (!this.A) {
                    int size = TcmAid.aU.size();
                    if (TcmAid.aT + 1 < size) {
                        if (TcmAid.aT + 1 == size - 1) {
                            a(this.r, this.v);
                            if (!this.L) {
                                a(this.p, this.v);
                            }
                        }
                        if (this.q.isEnabled() == this.v) {
                            if (!this.L) {
                                a(this.o, this.u);
                            }
                            a(this.q, this.u);
                        }
                        TcmAid.aT++;
                        z2 = this.u;
                        break;
                    }
                } else {
                    this.z += 2;
                    z2 = this.u;
                    break;
                }
                break;
            case 3:
                if (this.A) {
                    this.y += 2;
                } else {
                    TcmAid.aT = TcmAid.aU.size() - 1;
                    if (!this.L) {
                        a(this.o, this.u);
                    }
                    a(this.q, this.u);
                    a(this.r, this.v);
                    if (!this.L) {
                        a(this.p, this.v);
                    }
                }
                z2 = this.u;
                break;
        }
        if (z2) {
            if (TcmAid.aT >= TcmAid.aU.size()) {
                TcmAid.aT = TcmAid.aU.size() - 1;
            }
            int intValue = ((Integer) TcmAid.aU.get(TcmAid.aT)).intValue();
            TcmAid.aW = "main.mastertung._id=" + Integer.toString(intValue);
            TcmAid.ba = intValue;
            a(false);
            this.f439a.post(new bf(this));
        }
    }

    private void a(ImageButton imageButton, boolean z2) {
        if (z2) {
            imageButton.setEnabled(this.u);
            imageButton.setVisibility(0);
            return;
        }
        imageButton.setEnabled(this.v);
        imageButton.setVisibility(4);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        e a2 = e.a(motionEvent);
        ImageView imageView = (ImageView) view;
        StringBuilder sb = new StringBuilder();
        int b2 = a2.b();
        int i2 = b2 & 255;
        sb.append("event ACTION_").append(new String[]{"DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE", "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?"}[i2]);
        if (i2 == 5 || i2 == 6) {
            sb.append("(pid ").append(b2 >> 8);
            sb.append(')');
        }
        sb.append('[');
        for (int i3 = 0; i3 < a2.a(); i3++) {
            sb.append('#').append(i3);
            sb.append("(pid ").append(a2.c(i3));
            sb.append(")=").append((int) a2.a(i3));
            sb.append(',').append((int) a2.b(i3));
            if (i3 + 1 < a2.a()) {
                sb.append(';');
            }
        }
        sb.append(']');
        Log.d("TD:dumpEvent()", sb.toString());
        switch (a2.b() & 255) {
            case 0:
                this.P.set(this.O);
                this.R.set(a2.c(), a2.d());
                Log.d("TD:onTouch()", "mode=DRAG");
                this.Q = 1;
                break;
            case 1:
            case 6:
                this.Q = 0;
                Log.d("TD:onTouch()", "mode=NONE");
                break;
            case 2:
                if (this.Q != 1) {
                    if (this.Q == 2) {
                        float a3 = a(a2);
                        Log.d("TD:onTouch()", "newDist=" + a3);
                        if (a3 > 10.0f) {
                            this.O.set(this.P);
                            float f2 = a3 / this.T;
                            this.O.postScale(f2, f2, this.S.x, this.S.y);
                            break;
                        }
                    }
                } else {
                    this.O.set(this.P);
                    this.O.postTranslate(a2.c() - this.R.x, a2.d() - this.R.y);
                    break;
                }
                break;
            case 5:
                this.T = a(a2);
                Log.d("TD:onTouch()", "oldDist=" + this.T);
                if (this.T > 10.0f) {
                    this.P.set(this.O);
                    this.S.set((a2.a(0) + a2.a(1)) / 2.0f, (a2.b(1) + a2.b(0)) / 2.0f);
                    this.Q = 2;
                    Log.d("TD:onTouch()", "mode=ZOOM");
                    break;
                }
                break;
        }
        imageView.setImageMatrix(this.O);
        return true;
    }

    private static float a(e eVar) {
        float a2 = eVar.a(0) - eVar.a(1);
        float b2 = eVar.b(0) - eVar.b(1);
        return FloatMath.sqrt((a2 * a2) + (b2 * b2));
    }

    public void onClick(View view) {
    }
}
