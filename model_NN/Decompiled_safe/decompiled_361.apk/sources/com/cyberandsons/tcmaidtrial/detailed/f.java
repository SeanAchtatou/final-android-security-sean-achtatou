package com.cyberandsons.tcmaidtrial.detailed;

import android.view.View;

final class f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SixStagesDetail f578a;

    f(SixStagesDetail sixStagesDetail) {
        this.f578a = sixStagesDetail;
    }

    public final void onClick(View view) {
        SixStagesDetail sixStagesDetail = this.f578a;
        sixStagesDetail.setResult(2);
        sixStagesDetail.finish();
    }
}
