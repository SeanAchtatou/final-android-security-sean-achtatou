package com.cyberandsons.tcmaidtrial.detailed;

import android.view.MotionEvent;
import android.view.View;

final class fj implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HerbsDetail f590a;

    fj(HerbsDetail herbsDetail) {
        this.f590a = herbsDetail;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f590a.af.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f590a.c;
    }
}
