package com.cyberandsons.tcmaidtrial.detailed;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.Toast;

final class cm extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SixStagesDetail f510a;

    cm(SixStagesDetail sixStagesDetail) {
        this.f510a = sixStagesDetail;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        try {
            if (Math.abs(motionEvent.getY() - motionEvent2.getY()) > 250.0f) {
                return false;
            }
            if (motionEvent.getX() - motionEvent2.getX() <= 120.0f || Math.abs(f) <= 200.0f) {
                if (motionEvent2.getX() - motionEvent.getX() > 120.0f && Math.abs(f) > 200.0f) {
                    this.f510a.c();
                }
                return false;
            }
            Toast.makeText(this.f510a, "Please Swipe to the Right or Double-Tap", 0).show();
            return false;
        } catch (Exception e) {
        }
    }

    public final boolean onDoubleTap(MotionEvent motionEvent) {
        this.f510a.c();
        return true;
    }
}
