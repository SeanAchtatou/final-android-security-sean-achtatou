package com.cyberandsons.tcmaidtrial.detailed;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class du implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AuricularDetail f545a;

    du(AuricularDetail auricularDetail) {
        this.f545a = auricularDetail;
    }

    public final void onClick(View view) {
        AuricularDetail auricularDetail = this.f545a;
        ((InputMethodManager) auricularDetail.getSystemService("input_method")).hideSoftInputFromWindow(auricularDetail.f424b.getWindowToken(), 0);
    }
}
