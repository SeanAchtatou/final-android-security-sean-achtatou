package com.cyberandsons.tcmaidtrial.detailed;

import android.view.MotionEvent;
import android.view.View;

final class hh implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiagnosisDetail f642a;

    hh(DiagnosisDetail diagnosisDetail) {
        this.f642a = diagnosisDetail;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f642a.E.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f642a.c;
    }
}
