package com.cyberandsons.tcmaidtrial.detailed;

import android.view.MotionEvent;
import android.view.View;

final class hb implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiagnosisDetail f636a;

    hb(DiagnosisDetail diagnosisDetail) {
        this.f636a = diagnosisDetail;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f636a.I.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f636a.c;
    }
}
