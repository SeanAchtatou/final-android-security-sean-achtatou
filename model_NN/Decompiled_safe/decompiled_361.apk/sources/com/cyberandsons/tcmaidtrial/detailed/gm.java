package com.cyberandsons.tcmaidtrial.detailed;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class gm implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ WristAnkleDetail f620a;

    gm(WristAnkleDetail wristAnkleDetail) {
        this.f620a = wristAnkleDetail;
    }

    public final void onClick(View view) {
        WristAnkleDetail wristAnkleDetail = this.f620a;
        ((InputMethodManager) wristAnkleDetail.getSystemService("input_method")).hideSoftInputFromWindow(wristAnkleDetail.f442b.getWindowToken(), 0);
    }
}
