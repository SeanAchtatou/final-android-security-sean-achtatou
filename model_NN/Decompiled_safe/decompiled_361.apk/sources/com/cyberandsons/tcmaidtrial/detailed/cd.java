package com.cyberandsons.tcmaidtrial.detailed;

import android.view.MotionEvent;
import android.view.View;

final class cd implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TungDetail f501a;

    cd(TungDetail tungDetail) {
        this.f501a = tungDetail;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f501a.M.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f501a.c;
    }
}
