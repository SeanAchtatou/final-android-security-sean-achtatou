package com.cyberandsons.tcmaidtrial.detailed;

import android.view.MotionEvent;
import android.view.View;

final class cw implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AuricularDetail f520a;

    cw(AuricularDetail auricularDetail) {
        this.f520a = auricularDetail;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f520a.J.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f520a.c;
    }
}
