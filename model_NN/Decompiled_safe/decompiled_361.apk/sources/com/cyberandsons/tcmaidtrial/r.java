package com.cyberandsons.tcmaidtrial;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import com.cyberandsons.tcmaidtrial.areas.bk;
import com.cyberandsons.tcmaidtrial.misc.a;
import com.cyberandsons.tcmaidtrial.misc.b;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.io.IOException;

final class r implements bk {

    /* renamed from: a  reason: collision with root package name */
    private SQLiteDatabase f1089a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ TcmAid f1090b;

    /* synthetic */ r(TcmAid tcmAid) {
        this(tcmAid, (byte) 0);
    }

    private r(TcmAid tcmAid, byte b2) {
        this.f1090b = tcmAid;
    }

    public final void a(String str, String str2) {
        boolean z;
        if (dh.A) {
            Log.d("TA:NonAndroidMarketDialog", "DeviceID is " + str);
            Log.d("TA:NonAndroidMarketDialog", "RegCod is " + str2);
            try {
                String a2 = a.a(str, b.a(this.f1090b.getString(C0000R.string.masterpassword), this.f1090b.getString(C0000R.string.rpnstring)));
                Log.i("RCE", a2);
                if (str2.compareTo(a2) != 0) {
                    z = this.f1090b.f161b;
                    Toast makeText = Toast.makeText(this.f1090b, (int) C0000R.string.invalid_registration, 1);
                    makeText.setGravity(17, makeText.getXOffset() / 2, makeText.getYOffset() / 2);
                    makeText.show();
                } else {
                    z = this.f1090b.f160a;
                    Toast makeText2 = Toast.makeText(this.f1090b, (int) C0000R.string.valid_registration, 1);
                    makeText2.setGravity(17, makeText2.getXOffset() / 2, makeText2.getYOffset() / 2);
                    makeText2.show();
                }
                this.f1089a = this.f1090b.a(this.f1089a, this.f1090b.f161b);
                this.f1090b.ds.y(z, this.f1089a);
                this.f1089a.close();
            } catch (IOException e) {
                Log.i("RCE-IOE", e.getLocalizedMessage());
            } catch (Exception e2) {
                Log.i("RCE-E", e2.getLocalizedMessage());
            }
        }
        this.f1090b.startActivity(this.f1090b.getIntent());
        this.f1090b.finish();
    }
}
