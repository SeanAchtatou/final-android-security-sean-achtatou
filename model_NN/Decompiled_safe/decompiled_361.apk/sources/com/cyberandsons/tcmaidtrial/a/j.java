package com.cyberandsons.tcmaidtrial.a;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public final class j {
    public static void a(String str, SQLiteDatabase sQLiteDatabase) {
        int a2 = q.a("SELECT MAX(userDB.searches._id) FROM userDB.searches ", sQLiteDatabase) + 1;
        String replaceAll = str.replaceAll("' ", "''");
        if (c(replaceAll, sQLiteDatabase)) {
            try {
                sQLiteDatabase.execSQL("INSERT INTO userDB.searches (_id, searchString) VALUES (" + Integer.toString(a2) + ", '" + replaceAll + "')");
            } catch (SQLException e) {
                q.a(e);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x003e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean c(java.lang.String r4, android.database.sqlite.SQLiteDatabase r5) {
        /*
            r3 = 0
            r0 = 1
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "SELECT _id FROM userDB.searches WHERE searchString='"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r2 = "' "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r2 = 0
            android.database.Cursor r4 = r5.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x002f, all -> 0x003a }
            android.database.sqlite.SQLiteCursor r4 = (android.database.sqlite.SQLiteCursor) r4     // Catch:{ SQLException -> 0x002f, all -> 0x003a }
            int r1 = r4.getCount()     // Catch:{ SQLException -> 0x0048, all -> 0x0042 }
            if (r1 <= 0) goto L_0x0029
            r0 = 0
        L_0x0029:
            if (r4 == 0) goto L_0x002e
            r4.close()
        L_0x002e:
            return r0
        L_0x002f:
            r1 = move-exception
            r2 = r3
        L_0x0031:
            com.cyberandsons.tcmaidtrial.a.q.a(r1)     // Catch:{ all -> 0x0045 }
            if (r2 == 0) goto L_0x002e
            r2.close()
            goto L_0x002e
        L_0x003a:
            r0 = move-exception
            r1 = r3
        L_0x003c:
            if (r1 == 0) goto L_0x0041
            r1.close()
        L_0x0041:
            throw r0
        L_0x0042:
            r0 = move-exception
            r1 = r4
            goto L_0x003c
        L_0x0045:
            r0 = move-exception
            r1 = r2
            goto L_0x003c
        L_0x0048:
            r1 = move-exception
            r2 = r4
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.j.c(java.lang.String, android.database.sqlite.SQLiteDatabase):boolean");
    }

    public static void b(String str, SQLiteDatabase sQLiteDatabase) {
        String str2 = "DELETE FROM userDB.searches ";
        if (str != null && str.length() > 0) {
            str2 = str2 + " WHERE searchString='" + str + "' ";
        }
        try {
            sQLiteDatabase.execSQL(str2);
        } catch (SQLException e) {
            q.a(e);
        }
    }
}
