package com.cyberandsons.tcmaidtrial.a;

public final class r {

    /* renamed from: a  reason: collision with root package name */
    private int f181a;

    /* renamed from: b  reason: collision with root package name */
    private String f182b;
    private String c;
    private String d;
    private String e;

    public r() {
    }

    public r(int i, String str, String str2, String str3, String str4) {
        this.f181a = i;
        this.f182b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
    }

    public final int a() {
        return this.f181a;
    }

    public final String b() {
        return this.f182b;
    }

    public final String c() {
        return this.c;
    }

    public final String d() {
        return this.d;
    }

    public final String e() {
        return this.e;
    }
}
