package com.cyberandsons.tcmaidtrial.a;

import android.database.sqlite.SQLiteDatabase;

public final class m {
    public static String a(String str, String str2, int i) {
        String str3;
        String str4 = "INSERT INTO " + str + " (_id, ";
        switch (i) {
            case 2:
                str3 = str4 + "english";
                break;
            case 3:
                str3 = str4 + "botanical";
                break;
            default:
                str3 = str4 + "pinyin";
                break;
        }
        return str3 + ", mmcategory, temp) " + str2;
    }

    public static String a(String str) {
        return "DELETE FROM " + str;
    }

    public static String b(String str) {
        return "SELECT _id, " + str + ", " + "mmcategory, " + "CASE WHEN temp LIKE '%cold%' THEN 0 " + "WHEN temp LIKE '%cool%' THEN 1 " + "WHEN temp LIKE '%neutral%' THEN 2 " + "WHEN temp LIKE '%warm%' THEN 3 " + "WHEN temp LIKE '%hot%' THEN 4 END AS temp " + "FROM main.materiamedica ";
    }

    public static String c(String str) {
        return "SELECT _id, " + str + ", " + "mmcategory, " + "CASE WHEN temp LIKE '%cold%' THEN 0 " + "WHEN temp LIKE '%cool%' THEN 1 " + "WHEN temp LIKE '%neutral%' THEN 2 " + "WHEN temp LIKE '%warm%' THEN 3 " + "WHEN temp LIKE '%hot%' THEN 4 END AS temp " + "FROM userDB.materiamedica ";
    }

    public static String d(String str) {
        return "SELECT _id, " + str + ", " + "mmcategory, " + "CASE WHEN temp LIKE '%cold%' THEN 0 " + "WHEN temp LIKE '%cool%' THEN 1 " + "WHEN temp LIKE '%neutral%' THEN 2 " + "WHEN temp LIKE '%warm%' THEN 3 " + "WHEN temp LIKE '%hot%' THEN 4 END AS temp " + "FROM main.materiamedica ";
    }

    public static String e(String str) {
        return "SELECT _id, " + str + ", " + "mmcategory, " + "CASE WHEN temp LIKE '%cold%' THEN 0 " + "WHEN temp LIKE '%cool%' THEN 1 " + "WHEN temp LIKE '%neutral%' THEN 2 " + "WHEN temp LIKE '%warm%' THEN 3 " + "WHEN temp LIKE '%hot%' THEN 4 END AS temp " + "FROM userDB.materiamedica ";
    }

    public static String a(String str, SQLiteDatabase sQLiteDatabase, int i) {
        String str2;
        String obj = l.a(i, sQLiteDatabase).toString();
        boolean z = false;
        if (obj.length() > 0) {
            str2 = q.a("materiamedica", "_id", obj);
            if (str2.length() > 0) {
                z = true;
            }
        } else {
            str2 = null;
        }
        if (z) {
            return b(str) + " WHERE (main" + str2 + ") " + " UNION " + c(str) + " WHERE (userDB" + str2 + ") " + " AND userDB.materiamedica._id>1000 ORDER BY 2";
        }
        return b(str) + " UNION " + c(str) + " WHERE userDB.materiamedica._id>1000 ORDER BY 2";
    }

    public static String f(String str) {
        return String.format("SELECT DISTINCT main.mmcategory.item_name FROM main.materiamedica LEFT JOIN main.mmcategory ON main.mmcategory._id = main.materiamedica.mmcategory  %s UNION SELECT userDB.herbcategory.name FROM userDB.herbcategory ORDER BY 1 ", str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0050  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(java.lang.String r4, android.database.sqlite.SQLiteDatabase r5) {
        /*
            r3 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT CASE WHEN temp LIKE '%cold%' THEN 0 WHEN temp LIKE '%cool%' THEN 1 WHEN temp LIKE '%neutral%' THEN 2 WHEN temp LIKE '%warm%' THEN 3 WHEN temp LIKE '%hot%' THEN 4 END AS temp FROM main.materiamedica WHERE pinyin like '"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r1 = "'"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = 2
            r2 = 0
            android.database.Cursor r4 = r5.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x0033, all -> 0x003f }
            android.database.sqlite.SQLiteCursor r4 = (android.database.sqlite.SQLiteCursor) r4     // Catch:{ SQLException -> 0x0033, all -> 0x003f }
            boolean r0 = r4.moveToFirst()     // Catch:{ SQLException -> 0x004d, all -> 0x0047 }
            if (r0 == 0) goto L_0x0052
            r0 = 0
            int r0 = r4.getInt(r0)     // Catch:{ SQLException -> 0x004d, all -> 0x0047 }
        L_0x002d:
            if (r4 == 0) goto L_0x0032
            r4.close()
        L_0x0032:
            return r0
        L_0x0033:
            r0 = move-exception
            r2 = r3
        L_0x0035:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x004a }
            if (r2 == 0) goto L_0x0050
            r2.close()
            r0 = r1
            goto L_0x0032
        L_0x003f:
            r0 = move-exception
            r1 = r3
        L_0x0041:
            if (r1 == 0) goto L_0x0046
            r1.close()
        L_0x0046:
            throw r0
        L_0x0047:
            r0 = move-exception
            r1 = r4
            goto L_0x0041
        L_0x004a:
            r0 = move-exception
            r1 = r2
            goto L_0x0041
        L_0x004d:
            r0 = move-exception
            r2 = r4
            goto L_0x0035
        L_0x0050:
            r0 = r1
            goto L_0x0032
        L_0x0052:
            r0 = r1
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.m.a(java.lang.String, android.database.sqlite.SQLiteDatabase):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x004e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(int r4, android.database.sqlite.SQLiteDatabase r5) {
        /*
            r3 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT CASE WHEN temp LIKE '%cold%' THEN 0 WHEN temp LIKE '%cool%' THEN 1 WHEN temp LIKE '%neutral%' THEN 2 WHEN temp LIKE '%warm%' THEN 3 WHEN temp LIKE '%hot%' THEN 4 END AS temp FROM main.materiamedica WHERE _id="
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = java.lang.Integer.toString(r4)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = 2
            r2 = 0
            android.database.Cursor r4 = r5.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x0031, all -> 0x003d }
            android.database.sqlite.SQLiteCursor r4 = (android.database.sqlite.SQLiteCursor) r4     // Catch:{ SQLException -> 0x0031, all -> 0x003d }
            boolean r0 = r4.moveToFirst()     // Catch:{ SQLException -> 0x004b, all -> 0x0045 }
            if (r0 == 0) goto L_0x0050
            r0 = 0
            int r0 = r4.getInt(r0)     // Catch:{ SQLException -> 0x004b, all -> 0x0045 }
        L_0x002b:
            if (r4 == 0) goto L_0x0030
            r4.close()
        L_0x0030:
            return r0
        L_0x0031:
            r0 = move-exception
            r2 = r3
        L_0x0033:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0048 }
            if (r2 == 0) goto L_0x004e
            r2.close()
            r0 = r1
            goto L_0x0030
        L_0x003d:
            r0 = move-exception
            r1 = r3
        L_0x003f:
            if (r1 == 0) goto L_0x0044
            r1.close()
        L_0x0044:
            throw r0
        L_0x0045:
            r0 = move-exception
            r1 = r4
            goto L_0x003f
        L_0x0048:
            r0 = move-exception
            r1 = r2
            goto L_0x003f
        L_0x004b:
            r0 = move-exception
            r2 = r4
            goto L_0x0033
        L_0x004e:
            r0 = r1
            goto L_0x0030
        L_0x0050:
            r0 = r1
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.m.a(int, android.database.sqlite.SQLiteDatabase):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x009f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.CharSequence b(java.lang.String r5, android.database.sqlite.SQLiteDatabase r6) {
        /*
            r4 = 0
            r3 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT main.formulas.pinyin FROM main.formulas WHERE main.formulas.useiPhone=1 AND main.formulas.ingredients LIKE '%: "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r1 = " %' "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "UNION "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "SELECT userDB.formulas.pinyin "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "FROM userDB.formulas "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "WHERE userDB.formulas.ingredients "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "LIKE '%: "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r1 = " %' "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "ORDER BY 1"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            r2 = 0
            android.database.Cursor r5 = r6.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x0090, all -> 0x009b }
            android.database.sqlite.SQLiteCursor r5 = (android.database.sqlite.SQLiteCursor) r5     // Catch:{ SQLException -> 0x0090, all -> 0x009b }
            boolean r0 = r5.moveToFirst()     // Catch:{ SQLException -> 0x00a9, all -> 0x00a3 }
            if (r0 == 0) goto L_0x0086
            r0 = r3
        L_0x005c:
            r2 = 0
            java.lang.String r2 = r5.getString(r2)     // Catch:{ SQLException -> 0x00a9, all -> 0x00a3 }
            if (r0 <= 0) goto L_0x0068
            java.lang.String r3 = "\n"
            r1.append(r3)     // Catch:{ SQLException -> 0x00a9, all -> 0x00a3 }
        L_0x0068:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x00a9, all -> 0x00a3 }
            r3.<init>()     // Catch:{ SQLException -> 0x00a9, all -> 0x00a3 }
            java.lang.String r4 = "- "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x00a9, all -> 0x00a3 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ SQLException -> 0x00a9, all -> 0x00a3 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x00a9, all -> 0x00a3 }
            r1.append(r2)     // Catch:{ SQLException -> 0x00a9, all -> 0x00a3 }
            int r0 = r0 + 1
            boolean r2 = r5.moveToNext()     // Catch:{ SQLException -> 0x00a9, all -> 0x00a3 }
            if (r2 != 0) goto L_0x005c
        L_0x0086:
            if (r5 == 0) goto L_0x008b
            r5.close()
        L_0x008b:
            java.lang.String r0 = r1.toString()
            return r0
        L_0x0090:
            r0 = move-exception
            r2 = r4
        L_0x0092:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x00a6 }
            if (r2 == 0) goto L_0x008b
            r2.close()
            goto L_0x008b
        L_0x009b:
            r0 = move-exception
            r1 = r4
        L_0x009d:
            if (r1 == 0) goto L_0x00a2
            r1.close()
        L_0x00a2:
            throw r0
        L_0x00a3:
            r0 = move-exception
            r1 = r5
            goto L_0x009d
        L_0x00a6:
            r0 = move-exception
            r1 = r2
            goto L_0x009d
        L_0x00a9:
            r0 = move-exception
            r2 = r5
            goto L_0x0092
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.m.b(java.lang.String, android.database.sqlite.SQLiteDatabase):java.lang.CharSequence");
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x00d8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.CharSequence a(java.lang.String r7, int r8, android.database.sqlite.SQLiteDatabase r9) {
        /*
            r4 = 0
            r3 = 0
            if (r8 != 0) goto L_0x0087
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT main.herb_interaction.drug, main.herb_interaction.action FROM main.herb_interaction WHERE main.herb_interaction.herb='"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r7.toLowerCase()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "' AND main.herb_interaction.type='Herb-To-Herb Interaction' "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "ORDER BY main.herb_interaction.drug"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
        L_0x0027:
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            r2 = 0
            android.database.Cursor r0 = r9.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x00e4, all -> 0x00d4 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x00e4, all -> 0x00d4 }
            boolean r2 = r0.moveToFirst()     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            if (r2 == 0) goto L_0x007d
            r2 = r3
        L_0x003a:
            r3 = 0
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            r4 = 1
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            if (r2 <= 0) goto L_0x004b
            java.lang.String r5 = "\n\n"
            r1.append(r5)     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
        L_0x004b:
            if (r8 != 0) goto L_0x00ac
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            r5.<init>()     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            r5 = 32
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            r4 = 32
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.misc.ad.a(r7)     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            r1.append(r3)     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
        L_0x0075:
            int r2 = r2 + 1
            boolean r3 = r0.moveToNext()     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            if (r3 != 0) goto L_0x003a
        L_0x007d:
            if (r0 == 0) goto L_0x0082
            r0.close()
        L_0x0082:
            java.lang.String r0 = r1.toString()
            return r0
        L_0x0087:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT main.herb_interaction.drug, main.herb_interaction.interaction FROM main.herb_interaction WHERE main.herb_interaction.herb='"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r7.toLowerCase()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "' AND main.herb_interaction.type='Herb-To-Drug Interaction' "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "ORDER BY main.herb_interaction.drug"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            goto L_0x0027
        L_0x00ac:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            r5.<init>()     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            java.lang.String r5 = ": "
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            r1.append(r3)     // Catch:{ SQLException -> 0x00c7, all -> 0x00dc }
            goto L_0x0075
        L_0x00c7:
            r2 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
        L_0x00cb:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x00e1 }
            if (r2 == 0) goto L_0x0082
            r2.close()
            goto L_0x0082
        L_0x00d4:
            r0 = move-exception
            r1 = r4
        L_0x00d6:
            if (r1 == 0) goto L_0x00db
            r1.close()
        L_0x00db:
            throw r0
        L_0x00dc:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00d6
        L_0x00e1:
            r0 = move-exception
            r1 = r2
            goto L_0x00d6
        L_0x00e4:
            r0 = move-exception
            r2 = r4
            goto L_0x00cb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.m.a(java.lang.String, int, android.database.sqlite.SQLiteDatabase):java.lang.CharSequence");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0064  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.CharSequence c(java.lang.String r3, android.database.sqlite.SQLiteDatabase r4) {
        /*
            r2 = 0
            int r0 = java.lang.Integer.parseInt(r3)
            r1 = 1000(0x3e8, float:1.401E-42)
            if (r0 <= r1) goto L_0x0034
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT userDB.herbcategory.name FROM userDB.herbcategory WHERE userDB.herbcategory._id="
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
        L_0x001c:
            r1 = 0
            android.database.Cursor r3 = r4.rawQuery(r0, r1)     // Catch:{ SQLException -> 0x0048, all -> 0x0054 }
            android.database.sqlite.SQLiteCursor r3 = (android.database.sqlite.SQLiteCursor) r3     // Catch:{ SQLException -> 0x0048, all -> 0x0054 }
            boolean r0 = r3.moveToFirst()     // Catch:{ SQLException -> 0x0061, all -> 0x005c }
            if (r0 == 0) goto L_0x0066
            r0 = 0
            java.lang.String r0 = r3.getString(r0)     // Catch:{ SQLException -> 0x0061, all -> 0x005c }
        L_0x002e:
            if (r3 == 0) goto L_0x0033
            r3.close()
        L_0x0033:
            return r0
        L_0x0034:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT main.mmcategory.item_name FROM main.mmcategory WHERE main.mmcategory._id="
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            goto L_0x001c
        L_0x0048:
            r0 = move-exception
            r1 = r2
        L_0x004a:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x005f }
            if (r1 == 0) goto L_0x0064
            r1.close()
            r0 = r2
            goto L_0x0033
        L_0x0054:
            r0 = move-exception
            r1 = r2
        L_0x0056:
            if (r1 == 0) goto L_0x005b
            r1.close()
        L_0x005b:
            throw r0
        L_0x005c:
            r0 = move-exception
            r1 = r3
            goto L_0x0056
        L_0x005f:
            r0 = move-exception
            goto L_0x0056
        L_0x0061:
            r0 = move-exception
            r1 = r3
            goto L_0x004a
        L_0x0064:
            r0 = r2
            goto L_0x0033
        L_0x0066:
            r0 = r2
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.m.c(java.lang.String, android.database.sqlite.SQLiteDatabase):java.lang.CharSequence");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0050  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int d(java.lang.String r4, android.database.sqlite.SQLiteDatabase r5) {
        /*
            r3 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT main.mmcategory._id FROM main.mmcategory WHERE main.mmcategory.item_name='"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r1 = "' "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = -1
            r2 = 0
            android.database.Cursor r4 = r5.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x0033, all -> 0x003f }
            android.database.sqlite.SQLiteCursor r4 = (android.database.sqlite.SQLiteCursor) r4     // Catch:{ SQLException -> 0x0033, all -> 0x003f }
            boolean r0 = r4.moveToFirst()     // Catch:{ SQLException -> 0x004d, all -> 0x0047 }
            if (r0 == 0) goto L_0x0052
            r0 = 0
            int r0 = r4.getInt(r0)     // Catch:{ SQLException -> 0x004d, all -> 0x0047 }
        L_0x002d:
            if (r4 == 0) goto L_0x0032
            r4.close()
        L_0x0032:
            return r0
        L_0x0033:
            r0 = move-exception
            r2 = r3
        L_0x0035:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x004a }
            if (r2 == 0) goto L_0x0050
            r2.close()
            r0 = r1
            goto L_0x0032
        L_0x003f:
            r0 = move-exception
            r1 = r3
        L_0x0041:
            if (r1 == 0) goto L_0x0046
            r1.close()
        L_0x0046:
            throw r0
        L_0x0047:
            r0 = move-exception
            r1 = r4
            goto L_0x0041
        L_0x004a:
            r0 = move-exception
            r1 = r2
            goto L_0x0041
        L_0x004d:
            r0 = move-exception
            r2 = r4
            goto L_0x0035
        L_0x0050:
            r0 = r1
            goto L_0x0032
        L_0x0052:
            r0 = r1
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.m.d(java.lang.String, android.database.sqlite.SQLiteDatabase):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int b(java.lang.String r6, int r7, android.database.sqlite.SQLiteDatabase r8) {
        /*
            r5 = 0
            r4 = 39
            switch(r7) {
                case 2: goto L_0x0090;
                case 3: goto L_0x0094;
                default: goto L_0x0006;
            }
        L_0x0006:
            java.lang.String r0 = "pinyin"
        L_0x0008:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = " WHERE main.materiamedica."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            java.lang.String r2 = "='"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = " WHERE userDB.materiamedica."
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = "='"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = b(r0)
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r3 = " UNION "
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r0 = c(r0)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r1 = " AND userDB.materiamedica._id>1000 ORDER BY 2"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = -1
            r2 = 0
            android.database.Cursor r6 = r8.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x0098, all -> 0x00a4 }
            android.database.sqlite.SQLiteCursor r6 = (android.database.sqlite.SQLiteCursor) r6     // Catch:{ SQLException -> 0x0098, all -> 0x00a4 }
            boolean r0 = r6.moveToFirst()     // Catch:{ SQLException -> 0x00b2, all -> 0x00ac }
            if (r0 == 0) goto L_0x00b7
            r0 = 0
            int r0 = r6.getInt(r0)     // Catch:{ SQLException -> 0x00b2, all -> 0x00ac }
        L_0x008a:
            if (r6 == 0) goto L_0x008f
            r6.close()
        L_0x008f:
            return r0
        L_0x0090:
            java.lang.String r0 = "english"
            goto L_0x0008
        L_0x0094:
            java.lang.String r0 = "botanical"
            goto L_0x0008
        L_0x0098:
            r0 = move-exception
            r2 = r5
        L_0x009a:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x00af }
            if (r2 == 0) goto L_0x00b5
            r2.close()
            r0 = r1
            goto L_0x008f
        L_0x00a4:
            r0 = move-exception
            r1 = r5
        L_0x00a6:
            if (r1 == 0) goto L_0x00ab
            r1.close()
        L_0x00ab:
            throw r0
        L_0x00ac:
            r0 = move-exception
            r1 = r6
            goto L_0x00a6
        L_0x00af:
            r0 = move-exception
            r1 = r2
            goto L_0x00a6
        L_0x00b2:
            r0 = move-exception
            r2 = r6
            goto L_0x009a
        L_0x00b5:
            r0 = r1
            goto L_0x008f
        L_0x00b7:
            r0 = r1
            goto L_0x008a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.m.b(java.lang.String, int, android.database.sqlite.SQLiteDatabase):int");
    }

    public static String g(String str) {
        StringBuffer stringBuffer = new StringBuffer("(");
        String[] split = str.split(",");
        boolean z = true;
        for (int i = 0; i < split.length; i++) {
            if (z) {
                stringBuffer.append(String.format("%d", Integer.valueOf(Integer.parseInt(split[i]))));
                z = false;
            } else {
                stringBuffer.append(String.format(",%d", Integer.valueOf(Integer.parseInt(split[i]))));
            }
        }
        stringBuffer.append(')');
        return stringBuffer.toString();
    }
}
