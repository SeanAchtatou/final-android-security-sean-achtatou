package com.cyberandsons.tcmaidtrial.a;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    private int f173a;

    /* renamed from: b  reason: collision with root package name */
    private String f174b;
    private String c;
    private String d;
    private String e;
    private String f;
    private boolean g;
    private String h;

    public i() {
    }

    public i(int i, String str, String str2, String str3, String str4, String str5, String str6) {
        this.f173a = i;
        this.f174b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.h = str6;
        if (str6 != null && str6.length() > 0) {
            this.g = true;
        }
    }

    public final int a() {
        return this.f173a;
    }

    public final String b() {
        return this.f174b;
    }

    public final String c() {
        return this.c;
    }

    public final String d() {
        return this.d;
    }

    public final String e() {
        return this.e;
    }

    public final String f() {
        return this.f;
    }

    public final String g() {
        return this.h;
    }

    public final boolean h() {
        return this.g;
    }
}
