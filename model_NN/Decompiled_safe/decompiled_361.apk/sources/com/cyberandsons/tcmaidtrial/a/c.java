package com.cyberandsons.tcmaidtrial.a;

import android.database.sqlite.SQLiteDatabase;

public final class c {
    public static String a(String str, String str2, int i) {
        String str3;
        String str4 = "INSERT INTO " + str + " (_id, ";
        switch (i) {
            case 2:
                str3 = str4 + "english";
                break;
            default:
                str3 = str4 + "pinyin";
                break;
        }
        return str3 + ", fcategory) " + str2;
    }

    public static String a(String str) {
        return "DELETE FROM " + str;
    }

    public static String b(String str) {
        return "SELECT main.formulas._id, main.formulas." + str + ", " + "main.formulas.fcategory " + "FROM main.formulas ";
    }

    public static String c(String str) {
        return "SELECT userDB.formulas._id, userDB.formulas." + str + ", " + "userDB.formulas.fcategory " + "FROM userDB.formulas ";
    }

    public static String a(String str, n nVar, String str2) {
        String str3;
        boolean z;
        boolean z2 = nVar.z();
        String p = nVar.p();
        if (!z2 || p.length() <= 0) {
            str3 = null;
            z = false;
        } else {
            str3 = q.a("formulas", "_id", p);
            z = str3.length() > 0;
        }
        if (z) {
            return b(str) + " WHERE (main" + str3 + ") AND main.formulas.useiPhone=1 " + " UNION " + c(str) + " WHERE (userDB" + str3 + ") " + " AND userDB.formulas._id>1000 ORDER BY 2";
        }
        return b(str) + " WHERE main.formulas.useiPhone=1 " + str2 + " UNION " + c(str) + " WHERE userDB.formulas._id>1000 ORDER BY 2 ";
    }

    public static String a(String str, SQLiteDatabase sQLiteDatabase, int i) {
        String str2;
        String obj = d.a(i, sQLiteDatabase).toString();
        boolean z = false;
        if (obj.length() > 0) {
            str2 = q.a("formulas", "_id", obj);
            if (str2.length() > 0) {
                z = true;
            }
        } else {
            str2 = null;
        }
        if (z) {
            return b(str) + " WHERE (main" + str2 + ") " + " UNION " + c(str) + " WHERE (userDB" + str2 + ") " + " AND userDB.formulas._id>1000 ORDER BY 2";
        }
        return b(str) + " UNION " + c(str) + " WHERE userDB.formulas._id>1000 ORDER BY 2";
    }

    public static String a(String str, String str2, String str3) {
        return b(str3) + " WHERE main.formulas.useiPhone=1 " + str + " UNION " + c(str3) + " WHERE userDB.formulas._id>1000 " + str2 + " ORDER BY 2 ";
    }

    public static String a(String str, String str2) {
        return b(str2) + str + " UNION " + c(str2) + str + " ORDER BY 2 ";
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0064  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.CharSequence a(java.lang.String r3, android.database.sqlite.SQLiteDatabase r4) {
        /*
            r2 = 0
            int r0 = java.lang.Integer.parseInt(r3)
            r1 = 1000(0x3e8, float:1.401E-42)
            if (r0 <= r1) goto L_0x0034
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT userDB.formulascategory.name FROM userDB.formulascategory WHERE userDB.formulascategory._id="
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
        L_0x001c:
            r1 = 0
            android.database.Cursor r3 = r4.rawQuery(r0, r1)     // Catch:{ SQLException -> 0x0048, all -> 0x0054 }
            android.database.sqlite.SQLiteCursor r3 = (android.database.sqlite.SQLiteCursor) r3     // Catch:{ SQLException -> 0x0048, all -> 0x0054 }
            boolean r0 = r3.moveToFirst()     // Catch:{ SQLException -> 0x0061, all -> 0x005c }
            if (r0 == 0) goto L_0x0066
            r0 = 0
            java.lang.String r0 = r3.getString(r0)     // Catch:{ SQLException -> 0x0061, all -> 0x005c }
        L_0x002e:
            if (r3 == 0) goto L_0x0033
            r3.close()
        L_0x0033:
            return r0
        L_0x0034:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT main.fcategory.item_name FROM main.fcategory WHERE main.fcategory._id="
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            goto L_0x001c
        L_0x0048:
            r0 = move-exception
            r1 = r2
        L_0x004a:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x005f }
            if (r1 == 0) goto L_0x0064
            r1.close()
            r0 = r2
            goto L_0x0033
        L_0x0054:
            r0 = move-exception
            r1 = r2
        L_0x0056:
            if (r1 == 0) goto L_0x005b
            r1.close()
        L_0x005b:
            throw r0
        L_0x005c:
            r0 = move-exception
            r1 = r3
            goto L_0x0056
        L_0x005f:
            r0 = move-exception
            goto L_0x0056
        L_0x0061:
            r0 = move-exception
            r1 = r3
            goto L_0x004a
        L_0x0064:
            r0 = r2
            goto L_0x0033
        L_0x0066:
            r0 = r2
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.c.a(java.lang.String, android.database.sqlite.SQLiteDatabase):java.lang.CharSequence");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0050  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int b(java.lang.String r4, android.database.sqlite.SQLiteDatabase r5) {
        /*
            r3 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT main.fcategory._id FROM main.fcategory WHERE main.fcategory.item_name='"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r1 = "' "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = -1
            r2 = 0
            android.database.Cursor r4 = r5.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x0033, all -> 0x003f }
            android.database.sqlite.SQLiteCursor r4 = (android.database.sqlite.SQLiteCursor) r4     // Catch:{ SQLException -> 0x0033, all -> 0x003f }
            boolean r0 = r4.moveToFirst()     // Catch:{ SQLException -> 0x004d, all -> 0x0047 }
            if (r0 == 0) goto L_0x0052
            r0 = 0
            int r0 = r4.getInt(r0)     // Catch:{ SQLException -> 0x004d, all -> 0x0047 }
        L_0x002d:
            if (r4 == 0) goto L_0x0032
            r4.close()
        L_0x0032:
            return r0
        L_0x0033:
            r0 = move-exception
            r2 = r3
        L_0x0035:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x004a }
            if (r2 == 0) goto L_0x0050
            r2.close()
            r0 = r1
            goto L_0x0032
        L_0x003f:
            r0 = move-exception
            r1 = r3
        L_0x0041:
            if (r1 == 0) goto L_0x0046
            r1.close()
        L_0x0046:
            throw r0
        L_0x0047:
            r0 = move-exception
            r1 = r4
            goto L_0x0041
        L_0x004a:
            r0 = move-exception
            r1 = r2
            goto L_0x0041
        L_0x004d:
            r0 = move-exception
            r2 = r4
            goto L_0x0035
        L_0x0050:
            r0 = r1
            goto L_0x0032
        L_0x0052:
            r0 = r1
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.c.b(java.lang.String, android.database.sqlite.SQLiteDatabase):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0087  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(java.lang.String r6, int r7, android.database.sqlite.SQLiteDatabase r8) {
        /*
            r5 = 0
            r4 = 39
            switch(r7) {
                case 2: goto L_0x0067;
                default: goto L_0x0006;
            }
        L_0x0006:
            java.lang.String r0 = "pinyin"
        L_0x0008:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = " AND main.formulas."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            java.lang.String r2 = "='"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = " AND userDB.formulas."
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = "='"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            java.lang.String r0 = a(r1, r2, r0)
            r1 = -1
            r2 = 0
            android.database.Cursor r6 = r8.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x006a, all -> 0x0076 }
            android.database.sqlite.SQLiteCursor r6 = (android.database.sqlite.SQLiteCursor) r6     // Catch:{ SQLException -> 0x006a, all -> 0x0076 }
            boolean r0 = r6.moveToFirst()     // Catch:{ SQLException -> 0x0084, all -> 0x007e }
            if (r0 == 0) goto L_0x0089
            r0 = 0
            int r0 = r6.getInt(r0)     // Catch:{ SQLException -> 0x0084, all -> 0x007e }
        L_0x0061:
            if (r6 == 0) goto L_0x0066
            r6.close()
        L_0x0066:
            return r0
        L_0x0067:
            java.lang.String r0 = "english"
            goto L_0x0008
        L_0x006a:
            r0 = move-exception
            r2 = r5
        L_0x006c:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0081 }
            if (r2 == 0) goto L_0x0087
            r2.close()
            r0 = r1
            goto L_0x0066
        L_0x0076:
            r0 = move-exception
            r1 = r5
        L_0x0078:
            if (r1 == 0) goto L_0x007d
            r1.close()
        L_0x007d:
            throw r0
        L_0x007e:
            r0 = move-exception
            r1 = r6
            goto L_0x0078
        L_0x0081:
            r0 = move-exception
            r1 = r2
            goto L_0x0078
        L_0x0084:
            r0 = move-exception
            r2 = r6
            goto L_0x006c
        L_0x0087:
            r0 = r1
            goto L_0x0066
        L_0x0089:
            r0 = r1
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.c.a(java.lang.String, int, android.database.sqlite.SQLiteDatabase):int");
    }

    public static String d(String str) {
        StringBuffer stringBuffer = new StringBuffer("(");
        String[] split = str.split(",");
        boolean z = true;
        for (int i = 0; i < split.length; i++) {
            if (z) {
                stringBuffer.append(String.format("%d", Integer.valueOf(Integer.parseInt(split[i]))));
                z = false;
            } else {
                stringBuffer.append(String.format(",%d", Integer.valueOf(Integer.parseInt(split[i]))));
            }
        }
        stringBuffer.append(')');
        return stringBuffer.toString();
    }
}
