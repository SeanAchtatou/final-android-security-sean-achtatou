package com.cyberandsons.tcmaidtrial.a;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private int f169a;

    /* renamed from: b  reason: collision with root package name */
    private String f170b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;

    public g() {
    }

    public g(int i2, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9) {
        this.f169a = i2;
        this.f170b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
        this.h = str7;
        this.i = str8;
        this.j = str9;
    }

    public final int a() {
        return this.f169a;
    }

    public final String b() {
        return this.f170b;
    }

    public final String c() {
        return this.c;
    }

    public final String d() {
        return this.d;
    }

    public final String e() {
        return this.g;
    }

    public final String f() {
        return this.h;
    }

    public final String g() {
        return this.i;
    }

    public final String h() {
        return this.j;
    }
}
