package com.cyberandsons.tcmaidtrial.a;

public final class s {

    /* renamed from: a  reason: collision with root package name */
    private boolean f183a = true;

    /* renamed from: b  reason: collision with root package name */
    private boolean f184b;

    public s() {
        this.f184b = !this.f183a;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0061  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r4, java.lang.String r5, int r6, android.database.sqlite.SQLiteDatabase r7) {
        /*
            r3 = 0
            java.lang.String r0 = ""
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "SELECT "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r2 = " FROM "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "main.tonemarks"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " WHERE "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r5)
            r2 = 61
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = java.lang.Integer.toString(r6)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r2 = 0
            android.database.Cursor r4 = r7.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x0052, all -> 0x005d }
            android.database.sqlite.SQLiteCursor r4 = (android.database.sqlite.SQLiteCursor) r4     // Catch:{ SQLException -> 0x0052, all -> 0x005d }
            boolean r1 = r4.moveToFirst()     // Catch:{ SQLException -> 0x006b, all -> 0x0065 }
            if (r1 == 0) goto L_0x004c
            r1 = 0
            java.lang.String r0 = r4.getString(r1)     // Catch:{ SQLException -> 0x006b, all -> 0x0065 }
        L_0x004c:
            if (r4 == 0) goto L_0x0051
            r4.close()
        L_0x0051:
            return r0
        L_0x0052:
            r1 = move-exception
            r2 = r3
        L_0x0054:
            com.cyberandsons.tcmaidtrial.a.q.a(r1)     // Catch:{ all -> 0x0068 }
            if (r2 == 0) goto L_0x0051
            r2.close()
            goto L_0x0051
        L_0x005d:
            r0 = move-exception
            r1 = r3
        L_0x005f:
            if (r1 == 0) goto L_0x0064
            r1.close()
        L_0x0064:
            throw r0
        L_0x0065:
            r0 = move-exception
            r1 = r4
            goto L_0x005f
        L_0x0068:
            r0 = move-exception
            r1 = r2
            goto L_0x005f
        L_0x006b:
            r1 = move-exception
            r2 = r4
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.s.a(java.lang.String, java.lang.String, int, android.database.sqlite.SQLiteDatabase):java.lang.String");
    }
}
