package com.cyberandsons.tcmaidtrial.a;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.cyberandsons.tcmaidtrial.C0000R;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public final class q extends SQLiteOpenHelper {

    /* renamed from: b  reason: collision with root package name */
    private static String f179b;
    private static String c;
    private static String d;
    private static boolean e = false;

    /* renamed from: a  reason: collision with root package name */
    private final Context f180a;

    public q(Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 1);
        c = str;
        f179b = context.getString(C0000R.string.tcmDatabasePath);
        d = context.getString(C0000R.string.tcmUserDatabase);
        this.f180a = context;
    }

    public static void a(SQLiteDatabase sQLiteDatabase) {
        a("SELECT MAX(showEnglishInPointList) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN showEnglishInPointList INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
        a("SELECT MAX(showEnglishInTungList) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN showEnglishInTungList INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
        a("SELECT MAX(showPointChineseCharInList) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN showPointChineseCharInList INTEGER NOT NULL DEFAULT 1", sQLiteDatabase);
        a("SELECT MAX(showTungChineseCharInList) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN showTungChineseCharInList INTEGER NOT NULL DEFAULT 1", sQLiteDatabase);
        a("SELECT MAX(showFormulaChineseCharInList) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN showFormulaChineseCharInList INTEGER NOT NULL DEFAULT 1", sQLiteDatabase);
        a("SELECT MAX(showChangeInNetworkStatus) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN showChangeInNetworkStatus INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
        a("SELECT MAX(initialTimeStamp) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN initialTimeStamp TIMESTAMP NOT NULL DEFAULT '1900-01-01 12:00:00'", sQLiteDatabase);
        a("SELECT MAX(showPointMeridians) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN showPointMeridians INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
        a("SELECT MAX(showAdjacentPointMeridians) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN showAdjacentPointMeridians INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
        a("SELECT MAX(alwaysShowAnswers) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN alwaysShowAnswers INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
        a("SELECT MAX(usePulseDiagImages) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN usePulseDiagImages INTEGER NOT NULL DEFAULT 1", sQLiteDatabase);
        a("SELECT MAX(pulseDiagSearch) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN pulseDiagSearch text NOT NULL DEFAULT '0,2'", sQLiteDatabase);
        a("SELECT MAX(searchPulseDiag) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN searchPulseDiag INTEGER NOT NULL  DEFAULT 1", sQLiteDatabase);
        a("SELECT COUNT(*) FROM userDB.pulsediag", "CREATE TABLE userDB.pulsediag (_id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR, pinyin VARCHAR, alternate_names VARCHAR, category VARCHAR, description TEXT, sensation TEXT, indications TEXT, left_cun TEXT, left_guan TEXT, left_chi TEXT, right_cun TEXT, right_guan TEXT, right_chi TEXT, bilateral_cun TEXT, bilateral_guan TEXT, bilateral_chi TEXT, combinations TEXT, notes TEXT);", sQLiteDatabase);
        a("SELECT MAX(alt_image) FROM userDB.auricular", "ALTER TABLE userDB.auricular ADD COLUMN alt_image VARCHAR", sQLiteDatabase);
        a("SELECT MAX(alt_image) FROM userDB.materiamedica", "ALTER TABLE userDB.materiamedica ADD COLUMN alt_image VARCHAR", sQLiteDatabase);
        a("SELECT MAX(alt_image) FROM userDB.mastertung", "ALTER TABLE userDB.mastertung ADD COLUMN alt_image VARCHAR", sQLiteDatabase);
        a("SELECT MAX(alt_image) FROM userDB.pointslocation", "ALTER TABLE userDB.pointslocation ADD COLUMN alt_image VARCHAR", sQLiteDatabase);
        a("SELECT MAX(alt_image) FROM userDB.pulsediag", "ALTER TABLE userDB.pulsediag ADD COLUMN alt_image VARCHAR", sQLiteDatabase);
        a("SELECT MAX(alt_image) FROM userDB.wristankle", "ALTER TABLE userDB.wristankle ADD COLUMN alt_image VARCHAR", sQLiteDatabase);
        a("SELECT MAX(useNonAndroidMarket) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN useNonAndroidMarket INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
        a("SELECT COUNT(*) FROM userDB.formulascategory", "CREATE TABLE userDB.formulascategory (_id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR, desc VARCHAR, formulas VARCHAR)", sQLiteDatabase);
        a("SELECT COUNT(*) FROM userDB.herbcategory", "CREATE TABLE userDB.herbcategory (_id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR, desc VARCHAR, herbs VARCHAR)", sQLiteDatabase);
        a("SELECT MAX(useHomeAsFavorite) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN useHomeAsFavorite INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
        a("SELECT MAX(useTraditionCharacters) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN useTraditionCharacters INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
        a("SELECT MAX(hideLeftRightArrows) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN hideLeftRightArrows INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
        a("SELECT MAX(showNCCAOMHerbs) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN showNCCAOMHerbs INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
        a("SELECT MAX(showNCCAOMFormulas) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN showNCCAOMFormulas INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
        a("SELECT MAX(showCABHerbs) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN showCABHerbs INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
        a("SELECT MAX(showCABFormulas) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN showCABFormulas INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
        a("SELECT MAX(coldherbs) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN coldherbs INTEGER NOT NULL DEFAULT " + Integer.toString(-16776961), sQLiteDatabase);
        a("SELECT MAX(coolherbs) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN coolherbs INTEGER NOT NULL DEFAULT " + Integer.toString(-13245185), sQLiteDatabase);
        a("SELECT MAX(neutralherbs) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN neutralherbs INTEGER NOT NULL DEFAULT " + Integer.toString(-1), sQLiteDatabase);
        a("SELECT MAX(warmherbs) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN warmherbs INTEGER NOT NULL DEFAULT " + Integer.toString(-2130432), sQLiteDatabase);
        a("SELECT MAX(hotherbs) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN hotherbs INTEGER NOT NULL DEFAULT " + Integer.toString(-65536), sQLiteDatabase);
        a("SELECT MAX(useColoredHerbNames) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN useColoredHerbNames INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
        a("SELECT MAX(scope) FROM userDB.searches", "ALTER TABLE userDB.searches ADD COLUMN scope INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
        a("SELECT MAX(allowQuickAdd) FROM userDB.settings", "ALTER TABLE userDB.settings ADD COLUMN allowQuickAdd INTEGER NOT NULL DEFAULT 0", sQLiteDatabase);
    }

    private static void a(String str, String str2, SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.rawQuery(str, null);
        } catch (SQLException e2) {
            if (e2.getMessage().indexOf("no such column") >= 0 || e2.getMessage().indexOf("no such table") >= 0) {
                try {
                    sQLiteDatabase.execSQL(str2);
                } catch (SQLException e3) {
                    a(e3);
                }
            }
        }
    }

    public static String a(String str, String str2, String str3) {
        StringBuffer stringBuffer = new StringBuffer('.' + str + '.' + str2 + " IN (");
        String[] split = str3.split(",");
        boolean z = true;
        for (int i = 0; i < split.length; i++) {
            if (z) {
                stringBuffer.append(split[i]);
                z = false;
            } else {
                stringBuffer.append(',' + split[i]);
            }
        }
        stringBuffer.append(')');
        return stringBuffer.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0020  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(java.lang.String r3, android.database.sqlite.SQLiteDatabase r4) {
        /*
            r1 = 0
            r2 = 0
            r0 = 0
            android.database.Cursor r3 = r4.rawQuery(r3, r0)     // Catch:{ SQLException -> 0x001a }
            android.database.sqlite.SQLiteCursor r3 = (android.database.sqlite.SQLiteCursor) r3     // Catch:{ SQLException -> 0x001a }
            boolean r0 = r3.moveToFirst()     // Catch:{ SQLException -> 0x002f, all -> 0x002c }
            if (r0 == 0) goto L_0x0034
            r0 = 0
            int r0 = r3.getInt(r0)     // Catch:{ SQLException -> 0x002f, all -> 0x002c }
        L_0x0014:
            if (r3 == 0) goto L_0x0019
            r3.close()
        L_0x0019:
            return r0
        L_0x001a:
            r0 = move-exception
        L_0x001b:
            a(r0)     // Catch:{ all -> 0x0025 }
            if (r1 == 0) goto L_0x0032
            r1.close()
            r0 = r2
            goto L_0x0019
        L_0x0025:
            r0 = move-exception
        L_0x0026:
            if (r1 == 0) goto L_0x002b
            r1.close()
        L_0x002b:
            throw r0
        L_0x002c:
            r0 = move-exception
            r1 = r3
            goto L_0x0026
        L_0x002f:
            r0 = move-exception
            r1 = r3
            goto L_0x001b
        L_0x0032:
            r0 = r2
            goto L_0x0019
        L_0x0034:
            r0 = r2
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.q.a(java.lang.String, android.database.sqlite.SQLiteDatabase):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0059  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(java.lang.String r4, java.lang.String r5, java.lang.String r6, java.lang.String r7, android.database.sqlite.SQLiteDatabase r8) {
        /*
            r3 = 0
            java.lang.Integer r0 = com.cyberandsons.tcmaidtrial.misc.dh.f946a
            int r0 = r0.intValue()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r4)
            r2 = 32
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r2 = "='"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r2 = "' "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r2 = 0
            android.database.Cursor r4 = r8.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x004a, all -> 0x0055 }
            android.database.sqlite.SQLiteCursor r4 = (android.database.sqlite.SQLiteCursor) r4     // Catch:{ SQLException -> 0x004a, all -> 0x0055 }
            boolean r1 = r4.moveToFirst()     // Catch:{ SQLException -> 0x0063, all -> 0x005d }
            if (r1 == 0) goto L_0x0044
            r1 = 0
            int r0 = r4.getInt(r1)     // Catch:{ SQLException -> 0x0063, all -> 0x005d }
        L_0x0044:
            if (r4 == 0) goto L_0x0049
            r4.close()
        L_0x0049:
            return r0
        L_0x004a:
            r1 = move-exception
            r2 = r3
        L_0x004c:
            a(r1)     // Catch:{ all -> 0x0060 }
            if (r2 == 0) goto L_0x0049
            r2.close()
            goto L_0x0049
        L_0x0055:
            r0 = move-exception
            r1 = r3
        L_0x0057:
            if (r1 == 0) goto L_0x005c
            r1.close()
        L_0x005c:
            throw r0
        L_0x005d:
            r0 = move-exception
            r1 = r4
            goto L_0x0057
        L_0x0060:
            r0 = move-exception
            r1 = r2
            goto L_0x0057
        L_0x0063:
            r1 = move-exception
            r2 = r4
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.q.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, android.database.sqlite.SQLiteDatabase):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0056  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.lang.String r4, int r5, android.database.sqlite.SQLiteDatabase r6) {
        /*
            r3 = 0
            r2 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r1 = " WHERE _id="
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = java.lang.Integer.toString(r5)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = 0
            android.database.Cursor r4 = r6.rawQuery(r0, r1)     // Catch:{ SQLException -> 0x003a, all -> 0x0046 }
            android.database.sqlite.SQLiteCursor r4 = (android.database.sqlite.SQLiteCursor) r4     // Catch:{ SQLException -> 0x003a, all -> 0x0046 }
            boolean r0 = r4.moveToFirst()     // Catch:{ SQLException -> 0x0053, all -> 0x004e }
            if (r0 == 0) goto L_0x0058
            r0 = 0
            int r0 = r4.getInt(r0)     // Catch:{ SQLException -> 0x0053, all -> 0x004e }
            if (r0 != 0) goto L_0x0038
            r0 = r2
        L_0x0032:
            if (r4 == 0) goto L_0x0037
            r4.close()
        L_0x0037:
            return r0
        L_0x0038:
            r0 = 1
            goto L_0x0032
        L_0x003a:
            r0 = move-exception
            r1 = r3
        L_0x003c:
            a(r0)     // Catch:{ all -> 0x0051 }
            if (r1 == 0) goto L_0x0056
            r1.close()
            r0 = r2
            goto L_0x0037
        L_0x0046:
            r0 = move-exception
            r1 = r3
        L_0x0048:
            if (r1 == 0) goto L_0x004d
            r1.close()
        L_0x004d:
            throw r0
        L_0x004e:
            r0 = move-exception
            r1 = r4
            goto L_0x0048
        L_0x0051:
            r0 = move-exception
            goto L_0x0048
        L_0x0053:
            r0 = move-exception
            r1 = r4
            goto L_0x003c
        L_0x0056:
            r0 = r2
            goto L_0x0037
        L_0x0058:
            r0 = r2
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.q.a(java.lang.String, int, android.database.sqlite.SQLiteDatabase):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x007c, code lost:
        throw new java.lang.Error("Error copying database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x007d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        android.util.Log.e("DBH:copyDataBase()", r2.getLocalizedMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        a(r2);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0074 A[ExcHandler: IOException (e java.io.IOException), Splitter:B:10:0x002d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r5 = this;
            boolean r0 = r5.b()
            if (r0 == 0) goto L_0x001b
            java.lang.String r0 = "DBH:createDataBase()"
            java.lang.String r1 = "database exists"
            android.util.Log.i(r0, r1)
        L_0x000d:
            boolean r0 = c()
            if (r0 == 0) goto L_0x0130
            java.lang.String r0 = "DBH:createDataBase()"
            java.lang.String r1 = "user database exists"
            android.util.Log.i(r0, r1)
        L_0x001a:
            return
        L_0x001b:
            boolean r0 = com.cyberandsons.tcmaidtrial.a.q.e
            if (r0 == 0) goto L_0x0026
            java.lang.String r0 = "DBH:createDataBase()"
            java.lang.String r1 = "before this.getWritableDatabase()"
            android.util.Log.i(r0, r1)
        L_0x0026:
            android.database.sqlite.SQLiteDatabase r0 = r5.getReadableDatabase()
            r0.close()
            boolean r0 = com.cyberandsons.tcmaidtrial.a.q.e     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            if (r0 == 0) goto L_0x003d
            java.lang.String r0 = "DBH:createDataBase()"
            java.lang.String r1 = "before copyDataBase()"
            android.util.Log.i(r0, r1)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.a.q.f179b     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            a(r0)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
        L_0x003d:
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            r1.<init>()     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.a.q.f179b     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.a.q.c     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x007d, Exception -> 0x008d, IOException -> 0x0074 }
            r2.<init>(r1)     // Catch:{ FileNotFoundException -> 0x007d, Exception -> 0x008d, IOException -> 0x0074 }
            r0 = r2
        L_0x0059:
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            android.content.Context r3 = r5.f180a     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            android.content.res.Resources r3 = r3.getResources()     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            r4 = 2131034114(0x7f050002, float:1.7678736E38)
            java.io.InputStream r3 = r3.openRawResource(r4)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
        L_0x006a:
            int r4 = r3.read(r2)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            if (r4 <= 0) goto L_0x0092
            r0.write(r2)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            goto L_0x006a
        L_0x0074:
            r0 = move-exception
            java.lang.Error r0 = new java.lang.Error
            java.lang.String r1 = "Error copying database"
            r0.<init>(r1)
            throw r0
        L_0x007d:
            r2 = move-exception
            java.lang.String r3 = "DBH:copyDataBase()"
            java.lang.String r2 = r2.getLocalizedMessage()     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            android.util.Log.e(r3, r2)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            goto L_0x0059
        L_0x0088:
            r0 = move-exception
            a(r0)
            goto L_0x000d
        L_0x008d:
            r2 = move-exception
            a(r2)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            goto L_0x0059
        L_0x0092:
            r3.close()     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            android.content.Context r3 = r5.f180a     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            android.content.res.Resources r3 = r3.getResources()     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            r4 = 2131034115(0x7f050003, float:1.7678738E38)
            java.io.InputStream r3 = r3.openRawResource(r4)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
        L_0x00a2:
            int r4 = r3.read(r2)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            if (r4 <= 0) goto L_0x00ac
            r0.write(r2)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            goto L_0x00a2
        L_0x00ac:
            r3.close()     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            android.content.Context r3 = r5.f180a     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            android.content.res.Resources r3 = r3.getResources()     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            r4 = 2131034116(0x7f050004, float:1.767874E38)
            java.io.InputStream r3 = r3.openRawResource(r4)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
        L_0x00bc:
            int r4 = r3.read(r2)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            if (r4 <= 0) goto L_0x00c6
            r0.write(r2)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            goto L_0x00bc
        L_0x00c6:
            r3.close()     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            android.content.Context r3 = r5.f180a     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            android.content.res.Resources r3 = r3.getResources()     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            r4 = 2131034117(0x7f050005, float:1.7678742E38)
            java.io.InputStream r3 = r3.openRawResource(r4)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
        L_0x00d6:
            int r4 = r3.read(r2)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            if (r4 <= 0) goto L_0x00e2
            if (r4 <= 0) goto L_0x00d6
            r0.write(r2)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            goto L_0x00d6
        L_0x00e2:
            r3.close()     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            android.content.Context r3 = r5.f180a     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            android.content.res.Resources r3 = r3.getResources()     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            r4 = 2131034118(0x7f050006, float:1.7678745E38)
            java.io.InputStream r3 = r3.openRawResource(r4)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
        L_0x00f2:
            int r4 = r3.read(r2)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            if (r4 <= 0) goto L_0x00fe
            if (r4 <= 0) goto L_0x00f2
            r0.write(r2)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            goto L_0x00f2
        L_0x00fe:
            r3.close()     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            if (r0 == 0) goto L_0x0106
            r0.close()     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
        L_0x0106:
            java.lang.String r0 = "DBH:copyDataBase()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            r2.<init>()     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            java.lang.String r2 = " copied."
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            android.util.Log.d(r0, r1)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            boolean r0 = com.cyberandsons.tcmaidtrial.a.q.e     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            if (r0 == 0) goto L_0x000d
            java.lang.String r0 = "DBH:createDataBase()"
            java.lang.String r1 = "after copyDataBase()"
            android.util.Log.i(r0, r1)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.a.q.f179b     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            a(r0)     // Catch:{ IOException -> 0x0074, Exception -> 0x0088 }
            goto L_0x000d
        L_0x0130:
            boolean r0 = com.cyberandsons.tcmaidtrial.a.q.e
            if (r0 == 0) goto L_0x0140
            java.lang.String r0 = "DBH:createDataBase()"
            java.lang.String r1 = "before userDB this.getWritableDatabase()"
            android.util.Log.i(r0, r1)
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.a.q.f179b
            a(r0)
        L_0x0140:
            android.database.sqlite.SQLiteDatabase r0 = r5.getWritableDatabase()
            r0.close()
            boolean r0 = com.cyberandsons.tcmaidtrial.a.q.e     // Catch:{ IOException -> 0x016c, Exception -> 0x0175 }
            if (r0 == 0) goto L_0x0157
            java.lang.String r0 = "DBH:createDataBase()"
            java.lang.String r1 = "before copyUserDataBase()"
            android.util.Log.i(r0, r1)     // Catch:{ IOException -> 0x016c, Exception -> 0x0175 }
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.a.q.f179b     // Catch:{ IOException -> 0x016c, Exception -> 0x0175 }
            a(r0)     // Catch:{ IOException -> 0x016c, Exception -> 0x0175 }
        L_0x0157:
            r5.d()     // Catch:{ IOException -> 0x016c, Exception -> 0x0175 }
            boolean r0 = com.cyberandsons.tcmaidtrial.a.q.e     // Catch:{ IOException -> 0x016c, Exception -> 0x0175 }
            if (r0 == 0) goto L_0x001a
            java.lang.String r0 = "DBH:createDataBase()"
            java.lang.String r1 = "after copyUserDataBase()"
            android.util.Log.i(r0, r1)     // Catch:{ IOException -> 0x016c, Exception -> 0x0175 }
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.a.q.f179b     // Catch:{ IOException -> 0x016c, Exception -> 0x0175 }
            a(r0)     // Catch:{ IOException -> 0x016c, Exception -> 0x0175 }
            goto L_0x001a
        L_0x016c:
            r0 = move-exception
            java.lang.Error r0 = new java.lang.Error
            java.lang.String r1 = "Error copying User database"
            r0.<init>(r1)
            throw r0
        L_0x0175:
            r0 = move-exception
            a(r0)
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.q.a():void");
    }

    private boolean b() {
        SQLiteDatabase sQLiteDatabase;
        String str = f179b + c;
        try {
            sQLiteDatabase = SQLiteDatabase.openDatabase(str, null, 16);
        } catch (SQLiteException e2) {
            String string = this.f180a.getString(C0000R.string.tcmDatabase);
            File file = new File(f179b);
            if (file.isDirectory()) {
                for (File file2 : file.listFiles()) {
                    if (file2.getName().indexOf(string) >= 0) {
                        Log.d("DBH:checkDatabase()", "Removing " + file2.getName());
                        try {
                            file2.delete();
                        } catch (SecurityException e3) {
                            Log.e("DBH:checkDatabase()", "Error deleting old DB - " + e3.getLocalizedMessage());
                        }
                    }
                }
            }
            sQLiteDatabase = null;
        } catch (Exception e4) {
            a(e4);
            sQLiteDatabase = null;
        }
        if (sQLiteDatabase != null) {
            sQLiteDatabase.close();
        }
        Log.d("DBH:checkDataBase()", str + (sQLiteDatabase != null ? " EXISTS" : " DOESN'T EXIST"));
        if (sQLiteDatabase != null) {
            return true;
        }
        return false;
    }

    private static boolean c() {
        SQLiteDatabase sQLiteDatabase;
        String str = f179b + d;
        a(f179b);
        try {
            sQLiteDatabase = SQLiteDatabase.openDatabase(str, null, 16);
        } catch (SQLiteException e2) {
            a((SQLException) e2);
            sQLiteDatabase = null;
        } catch (Exception e3) {
            a(e3);
            sQLiteDatabase = null;
        }
        if (sQLiteDatabase != null) {
            sQLiteDatabase.close();
        }
        Log.d("DBH:checkUserDataBase()", str + (sQLiteDatabase != null ? " EXISTS" : " DOESN'T EXIST"));
        if (sQLiteDatabase != null) {
            return true;
        }
        return false;
    }

    private static void a(String str) {
        try {
            File file = new File(str);
            if (file.isDirectory()) {
                for (File file2 : file.listFiles()) {
                    Log.d("name", file2.getName());
                    Log.d("size", Long.toString(file2.getAbsoluteFile().length()));
                }
            }
        } catch (Exception e2) {
            a(e2);
        }
    }

    private void d() {
        InputStream open = this.f180a.getAssets().open(d);
        String str = f179b + d;
        FileOutputStream fileOutputStream = new FileOutputStream(str);
        byte[] bArr = new byte[1024];
        while (true) {
            int read = open.read(bArr);
            if (read > 0) {
                fileOutputStream.write(bArr, 0, read);
            } else {
                fileOutputStream.flush();
                fileOutputStream.close();
                open.close();
                Log.d("DBH:copyUserDataBase()", str + " copied.");
                return;
            }
        }
    }

    public final synchronized void close() {
        super.close();
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int b(java.lang.String r4, android.database.sqlite.SQLiteDatabase r5) {
        /*
            r2 = 0
            r0 = -1
            r1 = 0
            android.database.Cursor r1 = r5.rawQuery(r4, r1)     // Catch:{ SQLException -> 0x0018, all -> 0x0022 }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLException -> 0x002f, all -> 0x002a }
            if (r2 == 0) goto L_0x0012
            r2 = 0
            int r0 = r1.getInt(r2)     // Catch:{ SQLException -> 0x002f, all -> 0x002a }
        L_0x0012:
            if (r1 == 0) goto L_0x0017
            r1.close()
        L_0x0017:
            return r0
        L_0x0018:
            r1 = move-exception
        L_0x0019:
            a(r1)     // Catch:{ all -> 0x002c }
            if (r2 == 0) goto L_0x0017
            r2.close()
            goto L_0x0017
        L_0x0022:
            r0 = move-exception
            r1 = r2
        L_0x0024:
            if (r1 == 0) goto L_0x0029
            r1.close()
        L_0x0029:
            throw r0
        L_0x002a:
            r0 = move-exception
            goto L_0x0024
        L_0x002c:
            r0 = move-exception
            r1 = r2
            goto L_0x0024
        L_0x002f:
            r2 = move-exception
            r3 = r2
            r2 = r1
            r1 = r3
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.q.b(java.lang.String, android.database.sqlite.SQLiteDatabase):int");
    }

    public static void a(SQLException sQLException) {
        Log.e("processSQLException", "\n*** SQLException caught ***\n");
        if (sQLException != null) {
            Log.e("processSQLException", "Message:  " + sQLException.getMessage());
            Log.e("processSQLException", "");
            for (Throwable cause = sQLException.getCause(); cause != null; cause = cause.getCause()) {
                Log.e("processSQLException", "Cause:" + cause);
            }
        }
    }

    public static void a(Exception exc) {
        Log.e("processException", "\n*** Exception caught ***\n");
        if (exc != null) {
            Log.e("processException", "Message:  " + exc.getMessage());
            Log.e("processException", "");
            for (Throwable cause = exc.getCause(); cause != null; cause = cause.getCause()) {
                Log.e("processException", "Cause:" + cause);
            }
        }
    }
}
