package com.cyberandsons.tcmaidtrial.a;

public final class l {
    /* JADX WARNING: Removed duplicated region for block: B:14:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0041  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(java.lang.String r4, android.database.sqlite.SQLiteDatabase r5) {
        /*
            r3 = 0
            r2 = 0
            java.lang.String r0 = "SELECT _id FROM userDB.herbcategory WHERE name='%s'"
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r1[r2] = r4
            java.lang.String r0 = java.lang.String.format(r0, r1)
            r1 = 0
            android.database.Cursor r4 = r5.rawQuery(r0, r1)     // Catch:{ SQLException -> 0x0025, all -> 0x0031 }
            android.database.sqlite.SQLiteCursor r4 = (android.database.sqlite.SQLiteCursor) r4     // Catch:{ SQLException -> 0x0025, all -> 0x0031 }
            boolean r0 = r4.moveToFirst()     // Catch:{ SQLException -> 0x003e, all -> 0x0039 }
            if (r0 == 0) goto L_0x0043
            r0 = 0
            int r0 = r4.getInt(r0)     // Catch:{ SQLException -> 0x003e, all -> 0x0039 }
        L_0x001f:
            if (r4 == 0) goto L_0x0024
            r4.close()
        L_0x0024:
            return r0
        L_0x0025:
            r0 = move-exception
            r1 = r3
        L_0x0027:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x003c }
            if (r1 == 0) goto L_0x0041
            r1.close()
            r0 = r2
            goto L_0x0024
        L_0x0031:
            r0 = move-exception
            r1 = r3
        L_0x0033:
            if (r1 == 0) goto L_0x0038
            r1.close()
        L_0x0038:
            throw r0
        L_0x0039:
            r0 = move-exception
            r1 = r4
            goto L_0x0033
        L_0x003c:
            r0 = move-exception
            goto L_0x0033
        L_0x003e:
            r0 = move-exception
            r1 = r4
            goto L_0x0027
        L_0x0041:
            r0 = r2
            goto L_0x0024
        L_0x0043:
            r0 = r2
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.l.a(java.lang.String, android.database.sqlite.SQLiteDatabase):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0045  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.CharSequence a(int r5, android.database.sqlite.SQLiteDatabase r6) {
        /*
            r4 = 0
            r3 = 0
            java.lang.String r0 = "SELECT herbs  FROM userDB.herbcategory WHERE userDB.herbcategory._id=%d"
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.Integer r2 = java.lang.Integer.valueOf(r5)
            r1[r4] = r2
            java.lang.String r0 = java.lang.String.format(r0, r1)
            r1 = 0
            android.database.Cursor r5 = r6.rawQuery(r0, r1)     // Catch:{ SQLException -> 0x0029, all -> 0x0035 }
            android.database.sqlite.SQLiteCursor r5 = (android.database.sqlite.SQLiteCursor) r5     // Catch:{ SQLException -> 0x0029, all -> 0x0035 }
            boolean r0 = r5.moveToFirst()     // Catch:{ SQLException -> 0x0042, all -> 0x003d }
            if (r0 == 0) goto L_0x0047
            r0 = 0
            java.lang.String r0 = r5.getString(r0)     // Catch:{ SQLException -> 0x0042, all -> 0x003d }
        L_0x0023:
            if (r5 == 0) goto L_0x0028
            r5.close()
        L_0x0028:
            return r0
        L_0x0029:
            r0 = move-exception
            r1 = r3
        L_0x002b:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0040 }
            if (r1 == 0) goto L_0x0045
            r1.close()
            r0 = r3
            goto L_0x0028
        L_0x0035:
            r0 = move-exception
            r1 = r3
        L_0x0037:
            if (r1 == 0) goto L_0x003c
            r1.close()
        L_0x003c:
            throw r0
        L_0x003d:
            r0 = move-exception
            r1 = r5
            goto L_0x0037
        L_0x0040:
            r0 = move-exception
            goto L_0x0037
        L_0x0042:
            r0 = move-exception
            r1 = r5
            goto L_0x002b
        L_0x0045:
            r0 = r3
            goto L_0x0028
        L_0x0047:
            r0 = r3
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.l.a(int, android.database.sqlite.SQLiteDatabase):java.lang.CharSequence");
    }
}
