package com.cyberandsons.tcmaidtrial.a;

import android.content.ContentValues;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public final class n {
    private String A;
    private String B;
    private int C;
    private int D;
    private int E;
    private long F;
    private int G;
    private int H;
    private int I;
    private int J;
    private int K;
    private int L;
    private int M;
    private String N;
    private String O;
    private String P;
    private String Q;
    private String R;
    private String S;
    private String T;
    private String U;
    private String V;
    private int W;
    private int X;
    private int Y;
    private int Z;

    /* renamed from: a  reason: collision with root package name */
    private boolean f177a = true;
    private int aa;
    private int ab;
    private long ac;
    private int ad;
    private int ae;
    private int af;
    private int ag;
    private int ah;
    private int ai;
    private int aj;
    private int ak;
    private int al;
    private int am;
    private int an;
    private int ao;
    private int ap;
    private int aq;
    private int ar;
    private int as;
    private int at;
    private int au;
    private int av;
    private int aw;
    private int ax;
    private int ay;

    /* renamed from: b  reason: collision with root package name */
    private boolean f178b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private String t;
    private String u;
    private int v;
    private String w;
    private String x;
    private String y;
    private String z;

    public n() {
        boolean z2;
        if (!this.f177a) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.f178b = z2;
    }

    private static void a(String str, String str2, SQLiteDatabase sQLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(str, str2);
        try {
            sQLiteDatabase.update("userDB.settings", contentValues, "_id=1", null);
        } catch (SQLException e2) {
            q.a(e2);
        }
    }

    private static void a(String str, int i2, SQLiteDatabase sQLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(str, Integer.valueOf(i2));
        try {
            sQLiteDatabase.update("userDB.settings", contentValues, "_id=1", null);
        } catch (SQLException e2) {
            q.a(e2);
        }
    }

    public static boolean a(String str, int i2) {
        if (str == null || str.length() == 0) {
            return false;
        }
        String[] split = str.split(",");
        for (String parseInt : split) {
            if (i2 == Integer.parseInt(parseInt)) {
                return true;
            }
        }
        return false;
    }

    public static int a(String str) {
        if (str == null || str.length() <= 0) {
            return 0;
        }
        return str.split(",").length;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0285  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x028d  */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.database.sqlite.SQLiteDatabase r6) {
        /*
            r5 = this;
            r3 = 0
            r2 = 1
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT _id, useAuricularImages, useHerbImages, usePointImages, useTungImages, useWristAnkleImages, searchAuricular, searchDiagnosis, searchFormulas, searchHerbs, searchPoints, searchTung, searchWristAnkle, useEditCapitalize, useEditCorrection, displayAllAdjacent, pointsInColor, showOnlyPointNames, useFavorites,favoriteAuricular, favoriteDiags, favoriteHerbs, favoriteFormulas, favoritePoints, lastRelease,searchSixChannels, syncTimeStamp, email, doTraditional, usePointsLabel, formulaDisplayName, herbDisplayName, pushnotifregistered, auricularSearch, diagnosisSearch, formulaSearch, herbSearch, pointSearch, sixstageSearch, tungSearch, wristankleSearch, showFormulaChineseCharInList, showHerbChineseCharInList, showPointChineseCharInList, showTungChineseCharInList, showEnglishInPointList, showEnglishInTungList, initialTimeStamp, showPointMeridians, showAdjacentPointMeridians, alwaysShowAnswers, usePulseDiagImages, pulseDiagSearch, searchPulseDiag, useNonAndroidMarket, useHomeAsFavorite, useTraditionCharacters, hideLeftRightArrows, showNCCAOMHerbs, showNCCAOMFormulas, showCABHerbs, showCABFormulas, coldherbs, coolherbs, neutralherbs, warmherbs, hotherbs, useColoredHerbNames, allowQuickAdd, showChangeInNetworkStatus, useTanImages, showTanPoints, showTungPoints, searchTan, searchTcmIM, regKey  FROM userDB.settings WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "_id = 1"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = 0
            android.database.Cursor r0 = r6.rawQuery(r0, r1)     // Catch:{ SQLException -> 0x027e, all -> 0x0289 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x027e, all -> 0x0289 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r0.moveToFirst()     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            if (r1 != r2) goto L_0x0278
            r1 = 2
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.c = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 3
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.d = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 70
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.f = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 4
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.g = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 71
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.h = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 72
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.i = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 6
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.j = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 7
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.k = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 8
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.l = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 9
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.m = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 10
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.n = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 73
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.p = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 74
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.q = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 11
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.r = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 12
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.s = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 75
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.t = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 27
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.u = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 15
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.v = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 19
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.w = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 20
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.x = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 22
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.y = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 21
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.z = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 23
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.A = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 24
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.B = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 16
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.C = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 25
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.D = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 17
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.E = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 26
            long r1 = r0.getLong(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.F = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 1
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.G = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 13
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.H = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 14
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.I = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 18
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.J = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 5
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.K = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 28
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.L = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 29
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.M = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 33
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.N = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 34
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.O = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 35
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.P = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 36
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.Q = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 37
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.R = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 38
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.T = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 39
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.U = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 40
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.V = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 30
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.W = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 31
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.X = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 32
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.Y = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 42
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.Z = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 45
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.aa = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 46
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.ab = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 47
            long r1 = r0.getLong(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.ac = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 43
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.ad = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 44
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.ae = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 41
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.af = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 69
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.ag = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 48
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.ah = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 49
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.ai = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 50
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.aj = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 51
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.e = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 52
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.S = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 53
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.o = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 54
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.ak = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 55
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.al = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 56
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.am = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 57
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.an = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 60
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.aq = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 61
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.ar = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 58
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.ao = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 59
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.ap = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 67
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.ax = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 62
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.as = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 63
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.at = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 64
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.au = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 65
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.av = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 66
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.aw = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r1 = 68
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
            r5.ay = r1     // Catch:{ SQLException -> 0x0298, all -> 0x0291 }
        L_0x0278:
            if (r0 == 0) goto L_0x027d
            r0.close()
        L_0x027d:
            return
        L_0x027e:
            r0 = move-exception
            r1 = r3
        L_0x0280:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0296 }
            if (r1 == 0) goto L_0x027d
            r1.close()
            goto L_0x027d
        L_0x0289:
            r0 = move-exception
            r1 = r3
        L_0x028b:
            if (r1 == 0) goto L_0x0290
            r1.close()
        L_0x0290:
            throw r0
        L_0x0291:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x028b
        L_0x0296:
            r0 = move-exception
            goto L_0x028b
        L_0x0298:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0280
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.a.n.a(android.database.sqlite.SQLiteDatabase):void");
    }

    public final boolean a() {
        return this.c == 1 ? this.f177a : this.f178b;
    }

    public final void a(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.c = z2 ? 1 : 0;
        a("useHerbImages", this.c, sQLiteDatabase);
    }

    public final boolean b() {
        return this.d == 1 ? this.f177a : this.f178b;
    }

    public final void b(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.d = z2 ? 1 : 0;
        a("usePointImages", this.d, sQLiteDatabase);
    }

    public final boolean c() {
        return this.g == 1 ? this.f177a : this.f178b;
    }

    public final void c(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.g = z2 ? 1 : 0;
        a("useTungImages", this.g, sQLiteDatabase);
    }

    public final boolean d() {
        return this.j == 1 ? this.f177a : this.f178b;
    }

    public final boolean e() {
        return this.k == 1 ? this.f177a : this.f178b;
    }

    public final boolean f() {
        return this.l == 1 ? this.f177a : this.f178b;
    }

    public final boolean g() {
        return this.m == 1 ? this.f177a : this.f178b;
    }

    public final boolean h() {
        return this.n == 1 ? this.f177a : this.f178b;
    }

    public final boolean i() {
        return this.o == 1 ? this.f177a : this.f178b;
    }

    public final boolean j() {
        return this.r == 1 ? this.f177a : this.f178b;
    }

    public final boolean k() {
        return this.s == 1 ? this.f177a : this.f178b;
    }

    public final String l() {
        return this.u;
    }

    public final void a(String str, SQLiteDatabase sQLiteDatabase) {
        this.u = str;
        a("email", str, sQLiteDatabase);
    }

    public final boolean m() {
        return this.v == 1 ? this.f177a : this.f178b;
    }

    public final void d(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.v = z2 ? 1 : 0;
        a("displayAllAdjacent", this.v, sQLiteDatabase);
    }

    public final String n() {
        return this.w;
    }

    public final void a(String str, String str2, String str3, String str4, String str5, SQLiteDatabase sQLiteDatabase) {
        this.w = str;
        this.x = str2;
        this.z = str3;
        this.y = str4;
        this.A = str5;
        ContentValues contentValues = new ContentValues();
        contentValues.put("favoriteAuricular", str);
        contentValues.put("favoriteDiags", str2);
        contentValues.put("favoriteHerbs", str3);
        contentValues.put("favoriteFormulas", str4);
        contentValues.put("favoritePoints", str5);
        try {
            sQLiteDatabase.update("userDB.settings", contentValues, "_id=1", null);
        } catch (SQLException e2) {
            q.a(e2);
        }
    }

    public final void b(String str, SQLiteDatabase sQLiteDatabase) {
        this.w = str;
        a("favoriteAuricular", this.w, sQLiteDatabase);
    }

    public final String o() {
        return this.x;
    }

    public final void c(String str, SQLiteDatabase sQLiteDatabase) {
        this.x = str;
        a("favoriteDiags", this.x, sQLiteDatabase);
    }

    public final String p() {
        return this.y;
    }

    public final void d(String str, SQLiteDatabase sQLiteDatabase) {
        this.y = str;
        a("favoriteFormulas", this.y, sQLiteDatabase);
    }

    public final String q() {
        return this.z;
    }

    public final void e(String str, SQLiteDatabase sQLiteDatabase) {
        this.z = str;
        a("favoriteHerbs", this.z, sQLiteDatabase);
    }

    public final String r() {
        return this.A;
    }

    public final void f(String str, SQLiteDatabase sQLiteDatabase) {
        this.A = str;
        a("favoritePoints", this.A, sQLiteDatabase);
    }

    public final String s() {
        return this.B;
    }

    public final void g(String str, SQLiteDatabase sQLiteDatabase) {
        this.B = str;
        a("lastRelease", this.B, sQLiteDatabase);
    }

    public final boolean t() {
        return this.C == 1 ? this.f177a : this.f178b;
    }

    public final void e(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.C = z2 ? 1 : 0;
        a("pointsInColor", this.C, sQLiteDatabase);
    }

    public final boolean u() {
        return this.D == 1 ? this.f177a : this.f178b;
    }

    public final boolean v() {
        return this.E == 1 ? this.f177a : this.f178b;
    }

    public final void f(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.E = z2 ? 1 : 0;
        a("showOnlyPointNames", this.E, sQLiteDatabase);
    }

    public final boolean w() {
        return this.G == 1 ? this.f177a : this.f178b;
    }

    public final void g(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.G = z2 ? 1 : 0;
        a("useAuricularImages", this.G, sQLiteDatabase);
    }

    public final boolean x() {
        return this.H == 1 ? this.f177a : this.f178b;
    }

    public final void h(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.H = z2 ? 1 : 0;
        a("useEditCapitalize", this.H, sQLiteDatabase);
    }

    public final boolean y() {
        return this.I == 1 ? this.f177a : this.f178b;
    }

    public final void i(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.I = z2 ? 1 : 0;
        a("useEditCorrection", this.I, sQLiteDatabase);
    }

    public final boolean z() {
        return this.J == 1 ? this.f177a : this.f178b;
    }

    public final void j(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.J = z2 ? 1 : 0;
        a("useFavorites", this.J, sQLiteDatabase);
    }

    public final boolean A() {
        return this.K == 1 ? this.f177a : this.f178b;
    }

    public final void k(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.K = z2 ? 1 : 0;
        a("useWristAnkleImages", this.K, sQLiteDatabase);
    }

    public final boolean B() {
        return this.L == 1 ? this.f177a : this.f178b;
    }

    public final void l(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.L = z2 ? 1 : 0;
        a("doTraditional", this.L, sQLiteDatabase);
    }

    public final boolean C() {
        return this.M == 1 ? this.f177a : this.f178b;
    }

    public final void m(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.M = z2 ? 1 : 0;
        a("usePointsLabel", this.M, sQLiteDatabase);
    }

    public final String D() {
        return this.N;
    }

    public final void h(String str, SQLiteDatabase sQLiteDatabase) {
        this.N = str;
        a("auricularSearch", this.N, sQLiteDatabase);
    }

    public final String E() {
        return this.O;
    }

    public final void i(String str, SQLiteDatabase sQLiteDatabase) {
        this.O = str;
        a("diagnosisSearch", this.O, sQLiteDatabase);
    }

    public final String F() {
        return this.P;
    }

    public final void j(String str, SQLiteDatabase sQLiteDatabase) {
        this.P = str;
        a("formulaSearch", this.P, sQLiteDatabase);
    }

    public final String G() {
        return this.Q;
    }

    public final void k(String str, SQLiteDatabase sQLiteDatabase) {
        this.Q = str;
        a("herbSearch", this.Q, sQLiteDatabase);
    }

    public final String H() {
        return this.R;
    }

    public final void l(String str, SQLiteDatabase sQLiteDatabase) {
        this.R = str;
        a("pointSearch", this.R, sQLiteDatabase);
    }

    public final boolean I() {
        return this.e == 1 ? this.f177a : this.f178b;
    }

    public final void n(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.e = z2 ? 1 : 0;
        a("usePulseDiagImages", this.e, sQLiteDatabase);
    }

    public final String J() {
        return this.S;
    }

    public final void m(String str, SQLiteDatabase sQLiteDatabase) {
        this.S = str;
        a("pulseDiagSearch", this.S, sQLiteDatabase);
    }

    public final String K() {
        return this.T;
    }

    public final void n(String str, SQLiteDatabase sQLiteDatabase) {
        this.T = str;
        a("sixstageSearch", this.T, sQLiteDatabase);
    }

    public final String L() {
        return this.U;
    }

    public final void o(String str, SQLiteDatabase sQLiteDatabase) {
        this.U = str;
        a("tungSearch", this.U, sQLiteDatabase);
    }

    public final String M() {
        return this.V;
    }

    public final void p(String str, SQLiteDatabase sQLiteDatabase) {
        this.V = str;
        a("wristankleSearch", this.V, sQLiteDatabase);
    }

    public final int N() {
        return this.W;
    }

    public final void a(int i2, SQLiteDatabase sQLiteDatabase) {
        this.W = i2;
        a("formulaDisplayName", i2, sQLiteDatabase);
    }

    public final int O() {
        return this.X;
    }

    public final void b(int i2, SQLiteDatabase sQLiteDatabase) {
        this.X = i2;
        a("herbDisplayName", i2, sQLiteDatabase);
    }

    public final boolean P() {
        return this.Y == 1 ? this.f177a : this.f178b;
    }

    public final int Q() {
        return this.Z;
    }

    public final void o(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.Z = z2 ? 1 : 0;
        a("showHerbChineseCharInList", this.Z, sQLiteDatabase);
    }

    public final int R() {
        return this.aa;
    }

    public final void p(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.aa = z2 ? 1 : 0;
        a("showEnglishInPointList", this.aa, sQLiteDatabase);
    }

    public final int S() {
        return this.ab;
    }

    public final void q(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.ab = z2 ? 1 : 0;
        a("showEnglishInTungList", this.ab, sQLiteDatabase);
    }

    public final long T() {
        return this.ac;
    }

    public final void a(long j2, SQLiteDatabase sQLiteDatabase) {
        this.ac = j2;
        long j3 = this.ac;
        ContentValues contentValues = new ContentValues();
        contentValues.put("initialTimeStamp", Long.valueOf(j3));
        try {
            sQLiteDatabase.update("userDB.settings", contentValues, "_id=1", null);
        } catch (SQLException e2) {
            q.a(e2);
        }
    }

    public final int U() {
        return this.ad;
    }

    public final void r(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.ad = z2 ? 1 : 0;
        a("showPointChineseCharInList", this.ad, sQLiteDatabase);
    }

    public final int V() {
        return this.ae;
    }

    public final void s(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.ae = z2 ? 1 : 0;
        a("showTungChineseCharInList", this.ae, sQLiteDatabase);
    }

    public final int W() {
        return this.af;
    }

    public final void t(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.af = z2 ? 1 : 0;
        a("showFormulaChineseCharInList", this.af, sQLiteDatabase);
    }

    public final void u(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.ag = z2 ? 1 : 0;
        a("showChangeInNetworkStatus", this.ag, sQLiteDatabase);
    }

    public final boolean X() {
        return this.ah == 1 ? this.f177a : this.f178b;
    }

    public final void v(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.ah = z2 ? 1 : 0;
        a("showPointMeridians", this.ah, sQLiteDatabase);
    }

    public final boolean Y() {
        return this.ai == 1 ? this.f177a : this.f178b;
    }

    public final void w(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.ai = z2 ? 1 : 0;
        a("showAdjacentPointMeridians", this.ai, sQLiteDatabase);
    }

    public final boolean Z() {
        return this.aj == 1 ? this.f177a : this.f178b;
    }

    public final void x(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.aj = z2 ? 1 : 0;
        a("alwaysShowAnswers", this.aj, sQLiteDatabase);
    }

    public final boolean aa() {
        return this.ak == 1 ? this.f177a : this.f178b;
    }

    public final void y(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.ak = z2 ? 1 : 0;
        a("useNonAndroidMarket", this.ak, sQLiteDatabase);
    }

    public final boolean ab() {
        return this.al == 1 ? this.f177a : this.f178b;
    }

    public final void z(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.al = z2 ? 1 : 0;
        a("useHomeAsFavorite", this.al, sQLiteDatabase);
    }

    public final int ac() {
        return this.am;
    }

    public final void c(int i2, SQLiteDatabase sQLiteDatabase) {
        this.am = i2;
        a("useTraditionCharacters", this.am, sQLiteDatabase);
    }

    public final boolean ad() {
        return this.an == 1 ? this.f177a : this.f178b;
    }

    public final void A(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.an = z2 ? 1 : 0;
        a("hideLeftRightArrows", this.an, sQLiteDatabase);
    }

    public final boolean ae() {
        return this.ao == 1 ? this.f177a : this.f178b;
    }

    public final void B(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.ao = z2 ? 1 : 0;
        a("showNCCAOMHerbs", this.ao, sQLiteDatabase);
    }

    public final boolean af() {
        return this.ap == 1 ? this.f177a : this.f178b;
    }

    public final void C(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.ap = z2 ? 1 : 0;
        a("showNCCAOMFormulas", this.ap, sQLiteDatabase);
    }

    public final boolean ag() {
        return this.aq == 1 ? this.f177a : this.f178b;
    }

    public final void D(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.aq = z2 ? 1 : 0;
        a("showCABHerbs", this.aq, sQLiteDatabase);
    }

    public final boolean ah() {
        return this.ar == 1 ? this.f177a : this.f178b;
    }

    public final void E(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.ar = z2 ? 1 : 0;
        a("showCABFormulas", this.ar, sQLiteDatabase);
    }

    public final int ai() {
        return this.as;
    }

    public final void d(int i2, SQLiteDatabase sQLiteDatabase) {
        this.as = i2;
        a("coldherbs", this.as, sQLiteDatabase);
    }

    public final int aj() {
        return this.at;
    }

    public final void e(int i2, SQLiteDatabase sQLiteDatabase) {
        this.at = i2;
        a("coolherbs", this.at, sQLiteDatabase);
    }

    public final int ak() {
        return this.au;
    }

    public final void f(int i2, SQLiteDatabase sQLiteDatabase) {
        this.au = i2;
        a("neutralherbs", this.au, sQLiteDatabase);
    }

    public final int al() {
        return this.av;
    }

    public final void g(int i2, SQLiteDatabase sQLiteDatabase) {
        this.av = i2;
        a("warmherbs", this.av, sQLiteDatabase);
    }

    public final int am() {
        return this.aw;
    }

    public final void h(int i2, SQLiteDatabase sQLiteDatabase) {
        this.aw = i2;
        a("hotherbs", this.aw, sQLiteDatabase);
    }

    public final boolean an() {
        return this.ax == 1 ? this.f177a : this.f178b;
    }

    public final void F(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.ax = z2 ? 1 : 0;
        a("useColoredHerbNames", this.ax, sQLiteDatabase);
    }

    public final void G(boolean z2, SQLiteDatabase sQLiteDatabase) {
        this.ay = z2 ? 1 : 0;
        a("allowQuickAdd", this.ay, sQLiteDatabase);
    }
}
