package com.cyberandsons.tcmaidtrial.a;

import android.content.ContentValues;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class k {

    /* renamed from: a  reason: collision with root package name */
    private int f175a;

    /* renamed from: b  reason: collision with root package name */
    private int f176b;
    private String c;
    private String d;
    private String e;
    private String f;
    private int g;
    private int h;
    private int i;
    private Date j;

    public k() {
    }

    public k(int i2, String str, String str2, String str3, String str4, int i3, int i4, int i5, Date date) {
        this.f175a = -1;
        this.f176b = i2;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = str4;
        this.g = i3;
        this.h = i4;
        this.i = i5;
        this.j = date;
    }

    public final void a(SQLiteDatabase sQLiteDatabase) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ContentValues contentValues = new ContentValues();
        contentValues.put("area", Integer.toString(this.f176b));
        contentValues.put("questions", Integer.toString(this.g));
        contentValues.put("correct", Integer.toString(this.h));
        contentValues.put("seconds", Integer.toString(this.i));
        contentValues.put("type", this.c);
        contentValues.put("category", this.d);
        contentValues.put("missed", this.e);
        contentValues.put("id_missed", this.f);
        contentValues.put("date", simpleDateFormat.format(this.j));
        try {
            sQLiteDatabase.insert("userDB.quizsessions", null, contentValues);
        } catch (SQLException e2) {
            q.a(e2);
        }
    }
}
