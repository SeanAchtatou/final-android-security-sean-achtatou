package com.cyberandsons.tcmaidtrial.a;

public final class t {
    public static String a(String str) {
        return "INSERT INTO t_zangfupathology (_id, pathology, pathtype) " + str;
    }

    public static String a(int i, String str, String str2, n nVar) {
        String str3;
        boolean z;
        boolean z2 = nVar.z();
        String o = nVar.o();
        boolean B = nVar.B();
        if (i != 1) {
            B = false;
        }
        if (!z2 || o.length() <= 0) {
            str3 = "";
            z = false;
        } else {
            str3 = q.a("zangfupathology", "_id", o);
            z = str3.length() > 0;
        }
        if (z) {
            return "SELECT main.zangfupathology._id, lower(main.zangfupathology.pathology), main.zangfupathology.pathtype FROM main.zangfupathology " + " WHERE (main" + str3 + ") AND main.zangfupathology.pathtype=" + Integer.toString(i) + " AND (" + str + ") " + " UNION " + "SELECT userDB.zangfupathology._id, lower(userDB.zangfupathology.pathology), userDB.zangfupathology.pathtype FROM userDB.zangfupathology " + " WHERE (userDB" + str3 + ") AND userDB.zangfupathology.pathtype=" + Integer.toString(i) + " AND (" + str2 + ") " + (B ? "" : " ORDER BY 2 ");
        }
        return "SELECT main.zangfupathology._id, lower(main.zangfupathology.pathology), main.zangfupathology.pathtype FROM main.zangfupathology " + " WHERE main.zangfupathology.pathtype=" + Integer.toString(i) + " AND (" + str + ") " + " UNION " + "SELECT userDB.zangfupathology._id, lower(userDB.zangfupathology.pathology), userDB.zangfupathology.pathtype FROM userDB.zangfupathology " + " WHERE userDB.zangfupathology.pathtype=" + Integer.toString(i) + " AND (" + str2 + ") " + (B ? "" : " ORDER BY 2 ");
    }

    public static String[] a() {
        return new String[]{"main.zangfupathology.pathology", "main.zangfupathology.symptoms", "main.zangfupathology.txPrinciples", "main.zangfupathology.tongue", "main.zangfupathology.pulse", "main.zangfupathology.points", "main.zangfupathology.formula", "main.zangfupathology.notes"};
    }

    public static String[] b() {
        return new String[]{"userDB.zangfupathology.pathology", "userDB.zangfupathology.symptoms", "userDB.zangfupathology.txPrinciples", "userDB.zangfupathology.tongue", "userDB.zangfupathology.pulse", "userDB.zangfupathology.points", "userDB.zangfupathology.formula", "userDB.zangfupathology.notes"};
    }

    public static String c() {
        return "SELECT main.zangfupathology._id, lower(main.zangfupathology.pathology), main.zangfupathology.pathtype FROM main.zangfupathology " + " UNION " + "SELECT userDB.zangfupathology._id, lower(userDB.zangfupathology.pathology), userDB.zangfupathology.pathtype FROM userDB.zangfupathology " + " WHERE userDB.zangfupathology._id>1000 ORDER BY 2 ";
    }

    public static String a(int i, n nVar) {
        String str;
        boolean z;
        boolean z2 = nVar.z();
        String o = nVar.o();
        boolean B = nVar.B();
        if (i != 1) {
            B = false;
        }
        if (!z2 || o.length() <= 0) {
            str = "";
            z = false;
        } else {
            str = q.a("zangfupathology", "_id", o);
            z = str.length() > 0;
        }
        if (z) {
            return "SELECT main.zangfupathology._id, lower(main.zangfupathology.pathology), main.zangfupathology.pathtype FROM main.zangfupathology " + " WHERE (main" + str + ") AND main.zangfupathology.pathtype=" + Integer.toString(i) + " UNION " + "SELECT userDB.zangfupathology._id, lower(userDB.zangfupathology.pathology), userDB.zangfupathology.pathtype FROM userDB.zangfupathology " + " WHERE (userDB" + str + ") AND userDB.zangfupathology.pathtype=" + Integer.toString(i) + " AND userDB.zangfupathology._id>1000 " + (B ? "" : " ORDER BY 2 ");
        }
        return "SELECT main.zangfupathology._id, lower(main.zangfupathology.pathology), main.zangfupathology.pathtype FROM main.zangfupathology " + " WHERE main.zangfupathology.pathtype=" + Integer.toString(i) + " UNION " + "SELECT userDB.zangfupathology._id, lower(userDB.zangfupathology.pathology), userDB.zangfupathology.pathtype FROM userDB.zangfupathology " + " WHERE userDB.zangfupathology.pathtype=" + Integer.toString(i) + " AND userDB.zangfupathology._id>1000 " + (B ? "" : " ORDER BY 2 ");
    }

    public static String b(String str) {
        StringBuffer stringBuffer = new StringBuffer("(");
        String[] split = str.split(",");
        boolean z = true;
        for (int i = 0; i < split.length; i++) {
            if (z) {
                stringBuffer.append(String.format("%d", Integer.valueOf(Integer.parseInt(split[i]))));
                z = false;
            } else {
                stringBuffer.append(String.format(",%d", Integer.valueOf(Integer.parseInt(split[i]))));
            }
        }
        stringBuffer.append(')');
        return stringBuffer.toString();
    }
}
