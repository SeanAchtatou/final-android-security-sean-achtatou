package com.cyberandsons.tcmaidtrial.a;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private int f165a;

    /* renamed from: b  reason: collision with root package name */
    private String f166b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;

    public e() {
    }

    public e(int i, String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        this.f165a = i;
        this.f166b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
        this.h = str7;
    }

    public final int a() {
        return this.f165a;
    }

    public final String b() {
        return this.f166b;
    }

    public final String c() {
        return this.c;
    }

    public final String d() {
        return this.d;
    }

    public final String e() {
        return this.e;
    }

    public final String f() {
        return this.f;
    }

    public final String g() {
        return this.g;
    }

    public final String h() {
        return this.h;
    }
}
