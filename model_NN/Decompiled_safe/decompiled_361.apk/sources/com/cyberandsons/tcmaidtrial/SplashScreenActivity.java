package com.cyberandsons.tcmaidtrial;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class SplashScreenActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private ImageView f159a;

    public void onCreate(Bundle bundle) {
        int i;
        float f;
        super.onCreate(bundle);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        float f2 = (float) displayMetrics.heightPixels;
        try {
            i = ((WindowManager) getApplicationContext().getSystemService("window")).getDefaultDisplay().getOrientation();
        } catch (Throwable th) {
            Log.e("SSA", "Exception when determining orientation", th);
            i = 0;
        }
        if (i == 1) {
            Log.i("SSA", "Rotation_90");
            setRequestedOrientation(0);
            f = (float) displayMetrics.widthPixels;
        } else if (i == 2) {
            Log.i("SSA", "Rotation_180");
            setRequestedOrientation(1);
            f = f2;
        } else if (i == 3) {
            Log.i("SSA", "Rotation_270");
            setRequestedOrientation(0);
            f = (float) displayMetrics.widthPixels;
        } else {
            Log.i("SSA", "Rotation_0");
            setRequestedOrientation(1);
            f = f2;
        }
        setContentView((int) C0000R.layout.splash);
        this.f159a = (ImageView) findViewById(C0000R.id.a_image);
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), C0000R.drawable.tcmclinicaidsplash);
        float height = f / ((float) decodeResource.getHeight());
        this.f159a.setImageBitmap(dh.a(BitmapFactory.decodeResource(getResources(), C0000R.drawable.tcmclinicaidsplash), (int) (((float) decodeResource.getWidth()) * height), (int) (((float) decodeResource.getHeight()) * height)).getBitmap());
        new o(this).start();
    }
}
