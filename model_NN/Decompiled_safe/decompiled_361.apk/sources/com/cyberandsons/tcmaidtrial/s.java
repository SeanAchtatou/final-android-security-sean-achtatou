package com.cyberandsons.tcmaidtrial;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class s implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TcmAid f1091a;

    s(TcmAid tcmAid) {
        this.f1091a = tcmAid;
    }

    public final void onClick(View view) {
        ((InputMethodManager) this.f1091a.getSystemService("input_method")).toggleSoftInput(2, 2);
    }
}
