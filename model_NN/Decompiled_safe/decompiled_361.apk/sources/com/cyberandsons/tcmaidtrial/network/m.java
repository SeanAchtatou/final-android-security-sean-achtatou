package com.cyberandsons.tcmaidtrial.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Formatter;

public final class m {

    /* renamed from: a  reason: collision with root package name */
    private OutputStream f1002a;

    /* renamed from: b  reason: collision with root package name */
    private d f1003b = new d();
    private boolean c;
    private /* synthetic */ v d;

    public m(v vVar, OutputStream outputStream) {
        this.d = vVar;
        this.f1002a = outputStream;
    }

    public final void a() {
        this.c = true;
    }

    public final OutputStream b() {
        if (this.c) {
            return null;
        }
        return this.f1002a;
    }

    public final d c() {
        return this.f1003b;
    }

    public final void a(int i) {
        if (!this.f1003b.c("Date")) {
            this.f1003b.a("Date", v.a(System.currentTimeMillis()));
        }
        this.f1003b.a("Server", "freeutils-HTTPServer/1.0");
        this.f1002a.write(("HTTP/1.1 " + i + ' ' + v.f1017b[i]).getBytes("ISO8859_1"));
        this.f1002a.write(v.f1016a);
        this.f1003b.a(this.f1002a);
        this.f1002a.flush();
    }

    public final void a(int i, long j, long j2, String str, String str2, long[] jArr) {
        long j3;
        int i2;
        long j4;
        String str3;
        if (jArr != null) {
            this.f1003b.a("Content-Range", "bytes " + jArr[0] + '-' + jArr[1] + '/' + (j >= 0 ? Long.valueOf(j) : "*"));
            j3 = (jArr[1] - jArr[0]) + 1;
            i2 = i == 200 ? 206 : i;
        } else {
            j3 = j;
            i2 = i;
        }
        if (j3 >= 0 && !this.f1003b.c("Content-Length") && !this.f1003b.c("Transfer-Encoding")) {
            this.f1003b.a("Content-Length", Long.toString(j3));
        }
        if (!this.f1003b.c("Content-Type")) {
            if (str2 == null) {
                str3 = "application/octet-stream";
            } else {
                str3 = str2;
            }
            this.f1003b.a("Content-Type", str3);
        }
        if (j2 > 0 && !this.f1003b.c("Last-Modified")) {
            if (j2 > System.currentTimeMillis()) {
                j4 = System.currentTimeMillis();
            } else {
                j4 = j2;
            }
            this.f1003b.a("Last-Modified", v.a(j4));
        }
        if (str != null && !this.f1003b.c("ETag")) {
            this.f1003b.a("ETag", str);
        }
        a(i2);
    }

    public final void a(int i, String str) {
        byte[] bytes = str.getBytes("UTF-8");
        a(i, (long) bytes.length, -1, "\"H" + Integer.toHexString(str.hashCode()) + "\"", "text/html; charset=utf-8", null);
        if (!this.c) {
            this.f1002a.write(bytes);
        }
        this.f1002a.flush();
    }

    public final void b(int i, String str) {
        Formatter formatter = new Formatter();
        formatter.format("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">%n<html>%n<head><title>%d %s</title></head>%n<body><h1>%d %s</h1>%n<p>%s</p>%n</body></html>", Integer.valueOf(i), v.f1017b[i], Integer.valueOf(i), v.f1017b[i], v.f(str));
        a(i, formatter.toString());
    }

    public final void b(int i) {
        b(i, i < 400 ? ":)" : "sorry it didn't work out :(");
    }

    public final void a(InputStream inputStream, long j, long[] jArr) {
        long j2;
        if (!this.c) {
            if (jArr != null) {
                long j3 = jArr[0];
                long j4 = (jArr[1] - jArr[0]) + 1;
                while (j3 > 0) {
                    long skip = inputStream.skip(j3);
                    if (skip == 0) {
                        throw new IOException("can't skip to " + jArr[0]);
                    }
                    j3 -= skip;
                }
                j2 = j4;
            } else {
                j2 = j;
            }
            v.a(inputStream, this.f1002a, j2);
        }
        this.f1002a.flush();
    }

    public final void a(String str) {
        try {
            String aSCIIString = new URI(str).toASCIIString();
            this.f1003b.a("Location", aSCIIString);
            b(301, "Permanently moved to " + aSCIIString);
        } catch (URISyntaxException e) {
            throw new IOException("malformed URL: " + str);
        }
    }
}
