package com.cyberandsons.tcmaidtrial.network;

import java.io.InputStream;
import java.io.OutputStream;

public final class af {
    public static void a(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[4096];
        int read = inputStream.read(bArr, 0, 4096);
        while (read >= 0) {
            outputStream.write(bArr, 0, read);
            read = inputStream.read(bArr, 0, 4096);
        }
    }
}
