package com.cyberandsons.tcmaidtrial.network;

import a.a.a;
import a.a.a.l;
import a.a.a.n;
import a.a.a.u;
import a.a.a.w;
import a.a.a.x;
import a.a.ah;
import a.a.i;
import a.a.j;
import a.a.m;
import a.b.d;
import a.b.t;
import java.util.Date;
import java.util.Properties;

public final class ac extends ah {

    /* renamed from: a  reason: collision with root package name */
    private String f975a = "";

    /* renamed from: b  reason: collision with root package name */
    private String f976b = "";
    private String[] c;
    private String d = "";
    private String e = "465";
    private String f = "465";
    private String g = "smtp.gmail.com";
    private String h = "";
    private String i = "";
    private boolean j = true;
    private boolean k = true;
    private m l = new l();

    public ac() {
        d dVar = (d) t.a();
        dVar.a("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
        dVar.a("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
        dVar.a("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
        dVar.a("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
        dVar.a("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
        t.a(dVar);
    }

    public final boolean b() {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", this.g);
        if (this.k) {
            properties.put("mail.debug", "true");
        }
        if (this.j) {
            properties.put("mail.smtp.auth", "true");
        }
        properties.put("mail.smtp.port", this.e);
        properties.put("mail.smtp.socketFactory.port", this.f);
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.socketFactory.fallback", "false");
        if (this.f975a.equals("") || this.f976b.equals("") || this.c.length <= 0 || this.d.equals("") || this.h.equals("") || this.i.equals("")) {
            return false;
        }
        x xVar = new x(j.a(properties, this));
        xVar.a(new w(this.d));
        w[] wVarArr = new w[this.c.length];
        for (int i2 = 0; i2 < this.c.length; i2++) {
            wVarArr[i2] = new w(this.c[i2]);
        }
        xVar.a(u.f47b, wVarArr);
        xVar.d(this.h);
        xVar.a(new Date());
        n nVar = new n();
        nVar.a(this.i);
        if (this.l.a() > 0) {
            this.l.a(0);
        }
        this.l.a((a) nVar);
        xVar.a(this.l);
        a.a.w.a(xVar);
        return true;
    }

    public final i a() {
        return new i(this.f975a, this.f976b);
    }

    public final void a(String str) {
        this.i = str;
    }

    public final void b(String str) {
        this.f975a = str;
    }

    public final void c(String str) {
        this.f976b = str;
    }

    public final void a(String[] strArr) {
        this.c = strArr;
    }

    public final void d(String str) {
        this.d = str;
    }

    public final void e(String str) {
        this.g = str;
    }

    public final void f(String str) {
        this.h = str;
    }
}
