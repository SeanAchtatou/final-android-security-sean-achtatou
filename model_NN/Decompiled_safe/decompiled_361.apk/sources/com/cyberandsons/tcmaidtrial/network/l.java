package com.cyberandsons.tcmaidtrial.network;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class l extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f1000a;

    /* renamed from: b  reason: collision with root package name */
    private int f1001b;
    private byte[] c = new byte[4096];
    private int d;
    private int e;
    private int f;

    protected l(InputStream inputStream, byte[] bArr) {
        super(inputStream);
        int length = bArr.length;
        if (length <= 0 || length > 70) {
            throw new IllegalArgumentException("invalid boundary length");
        }
        this.f1000a = new byte[(length + 2)];
        byte[] bArr2 = this.f1000a;
        this.f1000a[1] = 45;
        bArr2[0] = 45;
        System.arraycopy(bArr, 0, this.f1000a, 2, length);
        this.f1001b = length + 8;
    }

    public final int read() {
        if (!b()) {
            return -1;
        }
        byte[] bArr = this.c;
        int i = this.d;
        this.d = i + 1;
        return bArr[i];
    }

    public final int read(byte[] bArr, int i, int i2) {
        if (!b()) {
            return -1;
        }
        int min = Math.min(this.e - this.d, i2);
        System.arraycopy(this.c, this.d, bArr, i, min);
        this.d += min;
        return min;
    }

    public final long skip(long j) {
        if (!b()) {
            return 0;
        }
        long min = Math.min((long) (this.e - this.d), j);
        this.d = (int) (((long) this.d) + min);
        return min;
    }

    public final int available() {
        return this.e - this.d;
    }

    public final boolean markSupported() {
        return false;
    }

    public final boolean a() {
        do {
        } while (skip((long) this.c.length) != 0);
        int i = c()[1];
        this.e = i;
        this.d = i;
        this.f -= this.e;
        return b();
    }

    private boolean b() {
        int i;
        if (this.d != this.e) {
            return true;
        }
        if (this.e > 0 && this.f > 0) {
            System.arraycopy(this.c, this.e, this.c, 0, this.f);
        }
        this.e = 0;
        this.d = 0;
        do {
            int read = super.read(this.c, this.f, this.c.length - this.f);
            if (read >= 0) {
                this.f = read + this.f;
            } else if (this.f < this.f1001b) {
                if (this.f == 0) {
                    return false;
                }
                throw new IOException("missing end boundary");
            }
        } while (this.f < this.f1001b);
        int i2 = c()[0];
        this.d = 0;
        if (i2 == -1) {
            i = this.f - this.f1001b;
        } else {
            i = i2;
        }
        this.e = i;
        this.f -= this.e;
        if (i2 != 0) {
            return true;
        }
        return false;
    }

    private int[] c() {
        int i;
        int i2;
        int i3 = 0;
        while (i3 <= this.f - this.f1000a.length) {
            int i4 = 0;
            while (i4 < this.f1000a.length && this.c[i3 + i4] == this.f1000a[i4]) {
                i4++;
            }
            if (i4 == this.f1000a.length) {
                if (this.c[i3 + i4] == 45 && this.c[i3 + i4 + 1] == 45) {
                    i4 += 2;
                }
                if (this.c[i3 + i4] == v.f1016a[0] && this.c[i3 + i4 + 1] == v.f1016a[1]) {
                    if (i3 > 1 && this.c[i3 - 2] == v.f1016a[0] && this.c[i3 - 1] == v.f1016a[1]) {
                        int i5 = i4 + 2;
                        i = i3 - 2;
                        i2 = i5;
                    } else {
                        int i6 = i4;
                        i = i3;
                        i2 = i6;
                    }
                    return new int[]{i, i2 + 2};
                }
                throw new IOException("boundary must end with CRLF");
            }
            i3++;
        }
        return new int[]{-1, -1};
    }
}
