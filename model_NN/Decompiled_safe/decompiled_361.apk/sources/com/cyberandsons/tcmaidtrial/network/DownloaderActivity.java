package com.cyberandsons.tcmaidtrial.network;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;

public class DownloaderActivity extends Activity {
    private static boolean h = false;

    /* renamed from: a  reason: collision with root package name */
    private TextView f970a;

    /* renamed from: b  reason: collision with root package name */
    private TextView f971b;
    private TextView c;
    private final DecimalFormat d = new DecimalFormat("0.00 %");
    private long e;
    /* access modifiers changed from: private */
    public Thread f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public final Handler i = new q(this);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    static /* synthetic */ void a(DownloaderActivity downloaderActivity, int i2) {
        String str;
        downloaderActivity.f970a.setText(downloaderActivity.d.format(((double) i2) / 10000.0d));
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (downloaderActivity.e == 0) {
            downloaderActivity.e = elapsedRealtime;
        }
        long j = elapsedRealtime - downloaderActivity.e;
        String string = downloaderActivity.getString(C0000R.string.download_activity_time_remaining_unknown);
        if (j <= 3000 || i2 <= 100) {
            str = string;
        } else {
            long max = Math.max(0L, ((10000 * j) / ((long) i2)) - j);
            str = max > 86400000 ? Long.toString(((max + 86400000) - 1) / 86400000) + ' ' + downloaderActivity.getString(C0000R.string.download_activity_time_remaining_days) : max > 3600000 ? Long.toString(((max + 3600000) - 1) / 3600000) + ' ' + downloaderActivity.getString(C0000R.string.download_activity_time_remaining_hours) : max > 60000 ? Long.toString(((max + 60000) - 1) / 60000) + ' ' + downloaderActivity.getString(C0000R.string.download_activity_time_remaining_minutes) : Long.toString(((max + 1000) - 1) / 1000) + ' ' + downloaderActivity.getString(C0000R.string.download_activity_time_remaining_seconds);
        }
        downloaderActivity.f971b.setText(str);
    }

    static /* synthetic */ void a(DownloaderActivity downloaderActivity, int i2, int i3) {
        float f2 = ((float) i2) / ((float) i3);
        if (i2 == 1) {
            downloaderActivity.c.setText((int) C0000R.string.download_activity_copy_resources);
        }
        downloaderActivity.f970a.setText(downloaderActivity.d.format((double) f2));
        downloaderActivity.f971b.setText(downloaderActivity.getString(C0000R.string.download_activity_unzipping) + Integer.toString(i2) + " of " + Integer.toString(i3) + '.');
    }

    static /* synthetic */ void a(DownloaderActivity downloaderActivity, String str) {
        Log.e("Downloader", "Download stopped: " + str);
        int indexOf = str.indexOf(10);
        String substring = indexOf >= 0 ? str.substring(0, indexOf) : str;
        AlertDialog create = new AlertDialog.Builder(downloaderActivity).create();
        create.setTitle((int) C0000R.string.download_activity_download_stopped);
        if (!downloaderActivity.g) {
            create.setMessage(substring);
        }
        create.setButton(downloaderActivity.getString(C0000R.string.download_activity_retry), new s(downloaderActivity));
        create.setButton2(downloaderActivity.getString(C0000R.string.download_activity_quit), new r(downloaderActivity));
        try {
            create.show();
        } catch (WindowManager.BadTokenException e2) {
        }
    }

    static /* synthetic */ void a(OutputStream outputStream) {
        if (outputStream != null) {
            try {
                outputStream.close();
            } catch (IOException e2) {
            }
        }
    }

    static /* synthetic */ void f(DownloaderActivity downloaderActivity) {
        Log.i("Downloader", "Download succeeded");
        downloaderActivity.startActivity((Intent) downloaderActivity.getIntent().getParcelableExtra("PreconditionActivityHelper_wrappedIntent"));
        downloaderActivity.finish();
    }

    static /* synthetic */ void g(DownloaderActivity downloaderActivity) {
        downloaderActivity.f970a.setText(downloaderActivity.getString(C0000R.string.download_activity_verifying));
        downloaderActivity.f971b.setText("");
    }

    public static boolean a(Activity activity, String str, String str2, String str3, String str4, String str5) {
        File file = new File(str4);
        if (file.exists()) {
            n b2 = b(file, ".downloadConfig");
            if (b2 != null ? b2.f1004a.equals(str3) : false) {
                Log.i("Downloader", "Versions match, no need to download.");
                return true;
            }
        }
        Intent intent = new Intent();
        intent.setClass(activity, DownloaderActivity.class);
        intent.putExtra("PreconditionActivityHelper_wrappedIntent", activity.getIntent());
        intent.putExtra("DownloaderActivity_custom_text", str);
        intent.putExtra("DownloaderActivity_config_url", str2);
        intent.putExtra("DownloaderActivity_config_version", str3);
        intent.putExtra("DownloaderActivity_data_path", str4);
        intent.putExtra("DownloaderActivity_user_agent", str5);
        activity.startActivity(intent);
        activity.finish();
        return false;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    public static n b(File file, String str) {
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(file, str));
            try {
                n a2 = ab.a(fileInputStream);
                b(fileInputStream);
                return a2;
            } catch (Exception e2) {
                Log.e("Downloader", "Unable to read local config file", e2);
                b(fileInputStream);
                return null;
            } catch (Throwable th) {
                b(fileInputStream);
                throw th;
            }
        } catch (FileNotFoundException e3) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (dh.d) {
            setRequestedOrientation(1);
        }
        Intent intent = getIntent();
        setContentView((int) C0000R.layout.downloader);
        ((TextView) findViewById(C0000R.id.customText)).setText(intent.getStringExtra("DownloaderActivity_custom_text"));
        ((TextView) findViewById(C0000R.id.customSubText)).setText((int) C0000R.string.dl_name_sub);
        this.f970a = (TextView) findViewById(C0000R.id.progress);
        this.f971b = (TextView) findViewById(C0000R.id.time_remaining);
        this.c = (TextView) findViewById(C0000R.id.time_remaining_label);
        ((Button) findViewById(C0000R.id.cancel)).setOnClickListener(new p(this));
        a();
    }

    /* access modifiers changed from: private */
    public void a() {
        this.g = false;
        h = false;
        this.f970a.setText("");
        this.f971b.setText("");
        this.f = new Thread(new c(this), "Downloader");
        this.f.setPriority(4);
        this.f.start();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.g = true;
        h = true;
        this.f.interrupt();
        try {
            this.f.join();
        } catch (InterruptedException e2) {
        }
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public static void b(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e2) {
            }
        }
    }
}
