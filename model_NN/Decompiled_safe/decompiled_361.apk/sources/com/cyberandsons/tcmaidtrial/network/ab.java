package com.cyberandsons.tcmaidtrial.network;

import android.util.Xml;
import java.io.InputStream;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

final class ab extends DefaultHandler {

    /* renamed from: a  reason: collision with root package name */
    private n f974a = new n();

    public static n a(InputStream inputStream) {
        ab abVar = new ab();
        Xml.parse(inputStream, Xml.findEncodingByName("UTF-8"), abVar);
        return abVar.f974a;
    }

    private ab() {
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        if (str2.equals("config")) {
            this.f974a.f1004a = a(attributes, "version");
        } else if (str2.equals("file")) {
            this.f974a.f1005b.add(new f(attributes.getValue("", "src"), a(attributes, "dest"), attributes.getValue("", "md5"), b(attributes, "size")));
        } else if (str2.equals("part")) {
            String a2 = a(attributes, "src");
            String value = attributes.getValue("", "md5");
            long b2 = b(attributes, "size");
            int size = this.f974a.f1005b.size();
            if (size > 0) {
                ((f) this.f974a.f1005b.get(size - 1)).f990a.add(new i(a2, value, b2));
            }
        }
    }

    private static String a(Attributes attributes, String str) {
        String value = attributes.getValue("", str);
        if (value != null) {
            return value;
        }
        throw new SAXException("Expected attribute " + str);
    }

    private static long b(Attributes attributes, String str) {
        String value = attributes.getValue("", str);
        if (value == null) {
            return -1;
        }
        return Long.parseLong(value);
    }
}
