package com.cyberandsons.tcmaidtrial.network;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class w extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    protected long f1018a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f1019b;

    public w(InputStream inputStream, long j, boolean z) {
        super(inputStream);
        long j2;
        if (inputStream == null) {
            throw new NullPointerException("input stream is null");
        }
        if (j < 0) {
            j2 = 0;
        } else {
            j2 = j;
        }
        this.f1018a = j2;
        this.f1019b = z;
    }

    public int read() {
        int read = this.f1018a == 0 ? -1 : this.in.read();
        if (read != -1 || this.f1018a <= 0 || !this.f1019b) {
            this.f1018a = read == -1 ? 0 : this.f1018a - 1;
            return read;
        }
        throw new IOException("unexpected end of stream");
    }

    public int read(byte[] bArr, int i, int i2) {
        int read;
        if (this.f1018a == 0) {
            read = -1;
        } else {
            read = this.in.read(bArr, i, ((long) i2) > this.f1018a ? (int) this.f1018a : i2);
        }
        if (read != -1 || this.f1018a <= 0 || !this.f1019b) {
            this.f1018a = read == -1 ? 0 : this.f1018a - ((long) read);
            return read;
        }
        throw new IOException("unexpected end of stream");
    }

    public long skip(long j) {
        long j2;
        InputStream inputStream = this.in;
        if (j > this.f1018a) {
            j2 = this.f1018a;
        } else {
            j2 = j;
        }
        long skip = inputStream.skip(j2);
        this.f1018a -= skip;
        return skip;
    }

    public int available() {
        int available = this.in.available();
        return ((long) available) > this.f1018a ? (int) this.f1018a : available;
    }

    public boolean markSupported() {
        return false;
    }
}
