package com.cyberandsons.tcmaidtrial.network;

import java.util.ArrayList;
import java.util.Iterator;

final class f {

    /* renamed from: a  reason: collision with root package name */
    ArrayList f990a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    String f991b;

    public f(String str, String str2, String str3, long j) {
        if (str != null) {
            this.f990a.add(new i(str, str3, j));
        }
        this.f991b = str2;
    }

    /* access modifiers changed from: package-private */
    public final long a() {
        Iterator it = this.f990a.iterator();
        long j = 0;
        while (it.hasNext()) {
            i iVar = (i) it.next();
            if (iVar.c > 0) {
                j += iVar.c;
            }
        }
        return j;
    }
}
