package com.cyberandsons.tcmaidtrial.network;

import java.io.IOException;
import java.net.Socket;

final class o implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Socket f1006a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ x f1007b;

    o(x xVar, Socket socket) {
        this.f1007b = xVar;
        this.f1006a = socket;
    }

    public final void run() {
        try {
            this.f1006a.setSoTimeout(10000);
            this.f1007b.f1020a.a(this.f1006a);
            try {
                this.f1006a.close();
            } catch (IOException e) {
            }
        } catch (IOException e2) {
            try {
                this.f1006a.close();
            } catch (IOException e3) {
            }
        } catch (Throwable th) {
            try {
                this.f1006a.close();
            } catch (IOException e4) {
            }
            throw th;
        }
    }
}
