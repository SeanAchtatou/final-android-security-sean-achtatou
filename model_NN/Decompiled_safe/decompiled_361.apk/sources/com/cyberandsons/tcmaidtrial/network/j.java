package com.cyberandsons.tcmaidtrial.network;

import android.util.Log;
import com.cyberandsons.tcmaidtrial.TcmAid;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

final class j extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private BufferedReader f996a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ u f997b;

    j(u uVar) {
        this.f997b = uVar;
    }

    public final void run() {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet();
            httpGet.setURI(new URI(this.f997b.f1014a));
            this.f996a = new BufferedReader(new InputStreamReader(defaultHttpClient.execute(httpGet).getEntity().getContent()));
            StringBuilder sb = new StringBuilder("");
            String property = System.getProperty("line.separator");
            while (true) {
                String readLine = this.f996a.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine + property);
            }
            this.f996a.close();
            String sb2 = sb.toString();
            int indexOf = sb2.indexOf(this.f997b.f1015b) + this.f997b.f1015b.length() + 1;
            int parseInt = Integer.parseInt(sb2.substring(indexOf, sb2.indexOf("<", indexOf)));
            if (parseInt == 301) {
                Log.i("NH:checkUpdate()", "Status = Trial in progress.");
                TcmAid.g = false;
            } else if (parseInt == 302) {
                Log.i("NH:checkUpdate()", "Status = Trial over!");
                TcmAid.g = true;
            }
        } catch (IOException e) {
            Log.i("NH:checkUpdate()", "IOE: " + e.getLocalizedMessage());
        } catch (URISyntaxException e2) {
            Log.i("NH:checkUpdate()", "URISE: " + e2.getLocalizedMessage());
        } catch (StringIndexOutOfBoundsException e3) {
            Log.i("NH:phoneHome()", "SIOOBE: " + e3.getLocalizedMessage());
        }
    }
}
