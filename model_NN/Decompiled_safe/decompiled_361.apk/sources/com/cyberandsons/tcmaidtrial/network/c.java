package com.cyberandsons.tcmaidtrial.network;

import android.content.Intent;
import android.os.Message;
import android.util.Log;
import com.cyberandsons.tcmaidtrial.C0000R;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.DefaultHttpClient;

final class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private DefaultHttpClient f984a;

    /* renamed from: b  reason: collision with root package name */
    private HttpGet f985b;
    private String c;
    private String d;
    private String e;
    private File f;
    private long g;
    private long h;
    private int i;
    private byte[] j;
    private /* synthetic */ DownloaderActivity k;

    /* synthetic */ c(DownloaderActivity downloaderActivity) {
        this(downloaderActivity, (byte) 0);
    }

    private c(DownloaderActivity downloaderActivity, byte b2) {
        this.k = downloaderActivity;
        this.j = new byte[32768];
    }

    public final void run() {
        Intent intent = this.k.getIntent();
        this.c = intent.getStringExtra("DownloaderActivity_config_url");
        this.d = intent.getStringExtra("DownloaderActivity_config_version");
        this.e = intent.getStringExtra("DownloaderActivity_data_path");
        this.f = new File(this.e);
        try {
            this.f984a = new DefaultHttpClient();
            n a2 = a();
            b(a2);
            a(a2);
            c(a2);
            File file = new File(this.f, ".downloadConfig_filtered");
            if (!file.delete()) {
                throw new IOException("could not delete " + file);
            }
            if (!new File(this.f, ".downloadConfig_temp").renameTo(new File(this.f, ".downloadConfig"))) {
                Log.e("Downloader", "renameTo() failed.");
            }
            a(new File(this.k.getString(C0000R.string.download_image_path)));
            this.k.i.sendMessage(Message.obtain(this.k.i, 0));
        } catch (Exception e2) {
            this.k.i.sendMessage(Message.obtain(this.k.i, 1, e2.toString() + "\n" + Log.getStackTraceString(e2)));
        }
    }

    private void a(n nVar) {
        while (true) {
            try {
                this.h = 0;
                Iterator it = nVar.f1005b.iterator();
                while (it.hasNext()) {
                    Iterator it2 = ((f) it.next()).f990a.iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            i iVar = (i) it2.next();
                            if (iVar.c < 0) {
                                String a2 = a(iVar.f994a);
                                Log.i("Downloader", "Head " + a2);
                                HttpResponse execute = this.f984a.execute(new HttpHead(a2));
                                if (execute.getStatusLine().getStatusCode() != 200) {
                                    throw new IOException("Unexpected Http status code " + execute.getStatusLine().getStatusCode());
                                }
                                Header[] headers = execute.getHeaders("Content-Length");
                                iVar.c = headers.length > 0 ? Long.parseLong(headers[0].getValue()) : -1;
                            }
                        }
                    }
                }
                this.g = nVar.a();
                Log.i("Downloader", "Total bytes to download: " + this.g);
                Iterator it3 = nVar.f1005b.iterator();
                while (it3.hasNext()) {
                    a((f) it3.next());
                }
                return;
            } catch (SocketException e2) {
                if (this.k.g) {
                    throw e2;
                }
            } catch (SocketTimeoutException e3) {
                if (this.k.g) {
                    throw e3;
                }
            }
            Log.i("Downloader", "Network connectivity issue, retrying.");
        }
    }

    private void b(n nVar) {
        File file = new File(this.f, ".downloadConfig_filtered");
        if (!file.exists()) {
            File file2 = new File(this.f, ".downloadConfig_temp");
            HashSet hashSet = new HashSet();
            hashSet.add(file2.getCanonicalPath());
            HashMap hashMap = new HashMap();
            Iterator it = nVar.f1005b.iterator();
            while (it.hasNext()) {
                f fVar = (f) it.next();
                hashMap.put(new File(this.f, fVar.f991b).getCanonicalPath(), fVar);
            }
            a(this.f, hashMap, hashSet, false);
            DownloaderActivity.a(new FileOutputStream(file));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.cyberandsons.tcmaidtrial.network.c.a(com.cyberandsons.tcmaidtrial.network.f, boolean):boolean
     arg types: [com.cyberandsons.tcmaidtrial.network.f, int]
     candidates:
      com.cyberandsons.tcmaidtrial.network.c.a(java.lang.String, java.lang.String):java.io.File
      com.cyberandsons.tcmaidtrial.network.c.a(java.lang.String, boolean):java.io.FileOutputStream
      com.cyberandsons.tcmaidtrial.network.c.a(java.security.MessageDigest, int):void
      com.cyberandsons.tcmaidtrial.network.c.a(com.cyberandsons.tcmaidtrial.network.f, boolean):boolean */
    private boolean a(File file, HashMap hashMap, HashSet hashSet, boolean z) {
        boolean z2;
        boolean a2;
        if (file.isDirectory()) {
            boolean z3 = true;
            for (File a3 : file.listFiles()) {
                z3 &= a(a3, hashMap, hashSet, true);
            }
            z2 = z3;
        } else {
            z2 = true;
        }
        if (z) {
            if (!file.isDirectory()) {
                String canonicalPath = file.getCanonicalPath();
                if (hashSet.contains(canonicalPath)) {
                    a2 = true;
                } else {
                    f fVar = (f) hashMap.get(canonicalPath);
                    a2 = fVar == null ? false : a(fVar, false);
                }
                if (!a2) {
                    return file.delete() & z2;
                }
            } else if (file.listFiles().length == 0) {
                return file.delete() & z2;
            }
        }
        return z2;
    }

    /* JADX INFO: finally extract failed */
    private n a() {
        n nVar;
        if (this.f.exists()) {
            nVar = DownloaderActivity.b(this.f, ".downloadConfig_temp");
            if (nVar == null || !this.d.equals(nVar.f1004a)) {
                if (nVar == null) {
                    Log.i("Downloader", "Couldn't find local config.");
                } else {
                    Log.i("Downloader", "Local version out of sync. Wanted " + this.d + " but have " + nVar.f1004a);
                }
                nVar = null;
            }
        } else {
            Log.i("Downloader", "Creating directory " + this.e);
            if (!this.f.mkdirs()) {
                Log.e("Downloader", "mkdirs() failed.");
            }
            if (!this.f.mkdir()) {
                Log.e("Downloader", "mkdir() failed.");
            }
            if (!this.f.exists()) {
                throw new aa(this.k, "Could not create the directory " + this.e);
            }
            nVar = null;
        }
        if (nVar == null) {
            FileInputStream fileInputStream = new FileInputStream(a(this.c, ".downloadConfig_temp"));
            try {
                nVar = ab.a(fileInputStream);
                DownloaderActivity.b(fileInputStream);
                if (!nVar.f1004a.equals(this.d)) {
                    throw new aa(this.k, "Configuration file version mismatch. Expected " + this.d + " received " + nVar.f1004a);
                }
            } catch (Throwable th) {
                DownloaderActivity.b(fileInputStream);
                throw th;
            }
        }
        return nVar;
    }

    private void a(f fVar) {
        FileOutputStream fileOutputStream;
        FileOutputStream fileOutputStream2;
        MessageDigest messageDigest;
        FileOutputStream fileOutputStream3;
        FileInputStream fileInputStream;
        boolean z = false;
        File file = new File(this.f, fVar.f991b);
        long j2 = 0;
        if (file.exists() && file.isFile()) {
            z = true;
            j2 = file.length();
            this.h = this.h + j2;
        }
        boolean z2 = z;
        long j3 = j2;
        try {
            Iterator it = fVar.f990a.iterator();
            long j4 = 0;
            long j5 = j3;
            fileOutputStream = null;
            while (it.hasNext()) {
                try {
                    i iVar = (i) it.next();
                    if (iVar.c > j5 || iVar.c == 0) {
                        if (iVar.f995b != null) {
                            MessageDigest b2 = b();
                            if (j5 > 0) {
                                File file2 = new File(this.f, fVar.f991b);
                                File parentFile = file2.getParentFile();
                                if (!parentFile.exists() && !parentFile.mkdirs()) {
                                    Log.e("Downloader", "mkdirs() failed.");
                                }
                                if (!parentFile.exists()) {
                                    throw new aa(this.k, "Could not create directory " + parentFile.toString());
                                }
                                fileInputStream = new FileInputStream(file2);
                                fileInputStream.skip(j4);
                                a(fileInputStream, j5, b2);
                                DownloaderActivity.b(fileInputStream);
                                messageDigest = b2;
                            } else {
                                messageDigest = b2;
                            }
                        } else {
                            messageDigest = null;
                        }
                        if (fileOutputStream == null) {
                            fileOutputStream3 = a(fVar.f991b, z2);
                        } else {
                            fileOutputStream3 = fileOutputStream;
                        }
                        try {
                            a(iVar.f994a, fileOutputStream3, j5, iVar.c, messageDigest);
                            if (messageDigest != null) {
                                String a2 = a(messageDigest);
                                if (!a2.equalsIgnoreCase(iVar.f995b)) {
                                    Log.e("Downloader", "web MD5 checksums don't match. " + iVar.f994a + "\nExpected " + iVar.f995b + "\n     got " + a2);
                                    DownloaderActivity.a(fileOutputStream3);
                                    if (!file.delete()) {
                                        Log.e("Downloader", "web MD5 checksum delete() failed.");
                                    }
                                    throw new aa(this.k, "Received bad data from web server");
                                }
                                Log.i("Downloader", "web MD5 checksum matches.");
                            }
                            fileOutputStream2 = fileOutputStream3;
                        } catch (Throwable th) {
                            th = th;
                            fileOutputStream = fileOutputStream3;
                            DownloaderActivity.a(fileOutputStream);
                            throw th;
                        }
                    } else {
                        fileOutputStream2 = fileOutputStream;
                    }
                    try {
                        long min = j5 - Math.min(j5, iVar.c);
                        j4 = iVar.c + j4;
                        j5 = min;
                        fileOutputStream = fileOutputStream2;
                    } catch (Throwable th2) {
                        Throwable th3 = th2;
                        fileOutputStream = fileOutputStream2;
                        th = th3;
                        DownloaderActivity.a(fileOutputStream);
                        throw th;
                    }
                } catch (Throwable th4) {
                    th = th4;
                    DownloaderActivity.a(fileOutputStream);
                    throw th;
                }
            }
            DownloaderActivity.a(fileOutputStream);
        } catch (Throwable th5) {
            th = th5;
            fileOutputStream = null;
            DownloaderActivity.a(fileOutputStream);
            throw th;
        }
    }

    private void a(File file) {
        BufferedInputStream bufferedInputStream;
        BufferedOutputStream bufferedOutputStream;
        String str = file.getParent() + '/';
        try {
            ZipFile zipFile = new ZipFile(file);
            int size = zipFile.size();
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            int i2 = 1;
            while (entries.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                if (zipEntry.isDirectory()) {
                    b(new File(str, zipEntry.getName()));
                } else {
                    File file2 = new File(str, zipEntry.getName());
                    if (!file2.getParentFile().exists()) {
                        b(file2.getParentFile());
                    }
                    Log.i("unzipEntry", "Extracting: " + zipEntry);
                    bufferedInputStream = new BufferedInputStream(zipFile.getInputStream(zipEntry), 8192);
                    bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file2), 8192);
                    af.a(bufferedInputStream, bufferedOutputStream);
                    bufferedOutputStream.close();
                    bufferedInputStream.close();
                }
                this.k.i.sendMessage(Message.obtain(this.k.i, 4, i2, size));
                i2++;
            }
        } catch (ZipException e2) {
            Log.e("unzipFile", "Error while extracting file " + file);
        } catch (IOException e3) {
            Log.e("unzipFile", "Error while extracting file " + file);
        } catch (Throwable th) {
            bufferedOutputStream.close();
            bufferedInputStream.close();
            throw th;
        }
    }

    private static void b(File file) {
        Log.i("createDir", "Creating dir " + file.getName());
        if (!file.mkdirs()) {
            throw new RuntimeException("Can not create dir " + file);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.cyberandsons.tcmaidtrial.network.c.a(com.cyberandsons.tcmaidtrial.network.f, boolean):boolean
     arg types: [com.cyberandsons.tcmaidtrial.network.f, int]
     candidates:
      com.cyberandsons.tcmaidtrial.network.c.a(java.lang.String, java.lang.String):java.io.File
      com.cyberandsons.tcmaidtrial.network.c.a(java.lang.String, boolean):java.io.FileOutputStream
      com.cyberandsons.tcmaidtrial.network.c.a(java.security.MessageDigest, int):void
      com.cyberandsons.tcmaidtrial.network.c.a(com.cyberandsons.tcmaidtrial.network.f, boolean):boolean */
    private void c(n nVar) {
        String str;
        Log.i("Downloader", "Verifying...");
        String str2 = null;
        Iterator it = nVar.f1005b.iterator();
        while (true) {
            str = str2;
            if (!it.hasNext()) {
                break;
            }
            f fVar = (f) it.next();
            if (a(fVar, true)) {
                str2 = str;
            } else if (str == null) {
                str2 = fVar.f991b;
            } else {
                str2 = str + ' ' + fVar.f991b;
            }
        }
        if (str != null) {
            throw new aa(this.k, "Possible bad SD-Card. MD5 sum incorrect for file(s) " + str);
        }
    }

    /* JADX INFO: finally extract failed */
    private boolean a(f fVar, boolean z) {
        Log.i("Downloader", "verifying " + fVar.f991b);
        this.k.i.sendMessage(Message.obtain(this.k.i, 3));
        File file = new File(this.f, fVar.f991b);
        if (!file.exists()) {
            Log.e("Downloader", "File does not exist: " + file.toString());
            return false;
        }
        long a2 = fVar.a();
        long length = file.length();
        if (a2 != length) {
            Log.e("Downloader", "Length doesn't match. Expected " + a2 + " got " + length);
            if (z) {
                if (!file.delete()) {
                    Log.e("Downloader", "delete() failed.");
                }
                return false;
            }
        }
        FileInputStream fileInputStream = new FileInputStream(file);
        try {
            Iterator it = fVar.f990a.iterator();
            while (it.hasNext()) {
                i iVar = (i) it.next();
                if (iVar.f995b != null) {
                    MessageDigest b2 = b();
                    a(fileInputStream, iVar.c, b2);
                    String a3 = a(b2);
                    if (!a3.equalsIgnoreCase(iVar.f995b)) {
                        Log.e("Downloader", "MD5 checksums don't match. " + iVar.f994a + " Expected " + iVar.f995b + " got " + a3);
                        if (z) {
                            DownloaderActivity.b(fileInputStream);
                            if (!file.delete()) {
                                Log.e("Downloader", "delete() failed.");
                            }
                        }
                        DownloaderActivity.b(fileInputStream);
                        return false;
                    }
                }
            }
            DownloaderActivity.b(fileInputStream);
            return true;
        } catch (Throwable th) {
            DownloaderActivity.b(fileInputStream);
            throw th;
        }
    }

    private void a(FileInputStream fileInputStream, long j2, MessageDigest messageDigest) {
        int read;
        long j3 = j2;
        while (j3 > 0 && (read = fileInputStream.read(this.j, 0, (int) Math.min((long) this.j.length, j3))) >= 0) {
            a(messageDigest, read);
            j3 -= (long) read;
        }
    }

    private MessageDigest b() {
        try {
            return MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e2) {
            throw new aa(this.k, "Couldn't create MD5 digest");
        }
    }

    private void a(MessageDigest messageDigest, int i2) {
        if (i2 == this.j.length) {
            messageDigest.update(this.j);
            return;
        }
        byte[] bArr = new byte[i2];
        System.arraycopy(this.j, 0, bArr, 0, i2);
        messageDigest.update(bArr);
    }

    private static String a(MessageDigest messageDigest) {
        StringBuilder sb = new StringBuilder();
        for (byte b2 : messageDigest.digest()) {
            sb.append(Integer.toHexString((b2 >> 4) & 15));
            sb.append(Integer.toHexString(b2 & 15));
        }
        return sb.toString();
    }

    private String a(String str) {
        return new URL(new URL(this.c), str).toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.cyberandsons.tcmaidtrial.network.c.a(java.lang.String, boolean):java.io.FileOutputStream
     arg types: [java.lang.String, int]
     candidates:
      com.cyberandsons.tcmaidtrial.network.c.a(java.lang.String, java.lang.String):java.io.File
      com.cyberandsons.tcmaidtrial.network.c.a(java.security.MessageDigest, int):void
      com.cyberandsons.tcmaidtrial.network.c.a(com.cyberandsons.tcmaidtrial.network.f, boolean):boolean
      com.cyberandsons.tcmaidtrial.network.c.a(java.lang.String, boolean):java.io.FileOutputStream */
    private File a(String str, String str2) {
        File file = new File(this.f, str2);
        FileOutputStream a2 = a(str2, false);
        try {
            a(str, a2, 0, -1, null);
            return file;
        } finally {
            a2.close();
        }
    }

    private void a(String str, FileOutputStream fileOutputStream, long j2, long j3, MessageDigest messageDigest) {
        boolean z = j3 >= 0;
        if (j2 < 0) {
            throw new IllegalArgumentException("Negative startOffset:" + j2);
        } else if (!z || j2 <= j3) {
            String a2 = a(str);
            Log.i("Downloader", "Get " + a2);
            this.f985b = new HttpGet(a2);
            int i2 = 200;
            if (j2 > 0) {
                String str2 = "bytes=" + j2 + '-';
                if (j3 >= 0) {
                    str2 = str2 + (j3 - 1);
                }
                Log.i("Downloader", "requesting byte range " + str2);
                this.f985b.addHeader("Range", str2);
                i2 = 206;
            }
            HttpResponse execute = this.f984a.execute(this.f985b);
            long j4 = 0;
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode != i2) {
                if (statusCode == 200 && i2 == 206) {
                    Log.i("Downloader", "Byte range request ignored");
                    j4 = j2;
                } else {
                    throw new IOException("Unexpected Http status code " + statusCode + " expected " + i2);
                }
            }
            InputStream content = execute.getEntity().getContent();
            if (j4 > 0 && content.skip(j4) != j4) {
                Log.e("Downloader", "skip issue.");
            }
            try {
                long a3 = a(content, fileOutputStream, messageDigest);
                if (z) {
                    long j5 = j3 - j2;
                    if (j5 != a3) {
                        Log.e("Downloader", "Bad file transfer from server: " + str + " Expected " + j5 + " Received " + a3);
                        throw new aa(this.k, "Incorrect number of bytes received from server");
                    }
                }
            } finally {
                content.close();
                this.f985b = null;
            }
        } else {
            throw new IllegalArgumentException("startOffset > expectedLength" + j2 + ' ' + j3);
        }
    }

    private FileOutputStream a(String str, boolean z) {
        File file = new File(this.f, str);
        File parentFile = file.getParentFile();
        if (!parentFile.exists() && !parentFile.mkdirs()) {
            Log.e("Downloader", "mkdirs() failed.");
        }
        if (parentFile.exists()) {
            return new FileOutputStream(file, z);
        }
        throw new aa(this.k, "Could not create directory " + parentFile.toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    private long a(InputStream inputStream, FileOutputStream fileOutputStream, MessageDigest messageDigest) {
        long j2 = 0;
        while (!Thread.interrupted()) {
            int read = inputStream.read(this.j);
            if (read < 0) {
                return j2;
            }
            if (messageDigest != null) {
                a(messageDigest, read);
            }
            j2 += (long) read;
            fileOutputStream.write(this.j, 0, read);
            this.h += (long) read;
            int min = (int) Math.min(this.g, (this.h * 10000) / Math.max(1L, this.g));
            if (min != this.i) {
                this.i = min;
                this.k.i.sendMessage(Message.obtain(this.k.i, 2, min, 0));
            }
        }
        Log.i("Downloader", "downloader thread interrupted.");
        this.f985b.abort();
        throw new aa(this.k, "Thread interrupted");
    }
}
