package com.cyberandsons.tcmaidtrial.network;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

public final class y implements Iterator {

    /* renamed from: a  reason: collision with root package name */
    private l f1021a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f1022b;

    public y(ae aeVar) {
        Map e = aeVar.e().e("Content-Type");
        if (!e.containsKey("multipart/form-data")) {
            throw new IllegalArgumentException("given request is not of type multipart/form-data");
        }
        String str = (String) e.get("boundary");
        if (str == null) {
            throw new IllegalArgumentException("Content-Type is missing boundry");
        }
        this.f1021a = new l(aeVar.d(), str.getBytes("US-ASCII"));
    }

    public final boolean hasNext() {
        try {
            if (!this.f1022b) {
                boolean a2 = this.f1021a.a();
                this.f1022b = a2;
                return a2;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: a */
    public final z next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        this.f1022b = false;
        z zVar = new z();
        try {
            zVar.c = v.b(this.f1021a);
            zVar.f1023a = (String) zVar.c.e("Content-Disposition").get("name");
            zVar.f1024b = (String) zVar.c.e("Content-Disposition").get("filename");
            zVar.d = this.f1021a;
            return zVar;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
