package com.cyberandsons.tcmaidtrial.network;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import java.util.UUID;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public final class u {

    /* renamed from: a  reason: collision with root package name */
    String f1014a = "";

    /* renamed from: b  reason: collision with root package name */
    String f1015b = "";
    private boolean c = false;
    private boolean d = false;
    private String e = "";
    private Context f;
    private Thread g = new j(this);

    public u(Context context) {
        this.f = context;
    }

    private String b() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.f.getSystemService("phone");
            return new UUID((long) ("" + Settings.Secure.getString(this.f.getContentResolver(), "android_id")).hashCode(), (((long) ("" + telephonyManager.getDeviceId()).hashCode()) << 32) | ((long) ("" + telephonyManager.getSimSerialNumber()).hashCode())).toString();
        } catch (Exception e2) {
            Log.d("getUIDTelephony()", e2.getLocalizedMessage());
            return "";
        }
    }

    private String c() {
        try {
            return ((WifiManager) this.f.getSystemService("wifi")).getConnectionInfo().getMacAddress();
        } catch (Exception e2) {
            Log.d("NH:getUIDWifi()", e2.getLocalizedMessage());
            return "";
        }
    }

    public final String a() {
        String b2 = b();
        if (b2 == null || b2.length() == 0) {
            b2 = c();
        }
        if (b2 == null || b2.length() == 0) {
            return Settings.Secure.getString(this.f.getContentResolver(), "android_id");
        }
        return b2;
    }

    public final void a(boolean z) {
        String a2 = a();
        if (TcmAid.d == -1 && TcmAid.c) {
            TcmAid.d = 0;
        }
        this.e = String.format("?UUID=%s&App=%s&Key=%d&Name=%s&Version=%s&Locale=%s&VersionOS=%s&Model=%s", a2, this.f.getString(C0000R.string.reg_app_name), 0, Build.USER, this.f.getString(C0000R.string.versionName), Locale.getDefault().toString(), Build.VERSION.RELEASE, Build.MODEL);
        if (this.c) {
            this.e += "&Update=yes";
        }
        if (this.d) {
            this.e += "&debug=1";
        }
        this.f1014a = this.f.getString(C0000R.string.urlpath) + this.e;
        this.f1015b = this.f.getString(C0000R.string.statusstring);
        if (z) {
            this.g.start();
            return;
        }
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet();
            httpGet.setURI(new URI(this.f1014a));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(defaultHttpClient.execute(httpGet).getEntity().getContent()));
            StringBuilder sb = new StringBuilder("");
            String property = System.getProperty("line.separator");
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine + property);
            }
            bufferedReader.close();
            String sb2 = sb.toString();
            int indexOf = sb2.indexOf(this.f1015b) + this.f1015b.length() + 1;
            int parseInt = Integer.parseInt(sb2.substring(indexOf, sb2.indexOf("<", indexOf)));
            if (parseInt == 301) {
                Log.i("NH:phoneHome()", "Status = Trial in progress.");
                TcmAid.g = false;
            } else if (parseInt == 302) {
                Log.i("NH:phoneHome()", "Status = Trial over!");
                TcmAid.g = true;
            }
        } catch (IOException e2) {
            Log.i("NH:phoneHome()", "IOE: " + e2.getLocalizedMessage());
        } catch (URISyntaxException e3) {
            Log.i("NH:phoneHome()", "URISE: " + e3.getLocalizedMessage());
        } catch (StringIndexOutOfBoundsException e4) {
            Log.i("NH:phoneHome()", "SIOOBE: " + e4.getLocalizedMessage());
        }
    }
}
