package com.cyberandsons.tcmaidtrial.network;

public final class t {

    /* renamed from: a  reason: collision with root package name */
    private String f1012a;

    /* renamed from: b  reason: collision with root package name */
    private String f1013b;

    public t(String str, String str2) {
        this.f1012a = str.trim();
        this.f1013b = str2.trim();
        if (this.f1012a.length() == 0) {
            throw new IllegalArgumentException("name cannot be empty");
        }
    }

    public final String a() {
        return this.f1012a;
    }

    public final String b() {
        return this.f1013b;
    }
}
