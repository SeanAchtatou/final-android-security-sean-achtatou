package com.cyberandsons.tcmaidtrial.network;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private String f988a = null;

    /* renamed from: b  reason: collision with root package name */
    private Set f989b = new CopyOnWriteArraySet();
    private Map c = new ConcurrentHashMap();
    private volatile String d = "index.html";
    private volatile boolean e;

    public final String a() {
        return this.f988a;
    }

    public final Set b() {
        return Collections.unmodifiableSet(this.f989b);
    }

    public final String c() {
        return this.d;
    }

    public final void d() {
        this.e = true;
    }

    public final boolean e() {
        return this.e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.cyberandsons.tcmaidtrial.network.v.b(java.lang.String, char):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.cyberandsons.tcmaidtrial.network.v.b(com.cyberandsons.tcmaidtrial.network.ae, com.cyberandsons.tcmaidtrial.network.m):void
      com.cyberandsons.tcmaidtrial.network.v.b(java.lang.String, char):java.lang.String */
    public final g a(String str) {
        String b2 = v.b(str, '/');
        g gVar = null;
        while (gVar == null && b2 != null) {
            gVar = (g) this.c.get(b2);
            b2 = v.c(b2);
        }
        return gVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.cyberandsons.tcmaidtrial.network.v.b(java.lang.String, char):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.cyberandsons.tcmaidtrial.network.v.b(com.cyberandsons.tcmaidtrial.network.ae, com.cyberandsons.tcmaidtrial.network.m):void
      com.cyberandsons.tcmaidtrial.network.v.b(java.lang.String, char):java.lang.String */
    public final void a(String str, g gVar) {
        if (str == null || !str.startsWith("/")) {
            throw new IllegalArgumentException("invalid path: " + str);
        }
        this.c.put(v.b(str, '/'), gVar);
    }
}
