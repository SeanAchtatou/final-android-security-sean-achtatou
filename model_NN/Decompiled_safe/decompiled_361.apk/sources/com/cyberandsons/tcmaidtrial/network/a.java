package com.cyberandsons.tcmaidtrial.network;

import java.io.IOException;
import java.io.InputStream;

public final class a extends w {

    /* renamed from: b  reason: collision with root package name */
    private d f972b;
    private boolean c;

    public a(InputStream inputStream, d dVar) {
        super(inputStream, 0, true);
        this.f972b = dVar;
    }

    public final int read() {
        if (this.f1018a > 0 || a() >= 0) {
            return super.read();
        }
        return -1;
    }

    public final int read(byte[] bArr, int i, int i2) {
        if (this.f1018a > 0 || a() >= 0) {
            return super.read(bArr, i, i2);
        }
        return -1;
    }

    private long a() {
        if (this.f1018a == 0) {
            if (!this.c) {
                this.c = true;
            } else if (v.a(this.in).length() > 0) {
                throw new IOException("chunk data must end with CRLF");
            }
            this.f1018a = a(v.a(this.in));
            if (this.f1018a == 0) {
                this.f1018a = -1;
                d b2 = v.b(this.in);
                if (this.f972b != null) {
                    this.f972b.a(b2);
                }
            }
        }
        return this.f1018a;
    }

    private static long a(String str) {
        String str2;
        int indexOf = str.indexOf(59);
        if (indexOf >= 0) {
            str2 = str.substring(0, indexOf);
        } else {
            str2 = str;
        }
        try {
            return v.a(str2, 16);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("invalid chunk size line: \"" + str2 + "\"");
        }
    }
}
