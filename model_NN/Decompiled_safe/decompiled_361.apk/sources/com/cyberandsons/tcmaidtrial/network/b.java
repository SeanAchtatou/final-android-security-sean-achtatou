package com.cyberandsons.tcmaidtrial.network;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public final class b extends FilterOutputStream {

    /* renamed from: a  reason: collision with root package name */
    private int f983a;

    public b(OutputStream outputStream) {
        super(outputStream);
        if (outputStream == null) {
            throw new NullPointerException("output stream is null");
        }
    }

    private void a(long j) {
        if (j < 0) {
            throw new IllegalArgumentException("invalid size: " + j);
        }
        if (this.f983a > 0) {
            this.out.write(v.f1016a);
        } else if (this.f983a == 0) {
            this.f983a = 1;
        } else if (this.f983a < 0) {
            throw new IOException("chunked stream has already ended");
        }
        this.out.write(Long.toHexString(j).getBytes("ISO8859_1"));
        this.out.write(v.f1016a);
    }

    public final void a() {
        a(0);
        this.out.write(v.f1016a);
        this.f983a = -1;
    }

    public final void a(byte[] bArr, int i) {
        if (i > 0) {
            a((long) i);
        }
        write(bArr, 0, i);
    }
}
