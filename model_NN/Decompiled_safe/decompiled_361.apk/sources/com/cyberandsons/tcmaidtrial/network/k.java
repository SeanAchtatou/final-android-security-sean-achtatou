package com.cyberandsons.tcmaidtrial.network;

import a.a.a.w;
import a.a.a.x;
import a.a.ag;
import a.a.ah;
import a.a.i;
import a.a.j;
import a.a.q;
import a.b.a;
import java.util.Properties;

public final class k extends ah {

    /* renamed from: a  reason: collision with root package name */
    private String f998a;

    /* renamed from: b  reason: collision with root package name */
    private String f999b;
    private String c;
    private j d;

    public k(String str, String str2, String str3) {
        this.f999b = str;
        this.c = str2;
        this.f998a = str3;
        Properties properties = new Properties();
        properties.setProperty("mail.transport.protocol", "smtp");
        properties.setProperty("mail.host", this.f998a);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.socketFactory.fallback", "false");
        properties.setProperty("mail.smtp.quitwait", "false");
        this.d = j.b(properties, this);
    }

    /* access modifiers changed from: protected */
    public final i a() {
        return new i(this.f999b, this.c);
    }

    public final synchronized void a(String str, String str2, String str3, String str4) {
        x xVar = new x(this.d);
        a aVar = new a(new ad(this, str2.getBytes(), "text/plain"));
        xVar.b(new w(str3));
        xVar.d(str);
        xVar.a(aVar);
        if (str4.indexOf(44) > 0) {
            xVar.a(ag.f47b, w.a(str4));
        } else {
            xVar.a(ag.f47b, new q[]{new w(str4)});
        }
        a.a.w.a(xVar);
    }
}
