package com.cyberandsons.tcmaidtrial.network;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Formatter;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public final class v {

    /* renamed from: a  reason: collision with root package name */
    public static final byte[] f1016a = {13, 10};

    /* renamed from: b  reason: collision with root package name */
    protected static final String[] f1017b;
    private static String[] f = {"EEE, dd MMM yyyy HH:mm:ss Z", "EEEE, dd-MMM-yy HH:mm:ss Z", "EEE MMM d HH:mm:ss yyyy"};
    private static Map g = new ConcurrentHashMap();
    protected volatile int c;
    protected volatile Executor d;
    protected volatile ServerSocket e;
    private Map h;

    static {
        String[] strArr = new String[600];
        f1017b = strArr;
        Arrays.fill(strArr, "Unknown Status");
        f1017b[100] = "Continue";
        f1017b[200] = "OK";
        f1017b[204] = "No Content";
        f1017b[206] = "Partial Content";
        f1017b[301] = "Moved Permanently";
        f1017b[302] = "Found";
        f1017b[304] = "Not Modified";
        f1017b[307] = "Temporary Redirect";
        f1017b[400] = "Bad Request";
        f1017b[401] = "Unauthorized";
        f1017b[403] = "Forbidden";
        f1017b[404] = "Not Found";
        f1017b[412] = "Precondition Failed";
        f1017b[413] = "Request Entity Too Large";
        f1017b[414] = "Request-URI Too Large";
        f1017b[416] = "Requested Range Not Satisfiable";
        f1017b[417] = "Expectation Failed";
        f1017b[500] = "Internal Server Error";
        f1017b[501] = "Not Implemented";
        f1017b[502] = "Bad Gateway";
        f1017b[503] = "Service Unavailable";
        f1017b[504] = "Gateway Time-out";
        a("application/java-archive", "jar");
        a("application/javascript", "js");
        a("application/json", "json");
        a("application/msword", "doc");
        a("application/octet-stream", "exe");
        a("application/pdf", "pdf");
        a("application/vnd.ms-excel", "xls");
        a("application/vnd.ms-powerpoint", "ppt");
        a("application/x-compressed", "tgz");
        a("application/x-gzip", "gz");
        a("application/x-tar", "tar");
        a("application/xhtml+xml", "xhtml");
        a("application/zip", "zip");
        a("audio/mpeg", "mp3");
        a("image/gif", "gif");
        a("image/jpeg", "jpg", "jpeg");
        a("image/png", "png");
        a("image/svg+xml", "svg");
        a("image/x-icon", "ico");
        a("text/css", "css");
        a("text/html; charset=utf-8", "htm", "html");
        a("text/plain", "txt", "text", "log");
        a("text/xml", "xml");
    }

    public v(int i) {
        this.h = new ConcurrentHashMap();
        this.c = i;
        e eVar = new e();
        String a2 = eVar.a();
        this.h.put(a2 == null ? "~DEFAULT~" : a2, eVar);
    }

    public v() {
        this(80);
    }

    public final e a(String str) {
        return (e) this.h.get(str == null ? "~DEFAULT~" : str);
    }

    public final synchronized void a() {
        if (this.e == null) {
            this.e = new ServerSocket(this.c);
            if (this.d == null) {
                this.d = Executors.newCachedThreadPool();
            }
            for (e eVar : Collections.unmodifiableSet(new HashSet(this.h.values()))) {
                for (String put : eVar.b()) {
                    this.h.put(put, eVar);
                }
            }
            new x(this).start();
        }
    }

    public final synchronized void b() {
        try {
            if (this.e != null) {
                this.e.close();
            }
        } catch (IOException e2) {
        }
        this.e = null;
    }

    /* access modifiers changed from: protected */
    public final void a(Socket socket) {
        ae aeVar;
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(socket.getOutputStream(), 4096);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(socket.getInputStream(), 4096);
        do {
            m mVar = new m(this, bufferedOutputStream);
            try {
                aeVar = new ae(this, bufferedInputStream);
                try {
                    a(aeVar, mVar);
                    bufferedOutputStream.flush();
                    aeVar.h();
                } catch (InterruptedIOException e2) {
                    return;
                } catch (IOException e3) {
                    mVar.b(500, "error processing request: " + e3.getMessage());
                    return;
                }
            } catch (InterruptedIOException e4) {
                return;
            } catch (IOException e5) {
                mVar.b(400, "invalid request: " + e5.getMessage());
                return;
            }
        } while (!"close".equalsIgnoreCase(aeVar.e().a("Connection")));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, char):java.lang.String[]
     arg types: [java.lang.String, int]
     candidates:
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, int):long
      com.cyberandsons.tcmaidtrial.network.v.a(java.io.File, java.lang.String):java.lang.String
      com.cyberandsons.tcmaidtrial.network.v.a(com.cyberandsons.tcmaidtrial.network.ae, com.cyberandsons.tcmaidtrial.network.m):void
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, java.lang.String[]):void
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String[], java.lang.String):boolean
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, long):long[]
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, char):java.lang.String[] */
    private void a(ae aeVar, m mVar) {
        d e2 = aeVar.e();
        String c2 = aeVar.c();
        if (c2.equals("HTTP/1.1")) {
            if (!e2.c("Host")) {
                mVar.b(400, "missing required Host header");
                return;
            }
            String a2 = e2.a("Expect");
            if (a2 != null) {
                if (a2.equalsIgnoreCase("100-continue")) {
                    new m(this, mVar.b()).a(100);
                } else {
                    mVar.b(417);
                    return;
                }
            }
        } else if (c2.equals("HTTP/1.0") || c2.equals("HTTP/0.9")) {
            for (String d2 : a(e2.a("Connection"), ',')) {
                e2.d(d2);
            }
        } else {
            mVar.b(400, "unknown version: " + c2);
            return;
        }
        String a3 = aeVar.a();
        if (a3.equals("GET") || a3.equals("POST")) {
            c(aeVar, mVar);
        } else if (a3.equals("HEAD")) {
            mVar.a();
            c(aeVar, mVar);
        } else if (a3.equals("OPTIONS")) {
            mVar.c().a("Allow", "GET, HEAD, POST, OPTIONS, TRACE");
            mVar.c().a("Content-Length", "0");
            mVar.a(200);
        } else if (a3.equals("TRACE")) {
            b(aeVar, mVar);
        } else {
            mVar.b(501, "unsupported method: " + a3);
        }
    }

    private static void b(ae aeVar, m mVar) {
        mVar.c().a("Content-Type", "message/http");
        mVar.c().a("Transfer-Encoding", "chunked");
        mVar.a(200);
        b bVar = new b(mVar.b());
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(("TRACE " + aeVar.b() + ' ' + aeVar.c()).getBytes("ISO8859_1"));
        byteArrayOutputStream.write(f1016a);
        aeVar.e().a(byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        bVar.a(byteArray, byteArray.length);
        byte[] bArr = new byte[4096];
        InputStream d2 = aeVar.d();
        while (true) {
            int read = d2.read(bArr);
            if (read != -1) {
                bVar.a(bArr, read);
            } else {
                bVar.a();
                bVar.close();
                return;
            }
        }
    }

    private static void c(ae aeVar, m mVar) {
        int i;
        String c2;
        String f2 = aeVar.f();
        g a2 = aeVar.i().a(f2);
        if (a2 == null) {
            mVar.b(404);
            return;
        }
        if (!f2.endsWith("/") || (c2 = aeVar.i().c()) == null) {
            i = 404;
        } else {
            aeVar.a(f2 + c2);
            int a3 = a2.a(aeVar, mVar);
            aeVar.a(f2);
            i = a3;
        }
        if (i == 404) {
            i = a2.a(aeVar, mVar);
        }
        if (i > 0) {
            mVar.b(i);
        }
    }

    private static void a(String str, String... strArr) {
        for (String lowerCase : strArr) {
            g.put(lowerCase.toLowerCase(), str.toLowerCase());
        }
    }

    public static String c() {
        try {
            return InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException e2) {
            return "localhost";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, char):java.lang.String[]
     arg types: [java.lang.String, int]
     candidates:
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, int):long
      com.cyberandsons.tcmaidtrial.network.v.a(java.io.File, java.lang.String):java.lang.String
      com.cyberandsons.tcmaidtrial.network.v.a(com.cyberandsons.tcmaidtrial.network.ae, com.cyberandsons.tcmaidtrial.network.m):void
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, java.lang.String[]):void
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String[], java.lang.String):boolean
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, long):long[]
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, char):java.lang.String[] */
    public static long[] a(String str, long j) {
        long j2;
        long a2;
        long a3;
        try {
            long j3 = Long.MAX_VALUE;
            long j4 = Long.MIN_VALUE;
            for (String str2 : a(str, ',')) {
                int indexOf = str2.indexOf(45);
                if (indexOf == 0) {
                    a2 = j - a(str2.substring(1), 10);
                    a3 = j - 1;
                } else if (indexOf == str2.length() - 1) {
                    a2 = a(str2.substring(0, indexOf), 10);
                    a3 = j - 1;
                } else {
                    a2 = a(str2.substring(0, indexOf), 10);
                    a3 = a(str2.substring(indexOf + 1), 10);
                }
                if (a3 < a2) {
                    throw new RuntimeException();
                }
                if (a2 < j3) {
                    j3 = a2;
                }
                if (a3 > j4) {
                    j4 = a3;
                }
            }
            if (j4 < 0) {
                throw new RuntimeException();
            }
            if (j4 < j || j3 >= j) {
                j2 = j4;
            } else {
                j2 = j - 1;
            }
            return new long[]{j3, j2};
        } catch (RuntimeException e2) {
            return null;
        }
    }

    public static long a(String str, int i) {
        long parseLong = Long.parseLong(str, i);
        if (str.charAt(0) != '-') {
            return parseLong;
        }
        throw new NumberFormatException("invalid digit: '-'");
    }

    public static Date b(String str) {
        String[] strArr = f;
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(strArr[i], Locale.US);
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                return simpleDateFormat.parse(str);
            } catch (ParseException e2) {
                i++;
            }
        }
        throw new IllegalArgumentException("invalid date format: " + str);
    }

    public static String a(long j) {
        return String.format("%ta, %<td %<tb %<tY %<tT GMT", Long.valueOf(j));
    }

    public static String[] a(String str, char c2) {
        int i = 0;
        if (str == null) {
            return new String[0];
        }
        ArrayList arrayList = new ArrayList();
        int length = str.length();
        while (i < length) {
            int indexOf = str.indexOf(c2, i);
            if (indexOf == -1) {
                indexOf = length;
            }
            String trim = str.substring(i, indexOf).trim();
            if (trim.length() > 0) {
                arrayList.add(trim);
            }
            i = indexOf + 1;
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.cyberandsons.tcmaidtrial.network.v.b(java.lang.String, char):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.cyberandsons.tcmaidtrial.network.v.b(com.cyberandsons.tcmaidtrial.network.ae, com.cyberandsons.tcmaidtrial.network.m):void
      com.cyberandsons.tcmaidtrial.network.v.b(java.lang.String, char):java.lang.String */
    public static String c(String str) {
        String b2 = b(str, '/');
        int lastIndexOf = b2.lastIndexOf(47);
        if (lastIndexOf == -1) {
            return null;
        }
        return b2.substring(0, lastIndexOf);
    }

    public static String b(String str, char c2) {
        int length = str.length();
        int i = length;
        while (i > 0 && str.charAt(i - 1) == c2) {
            i--;
        }
        return i == length ? str : str.substring(0, i);
    }

    public static String d(String str) {
        int length = str.length();
        int i = 0;
        while (i < length && str.charAt(i) == '\"') {
            i++;
        }
        return i == 0 ? str : str.substring(i);
    }

    public static String e(String str) {
        int i = -1;
        String str2 = str;
        while (true) {
            i = str2.indexOf(47, i + 1);
            if (i < 0) {
                return str2;
            }
            int i2 = i + 1;
            while (i2 < str2.length() && str2.charAt(i2) == '/') {
                i2++;
            }
            if (i2 > i + 1) {
                str2 = str2.substring(0, i + 1) + str2.substring(i2);
            }
        }
    }

    private static String b(long j) {
        char[] cArr = {' ', 'K', 'M', 'G', 'T', 'P'};
        double d2 = (double) j;
        int i = 0;
        while (d2 >= 1000.0d) {
            i++;
            d2 /= 1024.0d;
        }
        return String.format(d2 < 10.0d ? "%.1f%c" : "%.0f%c", Double.valueOf(d2), Character.valueOf(cArr[i]));
    }

    public static String f(String str) {
        int i = 0;
        int length = str.length();
        StringBuilder sb = new StringBuilder(length + 30);
        for (int i2 = 0; i2 < length; i2++) {
            String str2 = null;
            switch (str.charAt(i2)) {
                case '\"':
                    str2 = "&quot;";
                    break;
                case '&':
                    str2 = "&amp;";
                    break;
                case '\'':
                    str2 = "&#39;";
                    break;
                case '<':
                    str2 = "&lt;";
                    break;
                case '>':
                    str2 = "&gt;";
                    break;
            }
            if (str2 != null) {
                sb.append(str.substring(i, i2)).append(str2);
                i = i2 + 1;
            }
        }
        return i == 0 ? str : sb.append(str.substring(i)).toString();
    }

    public static void a(InputStream inputStream, OutputStream outputStream, long j) {
        long j2;
        byte[] bArr = new byte[4096];
        long j3 = j;
        while (j3 != 0) {
            int read = inputStream.read(bArr, 0, (j3 < 0 || ((long) bArr.length) < j3) ? bArr.length : (int) j3);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
                if (j3 > 0) {
                    j2 = (long) read;
                } else {
                    j2 = 0;
                }
                j3 -= j2;
            } else if (j3 > 0) {
                throw new IOException("unexpected end of stream");
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.io.InputStream r10) {
        /*
            r2 = 512(0x200, float:7.175E-43)
            r9 = 1
            r8 = -1
            r7 = 8192(0x2000, float:1.14794E-41)
            r6 = 0
            java.lang.String r0 = "ISO8859_1"
            byte[] r1 = new byte[r2]
            r3 = r2
            r2 = r1
            r1 = r6
        L_0x000e:
            int r4 = r10.read()
            if (r4 == r8) goto L_0x0050
            r5 = 10
            if (r4 == r5) goto L_0x0050
            if (r1 != r3) goto L_0x0046
            if (r1 != r7) goto L_0x003b
            java.io.IOException r0 = new java.io.IOException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "token too large ("
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            r2 = 41
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x003b:
            int r5 = r3 * 2
            if (r7 >= r5) goto L_0x004d
            r3 = r7
        L_0x0040:
            byte[] r5 = new byte[r3]
            java.lang.System.arraycopy(r2, r6, r5, r6, r1)
            r2 = r5
        L_0x0046:
            int r5 = r1 + 1
            byte r4 = (byte) r4
            r2[r1] = r4
            r1 = r5
            goto L_0x000e
        L_0x004d:
            int r3 = r3 * 2
            goto L_0x0040
        L_0x0050:
            if (r4 != r8) goto L_0x005a
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "unexpected end of stream"
            r0.<init>(r1)
            throw r0
        L_0x005a:
            java.lang.String r3 = new java.lang.String
            r3.<init>(r2, r6, r1, r0)
            int r0 = r3.length()
            if (r0 <= 0) goto L_0x007c
            int r0 = r3.length()
            int r0 = r0 - r9
            char r0 = r3.charAt(r0)
            r1 = 13
            if (r0 != r1) goto L_0x007c
            int r0 = r3.length()
            int r0 = r0 - r9
            java.lang.String r0 = r3.substring(r6, r0)
        L_0x007b:
            return r0
        L_0x007c:
            r0 = r3
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.network.v.a(java.io.InputStream):java.lang.String");
    }

    public static d b(InputStream inputStream) {
        d dVar = new d();
        String str = "";
        int i = 0;
        do {
            String a2 = a(inputStream);
            if (a2.length() <= 0) {
                return dVar;
            }
            int i2 = 0;
            while (i2 < a2.length() && Character.isWhitespace(a2.charAt(i2))) {
                i2++;
            }
            if (i2 > 0) {
                str = str + ' ' + a2.substring(i2);
            } else {
                str = a2;
            }
            int indexOf = str.indexOf(58);
            if (indexOf == -1) {
                throw new IOException("invalid header: \"" + str + "\"");
            }
            String substring = str.substring(0, indexOf);
            String trim = str.substring(indexOf + 1).trim();
            t b2 = dVar.b(substring, trim);
            if (b2 != null && i2 == 0) {
                String str2 = b2.b() + ", " + trim;
                dVar.b(substring, str2);
                str = substring + ": " + str2;
            }
            i++;
        } while (i <= 100);
        throw new IOException("too many header lines");
    }

    private static boolean a(String[] strArr, String str) {
        if (str == null || str.startsWith("W/")) {
            return false;
        }
        for (String str2 : strArr) {
            if (str2.equals("*") || (str2.equals(str) && !str2.startsWith("W/"))) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, char):java.lang.String[]
     arg types: [java.lang.String, int]
     candidates:
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, int):long
      com.cyberandsons.tcmaidtrial.network.v.a(java.io.File, java.lang.String):java.lang.String
      com.cyberandsons.tcmaidtrial.network.v.a(com.cyberandsons.tcmaidtrial.network.ae, com.cyberandsons.tcmaidtrial.network.m):void
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, java.lang.String[]):void
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String[], java.lang.String):boolean
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, long):long[]
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, char):java.lang.String[] */
    private static int a(ae aeVar, long j, String str) {
        boolean z;
        int i;
        boolean z2;
        int i2;
        d e2 = aeVar.e();
        String a2 = e2.a("If-Match");
        if (a2 != null && !a(a(a2, ','), str)) {
            return 412;
        }
        Date b2 = e2.b("If-Unmodified-Since");
        if (b2 != null && j > b2.getTime()) {
            return 412;
        }
        Date b3 = e2.b("If-Modified-Since");
        if (b3 == null || b3.getTime() > System.currentTimeMillis()) {
            z = false;
            i = 200;
        } else if (j > b3.getTime()) {
            z = true;
            i = 200;
        } else {
            i = 304;
            z = false;
        }
        String a3 = e2.a("If-None-Match");
        if (a3 == null) {
            z2 = z;
            i2 = i;
        } else if (a(a(a3, ','), str)) {
            boolean z3 = z;
            i2 = (aeVar.a().equals("GET") || aeVar.a().equals("HEAD")) ? 304 : 412;
            z2 = z3;
        } else {
            z2 = true;
            i2 = i;
        }
        if (z2) {
            return 200;
        }
        return i2;
    }

    public static int a(File file, String str, ae aeVar, m mVar) {
        long[] jArr;
        String substring = aeVar.f().substring(str.length());
        File canonicalFile = new File(file, substring).getCanonicalFile();
        if (!canonicalFile.exists() || canonicalFile.isHidden()) {
            return 404;
        }
        if (canonicalFile.canRead()) {
            if (canonicalFile.getPath().startsWith(file.getPath())) {
                if (!canonicalFile.isDirectory()) {
                    long length = canonicalFile.length();
                    long lastModified = canonicalFile.lastModified();
                    String str2 = "W/\"" + lastModified + "\"";
                    int i = 200;
                    long[] a2 = aeVar.a(length);
                    if (a2 == null) {
                        i = a(aeVar, lastModified, str2);
                        jArr = a2;
                    } else {
                        String a3 = aeVar.e().a("If-Range");
                        if (a3 == null) {
                            if (a2[0] >= length) {
                                i = 416;
                                jArr = a2;
                            } else {
                                i = a(aeVar, lastModified, str2);
                                jArr = a2;
                            }
                        } else if (a2[0] >= length) {
                            jArr = null;
                        } else if (a3.startsWith("\"") || a3.startsWith("W/")) {
                            jArr = !a3.equals(str2) ? null : a2;
                        } else {
                            Date b2 = aeVar.e().b("If-Range");
                            if (b2 != null && lastModified > b2.getTime()) {
                                a2 = null;
                            }
                            jArr = a2;
                        }
                    }
                    d c2 = mVar.c();
                    switch (i) {
                        case 200:
                            String name = canonicalFile.getName();
                            int lastIndexOf = name.lastIndexOf(46);
                            String str3 = lastIndexOf < 0 ? "application/octet-stream" : (String) g.get(name.substring(lastIndexOf + 1).toLowerCase());
                            mVar.a(200, length, lastModified, str2, str3 != null ? str3 : "application/octet-stream", jArr);
                            FileInputStream fileInputStream = new FileInputStream(canonicalFile);
                            try {
                                mVar.a(fileInputStream, length, jArr);
                                break;
                            } finally {
                                fileInputStream.close();
                            }
                        case 304:
                            c2.a("ETag", str2);
                            c2.a("Last-Modified", a(lastModified));
                            mVar.a(304);
                            break;
                        case 412:
                            mVar.a(412);
                            break;
                        case 416:
                            c2.a("Content-Range", "bytes */" + length);
                            mVar.a(416);
                            break;
                        default:
                            mVar.a(500);
                            break;
                    }
                } else if (!substring.endsWith("/") && substring.length() != 0) {
                    mVar.a(aeVar.g() + aeVar.f() + '/');
                } else if (!aeVar.i().e()) {
                    return 403;
                } else {
                    mVar.a(200, a(canonicalFile, aeVar.f()));
                }
                return 0;
            }
        }
        return 403;
    }

    private static String a(File file, String str) {
        if (!str.endsWith("/")) {
            str = str + "/";
        }
        int i = 21;
        for (String str2 : file.list()) {
            if (str2.length() > i) {
                i = str2.length();
            }
        }
        int i2 = i + 2;
        Formatter formatter = new Formatter(Locale.US);
        formatter.format("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">%n<html><head><title>Index of %s</title></head>%n<body><h1>Index of %s</h1>%n<pre> Name%" + (i2 - 5) + "s Last modified      Size<hr>", str, str, "");
        if (str.length() > 1) {
            formatter.format(" <a href=\"%s/\">Parent Directory</a>%" + (i2 + 5) + "s-%n", c(str), "");
        }
        for (File file2 : file.listFiles()) {
            try {
                String str3 = file2.getName() + (file2.isDirectory() ? "/" : "");
                formatter.format(" <a href=\"%s\">%s</a>%-" + (i2 - str3.length()) + "s&#8206;%td-%<tb-%<tY %<tR%6s%n", new URI(null, str + str3, null).toASCIIString(), str3, "", Long.valueOf(file2.lastModified()), file2.isDirectory() ? "- " : b(file2.length()));
            } catch (URISyntaxException e2) {
            }
        }
        formatter.format("</pre></body></html>", new Object[0]);
        return formatter.toString();
    }
}
