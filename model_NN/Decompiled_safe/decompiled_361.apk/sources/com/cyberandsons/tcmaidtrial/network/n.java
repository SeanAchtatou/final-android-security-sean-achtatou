package com.cyberandsons.tcmaidtrial.network;

import java.util.ArrayList;
import java.util.Iterator;

final class n {

    /* renamed from: a  reason: collision with root package name */
    String f1004a;

    /* renamed from: b  reason: collision with root package name */
    ArrayList f1005b;

    /* synthetic */ n() {
        this((byte) 0);
    }

    private n(byte b2) {
        this.f1005b = new ArrayList();
    }

    /* access modifiers changed from: package-private */
    public final long a() {
        long j = 0;
        Iterator it = this.f1005b.iterator();
        while (it.hasNext()) {
            j += ((f) it.next()).a();
        }
        return j;
    }
}
