package com.cyberandsons.tcmaidtrial.network;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public final class ae {

    /* renamed from: a  reason: collision with root package name */
    private String f979a;

    /* renamed from: b  reason: collision with root package name */
    private URI f980b;
    private String c;
    private d d;
    private InputStream e;
    private /* synthetic */ v f;

    public ae(v vVar, InputStream inputStream) {
        this.f = vVar;
        a(inputStream);
        this.d = v.b(inputStream);
        String a2 = this.d.a("Transfer-Encoding");
        if (a2 == null || a2.equals("identity")) {
            String a3 = this.d.a("Content-Length");
            this.e = new w(inputStream, a3 == null ? 0 : v.a(a3, 10), false);
        } else if (a2.toLowerCase().contains("chunked")) {
            this.e = new a(inputStream, this.d);
        } else {
            this.e = inputStream;
        }
    }

    public final String a() {
        return this.f979a;
    }

    public final URI b() {
        return this.f980b;
    }

    public final String c() {
        return this.c;
    }

    public final InputStream d() {
        return this.e;
    }

    public final d e() {
        return this.d;
    }

    public final String f() {
        return this.f980b.getPath();
    }

    public final void a(String str) {
        try {
            this.f980b = new URI(this.f980b.getScheme(), this.f980b.getHost(), v.e(str), this.f980b.getFragment());
        } catch (URISyntaxException e2) {
            throw new IllegalArgumentException("error setting path", e2);
        }
    }

    public final URL g() {
        String host = this.f980b.getHost();
        if (host == null && (host = this.d.a("Host")) == null) {
            host = v.c();
        }
        int indexOf = host.indexOf(58);
        if (indexOf != -1) {
            host = host.substring(0, indexOf);
        }
        try {
            return new URL("http", host, this.f.c, "");
        } catch (MalformedURLException e2) {
            return null;
        }
    }

    public final void h() {
        if (this.e.read() != -1) {
            do {
            } while (this.e.read(new byte[4096]) != -1);
        }
    }

    public final long[] a(long j) {
        String a2 = this.d.a("Range");
        if (a2 == null || !a2.startsWith("bytes=")) {
            return null;
        }
        return v.a(a2.substring(6), j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, char):java.lang.String[]
     arg types: [java.lang.String, int]
     candidates:
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, int):long
      com.cyberandsons.tcmaidtrial.network.v.a(java.io.File, java.lang.String):java.lang.String
      com.cyberandsons.tcmaidtrial.network.v.a(com.cyberandsons.tcmaidtrial.network.ae, com.cyberandsons.tcmaidtrial.network.m):void
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, java.lang.String[]):void
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String[], java.lang.String):boolean
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, long):long[]
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, char):java.lang.String[] */
    private void a(InputStream inputStream) {
        String a2;
        do {
            a2 = v.a(inputStream);
        } while (a2.length() == 0);
        String[] a3 = v.a(a2, ' ');
        if (a3.length != 3) {
            throw new IOException("invalid request line: \"" + a2 + "\"");
        }
        try {
            this.f979a = a3[0];
            this.f980b = new URI(v.e(a3[1]));
            this.c = a3[2];
        } catch (URISyntaxException e2) {
            throw new IOException("invalid URI: " + e2.getMessage());
        }
    }

    public final e i() {
        e a2 = this.f.a(g().getHost());
        return a2 != null ? a2 : this.f.a((String) null);
    }
}
