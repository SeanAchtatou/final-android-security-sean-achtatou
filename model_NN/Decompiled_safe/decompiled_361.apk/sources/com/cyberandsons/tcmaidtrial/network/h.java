package com.cyberandsons.tcmaidtrial.network;

import java.util.Iterator;
import java.util.NoSuchElementException;

final class h implements Iterator {

    /* renamed from: a  reason: collision with root package name */
    private int f992a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ d f993b;

    h(d dVar) {
        this.f993b = dVar;
    }

    public final /* bridge */ /* synthetic */ Object next() {
        if (this.f992a == this.f993b.f987b) {
            throw new NoSuchElementException();
        }
        t[] tVarArr = this.f993b.f986a;
        int i = this.f992a;
        this.f992a = i + 1;
        return tVarArr[i];
    }

    public final boolean hasNext() {
        return this.f992a < this.f993b.f987b;
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
