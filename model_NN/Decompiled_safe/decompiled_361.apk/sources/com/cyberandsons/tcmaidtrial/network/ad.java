package com.cyberandsons.tcmaidtrial.network;

import a.b.g;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public final class ad implements g {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f977a;

    /* renamed from: b  reason: collision with root package name */
    private String f978b;
    private /* synthetic */ k c;

    public ad(k kVar, byte[] bArr, String str) {
        this.c = kVar;
        this.f977a = bArr;
        this.f978b = str;
    }

    public final String b() {
        if (this.f978b == null) {
            return "application/octet-stream";
        }
        return this.f978b;
    }

    public final InputStream a() {
        return new ByteArrayInputStream(this.f977a);
    }

    public final String c() {
        return "ByteArrayDataSource";
    }
}
