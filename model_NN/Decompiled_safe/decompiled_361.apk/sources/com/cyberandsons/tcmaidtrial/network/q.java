package com.cyberandsons.tcmaidtrial.network;

import android.os.Handler;
import android.os.Message;

final class q extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DownloaderActivity f1009a;

    q(DownloaderActivity downloaderActivity) {
        this.f1009a = downloaderActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                DownloaderActivity.f(this.f1009a);
                return;
            case 1:
                DownloaderActivity.a(this.f1009a, (String) message.obj);
                return;
            case 2:
                DownloaderActivity.a(this.f1009a, message.arg1);
                return;
            case 3:
                DownloaderActivity.g(this.f1009a);
                return;
            case 4:
                DownloaderActivity.a(this.f1009a, message.arg1, message.arg2);
                return;
            default:
                throw new IllegalArgumentException("Unknown message id " + message.what);
        }
    }
}
