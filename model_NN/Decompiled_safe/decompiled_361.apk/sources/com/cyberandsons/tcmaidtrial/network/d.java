package com.cyberandsons.tcmaidtrial.network;

import java.io.OutputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public final class d implements Iterable {

    /* renamed from: a  reason: collision with root package name */
    protected t[] f986a = new t[12];

    /* renamed from: b  reason: collision with root package name */
    protected int f987b;

    public final String a(String str) {
        for (int i = 0; i < this.f987b; i++) {
            if (this.f986a[i].a().equalsIgnoreCase(str)) {
                return this.f986a[i].b();
            }
        }
        return null;
    }

    public final Date b(String str) {
        try {
            String a2 = a(str);
            if (a2 == null) {
                return null;
            }
            return v.b(a2);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    public final boolean c(String str) {
        return a(str) != null;
    }

    public final void a(String str, String str2) {
        t tVar = new t(str, str2);
        if (this.f987b == this.f986a.length) {
            t[] tVarArr = new t[(this.f987b * 2)];
            System.arraycopy(this.f986a, 0, tVarArr, 0, this.f987b);
            this.f986a = tVarArr;
        }
        t[] tVarArr2 = this.f986a;
        int i = this.f987b;
        this.f987b = i + 1;
        tVarArr2[i] = tVar;
    }

    public final void a(d dVar) {
        Iterator it = dVar.iterator();
        while (it.hasNext()) {
            t tVar = (t) it.next();
            a(tVar.a(), tVar.b());
        }
    }

    public final t b(String str, String str2) {
        for (int i = 0; i < this.f987b; i++) {
            if (this.f986a[i].a().equalsIgnoreCase(str)) {
                t tVar = this.f986a[i];
                this.f986a[i] = new t(str, str2);
                return tVar;
            }
        }
        a(str, str2);
        return null;
    }

    public final void d(String str) {
        int i = 0;
        for (int i2 = 0; i2 < this.f987b; i2++) {
            if (!this.f986a[i2].a().equalsIgnoreCase(str)) {
                this.f986a[i] = this.f986a[i2];
                i++;
            }
        }
        while (this.f987b > i) {
            t[] tVarArr = this.f986a;
            int i3 = this.f987b - 1;
            this.f987b = i3;
            tVarArr[i3] = null;
        }
    }

    public final void a(OutputStream outputStream) {
        for (int i = 0; i < this.f987b; i++) {
            outputStream.write((this.f986a[i].a() + ": " + this.f986a[i].b()).getBytes("ISO8859_1"));
            outputStream.write(v.f1016a);
        }
        outputStream.write(v.f1016a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, char):java.lang.String[]
     arg types: [java.lang.String, int]
     candidates:
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, int):long
      com.cyberandsons.tcmaidtrial.network.v.a(java.io.File, java.lang.String):java.lang.String
      com.cyberandsons.tcmaidtrial.network.v.a(com.cyberandsons.tcmaidtrial.network.ae, com.cyberandsons.tcmaidtrial.network.m):void
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, java.lang.String[]):void
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String[], java.lang.String):boolean
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, long):long[]
      com.cyberandsons.tcmaidtrial.network.v.a(java.lang.String, char):java.lang.String[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.cyberandsons.tcmaidtrial.network.v.b(java.lang.String, char):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.cyberandsons.tcmaidtrial.network.v.b(com.cyberandsons.tcmaidtrial.network.ae, com.cyberandsons.tcmaidtrial.network.m):void
      com.cyberandsons.tcmaidtrial.network.v.b(java.lang.String, char):java.lang.String */
    public final Map e(String str) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (String a2 : v.a(a(str), ';')) {
            String[] a3 = v.a(a2, '=');
            linkedHashMap.put(a3[0], a3.length == 1 ? "" : v.d(v.b(a3[1], '\"')));
        }
        return linkedHashMap;
    }

    public final Iterator iterator() {
        return new h(this);
    }
}
