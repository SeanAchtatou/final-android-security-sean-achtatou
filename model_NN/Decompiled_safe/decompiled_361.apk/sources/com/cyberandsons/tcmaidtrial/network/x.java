package com.cyberandsons.tcmaidtrial.network;

import java.io.IOException;

public final class x extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f1020a;

    public x(v vVar) {
        this.f1020a = vVar;
        setName(getClass().getSimpleName() + '-' + vVar.c);
    }

    public final void run() {
        while (!this.f1020a.e.isClosed()) {
            try {
                this.f1020a.d.execute(new o(this, this.f1020a.e.accept()));
            } catch (IOException e) {
                return;
            }
        }
    }
}
