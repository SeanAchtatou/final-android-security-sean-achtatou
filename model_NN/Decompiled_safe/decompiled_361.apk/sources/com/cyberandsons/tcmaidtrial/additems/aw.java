package com.cyberandsons.tcmaidtrial.additems;

import android.view.MotionEvent;
import android.view.View;

final class aw implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiagnosisAdd f225a;

    aw(DiagnosisAdd diagnosisAdd) {
        this.f225a = diagnosisAdd;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f225a.f189b;
    }
}
