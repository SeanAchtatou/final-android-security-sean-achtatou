package com.cyberandsons.tcmaidtrial.additems;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class bs implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SixStagesAdd f248a;

    bs(SixStagesAdd sixStagesAdd) {
        this.f248a = sixStagesAdd;
    }

    public final void onClick(View view) {
        SixStagesAdd sixStagesAdd = this.f248a;
        ((InputMethodManager) sixStagesAdd.getSystemService("input_method")).hideSoftInputFromWindow(sixStagesAdd.f198a.getWindowToken(), 0);
        sixStagesAdd.finish();
    }
}
