package com.cyberandsons.tcmaidtrial.additems;

import android.content.DialogInterface;

final class ai implements DialogInterface.OnMultiChoiceClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SixStagesAdd f211a;

    ai(SixStagesAdd sixStagesAdd) {
        this.f211a = sixStagesAdd;
    }

    public final void onClick(DialogInterface dialogInterface, int i, boolean z) {
        if (z) {
            this.f211a.c.add(this.f211a.d[i]);
        } else {
            this.f211a.c.remove(this.f211a.d[i]);
        }
        this.f211a.b();
    }
}
