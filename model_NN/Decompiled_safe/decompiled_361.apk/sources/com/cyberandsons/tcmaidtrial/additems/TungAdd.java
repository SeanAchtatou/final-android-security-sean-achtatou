package com.cyberandsons.tcmaidtrial.additems;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import org.acra.ErrorReporter;

public class TungAdd extends Activity implements AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    EditText f200a;

    /* renamed from: b  reason: collision with root package name */
    EditText f201b;
    String c;
    boolean d;
    private ScrollView e;
    private ImageView f;
    private EditText g;
    private EditText h;
    private EditText i;
    private EditText j;
    private EditText k;
    private EditText l;
    private Spinner m;
    private String n;
    private ImageButton o;
    private ImageButton p;
    private ImageButton q;
    private boolean r = true;
    private boolean s;
    private boolean t;
    private String u;
    private boolean v;
    private SQLiteDatabase w;
    private n x;

    public TungAdd() {
        this.s = !this.r;
        this.t = this.s;
        this.d = this.s;
        this.v = this.s;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.w == null || !this.w.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("TA:openDataBase()", str + " opened.");
                this.w = SQLiteDatabase.openDatabase(str, null, 16);
                this.w.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.w.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("TA:openDataBase()", str2 + " ATTACHed.");
                this.x = new n();
                this.x.a(this.w);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new af(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        setContentView((int) C0000R.layout.tung_detailed_add);
        this.e = (ScrollView) findViewById(C0000R.id.svDetail);
        this.e.smoothScrollTo(0, 0);
        getWindow().setSoftInputMode(3);
        b();
        this.f = (ImageView) findViewById(C0000R.id.t_image);
        if (this.f200a != null) {
            this.f200a = null;
        }
        this.f200a = (EditText) findViewById(C0000R.id.t_name);
        this.f200a.setOnTouchListener(new o(this));
        if (this.g != null) {
            this.g = null;
        }
        this.g = (EditText) findViewById(C0000R.id.t_number);
        this.g.setOnTouchListener(new n(this));
        if (this.m != null) {
            this.m = null;
        }
        this.m = (Spinner) findViewById(C0000R.id.t_pointcategory);
        this.m.setPrompt("Select from these categories...");
        this.m.setOnItemSelectedListener(this);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, TcmAid.aV);
        arrayAdapter.setDropDownViewResource(17367049);
        this.m.setAdapter((SpinnerAdapter) arrayAdapter);
        this.m.setSelection(0);
        if (this.h != null) {
            this.h = null;
        }
        this.h = (EditText) findViewById(C0000R.id.t_location);
        this.h.setOnTouchListener(new m(this));
        if (this.i != null) {
            this.i = null;
        }
        this.i = (EditText) findViewById(C0000R.id.t_englishname);
        this.i.setOnTouchListener(new r(this));
        if (this.j != null) {
            this.j = null;
        }
        this.j = (EditText) findViewById(C0000R.id.t_indications);
        this.j.setOnTouchListener(new q(this));
        if (this.k != null) {
            this.k = null;
        }
        this.k = (EditText) findViewById(C0000R.id.t_anatomy);
        this.k.setOnTouchListener(new ae(this));
        if (this.l != null) {
            this.l = null;
        }
        this.l = (EditText) findViewById(C0000R.id.t_preparation);
        this.l.setOnTouchListener(new ag(this));
        if (this.f201b != null) {
            this.f201b = null;
        }
        this.f201b = (EditText) findViewById(C0000R.id.t_notes);
        this.f201b.setOnTouchListener(new ah(this));
        try {
            if (this.t && this.u != null && this.u.length() > 0) {
                Bitmap decodeFile = BitmapFactory.decodeFile(this.u);
                if (decodeFile == null) {
                    this.f.setImageResource(C0000R.drawable.nopicture);
                    return;
                }
                this.f.setImageDrawable(dh.a(decodeFile, TcmAid.cS - 0, TcmAid.cS - 0, getWindow()));
                this.t = this.r;
            }
        } catch (NullPointerException e3) {
            ErrorReporter.a().a("myVariable", this.u);
            ErrorReporter.a().handleSilentException(new NullPointerException("TA:loadUserModifiedData()"));
        }
    }

    public void onDestroy() {
        TcmAid.aV.clear();
        try {
            if (this.w != null && this.w.isOpen()) {
                this.w.close();
                this.w = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
        if (i3 == -1) {
            Uri data = intent.getData();
            Log.i("onActivityResult", data.toString());
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(getContentResolver().openInputStream(data));
                if (decodeStream != null) {
                    BitmapDrawable a2 = dh.a(decodeStream, TcmAid.cS - 0, TcmAid.cS - 0, getWindow());
                    this.f.setImageDrawable(a2);
                    this.t = this.r;
                    this.u = String.format("%s/%s_alt.png", getString(C0000R.string.image_path), this.c);
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(this.u);
                        a2.getBitmap().compress(Bitmap.CompressFormat.PNG, 90, fileOutputStream);
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    } catch (Exception e2) {
                        Log.e("TA:onActivityResult", "Failed to write alternate image: " + e2.getLocalizedMessage());
                    }
                }
            } catch (FileNotFoundException e3) {
                Log.e("TA:onActivityResult", "Failed to process alternate image: " + e3.getLocalizedMessage());
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            this.v = this.r;
            TcmAid.bl = this.r;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.f201b.getWindowToken(), 0);
        String str = this.u;
        boolean z = this.r;
        ContentValues contentValues = new ContentValues();
        int b2 = q.b("SELECT MAX(_id) FROM userDB.mastertung", this.w) + 1;
        if (b2 <= 1000) {
            b2 = 1001;
        }
        contentValues.put("_id", Integer.toString(b2));
        contentValues.put("points", this.f200a.getText().toString());
        contentValues.put("location", this.h.getText().toString());
        contentValues.put("english_name", this.i.getText().toString());
        contentValues.put("description", this.j.getText().toString());
        contentValues.put("anatomy", this.k.getText().toString());
        contentValues.put("preparation", this.l.getText().toString());
        contentValues.put("note", this.f201b.getText().toString());
        contentValues.put("proc", "");
        contentValues.put("number", String.format("%s.%s", this.n, this.g.getText().toString()));
        contentValues.put("group_number", this.n);
        contentValues.put("importance", "");
        if (z) {
            contentValues.put("alt_image", str);
        }
        try {
            this.w.insert("userDB.mastertung", "group_number", contentValues);
        } catch (SQLException e2) {
            q.a(e2);
        }
        TcmAid.bl = this.r;
        finish();
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:42:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b() {
        /*
            r7 = this;
            r5 = 0
            r0 = 2131361808(0x7f0a0010, float:1.8343379E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r7.o = r0
            android.widget.ImageButton r0 = r7.o
            com.cyberandsons.tcmaidtrial.additems.k r1 = new com.cyberandsons.tcmaidtrial.additems.k
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            r0 = 2131361810(0x7f0a0012, float:1.8343383E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r7.p = r0
            android.widget.ImageButton r0 = r7.p
            com.cyberandsons.tcmaidtrial.additems.l r1 = new com.cyberandsons.tcmaidtrial.additems.l
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            r0 = 2131361809(0x7f0a0011, float:1.834338E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r7.q = r0
            android.widget.ImageButton r0 = r7.q
            com.cyberandsons.tcmaidtrial.additems.p r1 = new com.cyberandsons.tcmaidtrial.additems.p
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            java.lang.String r0 = ""
            java.lang.String r1 = ""
            java.lang.String r2 = "SELECT DISTINCT main.mastertung.group_number FROM main.mastertung ORDER BY main.mastertung.group_number "
            com.cyberandsons.tcmaidtrial.TcmAid.aZ = r2     // Catch:{ SQLException -> 0x007d, NullPointerException -> 0x0088, all -> 0x00bf }
            java.lang.String r1 = "Pre-rawQuery"
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.aZ     // Catch:{ SQLException -> 0x007d, NullPointerException -> 0x0088, all -> 0x00bf }
            android.database.sqlite.SQLiteDatabase r2 = r7.w     // Catch:{ SQLException -> 0x007d, NullPointerException -> 0x0088, all -> 0x00bf }
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.TcmAid.aZ     // Catch:{ SQLException -> 0x007d, NullPointerException -> 0x0088, all -> 0x00bf }
            r4 = 0
            android.database.Cursor r7 = r2.rawQuery(r3, r4)     // Catch:{ SQLException -> 0x007d, NullPointerException -> 0x0088, all -> 0x00bf }
            android.database.sqlite.SQLiteCursor r7 = (android.database.sqlite.SQLiteCursor) r7     // Catch:{ SQLException -> 0x007d, NullPointerException -> 0x0088, all -> 0x00bf }
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.aV     // Catch:{ SQLException -> 0x00d5, NullPointerException -> 0x00d1, all -> 0x00c7 }
            r2.clear()     // Catch:{ SQLException -> 0x00d5, NullPointerException -> 0x00d1, all -> 0x00c7 }
            java.lang.String r1 = "Pre-cursor loop"
            boolean r2 = r7.moveToFirst()     // Catch:{ SQLException -> 0x00d5, NullPointerException -> 0x00d1, all -> 0x00c7 }
            if (r2 == 0) goto L_0x0074
        L_0x0064:
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.aV     // Catch:{ SQLException -> 0x00d5, NullPointerException -> 0x00d1, all -> 0x00c7 }
            r3 = 0
            java.lang.String r3 = r7.getString(r3)     // Catch:{ SQLException -> 0x00d5, NullPointerException -> 0x00d1, all -> 0x00c7 }
            r2.add(r3)     // Catch:{ SQLException -> 0x00d5, NullPointerException -> 0x00d1, all -> 0x00c7 }
            boolean r2 = r7.moveToNext()     // Catch:{ SQLException -> 0x00d5, NullPointerException -> 0x00d1, all -> 0x00c7 }
            if (r2 != 0) goto L_0x0064
        L_0x0074:
            r7.close()     // Catch:{ SQLException -> 0x00d5, NullPointerException -> 0x00d1, all -> 0x00c7 }
            if (r7 == 0) goto L_0x007c
            r7.close()
        L_0x007c:
            return
        L_0x007d:
            r0 = move-exception
            r1 = r5
        L_0x007f:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x00ca }
            if (r1 == 0) goto L_0x007c
            r1.close()
            goto L_0x007c
        L_0x0088:
            r2 = move-exception
            r2 = r0
            r0 = r5
        L_0x008b:
            org.acra.ErrorReporter r3 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x00cc }
            java.lang.String r4 = "myVariable"
            r3.a(r4, r2)     // Catch:{ all -> 0x00cc }
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x00cc }
            java.lang.NullPointerException r3 = new java.lang.NullPointerException     // Catch:{ all -> 0x00cc }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00cc }
            r4.<init>()     // Catch:{ all -> 0x00cc }
            java.lang.String r5 = "TA:load_details( last step completed = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00cc }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x00cc }
            java.lang.String r4 = " )"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x00cc }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00cc }
            r3.<init>(r1)     // Catch:{ all -> 0x00cc }
            r2.handleSilentException(r3)     // Catch:{ all -> 0x00cc }
            if (r0 == 0) goto L_0x007c
            r0.close()
            goto L_0x007c
        L_0x00bf:
            r0 = move-exception
            r1 = r5
        L_0x00c1:
            if (r1 == 0) goto L_0x00c6
            r1.close()
        L_0x00c6:
            throw r0
        L_0x00c7:
            r0 = move-exception
            r1 = r7
            goto L_0x00c1
        L_0x00ca:
            r0 = move-exception
            goto L_0x00c1
        L_0x00cc:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00c1
        L_0x00d1:
            r2 = move-exception
            r2 = r0
            r0 = r7
            goto L_0x008b
        L_0x00d5:
            r0 = move-exception
            r1 = r7
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.additems.TungAdd.b():void");
    }

    public void onItemSelected(AdapterView adapterView, View view, int i2, long j2) {
        this.n = (String) TcmAid.aV.get(i2);
    }

    public void onNothingSelected(AdapterView adapterView) {
    }
}
