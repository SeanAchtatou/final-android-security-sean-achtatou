package com.cyberandsons.tcmaidtrial.additems;

import android.content.DialogInterface;

final class cg implements DialogInterface.OnMultiChoiceClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiagnosisAdd f263a;

    cg(DiagnosisAdd diagnosisAdd) {
        this.f263a = diagnosisAdd;
    }

    public final void onClick(DialogInterface dialogInterface, int i, boolean z) {
        if (z) {
            this.f263a.c.add(this.f263a.d[i]);
        } else {
            this.f263a.c.remove(this.f263a.d[i]);
        }
        this.f263a.b();
    }
}
