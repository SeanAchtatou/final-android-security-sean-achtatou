package com.cyberandsons.tcmaidtrial.additems;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.misc.ad;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import org.acra.ErrorReporter;

public class PulseDiagAdd extends Activity implements AdapterView.OnItemSelectedListener {
    private boolean A;
    private boolean B;
    private String C;
    private boolean D;
    private SQLiteDatabase E;
    private n F;

    /* renamed from: a  reason: collision with root package name */
    EditText f196a;

    /* renamed from: b  reason: collision with root package name */
    EditText f197b;
    String c;
    boolean d;
    private ScrollView e;
    private ImageView f;
    private EditText g;
    private EditText h;
    private EditText i;
    private EditText j;
    private EditText k;
    private EditText l;
    private EditText m;
    private EditText n;
    private EditText o;
    private EditText p;
    private EditText q;
    private EditText r;
    private EditText s;
    private EditText t;
    private Spinner u;
    private String v;
    private ImageButton w;
    private ImageButton x;
    private ImageButton y;
    private boolean z = true;

    public PulseDiagAdd() {
        this.A = !this.z;
        this.B = this.A;
        this.d = this.A;
        this.D = this.A;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.E == null || !this.E.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("PDA:openDataBase()", str + " opened.");
                this.E = SQLiteDatabase.openDatabase(str, null, 16);
                this.E.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.E.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("PDA:openDataBase()", str2 + " ATTACHed.");
                this.F = new n();
                this.F.a(this.E);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new ad(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        setContentView((int) C0000R.layout.pulsediag_detailed_add);
        this.e = (ScrollView) findViewById(C0000R.id.svDetail);
        this.e.smoothScrollTo(0, 0);
        getWindow().setSoftInputMode(3);
        b();
        this.f = (ImageView) findViewById(C0000R.id.pd_image);
        if (this.f196a != null) {
            this.f196a = null;
        }
        this.f196a = (EditText) findViewById(C0000R.id.pd_name);
        this.f196a.setOnTouchListener(new dr(this));
        if (this.u != null) {
            this.u = null;
        }
        this.u = (Spinner) findViewById(C0000R.id.pd_category);
        this.u.setPrompt("Select from these categories...");
        this.u.setOnItemSelectedListener(this);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, TcmAid.aA);
        arrayAdapter.setDropDownViewResource(17367049);
        this.u.setAdapter((SpinnerAdapter) arrayAdapter);
        this.u.setSelection(0);
        if (this.g != null) {
            this.g = null;
        }
        this.g = (EditText) findViewById(C0000R.id.pd_description);
        this.g.setOnTouchListener(new dq(this));
        if (this.h != null) {
            this.h = null;
        }
        this.h = (EditText) findViewById(C0000R.id.pd_sensation);
        this.h.setOnTouchListener(new dp(this));
        if (this.i != null) {
            this.i = null;
        }
        this.i = (EditText) findViewById(C0000R.id.pd_indications);
        this.i.setOnTouchListener(new dl(this));
        if (this.j != null) {
            this.j = null;
        }
        this.j = (EditText) findViewById(C0000R.id.pd_leftCun);
        this.j.setOnTouchListener(new dk(this));
        if (this.k != null) {
            this.k = null;
        }
        this.k = (EditText) findViewById(C0000R.id.pd_leftGuan);
        this.k.setOnTouchListener(new ab(this));
        if (this.l != null) {
            this.l = null;
        }
        this.l = (EditText) findViewById(C0000R.id.pd_leftChi);
        this.l.setOnTouchListener(new z(this));
        if (this.m != null) {
            this.m = null;
        }
        this.m = (EditText) findViewById(C0000R.id.pd_rightCun);
        this.m.setOnTouchListener(new y(this));
        if (this.n != null) {
            this.n = null;
        }
        this.n = (EditText) findViewById(C0000R.id.pd_rightGuan);
        this.n.setOnTouchListener(new x(this));
        if (this.o != null) {
            this.o = null;
        }
        this.o = (EditText) findViewById(C0000R.id.pd_rightChi);
        this.o.setOnTouchListener(new w(this));
        if (this.p != null) {
            this.p = null;
        }
        this.p = (EditText) findViewById(C0000R.id.pd_bilateralCun);
        this.p.setOnTouchListener(new v(this));
        if (this.q != null) {
            this.q = null;
        }
        this.q = (EditText) findViewById(C0000R.id.pd_bilateralGuan);
        this.q.setOnTouchListener(new u(this));
        if (this.r != null) {
            this.r = null;
        }
        this.r = (EditText) findViewById(C0000R.id.pd_bilateralChi);
        this.r.setOnTouchListener(new t(this));
        if (this.s != null) {
            this.s = null;
        }
        this.s = (EditText) findViewById(C0000R.id.pd_combinations);
        this.s.setOnTouchListener(new s(this));
        if (this.t != null) {
            this.t = null;
        }
        this.t = (EditText) findViewById(C0000R.id.pd_alternateNames);
        this.t.setOnTouchListener(new aa(this));
        if (this.f197b != null) {
            this.f197b = null;
        }
        this.f197b = (EditText) findViewById(C0000R.id.pd_notes);
        this.f197b.setOnTouchListener(new ac(this));
        try {
            if (this.B && this.C != null && this.C.length() > 0) {
                Bitmap decodeFile = BitmapFactory.decodeFile(this.C);
                if (decodeFile == null) {
                    this.f.setImageResource(C0000R.drawable.nopicture);
                    return;
                }
                this.f.setImageDrawable(dh.a(decodeFile, TcmAid.cS - 0, TcmAid.cS - 0, getWindow()));
                this.B = this.z;
            }
        } catch (NullPointerException e3) {
            ErrorReporter.a().a("myVariable", this.C);
            ErrorReporter.a().handleSilentException(new NullPointerException("PDA:loadUserModifiedData()"));
        }
    }

    public void onDestroy() {
        TcmAid.aA.clear();
        try {
            if (this.E != null && this.E.isOpen()) {
                this.E.close();
                this.E = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
        if (i3 == -1) {
            Uri data = intent.getData();
            Log.i("onActivityResult", data.toString());
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(getContentResolver().openInputStream(data));
                if (decodeStream != null) {
                    BitmapDrawable a2 = dh.a(decodeStream, TcmAid.cS - 0, TcmAid.cS - 0, getWindow());
                    this.f.setImageDrawable(a2);
                    this.B = this.z;
                    this.C = String.format("%s/%s_alt.png", getString(C0000R.string.image_path), this.c);
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(this.C);
                        a2.getBitmap().compress(Bitmap.CompressFormat.PNG, 90, fileOutputStream);
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    } catch (Exception e2) {
                        Log.e("PDA:onActivityResult", "Failed to write alternate image: " + e2.getLocalizedMessage());
                    }
                }
            } catch (FileNotFoundException e3) {
                Log.e("PDA:onActivityResult", "Failed to process alternate image: " + e3.getLocalizedMessage());
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            this.D = this.z;
            TcmAid.bl = this.z;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.f197b.getWindowToken(), 0);
        String str = this.C;
        boolean z2 = this.z;
        ContentValues contentValues = new ContentValues();
        int b2 = q.b("SELECT MAX(_id) FROM userDB.pulsediag", this.E) + 1;
        if (b2 <= 1000) {
            b2 = 1001;
        }
        contentValues.put("_id", Integer.toString(b2));
        contentValues.put("name", ad.a(this.f196a.getText().toString()));
        contentValues.put("category", this.v);
        contentValues.put("description", this.g.getText().toString());
        contentValues.put("sensation", this.h.getText().toString());
        contentValues.put("indications", this.i.getText().toString());
        contentValues.put("left_cun", this.j.getText().toString());
        contentValues.put("left_guan", this.k.getText().toString());
        contentValues.put("left_chi", this.l.getText().toString());
        contentValues.put("right_cun", this.m.getText().toString());
        contentValues.put("right_guan", this.n.getText().toString());
        contentValues.put("right_cun", this.o.getText().toString());
        contentValues.put("bilateral_cun", this.p.getText().toString());
        contentValues.put("bilateral_guan", this.q.getText().toString());
        contentValues.put("bilateral_chi", this.r.getText().toString());
        contentValues.put("combinations", this.s.getText().toString());
        contentValues.put("alternate_names", this.t.getText().toString());
        contentValues.put("notes", this.f197b.getText().toString());
        if (z2) {
            contentValues.put("alt_image", str);
        }
        try {
            this.E.insert("userDB.pulsediag", null, contentValues);
        } catch (SQLException e2) {
            q.a(e2);
        }
        TcmAid.bl = this.z;
        finish();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0077, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0078, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0083, code lost:
        r2 = "";
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00b5, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00b9, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00ba, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00bd, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0077 A[ExcHandler: SQLException (e android.database.SQLException), Splitter:B:1:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00b9 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:44:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b() {
        /*
            r7 = this;
            r5 = 0
            r0 = 2131361808(0x7f0a0010, float:1.8343379E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r7.w = r0
            android.widget.ImageButton r0 = r7.w
            com.cyberandsons.tcmaidtrial.additems.dn r1 = new com.cyberandsons.tcmaidtrial.additems.dn
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            r0 = 2131361810(0x7f0a0012, float:1.8343383E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r7.x = r0
            android.widget.ImageButton r0 = r7.x
            com.cyberandsons.tcmaidtrial.additems.dm r1 = new com.cyberandsons.tcmaidtrial.additems.dm
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            r0 = 2131361809(0x7f0a0011, float:1.834338E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r7.y = r0
            android.widget.ImageButton r0 = r7.y
            com.cyberandsons.tcmaidtrial.additems.ds r1 = new com.cyberandsons.tcmaidtrial.additems.ds
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            java.lang.String r0 = ""
            java.lang.String r1 = ""
            java.lang.String r2 = "SELECT DISTINCT main.pulsediag.category FROM main.pulsediag "
            com.cyberandsons.tcmaidtrial.TcmAid.aG = r2     // Catch:{ SQLException -> 0x0077, NullPointerException -> 0x0082, all -> 0x00b9 }
            android.database.sqlite.SQLiteDatabase r0 = r7.E     // Catch:{ SQLException -> 0x0077, NullPointerException -> 0x00cb, all -> 0x00b9 }
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.TcmAid.aG     // Catch:{ SQLException -> 0x0077, NullPointerException -> 0x00cb, all -> 0x00b9 }
            r4 = 0
            android.database.Cursor r7 = r0.rawQuery(r3, r4)     // Catch:{ SQLException -> 0x0077, NullPointerException -> 0x00cb, all -> 0x00b9 }
            android.database.sqlite.SQLiteCursor r7 = (android.database.sqlite.SQLiteCursor) r7     // Catch:{ SQLException -> 0x0077, NullPointerException -> 0x00cb, all -> 0x00b9 }
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.aA     // Catch:{ SQLException -> 0x00d1, NullPointerException -> 0x00ce, all -> 0x00c1 }
            r0.clear()     // Catch:{ SQLException -> 0x00d1, NullPointerException -> 0x00ce, all -> 0x00c1 }
            boolean r0 = r7.moveToFirst()     // Catch:{ SQLException -> 0x00d1, NullPointerException -> 0x00ce, all -> 0x00c1 }
            if (r0 == 0) goto L_0x006e
        L_0x005e:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.aA     // Catch:{ SQLException -> 0x00d1, NullPointerException -> 0x00ce, all -> 0x00c1 }
            r3 = 0
            java.lang.String r3 = r7.getString(r3)     // Catch:{ SQLException -> 0x00d1, NullPointerException -> 0x00ce, all -> 0x00c1 }
            r0.add(r3)     // Catch:{ SQLException -> 0x00d1, NullPointerException -> 0x00ce, all -> 0x00c1 }
            boolean r0 = r7.moveToNext()     // Catch:{ SQLException -> 0x00d1, NullPointerException -> 0x00ce, all -> 0x00c1 }
            if (r0 != 0) goto L_0x005e
        L_0x006e:
            r7.close()     // Catch:{ SQLException -> 0x00d1, NullPointerException -> 0x00ce, all -> 0x00c1 }
            if (r7 == 0) goto L_0x0076
            r7.close()
        L_0x0076:
            return
        L_0x0077:
            r0 = move-exception
            r1 = r5
        L_0x0079:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x00c4 }
            if (r1 == 0) goto L_0x0076
            r1.close()
            goto L_0x0076
        L_0x0082:
            r2 = move-exception
            r2 = r0
            r0 = r5
        L_0x0085:
            org.acra.ErrorReporter r3 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x00c6 }
            java.lang.String r4 = "myVariable"
            r3.a(r4, r2)     // Catch:{ all -> 0x00c6 }
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x00c6 }
            java.lang.NullPointerException r3 = new java.lang.NullPointerException     // Catch:{ all -> 0x00c6 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c6 }
            r4.<init>()     // Catch:{ all -> 0x00c6 }
            java.lang.String r5 = "PDA:load_details( last step completed = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00c6 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x00c6 }
            java.lang.String r4 = " )"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x00c6 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00c6 }
            r3.<init>(r1)     // Catch:{ all -> 0x00c6 }
            r2.handleSilentException(r3)     // Catch:{ all -> 0x00c6 }
            if (r0 == 0) goto L_0x0076
            r0.close()
            goto L_0x0076
        L_0x00b9:
            r0 = move-exception
            r1 = r5
        L_0x00bb:
            if (r1 == 0) goto L_0x00c0
            r1.close()
        L_0x00c0:
            throw r0
        L_0x00c1:
            r0 = move-exception
            r1 = r7
            goto L_0x00bb
        L_0x00c4:
            r0 = move-exception
            goto L_0x00bb
        L_0x00c6:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00bb
        L_0x00cb:
            r0 = move-exception
            r0 = r5
            goto L_0x0085
        L_0x00ce:
            r0 = move-exception
            r0 = r7
            goto L_0x0085
        L_0x00d1:
            r0 = move-exception
            r1 = r7
            goto L_0x0079
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.additems.PulseDiagAdd.b():void");
    }

    public void onItemSelected(AdapterView adapterView, View view, int i2, long j2) {
        this.v = (String) TcmAid.aA.get(i2);
    }

    public void onNothingSelected(AdapterView adapterView) {
    }
}
