package com.cyberandsons.tcmaidtrial.additems;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.m;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.misc.ad;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import org.acra.ErrorReporter;

public class HerbAdd extends Activity implements AdapterView.OnItemSelectedListener {
    private ImageButton A;
    private ImageButton B;
    private boolean C = true;
    private boolean D;
    private boolean E;
    private String F;
    private boolean G;
    private SQLiteDatabase H;
    private n I;

    /* renamed from: a  reason: collision with root package name */
    EditText f192a;

    /* renamed from: b  reason: collision with root package name */
    EditText f193b;
    String c;
    boolean d;
    private ScrollView e;
    private ImageView f;
    private Spinner g;
    private String h;
    private EditText i;
    private EditText j;
    private EditText k;
    private EditText l;
    private EditText m;
    private EditText n;
    private EditText o;
    private EditText p;
    private EditText q;
    private EditText r;
    private EditText s;
    private EditText t;
    private EditText u;
    private EditText v;
    private EditText w;
    private EditText x;
    private EditText y;
    private ImageButton z;

    public HerbAdd() {
        this.D = !this.C;
        this.E = this.D;
        this.d = this.D;
        this.G = this.D;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.H == null || !this.H.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("HA:openDataBase()", str + " opened.");
                this.H = SQLiteDatabase.openDatabase(str, null, 16);
                this.H.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.H.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("HA:openDataBase()", str2 + " ATTACHed.");
                this.I = new n();
                this.I.a(this.H);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new cf(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        setContentView((int) C0000R.layout.herbs_detailed_add);
        this.e = (ScrollView) findViewById(C0000R.id.svDetail);
        this.e.smoothScrollTo(0, 0);
        getWindow().setSoftInputMode(3);
        b();
        this.f = (ImageView) findViewById(C0000R.id.h_image);
        if (this.f192a != null) {
            this.f192a = null;
        }
        this.f192a = (EditText) findViewById(C0000R.id.h_name);
        this.f192a.setOnTouchListener(new di(this));
        if (this.g != null) {
            this.g = null;
        }
        this.g = (Spinner) findViewById(C0000R.id.h_category);
        this.g.setPrompt("Select from these categories...");
        this.g.setOnItemSelectedListener(this);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, TcmAid.ab);
        arrayAdapter.setDropDownViewResource(17367049);
        this.g.setAdapter((SpinnerAdapter) arrayAdapter);
        this.g.setSelection(0);
        if (this.i != null) {
            this.i = null;
        }
        this.i = (EditText) findViewById(C0000R.id.h_keychar);
        this.i.setOnTouchListener(new dj(this));
        if (this.j != null) {
            this.j = null;
        }
        this.j = (EditText) findViewById(C0000R.id.h_english);
        this.j.setOnTouchListener(new dc(this));
        if (this.k != null) {
            this.k = null;
        }
        this.k = (EditText) findViewById(C0000R.id.h_botanical);
        this.k.setOnTouchListener(new db(this));
        if (this.l != null) {
            this.l = null;
        }
        this.l = (EditText) findViewById(C0000R.id.h_meridians);
        this.l.setOnTouchListener(new dd(this));
        if (this.m != null) {
            this.m = null;
        }
        this.m = (EditText) findViewById(C0000R.id.h_taste);
        this.m.setOnTouchListener(new ci(this));
        if (this.n != null) {
            this.n = null;
        }
        this.n = (EditText) findViewById(C0000R.id.h_temperature);
        this.n.setOnTouchListener(new ck(this));
        if (this.o != null) {
            this.o = null;
        }
        this.o = (EditText) findViewById(C0000R.id.h_functions);
        this.o.setOnTouchListener(new cp(this));
        if (this.p != null) {
            this.p = null;
        }
        this.p = (EditText) findViewById(C0000R.id.h_indications);
        this.p.setOnTouchListener(new cq(this));
        if (this.q != null) {
            this.q = null;
        }
        this.q = (EditText) findViewById(C0000R.id.h_contraindications);
        this.q.setOnTouchListener(new cr(this));
        if (this.r != null) {
            this.r = null;
        }
        this.r = (EditText) findViewById(C0000R.id.h_dosage);
        this.r.setOnTouchListener(new cs(this));
        if (this.s != null) {
            this.s = null;
        }
        this.s = (EditText) findViewById(C0000R.id.h_cautions);
        this.s.setOnTouchListener(new cl(this));
        if (this.t != null) {
            this.t = null;
        }
        this.t = (EditText) findViewById(C0000R.id.h_combinations);
        this.t.setOnTouchListener(new cm(this));
        if (this.u != null) {
            this.u = null;
        }
        this.u = (EditText) findViewById(C0000R.id.h_generalfunctions);
        this.u.setOnTouchListener(new cn(this));
        if (this.v != null) {
            this.v = null;
        }
        this.v = (EditText) findViewById(C0000R.id.h_toxicology);
        this.v.setOnTouchListener(new co(this));
        if (this.w != null) {
            this.f193b = null;
        }
        this.w = (EditText) findViewById(C0000R.id.h_pharmacology);
        this.w.setOnTouchListener(new cd(this));
        if (this.f193b != null) {
            this.f193b = null;
        }
        this.f193b = (EditText) findViewById(C0000R.id.h_notes);
        this.f193b.setOnTouchListener(new cb(this));
        if (this.x != null) {
            this.x = null;
        }
        this.x = (EditText) findViewById(C0000R.id.h_herbherb);
        this.x.setOnTouchListener(new cc(this));
        if (this.y != null) {
            this.y = null;
        }
        this.y = (EditText) findViewById(C0000R.id.h_herbdrug);
        this.y.setOnTouchListener(new ce(this));
        try {
            if (this.E && this.F != null && this.F.length() > 0) {
                Bitmap decodeFile = BitmapFactory.decodeFile(this.F);
                if (decodeFile == null) {
                    this.f.setImageResource(C0000R.drawable.nopicture);
                    return;
                }
                this.f.setImageDrawable(dh.a(decodeFile, TcmAid.cS - 0, TcmAid.cS - 0, getWindow()));
                this.E = this.C;
            }
        } catch (NullPointerException e3) {
            ErrorReporter.a().a("myVariable", this.F);
            ErrorReporter.a().handleSilentException(new NullPointerException("HA:loadUserModifiedData()"));
        }
    }

    public void onDestroy() {
        TcmAid.ab.clear();
        try {
            if (this.H != null && this.H.isOpen()) {
                this.H.close();
                this.H = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
        if (i3 == -1) {
            Uri data = intent.getData();
            Log.i("onActivityResult", data.toString());
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(getContentResolver().openInputStream(data));
                if (decodeStream != null) {
                    BitmapDrawable a2 = dh.a(decodeStream, TcmAid.cS - 0, TcmAid.cS - 0, getWindow());
                    this.f.setImageDrawable(a2);
                    this.E = this.C;
                    this.F = String.format("%s/%s_alt.png", getString(C0000R.string.image_path), this.c);
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(this.F);
                        a2.getBitmap().compress(Bitmap.CompressFormat.PNG, 90, fileOutputStream);
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    } catch (Exception e2) {
                        Log.e("AD:onActivityResult", "Failed to write alternate image: " + e2.getLocalizedMessage());
                    }
                }
            } catch (FileNotFoundException e3) {
                Log.e("HA:onActivityResult", "Failed to process alternate image: " + e3.getLocalizedMessage());
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            this.G = this.C;
            TcmAid.bl = this.C;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.f193b.getWindowToken(), 0);
        String str = this.F;
        boolean z2 = this.C;
        ContentValues contentValues = new ContentValues();
        int b2 = q.b("SELECT MAX(_id) FROM userDB.materiamedica", this.H) + 1;
        if (b2 <= 1000) {
            b2 = 1001;
        }
        contentValues.put("_id", Integer.toString(b2));
        contentValues.put("pinyin", ad.a(this.f192a.getText().toString()));
        contentValues.put("mmcategory", Integer.valueOf(m.d(this.h, this.H)));
        contentValues.put("mmcategory_item_name", this.h);
        contentValues.put("subcategory", this.i.getText().toString());
        contentValues.put("english", this.j.getText().toString());
        contentValues.put("botanical", this.k.getText().toString());
        contentValues.put("meridians", this.l.getText().toString());
        contentValues.put("taste", this.m.getText().toString());
        contentValues.put("temp", this.n.getText().toString());
        contentValues.put("functions", this.o.getText().toString());
        contentValues.put("indications", this.p.getText().toString());
        contentValues.put("contraindication", this.q.getText().toString());
        contentValues.put("dosage", this.r.getText().toString());
        contentValues.put("cautions", this.s.getText().toString());
        contentValues.put("combinations", this.t.getText().toString());
        contentValues.put("general_functions", this.u.getText().toString());
        contentValues.put("toxicology", this.v.getText().toString());
        contentValues.put("pharmacology", this.w.getText().toString());
        contentValues.put("remarks", this.f193b.getText().toString());
        contentValues.put("herb_herb", this.x.getText().toString());
        contentValues.put("herb_drug", this.y.getText().toString());
        contentValues.put("preparation", "");
        if (z2) {
            contentValues.put("alt_image", str);
        }
        try {
            this.H.insert("userDB.materiamedica", null, contentValues);
        } catch (SQLException e2) {
            q.a(e2);
        }
        TcmAid.bl = this.C;
        finish();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0090, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0091, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0097, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00ce, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00d2, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00d3, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00d6, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00e5, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0090 A[ExcHandler: SQLException (e android.database.SQLException), Splitter:B:1:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00d2 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:53:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b() {
        /*
            r7 = this;
            r5 = 0
            r0 = 2131361808(0x7f0a0010, float:1.8343379E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r7.z = r0
            android.widget.ImageButton r0 = r7.z
            com.cyberandsons.tcmaidtrial.additems.df r1 = new com.cyberandsons.tcmaidtrial.additems.df
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            r0 = 2131361810(0x7f0a0012, float:1.8343383E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r7.A = r0
            android.widget.ImageButton r0 = r7.A
            com.cyberandsons.tcmaidtrial.additems.dg r1 = new com.cyberandsons.tcmaidtrial.additems.dg
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            r0 = 2131361809(0x7f0a0011, float:1.834338E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r7.B = r0
            android.widget.ImageButton r0 = r7.B
            com.cyberandsons.tcmaidtrial.additems.dh r1 = new com.cyberandsons.tcmaidtrial.additems.dh
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            java.lang.String r0 = ""
            java.lang.String r1 = ""
            java.lang.String r2 = ""
            com.cyberandsons.tcmaidtrial.a.n r3 = r7.I     // Catch:{ SQLException -> 0x0090, NullPointerException -> 0x009b, all -> 0x00d2 }
            boolean r3 = r3.ag()     // Catch:{ SQLException -> 0x0090, NullPointerException -> 0x009b, all -> 0x00d2 }
            if (r3 == 0) goto L_0x0085
            java.lang.String r2 = " WHERE main.materiamedica.req_state_board=1 "
        L_0x0050:
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.a.m.f(r2)     // Catch:{ SQLException -> 0x0090, NullPointerException -> 0x009b, all -> 0x00d2 }
            com.cyberandsons.tcmaidtrial.TcmAid.ag = r2     // Catch:{ SQLException -> 0x0090, NullPointerException -> 0x009b, all -> 0x00d2 }
            android.database.sqlite.SQLiteDatabase r0 = r7.H     // Catch:{ SQLException -> 0x0090, NullPointerException -> 0x00e4, all -> 0x00d2 }
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.TcmAid.ag     // Catch:{ SQLException -> 0x0090, NullPointerException -> 0x00e4, all -> 0x00d2 }
            r4 = 0
            android.database.Cursor r7 = r0.rawQuery(r3, r4)     // Catch:{ SQLException -> 0x0090, NullPointerException -> 0x00e4, all -> 0x00d2 }
            android.database.sqlite.SQLiteCursor r7 = (android.database.sqlite.SQLiteCursor) r7     // Catch:{ SQLException -> 0x0090, NullPointerException -> 0x00e4, all -> 0x00d2 }
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.ab     // Catch:{ SQLException -> 0x00ea, NullPointerException -> 0x00e7, all -> 0x00da }
            r0.clear()     // Catch:{ SQLException -> 0x00ea, NullPointerException -> 0x00e7, all -> 0x00da }
            boolean r0 = r7.moveToFirst()     // Catch:{ SQLException -> 0x00ea, NullPointerException -> 0x00e7, all -> 0x00da }
            if (r0 == 0) goto L_0x007c
        L_0x006c:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.ab     // Catch:{ SQLException -> 0x00ea, NullPointerException -> 0x00e7, all -> 0x00da }
            r3 = 0
            java.lang.String r3 = r7.getString(r3)     // Catch:{ SQLException -> 0x00ea, NullPointerException -> 0x00e7, all -> 0x00da }
            r0.add(r3)     // Catch:{ SQLException -> 0x00ea, NullPointerException -> 0x00e7, all -> 0x00da }
            boolean r0 = r7.moveToNext()     // Catch:{ SQLException -> 0x00ea, NullPointerException -> 0x00e7, all -> 0x00da }
            if (r0 != 0) goto L_0x006c
        L_0x007c:
            r7.close()     // Catch:{ SQLException -> 0x00ea, NullPointerException -> 0x00e7, all -> 0x00da }
            if (r7 == 0) goto L_0x0084
            r7.close()
        L_0x0084:
            return
        L_0x0085:
            com.cyberandsons.tcmaidtrial.a.n r3 = r7.I     // Catch:{ SQLException -> 0x0090, NullPointerException -> 0x009b, all -> 0x00d2 }
            boolean r3 = r3.ae()     // Catch:{ SQLException -> 0x0090, NullPointerException -> 0x009b, all -> 0x00d2 }
            if (r3 == 0) goto L_0x0050
            java.lang.String r2 = " WHERE main.materiamedica.nccaom_req=1 "
            goto L_0x0050
        L_0x0090:
            r0 = move-exception
            r1 = r5
        L_0x0092:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x00dd }
            if (r1 == 0) goto L_0x0084
            r1.close()
            goto L_0x0084
        L_0x009b:
            r2 = move-exception
            r2 = r0
            r0 = r5
        L_0x009e:
            org.acra.ErrorReporter r3 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x00df }
            java.lang.String r4 = "myVariable"
            r3.a(r4, r2)     // Catch:{ all -> 0x00df }
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x00df }
            java.lang.NullPointerException r3 = new java.lang.NullPointerException     // Catch:{ all -> 0x00df }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00df }
            r4.<init>()     // Catch:{ all -> 0x00df }
            java.lang.String r5 = "HA:load_details( last step completed = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00df }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x00df }
            java.lang.String r4 = " )"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x00df }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00df }
            r3.<init>(r1)     // Catch:{ all -> 0x00df }
            r2.handleSilentException(r3)     // Catch:{ all -> 0x00df }
            if (r0 == 0) goto L_0x0084
            r0.close()
            goto L_0x0084
        L_0x00d2:
            r0 = move-exception
            r1 = r5
        L_0x00d4:
            if (r1 == 0) goto L_0x00d9
            r1.close()
        L_0x00d9:
            throw r0
        L_0x00da:
            r0 = move-exception
            r1 = r7
            goto L_0x00d4
        L_0x00dd:
            r0 = move-exception
            goto L_0x00d4
        L_0x00df:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00d4
        L_0x00e4:
            r0 = move-exception
            r0 = r5
            goto L_0x009e
        L_0x00e7:
            r0 = move-exception
            r0 = r7
            goto L_0x009e
        L_0x00ea:
            r0 = move-exception
            r1 = r7
            goto L_0x0092
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.additems.HerbAdd.b():void");
    }

    public void onItemSelected(AdapterView adapterView, View view, int i2, long j2) {
        this.h = (String) TcmAid.ab.get(i2);
    }

    public void onNothingSelected(AdapterView adapterView) {
    }
}
