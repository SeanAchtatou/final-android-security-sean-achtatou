package com.cyberandsons.tcmaidtrial.additems;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class br implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointsAdd f247a;

    br(PointsAdd pointsAdd) {
        this.f247a = pointsAdd;
    }

    public final void onClick(View view) {
        PointsAdd pointsAdd = this.f247a;
        ((InputMethodManager) pointsAdd.getSystemService("input_method")).hideSoftInputFromWindow(pointsAdd.f195b.getWindowToken(), 0);
        pointsAdd.finish();
    }
}
