package com.cyberandsons.tcmaidtrial.additems;

import android.app.AlertDialog;
import android.content.Intent;
import android.provider.MediaStore;
import android.view.View;

final class p implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TungAdd f314a;

    p(TungAdd tungAdd) {
        this.f314a = tungAdd;
    }

    public final void onClick(View view) {
        TungAdd tungAdd = this.f314a;
        tungAdd.c = tungAdd.f200a.getText().toString();
        if (tungAdd.c == null || tungAdd.c.length() == 0) {
            AlertDialog create = new AlertDialog.Builder(tungAdd).create();
            create.setTitle("Attention:");
            create.setMessage("Please enter a point name before selecting an image.");
            create.setButton("OK", new j(tungAdd));
            create.show();
            return;
        }
        tungAdd.startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 0);
    }
}
