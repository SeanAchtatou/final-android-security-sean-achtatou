package com.cyberandsons.tcmaidtrial.additems;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class l implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TungAdd f310a;

    l(TungAdd tungAdd) {
        this.f310a = tungAdd;
    }

    public final void onClick(View view) {
        TungAdd tungAdd = this.f310a;
        ((InputMethodManager) tungAdd.getSystemService("input_method")).hideSoftInputFromWindow(tungAdd.f201b.getWindowToken(), 0);
        tungAdd.finish();
    }
}
