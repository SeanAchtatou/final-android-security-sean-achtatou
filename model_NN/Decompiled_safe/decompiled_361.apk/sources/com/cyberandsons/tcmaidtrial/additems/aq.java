package com.cyberandsons.tcmaidtrial.additems;

import android.view.MotionEvent;
import android.view.View;

final class aq implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiagnosisAdd f219a;

    aq(DiagnosisAdd diagnosisAdd) {
        this.f219a = diagnosisAdd;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f219a.f189b;
    }
}
