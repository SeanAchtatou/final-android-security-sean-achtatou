package com.cyberandsons.tcmaidtrial.additems;

import android.content.DialogInterface;

final class aj implements DialogInterface.OnMultiChoiceClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SixStagesAdd f212a;

    aj(SixStagesAdd sixStagesAdd) {
        this.f212a = sixStagesAdd;
    }

    public final void onClick(DialogInterface dialogInterface, int i, boolean z) {
        if (z) {
            this.f212a.e.add(this.f212a.f[i]);
        } else {
            this.f212a.e.remove(this.f212a.f[i]);
        }
        this.f212a.c();
    }
}
