package com.cyberandsons.tcmaidtrial.additems;

import android.app.AlertDialog;
import android.content.Intent;
import android.provider.MediaStore;
import android.view.View;

final class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AuricularAdd f283a;

    d(AuricularAdd auricularAdd) {
        this.f283a = auricularAdd;
    }

    public final void onClick(View view) {
        AuricularAdd auricularAdd = this.f283a;
        auricularAdd.c = auricularAdd.f186a.getText().toString();
        if (auricularAdd.c == null || auricularAdd.c.length() == 0) {
            AlertDialog create = new AlertDialog.Builder(auricularAdd).create();
            create.setTitle("Attention:");
            create.setMessage("Please enter a point name before selecting an image.");
            create.setButton("OK", new i(auricularAdd));
            create.show();
            return;
        }
        auricularAdd.startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 0);
    }
}
