package com.cyberandsons.tcmaidtrial.additems;

import android.content.DialogInterface;

final class cj implements DialogInterface.OnMultiChoiceClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiagnosisAdd f266a;

    cj(DiagnosisAdd diagnosisAdd) {
        this.f266a = diagnosisAdd;
    }

    public final void onClick(DialogInterface dialogInterface, int i, boolean z) {
        if (z) {
            this.f266a.e.add(this.f266a.f[i]);
        } else {
            this.f266a.e.remove(this.f266a.f[i]);
        }
        this.f266a.c();
    }
}
