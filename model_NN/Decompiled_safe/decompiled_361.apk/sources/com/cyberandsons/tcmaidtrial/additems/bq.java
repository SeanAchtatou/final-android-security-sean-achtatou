package com.cyberandsons.tcmaidtrial.additems;

import android.app.AlertDialog;
import android.content.Intent;
import android.provider.MediaStore;
import android.view.View;

final class bq implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointsAdd f246a;

    bq(PointsAdd pointsAdd) {
        this.f246a = pointsAdd;
    }

    public final void onClick(View view) {
        PointsAdd pointsAdd = this.f246a;
        pointsAdd.c = pointsAdd.f194a.getText().toString();
        if (pointsAdd.c == null || pointsAdd.c.length() == 0) {
            AlertDialog create = new AlertDialog.Builder(pointsAdd).create();
            create.setTitle("Attention:");
            create.setMessage("Please enter a point name before selecting an image.");
            create.setButton("OK", new bp(pointsAdd));
            create.show();
            return;
        }
        pointsAdd.startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 0);
    }
}
