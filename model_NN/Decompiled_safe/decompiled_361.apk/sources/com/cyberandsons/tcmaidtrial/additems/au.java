package com.cyberandsons.tcmaidtrial.additems;

import android.view.MotionEvent;
import android.view.View;

final class au implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiagnosisAdd f223a;

    au(DiagnosisAdd diagnosisAdd) {
        this.f223a = diagnosisAdd;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f223a.f189b;
    }
}
