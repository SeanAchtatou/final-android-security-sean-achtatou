package com.cyberandsons.tcmaidtrial.additems;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class g implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AuricularAdd f305a;

    g(AuricularAdd auricularAdd) {
        this.f305a = auricularAdd;
    }

    public final void onClick(View view) {
        AuricularAdd auricularAdd = this.f305a;
        ((InputMethodManager) auricularAdd.getSystemService("input_method")).hideSoftInputFromWindow(auricularAdd.f187b.getWindowToken(), 0);
        auricularAdd.finish();
    }
}
