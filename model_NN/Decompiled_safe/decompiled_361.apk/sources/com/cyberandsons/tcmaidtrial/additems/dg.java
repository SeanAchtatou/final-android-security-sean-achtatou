package com.cyberandsons.tcmaidtrial.additems;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class dg implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HerbAdd f290a;

    dg(HerbAdd herbAdd) {
        this.f290a = herbAdd;
    }

    public final void onClick(View view) {
        HerbAdd herbAdd = this.f290a;
        ((InputMethodManager) herbAdd.getSystemService("input_method")).hideSoftInputFromWindow(herbAdd.f193b.getWindowToken(), 0);
        herbAdd.finish();
    }
}
