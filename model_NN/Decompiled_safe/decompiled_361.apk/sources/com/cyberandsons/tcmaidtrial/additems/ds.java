package com.cyberandsons.tcmaidtrial.additems;

import android.app.AlertDialog;
import android.content.Intent;
import android.provider.MediaStore;
import android.view.View;

final class ds implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PulseDiagAdd f302a;

    ds(PulseDiagAdd pulseDiagAdd) {
        this.f302a = pulseDiagAdd;
    }

    public final void onClick(View view) {
        PulseDiagAdd pulseDiagAdd = this.f302a;
        pulseDiagAdd.c = pulseDiagAdd.f196a.getText().toString();
        if (pulseDiagAdd.c == null || pulseDiagAdd.c.length() == 0) {
            AlertDialog create = new AlertDialog.Builder(pulseDiagAdd).create();
            create.setTitle("Attention:");
            create.setMessage("Please enter a pulse name before selecting an image.");
            create.setButton("OK", new Cdo(pulseDiagAdd));
            create.show();
            return;
        }
        pulseDiagAdd.startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 0);
    }
}
