package com.cyberandsons.tcmaidtrial.additems;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class ak implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DiagnosisAdd f213a;

    ak(DiagnosisAdd diagnosisAdd) {
        this.f213a = diagnosisAdd;
    }

    public final void onClick(View view) {
        DiagnosisAdd diagnosisAdd = this.f213a;
        ((InputMethodManager) diagnosisAdd.getSystemService("input_method")).hideSoftInputFromWindow(diagnosisAdd.f188a.getWindowToken(), 0);
        diagnosisAdd.finish();
    }
}
