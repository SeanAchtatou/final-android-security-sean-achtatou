package com.cyberandsons.tcmaidtrial.additems;

import android.app.AlertDialog;
import android.content.Intent;
import android.provider.MediaStore;
import android.view.View;

final class dh implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HerbAdd f291a;

    dh(HerbAdd herbAdd) {
        this.f291a = herbAdd;
    }

    public final void onClick(View view) {
        HerbAdd herbAdd = this.f291a;
        herbAdd.c = herbAdd.f192a.getText().toString();
        if (herbAdd.c == null || herbAdd.c.length() == 0) {
            AlertDialog create = new AlertDialog.Builder(herbAdd).create();
            create.setTitle("Attention:");
            create.setMessage("Please enter an herb name before selecting an image.");
            create.setButton("OK", new de(herbAdd));
            create.show();
            return;
        }
        herbAdd.startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 0);
    }
}
