package com.cyberandsons.tcmaidtrial.additems;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.c;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.misc.ad;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class FormulasAdd extends Activity implements AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    EditText f190a;

    /* renamed from: b  reason: collision with root package name */
    boolean f191b;
    private ScrollView c;
    private EditText d;
    private Spinner e;
    private String f;
    private EditText g;
    private EditText h;
    private EditText i;
    private EditText j;
    private EditText k;
    private EditText l;
    private EditText m;
    private EditText n;
    private EditText o;
    private EditText p;
    private EditText q;
    private ImageButton r;
    private ImageButton s;
    private ImageButton t;
    private boolean u = true;
    private boolean v;
    private boolean w;
    private SQLiteDatabase x;
    private n y;

    public FormulasAdd() {
        this.v = !this.u;
        this.f191b = this.v;
        this.w = this.v;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.x == null || !this.x.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("FA:openDataBase()", str + " opened.");
                this.x = SQLiteDatabase.openDatabase(str, null, 16);
                this.x.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.x.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("FA:openDataBase()", str2 + " ATTACHed.");
                this.y = new n();
                this.y.a(this.x);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new ar(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        setContentView((int) C0000R.layout.formulas_detailed_add);
        this.c = (ScrollView) findViewById(C0000R.id.svDetail);
        this.c.smoothScrollTo(0, 0);
        getWindow().setSoftInputMode(3);
        b();
        if (this.d != null) {
            this.d = null;
        }
        this.d = (EditText) findViewById(C0000R.id.f_name);
        this.d.setOnTouchListener(new bb(this));
        this.e = (Spinner) findViewById(C0000R.id.f_category);
        if (this.g != null) {
            this.g = null;
        }
        this.g = (EditText) findViewById(C0000R.id.f_english);
        this.g.setOnTouchListener(new ba(this));
        if (this.h != null) {
            this.h = null;
        }
        this.h = (EditText) findViewById(C0000R.id.f_diagnosis);
        this.h.setOnTouchListener(new bd(this));
        if (this.i != null) {
            this.i = null;
        }
        this.i = (EditText) findViewById(C0000R.id.f_indications);
        this.i.setOnTouchListener(new bc(this));
        if (this.j != null) {
            this.j = null;
        }
        this.j = (EditText) findViewById(C0000R.id.f_functions);
        this.j.setOnTouchListener(new bg(this));
        if (this.k != null) {
            this.k = null;
        }
        this.k = (EditText) findViewById(C0000R.id.f_tongue);
        this.k.setOnTouchListener(new bh(this));
        if (this.l != null) {
            this.l = null;
        }
        this.l = (EditText) findViewById(C0000R.id.f_pulse);
        this.l.setOnTouchListener(new bi(this));
        if (this.m != null) {
            this.m = null;
        }
        this.m = (EditText) findViewById(C0000R.id.f_ingredients);
        this.m.setOnTouchListener(new an(this));
        if (this.n != null) {
            this.n = null;
        }
        this.n = (EditText) findViewById(C0000R.id.f_herbbreakdown);
        this.n.setOnTouchListener(new al(this));
        if (this.o != null) {
            this.o = null;
        }
        this.o = (EditText) findViewById(C0000R.id.f_modification);
        this.o.setOnTouchListener(new az(this));
        if (this.p != null) {
            this.p = null;
        }
        this.p = (EditText) findViewById(C0000R.id.f_contraindications);
        this.p.setOnTouchListener(new ax(this));
        if (this.q != null) {
            this.q = null;
        }
        this.q = (EditText) findViewById(C0000R.id.f_westernusage);
        this.q.setOnTouchListener(new av(this));
        if (this.f190a != null) {
            this.f190a = null;
        }
        this.f190a = (EditText) findViewById(C0000R.id.f_notes);
        this.f190a.setOnTouchListener(new at(this));
    }

    public void onDestroy() {
        TcmAid.O.clear();
        try {
            if (this.x != null && this.x.isOpen()) {
                this.x.close();
                this.x = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            this.w = this.u;
            TcmAid.bl = this.u;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.f190a.getWindowToken(), 0);
        ContentValues contentValues = new ContentValues();
        int b2 = q.b("SELECT MAX(_id) FROM userDB.formulas", this.x) + 1;
        if (b2 <= 1000) {
            b2 = 1001;
        }
        contentValues.put("_id", Integer.toString(b2));
        contentValues.put("pinyin", ad.a(this.d.getText().toString()));
        contentValues.put("fcategory", Integer.valueOf(c.b(this.f, this.x)));
        contentValues.put("english", this.g.getText().toString());
        contentValues.put("diagnosis", this.h.getText().toString());
        contentValues.put("symptoms", this.i.getText().toString());
        contentValues.put("functions", this.j.getText().toString());
        contentValues.put("tongue", this.k.getText().toString());
        contentValues.put("pulse", this.l.getText().toString());
        contentValues.put("ingredients", this.m.getText().toString());
        contentValues.put("herb_breakdown", this.n.getText().toString());
        contentValues.put("modification", this.o.getText().toString());
        contentValues.put("contraindication", this.p.getText().toString());
        contentValues.put("western_USE", this.q.getText().toString());
        contentValues.put("notes", this.f190a.getText().toString());
        try {
            this.x.insert("userDB.formulas", null, contentValues);
        } catch (SQLException e2) {
            q.a(e2);
        }
        TcmAid.bl = this.u;
        finish();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0074, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0075, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007b, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0080, code lost:
        r2 = "";
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00b2, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00b6, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00b7, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0074 A[ExcHandler: SQLException (e android.database.SQLException), Splitter:B:1:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00b6 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:44:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b() {
        /*
            r7 = this;
            r5 = 0
            r0 = 2131361808(0x7f0a0010, float:1.8343379E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r7.r = r0
            android.widget.ImageButton r0 = r7.r
            com.cyberandsons.tcmaidtrial.additems.bf r1 = new com.cyberandsons.tcmaidtrial.additems.bf
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            r0 = 2131361810(0x7f0a0012, float:1.8343383E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r7.s = r0
            android.widget.ImageButton r0 = r7.s
            com.cyberandsons.tcmaidtrial.additems.be r1 = new com.cyberandsons.tcmaidtrial.additems.be
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            r0 = 2131361809(0x7f0a0011, float:1.834338E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r7.t = r0
            android.widget.ImageButton r0 = r7.t
            r1 = 8
            r0.setVisibility(r1)
            java.lang.String r0 = ""
            java.lang.String r1 = ""
            java.lang.String r2 = "SELECT item_name FROM main.majorFcategory ORDER by item_name"
            com.cyberandsons.tcmaidtrial.TcmAid.T = r2     // Catch:{ SQLException -> 0x0074, NullPointerException -> 0x007f, all -> 0x00b6 }
            android.database.sqlite.SQLiteDatabase r0 = r7.x     // Catch:{ SQLException -> 0x0074, NullPointerException -> 0x00c8, all -> 0x00b6 }
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.TcmAid.T     // Catch:{ SQLException -> 0x0074, NullPointerException -> 0x00c8, all -> 0x00b6 }
            r4 = 0
            android.database.Cursor r7 = r0.rawQuery(r3, r4)     // Catch:{ SQLException -> 0x0074, NullPointerException -> 0x00c8, all -> 0x00b6 }
            android.database.sqlite.SQLiteCursor r7 = (android.database.sqlite.SQLiteCursor) r7     // Catch:{ SQLException -> 0x0074, NullPointerException -> 0x00c8, all -> 0x00b6 }
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.O     // Catch:{ SQLException -> 0x00ce, NullPointerException -> 0x00cb, all -> 0x00be }
            r0.clear()     // Catch:{ SQLException -> 0x00ce, NullPointerException -> 0x00cb, all -> 0x00be }
            boolean r0 = r7.moveToFirst()     // Catch:{ SQLException -> 0x00ce, NullPointerException -> 0x00cb, all -> 0x00be }
            if (r0 == 0) goto L_0x006b
        L_0x005b:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.O     // Catch:{ SQLException -> 0x00ce, NullPointerException -> 0x00cb, all -> 0x00be }
            r3 = 0
            java.lang.String r3 = r7.getString(r3)     // Catch:{ SQLException -> 0x00ce, NullPointerException -> 0x00cb, all -> 0x00be }
            r0.add(r3)     // Catch:{ SQLException -> 0x00ce, NullPointerException -> 0x00cb, all -> 0x00be }
            boolean r0 = r7.moveToNext()     // Catch:{ SQLException -> 0x00ce, NullPointerException -> 0x00cb, all -> 0x00be }
            if (r0 != 0) goto L_0x005b
        L_0x006b:
            r7.close()     // Catch:{ SQLException -> 0x00ce, NullPointerException -> 0x00cb, all -> 0x00be }
            if (r7 == 0) goto L_0x0073
            r7.close()
        L_0x0073:
            return
        L_0x0074:
            r0 = move-exception
            r1 = r5
        L_0x0076:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x00c1 }
            if (r1 == 0) goto L_0x0073
            r1.close()
            goto L_0x0073
        L_0x007f:
            r2 = move-exception
            r2 = r0
            r0 = r5
        L_0x0082:
            org.acra.ErrorReporter r3 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x00c3 }
            java.lang.String r4 = "myVariable"
            r3.a(r4, r2)     // Catch:{ all -> 0x00c3 }
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x00c3 }
            java.lang.NullPointerException r3 = new java.lang.NullPointerException     // Catch:{ all -> 0x00c3 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c3 }
            r4.<init>()     // Catch:{ all -> 0x00c3 }
            java.lang.String r5 = "FA:load_details( last step completed = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00c3 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x00c3 }
            java.lang.String r4 = " )"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x00c3 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00c3 }
            r3.<init>(r1)     // Catch:{ all -> 0x00c3 }
            r2.handleSilentException(r3)     // Catch:{ all -> 0x00c3 }
            if (r0 == 0) goto L_0x0073
            r0.close()
            goto L_0x0073
        L_0x00b6:
            r0 = move-exception
            r1 = r5
        L_0x00b8:
            if (r1 == 0) goto L_0x00bd
            r1.close()
        L_0x00bd:
            throw r0
        L_0x00be:
            r0 = move-exception
            r1 = r7
            goto L_0x00b8
        L_0x00c1:
            r0 = move-exception
            goto L_0x00b8
        L_0x00c3:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00b8
        L_0x00c8:
            r0 = move-exception
            r0 = r5
            goto L_0x0082
        L_0x00cb:
            r0 = move-exception
            r0 = r7
            goto L_0x0082
        L_0x00ce:
            r0 = move-exception
            r1 = r7
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.additems.FormulasAdd.b():void");
    }

    public void onItemSelected(AdapterView adapterView, View view, int i2, long j2) {
        this.f = (String) TcmAid.O.get(i2);
    }

    public void onNothingSelected(AdapterView adapterView) {
    }
}
