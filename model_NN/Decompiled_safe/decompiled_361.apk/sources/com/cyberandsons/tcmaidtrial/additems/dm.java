package com.cyberandsons.tcmaidtrial.additems;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class dm implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PulseDiagAdd f296a;

    dm(PulseDiagAdd pulseDiagAdd) {
        this.f296a = pulseDiagAdd;
    }

    public final void onClick(View view) {
        PulseDiagAdd pulseDiagAdd = this.f296a;
        ((InputMethodManager) pulseDiagAdd.getSystemService("input_method")).hideSoftInputFromWindow(pulseDiagAdd.f197b.getWindowToken(), 0);
        pulseDiagAdd.finish();
    }
}
