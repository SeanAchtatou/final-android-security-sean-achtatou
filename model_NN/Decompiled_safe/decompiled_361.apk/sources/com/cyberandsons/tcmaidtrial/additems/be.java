package com.cyberandsons.tcmaidtrial.additems;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

final class be implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FormulasAdd f234a;

    be(FormulasAdd formulasAdd) {
        this.f234a = formulasAdd;
    }

    public final void onClick(View view) {
        FormulasAdd formulasAdd = this.f234a;
        ((InputMethodManager) formulasAdd.getSystemService("input_method")).hideSoftInputFromWindow(formulasAdd.f190a.getWindowToken(), 0);
        formulasAdd.finish();
    }
}
