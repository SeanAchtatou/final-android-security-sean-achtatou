package com.cyberandsons.tcmaidtrial.additems;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.misc.ad;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.util.ArrayList;
import java.util.Iterator;

public class SixStagesAdd extends Activity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    EditText f198a;

    /* renamed from: b  reason: collision with root package name */
    boolean f199b;
    protected ArrayList c;
    CharSequence[] d;
    protected ArrayList e;
    CharSequence[] f;
    private ScrollView g;
    private EditText h;
    private EditText i;
    private EditText j;
    private EditText k;
    private EditText l;
    private Button m;
    private Button n;
    private ImageButton o;
    private ImageButton p;
    private ImageButton q;
    private boolean r = true;
    private boolean s;
    private boolean t;
    private boolean u;
    private SQLiteDatabase v;
    private n w;

    public SixStagesAdd() {
        this.s = !this.r;
        this.t = this.s;
        this.f199b = this.s;
        this.u = this.s;
        this.c = new ArrayList();
        this.e = new ArrayList();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.v == null || !this.v.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("SSA:openDataBase()", str + " opened.");
                this.v = SQLiteDatabase.openDatabase(str, null, 16);
                this.v.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.v.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("SSA:openDataBase()", str2 + " ATTACHed.");
                this.w = new n();
                this.w.a(this.v);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new bu(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        setContentView((int) C0000R.layout.sixstages_detailed_add);
        this.g = (ScrollView) findViewById(C0000R.id.svDetail);
        this.g.smoothScrollTo(0, 0);
        getWindow().setSoftInputMode(3);
        d();
        if (this.h != null) {
            this.h = null;
        }
        this.h = (EditText) findViewById(C0000R.id.ss_name);
        this.h.setOnTouchListener(new bz(this));
        if (this.j != null) {
            this.j = null;
        }
        this.j = (EditText) findViewById(C0000R.id.ss_description);
        this.j.setOnTouchListener(new ca(this));
        if (this.i != null) {
            this.i = null;
        }
        this.i = (EditText) findViewById(C0000R.id.ss_indications);
        this.i.setOnTouchListener(new bx(this));
        if (this.k != null) {
            this.k = null;
        }
        this.k = (EditText) findViewById(C0000R.id.ss_tongue);
        this.k.setOnTouchListener(new by(this));
        if (this.l != null) {
            this.l = null;
        }
        this.l = (EditText) findViewById(C0000R.id.ss_pulse);
        this.l.setOnTouchListener(new bv(this));
        if (this.m != null) {
            this.m = null;
        }
        this.m = (Button) findViewById(C0000R.id.ss_points);
        this.m.setOnClickListener(this);
        if (this.n != null) {
            this.n = null;
        }
        this.n = (Button) findViewById(C0000R.id.ss_formulas);
        this.n.setOnClickListener(this);
        if (this.f198a != null) {
            this.f198a = null;
        }
        this.f198a = (EditText) findViewById(C0000R.id.ss_notes);
        this.f198a.setOnTouchListener(new bw(this));
    }

    public void onDestroy() {
        this.e.clear();
        this.c.clear();
        try {
            if (this.v != null && this.v.isOpen()) {
                this.v.close();
                this.v = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            this.u = this.r;
            TcmAid.bl = this.r;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.f198a.getWindowToken(), 0);
        ContentValues contentValues = new ContentValues();
        int b2 = q.b("SELECT MAX(_id) FROM userDB.sixchannels", this.v) + 1;
        if (b2 <= 1000) {
            b2 = 1001;
        }
        contentValues.put("_id", Integer.toString(b2));
        contentValues.put("pathology", ad.a(this.h.getText().toString()));
        contentValues.put("symptoms", this.i.getText().toString());
        contentValues.put("english", this.j.getText().toString());
        contentValues.put("tongue", this.k.getText().toString());
        contentValues.put("pulse", this.l.getText().toString());
        contentValues.put("points", this.m.getText().toString());
        contentValues.put("formula", this.n.getText().toString());
        contentValues.put("notes", this.f198a.getText().toString());
        try {
            this.v.insert("userDB.sixchannels", null, contentValues);
        } catch (SQLException e2) {
            q.a(e2);
        }
        TcmAid.bl = this.r;
        finish();
    }

    /* JADX WARN: Type inference failed for: r2v22, types: [android.database.Cursor] */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00c6, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00c7, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        com.cyberandsons.tcmaidtrial.a.q.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00cb, code lost:
        if (r2 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00cd, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00d2, code lost:
        r3 = "";
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0104, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0108, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0109, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0115, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x011d, code lost:
        r2 = "Pre-rawQuery";
        r3 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0123, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0124, code lost:
        r10 = r2;
        r2 = r1;
        r1 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c6 A[ExcHandler: SQLException (e android.database.SQLException), Splitter:B:1:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0108 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0123 A[ExcHandler: SQLException (r2v16 'e' android.database.SQLException A[CUSTOM_DECLARE]), Splitter:B:5:0x0054] */
    /* JADX WARNING: Removed duplicated region for block: B:63:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:65:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d() {
        /*
            r11 = this;
            r9 = 0
            r5 = 0
            r8 = 1
            r1 = 2131361808(0x7f0a0010, float:1.8343379E38)
            android.view.View r1 = r11.findViewById(r1)
            android.widget.ImageButton r1 = (android.widget.ImageButton) r1
            r11.o = r1
            android.widget.ImageButton r1 = r11.o
            com.cyberandsons.tcmaidtrial.additems.bt r2 = new com.cyberandsons.tcmaidtrial.additems.bt
            r2.<init>(r11)
            r1.setOnClickListener(r2)
            r1 = 2131361810(0x7f0a0012, float:1.8343383E38)
            android.view.View r1 = r11.findViewById(r1)
            android.widget.ImageButton r1 = (android.widget.ImageButton) r1
            r11.p = r1
            android.widget.ImageButton r1 = r11.p
            com.cyberandsons.tcmaidtrial.additems.bs r2 = new com.cyberandsons.tcmaidtrial.additems.bs
            r2.<init>(r11)
            r1.setOnClickListener(r2)
            r1 = 2131361809(0x7f0a0011, float:1.834338E38)
            android.view.View r1 = r11.findViewById(r1)
            android.widget.ImageButton r1 = (android.widget.ImageButton) r1
            r11.q = r1
            android.widget.ImageButton r1 = r11.q
            r2 = 8
            r1.setVisibility(r2)
            java.lang.String r1 = ""
            java.lang.String r2 = ""
            com.cyberandsons.tcmaidtrial.a.n r3 = r11.w     // Catch:{ SQLException -> 0x00c6, NullPointerException -> 0x00d1, all -> 0x0108 }
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.a.a.a(r3)     // Catch:{ SQLException -> 0x00c6, NullPointerException -> 0x00d1, all -> 0x0108 }
            java.lang.String r2 = "Pre-rawQuery"
            android.database.sqlite.SQLiteDatabase r1 = r11.v     // Catch:{ SQLException -> 0x00c6, NullPointerException -> 0x0117, all -> 0x0108 }
            r4 = 0
            android.database.Cursor r1 = r1.rawQuery(r3, r4)     // Catch:{ SQLException -> 0x00c6, NullPointerException -> 0x0117, all -> 0x0108 }
            android.database.sqlite.SQLiteCursor r1 = (android.database.sqlite.SQLiteCursor) r1     // Catch:{ SQLException -> 0x00c6, NullPointerException -> 0x0117, all -> 0x0108 }
            int r4 = r1.getCount()     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011a }
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011a }
            r11.d = r4     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011a }
            java.lang.String r2 = "Pre-cursor loop"
            boolean r4 = r1.moveToFirst()     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011a }
            if (r4 == 0) goto L_0x0076
            r4 = r9
        L_0x0065:
            java.lang.CharSequence[] r5 = r11.d     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011a }
            int r6 = r4 + 1
            r7 = 1
            java.lang.String r7 = r1.getString(r7)     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011a }
            r5[r4] = r7     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011a }
            boolean r4 = r1.moveToNext()     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011a }
            if (r4 != 0) goto L_0x012a
        L_0x0076:
            r1.close()     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011a }
            com.cyberandsons.tcmaidtrial.a.n r4 = r11.w     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011a }
            int r4 = r4.N()     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011a }
            if (r4 == 0) goto L_0x0083
            if (r4 != r8) goto L_0x00c3
        L_0x0083:
            java.lang.String r4 = "pinyin"
        L_0x0085:
            java.lang.String r5 = ""
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.a.c.a(r5, r4)     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011a }
            java.lang.String r3 = "Pre-rawQuery"
            android.database.sqlite.SQLiteDatabase r2 = r11.v     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011c }
            r5 = 0
            android.database.Cursor r2 = r2.rawQuery(r4, r5)     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011c }
            r0 = r2
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011c }
            r1 = r0
            int r2 = r1.getCount()     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011c }
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011c }
            r11.f = r2     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x011c }
            java.lang.String r2 = "Pre-cursor loop"
            boolean r3 = r1.moveToFirst()     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x0120 }
            if (r3 == 0) goto L_0x00ba
            r3 = r9
        L_0x00a9:
            java.lang.CharSequence[] r5 = r11.f     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x0120 }
            int r6 = r3 + 1
            r7 = 1
            java.lang.String r7 = r1.getString(r7)     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x0120 }
            r5[r3] = r7     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x0120 }
            boolean r3 = r1.moveToNext()     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x0120 }
            if (r3 != 0) goto L_0x0128
        L_0x00ba:
            r1.close()     // Catch:{ SQLException -> 0x0123, NullPointerException -> 0x0120 }
            if (r1 == 0) goto L_0x00c2
            r1.close()
        L_0x00c2:
            return
        L_0x00c3:
            java.lang.String r4 = "english"
            goto L_0x0085
        L_0x00c6:
            r1 = move-exception
            r2 = r5
        L_0x00c8:
            com.cyberandsons.tcmaidtrial.a.q.a(r1)     // Catch:{ all -> 0x0115 }
            if (r2 == 0) goto L_0x00c2
            r2.close()
            goto L_0x00c2
        L_0x00d1:
            r3 = move-exception
            r3 = r1
            r1 = r5
        L_0x00d4:
            org.acra.ErrorReporter r4 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0110 }
            java.lang.String r5 = "myVariable"
            r4.a(r5, r3)     // Catch:{ all -> 0x0110 }
            org.acra.ErrorReporter r3 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0110 }
            java.lang.NullPointerException r4 = new java.lang.NullPointerException     // Catch:{ all -> 0x0110 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0110 }
            r5.<init>()     // Catch:{ all -> 0x0110 }
            java.lang.String r6 = "AA:load_details( last step completed = "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0110 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ all -> 0x0110 }
            java.lang.String r5 = " )"
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ all -> 0x0110 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0110 }
            r4.<init>(r2)     // Catch:{ all -> 0x0110 }
            r3.handleSilentException(r4)     // Catch:{ all -> 0x0110 }
            if (r1 == 0) goto L_0x00c2
            r1.close()
            goto L_0x00c2
        L_0x0108:
            r1 = move-exception
            r2 = r5
        L_0x010a:
            if (r2 == 0) goto L_0x010f
            r2.close()
        L_0x010f:
            throw r1
        L_0x0110:
            r2 = move-exception
            r10 = r2
            r2 = r1
            r1 = r10
            goto L_0x010a
        L_0x0115:
            r1 = move-exception
            goto L_0x010a
        L_0x0117:
            r1 = move-exception
            r1 = r5
            goto L_0x00d4
        L_0x011a:
            r4 = move-exception
            goto L_0x00d4
        L_0x011c:
            r2 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x00d4
        L_0x0120:
            r3 = move-exception
            r3 = r4
            goto L_0x00d4
        L_0x0123:
            r2 = move-exception
            r10 = r2
            r2 = r1
            r1 = r10
            goto L_0x00c8
        L_0x0128:
            r3 = r6
            goto L_0x00a9
        L_0x012a:
            r4 = r6
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.additems.SixStagesAdd.d():void");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        StringBuilder sb = new StringBuilder();
        boolean z = this.r;
        Iterator it = this.c.iterator();
        boolean z2 = z;
        while (it.hasNext()) {
            CharSequence charSequence = (CharSequence) it.next();
            if (z2) {
                sb.append(charSequence);
                z2 = this.s;
            } else {
                sb.append(", " + ((Object) charSequence));
            }
        }
        this.m.setText(sb.toString());
    }

    private void e() {
        boolean[] zArr = new boolean[this.f.length];
        int length = this.f.length;
        for (int i2 = 0; i2 < length; i2++) {
            zArr[i2] = this.e.contains(this.f[i2]);
        }
        aj ajVar = new aj(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Formulas");
        builder.setMultiChoiceItems(this.f, zArr, ajVar);
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public final void c() {
        StringBuilder sb = new StringBuilder();
        boolean z = this.r;
        Iterator it = this.e.iterator();
        boolean z2 = z;
        while (it.hasNext()) {
            CharSequence charSequence = (CharSequence) it.next();
            if (z2) {
                sb.append(charSequence);
                z2 = this.s;
            } else {
                sb.append(", " + ((Object) charSequence));
            }
        }
        this.n.setText(sb.toString());
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.ss_points /*2131362320*/:
                boolean[] zArr = new boolean[this.d.length];
                int length = this.d.length;
                for (int i2 = 0; i2 < length; i2++) {
                    zArr[i2] = this.c.contains(this.d[i2]);
                }
                ai aiVar = new ai(this);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Select Points");
                builder.setMultiChoiceItems(this.d, zArr, aiVar);
                builder.create().show();
                return;
            case C0000R.id.ss_FormulaLbl /*2131362321*/:
            default:
                return;
            case C0000R.id.ss_formulas /*2131362322*/:
                e();
                return;
        }
    }
}
