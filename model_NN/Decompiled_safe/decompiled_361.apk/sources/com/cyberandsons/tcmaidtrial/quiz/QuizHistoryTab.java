package com.cyberandsons.tcmaidtrial.quiz;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class QuizHistoryTab extends ListActivity {
    private static String l;
    private static String m;
    private static String p = "SELECT _id, date, CASE area WHEN 0 THEN 'Auricular' WHEN 1 THEN 'Diagnosis' WHEN 2 THEN 'Herbs' WHEN 3 THEN 'Formulas' WHEN 4 THEN 'Points' ELSE 'PulseDiag' END, correct, questions, type from userDB.quizsessions";
    private static boolean q;

    /* renamed from: a  reason: collision with root package name */
    ImageButton f1039a;

    /* renamed from: b  reason: collision with root package name */
    ListView f1040b;
    private final String c = "userDB.quizsessions";
    private final String d = "_id";
    private final String e = "date";
    private final String f = "area";
    private final String g = "correct";
    private final String h = "questions";
    private final String i = "type";
    private String j;
    private String[] k;
    private final String n = "date";
    private final String o = "date";
    private boolean r;
    /* access modifiers changed from: private */
    public SQLiteCursor s;
    /* access modifiers changed from: private */
    public SQLiteDatabase t;
    private n u;
    private boolean v = true;
    private boolean w;

    public QuizHistoryTab() {
        boolean z;
        if (!this.v) {
            z = true;
        } else {
            z = false;
        }
        this.w = z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0125  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r8) {
        /*
            r7 = this;
            r3 = 0
            r2 = 4
            r5 = 1
            super.onCreate(r8)
            r7.d()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.d
            if (r0 == 0) goto L_0x0010
            r7.setRequestedOrientation(r5)
        L_0x0010:
            r0 = 2130903081(0x7f030029, float:1.741297E38)
            r7.setContentView(r0)
            r0 = 2131361903(0x7f0a006f, float:1.8343571E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.String r1 = "Quiz History"
            r0.setText(r1)
            java.lang.String r0 = "SELECT MAX(userDB.quizsessions._id) FROM userDB.quizsessions "
            android.database.sqlite.SQLiteDatabase r1 = r7.t
            int r1 = com.cyberandsons.tcmaidtrial.a.q.a(r0, r1)
            r0 = 16908298(0x102000a, float:2.3877257E-38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.ListView r0 = (android.widget.ListView) r0
            r7.f1040b = r0
            r0 = 2131361843(0x7f0a0033, float:1.834345E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            r7.f1039a = r0
            if (r1 > 0) goto L_0x004e
            android.widget.ImageButton r0 = r7.f1039a
            r0.setVisibility(r2)
            android.widget.ListView r0 = r7.f1040b
            r0.setVisibility(r2)
        L_0x004e:
            android.widget.ImageButton r0 = r7.f1039a
            com.cyberandsons.tcmaidtrial.quiz.g r1 = new com.cyberandsons.tcmaidtrial.quiz.g
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            boolean r0 = r7.r
            if (r0 == 0) goto L_0x00f0
            android.database.sqlite.SQLiteDatabase r0 = r7.t     // Catch:{ SQLException -> 0x0116, all -> 0x0121 }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.quiz.QuizHistoryTab.p     // Catch:{ SQLException -> 0x0116, all -> 0x0121 }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x0116, all -> 0x0121 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0116, all -> 0x0121 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.B     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            if (r2 == 0) goto L_0x00b1
            java.lang.String r2 = "QHT:doRawCheck()"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            r3.<init>()     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            java.lang.String r4 = "query count = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            android.util.Log.d(r2, r1)     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            java.lang.String r1 = "QHT:doRawCheck()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            r2.<init>()     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            java.lang.String r3 = "column count = '"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            int r3 = r0.getColumnCount()     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            java.lang.String r3 = "' "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            android.util.Log.d(r1, r2)     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
        L_0x00b1:
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            if (r1 == 0) goto L_0x00eb
        L_0x00b7:
            r1 = 1
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            r2 = 0
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.B     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            if (r3 == 0) goto L_0x00e5
            java.lang.String r3 = "QHT:doRawCheck()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            r4.<init>()     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            java.lang.String r4 = " - "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            android.util.Log.d(r3, r1)     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
        L_0x00e5:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0130, all -> 0x0129 }
            if (r1 != 0) goto L_0x00b7
        L_0x00eb:
            if (r0 == 0) goto L_0x00f0
            r0.close()
        L_0x00f0:
            android.database.sqlite.SQLiteCursor r0 = r7.b()
            r7.s = r0
            android.widget.ListAdapter r0 = r7.c()
            r7.setListAdapter(r0)
            r0 = 2131361994(0x7f0a00ca, float:1.8343756E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.String r1 = ""
            r0.setText(r1)
            android.widget.ListView r1 = r7.getListView()
            r1.setFastScrollEnabled(r5)
            r1.setEmptyView(r0)
            return
        L_0x0116:
            r0 = move-exception
            r1 = r3
        L_0x0118:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x012e }
            if (r1 == 0) goto L_0x00f0
            r1.close()
            goto L_0x00f0
        L_0x0121:
            r0 = move-exception
            r1 = r3
        L_0x0123:
            if (r1 == 0) goto L_0x0128
            r1.close()
        L_0x0128:
            throw r0
        L_0x0129:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0123
        L_0x012e:
            r0 = move-exception
            goto L_0x0123
        L_0x0130:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0118
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.quiz.QuizHistoryTab.onCreate(android.os.Bundle):void");
    }

    public void onResume() {
        int i2;
        int i3;
        d();
        Log.i("QuizHistoryTab", "onResume()");
        stopManagingCursor(this.s);
        if (this.s != null) {
            this.s.close();
            this.s = null;
        }
        this.s = b();
        setListAdapter(c());
        int a2 = q.a("SELECT MAX(userDB.quizsessions._id) FROM userDB.quizsessions ", this.t);
        ImageButton imageButton = this.f1039a;
        if (a2 == 0) {
            i2 = 4;
        } else {
            i2 = 0;
        }
        imageButton.setVisibility(i2);
        ListView listView = this.f1040b;
        if (a2 == 0) {
            i3 = 4;
        } else {
            i3 = 0;
        }
        listView.setVisibility(i3);
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        AlertDialog create = new AlertDialog.Builder(this).create();
        create.setTitle("Test Results");
        create.setMessage("Remove ALL Quiz Results?");
        create.setButton("Remove", new h(this));
        create.setButton2("Cancel", new e(this));
        create.show();
    }

    public void onDestroy() {
        try {
            if (this.t != null && this.t.isOpen()) {
                this.t.close();
                this.t = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        if (i2 >= 0) {
            super.onListItemClick(listView, view, i2, j2);
            if (dh.B) {
                Log.d("QHT:onListItemClick()", "position = " + Integer.toString(i2) + ", id = " + Long.toString(j2));
            }
            TcmAid.bn = "_id=" + Integer.toString((int) j2);
            TcmAid.bo = (int) j2;
            TcmAid.bm = i2;
            startActivityForResult(new Intent(this, QuizHistoryDetail.class), 1350);
        }
    }

    /* access modifiers changed from: private */
    public SQLiteCursor b() {
        try {
            this.s = (SQLiteCursor) this.t.query(q, "userDB.quizsessions", new String[]{"_id", "date", "area", "correct", "questions", "type"}, this.j, this.k, l, m, "date", null);
            startManagingCursor(this.s);
            int count = this.s.getCount();
            if (dh.B) {
                Log.d("QHT:createCursor()", "query count = " + Integer.toString(count));
            }
            TcmAid.cu.clear();
            if (this.s.moveToFirst()) {
                do {
                    TcmAid.cu.add(Integer.valueOf(this.s.getInt(0)));
                } while (this.s.moveToNext());
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        return this.s;
    }

    /* access modifiers changed from: private */
    public ListAdapter c() {
        return new c(this, this.s, new String[]{"_id", "date"}, new int[]{C0000R.id.text0, C0000R.id.text1}, "date");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }

    private void d() {
        if (this.t == null || !this.t.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("QHT:openDataBase()", str + " opened.");
                this.t = SQLiteDatabase.openDatabase(str, null, 16);
                this.t.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.t.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("QHT:openDataBase()", str2 + " ATTACHed.");
                this.u = new n();
                this.u.a(this.t);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new f(this));
                create.show();
            }
        }
    }
}
