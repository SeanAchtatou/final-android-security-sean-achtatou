package com.cyberandsons.tcmaidtrial.quiz;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.res.XmlResourceParser;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class SelectQuizTab extends Activity implements AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    private Button f1045a;

    /* renamed from: b  reason: collision with root package name */
    private Button f1046b;
    private Button c;
    private Button d;
    private Button e;
    private Button f;
    private Spinner g;
    private boolean h = true;
    private boolean i;
    private SQLiteDatabase j;
    private n k;

    public SelectQuizTab() {
        this.i = !this.h;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (TcmAid.cT < dh.f947b && TcmAid.cS < dh.f947b) {
            setRequestedOrientation(1);
        }
        setContentView((int) C0000R.layout.quizselecttab);
        this.f1045a = (Button) findViewById(C0000R.id.auricular_button);
        this.f1045a.setOnClickListener(new m(this));
        this.f1046b = (Button) findViewById(C0000R.id.diagnosis_button);
        this.f1046b.setOnClickListener(new n(this));
        this.c = (Button) findViewById(C0000R.id.herbs_button);
        this.c.setOnClickListener(new k(this));
        this.d = (Button) findViewById(C0000R.id.formulas_button);
        this.d.setOnClickListener(new l(this));
        this.e = (Button) findViewById(C0000R.id.points_button);
        this.e.setOnClickListener(new q(this));
        this.f = (Button) findViewById(C0000R.id.pulse_diag_button);
        this.f.setOnClickListener(new r(this));
        this.g = (Spinner) findViewById(C0000R.id.typesofquizzes);
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        i();
        switch (TcmAid.bw) {
            case -1:
                Log.i("SelectQuizTab", "onResume() - kQuizYourNothing");
                g();
                h();
                break;
            case 0:
                Log.i("SelectQuizTab", "onResume() - kQuizYourSelectTab");
                break;
            case 1:
                Log.i("SelectQuizTab", "onResume() - kQuizYourSpecificTab");
                break;
            case 2:
                Log.i("SelectQuizTab", "onResume() - kQuizYourRunTab");
                g();
                h();
                break;
        }
        super.onResume();
    }

    public void onDestroy() {
        TcmAid.bp = -1;
        TcmAid.bw = -1;
        try {
            if (this.j != null && this.j.isOpen()) {
                this.j.close();
                this.j = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    private void g() {
        switch (TcmAid.bp) {
            case 0:
                this.f1045a.setTypeface(Typeface.DEFAULT_BOLD, 1);
                this.f1045a.setTextColor(-16777216);
                return;
            case 1:
                this.f1046b.setTypeface(Typeface.DEFAULT_BOLD, 1);
                this.f1046b.setTextColor(-16777216);
                return;
            case 2:
                this.c.setTypeface(Typeface.DEFAULT_BOLD, 1);
                this.c.setTextColor(-16777216);
                return;
            case 3:
                this.d.setTypeface(Typeface.DEFAULT_BOLD, 1);
                this.d.setTextColor(-16777216);
                return;
            case 4:
                this.e.setTypeface(Typeface.DEFAULT_BOLD, 1);
                this.e.setTextColor(-16777216);
                return;
            case 5:
                this.f.setTypeface(Typeface.DEFAULT_BOLD, 1);
                this.f.setTextColor(-16777216);
                return;
            default:
                return;
        }
    }

    /* JADX WARN: Type inference failed for: r2v33, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r2v51, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r2v63, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r2v72, types: [android.database.Cursor] */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x042e, code lost:
        r3 = "openDatabase()";
        r2 = "Not Initialized";
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0486, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x048b, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x048c, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x0498, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x04a1, code lost:
        r3 = "openDatabase()";
        r2 = "SELECT DISTINCT main.auricular.pointcat FROM main.auricular ORDER BY main.auricular.pointcat ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x04a6, code lost:
        r2 = "SELECT DISTINCT main.auricular.pointcat FROM main.auricular ORDER BY main.auricular.pointcat ";
        r3 = "Diagnosis Category";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x04aa, code lost:
        r2 = r5;
        r3 = "Diagnosis Category";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x04ae, code lost:
        r2 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x04b5, code lost:
        r3 = "Prepping TestArrays";
        r2 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x04b9, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x04ba, code lost:
        r8 = r2;
        r2 = r1;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x04bf, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x04c0, code lost:
        r8 = r2;
        r2 = r1;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x04c5, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x04c6, code lost:
        r8 = r2;
        r2 = r1;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x03da, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x03db, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
        com.cyberandsons.tcmaidtrial.a.q.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x03df, code lost:
        if (r2 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x03e1, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x03e5, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x03e6, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        android.util.Log.e("SQT:load_details()", "XPPE: " + r1.getLocalizedMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0403, code lost:
        if (r2 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0405, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0409, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x040a, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
        android.util.Log.e("SQT:load_details()", "IOE: " + r1.getLocalizedMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0427, code lost:
        if (r2 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0429, code lost:
        r2.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0486  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x048b A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x048f  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x04b9 A[ExcHandler: IOException (r2v24 'e' java.io.IOException A[CUSTOM_DECLARE]), PHI: r1 
      PHI: (r1v28 android.database.sqlite.SQLiteCursor) = (r1v30 android.database.sqlite.SQLiteCursor), (r1v30 android.database.sqlite.SQLiteCursor), (r1v30 android.database.sqlite.SQLiteCursor), (r1v33 android.database.sqlite.SQLiteCursor), (r1v33 android.database.sqlite.SQLiteCursor), (r1v34 android.database.sqlite.SQLiteCursor), (r1v35 android.database.sqlite.SQLiteCursor), (r1v35 android.database.sqlite.SQLiteCursor) binds: [B:26:0x01cd, B:72:0x03c2, B:40:0x023a, B:76:0x03ce, B:52:0x02ba, B:61:0x0324, B:67:0x0351, B:68:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:5:0x0037] */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x04bf A[ExcHandler: XmlPullParserException (r2v22 'e' org.xmlpull.v1.XmlPullParserException A[CUSTOM_DECLARE]), PHI: r1 
      PHI: (r1v26 android.database.sqlite.SQLiteCursor) = (r1v30 android.database.sqlite.SQLiteCursor), (r1v30 android.database.sqlite.SQLiteCursor), (r1v30 android.database.sqlite.SQLiteCursor), (r1v33 android.database.sqlite.SQLiteCursor), (r1v33 android.database.sqlite.SQLiteCursor), (r1v34 android.database.sqlite.SQLiteCursor), (r1v35 android.database.sqlite.SQLiteCursor), (r1v35 android.database.sqlite.SQLiteCursor) binds: [B:26:0x01cd, B:72:0x03c2, B:40:0x023a, B:76:0x03ce, B:52:0x02ba, B:61:0x0324, B:67:0x0351, B:68:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:5:0x0037] */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x04c5 A[ExcHandler: SQLException (r2v20 'e' android.database.SQLException A[CUSTOM_DECLARE]), PHI: r1 
      PHI: (r1v24 android.database.sqlite.SQLiteCursor) = (r1v30 android.database.sqlite.SQLiteCursor), (r1v30 android.database.sqlite.SQLiteCursor), (r1v30 android.database.sqlite.SQLiteCursor), (r1v33 android.database.sqlite.SQLiteCursor), (r1v33 android.database.sqlite.SQLiteCursor), (r1v34 android.database.sqlite.SQLiteCursor), (r1v35 android.database.sqlite.SQLiteCursor), (r1v35 android.database.sqlite.SQLiteCursor) binds: [B:26:0x01cd, B:72:0x03c2, B:40:0x023a, B:76:0x03ce, B:52:0x02ba, B:61:0x0324, B:67:0x0351, B:68:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:5:0x0037] */
    /* JADX WARNING: Removed duplicated region for block: B:143:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:145:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:147:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:149:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x03da A[ExcHandler: SQLException (e android.database.SQLException), Splitter:B:1:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x03e1  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x03e5 A[ExcHandler: XmlPullParserException (e org.xmlpull.v1.XmlPullParserException), Splitter:B:1:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0405  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0409 A[ExcHandler: IOException (e java.io.IOException), Splitter:B:1:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0429  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void h() {
        /*
            r9 = this;
            r6 = 0
            r4 = 1
            r7 = 0
            java.lang.String r1 = "Not Initialized"
            android.widget.ArrayAdapter r2 = new android.widget.ArrayAdapter
            r3 = 17367048(0x1090008, float:2.5162948E-38)
            java.lang.String[] r4 = new java.lang.String[r4]
            java.lang.String r5 = "Select Quiz Area from above..."
            r4[r7] = r5
            r2.<init>(r9, r3, r4)
            r3 = 17367049(0x1090009, float:2.516295E-38)
            r2.setDropDownViewResource(r3)
            android.widget.Spinner r3 = r9.g
            r3.setAdapter(r2)
            android.widget.Spinner r2 = r9.g
            r2.setSelection(r7)
            r9.i()
            java.lang.String r2 = "openDatabase()"
            java.lang.String r3 = "SELECT DISTINCT main.auricular.pointcat FROM main.auricular ORDER BY main.auricular.pointcat "
            com.cyberandsons.tcmaidtrial.TcmAid.q = r3     // Catch:{ SQLException -> 0x03da, XmlPullParserException -> 0x03e5, IOException -> 0x0409, NullPointerException -> 0x042d, all -> 0x048b }
            android.database.sqlite.SQLiteDatabase r1 = r9.j     // Catch:{ SQLException -> 0x03da, XmlPullParserException -> 0x03e5, IOException -> 0x0409, NullPointerException -> 0x049a, all -> 0x048b }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.q     // Catch:{ SQLException -> 0x03da, XmlPullParserException -> 0x03e5, IOException -> 0x0409, NullPointerException -> 0x049a, all -> 0x048b }
            r5 = 0
            android.database.Cursor r1 = r1.rawQuery(r4, r5)     // Catch:{ SQLException -> 0x03da, XmlPullParserException -> 0x03e5, IOException -> 0x0409, NullPointerException -> 0x049a, all -> 0x048b }
            android.database.sqlite.SQLiteCursor r1 = (android.database.sqlite.SQLiteCursor) r1     // Catch:{ SQLException -> 0x03da, XmlPullParserException -> 0x03e5, IOException -> 0x0409, NullPointerException -> 0x049a, all -> 0x048b }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.j     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            r4.clear()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            boolean r4 = r1.moveToFirst()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            if (r4 == 0) goto L_0x0052
        L_0x0042:
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.j     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            r5 = 0
            java.lang.String r5 = r1.getString(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            boolean r4 = r1.moveToNext()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            if (r4 != 0) goto L_0x0042
        L_0x0052:
            r1.close()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.j     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "NADA"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.j     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Battlefield"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.j     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "ACACD"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.j     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Stop Smoking"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.j     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Weight Loss"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            com.cyberandsons.tcmaidtrial.quiz.o r4 = new com.cyberandsons.tcmaidtrial.quiz.o     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            r4.<init>(r9)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r5 = com.cyberandsons.tcmaidtrial.TcmAid.j     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.Collections.sort(r5, r4)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r2 = "Auricular query"
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.j     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            r5 = 0
            java.lang.String r6 = "<Select from Auricular categories>"
            r4.add(r5, r6)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.j     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            r5 = 1
            java.lang.String r6 = "20 Random Auricular"
            r4.add(r5, r6)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            com.cyberandsons.tcmaidtrial.a.n r4 = r9.k     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r4 = r4.n()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            int r4 = com.cyberandsons.tcmaidtrial.a.n.a(r4)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            if (r4 <= 0) goto L_0x00bd
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            r5.<init>()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.StringBuilder r4 = r5.append(r4)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = " Favorite Auricular"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r5 = com.cyberandsons.tcmaidtrial.TcmAid.j     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            r6 = 2
            r5.add(r6, r4)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
        L_0x00bd:
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            r4.clear()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "<Select from Diagnosis categories"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "20 Random Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Lung Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Large Intestine Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Stomach Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Spleen Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Heart Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Small Intestine Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Kidney Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Urinary Bladder Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Liver Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Gallbladder Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Yin Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Yang Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Qi Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Blood Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Heat Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Cold Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Damp Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Phlegm Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Dryness Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Collapse Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Wind Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Fire Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = "Stagnation Etiologies"
            r4.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            com.cyberandsons.tcmaidtrial.a.n r4 = r9.k     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r4 = r4.o()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            int r4 = com.cyberandsons.tcmaidtrial.a.n.a(r4)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            if (r4 <= 0) goto L_0x019a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            r5.<init>()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.StringBuilder r4 = r5.append(r4)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r5 = " Favorite Diagnosis"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            java.util.ArrayList r5 = com.cyberandsons.tcmaidtrial.TcmAid.z     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
            r6 = 2
            r5.add(r6, r4)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a0 }
        L_0x019a:
            java.lang.String r4 = "Diagnosis Category"
            java.lang.String r5 = "SELECT item_name FROM main.meridian WHERE item_name NOT LIKE '%-%' UNION SELECT userDB.pointcategory.name FROM userDB.pointcategory ORDER BY 1"
            com.cyberandsons.tcmaidtrial.TcmAid.ar = r5     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a5 }
            android.database.sqlite.SQLiteDatabase r2 = r9.j     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a9 }
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.TcmAid.ar     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a9 }
            r6 = 0
            android.database.Cursor r2 = r2.rawQuery(r3, r6)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a9 }
            r0 = r2
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a9 }
            r1 = r0
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.am     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a9 }
            r2.clear()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a9 }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a9 }
            if (r2 == 0) goto L_0x01c8
        L_0x01b8:
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.am     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a9 }
            r3 = 0
            java.lang.String r3 = r1.getString(r3)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a9 }
            r2.add(r3)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a9 }
            boolean r2 = r1.moveToNext()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a9 }
            if (r2 != 0) goto L_0x01b8
        L_0x01c8:
            r1.close()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04a9 }
            java.lang.String r3 = "Point query"
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.am     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            r4 = 0
            java.lang.String r6 = "<Select from Point categories>"
            r2.add(r4, r6)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.am     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            r4 = 1
            java.lang.String r6 = "20 Random Points"
            r2.add(r4, r6)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            com.cyberandsons.tcmaidtrial.a.n r2 = r9.k     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.lang.String r2 = r2.r()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            int r2 = com.cyberandsons.tcmaidtrial.a.n.a(r2)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            if (r2 <= 0) goto L_0x0206
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            r4.<init>()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.lang.String r4 = " Favorite Points"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.am     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            r6 = 2
            r4.add(r6, r2)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
        L_0x0206:
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.am     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.util.Iterator r4 = r2.iterator()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            r6 = r7
        L_0x020d:
            boolean r2 = r4.hasNext()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            if (r2 == 0) goto L_0x0228
            java.lang.Object r2 = r4.next()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.lang.String r7 = "Gallbladder"
            boolean r2 = r2.equalsIgnoreCase(r7)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            if (r2 == 0) goto L_0x03bd
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.am     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.lang.String r4 = "Extra Points"
            r2.add(r6, r4)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
        L_0x0228:
            java.lang.String r2 = ""
            com.cyberandsons.tcmaidtrial.a.n r4 = r9.k     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            boolean r4 = r4.ag()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            if (r4 == 0) goto L_0x03c2
            java.lang.String r2 = " WHERE main.materiamedica.req_state_board=1 "
        L_0x0234:
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.a.m.f(r2)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            com.cyberandsons.tcmaidtrial.TcmAid.ag = r4     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            android.database.sqlite.SQLiteDatabase r2 = r9.j     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            java.lang.String r5 = com.cyberandsons.tcmaidtrial.TcmAid.ag     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r6 = 0
            android.database.Cursor r2 = r2.rawQuery(r5, r6)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r0 = r2
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r1 = r0
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.ab     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r2.clear()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            if (r2 == 0) goto L_0x0262
        L_0x0252:
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.ab     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r5 = 0
            java.lang.String r5 = r1.getString(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r2.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            boolean r2 = r1.moveToNext()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            if (r2 != 0) goto L_0x0252
        L_0x0262:
            r1.close()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            java.lang.String r3 = "Herb query"
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.ab     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r5 = 0
            java.lang.String r6 = "<Select from Herb categories>"
            r2.add(r5, r6)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.ab     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r5 = 1
            java.lang.String r6 = "20 Random Herbs"
            r2.add(r5, r6)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            com.cyberandsons.tcmaidtrial.a.n r2 = r9.k     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            java.lang.String r2 = r2.q()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            int r2 = com.cyberandsons.tcmaidtrial.a.n.a(r2)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            if (r2 <= 0) goto L_0x02a0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r5.<init>()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            java.lang.String r5 = " Favorite Herbs"
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            java.util.ArrayList r5 = com.cyberandsons.tcmaidtrial.TcmAid.ab     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r6 = 2
            r5.add(r6, r2)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
        L_0x02a0:
            java.lang.String r2 = ""
            com.cyberandsons.tcmaidtrial.a.n r5 = r9.k     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            boolean r5 = r5.ah()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            if (r5 == 0) goto L_0x03ce
            java.lang.String r2 = " AND main.formulas.req_state_board=1 "
        L_0x02ac:
            java.lang.String r5 = "SELECT main.majorFcategory.item_name, count(main.formulas.pinyin) AS cnt FROM main.majorFcategory JOIN main.formulas ON main.majorFcategory._id=main.formulas.majorcategory %s GROUP BY main.majorFcategory.item_name HAVING count(main.formulas.pinyin) > 2 UNION SELECT userDB.formulascategory.name, 0 FROM userDB.formulascategory ORDER BY 1"
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r7 = 0
            r6[r7] = r2     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            java.lang.String r5 = java.lang.String.format(r5, r6)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            com.cyberandsons.tcmaidtrial.TcmAid.T = r5     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            android.database.sqlite.SQLiteDatabase r2 = r9.j     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.T     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            r6 = 0
            android.database.Cursor r2 = r2.rawQuery(r4, r6)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            r0 = r2
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            r1 = r0
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.O     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            r2.clear()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            if (r2 == 0) goto L_0x02e2
        L_0x02d2:
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.O     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            r4 = 0
            java.lang.String r4 = r1.getString(r4)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            r2.add(r4)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            boolean r2 = r1.moveToNext()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            if (r2 != 0) goto L_0x02d2
        L_0x02e2:
            r1.close()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.lang.String r3 = "Formula query"
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.O     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            r4 = 0
            java.lang.String r6 = "<Select from Formula categories>"
            r2.add(r4, r6)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.O     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            r4 = 1
            java.lang.String r6 = "20 Random Formulas"
            r2.add(r4, r6)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            com.cyberandsons.tcmaidtrial.a.n r2 = r9.k     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.lang.String r2 = r2.p()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            int r2 = com.cyberandsons.tcmaidtrial.a.n.a(r2)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            if (r2 <= 0) goto L_0x0320
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            r4.<init>()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.lang.String r4 = " Favorite Formulas"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.O     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            r6 = 2
            r4.add(r6, r2)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
        L_0x0320:
            java.lang.String r4 = "SELECT DISTINCT main.pulsediag.category FROM main.pulsediag "
            com.cyberandsons.tcmaidtrial.TcmAid.aG = r4     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            android.database.sqlite.SQLiteDatabase r2 = r9.j     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            java.lang.String r5 = com.cyberandsons.tcmaidtrial.TcmAid.aG     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r6 = 0
            android.database.Cursor r2 = r2.rawQuery(r5, r6)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r0 = r2
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r1 = r0
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.aA     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r2.clear()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            if (r2 == 0) goto L_0x034c
        L_0x033c:
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.aA     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r5 = 0
            java.lang.String r5 = r1.getString(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            r2.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            boolean r2 = r1.moveToNext()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            if (r2 != 0) goto L_0x033c
        L_0x034c:
            r1.close()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            java.lang.String r2 = "PulseDiag query"
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.aA     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            r5 = 0
            java.lang.String r6 = "<Select from Pulse categories>"
            r3.add(r5, r6)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.aA     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            r5 = 1
            java.lang.String r6 = "Random Pulses"
            r3.add(r5, r6)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            com.cyberandsons.tcmaidtrial.a.n r3 = r9.k     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.lang.String r3 = r3.n()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            com.cyberandsons.tcmaidtrial.a.n.a(r3)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.by     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            r3.clear()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.by     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.lang.String r5 = "<Open for Auricular Quizzes>"
            r3.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.bG     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            r3.clear()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.bG     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.lang.String r5 = "<Open for Diagnosis Quizzes>"
            r3.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.bO     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            r3.clear()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.bO     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.lang.String r5 = "<Open for Herb Quizzes>"
            r3.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.bW     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            r3.clear()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.bW     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.lang.String r5 = "<Open for Formula Quizzes>"
            r3.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.ce     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            r3.clear()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.ce     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.lang.String r5 = "<Open for Point Quizzes>"
            r3.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.cm     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            r3.clear()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.cm     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.lang.String r5 = "<Open for Pulse Quizzes>"
            r3.add(r5)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            java.lang.String r2 = "Prepping TestArrays"
            a(r9)     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b4 }
            if (r1 == 0) goto L_0x03bc
            r1.close()
        L_0x03bc:
            return
        L_0x03bd:
            int r2 = r6 + 1
            r6 = r2
            goto L_0x020d
        L_0x03c2:
            com.cyberandsons.tcmaidtrial.a.n r4 = r9.k     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            boolean r4 = r4.ae()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04ad }
            if (r4 == 0) goto L_0x0234
            java.lang.String r2 = " WHERE main.materiamedica.nccaom_req=1 "
            goto L_0x0234
        L_0x03ce:
            com.cyberandsons.tcmaidtrial.a.n r5 = r9.k     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            boolean r5 = r5.af()     // Catch:{ SQLException -> 0x04c5, XmlPullParserException -> 0x04bf, IOException -> 0x04b9, NullPointerException -> 0x04b0 }
            if (r5 == 0) goto L_0x02ac
            java.lang.String r2 = " AND main.formulas.nccaom_req=1 "
            goto L_0x02ac
        L_0x03da:
            r1 = move-exception
            r2 = r6
        L_0x03dc:
            com.cyberandsons.tcmaidtrial.a.q.a(r1)     // Catch:{ all -> 0x0498 }
            if (r2 == 0) goto L_0x03bc
            r2.close()
            goto L_0x03bc
        L_0x03e5:
            r1 = move-exception
            r2 = r6
        L_0x03e7:
            java.lang.String r3 = "SQT:load_details()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0498 }
            r4.<init>()     // Catch:{ all -> 0x0498 }
            java.lang.String r5 = "XPPE: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0498 }
            java.lang.String r1 = r1.getLocalizedMessage()     // Catch:{ all -> 0x0498 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x0498 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0498 }
            android.util.Log.e(r3, r1)     // Catch:{ all -> 0x0498 }
            if (r2 == 0) goto L_0x03bc
            r2.close()
            goto L_0x03bc
        L_0x0409:
            r1 = move-exception
            r2 = r6
        L_0x040b:
            java.lang.String r3 = "SQT:load_details()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0498 }
            r4.<init>()     // Catch:{ all -> 0x0498 }
            java.lang.String r5 = "IOE: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0498 }
            java.lang.String r1 = r1.getLocalizedMessage()     // Catch:{ all -> 0x0498 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x0498 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0498 }
            android.util.Log.e(r3, r1)     // Catch:{ all -> 0x0498 }
            if (r2 == 0) goto L_0x03bc
            r2.close()
            goto L_0x03bc
        L_0x042d:
            r3 = move-exception
            r3 = r2
            r2 = r1
            r1 = r6
        L_0x0431:
            org.acra.ErrorReporter r4 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0493 }
            java.lang.String r5 = "myVariable"
            r4.a(r5, r2)     // Catch:{ all -> 0x0493 }
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0493 }
            java.lang.NullPointerException r4 = new java.lang.NullPointerException     // Catch:{ all -> 0x0493 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0493 }
            r5.<init>()     // Catch:{ all -> 0x0493 }
            java.lang.String r6 = "SQT:load_details( last step completed = "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0493 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ all -> 0x0493 }
            java.lang.String r5 = " )"
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ all -> 0x0493 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0493 }
            r4.<init>(r3)     // Catch:{ all -> 0x0493 }
            r2.handleSilentException(r4)     // Catch:{ all -> 0x0493 }
            android.app.AlertDialog$Builder r2 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x0493 }
            r2.<init>(r9)     // Catch:{ all -> 0x0493 }
            android.app.AlertDialog r2 = r2.create()     // Catch:{ all -> 0x0493 }
            java.lang.String r3 = "Attention:"
            r2.setTitle(r3)     // Catch:{ all -> 0x0493 }
            r3 = 2131099675(0x7f06001b, float:1.781171E38)
            java.lang.String r3 = r9.getString(r3)     // Catch:{ all -> 0x0493 }
            r2.setMessage(r3)     // Catch:{ all -> 0x0493 }
            java.lang.String r3 = "OK"
            com.cyberandsons.tcmaidtrial.quiz.p r4 = new com.cyberandsons.tcmaidtrial.quiz.p     // Catch:{ all -> 0x0493 }
            r4.<init>(r9)     // Catch:{ all -> 0x0493 }
            r2.setButton(r3, r4)     // Catch:{ all -> 0x0493 }
            r2.show()     // Catch:{ all -> 0x0493 }
            if (r1 == 0) goto L_0x03bc
            r1.close()
            goto L_0x03bc
        L_0x048b:
            r1 = move-exception
            r2 = r6
        L_0x048d:
            if (r2 == 0) goto L_0x0492
            r2.close()
        L_0x0492:
            throw r1
        L_0x0493:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x048d
        L_0x0498:
            r1 = move-exception
            goto L_0x048d
        L_0x049a:
            r1 = move-exception
            r1 = r6
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x0431
        L_0x04a0:
            r4 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x0431
        L_0x04a5:
            r2 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x0431
        L_0x04a9:
            r2 = move-exception
            r2 = r5
            r3 = r4
            goto L_0x0431
        L_0x04ad:
            r2 = move-exception
            r2 = r5
            goto L_0x0431
        L_0x04b0:
            r2 = move-exception
            r2 = r4
            goto L_0x0431
        L_0x04b4:
            r3 = move-exception
            r3 = r2
            r2 = r4
            goto L_0x0431
        L_0x04b9:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x040b
        L_0x04bf:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x03e7
        L_0x04c5:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x03dc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.quiz.SelectQuizTab.h():void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static void a(Activity activity) {
        String str;
        String str2;
        int i2;
        int i3;
        int i4;
        int i5;
        String str3 = "";
        XmlResourceParser xml = activity.getResources().getXml(C0000R.xml.quizes);
        xml.next();
        int i6 = 0;
        int i7 = -1;
        int i8 = 0;
        String str4 = "";
        for (int eventType = xml.getEventType(); eventType != 1; eventType = xml.next()) {
            if (eventType == 0) {
                if (dh.o) {
                    Log.i("SQT", "START_DOCUMENT");
                    str = str3;
                    str2 = str4;
                    i2 = i6;
                    i3 = i7;
                    i4 = i8;
                }
                str = str3;
                str2 = str4;
                i2 = i6;
                i3 = i7;
                i4 = i8;
            } else if (eventType == 2) {
                int depth = xml.getDepth();
                String str5 = str3;
                str2 = xml.getName();
                i2 = i6;
                i3 = i7;
                i4 = depth;
                str = str5;
            } else if (eventType == 3) {
                if (dh.o) {
                    Log.i("SQT", "END_TAG");
                    str = str3;
                    str2 = str4;
                    i2 = i6;
                    i3 = i7;
                    i4 = i8;
                }
                str = str3;
                str2 = str4;
                i2 = i6;
                i3 = i7;
                i4 = i8;
            } else {
                if (eventType == 4) {
                    switch (i8) {
                        case 5:
                            if (str4.compareToIgnoreCase("string") == 0) {
                                int i9 = i7 + 1;
                                if (dh.o) {
                                    Log.i("Area: ", xml.getText());
                                }
                                i3 = i9;
                                i4 = i8;
                                str = xml.getText();
                                str2 = str4;
                                i2 = 0;
                                break;
                            }
                            break;
                        case 7:
                            if (str4.compareToIgnoreCase("string") == 0) {
                                switch (i6) {
                                    case 0:
                                        if (dh.o) {
                                            Log.i("Title: ", str3 + ": " + xml.getText());
                                        }
                                        switch (i7) {
                                            case 0:
                                                TcmAid.by.add(xml.getText());
                                                i5 = i6;
                                                break;
                                            case 1:
                                                TcmAid.bG.add(xml.getText());
                                                i5 = i6;
                                                break;
                                            case 2:
                                                TcmAid.bO.add(xml.getText());
                                                i5 = i6;
                                                break;
                                            case 3:
                                                TcmAid.bW.add(xml.getText());
                                                i5 = i6;
                                                break;
                                            case 4:
                                                TcmAid.ce.add(xml.getText());
                                                i5 = i6;
                                                break;
                                            case 5:
                                                TcmAid.cm.add(xml.getText());
                                            default:
                                                i5 = i6;
                                                break;
                                        }
                                    case 1:
                                        if (dh.o) {
                                            Log.i("Select: ", str3 + ": " + xml.getText().replace(".id", "._id"));
                                        }
                                        switch (i7) {
                                            case 0:
                                                TcmAid.bz.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 1:
                                                TcmAid.bH.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 2:
                                                TcmAid.bP.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 3:
                                                TcmAid.bX.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 4:
                                                TcmAid.cf.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 5:
                                                TcmAid.cn.add(xml.getText().replace(".id", "._id"));
                                            default:
                                                i5 = i6;
                                                break;
                                        }
                                    case 2:
                                        if (dh.o) {
                                            Log.i("Where: ", str3 + ": " + xml.getText().replace(".id", "._id"));
                                        }
                                        switch (i7) {
                                            case 0:
                                                TcmAid.bA.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 1:
                                                TcmAid.bI.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 2:
                                                TcmAid.bQ.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 3:
                                                TcmAid.bY.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 4:
                                                TcmAid.cg.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 5:
                                                TcmAid.co.add(xml.getText().replace(".id", "._id"));
                                            default:
                                                i5 = i6;
                                                break;
                                        }
                                    case 3:
                                        if (dh.o) {
                                            Log.i("Random Where: ", str3 + ": " + xml.getText().replace(".id", "._id"));
                                        }
                                        switch (i7) {
                                            case 0:
                                                TcmAid.bB.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 1:
                                                TcmAid.bJ.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 2:
                                                TcmAid.bR.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 3:
                                                TcmAid.bZ.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 4:
                                                TcmAid.ch.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 5:
                                                TcmAid.cp.add(xml.getText().replace(".id", "._id"));
                                            default:
                                                i5 = i6;
                                                break;
                                        }
                                    case 4:
                                        if (dh.o) {
                                            Log.i("Favorite Where: ", str3 + ": " + xml.getText().replace(".id", "._id"));
                                        }
                                        switch (i7) {
                                            case 0:
                                                TcmAid.bC.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 1:
                                                TcmAid.bK.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 2:
                                                TcmAid.bS.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 3:
                                                TcmAid.ca.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 4:
                                                TcmAid.ci.add(xml.getText().replace(".id", "._id"));
                                                i5 = i6;
                                                break;
                                            case 5:
                                                TcmAid.cq.add(xml.getText().replace(".id", "._id"));
                                            default:
                                                i5 = i6;
                                                break;
                                        }
                                    case 5:
                                        if (dh.o) {
                                            Log.i("Question: ", str3 + ": " + xml.getText());
                                        }
                                        switch (i7) {
                                            case 0:
                                                TcmAid.bD.add(xml.getText());
                                                i5 = i6;
                                                break;
                                            case 1:
                                                TcmAid.bL.add(xml.getText());
                                                i5 = i6;
                                                break;
                                            case 2:
                                                TcmAid.bT.add(xml.getText());
                                                i5 = i6;
                                                break;
                                            case 3:
                                                TcmAid.cb.add(xml.getText());
                                                i5 = i6;
                                                break;
                                            case 4:
                                                TcmAid.cj.add(xml.getText());
                                                i5 = i6;
                                                break;
                                            case 5:
                                                TcmAid.cr.add(xml.getText());
                                            default:
                                                i5 = i6;
                                                break;
                                        }
                                    case 6:
                                        if (dh.o) {
                                            Log.i("Field: ", str3 + ": " + xml.getText());
                                        }
                                        switch (i7) {
                                            case 0:
                                                TcmAid.bE.add(Integer.valueOf(Integer.parseInt(xml.getText())));
                                                i5 = i6;
                                                break;
                                            case 1:
                                                TcmAid.bM.add(Integer.valueOf(Integer.parseInt(xml.getText())));
                                                i5 = i6;
                                                break;
                                            case 2:
                                                TcmAid.bU.add(Integer.valueOf(Integer.parseInt(xml.getText())));
                                                i5 = i6;
                                                break;
                                            case 3:
                                                TcmAid.cc.add(Integer.valueOf(Integer.parseInt(xml.getText())));
                                                i5 = i6;
                                                break;
                                            case 4:
                                                TcmAid.ck.add(Integer.valueOf(Integer.parseInt(xml.getText())));
                                                i5 = i6;
                                                break;
                                            case 5:
                                                TcmAid.cs.add(Integer.valueOf(Integer.parseInt(xml.getText())));
                                            default:
                                                i5 = i6;
                                                break;
                                        }
                                    case 7:
                                        if (dh.o) {
                                            Log.i("AuxField: ", str3 + ": " + xml.getText());
                                        }
                                        switch (i7) {
                                            case 0:
                                                TcmAid.bF.add(Integer.valueOf(Integer.parseInt(xml.getText())));
                                                break;
                                            case 1:
                                                TcmAid.bN.add(Integer.valueOf(Integer.parseInt(xml.getText())));
                                                break;
                                            case 2:
                                                TcmAid.bV.add(Integer.valueOf(Integer.parseInt(xml.getText())));
                                                break;
                                            case 3:
                                                TcmAid.cd.add(Integer.valueOf(Integer.parseInt(xml.getText())));
                                                break;
                                            case 4:
                                                TcmAid.cl.add(Integer.valueOf(Integer.parseInt(xml.getText())));
                                                break;
                                            case 5:
                                                TcmAid.ct.add(Integer.valueOf(Integer.parseInt(xml.getText())));
                                                break;
                                        }
                                        i5 = -1;
                                        break;
                                    default:
                                        i5 = i6;
                                        break;
                                }
                                i3 = i7;
                                i4 = i8;
                                int i10 = i5 + 1;
                                str = str3;
                                str2 = str4;
                                i2 = i10;
                                break;
                            }
                            break;
                    }
                }
                str = str3;
                str2 = str4;
                i2 = i6;
                i3 = i7;
                i4 = i8;
            }
            i8 = i4;
            i7 = i3;
            i6 = i2;
            str4 = str2;
            str3 = str;
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        a(this.f1045a);
        TcmAid.bp = 0;
        this.g.setPrompt("Select from the following Auricular quizzes.");
        this.g.setOnItemSelectedListener(this);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, TcmAid.by);
        arrayAdapter.setDropDownViewResource(17367049);
        this.g.setAdapter((SpinnerAdapter) arrayAdapter);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        a(this.f1046b);
        TcmAid.bp = 1;
        this.g.setPrompt("Select from the following Diagnosis quizzes.");
        this.g.setOnItemSelectedListener(this);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, TcmAid.bG);
        arrayAdapter.setDropDownViewResource(17367049);
        this.g.setAdapter((SpinnerAdapter) arrayAdapter);
    }

    /* access modifiers changed from: protected */
    public final void c() {
        a(this.c);
        TcmAid.bp = 2;
        this.g.setPrompt("Select from the following Herbs quizzes.");
        this.g.setOnItemSelectedListener(this);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, TcmAid.bO);
        arrayAdapter.setDropDownViewResource(17367049);
        this.g.setAdapter((SpinnerAdapter) arrayAdapter);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        a(this.d);
        TcmAid.bp = 3;
        this.g.setPrompt("Select from the following Formulas quizzes.");
        this.g.setOnItemSelectedListener(this);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, TcmAid.bW);
        arrayAdapter.setDropDownViewResource(17367049);
        this.g.setAdapter((SpinnerAdapter) arrayAdapter);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        a(this.e);
        TcmAid.bp = 4;
        this.g.setPrompt("Select from the following Points quizzes.");
        this.g.setOnItemSelectedListener(this);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, TcmAid.ce);
        arrayAdapter.setDropDownViewResource(17367049);
        this.g.setAdapter((SpinnerAdapter) arrayAdapter);
    }

    /* access modifiers changed from: protected */
    public final void f() {
        a(this.f);
        TcmAid.bp = 5;
        this.g.setPrompt("Select from the following Pulse quizzes.");
        this.g.setOnItemSelectedListener(this);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, TcmAid.cm);
        arrayAdapter.setDropDownViewResource(17367049);
        this.g.setAdapter((SpinnerAdapter) arrayAdapter);
    }

    private void a(Button button) {
        this.f1045a.setTypeface(Typeface.DEFAULT_BOLD, 1);
        this.f1045a.setTextColor(-16777216);
        this.f1046b.setTypeface(Typeface.DEFAULT_BOLD, 1);
        this.f1046b.setTextColor(-16777216);
        this.c.setTypeface(Typeface.DEFAULT_BOLD, 1);
        this.c.setTextColor(-16777216);
        this.d.setTypeface(Typeface.DEFAULT_BOLD, 1);
        this.d.setTextColor(-16777216);
        this.e.setTypeface(Typeface.DEFAULT_BOLD, 1);
        this.e.setTextColor(-16777216);
        this.f.setTypeface(Typeface.DEFAULT_BOLD, 1);
        this.f.setTextColor(-16777216);
        button.setTextColor(-256);
    }

    public void onItemSelected(AdapterView adapterView, View view, int i2, long j2) {
        TcmAid.bq = i2;
        if (i2 < 0) {
            TcmAid.bq = 0;
        }
        TcmAid.bw = 0;
        switch (TcmAid.bp) {
            case 2:
                a((String) TcmAid.bO.get(TcmAid.bq));
                break;
            case 3:
                a((String) TcmAid.bW.get(TcmAid.bq));
                break;
        }
        if (dh.B) {
            Log.i("SQT:onItemSelected()", "TcmAid.qyTestArea = " + Integer.toString(TcmAid.bp) + " - " + dh.am[TcmAid.bp]);
            switch (TcmAid.bp) {
                case 0:
                    TcmAid.br = (String) TcmAid.by.get(TcmAid.bq);
                    break;
                case 1:
                    TcmAid.br = (String) TcmAid.bG.get(TcmAid.bq);
                    break;
                case 2:
                    TcmAid.br = (String) TcmAid.bO.get(TcmAid.bq);
                    break;
                case 3:
                    TcmAid.br = (String) TcmAid.bW.get(TcmAid.bq);
                    break;
                case 4:
                    TcmAid.br = (String) TcmAid.ce.get(TcmAid.bq);
                    break;
                case 5:
                    TcmAid.br = (String) TcmAid.cm.get(TcmAid.bq);
                    break;
            }
            Log.i("SQT:onItemSelected()", "TcmAid.qyTestType = " + dh.am[TcmAid.bp].toLowerCase() + "TestArray[" + Integer.toString(TcmAid.bq) + "] - " + TcmAid.br);
            Log.i("SQT:onItemSelected()", "TcmAid.qyTabsFinished = " + Integer.toString(0));
        }
    }

    public void onNothingSelected(AdapterView adapterView) {
    }

    private void a(String str) {
        if (str.equalsIgnoreCase("Identification by Category")) {
            switch (TcmAid.bp) {
                case 0:
                case 1:
                default:
                    return;
                case 2:
                    if (((String) TcmAid.ab.get(1)).equalsIgnoreCase("20 Random Herbs")) {
                        if (n.a(this.k.q()) > 0) {
                            TcmAid.ab.remove(2);
                        }
                        TcmAid.ab.remove(1);
                        return;
                    }
                    return;
                case 3:
                    if (((String) TcmAid.O.get(1)).equalsIgnoreCase("20 Random Formulas")) {
                        if (n.a(this.k.p()) > 0) {
                            TcmAid.O.remove(2);
                        }
                        TcmAid.O.remove(1);
                        return;
                    }
                    return;
            }
        } else {
            switch (TcmAid.bp) {
                case 0:
                case 1:
                default:
                    return;
                case 2:
                    if (!((String) TcmAid.ab.get(1)).equalsIgnoreCase("20 Random Herbs")) {
                        TcmAid.ab.add(1, "20 Random Herbs");
                        int a2 = n.a(this.k.q());
                        if (a2 > 0) {
                            TcmAid.ab.add(2, Integer.toString(a2) + " Favorite Herbs");
                            return;
                        }
                        return;
                    }
                    return;
                case 3:
                    if (!((String) TcmAid.O.get(1)).equalsIgnoreCase("20 Random Formulas")) {
                        TcmAid.O.add(1, "20 Random Formulas");
                        int a3 = n.a(this.k.p());
                        if (a3 > 0) {
                            TcmAid.O.add(2, Integer.toString(a3) + " Favorite Formulas");
                            return;
                        }
                        return;
                    }
                    return;
            }
        }
    }

    private void i() {
        if (this.j == null || !this.j.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("SQT:openDataBase()", str + " opened.");
                this.j = SQLiteDatabase.openDatabase(str, null, 16);
                this.j.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.j.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("SQT:openDataBase()", str2 + " ATTACHed.");
                this.k = new n();
                this.k.a(this.j);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new s(this));
                create.show();
            }
        }
    }
}
