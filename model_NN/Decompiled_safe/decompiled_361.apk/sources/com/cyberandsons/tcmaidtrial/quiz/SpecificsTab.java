package com.cyberandsons.tcmaidtrial.quiz;

import android.app.Activity;
import android.app.AlertDialog;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class SpecificsTab extends Activity implements AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    CheckBox f1047a;

    /* renamed from: b  reason: collision with root package name */
    private TextView f1048b;
    private TextView c;
    private TextView d;
    private CheckBox e;
    private Spinner f;
    private SQLiteDatabase g;
    private n h;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.g == null || !this.g.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("SQT:openDataBase()", str + " opened.");
                this.g = SQLiteDatabase.openDatabase(str, null, 16);
                this.g.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.g.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("SQT:openDataBase()", str2 + " ATTACHed.");
                this.h = new n();
                this.h.a(this.g);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new v(this));
                create.show();
            }
        }
        if (TcmAid.cT < dh.f947b && TcmAid.cS < dh.f947b) {
            setRequestedOrientation(1);
        }
        setContentView((int) C0000R.layout.quizspecifictab);
        this.f1048b = (TextView) findViewById(C0000R.id.testarea);
        this.c = (TextView) findViewById(C0000R.id.testtype);
        this.d = (TextView) findViewById(C0000R.id.testcategory);
        this.f1047a = (CheckBox) findViewById(C0000R.id.show_answers);
        this.f1047a.setOnClickListener(new ab(this));
        this.e = (CheckBox) findViewById(C0000R.id.always_show_answers);
        this.e.setOnClickListener(new aa(this));
        boolean Z = this.h.Z();
        TcmAid.bv = Z;
        if (Z) {
            TcmAid.bu = true;
            this.f1047a.setSelected(true);
            this.f1047a.setChecked(true);
            this.f1047a.setEnabled(false);
            this.e.setSelected(true);
            this.e.setChecked(true);
        } else {
            TcmAid.bu = false;
        }
        this.f = (Spinner) findViewById(C0000R.id.categoriesofquizzes);
    }

    public void onPause() {
        super.onPause();
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x0193  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01d7  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01df  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x027a  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x02be  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02c6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onResume() {
        /*
            r11 = this;
            r9 = 1
            r8 = 0
            r7 = 17367048(0x1090008, float:2.5162948E-38)
            r6 = 0
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.bw
            switch(r0) {
                case -1: goto L_0x000f;
                case 0: goto L_0x0017;
                case 1: goto L_0x0316;
                case 2: goto L_0x031f;
                default: goto L_0x000b;
            }
        L_0x000b:
            super.onResume()
            return
        L_0x000f:
            java.lang.String r0 = "SpecificsTab"
            java.lang.String r1 = "onResume() - kQuizYourNothing"
            android.util.Log.i(r0, r1)
            goto L_0x000b
        L_0x0017:
            java.lang.String r0 = "SpecificsTab"
            java.lang.String r1 = "onResume() - kQuizYourSelectTab"
            android.util.Log.i(r0, r1)
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.bq
            if (r0 < 0) goto L_0x000b
            java.lang.String r0 = ""
            java.lang.String r1 = ""
            java.lang.String r2 = ""
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.bp
            switch(r3) {
                case 0: goto L_0x0075;
                case 1: goto L_0x0088;
                case 2: goto L_0x009b;
                case 3: goto L_0x00ae;
                case 4: goto L_0x00c2;
                case 5: goto L_0x00d6;
                default: goto L_0x002d;
            }
        L_0x002d:
            r10 = r2
            r2 = r0
            r0 = r10
        L_0x0030:
            android.widget.TextView r3 = r11.f1048b
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Testing "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            r3.setText(r2)
            android.widget.TextView r2 = r11.c
            r2.setText(r1)
            android.widget.TextView r2 = r11.d
            r2.setText(r0)
            android.widget.Spinner r0 = r11.f
            java.lang.String r2 = "Select from these categories..."
            r0.setPrompt(r2)
            android.widget.Spinner r0 = r11.f
            r0.setOnItemSelectedListener(r11)
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.bp
            switch(r0) {
                case 0: goto L_0x00ea;
                case 1: goto L_0x00f3;
                case 2: goto L_0x00fc;
                case 3: goto L_0x01e3;
                case 4: goto L_0x02ca;
                case 5: goto L_0x030d;
                default: goto L_0x0063;
            }
        L_0x0063:
            r0 = r8
        L_0x0064:
            r1 = 17367049(0x1090009, float:2.516295E-38)
            r0.setDropDownViewResource(r1)
            android.widget.Spinner r1 = r11.f
            r1.setAdapter(r0)
            android.widget.Spinner r0 = r11.f
            r0.setSelection(r6)
            goto L_0x000b
        L_0x0075:
            java.lang.String r1 = "Auricular"
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.by
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.bq
            java.lang.Object r0 = r0.get(r2)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r2 = "Select an auricular category..."
            r10 = r2
            r2 = r1
            r1 = r0
            r0 = r10
            goto L_0x0030
        L_0x0088:
            java.lang.String r1 = "Diagnosis"
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.bG
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.bq
            java.lang.Object r0 = r0.get(r2)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r2 = "Select a diagnosis category..."
            r10 = r2
            r2 = r1
            r1 = r0
            r0 = r10
            goto L_0x0030
        L_0x009b:
            java.lang.String r1 = "Herbs"
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.bO
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.bq
            java.lang.Object r0 = r0.get(r2)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r2 = "Select an herb category..."
            r10 = r2
            r2 = r1
            r1 = r0
            r0 = r10
            goto L_0x0030
        L_0x00ae:
            java.lang.String r1 = "Formulas"
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.bW
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.bq
            java.lang.Object r0 = r0.get(r2)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r2 = "Select a formula category..."
            r10 = r2
            r2 = r1
            r1 = r0
            r0 = r10
            goto L_0x0030
        L_0x00c2:
            java.lang.String r1 = "Points"
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.ce
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.bq
            java.lang.Object r0 = r0.get(r2)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r2 = "Select a point category..."
            r10 = r2
            r2 = r1
            r1 = r0
            r0 = r10
            goto L_0x0030
        L_0x00d6:
            java.lang.String r1 = "Pulse"
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.cm
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.bq
            java.lang.Object r0 = r0.get(r2)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r2 = "Select a pulse category..."
            r10 = r2
            r2 = r1
            r1 = r0
            r0 = r10
            goto L_0x0030
        L_0x00ea:
            android.widget.ArrayAdapter r0 = new android.widget.ArrayAdapter
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.j
            r0.<init>(r11, r7, r1)
            goto L_0x0064
        L_0x00f3:
            android.widget.ArrayAdapter r0 = new android.widget.ArrayAdapter
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.z
            r0.<init>(r11, r7, r1)
            goto L_0x0064
        L_0x00fc:
            java.lang.String r0 = "NCCAOM"
            int r0 = r1.indexOf(r0)
            if (r0 < 0) goto L_0x0183
            java.lang.String r0 = " WHERE main.materiamedica.nccaom_req=1 "
            java.lang.String r1 = "SELECT main.mmcategory.item_name, count(main.materiamedica.pinyin) AS cnt FROM main.mmcategory JOIN main.materiamedica ON main.mmcategory._id=main.materiamedica.mmcategory %s GROUP BY main.mmcategory.item_name HAVING count(main.materiamedica.pinyin) > 2"
            java.lang.Object[] r2 = new java.lang.Object[r9]
            r2[r6] = r0
            java.lang.String r0 = java.lang.String.format(r1, r2)
            com.cyberandsons.tcmaidtrial.TcmAid.ag = r0
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.ag
            android.database.sqlite.SQLiteDatabase r0 = r11.g     // Catch:{ SQLException -> 0x018c, NullPointerException -> 0x0197, all -> 0x01db }
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.TcmAid.ag     // Catch:{ SQLException -> 0x018c, NullPointerException -> 0x0197, all -> 0x01db }
            r3 = 0
            android.database.Cursor r0 = r0.rawQuery(r2, r3)     // Catch:{ SQLException -> 0x018c, NullPointerException -> 0x0197, all -> 0x01db }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x018c, NullPointerException -> 0x0197, all -> 0x01db }
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.ab     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            r2.clear()     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            boolean r2 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            if (r2 == 0) goto L_0x013a
        L_0x012a:
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.ab     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            r3 = 0
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            r2.add(r3)     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            boolean r2 = r0.moveToNext()     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            if (r2 != 0) goto L_0x012a
        L_0x013a:
            r0.close()     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.ab     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            r3 = 0
            java.lang.String r4 = "<Select from Herb categories>"
            r2.add(r3, r4)     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.ab     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            r3 = 1
            java.lang.String r4 = "20 Random Herbs"
            r2.add(r3, r4)     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.ab     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            r3 = 2
            java.lang.String r4 = "All Random Herbs"
            r2.add(r3, r4)     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            com.cyberandsons.tcmaidtrial.a.n r2 = r11.h     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            java.lang.String r2 = r2.q()     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            int r2 = com.cyberandsons.tcmaidtrial.a.n.a(r2)     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            if (r2 <= 0) goto L_0x017e
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            r3.<init>()     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            java.lang.String r3 = " Favorite Herbs"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.ab     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
            r4 = 3
            r3.add(r4, r2)     // Catch:{ SQLException -> 0x0344, NullPointerException -> 0x0341 }
        L_0x017e:
            if (r0 == 0) goto L_0x0183
            r0.close()
        L_0x0183:
            android.widget.ArrayAdapter r0 = new android.widget.ArrayAdapter
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.ab
            r0.<init>(r11, r7, r1)
            goto L_0x0064
        L_0x018c:
            r0 = move-exception
            r1 = r8
        L_0x018e:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x033e }
            if (r1 == 0) goto L_0x0183
            r1.close()
            goto L_0x0183
        L_0x0197:
            r0 = move-exception
            r0 = r8
        L_0x0199:
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0338 }
            java.lang.String r3 = "myVariable"
            r2.a(r3, r1)     // Catch:{ all -> 0x0338 }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0338 }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x0338 }
            java.lang.String r3 = "SPT:load_details( last step completed = redo Major Category )"
            r2.<init>(r3)     // Catch:{ all -> 0x0338 }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x0338 }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x0338 }
            r1.<init>(r11)     // Catch:{ all -> 0x0338 }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x0338 }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x0338 }
            r2 = 2131099675(0x7f06001b, float:1.781171E38)
            java.lang.String r2 = r11.getString(r2)     // Catch:{ all -> 0x0338 }
            r1.setMessage(r2)     // Catch:{ all -> 0x0338 }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.quiz.y r3 = new com.cyberandsons.tcmaidtrial.quiz.y     // Catch:{ all -> 0x0338 }
            r3.<init>(r11)     // Catch:{ all -> 0x0338 }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x0338 }
            r1.show()     // Catch:{ all -> 0x0338 }
            if (r0 == 0) goto L_0x0183
            r0.close()
            goto L_0x0183
        L_0x01db:
            r0 = move-exception
            r1 = r8
        L_0x01dd:
            if (r1 == 0) goto L_0x01e2
            r1.close()
        L_0x01e2:
            throw r0
        L_0x01e3:
            java.lang.String r0 = "NCCAOM"
            int r0 = r1.indexOf(r0)
            if (r0 < 0) goto L_0x026a
            java.lang.String r0 = " WHERE main.formulas.nccaom_req=1 "
            java.lang.String r1 = "SELECT main.majorFcategory.item_name, count(main.formulas.pinyin) AS cnt FROM main.majorFcategory JOIN main.formulas ON main.majorFcategory._id=main.formulas.majorcategory %s GROUP BY main.majorFcategory.item_name HAVING count(main.formulas.pinyin) > 2"
            java.lang.Object[] r2 = new java.lang.Object[r9]
            r2[r6] = r0
            java.lang.String r0 = java.lang.String.format(r1, r2)
            com.cyberandsons.tcmaidtrial.TcmAid.T = r0
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.T
            android.database.sqlite.SQLiteDatabase r0 = r11.g     // Catch:{ SQLException -> 0x0273, NullPointerException -> 0x027e, all -> 0x02c2 }
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.TcmAid.T     // Catch:{ SQLException -> 0x0273, NullPointerException -> 0x027e, all -> 0x02c2 }
            r3 = 0
            android.database.Cursor r0 = r0.rawQuery(r2, r3)     // Catch:{ SQLException -> 0x0273, NullPointerException -> 0x027e, all -> 0x02c2 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0273, NullPointerException -> 0x027e, all -> 0x02c2 }
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.O     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            r2.clear()     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            boolean r2 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            if (r2 == 0) goto L_0x0221
        L_0x0211:
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.O     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            r3 = 0
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            r2.add(r3)     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            boolean r2 = r0.moveToNext()     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            if (r2 != 0) goto L_0x0211
        L_0x0221:
            r0.close()     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.O     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            r3 = 0
            java.lang.String r4 = "<Select from Formula categories>"
            r2.add(r3, r4)     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.O     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            r3 = 1
            java.lang.String r4 = "20 Random Formulas"
            r2.add(r3, r4)     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.O     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            r3 = 2
            java.lang.String r4 = "All Random Formulas"
            r2.add(r3, r4)     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            com.cyberandsons.tcmaidtrial.a.n r2 = r11.h     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            java.lang.String r2 = r2.p()     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            int r2 = com.cyberandsons.tcmaidtrial.a.n.a(r2)     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            if (r2 <= 0) goto L_0x0265
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            r3.<init>()     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            java.lang.String r3 = " Favorite Formulas"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.O     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
            r4 = 3
            r3.add(r4, r2)     // Catch:{ SQLException -> 0x0332, NullPointerException -> 0x032f }
        L_0x0265:
            if (r0 == 0) goto L_0x026a
            r0.close()
        L_0x026a:
            android.widget.ArrayAdapter r0 = new android.widget.ArrayAdapter
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.O
            r0.<init>(r11, r7, r1)
            goto L_0x0064
        L_0x0273:
            r0 = move-exception
            r1 = r8
        L_0x0275:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x032d }
            if (r1 == 0) goto L_0x026a
            r1.close()
            goto L_0x026a
        L_0x027e:
            r0 = move-exception
            r0 = r8
        L_0x0280:
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0328 }
            java.lang.String r3 = "myVariable"
            r2.a(r3, r1)     // Catch:{ all -> 0x0328 }
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x0328 }
            java.lang.NullPointerException r2 = new java.lang.NullPointerException     // Catch:{ all -> 0x0328 }
            java.lang.String r3 = "SPT:load_details( last step completed = redo Major Category )"
            r2.<init>(r3)     // Catch:{ all -> 0x0328 }
            r1.handleSilentException(r2)     // Catch:{ all -> 0x0328 }
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder     // Catch:{ all -> 0x0328 }
            r1.<init>(r11)     // Catch:{ all -> 0x0328 }
            android.app.AlertDialog r1 = r1.create()     // Catch:{ all -> 0x0328 }
            java.lang.String r2 = "Attention:"
            r1.setTitle(r2)     // Catch:{ all -> 0x0328 }
            r2 = 2131099675(0x7f06001b, float:1.781171E38)
            java.lang.String r2 = r11.getString(r2)     // Catch:{ all -> 0x0328 }
            r1.setMessage(r2)     // Catch:{ all -> 0x0328 }
            java.lang.String r2 = "OK"
            com.cyberandsons.tcmaidtrial.quiz.w r3 = new com.cyberandsons.tcmaidtrial.quiz.w     // Catch:{ all -> 0x0328 }
            r3.<init>(r11)     // Catch:{ all -> 0x0328 }
            r1.setButton(r2, r3)     // Catch:{ all -> 0x0328 }
            r1.show()     // Catch:{ all -> 0x0328 }
            if (r0 == 0) goto L_0x026a
            r0.close()
            goto L_0x026a
        L_0x02c2:
            r0 = move-exception
            r1 = r8
        L_0x02c4:
            if (r1 == 0) goto L_0x02c9
            r1.close()
        L_0x02c9:
            throw r0
        L_0x02ca:
            java.lang.String r0 = "Point Categories/Attributes"
            int r0 = r1.indexOf(r0)
            if (r0 < 0) goto L_0x0304
            java.lang.String r0 = "SELECT item_name FROM main.meridian WHERE item_name NOT LIKE '%-%' UNION SELECT userDB.pointcategory.name FROM userDB.pointcategory ORDER BY 1"
            com.cyberandsons.tcmaidtrial.TcmAid.ar = r0
            android.database.sqlite.SQLiteDatabase r0 = r11.g
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.ar
            android.database.Cursor r0 = r0.rawQuery(r1, r8)
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.am
            r1.clear()
            boolean r1 = r0.moveToFirst()
            if (r1 == 0) goto L_0x02fa
        L_0x02eb:
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.am
            java.lang.String r2 = r0.getString(r6)
            r1.add(r2)
            boolean r1 = r0.moveToNext()
            if (r1 != 0) goto L_0x02eb
        L_0x02fa:
            r0.close()
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.am
            java.lang.String r1 = "<Select from Point categories>"
            r0.add(r6, r1)
        L_0x0304:
            android.widget.ArrayAdapter r0 = new android.widget.ArrayAdapter
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.am
            r0.<init>(r11, r7, r1)
            goto L_0x0064
        L_0x030d:
            android.widget.ArrayAdapter r0 = new android.widget.ArrayAdapter
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.aA
            r0.<init>(r11, r7, r1)
            goto L_0x0064
        L_0x0316:
            java.lang.String r0 = "SpecificsTab"
            java.lang.String r1 = "onResume() - kQuizYourSpecificTab"
            android.util.Log.i(r0, r1)
            goto L_0x000b
        L_0x031f:
            java.lang.String r0 = "SpecificsTab"
            java.lang.String r1 = "onResume() - kQuizYourRunTab"
            android.util.Log.i(r0, r1)
            goto L_0x000b
        L_0x0328:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x02c4
        L_0x032d:
            r0 = move-exception
            goto L_0x02c4
        L_0x032f:
            r2 = move-exception
            goto L_0x0280
        L_0x0332:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0275
        L_0x0338:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x01dd
        L_0x033e:
            r0 = move-exception
            goto L_0x01dd
        L_0x0341:
            r2 = move-exception
            goto L_0x0199
        L_0x0344:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x018e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.quiz.SpecificsTab.onResume():void");
    }

    public void onDestroy() {
        TcmAid.bp = -1;
        TcmAid.bw = -1;
        try {
            if (this.g != null && this.g.isOpen()) {
                this.g.close();
                this.g = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        boolean isChecked = this.e.isChecked();
        TcmAid.bv = isChecked;
        if (isChecked) {
            TcmAid.bu = true;
            this.f1047a.setSelected(true);
            this.f1047a.setChecked(true);
            this.f1047a.invalidate();
            this.f1047a.setEnabled(false);
            this.h.x(true, this.g);
            return;
        }
        this.f1047a.setEnabled(true);
        this.h.x(false, this.g);
    }

    public void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        TcmAid.bs = i;
        if (i < 0) {
            TcmAid.bs = 0;
        }
        TcmAid.bw = 1;
        if (dh.B) {
            switch (TcmAid.bp) {
                case 0:
                    TcmAid.bt = (String) TcmAid.j.get(TcmAid.bs);
                    break;
                case 1:
                    TcmAid.bt = (String) TcmAid.z.get(TcmAid.bs);
                    break;
                case 2:
                    TcmAid.bt = (String) TcmAid.ab.get(TcmAid.bs);
                    break;
                case 3:
                    TcmAid.bt = (String) TcmAid.O.get(TcmAid.bs);
                    break;
                case 4:
                    TcmAid.bt = (String) TcmAid.am.get(TcmAid.bs);
                    break;
                case 5:
                    TcmAid.bt = (String) TcmAid.aA.get(TcmAid.bs);
                    break;
            }
            Log.i("ST:onItemSelected()", "TcmAid.qyTestCategory = " + dh.am[TcmAid.bp].toLowerCase() + "CategoryArray[" + Integer.toString(TcmAid.bs) + "] - " + TcmAid.bt);
            Log.i("ST:onItemSelected()", "TcmAid.qyTabsFinished = " + Integer.toString(1));
        }
    }

    public void onNothingSelected(AdapterView adapterView) {
    }
}
