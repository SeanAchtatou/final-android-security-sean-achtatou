package com.cyberandsons.tcmaidtrial.quiz;

import android.database.Cursor;
import android.text.Html;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class ai implements SimpleCursorAdapter.ViewBinder {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1059a = false;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ c f1060b;

    ai(c cVar) {
        this.f1060b = cVar;
    }

    public final boolean setViewValue(View view, Cursor cursor, int i) {
        TextView textView = (TextView) view;
        if (i == 1) {
            String format = String.format("%.1f", Float.valueOf((((float) cursor.getInt(3)) / ((float) cursor.getInt(4))) * 100.0f));
            textView.setText(Html.fromHtml("<b>" + cursor.getString(1) + "&nbsp;&nbsp;&nbsp;" + String.format("%-12s", dh.am[cursor.getInt(2)]) + dh.a("Auricular  " + format, dh.am[cursor.getInt(2)], textView.getTextSize()).toString() + format + "%<br />" + "<small>" + cursor.getString(5) + "</small>"));
            this.f1059a = true;
        }
        return this.f1059a;
    }
}
