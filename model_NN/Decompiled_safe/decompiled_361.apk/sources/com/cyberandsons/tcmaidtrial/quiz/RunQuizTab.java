package com.cyberandsons.tcmaidtrial.quiz;

import android.app.Activity;
import android.app.AlertDialog;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.a;
import com.cyberandsons.tcmaidtrial.a.c;
import com.cyberandsons.tcmaidtrial.a.d;
import com.cyberandsons.tcmaidtrial.a.l;
import com.cyberandsons.tcmaidtrial.a.m;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.o;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.a.t;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.util.ArrayList;

public class RunQuizTab extends Activity {

    /* renamed from: a  reason: collision with root package name */
    public static ArrayList f1043a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    public static ArrayList f1044b = new ArrayList();
    public static ArrayList c = new ArrayList();
    public static ArrayList d = new ArrayList();
    public static ArrayList e = new ArrayList();
    public static ArrayList f = new ArrayList();
    public static ArrayList g = new ArrayList();
    public static ArrayList h = new ArrayList();
    private static ArrayList w = new ArrayList();
    private static ArrayList x = new ArrayList();
    private boolean A = true;
    private boolean B;
    i i;
    t j;
    private TextView k;
    private TextView l;
    private TextView m;
    private TextView n;
    private TextView o;
    private TextView p;
    private ImageView q;
    private ImageView r;
    private ImageView s;
    private Button t;
    private Button u;
    private Button v;
    private SQLiteDatabase y;
    private n z;

    public RunQuizTab() {
        this.B = !this.A;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0028 A[Catch:{ c -> 0x00be }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void a(com.cyberandsons.tcmaidtrial.quiz.RunQuizTab r13, int r14) {
        /*
            r11 = 4
            r10 = 3
            r9 = 2
            r8 = 1
            r7 = 0
            com.cyberandsons.tcmaidtrial.quiz.t r1 = r13.j     // Catch:{ c -> 0x00be }
            int r0 = r1.f     // Catch:{ c -> 0x00be }
            if (r0 != r14) goto L_0x0088
            boolean r0 = r1.z     // Catch:{ c -> 0x00be }
            r2 = r0
        L_0x000e:
            if (r2 != 0) goto L_0x026b
            boolean r0 = r1.k     // Catch:{ c -> 0x00be }
            if (r0 != 0) goto L_0x001a
            int r0 = r1.j     // Catch:{ c -> 0x00be }
            int r0 = r0 + 1
            r1.j = r0     // Catch:{ c -> 0x00be }
        L_0x001a:
            r0 = 0
            r3 = -1
            int r4 = com.cyberandsons.tcmaidtrial.TcmAid.bp     // Catch:{ c -> 0x00be }
            switch(r4) {
                case 0: goto L_0x008c;
                case 1: goto L_0x00ef;
                case 2: goto L_0x013b;
                case 3: goto L_0x0187;
                case 4: goto L_0x01d3;
                case 5: goto L_0x021f;
                default: goto L_0x0021;
            }     // Catch:{ c -> 0x00be }
        L_0x0021:
            r12 = r3
            r3 = r0
            r0 = r12
        L_0x0024:
            boolean r4 = r1.k     // Catch:{ c -> 0x00be }
            if (r4 != 0) goto L_0x0036
            java.util.ArrayList r4 = r1.u     // Catch:{ c -> 0x00be }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ c -> 0x00be }
            r4.add(r0)     // Catch:{ c -> 0x00be }
            java.util.ArrayList r0 = r1.t     // Catch:{ c -> 0x00be }
            r0.add(r3)     // Catch:{ c -> 0x00be }
        L_0x0036:
            boolean r0 = r1.z     // Catch:{ c -> 0x00be }
            r1.k = r0     // Catch:{ c -> 0x00be }
        L_0x003a:
            r0 = r2
        L_0x003b:
            if (r0 != 0) goto L_0x0271
            boolean r1 = com.cyberandsons.tcmaidtrial.TcmAid.bu
            if (r1 == 0) goto L_0x0271
            android.widget.ImageView r0 = r13.r
            r0.setVisibility(r11)
            android.widget.ImageView r0 = r13.s
            r0.setVisibility(r7)
            com.cyberandsons.tcmaidtrial.quiz.t r0 = r13.j
            int r0 = r0.f
            java.lang.String[] r1 = new java.lang.String[r10]
            java.lang.String r2 = "A"
            r1[r7] = r2
            java.lang.String r2 = "B"
            r1[r8] = r2
            java.lang.String r2 = "C"
            r1[r9] = r2
            r0 = r1[r0]
            java.lang.String r1 = "Correct answer is: '%s'.\nClick OK, then click on correct answer to continue to next question."
            java.lang.Object[] r2 = new java.lang.Object[r8]
            r2[r7] = r0
            java.lang.String r0 = java.lang.String.format(r1, r2)
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder
            r1.<init>(r13)
            android.app.AlertDialog r1 = r1.create()
            java.lang.String r2 = ""
            r1.setTitle(r2)
            r1.setMessage(r0)
            java.lang.String r0 = "OK"
            com.cyberandsons.tcmaidtrial.quiz.x r2 = new com.cyberandsons.tcmaidtrial.quiz.x
            r2.<init>(r13)
            r1.setButton(r0, r2)
            r1.show()
        L_0x0087:
            return
        L_0x0088:
            boolean r0 = r1.A     // Catch:{ c -> 0x00be }
            r2 = r0
            goto L_0x000e
        L_0x008c:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f1043a     // Catch:{ c -> 0x00be }
            int r0 = r0.size()     // Catch:{ c -> 0x00be }
            int r3 = r1.e     // Catch:{ c -> 0x00be }
            if (r3 <= r0) goto L_0x00d5
            com.cyberandsons.tcmaidtrial.b.c r2 = new com.cyberandsons.tcmaidtrial.b.c     // Catch:{ c -> 0x00be }
            java.lang.String r3 = "isAnswerCorrect() : Area = %d\nArrSz = %d\nCorrAns = %d"
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ c -> 0x00be }
            r5 = 0
            int r6 = com.cyberandsons.tcmaidtrial.TcmAid.bp     // Catch:{ c -> 0x00be }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ c -> 0x00be }
            r4[r5] = r6     // Catch:{ c -> 0x00be }
            r5 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ c -> 0x00be }
            r4[r5] = r0     // Catch:{ c -> 0x00be }
            r0 = 2
            int r1 = r1.e     // Catch:{ c -> 0x00be }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ c -> 0x00be }
            r4[r0] = r1     // Catch:{ c -> 0x00be }
            java.lang.String r0 = java.lang.String.format(r3, r4)     // Catch:{ c -> 0x00be }
            r2.<init>(r0)     // Catch:{ c -> 0x00be }
            throw r2     // Catch:{ c -> 0x00be }
        L_0x00be:
            r0 = move-exception
            java.lang.String r1 = "Correct Answer Problem"
            java.lang.String r2 = "Checking the correct answer resulted in an unexpected condition.\n\nSupport is being notified of this test configuration.\n\nPlease select another quiz."
            java.lang.Object[] r3 = new java.lang.Object[r7]
            java.lang.String r2 = java.lang.String.format(r2, r3)
            java.lang.String r3 = "answerSelected()"
            java.lang.String r0 = r0.getMessage()
            r13.a(r3, r1, r2, r0)
            r0 = r7
            goto L_0x003b
        L_0x00d5:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f1043a     // Catch:{ c -> 0x00be }
            int r3 = r1.e     // Catch:{ c -> 0x00be }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.f r0 = (com.cyberandsons.tcmaidtrial.a.f) r0     // Catch:{ c -> 0x00be }
            r1.m = r0     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.f r0 = r1.m     // Catch:{ c -> 0x00be }
            int r0 = r0.a()     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.f r3 = r1.m     // Catch:{ c -> 0x00be }
            java.lang.String r3 = r3.b()     // Catch:{ c -> 0x00be }
            goto L_0x0024
        L_0x00ef:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f1044b     // Catch:{ c -> 0x00be }
            int r0 = r0.size()     // Catch:{ c -> 0x00be }
            int r3 = r1.e     // Catch:{ c -> 0x00be }
            if (r3 <= r0) goto L_0x0121
            com.cyberandsons.tcmaidtrial.b.c r2 = new com.cyberandsons.tcmaidtrial.b.c     // Catch:{ c -> 0x00be }
            java.lang.String r3 = "isAnswerCorrect() : Area = %d\nArrSz = %d\nCorrAns = %d"
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ c -> 0x00be }
            r5 = 0
            int r6 = com.cyberandsons.tcmaidtrial.TcmAid.bp     // Catch:{ c -> 0x00be }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ c -> 0x00be }
            r4[r5] = r6     // Catch:{ c -> 0x00be }
            r5 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ c -> 0x00be }
            r4[r5] = r0     // Catch:{ c -> 0x00be }
            r0 = 2
            int r1 = r1.e     // Catch:{ c -> 0x00be }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ c -> 0x00be }
            r4[r0] = r1     // Catch:{ c -> 0x00be }
            java.lang.String r0 = java.lang.String.format(r3, r4)     // Catch:{ c -> 0x00be }
            r2.<init>(r0)     // Catch:{ c -> 0x00be }
            throw r2     // Catch:{ c -> 0x00be }
        L_0x0121:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f1044b     // Catch:{ c -> 0x00be }
            int r3 = r1.e     // Catch:{ c -> 0x00be }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.r r0 = (com.cyberandsons.tcmaidtrial.a.r) r0     // Catch:{ c -> 0x00be }
            r1.n = r0     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.r r0 = r1.n     // Catch:{ c -> 0x00be }
            int r0 = r0.a()     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.r r3 = r1.n     // Catch:{ c -> 0x00be }
            java.lang.String r3 = r3.b()     // Catch:{ c -> 0x00be }
            goto L_0x0024
        L_0x013b:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.c     // Catch:{ c -> 0x00be }
            int r0 = r0.size()     // Catch:{ c -> 0x00be }
            int r3 = r1.e     // Catch:{ c -> 0x00be }
            if (r3 <= r0) goto L_0x016d
            com.cyberandsons.tcmaidtrial.b.c r2 = new com.cyberandsons.tcmaidtrial.b.c     // Catch:{ c -> 0x00be }
            java.lang.String r3 = "isAnswerCorrect() : Area = %d\nArrSz = %d\nCorrAns = %d"
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ c -> 0x00be }
            r5 = 0
            int r6 = com.cyberandsons.tcmaidtrial.TcmAid.bp     // Catch:{ c -> 0x00be }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ c -> 0x00be }
            r4[r5] = r6     // Catch:{ c -> 0x00be }
            r5 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ c -> 0x00be }
            r4[r5] = r0     // Catch:{ c -> 0x00be }
            r0 = 2
            int r1 = r1.e     // Catch:{ c -> 0x00be }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ c -> 0x00be }
            r4[r0] = r1     // Catch:{ c -> 0x00be }
            java.lang.String r0 = java.lang.String.format(r3, r4)     // Catch:{ c -> 0x00be }
            r2.<init>(r0)     // Catch:{ c -> 0x00be }
            throw r2     // Catch:{ c -> 0x00be }
        L_0x016d:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.c     // Catch:{ c -> 0x00be }
            int r3 = r1.e     // Catch:{ c -> 0x00be }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.g r0 = (com.cyberandsons.tcmaidtrial.a.g) r0     // Catch:{ c -> 0x00be }
            r1.o = r0     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.g r0 = r1.o     // Catch:{ c -> 0x00be }
            int r0 = r0.a()     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.g r3 = r1.o     // Catch:{ c -> 0x00be }
            java.lang.String r3 = r3.b()     // Catch:{ c -> 0x00be }
            goto L_0x0024
        L_0x0187:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.e     // Catch:{ c -> 0x00be }
            int r0 = r0.size()     // Catch:{ c -> 0x00be }
            int r3 = r1.e     // Catch:{ c -> 0x00be }
            if (r3 <= r0) goto L_0x01b9
            com.cyberandsons.tcmaidtrial.b.c r2 = new com.cyberandsons.tcmaidtrial.b.c     // Catch:{ c -> 0x00be }
            java.lang.String r3 = "isAnswerCorrect() : Area = %d\nArrSz = %d\nCorrAns = %d"
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ c -> 0x00be }
            r5 = 0
            int r6 = com.cyberandsons.tcmaidtrial.TcmAid.bp     // Catch:{ c -> 0x00be }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ c -> 0x00be }
            r4[r5] = r6     // Catch:{ c -> 0x00be }
            r5 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ c -> 0x00be }
            r4[r5] = r0     // Catch:{ c -> 0x00be }
            r0 = 2
            int r1 = r1.e     // Catch:{ c -> 0x00be }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ c -> 0x00be }
            r4[r0] = r1     // Catch:{ c -> 0x00be }
            java.lang.String r0 = java.lang.String.format(r3, r4)     // Catch:{ c -> 0x00be }
            r2.<init>(r0)     // Catch:{ c -> 0x00be }
            throw r2     // Catch:{ c -> 0x00be }
        L_0x01b9:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.e     // Catch:{ c -> 0x00be }
            int r3 = r1.e     // Catch:{ c -> 0x00be }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.e r0 = (com.cyberandsons.tcmaidtrial.a.e) r0     // Catch:{ c -> 0x00be }
            r1.p = r0     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.e r0 = r1.p     // Catch:{ c -> 0x00be }
            int r0 = r0.a()     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.e r3 = r1.p     // Catch:{ c -> 0x00be }
            java.lang.String r3 = r3.b()     // Catch:{ c -> 0x00be }
            goto L_0x0024
        L_0x01d3:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.g     // Catch:{ c -> 0x00be }
            int r0 = r0.size()     // Catch:{ c -> 0x00be }
            int r3 = r1.e     // Catch:{ c -> 0x00be }
            if (r3 <= r0) goto L_0x0205
            com.cyberandsons.tcmaidtrial.b.c r2 = new com.cyberandsons.tcmaidtrial.b.c     // Catch:{ c -> 0x00be }
            java.lang.String r3 = "isAnswerCorrect() : Area = %d\nArrSz = %d\nCorrAns = %d"
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ c -> 0x00be }
            r5 = 0
            int r6 = com.cyberandsons.tcmaidtrial.TcmAid.bp     // Catch:{ c -> 0x00be }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ c -> 0x00be }
            r4[r5] = r6     // Catch:{ c -> 0x00be }
            r5 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ c -> 0x00be }
            r4[r5] = r0     // Catch:{ c -> 0x00be }
            r0 = 2
            int r1 = r1.e     // Catch:{ c -> 0x00be }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ c -> 0x00be }
            r4[r0] = r1     // Catch:{ c -> 0x00be }
            java.lang.String r0 = java.lang.String.format(r3, r4)     // Catch:{ c -> 0x00be }
            r2.<init>(r0)     // Catch:{ c -> 0x00be }
            throw r2     // Catch:{ c -> 0x00be }
        L_0x0205:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.g     // Catch:{ c -> 0x00be }
            int r3 = r1.e     // Catch:{ c -> 0x00be }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.i r0 = (com.cyberandsons.tcmaidtrial.a.i) r0     // Catch:{ c -> 0x00be }
            r1.q = r0     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.i r0 = r1.q     // Catch:{ c -> 0x00be }
            int r0 = r0.a()     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.i r3 = r1.q     // Catch:{ c -> 0x00be }
            java.lang.String r3 = r3.b()     // Catch:{ c -> 0x00be }
            goto L_0x0024
        L_0x021f:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.h     // Catch:{ c -> 0x00be }
            int r0 = r0.size()     // Catch:{ c -> 0x00be }
            int r3 = r1.e     // Catch:{ c -> 0x00be }
            if (r3 <= r0) goto L_0x0251
            com.cyberandsons.tcmaidtrial.b.c r2 = new com.cyberandsons.tcmaidtrial.b.c     // Catch:{ c -> 0x00be }
            java.lang.String r3 = "isAnswerCorrect() : Area = %d\nArrSz = %d\nCorrAns = %d"
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ c -> 0x00be }
            r5 = 0
            int r6 = com.cyberandsons.tcmaidtrial.TcmAid.bp     // Catch:{ c -> 0x00be }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ c -> 0x00be }
            r4[r5] = r6     // Catch:{ c -> 0x00be }
            r5 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ c -> 0x00be }
            r4[r5] = r0     // Catch:{ c -> 0x00be }
            r0 = 2
            int r1 = r1.e     // Catch:{ c -> 0x00be }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ c -> 0x00be }
            r4[r0] = r1     // Catch:{ c -> 0x00be }
            java.lang.String r0 = java.lang.String.format(r3, r4)     // Catch:{ c -> 0x00be }
            r2.<init>(r0)     // Catch:{ c -> 0x00be }
            throw r2     // Catch:{ c -> 0x00be }
        L_0x0251:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.h     // Catch:{ c -> 0x00be }
            int r3 = r1.e     // Catch:{ c -> 0x00be }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.h r0 = (com.cyberandsons.tcmaidtrial.a.h) r0     // Catch:{ c -> 0x00be }
            r1.r = r0     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.h r0 = r1.r     // Catch:{ c -> 0x00be }
            int r0 = r0.a()     // Catch:{ c -> 0x00be }
            com.cyberandsons.tcmaidtrial.a.h r3 = r1.r     // Catch:{ c -> 0x00be }
            java.lang.String r3 = r3.b()     // Catch:{ c -> 0x00be }
            goto L_0x0024
        L_0x026b:
            boolean r0 = r1.A     // Catch:{ c -> 0x00be }
            r1.k = r0     // Catch:{ c -> 0x00be }
            goto L_0x003a
        L_0x0271:
            if (r0 == 0) goto L_0x0284
            android.widget.ImageView r0 = r13.r
            r0.setVisibility(r7)
            android.widget.ImageView r0 = r13.s
            r0.setVisibility(r11)
        L_0x027d:
            android.database.sqlite.SQLiteDatabase r0 = r13.y
            r13.b(r0)
            goto L_0x0087
        L_0x0284:
            android.widget.ImageView r0 = r13.r
            r0.setVisibility(r11)
            android.widget.ImageView r0 = r13.s
            r0.setVisibility(r7)
            goto L_0x027d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.a(com.cyberandsons.tcmaidtrial.quiz.RunQuizTab, int):void");
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a();
        if (TcmAid.cT < dh.f947b && TcmAid.cS < dh.f947b) {
            setRequestedOrientation(1);
        }
        setContentView((int) C0000R.layout.quizruntab);
        this.k = (TextView) findViewById(C0000R.id.quizcounter);
        this.l = (TextView) findViewById(C0000R.id.quizname);
        this.p = (TextView) findViewById(C0000R.id.quizquestion);
        this.p.setVisibility(0);
        this.q = (ImageView) findViewById(C0000R.id.qy_image);
        this.q.setVisibility(4);
        this.m = (TextView) findViewById(C0000R.id.answer_text_a);
        this.n = (TextView) findViewById(C0000R.id.answer_text_b);
        this.o = (TextView) findViewById(C0000R.id.answer_text_c);
        this.r = (ImageView) findViewById(C0000R.id.correct);
        this.r.setVisibility(0);
        this.s = (ImageView) findViewById(C0000R.id.wrong);
        this.s.setVisibility(0);
        this.t = (Button) findViewById(C0000R.id.answer_a);
        this.t.setOnClickListener(new ac(this));
        this.u = (Button) findViewById(C0000R.id.answer_b);
        this.u.setOnClickListener(new af(this));
        this.v = (Button) findViewById(C0000R.id.answer_c);
        this.v.setOnClickListener(new ag(this));
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        a();
        Log.i("RQT:onResume()", "onResume()");
        switch (TcmAid.bw) {
            case -1:
                Log.i("RQT:onResume()", "finished kQuizYourNothing");
                break;
            case 0:
                Log.i("RQT:onResume()", "finished kQuizYourSelectTab");
                break;
            case 1:
                Log.i("RQT:onResume()", "finished kQuizYourSpecificTab");
                Log.i("RQT:onResume()", "TcmAid.qyTestArea = " + Integer.toString(TcmAid.bp));
                Log.i("RQT:onResume()", "TcmAid.qyTestCategory = " + Integer.toString(TcmAid.bs));
                Log.i("RQT:onResume()", "TcmAid.qyTestType = " + Integer.toString(TcmAid.bq));
                if (TcmAid.bq > 0 && TcmAid.bp >= 0 && TcmAid.bs > 0) {
                    this.k.setText("0/0");
                    String str = "";
                    int i2 = TcmAid.bq - 1;
                    if (i2 < 0) {
                        i2 = 0;
                    }
                    int i3 = TcmAid.bs;
                    switch (TcmAid.bp) {
                        case 0:
                            str = (String) TcmAid.bD.get(i2);
                            break;
                        case 1:
                            str = (String) TcmAid.bL.get(i2);
                            break;
                        case 2:
                            str = (String) TcmAid.bT.get(i2);
                            break;
                        case 3:
                            str = (String) TcmAid.cb.get(i2);
                            break;
                        case 4:
                            str = (String) TcmAid.cj.get(i2);
                            break;
                        case 5:
                            str = (String) TcmAid.cr.get(i2);
                            break;
                    }
                    this.l.setText(str);
                    a(i2, TcmAid.bq, i3);
                    b(this.y);
                    if (this.j != null) {
                        this.j.v = System.currentTimeMillis();
                        break;
                    }
                }
                break;
            case 2:
                Log.i("RQT:onResume()", "finished kQuizYourRunTab");
                break;
        }
        super.onResume();
    }

    public void onDestroy() {
        try {
            if (this.y != null && this.y.isOpen()) {
                this.y.close();
                this.y = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    private void a(int i2, int i3, int i4) {
        int a2;
        this.i = new i();
        this.i.l = this.B;
        this.i.m = this.B;
        this.i.k = this.B;
        switch (TcmAid.bp) {
            case 0:
                this.i.f1069b = (String) TcmAid.bz.get(i2);
                this.i.c = (String) TcmAid.bA.get(i2);
                this.i.d = (String) TcmAid.bB.get(i2);
                this.i.e = (String) TcmAid.bC.get(i2);
                this.i.f = (String) TcmAid.bD.get(i2);
                this.i.g = (String) TcmAid.by.get(i3);
                this.i.i = ((Integer) TcmAid.bE.get(i2)).intValue();
                this.i.j = ((Integer) TcmAid.bF.get(i2)).intValue();
                this.i.h = (String) TcmAid.j.get(i4);
                break;
            case 1:
                this.i.f1069b = (String) TcmAid.bH.get(i2);
                this.i.c = (String) TcmAid.bI.get(i2);
                this.i.d = (String) TcmAid.bJ.get(i2);
                this.i.e = (String) TcmAid.bK.get(i2);
                this.i.f = (String) TcmAid.bL.get(i2);
                this.i.g = (String) TcmAid.bG.get(i3);
                this.i.i = ((Integer) TcmAid.bM.get(i2)).intValue();
                this.i.j = ((Integer) TcmAid.bN.get(i2)).intValue();
                this.i.h = (String) TcmAid.z.get(i4);
                break;
            case 2:
                this.i.f1069b = (String) TcmAid.bP.get(i2);
                this.i.c = (String) TcmAid.bQ.get(i2);
                this.i.d = (String) TcmAid.bR.get(i2);
                this.i.e = (String) TcmAid.bS.get(i2);
                this.i.f = (String) TcmAid.bT.get(i2);
                this.i.g = (String) TcmAid.bO.get(i3);
                this.i.i = ((Integer) TcmAid.bU.get(i2)).intValue();
                this.i.j = ((Integer) TcmAid.bV.get(i2)).intValue();
                this.i.h = (String) TcmAid.ab.get(i4);
                if (this.i.g.equalsIgnoreCase("Identification by Category")) {
                    this.i.n = this.A;
                    break;
                }
                break;
            case 3:
                this.i.f1069b = (String) TcmAid.bX.get(i2);
                this.i.c = (String) TcmAid.bY.get(i2);
                this.i.d = (String) TcmAid.bZ.get(i2);
                this.i.e = (String) TcmAid.ca.get(i2);
                this.i.f = (String) TcmAid.cb.get(i2);
                this.i.g = (String) TcmAid.bW.get(i3);
                this.i.i = ((Integer) TcmAid.cc.get(i2)).intValue();
                this.i.j = ((Integer) TcmAid.cd.get(i2)).intValue();
                this.i.h = (String) TcmAid.O.get(i4);
                if (this.i.g.equalsIgnoreCase("Identification by Category")) {
                    this.i.n = this.A;
                    break;
                }
                break;
            case 4:
                this.i.f1069b = (String) TcmAid.cf.get(i2);
                this.i.c = (String) TcmAid.cg.get(i2);
                this.i.d = (String) TcmAid.ch.get(i2);
                this.i.e = (String) TcmAid.ci.get(i2);
                this.i.f = (String) TcmAid.cj.get(i2);
                this.i.g = (String) TcmAid.ce.get(i3);
                this.i.i = ((Integer) TcmAid.ck.get(i2)).intValue();
                this.i.j = ((Integer) TcmAid.cl.get(i2)).intValue();
                this.i.h = (String) TcmAid.am.get(i4);
                if (this.i.g.equalsIgnoreCase("Point Categories/Attributes")) {
                    this.i.o = this.A;
                    break;
                }
                break;
            case 5:
                this.i.f1069b = (String) TcmAid.cn.get(i2);
                this.i.c = (String) TcmAid.co.get(i2);
                this.i.d = (String) TcmAid.cp.get(i2);
                this.i.e = (String) TcmAid.cq.get(i2);
                this.i.f = (String) TcmAid.cr.get(i2);
                this.i.g = (String) TcmAid.cm.get(i3);
                this.i.i = ((Integer) TcmAid.cs.get(i2)).intValue();
                this.i.j = ((Integer) TcmAid.ct.get(i2)).intValue();
                this.i.h = (String) TcmAid.aA.get(i4);
                break;
        }
        if (this.i.g.indexOf("Image") > 0) {
            this.i.k = this.A;
        } else {
            this.i.k = this.B;
        }
        if (this.i.h.indexOf("Random") > 0) {
            if (this.i.h.indexOf("All Random") >= 0) {
                this.i.f1068a = String.format("%s %s", this.i.f1069b, this.i.d.replace("LIMIT 20", ""));
            } else {
                this.i.f1068a = String.format("%s %s", this.i.f1069b, this.i.d);
            }
            if (TcmAid.bp == 4 && this.i.k) {
                this.i.l = this.A;
            }
        }
        if (dh.B) {
            this.i.a();
        }
        if (this.i.h.indexOf("Favorite") > 0) {
            String str = "";
            switch (TcmAid.bp) {
                case 0:
                    String n2 = this.z.n();
                    StringBuffer stringBuffer = new StringBuffer("(");
                    String[] split = n2.split(",");
                    boolean z2 = true;
                    for (int i5 = 0; i5 < split.length; i5++) {
                        if (z2) {
                            stringBuffer.append(String.format("%d", Integer.valueOf(Integer.parseInt(split[i5]))));
                            z2 = false;
                        } else {
                            stringBuffer.append(String.format(",%d", Integer.valueOf(Integer.parseInt(split[i5]))));
                        }
                    }
                    stringBuffer.append(')');
                    str = stringBuffer.toString();
                    break;
                case 1:
                    str = t.b(this.z.o());
                    break;
                case 2:
                    str = m.g(this.z.q());
                    break;
                case 3:
                    str = c.d(this.z.p());
                    break;
                case 4:
                    str = a.a(this.z.r());
                    break;
                case 5:
                    str = a.a(this.z.r());
                    break;
            }
            this.i.f1068a = String.format("%s %s %s", this.i.f1069b, this.i.e, str);
            this.i.m = this.A;
        } else if (this.i.h.indexOf("Random") < 0) {
            if (TcmAid.bp == 1) {
                this.i.f1068a = String.format("%s %s'%%%s%%'", this.i.f1069b, this.i.c, this.i.h.substring(0, this.i.h.indexOf(" Etiologies")));
            } else {
                boolean z3 = this.A;
                if (TcmAid.bp == 2) {
                    int a3 = l.a(this.i.h, this.y);
                    if (a3 > 0) {
                        String str2 = (String) l.a(a3, this.y);
                        if (str2.length() > 0) {
                            String g2 = m.g(str2);
                            if (g2.length() > 0) {
                                z3 = this.B;
                                this.i.f1068a = String.format("%s %s %s", this.i.f1069b, this.i.e, g2);
                                this.i.m = this.A;
                            }
                        }
                    }
                } else if (TcmAid.bp == 3) {
                    int a4 = d.a(this.i.h, this.y);
                    if (a4 > 0) {
                        String str3 = (String) d.a(a4, this.y);
                        if (str3.length() > 0) {
                            String d2 = c.d(str3);
                            if (d2.length() > 0) {
                                z3 = this.B;
                                this.i.f1068a = String.format("%s %s %s", this.i.f1069b, this.i.e, d2);
                                this.i.m = this.A;
                            }
                        }
                    }
                } else if (TcmAid.bp == 4 && (a2 = o.a(this.i.h, this.y)) > 0) {
                    String a5 = o.a(a2, this.y);
                    if (a5.length() > 0) {
                        String[] split2 = a5.split(",");
                        boolean z4 = this.A;
                        String str4 = "";
                        for (String a6 : split2) {
                            int a7 = a.a(a6, this.y);
                            if (a7 != 0) {
                                if (z4) {
                                    str4 = String.format("%d", Integer.valueOf(a7));
                                    z4 = this.B;
                                } else {
                                    str4 = String.format("%s,%d", str4, Integer.valueOf(a7));
                                }
                            }
                        }
                        String a8 = a.a(str4);
                        if (a8.length() > 0) {
                            z3 = this.B;
                            this.i.f1068a = String.format("%s %s %s", this.i.f1069b, this.i.e, a8);
                            this.i.m = this.A;
                        }
                    }
                }
                if (z3) {
                    this.i.f1068a = String.format("%s %s'%s'", this.i.f1069b, this.i.c, this.i.h);
                }
            }
        }
        if (dh.B) {
            this.i.a();
        }
        a(this.y);
    }

    /* JADX WARN: Type inference failed for: r1v332, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r1v369, types: [android.database.Cursor] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x09d8  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0128  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0130  */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x001c  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01bb  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01c2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.database.sqlite.SQLiteDatabase r15) {
        /*
            r14 = this;
            r13 = 4
            r12 = 1
            r11 = 2
            r10 = 3
            r9 = 0
            java.lang.String r1 = ""
            com.cyberandsons.tcmaidtrial.quiz.QuizController.f1035a = r10
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.bp
            switch(r2) {
                case 0: goto L_0x0035;
                case 1: goto L_0x0134;
                case 2: goto L_0x01c6;
                case 3: goto L_0x0234;
                case 4: goto L_0x0297;
                case 5: goto L_0x099d;
                default: goto L_0x000e;
            }
        L_0x000e:
            r2 = r1
            r1 = r9
        L_0x0010:
            android.widget.ImageView r3 = r14.r
            r3.setVisibility(r13)
            android.widget.ImageView r3 = r14.s
            r3.setVisibility(r13)
            if (r1 >= r10) goto L_0x09d8
            r1 = 0
            r14.j = r1
            java.lang.String r1 = "Too Few Quiz Questions"
            java.lang.String r3 = "The quiz you selected has less than %d questions.\n\nSupport is being notified of this test configuration.\n\nPlease select another quiz."
            java.lang.Object[] r4 = new java.lang.Object[r12]
            java.lang.Integer r5 = java.lang.Integer.valueOf(r10)
            r4[r9] = r5
            java.lang.String r3 = java.lang.String.format(r3, r4)
            java.lang.String r4 = "setupInitialQuestion()"
            r14.a(r4, r1, r3, r2)
        L_0x0034:
            return
        L_0x0035:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.f1068a
            java.lang.String r2 = "pointcat='NADA'"
            boolean r2 = r1.contains(r2)
            if (r2 == 0) goto L_0x00ca
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.f1068a
            java.lang.String r2 = "pointcat='NADA'"
            java.lang.String r3 = "nada=1"
            java.lang.String r1 = r1.replace(r2, r3)
        L_0x004d:
            com.cyberandsons.tcmaidtrial.TcmAid.o = r1
            boolean r1 = com.cyberandsons.tcmaidtrial.misc.dh.B
            if (r1 == 0) goto L_0x005a
            java.lang.String r1 = "RQT:setupInitialQuestion"
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.TcmAid.o
            android.util.Log.i(r1, r2)
        L_0x005a:
            r2 = 0
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.o     // Catch:{ SQLException -> 0x0122 }
            r3 = 0
            android.database.Cursor r1 = r15.rawQuery(r1, r3)     // Catch:{ SQLException -> 0x0122 }
            r0 = r1
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0122 }
            r7 = r0
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f1043a     // Catch:{ SQLException -> 0x0a0c, all -> 0x0a08 }
            r1.clear()     // Catch:{ SQLException -> 0x0a0c, all -> 0x0a08 }
            boolean r1 = r7.moveToFirst()     // Catch:{ SQLException -> 0x0a0c, all -> 0x0a08 }
            if (r1 == 0) goto L_0x009e
        L_0x0071:
            r1 = 0
            int r2 = r7.getInt(r1)     // Catch:{ SQLException -> 0x0a0c, all -> 0x0a08 }
            r1 = 1
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLException -> 0x0a0c, all -> 0x0a08 }
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.misc.ad.a(r1)     // Catch:{ SQLException -> 0x0a0c, all -> 0x0a08 }
            r1 = 2
            java.lang.String r4 = r7.getString(r1)     // Catch:{ SQLException -> 0x0a0c, all -> 0x0a08 }
            r1 = 3
            java.lang.String r5 = r7.getString(r1)     // Catch:{ SQLException -> 0x0a0c, all -> 0x0a08 }
            r1 = 4
            java.lang.String r6 = r7.getString(r1)     // Catch:{ SQLException -> 0x0a0c, all -> 0x0a08 }
            java.util.ArrayList r8 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f1043a     // Catch:{ SQLException -> 0x0a0c, all -> 0x0a08 }
            com.cyberandsons.tcmaidtrial.a.f r1 = new com.cyberandsons.tcmaidtrial.a.f     // Catch:{ SQLException -> 0x0a0c, all -> 0x0a08 }
            r1.<init>(r2, r3, r4, r5, r6)     // Catch:{ SQLException -> 0x0a0c, all -> 0x0a08 }
            r8.add(r1)     // Catch:{ SQLException -> 0x0a0c, all -> 0x0a08 }
            boolean r1 = r7.moveToNext()     // Catch:{ SQLException -> 0x0a0c, all -> 0x0a08 }
            if (r1 != 0) goto L_0x0071
        L_0x009e:
            if (r7 == 0) goto L_0x00a3
            r7.close()
        L_0x00a3:
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f1043a
            int r1 = r1.size()
            java.lang.String r2 = "%s by %s = %d records"
            java.lang.Object[] r3 = new java.lang.Object[r10]
            com.cyberandsons.tcmaidtrial.quiz.i r4 = r14.i
            java.lang.String r4 = r4.g
            r3[r9] = r4
            com.cyberandsons.tcmaidtrial.quiz.i r4 = r14.i
            java.lang.String r4 = r4.h
            r3[r12] = r4
            java.lang.Integer r4 = java.lang.Integer.valueOf(r1)
            r3[r11] = r4
            java.lang.String r2 = java.lang.String.format(r2, r3)
            java.lang.String r3 = "RQT:setupInitialQuestion"
            android.util.Log.i(r3, r2)
            goto L_0x0010
        L_0x00ca:
            java.lang.String r2 = "pointcat='Battlefield'"
            boolean r2 = r1.contains(r2)
            if (r2 == 0) goto L_0x00e0
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.f1068a
            java.lang.String r2 = "pointcat='Battlefield'"
            java.lang.String r3 = "battlefield=1"
            java.lang.String r1 = r1.replace(r2, r3)
            goto L_0x004d
        L_0x00e0:
            java.lang.String r2 = "pointcat='ACACD'"
            boolean r2 = r1.contains(r2)
            if (r2 == 0) goto L_0x00f6
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.f1068a
            java.lang.String r2 = "pointcat='ACACD'"
            java.lang.String r3 = "acacd=1"
            java.lang.String r1 = r1.replace(r2, r3)
            goto L_0x004d
        L_0x00f6:
            java.lang.String r2 = "pointcat='Stop Smoking'"
            boolean r2 = r1.contains(r2)
            if (r2 == 0) goto L_0x010c
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.f1068a
            java.lang.String r2 = "pointcat='Stop Smoking'"
            java.lang.String r3 = "stopsmoking=1"
            java.lang.String r1 = r1.replace(r2, r3)
            goto L_0x004d
        L_0x010c:
            java.lang.String r2 = "pointcat='Weight Loss'"
            boolean r2 = r1.contains(r2)
            if (r2 == 0) goto L_0x004d
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.f1068a
            java.lang.String r2 = "pointcat='Weight Loss'"
            java.lang.String r3 = "weightloss=1"
            java.lang.String r1 = r1.replace(r2, r3)
            goto L_0x004d
        L_0x0122:
            r1 = move-exception
        L_0x0123:
            com.cyberandsons.tcmaidtrial.a.q.a(r1)     // Catch:{ all -> 0x012d }
            if (r2 == 0) goto L_0x00a3
            r2.close()
            goto L_0x00a3
        L_0x012d:
            r1 = move-exception
        L_0x012e:
            if (r2 == 0) goto L_0x0133
            r2.close()
        L_0x0133:
            throw r1
        L_0x0134:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.f1068a
            com.cyberandsons.tcmaidtrial.TcmAid.E = r1
            boolean r1 = com.cyberandsons.tcmaidtrial.misc.dh.B
            if (r1 == 0) goto L_0x0145
            java.lang.String r1 = "RQT:setupInitialQuestion"
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.TcmAid.E
            android.util.Log.i(r1, r2)
        L_0x0145:
            r2 = 0
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.E     // Catch:{ SQLException -> 0x01b5 }
            r3 = 0
            android.database.Cursor r1 = r15.rawQuery(r1, r3)     // Catch:{ SQLException -> 0x01b5 }
            r0 = r1
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x01b5 }
            r7 = r0
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f1044b     // Catch:{ SQLException -> 0x0a04, all -> 0x0a00 }
            r1.clear()     // Catch:{ SQLException -> 0x0a04, all -> 0x0a00 }
            boolean r1 = r7.moveToFirst()     // Catch:{ SQLException -> 0x0a04, all -> 0x0a00 }
            if (r1 == 0) goto L_0x0189
        L_0x015c:
            r1 = 0
            int r2 = r7.getInt(r1)     // Catch:{ SQLException -> 0x0a04, all -> 0x0a00 }
            r1 = 1
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLException -> 0x0a04, all -> 0x0a00 }
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.misc.ad.a(r1)     // Catch:{ SQLException -> 0x0a04, all -> 0x0a00 }
            r1 = 2
            java.lang.String r4 = r7.getString(r1)     // Catch:{ SQLException -> 0x0a04, all -> 0x0a00 }
            r1 = 3
            java.lang.String r5 = r7.getString(r1)     // Catch:{ SQLException -> 0x0a04, all -> 0x0a00 }
            r1 = 4
            java.lang.String r6 = r7.getString(r1)     // Catch:{ SQLException -> 0x0a04, all -> 0x0a00 }
            java.util.ArrayList r8 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f1044b     // Catch:{ SQLException -> 0x0a04, all -> 0x0a00 }
            com.cyberandsons.tcmaidtrial.a.r r1 = new com.cyberandsons.tcmaidtrial.a.r     // Catch:{ SQLException -> 0x0a04, all -> 0x0a00 }
            r1.<init>(r2, r3, r4, r5, r6)     // Catch:{ SQLException -> 0x0a04, all -> 0x0a00 }
            r8.add(r1)     // Catch:{ SQLException -> 0x0a04, all -> 0x0a00 }
            boolean r1 = r7.moveToNext()     // Catch:{ SQLException -> 0x0a04, all -> 0x0a00 }
            if (r1 != 0) goto L_0x015c
        L_0x0189:
            if (r7 == 0) goto L_0x018e
            r7.close()
        L_0x018e:
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f1044b
            int r1 = r1.size()
            java.lang.String r2 = "%s by %s = %d records"
            java.lang.Object[] r3 = new java.lang.Object[r10]
            com.cyberandsons.tcmaidtrial.quiz.i r4 = r14.i
            java.lang.String r4 = r4.g
            r3[r9] = r4
            com.cyberandsons.tcmaidtrial.quiz.i r4 = r14.i
            java.lang.String r4 = r4.h
            r3[r12] = r4
            java.lang.Integer r4 = java.lang.Integer.valueOf(r1)
            r3[r11] = r4
            java.lang.String r2 = java.lang.String.format(r2, r3)
            java.lang.String r3 = "RQT:setupInitialQuestion"
            android.util.Log.i(r3, r2)
            goto L_0x0010
        L_0x01b5:
            r1 = move-exception
        L_0x01b6:
            com.cyberandsons.tcmaidtrial.a.q.a(r1)     // Catch:{ all -> 0x01bf }
            if (r2 == 0) goto L_0x018e
            r2.close()
            goto L_0x018e
        L_0x01bf:
            r1 = move-exception
        L_0x01c0:
            if (r2 == 0) goto L_0x01c5
            r2.close()
        L_0x01c5:
            throw r1
        L_0x01c6:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            boolean r1 = r1.n
            if (r1 == 0) goto L_0x01f9
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.f1068a
            java.lang.String r2 = "item_name="
            java.lang.String r3 = "item_name!="
            java.lang.String r1 = r1.replace(r2, r3)
            com.cyberandsons.tcmaidtrial.TcmAid.af = r1
            boolean r1 = com.cyberandsons.tcmaidtrial.misc.dh.B
            if (r1 == 0) goto L_0x01e5
            java.lang.String r1 = "RQT:setupInitialQuestion"
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.TcmAid.af
            android.util.Log.i(r1, r2)
        L_0x01e5:
            c(r15)
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.d
            r1.clear()
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.d
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.c
            r1.addAll(r2)
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.c
            r1.clear()
        L_0x01f9:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.f1068a
            com.cyberandsons.tcmaidtrial.TcmAid.af = r1
            boolean r1 = com.cyberandsons.tcmaidtrial.misc.dh.B
            if (r1 == 0) goto L_0x020a
            java.lang.String r1 = "RQT:setupInitialQuestion"
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.TcmAid.af
            android.util.Log.i(r1, r2)
        L_0x020a:
            c(r15)
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.c
            int r1 = r1.size()
            java.lang.String r2 = "%s by %s = %d records"
            java.lang.Object[] r3 = new java.lang.Object[r10]
            com.cyberandsons.tcmaidtrial.quiz.i r4 = r14.i
            java.lang.String r4 = r4.g
            r3[r9] = r4
            com.cyberandsons.tcmaidtrial.quiz.i r4 = r14.i
            java.lang.String r4 = r4.h
            r3[r12] = r4
            java.lang.Integer r4 = java.lang.Integer.valueOf(r1)
            r3[r11] = r4
            java.lang.String r2 = java.lang.String.format(r2, r3)
            java.lang.String r3 = "RQT:setupInitialQuestion"
            android.util.Log.i(r3, r2)
            goto L_0x0010
        L_0x0234:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            boolean r1 = r1.n
            if (r1 == 0) goto L_0x025c
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.f1068a
            java.lang.String r2 = "item_name="
            java.lang.String r3 = "item_name!="
            java.lang.String r1 = r1.replace(r2, r3)
            com.cyberandsons.tcmaidtrial.TcmAid.S = r1
            d(r15)
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f
            r1.clear()
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.e
            r1.addAll(r2)
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.e
            r1.clear()
        L_0x025c:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.f1068a
            com.cyberandsons.tcmaidtrial.TcmAid.S = r1
            boolean r1 = com.cyberandsons.tcmaidtrial.misc.dh.B
            if (r1 == 0) goto L_0x026d
            java.lang.String r1 = "RQT:setupInitialQuestion"
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.TcmAid.S
            android.util.Log.i(r1, r2)
        L_0x026d:
            d(r15)
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.e
            int r1 = r1.size()
            java.lang.String r2 = "%s by %s = %d records"
            java.lang.Object[] r3 = new java.lang.Object[r10]
            com.cyberandsons.tcmaidtrial.quiz.i r4 = r14.i
            java.lang.String r4 = r4.g
            r3[r9] = r4
            com.cyberandsons.tcmaidtrial.quiz.i r4 = r14.i
            java.lang.String r4 = r4.h
            r3[r12] = r4
            java.lang.Integer r4 = java.lang.Integer.valueOf(r1)
            r3[r11] = r4
            java.lang.String r2 = java.lang.String.format(r2, r3)
            java.lang.String r3 = "RQT:setupInitialQuestion"
            android.util.Log.i(r3, r2)
            goto L_0x0010
        L_0x0297:
            boolean r1 = r14.B
            com.cyberandsons.tcmaidtrial.quiz.i r2 = r14.i
            java.lang.String r2 = r2.h
            java.lang.String r3 = "Extra Points"
            boolean r2 = r2.equalsIgnoreCase(r3)
            if (r2 == 0) goto L_0x0300
            com.cyberandsons.tcmaidtrial.quiz.i r2 = r14.i
            java.lang.String r2 = r2.f1068a
            java.lang.String r3 = "item_name='Extra Points'"
            java.lang.String r4 = "item_name LIKE '%-%' AND main.pointslocation.func != ''"
            java.lang.String r2 = r2.replace(r3, r4)
            com.cyberandsons.tcmaidtrial.quiz.i r3 = r14.i
            r3.f1068a = r2
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.B
            if (r2 == 0) goto L_0x02c2
            java.lang.String r2 = "RQT:setupInitialQuestion"
            com.cyberandsons.tcmaidtrial.quiz.i r3 = r14.i
            java.lang.String r3 = r3.f1068a
            android.util.Log.i(r2, r3)
        L_0x02c2:
            com.cyberandsons.tcmaidtrial.quiz.i r2 = r14.i
            java.lang.String r2 = r2.f1068a
            com.cyberandsons.tcmaidtrial.TcmAid.ar = r2
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.B
            if (r2 != 0) goto L_0x02d3
            java.lang.String r2 = "RQT:setupInitialQuestion"
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.TcmAid.ar
            android.util.Log.i(r2, r3)
        L_0x02d3:
            if (r1 == 0) goto L_0x0997
            r1 = r9
        L_0x02d6:
            a(r15, r1)
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.g
            int r1 = r1.size()
            java.lang.String r2 = "%s by %s = %d records"
            java.lang.Object[] r3 = new java.lang.Object[r10]
            com.cyberandsons.tcmaidtrial.quiz.i r4 = r14.i
            java.lang.String r4 = r4.g
            r3[r9] = r4
            com.cyberandsons.tcmaidtrial.quiz.i r4 = r14.i
            java.lang.String r4 = r4.h
            r3[r12] = r4
            java.lang.Integer r4 = java.lang.Integer.valueOf(r1)
            r3[r11] = r4
            java.lang.String r2 = java.lang.String.format(r2, r3)
            java.lang.String r3 = "RQT:setupInitialQuestion"
            android.util.Log.i(r3, r2)
            goto L_0x0010
        L_0x0300:
            com.cyberandsons.tcmaidtrial.quiz.i r2 = r14.i
            boolean r2 = r2.o
            if (r2 == 0) goto L_0x02c2
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r1.clear()
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.h
            java.lang.String r2 = "Ren"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 != 0) goto L_0x0392
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.h
            java.lang.String r2 = "Du"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 != 0) goto L_0x0392
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            java.lang.Integer r2 = java.lang.Integer.valueOf(r11)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            java.lang.Integer r2 = java.lang.Integer.valueOf(r10)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            java.lang.Integer r2 = java.lang.Integer.valueOf(r13)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 5
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 6
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 102(0x66, float:1.43E-43)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 103(0x67, float:1.44E-43)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 104(0x68, float:1.46E-43)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 105(0x69, float:1.47E-43)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
        L_0x0392:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.h
            java.lang.String r2 = "Lung"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x0436
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 13
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 95
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 25
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 51
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 67
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 77
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 53
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
        L_0x03f9:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.f1068a
            boolean r2 = r14.A
            r3 = r2
            r4 = r1
            r2 = r9
        L_0x0402:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            int r1 = r1.size()
            if (r2 >= r1) goto L_0x097c
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            java.lang.Object r1 = r1.get(r2)
            java.lang.Integer r1 = (java.lang.Integer) r1
            java.lang.String r5 = "%s %s main.pointslocation.specificpoint1=%d OR main.pointslocation.specificpoint2=%d OR main.pointslocation.specificpoint3=%d OR main.pointslocation.specificpoint4=%d) "
            r6 = 6
            java.lang.Object[] r6 = new java.lang.Object[r6]
            r6[r9] = r4
            if (r3 == 0) goto L_0x0978
            java.lang.String r3 = " AND (("
        L_0x0421:
            r6[r12] = r3
            r6[r11] = r1
            r6[r10] = r1
            r6[r13] = r1
            r3 = 5
            r6[r3] = r1
            java.lang.String r1 = java.lang.String.format(r5, r6)
            boolean r3 = r14.B
            int r2 = r2 + 1
            r4 = r1
            goto L_0x0402
        L_0x0436:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.h
            java.lang.String r2 = "Large Intestine"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x0477
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 11
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 93
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 23
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 50
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            goto L_0x03f9
        L_0x0477:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.h
            java.lang.String r2 = "Stomach"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x04ed
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 17
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 100
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 29
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 48
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 65
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 83
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 84
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 85
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            goto L_0x03f9
        L_0x04ed:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.h
            java.lang.String r2 = "Spleen"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x0549
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 16
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 99
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 28
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 96
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 55
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 59
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            goto L_0x03f9
        L_0x0549:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.h
            java.lang.String r2 = "Heart"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x057e
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 9
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 91
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 21
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            goto L_0x03f9
        L_0x057e:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.h
            java.lang.String r2 = "Small Intestine"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x05c0
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 15
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 98
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 27
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 54
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            goto L_0x03f9
        L_0x05c0:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.h
            java.lang.String r2 = "Bladder"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x06f8
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 7
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 89
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 19
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 31
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 35
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 36
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 37
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 38
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 39
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 40
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 41
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 42
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 43
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 44
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 45
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 46
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 47
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 49
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 73
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 74
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 86
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 81
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 56
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            goto L_0x03f9
        L_0x06f8:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.h
            java.lang.String r2 = "Kidney"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x0754
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 10
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 92
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 22
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 33
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 34
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 58
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            goto L_0x03f9
        L_0x0754:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.h
            java.lang.String r2 = "Pericardium"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x0796
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 14
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 97
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 26
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 59
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            goto L_0x03f9
        L_0x0796:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.h
            java.lang.String r2 = "San Jiao"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x07d8
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 18
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 101(0x65, float:1.42E-43)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 30
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 57
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            goto L_0x03f9
        L_0x07d8:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.h
            java.lang.String r2 = "Gallbladder"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x0868
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 8
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 90
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 20
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 32
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 64
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 62
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 79
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 76
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 82
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 52
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            goto L_0x03f9
        L_0x0868:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.h
            java.lang.String r2 = "Liver"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x08c4
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 12
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 94
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 24
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 70
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 66
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 80
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            goto L_0x03f9
        L_0x08c4:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.h
            java.lang.String r2 = "Ren"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x0952
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 87
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            java.lang.Integer r2 = java.lang.Integer.valueOf(r11)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 71
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 63
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 69
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 60
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 68
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 72
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 75
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 78
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            goto L_0x03f9
        L_0x0952:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.h
            java.lang.String r2 = "Du"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x03f9
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            r2 = 88
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.add(r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.util.ArrayList r1 = r1.p
            java.lang.Integer r2 = java.lang.Integer.valueOf(r11)
            r1.add(r2)
            goto L_0x03f9
        L_0x0978:
            java.lang.String r3 = " OR ("
            goto L_0x0421
        L_0x097c:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r2 = ")"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.cyberandsons.tcmaidtrial.quiz.i r2 = r14.i
            r2.f1068a = r1
            boolean r1 = r14.A
            goto L_0x02c2
        L_0x0997:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            int r1 = r1.j
            goto L_0x02d6
        L_0x099d:
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            java.lang.String r1 = r1.f1068a
            com.cyberandsons.tcmaidtrial.TcmAid.aF = r1
            boolean r1 = com.cyberandsons.tcmaidtrial.misc.dh.B
            if (r1 == 0) goto L_0x09ae
            java.lang.String r1 = "RQT:setupInitialQuestion"
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.TcmAid.E
            android.util.Log.i(r1, r2)
        L_0x09ae:
            e(r15)
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.h
            int r1 = r1.size()
            java.lang.String r2 = "%s by %s = %d records"
            java.lang.Object[] r3 = new java.lang.Object[r10]
            com.cyberandsons.tcmaidtrial.quiz.i r4 = r14.i
            java.lang.String r4 = r4.g
            r3[r9] = r4
            com.cyberandsons.tcmaidtrial.quiz.i r4 = r14.i
            java.lang.String r4 = r4.h
            r3[r12] = r4
            java.lang.Integer r4 = java.lang.Integer.valueOf(r1)
            r3[r11] = r4
            java.lang.String r2 = java.lang.String.format(r2, r3)
            java.lang.String r3 = "RQT:setupInitialQuestion"
            android.util.Log.i(r3, r2)
            goto L_0x0010
        L_0x09d8:
            com.cyberandsons.tcmaidtrial.quiz.t r1 = new com.cyberandsons.tcmaidtrial.quiz.t
            com.cyberandsons.tcmaidtrial.quiz.i r2 = r14.i
            int r2 = r2.i
            com.cyberandsons.tcmaidtrial.quiz.i r3 = r14.i
            boolean r3 = r3.l
            com.cyberandsons.tcmaidtrial.quiz.i r4 = r14.i
            boolean r4 = r4.k
            com.cyberandsons.tcmaidtrial.quiz.i r5 = r14.i
            int r6 = r5.j
            r5 = r15
            r1.<init>(r2, r3, r4, r5, r6)
            r14.j = r1
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r14.i
            boolean r1 = r1.l
            if (r1 == 0) goto L_0x0034
            com.cyberandsons.tcmaidtrial.quiz.t r1 = r14.j
            com.cyberandsons.tcmaidtrial.quiz.i r2 = r14.i
            java.lang.String r2 = r2.f1068a
            r1.l = r2
            goto L_0x0034
        L_0x0a00:
            r1 = move-exception
            r2 = r7
            goto L_0x01c0
        L_0x0a04:
            r1 = move-exception
            r2 = r7
            goto L_0x01b6
        L_0x0a08:
            r1 = move-exception
            r2 = r7
            goto L_0x012e
        L_0x0a0c:
            r1 = move-exception
            r2 = r7
            goto L_0x0123
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.a(android.database.sqlite.SQLiteDatabase):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:171:0x04f8  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0238 A[Catch:{ b -> 0x015f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(android.database.sqlite.SQLiteDatabase r12) {
        /*
            r11 = this;
            com.cyberandsons.tcmaidtrial.quiz.t r0 = r11.j     // Catch:{ b -> 0x015f }
            if (r0 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            com.cyberandsons.tcmaidtrial.quiz.t r1 = r11.j     // Catch:{ b -> 0x015f }
            com.cyberandsons.tcmaidtrial.quiz.i r0 = r11.i     // Catch:{ b -> 0x015f }
            boolean r2 = r0.n     // Catch:{ b -> 0x015f }
            java.lang.String r3 = ""
            boolean r4 = r1.A     // Catch:{ b -> 0x015f }
            boolean r5 = r1.A     // Catch:{ b -> 0x015f }
            r0 = -1
            r1.g = r0     // Catch:{ b -> 0x015f }
            r0 = -1
            r1.h = r0     // Catch:{ b -> 0x015f }
            r0 = -1
            r1.i = r0     // Catch:{ b -> 0x015f }
            java.util.Random r6 = new java.util.Random     // Catch:{ b -> 0x015f }
            r6.<init>()     // Catch:{ b -> 0x015f }
            r0 = 0
            r7 = r0
        L_0x0021:
            int r0 = r1.c     // Catch:{ b -> 0x015f }
            if (r7 >= r0) goto L_0x0036
            java.util.ArrayList r0 = r1.s     // Catch:{ b -> 0x015f }
            java.lang.Object r0 = r0.get(r7)     // Catch:{ b -> 0x015f }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ b -> 0x015f }
            boolean r0 = r0.booleanValue()     // Catch:{ b -> 0x015f }
            if (r0 != 0) goto L_0x00f0
            boolean r0 = r1.z     // Catch:{ b -> 0x015f }
            r5 = r0
        L_0x0036:
            if (r5 == 0) goto L_0x01f6
            boolean r0 = r1.x     // Catch:{ b -> 0x015f }
            if (r0 == 0) goto L_0x0121
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.bp     // Catch:{ b -> 0x015f }
            r7 = 4
            if (r0 != r7) goto L_0x0121
            int r0 = r1.d     // Catch:{ b -> 0x015f }
            r7 = 20
            if (r0 != r7) goto L_0x00f5
            boolean r0 = r1.A     // Catch:{ b -> 0x015f }
        L_0x0049:
            if (r0 != 0) goto L_0x0238
            r0 = 0
        L_0x004c:
            if (r0 != 0) goto L_0x04f8
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.bw
            r1 = 2
            if (r0 != r1) goto L_0x0004
            android.widget.TextView r0 = r11.k
            java.lang.String r1 = "%d/%d"
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            com.cyberandsons.tcmaidtrial.quiz.t r4 = r11.j
            int r4 = r4.d
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r2[r3] = r4
            r3 = 1
            com.cyberandsons.tcmaidtrial.quiz.t r4 = r11.j
            int r4 = r4.c
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r2[r3] = r4
            java.lang.String r1 = java.lang.String.format(r1, r2)
            r0.setText(r1)
            com.cyberandsons.tcmaidtrial.quiz.t r0 = r11.j
            long r1 = java.lang.System.currentTimeMillis()
            long r3 = r0.v
            long r1 = r1 - r3
            r3 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 / r3
            int r1 = (int) r1
            r0.w = r1
            java.lang.String r0 = "%s\nTo save results, press 'Save', otherwise 'Cancel'"
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r2 = 0
            com.cyberandsons.tcmaidtrial.quiz.t r3 = r11.j
            java.lang.String r4 = "Your score is : %.1f%% - %d out of %d"
            r5 = 3
            java.lang.Object[] r5 = new java.lang.Object[r5]
            r6 = 0
            int r7 = r3.c
            int r8 = r3.j
            int r7 = r7 - r8
            float r7 = (float) r7
            int r8 = r3.c
            float r8 = (float) r8
            float r7 = r7 / r8
            r8 = 1120403456(0x42c80000, float:100.0)
            float r7 = r7 * r8
            java.lang.Float r7 = java.lang.Float.valueOf(r7)
            r5[r6] = r7
            r6 = 1
            int r7 = r3.c
            int r8 = r3.j
            int r7 = r7 - r8
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            r5[r6] = r7
            r6 = 2
            int r3 = r3.c
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r5[r6] = r3
            java.lang.String r3 = java.lang.String.format(r4, r5)
            r1[r2] = r3
            java.lang.String r0 = java.lang.String.format(r0, r1)
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder
            r1.<init>(r11)
            android.app.AlertDialog r1 = r1.create()
            java.lang.String r2 = "Your Results:"
            r1.setTitle(r2)
            r1.setMessage(r0)
            java.lang.String r0 = "Save"
            com.cyberandsons.tcmaidtrial.quiz.ad r2 = new com.cyberandsons.tcmaidtrial.quiz.ad
            r2.<init>(r11, r12)
            r1.setButton(r0, r2)
            java.lang.String r0 = "Cancel"
            com.cyberandsons.tcmaidtrial.quiz.ae r2 = new com.cyberandsons.tcmaidtrial.quiz.ae
            r2.<init>(r11)
            r1.setButton2(r0, r2)
            r1.show()
            goto L_0x0004
        L_0x00f0:
            int r0 = r7 + 1
            r7 = r0
            goto L_0x0021
        L_0x00f5:
            java.lang.String r0 = r1.l     // Catch:{ b -> 0x015f }
            com.cyberandsons.tcmaidtrial.TcmAid.ar = r0     // Catch:{ b -> 0x015f }
            android.database.sqlite.SQLiteDatabase r0 = r1.y     // Catch:{ b -> 0x015f }
            int r7 = r1.f1080a     // Catch:{ b -> 0x015f }
            a(r0, r7)     // Catch:{ b -> 0x015f }
            r0 = 0
        L_0x0101:
            int r7 = r1.c     // Catch:{ b -> 0x015f }
            if (r0 >= r7) goto L_0x0121
            java.util.ArrayList r7 = r1.s     // Catch:{ b -> 0x015f }
            java.util.ArrayList r8 = r1.s     // Catch:{ b -> 0x015f }
            int r8 = r8.size()     // Catch:{ b -> 0x015f }
            r9 = 1
            int r8 = r8 - r9
            r7.remove(r8)     // Catch:{ b -> 0x015f }
            java.util.ArrayList r7 = r1.s     // Catch:{ b -> 0x015f }
            r8 = 0
            boolean r9 = r1.A     // Catch:{ b -> 0x015f }
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)     // Catch:{ b -> 0x015f }
            r7.add(r8, r9)     // Catch:{ b -> 0x015f }
            int r0 = r0 + 1
            goto L_0x0101
        L_0x0121:
            boolean r0 = r1.x     // Catch:{ b -> 0x015f }
            if (r0 == 0) goto L_0x0154
            r0 = 3
            r7 = r0
        L_0x0127:
            r0 = 0
        L_0x0128:
            if (r4 != 0) goto L_0x0184
            int r8 = r6.nextInt(r7)     // Catch:{ b -> 0x015f }
            java.util.ArrayList r0 = r1.s     // Catch:{ b -> 0x015f }
            java.lang.Object r0 = r0.get(r8)     // Catch:{ b -> 0x015f }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ b -> 0x015f }
            boolean r0 = r0.booleanValue()     // Catch:{ b -> 0x015f }
            if (r0 != 0) goto L_0x06ed
            r0 = 3
            int r0 = r6.nextInt(r0)     // Catch:{ b -> 0x015f }
            switch(r0) {
                case 0: goto L_0x0158;
                case 1: goto L_0x0176;
                case 2: goto L_0x017d;
                default: goto L_0x0144;
            }     // Catch:{ b -> 0x015f }
        L_0x0144:
            java.util.ArrayList r0 = r1.s     // Catch:{ b -> 0x015f }
            boolean r4 = r1.z     // Catch:{ b -> 0x015f }
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)     // Catch:{ b -> 0x015f }
            r0.set(r8, r4)     // Catch:{ b -> 0x015f }
            boolean r0 = r1.z     // Catch:{ b -> 0x015f }
        L_0x0151:
            r4 = r0
            r0 = r8
            goto L_0x0128
        L_0x0154:
            int r0 = r1.c     // Catch:{ b -> 0x015f }
            r7 = r0
            goto L_0x0127
        L_0x0158:
            r1.f = r0     // Catch:{ b -> 0x015f }
            r1.g = r8     // Catch:{ b -> 0x015f }
            r1.e = r8     // Catch:{ b -> 0x015f }
            goto L_0x0144
        L_0x015f:
            r0 = move-exception
            java.lang.String r1 = "Next Question Problem"
            java.lang.String r2 = "Building the next question resulted in an unexpected condition.\n\nSupport is being notified of this test configuration.\n\nPlease select another quiz."
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            java.lang.String r2 = java.lang.String.format(r2, r3)
            java.lang.String r3 = "askNextQuestion()"
            java.lang.String r0 = r0.getMessage()
            r11.a(r3, r1, r2, r0)
            goto L_0x0004
        L_0x0176:
            r1.f = r0     // Catch:{ b -> 0x015f }
            r1.h = r8     // Catch:{ b -> 0x015f }
            r1.e = r8     // Catch:{ b -> 0x015f }
            goto L_0x0144
        L_0x017d:
            r1.f = r0     // Catch:{ b -> 0x015f }
            r1.i = r8     // Catch:{ b -> 0x015f }
            r1.e = r8     // Catch:{ b -> 0x015f }
            goto L_0x0144
        L_0x0184:
            boolean r4 = r1.A     // Catch:{ b -> 0x015f }
            r10 = r7
            r7 = r4
            r4 = r10
        L_0x0189:
            if (r7 != 0) goto L_0x01f0
            if (r2 == 0) goto L_0x0192
            int r8 = com.cyberandsons.tcmaidtrial.TcmAid.bp     // Catch:{ b -> 0x015f }
            switch(r8) {
                case 0: goto L_0x0192;
                case 1: goto L_0x0192;
                case 2: goto L_0x01b6;
                case 3: goto L_0x01bd;
                default: goto L_0x0192;
            }     // Catch:{ b -> 0x015f }
        L_0x0192:
            int r8 = r1.g     // Catch:{ b -> 0x015f }
            r9 = -1
            if (r8 != r9) goto L_0x01c4
            int r8 = r6.nextInt(r4)     // Catch:{ b -> 0x015f }
            if (r8 == r0) goto L_0x0189
            int r9 = r1.h     // Catch:{ b -> 0x015f }
            if (r8 == r9) goto L_0x0189
            int r9 = r1.i     // Catch:{ b -> 0x015f }
            if (r8 == r9) goto L_0x0189
            r1.g = r8     // Catch:{ b -> 0x015f }
        L_0x01a7:
            int r8 = r1.g     // Catch:{ b -> 0x015f }
            if (r8 < 0) goto L_0x0189
            int r8 = r1.h     // Catch:{ b -> 0x015f }
            if (r8 < 0) goto L_0x0189
            int r8 = r1.i     // Catch:{ b -> 0x015f }
            if (r8 < 0) goto L_0x0189
            boolean r7 = r1.z     // Catch:{ b -> 0x015f }
            goto L_0x0189
        L_0x01b6:
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.d     // Catch:{ b -> 0x015f }
            int r4 = r4.size()     // Catch:{ b -> 0x015f }
            goto L_0x0192
        L_0x01bd:
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f     // Catch:{ b -> 0x015f }
            int r4 = r4.size()     // Catch:{ b -> 0x015f }
            goto L_0x0192
        L_0x01c4:
            int r8 = r1.h     // Catch:{ b -> 0x015f }
            r9 = -1
            if (r8 != r9) goto L_0x01da
            int r8 = r6.nextInt(r4)     // Catch:{ b -> 0x015f }
            if (r8 == r0) goto L_0x0189
            int r9 = r1.g     // Catch:{ b -> 0x015f }
            if (r8 == r9) goto L_0x0189
            int r9 = r1.i     // Catch:{ b -> 0x015f }
            if (r8 == r9) goto L_0x0189
            r1.h = r8     // Catch:{ b -> 0x015f }
            goto L_0x01a7
        L_0x01da:
            int r8 = r1.i     // Catch:{ b -> 0x015f }
            r9 = -1
            if (r8 != r9) goto L_0x01a7
            int r8 = r6.nextInt(r4)     // Catch:{ b -> 0x015f }
            if (r8 == r0) goto L_0x0189
            int r9 = r1.g     // Catch:{ b -> 0x015f }
            if (r8 == r9) goto L_0x0189
            int r9 = r1.h     // Catch:{ b -> 0x015f }
            if (r8 == r9) goto L_0x0189
            r1.i = r8     // Catch:{ b -> 0x015f }
            goto L_0x01a7
        L_0x01f0:
            int r0 = r1.d     // Catch:{ b -> 0x015f }
            int r0 = r0 + 1
            r1.d = r0     // Catch:{ b -> 0x015f }
        L_0x01f6:
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.B     // Catch:{ b -> 0x015f }
            if (r0 == 0) goto L_0x0235
            java.lang.String r0 = "RQ:shakeTheDice()"
            java.lang.String r2 = "CorrAns = %d, CorrBtn = %d, 0-AnsA = %d, 1-AnsB = %d, 2-AnsC = %d"
            r4 = 5
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ b -> 0x015f }
            r6 = 0
            int r7 = r1.e     // Catch:{ b -> 0x015f }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ b -> 0x015f }
            r4[r6] = r7     // Catch:{ b -> 0x015f }
            r6 = 1
            int r7 = r1.f     // Catch:{ b -> 0x015f }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ b -> 0x015f }
            r4[r6] = r7     // Catch:{ b -> 0x015f }
            r6 = 2
            int r7 = r1.g     // Catch:{ b -> 0x015f }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ b -> 0x015f }
            r4[r6] = r7     // Catch:{ b -> 0x015f }
            r6 = 3
            int r7 = r1.h     // Catch:{ b -> 0x015f }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ b -> 0x015f }
            r4[r6] = r7     // Catch:{ b -> 0x015f }
            r6 = 4
            int r7 = r1.i     // Catch:{ b -> 0x015f }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ b -> 0x015f }
            r4[r6] = r7     // Catch:{ b -> 0x015f }
            java.lang.String r2 = java.lang.String.format(r2, r4)     // Catch:{ b -> 0x015f }
            android.util.Log.i(r0, r2)     // Catch:{ b -> 0x015f }
        L_0x0235:
            r0 = r5
            goto L_0x0049
        L_0x0238:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.bp     // Catch:{ b -> 0x015f }
            switch(r0) {
                case 0: goto L_0x0240;
                case 1: goto L_0x02a6;
                case 2: goto L_0x030c;
                case 3: goto L_0x0382;
                case 4: goto L_0x0400;
                case 5: goto L_0x047a;
                default: goto L_0x023d;
            }     // Catch:{ b -> 0x015f }
        L_0x023d:
            r0 = r3
            goto L_0x004c
        L_0x0240:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f1043a     // Catch:{ b -> 0x015f }
            int r0 = r0.size()     // Catch:{ b -> 0x015f }
            int r2 = r1.e     // Catch:{ b -> 0x015f }
            if (r2 <= r0) goto L_0x0272
            com.cyberandsons.tcmaidtrial.b.b r2 = new com.cyberandsons.tcmaidtrial.b.b     // Catch:{ b -> 0x015f }
            java.lang.String r3 = "getNextQuestion() : Area = %d\nArrSz = %d\nCorrAns = %d"
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ b -> 0x015f }
            r5 = 0
            int r6 = com.cyberandsons.tcmaidtrial.TcmAid.bp     // Catch:{ b -> 0x015f }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ b -> 0x015f }
            r4[r5] = r6     // Catch:{ b -> 0x015f }
            r5 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ b -> 0x015f }
            r4[r5] = r0     // Catch:{ b -> 0x015f }
            r0 = 2
            int r1 = r1.e     // Catch:{ b -> 0x015f }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ b -> 0x015f }
            r4[r0] = r1     // Catch:{ b -> 0x015f }
            java.lang.String r0 = java.lang.String.format(r3, r4)     // Catch:{ b -> 0x015f }
            r2.<init>(r0)     // Catch:{ b -> 0x015f }
            throw r2     // Catch:{ b -> 0x015f }
        L_0x0272:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f1043a     // Catch:{ b -> 0x015f }
            int r2 = r1.e     // Catch:{ b -> 0x015f }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ b -> 0x015f }
            com.cyberandsons.tcmaidtrial.a.f r0 = (com.cyberandsons.tcmaidtrial.a.f) r0     // Catch:{ b -> 0x015f }
            r1.m = r0     // Catch:{ b -> 0x015f }
            int r0 = r1.f1081b     // Catch:{ b -> 0x015f }
            switch(r0) {
                case 0: goto L_0x028b;
                case 1: goto L_0x028e;
                case 2: goto L_0x0296;
                case 3: goto L_0x029e;
                default: goto L_0x0283;
            }     // Catch:{ b -> 0x015f }
        L_0x0283:
            com.cyberandsons.tcmaidtrial.a.f r0 = r1.m     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.e()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x028b:
            r0 = r3
            goto L_0x004c
        L_0x028e:
            com.cyberandsons.tcmaidtrial.a.f r0 = r1.m     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.b()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x0296:
            com.cyberandsons.tcmaidtrial.a.f r0 = r1.m     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.c()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x029e:
            com.cyberandsons.tcmaidtrial.a.f r0 = r1.m     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.d()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x02a6:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f1044b     // Catch:{ b -> 0x015f }
            int r0 = r0.size()     // Catch:{ b -> 0x015f }
            int r2 = r1.e     // Catch:{ b -> 0x015f }
            if (r2 <= r0) goto L_0x02d8
            com.cyberandsons.tcmaidtrial.b.b r2 = new com.cyberandsons.tcmaidtrial.b.b     // Catch:{ b -> 0x015f }
            java.lang.String r3 = "getNextQuestion() : Area = %d\nArrSz = %d\nCorrAns = %d"
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ b -> 0x015f }
            r5 = 0
            int r6 = com.cyberandsons.tcmaidtrial.TcmAid.bp     // Catch:{ b -> 0x015f }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ b -> 0x015f }
            r4[r5] = r6     // Catch:{ b -> 0x015f }
            r5 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ b -> 0x015f }
            r4[r5] = r0     // Catch:{ b -> 0x015f }
            r0 = 2
            int r1 = r1.e     // Catch:{ b -> 0x015f }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ b -> 0x015f }
            r4[r0] = r1     // Catch:{ b -> 0x015f }
            java.lang.String r0 = java.lang.String.format(r3, r4)     // Catch:{ b -> 0x015f }
            r2.<init>(r0)     // Catch:{ b -> 0x015f }
            throw r2     // Catch:{ b -> 0x015f }
        L_0x02d8:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.f1044b     // Catch:{ b -> 0x015f }
            int r2 = r1.e     // Catch:{ b -> 0x015f }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ b -> 0x015f }
            com.cyberandsons.tcmaidtrial.a.r r0 = (com.cyberandsons.tcmaidtrial.a.r) r0     // Catch:{ b -> 0x015f }
            r1.n = r0     // Catch:{ b -> 0x015f }
            int r0 = r1.f1081b     // Catch:{ b -> 0x015f }
            switch(r0) {
                case 0: goto L_0x02f1;
                case 1: goto L_0x02f4;
                case 2: goto L_0x02fc;
                case 3: goto L_0x0304;
                default: goto L_0x02e9;
            }     // Catch:{ b -> 0x015f }
        L_0x02e9:
            com.cyberandsons.tcmaidtrial.a.r r0 = r1.n     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.e()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x02f1:
            r0 = r3
            goto L_0x004c
        L_0x02f4:
            com.cyberandsons.tcmaidtrial.a.r r0 = r1.n     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.b()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x02fc:
            com.cyberandsons.tcmaidtrial.a.r r0 = r1.n     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.c()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x0304:
            com.cyberandsons.tcmaidtrial.a.r r0 = r1.n     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.d()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x030c:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.c     // Catch:{ b -> 0x015f }
            int r0 = r0.size()     // Catch:{ b -> 0x015f }
            int r2 = r1.e     // Catch:{ b -> 0x015f }
            if (r2 <= r0) goto L_0x033e
            com.cyberandsons.tcmaidtrial.b.b r2 = new com.cyberandsons.tcmaidtrial.b.b     // Catch:{ b -> 0x015f }
            java.lang.String r3 = "getNextQuestion() : Area = %d\nArrSz = %d\nCorrAns = %d"
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ b -> 0x015f }
            r5 = 0
            int r6 = com.cyberandsons.tcmaidtrial.TcmAid.bp     // Catch:{ b -> 0x015f }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ b -> 0x015f }
            r4[r5] = r6     // Catch:{ b -> 0x015f }
            r5 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ b -> 0x015f }
            r4[r5] = r0     // Catch:{ b -> 0x015f }
            r0 = 2
            int r1 = r1.e     // Catch:{ b -> 0x015f }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ b -> 0x015f }
            r4[r0] = r1     // Catch:{ b -> 0x015f }
            java.lang.String r0 = java.lang.String.format(r3, r4)     // Catch:{ b -> 0x015f }
            r2.<init>(r0)     // Catch:{ b -> 0x015f }
            throw r2     // Catch:{ b -> 0x015f }
        L_0x033e:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.c     // Catch:{ b -> 0x015f }
            int r2 = r1.e     // Catch:{ b -> 0x015f }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ b -> 0x015f }
            com.cyberandsons.tcmaidtrial.a.g r0 = (com.cyberandsons.tcmaidtrial.a.g) r0     // Catch:{ b -> 0x015f }
            r1.o = r0     // Catch:{ b -> 0x015f }
            int r0 = r1.f1081b     // Catch:{ b -> 0x015f }
            switch(r0) {
                case 0: goto L_0x0357;
                case 1: goto L_0x035a;
                case 2: goto L_0x0362;
                case 3: goto L_0x036a;
                case 4: goto L_0x0372;
                case 5: goto L_0x034f;
                case 6: goto L_0x034f;
                case 7: goto L_0x037a;
                default: goto L_0x034f;
            }     // Catch:{ b -> 0x015f }
        L_0x034f:
            com.cyberandsons.tcmaidtrial.a.g r0 = r1.o     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.e()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x0357:
            r0 = r3
            goto L_0x004c
        L_0x035a:
            com.cyberandsons.tcmaidtrial.a.g r0 = r1.o     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.g()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x0362:
            com.cyberandsons.tcmaidtrial.a.g r0 = r1.o     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.c()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x036a:
            com.cyberandsons.tcmaidtrial.a.g r0 = r1.o     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.d()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x0372:
            com.cyberandsons.tcmaidtrial.a.g r0 = r1.o     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.f()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x037a:
            com.cyberandsons.tcmaidtrial.a.g r0 = r1.o     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.h()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x0382:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.e     // Catch:{ b -> 0x015f }
            int r0 = r0.size()     // Catch:{ b -> 0x015f }
            int r2 = r1.e     // Catch:{ b -> 0x015f }
            if (r2 <= r0) goto L_0x03b4
            com.cyberandsons.tcmaidtrial.b.b r2 = new com.cyberandsons.tcmaidtrial.b.b     // Catch:{ b -> 0x015f }
            java.lang.String r3 = "getNextQuestion() : Area = %d\nArrSz = %d\nCorrAns = %d"
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ b -> 0x015f }
            r5 = 0
            int r6 = com.cyberandsons.tcmaidtrial.TcmAid.bp     // Catch:{ b -> 0x015f }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ b -> 0x015f }
            r4[r5] = r6     // Catch:{ b -> 0x015f }
            r5 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ b -> 0x015f }
            r4[r5] = r0     // Catch:{ b -> 0x015f }
            r0 = 2
            int r1 = r1.e     // Catch:{ b -> 0x015f }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ b -> 0x015f }
            r4[r0] = r1     // Catch:{ b -> 0x015f }
            java.lang.String r0 = java.lang.String.format(r3, r4)     // Catch:{ b -> 0x015f }
            r2.<init>(r0)     // Catch:{ b -> 0x015f }
            throw r2     // Catch:{ b -> 0x015f }
        L_0x03b4:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.e     // Catch:{ b -> 0x015f }
            int r2 = r1.e     // Catch:{ b -> 0x015f }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ b -> 0x015f }
            com.cyberandsons.tcmaidtrial.a.e r0 = (com.cyberandsons.tcmaidtrial.a.e) r0     // Catch:{ b -> 0x015f }
            r1.p = r0     // Catch:{ b -> 0x015f }
            int r0 = r1.f1081b     // Catch:{ b -> 0x015f }
            switch(r0) {
                case 0: goto L_0x03cd;
                case 1: goto L_0x03d0;
                case 2: goto L_0x03d8;
                case 3: goto L_0x03e0;
                case 4: goto L_0x03e8;
                case 5: goto L_0x03f0;
                case 6: goto L_0x03c5;
                case 7: goto L_0x03f8;
                default: goto L_0x03c5;
            }     // Catch:{ b -> 0x015f }
        L_0x03c5:
            com.cyberandsons.tcmaidtrial.a.e r0 = r1.p     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.g()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x03cd:
            r0 = r3
            goto L_0x004c
        L_0x03d0:
            com.cyberandsons.tcmaidtrial.a.e r0 = r1.p     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.b()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x03d8:
            com.cyberandsons.tcmaidtrial.a.e r0 = r1.p     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.c()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x03e0:
            com.cyberandsons.tcmaidtrial.a.e r0 = r1.p     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.d()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x03e8:
            com.cyberandsons.tcmaidtrial.a.e r0 = r1.p     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.e()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x03f0:
            com.cyberandsons.tcmaidtrial.a.e r0 = r1.p     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.f()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x03f8:
            com.cyberandsons.tcmaidtrial.a.e r0 = r1.p     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.h()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x0400:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.g     // Catch:{ b -> 0x015f }
            int r0 = r0.size()     // Catch:{ b -> 0x015f }
            int r2 = r1.e     // Catch:{ b -> 0x015f }
            if (r2 <= r0) goto L_0x0432
            com.cyberandsons.tcmaidtrial.b.b r2 = new com.cyberandsons.tcmaidtrial.b.b     // Catch:{ b -> 0x015f }
            java.lang.String r3 = "getNextQuestion() : Area = %d\nArrSz = %d\nCorrAns = %d"
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ b -> 0x015f }
            r5 = 0
            int r6 = com.cyberandsons.tcmaidtrial.TcmAid.bp     // Catch:{ b -> 0x015f }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ b -> 0x015f }
            r4[r5] = r6     // Catch:{ b -> 0x015f }
            r5 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ b -> 0x015f }
            r4[r5] = r0     // Catch:{ b -> 0x015f }
            r0 = 2
            int r1 = r1.e     // Catch:{ b -> 0x015f }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ b -> 0x015f }
            r4[r0] = r1     // Catch:{ b -> 0x015f }
            java.lang.String r0 = java.lang.String.format(r3, r4)     // Catch:{ b -> 0x015f }
            r2.<init>(r0)     // Catch:{ b -> 0x015f }
            throw r2     // Catch:{ b -> 0x015f }
        L_0x0432:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.g     // Catch:{ b -> 0x015f }
            int r2 = r1.e     // Catch:{ b -> 0x015f }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ b -> 0x015f }
            com.cyberandsons.tcmaidtrial.a.i r0 = (com.cyberandsons.tcmaidtrial.a.i) r0     // Catch:{ b -> 0x015f }
            r1.q = r0     // Catch:{ b -> 0x015f }
            int r0 = r1.f1081b     // Catch:{ b -> 0x015f }
            switch(r0) {
                case 0: goto L_0x044b;
                case 1: goto L_0x044e;
                case 2: goto L_0x0456;
                case 3: goto L_0x045e;
                case 4: goto L_0x0443;
                case 5: goto L_0x0443;
                case 6: goto L_0x0466;
                default: goto L_0x0443;
            }     // Catch:{ b -> 0x015f }
        L_0x0443:
            com.cyberandsons.tcmaidtrial.a.i r0 = r1.q     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.e()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x044b:
            r0 = r3
            goto L_0x004c
        L_0x044e:
            com.cyberandsons.tcmaidtrial.a.i r0 = r1.q     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.b()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x0456:
            com.cyberandsons.tcmaidtrial.a.i r0 = r1.q     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.c()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x045e:
            com.cyberandsons.tcmaidtrial.a.i r0 = r1.q     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.d()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x0466:
            java.lang.String r0 = "What point is the '%s'"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ b -> 0x015f }
            r3 = 0
            com.cyberandsons.tcmaidtrial.a.i r1 = r1.q     // Catch:{ b -> 0x015f }
            java.lang.String r1 = r1.g()     // Catch:{ b -> 0x015f }
            r2[r3] = r1     // Catch:{ b -> 0x015f }
            java.lang.String r0 = java.lang.String.format(r0, r2)     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x047a:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.h     // Catch:{ b -> 0x015f }
            int r0 = r0.size()     // Catch:{ b -> 0x015f }
            int r2 = r1.e     // Catch:{ b -> 0x015f }
            if (r2 <= r0) goto L_0x04ac
            com.cyberandsons.tcmaidtrial.b.b r2 = new com.cyberandsons.tcmaidtrial.b.b     // Catch:{ b -> 0x015f }
            java.lang.String r3 = "getNextQuestion() : Area = %d\nArrSz = %d\nCorrAns = %d"
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ b -> 0x015f }
            r5 = 0
            int r6 = com.cyberandsons.tcmaidtrial.TcmAid.bp     // Catch:{ b -> 0x015f }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ b -> 0x015f }
            r4[r5] = r6     // Catch:{ b -> 0x015f }
            r5 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ b -> 0x015f }
            r4[r5] = r0     // Catch:{ b -> 0x015f }
            r0 = 2
            int r1 = r1.e     // Catch:{ b -> 0x015f }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ b -> 0x015f }
            r4[r0] = r1     // Catch:{ b -> 0x015f }
            java.lang.String r0 = java.lang.String.format(r3, r4)     // Catch:{ b -> 0x015f }
            r2.<init>(r0)     // Catch:{ b -> 0x015f }
            throw r2     // Catch:{ b -> 0x015f }
        L_0x04ac:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.h     // Catch:{ b -> 0x015f }
            int r2 = r1.e     // Catch:{ b -> 0x015f }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ b -> 0x015f }
            com.cyberandsons.tcmaidtrial.a.h r0 = (com.cyberandsons.tcmaidtrial.a.h) r0     // Catch:{ b -> 0x015f }
            r1.r = r0     // Catch:{ b -> 0x015f }
            int r0 = r1.f1081b     // Catch:{ b -> 0x015f }
            switch(r0) {
                case 0: goto L_0x04c5;
                case 1: goto L_0x04c8;
                case 2: goto L_0x04d0;
                case 3: goto L_0x04d8;
                case 4: goto L_0x04e0;
                case 5: goto L_0x04e8;
                case 6: goto L_0x04f0;
                default: goto L_0x04bd;
            }     // Catch:{ b -> 0x015f }
        L_0x04bd:
            com.cyberandsons.tcmaidtrial.a.h r0 = r1.r     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.f()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x04c5:
            r0 = r3
            goto L_0x004c
        L_0x04c8:
            com.cyberandsons.tcmaidtrial.a.h r0 = r1.r     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.b()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x04d0:
            com.cyberandsons.tcmaidtrial.a.h r0 = r1.r     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.d()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x04d8:
            com.cyberandsons.tcmaidtrial.a.h r0 = r1.r     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.e()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x04e0:
            com.cyberandsons.tcmaidtrial.a.h r0 = r1.r     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.f()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x04e8:
            com.cyberandsons.tcmaidtrial.a.h r0 = r1.r     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.g()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x04f0:
            com.cyberandsons.tcmaidtrial.a.h r0 = r1.r     // Catch:{ b -> 0x015f }
            java.lang.String r0 = r0.c()     // Catch:{ b -> 0x015f }
            goto L_0x004c
        L_0x04f8:
            r1 = 2
            com.cyberandsons.tcmaidtrial.TcmAid.bw = r1
            java.lang.String r1 = "\\r\n"
            java.lang.String r2 = "\n"
            java.lang.String r0 = r0.replace(r1, r2)
            com.cyberandsons.tcmaidtrial.quiz.i r1 = r11.i
            boolean r1 = r1.k
            boolean r2 = r11.A
            if (r1 != r2) goto L_0x06ce
            android.widget.ImageView r1 = r11.q
            r2 = 0
            r1.setVisibility(r2)
            android.widget.TextView r1 = r11.p
            r2 = 4
            r1.setVisibility(r2)
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.bp
            if (r1 != 0) goto L_0x05a5
            java.lang.String r0 = "%s _id=%d"
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r2 = 0
            java.lang.String r3 = "SELECT main.auricular.point, main.auricular.label, main.auricular.row1, main.auricular.col1, main.auricular.row2, main.auricular.col2, main.auricular.row3, main.auricular.col3, main.auricular.row4, main.auricular.col4, main.auricular.row5, main.auricular.col5, main.auricular.lblRow, main.auricular.lblCol, main.auricular.red, main.auricular.green, main.auricular.blue, main.auricular.depth, main.auricular.blocksz, main.auricular.blockdir, main.auricular._id FROM main.auricular WHERE "
            r1[r2] = r3
            r2 = 1
            com.cyberandsons.tcmaidtrial.quiz.t r3 = r11.j
            com.cyberandsons.tcmaidtrial.a.f r3 = r3.m
            int r3 = r3.a()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r1[r2] = r3
            java.lang.String r0 = java.lang.String.format(r0, r1)
            com.cyberandsons.tcmaidtrial.TcmAid.p = r0
            com.cyberandsons.tcmaidtrial.a.n r0 = r11.z
            a(r12, r0)
            java.lang.String r0 = "auricular"
        L_0x0541:
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.bp
            switch(r1) {
                case 0: goto L_0x05d8;
                case 1: goto L_0x0546;
                case 2: goto L_0x062e;
                case 3: goto L_0x0546;
                case 4: goto L_0x065b;
                case 5: goto L_0x06a5;
                default: goto L_0x0546;
            }
        L_0x0546:
            android.widget.TextView r0 = r11.m
            com.cyberandsons.tcmaidtrial.quiz.t r1 = r11.j
            com.cyberandsons.tcmaidtrial.quiz.i r2 = r11.i
            boolean r2 = r2.n
            com.cyberandsons.tcmaidtrial.quiz.i r3 = r11.i
            boolean r3 = r3.o
            java.lang.String r1 = r1.a(r2, r3)
            r0.setText(r1)
            android.widget.TextView r0 = r11.n
            com.cyberandsons.tcmaidtrial.quiz.t r1 = r11.j
            com.cyberandsons.tcmaidtrial.quiz.i r2 = r11.i
            boolean r2 = r2.n
            com.cyberandsons.tcmaidtrial.quiz.i r3 = r11.i
            boolean r3 = r3.o
            java.lang.String r1 = r1.b(r2, r3)
            r0.setText(r1)
            android.widget.TextView r0 = r11.o
            com.cyberandsons.tcmaidtrial.quiz.t r1 = r11.j
            com.cyberandsons.tcmaidtrial.quiz.i r2 = r11.i
            boolean r2 = r2.n
            com.cyberandsons.tcmaidtrial.quiz.i r3 = r11.i
            boolean r3 = r3.o
            java.lang.String r1 = r1.c(r2, r3)
            r0.setText(r1)
            android.widget.TextView r0 = r11.k
            java.lang.String r1 = "%d/%d"
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            com.cyberandsons.tcmaidtrial.quiz.t r4 = r11.j
            int r4 = r4.d
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r2[r3] = r4
            r3 = 1
            com.cyberandsons.tcmaidtrial.quiz.t r4 = r11.j
            int r4 = r4.c
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r2[r3] = r4
            java.lang.String r1 = java.lang.String.format(r1, r2)
            r0.setText(r1)
            goto L_0x0004
        L_0x05a5:
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.bp
            r2 = 4
            if (r1 != r2) goto L_0x0541
            java.lang.String r0 = "%s _id=%d"
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r2 = 0
            java.lang.String r3 = "SELECT main.pointslocation.common_name, main.pointslocation.image, main.pointslocation.label, main.pointslocation.row1, main.pointslocation.col1, main.pointslocation.row2, main.pointslocation.col2, main.pointslocation.row3, main.pointslocation.col3, main.pointslocation.row4, main.pointslocation.col4, main.pointslocation.row5, main.pointslocation.col5, main.pointslocation.lblRow, main.pointslocation.lblCol, main.pointslocation.red, main.pointslocation.green, main.pointslocation.blue, main.pointslocation.depth, main.pointslocation.scaleRow1, main.pointslocation.scaleCol1, main.pointslocation.scaleRow2, main.pointslocation.scaleCol2, main.pointslocation.scaleSize, main.pointslocation.scaleMarkers, main.pointslocation.meridian_points, main.pointslocation._id FROM main.pointslocation WHERE "
            r1[r2] = r3
            r2 = 1
            com.cyberandsons.tcmaidtrial.quiz.t r3 = r11.j
            com.cyberandsons.tcmaidtrial.a.i r3 = r3.q
            int r3 = r3.a()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r1[r2] = r3
            java.lang.String r0 = java.lang.String.format(r0, r1)
            com.cyberandsons.tcmaidtrial.TcmAid.ar = r0
            com.cyberandsons.tcmaidtrial.a.n r0 = r11.z
            b(r12, r0)
            com.cyberandsons.tcmaidtrial.quiz.t r0 = r11.j
            com.cyberandsons.tcmaidtrial.a.i r0 = r0.q
            java.lang.String r0 = r0.f()
            goto L_0x0541
        L_0x05d8:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.w
            r1 = 0
            java.lang.Object r12 = r0.get(r1)
            com.cyberandsons.tcmaidtrial.a.b r12 = (com.cyberandsons.tcmaidtrial.a.b) r12
            android.content.res.Resources r0 = r11.getResources()
            r1 = 2130837506(0x7f020002, float:1.7279968E38)
            java.lang.String r0 = r0.getString(r1)
            android.content.res.Resources r1 = r11.getResources()
            r2 = 2130837506(0x7f020002, float:1.7279968E38)
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeResource(r1, r2)
            r2 = 280(0x118, float:3.92E-43)
            r3 = 280(0x118, float:3.92E-43)
            android.graphics.drawable.BitmapDrawable r1 = com.cyberandsons.tcmaidtrial.misc.dh.a(r1, r2, r3)
            android.graphics.Bitmap r1 = r1.getBitmap()
            com.cyberandsons.tcmaidtrial.draw.a r2 = new com.cyberandsons.tcmaidtrial.draw.a
            int r3 = r1.getHeight()
            int r4 = r1.getWidth()
            r2.<init>(r11, r1, r3, r4)
            r2.a(r0, r12)
            android.graphics.Bitmap r0 = r2.a()
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.cS
            r2 = 0
            int r1 = r1 - r2
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.cS
            r3 = 0
            int r2 = r2 - r3
            android.view.Window r3 = r11.getWindow()
            android.graphics.drawable.BitmapDrawable r0 = com.cyberandsons.tcmaidtrial.misc.dh.a(r0, r1, r2, r3)
            android.widget.ImageView r1 = r11.q
            r1.setImageDrawable(r0)
            goto L_0x0546
        L_0x062e:
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.misc.dh.a(r0)
            java.lang.String r1 = "herbs"
            android.graphics.Bitmap r0 = com.cyberandsons.tcmaidtrial.misc.dh.b(r0, r1)
            if (r0 != 0) goto L_0x0644
            android.widget.ImageView r0 = r11.q
            r1 = 2130837543(0x7f020027, float:1.7280043E38)
            r0.setImageResource(r1)
            goto L_0x0546
        L_0x0644:
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.cS
            r2 = 0
            int r1 = r1 - r2
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.cS
            r3 = 0
            int r2 = r2 - r3
            android.view.Window r3 = r11.getWindow()
            android.graphics.drawable.BitmapDrawable r0 = com.cyberandsons.tcmaidtrial.misc.dh.a(r0, r1, r2, r3)
            android.widget.ImageView r1 = r11.q
            r1.setImageDrawable(r0)
            goto L_0x0546
        L_0x065b:
            java.lang.String r1 = "points"
            android.graphics.Bitmap r1 = com.cyberandsons.tcmaidtrial.misc.dh.b(r0, r1)
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.x
            r3 = 0
            java.lang.Object r12 = r2.get(r3)
            com.cyberandsons.tcmaidtrial.a.b r12 = (com.cyberandsons.tcmaidtrial.a.b) r12
            if (r1 != 0) goto L_0x0676
            android.widget.ImageView r0 = r11.q
            r1 = 2130837543(0x7f020027, float:1.7280043E38)
            r0.setImageResource(r1)
            goto L_0x0546
        L_0x0676:
            com.cyberandsons.tcmaidtrial.draw.a r2 = new com.cyberandsons.tcmaidtrial.draw.a
            int r3 = r1.getHeight()
            int r4 = r1.getWidth()
            r2.<init>(r11, r1, r3, r4)
            boolean r1 = r11.A
            boolean r3 = r11.B
            r2.a(r0, r12, r1, r3)
            android.graphics.Bitmap r0 = r2.a()
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.cS
            r2 = 0
            int r1 = r1 - r2
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.cS
            r3 = 0
            int r2 = r2 - r3
            android.view.Window r3 = r11.getWindow()
            android.graphics.drawable.BitmapDrawable r0 = com.cyberandsons.tcmaidtrial.misc.dh.a(r0, r1, r2, r3)
            android.widget.ImageView r1 = r11.q
            r1.setImageDrawable(r0)
            goto L_0x0546
        L_0x06a5:
            java.lang.String r1 = "pulsediag"
            android.graphics.Bitmap r0 = com.cyberandsons.tcmaidtrial.misc.dh.b(r0, r1)
            if (r0 != 0) goto L_0x06b7
            android.widget.ImageView r0 = r11.q
            r1 = 2130837543(0x7f020027, float:1.7280043E38)
            r0.setImageResource(r1)
            goto L_0x0546
        L_0x06b7:
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.cS
            r2 = 0
            int r1 = r1 - r2
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.cS
            r3 = 0
            int r2 = r2 - r3
            android.view.Window r3 = r11.getWindow()
            android.graphics.drawable.BitmapDrawable r0 = com.cyberandsons.tcmaidtrial.misc.dh.a(r0, r1, r2, r3)
            android.widget.ImageView r1 = r11.q
            r1.setImageDrawable(r0)
            goto L_0x0546
        L_0x06ce:
            android.widget.TextView r1 = r11.p
            r1.getTextSize()
            android.widget.TextView r1 = r11.p
            r2 = 1099956224(0x41900000, float:18.0)
            r1.setTextSize(r2)
            android.widget.ImageView r1 = r11.q
            r2 = 4
            r1.setVisibility(r2)
            android.widget.TextView r1 = r11.p
            r2 = 0
            r1.setVisibility(r2)
            android.widget.TextView r1 = r11.p
            r1.setText(r0)
            goto L_0x0546
        L_0x06ed:
            r0 = r4
            goto L_0x0151
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.b(android.database.sqlite.SQLiteDatabase):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0151  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x015a  */
    /* JADX WARNING: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(android.database.sqlite.SQLiteDatabase r35, com.cyberandsons.tcmaidtrial.a.n r36) {
        /*
            r3 = 0
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.p     // Catch:{ SQLException -> 0x0149, all -> 0x0155 }
            r5 = 0
            r0 = r35
            r1 = r4
            r2 = r5
            android.database.Cursor r35 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x0149, all -> 0x0155 }
            android.database.sqlite.SQLiteCursor r35 = (android.database.sqlite.SQLiteCursor) r35     // Catch:{ SQLException -> 0x0149, all -> 0x0155 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.w     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r3.clear()     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            boolean r3 = r35.moveToFirst()     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            if (r3 == 0) goto L_0x0135
        L_0x0019:
            r3 = 0
            r0 = r35
            r1 = r3
            java.lang.String r6 = r0.getString(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            java.lang.String r7 = "auricular"
            r3 = 1
            r0 = r35
            r1 = r3
            java.lang.String r8 = r0.getString(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r3 = 2
            r0 = r35
            r1 = r3
            int r9 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r3 = 3
            r0 = r35
            r1 = r3
            int r10 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r3 = 4
            r0 = r35
            r1 = r3
            int r11 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r3 = 5
            r0 = r35
            r1 = r3
            int r12 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r3 = 6
            r0 = r35
            r1 = r3
            int r13 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r3 = 7
            r0 = r35
            r1 = r3
            int r14 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r3 = 8
            r0 = r35
            r1 = r3
            int r15 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r3 = 9
            r0 = r35
            r1 = r3
            int r16 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r3 = 10
            r0 = r35
            r1 = r3
            int r17 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r3 = 11
            r0 = r35
            r1 = r3
            int r18 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r3 = 12
            r0 = r35
            r1 = r3
            int r19 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r3 = 13
            r0 = r35
            r1 = r3
            int r20 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r3 = 14
            r0 = r35
            r1 = r3
            float r3 = r0.getFloat(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r4 = 1065353216(0x3f800000, float:1.0)
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 != 0) goto L_0x013b
            r3 = 1132396544(0x437f0000, float:255.0)
        L_0x00a2:
            r4 = 15
            r0 = r35
            r1 = r4
            float r4 = r0.getFloat(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r5 = 1065353216(0x3f800000, float:1.0)
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 != 0) goto L_0x013e
            r4 = 1124073472(0x43000000, float:128.0)
            r5 = r4
        L_0x00b4:
            r4 = 16
            r0 = r35
            r1 = r4
            float r4 = r0.getFloat(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r21 = 1065353216(0x3f800000, float:1.0)
            int r4 = (r4 > r21 ? 1 : (r4 == r21 ? 0 : -1))
            if (r4 != 0) goto L_0x0142
            r4 = 1132396544(0x437f0000, float:255.0)
            r21 = r4
        L_0x00c7:
            r4 = 17
            r0 = r35
            r1 = r4
            java.lang.String r27 = r0.getString(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r4 = 18
            r0 = r35
            r1 = r4
            int r28 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r4 = 19
            r0 = r35
            r1 = r4
            java.lang.String r29 = r0.getString(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r4 = 20
            r0 = r35
            r1 = r4
            int r4 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            boolean r22 = r36.t()     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r23 = 1
            r0 = r22
            r1 = r23
            if (r0 != r1) goto L_0x0146
            r22 = 1
        L_0x00f9:
            if (r22 != 0) goto L_0x0170
            r3 = 0
            r5 = 0
            r21 = 0
            r32 = r21
            r31 = r5
            r30 = r3
        L_0x0105:
            java.util.ArrayList r33 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.w     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            com.cyberandsons.tcmaidtrial.a.b r3 = new com.cyberandsons.tcmaidtrial.a.b     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            java.lang.String r5 = ""
            r21 = 0
            r22 = 0
            r23 = 0
            r24 = 0
            r25 = 0
            r26 = 0
            r0 = r30
            int r0 = (int) r0     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r30 = r0
            r0 = r31
            int r0 = (int) r0     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r31 = r0
            r0 = r32
            int r0 = (int) r0     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r32 = r0
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            r0 = r33
            r1 = r3
            r0.add(r1)     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            boolean r3 = r35.moveToNext()     // Catch:{ SQLException -> 0x0168, all -> 0x015e }
            if (r3 != 0) goto L_0x0019
        L_0x0135:
            if (r35 == 0) goto L_0x013a
            r35.close()
        L_0x013a:
            return
        L_0x013b:
            r3 = 0
            goto L_0x00a2
        L_0x013e:
            r4 = 0
            r5 = r4
            goto L_0x00b4
        L_0x0142:
            r4 = 0
            r21 = r4
            goto L_0x00c7
        L_0x0146:
            r22 = 0
            goto L_0x00f9
        L_0x0149:
            r35 = move-exception
            r36 = r3
        L_0x014c:
            com.cyberandsons.tcmaidtrial.a.q.a(r35)     // Catch:{ all -> 0x0166 }
            if (r36 == 0) goto L_0x013a
            r36.close()
            goto L_0x013a
        L_0x0155:
            r35 = move-exception
            r36 = r3
        L_0x0158:
            if (r36 == 0) goto L_0x015d
            r36.close()
        L_0x015d:
            throw r35
        L_0x015e:
            r36 = move-exception
            r34 = r36
            r36 = r35
            r35 = r34
            goto L_0x0158
        L_0x0166:
            r35 = move-exception
            goto L_0x0158
        L_0x0168:
            r36 = move-exception
            r34 = r36
            r36 = r35
            r35 = r34
            goto L_0x014c
        L_0x0170:
            r32 = r21
            r31 = r5
            r30 = r3
            goto L_0x0105
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.a(android.database.sqlite.SQLiteDatabase, com.cyberandsons.tcmaidtrial.a.n):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void c(android.database.sqlite.SQLiteDatabase r12) {
        /*
            r2 = 0
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.af     // Catch:{ SQLException -> 0x007a, all -> 0x0085 }
            r1 = 0
            android.database.Cursor r12 = r12.rawQuery(r0, r1)     // Catch:{ SQLException -> 0x007a, all -> 0x0085 }
            android.database.sqlite.SQLiteCursor r12 = (android.database.sqlite.SQLiteCursor) r12     // Catch:{ SQLException -> 0x007a, all -> 0x0085 }
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.c     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            r0.clear()     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            boolean r0 = r12.moveToFirst()     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            if (r0 == 0) goto L_0x0074
        L_0x0015:
            r0 = 0
            int r1 = r12.getInt(r0)     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            r0 = 1
            java.lang.String r0 = r12.getString(r0)     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.misc.ad.a(r0)     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            r0 = 2
            java.lang.String r3 = r12.getString(r0)     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            r0 = 3
            java.lang.String r4 = r12.getString(r0)     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            r0 = 4
            java.lang.String r5 = r12.getString(r0)     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            r0 = 5
            java.lang.String r6 = r12.getString(r0)     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            r0 = 6
            java.lang.String r7 = r12.getString(r0)     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            r0 = 7
            java.lang.String r9 = r12.getString(r0)     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            r0 = 8
            java.lang.String r10 = r12.getString(r0)     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            r0.<init>()     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            java.lang.String r8 = "Taste: "
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            java.lang.String r8 = "\nTemperature: "
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            java.lang.String r8 = r0.toString()     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            java.util.ArrayList r11 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.c     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            com.cyberandsons.tcmaidtrial.a.g r0 = new com.cyberandsons.tcmaidtrial.a.g     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            r11.add(r0)     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            boolean r0 = r12.moveToNext()     // Catch:{ SQLException -> 0x0092, all -> 0x008d }
            if (r0 != 0) goto L_0x0015
        L_0x0074:
            if (r12 == 0) goto L_0x0079
            r12.close()
        L_0x0079:
            return
        L_0x007a:
            r0 = move-exception
            r1 = r2
        L_0x007c:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0090 }
            if (r1 == 0) goto L_0x0079
            r1.close()
            goto L_0x0079
        L_0x0085:
            r0 = move-exception
            r1 = r2
        L_0x0087:
            if (r1 == 0) goto L_0x008c
            r1.close()
        L_0x008c:
            throw r0
        L_0x008d:
            r0 = move-exception
            r1 = r12
            goto L_0x0087
        L_0x0090:
            r0 = move-exception
            goto L_0x0087
        L_0x0092:
            r0 = move-exception
            r1 = r12
            goto L_0x007c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.c(android.database.sqlite.SQLiteDatabase):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void d(android.database.sqlite.SQLiteDatabase r10) {
        /*
            r2 = 0
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.S     // Catch:{ SQLException -> 0x0057, all -> 0x0062 }
            r1 = 0
            android.database.Cursor r10 = r10.rawQuery(r0, r1)     // Catch:{ SQLException -> 0x0057, all -> 0x0062 }
            android.database.sqlite.SQLiteCursor r10 = (android.database.sqlite.SQLiteCursor) r10     // Catch:{ SQLException -> 0x0057, all -> 0x0062 }
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.e     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            r0.clear()     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            boolean r0 = r10.moveToFirst()     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            if (r0 == 0) goto L_0x0051
        L_0x0015:
            r0 = 0
            int r1 = r10.getInt(r0)     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            r0 = 1
            java.lang.String r0 = r10.getString(r0)     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.misc.ad.a(r0)     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            r0 = 2
            java.lang.String r3 = r10.getString(r0)     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            r0 = 3
            java.lang.String r4 = r10.getString(r0)     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            r0 = 4
            java.lang.String r5 = r10.getString(r0)     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            r0 = 5
            java.lang.String r6 = r10.getString(r0)     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            r0 = 6
            java.lang.String r7 = r10.getString(r0)     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            r0 = 7
            java.lang.String r8 = r10.getString(r0)     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            java.util.ArrayList r9 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.e     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            com.cyberandsons.tcmaidtrial.a.e r0 = new com.cyberandsons.tcmaidtrial.a.e     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            r9.add(r0)     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            boolean r0 = r10.moveToNext()     // Catch:{ SQLException -> 0x006f, all -> 0x006a }
            if (r0 != 0) goto L_0x0015
        L_0x0051:
            if (r10 == 0) goto L_0x0056
            r10.close()
        L_0x0056:
            return
        L_0x0057:
            r0 = move-exception
            r1 = r2
        L_0x0059:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x006d }
            if (r1 == 0) goto L_0x0056
            r1.close()
            goto L_0x0056
        L_0x0062:
            r0 = move-exception
            r1 = r2
        L_0x0064:
            if (r1 == 0) goto L_0x0069
            r1.close()
        L_0x0069:
            throw r0
        L_0x006a:
            r0 = move-exception
            r1 = r10
            goto L_0x0064
        L_0x006d:
            r0 = move-exception
            goto L_0x0064
        L_0x006f:
            r0 = move-exception
            r1 = r10
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.d(android.database.sqlite.SQLiteDatabase):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(android.database.sqlite.SQLiteDatabase r10, int r11) {
        /*
            r2 = 0
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.ar     // Catch:{ SQLException -> 0x0053, all -> 0x005e }
            r1 = 0
            android.database.Cursor r10 = r10.rawQuery(r0, r1)     // Catch:{ SQLException -> 0x0053, all -> 0x005e }
            android.database.sqlite.SQLiteCursor r10 = (android.database.sqlite.SQLiteCursor) r10     // Catch:{ SQLException -> 0x0053, all -> 0x005e }
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.g     // Catch:{ SQLException -> 0x006b, all -> 0x0066 }
            r0.clear()     // Catch:{ SQLException -> 0x006b, all -> 0x0066 }
            boolean r0 = r10.moveToFirst()     // Catch:{ SQLException -> 0x006b, all -> 0x0066 }
            if (r0 == 0) goto L_0x004d
        L_0x0015:
            r0 = 0
            int r1 = r10.getInt(r0)     // Catch:{ SQLException -> 0x006b, all -> 0x0066 }
            r0 = 1
            java.lang.String r2 = r10.getString(r0)     // Catch:{ SQLException -> 0x006b, all -> 0x0066 }
            r0 = 2
            java.lang.String r3 = r10.getString(r0)     // Catch:{ SQLException -> 0x006b, all -> 0x0066 }
            r0 = 3
            java.lang.String r4 = r10.getString(r0)     // Catch:{ SQLException -> 0x006b, all -> 0x0066 }
            r0 = 4
            java.lang.String r5 = r10.getString(r0)     // Catch:{ SQLException -> 0x006b, all -> 0x0066 }
            r0 = 5
            java.lang.String r6 = r10.getString(r0)     // Catch:{ SQLException -> 0x006b, all -> 0x0066 }
            r0 = 6
            java.lang.String r7 = r10.getString(r0)     // Catch:{ SQLException -> 0x006b, all -> 0x0066 }
            java.util.ArrayList r8 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.g     // Catch:{ SQLException -> 0x006b, all -> 0x0066 }
            com.cyberandsons.tcmaidtrial.a.i r0 = new com.cyberandsons.tcmaidtrial.a.i     // Catch:{ SQLException -> 0x006b, all -> 0x0066 }
            r9 = -1
            if (r11 != r9) goto L_0x0041
            java.lang.String r7 = ""
        L_0x0041:
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLException -> 0x006b, all -> 0x0066 }
            r8.add(r0)     // Catch:{ SQLException -> 0x006b, all -> 0x0066 }
            boolean r0 = r10.moveToNext()     // Catch:{ SQLException -> 0x006b, all -> 0x0066 }
            if (r0 != 0) goto L_0x0015
        L_0x004d:
            if (r10 == 0) goto L_0x0052
            r10.close()
        L_0x0052:
            return
        L_0x0053:
            r0 = move-exception
            r1 = r2
        L_0x0055:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0069 }
            if (r1 == 0) goto L_0x0052
            r1.close()
            goto L_0x0052
        L_0x005e:
            r0 = move-exception
            r1 = r2
        L_0x0060:
            if (r1 == 0) goto L_0x0065
            r1.close()
        L_0x0065:
            throw r0
        L_0x0066:
            r0 = move-exception
            r1 = r10
            goto L_0x0060
        L_0x0069:
            r0 = move-exception
            goto L_0x0060
        L_0x006b:
            r0 = move-exception
            r1 = r10
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.a(android.database.sqlite.SQLiteDatabase, int):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x01a2  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01ab  */
    /* JADX WARNING: Removed duplicated region for block: B:57:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void b(android.database.sqlite.SQLiteDatabase r35, com.cyberandsons.tcmaidtrial.a.n r36) {
        /*
            r3 = 0
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.ar     // Catch:{ SQLException -> 0x019a, all -> 0x01a6 }
            r5 = 0
            r0 = r35
            r1 = r4
            r2 = r5
            android.database.Cursor r35 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x019a, all -> 0x01a6 }
            android.database.sqlite.SQLiteCursor r35 = (android.database.sqlite.SQLiteCursor) r35     // Catch:{ SQLException -> 0x019a, all -> 0x01a6 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.x     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r3.clear()     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            boolean r3 = r35.moveToFirst()     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            if (r3 == 0) goto L_0x017a
        L_0x0019:
            r3 = 0
            r0 = r35
            r1 = r3
            java.lang.String r6 = r0.getString(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r3 = 1
            r0 = r35
            r1 = r3
            java.lang.String r7 = r0.getString(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r3 = 2
            r0 = r35
            r1 = r3
            java.lang.String r8 = r0.getString(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r3 = 3
            r0 = r35
            r1 = r3
            int r9 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r3 = 4
            r0 = r35
            r1 = r3
            int r10 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r3 = 5
            r0 = r35
            r1 = r3
            int r11 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r3 = 6
            r0 = r35
            r1 = r3
            int r12 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r3 = 7
            r0 = r35
            r1 = r3
            int r13 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r3 = 8
            r0 = r35
            r1 = r3
            int r14 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r3 = 9
            r0 = r35
            r1 = r3
            int r15 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r3 = 10
            r0 = r35
            r1 = r3
            int r16 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r3 = 11
            r0 = r35
            r1 = r3
            int r17 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r3 = 12
            r0 = r35
            r1 = r3
            int r18 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r3 = 13
            r0 = r35
            r1 = r3
            int r19 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r3 = 14
            r0 = r35
            r1 = r3
            int r20 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r3 = 15
            r0 = r35
            r1 = r3
            float r3 = r0.getFloat(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r4 = 1065353216(0x3f800000, float:1.0)
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 != 0) goto L_0x0180
            r3 = 1132396544(0x437f0000, float:255.0)
        L_0x00a9:
            r4 = 16
            r0 = r35
            r1 = r4
            float r4 = r0.getFloat(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r5 = 1065353216(0x3f800000, float:1.0)
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 != 0) goto L_0x0183
            r4 = 1124073472(0x43000000, float:128.0)
            r5 = r4
        L_0x00bb:
            r4 = 17
            r0 = r35
            r1 = r4
            float r4 = r0.getFloat(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r21 = 1065353216(0x3f800000, float:1.0)
            int r4 = (r4 > r21 ? 1 : (r4 == r21 ? 0 : -1))
            if (r4 != 0) goto L_0x0187
            r4 = 1132396544(0x437f0000, float:255.0)
            r28 = r4
        L_0x00ce:
            r4 = 18
            r0 = r35
            r1 = r4
            java.lang.String r27 = r0.getString(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r4 = 19
            r0 = r35
            r1 = r4
            int r22 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r4 = 20
            r0 = r35
            r1 = r4
            int r21 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r4 = 21
            r0 = r35
            r1 = r4
            int r24 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r4 = 22
            r0 = r35
            r1 = r4
            int r23 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r4 = 23
            r0 = r35
            r1 = r4
            int r25 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r4 = 24
            r0 = r35
            r1 = r4
            int r26 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r4 = 25
            r0 = r35
            r1 = r4
            int r4 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            boolean r29 = r36.t()     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r30 = 1
            r0 = r29
            r1 = r30
            if (r0 != r1) goto L_0x018c
            r29 = 1
        L_0x0124:
            if (r29 != 0) goto L_0x01c1
            java.lang.String r3 = "foot_1"
            int r3 = r7.compareTo(r3)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            if (r3 == 0) goto L_0x0146
            java.lang.String r3 = "foot_2"
            int r3 = r7.compareTo(r3)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            if (r3 == 0) goto L_0x0146
            java.lang.String r3 = "hand_1"
            int r3 = r7.compareTo(r3)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            if (r3 == 0) goto L_0x0146
            java.lang.String r3 = "hand_2"
            int r3 = r7.compareTo(r3)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            if (r3 != 0) goto L_0x018f
        L_0x0146:
            r3 = 1065353216(0x3f800000, float:1.0)
            r5 = 1065353216(0x3f800000, float:1.0)
            r28 = 1065353216(0x3f800000, float:1.0)
            r32 = r28
            r31 = r5
            r30 = r3
        L_0x0152:
            java.util.ArrayList r33 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.x     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            com.cyberandsons.tcmaidtrial.a.b r3 = new com.cyberandsons.tcmaidtrial.a.b     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            java.lang.String r5 = ""
            r28 = 0
            java.lang.String r29 = ""
            r0 = r30
            int r0 = (int) r0     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r30 = r0
            r0 = r31
            int r0 = (int) r0     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r31 = r0
            r0 = r32
            int r0 = (int) r0     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r32 = r0
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            r0 = r33
            r1 = r3
            r0.add(r1)     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            boolean r3 = r35.moveToNext()     // Catch:{ SQLException -> 0x01b9, all -> 0x01af }
            if (r3 != 0) goto L_0x0019
        L_0x017a:
            if (r35 == 0) goto L_0x017f
            r35.close()
        L_0x017f:
            return
        L_0x0180:
            r3 = 0
            goto L_0x00a9
        L_0x0183:
            r4 = 0
            r5 = r4
            goto L_0x00bb
        L_0x0187:
            r4 = 0
            r28 = r4
            goto L_0x00ce
        L_0x018c:
            r29 = 0
            goto L_0x0124
        L_0x018f:
            r3 = 0
            r5 = 0
            r28 = 0
            r32 = r28
            r31 = r5
            r30 = r3
            goto L_0x0152
        L_0x019a:
            r35 = move-exception
            r36 = r3
        L_0x019d:
            com.cyberandsons.tcmaidtrial.a.q.a(r35)     // Catch:{ all -> 0x01b7 }
            if (r36 == 0) goto L_0x017f
            r36.close()
            goto L_0x017f
        L_0x01a6:
            r35 = move-exception
            r36 = r3
        L_0x01a9:
            if (r36 == 0) goto L_0x01ae
            r36.close()
        L_0x01ae:
            throw r35
        L_0x01af:
            r36 = move-exception
            r34 = r36
            r36 = r35
            r35 = r34
            goto L_0x01a9
        L_0x01b7:
            r35 = move-exception
            goto L_0x01a9
        L_0x01b9:
            r36 = move-exception
            r34 = r36
            r36 = r35
            r35 = r34
            goto L_0x019d
        L_0x01c1:
            r32 = r28
            r31 = r5
            r30 = r3
            goto L_0x0152
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.b(android.database.sqlite.SQLiteDatabase, com.cyberandsons.tcmaidtrial.a.n):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void e(android.database.sqlite.SQLiteDatabase r7) {
        /*
            r2 = 0
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.aF     // Catch:{ SQLException -> 0x0048, all -> 0x0053 }
            r1 = 0
            android.database.Cursor r7 = r7.rawQuery(r0, r1)     // Catch:{ SQLException -> 0x0048, all -> 0x0053 }
            android.database.sqlite.SQLiteCursor r7 = (android.database.sqlite.SQLiteCursor) r7     // Catch:{ SQLException -> 0x0048, all -> 0x0053 }
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.h     // Catch:{ SQLException -> 0x0060, all -> 0x005b }
            r0.clear()     // Catch:{ SQLException -> 0x0060, all -> 0x005b }
            boolean r0 = r7.moveToFirst()     // Catch:{ SQLException -> 0x0060, all -> 0x005b }
            if (r0 == 0) goto L_0x0042
        L_0x0015:
            r0 = 0
            int r1 = r7.getInt(r0)     // Catch:{ SQLException -> 0x0060, all -> 0x005b }
            r0 = 1
            java.lang.String r0 = r7.getString(r0)     // Catch:{ SQLException -> 0x0060, all -> 0x005b }
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.misc.ad.a(r0)     // Catch:{ SQLException -> 0x0060, all -> 0x005b }
            r0 = 2
            java.lang.String r3 = r7.getString(r0)     // Catch:{ SQLException -> 0x0060, all -> 0x005b }
            r0 = 3
            java.lang.String r4 = r7.getString(r0)     // Catch:{ SQLException -> 0x0060, all -> 0x005b }
            r0 = 4
            java.lang.String r5 = r7.getString(r0)     // Catch:{ SQLException -> 0x0060, all -> 0x005b }
            java.util.ArrayList r6 = com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.h     // Catch:{ SQLException -> 0x0060, all -> 0x005b }
            com.cyberandsons.tcmaidtrial.a.h r0 = new com.cyberandsons.tcmaidtrial.a.h     // Catch:{ SQLException -> 0x0060, all -> 0x005b }
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ SQLException -> 0x0060, all -> 0x005b }
            r6.add(r0)     // Catch:{ SQLException -> 0x0060, all -> 0x005b }
            boolean r0 = r7.moveToNext()     // Catch:{ SQLException -> 0x0060, all -> 0x005b }
            if (r0 != 0) goto L_0x0015
        L_0x0042:
            if (r7 == 0) goto L_0x0047
            r7.close()
        L_0x0047:
            return
        L_0x0048:
            r0 = move-exception
            r1 = r2
        L_0x004a:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x005e }
            if (r1 == 0) goto L_0x0047
            r1.close()
            goto L_0x0047
        L_0x0053:
            r0 = move-exception
            r1 = r2
        L_0x0055:
            if (r1 == 0) goto L_0x005a
            r1.close()
        L_0x005a:
            throw r0
        L_0x005b:
            r0 = move-exception
            r1 = r7
            goto L_0x0055
        L_0x005e:
            r0 = move-exception
            goto L_0x0055
        L_0x0060:
            r0 = move-exception
            r1 = r7
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.quiz.RunQuizTab.e(android.database.sqlite.SQLiteDatabase):void");
    }

    private void a(String str, String str2, String str3, String str4) {
        AlertDialog create = new AlertDialog.Builder(this).create();
        create.setTitle(str2);
        create.setMessage(str3);
        create.setButton("OK", new z(this, str, str4));
        create.show();
    }

    private void a() {
        if (this.y == null || !this.y.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("RQT:openDataBase()", str + " opened.");
                this.y = SQLiteDatabase.openDatabase(str, null, 16);
                this.y.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.y.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("RQT:openDataBase()", str2 + " ATTACHed.");
                this.z = new n();
                this.z.a(this.y);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new u(this));
                create.show();
            }
        }
    }
}
