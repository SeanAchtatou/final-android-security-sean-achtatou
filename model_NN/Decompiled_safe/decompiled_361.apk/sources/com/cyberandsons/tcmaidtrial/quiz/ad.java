package com.cyberandsons.tcmaidtrial.quiz;

import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.k;
import java.util.Date;

final class ad implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SQLiteDatabase f1053a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ RunQuizTab f1054b;

    ad(RunQuizTab runQuizTab, SQLiteDatabase sQLiteDatabase) {
        this.f1054b = runQuizTab;
        this.f1053a = sQLiteDatabase;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        int i2 = TcmAid.bp;
        String str = this.f1054b.i.g;
        String str2 = this.f1054b.i.h;
        String b2 = this.f1054b.j.b();
        String c = this.f1054b.j.c();
        int a2 = this.f1054b.j.a();
        t tVar = this.f1054b.j;
        new k(i2, str, str2, b2, c, a2, tVar.c - tVar.j, this.f1054b.j.w, new Date()).a(this.f1053a);
        TcmAid.bx.setCurrentTab(0);
        TcmAid.bx.getTabWidget().postInvalidate();
        TcmAid.bx.postInvalidate();
    }
}
