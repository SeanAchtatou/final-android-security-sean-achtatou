package com.cyberandsons.tcmaidtrial.quiz;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.ScrollView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.c;
import com.cyberandsons.tcmaidtrial.a.m;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.lists.AuricularList;
import com.cyberandsons.tcmaidtrial.lists.DiagnosisList;
import com.cyberandsons.tcmaidtrial.lists.FormulasList;
import com.cyberandsons.tcmaidtrial.lists.HerbList;
import com.cyberandsons.tcmaidtrial.lists.PointsList;
import com.cyberandsons.tcmaidtrial.lists.PulseDiagList;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class QuizHistoryDetail extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private TextView f1037a;

    /* renamed from: b  reason: collision with root package name */
    private TextView f1038b;
    private TextView c;
    private TextView d;
    private TextView e;
    private TextView f;
    private int g;
    private int h;
    private int i;
    private int j;
    private String k;
    private String l;
    private boolean m = true;
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;
    private SQLiteDatabase r;
    private n s;

    public QuizHistoryDetail() {
        this.n = !this.m;
        this.o = this.n;
        this.p = this.m;
        this.q = this.n;
    }

    static /* synthetic */ void a(QuizHistoryDetail quizHistoryDetail) {
        switch (quizHistoryDetail.j) {
            case 0:
                if (quizHistoryDetail.q) {
                    quizHistoryDetail.q = quizHistoryDetail.n;
                    return;
                }
                String str = quizHistoryDetail.k;
                TcmAid.o = "SELECT main.auricular._id, lower(main.auricular.point), main.auricular.pointcat, main.auricular.nada, main.auricular.battlefield, main.auricular.acacd, main.auricular.stopsmoking, main.auricular.weightloss FROM main.auricular " + " WHERE (main" + str + ')' + " UNION " + "SELECT userDB.auricular._id, lower(userDB.auricular.point), userDB.auricular.pointcat, 0, 0, 0, 0, 0 FROM userDB.auricular " + " WHERE (userDB" + str + ") " + " AND userDB.auricular._id>1000 ORDER BY 2";
                TcmAid.m = null;
                quizHistoryDetail.startActivityForResult(new Intent(quizHistoryDetail, AuricularList.class), 1350);
                return;
            case 1:
                if (quizHistoryDetail.q) {
                    quizHistoryDetail.q = quizHistoryDetail.n;
                    return;
                }
                String str2 = quizHistoryDetail.k;
                TcmAid.E = "SELECT main.zangfupathology._id, lower(main.zangfupathology.pathology), main.zangfupathology.pathtype FROM main.zangfupathology " + " WHERE (main" + str2 + ')' + " UNION " + "SELECT userDB.zangfupathology._id, lower(userDB.zangfupathology.pathology), userDB.zangfupathology.pathtype FROM userDB.zangfupathology " + " WHERE (userDB" + str2 + ") " + " AND userDB.zangfupathology._id>1000 ORDER BY 2";
                TcmAid.C = null;
                quizHistoryDetail.startActivityForResult(new Intent(quizHistoryDetail, DiagnosisList.class), 1350);
                return;
            case 2:
                if (quizHistoryDetail.q) {
                    quizHistoryDetail.q = quizHistoryDetail.n;
                    return;
                }
                int O = quizHistoryDetail.s.O();
                String str3 = (O == 0 || O == 1) ? "pinyin" : O == 2 ? "english" : "botanical";
                String str4 = quizHistoryDetail.k;
                TcmAid.af = m.b(str3) + " WHERE (main" + str4 + ')' + " UNION " + m.c(str3) + " WHERE (userDB" + str4 + ") " + " AND userDB.materiamedica._id>1000 ORDER BY 2";
                TcmAid.ad = null;
                quizHistoryDetail.startActivityForResult(new Intent(quizHistoryDetail, HerbList.class), 1350);
                return;
            case 3:
                if (quizHistoryDetail.q) {
                    quizHistoryDetail.q = quizHistoryDetail.n;
                    return;
                }
                int N = quizHistoryDetail.s.N();
                String str5 = (N == 0 || N == 1) ? "pinyin" : "english";
                String str6 = quizHistoryDetail.k;
                TcmAid.S = c.b(str5) + " WHERE (main" + str6 + ')' + " UNION " + c.c(str5) + " WHERE (userDB" + str6 + ") " + " AND userDB.formulas._id>1000 ORDER BY 2";
                TcmAid.Q = null;
                quizHistoryDetail.startActivityForResult(new Intent(quizHistoryDetail, FormulasList.class), 1350);
                return;
            case 4:
                if (quizHistoryDetail.q) {
                    quizHistoryDetail.q = quizHistoryDetail.n;
                    return;
                }
                String str7 = quizHistoryDetail.k;
                TcmAid.ar = "SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian " + " WHERE (main" + str7 + ')' + " UNION " + "SELECT userDB.pointslocation._id, main.meridian.abbr_name, userDB.pointslocation.pointnum, userDB.pointslocation.name, userDB.pointslocation.englishname, userDB.pointslocation.indication FROM userDB.pointslocation LEFT JOIN main.meridian ON main.meridian._id = userDB.pointslocation.meridian " + " WHERE (userDB" + str7 + ") " + " AND userDB.pointslocation._id>1000 ORDER BY 2";
                TcmAid.ap = null;
                quizHistoryDetail.startActivityForResult(new Intent(quizHistoryDetail, PointsList.class), 1350);
                return;
            case 5:
                if (quizHistoryDetail.q) {
                    quizHistoryDetail.q = quizHistoryDetail.n;
                    return;
                }
                String str8 = quizHistoryDetail.k;
                TcmAid.aF = "SELECT main.pulsediag._id, main.pulsediag.name, main.pulsediag.category FROM main.pulsediag " + " WHERE (main" + str8 + ')' + " UNION " + "SELECT userDB.pulsediag._id, userDB.pulsediag.name, userDB.pulsediag.category FROM userDB.pulsediag " + " WHERE (userDB" + str8 + ") " + " AND userDB.pulsediag._id>1000 ORDER BY 2";
                TcmAid.aD = null;
                quizHistoryDetail.startActivityForResult(new Intent(quizHistoryDetail, PulseDiagList.class), 1350);
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.r == null || !this.r.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("QHD:openDataBase()", str + " opened.");
                this.r = SQLiteDatabase.openDatabase(str, null, 16);
                this.r.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.r.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("QHD:openDataBase()", str2 + " ATTACHed.");
                this.s = new n();
                this.s.a(this.r);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new b(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        setContentView((int) C0000R.layout.quiz_history_detailed);
        this.f1037a = (TextView) findViewById(C0000R.id.tc_label);
        ((ScrollView) findViewById(C0000R.id.pointsDetail)).smoothScrollTo(0, 0);
        getWindow().setSoftInputMode(3);
        if (this.f1038b != null) {
            this.f1038b = null;
        }
        this.f1038b = (TextView) findViewById(C0000R.id.quizArea);
        if (this.c != null) {
            this.c = null;
        }
        this.c = (TextView) findViewById(C0000R.id.quizType);
        if (this.d != null) {
            this.d = null;
        }
        this.d = (TextView) findViewById(C0000R.id.category);
        if (this.e != null) {
            this.e = null;
        }
        this.e = (TextView) findViewById(C0000R.id.score);
        if (this.f != null) {
            this.f = null;
        }
        this.f = (TextView) findViewById(C0000R.id.missedItems);
        this.f.setOnClickListener(new a(this));
        a();
    }

    public void onDestroy() {
        TcmAid.o = null;
        TcmAid.af = null;
        TcmAid.S = null;
        TcmAid.ar = null;
        TcmAid.aF = null;
        TcmAid.E = null;
        try {
            if (this.r != null && this.r.isOpen()) {
                this.r.close();
                this.r = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            this.q = this.m;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x0232  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0239  */
    /* JADX WARNING: Removed duplicated region for block: B:56:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a() {
        /*
            r19 = this;
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "SELECT userDB.quizsessions._id, userDB.quizsessions.area, userDB.quizsessions.questions, userDB.quizsessions.correct, userDB.quizsessions.seconds, userDB.quizsessions.type, userDB.quizsessions.category, userDB.quizsessions.missed, userDB.quizsessions.id_missed, userDB.quizsessions.date FROM userDB.quizsessions WHERE "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.TcmAid.bn
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.B
            if (r3 == 0) goto L_0x001e
            java.lang.String r3 = "QHD:loadSystemSuppliedData()"
            android.util.Log.d(r3, r2)
        L_0x001e:
            r3 = 0
            r0 = r19
            android.database.sqlite.SQLiteDatabase r0 = r0.r     // Catch:{ SQLException -> 0x022c }
            r4 = r0
            r5 = 0
            android.database.Cursor r2 = r4.rawQuery(r2, r5)     // Catch:{ SQLException -> 0x022c }
            android.database.sqlite.SQLiteCursor r2 = (android.database.sqlite.SQLiteCursor) r2     // Catch:{ SQLException -> 0x022c }
            int r3 = r2.getCount()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            boolean r4 = com.cyberandsons.tcmaidtrial.misc.dh.y     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            if (r4 == 0) goto L_0x0075
            java.lang.String r4 = "QHD:loadSystemSuppliedData()"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r5.<init>()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r6 = "query count = "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r6 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r5 = r5.toString()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            android.util.Log.d(r4, r5)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r4 = "QHD:loadSystemSuppliedData()"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r5.<init>()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r6 = "column count = '"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            int r6 = r2.getColumnCount()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r6 = "' "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r5 = r5.toString()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            android.util.Log.d(r4, r5)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
        L_0x0075:
            boolean r4 = r2.moveToFirst()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            if (r4 == 0) goto L_0x0226
            r4 = 1
            if (r3 != r4) goto L_0x0226
            r3 = 1
            int r3 = r2.getInt(r3)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r0 = r3
            r1 = r19
            r1.j = r0     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r3 = 2
            int r3 = r2.getInt(r3)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r0 = r3
            r1 = r19
            r1.g = r0     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r3 = 3
            int r3 = r2.getInt(r3)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r0 = r3
            r1 = r19
            r1.h = r0     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r3 = 4
            int r3 = r2.getInt(r3)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r0 = r3
            r1 = r19
            r1.i = r0     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r3 = 8
            java.lang.String r3 = r2.getString(r3)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r0 = r3
            r1 = r19
            r1.k = r0     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r3 = 9
            java.lang.String r3 = r2.getString(r3)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r0 = r3
            r1 = r19
            r1.l = r0     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r0 = r19
            android.widget.TextView r0 = r0.f1038b     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r3 = r0
            java.lang.String[] r4 = com.cyberandsons.tcmaidtrial.misc.dh.am     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r0 = r19
            int r0 = r0.j     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r5 = r0
            r4 = r4[r5]     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r3.setText(r4)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r0 = r19
            android.widget.TextView r0 = r0.c     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r3 = r0
            r4 = 5
            java.lang.String r4 = r2.getString(r4)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r3.setText(r4)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r0 = r19
            android.widget.TextView r0 = r0.d     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r3 = r0
            r4 = 6
            java.lang.String r4 = r2.getString(r4)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r3.setText(r4)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r0 = r19
            int r0 = r0.h     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r3 = r0
            float r3 = (float) r3     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r0 = r19
            int r0 = r0.g     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r4 = r0
            float r4 = (float) r4     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            float r3 = r3 / r4
            r4 = 1120403456(0x42c80000, float:100.0)
            float r3 = r3 * r4
            r0 = r19
            android.widget.TextView r0 = r0.e     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r4 = r0
            java.lang.String r5 = "(%d correct out of %d) - %.1f%% Correct\nElapsed Time: %s"
            r6 = 4
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r7 = 0
            r0 = r19
            int r0 = r0.h     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r8 = r0
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r6[r7] = r8     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r7 = 1
            r0 = r19
            int r0 = r0.g     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r8 = r0
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r6[r7] = r8     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r7 = 2
            java.lang.Float r3 = java.lang.Float.valueOf(r3)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r6[r7] = r3     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r3 = 3
            r0 = r19
            int r0 = r0.i     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r7 = r0
            int r7 = java.lang.Math.abs(r7)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            long r7 = (long) r7     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r9 = 60
            long r9 = r7 / r9
            r11 = 60
            long r11 = r9 % r11
            long r9 = r9 - r11
            r13 = 60
            long r9 = r9 / r13
            r13 = 24
            long r13 = r9 % r13
            long r9 = r9 - r13
            r15 = 24
            long r9 = r9 / r15
            java.lang.String r15 = ""
            r16 = 0
            int r16 = (r9 > r16 ? 1 : (r9 == r16 ? 0 : -1))
            if (r16 <= 0) goto L_0x01a4
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r7.<init>()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r8 = " days and  "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.StringBuilder r7 = r7.append(r13)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r8 = " hours"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r7 = r7.toString()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
        L_0x0164:
            r6[r3] = r7     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r3 = java.lang.String.format(r5, r6)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r4.setText(r3)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r3 = 7
            java.lang.String r3 = r2.getString(r3)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r4 = ";"
            java.lang.String[] r3 = r3.split(r4)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r4.<init>()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r0 = r19
            boolean r0 = r0.m     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r5 = r0
            r6 = 0
            r18 = r6
            r6 = r5
            r5 = r18
        L_0x0188:
            int r7 = r3.length     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            if (r5 >= r7) goto L_0x021a
            r7 = r3[r5]     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r7 = r7.trim()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            if (r6 == 0) goto L_0x0208
            r0 = r19
            boolean r0 = r0.n     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r6 = r0
            r18 = r7
            r7 = r6
            r6 = r18
        L_0x019d:
            r4.append(r6)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            int r5 = r5 + 1
            r6 = r7
            goto L_0x0188
        L_0x01a4:
            r9 = 0
            int r9 = (r13 > r9 ? 1 : (r13 == r9 ? 0 : -1))
            if (r9 <= 0) goto L_0x01c8
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r7.<init>()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.StringBuilder r7 = r7.append(r13)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r8 = " hours and "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r8 = " minutes"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r7 = r7.toString()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            goto L_0x0164
        L_0x01c8:
            r9 = 0
            int r9 = (r11 > r9 ? 1 : (r11 == r9 ? 0 : -1))
            if (r9 <= 0) goto L_0x01ed
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r9.<init>()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.StringBuilder r9 = r9.append(r11)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r10 = " minutes and "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.StringBuilder r7 = r9.append(r7)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r8 = " seconds"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r7 = r7.toString()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            goto L_0x0164
        L_0x01ed:
            r9 = 0
            int r9 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r9 <= 0) goto L_0x024b
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r9.<init>()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.StringBuilder r7 = r9.append(r7)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r8 = " seconds"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r7 = r7.toString()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            goto L_0x0164
        L_0x0208:
            java.lang.String r8 = "\n%s"
            r9 = 1
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r10 = 0
            r9[r10] = r7     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            java.lang.String r7 = java.lang.String.format(r8, r9)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r18 = r7
            r7 = r6
            r6 = r18
            goto L_0x019d
        L_0x021a:
            r0 = r19
            android.widget.TextView r0 = r0.f     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r3 = r0
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
            r3.setText(r4)     // Catch:{ SQLException -> 0x0244, all -> 0x023d }
        L_0x0226:
            if (r2 == 0) goto L_0x022b
            r2.close()
        L_0x022b:
            return
        L_0x022c:
            r2 = move-exception
        L_0x022d:
            com.cyberandsons.tcmaidtrial.a.q.a(r2)     // Catch:{ all -> 0x0236 }
            if (r3 == 0) goto L_0x022b
            r3.close()
            goto L_0x022b
        L_0x0236:
            r2 = move-exception
        L_0x0237:
            if (r3 == 0) goto L_0x023c
            r3.close()
        L_0x023c:
            throw r2
        L_0x023d:
            r3 = move-exception
            r18 = r3
            r3 = r2
            r2 = r18
            goto L_0x0237
        L_0x0244:
            r3 = move-exception
            r18 = r3
            r3 = r2
            r2 = r18
            goto L_0x022d
        L_0x024b:
            r7 = r15
            goto L_0x0164
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.quiz.QuizHistoryDetail.a():void");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }
}
