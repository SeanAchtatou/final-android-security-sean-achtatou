package com.cyberandsons.tcmaidtrial.quiz;

import android.content.Intent;
import android.view.View;

final class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ QuizIntroduction f1063a;

    d(QuizIntroduction quizIntroduction) {
        this.f1063a = quizIntroduction;
    }

    public final void onClick(View view) {
        QuizIntroduction quizIntroduction = this.f1063a;
        quizIntroduction.startActivityForResult(new Intent(quizIntroduction, QuizController.class), 1350);
    }
}
