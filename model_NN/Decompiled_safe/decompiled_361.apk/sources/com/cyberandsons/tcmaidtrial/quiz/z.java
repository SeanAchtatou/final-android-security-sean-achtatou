package com.cyberandsons.tcmaidtrial.quiz;

import android.content.DialogInterface;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.b.a;
import com.cyberandsons.tcmaidtrial.misc.dh;
import org.acra.ErrorReporter;

final class z implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ String f1087a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ String f1088b;
    private /* synthetic */ int c = 0;
    private /* synthetic */ RunQuizTab d;

    z(RunQuizTab runQuizTab, String str, String str2) {
        this.d = runQuizTab;
        this.f1087a = str;
        this.f1088b = str2;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        ErrorReporter.a().a("myVariable", String.format("Area = [%d] %s\nType = [%d] %s\nCategory = [%d] %s", Integer.valueOf(TcmAid.bp), dh.am[TcmAid.bp].toLowerCase(), Integer.valueOf(TcmAid.bq), TcmAid.br, Integer.valueOf(TcmAid.bs), TcmAid.bt));
        ErrorReporter.a().handleSilentException(new a("RQT:sendSilentExceptionAndAdviseUser( " + this.f1087a + " ) = " + this.f1088b + " )"));
        TcmAid.bs = -1;
        TcmAid.bq = -1;
        TcmAid.bw = -1;
        TcmAid.bx.setCurrentTab(this.c);
        TcmAid.bx.getTabWidget().postInvalidate();
        TcmAid.bx.postInvalidate();
    }
}
