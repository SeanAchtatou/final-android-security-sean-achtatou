package com.cyberandsons.tcmaidtrial.quiz;

import android.content.Context;
import android.database.Cursor;
import android.widget.AlphabetIndexer;
import android.widget.SectionIndexer;
import android.widget.SimpleCursorAdapter;
import com.cyberandsons.tcmaidtrial.C0000R;

final class c extends SimpleCursorAdapter implements SectionIndexer {

    /* renamed from: a  reason: collision with root package name */
    private AlphabetIndexer f1062a;

    public c(Context context, Cursor cursor, String[] strArr, int[] iArr, String str) {
        super(context, C0000R.layout.list_row, cursor, strArr, iArr);
        setViewBinder(new ai(this));
        this.f1062a = new AlphabetIndexer(cursor, cursor.getColumnIndexOrThrow(str), "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    }

    public final int getPositionForSection(int i) {
        return this.f1062a.getPositionForSection(i);
    }

    public final int getSectionForPosition(int i) {
        return this.f1062a.getSectionForPosition(i);
    }

    public final Object[] getSections() {
        return this.f1062a.getSections();
    }
}
