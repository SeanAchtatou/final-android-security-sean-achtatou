package com.cyberandsons.tcmaidtrial.quiz;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

final class j extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ QuizController f1070a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(QuizController quizController, Context context, int i, String str) {
        super(context);
        this.f1070a = quizController;
        ImageView imageView = new ImageView(context);
        imageView.setImageResource(i);
        TextView textView = new TextView(context);
        textView.setText(str);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setGravity(1);
        setOrientation(1);
        addView(imageView);
        addView(textView);
    }
}
