package com.cyberandsons.tcmaidtrial.quiz;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.e;
import com.cyberandsons.tcmaidtrial.a.f;
import com.cyberandsons.tcmaidtrial.a.g;
import com.cyberandsons.tcmaidtrial.a.h;
import com.cyberandsons.tcmaidtrial.a.i;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.a.r;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.util.ArrayList;

public final class t {
    boolean A;
    private boolean B;

    /* renamed from: a  reason: collision with root package name */
    int f1080a;

    /* renamed from: b  reason: collision with root package name */
    int f1081b;
    int c;
    int d;
    int e;
    int f;
    int g;
    int h;
    int i;
    int j;
    boolean k;
    String l;
    f m;
    r n;
    g o;
    e p;
    i q;
    h r;
    ArrayList s;
    ArrayList t;
    ArrayList u;
    long v;
    int w;
    boolean x;
    SQLiteDatabase y;
    boolean z = true;

    public t() {
        this.A = !this.z;
    }

    public t(int i2, boolean z2, boolean z3, SQLiteDatabase sQLiteDatabase, int i3) {
        this.A = !this.z;
        this.f1080a = i3;
        this.f1081b = i2;
        this.x = z2;
        this.B = z3;
        this.y = sQLiteDatabase;
        this.c = a();
        this.s = new ArrayList(this.c);
        this.t = new ArrayList();
        this.u = new ArrayList();
        for (int i4 = 0; i4 < this.c; i4++) {
            switch (TcmAid.bp) {
                case 0:
                    this.m = (f) RunQuizTab.f1043a.get(i4);
                    this.s.add(Boolean.valueOf(this.A));
                    break;
                case 1:
                    this.n = (r) RunQuizTab.f1044b.get(i4);
                    this.s.add(Boolean.valueOf(this.A));
                    break;
                case 2:
                    this.o = (g) RunQuizTab.c.get(i4);
                    this.s.add(Boolean.valueOf(this.A));
                    break;
                case 3:
                    this.p = (e) RunQuizTab.e.get(i4);
                    this.s.add(Boolean.valueOf(this.A));
                    break;
                case 4:
                    this.s.add(Boolean.valueOf(this.A));
                    break;
                case 5:
                    this.r = (h) RunQuizTab.h.get(i4);
                    this.s.add(Boolean.valueOf(this.A));
                    break;
                default:
                    this.s.add(Boolean.valueOf(this.A));
                    break;
            }
        }
        this.d = 0;
        this.j = 0;
    }

    public final String a(boolean z2, boolean z3) {
        String str = null;
        try {
            switch (TcmAid.bp) {
                case 0:
                    this.m = (f) RunQuizTab.f1043a.get(this.g);
                    str = this.m.b();
                    break;
                case 1:
                    this.n = (r) RunQuizTab.f1044b.get(this.g);
                    str = this.n.b();
                    break;
                case 2:
                    if (!z2 || this.e == this.g) {
                        this.o = (g) RunQuizTab.c.get(this.g);
                    } else {
                        this.o = (g) RunQuizTab.d.get(this.g);
                    }
                    str = this.o.b();
                    break;
                case 3:
                    if (z2) {
                        if (this.e != this.g) {
                            this.p = (e) RunQuizTab.f.get(this.g);
                            str = this.p.b();
                            break;
                        }
                    }
                    this.p = (e) RunQuizTab.e.get(this.g);
                    str = this.p.b();
                case 4:
                    this.q = (i) RunQuizTab.g.get(this.g);
                    str = this.q.b();
                    if (!z3 && this.q.h()) {
                        str = str + " - " + this.q.g();
                        break;
                    }
                case 5:
                    this.r = (h) RunQuizTab.h.get(this.g);
                    str = this.r.b();
                    break;
            }
        } catch (IndexOutOfBoundsException e2) {
            str = "";
        }
        String str2 = " ";
        if (this.e == this.g) {
            str2 = " ";
        }
        return str2 + "A. " + str;
    }

    public final String b(boolean z2, boolean z3) {
        String str = null;
        try {
            switch (TcmAid.bp) {
                case 0:
                    this.m = (f) RunQuizTab.f1043a.get(this.h);
                    str = this.m.b();
                    break;
                case 1:
                    this.n = (r) RunQuizTab.f1044b.get(this.h);
                    str = this.n.b();
                    break;
                case 2:
                    if (!z2 || this.e == this.h) {
                        this.o = (g) RunQuizTab.c.get(this.h);
                    } else {
                        this.o = (g) RunQuizTab.d.get(this.h);
                    }
                    str = this.o.b();
                    break;
                case 3:
                    if (z2) {
                        if (this.e != this.h) {
                            this.p = (e) RunQuizTab.f.get(this.h);
                            str = this.p.b();
                            break;
                        }
                    }
                    this.p = (e) RunQuizTab.e.get(this.h);
                    str = this.p.b();
                case 4:
                    this.q = (i) RunQuizTab.g.get(this.h);
                    str = this.q.b();
                    if (!z3 && this.q.h()) {
                        str = str + " - " + this.q.g();
                        break;
                    }
                case 5:
                    this.r = (h) RunQuizTab.h.get(this.h);
                    str = this.r.b();
                    break;
            }
        } catch (IndexOutOfBoundsException e2) {
            str = "";
        }
        String str2 = " ";
        if (this.e == this.h) {
            str2 = " ";
        }
        return str2 + "B. " + str;
    }

    public final String c(boolean z2, boolean z3) {
        String str = null;
        try {
            switch (TcmAid.bp) {
                case 0:
                    this.m = (f) RunQuizTab.f1043a.get(this.i);
                    str = this.m.b();
                    break;
                case 1:
                    this.n = (r) RunQuizTab.f1044b.get(this.i);
                    str = this.n.b();
                    break;
                case 2:
                    if (!z2 || this.e == this.i) {
                        this.o = (g) RunQuizTab.c.get(this.i);
                    } else {
                        this.o = (g) RunQuizTab.d.get(this.i);
                    }
                    str = this.o.b();
                    break;
                case 3:
                    if (z2) {
                        if (this.e != this.i) {
                            this.p = (e) RunQuizTab.f.get(this.i);
                            str = this.p.b();
                            break;
                        }
                    }
                    this.p = (e) RunQuizTab.e.get(this.i);
                    str = this.p.b();
                case 4:
                    this.q = (i) RunQuizTab.g.get(this.i);
                    str = this.q.b();
                    if (!z3 && this.q.h()) {
                        str = str + " - " + this.q.g();
                        break;
                    }
                case 5:
                    this.r = (h) RunQuizTab.h.get(this.i);
                    str = this.r.b();
                    break;
            }
        } catch (IndexOutOfBoundsException e2) {
            str = "";
        }
        String str2 = " ";
        if (this.e == this.i) {
            str2 = " ";
        }
        return str2 + "C. " + str;
    }

    public final int a() {
        switch (TcmAid.bp) {
            case 0:
                return RunQuizTab.f1043a.size();
            case 1:
                return RunQuizTab.f1044b.size();
            case 2:
                return RunQuizTab.c.size();
            case 3:
                return RunQuizTab.e.size();
            case 4:
                int size = RunQuizTab.g.size();
                if (this.x) {
                    return 20;
                }
                return size;
            case 5:
                return RunQuizTab.h.size();
            default:
                return 0;
        }
    }

    public final String b() {
        StringBuffer stringBuffer = new StringBuffer();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.t.size()) {
                return stringBuffer.toString();
            }
            if (i3 > 0) {
                stringBuffer.append(";");
            }
            stringBuffer.append((String) this.t.get(i3));
            i2 = i3 + 1;
        }
    }

    public final String c() {
        StringBuffer stringBuffer = new StringBuffer();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.u.size()) {
                break;
            }
            if (i3 > 0) {
                stringBuffer.append(",");
            }
            stringBuffer.append(((Integer) this.u.get(i3)).toString());
            i2 = i3 + 1;
        }
        String str = "";
        switch (TcmAid.bp) {
            case 0:
                str = q.a("auricular", "_id", stringBuffer.toString());
                break;
            case 1:
                str = q.a("zangfupathology", "_id", stringBuffer.toString());
                break;
            case 2:
                str = q.a("materiamedica", "_id", stringBuffer.toString());
                break;
            case 3:
                str = q.a("formulas", "_id", stringBuffer.toString());
                break;
            case 4:
                str = q.a("pointslocation", "_id", stringBuffer.toString());
                break;
            case 5:
                str = q.a("pulsediag", "_id", stringBuffer.toString());
                break;
        }
        if (dh.B) {
            Log.i("RQ:getMissed_IdItems()", str);
        }
        return str;
    }
}
