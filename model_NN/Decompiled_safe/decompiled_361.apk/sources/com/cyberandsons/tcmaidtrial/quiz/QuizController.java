package com.cyberandsons.tcmaidtrial.quiz;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.TabHost;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class QuizController extends TabActivity implements TabHost.OnTabChangeListener {

    /* renamed from: a  reason: collision with root package name */
    public static int f1035a = -1;

    /* renamed from: b  reason: collision with root package name */
    private TabHost.TabSpec f1036b;
    private TabHost.TabSpec c;
    private TabHost.TabSpec d;
    private TabHost.TabSpec e;
    private int f = -1;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (TcmAid.cT < dh.f947b && TcmAid.cS < dh.f947b) {
            setRequestedOrientation(1);
        }
        setContentView((int) C0000R.layout.quiztab);
        TabHost tabHost = (TabHost) findViewById(16908306);
        TcmAid.bx = tabHost;
        tabHost.setOnTabChangedListener(this);
        this.f1036b = TcmAid.bx.newTabSpec("Select Quiz");
        this.c = TcmAid.bx.newTabSpec("Specifics");
        this.d = TcmAid.bx.newTabSpec("Run Quiz");
        this.e = TcmAid.bx.newTabSpec("Quiz History");
        this.f1036b.setIndicator(new j(this, this, C0000R.drawable.index_cards, "Select Quiz"));
        this.c.setIndicator(new j(this, this, C0000R.drawable.gameplan, "Specifics"));
        this.d.setIndicator(new j(this, this, C0000R.drawable.runner, "Run Quiz"));
        this.e.setIndicator(new j(this, this, C0000R.drawable.trophy, "Quiz History"));
        this.f1036b.setContent(new Intent(this, SelectQuizTab.class));
        this.c.setContent(new Intent(this, SpecificsTab.class));
        this.d.setContent(new Intent(this, RunQuizTab.class));
        this.e.setContent(new Intent(this, QuizHistoryTab.class));
        TcmAid.bx.addTab(this.f1036b);
        TcmAid.bx.addTab(this.c);
        TcmAid.bx.addTab(this.d);
        TcmAid.bx.addTab(this.e);
        for (int i = 0; i < TcmAid.bx.getTabWidget().getChildCount(); i++) {
            TcmAid.bx.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#7392B5"));
        }
        TcmAid.bx.setCurrentTab(0);
        TcmAid.bx.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor("#4E4E9C"));
        TcmAid.bp = -1;
        TcmAid.bs = -1;
        TcmAid.bq = -1;
    }

    public void onDestroy() {
        TcmAid.by.clear();
        TcmAid.j.clear();
        TcmAid.bz.clear();
        TcmAid.bA.clear();
        TcmAid.bB.clear();
        TcmAid.bC.clear();
        TcmAid.bD.clear();
        TcmAid.bE.clear();
        TcmAid.bF.clear();
        TcmAid.bG.clear();
        TcmAid.z.clear();
        TcmAid.bH.clear();
        TcmAid.bI.clear();
        TcmAid.bJ.clear();
        TcmAid.bK.clear();
        TcmAid.bL.clear();
        TcmAid.bM.clear();
        TcmAid.bN.clear();
        TcmAid.bO.clear();
        TcmAid.ab.clear();
        TcmAid.bP.clear();
        TcmAid.bQ.clear();
        TcmAid.bR.clear();
        TcmAid.bS.clear();
        TcmAid.bT.clear();
        TcmAid.bU.clear();
        TcmAid.bV.clear();
        TcmAid.bW.clear();
        TcmAid.O.clear();
        TcmAid.bX.clear();
        TcmAid.bY.clear();
        TcmAid.bZ.clear();
        TcmAid.ca.clear();
        TcmAid.cb.clear();
        TcmAid.cc.clear();
        TcmAid.cd.clear();
        TcmAid.ce.clear();
        TcmAid.am.clear();
        TcmAid.cf.clear();
        TcmAid.cg.clear();
        TcmAid.ch.clear();
        TcmAid.ci.clear();
        TcmAid.cj.clear();
        TcmAid.ck.clear();
        TcmAid.cl.clear();
        TcmAid.cm.clear();
        TcmAid.cn.clear();
        TcmAid.co.clear();
        TcmAid.cp.clear();
        TcmAid.cq.clear();
        TcmAid.cr.clear();
        TcmAid.cs.clear();
        TcmAid.ct.clear();
        super.onDestroy();
    }

    public void onTabChanged(String str) {
        this.f = -1;
        String str2 = "";
        if (dh.B) {
            Log.i("QC:onTabChanged()", "tabId = " + str);
            Log.i("QC:onTabChanged()", "TcmAid.qyTestArea = " + Integer.toString(TcmAid.bp));
            Log.i("QC:onTabChanged()", "TcmAid.qyTestCategory = " + Integer.toString(TcmAid.bs));
            Log.i("QC:onTabChanged()", "TcmAid.qyTestType = " + Integer.toString(TcmAid.bq));
        }
        if (str.compareTo("Select Quiz") == 0) {
            for (int i = 0; i < TcmAid.bx.getTabWidget().getChildCount(); i++) {
                TcmAid.bx.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#7392B5"));
            }
            TcmAid.bx.getTabWidget().getChildAt(TcmAid.bx.getCurrentTab()).setBackgroundColor(Color.parseColor("#4E4E9C"));
        } else if (str.compareTo("Specifics") == 0) {
            if (TcmAid.bw < 0 || TcmAid.bq == 0) {
                str2 = TcmAid.bq == 0 ? "You must choose a quiz type from the dropdown list before choosing quiz 'Specifics' tab." : "You must finish choosing 'Select Quiz' tab before choosing quiz 'Specifics' tab.";
                this.f = 0;
            }
        } else if (str.compareTo("Run Quiz") == 0) {
            if (TcmAid.bw <= 0 || TcmAid.bq == 0 || TcmAid.bs <= 0 || TcmAid.bw == -1) {
                if (TcmAid.bs <= 0) {
                    str2 = "You must finish choosing quiz 'Specifics' tab before running quiz.";
                    this.f = 1;
                } else {
                    str2 = "You must finish choosing 'Select Quiz' and quiz 'Specifics' tabs before running quiz.";
                    this.f = 0;
                }
            } else if (TcmAid.bw == 0) {
                str2 = "You must finish choosing quiz 'Specifics' tab before running quiz.";
                this.f = 1;
            }
        }
        if (this.f != -1) {
            TcmAid.bx.setCurrentTab(this.f);
            TcmAid.bx.postInvalidate();
            AlertDialog create = new AlertDialog.Builder(this).create();
            create.setTitle("Attention:");
            create.setMessage(str2);
            create.setButton("OK", new ah(this));
            create.show();
            return;
        }
        for (int i2 = 0; i2 < TcmAid.bx.getTabWidget().getChildCount(); i2++) {
            TcmAid.bx.getTabWidget().getChildAt(i2).setBackgroundColor(Color.parseColor("#7392B5"));
        }
        TcmAid.bx.getTabWidget().getChildAt(TcmAid.bx.getCurrentTab()).setBackgroundColor(Color.parseColor("#4E4E9C"));
    }
}
