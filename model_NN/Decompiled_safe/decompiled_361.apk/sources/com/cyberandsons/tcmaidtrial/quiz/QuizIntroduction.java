package com.cyberandsons.tcmaidtrial.quiz;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class QuizIntroduction extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Button f1041a;

    /* renamed from: b  reason: collision with root package name */
    private TextView f1042b;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (TcmAid.cT < dh.f947b && TcmAid.cS < dh.f947b) {
            setRequestedOrientation(1);
        }
        setContentView((int) C0000R.layout.quizintro);
        this.f1042b = (TextView) findViewById(C0000R.id.tc_label);
        this.f1042b.setText("Quiz Yourself");
        this.f1042b.setGravity(1);
        this.f1041a = (Button) findViewById(C0000R.id.beg_button);
        this.f1041a.setGravity(1);
        this.f1041a.setOnClickListener(new d(this));
        TcmAid.bp = -1;
        TcmAid.bw = -1;
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
