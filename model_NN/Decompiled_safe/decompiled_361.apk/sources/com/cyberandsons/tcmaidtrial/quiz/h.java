package com.cyberandsons.tcmaidtrial.quiz;

import android.content.DialogInterface;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.widget.ListView;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.q;

final class h implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ QuizHistoryTab f1067a;

    h(QuizHistoryTab quizHistoryTab) {
        this.f1067a = quizHistoryTab;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        int i2;
        try {
            this.f1067a.t.execSQL("DELETE FROM userDB.quizsessions ");
        } catch (SQLException e) {
            q.a(e);
        }
        this.f1067a.stopManagingCursor(this.f1067a.s);
        if (this.f1067a.s != null) {
            this.f1067a.s.close();
            SQLiteCursor unused = this.f1067a.s = null;
        }
        SQLiteCursor unused2 = this.f1067a.s = this.f1067a.b();
        this.f1067a.setListAdapter(this.f1067a.c());
        int a2 = q.a("SELECT MAX(userDB.quizsessions._id) FROM userDB.quizsessions ", this.f1067a.t);
        this.f1067a.f1039a.setVisibility(a2 == 0 ? 4 : 0);
        ListView listView = this.f1067a.f1040b;
        if (a2 == 0) {
            i2 = 4;
        } else {
            i2 = 0;
        }
        listView.setVisibility(i2);
        if (a2 > 0) {
            TcmAid.bx.setCurrentTab(0);
            TcmAid.bx.getTabWidget().postInvalidate();
            TcmAid.bx.postInvalidate();
        }
    }
}
