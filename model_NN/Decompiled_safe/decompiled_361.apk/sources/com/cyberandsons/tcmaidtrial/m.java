package com.cyberandsons.tcmaidtrial;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;
import com.cyberandsons.tcmaidtrial.areas.SearchArea;
import org.acra.ErrorReporter;

final class m extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private ProgressDialog f842a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f843b;
    private /* synthetic */ TcmAid c;

    /* synthetic */ m(TcmAid tcmAid) {
        this(tcmAid, (byte) 0);
    }

    private m(TcmAid tcmAid, byte b2) {
        this.c = tcmAid;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        Integer num = (Integer) obj;
        this.f842a.dismiss();
        if (this.f843b) {
            Toast makeText = Toast.makeText(this.c, (int) C0000R.string.search_exception, 1);
            makeText.setGravity(17, makeText.getXOffset() / 2, makeText.getYOffset() / 2);
            makeText.show();
            return;
        }
        try {
            TcmAid.cP = num.toString() + " TCM Items";
            this.c.startActivityForResult(new Intent(this.c, SearchArea.class), 1350);
        } catch (IllegalArgumentException e) {
            AlertDialog create = new AlertDialog.Builder(this.c).create();
            create.setTitle("Attention:");
            create.setMessage(this.c.getString(C0000R.string.search_results_exception));
            create.setButton("OK", new a(this));
            create.show();
        }
    }

    public final void onPreExecute() {
        this.f842a = ProgressDialog.show(this.c, "", "Searching ...", true, false);
        this.f842a.setProgressStyle(0);
        this.f843b = false;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Integer doInBackground(String... strArr) {
        int i;
        try {
            i = TcmAid.a(strArr[0], this.c.f160a, this.c.dr);
        } catch (IllegalStateException e) {
            this.f843b = true;
            ErrorReporter.a().a("myVariable", "search tainted");
            ErrorReporter.a().handleSilentException(new Exception("TCM:doInBackground()"));
            i = 0;
        }
        return Integer.valueOf(i);
    }
}
