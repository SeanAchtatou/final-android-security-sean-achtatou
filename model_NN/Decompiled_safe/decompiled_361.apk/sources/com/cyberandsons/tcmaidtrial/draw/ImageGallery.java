package com.cyberandsons.tcmaidtrial.draw;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class ImageGallery extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Gallery f681a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ImageView f682b;
    /* access modifiers changed from: private */
    public TextView c;
    /* access modifiers changed from: private */
    public String[] d;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.imagegallery);
        this.d = TcmAid.U.split(",");
        this.f682b = (ImageView) findViewById(C0000R.id.ImageView01);
        this.c = (TextView) findViewById(C0000R.id.itemname);
        Bitmap b2 = dh.b(dh.a(this.d[0]), "herbs");
        if (b2 == null) {
            this.f682b.setImageResource(C0000R.drawable.nopicture);
        } else {
            this.f682b.setImageDrawable(dh.a(b2, TcmAid.cS - 0, TcmAid.cS - 0, getWindow()));
            this.c.setText(this.d[0]);
        }
        this.f681a = (Gallery) findViewById(C0000R.id.examplegallery);
        this.f681a.setAdapter((SpinnerAdapter) new c(this, this));
        this.f681a.setOnItemClickListener(new d(this));
    }
}
