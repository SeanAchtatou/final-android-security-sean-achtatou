package com.cyberandsons.tcmaidtrial.draw;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.view.View;
import com.cyberandsons.tcmaidtrial.a.b;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.util.Iterator;

public final class a extends View {

    /* renamed from: a  reason: collision with root package name */
    private Canvas f683a;

    /* renamed from: b  reason: collision with root package name */
    private Paint f684b;
    private Bitmap c;
    private Bitmap d;
    private int e;
    private int f;
    private boolean g = true;
    private boolean h;

    public a(Context context, Bitmap bitmap, int i, int i2) {
        super(context);
        this.h = !this.g;
        this.d = bitmap;
        this.e = i;
        this.f = i2;
        this.c = Bitmap.createBitmap(this.f, this.e, Bitmap.Config.ARGB_8888);
        this.f684b = new Paint();
        this.f684b.setDither(this.g);
        this.f684b.setAntiAlias(this.g);
        this.f683a = new Canvas(this.c);
        this.f683a.drawBitmap(this.d, (Rect) null, new Rect(0, 0, this.f, this.e), (Paint) null);
    }

    public final Bitmap a() {
        return this.c;
    }

    public final void a(int i, String str, b[] bVarArr, boolean z) {
        int i2;
        int i3;
        int i4;
        boolean z2;
        int i5;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        boolean z3 = this.h;
        int i9 = 0;
        while (i6 < bVarArr.length) {
            b bVar = bVarArr[i6];
            if (i == bVar.f163a) {
                i3 = i8;
                i4 = i9;
                z2 = z3;
                i5 = i8;
            } else {
                if (i9 != 0 || !bVar.x.equalsIgnoreCase("sr")) {
                    i2 = i9;
                } else {
                    i2 = i9 + 1;
                    z3 = this.g;
                }
                a(i, bVar, str);
                if (bVar.f163a != -2) {
                    a(i, bVar, z, z3, str);
                }
                i3 = i8 + 1;
                i4 = i2;
                z2 = this.h;
                i5 = i7;
            }
            i6++;
            i7 = i5;
            i8 = i3;
            i9 = i4;
            z3 = z2;
        }
        b bVar2 = bVarArr[i7];
        a(i, bVar2, str);
        a(i, bVar2, z, z3, str);
    }

    public final void a(String str, b bVar, boolean z, boolean z2) {
        bVar.f163a = 0;
        a(0, bVar, str);
        a(0, bVar, z2, (!bVar.x.equalsIgnoreCase("sr") || !z) ? this.h : this.g, str);
    }

    private void a(int i, b bVar, String str) {
        if (bVar.D != null && bVar.D.size() > 0) {
            if (i == bVar.f163a) {
                a(str, (float) bVar.A, (float) bVar.B, (float) bVar.C);
                this.f684b.setStrokeWidth(2.25f);
            } else {
                this.f684b.setStyle(Paint.Style.STROKE);
                this.f684b.setColor(Color.argb(255, bVar.A, bVar.B, bVar.C));
                this.f684b.setStrokeWidth(1.75f);
            }
            float[] fArr = new float[(bVar.D.size() * 2)];
            Iterator it = bVar.D.iterator();
            int i2 = 0;
            while (it.hasNext()) {
                Point point = (Point) it.next();
                int i3 = i2 + 1;
                fArr[i2] = (float) point.y;
                i2 = i3 + 1;
                fArr[i3] = (float) point.x;
            }
            this.f683a.drawLines(fArr, this.f684b);
        }
    }

    public final void a(String str, b bVar) {
        float f2;
        float f3;
        boolean z = this.h;
        if (!z || bVar.f163a != 0) {
            this.f684b.setStyle(Paint.Style.FILL);
            this.f684b.setColor(Color.argb(255, bVar.A, bVar.B, bVar.C));
            this.f684b.setStrokeWidth(2.0f);
        } else {
            a(str, (float) bVar.A, (float) bVar.B, (float) bVar.C);
        }
        if (bVar.x.equalsIgnoreCase("sc")) {
            this.f683a.drawCircle(((float) bVar.g) - 0.0f, ((float) (bVar.f - 10)) - 0.0f, 5.0f, this.f684b);
            if (bVar.h > 0) {
                this.f683a.drawCircle(((float) bVar.i) - 0.0f, ((float) (bVar.h - 10)) - 0.0f, 5.0f, this.f684b);
            }
            if (bVar.j > 0) {
                this.f683a.drawCircle(((float) bVar.k) - 0.0f, ((float) (bVar.j - 10)) - 0.0f, 5.0f, this.f684b);
            }
            if (bVar.l > 0) {
                this.f683a.drawCircle(((float) bVar.m) - 0.0f, ((float) (bVar.l - 10)) - 0.0f, 5.0f, this.f684b);
            }
            if (bVar.n > 0) {
                this.f683a.drawCircle(((float) bVar.o) - 0.0f, ((float) (bVar.n - 10)) - 0.0f, 5.0f, this.f684b);
            }
        } else if (bVar.x.equalsIgnoreCase("tc")) {
            this.f683a.drawCircle(((float) bVar.g) - 0.0f, ((float) (bVar.f - 10)) - 0.0f, 2.0f, this.f684b);
            if (bVar.h > 0) {
                this.f683a.drawCircle(((float) bVar.i) - 0.0f, ((float) (bVar.h - 10)) - 0.0f, 2.0f, this.f684b);
            }
            if (bVar.j > 0) {
                this.f683a.drawCircle(((float) bVar.k) - 0.0f, ((float) (bVar.j - 10)) - 0.0f, 2.0f, this.f684b);
            }
            if (bVar.l > 0) {
                this.f683a.drawCircle(((float) bVar.m) - 0.0f, ((float) (bVar.l - 10)) - 0.0f, 2.0f, this.f684b);
            }
            if (bVar.n > 0) {
                this.f683a.drawCircle(((float) bVar.o) - 0.0f, ((float) (bVar.n - 10)) - 0.0f, 2.0f, this.f684b);
            }
        } else if (bVar.x.equalsIgnoreCase("ha")) {
            this.f684b.setColor(Color.argb(255, bVar.A, bVar.B, bVar.C));
            this.f683a.drawCircle(((float) bVar.g) - 0.0f, ((float) (bVar.f - 10)) - 0.0f, 5.0f, this.f684b);
            this.f684b.setColor(Color.argb(100, bVar.A, bVar.B, bVar.C));
            if (bVar.h > 0) {
                this.f683a.drawCircle(((float) bVar.i) - 0.0f, ((float) (bVar.h - 10)) - 0.0f, 5.0f, this.f684b);
            }
            if (bVar.j > 0) {
                this.f683a.drawCircle(((float) bVar.k) - 0.0f, ((float) (bVar.j - 10)) - 0.0f, 5.0f, this.f684b);
            }
            if (bVar.l > 0) {
                this.f683a.drawCircle(((float) bVar.m) - 0.0f, ((float) (bVar.l - 10)) - 0.0f, 5.0f, this.f684b);
            }
            if (bVar.n > 0) {
                this.f683a.drawCircle(((float) bVar.o) - 0.0f, ((float) (bVar.n - 10)) - 0.0f, 5.0f, this.f684b);
            }
        } else if (bVar.x.equalsIgnoreCase("ss")) {
            if (bVar.y == 0) {
                this.f683a.drawRect(new RectF(((float) bVar.g) - 5.0f, ((float) (bVar.f - 10)) - 5.0f, (((float) bVar.g) - 5.0f) + 10.0f, (((float) (bVar.f - 10)) - 5.0f) + 10.0f), this.f684b);
                if (bVar.h > 0) {
                    this.f683a.drawRect(new RectF(((float) bVar.i) - 5.0f, ((float) (bVar.h - 10)) - 5.0f, (((float) bVar.i) - 5.0f) + 10.0f, (((float) (bVar.h - 10)) - 5.0f) + 10.0f), this.f684b);
                }
                if (bVar.j > 0) {
                    this.f683a.drawRect(new RectF(((float) bVar.k) - 5.0f, ((float) (bVar.j - 10)) - 5.0f, (((float) bVar.k) - 5.0f) + 10.0f, (((float) (bVar.j - 10)) - 5.0f) + 10.0f), this.f684b);
                }
                if (bVar.l > 0) {
                    this.f683a.drawRect(new RectF(((float) bVar.m) - 5.0f, ((float) (bVar.l - 10)) - 5.0f, (((float) bVar.m) - 5.0f) + 10.0f, (((float) (bVar.l - 10)) - 5.0f) + 10.0f), this.f684b);
                }
                if (bVar.n > 0) {
                    this.f683a.drawRect(new RectF(((float) bVar.o) - 5.0f, ((float) (bVar.n - 10)) - 5.0f, (((float) bVar.o) - 5.0f) + 10.0f, (((float) (bVar.n - 10)) - 5.0f) + 10.0f), this.f684b);
                }
            } else if (bVar.y > 0) {
                if (bVar.z.equalsIgnoreCase("vt")) {
                    f2 = (float) bVar.y;
                    f3 = 5.0f;
                } else if (bVar.z.equalsIgnoreCase("hz")) {
                    f3 = (float) bVar.y;
                    f2 = 5.0f;
                } else {
                    f2 = 5.0f;
                    f3 = 5.0f;
                }
                this.f683a.drawRect(new RectF(((float) bVar.g) - 5.0f, ((float) (bVar.f - 10)) - 5.0f, f3 + (((float) bVar.g) - 5.0f), f2 + (((float) (bVar.f - 10)) - 5.0f)), this.f684b);
            }
        } else if (bVar.x.equalsIgnoreCase("dg")) {
            this.f683a.save();
            this.f683a.rotate(20.0f, (float) (bVar.g + 5), (float) (bVar.f - 8));
            this.f683a.drawRect(new RectF((float) bVar.g, (float) (bVar.f - 8), (float) bVar.i, (float) (bVar.h - 5)), this.f684b);
            this.f683a.restore();
        } else if (bVar.x.equalsIgnoreCase("ov")) {
            this.f683a.drawOval(new RectF((float) bVar.g, (float) bVar.f, (float) (bVar.g + 20), (float) bVar.h), this.f684b);
        } else if (bVar.x.equalsIgnoreCase("fl")) {
            this.f684b.setStyle(Paint.Style.STROKE);
            this.f684b.setStrokeWidth(4.0f);
            Path path = new Path();
            Point point = new Point(bVar.g, bVar.f);
            Point point2 = new Point(bVar.m, bVar.l);
            Point point3 = new Point(bVar.i, bVar.h);
            Point point4 = new Point(bVar.k, bVar.j);
            path.moveTo((float) point.x, (float) point.y);
            path.cubicTo((float) point3.x, (float) point3.y, (float) point4.x, (float) point4.y, (float) point2.x, (float) point2.y);
            this.f683a.drawPath(path, this.f684b);
        } else if (bVar.x.equalsIgnoreCase("sr")) {
            this.f684b.setColor(-16777216);
            this.f683a.drawCircle(((float) bVar.g) - 0.0f, ((float) (bVar.f - 10)) - 0.0f, 5.0f, this.f684b);
            if (bVar.h > 0) {
                this.f683a.drawCircle(((float) bVar.i) - 0.0f, ((float) (bVar.h - 10)) - 0.0f, 5.0f, this.f684b);
            }
            if (bVar.j > 0) {
                this.f683a.drawCircle(((float) bVar.k) - 0.0f, ((float) (bVar.j - 10)) - 0.0f, 5.0f, this.f684b);
            }
            if (bVar.l > 0) {
                this.f683a.drawCircle(((float) bVar.m) - 0.0f, ((float) (bVar.l - 10)) - 0.0f, 5.0f, this.f684b);
            }
            if (bVar.n > 0) {
                this.f683a.drawCircle(((float) bVar.o) - 0.0f, ((float) (bVar.n - 10)) - 0.0f, 5.0f, this.f684b);
            }
            a(bVar);
        }
        if (z) {
            if (bVar.f163a == 0) {
                a(str, (float) bVar.A, (float) bVar.B, (float) bVar.C);
            } else {
                this.f684b.setColor(-16777216);
                this.f684b.setStrokeWidth(2.0f);
            }
            this.f683a.drawText(bVar.c, (float) bVar.q, (float) bVar.p, this.f684b);
            float measureText = bVar.q < bVar.g ? this.f684b.measureText(bVar.c) : 0.0f;
            Point point5 = new Point();
            Point point6 = new Point();
            if (((double) bVar.i) == 0.0d) {
                point5.x = bVar.g;
                point5.y = bVar.f;
            } else {
                point5.x = bVar.i;
                point5.y = bVar.h;
            }
            point6.x = (int) (measureText + ((float) bVar.q));
            point6.y = bVar.p + 10;
            this.f683a.drawLine((float) point5.x, (float) point5.y, (float) point6.x, (float) point6.y, this.f684b);
        }
    }

    private void a(int i, b bVar, boolean z, boolean z2, String str) {
        float f2;
        this.f684b.setStrokeWidth(2.0f);
        this.f684b.setStyle(Paint.Style.FILL);
        if (bVar.e.charAt(0) == '+') {
            if (!z || i != bVar.f163a) {
                this.f684b.setStyle(Paint.Style.FILL);
                this.f684b.setColor(Color.argb(255, bVar.A, bVar.B, bVar.C));
                this.f684b.setStrokeWidth(1.0f);
            } else {
                a(str, (float) bVar.A, (float) bVar.B, (float) bVar.C);
            }
            this.f683a.drawCircle(((float) bVar.g) - 0.0f, ((float) bVar.f) - 0.0f, 5.0f, this.f684b);
            String[] split = bVar.e.substring(1).split(",");
            int i2 = 0;
            for (String parseInt : split) {
                i2 += Integer.parseInt(parseInt);
                this.f683a.drawCircle(((float) bVar.g) - 0.0f, (((float) bVar.f) - 0.0f) + ((float) i2), 5.0f, this.f684b);
            }
        } else {
            String str2 = bVar.x;
            if (str2.equalsIgnoreCase("ss")) {
                this.f684b.setColor(-16777216);
                this.f683a.drawRect(new RectF(((float) bVar.g) - 5.0f, ((float) (bVar.f - 10)) - 5.0f, (((float) bVar.g) - 5.0f) + 10.0f, (((float) (bVar.f - 10)) - 5.0f) + 10.0f), this.f684b);
                if (bVar.h > 0) {
                    this.f683a.drawRect(new RectF(((float) bVar.i) - 5.0f, ((float) (bVar.h - 10)) - 5.0f, (((float) bVar.i) - 5.0f) + 10.0f, (((float) (bVar.h - 10)) - 5.0f) + 10.0f), this.f684b);
                }
                if (bVar.j > 0) {
                    this.f683a.drawRect(new RectF(((float) bVar.k) - 5.0f, ((float) (bVar.j - 10)) - 5.0f, (((float) bVar.k) - 5.0f) + 10.0f, (((float) (bVar.j - 10)) - 5.0f) + 10.0f), this.f684b);
                }
                if (bVar.l > 0) {
                    this.f683a.drawRect(new RectF(((float) bVar.m) - 5.0f, ((float) (bVar.l - 10)) - 5.0f, (((float) bVar.m) - 5.0f) + 10.0f, (((float) (bVar.l - 10)) - 5.0f) + 10.0f), this.f684b);
                }
                if (bVar.n > 0) {
                    this.f683a.drawRect(new RectF(((float) bVar.o) - 5.0f, ((float) (bVar.n - 10)) - 5.0f, (((float) bVar.o) - 5.0f) + 10.0f, (((float) (bVar.n - 10)) - 5.0f) + 10.0f), this.f684b);
                }
                if (!z || i != bVar.f163a) {
                    this.f684b.setStyle(Paint.Style.FILL);
                    this.f684b.setColor(Color.argb(255, bVar.A, bVar.B, bVar.C));
                    this.f684b.setStrokeWidth(1.0f);
                } else {
                    a(str, (float) bVar.A, (float) bVar.B, (float) bVar.C);
                }
                this.f683a.drawRect(new RectF(((float) bVar.g) - 5.0f, ((float) (bVar.f - 10)) - 5.0f, (((float) bVar.g) - 5.0f) + 8.0f, (((float) (bVar.f - 10)) - 5.0f) + 8.0f), this.f684b);
                if (bVar.h > 0) {
                    this.f683a.drawRect(new RectF(((float) bVar.i) - 5.0f, ((float) (bVar.h - 10)) - 5.0f, (((float) bVar.i) - 5.0f) + 8.0f, (((float) (bVar.h - 10)) - 5.0f) + 8.0f), this.f684b);
                }
                if (bVar.j > 0) {
                    this.f683a.drawRect(new RectF(((float) bVar.k) - 5.0f, ((float) (bVar.j - 10)) - 5.0f, (((float) bVar.k) - 5.0f) + 8.0f, (((float) (bVar.j - 10)) - 5.0f) + 8.0f), this.f684b);
                }
                if (bVar.l > 0) {
                    this.f683a.drawRect(new RectF(((float) bVar.m) - 5.0f, ((float) (bVar.l - 10)) - 5.0f, (((float) bVar.m) - 5.0f) + 8.0f, (((float) (bVar.l - 10)) - 5.0f) + 8.0f), this.f684b);
                }
                if (bVar.n > 0) {
                    this.f683a.drawRect(new RectF(((float) bVar.o) - 5.0f, ((float) (bVar.n - 10)) - 5.0f, (((float) bVar.o) - 5.0f) + 8.0f, (((float) (bVar.n - 10)) - 5.0f) + 8.0f), this.f684b);
                }
            } else if (str2.equalsIgnoreCase("sr") || str2.equalsIgnoreCase("sc") || str2.equalsIgnoreCase("db")) {
                this.f684b.setColor(-16777216);
                this.f683a.drawCircle(((float) bVar.g) - 0.0f, ((float) bVar.f) - 0.0f, 5.0f, this.f684b);
                if (bVar.h > 0) {
                    this.f683a.drawCircle(((float) bVar.i) - 0.0f, ((float) bVar.h) - 0.0f, 5.0f, this.f684b);
                }
                if (bVar.j > 0) {
                    this.f683a.drawCircle(((float) bVar.k) - 0.0f, ((float) bVar.j) - 0.0f, 5.0f, this.f684b);
                }
                if (bVar.l > 0) {
                    this.f683a.drawCircle(((float) bVar.m) - 0.0f, ((float) bVar.l) - 0.0f, 5.0f, this.f684b);
                }
                if (bVar.n > 0) {
                    this.f683a.drawCircle(((float) bVar.o) - 0.0f, ((float) bVar.n) - 0.0f, 5.0f, this.f684b);
                }
                if (!z || i != bVar.f163a) {
                    this.f684b.setStyle(Paint.Style.FILL);
                    this.f684b.setColor(Color.argb(255, bVar.A, bVar.B, bVar.C));
                    this.f684b.setStrokeWidth(1.0f);
                } else {
                    a(str, (float) bVar.A, (float) bVar.B, (float) bVar.C);
                }
                this.f683a.drawCircle(((float) bVar.g) - 0.0f, ((float) bVar.f) - 0.0f, 4.0f, this.f684b);
                if (bVar.h > 0) {
                    this.f683a.drawCircle(((float) bVar.i) - 0.0f, ((float) bVar.h) - 0.0f, 4.0f, this.f684b);
                }
                if (bVar.j > 0) {
                    this.f683a.drawCircle(((float) bVar.k) - 0.0f, ((float) bVar.j) - 0.0f, 4.0f, this.f684b);
                }
                if (bVar.l > 0) {
                    this.f683a.drawCircle(((float) bVar.m) - 0.0f, ((float) bVar.l) - 0.0f, 4.0f, this.f684b);
                }
                if (bVar.n > 0) {
                    this.f683a.drawCircle(((float) bVar.o) - 0.0f, ((float) bVar.n) - 0.0f, 4.0f, this.f684b);
                }
            } else if (str2.equalsIgnoreCase("tc")) {
                this.f684b.setColor(-16777216);
                this.f683a.drawCircle(((float) bVar.g) - 0.0f, ((float) bVar.f) - 0.0f, 3.2f, this.f684b);
                if (bVar.h > 0) {
                    this.f683a.drawCircle(((float) bVar.i) - 0.0f, ((float) bVar.h) - 0.0f, 3.2f, this.f684b);
                }
                if (bVar.j > 0) {
                    this.f683a.drawCircle(((float) bVar.k) - 0.0f, ((float) bVar.j) - 0.0f, 3.2f, this.f684b);
                }
                if (bVar.l > 0) {
                    this.f683a.drawCircle(((float) bVar.m) - 0.0f, ((float) bVar.l) - 0.0f, 3.2f, this.f684b);
                }
                if (bVar.n > 0) {
                    this.f683a.drawCircle(((float) bVar.o) - 0.0f, ((float) bVar.n) - 0.0f, 3.2f, this.f684b);
                }
                if (!z || i != bVar.f163a) {
                    this.f684b.setStyle(Paint.Style.FILL);
                    this.f684b.setColor(Color.argb(255, bVar.A, bVar.B, bVar.C));
                    this.f684b.setStrokeWidth(1.0f);
                } else {
                    a(str, (float) bVar.A, (float) bVar.B, (float) bVar.C);
                }
                this.f683a.drawCircle(((float) bVar.g) - 0.0f, ((float) bVar.f) - 0.0f, 3.2f, this.f684b);
                if (bVar.h > 0) {
                    this.f683a.drawCircle(((float) bVar.i) - 0.0f, ((float) bVar.h) - 0.0f, 3.2f, this.f684b);
                }
                if (bVar.j > 0) {
                    this.f683a.drawCircle(((float) bVar.k) - 0.0f, ((float) bVar.j) - 0.0f, 3.2f, this.f684b);
                }
                if (bVar.l > 0) {
                    this.f683a.drawCircle(((float) bVar.m) - 0.0f, ((float) bVar.l) - 0.0f, 3.2f, this.f684b);
                }
                if (bVar.n > 0) {
                    this.f683a.drawCircle(((float) bVar.o) - 0.0f, ((float) bVar.n) - 0.0f, 3.2f, this.f684b);
                }
            }
            if (z2) {
                a(bVar);
            }
        }
        if (z) {
            if (i == bVar.f163a) {
                a(str, (float) bVar.A, (float) bVar.B, (float) bVar.C);
            } else {
                this.f684b.setStyle(Paint.Style.FILL);
                this.f684b.setColor(Color.argb(255, bVar.A, bVar.B, bVar.C));
                this.f684b.setStrokeWidth(1.0f);
            }
            this.f683a.drawText(bVar.c, (float) bVar.q, (float) bVar.p, this.f684b);
            if (bVar.q < bVar.g) {
                f2 = this.f684b.measureText(bVar.c);
            } else {
                f2 = 0.0f;
            }
            this.f683a.drawLine((float) bVar.g, (float) bVar.f, f2 + ((float) bVar.q), (float) bVar.p, this.f684b);
        }
    }

    private void a(String str, float f2, float f3, float f4) {
        if (str.equalsIgnoreCase("front_1") || str.equalsIgnoreCase("front_2") || str.equalsIgnoreCase("front_3") || str.equalsIgnoreCase("upperarm_1")) {
            if (((double) f2) == 0.0d && ((double) f3) == 0.0d && ((double) f4) == 0.0d) {
                this.f684b.setColor(-16777216);
            } else {
                this.f684b.setColor(-16776961);
            }
        } else if (str.equalsIgnoreCase("back_1") || str.equalsIgnoreCase("back_2") || str.equalsIgnoreCase("back_3") || str.equalsIgnoreCase("back_4") || str.equalsIgnoreCase("auricular")) {
            if (((double) f2) == 0.0d && ((double) f3) == 0.0d && ((double) f4) == 0.0d) {
                this.f684b.setColor(-16777216);
            } else {
                this.f684b.setColor(-65536);
            }
        } else if (((double) f2) != 0.0d || ((double) f3) != 0.0d || ((double) f4) != 0.0d) {
            this.f684b.setColor(Color.argb(255, 255, 140, 0));
        } else if (str.equalsIgnoreCase("foot_1") || str.equalsIgnoreCase("foot_2") || str.equalsIgnoreCase("hand_1") || str.equalsIgnoreCase("hand_2")) {
            this.f684b.setColor(-1);
        } else {
            this.f684b.setColor(-16777216);
        }
        this.f684b.setStrokeWidth(2.0f);
    }

    private void a(b bVar) {
        float f2;
        float f3;
        float f4;
        float f5;
        float f6;
        float f7;
        float f8;
        float f9;
        float f10;
        float f11;
        float f12;
        float f13;
        float f14;
        float f15;
        this.f684b.setStrokeWidth(1.0f);
        this.f684b.setColor(-16777216);
        float f16 = (float) (bVar.t - bVar.r);
        float f17 = (float) (bVar.u - bVar.s);
        float f18 = (f16 == 0.0f || f17 == 0.0f) ? 0.0f : f16 / f17;
        int i = bVar.v;
        float[] fArr = new float[((i + 2) * 4)];
        int i2 = bVar.w;
        float f19 = f17 / ((float) i);
        float f20 = f16 / ((float) i);
        int i3 = bVar.r;
        int i4 = bVar.s;
        Point point = new Point();
        Point point2 = new Point();
        point.x = bVar.r;
        point.y = bVar.s;
        point2.x = (int) (((float) i3) + (((float) i) * f20));
        point2.y = (int) (((float) i4) + (((float) i) * f19));
        fArr[0] = (float) point.x;
        fArr[1] = (float) point.y;
        fArr[2] = (float) point2.x;
        fArr[3] = (float) point2.y;
        int i5 = 0 + 1 + 1 + 1 + 1;
        for (int i6 = 0; i6 <= i; i6++) {
            float f21 = ((float) i3) + (((float) i6) * f20);
            float f22 = ((float) i4) + (((float) i6) * f19);
            point.x = (int) f21;
            point.y = (int) f22;
            if (i6 == 0) {
                float f23 = f22 - (30.0f * f18);
                if (f17 == 0.0f) {
                    f23 = f22 - 30.0f;
                    f12 = f21;
                } else if (bVar.e.equalsIgnoreCase("lt")) {
                    f12 = (((float) i3) - (((float) i6) * f20)) - 30.0f;
                } else {
                    f12 = 30.0f + f21;
                }
                point2.x = (int) f12;
                point2.y = (int) f23;
                if (dh.I) {
                    Log.d("Top Tick", "Tick #" + Math.abs(i6 - i) + " - startCol=" + Integer.toString(point.x) + " - startRow=" + Integer.toString(point.y) + " - endCol=" + Integer.toString(point2.x) + " - endRow=" + Integer.toString(point2.y));
                }
                String num = Integer.toString(Math.abs(i6 - i));
                float f24 = 15.0f * f18;
                if (bVar.e.equalsIgnoreCase("lt")) {
                    f13 = (((float) i3) - (((float) i6) * f20)) + 7.0f;
                } else {
                    f13 = f21 - 15.0f;
                }
                float f25 = f22 - f24;
                if (bVar.r <= bVar.t) {
                    if (bVar.s > bVar.u) {
                        f14 = f22 - 15.0f;
                    } else if (((double) f20) == 0.0d && ((double) f24) == 0.0d) {
                        f14 = f22 - 5.0f;
                    } else {
                        f14 = f25;
                    }
                } else if (f24 <= 0.0f && ((double) f24) > -1.0d) {
                    f14 = (f24 * 10.0f) + f22;
                } else if (((double) f24) < -1.0d) {
                    f14 = (f24 * 2.0f) + f22;
                } else {
                    f14 = (f24 * 4.0f) + f22;
                }
                if (f17 == 0.0f) {
                    f13 += 10.0f;
                }
                if (f22 > f14) {
                    f15 = f22 + 5.0f;
                } else {
                    f15 = f14;
                }
                this.f683a.drawText(num, f13, f15, this.f684b);
                if (dh.P) {
                    Log.d("Top Text", num + " - endCol=" + Float.toString(f13) + " - endRow=" + Float.toString(f15));
                }
            } else if (i6 == i) {
                float f26 = f22 - (30.0f * f18);
                if (f17 == 0.0f) {
                    f26 = f22 - 30.0f;
                    f8 = f21;
                } else if (bVar.e.equalsIgnoreCase("lt")) {
                    f8 = (((float) i3) - (((float) i6) * f20)) - 30.0f;
                } else {
                    f8 = 30.0f + f21;
                }
                point2.x = (int) f8;
                point2.y = (int) f26;
                if (dh.P) {
                    Log.d("Bottom Tick", "Tick #" + Math.abs(i6 - i) + " - startCol=" + Integer.toString(point.x) + " - startRow=" + Integer.toString(point.y) + " - endCol=" + Integer.toString(point2.x) + " - endRow=" + Integer.toString(point2.y));
                }
                String num2 = Integer.toString(Math.abs(i6 - i));
                float f27 = 15.0f * f18;
                if (bVar.e.equalsIgnoreCase("lt")) {
                    f9 = (((float) i3) - (((float) i6) * f20)) + 7.0f;
                } else {
                    f9 = f21 - 15.0f;
                }
                float f28 = f22 - f27;
                if (bVar.r <= bVar.t) {
                    if (bVar.s > bVar.u) {
                        f10 = f22 - 15.0f;
                    } else if (((double) f20) == 0.0d && ((double) f27) == 0.0d) {
                        f10 = f22 - 5.0f;
                    } else {
                        f10 = f28;
                    }
                } else if (f27 <= 0.0f && ((double) f27) > -1.0d) {
                    f10 = (f27 * 10.0f) + f22;
                } else if (((double) f27) < -1.0d) {
                    f10 = (f27 * 2.0f) + f22;
                } else {
                    f10 = (f27 * 4.0f) + f22;
                }
                if (f17 == 0.0f) {
                    f9 += 10.0f;
                }
                if (f22 > f10) {
                    f11 = f22 + 5.0f;
                } else {
                    f11 = f10;
                }
                this.f683a.drawText(num2, f9, f11, this.f684b);
                if (dh.P) {
                    Log.d("Bottom Text", num2 + " - endCol=" + Float.toString(f9) + " - endRow=" + Float.toString(f11));
                }
            } else if (i6 % (i / i2) == 0) {
                float f29 = 15.0f * f18;
                float f30 = f22 - f29;
                if (f17 == 0.0f) {
                    f30 = f22 - 15.0f;
                    f4 = f21;
                } else if (bVar.e.equalsIgnoreCase("lt")) {
                    f4 = (((float) i3) - (((float) i6) * f20)) - 15.0f;
                } else {
                    f4 = 15.0f + f21;
                }
                point2.x = (int) f4;
                point2.y = (int) f30;
                if (dh.P) {
                    Log.d("Major Ticks", "Tick #" + Math.abs(i6 - i) + " - startCol=" + Integer.toString(point.x) + " - startRow=" + Integer.toString(point.y) + " - endCol=" + Integer.toString(point2.x) + " - endRow=" + Integer.toString(point2.y));
                }
                String num3 = Integer.toString(Math.abs(i6 - i));
                if (bVar.e.equalsIgnoreCase("lt")) {
                    f5 = (((float) i3) - (((float) i6) * f20)) + 7.0f;
                } else {
                    f5 = f21 - 15.0f;
                }
                float f31 = f22 - f29;
                if (bVar.r <= bVar.t) {
                    if (bVar.s > bVar.u) {
                        f6 = f22 - 15.0f;
                    } else if (((double) f20) == 0.0d && ((double) f29) == 0.0d) {
                        f6 = f22 - 5.0f;
                    } else {
                        f6 = f31;
                    }
                } else if (f29 <= 0.0f && ((double) f29) > -1.0d) {
                    f6 = (f29 * 10.0f) + f22;
                } else if (((double) f29) < -1.0d) {
                    f6 = (f29 * 2.0f) + f22;
                } else {
                    f6 = (f29 * 4.0f) + f22;
                }
                if (f17 == 0.0f) {
                    f5 += 10.0f;
                }
                if (f22 > f6) {
                    f7 = f22 + 5.0f;
                } else {
                    f7 = f6;
                }
                this.f683a.drawText(num3, f5, f7, this.f684b);
                if (dh.P) {
                    Log.d("Major Tick Text", num3 + " - endCol=" + Float.toString(f5) + " - endRow=" + Float.toString(f7));
                }
            } else {
                float f32 = f22 - (10.0f * f18);
                if (f17 == 0.0f) {
                    float f33 = f22 - 10.0f;
                    f2 = f21;
                    f3 = f33;
                } else if (bVar.e.equalsIgnoreCase("lt")) {
                    f2 = (((float) i3) - (((float) i6) * f20)) - 10.0f;
                    f3 = f32;
                } else {
                    f2 = f21 + 10.0f;
                    f3 = f32;
                }
                point2.x = (int) f2;
                point2.y = (int) f3;
                if (dh.P) {
                    Log.d("Minor Ticks", "Tick #" + Math.abs(i6 - i) + " - startCol=" + Integer.toString(point.x) + " - startRow=" + Integer.toString(point.y) + " - endCol=" + Integer.toString(point2.x) + " - endRow=" + Integer.toString(point2.y));
                }
            }
            int i7 = i5 + 1;
            fArr[i5] = (float) point.x;
            int i8 = i7 + 1;
            fArr[i7] = (float) point.y;
            int i9 = i8 + 1;
            fArr[i8] = (float) point2.x;
            i5 = i9 + 1;
            fArr[i9] = (float) point2.y;
            if (dh.P) {
                Log.d("Draw Line", " - s.x=" + Integer.toString(point.x) + " - s.y=" + Integer.toString(point.y) + " - e.x=" + Integer.toString(point2.x) + " - e.y=" + Integer.toString(point2.y));
            }
        }
        this.f683a.drawLines(fArr, this.f684b);
    }
}
