package com.cyberandsons.tcmaidtrial.draw;

import android.view.MotionEvent;

public final class b extends e {
    protected b(MotionEvent motionEvent) {
        super(motionEvent);
    }

    public final float a(int i) {
        return this.f697a.getX(i);
    }

    public final float b(int i) {
        return this.f697a.getY(i);
    }

    public final int a() {
        return this.f697a.getPointerCount();
    }

    public final int c(int i) {
        return this.f697a.getPointerId(i);
    }
}
