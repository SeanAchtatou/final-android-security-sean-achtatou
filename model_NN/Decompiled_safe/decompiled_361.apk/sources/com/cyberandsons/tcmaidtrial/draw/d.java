package com.cyberandsons.tcmaidtrial.draw;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.AdapterView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class d implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ImageGallery f696a;

    d(ImageGallery imageGallery) {
        this.f696a = imageGallery;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Bitmap b2 = dh.b(dh.a(this.f696a.d[i]), "herbs");
        if (b2 == null) {
            this.f696a.f682b.setImageResource(C0000R.drawable.nopicture);
            return;
        }
        this.f696a.f682b.setImageDrawable(dh.a(b2, TcmAid.cS - 0, TcmAid.cS - 0, this.f696a.getWindow()));
        this.f696a.c.setText(this.f696a.d[i]);
    }
}
