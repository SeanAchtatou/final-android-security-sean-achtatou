package com.cyberandsons.tcmaidtrial.draw.colormixer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

public final class b extends AlertDialog implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private ColorMixer f692a = null;

    /* renamed from: b  reason: collision with root package name */
    private int f693b;
    private e c = null;

    public b(Context context, int i, e eVar) {
        super(context);
        this.f693b = i;
        this.c = eVar;
        a aVar = new a("cwac-colormixer", context);
        this.f692a = new ColorMixer(context);
        this.f692a.a(i);
        setView(this.f692a);
        setButton(context.getText(aVar.a("set", "string", true)), this);
        setButton2(context.getText(aVar.a("cancel", "string", true)), (DialogInterface.OnClickListener) null);
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (this.f693b != this.f692a.a()) {
            this.c.a(this.f692a.a());
        }
    }

    public final Bundle onSaveInstanceState() {
        Bundle onSaveInstanceState = super.onSaveInstanceState();
        onSaveInstanceState.putInt("c", this.f692a.a());
        return onSaveInstanceState;
    }

    public final void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.f692a.a(bundle.getInt("c"));
    }
}
