package com.cyberandsons.tcmaidtrial.draw.colormixer;

import android.widget.SeekBar;

final class c implements SeekBar.OnSeekBarChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ColorMixer f694a;

    c(ColorMixer colorMixer) {
        this.f694a = colorMixer;
    }

    public final void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        int a2 = this.f694a.a();
        this.f694a.f688b.setBackgroundColor(a2);
        if (this.f694a.f != null) {
            this.f694a.f.a(a2);
        }
    }

    public final void onStartTrackingTouch(SeekBar seekBar) {
    }

    public final void onStopTrackingTouch(SeekBar seekBar) {
    }
}
