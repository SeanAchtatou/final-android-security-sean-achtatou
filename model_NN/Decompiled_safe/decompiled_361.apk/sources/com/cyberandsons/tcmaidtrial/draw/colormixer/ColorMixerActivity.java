package com.cyberandsons.tcmaidtrial.draw.colormixer;

import android.app.Activity;
import android.os.Bundle;

public class ColorMixerActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private ColorMixer f689a = null;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setFlags(4, 4);
        a aVar = new a("cwac-colormixer", this);
        setContentView(aVar.b("activity"));
        this.f689a = (ColorMixer) findViewById(aVar.c("mixer"));
        String stringExtra = getIntent().getStringExtra("t");
        if (stringExtra != null) {
            setTitle(stringExtra);
        }
        this.f689a.a(getIntent().getIntExtra("c", this.f689a.a()));
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("c", this.f689a.a());
    }

    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.f689a.a(bundle.getInt("c"));
    }
}
