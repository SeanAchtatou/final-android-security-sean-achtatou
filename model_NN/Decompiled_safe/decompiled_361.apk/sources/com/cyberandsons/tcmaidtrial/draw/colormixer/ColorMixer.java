package com.cyberandsons.tcmaidtrial.draw.colormixer;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

public class ColorMixer extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private a f687a = null;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public View f688b = null;
    private SeekBar c = null;
    private SeekBar d = null;
    private SeekBar e = null;
    /* access modifiers changed from: private */
    public e f = null;
    private SeekBar.OnSeekBarChangeListener g = new c(this);

    public ColorMixer(Context context) {
        super(context);
        a((AttributeSet) null);
    }

    public ColorMixer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(attributeSet);
    }

    public ColorMixer(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(attributeSet);
    }

    public final int a() {
        return Color.argb(255, this.c.getProgress(), this.e.getProgress(), this.d.getProgress());
    }

    public final void a(int i) {
        this.c.setProgress(Color.red(i));
        this.e.setProgress(Color.green(i));
        this.d.setProgress(Color.blue(i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, com.cyberandsons.tcmaidtrial.draw.colormixer.ColorMixer, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a(AttributeSet attributeSet) {
        this.f687a = new a("cwac-colormixer", getContext());
        ((Activity) getContext()).getLayoutInflater().inflate(this.f687a.b("main"), (ViewGroup) this, true);
        this.f688b = findViewById(this.f687a.c("swatch"));
        this.c = (SeekBar) findViewById(this.f687a.c("red"));
        this.c.setMax(255);
        this.c.setOnSeekBarChangeListener(this.g);
        this.e = (SeekBar) findViewById(this.f687a.c("green"));
        this.e.setMax(255);
        this.e.setOnSeekBarChangeListener(this.g);
        this.d = (SeekBar) findViewById(this.f687a.c("blue"));
        this.d.setMax(255);
        this.d.setOnSeekBarChangeListener(this.g);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, this.f687a.a("ColorMixer"), 0, 0);
            a(obtainStyledAttributes.getInt(this.f687a.a("ColorMixer" + "_" + "color", "styleable", false), -5978567));
            obtainStyledAttributes.recycle();
        }
    }

    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("superState", super.onSaveInstanceState());
        bundle.putInt("color", a());
        return bundle;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        Bundle bundle = (Bundle) parcelable;
        super.onRestoreInstanceState(bundle.getParcelable("superState"));
        a(bundle.getInt("color"));
    }
}
