package com.cyberandsons.tcmaidtrial.draw;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.misc.dh;
import com.cyberandsons.tcmaidtrial.p;

public final class c extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private int f685a;

    /* renamed from: b  reason: collision with root package name */
    private Context f686b;
    private /* synthetic */ ImageGallery c;

    public c(ImageGallery imageGallery, Context context) {
        this.c = imageGallery;
        this.f686b = context;
        TypedArray obtainStyledAttributes = imageGallery.obtainStyledAttributes(p.f1027a);
        this.f685a = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
    }

    public final int getCount() {
        return this.c.d.length;
    }

    public final Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imageView = new ImageView(this.f686b);
        Bitmap b2 = dh.b(dh.a(this.c.d[i]), "herbs");
        if (b2 == null) {
            imageView.setImageResource(C0000R.drawable.nopicture);
        } else {
            imageView.setImageDrawable(dh.a(b2, TcmAid.cS - 0, TcmAid.cS - 0, this.c.getWindow()));
        }
        imageView.setLayoutParams(new Gallery.LayoutParams(100, 100));
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setBackgroundResource(this.f685a);
        return imageView;
    }
}
