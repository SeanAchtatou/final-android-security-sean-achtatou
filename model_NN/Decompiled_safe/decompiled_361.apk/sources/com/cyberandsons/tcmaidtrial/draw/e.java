package com.cyberandsons.tcmaidtrial.draw;

import android.view.MotionEvent;

public class e {

    /* renamed from: a  reason: collision with root package name */
    protected MotionEvent f697a;

    protected e(MotionEvent motionEvent) {
        this.f697a = motionEvent;
    }

    public static e a(MotionEvent motionEvent) {
        try {
            return new b(motionEvent);
        } catch (VerifyError e) {
            return new e(motionEvent);
        }
    }

    public final int b() {
        return this.f697a.getAction();
    }

    public final float c() {
        return this.f697a.getX();
    }

    public float a(int i) {
        d(i);
        return this.f697a.getX();
    }

    public final float d() {
        return this.f697a.getY();
    }

    public float b(int i) {
        d(i);
        return this.f697a.getY();
    }

    public int a() {
        return 1;
    }

    public int c(int i) {
        d(i);
        return 0;
    }

    private static void d(int i) {
        if (i > 0) {
            throw new IllegalArgumentException("Invalid pointer index for Donut/Cupcake");
        }
    }
}
