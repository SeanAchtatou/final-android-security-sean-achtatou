package com.cyberandsons.tcmaidtrial.draw.colormixer;

import java.util.LinkedHashMap;
import java.util.Map;

public final class d extends LinkedHashMap {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f695a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(a aVar) {
        super(101, 1.1f, true);
        this.f695a = aVar;
    }

    /* access modifiers changed from: protected */
    public final boolean removeEldestEntry(Map.Entry entry) {
        return size() > 101;
    }
}
