package com.cyberandsons.tcmaidtrial.draw.colormixer;

import android.content.Context;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Map;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private String f690a = null;

    /* renamed from: b  reason: collision with root package name */
    private Class f691b = null;
    private Map c = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public a(String str, Context context) {
        this.f690a = str.replace('.', '_').replace('-', '_');
        try {
            this.f691b = Class.forName(context.getPackageName() + ".R");
            this.c = Collections.synchronizedMap(new d(this));
        } catch (Throwable th) {
            throw new RuntimeException("Exception finding R class", th);
        }
    }

    public final int[] a(String str) {
        Field declaredField;
        try {
            Class d = d("styleable");
            if (!(d == null || (declaredField = d.getDeclaredField(str)) == null)) {
                Object obj = declaredField.get(d);
                if (obj instanceof int[]) {
                    int[] iArr = new int[Array.getLength(obj)];
                    for (int i = 0; i < Array.getLength(obj); i++) {
                        iArr[i] = Array.getInt(obj, i);
                    }
                    return iArr;
                }
            }
            return new int[0];
        } catch (Throwable th) {
            throw new RuntimeException("Exception finding styleable", th);
        }
    }

    public final int b(String str) {
        return a(str, "layout", true);
    }

    public final int c(String str) {
        return a(str, "id", false);
    }

    public final int a(String str, String str2, boolean z) {
        String str3;
        Field declaredField;
        StringBuilder sb = new StringBuilder(str);
        sb.append('|');
        sb.append(str2);
        Integer num = (Integer) this.c.get(sb.toString());
        if (num != null) {
            return num.intValue();
        }
        if (str.startsWith(this.f690a) || !z) {
            str3 = str;
        } else {
            str3 = this.f690a + '_' + str;
        }
        try {
            Class d = d(str2);
            if (d == null || (declaredField = d.getDeclaredField(str3)) == null) {
                return -1;
            }
            int i = declaredField.getInt(d);
            this.c.put(sb.toString(), Integer.valueOf(i));
            return i;
        } catch (Throwable th) {
            throw new RuntimeException("Exception finding resource identifier", th);
        }
    }

    private Class d(String str) {
        for (Class<?> cls : this.f691b.getClasses()) {
            if (str.equals(cls.getSimpleName())) {
                return cls;
            }
        }
        return null;
    }
}
