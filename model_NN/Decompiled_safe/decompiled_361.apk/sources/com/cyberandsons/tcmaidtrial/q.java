package com.cyberandsons.tcmaidtrial;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.android.vending.licensing.e;
import com.android.vending.licensing.v;
import com.cyberandsons.tcmaidtrial.network.u;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;

final class q implements e {

    /* renamed from: a  reason: collision with root package name */
    private u f1033a;

    /* renamed from: b  reason: collision with root package name */
    private SQLiteDatabase f1034b;
    private /* synthetic */ TcmAid c;

    public q(TcmAid tcmAid, u uVar) {
        this.c = tcmAid;
        this.f1033a = uVar;
    }

    public final void a() {
        if (!this.c.isFinishing()) {
            TcmAid.d = 0;
            Log.i("TA:MLCC.allow()", this.c.getString(C0000R.string.allow));
            if (TcmAid.e) {
                this.f1033a.a(this.c.f161b);
            }
        }
    }

    public final void b() {
        if (!this.c.isFinishing()) {
            TcmAid.d = 16;
            Log.i("TA:MLCC.dontAllow()", this.c.getString(C0000R.string.dont_allow));
            if (TcmAid.e) {
                this.f1033a.a(this.c.f161b);
            }
            long T = this.c.ds.T();
            if (T == 0) {
                Log.d("timestamp", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Timestamp(System.currentTimeMillis())));
                this.f1034b = this.c.a(this.f1034b, this.c.f161b);
                this.c.ds.a(System.currentTimeMillis(), this.f1034b);
                this.f1034b.close();
                return;
            }
            Calendar instance = Calendar.getInstance();
            instance.setTimeInMillis(T);
            if (((int) (((((Calendar.getInstance().getTimeInMillis() - instance.getTimeInMillis()) / 1000) / 60) / 60) / 24)) >= 7) {
                this.c.showDialog(0);
            }
        }
    }

    public final void a(v vVar) {
        if (!this.c.isFinishing()) {
            TcmAid.d = 32;
            Log.i("TA:MLCC.applicationError()", String.format(this.c.getString(C0000R.string.application_error), vVar));
        }
    }
}
