package com.cyberandsons.tcmaidtrial;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import com.android.vending.licensing.g;
import com.android.vending.licensing.i;
import com.android.vending.licensing.u;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.areas.DiagnosisCategoryArea;
import com.cyberandsons.tcmaidtrial.areas.MateriaMedicaArea;
import com.cyberandsons.tcmaidtrial.areas.PointsArea;
import com.cyberandsons.tcmaidtrial.areas.r;
import com.cyberandsons.tcmaidtrial.iap.InventoryListActivity;
import com.cyberandsons.tcmaidtrial.iap.j;
import com.cyberandsons.tcmaidtrial.misc.PreviousSearch;
import com.cyberandsons.tcmaidtrial.misc.SendEmail;
import com.cyberandsons.tcmaidtrial.misc.SettingsData;
import com.cyberandsons.tcmaidtrial.misc.SignPostDisplay;
import com.cyberandsons.tcmaidtrial.misc.b;
import com.cyberandsons.tcmaidtrial.misc.bi;
import com.cyberandsons.tcmaidtrial.misc.dh;
import com.cyberandsons.tcmaidtrial.network.DownloaderActivity;
import com.cyberandsons.tcmaidtrial.network.e;
import com.cyberandsons.tcmaidtrial.network.v;
import com.cyberandsons.tcmaidtrial.push.IntentReceiver;
import com.cyberandsons.tcmaidtrial.push.PushPreferencesActivity;
import com.cyberandsons.tcmaidtrial.quiz.QuizIntroduction;
import com.urbanairship.b.h;
import com.urbanairship.push.f;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TcmAid extends Activity {
    public static int A = 99;
    public static String B;
    public static String C;
    public static String[] D;
    public static String E;
    public static String F;
    public static String G;
    public static String H;
    public static int I = -1;
    public static int J;
    public static ArrayList K = new ArrayList();
    public static int L;
    public static ArrayList M = new ArrayList();
    public static ArrayList N = new ArrayList();
    public static ArrayList O = new ArrayList();
    public static boolean P;
    public static String Q;
    public static String[] R;
    public static String S;
    public static String T;
    public static String U;
    public static int V = -1;
    public static int W;
    public static ArrayList X = new ArrayList();
    public static int Y;
    public static ArrayList Z = new ArrayList();
    public static ArrayList aA = new ArrayList();
    public static ArrayList aB = new ArrayList();
    public static String aC;
    public static String aD;
    public static String[] aE;
    public static String aF;
    public static String aG;
    public static int aH = -1;
    public static int aI;
    public static ArrayList aJ = new ArrayList();
    public static int aK;
    public static ArrayList aL = new ArrayList();
    public static String aM;
    public static String aN;
    public static String[] aO;
    public static String aP;
    public static int aQ = -1;
    public static int aR;
    public static ArrayList aS = new ArrayList();
    public static int aT;
    public static ArrayList aU = new ArrayList();
    public static ArrayList aV = new ArrayList();
    public static String aW;
    public static String[] aX;
    public static String aY;
    public static String aZ;
    public static ArrayList aa = new ArrayList();
    public static ArrayList ab = new ArrayList();
    public static boolean ac;
    public static String ad;
    public static String[] ae;
    public static String af;
    public static String ag;
    public static int ah = -1;
    public static int ai;
    public static ArrayList aj = new ArrayList();
    public static int ak;
    public static ArrayList al = new ArrayList();
    public static ArrayList am = new ArrayList();
    public static String an;
    public static String ao;
    public static String ap;
    public static String[] aq;
    public static String ar;
    public static String as;
    public static int at = -1;
    public static int au;
    public static int av;
    public static ArrayList aw = new ArrayList();
    public static ArrayList ax = new ArrayList();
    public static int ay;
    public static ArrayList az = new ArrayList();
    public static ArrayList bA = new ArrayList();
    public static ArrayList bB = new ArrayList();
    public static ArrayList bC = new ArrayList();
    public static ArrayList bD = new ArrayList();
    public static ArrayList bE = new ArrayList();
    public static ArrayList bF = new ArrayList();
    public static ArrayList bG = new ArrayList();
    public static ArrayList bH = new ArrayList();
    public static ArrayList bI = new ArrayList();
    public static ArrayList bJ = new ArrayList();
    public static ArrayList bK = new ArrayList();
    public static ArrayList bL = new ArrayList();
    public static ArrayList bM = new ArrayList();
    public static ArrayList bN = new ArrayList();
    public static ArrayList bO = new ArrayList();
    public static ArrayList bP = new ArrayList();
    public static ArrayList bQ = new ArrayList();
    public static ArrayList bR = new ArrayList();
    public static ArrayList bS = new ArrayList();
    public static ArrayList bT = new ArrayList();
    public static ArrayList bU = new ArrayList();
    public static ArrayList bV = new ArrayList();
    public static ArrayList bW = new ArrayList();
    public static ArrayList bX = new ArrayList();
    public static ArrayList bY = new ArrayList();
    public static ArrayList bZ = new ArrayList();
    public static int ba = -1;
    public static int bb;
    public static ArrayList bc = new ArrayList();
    public static int bd;
    public static ArrayList be = new ArrayList();
    public static String bf;
    public static String[] bg;
    public static String bh;
    public static int bi = -1;
    public static int bj;
    public static ArrayList bk = new ArrayList();
    public static boolean bl = false;
    public static int bm;
    public static String bn;
    public static int bo = -1;
    public static int bp;
    public static int bq;
    public static String br;
    public static int bs;
    public static String bt;
    public static boolean bu;
    public static boolean bv;
    public static int bw;
    public static TabHost bx;
    public static ArrayList by = new ArrayList();
    public static ArrayList bz = new ArrayList();
    public static boolean c = false;
    public static boolean cA = false;
    public static boolean cB = false;
    public static boolean cC = false;
    public static boolean cD = false;
    public static boolean cE = false;
    public static boolean cF = false;
    public static boolean cG = false;
    public static boolean cH = false;
    public static boolean cI = false;
    public static boolean cJ = false;
    public static String cK;
    public static boolean cL = false;
    public static e cM;
    public static v cN;
    public static int cO = -1;
    public static String cP;
    public static String cQ;
    public static int cR = 0;
    public static int cS;
    public static int cT;
    public static int cU;
    public static int cV;
    public static int cW;
    public static int cX;
    public static ArrayList ca = new ArrayList();
    public static ArrayList cb = new ArrayList();
    public static ArrayList cc = new ArrayList();
    public static ArrayList cd = new ArrayList();
    public static ArrayList ce = new ArrayList();
    public static ArrayList cf = new ArrayList();
    public static ArrayList cg = new ArrayList();
    public static ArrayList ch = new ArrayList();
    public static ArrayList ci = new ArrayList();
    public static ArrayList cj = new ArrayList();
    public static ArrayList ck = new ArrayList();
    public static ArrayList cl = new ArrayList();
    public static ArrayList cm = new ArrayList();
    public static ArrayList cn = new ArrayList();
    public static ArrayList co = new ArrayList();
    public static ArrayList cp = new ArrayList();
    public static ArrayList cq = new ArrayList();
    public static ArrayList cr = new ArrayList();
    public static ArrayList cs = new ArrayList();
    public static ArrayList ct = new ArrayList();
    public static ArrayList cu = new ArrayList();
    public static boolean cv = false;
    public static boolean cw = false;
    public static boolean cx = false;
    public static boolean cy = false;
    public static boolean cz = false;
    public static int d = -1;
    private static ArrayList dA = new ArrayList();
    private static String dB;
    private static ArrayList dC = new ArrayList();
    private static ArrayList dD = new ArrayList();
    private static ArrayList dE = new ArrayList();
    private static boolean dF = false;
    private static boolean dG = false;
    private static boolean dH = false;
    private static boolean dI = false;
    private static boolean dJ = false;
    private static boolean dK = false;
    private static boolean dL = false;
    private static boolean dM = false;
    private static boolean dN = false;
    private static boolean dO = false;
    private static boolean dP = false;
    private static boolean dQ = false;
    private static boolean dR = false;
    private static boolean dS = false;
    private static String dU = "";
    private static String dV = "";
    private static int dW;
    private static boolean da = (!c);
    private static boolean db = false;
    private static boolean dc = false;
    private static boolean dd = false;
    private static int dt;
    private static ArrayList du = new ArrayList();
    private static int dv;
    private static ArrayList dw = new ArrayList();
    private static int dx;
    private static ArrayList dy = new ArrayList();
    private static String dz;
    public static boolean e = true;
    public static String f;
    public static boolean g = false;
    public static int h;
    public static ArrayList i = new ArrayList();
    public static ArrayList j = new ArrayList();
    public static ArrayList k = new ArrayList();
    public static boolean l;
    public static String m;
    public static String[] n;
    public static String o;
    public static String p;
    public static String q;
    public static int r = -1;
    public static int s;
    public static ArrayList t = new ArrayList();
    public static int u;
    public static ArrayList v = new ArrayList();
    public static ArrayList w = new ArrayList();
    public static ArrayList x = new ArrayList();
    public static ArrayList y = new ArrayList();
    public static ArrayList z = new ArrayList();

    /* renamed from: a  reason: collision with root package name */
    boolean f160a = true;

    /* renamed from: b  reason: collision with root package name */
    boolean f161b;
    private com.android.vending.licensing.e cY;
    private u cZ;
    private boolean dT;
    private EditText de;
    private Button df;
    private Button dg;
    private Button dh;
    private Button di;
    private ImageButton dj;
    private ImageButton dk;
    private ImageButton dl;
    private ImageButton dm;
    private ImageButton dn;

    /* renamed from: do  reason: not valid java name */
    private ImageButton f0do;
    private ImageButton dp;
    private q dq;
    /* access modifiers changed from: private */
    public SQLiteDatabase dr;
    /* access modifiers changed from: private */
    public n ds;

    public TcmAid() {
        boolean z2;
        if (!this.f160a) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.f161b = z2;
        this.dT = this.f161b;
    }

    static /* synthetic */ void a(TcmAid tcmAid) {
        if (tcmAid.g()) {
            d();
            tcmAid.startActivityForResult(new Intent(tcmAid, DiagnosisCategoryArea.class), 1350);
        }
    }

    static /* synthetic */ void b(TcmAid tcmAid) {
        if (tcmAid.g()) {
            d();
            tcmAid.startActivityForResult(new Intent(tcmAid, MateriaMedicaArea.class), 1350);
        }
    }

    static /* synthetic */ void c(TcmAid tcmAid) {
        if (tcmAid.g()) {
            d();
            tcmAid.startActivityForResult(new Intent(tcmAid, PointsArea.class), 1350);
        }
    }

    static /* synthetic */ void d(TcmAid tcmAid) {
        if (tcmAid.g()) {
            d();
            tcmAid.startActivityForResult(new Intent(tcmAid, QuizIntroduction.class), 1350);
        }
    }

    static /* synthetic */ void e(TcmAid tcmAid) {
        if (tcmAid.g()) {
            cK = " _id=1";
            tcmAid.startActivityForResult(new Intent(tcmAid, SettingsData.class), 1350);
        }
    }

    static /* synthetic */ void f(TcmAid tcmAid) {
        if (tcmAid.g()) {
            cK = " _id=1";
            tcmAid.startActivityForResult(new Intent(tcmAid, SendEmail.class), 1350);
        }
    }

    static /* synthetic */ void g(TcmAid tcmAid) {
        if (tcmAid.g()) {
            d();
            cK = " _id>=0";
            tcmAid.startActivityForResult(new Intent(tcmAid, PreviousSearch.class), 1350);
        }
    }

    static /* synthetic */ void h(TcmAid tcmAid) {
        if (tcmAid.g()) {
            tcmAid.dT = !tcmAid.dT;
            tcmAid.ds.j(tcmAid.dT, tcmAid.dr);
            tcmAid.dm.setImageDrawable(tcmAid.getResources().getDrawable(tcmAid.dT ? 17301618 : 17301619));
        }
    }

    static /* synthetic */ void i(TcmAid tcmAid) {
        if (tcmAid.g()) {
            tcmAid.startActivityForResult(new Intent(tcmAid, SignPostDisplay.class), 1350);
        }
    }

    static /* synthetic */ void j(TcmAid tcmAid) {
        String str;
        PackageInfo packageInfo;
        try {
            PackageInfo packageInfo2 = tcmAid.getPackageManager().getPackageInfo(tcmAid.getPackageName(), 0);
            try {
                Date date = new Date(new File(packageInfo2.applicationInfo.publicSourceDir).lastModified());
                str = "Version: " + packageInfo2.versionName + "<br>Database Level: " + tcmAid.getString(C0000R.string.database_level) + "<br>UUID: " + f + "<br>" + new SimpleDateFormat("yyyyMMdd.HHmmss").format((Object) date);
            } catch (Exception e2) {
                str = "Version " + packageInfo.versionName;
            }
        } catch (PackageManager.NameNotFoundException e3) {
            Log.e("TA:inIButton_Click", "NNFE: " + e3.getLocalizedMessage());
            str = "Cannot load Version!";
        }
        new AlertDialog.Builder(tcmAid).setIcon((int) C0000R.drawable.icon).setTitle("About TcmClinicAid").setMessage(Html.fromHtml("<strong>" + str + "</strong><br>" + "©" + " 2010 CyberAndSons.com<br><small><i>All rights reserved.</i></small>")).setPositiveButton("Dismiss", new g(tcmAid)).create().show();
    }

    public void onCreate(Bundle bundle) {
        PackageInfo packageInfo;
        boolean z2;
        super.onCreate(bundle);
        if (dh.d) {
            setRequestedOrientation(1);
        }
        if (dd || DownloaderActivity.a(this, getString(C0000R.string.dl_name), getString(C0000R.string.file_config_url), getString(C0000R.string.config_version), getString(C0000R.string.data_path), getString(C0000R.string.user_agent))) {
            setContentView((int) C0000R.layout.main);
            String string = getString(C0000R.string.app_name);
            if (string.compareTo(getString(C0000R.string.app_name_trial)) == 0) {
                db = true;
                da = false;
                dh.ap = false;
            }
            e();
            File file = new File(getString(C0000R.string.image_path));
            try {
                if (file.exists()) {
                    Log.i("TCA:prepareUserImageDir", getString(C0000R.string.image_path) + " exist.");
                } else {
                    getString(C0000R.string.image_path);
                    if (!file.mkdirs()) {
                        Log.e("TCA:prepareUserImageDir", "mkdirs() failed.");
                    }
                    if (!file.mkdir()) {
                        Log.e("TCA:prepareUserImageDir", "mkdir() failed.");
                    }
                    if (!file.exists()) {
                        Log.i("TCA:prepareUserImageDir", getString(C0000R.string.image_path) + " could not be created.");
                    }
                }
            } catch (SecurityException e2) {
                Log.d("TCA:prepareUserImageDir", getString(C0000R.string.image_path) + " error: " + e2.getLocalizedMessage());
            }
            b();
            c();
            this.dq = new q(this, getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
            try {
                this.dq.a();
                a(false);
                q.a(this.dr);
                this.dq.close();
            } catch (SQLException e3) {
                this.dq.close();
                throw e3;
            } catch (IOException e4) {
                this.dq.close();
                Log.e("TA:prepareDatabases()", e4.getLocalizedMessage());
            }
            f();
            a(true);
            try {
                a("index.html", getApplicationContext().getString(C0000R.string.httpRoot));
                a("tcmClinicAid.png", getApplicationContext().getString(C0000R.string.httpRoot));
                a("ic_menu_add.png", getApplicationContext().getString(C0000R.string.httpRoot));
                a("ic_menu_manage.png", getApplicationContext().getString(C0000R.string.httpRoot));
                a("ic_menu_send.png", getApplicationContext().getString(C0000R.string.httpRoot));
                a("star_big_off.png", getApplicationContext().getString(C0000R.string.httpRoot));
                a("star_big_on.png", getApplicationContext().getString(C0000R.string.httpRoot));
                com.cyberandsons.tcmaidtrial.network.u uVar = new com.cyberandsons.tcmaidtrial.network.u(this);
                f = uVar.a();
                if (dc) {
                    boolean aa2 = this.ds.aa();
                    if (!aa2) {
                        new r(this, "", "", new r(this)).show();
                    }
                    if (!aa2) {
                        Toast makeText = Toast.makeText(this, (int) C0000R.string.invalid_non_android_registration, 1);
                        makeText.setGravity(17, makeText.getXOffset() / 2, makeText.getYOffset() / 2);
                        makeText.show();
                    }
                } else if (!db) {
                    NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
                    if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                        z2 = false;
                    } else {
                        activeNetworkInfo.isRoaming();
                        z2 = true;
                    }
                    if (z2) {
                        Log.i("TA:doNetworkStuff()", "We have internet!");
                        if (Build.FINGERPRINT.startsWith("generic")) {
                            da = this.f161b;
                        }
                        if (da) {
                            String a2 = uVar.a();
                            Log.i("TA:setupLVL()", "deviceId = " + a2);
                            this.cY = new q(this, uVar);
                            this.cZ = new u(this, new i(this, new g(new byte[]{-46, 65, -30, Byte.MIN_VALUE, 103, 57, -74, -64, -51, -88, 95, -45, 77, 117, -36, -113, 91, -32, -64, 29}, getPackageName(), a2)), "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoYaKUgR7ItTqAnKxvQe4Qopkridek0Ly3yow6FpVY05oGFmTMiSZxK+qfkYf3BKq2NZigYiZZrSEqhhwND6xxqd0YsaQncGTSgbRK2LmLYN9lAtw6eqy4yWJ90ZrxTR7GSCEGga3veTbtS7kAX3lV7VituVol7c4s2uywlDdnzaLkdOzsrAfQa7Wt1gDvYXdJsqD6lB184EpUlmEw9qYoFP25Kpd/WXt5SWXkqt2glTs7vXfoJRGlfbsIUFcP1/5kZ4YOIIj5fEbSPBwFU10zKdgVaLwmVkjTGPRfae0c9pbIKDaG90vn/gr9kukFC7P2XhgSSoV+oVQBrGE0MoX/QIDAQAB");
                            this.cZ.a(this.cY);
                        }
                        if (e && !da) {
                            uVar.a(this.f160a);
                        }
                    } else {
                        Log.i("TA:doNetworkStuff()", "DAMN! No internet!");
                    }
                } else if (h()) {
                    g = false;
                }
            } catch (IOException e5) {
                Log.e("TA:doHouseKeeping()", "IOE: " + e5.getLocalizedMessage());
            }
            try {
                packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e6) {
                Log.e("TA:isNewRelease:", e6.getLocalizedMessage());
                packageInfo = null;
            }
            String str = packageInfo != null ? packageInfo.versionName != null ? packageInfo.versionName : null : null;
            String s2 = this.ds.s();
            if (str != null && !s2.equalsIgnoreCase(str)) {
                this.ds.g(str, this.dr);
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("New RELEASE!");
                create.setMessage("Check the Info Page (the Question Mark at the bottom right corner) for What's New with Release " + str);
                create.setButton("Dismiss", new e(this));
                create.show();
            }
            if (dh.ao && !this.ds.P()) {
                com.urbanairship.push.e eVar = new com.urbanairship.push.e();
                eVar.f = C0000R.drawable.icon_small;
                eVar.f1277a = C0000R.layout.notification;
                eVar.e = C0000R.drawable.icon;
                eVar.f1278b = C0000R.id.icon;
                eVar.c = C0000R.id.subject;
                eVar.d = C0000R.id.message;
                f.b().a(eVar);
                f.b().a(IntentReceiver.class);
                com.urbanairship.e.d("MyApplication onCreate - App APID: " + f.b().h().d());
            }
            if (dh.ap) {
                h.a().a(new j());
            }
            TextView textView = (TextView) findViewById(C0000R.id.tc_label);
            textView.setText(string);
            Paint paint = new Paint();
            paint.setTextSize(textView.getTextSize());
            paint.setAntiAlias(true);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            cS = displayMetrics.widthPixels;
            cT = displayMetrics.heightPixels;
            textView.setPadding((displayMetrics.widthPixels / 2) - (((int) (paint.measureText(string) + 0.5f)) / 2), 10, 0, 10);
            this.de = (EditText) findViewById(C0000R.id.searchEditText);
            this.de.setInputType(0);
            this.de.setOnClickListener(new s(this));
            this.dp = (ImageButton) findViewById(C0000R.id.searchButton);
            this.dp.setOnClickListener(new t(this));
            this.df = (Button) findViewById(C0000R.id.dxButton);
            this.df.setOnClickListener(new u(this));
            this.dg = (Button) findViewById(C0000R.id.mmButton);
            this.dg.setOnClickListener(new v(this));
            this.dh = (Button) findViewById(C0000R.id.ptButton);
            this.dh.setOnClickListener(new w(this));
            this.di = (Button) findViewById(C0000R.id.qyButton);
            this.di.setOnClickListener(new x(this));
            this.dj = (ImageButton) findViewById(C0000R.id.toolbox);
            this.dj.setOnClickListener(new y(this));
            this.dk = (ImageButton) findViewById(C0000R.id.email);
            this.dk.setOnClickListener(new z(this));
            this.dl = (ImageButton) findViewById(C0000R.id.magnifyingglass);
            this.dl.setOnClickListener(new aa(this));
            this.dm = (ImageButton) findViewById(C0000R.id.favorites);
            this.dT = this.ds.z();
            this.dm.setImageDrawable(getResources().getDrawable(this.dT ? 17301618 : 17301619));
            this.dm.setOnClickListener(new b(this));
            this.dn = (ImageButton) findViewById(C0000R.id.ic_menu_help);
            this.dn.setOnClickListener(new c(this));
            this.f0do = (ImageButton) findViewById(C0000R.id.info);
            this.f0do.setOnClickListener(new d(this));
        }
    }

    public void onDestroy() {
        Log.d("TCA:onDestroy", "calling tcmDbHelper.close()");
        if (this.dq != null) {
            this.dq.close();
        }
        super.onDestroy();
        if (da && this.cZ != null) {
            this.cZ.a();
        }
    }

    public void onResume() {
        f();
        a(true);
        d();
        s = 0;
        t.clear();
        dt = 0;
        du.clear();
        J = 0;
        K.clear();
        W = 0;
        X.clear();
        dv = 0;
        dw.clear();
        ai = 0;
        aj.clear();
        dx = 0;
        dy.clear();
        au = 0;
        aw.clear();
        aR = 0;
        aS.clear();
        bb = 0;
        bc.clear();
        bj = 0;
        bk.clear();
        e();
        super.onResume();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 2, 0, "Shop").setIcon((int) C0000R.drawable.cart);
        menu.add(0, 3, 0, "Push").setIcon(17301577);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 2:
                if (!dh.ap) {
                    AlertDialog create = new AlertDialog.Builder(this).create();
                    create.setTitle("NOT AVAILABLE!");
                    create.setMessage("InApp Purchasing is not currently available! Please stay tuned.");
                    create.setButton("Dismiss", new k(this));
                    create.show();
                } else {
                    startActivity(new Intent(getBaseContext(), InventoryListActivity.class));
                }
                return true;
            case 3:
                if (!dh.ao) {
                    AlertDialog create2 = new AlertDialog.Builder(this).create();
                    create2.setTitle("NOT AVAILABLE!");
                    create2.setMessage("Push Notification is not currently available! Please stay tuned.");
                    create2.setButton("Dismiss", new j(this));
                    create2.show();
                } else {
                    startActivity(new Intent(getBaseContext(), PushPreferencesActivity.class));
                }
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    private void b() {
        String[] strArr = {"da_ji2.jpg", "su_ya.jpg", "bei_sha_shen.png", "jin_qian_bai_hua_she.png", "guang_huo_xiang.png", "nan_sha_shen.png"};
        int[] iArr = {C0000R.raw.da_ji2, C0000R.raw.su_ya, C0000R.raw.bei_sha_shen, C0000R.raw.jin_qian_bai_hua_she, C0000R.raw.guang_huo_xiang, C0000R.raw.nan_sha_shen};
        for (int i2 = 0; i2 < iArr.length; i2++) {
            String str = strArr[i2];
            int i3 = iArr[i2];
            try {
                File file = new File("/sdcard/tcmclinicaid/herbs/" + str);
                if (!file.exists()) {
                    InputStream openRawResource = getBaseContext().getResources().openRawResource(i3);
                    FileOutputStream fileOutputStream = new FileOutputStream(file.getAbsolutePath());
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = openRawResource.read(bArr);
                        if (read <= 0) {
                            break;
                        }
                        fileOutputStream.write(bArr, 0, read);
                    }
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    openRawResource.close();
                }
            } catch (IOException e2) {
                Log.e("cpHerbImagesSDC", e2.getLocalizedMessage());
            }
        }
    }

    private void c() {
        String[] strArr = new String[0];
        int[] iArr = new int[0];
        for (int i2 = 0; i2 < iArr.length; i2++) {
            String str = strArr[i2];
            int i3 = iArr[i2];
            try {
                File file = new File("/sdcard/tcmclinicaid/points/" + str);
                if (!file.exists()) {
                    InputStream openRawResource = getBaseContext().getResources().openRawResource(i3);
                    FileOutputStream fileOutputStream = new FileOutputStream(file.getAbsolutePath());
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = openRawResource.read(bArr);
                        if (read <= 0) {
                            break;
                        }
                        fileOutputStream.write(bArr, 0, read);
                    }
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    openRawResource.close();
                }
            } catch (IOException e2) {
                Log.e("cpPointImagesSDC", e2.getLocalizedMessage());
            }
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        return new AlertDialog.Builder(this).setTitle((int) C0000R.string.unlicensed_dialog_title).setMessage((int) C0000R.string.unlicensed_dialog_body).setPositiveButton((int) C0000R.string.buy_button, new h(this)).setNegativeButton((int) C0000R.string.quit_button, new i(this)).create();
    }

    private void a(String str, String str2) {
        InputStream open = getApplicationContext().getAssets().open(str);
        String str3 = str2 + str;
        FileOutputStream fileOutputStream = new FileOutputStream(str3);
        byte[] bArr = new byte[1024];
        while (true) {
            try {
                int read = open.read(bArr);
                if (read <= 0) {
                    break;
                }
                fileOutputStream.write(bArr, 0, read);
            } catch (IOException e2) {
                Log.e("TA:copyServerFilesFromAssets()", "IOE: " + e2.getLocalizedMessage());
            }
        }
        fileOutputStream.flush();
        fileOutputStream.close();
        open.close();
        if (dh.Q) {
            Log.d("TCA:copyServerFilesFromAssets()", str3 + " copied.");
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (g()) {
            a(this.f160a);
            cQ = this.de.getText().toString();
            this.de.setText("");
            ((InputMethodManager) getSystemService("input_method")).toggleSoftInput(0, 0);
            if (cQ.length() != 0) {
                new m(this).execute(cQ);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:327:0x0eff, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:328:0x0f00, code lost:
        r2 = null;
        r3 = 0;
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:335:0x0f15, code lost:
        throw new java.lang.IllegalStateException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:338:0x0f1c, code lost:
        throw new java.lang.IllegalStateException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:340:0x0f21, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:341:0x0f22, code lost:
        r2 = null;
        r3 = 0;
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:344:0x0f2c, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:345:0x0f2d, code lost:
        r3 = r1;
        r1 = true;
        r12 = r0;
        r0 = r2;
        r2 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:346:0x0f33, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:347:0x0f34, code lost:
        r12 = r2;
        r2 = r1;
        r1 = r0;
        r0 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:348:0x0f39, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:349:0x0f3a, code lost:
        r2 = r1;
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:350:0x0f3d, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:351:0x0f3e, code lost:
        r2 = r0;
        r0 = r1;
        r1 = true;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x030c A[Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }] */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x04fd A[Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }] */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x06ca A[Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }] */
    /* JADX WARNING: Removed duplicated region for block: B:212:0x0883 A[Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }] */
    /* JADX WARNING: Removed duplicated region for block: B:236:0x0a48 A[Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }] */
    /* JADX WARNING: Removed duplicated region for block: B:260:0x0b89 A[Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }] */
    /* JADX WARNING: Removed duplicated region for block: B:284:0x0cca A[Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }] */
    /* JADX WARNING: Removed duplicated region for block: B:306:0x0ddb A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:309:0x0de2  */
    /* JADX WARNING: Removed duplicated region for block: B:333:0x0f0f A[ExcHandler: NullPointerException (e java.lang.NullPointerException), Splitter:B:34:0x0198] */
    /* JADX WARNING: Removed duplicated region for block: B:336:0x0f16 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x019e A[Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(java.lang.String r13, boolean r14, android.database.sqlite.SQLiteDatabase r15) {
        /*
            r7 = 2
            r11 = 0
            r10 = 1
            r1 = 0
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.i
            r0.clear()
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.w
            r0.clear()
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.x
            r0.clear()
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.y
            r0.clear()
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.Z
            r0.clear()
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.M
            r0.clear()
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.al
            r0.clear()
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.aL
            r0.clear()
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.aU
            r0.clear()
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.be
            r0.clear()
            com.cyberandsons.tcmaidtrial.a.n r2 = new com.cyberandsons.tcmaidtrial.a.n     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r2.<init>()     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r2.a(r15)     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r0 = r2.d()     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r0 == 0) goto L_0x0f61
            java.lang.String r0 = r2.D()     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r3 = 4
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4 = 0
            java.lang.String r5 = "main.auricular.point"
            r3[r4] = r5     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4 = 1
            java.lang.String r5 = "main.auricular.indication"
            r3[r4] = r5     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4 = 2
            java.lang.String r5 = "main.auricular.pointcat"
            r3[r4] = r5     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4 = 3
            java.lang.String r5 = "main.auricular.notes"
            r3[r4] = r5     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4 = 4
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5 = 0
            java.lang.String r6 = "userDB.auricular.point"
            r4[r5] = r6     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5 = 1
            java.lang.String r6 = "userDB.auricular.indication"
            r4[r5] = r6     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5 = 2
            java.lang.String r6 = "userDB.auricular.pointcat"
            r4[r5] = r6     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5 = 3
            java.lang.String r6 = "userDB.auricular.notes"
            r4[r5] = r6     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            a(r0, r3, r4)     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r0 = r0.length()     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r0 <= 0) goto L_0x0f61
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r0 = r0.length()     // Catch:{ SQLException -> 0x0eff, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r0 <= 0) goto L_0x0f61
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = b(r13, r0)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r3 = b(r13, r3)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r4 = r2.z()     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = r2.n()     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 0
            if (r4 == 0) goto L_0x0f5d
            int r4 = r5.length()     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 <= 0) goto L_0x0f5d
            java.lang.String r4 = "auricular"
            java.lang.String r6 = "_id"
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.a.q.a(r4, r6, r5)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = r4.length()     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r5 <= 0) goto L_0x0f5a
            r5 = r10
        L_0x00b5:
            if (r5 == 0) goto L_0x0de6
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5.<init>()     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = "SELECT main.auricular._id, lower(main.auricular.point), main.auricular.pointcat, main.auricular.nada, main.auricular.battlefield, main.auricular.acacd, main.auricular.stopsmoking, main.auricular.weightloss FROM main.auricular "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = " WHERE (main"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r5 = r5.append(r4)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = ") AND ("
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5 = 41
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = " UNION "
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = "SELECT userDB.auricular._id, lower(userDB.auricular.point), userDB.auricular.pointcat, 0, 0, 0, 0, 0 FROM userDB.auricular "
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = " WHERE (userDB"
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = ") AND ("
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r3 = ") "
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r3 = " ORDER BY 2"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x010c:
            com.cyberandsons.tcmaidtrial.TcmAid.o = r0     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.g     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r0 == 0) goto L_0x0119
            java.lang.String r0 = "TCA:Search Auricular - "
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.TcmAid.o     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r0, r3)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x0119:
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.o     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r3 = 0
            android.database.Cursor r0 = r15.rawQuery(r0, r3)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0f26, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 == 0) goto L_0x013c
        L_0x0128:
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0f26, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.i     // Catch:{ SQLException -> 0x0f26, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ SQLException -> 0x0f26, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r3.add(r1)     // Catch:{ SQLException -> 0x0f26, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0f26, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 != 0) goto L_0x0128
        L_0x013c:
            java.lang.String r1 = "TCA:auricular count = "
            int r3 = r0.getCount()     // Catch:{ SQLException -> 0x0f26, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x0f26, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r1, r3)     // Catch:{ SQLException -> 0x0f26, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0f26, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r1 + 0
            r0.close()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.s     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r3 = r3 + 1
            com.cyberandsons.tcmaidtrial.TcmAid.s = r3     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.t     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.o     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r3.add(r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.R     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r3 == 0) goto L_0x0195
            java.lang.String r3 = "TCA:doTheSearch():"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.<init>()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = "auCounter++ = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = com.cyberandsons.tcmaidtrial.TcmAid.s     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = ", auCounterArrary.count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r5 = com.cyberandsons.tcmaidtrial.TcmAid.t     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = r5.size()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x0195:
            r3 = r1
            r1 = r0
            r0 = r10
        L_0x0198:
            boolean r4 = r2.e()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 == 0) goto L_0x0306
            java.lang.String r4 = r2.E()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String[] r5 = com.cyberandsons.tcmaidtrial.a.t.a()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String[] r6 = com.cyberandsons.tcmaidtrial.a.t.b()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            a(r4, r5, r6)     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r4 = r4.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 <= 0) goto L_0x0216
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r4 = r4.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 <= 0) goto L_0x0216
            r0 = 0
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = b(r13, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = b(r13, r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.a.t.a(r0, r4, r5, r2)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            com.cyberandsons.tcmaidtrial.TcmAid.F = r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.j     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r0 == 0) goto L_0x01db
            java.lang.String r0 = "TCA:Search Diagnosis (Patterns by Internal Organs) - "
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.F     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x01db:
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.F     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4 = 0
            android.database.Cursor r0 = r15.rawQuery(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 == 0) goto L_0x01fe
        L_0x01ea:
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.w     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.add(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 != 0) goto L_0x01ea
        L_0x01fe:
            java.lang.String r1 = "TCA:zf count = "
            int r4 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r1, r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r1 + r3
            r0.close()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r3 = r1
            r1 = r0
            r0 = r10
        L_0x0216:
            java.lang.String r4 = r2.E()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String[] r5 = com.cyberandsons.tcmaidtrial.a.t.a()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String[] r6 = com.cyberandsons.tcmaidtrial.a.t.b()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            a(r4, r5, r6)     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r4 = r4.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 <= 0) goto L_0x028e
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r4 = r4.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 <= 0) goto L_0x028e
            r0 = 1
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = b(r13, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = b(r13, r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.a.t.a(r0, r4, r5, r2)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            com.cyberandsons.tcmaidtrial.TcmAid.G = r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.j     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r0 == 0) goto L_0x0253
            java.lang.String r0 = "TCA:Search Diagnosis (Four Levels) - "
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.G     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x0253:
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.G     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4 = 0
            android.database.Cursor r0 = r15.rawQuery(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 == 0) goto L_0x0276
        L_0x0262:
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.x     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.add(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 != 0) goto L_0x0262
        L_0x0276:
            java.lang.String r1 = "TCA:fl count = "
            int r4 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r1, r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r1 + r3
            r0.close()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r3 = r1
            r1 = r0
            r0 = r10
        L_0x028e:
            java.lang.String r4 = r2.E()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String[] r5 = com.cyberandsons.tcmaidtrial.a.t.a()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String[] r6 = com.cyberandsons.tcmaidtrial.a.t.b()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            a(r4, r5, r6)     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r4 = r4.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 <= 0) goto L_0x0306
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r4 = r4.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 <= 0) goto L_0x0306
            r0 = 3
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = b(r13, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = b(r13, r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.a.t.a(r0, r4, r5, r2)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            com.cyberandsons.tcmaidtrial.TcmAid.H = r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.j     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r0 == 0) goto L_0x02cb
            java.lang.String r0 = "TCA:Search Diagnosis (Residual Pathogenic) - "
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.H     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x02cb:
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.H     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4 = 0
            android.database.Cursor r0 = r15.rawQuery(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 == 0) goto L_0x02ee
        L_0x02da:
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.y     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.add(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 != 0) goto L_0x02da
        L_0x02ee:
            java.lang.String r1 = "TCA:rp count = "
            int r4 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r1, r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r1 + r3
            r0.close()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r3 = r1
            r1 = r0
            r0 = r10
        L_0x0306:
            boolean r4 = r2.g()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 == 0) goto L_0x04f7
            int r4 = r2.O()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 == 0) goto L_0x0314
            if (r4 != r10) goto L_0x0e23
        L_0x0314:
            java.lang.String r4 = "pinyin"
        L_0x0316:
            java.lang.String r5 = r2.G()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 16
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 0
            java.lang.String r8 = "main.materiamedica.pinyin"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 1
            java.lang.String r8 = "main.materiamedica.english"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 2
            java.lang.String r8 = "main.materiamedica.botanical"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 3
            java.lang.String r8 = "main.materiamedica.meridians"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 4
            java.lang.String r8 = "main.materiamedica.taste"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 5
            java.lang.String r8 = "main.materiamedica.temp"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 6
            java.lang.String r8 = "main.materiamedica.functions"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 7
            java.lang.String r8 = "main.materiamedica.indications"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 8
            java.lang.String r8 = "main.materiamedica.contraindication"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 9
            java.lang.String r8 = "main.materiamedica.dosage"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 10
            java.lang.String r8 = "main.materiamedica.cautions"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 11
            java.lang.String r8 = "main.materiamedica.combinations"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 12
            java.lang.String r8 = "main.materiamedica.general_functions"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 13
            java.lang.String r8 = "main.materiamedica.toxicology"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 14
            java.lang.String r8 = "main.materiamedica.pharmacology"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 15
            java.lang.String r8 = "main.materiamedica.remarks"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 16
            java.lang.String[] r7 = new java.lang.String[r7]     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 0
            java.lang.String r9 = "userDB.materiamedica.pinyin"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 1
            java.lang.String r9 = "userDB.materiamedica.english"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 2
            java.lang.String r9 = "userDB.materiamedica.botanical"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 3
            java.lang.String r9 = "userDB.materiamedica.meridians"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 4
            java.lang.String r9 = "userDB.materiamedica.taste"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 5
            java.lang.String r9 = "userDB.materiamedica.temp"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 6
            java.lang.String r9 = "userDB.materiamedica.functions"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 7
            java.lang.String r9 = "userDB.materiamedica.indications"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 8
            java.lang.String r9 = "userDB.materiamedica.contraindication"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 9
            java.lang.String r9 = "userDB.materiamedica.dosage"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 10
            java.lang.String r9 = "userDB.materiamedica.cautions"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 11
            java.lang.String r9 = "userDB.materiamedica.combinations"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 12
            java.lang.String r9 = "userDB.materiamedica.general_functions"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 13
            java.lang.String r9 = "userDB.materiamedica.toxicology"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 14
            java.lang.String r9 = "userDB.materiamedica.pharmacology"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 15
            java.lang.String r9 = "userDB.materiamedica.remarks"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            a(r5, r6, r7)     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = r5.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r5 <= 0) goto L_0x04f7
            java.lang.String r5 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = r5.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r5 <= 0) goto L_0x04f7
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = b(r13, r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = b(r13, r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r6 = r2.z()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r7 = r2.q()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 0
            if (r6 == 0) goto L_0x0f56
            int r6 = r7.length()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r6 <= 0) goto L_0x0f56
            java.lang.String r6 = "materiamedica"
            java.lang.String r8 = "_id"
            java.lang.String r6 = com.cyberandsons.tcmaidtrial.a.q.a(r6, r8, r7)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r7 = r6.length()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r7 <= 0) goto L_0x0f53
            r7 = r10
        L_0x0411:
            if (r7 == 0) goto L_0x0e2d
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7.<init>()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r8 = com.cyberandsons.tcmaidtrial.a.m.b(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r8 = " WHERE (main"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r7 = r7.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r8 = ") AND ("
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 41
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r7 = " UNION "
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.a.m.c(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = " WHERE (userDB"
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = ") AND ("
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = ") "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = " ORDER BY 2"
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x046c:
            com.cyberandsons.tcmaidtrial.TcmAid.af = r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.h     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r0 == 0) goto L_0x0479
            java.lang.String r0 = "TCA:Search Herbs - "
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.af     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x0479:
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.af     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4 = 0
            android.database.Cursor r0 = r15.rawQuery(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 == 0) goto L_0x049c
        L_0x0488:
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.Z     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.add(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 != 0) goto L_0x0488
        L_0x049c:
            java.lang.String r1 = "TCA:herbs count = "
            int r4 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r1, r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r1 + r3
            r0.close()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.ai     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r3 = r3 + 1
            com.cyberandsons.tcmaidtrial.TcmAid.ai = r3     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.aj     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.af     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r3.add(r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.U     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r3 == 0) goto L_0x04f4
            java.lang.String r3 = "TCA:doTheSearch():"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.<init>()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = "hbCounter++ = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = com.cyberandsons.tcmaidtrial.TcmAid.ai     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = ", hbCounterArrary.count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r5 = com.cyberandsons.tcmaidtrial.TcmAid.aj     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = r5.size()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x04f4:
            r3 = r1
            r1 = r0
            r0 = r10
        L_0x04f7:
            boolean r4 = r2.f()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 == 0) goto L_0x06c4
            int r4 = r2.N()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 == 0) goto L_0x0505
            if (r4 != r10) goto L_0x0e6e
        L_0x0505:
            java.lang.String r4 = "pinyin"
        L_0x0507:
            java.lang.String r5 = r2.F()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 13
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 0
            java.lang.String r8 = "main.formulas.pinyin"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 1
            java.lang.String r8 = "main.formulas.english"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 2
            java.lang.String r8 = "main.formulas.diagnosis"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 3
            java.lang.String r8 = "main.formulas.symptoms"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 4
            java.lang.String r8 = "main.formulas.functions"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 5
            java.lang.String r8 = "main.formulas.tongue"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 6
            java.lang.String r8 = "main.formulas.pulse"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 7
            java.lang.String r8 = "main.formulas.ingredients"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 8
            java.lang.String r8 = "main.formulas.herb_breakdown"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 9
            java.lang.String r8 = "main.formulas.modification"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 10
            java.lang.String r8 = "main.formulas.contraindication"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 11
            java.lang.String r8 = "main.formulas.western_use"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 12
            java.lang.String r8 = "main.formulas.notes"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 13
            java.lang.String[] r7 = new java.lang.String[r7]     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 0
            java.lang.String r9 = "userDB.formulas.pinyin"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 1
            java.lang.String r9 = "userDB.formulas.english"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 2
            java.lang.String r9 = "userDB.formulas.diagnosis"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 3
            java.lang.String r9 = "userDB.formulas.symptoms"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 4
            java.lang.String r9 = "userDB.formulas.functions"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 5
            java.lang.String r9 = "userDB.formulas.tongue"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 6
            java.lang.String r9 = "userDB.formulas.pulse"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 7
            java.lang.String r9 = "userDB.formulas.ingredients"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 8
            java.lang.String r9 = "userDB.formulas.herb_breakdown"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 9
            java.lang.String r9 = "userDB.formulas.modification"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 10
            java.lang.String r9 = "userDB.formulas.contraindication"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 11
            java.lang.String r9 = "userDB.formulas.western_use"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 12
            java.lang.String r9 = "userDB.formulas.notes"
            r7[r8] = r9     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            a(r5, r6, r7)     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = r5.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r5 <= 0) goto L_0x06c4
            java.lang.String r5 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = r5.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r5 <= 0) goto L_0x06c4
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = b(r13, r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = b(r13, r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r6 = r2.z()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r7 = r2.p()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 0
            if (r6 == 0) goto L_0x0f4f
            int r6 = r7.length()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r6 <= 0) goto L_0x0f4f
            java.lang.String r6 = "formulas"
            java.lang.String r8 = "_id"
            java.lang.String r6 = com.cyberandsons.tcmaidtrial.a.q.a(r6, r8, r7)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r7 = r6.length()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r7 <= 0) goto L_0x0f4c
            r7 = r10
        L_0x05de:
            if (r7 == 0) goto L_0x0e72
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7.<init>()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r8 = com.cyberandsons.tcmaidtrial.a.c.b(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r8 = " WHERE (main"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r7 = r7.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r8 = ") AND main.formulas.useiPhone=1 AND ("
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 41
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r7 = " UNION "
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.a.c.c(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = " WHERE (userDB"
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = ") AND ("
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = ") "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = " ORDER BY 2"
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x0639:
            com.cyberandsons.tcmaidtrial.TcmAid.S = r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.i     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r0 == 0) goto L_0x0646
            java.lang.String r0 = "TCA:Search Formulas - "
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.S     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x0646:
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.S     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4 = 0
            android.database.Cursor r0 = r15.rawQuery(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 == 0) goto L_0x0669
        L_0x0655:
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.M     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.add(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 != 0) goto L_0x0655
        L_0x0669:
            java.lang.String r1 = "TCA:formulas count = "
            int r4 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r1, r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r1 + r3
            r0.close()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.W     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r3 = r3 + 1
            com.cyberandsons.tcmaidtrial.TcmAid.W = r3     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.X     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.S     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r3.add(r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.T     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r3 == 0) goto L_0x06c1
            java.lang.String r3 = "TCA:doTheSearch():"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.<init>()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = "fxCounter++ = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = com.cyberandsons.tcmaidtrial.TcmAid.W     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = ", fxCounterArrary.count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r5 = com.cyberandsons.tcmaidtrial.TcmAid.X     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = r5.size()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x06c1:
            r3 = r1
            r1 = r0
            r0 = r10
        L_0x06c4:
            boolean r4 = r2.h()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 == 0) goto L_0x087d
            java.lang.String r4 = r2.H()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5 = 12
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 0
            java.lang.String r7 = "main.pointslocation.common_name"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 1
            java.lang.String r7 = "main.pointslocation.location"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 2
            java.lang.String r7 = "main.pointslocation.englishname"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 3
            java.lang.String r7 = "main.pointslocation.indication"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 4
            java.lang.String r7 = "main.pointslocation.func"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 5
            java.lang.String r7 = "main.pointslocation.needling_method"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 6
            java.lang.String r7 = "main.pointslocation.contraindication"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 7
            java.lang.String r7 = "main.pointslocation.localapp"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 8
            java.lang.String r7 = "main.pointslocation.misc"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 9
            java.lang.String r7 = "main.pointslocation.needlingcombo"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 10
            java.lang.String r7 = "main.pointslocation.notes"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 11
            java.lang.String r7 = "main.pointslocation.name"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 12
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 0
            java.lang.String r8 = "userDB.pointslocation.common_name"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 1
            java.lang.String r8 = "userDB.pointslocation.location"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 2
            java.lang.String r8 = "userDB.pointslocation.englishname"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 3
            java.lang.String r8 = "userDB.pointslocation.indication"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 4
            java.lang.String r8 = "userDB.pointslocation.func"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 5
            java.lang.String r8 = "userDB.pointslocation.needling_method"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 6
            java.lang.String r8 = "userDB.pointslocation.contraindication"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 7
            java.lang.String r8 = "userDB.pointslocation.localapp"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 8
            java.lang.String r8 = "userDB.pointslocation.misc"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 9
            java.lang.String r8 = "userDB.pointslocation.needlingcombo"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 10
            java.lang.String r8 = "userDB.pointslocation.commentary"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 11
            java.lang.String r8 = "userDB.pointslocation.name"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            a(r4, r5, r6)     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r4 = r4.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 <= 0) goto L_0x087d
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r4 = r4.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 <= 0) goto L_0x087d
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = b(r13, r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = b(r13, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r5 = r2.z()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = r2.r()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r7 = r2.B()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r8 = 0
            if (r5 == 0) goto L_0x0f48
            int r5 = r6.length()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r5 <= 0) goto L_0x0f48
            java.lang.String r5 = "pointslocation"
            java.lang.String r8 = "_id"
            java.lang.String r5 = com.cyberandsons.tcmaidtrial.a.q.a(r5, r8, r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r6 = r5.length()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r6 <= 0) goto L_0x0f45
            r6 = r10
        L_0x0799:
            if (r6 == 0) goto L_0x0ebd
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6.<init>()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r8 = "SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian "
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r8 = " WHERE (main"
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r6 = r6.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r8 = ") AND ("
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r6.append(r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 41
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = " UNION "
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = "SELECT userDB.pointslocation._id, main.meridian.abbr_name, userDB.pointslocation.pointnum, userDB.pointslocation.name, userDB.pointslocation.englishname, userDB.pointslocation.indication FROM userDB.pointslocation LEFT JOIN main.meridian ON main.meridian._id = userDB.pointslocation.meridian "
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = " WHERE (userDB"
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = ") AND ("
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = ") "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r7 == 0) goto L_0x0eb9
            java.lang.String r4 = ""
        L_0x07ea:
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x07f2:
            com.cyberandsons.tcmaidtrial.TcmAid.ar = r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.f     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r0 == 0) goto L_0x07ff
            java.lang.String r0 = "TCA:Search Points - "
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.ar     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x07ff:
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.ar     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4 = 0
            android.database.Cursor r0 = r15.rawQuery(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 == 0) goto L_0x0822
        L_0x080e:
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.al     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.add(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 != 0) goto L_0x080e
        L_0x0822:
            java.lang.String r1 = "TCA:points count = "
            int r4 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r1, r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r1 + r3
            r0.close()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.au     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r3 = r3 + 1
            com.cyberandsons.tcmaidtrial.TcmAid.au = r3     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.aw     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.ar     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r3.add(r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.W     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r3 == 0) goto L_0x087a
            java.lang.String r3 = "TCA:doTheSearch():"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.<init>()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = "ptCounter++ = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = com.cyberandsons.tcmaidtrial.TcmAid.au     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = ", ptCounterArrary.count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r5 = com.cyberandsons.tcmaidtrial.TcmAid.aw     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = r5.size()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x087a:
            r3 = r1
            r1 = r0
            r0 = r10
        L_0x087d:
            boolean r4 = r2.i()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 == 0) goto L_0x0a42
            java.lang.String r4 = r2.J()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5 = 18
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 0
            java.lang.String r7 = "main.pulsediag.name"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 1
            java.lang.String r7 = "main.pulsediag.pinyin"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 2
            java.lang.String r7 = "main.pulsediag.category"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 3
            java.lang.String r7 = "main.pulsediag.description"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 4
            java.lang.String r7 = "main.pulsediag.sensation"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 5
            java.lang.String r7 = "main.pulsediag.indications"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 6
            java.lang.String r7 = "main.pulsediag.left_cun"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 7
            java.lang.String r7 = "main.pulsediag.left_guan"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 8
            java.lang.String r7 = "main.pulsediag.left_chi"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 9
            java.lang.String r7 = "main.pulsediag.right_cun"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 10
            java.lang.String r7 = "main.pulsediag.right_guan"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 11
            java.lang.String r7 = "main.pulsediag.right_chi"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 12
            java.lang.String r7 = "main.pulsediag.bilateral_cun"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 13
            java.lang.String r7 = "main.pulsediag.bilateral_guan"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 14
            java.lang.String r7 = "main.pulsediag.bilateral_chi"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 15
            java.lang.String r7 = "main.pulsediag.combinations"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 16
            java.lang.String r7 = "main.pulsediag.alternate_names"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 17
            java.lang.String r7 = "main.pulsediag.notes"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 18
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 0
            java.lang.String r8 = "userDB.pulsediag.name"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 1
            java.lang.String r8 = "userDB.pulsediag.pinyin"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 2
            java.lang.String r8 = "userDB.pulsediag.category"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 3
            java.lang.String r8 = "userDB.pulsediag.description"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 4
            java.lang.String r8 = "userDB.pulsediag.sensation"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 5
            java.lang.String r8 = "userDB.pulsediag.indications"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 6
            java.lang.String r8 = "userDB.pulsediag.left_cun"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 7
            java.lang.String r8 = "userDB.pulsediag.left_guan"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 8
            java.lang.String r8 = "userDB.pulsediag.left_chi"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 9
            java.lang.String r8 = "userDB.pulsediag.right_cun"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 10
            java.lang.String r8 = "userDB.pulsediag.right_guan"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 11
            java.lang.String r8 = "userDB.pulsediag.right_chi"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 12
            java.lang.String r8 = "userDB.pulsediag.bilateral_cun"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 13
            java.lang.String r8 = "userDB.pulsediag.bilateral_guan"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 14
            java.lang.String r8 = "userDB.pulsediag.bilateral_chi"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 15
            java.lang.String r8 = "userDB.pulsediag.combinations"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 16
            java.lang.String r8 = "userDB.pulsediag.alternate_names"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 17
            java.lang.String r8 = "userDB.pulsediag.notes"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            a(r4, r5, r6)     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r4 = r4.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 <= 0) goto L_0x0a42
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r4 = r4.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 <= 0) goto L_0x0a42
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = b(r13, r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = b(r13, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5.<init>()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = "SELECT main.pulsediag._id, main.pulsediag.name, main.pulsediag.category FROM main.pulsediag "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = " WHERE ("
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = ") "
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = " UNION "
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = "SELECT userDB.pulsediag._id, userDB.pulsediag.name, userDB.pulsediag.category FROM userDB.pulsediag "
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = " WHERE ("
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = ") "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = " ORDER BY 2"
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            com.cyberandsons.tcmaidtrial.TcmAid.aF = r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.j     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r0 == 0) goto L_0x09c4
            java.lang.String r0 = "TCA:Search Pulse Diag - "
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.aF     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x09c4:
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.aF     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4 = 0
            android.database.Cursor r0 = r15.rawQuery(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 == 0) goto L_0x09e7
        L_0x09d3:
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.az     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.add(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 != 0) goto L_0x09d3
        L_0x09e7:
            java.lang.String r1 = "TCA:pulse count = "
            int r4 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r1, r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r1 + r3
            r0.close()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.aI     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r3 = r3 + 1
            com.cyberandsons.tcmaidtrial.TcmAid.aI = r3     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.aJ     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.aF     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r3.add(r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.V     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r3 == 0) goto L_0x0a3f
            java.lang.String r3 = "TCA:doTheSearch():"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.<init>()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = "pdCounter++ = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = com.cyberandsons.tcmaidtrial.TcmAid.aI     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = ", pdCounterArrary.count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r5 = com.cyberandsons.tcmaidtrial.TcmAid.aJ     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = r5.size()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x0a3f:
            r3 = r1
            r1 = r0
            r0 = r10
        L_0x0a42:
            boolean r4 = r2.u()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 == 0) goto L_0x0b83
            java.lang.String r4 = r2.K()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5 = 8
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 0
            java.lang.String r7 = "main.sixchannels.pathology"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 1
            java.lang.String r7 = "main.sixchannels.english"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 2
            java.lang.String r7 = "main.sixchannels.symptoms"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 3
            java.lang.String r7 = "main.sixchannels.tongue"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 4
            java.lang.String r7 = "main.sixchannels.pulse"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 5
            java.lang.String r7 = "main.sixchannels.points"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 6
            java.lang.String r7 = "main.sixchannels.formula"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 7
            java.lang.String r7 = "main.sixchannels.notes"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 8
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 0
            java.lang.String r8 = "userDB.sixchannels.pathology"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 1
            java.lang.String r8 = "userDB.sixchannels.english"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 2
            java.lang.String r8 = "userDB.sixchannels.symptoms"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 3
            java.lang.String r8 = "userDB.sixchannels.tongue"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 4
            java.lang.String r8 = "userDB.sixchannels.pulse"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 5
            java.lang.String r8 = "userDB.sixchannels.points"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 6
            java.lang.String r8 = "userDB.sixchannels.formula"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 7
            java.lang.String r8 = "userDB.sixchannels.notes"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            a(r4, r5, r6)     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r4 = r4.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 <= 0) goto L_0x0b83
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r4 = r4.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 <= 0) goto L_0x0b83
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = b(r13, r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = b(r13, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5.<init>()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = "SELECT main.sixchannels._id, lower(main.sixchannels.pathology) FROM main.sixchannels "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = " WHERE "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = " UNION "
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = "SELECT userDB.sixchannels._id, lower(userDB.sixchannels.pathology) FROM userDB.sixchannels "
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = " WHERE ("
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = ") ORDER BY 2 "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            com.cyberandsons.tcmaidtrial.TcmAid.aP = r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.j     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r0 == 0) goto L_0x0b05
            java.lang.String r0 = "TCA:Search Six Channels - "
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.aP     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x0b05:
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.aP     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4 = 0
            android.database.Cursor r0 = r15.rawQuery(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 == 0) goto L_0x0b28
        L_0x0b14:
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.aL     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.add(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 != 0) goto L_0x0b14
        L_0x0b28:
            java.lang.String r1 = "TCA:six count = "
            int r4 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r1, r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r1 + r3
            r0.close()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.aR     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r3 = r3 + 1
            com.cyberandsons.tcmaidtrial.TcmAid.aR = r3     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.aS     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.aP     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r3.add(r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.X     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r3 == 0) goto L_0x0b80
            java.lang.String r3 = "TCA:doTheSearch():"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.<init>()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = "ssCounter++ = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = com.cyberandsons.tcmaidtrial.TcmAid.aR     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = ", ssCounterArrary.count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r5 = com.cyberandsons.tcmaidtrial.TcmAid.aS     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = r5.size()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x0b80:
            r3 = r1
            r1 = r0
            r0 = r10
        L_0x0b83:
            boolean r4 = r2.j()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 == 0) goto L_0x0cc4
            java.lang.String r4 = r2.L()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5 = 8
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 0
            java.lang.String r7 = "main.mastertung.point_number"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 1
            java.lang.String r7 = "main.mastertung.name"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 2
            java.lang.String r7 = "main.mastertung.english_name"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 3
            java.lang.String r7 = "main.mastertung.location"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 4
            java.lang.String r7 = "main.mastertung.indications"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 5
            java.lang.String r7 = "main.mastertung.anatomy"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 6
            java.lang.String r7 = "main.mastertung.preparation"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 7
            java.lang.String r7 = "main.mastertung.note"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 8
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 0
            java.lang.String r8 = "userDB.mastertung.number"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 1
            java.lang.String r8 = "userDB.mastertung.points"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 2
            java.lang.String r8 = "userDB.mastertung.english_name"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 3
            java.lang.String r8 = "userDB.mastertung.location"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 4
            java.lang.String r8 = "userDB.mastertung.description"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 5
            java.lang.String r8 = "userDB.mastertung.anatomy"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 6
            java.lang.String r8 = "userDB.mastertung.preparation"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r7 = 7
            java.lang.String r8 = "userDB.mastertung.note"
            r6[r7] = r8     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            a(r4, r5, r6)     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r4 = r4.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 <= 0) goto L_0x0cc4
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r4 = r4.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 <= 0) goto L_0x0cc4
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = b(r13, r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = b(r13, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5.<init>()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = "SELECT main.mastertung._id, main.mastertung.point_number, main.mastertung.name, main.mastertung.english_name FROM main.mastertung "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = " WHERE "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = " UNION "
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = "SELECT userDB.mastertung._id, userDB.mastertung.number, userDB.mastertung.points, userDB.mastertung.english_name FROM userDB.mastertung "
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = " WHERE ("
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = ") ORDER BY 3"
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            com.cyberandsons.tcmaidtrial.TcmAid.aY = r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.k     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r0 == 0) goto L_0x0c46
            java.lang.String r0 = "TCA:Search Tung - "
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.aY     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x0c46:
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.aY     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4 = 0
            android.database.Cursor r0 = r15.rawQuery(r0, r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 == 0) goto L_0x0c69
        L_0x0c55:
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.aU     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.add(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 != 0) goto L_0x0c55
        L_0x0c69:
            java.lang.String r1 = "TCA:tung count = "
            int r4 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r1, r4)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r1 + r3
            r0.close()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.bb     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r3 = r3 + 1
            com.cyberandsons.tcmaidtrial.TcmAid.bb = r3     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r3 = com.cyberandsons.tcmaidtrial.TcmAid.bc     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.TcmAid.aY     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r3.add(r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.Y     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r3 == 0) goto L_0x0cc1
            java.lang.String r3 = "TCA:doTheSearch():"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.<init>()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = "tuCounter++ = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = com.cyberandsons.tcmaidtrial.TcmAid.bb     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = ", tuCounterArrary.count = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r5 = com.cyberandsons.tcmaidtrial.TcmAid.bc     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r5 = r5.size()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x0cc1:
            r3 = r1
            r1 = r0
            r0 = r10
        L_0x0cc4:
            boolean r4 = r2.k()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r4 == 0) goto L_0x0f42
            java.lang.String r2 = r2.M()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4 = 4
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5 = 0
            java.lang.String r6 = "main.wristankle.zone"
            r4[r5] = r6     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5 = 1
            java.lang.String r6 = "main.wristankle.location"
            r4[r5] = r6     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5 = 2
            java.lang.String r6 = "main.wristankle.indications"
            r4[r5] = r6     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5 = 3
            java.lang.String r6 = "main.wristankle.notes"
            r4[r5] = r6     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5 = 4
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 0
            java.lang.String r7 = "userDB.wristankle.zone"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 1
            java.lang.String r7 = "userDB.wristankle.location"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 2
            java.lang.String r7 = "userDB.wristankle.indications"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 3
            java.lang.String r7 = "userDB.wristankle.notes"
            r5[r6] = r7     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            a(r2, r4, r5)     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r2 = r2.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r2 <= 0) goto L_0x0f42
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r2 = r2.length()     // Catch:{ SQLException -> 0x0f33, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r2 <= 0) goto L_0x0f42
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.dU     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = b(r13, r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.TcmAid.dV     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r2 = b(r13, r2)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.<init>()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = "SELECT main.wristankle._id, main.wristankle.zone FROM main.wristankle"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = " WHERE "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = " UNION "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = "SELECT userDB.wristankle._id, userDB.wristankle.zone FROM userDB.wristankle "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = " WHERE ("
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r2 = ") ORDER BY 2 "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            com.cyberandsons.tcmaidtrial.TcmAid.bh = r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.l     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r0 == 0) goto L_0x0d5d
            java.lang.String r0 = "TCA:Search WristAnkle - "
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.TcmAid.bh     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r0, r2)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x0d5d:
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.bh     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r2 = 0
            android.database.Cursor r0 = r15.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 == 0) goto L_0x0d80
        L_0x0d6c:
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.be     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r2.add(r1)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r1 != 0) goto L_0x0d6c
        L_0x0d80:
            java.lang.String r1 = "TCA:w/a count = "
            int r2 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r1, r2)     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0f3d, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r1 = r1 + r3
            r0.close()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.bj     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r2 = r2 + 1
            com.cyberandsons.tcmaidtrial.TcmAid.bj = r2     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r2 = com.cyberandsons.tcmaidtrial.TcmAid.bk     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.TcmAid.bh     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r2.add(r3)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.Z     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r2 == 0) goto L_0x0dd8
            java.lang.String r2 = "TCA:doTheSearch():"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r3.<init>()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = "waCounter++ = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r4 = com.cyberandsons.tcmaidtrial.TcmAid.bj     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = ", waCounterArrary.count = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.util.ArrayList r4 = com.cyberandsons.tcmaidtrial.TcmAid.bk     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            int r4 = r4.size()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            android.util.Log.d(r2, r3)     // Catch:{ SQLException -> 0x0f2c, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
        L_0x0dd8:
            r0 = r10
        L_0x0dd9:
            if (r0 != 0) goto L_0x0f1d
            if (r1 != 0) goto L_0x0f1d
            r0 = -1
            com.cyberandsons.tcmaidtrial.TcmAid.cR = r0
        L_0x0de0:
            if (r14 == 0) goto L_0x0de5
            com.cyberandsons.tcmaidtrial.a.j.a(r13, r15)
        L_0x0de5:
            return r1
        L_0x0de6:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r4.<init>()     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = "SELECT main.auricular._id, lower(main.auricular.point), main.auricular.pointcat, main.auricular.nada, main.auricular.battlefield, main.auricular.acacd, main.auricular.stopsmoking, main.auricular.weightloss FROM main.auricular "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = " WHERE "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = " UNION "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = "SELECT userDB.auricular._id, lower(userDB.auricular.point), userDB.auricular.pointcat, 0, 0, 0, 0, 0 FROM userDB.auricular "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = " WHERE ("
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r3 = ") "
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r3 = " ORDER BY 2"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0f21, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            goto L_0x010c
        L_0x0e23:
            if (r4 != r7) goto L_0x0e29
            java.lang.String r4 = "english"
            goto L_0x0316
        L_0x0e29:
            java.lang.String r4 = "botanical"
            goto L_0x0316
        L_0x0e2d:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6.<init>()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r7 = com.cyberandsons.tcmaidtrial.a.m.b(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r7 = " WHERE "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r6.append(r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6 = 32
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = " UNION "
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.a.m.c(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = " WHERE ("
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = ") ORDER BY 2"
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            goto L_0x046c
        L_0x0e6e:
            java.lang.String r4 = "english"
            goto L_0x0507
        L_0x0e72:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r6.<init>()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r7 = com.cyberandsons.tcmaidtrial.a.c.b(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r7 = " WHERE main.formulas.useiPhone=1 AND ("
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r6.append(r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = ") "
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = " UNION "
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = com.cyberandsons.tcmaidtrial.a.c.c(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = " WHERE ("
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = ") "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r4 = " ORDER BY 2"
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            goto L_0x0639
        L_0x0eb9:
            java.lang.String r4 = " ORDER BY 2, 3"
            goto L_0x07ea
        L_0x0ebd:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5.<init>()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = "SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r6 = " WHERE "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            r5 = 32
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = " UNION "
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = "SELECT userDB.pointslocation._id, main.meridian.abbr_name, userDB.pointslocation.pointnum, userDB.pointslocation.name, userDB.pointslocation.englishname, userDB.pointslocation.indication FROM userDB.pointslocation LEFT JOIN main.meridian ON main.meridian._id = userDB.pointslocation.meridian "
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r5 = " WHERE "
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            if (r7 == 0) goto L_0x0efc
            java.lang.String r4 = ""
        L_0x0ef2:
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0f39, NullPointerException -> 0x0f0f, Exception -> 0x0f16 }
            goto L_0x07f2
        L_0x0efc:
            java.lang.String r4 = " ORDER BY 2, 3"
            goto L_0x0ef2
        L_0x0eff:
            r0 = move-exception
            r2 = r1
            r3 = r11
            r1 = r11
        L_0x0f03:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)
            if (r2 == 0) goto L_0x0f0b
            r2.close()
        L_0x0f0b:
            r0 = r1
            r1 = r3
            goto L_0x0dd9
        L_0x0f0f:
            r0 = move-exception
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        L_0x0f16:
            r0 = move-exception
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        L_0x0f1d:
            com.cyberandsons.tcmaidtrial.TcmAid.cR = r10
            goto L_0x0de0
        L_0x0f21:
            r0 = move-exception
            r2 = r1
            r3 = r11
            r1 = r10
            goto L_0x0f03
        L_0x0f26:
            r1 = move-exception
            r2 = r0
            r3 = r11
            r0 = r1
            r1 = r10
            goto L_0x0f03
        L_0x0f2c:
            r2 = move-exception
            r3 = r1
            r1 = r10
            r12 = r0
            r0 = r2
            r2 = r12
            goto L_0x0f03
        L_0x0f33:
            r2 = move-exception
            r12 = r2
            r2 = r1
            r1 = r0
            r0 = r12
            goto L_0x0f03
        L_0x0f39:
            r0 = move-exception
            r2 = r1
            r1 = r10
            goto L_0x0f03
        L_0x0f3d:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r10
            goto L_0x0f03
        L_0x0f42:
            r1 = r3
            goto L_0x0dd9
        L_0x0f45:
            r6 = r11
            goto L_0x0799
        L_0x0f48:
            r5 = r8
            r6 = r11
            goto L_0x0799
        L_0x0f4c:
            r7 = r11
            goto L_0x05de
        L_0x0f4f:
            r6 = r8
            r7 = r11
            goto L_0x05de
        L_0x0f53:
            r7 = r11
            goto L_0x0411
        L_0x0f56:
            r6 = r8
            r7 = r11
            goto L_0x0411
        L_0x0f5a:
            r5 = r11
            goto L_0x00b5
        L_0x0f5d:
            r4 = r6
            r5 = r11
            goto L_0x00b5
        L_0x0f61:
            r0 = r11
            r3 = r11
            goto L_0x0198
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.TcmAid.a(java.lang.String, boolean, android.database.sqlite.SQLiteDatabase):int");
    }

    private static void d() {
        o = null;
        p = null;
        q = null;
        E = null;
        G = null;
        H = null;
        F = null;
        S = null;
        T = null;
        af = null;
        ag = null;
        as = null;
        ar = null;
        dz = null;
        aF = null;
        aG = null;
        dB = null;
        aP = null;
        aY = null;
        aZ = null;
        bh = null;
    }

    private static void e() {
        cV = -1;
        cU = -1;
        cW = -1;
        cX = -1;
        dW = -1;
    }

    private void f() {
        try {
            if (this.dr != null && this.dr.isOpen()) {
                this.dr.close();
                this.dr = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
    }

    private void a(boolean z2) {
        if (this.dr == null || !this.dr.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("TA:openDataBase()", str + " opened.");
                this.dr = SQLiteDatabase.openDatabase(str, null, 16);
                this.dr.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.dr.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("TA:openDataBase()", str2 + " ATTACHed.");
                if (z2) {
                    this.ds = new n();
                    this.ds.a(this.dr);
                }
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to open.");
                create.setButton("OK", new f(this));
                create.show();
            }
        }
    }

    /* access modifiers changed from: private */
    public SQLiteDatabase a(SQLiteDatabase sQLiteDatabase, boolean z2) {
        SQLiteDatabase sQLiteDatabase2;
        if (sQLiteDatabase != null && sQLiteDatabase.isOpen()) {
            return sQLiteDatabase;
        }
        try {
            String string = getString(C0000R.string.tcmDatabasePath);
            String string2 = getString(C0000R.string.tcmUserDatabase);
            String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
            Log.d("TA:openDataBase()", str + " opened.");
            sQLiteDatabase2 = SQLiteDatabase.openDatabase(str, null, 16);
            try {
                sQLiteDatabase2.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                sQLiteDatabase2.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("TA:openDataBase()", str2 + " ATTACHed.");
                if (!z2) {
                    return sQLiteDatabase2;
                }
                this.ds = new n();
                this.ds.a(sQLiteDatabase2);
                return sQLiteDatabase2;
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to open.");
                create.setButton("OK", new l(this));
                create.show();
                return sQLiteDatabase2;
            }
        } catch (SQLException e3) {
            sQLiteDatabase2 = sQLiteDatabase;
            AlertDialog create2 = new AlertDialog.Builder(this).create();
            create2.setTitle("Attention:");
            create2.setMessage("Please restart TCM Clinic Aid. The database was not able to open.");
            create2.setButton("OK", new l(this));
            create2.show();
            return sQLiteDatabase2;
        }
    }

    private static void a(String str, String[] strArr, String[] strArr2) {
        dU = "";
        dV = "";
        if (str.length() != 0) {
            boolean z2 = true;
            for (int i2 = 0; i2 < strArr.length; i2++) {
                String str2 = strArr[i2];
                if (n.a(str, i2)) {
                    if (z2) {
                        dU = str2;
                        dV = strArr2[i2];
                        z2 = false;
                    } else {
                        dU += ',' + str2;
                        dV += ',' + strArr2[i2];
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private static String b(String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        String[] split = str2.split(",");
        int i2 = 0;
        int i3 = 0;
        while (i2 < split.length) {
            String str3 = split[i2];
            if (i3 > 0) {
                stringBuffer.append(" OR " + str3 + " LIKE '%");
            } else {
                stringBuffer.setLength(0);
                stringBuffer.append(str3 + " LIKE '%");
            }
            int i4 = 0;
            for (String str4 : new bi(str.replace('\'', '\"')).a()) {
                if (i4 > 0) {
                    stringBuffer.append(" AND " + str3 + " LIKE '%");
                }
                StringBuffer stringBuffer2 = new StringBuffer();
                for (int i5 = 0; i5 < str4.length(); i5++) {
                    char charAt = str4.charAt(i5);
                    if (charAt != '\"') {
                        stringBuffer2.append(charAt);
                    }
                }
                stringBuffer.append(stringBuffer2.toString() + "%'");
                i4++;
            }
            i2++;
            i3++;
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            s = 0;
            t.clear();
            dt = 0;
            du.clear();
            J = 0;
            K.clear();
            W = 0;
            X.clear();
            dv = 0;
            dw.clear();
            ai = 0;
            aj.clear();
            dx = 0;
            dy.clear();
            au = 0;
            aw.clear();
            aR = 0;
            aS.clear();
            bb = 0;
            bc.clear();
            bj = 0;
            bk.clear();
        }
    }

    private boolean g() {
        if (!db) {
            return this.f160a;
        }
        if (g) {
            showDialog(0);
        }
        if (!g) {
            return true;
        }
        return false;
    }

    private boolean h() {
        String a2;
        boolean z2 = this.f161b;
        String string = getString(C0000R.string.templatePath);
        String string2 = getString(C0000R.string.unlockFileName);
        try {
            File file = new File(string);
            if (file.isDirectory()) {
                for (File name : file.listFiles()) {
                    Log.d("", name.getName());
                }
            }
            BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(string, string2)));
            String readLine = bufferedReader.readLine();
            if (!(readLine == null || (a2 = b.a(getString(C0000R.string.masterpassword), readLine)) == null || !a2.equalsIgnoreCase(f))) {
                z2 = this.f160a;
                Log.i("TCA:isTrialUnlockForUnlimitedUse()", "License Key installed.");
            }
            bufferedReader.close();
            return z2;
        } catch (FileNotFoundException e2) {
            return this.f161b;
        } catch (IOException e3) {
            return this.f161b;
        } catch (Exception e4) {
            return this.f161b;
        }
    }
}
