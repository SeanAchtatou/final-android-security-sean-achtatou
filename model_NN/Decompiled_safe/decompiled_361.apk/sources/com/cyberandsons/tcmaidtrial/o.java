package com.cyberandsons.tcmaidtrial;

import android.content.Intent;
import android.util.Log;

final class o extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private int f1025a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ SplashScreenActivity f1026b;

    o(SplashScreenActivity splashScreenActivity) {
        this.f1026b = splashScreenActivity;
    }

    public final void run() {
        try {
            super.run();
            while (this.f1025a < 5000) {
                sleep(100);
                this.f1025a += 100;
            }
        } catch (Exception e) {
            Log.i("SplashScreenActivity", "EXc=" + e);
        } finally {
            this.f1026b.startActivity(new Intent(this.f1026b, TcmAid.class));
            this.f1026b.finish();
        }
    }
}
