package com.cyberandsons.tcmaidtrial.lists;

import android.content.DialogInterface;
import android.database.sqlite.SQLiteCursor;
import com.cyberandsons.tcmaidtrial.TcmAid;

final class bo implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PulseDiagList f801a;

    bo(PulseDiagList pulseDiagList) {
        this.f801a = pulseDiagList;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i == 0) {
            boolean unused = this.f801a.u = this.f801a.f742b;
            int unused2 = this.f801a.w = 0;
        } else if (i == 1) {
            boolean unused3 = this.f801a.u = this.f801a.f742b;
            int unused4 = this.f801a.w = 1;
        } else if (i == 2) {
            boolean unused5 = this.f801a.u = this.f801a.f741a;
        }
        this.f801a.c = this.f801a.getListView().onSaveInstanceState();
        this.f801a.stopManagingCursor(this.f801a.r);
        if (this.f801a.r != null) {
            this.f801a.r.close();
            SQLiteCursor unused6 = this.f801a.r = (SQLiteCursor) null;
        }
        TcmAid.aD = null;
        SQLiteCursor unused7 = this.f801a.r = this.f801a.c();
        this.f801a.setListAdapter(this.f801a.d());
        this.f801a.getListView().invalidate();
        this.f801a.getListView().onRestoreInstanceState(this.f801a.c);
    }
}
