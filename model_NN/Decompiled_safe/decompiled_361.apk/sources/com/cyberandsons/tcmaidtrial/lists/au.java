package com.cyberandsons.tcmaidtrial.lists;

import android.content.DialogInterface;
import android.database.sqlite.SQLiteCursor;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.m;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;

final class au implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HerbList f778a;

    au(HerbList herbList) {
        this.f778a = herbList;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String str;
        boolean z;
        String str2;
        this.f778a.a(i);
        if (this.f778a.B > 1000) {
            str2 = m.a(this.f778a.y, this.f778a.r, this.f778a.B);
        } else {
            String str3 = "";
            if (this.f778a.s.ag()) {
                str3 = " AND main.materiamedica.req_state_board=1 ";
            } else if (this.f778a.s.ae()) {
                str3 = " AND main.materiamedica.nccaom_req=1 ";
            }
            String b2 = this.f778a.y;
            n d = this.f778a.s;
            boolean z2 = d.z();
            String q = d.q();
            if (!z2 || q.length() <= 0) {
                str = null;
                z = false;
            } else {
                str = q.a("materiamedica", "_id", q);
                z = str.length() > 0;
            }
            str2 = z ? m.b(b2) + " WHERE (main" + str + ") " + " UNION " + m.c(b2) + " WHERE (userDB" + str + ") " + " AND userDB.materiamedica._id>1000 ORDER BY 2" : m.b(b2) + str3 + " UNION " + m.c(b2) + " WHERE userDB.materiamedica._id>1000 ORDER BY 2";
        }
        TcmAid.af = str2;
        this.f778a.f737a = this.f778a.getListView().onSaveInstanceState();
        this.f778a.stopManagingCursor(this.f778a.q);
        if (this.f778a.q != null) {
            this.f778a.q.close();
            SQLiteCursor unused = this.f778a.q = (SQLiteCursor) null;
        }
        TcmAid.ad = null;
        SQLiteCursor unused2 = this.f778a.q = this.f778a.b();
        this.f778a.setListAdapter(this.f778a.c());
        this.f778a.getListView().invalidate();
        this.f778a.getListView().onRestoreInstanceState(this.f778a.f737a);
    }
}
