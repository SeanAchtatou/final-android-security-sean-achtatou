package com.cyberandsons.tcmaidtrial.lists;

import android.content.DialogInterface;

final class g implements DialogInterface.OnMultiChoiceClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FormulaCategoryList f818a;

    g(FormulaCategoryList formulaCategoryList) {
        this.f818a = formulaCategoryList;
    }

    public final void onClick(DialogInterface dialogInterface, int i, boolean z) {
        if (z) {
            this.f818a.f731a.add(this.f818a.f732b[i]);
        } else {
            this.f818a.f731a.remove(this.f818a.f732b[i]);
        }
        this.f818a.a();
    }
}
