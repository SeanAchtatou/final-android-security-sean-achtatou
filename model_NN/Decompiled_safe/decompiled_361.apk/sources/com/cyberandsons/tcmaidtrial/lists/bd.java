package com.cyberandsons.tcmaidtrial.lists;

import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.misc.ad;

final class bd implements SimpleCursorAdapter.ViewBinder {

    /* renamed from: a  reason: collision with root package name */
    private boolean f788a = false;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ bh f789b;

    bd(bh bhVar) {
        this.f789b = bhVar;
    }

    public final boolean setViewValue(View view, Cursor cursor, int i) {
        TextView textView = (TextView) view;
        if (i == 1) {
            textView.setText(ad.a(cursor.getString(1)));
            this.f788a = true;
        }
        return this.f788a;
    }
}
