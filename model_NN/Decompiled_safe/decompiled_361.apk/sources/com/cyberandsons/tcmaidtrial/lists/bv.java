package com.cyberandsons.tcmaidtrial.lists;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.SimpleCursorAdapter;
import com.cyberandsons.tcmaidtrial.C0000R;

final class bv extends SimpleCursorAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f808a = true;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f809b = false;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public SQLiteDatabase d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public boolean j;

    public bv(Context context, Cursor cursor, String[] strArr, int[] iArr, boolean z, boolean z2, int i2, boolean z3, int i3, int i4, int i5, int i6, int i7, SQLiteDatabase sQLiteDatabase) {
        super(context, C0000R.layout.list_row, cursor, strArr, iArr);
        this.f808a = z;
        this.f809b = z2;
        this.c = i2;
        this.d = sQLiteDatabase;
        this.j = z3;
        this.e = i3;
        this.f = i4;
        this.g = i5;
        this.h = i6;
        this.i = i7;
        setViewBinder(new aj(this));
    }
}
