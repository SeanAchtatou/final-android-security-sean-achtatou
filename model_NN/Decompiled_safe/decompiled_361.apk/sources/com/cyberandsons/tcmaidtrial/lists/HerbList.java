package com.cyberandsons.tcmaidtrial.lists;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.m;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.detailed.HerbsDetail;
import com.cyberandsons.tcmaidtrial.misc.dh;
import org.acra.ErrorReporter;

public class HerbList extends ListActivity {
    private static String j;
    private static String k;
    private static boolean o;
    private int A;
    /* access modifiers changed from: private */
    public int B;
    private int C;
    private int D;
    private int E;
    private int F;
    private int G;
    private boolean H;

    /* renamed from: a  reason: collision with root package name */
    Parcelable f737a;

    /* renamed from: b  reason: collision with root package name */
    private final String f738b = "t_herbs";
    private final String c = "main.materiamedica.";
    private String d = "_id";
    private String e = "pinyin";
    private String f = "mmcategory";
    private String g = "temp";
    private String h;
    private String[] i;
    private String l = "pinyin";
    private String m = "pinyin";
    private String n;
    private boolean p;
    /* access modifiers changed from: private */
    public SQLiteCursor q;
    /* access modifiers changed from: private */
    public SQLiteDatabase r;
    /* access modifiers changed from: private */
    public n s;
    private boolean t = true;
    private boolean u;
    private ImageButton v;
    private ImageButton w;
    private int x;
    /* access modifiers changed from: private */
    public String y;
    private boolean z;

    public HerbList() {
        boolean z2;
        if (!this.t) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.u = z2;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        e();
        try {
            if (dh.d) {
                setRequestedOrientation(1);
            }
            this.z = this.s.Q() == 1 ? this.t : this.u;
            this.x = this.s.O();
            if (TcmAid.cU != -1) {
                this.x = TcmAid.cU;
            }
            this.A = this.s.ac();
            this.H = this.s.an();
            this.C = this.s.ai();
            this.D = this.s.aj();
            this.E = this.s.ak();
            this.F = this.s.al();
            this.G = this.s.am();
            if (this.x == 0) {
                this.x += this.A;
            }
            a(this.x);
            this.n = "SELECT _id, " + this.y + " FROM materiamedica";
            if (this.H) {
                setContentView((int) C0000R.layout.list_content_with_empty_view_w_language_black);
            } else {
                setContentView((int) C0000R.layout.list_content_with_empty_view_w_language);
            }
            TextView textView = (TextView) findViewById(C0000R.id.tca_label);
            String str = "Individual Herbs";
            this.B = -1;
            if (TcmAid.ac) {
                this.B = Integer.parseInt(TcmAid.ae[0]);
                str = (String) m.c(TcmAid.ae[0], this.r);
            }
            if (TcmAid.cP != null) {
                textView.setText(TcmAid.cP);
            } else {
                textView.setText(str);
            }
            this.v = (ImageButton) findViewById(C0000R.id.add);
            this.v.setOnClickListener(new ax(this));
            this.w = (ImageButton) findViewById(C0000R.id.lang);
            this.w.setOnClickListener(new aw(this));
            if (this.p) {
                d();
            }
            this.q = b();
            setListAdapter(c());
            TextView textView2 = (TextView) findViewById(C0000R.id.empty);
            textView2.setText("");
            ListView listView = getListView();
            listView.setFastScrollEnabled(true);
            listView.setEmptyView(textView2);
        } catch (Exception e2) {
            ErrorReporter.a().a("myVariable", e2.getLocalizedMessage());
            ErrorReporter.a().handleSilentException(new Exception("HL:onCreate()"));
            AlertDialog create = new AlertDialog.Builder(this).create();
            create.setTitle("Attention:");
            create.setMessage(getString(C0000R.string.area_list_exception));
            create.setButton("OK", new av(this));
            create.show();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (this.B > 1000) {
            menu.add(0, 2, 0, "Delete").setIcon(17301564);
        }
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 2:
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Warning");
                create.setMessage("Are you sure you want to delete this Herb category and all it's content?");
                create.setButton("OK", new at(this));
                create.setButton2("Cancel", new ar(this));
                create.show();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Display Herbs by ...");
        builder.setSingleChoiceItems(new CharSequence[]{"Pinyin w/Simplified Characters", "Pinyin w/Traditional Characters", "English", "Phamaceutical"}, this.x, new au(this));
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        this.x = i2;
        if (this.x == 0) {
            this.y = "pinyin";
            this.e = "pinyin";
            this.l = "pinyin";
            this.m = "pinyin";
            this.z = this.t;
            this.A = 0;
        } else if (this.x == 1) {
            this.y = "pinyin";
            this.e = "pinyin";
            this.l = "pinyin";
            this.m = "pinyin";
            this.z = this.t;
            this.A = 1;
        } else if (this.x == 2) {
            this.y = "english";
            this.e = "english";
            this.l = "english";
            this.m = "english";
            this.z = this.u;
        } else {
            this.y = "botanical";
            this.e = "botanical";
            this.l = "botanical";
            this.m = "botanical";
            this.z = this.u;
        }
        TcmAid.cU = this.x;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        e();
        if (TcmAid.cz) {
            startActivity(getIntent());
            finish();
            TcmAid.cz = false;
        }
        if (TcmAid.bl) {
            TcmAid.bl = this.u;
            try {
                if (dh.e) {
                    if (TcmAid.ai > 0) {
                        TcmAid.ai--;
                        TcmAid.af = (String) TcmAid.aj.get(TcmAid.ai);
                        TcmAid.aj.remove(TcmAid.ai);
                        if (dh.U) {
                            Log.d("HL:onResume()", "hbCounter++ = " + Integer.toString(TcmAid.ai) + ", hbCounterArrary.count = " + Integer.toString(TcmAid.aj.size()));
                        }
                    }
                    if (dh.ad) {
                        Log.d("HL:onResume()", TcmAid.af);
                    }
                }
                stopManagingCursor(this.q);
                if (this.q != null) {
                    this.q.close();
                    this.q = null;
                }
                TcmAid.ad = null;
                this.q = b();
                setListAdapter(c());
                getListView().invalidate();
                getListView().onRestoreInstanceState(this.f737a);
            } catch (SQLException e2) {
                q.a(e2);
            } catch (Exception e3) {
                Log.e("HL:onResume()", e3.getLocalizedMessage());
            }
        }
        super.onResume();
    }

    public void onDestroy() {
        try {
            TcmAid.ad = null;
            TcmAid.ac = this.u;
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            if (this.r != null && this.r.isOpen()) {
                this.r.close();
                this.r = null;
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        if (i2 >= 0) {
            super.onListItemClick(listView, view, i2, j2);
            if (dh.s) {
                Log.d("HL:onListItemClick()", "position = " + Integer.toString(i2) + ", id = " + Long.toString(j2));
            }
            TcmAid.ad = "main.materiamedica._id=" + Integer.toString((int) j2);
            TcmAid.ah = (int) j2;
            TcmAid.Y = i2;
            TcmAid.ac = this.u;
            if (TcmAid.af != null) {
                TcmAid.ai++;
                TcmAid.aj.add(TcmAid.af);
                if (dh.U && dh.s) {
                    Log.d("HerbsList:", "hbCounter++ = " + Integer.toString(TcmAid.ai) + ", hbCounterArrary.count = " + Integer.toString(TcmAid.aj.size()));
                }
            }
            this.f737a = getListView().onSaveInstanceState();
            startActivityForResult(new Intent(this, HerbsDetail.class), 1350);
        }
    }

    /* access modifiers changed from: private */
    public SQLiteCursor b() {
        String str;
        boolean z2;
        try {
            if (dh.s) {
                Log.d("HL:createCursor()", "Inserting into t_herbs");
            }
            if (TcmAid.af == null) {
                String str2 = "";
                if (this.s.ag()) {
                    str2 = " WHERE main.materiamedica.req_state_board=1 ";
                } else if (this.s.ae()) {
                    str2 = " WHERE main.materiamedica.nccaom_req=1 ";
                }
                String str3 = this.y;
                n nVar = this.s;
                boolean z3 = nVar.z();
                String q2 = nVar.q();
                if (!z3 || q2.length() <= 0) {
                    str = null;
                    z2 = false;
                } else {
                    str = q.a("materiamedica", "_id", q2);
                    z2 = str.length() > 0;
                }
                TcmAid.af = z2 ? m.d(str3) + " WHERE (main" + str + ") " + " UNION " + m.e(str3) + " WHERE (userDB" + str + ") " + " AND userDB.materiamedica._id>1000 ORDER BY 2" : m.d(str3) + str2 + " UNION " + m.e(str3) + " WHERE userDB.materiamedica._id>1000 ORDER BY 2";
            }
            this.r.execSQL(m.a("t_herbs"));
            this.r.execSQL(m.a("t_herbs", TcmAid.af, this.x));
        } catch (SQLException e2) {
            q.a(e2);
        }
        if (TcmAid.ad != null) {
            this.h = TcmAid.ad;
            TcmAid.ad = null;
            this.i = TcmAid.ae;
            TcmAid.ae = null;
        }
        try {
            this.q = (SQLiteCursor) this.r.query(o, "t_herbs", new String[]{this.d, this.e, this.f, this.g}, this.h, this.i, j, k, this.l, null);
            startManagingCursor(this.q);
            int count = this.q.getCount();
            if (dh.s) {
                Log.d("HL:createCursor()", "query count = " + Integer.toString(count));
            }
            TcmAid.Z.clear();
            if (this.q.moveToFirst()) {
                do {
                    TcmAid.Z.add(Integer.valueOf(this.q.getInt(0)));
                } while (this.q.moveToNext());
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        return this.q;
    }

    /* access modifiers changed from: private */
    public ListAdapter c() {
        boolean z2 = true;
        String[] strArr = {this.d, this.e};
        int[] iArr = {C0000R.id.text0, C0000R.id.text1};
        SQLiteCursor sQLiteCursor = this.q;
        if (!(this.x == 0 || this.x == 1)) {
            z2 = false;
        }
        return new bv(this, sQLiteCursor, strArr, iArr, z2, this.z, this.A, this.H, this.C, this.D, this.E, this.F, this.G, this.r);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d() {
        /*
            r4 = this;
            r3 = 0
            android.database.sqlite.SQLiteDatabase r0 = r4.r     // Catch:{ SQLException -> 0x0096, all -> 0x00a1 }
            java.lang.String r1 = r4.n     // Catch:{ SQLException -> 0x0096, all -> 0x00a1 }
            r2 = 0
            android.database.Cursor r4 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x0096, all -> 0x00a1 }
            android.database.sqlite.SQLiteCursor r4 = (android.database.sqlite.SQLiteCursor) r4     // Catch:{ SQLException -> 0x0096, all -> 0x00a1 }
            int r0 = r4.getCount()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            boolean r1 = com.cyberandsons.tcmaidtrial.misc.dh.s     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            if (r1 == 0) goto L_0x0056
            java.lang.String r1 = "HL:doRawCheck()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            r2.<init>()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r3 = "query count = "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            android.util.Log.d(r1, r0)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r0 = "HL:doRawCheck()"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            r1.<init>()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r2 = "column count = '"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            int r2 = r4.getColumnCount()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r2 = "' "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            android.util.Log.d(r0, r1)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
        L_0x0056:
            boolean r0 = r4.moveToFirst()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            if (r0 == 0) goto L_0x0090
        L_0x005c:
            r0 = 1
            java.lang.String r0 = r4.getString(r0)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            r1 = 0
            int r1 = r4.getInt(r1)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.s     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            if (r2 == 0) goto L_0x008a
            java.lang.String r2 = "HL:doRawCheck()"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            r3.<init>()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r3 = " - "
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            android.util.Log.d(r2, r0)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
        L_0x008a:
            boolean r0 = r4.moveToNext()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            if (r0 != 0) goto L_0x005c
        L_0x0090:
            if (r4 == 0) goto L_0x0095
            r4.close()
        L_0x0095:
            return
        L_0x0096:
            r0 = move-exception
            r1 = r3
        L_0x0098:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x00ac }
            if (r1 == 0) goto L_0x0095
            r1.close()
            goto L_0x0095
        L_0x00a1:
            r0 = move-exception
            r1 = r3
        L_0x00a3:
            if (r1 == 0) goto L_0x00a8
            r1.close()
        L_0x00a8:
            throw r0
        L_0x00a9:
            r0 = move-exception
            r1 = r4
            goto L_0x00a3
        L_0x00ac:
            r0 = move-exception
            goto L_0x00a3
        L_0x00ae:
            r0 = move-exception
            r1 = r4
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.HerbList.d():void");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }

    private void e() {
        if (this.r == null || !this.r.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("HL:openDataBase()", str + " opened.");
                this.r = SQLiteDatabase.openDatabase(str, null, 16);
                this.r.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.r.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("HL:openDataBase()", str2 + " ATTACHed.");
                this.s = new n();
                this.s.a(this.r);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new as(this));
                create.show();
            }
        }
    }
}
