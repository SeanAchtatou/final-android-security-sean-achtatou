package com.cyberandsons.tcmaidtrial.lists;

import android.app.AlertDialog;
import android.view.View;

final class n implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FormulasList f826a;

    n(FormulasList formulasList) {
        this.f826a = formulasList;
    }

    public final void onClick(View view) {
        FormulasList formulasList = this.f826a;
        AlertDialog create = new AlertDialog.Builder(formulasList).create();
        create.setTitle("Attention:");
        create.setMessage("Adding new Formula records will be available in a future update.");
        create.setButton("OK", new m(formulasList));
        create.show();
    }
}
