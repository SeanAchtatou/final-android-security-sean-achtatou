package com.cyberandsons.tcmaidtrial.lists;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.m;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.util.ArrayList;
import java.util.Iterator;

public class HerbCategoryList extends ListActivity {
    private static String j;
    private static String k;
    private static String n = "SELECT main.mmcategory._id, main.mmcategory.item_name FROM main.mmcategory ORDER BY main.mmcategory.item_name";
    private static boolean o;

    /* renamed from: a  reason: collision with root package name */
    protected ArrayList f735a;

    /* renamed from: b  reason: collision with root package name */
    CharSequence[] f736b;
    StringBuilder c;
    EditText d;
    private final String e = "tc_herbs";
    private final String f = "_id";
    private final String g = "mmcategory";
    private String h;
    private String[] i;
    private final String l = "mmcategory";
    private final String m = "mmcategory";
    private boolean p;
    private SQLiteCursor q;
    private SQLiteDatabase r;
    private n s;
    private boolean t = true;
    private boolean u;
    private ImageButton v;

    public HerbCategoryList() {
        this.u = !this.t;
        this.f735a = new ArrayList();
    }

    static /* synthetic */ void a(HerbCategoryList herbCategoryList, String str, String str2, String str3) {
        boolean z;
        int i2;
        StringBuilder sb = new StringBuilder();
        String[] split = str3.split(",");
        boolean z2 = herbCategoryList.t;
        boolean z3 = z2;
        for (String trim : split) {
            int b2 = m.b(trim.trim(), herbCategoryList.s.O(), herbCategoryList.r);
            if (z3) {
                sb.append(Integer.toString(b2));
                z3 = herbCategoryList.u;
            } else {
                sb.append(',' + Integer.toString(b2));
            }
        }
        ContentValues contentValues = new ContentValues();
        boolean z4 = herbCategoryList.u;
        int a2 = q.a("SELECT _id, name, desc, herbs  FROM userDB.herbcategory WHERE ", "userDB.herbcategory.", "name", str, herbCategoryList.r);
        if (a2 == dh.f946a.intValue()) {
            int a3 = q.a("SELECT MAX(userDB.herbcategory._id) FROM userDB.herbcategory ", herbCategoryList.r) + 1;
            if (a3 < 1000) {
                a3 += 1000;
            }
            contentValues.put("_id", Integer.toString(a3));
            int i3 = a3;
            z = z4;
            i2 = i3;
        } else {
            contentValues.put("_id", Integer.toString(a2));
            int i4 = a2;
            z = herbCategoryList.t;
            i2 = i4;
        }
        contentValues.put("name", str);
        contentValues.put("desc", str2);
        contentValues.put("herbs", sb.toString());
        if (!z) {
            try {
                herbCategoryList.r.insert("userDB.herbcategory", null, contentValues);
            } catch (SQLException e2) {
                q.a(e2);
            }
        } else {
            herbCategoryList.r.update("userDB.herbcategory", contentValues, "_id=" + Integer.toString(i2), null);
        }
        if (herbCategoryList.q != null) {
            herbCategoryList.q.close();
            herbCategoryList.q = null;
        }
        herbCategoryList.q = herbCategoryList.d();
        herbCategoryList.setListAdapter(herbCategoryList.e());
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0101 A[SYNTHETIC, Splitter:B:29:0x0101] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x014b A[SYNTHETIC, Splitter:B:36:0x014b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r7) {
        /*
            r6 = this;
            r3 = 0
            super.onCreate(r7)
            r6.f()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.d     // Catch:{ Exception -> 0x0105 }
            if (r0 == 0) goto L_0x000f
            r0 = 1
            r6.setRequestedOrientation(r0)     // Catch:{ Exception -> 0x0105 }
        L_0x000f:
            r0 = 2130903082(0x7f03002a, float:1.7412972E38)
            r6.setContentView(r0)     // Catch:{ Exception -> 0x0105 }
            r0 = 2131362326(0x7f0a0216, float:1.834443E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0105 }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x0105 }
            java.lang.String r1 = "Herb Categories"
            r0.setText(r1)     // Catch:{ Exception -> 0x0105 }
            r0 = 2131362327(0x7f0a0217, float:1.8344431E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0105 }
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0     // Catch:{ Exception -> 0x0105 }
            r6.v = r0     // Catch:{ Exception -> 0x0105 }
            android.widget.ImageButton r0 = r6.v     // Catch:{ Exception -> 0x0105 }
            com.cyberandsons.tcmaidtrial.lists.aa r1 = new com.cyberandsons.tcmaidtrial.lists.aa     // Catch:{ Exception -> 0x0105 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0105 }
            r0.setOnClickListener(r1)     // Catch:{ Exception -> 0x0105 }
            boolean r0 = r6.p     // Catch:{ Exception -> 0x0105 }
            if (r0 == 0) goto L_0x00d0
            android.database.sqlite.SQLiteDatabase r0 = r6.r     // Catch:{ SQLException -> 0x00fa, all -> 0x0147 }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.lists.HerbCategoryList.n     // Catch:{ SQLException -> 0x00fa, all -> 0x0147 }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x00fa, all -> 0x0147 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x00fa, all -> 0x0147 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.t     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            if (r2 == 0) goto L_0x0091
            java.lang.String r2 = "HCL:doRawCheck()"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            r3.<init>()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r4 = "query count = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            android.util.Log.d(r2, r1)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r1 = "HCL:doRawCheck()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            r2.<init>()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r3 = "column count = '"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            int r3 = r0.getColumnCount()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r3 = "' "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            android.util.Log.d(r1, r2)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
        L_0x0091:
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            if (r1 == 0) goto L_0x00cb
        L_0x0097:
            r1 = 1
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            r2 = 0
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.t     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            if (r3 == 0) goto L_0x00c5
            java.lang.String r3 = "HCL:doRawCheck()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            r4.<init>()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r4 = " - "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            android.util.Log.d(r3, r1)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
        L_0x00c5:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            if (r1 != 0) goto L_0x0097
        L_0x00cb:
            if (r0 == 0) goto L_0x00d0
            r0.close()     // Catch:{ Exception -> 0x0105 }
        L_0x00d0:
            android.database.sqlite.SQLiteCursor r0 = r6.d()     // Catch:{ Exception -> 0x0105 }
            r6.q = r0     // Catch:{ Exception -> 0x0105 }
            android.widget.ListAdapter r0 = r6.e()     // Catch:{ Exception -> 0x0105 }
            r6.setListAdapter(r0)     // Catch:{ Exception -> 0x0105 }
            r6.c()     // Catch:{ Exception -> 0x0105 }
            r0 = 2131361994(0x7f0a00ca, float:1.8343756E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0105 }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x0105 }
            java.lang.String r1 = ""
            r0.setText(r1)     // Catch:{ Exception -> 0x0105 }
            android.widget.ListView r1 = r6.getListView()     // Catch:{ Exception -> 0x0105 }
            r2 = 1
            r1.setFastScrollEnabled(r2)     // Catch:{ Exception -> 0x0105 }
            r1.setEmptyView(r0)     // Catch:{ Exception -> 0x0105 }
        L_0x00f9:
            return
        L_0x00fa:
            r0 = move-exception
            r1 = r3
        L_0x00fc:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0154 }
            if (r1 == 0) goto L_0x00d0
            r1.close()     // Catch:{ Exception -> 0x0105 }
            goto L_0x00d0
        L_0x0105:
            r0 = move-exception
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()
            java.lang.String r2 = "myVariable"
            java.lang.String r0 = r0.getLocalizedMessage()
            r1.a(r2, r0)
            org.acra.ErrorReporter r0 = org.acra.ErrorReporter.a()
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r2 = "HCL:onCreate()"
            r1.<init>(r2)
            r0.handleSilentException(r1)
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r6)
            android.app.AlertDialog r0 = r0.create()
            java.lang.String r1 = "Attention:"
            r0.setTitle(r1)
            r1 = 2131099670(0x7f060016, float:1.78117E38)
            java.lang.String r1 = r6.getString(r1)
            r0.setMessage(r1)
            java.lang.String r1 = "OK"
            com.cyberandsons.tcmaidtrial.lists.ac r2 = new com.cyberandsons.tcmaidtrial.lists.ac
            r2.<init>(r6)
            r0.setButton(r1, r2)
            r0.show()
            goto L_0x00f9
        L_0x0147:
            r0 = move-exception
            r1 = r3
        L_0x0149:
            if (r1 == 0) goto L_0x014e
            r1.close()     // Catch:{ Exception -> 0x0105 }
        L_0x014e:
            throw r0     // Catch:{ Exception -> 0x0105 }
        L_0x014f:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0149
        L_0x0154:
            r0 = move-exception
            goto L_0x0149
        L_0x0156:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x00fc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.HerbCategoryList.onCreate(android.os.Bundle):void");
    }

    public void onDestroy() {
        this.f735a.clear();
        TcmAid.ad = null;
        TcmAid.ac = this.u;
        TcmAid.af = null;
        TcmAid.ag = null;
        try {
            if (this.r != null && this.r.isOpen()) {
                this.r.close();
                this.r = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        f();
        if (TcmAid.cA) {
            startActivity(getIntent());
            finish();
            TcmAid.cA = false;
        }
        super.onResume();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0087, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0088, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x008e, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0093, code lost:
        r2 = "";
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c5, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00c9, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00ca, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0087 A[ExcHandler: SQLException (e android.database.SQLException), Splitter:B:1:0x0006] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c9 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0006] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:54:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c() {
        /*
            r8 = this;
            r6 = 0
            r5 = 1
            java.lang.String r0 = ""
            java.lang.String r1 = ""
            com.cyberandsons.tcmaidtrial.a.n r2 = r8.s     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x0092, all -> 0x00c9 }
            int r2 = r2.O()     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x0092, all -> 0x00c9 }
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.cU     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x0092, all -> 0x00c9 }
            r4 = -1
            if (r3 == r4) goto L_0x0013
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.cU     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x0092, all -> 0x00c9 }
        L_0x0013:
            if (r2 == 0) goto L_0x0017
            if (r2 != r5) goto L_0x007e
        L_0x0017:
            java.lang.String r2 = "pinyin"
        L_0x0019:
            java.lang.String r3 = ""
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x0092, all -> 0x00c9 }
            r4.<init>()     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x0092, all -> 0x00c9 }
            java.lang.String r5 = com.cyberandsons.tcmaidtrial.a.m.b(r2)     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x0092, all -> 0x00c9 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x0092, all -> 0x00c9 }
            java.lang.StringBuilder r4 = r4.append(r3)     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x0092, all -> 0x00c9 }
            java.lang.String r5 = " UNION "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x0092, all -> 0x00c9 }
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.a.m.c(r2)     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x0092, all -> 0x00c9 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x0092, all -> 0x00c9 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x0092, all -> 0x00c9 }
            java.lang.String r3 = " ORDER BY 2 "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x0092, all -> 0x00c9 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x0092, all -> 0x00c9 }
            java.lang.String r1 = "Pre-rawQuery"
            android.database.sqlite.SQLiteDatabase r0 = r8.r     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x00d8, all -> 0x00c9 }
            r3 = 0
            android.database.Cursor r0 = r0.rawQuery(r2, r3)     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x00d8, all -> 0x00c9 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0087, NullPointerException -> 0x00d8, all -> 0x00c9 }
            int r3 = r0.getCount()     // Catch:{ SQLException -> 0x00dd, NullPointerException -> 0x00db }
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLException -> 0x00dd, NullPointerException -> 0x00db }
            r8.f736b = r3     // Catch:{ SQLException -> 0x00dd, NullPointerException -> 0x00db }
            java.lang.String r1 = "Pre-cursor loop"
            r3 = 0
            boolean r4 = r0.moveToFirst()     // Catch:{ SQLException -> 0x00dd, NullPointerException -> 0x00db }
            if (r4 == 0) goto L_0x0075
        L_0x0064:
            java.lang.CharSequence[] r4 = r8.f736b     // Catch:{ SQLException -> 0x00dd, NullPointerException -> 0x00db }
            int r5 = r3 + 1
            r6 = 1
            java.lang.String r6 = r0.getString(r6)     // Catch:{ SQLException -> 0x00dd, NullPointerException -> 0x00db }
            r4[r3] = r6     // Catch:{ SQLException -> 0x00dd, NullPointerException -> 0x00db }
            boolean r3 = r0.moveToNext()     // Catch:{ SQLException -> 0x00dd, NullPointerException -> 0x00db }
            if (r3 != 0) goto L_0x00e2
        L_0x0075:
            r0.close()     // Catch:{ SQLException -> 0x00dd, NullPointerException -> 0x00db }
            if (r0 == 0) goto L_0x007d
            r0.close()
        L_0x007d:
            return
        L_0x007e:
            r3 = 2
            if (r2 != r3) goto L_0x0084
            java.lang.String r2 = "english"
            goto L_0x0019
        L_0x0084:
            java.lang.String r2 = "botanical"
            goto L_0x0019
        L_0x0087:
            r0 = move-exception
            r1 = r6
        L_0x0089:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x00d6 }
            if (r1 == 0) goto L_0x007d
            r1.close()
            goto L_0x007d
        L_0x0092:
            r2 = move-exception
            r2 = r0
            r0 = r6
        L_0x0095:
            org.acra.ErrorReporter r3 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x00d1 }
            java.lang.String r4 = "myVariable"
            r3.a(r4, r2)     // Catch:{ all -> 0x00d1 }
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x00d1 }
            java.lang.NullPointerException r3 = new java.lang.NullPointerException     // Catch:{ all -> 0x00d1 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d1 }
            r4.<init>()     // Catch:{ all -> 0x00d1 }
            java.lang.String r5 = "HCL:loadControlResources( last step completed = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00d1 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x00d1 }
            java.lang.String r4 = " )"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x00d1 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00d1 }
            r3.<init>(r1)     // Catch:{ all -> 0x00d1 }
            r2.handleSilentException(r3)     // Catch:{ all -> 0x00d1 }
            if (r0 == 0) goto L_0x007d
            r0.close()
            goto L_0x007d
        L_0x00c9:
            r0 = move-exception
            r1 = r6
        L_0x00cb:
            if (r1 == 0) goto L_0x00d0
            r1.close()
        L_0x00d0:
            throw r0
        L_0x00d1:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x00cb
        L_0x00d6:
            r0 = move-exception
            goto L_0x00cb
        L_0x00d8:
            r0 = move-exception
            r0 = r6
            goto L_0x0095
        L_0x00db:
            r3 = move-exception
            goto L_0x0095
        L_0x00dd:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0089
        L_0x00e2:
            r3 = r5
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.HerbCategoryList.c():void");
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        String str;
        if (i2 >= 0) {
            super.onListItemClick(listView, view, i2, j2);
            if (dh.t) {
                Log.d("HCL:onListItemClick()", "position = " + Integer.toString(i2) + ", id = " + Long.toString(j2));
            }
            TcmAid.ae = new String[]{Integer.toString((int) j2)};
            TcmAid.ac = this.t;
            TcmAid.cA = true;
            if (j2 > 1000) {
                int O = this.s.O();
                if (TcmAid.cU != -1) {
                    O = TcmAid.cU;
                }
                switch (O) {
                    case 1:
                        str = "" + "english";
                        break;
                    case 2:
                        str = "" + "botanical";
                        break;
                    default:
                        str = "" + "pinyin";
                        break;
                }
                TcmAid.af = m.a(str, this.r, (int) j2);
            } else {
                TcmAid.ad = "mmcategory=?";
                TcmAid.af = null;
            }
            startActivityForResult(new Intent(this, HerbList.class), 1350);
        }
    }

    private SQLiteCursor d() {
        try {
            if (dh.t) {
                Log.d("HCL:createCursor()", "Inserting into tc_herbs");
            }
            if (TcmAid.ag == null) {
                String str = "";
                if (this.s.ag()) {
                    str = " WHERE main.materiamedica.req_state_board=1 ";
                } else if (this.s.ae()) {
                    str = " WHERE main.materiamedica.nccaom_req=1 ";
                }
                TcmAid.ag = String.format("SELECT DISTINCT main.mmcategory._id, main.mmcategory.item_name FROM main.materiamedica LEFT JOIN main.mmcategory ON main.mmcategory._id = main.materiamedica.mmcategory  %s UNION SELECT userDB.herbcategory._id, userDB.herbcategory.name FROM userDB.herbcategory ORDER BY 2 ", str);
            }
            this.r.execSQL(m.a("tc_herbs"));
            this.r.execSQL("INSERT INTO " + "tc_herbs" + " (_id, mmcategory) " + TcmAid.ag);
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            this.q = (SQLiteCursor) this.r.query(o, "tc_herbs", new String[]{"_id", "mmcategory"}, this.h, this.i, j, k, "mmcategory", null);
            startManagingCursor(this.q);
            int count = this.q.getCount();
            if (dh.t) {
                Log.d("HCL:createCursor()", "query count = " + Integer.toString(count));
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        return this.q;
    }

    private ListAdapter e() {
        return new r(this, this.q, new String[]{"_id", "mmcategory"}, new int[]{C0000R.id.text0, C0000R.id.text1}, "mmcategory");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }

    private void f() {
        if (this.r == null || !this.r.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("HCL:openDataBase()", str + " opened.");
                this.r = SQLiteDatabase.openDatabase(str, null, 16);
                this.r.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.r.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("HCL:openDataBase()", str2 + " ATTACHed.");
                this.s = new n();
                this.s.a(this.r);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new ab(this));
                create.show();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.c = new StringBuilder();
        boolean z = this.t;
        Iterator it = this.f735a.iterator();
        boolean z2 = z;
        while (it.hasNext()) {
            CharSequence charSequence = (CharSequence) it.next();
            if (z2) {
                this.c.append(charSequence);
                z2 = this.u;
            } else {
                this.c.append(',' + charSequence.toString());
            }
        }
    }

    public final void b() {
        this.f735a.clear();
        ae aeVar = new ae(this);
        this.d = new EditText(this);
        this.d.setHint("Enter Herb Category Name");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Herbs");
        builder.setMultiChoiceItems(this.f736b, new boolean[this.f736b.length], aeVar);
        builder.setView(this.d);
        AlertDialog create = builder.create();
        create.setOnCancelListener(new ad(this));
        create.show();
    }
}
