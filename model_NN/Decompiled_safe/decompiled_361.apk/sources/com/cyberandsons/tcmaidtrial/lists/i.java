package com.cyberandsons.tcmaidtrial.lists;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.SimpleCursorAdapter;
import com.cyberandsons.tcmaidtrial.C0000R;

final class i extends SimpleCursorAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f820a = false;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f821b = false;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public SQLiteDatabase d;

    public i(Context context, Cursor cursor, String[] strArr, int[] iArr, boolean z, boolean z2, int i, SQLiteDatabase sQLiteDatabase) {
        super(context, C0000R.layout.list_row, cursor, strArr, iArr);
        this.f820a = z;
        this.f821b = z2;
        this.c = i;
        this.d = sQLiteDatabase;
        setViewBinder(new bl(this));
    }
}
