package com.cyberandsons.tcmaidtrial.lists;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.p;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class AuricularCategoryList extends ListActivity {
    private static String j = "SELECT main.auricular._id, main.auricular.pointcat FROM main.auricular GROUP BY main.auricular.pointcat";
    private static boolean k;

    /* renamed from: a  reason: collision with root package name */
    private final String f725a = "tc_auricular";

    /* renamed from: b  reason: collision with root package name */
    private final String f726b = "_id";
    private final String c = "pointcat";
    private String d;
    private String[] e;
    private final String f = "pointcat";
    private String g;
    private final String h = "pointcat";
    private final String i = "pointcat";
    private boolean l;
    private SQLiteCursor m;
    private SQLiteDatabase n;
    private n o;
    private boolean p = true;
    private boolean q;

    public AuricularCategoryList() {
        boolean z;
        if (!this.p) {
            z = true;
        } else {
            z = false;
        }
        this.q = z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x010f A[SYNTHETIC, Splitter:B:29:0x010f] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0159 A[SYNTHETIC, Splitter:B:36:0x0159] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r8) {
        /*
            r7 = this;
            r3 = 0
            super.onCreate(r8)
            r7.b()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.d     // Catch:{ Exception -> 0x0113 }
            if (r0 == 0) goto L_0x000f
            r0 = 1
            r7.setRequestedOrientation(r0)     // Catch:{ Exception -> 0x0113 }
        L_0x000f:
            r0 = 2130903079(0x7f030027, float:1.7412966E38)
            r7.setContentView(r0)     // Catch:{ Exception -> 0x0113 }
            r0 = 2131362327(0x7f0a0217, float:1.8344431E38)
            android.view.View r0 = r7.findViewById(r0)     // Catch:{ Exception -> 0x0113 }
            android.widget.ImageView r0 = (android.widget.ImageView) r0     // Catch:{ Exception -> 0x0113 }
            r1 = 4
            r0.setVisibility(r1)     // Catch:{ Exception -> 0x0113 }
            r0 = 2131362326(0x7f0a0216, float:1.834443E38)
            android.view.View r0 = r7.findViewById(r0)     // Catch:{ Exception -> 0x0113 }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x0113 }
            java.lang.String r1 = "Auricular Categories"
            r0.setText(r1)     // Catch:{ Exception -> 0x0113 }
            boolean r0 = r7.l     // Catch:{ Exception -> 0x0113 }
            if (r0 == 0) goto L_0x00c8
            android.database.sqlite.SQLiteDatabase r0 = r7.n     // Catch:{ SQLException -> 0x0108, all -> 0x0155 }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.lists.AuricularCategoryList.j     // Catch:{ SQLException -> 0x0108, all -> 0x0155 }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x0108, all -> 0x0155 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0108, all -> 0x0155 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.q     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            if (r2 == 0) goto L_0x0089
            java.lang.String r2 = "ACL:doRawCheck()"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            r3.<init>()     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            java.lang.String r4 = "query count = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            android.util.Log.d(r2, r1)     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            java.lang.String r1 = "ACL:doRawCheck()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            r2.<init>()     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            java.lang.String r3 = "column count = '"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            int r3 = r0.getColumnCount()     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            java.lang.String r3 = "' "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            android.util.Log.d(r1, r2)     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
        L_0x0089:
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            if (r1 == 0) goto L_0x00c3
        L_0x008f:
            r1 = 1
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            r2 = 0
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.q     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            if (r3 == 0) goto L_0x00bd
            java.lang.String r3 = "ACL:doRawCheck()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            r4.<init>()     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            java.lang.String r4 = " - "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            android.util.Log.d(r3, r1)     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
        L_0x00bd:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0164, all -> 0x015d }
            if (r1 != 0) goto L_0x008f
        L_0x00c3:
            if (r0 == 0) goto L_0x00c8
            r0.close()     // Catch:{ Exception -> 0x0113 }
        L_0x00c8:
            android.database.sqlite.SQLiteCursor r0 = r7.a()     // Catch:{ Exception -> 0x0113 }
            r7.m = r0     // Catch:{ Exception -> 0x0113 }
            r0 = 2
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch:{ Exception -> 0x0113 }
            r0 = 0
            java.lang.String r1 = "_id"
            r3[r0] = r1     // Catch:{ Exception -> 0x0113 }
            r0 = 1
            java.lang.String r1 = "pointcat"
            r3[r0] = r1     // Catch:{ Exception -> 0x0113 }
            r0 = 2
            int[] r4 = new int[r0]     // Catch:{ Exception -> 0x0113 }
            r4 = {2131361998, 2131362004} // fill-array     // Catch:{ Exception -> 0x0113 }
            com.cyberandsons.tcmaidtrial.lists.bc r0 = new com.cyberandsons.tcmaidtrial.lists.bc     // Catch:{ Exception -> 0x0113 }
            android.database.sqlite.SQLiteCursor r2 = r7.m     // Catch:{ Exception -> 0x0113 }
            java.lang.String r5 = "pointcat"
            r1 = r7
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0113 }
            r7.setListAdapter(r0)     // Catch:{ Exception -> 0x0113 }
            r0 = 2131361994(0x7f0a00ca, float:1.8343756E38)
            android.view.View r0 = r7.findViewById(r0)     // Catch:{ Exception -> 0x0113 }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x0113 }
            java.lang.String r1 = ""
            r0.setText(r1)     // Catch:{ Exception -> 0x0113 }
            android.widget.ListView r1 = r7.getListView()     // Catch:{ Exception -> 0x0113 }
            r2 = 1
            r1.setFastScrollEnabled(r2)     // Catch:{ Exception -> 0x0113 }
            r1.setEmptyView(r0)     // Catch:{ Exception -> 0x0113 }
        L_0x0107:
            return
        L_0x0108:
            r0 = move-exception
            r1 = r3
        L_0x010a:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0162 }
            if (r1 == 0) goto L_0x00c8
            r1.close()     // Catch:{ Exception -> 0x0113 }
            goto L_0x00c8
        L_0x0113:
            r0 = move-exception
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()
            java.lang.String r2 = "myVariable"
            java.lang.String r0 = r0.getLocalizedMessage()
            r1.a(r2, r0)
            org.acra.ErrorReporter r0 = org.acra.ErrorReporter.a()
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r2 = "ACL:onCreate()"
            r1.<init>(r2)
            r0.handleSilentException(r1)
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r7)
            android.app.AlertDialog r0 = r0.create()
            java.lang.String r1 = "Attention:"
            r0.setTitle(r1)
            r1 = 2131099670(0x7f060016, float:1.78117E38)
            java.lang.String r1 = r7.getString(r1)
            r0.setMessage(r1)
            java.lang.String r1 = "OK"
            com.cyberandsons.tcmaidtrial.lists.bm r2 = new com.cyberandsons.tcmaidtrial.lists.bm
            r2.<init>(r7)
            r0.setButton(r1, r2)
            r0.show()
            goto L_0x0107
        L_0x0155:
            r0 = move-exception
            r1 = r3
        L_0x0157:
            if (r1 == 0) goto L_0x015c
            r1.close()     // Catch:{ Exception -> 0x0113 }
        L_0x015c:
            throw r0     // Catch:{ Exception -> 0x0113 }
        L_0x015d:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0157
        L_0x0162:
            r0 = move-exception
            goto L_0x0157
        L_0x0164:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x010a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.AuricularCategoryList.onCreate(android.os.Bundle):void");
    }

    public void onDestroy() {
        try {
            TcmAid.o = null;
            TcmAid.m = null;
            TcmAid.l = this.q;
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            if (this.n != null && this.n.isOpen()) {
                this.n.close();
                this.n = null;
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        b();
        if (TcmAid.cx) {
            TcmAid.m = null;
            TcmAid.n = null;
            startActivity(getIntent());
            finish();
            TcmAid.cx = false;
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        if (i2 >= 0) {
            super.onListItemClick(listView, view, i2, j2);
            String obj = ((TextView) view.findViewById(C0000R.id.text1)).getText().toString();
            if (obj.equalsIgnoreCase("NADA")) {
                TcmAid.o = "SELECT main.auricular._id, lower(main.auricular.point), main.auricular.pointcat, main.auricular.nada, main.auricular.battlefield, main.auricular.acacd, main.auricular.stopsmoking, main.auricular.weightloss FROM main.auricular " + " WHERE main.auricular.nada=1";
                TcmAid.m = "nada=?";
                TcmAid.l = this.p;
                TcmAid.n = new String[]{"1"};
                TcmAid.cP = "NADA";
            } else if (obj.equalsIgnoreCase("Battlefield")) {
                TcmAid.o = "SELECT main.auricular._id, lower(main.auricular.point), main.auricular.pointcat, main.auricular.nada, main.auricular.battlefield, main.auricular.acacd, main.auricular.stopsmoking, main.auricular.weightloss FROM main.auricular " + " WHERE main.auricular.battlefield=1";
                TcmAid.m = "battlefield=?";
                TcmAid.l = this.p;
                TcmAid.n = new String[]{"1"};
                TcmAid.cP = "Battlefield";
            } else if (obj.equalsIgnoreCase("ACACD")) {
                TcmAid.o = "SELECT main.auricular._id, lower(main.auricular.point), main.auricular.pointcat, main.auricular.nada, main.auricular.battlefield, main.auricular.acacd, main.auricular.stopsmoking, main.auricular.weightloss FROM main.auricular " + " WHERE main.auricular.acacd=1";
                TcmAid.m = "acacd=?";
                TcmAid.l = this.p;
                TcmAid.n = new String[]{"1"};
                TcmAid.cP = "ACACD";
            } else if (obj.equalsIgnoreCase("Stop Smoking")) {
                TcmAid.o = "SELECT main.auricular._id, lower(main.auricular.point), main.auricular.pointcat, main.auricular.nada, main.auricular.battlefield, main.auricular.acacd, main.auricular.stopsmoking, main.auricular.weightloss FROM main.auricular " + " WHERE main.auricular.stopsmoking=1";
                TcmAid.m = "stopsmoking=?";
                TcmAid.l = this.p;
                TcmAid.n = new String[]{"1"};
                TcmAid.cP = "Stop Smoking";
            } else if (obj.equalsIgnoreCase("Weight Loss")) {
                TcmAid.o = "SELECT main.auricular._id, lower(main.auricular.point), main.auricular.pointcat, main.auricular.nada, main.auricular.battlefield, main.auricular.acacd, main.auricular.stopsmoking, main.auricular.weightloss FROM main.auricular " + " WHERE main.auricular.weightloss=1";
                TcmAid.m = "weightloss=?";
                TcmAid.l = this.p;
                TcmAid.n = new String[]{"1"};
                TcmAid.cP = "Weight Loss";
            } else {
                TcmAid.o = null;
                TcmAid.m = "pointcat=?";
                TcmAid.l = this.p;
                TcmAid.n = new String[]{obj};
            }
            if (dh.q) {
                Log.d("ACL:onListItemClick()", "position = " + Integer.toString(i2) + ", id = " + Long.toString(j2));
            }
            TcmAid.cx = true;
            startActivityForResult(new Intent(this, AuricularList.class), 1350);
        }
    }

    private SQLiteCursor a() {
        try {
            if (dh.q) {
                Log.d("ACL:createCursor()", "Inserting into tc_auricular");
            }
            this.n.execSQL(p.a("tc_auricular"));
            this.n.execSQL(p.a("tc_auricular", "SELECT DISTINCT main.auricular._id, main.auricular.point, main.auricular.pointcat, main.auricular.nada, main.auricular.battlefield, main.auricular.acacd, main.auricular.stopsmoking, main.auricular.weightloss FROM main.auricular ORDER BY main.auricular.pointcat "));
            this.n.execSQL(p.a("tc_auricular", " VALUES(995,'','NADA',        1,0,0,0,0)"));
            this.n.execSQL(p.a("tc_auricular", " VALUES(996,'','Battlefield', 0,1,0,0,0)"));
            this.n.execSQL(p.a("tc_auricular", " VALUES(997,'','ACACD',       0,0,1,0,0)"));
            this.n.execSQL(p.a("tc_auricular", " VALUES(998,'','Stop Smoking',0,0,0,1,0)"));
            this.n.execSQL(p.a("tc_auricular", " VALUES(999,'','Weight Loss', 0,0,0,0,1)"));
        } catch (SQLException e2) {
            q.a(e2);
        }
        if (TcmAid.m != null) {
            this.d = TcmAid.m;
            TcmAid.m = null;
            this.e = TcmAid.n;
            TcmAid.n = null;
        }
        try {
            this.m = (SQLiteCursor) this.n.query(k, "tc_auricular", new String[]{"_id", "pointcat"}, this.d, this.e, "pointcat", this.g, "pointcat", null);
            startManagingCursor(this.m);
            int count = this.m.getCount();
            if (dh.q) {
                Log.d("ACL:createCursor()", "query count = " + Integer.toString(count));
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        return this.m;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }

    private void b() {
        if (this.n == null || !this.n.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("ACL:openDataBase()", str + " opened.");
                this.n = SQLiteDatabase.openDatabase(str, null, 16);
                this.n.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.n.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("ACL:openDataBase()", str2 + " ATTACHed.");
                this.o = new n();
                this.o.a(this.n);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new bn(this));
                create.show();
            }
        }
    }
}
