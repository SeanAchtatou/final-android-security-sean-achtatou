package com.cyberandsons.tcmaidtrial.lists;

import android.content.DialogInterface;
import android.database.sqlite.SQLiteCursor;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.c;

final class l implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FormulasList f824a;

    l(FormulasList formulasList) {
        this.f824a = formulasList;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f824a.a(i);
        if (this.f824a.z > 1000) {
            TcmAid.S = c.a(this.f824a.w, this.f824a.p, this.f824a.z);
        } else {
            String str = "";
            if (this.f824a.q.ah()) {
                str = " AND main.formulas.req_state_board=1 ";
            } else if (this.f824a.q.af()) {
                str = " AND main.formulas.nccaom_req=1 ";
            }
            TcmAid.S = c.a(this.f824a.w, this.f824a.q, str);
        }
        this.f824a.f733a = this.f824a.getListView().onSaveInstanceState();
        this.f824a.stopManagingCursor(this.f824a.o);
        if (this.f824a.o != null) {
            this.f824a.o.close();
            SQLiteCursor unused = this.f824a.o = (SQLiteCursor) null;
        }
        TcmAid.Q = null;
        SQLiteCursor unused2 = this.f824a.o = this.f824a.b();
        this.f824a.setListAdapter(this.f824a.c());
        this.f824a.getListView().invalidate();
        this.f824a.getListView().onRestoreInstanceState(this.f824a.f733a);
    }
}
