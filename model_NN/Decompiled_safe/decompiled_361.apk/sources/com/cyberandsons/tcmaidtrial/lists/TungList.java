package com.cyberandsons.tcmaidtrial.lists;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.additems.TungAdd;
import com.cyberandsons.tcmaidtrial.detailed.TungDetail;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class TungList extends ListActivity {
    private static String l;
    private static String m;
    private static String p = "SELECT _id, point_number FROM mastertung";
    private static boolean q;

    /* renamed from: a  reason: collision with root package name */
    boolean f747a = true;

    /* renamed from: b  reason: collision with root package name */
    boolean f748b;
    Parcelable c;
    private final String d = "t_tung";
    private final String e = "main.mastertung.";
    private final String f = "_id";
    private final String g = "point_number";
    private final String h = "name";
    private final String i = "english_name";
    private String j;
    private String[] k;
    private final String n = " CASE WHEN point_number GLOB '*[^0-9.]*' THEN point_number ELSE cast(point_number AS real) END";
    private final String o = "point_number";
    private boolean r;
    /* access modifiers changed from: private */
    public SQLiteCursor s;
    private SQLiteDatabase t;
    private n u;
    private ImageButton v;
    private ImageButton w;
    /* access modifiers changed from: private */
    public boolean x;
    private boolean y;
    /* access modifiers changed from: private */
    public int z;

    public TungList() {
        boolean z2;
        if (!this.f747a) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.f748b = z2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:50:0x019a A[SYNTHETIC, Splitter:B:50:0x019a] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01a3 A[Catch:{ Exception -> 0x0151 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r7) {
        /*
            r6 = this;
            r3 = 0
            r2 = 1
            super.onCreate(r7)
            r6.e()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.d     // Catch:{ Exception -> 0x0151 }
            if (r0 == 0) goto L_0x0010
            r0 = 1
            r6.setRequestedOrientation(r0)     // Catch:{ Exception -> 0x0151 }
        L_0x0010:
            com.cyberandsons.tcmaidtrial.a.n r0 = r6.u     // Catch:{ Exception -> 0x0151 }
            int r0 = r0.V()     // Catch:{ Exception -> 0x0151 }
            if (r0 != r2) goto L_0x013e
            boolean r0 = r6.f747a     // Catch:{ Exception -> 0x0151 }
        L_0x001a:
            r6.y = r0     // Catch:{ Exception -> 0x0151 }
            com.cyberandsons.tcmaidtrial.a.n r0 = r6.u     // Catch:{ Exception -> 0x0151 }
            int r0 = r0.S()     // Catch:{ Exception -> 0x0151 }
            if (r0 != r2) goto L_0x0142
            boolean r0 = r6.f747a     // Catch:{ Exception -> 0x0151 }
        L_0x0026:
            r6.x = r0     // Catch:{ Exception -> 0x0151 }
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.cX     // Catch:{ Exception -> 0x0151 }
            r1 = -1
            if (r0 == r1) goto L_0x0035
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.cX     // Catch:{ Exception -> 0x0151 }
            if (r0 != r2) goto L_0x0146
            boolean r0 = r6.f747a     // Catch:{ Exception -> 0x0151 }
        L_0x0033:
            r6.x = r0     // Catch:{ Exception -> 0x0151 }
        L_0x0035:
            com.cyberandsons.tcmaidtrial.a.n r0 = r6.u     // Catch:{ Exception -> 0x0151 }
            int r0 = r0.ac()     // Catch:{ Exception -> 0x0151 }
            r6.z = r0     // Catch:{ Exception -> 0x0151 }
            r0 = 2130903084(0x7f03002c, float:1.7412976E38)
            r6.setContentView(r0)     // Catch:{ Exception -> 0x0151 }
            r0 = 2131362326(0x7f0a0216, float:1.834443E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0151 }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x0151 }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cP     // Catch:{ Exception -> 0x0151 }
            if (r1 == 0) goto L_0x014a
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cP     // Catch:{ Exception -> 0x0151 }
            r0.setText(r1)     // Catch:{ Exception -> 0x0151 }
        L_0x0055:
            r0 = 2131362327(0x7f0a0217, float:1.8344431E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0151 }
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0     // Catch:{ Exception -> 0x0151 }
            r6.v = r0     // Catch:{ Exception -> 0x0151 }
            android.widget.ImageButton r0 = r6.v     // Catch:{ Exception -> 0x0151 }
            com.cyberandsons.tcmaidtrial.lists.w r1 = new com.cyberandsons.tcmaidtrial.lists.w     // Catch:{ Exception -> 0x0151 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0151 }
            r0.setOnClickListener(r1)     // Catch:{ Exception -> 0x0151 }
            r0 = 2131362328(0x7f0a0218, float:1.8344433E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0151 }
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0     // Catch:{ Exception -> 0x0151 }
            r6.w = r0     // Catch:{ Exception -> 0x0151 }
            android.widget.ImageButton r0 = r6.w     // Catch:{ Exception -> 0x0151 }
            com.cyberandsons.tcmaidtrial.lists.v r1 = new com.cyberandsons.tcmaidtrial.lists.v     // Catch:{ Exception -> 0x0151 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0151 }
            r0.setOnClickListener(r1)     // Catch:{ Exception -> 0x0151 }
            boolean r0 = r6.r     // Catch:{ Exception -> 0x0151 }
            if (r0 == 0) goto L_0x0117
            android.database.sqlite.SQLiteDatabase r0 = r6.t     // Catch:{ SQLException -> 0x0193, all -> 0x019f }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.lists.TungList.p     // Catch:{ SQLException -> 0x0193, all -> 0x019f }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x0193, all -> 0x019f }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0193, all -> 0x019f }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.H     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            if (r2 == 0) goto L_0x00d8
            java.lang.String r2 = "TL:doRawCheck()"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            r3.<init>()     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            java.lang.String r4 = "query count = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            android.util.Log.d(r2, r1)     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            java.lang.String r1 = "TL:doRawCheck()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            r2.<init>()     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            java.lang.String r3 = "column count = '"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            int r3 = r0.getColumnCount()     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            java.lang.String r3 = "' "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            android.util.Log.d(r1, r2)     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
        L_0x00d8:
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            if (r1 == 0) goto L_0x0112
        L_0x00de:
            r1 = 1
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            r2 = 0
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.H     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            if (r3 == 0) goto L_0x010c
            java.lang.String r3 = "TL:doRawCheck()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            r4.<init>()     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            java.lang.String r4 = " - "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            android.util.Log.d(r3, r1)     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
        L_0x010c:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x01ae, all -> 0x01a7 }
            if (r1 != 0) goto L_0x00de
        L_0x0112:
            if (r0 == 0) goto L_0x0117
            r0.close()     // Catch:{ Exception -> 0x0151 }
        L_0x0117:
            android.database.sqlite.SQLiteCursor r0 = r6.c()     // Catch:{ Exception -> 0x0151 }
            r6.s = r0     // Catch:{ Exception -> 0x0151 }
            android.widget.ListAdapter r0 = r6.d()     // Catch:{ Exception -> 0x0151 }
            r6.setListAdapter(r0)     // Catch:{ Exception -> 0x0151 }
            r0 = 2131361994(0x7f0a00ca, float:1.8343756E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0151 }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x0151 }
            java.lang.String r1 = ""
            r0.setText(r1)     // Catch:{ Exception -> 0x0151 }
            android.widget.ListView r1 = r6.getListView()     // Catch:{ Exception -> 0x0151 }
            r2 = 1
            r1.setFastScrollEnabled(r2)     // Catch:{ Exception -> 0x0151 }
            r1.setEmptyView(r0)     // Catch:{ Exception -> 0x0151 }
        L_0x013d:
            return
        L_0x013e:
            boolean r0 = r6.f748b     // Catch:{ Exception -> 0x0151 }
            goto L_0x001a
        L_0x0142:
            boolean r0 = r6.f748b     // Catch:{ Exception -> 0x0151 }
            goto L_0x0026
        L_0x0146:
            boolean r0 = r6.f748b     // Catch:{ Exception -> 0x0151 }
            goto L_0x0033
        L_0x014a:
            java.lang.String r1 = "Master Tung Points"
            r0.setText(r1)     // Catch:{ Exception -> 0x0151 }
            goto L_0x0055
        L_0x0151:
            r0 = move-exception
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()
            java.lang.String r2 = "myVariable"
            java.lang.String r0 = r0.getLocalizedMessage()
            r1.a(r2, r0)
            org.acra.ErrorReporter r0 = org.acra.ErrorReporter.a()
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r2 = "TL:onCreate()"
            r1.<init>(r2)
            r0.handleSilentException(r1)
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r6)
            android.app.AlertDialog r0 = r0.create()
            java.lang.String r1 = "Attention:"
            r0.setTitle(r1)
            r1 = 2131099670(0x7f060016, float:1.78117E38)
            java.lang.String r1 = r6.getString(r1)
            r0.setMessage(r1)
            java.lang.String r1 = "OK"
            com.cyberandsons.tcmaidtrial.lists.u r2 = new com.cyberandsons.tcmaidtrial.lists.u
            r2.<init>(r6)
            r0.setButton(r1, r2)
            r0.show()
            goto L_0x013d
        L_0x0193:
            r0 = move-exception
            r1 = r3
        L_0x0195:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x01ac }
            if (r1 == 0) goto L_0x0117
            r1.close()     // Catch:{ Exception -> 0x0151 }
            goto L_0x0117
        L_0x019f:
            r0 = move-exception
            r1 = r3
        L_0x01a1:
            if (r1 == 0) goto L_0x01a6
            r1.close()     // Catch:{ Exception -> 0x0151 }
        L_0x01a6:
            throw r0     // Catch:{ Exception -> 0x0151 }
        L_0x01a7:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x01a1
        L_0x01ac:
            r0 = move-exception
            goto L_0x01a1
        L_0x01ae:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0195
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.TungList.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public final void a() {
        CharSequence[] charSequenceArr = {"Pinyin w/Simplified Characters", "Pinyin w/Traditional Characters", "English"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Display Formulas by ...");
        builder.setSingleChoiceItems(charSequenceArr, this.x ? 2 : this.z == 0 ? 0 : 1, new t(this));
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        startActivityForResult(new Intent(this, TungAdd.class), 1350);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        e();
        if (TcmAid.cI) {
            startActivity(getIntent());
            finish();
            TcmAid.cI = false;
        }
        if (TcmAid.bl) {
            TcmAid.bl = this.f748b;
            try {
                if (dh.e) {
                    if (TcmAid.bb > 0) {
                        TcmAid.bb--;
                        TcmAid.aY = (String) TcmAid.bc.get(TcmAid.bb);
                        TcmAid.bc.remove(TcmAid.bb);
                        if (dh.W) {
                            Log.d("TL:onResume()", "tuCounter++ = " + Integer.toString(TcmAid.bb) + ", tuCounterArrary.count = " + Integer.toString(TcmAid.bc.size()));
                        }
                    }
                    if (dh.ah) {
                        Log.d("TL:onResume()", TcmAid.aY);
                    }
                }
                stopManagingCursor(this.s);
                if (this.s != null) {
                    this.s.close();
                    this.s = null;
                }
                TcmAid.aW = null;
                this.s = c();
                setListAdapter(d());
                getListView().invalidate();
                getListView().onRestoreInstanceState(this.c);
            } catch (SQLException e2) {
                q.a(e2);
            } catch (Exception e3) {
                Log.e("TL:onResume()", e3.getLocalizedMessage());
            }
        }
        super.onResume();
    }

    public void onDestroy() {
        try {
            TcmAid.aW = null;
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            if (this.t != null && this.t.isOpen()) {
                this.t.close();
                this.t = null;
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        if (i2 >= 0) {
            super.onListItemClick(listView, view, i2, j2);
            if (dh.H) {
                Log.d("TL:onListItemClick()", "position = " + Integer.toString(i2) + ", id = " + Long.toString(j2));
            }
            TcmAid.aW = "main.mastertung._id=" + Integer.toString((int) j2);
            TcmAid.ba = (int) j2;
            TcmAid.aT = i2;
            if (TcmAid.aY != null) {
                TcmAid.bb++;
                TcmAid.bc.add(TcmAid.aY);
                if (dh.Y) {
                    Log.d("TungList:", "tuCounter++ = " + Integer.toString(TcmAid.bb) + ", tuCounterArrary.count = " + Integer.toString(TcmAid.bc.size()));
                }
            }
            this.c = getListView().onSaveInstanceState();
            startActivityForResult(new Intent(this, TungDetail.class), 1350);
        }
    }

    /* access modifiers changed from: private */
    public SQLiteCursor c() {
        try {
            if (dh.H) {
                Log.d("TL:createCursor()", "Inserting into t_tung");
            }
            if (TcmAid.aY == null) {
                TcmAid.aY = "SELECT main.mastertung._id, main.mastertung.point_number, main.mastertung.name, main.mastertung.english_name FROM main.mastertung " + " UNION " + "SELECT userDB.mastertung._id, userDB.mastertung.number, userDB.mastertung.points, userDB.mastertung.english_name FROM userDB.mastertung " + "WHERE userDB.mastertung._id>1000 ORDER BY 3";
            }
            this.t.execSQL("DELETE FROM t_tung");
            this.t.execSQL("INSERT INTO t_tung (_id, point_number, name, english_name) " + TcmAid.aY);
        } catch (SQLException e2) {
            q.a(e2);
        }
        if (TcmAid.aW != null) {
            this.j = TcmAid.aW;
            TcmAid.aW = null;
            this.k = TcmAid.aX;
            TcmAid.aX = null;
        }
        try {
            this.s = (SQLiteCursor) this.t.query(q, "t_tung", new String[]{"_id", "point_number", "name", "english_name"}, this.j, this.k, l, m, " CASE WHEN point_number GLOB '*[^0-9.]*' THEN point_number ELSE cast(point_number AS real) END", null);
            startManagingCursor(this.s);
            int count = this.s.getCount();
            if (dh.H) {
                Log.d("TL:createCursor()", "query count = " + Integer.toString(count));
            }
            TcmAid.aU.clear();
            if (this.s.moveToFirst()) {
                do {
                    TcmAid.aU.add(Integer.valueOf(this.s.getInt(0)));
                } while (this.s.moveToNext());
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        return this.s;
    }

    /* access modifiers changed from: private */
    public ListAdapter d() {
        return new z(this, this.s, new String[]{"_id", "point_number", "name"}, new int[]{C0000R.id.text0, C0000R.id.tungNumber, C0000R.id.tungName}, this.x, this.y, this.z, this.t);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }

    private void e() {
        if (this.t == null || !this.t.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("TL:openDataBase()", str + " opened.");
                this.t = SQLiteDatabase.openDatabase(str, null, 16);
                this.t.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.t.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("TL:openDataBase()", str2 + " ATTACHed.");
                this.u = new n();
                this.u.a(this.t);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new s(this));
                create.show();
            }
        }
    }
}
