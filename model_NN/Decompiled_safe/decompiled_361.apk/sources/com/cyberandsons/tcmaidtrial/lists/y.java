package com.cyberandsons.tcmaidtrial.lists;

import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.a.s;
import com.cyberandsons.tcmaidtrial.misc.ad;

final class y implements SimpleCursorAdapter.ViewBinder {

    /* renamed from: a  reason: collision with root package name */
    private boolean f838a = false;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ z f839b;

    y(z zVar) {
        this.f839b = zVar;
    }

    public final boolean setViewValue(View view, Cursor cursor, int i) {
        TextView textView = (TextView) view;
        textView.setTextSize(18.0f);
        if (i == 1) {
            textView.setText(cursor.getString(1));
            this.f838a = true;
        }
        if (i == 2) {
            if (this.f839b.f840a) {
                textView.setText(ad.a(cursor.getString(3)));
            } else {
                int i2 = cursor.getInt(0);
                String a2 = s.a("ttonemarks", "tid", i2, this.f839b.d);
                if (a2 == null || a2.length() == 0) {
                    a2 = cursor.getString(2);
                }
                if (this.f839b.f841b) {
                    String a3 = s.a(this.f839b.c == 0 ? "tscc" : "ttcc", "tid", i2, this.f839b.d);
                    if (a3 != null) {
                        textView.setText(String.format("%s - %s", ad.a(a2), a3));
                    } else {
                        textView.setText(ad.a(a2));
                    }
                } else {
                    textView.setText(ad.a(a2));
                }
            }
            this.f838a = true;
        }
        return this.f838a;
    }
}
