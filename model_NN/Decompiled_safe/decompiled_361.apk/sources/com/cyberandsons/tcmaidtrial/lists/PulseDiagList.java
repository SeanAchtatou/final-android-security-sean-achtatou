package com.cyberandsons.tcmaidtrial.lists;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.additems.PulseDiagAdd;
import com.cyberandsons.tcmaidtrial.detailed.PulseDiagDetail;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class PulseDiagList extends ListActivity {
    private static String k;
    private static String l;
    private static String o = "SELECT _id, name, category FROM pulsediag";
    private static boolean p;

    /* renamed from: a  reason: collision with root package name */
    boolean f741a = true;

    /* renamed from: b  reason: collision with root package name */
    boolean f742b;
    Parcelable c;
    private final String d;
    private final String e;
    private final String f;
    private final String g;
    private final String h;
    private String i;
    private String[] j;
    private final String m;
    private final String n;
    private boolean q;
    /* access modifiers changed from: private */
    public SQLiteCursor r;
    private SQLiteDatabase s;
    private n t;
    /* access modifiers changed from: private */
    public boolean u;
    private boolean v;
    /* access modifiers changed from: private */
    public int w;
    private ImageButton x;
    private ImageButton y;

    public PulseDiagList() {
        this.f742b = !this.f741a;
        this.d = "t_pulsediag";
        this.e = "main.pulsediag.";
        this.f = "_id";
        this.g = "name";
        this.h = "category";
        this.m = "name";
        this.n = "name";
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0170 A[SYNTHETIC, Splitter:B:35:0x0170] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0178 A[Catch:{ Exception -> 0x0127 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r7) {
        /*
            r6 = this;
            r3 = 0
            super.onCreate(r7)
            r6.e()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.d     // Catch:{ Exception -> 0x0127 }
            if (r0 == 0) goto L_0x000f
            r0 = 1
            r6.setRequestedOrientation(r0)     // Catch:{ Exception -> 0x0127 }
        L_0x000f:
            boolean r0 = r6.f741a     // Catch:{ Exception -> 0x0127 }
            r6.v = r0     // Catch:{ Exception -> 0x0127 }
            boolean r0 = r6.f741a     // Catch:{ Exception -> 0x0127 }
            r6.u = r0     // Catch:{ Exception -> 0x0127 }
            com.cyberandsons.tcmaidtrial.a.n r0 = r6.t     // Catch:{ Exception -> 0x0127 }
            int r0 = r0.ac()     // Catch:{ Exception -> 0x0127 }
            r6.w = r0     // Catch:{ Exception -> 0x0127 }
            r0 = 2130903084(0x7f03002c, float:1.7412976E38)
            r6.setContentView(r0)     // Catch:{ Exception -> 0x0127 }
            r0 = 2131362326(0x7f0a0216, float:1.834443E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0127 }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x0127 }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cP     // Catch:{ Exception -> 0x0127 }
            if (r1 == 0) goto L_0x0120
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cP     // Catch:{ Exception -> 0x0127 }
            r0.setText(r1)     // Catch:{ Exception -> 0x0127 }
        L_0x0037:
            r0 = 2131362327(0x7f0a0217, float:1.8344431E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0127 }
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0     // Catch:{ Exception -> 0x0127 }
            r6.x = r0     // Catch:{ Exception -> 0x0127 }
            android.widget.ImageButton r0 = r6.x     // Catch:{ Exception -> 0x0127 }
            com.cyberandsons.tcmaidtrial.lists.br r1 = new com.cyberandsons.tcmaidtrial.lists.br     // Catch:{ Exception -> 0x0127 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0127 }
            r0.setOnClickListener(r1)     // Catch:{ Exception -> 0x0127 }
            r0 = 2131362328(0x7f0a0218, float:1.8344433E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0127 }
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0     // Catch:{ Exception -> 0x0127 }
            r6.y = r0     // Catch:{ Exception -> 0x0127 }
            android.widget.ImageButton r0 = r6.y     // Catch:{ Exception -> 0x0127 }
            com.cyberandsons.tcmaidtrial.lists.bq r1 = new com.cyberandsons.tcmaidtrial.lists.bq     // Catch:{ Exception -> 0x0127 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0127 }
            r0.setOnClickListener(r1)     // Catch:{ Exception -> 0x0127 }
            boolean r0 = r6.q     // Catch:{ Exception -> 0x0127 }
            if (r0 == 0) goto L_0x00f9
            android.database.sqlite.SQLiteDatabase r0 = r6.s     // Catch:{ SQLException -> 0x0169, all -> 0x0174 }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.lists.PulseDiagList.o     // Catch:{ SQLException -> 0x0169, all -> 0x0174 }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x0169, all -> 0x0174 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0169, all -> 0x0174 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.D     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            if (r2 == 0) goto L_0x00ba
            java.lang.String r2 = "PDL:doRawCheck()"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            r3.<init>()     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            java.lang.String r4 = "query count = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            android.util.Log.d(r2, r1)     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            java.lang.String r1 = "PDL:doRawCheck()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            r2.<init>()     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            java.lang.String r3 = "column count = '"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            int r3 = r0.getColumnCount()     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            java.lang.String r3 = "' "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            android.util.Log.d(r1, r2)     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
        L_0x00ba:
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            if (r1 == 0) goto L_0x00f4
        L_0x00c0:
            r1 = 1
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            r2 = 0
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.D     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            if (r3 == 0) goto L_0x00ee
            java.lang.String r3 = "PDL:doRawCheck()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            r4.<init>()     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            java.lang.String r4 = " - "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            android.util.Log.d(r3, r1)     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
        L_0x00ee:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0183, all -> 0x017c }
            if (r1 != 0) goto L_0x00c0
        L_0x00f4:
            if (r0 == 0) goto L_0x00f9
            r0.close()     // Catch:{ Exception -> 0x0127 }
        L_0x00f9:
            android.database.sqlite.SQLiteCursor r0 = r6.c()     // Catch:{ Exception -> 0x0127 }
            r6.r = r0     // Catch:{ Exception -> 0x0127 }
            android.widget.ListAdapter r0 = r6.d()     // Catch:{ Exception -> 0x0127 }
            r6.setListAdapter(r0)     // Catch:{ Exception -> 0x0127 }
            r0 = 2131361994(0x7f0a00ca, float:1.8343756E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0127 }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x0127 }
            java.lang.String r1 = ""
            r0.setText(r1)     // Catch:{ Exception -> 0x0127 }
            android.widget.ListView r1 = r6.getListView()     // Catch:{ Exception -> 0x0127 }
            r2 = 1
            r1.setFastScrollEnabled(r2)     // Catch:{ Exception -> 0x0127 }
            r1.setEmptyView(r0)     // Catch:{ Exception -> 0x0127 }
        L_0x011f:
            return
        L_0x0120:
            java.lang.String r1 = "Pulse Diagnosis"
            r0.setText(r1)     // Catch:{ Exception -> 0x0127 }
            goto L_0x0037
        L_0x0127:
            r0 = move-exception
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()
            java.lang.String r2 = "myVariable"
            java.lang.String r0 = r0.getLocalizedMessage()
            r1.a(r2, r0)
            org.acra.ErrorReporter r0 = org.acra.ErrorReporter.a()
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r2 = "PDL:onCreate()"
            r1.<init>(r2)
            r0.handleSilentException(r1)
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r6)
            android.app.AlertDialog r0 = r0.create()
            java.lang.String r1 = "Attention:"
            r0.setTitle(r1)
            r1 = 2131099670(0x7f060016, float:1.78117E38)
            java.lang.String r1 = r6.getString(r1)
            r0.setMessage(r1)
            java.lang.String r1 = "OK"
            com.cyberandsons.tcmaidtrial.lists.bp r2 = new com.cyberandsons.tcmaidtrial.lists.bp
            r2.<init>(r6)
            r0.setButton(r1, r2)
            r0.show()
            goto L_0x011f
        L_0x0169:
            r0 = move-exception
            r1 = r3
        L_0x016b:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0181 }
            if (r1 == 0) goto L_0x00f9
            r1.close()     // Catch:{ Exception -> 0x0127 }
            goto L_0x00f9
        L_0x0174:
            r0 = move-exception
            r1 = r3
        L_0x0176:
            if (r1 == 0) goto L_0x017b
            r1.close()     // Catch:{ Exception -> 0x0127 }
        L_0x017b:
            throw r0     // Catch:{ Exception -> 0x0127 }
        L_0x017c:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0176
        L_0x0181:
            r0 = move-exception
            goto L_0x0176
        L_0x0183:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x016b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.PulseDiagList.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public final void a() {
        CharSequence[] charSequenceArr = {"Pinyin w/Simplified Characters", "Pinyin w/Traditional Characters", "English"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Display Formulas by ...");
        builder.setSingleChoiceItems(charSequenceArr, this.u ? 2 : this.w == 0 ? 0 : 1, new bo(this));
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        startActivityForResult(new Intent(this, PulseDiagAdd.class), 1350);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        e();
        if (TcmAid.cG) {
            startActivity(getIntent());
            finish();
            TcmAid.cG = false;
        }
        if (TcmAid.bl) {
            TcmAid.bl = this.f742b;
            try {
                if (dh.e) {
                    if (TcmAid.aI > 0) {
                        TcmAid.aI--;
                        TcmAid.aF = (String) TcmAid.aJ.get(TcmAid.aI);
                        TcmAid.aJ.remove(TcmAid.aI);
                        if (dh.V) {
                            Log.d("PDL:onResume()", "pdCounter++ = " + Integer.toString(TcmAid.aI) + ", pdCounterArrary.count = " + Integer.toString(TcmAid.aJ.size()));
                        }
                    }
                    if (dh.af) {
                        Log.d("PDL:onResume()", TcmAid.aF);
                    }
                }
                stopManagingCursor(this.r);
                if (this.r != null) {
                    this.r.close();
                    this.r = null;
                }
                TcmAid.aD = null;
                this.r = c();
                setListAdapter(d());
                getListView().invalidate();
                getListView().onRestoreInstanceState(this.c);
            } catch (SQLException e2) {
                q.a(e2);
            } catch (Exception e3) {
                Log.e("PDL:onResume()", e3.getLocalizedMessage());
            }
        }
        super.onResume();
    }

    public void onDestroy() {
        try {
            TcmAid.aD = null;
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            if (this.s != null && this.s.isOpen()) {
                this.s.close();
                this.s = null;
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        if (i2 >= 0) {
            super.onListItemClick(listView, view, i2, j2);
            if (dh.D) {
                Log.d("PDL:onListItemClick()", "position = " + Integer.toString(i2) + ", id = " + Long.toString(j2));
            }
            TcmAid.aD = "main.pulsediag._id=" + Integer.toString((int) j2);
            TcmAid.aH = (int) j2;
            TcmAid.ay = i2;
            if (TcmAid.aF != null) {
                TcmAid.aI++;
                TcmAid.aJ.add(TcmAid.aF);
                if (dh.V) {
                    Log.d("PulseDiagList:", "pdCounter++ = " + Integer.toString(TcmAid.aI) + ", pdCounterArrary.count = " + Integer.toString(TcmAid.aJ.size()));
                }
            }
            this.c = getListView().onSaveInstanceState();
            startActivityForResult(new Intent(this, PulseDiagDetail.class), 1350);
        }
    }

    /* access modifiers changed from: private */
    public SQLiteCursor c() {
        boolean z;
        try {
            if (dh.D) {
                Log.d("PDL:createCursor()", "Inserting into t_pulsediag");
            }
            if (TcmAid.aF == null) {
                TcmAid.aF = "SELECT main.pulsediag._id, main.pulsediag.name, main.pulsediag.category FROM main.pulsediag " + " UNION " + "SELECT userDB.pulsediag._id, userDB.pulsediag.name, userDB.pulsediag.category FROM userDB.pulsediag " + " WHERE userDB.pulsediag._id>1000 ORDER BY 2 ";
            }
            this.s.execSQL("DELETE FROM t_pulsediag");
            this.s.execSQL("INSERT INTO t_pulsediag (_id, name, category) " + TcmAid.aF);
        } catch (SQLException e2) {
            q.a(e2);
        }
        if (TcmAid.aD != null) {
            this.i = TcmAid.aD;
            TcmAid.aD = null;
            this.j = TcmAid.aE;
            TcmAid.aE = null;
        }
        try {
            String[] strArr = {"_id", "name", "category"};
            boolean z2 = this.f742b;
            if (this.t.B()) {
                z = this.f741a;
            } else {
                z = z2;
            }
            this.r = (SQLiteCursor) this.s.query(p, "t_pulsediag", strArr, this.i, this.j, k, l, z ? null : "name", null);
            startManagingCursor(this.r);
            int count = this.r.getCount();
            if (dh.D) {
                Log.d("PDL:createCursor()", "query count = " + Integer.toString(count));
            }
            TcmAid.az.clear();
            if (this.r.moveToFirst()) {
                do {
                    TcmAid.az.add(Integer.valueOf(this.r.getInt(0)));
                } while (this.r.moveToNext());
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        return this.r;
    }

    /* access modifiers changed from: private */
    public ListAdapter d() {
        return new i(this, this.r, new String[]{"_id", "name"}, new int[]{C0000R.id.text0, C0000R.id.text1}, this.u, this.v, this.w, this.s);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }

    private void e() {
        if (this.s == null || !this.s.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("PDL:openDataBase()", str + " opened.");
                this.s = SQLiteDatabase.openDatabase(str, null, 16);
                this.s.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.s.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("PDL:openDataBase()", str2 + " ATTACHed.");
                this.t = new n();
                this.t.a(this.s);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new bz(this));
                create.show();
            }
        }
    }
}
