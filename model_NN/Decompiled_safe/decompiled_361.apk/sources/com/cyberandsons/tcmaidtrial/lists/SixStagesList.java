package com.cyberandsons.tcmaidtrial.lists;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.additems.SixStagesAdd;
import com.cyberandsons.tcmaidtrial.detailed.SixStagesDetail;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class SixStagesList extends ListActivity {
    private static String i;
    private static String j;
    private static String m = "SELECT _id, pathology FROM sixchannels";
    private static boolean n;

    /* renamed from: a  reason: collision with root package name */
    private boolean f745a = true;

    /* renamed from: b  reason: collision with root package name */
    private boolean f746b;
    private final String c;
    private final String d;
    private final String e;
    private final String f;
    private String g;
    private String[] h;
    private final String k;
    private final String l;
    private boolean o;
    private SQLiteCursor p;
    private SQLiteDatabase q;
    private n r;
    private ImageButton s;
    private Parcelable t;

    public SixStagesList() {
        this.f746b = !this.f745a;
        this.c = "t_sixchannels";
        this.d = "main.sixchannels.";
        this.e = "_id";
        this.f = "pathology";
        this.k = "pathology";
        this.l = "pathology";
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x014b A[SYNTHETIC, Splitter:B:35:0x014b] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0153 A[Catch:{ Exception -> 0x0102 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r7) {
        /*
            r6 = this;
            r3 = 0
            super.onCreate(r7)
            r6.d()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.d     // Catch:{ Exception -> 0x0102 }
            if (r0 == 0) goto L_0x000f
            r0 = 1
            r6.setRequestedOrientation(r0)     // Catch:{ Exception -> 0x0102 }
        L_0x000f:
            r0 = 2130903079(0x7f030027, float:1.7412966E38)
            r6.setContentView(r0)     // Catch:{ Exception -> 0x0102 }
            r0 = 2131362326(0x7f0a0216, float:1.834443E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0102 }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x0102 }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cP     // Catch:{ Exception -> 0x0102 }
            if (r1 == 0) goto L_0x00fb
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cP     // Catch:{ Exception -> 0x0102 }
            r0.setText(r1)     // Catch:{ Exception -> 0x0102 }
        L_0x0027:
            r0 = 2131362327(0x7f0a0217, float:1.8344431E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0102 }
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0     // Catch:{ Exception -> 0x0102 }
            r6.s = r0     // Catch:{ Exception -> 0x0102 }
            android.widget.ImageButton r0 = r6.s     // Catch:{ Exception -> 0x0102 }
            com.cyberandsons.tcmaidtrial.lists.an r1 = new com.cyberandsons.tcmaidtrial.lists.an     // Catch:{ Exception -> 0x0102 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0102 }
            r0.setOnClickListener(r1)     // Catch:{ Exception -> 0x0102 }
            boolean r0 = r6.o     // Catch:{ Exception -> 0x0102 }
            if (r0 == 0) goto L_0x00d4
            android.database.sqlite.SQLiteDatabase r0 = r6.q     // Catch:{ SQLException -> 0x0144, all -> 0x014f }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.lists.SixStagesList.m     // Catch:{ SQLException -> 0x0144, all -> 0x014f }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x0144, all -> 0x014f }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0144, all -> 0x014f }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.F     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            if (r2 == 0) goto L_0x0095
            java.lang.String r2 = "SSL:doRawCheck()"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            r3.<init>()     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            java.lang.String r4 = "query count = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            android.util.Log.d(r2, r1)     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            java.lang.String r1 = "SSL:doRawCheck()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            r2.<init>()     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            java.lang.String r3 = "column count = '"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            int r3 = r0.getColumnCount()     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            java.lang.String r3 = "' "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            android.util.Log.d(r1, r2)     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
        L_0x0095:
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            if (r1 == 0) goto L_0x00cf
        L_0x009b:
            r1 = 1
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            r2 = 0
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.F     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            if (r3 == 0) goto L_0x00c9
            java.lang.String r3 = "SSL:doRawCheck()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            r4.<init>()     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            java.lang.String r4 = " - "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            android.util.Log.d(r3, r1)     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
        L_0x00c9:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x015e, all -> 0x0157 }
            if (r1 != 0) goto L_0x009b
        L_0x00cf:
            if (r0 == 0) goto L_0x00d4
            r0.close()     // Catch:{ Exception -> 0x0102 }
        L_0x00d4:
            android.database.sqlite.SQLiteCursor r0 = r6.b()     // Catch:{ Exception -> 0x0102 }
            r6.p = r0     // Catch:{ Exception -> 0x0102 }
            android.widget.ListAdapter r0 = r6.c()     // Catch:{ Exception -> 0x0102 }
            r6.setListAdapter(r0)     // Catch:{ Exception -> 0x0102 }
            r0 = 2131361994(0x7f0a00ca, float:1.8343756E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0102 }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x0102 }
            java.lang.String r1 = ""
            r0.setText(r1)     // Catch:{ Exception -> 0x0102 }
            android.widget.ListView r1 = r6.getListView()     // Catch:{ Exception -> 0x0102 }
            r2 = 1
            r1.setFastScrollEnabled(r2)     // Catch:{ Exception -> 0x0102 }
            r1.setEmptyView(r0)     // Catch:{ Exception -> 0x0102 }
        L_0x00fa:
            return
        L_0x00fb:
            java.lang.String r1 = "Six Stages"
            r0.setText(r1)     // Catch:{ Exception -> 0x0102 }
            goto L_0x0027
        L_0x0102:
            r0 = move-exception
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()
            java.lang.String r2 = "myVariable"
            java.lang.String r0 = r0.getLocalizedMessage()
            r1.a(r2, r0)
            org.acra.ErrorReporter r0 = org.acra.ErrorReporter.a()
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r2 = "SSL:onCreate()"
            r1.<init>(r2)
            r0.handleSilentException(r1)
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r6)
            android.app.AlertDialog r0 = r0.create()
            java.lang.String r1 = "Attention:"
            r0.setTitle(r1)
            r1 = 2131099670(0x7f060016, float:1.78117E38)
            java.lang.String r1 = r6.getString(r1)
            r0.setMessage(r1)
            java.lang.String r1 = "OK"
            com.cyberandsons.tcmaidtrial.lists.aq r2 = new com.cyberandsons.tcmaidtrial.lists.aq
            r2.<init>(r6)
            r0.setButton(r1, r2)
            r0.show()
            goto L_0x00fa
        L_0x0144:
            r0 = move-exception
            r1 = r3
        L_0x0146:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x015c }
            if (r1 == 0) goto L_0x00d4
            r1.close()     // Catch:{ Exception -> 0x0102 }
            goto L_0x00d4
        L_0x014f:
            r0 = move-exception
            r1 = r3
        L_0x0151:
            if (r1 == 0) goto L_0x0156
            r1.close()     // Catch:{ Exception -> 0x0102 }
        L_0x0156:
            throw r0     // Catch:{ Exception -> 0x0102 }
        L_0x0157:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0151
        L_0x015c:
            r0 = move-exception
            goto L_0x0151
        L_0x015e:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0146
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.SixStagesList.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public final void a() {
        startActivityForResult(new Intent(this, SixStagesAdd.class), 1350);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        d();
        if (TcmAid.cH) {
            startActivity(getIntent());
            finish();
            TcmAid.cH = false;
        }
        if (TcmAid.bl) {
            TcmAid.bl = this.f746b;
            try {
                if (dh.e) {
                    if (TcmAid.aR > 0) {
                        TcmAid.aR--;
                        TcmAid.aP = (String) TcmAid.aS.get(TcmAid.aR);
                        TcmAid.aS.remove(TcmAid.aR);
                        if (dh.X) {
                            Log.d("SSL:onResume()", "ssCounter++ = " + Integer.toString(TcmAid.aR) + ", ssCounterArrary.count = " + Integer.toString(TcmAid.aS.size()));
                        }
                    }
                    if (dh.ag) {
                        Log.d("SSL:onResume()", TcmAid.aP);
                    }
                }
                stopManagingCursor(this.p);
                if (this.p != null) {
                    this.p.close();
                    this.p = null;
                }
                TcmAid.aN = null;
                this.p = b();
                setListAdapter(c());
                getListView().invalidate();
                getListView().onRestoreInstanceState(this.t);
            } catch (SQLException e2) {
                q.a(e2);
            } catch (Exception e3) {
                Log.e("SSL:onResume()", e3.getLocalizedMessage());
            }
        }
        super.onResume();
    }

    public void onDestroy() {
        try {
            TcmAid.aN = null;
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            if (this.q != null && this.q.isOpen()) {
                this.q.close();
                this.q = null;
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        if (i2 >= 0) {
            super.onListItemClick(listView, view, i2, j2);
            if (dh.F) {
                Log.d("SSL:onListItemClick()", "position = " + Integer.toString(i2) + ", id = " + Long.toString(j2));
            }
            TcmAid.aN = "main.sixchannels._id=" + Integer.toString((int) j2);
            TcmAid.aQ = (int) j2;
            TcmAid.aK = i2;
            if (TcmAid.aP != null) {
                TcmAid.aR++;
                TcmAid.aS.add(TcmAid.aP);
                if (dh.X) {
                    Log.d("SixStagesList:", "ssCounter++ = " + Integer.toString(TcmAid.aR) + ", ssCounterArrary.count = " + Integer.toString(TcmAid.aS.size()));
                }
            }
            this.t = getListView().onSaveInstanceState();
            startActivityForResult(new Intent(this, SixStagesDetail.class), 1350);
        }
    }

    private SQLiteCursor b() {
        boolean z;
        try {
            if (dh.F) {
                Log.d("SSL:createCursor()", "Inserting into t_sixchannels");
            }
            if (TcmAid.aP == null) {
                TcmAid.aP = "SELECT main.sixchannels._id, lower(main.sixchannels.pathology) FROM main.sixchannels " + " UNION " + "SELECT userDB.sixchannels._id, lower(userDB.sixchannels.pathology) FROM userDB.sixchannels " + " WHERE userDB.sixchannels._id>1000 " + (this.r.B() ? "" : " ORDER BY 2 ");
            }
            this.q.execSQL("DELETE FROM t_sixchannels");
            this.q.execSQL("INSERT INTO t_sixchannels (_id, pathology) " + TcmAid.aP);
        } catch (SQLException e2) {
            q.a(e2);
        }
        if (TcmAid.aN != null) {
            this.g = TcmAid.aN;
            TcmAid.aN = null;
            this.h = TcmAid.aO;
            TcmAid.aO = null;
        }
        try {
            String[] strArr = {"_id", "pathology"};
            boolean z2 = this.f746b;
            if (this.r.B()) {
                z = this.f745a;
            } else {
                z = z2;
            }
            this.p = (SQLiteCursor) this.q.query(n, "t_sixchannels", strArr, this.g, this.h, i, j, z ? null : "pathology", null);
            startManagingCursor(this.p);
            int count = this.p.getCount();
            if (dh.F) {
                Log.d("SSL:createCursor()", "query count = " + Integer.toString(count));
            }
            TcmAid.aL.clear();
            if (this.p.moveToFirst()) {
                do {
                    TcmAid.aL.add(Integer.valueOf(this.p.getInt(0)));
                } while (this.p.moveToNext());
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        return this.p;
    }

    private ListAdapter c() {
        return new bu(this, this.p, new String[]{"_id", "pathology"}, new int[]{C0000R.id.text0, C0000R.id.text1}, "pathology");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }

    private void d() {
        if (this.q == null || !this.q.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("SSL:openDataBase()", str + " opened.");
                this.q = SQLiteDatabase.openDatabase(str, null, 16);
                this.q.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.q.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("SSL:openDataBase()", str2 + " ATTACHed.");
                this.r = new n();
                this.r.a(this.q);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new ap(this));
                create.show();
            }
        }
    }
}
