package com.cyberandsons.tcmaidtrial.lists;

import android.content.DialogInterface;
import android.database.SQLException;
import com.cyberandsons.tcmaidtrial.a.q;

final class p implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FormulasList f828a;

    p(FormulasList formulasList) {
        this.f828a = formulasList;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        try {
            this.f828a.p.execSQL("DELETE FROM userDB.formulascategory WHERE _id=" + Integer.toString(this.f828a.z));
            this.f828a.finish();
        } catch (SQLException e) {
            q.a(e);
        }
    }
}
