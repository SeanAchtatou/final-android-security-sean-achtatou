package com.cyberandsons.tcmaidtrial.lists;

import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.a.s;
import com.cyberandsons.tcmaidtrial.misc.ad;

final class aj implements SimpleCursorAdapter.ViewBinder {

    /* renamed from: a  reason: collision with root package name */
    private boolean f764a = false;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ bv f765b;

    aj(bv bvVar) {
        this.f765b = bvVar;
    }

    public final boolean setViewValue(View view, Cursor cursor, int i) {
        TextView textView = (TextView) view;
        textView.setTextSize(18.0f);
        if (i == 1) {
            int i2 = cursor.getInt(0);
            int i3 = cursor.getInt(3);
            String a2 = s.a("mtonemarks", "mid", i2, this.f765b.d);
            if (a2 == null || a2.length() == 0 || !this.f765b.f808a) {
                a2 = cursor.getString(1);
            }
            if (this.f765b.f809b) {
                String a3 = s.a(this.f765b.c == 0 ? "mscc" : "mtcc", "mid", i2, this.f765b.d);
                if (a3 != null) {
                    textView.setText(String.format("%s\n%s", ad.a(a2), a3));
                } else {
                    textView.setText(ad.a(a2));
                }
            } else {
                textView.setText(ad.a(a2));
            }
            if (this.f765b.j) {
                switch (i3) {
                    case 0:
                        textView.setTextColor(this.f765b.e);
                        break;
                    case 1:
                        textView.setTextColor(this.f765b.f);
                        break;
                    case 2:
                        textView.setTextColor(this.f765b.g);
                        break;
                    case 3:
                        textView.setTextColor(this.f765b.h);
                        break;
                    case 4:
                        textView.setTextColor(this.f765b.i);
                        break;
                }
            }
            this.f764a = true;
        }
        return this.f764a;
    }
}
