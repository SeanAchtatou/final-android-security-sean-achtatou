package com.cyberandsons.tcmaidtrial.lists;

import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.a.s;
import com.cyberandsons.tcmaidtrial.misc.ad;

final class bl implements SimpleCursorAdapter.ViewBinder {

    /* renamed from: a  reason: collision with root package name */
    private boolean f797a = false;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ i f798b;

    bl(i iVar) {
        this.f798b = iVar;
    }

    public final boolean setViewValue(View view, Cursor cursor, int i) {
        TextView textView = (TextView) view;
        if (i == 1) {
            if (this.f798b.f820a) {
                textView.setText(ad.a(cursor.getString(1)));
            } else {
                int i2 = cursor.getInt(0);
                String a2 = s.a("pdtonemarks", "pdid", i2, this.f798b.d);
                if (a2 == null || a2.length() == 0) {
                    a2 = cursor.getString(1);
                }
                if (this.f798b.f821b) {
                    String a3 = s.a(this.f798b.c == 0 ? "pdscc" : "pdtcc", "pdid", i2, this.f798b.d);
                    if (a3 != null) {
                        textView.setText(String.format("%s - %s", ad.a(a2), a3));
                    } else {
                        textView.setText(ad.a(a2));
                    }
                } else {
                    textView.setText(ad.a(a2));
                }
            }
            this.f797a = true;
        }
        return this.f797a;
    }
}
