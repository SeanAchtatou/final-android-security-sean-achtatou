package com.cyberandsons.tcmaidtrial.lists;

import android.content.Intent;
import android.view.View;
import com.cyberandsons.tcmaidtrial.additems.HerbAdd;

final class ax implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HerbList f781a;

    ax(HerbList herbList) {
        this.f781a = herbList;
    }

    public final void onClick(View view) {
        HerbList herbList = this.f781a;
        herbList.startActivityForResult(new Intent(herbList, HerbAdd.class), 1350);
    }
}
