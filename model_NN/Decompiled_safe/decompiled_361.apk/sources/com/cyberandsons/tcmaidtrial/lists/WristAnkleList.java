package com.cyberandsons.tcmaidtrial.lists;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.detailed.WristAnkleDetail;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class WristAnkleList extends ListActivity {
    private static String g;
    private static String h;
    private static String k = "SELECT _id, zone FROM wristankle";
    private static boolean l;

    /* renamed from: a  reason: collision with root package name */
    private final String f749a = "t_wristankle";

    /* renamed from: b  reason: collision with root package name */
    private final String f750b = "main.wristankle.";
    private final String c = "_id";
    private final String d = "zone";
    private String e;
    private String[] f;
    private final String i = "zone";
    private final String j = "zone";
    private boolean m;
    private boolean n = true;
    private boolean o;
    private SQLiteCursor p;
    private SQLiteDatabase q;
    private n r;
    private Parcelable s;

    public WristAnkleList() {
        boolean z;
        if (!this.n) {
            z = true;
        } else {
            z = false;
        }
        this.o = z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0136 A[SYNTHETIC, Splitter:B:35:0x0136] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x013e A[Catch:{ Exception -> 0x00ed }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r7) {
        /*
            r6 = this;
            r3 = 0
            super.onCreate(r7)
            r6.c()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.d     // Catch:{ Exception -> 0x00ed }
            if (r0 == 0) goto L_0x000f
            r0 = 1
            r6.setRequestedOrientation(r0)     // Catch:{ Exception -> 0x00ed }
        L_0x000f:
            r0 = 2130903080(0x7f030028, float:1.7412968E38)
            r6.setContentView(r0)     // Catch:{ Exception -> 0x00ed }
            r0 = 2131361903(0x7f0a006f, float:1.8343571E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x00ed }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x00ed }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cP     // Catch:{ Exception -> 0x00ed }
            if (r1 == 0) goto L_0x00e6
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cP     // Catch:{ Exception -> 0x00ed }
            r0.setText(r1)     // Catch:{ Exception -> 0x00ed }
        L_0x0027:
            boolean r0 = r6.m     // Catch:{ Exception -> 0x00ed }
            if (r0 == 0) goto L_0x00bf
            android.database.sqlite.SQLiteDatabase r0 = r6.q     // Catch:{ SQLException -> 0x012f, all -> 0x013a }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.lists.WristAnkleList.k     // Catch:{ SQLException -> 0x012f, all -> 0x013a }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x012f, all -> 0x013a }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x012f, all -> 0x013a }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.J     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            if (r2 == 0) goto L_0x0080
            java.lang.String r2 = "WAL:doRawCheck()"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            r3.<init>()     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            java.lang.String r4 = "query count = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            android.util.Log.d(r2, r1)     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            java.lang.String r1 = "WAL:doRawCheck()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            r2.<init>()     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            java.lang.String r3 = "column count = '"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            int r3 = r0.getColumnCount()     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            java.lang.String r3 = "' "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            android.util.Log.d(r1, r2)     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
        L_0x0080:
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            if (r1 == 0) goto L_0x00ba
        L_0x0086:
            r1 = 1
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            r2 = 0
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.J     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            if (r3 == 0) goto L_0x00b4
            java.lang.String r3 = "WAL:doRawCheck()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            r4.<init>()     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            java.lang.String r4 = " - "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            android.util.Log.d(r3, r1)     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
        L_0x00b4:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0149, all -> 0x0142 }
            if (r1 != 0) goto L_0x0086
        L_0x00ba:
            if (r0 == 0) goto L_0x00bf
            r0.close()     // Catch:{ Exception -> 0x00ed }
        L_0x00bf:
            android.database.sqlite.SQLiteCursor r0 = r6.a()     // Catch:{ Exception -> 0x00ed }
            r6.p = r0     // Catch:{ Exception -> 0x00ed }
            android.widget.ListAdapter r0 = r6.b()     // Catch:{ Exception -> 0x00ed }
            r6.setListAdapter(r0)     // Catch:{ Exception -> 0x00ed }
            r0 = 2131361994(0x7f0a00ca, float:1.8343756E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x00ed }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x00ed }
            java.lang.String r1 = ""
            r0.setText(r1)     // Catch:{ Exception -> 0x00ed }
            android.widget.ListView r1 = r6.getListView()     // Catch:{ Exception -> 0x00ed }
            r2 = 1
            r1.setFastScrollEnabled(r2)     // Catch:{ Exception -> 0x00ed }
            r1.setEmptyView(r0)     // Catch:{ Exception -> 0x00ed }
        L_0x00e5:
            return
        L_0x00e6:
            java.lang.String r1 = "WristAnkle Points"
            r0.setText(r1)     // Catch:{ Exception -> 0x00ed }
            goto L_0x0027
        L_0x00ed:
            r0 = move-exception
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()
            java.lang.String r2 = "myVariable"
            java.lang.String r0 = r0.getLocalizedMessage()
            r1.a(r2, r0)
            org.acra.ErrorReporter r0 = org.acra.ErrorReporter.a()
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r2 = "WAL:onCreate()"
            r1.<init>(r2)
            r0.handleSilentException(r1)
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r6)
            android.app.AlertDialog r0 = r0.create()
            java.lang.String r1 = "Attention:"
            r0.setTitle(r1)
            r1 = 2131099670(0x7f060016, float:1.78117E38)
            java.lang.String r1 = r6.getString(r1)
            r0.setMessage(r1)
            java.lang.String r1 = "OK"
            com.cyberandsons.tcmaidtrial.lists.am r2 = new com.cyberandsons.tcmaidtrial.lists.am
            r2.<init>(r6)
            r0.setButton(r1, r2)
            r0.show()
            goto L_0x00e5
        L_0x012f:
            r0 = move-exception
            r1 = r3
        L_0x0131:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0147 }
            if (r1 == 0) goto L_0x00bf
            r1.close()     // Catch:{ Exception -> 0x00ed }
            goto L_0x00bf
        L_0x013a:
            r0 = move-exception
            r1 = r3
        L_0x013c:
            if (r1 == 0) goto L_0x0141
            r1.close()     // Catch:{ Exception -> 0x00ed }
        L_0x0141:
            throw r0     // Catch:{ Exception -> 0x00ed }
        L_0x0142:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x013c
        L_0x0147:
            r0 = move-exception
            goto L_0x013c
        L_0x0149:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0131
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.WristAnkleList.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        c();
        if (TcmAid.cJ) {
            startActivity(getIntent());
            finish();
            TcmAid.cJ = false;
        }
        if (TcmAid.bl) {
            TcmAid.bl = this.o;
            try {
                if (dh.e) {
                    if (TcmAid.bj > 0) {
                        TcmAid.bj--;
                        TcmAid.bh = (String) TcmAid.bk.get(TcmAid.bj);
                        TcmAid.bk.remove(TcmAid.bj);
                        if (dh.Z) {
                            Log.d("WAL:onResume()", "waCounter++ = " + Integer.toString(TcmAid.bj) + ", waCounterArrary.count = " + Integer.toString(TcmAid.bk.size()));
                        }
                    }
                    if (dh.ai) {
                        Log.d("WAL:onResume()", TcmAid.bh);
                    }
                }
                stopManagingCursor(this.p);
                if (this.p != null) {
                    this.p.close();
                    this.p = null;
                }
                TcmAid.bf = null;
                this.p = a();
                setListAdapter(b());
                getListView().invalidate();
                getListView().onRestoreInstanceState(this.s);
            } catch (SQLException e2) {
                q.a(e2);
            } catch (Exception e3) {
                Log.e("WAL:onResume()", e3.getLocalizedMessage());
            }
        }
        super.onResume();
    }

    public void onDestroy() {
        try {
            TcmAid.bf = null;
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            if (this.q != null && this.q.isOpen()) {
                this.q.close();
                this.q = null;
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        if (i2 >= 0) {
            super.onListItemClick(listView, view, i2, j2);
            if (dh.J) {
                Log.d("WAL:onListItemClick()", "position = " + Integer.toString(i2) + ", id = " + Long.toString(j2));
            }
            TcmAid.bf = "main.wristankle._id=" + Integer.toString((int) j2);
            TcmAid.bi = (int) j2;
            TcmAid.bd = i2;
            if (TcmAid.bh != null) {
                TcmAid.bj++;
                TcmAid.bk.add(TcmAid.bh);
                if (dh.Z) {
                    Log.d("WAL:WristAnkleList:", "waCounter++ = " + Integer.toString(TcmAid.bj) + ", waCounterArrary.count = " + Integer.toString(TcmAid.bk.size()));
                }
            }
            this.s = getListView().onSaveInstanceState();
            startActivityForResult(new Intent(this, WristAnkleDetail.class), 1350);
        }
    }

    private SQLiteCursor a() {
        try {
            if (dh.J) {
                Log.d("WAL:createCursor()", "Inserting into t_wristankle");
            }
            if (TcmAid.bh == null) {
                TcmAid.bh = "SELECT main.wristankle._id, main.wristankle.zone FROM main.wristankle" + " UNION " + "SELECT userDB.wristankle._id, userDB.wristankle.zone FROM userDB.wristankle " + " WHERE userDB.wristankle._id>1000 ORDER BY 2 ";
            }
            this.q.execSQL("DELETE FROM t_wristankle");
            this.q.execSQL("INSERT INTO t_wristankle (_id, zone) " + TcmAid.bh);
        } catch (SQLException e2) {
            q.a(e2);
        }
        if (TcmAid.bf != null) {
            this.e = TcmAid.bf;
            TcmAid.bf = null;
            this.f = TcmAid.bg;
            TcmAid.bg = null;
        }
        try {
            this.p = (SQLiteCursor) this.q.query(l, "t_wristankle", new String[]{"_id", "zone"}, this.e, this.f, g, h, "zone", null);
            startManagingCursor(this.p);
            int count = this.p.getCount();
            if (dh.J) {
                Log.d("WAL:createCursor()", "query count = " + Integer.toString(count));
            }
            TcmAid.be.clear();
            if (this.p.moveToFirst()) {
                do {
                    TcmAid.be.add(Integer.valueOf(this.p.getInt(0)));
                } while (this.p.moveToNext());
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        return this.p;
    }

    private ListAdapter b() {
        return new bb(this, this.p, new String[]{"_id", "zone"}, new int[]{C0000R.id.text0, C0000R.id.text1}, "zone");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }

    private void c() {
        if (this.q == null || !this.q.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("WAL:openDataBase()", str + " opened.");
                this.q = SQLiteDatabase.openDatabase(str, null, 16);
                this.q.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.q.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("WAL:openDataBase()", str2 + " ATTACHed.");
                this.r = new n();
                this.r.a(this.q);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new ao(this));
                create.show();
            }
        }
    }
}
