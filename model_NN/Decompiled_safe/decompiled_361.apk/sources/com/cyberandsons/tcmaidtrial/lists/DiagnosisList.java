package com.cyberandsons.tcmaidtrial.lists;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.additems.DiagnosisAdd;
import com.cyberandsons.tcmaidtrial.detailed.DiagnosisDetail;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class DiagnosisList extends ListActivity {
    private static String i;
    private static String j;
    private static String m = "SELECT _id, pathology FROM zangfupathology";
    private static boolean n;

    /* renamed from: a  reason: collision with root package name */
    private boolean f729a = true;

    /* renamed from: b  reason: collision with root package name */
    private boolean f730b;
    private final String c;
    private final String d;
    private final String e;
    private final String f;
    private String g;
    private String[] h;
    private final String k;
    private final String l;
    private boolean o;
    private SQLiteCursor p;
    private SQLiteDatabase q;
    private n r;
    private ImageButton s;
    private Parcelable t;

    public DiagnosisList() {
        this.f730b = !this.f729a;
        this.c = "t_zangfupathology";
        this.d = "main.zangfupathology.";
        this.e = "_id";
        this.f = "pathology";
        this.k = "pathology";
        this.l = "pathology";
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0117 A[SYNTHETIC, Splitter:B:37:0x0117] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0161 A[SYNTHETIC, Splitter:B:44:0x0161] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r7) {
        /*
            r6 = this;
            r3 = 0
            super.onCreate(r7)
            r6.d()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.d     // Catch:{ Exception -> 0x011b }
            if (r0 == 0) goto L_0x000f
            r0 = 1
            r6.setRequestedOrientation(r0)     // Catch:{ Exception -> 0x011b }
        L_0x000f:
            r0 = 2130903079(0x7f030027, float:1.7412966E38)
            r6.setContentView(r0)     // Catch:{ Exception -> 0x011b }
            r0 = 2131362326(0x7f0a0216, float:1.834443E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x011b }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x011b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cP     // Catch:{ Exception -> 0x011b }
            if (r1 == 0) goto L_0x00fe
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cP     // Catch:{ Exception -> 0x011b }
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0027:
            r1.setText(r0)     // Catch:{ Exception -> 0x011b }
            r0 = 2131362327(0x7f0a0217, float:1.8344431E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x011b }
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0     // Catch:{ Exception -> 0x011b }
            r6.s = r0     // Catch:{ Exception -> 0x011b }
            android.widget.ImageButton r0 = r6.s     // Catch:{ Exception -> 0x011b }
            com.cyberandsons.tcmaidtrial.lists.ba r1 = new com.cyberandsons.tcmaidtrial.lists.ba     // Catch:{ Exception -> 0x011b }
            r1.<init>(r6)     // Catch:{ Exception -> 0x011b }
            r0.setOnClickListener(r1)     // Catch:{ Exception -> 0x011b }
            boolean r0 = r6.o     // Catch:{ Exception -> 0x011b }
            if (r0 == 0) goto L_0x00d7
            android.database.sqlite.SQLiteDatabase r0 = r6.q     // Catch:{ SQLException -> 0x0110, all -> 0x015d }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.lists.DiagnosisList.m     // Catch:{ SQLException -> 0x0110, all -> 0x015d }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x0110, all -> 0x015d }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0110, all -> 0x015d }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.z     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            if (r2 == 0) goto L_0x0098
            java.lang.String r2 = "DL:doRawCheck()"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            r3.<init>()     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            java.lang.String r4 = "query count = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            android.util.Log.d(r2, r1)     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            java.lang.String r1 = "DL:doRawCheck()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            r2.<init>()     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            java.lang.String r3 = "column count = '"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            int r3 = r0.getColumnCount()     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            java.lang.String r3 = "' "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            android.util.Log.d(r1, r2)     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
        L_0x0098:
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            if (r1 == 0) goto L_0x00d2
        L_0x009e:
            r1 = 1
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            r2 = 0
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.z     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            if (r3 == 0) goto L_0x00cc
            java.lang.String r3 = "DL:doRawCheck()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            r4.<init>()     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            java.lang.String r4 = " - "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            android.util.Log.d(r3, r1)     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
        L_0x00cc:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x016c, all -> 0x0165 }
            if (r1 != 0) goto L_0x009e
        L_0x00d2:
            if (r0 == 0) goto L_0x00d7
            r0.close()     // Catch:{ Exception -> 0x011b }
        L_0x00d7:
            android.database.sqlite.SQLiteCursor r0 = r6.b()     // Catch:{ Exception -> 0x011b }
            r6.p = r0     // Catch:{ Exception -> 0x011b }
            android.widget.ListAdapter r0 = r6.c()     // Catch:{ Exception -> 0x011b }
            r6.setListAdapter(r0)     // Catch:{ Exception -> 0x011b }
            r0 = 2131361994(0x7f0a00ca, float:1.8343756E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x011b }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x011b }
            java.lang.String r1 = ""
            r0.setText(r1)     // Catch:{ Exception -> 0x011b }
            android.widget.ListView r1 = r6.getListView()     // Catch:{ Exception -> 0x011b }
            r2 = 1
            r1.setFastScrollEnabled(r2)     // Catch:{ Exception -> 0x011b }
            r1.setEmptyView(r0)     // Catch:{ Exception -> 0x011b }
        L_0x00fd:
            return
        L_0x00fe:
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.B     // Catch:{ Exception -> 0x011b }
            if (r1 != 0) goto L_0x0109
            java.lang.String r1 = "Diagnosis Pathologies"
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0027
        L_0x0109:
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.B     // Catch:{ Exception -> 0x011b }
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0027
        L_0x0110:
            r0 = move-exception
            r1 = r3
        L_0x0112:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x016a }
            if (r1 == 0) goto L_0x00d7
            r1.close()     // Catch:{ Exception -> 0x011b }
            goto L_0x00d7
        L_0x011b:
            r0 = move-exception
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()
            java.lang.String r2 = "myVariable"
            java.lang.String r0 = r0.getLocalizedMessage()
            r1.a(r2, r0)
            org.acra.ErrorReporter r0 = org.acra.ErrorReporter.a()
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r2 = "DL:onCreate()"
            r1.<init>(r2)
            r0.handleSilentException(r1)
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r6)
            android.app.AlertDialog r0 = r0.create()
            java.lang.String r1 = "Attention:"
            r0.setTitle(r1)
            r1 = 2131099670(0x7f060016, float:1.78117E38)
            java.lang.String r1 = r6.getString(r1)
            r0.setMessage(r1)
            java.lang.String r1 = "OK"
            com.cyberandsons.tcmaidtrial.lists.az r2 = new com.cyberandsons.tcmaidtrial.lists.az
            r2.<init>(r6)
            r0.setButton(r1, r2)
            r0.show()
            goto L_0x00fd
        L_0x015d:
            r0 = move-exception
            r1 = r3
        L_0x015f:
            if (r1 == 0) goto L_0x0164
            r1.close()     // Catch:{ Exception -> 0x011b }
        L_0x0164:
            throw r0     // Catch:{ Exception -> 0x011b }
        L_0x0165:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x015f
        L_0x016a:
            r0 = move-exception
            goto L_0x015f
        L_0x016c:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0112
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.DiagnosisList.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public final void a() {
        startActivityForResult(new Intent(this, DiagnosisAdd.class), 1350);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        d();
        if (TcmAid.cy) {
            startActivity(getIntent());
            finish();
            TcmAid.cy = false;
        }
        if (TcmAid.bl) {
            TcmAid.bl = this.f730b;
            try {
                if (dh.e) {
                    if (TcmAid.J > 0) {
                        TcmAid.J--;
                        TcmAid.E = (String) TcmAid.K.get(TcmAid.J);
                        TcmAid.K.remove(TcmAid.J);
                        if (dh.S) {
                            Log.d("DL:onResume()", "dgCounter++ = " + Integer.toString(TcmAid.J) + ", dgCounterArrary.count = " + Integer.toString(TcmAid.K.size()));
                        }
                    }
                    if (dh.ab) {
                        Log.d("DL:onResume()", TcmAid.E);
                    }
                }
                stopManagingCursor(this.p);
                if (this.p != null) {
                    this.p.close();
                    this.p = null;
                }
                TcmAid.C = null;
                this.p = b();
                setListAdapter(c());
                getListView().invalidate();
                getListView().onRestoreInstanceState(this.t);
            } catch (SQLException e2) {
                q.a(e2);
            } catch (Exception e3) {
                Log.e("DL:onResume()", e3.getLocalizedMessage());
            }
        }
        super.onResume();
    }

    public void onDestroy() {
        try {
            TcmAid.A = -1;
            TcmAid.C = null;
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            if (this.q != null && this.q.isOpen()) {
                this.q.close();
                this.q = null;
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        if (i2 >= 0) {
            super.onListItemClick(listView, view, i2, j2);
            Log.d("DL:onListItemClick()", "position = " + Integer.toString(i2) + ", id = " + Long.toString(j2));
            TcmAid.C = "main.zangfupathology._id=" + Integer.toString((int) j2);
            TcmAid.I = (int) j2;
            TcmAid.u = i2;
            if (TcmAid.E != null) {
                TcmAid.J++;
                TcmAid.K.add(TcmAid.E);
                if (dh.S) {
                    Log.d("DiagnosisList:", "dgCounter++ = " + Integer.toString(TcmAid.J) + ", dgCounterArrary.count = " + Integer.toString(TcmAid.K.size()));
                }
            }
            this.t = getListView().onSaveInstanceState();
            startActivityForResult(new Intent(this, DiagnosisDetail.class), 1350);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x008b A[Catch:{ SQLException -> 0x01bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a4 A[Catch:{ SQLException -> 0x01bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00cd A[Catch:{ SQLException -> 0x01bb }, LOOP:0: B:34:0x00cd->B:35:0x00e3, LOOP_START] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01b7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.database.sqlite.SQLiteCursor b() {
        /*
            r10 = this;
            r2 = 1
            r9 = 0
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.z     // Catch:{ SQLException -> 0x011b }
            if (r0 == 0) goto L_0x000d
            java.lang.String r0 = "DL:createCursor()"
            java.lang.String r1 = "Inserting into t_zangfupathology"
            android.util.Log.d(r0, r1)     // Catch:{ SQLException -> 0x011b }
        L_0x000d:
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.E     // Catch:{ SQLException -> 0x011b }
            if (r0 != 0) goto L_0x00e8
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.A     // Catch:{ SQLException -> 0x011b }
            r1 = 99
            if (r0 != r1) goto L_0x00e8
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.z     // Catch:{ SQLException -> 0x011b }
            if (r0 == 0) goto L_0x0024
            java.lang.String r0 = "DL:getAllSql"
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.a.t.c()     // Catch:{ SQLException -> 0x011b }
            android.util.Log.d(r0, r1)     // Catch:{ SQLException -> 0x011b }
        L_0x0024:
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.z     // Catch:{ SQLException -> 0x011b }
            if (r0 == 0) goto L_0x0035
            java.lang.String r0 = "DL:getListTempInsert"
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.a.t.c()     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.a.t.a(r1)     // Catch:{ SQLException -> 0x011b }
            android.util.Log.d(r0, r1)     // Catch:{ SQLException -> 0x011b }
        L_0x0035:
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.a.t.c()     // Catch:{ SQLException -> 0x011b }
            com.cyberandsons.tcmaidtrial.TcmAid.E = r0     // Catch:{ SQLException -> 0x011b }
            android.database.sqlite.SQLiteDatabase r0 = r10.q     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = "DELETE FROM t_zangfupathology"
            r0.execSQL(r1)     // Catch:{ SQLException -> 0x011b }
            android.database.sqlite.SQLiteDatabase r0 = r10.q     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.E     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.a.t.a(r1)     // Catch:{ SQLException -> 0x011b }
            r0.execSQL(r1)     // Catch:{ SQLException -> 0x011b }
        L_0x004d:
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.C
            if (r0 == 0) goto L_0x005d
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.C
            r10.g = r0
            com.cyberandsons.tcmaidtrial.TcmAid.C = r9
            java.lang.String[] r0 = com.cyberandsons.tcmaidtrial.TcmAid.D
            r10.h = r0
            com.cyberandsons.tcmaidtrial.TcmAid.D = r9
        L_0x005d:
            r0 = 2
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch:{ SQLException -> 0x01bb }
            r0 = 0
            java.lang.String r1 = "_id"
            r3[r0] = r1     // Catch:{ SQLException -> 0x01bb }
            r0 = 1
            java.lang.String r1 = "pathology"
            r3[r0] = r1     // Catch:{ SQLException -> 0x01bb }
            boolean r0 = r10.f730b     // Catch:{ SQLException -> 0x01bb }
            int r1 = com.cyberandsons.tcmaidtrial.TcmAid.A     // Catch:{ SQLException -> 0x01bb }
            if (r1 != r2) goto L_0x01c1
            com.cyberandsons.tcmaidtrial.a.n r1 = r10.r     // Catch:{ SQLException -> 0x01bb }
            boolean r1 = r1.B()     // Catch:{ SQLException -> 0x01bb }
            if (r1 == 0) goto L_0x01c1
            boolean r0 = r10.f729a     // Catch:{ SQLException -> 0x01bb }
            r8 = r0
        L_0x007b:
            android.database.sqlite.SQLiteDatabase r0 = r10.q     // Catch:{ SQLException -> 0x01bb }
            boolean r1 = com.cyberandsons.tcmaidtrial.lists.DiagnosisList.n     // Catch:{ SQLException -> 0x01bb }
            java.lang.String r2 = "t_zangfupathology"
            java.lang.String r4 = r10.g     // Catch:{ SQLException -> 0x01bb }
            java.lang.String[] r5 = r10.h     // Catch:{ SQLException -> 0x01bb }
            java.lang.String r6 = com.cyberandsons.tcmaidtrial.lists.DiagnosisList.i     // Catch:{ SQLException -> 0x01bb }
            java.lang.String r7 = com.cyberandsons.tcmaidtrial.lists.DiagnosisList.j     // Catch:{ SQLException -> 0x01bb }
            if (r8 == 0) goto L_0x01b7
            r8 = r9
        L_0x008c:
            r9 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ SQLException -> 0x01bb }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x01bb }
            r10.p = r0     // Catch:{ SQLException -> 0x01bb }
            android.database.sqlite.SQLiteCursor r0 = r10.p     // Catch:{ SQLException -> 0x01bb }
            r10.startManagingCursor(r0)     // Catch:{ SQLException -> 0x01bb }
            android.database.sqlite.SQLiteCursor r0 = r10.p     // Catch:{ SQLException -> 0x01bb }
            int r0 = r0.getCount()     // Catch:{ SQLException -> 0x01bb }
            boolean r1 = com.cyberandsons.tcmaidtrial.misc.dh.z     // Catch:{ SQLException -> 0x01bb }
            if (r1 == 0) goto L_0x00c0
            java.lang.String r1 = "DL:createCursor()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x01bb }
            r2.<init>()     // Catch:{ SQLException -> 0x01bb }
            java.lang.String r3 = "query count = "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x01bb }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ SQLException -> 0x01bb }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ SQLException -> 0x01bb }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x01bb }
            android.util.Log.d(r1, r0)     // Catch:{ SQLException -> 0x01bb }
        L_0x00c0:
            java.util.ArrayList r0 = com.cyberandsons.tcmaidtrial.TcmAid.v     // Catch:{ SQLException -> 0x01bb }
            r0.clear()     // Catch:{ SQLException -> 0x01bb }
            android.database.sqlite.SQLiteCursor r0 = r10.p     // Catch:{ SQLException -> 0x01bb }
            boolean r0 = r0.moveToFirst()     // Catch:{ SQLException -> 0x01bb }
            if (r0 == 0) goto L_0x00e5
        L_0x00cd:
            android.database.sqlite.SQLiteCursor r0 = r10.p     // Catch:{ SQLException -> 0x01bb }
            r1 = 0
            int r0 = r0.getInt(r1)     // Catch:{ SQLException -> 0x01bb }
            java.util.ArrayList r1 = com.cyberandsons.tcmaidtrial.TcmAid.v     // Catch:{ SQLException -> 0x01bb }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ SQLException -> 0x01bb }
            r1.add(r0)     // Catch:{ SQLException -> 0x01bb }
            android.database.sqlite.SQLiteCursor r0 = r10.p     // Catch:{ SQLException -> 0x01bb }
            boolean r0 = r0.moveToNext()     // Catch:{ SQLException -> 0x01bb }
            if (r0 != 0) goto L_0x00cd
        L_0x00e5:
            android.database.sqlite.SQLiteCursor r0 = r10.p
            return r0
        L_0x00e8:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.A     // Catch:{ SQLException -> 0x011b }
            switch(r0) {
                case 0: goto L_0x0121;
                case 1: goto L_0x0153;
                case 2: goto L_0x00ed;
                case 3: goto L_0x0185;
                default: goto L_0x00ed;
            }     // Catch:{ SQLException -> 0x011b }
        L_0x00ed:
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.z     // Catch:{ SQLException -> 0x011b }
            if (r0 == 0) goto L_0x00f8
            java.lang.String r0 = "DL:getAllSql"
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.E     // Catch:{ SQLException -> 0x011b }
            android.util.Log.d(r0, r1)     // Catch:{ SQLException -> 0x011b }
        L_0x00f8:
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.z     // Catch:{ SQLException -> 0x011b }
            if (r0 == 0) goto L_0x0107
            java.lang.String r0 = "DL:getListTempInsert"
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.E     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.a.t.a(r1)     // Catch:{ SQLException -> 0x011b }
            android.util.Log.d(r0, r1)     // Catch:{ SQLException -> 0x011b }
        L_0x0107:
            android.database.sqlite.SQLiteDatabase r0 = r10.q     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = "DELETE FROM t_zangfupathology"
            r0.execSQL(r1)     // Catch:{ SQLException -> 0x011b }
            android.database.sqlite.SQLiteDatabase r0 = r10.q     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.E     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.a.t.a(r1)     // Catch:{ SQLException -> 0x011b }
            r0.execSQL(r1)     // Catch:{ SQLException -> 0x011b }
            goto L_0x004d
        L_0x011b:
            r0 = move-exception
            com.cyberandsons.tcmaidtrial.a.q.a(r0)
            goto L_0x004d
        L_0x0121:
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.z     // Catch:{ SQLException -> 0x011b }
            if (r0 == 0) goto L_0x012c
            java.lang.String r0 = "DL:getAllSql"
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.F     // Catch:{ SQLException -> 0x011b }
            android.util.Log.d(r0, r1)     // Catch:{ SQLException -> 0x011b }
        L_0x012c:
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.z     // Catch:{ SQLException -> 0x011b }
            if (r0 == 0) goto L_0x013b
            java.lang.String r0 = "DL:getListTempInsert"
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.F     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.a.t.a(r1)     // Catch:{ SQLException -> 0x011b }
            android.util.Log.d(r0, r1)     // Catch:{ SQLException -> 0x011b }
        L_0x013b:
            android.database.sqlite.SQLiteDatabase r0 = r10.q     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = "DELETE FROM t_zangfupathology"
            r0.execSQL(r1)     // Catch:{ SQLException -> 0x011b }
            android.database.sqlite.SQLiteDatabase r0 = r10.q     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.F     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.a.t.a(r1)     // Catch:{ SQLException -> 0x011b }
            r0.execSQL(r1)     // Catch:{ SQLException -> 0x011b }
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.F     // Catch:{ SQLException -> 0x011b }
            com.cyberandsons.tcmaidtrial.TcmAid.E = r0     // Catch:{ SQLException -> 0x011b }
            goto L_0x004d
        L_0x0153:
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.z     // Catch:{ SQLException -> 0x011b }
            if (r0 == 0) goto L_0x015e
            java.lang.String r0 = "DL:getAllSql"
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.G     // Catch:{ SQLException -> 0x011b }
            android.util.Log.d(r0, r1)     // Catch:{ SQLException -> 0x011b }
        L_0x015e:
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.z     // Catch:{ SQLException -> 0x011b }
            if (r0 == 0) goto L_0x016d
            java.lang.String r0 = "DL:getListTempInsert"
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.G     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.a.t.a(r1)     // Catch:{ SQLException -> 0x011b }
            android.util.Log.d(r0, r1)     // Catch:{ SQLException -> 0x011b }
        L_0x016d:
            android.database.sqlite.SQLiteDatabase r0 = r10.q     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = "DELETE FROM t_zangfupathology"
            r0.execSQL(r1)     // Catch:{ SQLException -> 0x011b }
            android.database.sqlite.SQLiteDatabase r0 = r10.q     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.G     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.a.t.a(r1)     // Catch:{ SQLException -> 0x011b }
            r0.execSQL(r1)     // Catch:{ SQLException -> 0x011b }
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.G     // Catch:{ SQLException -> 0x011b }
            com.cyberandsons.tcmaidtrial.TcmAid.E = r0     // Catch:{ SQLException -> 0x011b }
            goto L_0x004d
        L_0x0185:
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.z     // Catch:{ SQLException -> 0x011b }
            if (r0 == 0) goto L_0x0190
            java.lang.String r0 = "DL:getAllSql"
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.H     // Catch:{ SQLException -> 0x011b }
            android.util.Log.d(r0, r1)     // Catch:{ SQLException -> 0x011b }
        L_0x0190:
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.z     // Catch:{ SQLException -> 0x011b }
            if (r0 == 0) goto L_0x019f
            java.lang.String r0 = "DL:getListTempInsert"
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.H     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.a.t.a(r1)     // Catch:{ SQLException -> 0x011b }
            android.util.Log.d(r0, r1)     // Catch:{ SQLException -> 0x011b }
        L_0x019f:
            android.database.sqlite.SQLiteDatabase r0 = r10.q     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = "DELETE FROM t_zangfupathology"
            r0.execSQL(r1)     // Catch:{ SQLException -> 0x011b }
            android.database.sqlite.SQLiteDatabase r0 = r10.q     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.H     // Catch:{ SQLException -> 0x011b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.a.t.a(r1)     // Catch:{ SQLException -> 0x011b }
            r0.execSQL(r1)     // Catch:{ SQLException -> 0x011b }
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.TcmAid.H     // Catch:{ SQLException -> 0x011b }
            com.cyberandsons.tcmaidtrial.TcmAid.E = r0     // Catch:{ SQLException -> 0x011b }
            goto L_0x004d
        L_0x01b7:
            java.lang.String r8 = "pathology"
            goto L_0x008c
        L_0x01bb:
            r0 = move-exception
            com.cyberandsons.tcmaidtrial.a.q.a(r0)
            goto L_0x00e5
        L_0x01c1:
            r8 = r0
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.DiagnosisList.b():android.database.sqlite.SQLiteCursor");
    }

    private ListAdapter c() {
        return new bt(this, this.p, new String[]{"_id", "pathology"}, new int[]{C0000R.id.text0, C0000R.id.text1}, "pathology");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }

    private void d() {
        if (this.q == null || !this.q.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("DL:openDataBase()", str + " opened.");
                this.q = SQLiteDatabase.openDatabase(str, null, 16);
                this.q.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.q.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("DL:openDataBase()", str2 + " ATTACHed.");
                this.r = new n();
                this.r.a(this.q);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new ay(this));
                create.show();
            }
        }
    }
}
