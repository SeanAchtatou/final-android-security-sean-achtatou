package com.cyberandsons.tcmaidtrial.lists;

import android.content.DialogInterface;

final class ae implements DialogInterface.OnMultiChoiceClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ HerbCategoryList f756a;

    ae(HerbCategoryList herbCategoryList) {
        this.f756a = herbCategoryList;
    }

    public final void onClick(DialogInterface dialogInterface, int i, boolean z) {
        if (z) {
            this.f756a.f735a.add(this.f756a.f736b[i]);
        } else {
            this.f756a.f735a.remove(this.f756a.f736b[i]);
        }
        this.f756a.a();
    }
}
