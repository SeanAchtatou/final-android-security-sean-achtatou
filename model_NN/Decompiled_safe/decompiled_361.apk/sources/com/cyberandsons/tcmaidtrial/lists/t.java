package com.cyberandsons.tcmaidtrial.lists;

import android.content.DialogInterface;
import android.database.sqlite.SQLiteCursor;
import com.cyberandsons.tcmaidtrial.TcmAid;

final class t implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TungList f832a;

    t(TungList tungList) {
        this.f832a = tungList;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        int i2;
        if (i == 0) {
            boolean unused = this.f832a.x = this.f832a.f748b;
            int unused2 = this.f832a.z = 0;
        } else if (i == 1) {
            boolean unused3 = this.f832a.x = this.f832a.f748b;
            int unused4 = this.f832a.z = 1;
        } else if (i == 2) {
            boolean unused5 = this.f832a.x = this.f832a.f747a;
        }
        if (this.f832a.x) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        TcmAid.cX = i2;
        this.f832a.c = this.f832a.getListView().onSaveInstanceState();
        this.f832a.stopManagingCursor(this.f832a.s);
        if (this.f832a.s != null) {
            this.f832a.s.close();
            SQLiteCursor unused6 = this.f832a.s = (SQLiteCursor) null;
        }
        TcmAid.aW = null;
        SQLiteCursor unused7 = this.f832a.s = this.f832a.c();
        this.f832a.setListAdapter(this.f832a.d());
        this.f832a.getListView().invalidate();
        this.f832a.getListView().onRestoreInstanceState(this.f832a.c);
    }
}
