package com.cyberandsons.tcmaidtrial.lists;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.p;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.additems.AuricularAdd;
import com.cyberandsons.tcmaidtrial.detailed.AuricularDetail;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class AuricularList extends ListActivity {
    private static String f;
    private static String g;
    private static String j = "SELECT _id, point FROM auricular";
    private static boolean k;

    /* renamed from: a  reason: collision with root package name */
    private final String f727a = "t_auricular";

    /* renamed from: b  reason: collision with root package name */
    private final String f728b = "_id";
    private final String c = "point";
    private String d;
    private String[] e;
    private final String h = "point";
    private final String i = "point";
    private boolean l;
    private SQLiteCursor m;
    private SQLiteDatabase n;
    private n o;
    private boolean p = true;
    private boolean q;
    private ImageButton r;
    private Parcelable s;

    public AuricularList() {
        boolean z;
        if (!this.p) {
            z = true;
        } else {
            z = false;
        }
        this.q = z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x0157 A[SYNTHETIC, Splitter:B:38:0x0157] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x015f A[Catch:{ Exception -> 0x010e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r7) {
        /*
            r6 = this;
            r3 = 0
            super.onCreate(r7)
            r6.d()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.d     // Catch:{ Exception -> 0x010e }
            if (r0 == 0) goto L_0x000f
            r0 = 1
            r6.setRequestedOrientation(r0)     // Catch:{ Exception -> 0x010e }
        L_0x000f:
            r0 = 2130903079(0x7f030027, float:1.7412966E38)
            r6.setContentView(r0)     // Catch:{ Exception -> 0x010e }
            r0 = 2131362326(0x7f0a0216, float:1.834443E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x010e }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x010e }
            r1 = 2131362327(0x7f0a0217, float:1.8344431E38)
            android.view.View r1 = r6.findViewById(r1)     // Catch:{ Exception -> 0x010e }
            android.widget.ImageButton r1 = (android.widget.ImageButton) r1     // Catch:{ Exception -> 0x010e }
            r6.r = r1     // Catch:{ Exception -> 0x010e }
            android.widget.ImageButton r1 = r6.r     // Catch:{ Exception -> 0x010e }
            com.cyberandsons.tcmaidtrial.lists.b r2 = new com.cyberandsons.tcmaidtrial.lists.b     // Catch:{ Exception -> 0x010e }
            r2.<init>(r6)     // Catch:{ Exception -> 0x010e }
            r1.setOnClickListener(r2)     // Catch:{ Exception -> 0x010e }
            java.lang.String r1 = "Auricular Points"
            boolean r2 = com.cyberandsons.tcmaidtrial.TcmAid.l     // Catch:{ Exception -> 0x010e }
            if (r2 == 0) goto L_0x003e
            java.lang.String[] r1 = com.cyberandsons.tcmaidtrial.TcmAid.n     // Catch:{ Exception -> 0x010e }
            r2 = 0
            r1 = r1[r2]     // Catch:{ Exception -> 0x010e }
        L_0x003e:
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.TcmAid.cP     // Catch:{ Exception -> 0x010e }
            if (r2 == 0) goto L_0x0109
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cP     // Catch:{ Exception -> 0x010e }
            r0.setText(r1)     // Catch:{ Exception -> 0x010e }
            r0 = 0
            com.cyberandsons.tcmaidtrial.TcmAid.cP = r0     // Catch:{ Exception -> 0x010e }
        L_0x004a:
            boolean r0 = r6.l     // Catch:{ Exception -> 0x010e }
            if (r0 == 0) goto L_0x00e2
            android.database.sqlite.SQLiteDatabase r0 = r6.n     // Catch:{ SQLException -> 0x0150, all -> 0x015b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.lists.AuricularList.j     // Catch:{ SQLException -> 0x0150, all -> 0x015b }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x0150, all -> 0x015b }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0150, all -> 0x015b }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.r     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            if (r2 == 0) goto L_0x00a3
            java.lang.String r2 = "AL:doRawCheck()"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            r3.<init>()     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            java.lang.String r4 = "query count = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            android.util.Log.d(r2, r1)     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            java.lang.String r1 = "AL:doRawCheck()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            r2.<init>()     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            java.lang.String r3 = "column count = '"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            int r3 = r0.getColumnCount()     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            java.lang.String r3 = "' "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            android.util.Log.d(r1, r2)     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
        L_0x00a3:
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            if (r1 == 0) goto L_0x00dd
        L_0x00a9:
            r1 = 1
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            r2 = 0
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.r     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            if (r3 == 0) goto L_0x00d7
            java.lang.String r3 = "AL:doRawCheck()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            r4.<init>()     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            java.lang.String r4 = " - "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            android.util.Log.d(r3, r1)     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
        L_0x00d7:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x016a, all -> 0x0163 }
            if (r1 != 0) goto L_0x00a9
        L_0x00dd:
            if (r0 == 0) goto L_0x00e2
            r0.close()     // Catch:{ Exception -> 0x010e }
        L_0x00e2:
            android.database.sqlite.SQLiteCursor r0 = r6.b()     // Catch:{ Exception -> 0x010e }
            r6.m = r0     // Catch:{ Exception -> 0x010e }
            android.widget.ListAdapter r0 = r6.c()     // Catch:{ Exception -> 0x010e }
            r6.setListAdapter(r0)     // Catch:{ Exception -> 0x010e }
            r0 = 2131361994(0x7f0a00ca, float:1.8343756E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x010e }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x010e }
            java.lang.String r1 = ""
            r0.setText(r1)     // Catch:{ Exception -> 0x010e }
            android.widget.ListView r1 = r6.getListView()     // Catch:{ Exception -> 0x010e }
            r2 = 1
            r1.setFastScrollEnabled(r2)     // Catch:{ Exception -> 0x010e }
            r1.setEmptyView(r0)     // Catch:{ Exception -> 0x010e }
        L_0x0108:
            return
        L_0x0109:
            r0.setText(r1)     // Catch:{ Exception -> 0x010e }
            goto L_0x004a
        L_0x010e:
            r0 = move-exception
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()
            java.lang.String r2 = "myVariable"
            java.lang.String r0 = r0.getLocalizedMessage()
            r1.a(r2, r0)
            org.acra.ErrorReporter r0 = org.acra.ErrorReporter.a()
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r2 = "AL:onCreate()"
            r1.<init>(r2)
            r0.handleSilentException(r1)
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r6)
            android.app.AlertDialog r0 = r0.create()
            java.lang.String r1 = "Attention:"
            r0.setTitle(r1)
            r1 = 2131099670(0x7f060016, float:1.78117E38)
            java.lang.String r1 = r6.getString(r1)
            r0.setMessage(r1)
            java.lang.String r1 = "OK"
            com.cyberandsons.tcmaidtrial.lists.c r2 = new com.cyberandsons.tcmaidtrial.lists.c
            r2.<init>(r6)
            r0.setButton(r1, r2)
            r0.show()
            goto L_0x0108
        L_0x0150:
            r0 = move-exception
            r1 = r3
        L_0x0152:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0168 }
            if (r1 == 0) goto L_0x00e2
            r1.close()     // Catch:{ Exception -> 0x010e }
            goto L_0x00e2
        L_0x015b:
            r0 = move-exception
            r1 = r3
        L_0x015d:
            if (r1 == 0) goto L_0x0162
            r1.close()     // Catch:{ Exception -> 0x010e }
        L_0x0162:
            throw r0     // Catch:{ Exception -> 0x010e }
        L_0x0163:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x015d
        L_0x0168:
            r0 = move-exception
            goto L_0x015d
        L_0x016a:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0152
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.AuricularList.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public final void a() {
        startActivityForResult(new Intent(this, AuricularAdd.class), 1350);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        d();
        if (TcmAid.cw) {
            startActivity(getIntent());
            finish();
            TcmAid.cw = false;
        }
        if (TcmAid.bl) {
            TcmAid.bl = this.q;
            try {
                if (dh.e) {
                    if (TcmAid.s > 0) {
                        TcmAid.s--;
                        TcmAid.o = (String) TcmAid.t.get(TcmAid.s);
                        TcmAid.t.remove(TcmAid.s);
                        if (dh.R) {
                            Log.d("AL:onResume()", "auCounter++ = " + Integer.toString(TcmAid.s) + ", auCounterArrary.count = " + Integer.toString(TcmAid.t.size()));
                        }
                    }
                    if (dh.aa) {
                        Log.d("AL:onResume()", TcmAid.o);
                    }
                }
                stopManagingCursor(this.m);
                if (this.m != null) {
                    this.m.close();
                    this.m = null;
                }
                TcmAid.m = null;
                this.m = b();
                setListAdapter(c());
                getListView().invalidate();
                getListView().onRestoreInstanceState(this.s);
            } catch (SQLException e2) {
                q.a(e2);
            } catch (Exception e3) {
                Log.e("AL:onResume()", e3.getLocalizedMessage());
            }
        }
        super.onResume();
    }

    public void onDestroy() {
        try {
            TcmAid.m = null;
            TcmAid.l = this.q;
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            if (this.n != null && this.n.isOpen()) {
                this.n.close();
                this.n = null;
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        if (i2 >= 0) {
            super.onListItemClick(listView, view, i2, j2);
            if (dh.r) {
                Log.d("AL:onListItemClick()", "position = " + Integer.toString(i2) + ", id = " + Long.toString(j2));
            }
            TcmAid.m = "_id=" + Integer.toString((int) j2);
            TcmAid.r = (int) j2;
            TcmAid.h = i2;
            TcmAid.l = this.q;
            if (TcmAid.o != null) {
                TcmAid.s++;
                TcmAid.t.add(TcmAid.o);
                if (dh.R) {
                    Log.d("AuricularList:", "auCounter++ = " + Integer.toString(TcmAid.s) + ", auCounterArrary.count = " + Integer.toString(TcmAid.t.size()));
                }
            }
            this.s = getListView().onSaveInstanceState();
            startActivityForResult(new Intent(this, AuricularDetail.class), 1350);
        }
    }

    private SQLiteCursor b() {
        String str;
        boolean z;
        try {
            if (dh.r) {
                Log.d("AL:createCursor()", "Inserting into t_auricular");
            }
            if (TcmAid.o == null) {
                n nVar = this.o;
                boolean z2 = nVar.z();
                String n2 = nVar.n();
                if (!z2 || n2.length() <= 0) {
                    str = null;
                    z = false;
                } else {
                    str = q.a("auricular", "_id", n2);
                    z = str.length() > 0;
                }
                TcmAid.o = z ? "SELECT main.auricular._id, lower(main.auricular.point), main.auricular.pointcat, main.auricular.nada, main.auricular.battlefield, main.auricular.acacd, main.auricular.stopsmoking, main.auricular.weightloss FROM main.auricular " + " WHERE (main" + str + ") " + " UNION " + "SELECT userDB.auricular._id, lower(userDB.auricular.point), userDB.auricular.pointcat, 0, 0, 0, 0, 0 FROM userDB.auricular " + " WHERE (userDB" + str + ") " + " AND userDB.auricular._id>1000 ORDER BY 2" : "SELECT main.auricular._id, lower(main.auricular.point), main.auricular.pointcat, main.auricular.nada, main.auricular.battlefield, main.auricular.acacd, main.auricular.stopsmoking, main.auricular.weightloss FROM main.auricular " + " UNION " + "SELECT userDB.auricular._id, lower(userDB.auricular.point), userDB.auricular.pointcat, 0, 0, 0, 0, 0 FROM userDB.auricular " + " WHERE userDB.auricular._id>1000 ORDER BY 2";
            }
            this.n.execSQL(p.a("t_auricular"));
            this.n.execSQL(p.a("t_auricular", TcmAid.o));
        } catch (SQLException e2) {
            q.a(e2);
        }
        if (TcmAid.m != null) {
            this.d = TcmAid.m;
            TcmAid.m = null;
            this.e = TcmAid.n;
            TcmAid.n = null;
        }
        try {
            this.m = (SQLiteCursor) this.n.query(k, "t_auricular", new String[]{"_id", "point"}, this.d, this.e, f, g, "point", null);
            startManagingCursor(this.m);
            int count = this.m.getCount();
            if (dh.r) {
                Log.d("AL:createCursor()", "query count = " + Integer.toString(count));
            }
            TcmAid.i.clear();
            if (this.m.moveToFirst()) {
                do {
                    TcmAid.i.add(Integer.valueOf(this.m.getInt(0)));
                } while (this.m.moveToNext());
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        return this.m;
    }

    private ListAdapter c() {
        return new bh(this, this.m, new String[]{"_id", "point"}, new int[]{C0000R.id.text0, C0000R.id.text1}, "point");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }

    private void d() {
        if (this.n == null || !this.n.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("AL:openDataBase()", str + " opened.");
                this.n = SQLiteDatabase.openDatabase(str, null, 16);
                this.n.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.n.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("AL:openDataBase()", str2 + " ATTACHed.");
                this.o = new n();
                this.o.a(this.n);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new a(this));
                create.show();
            }
        }
    }
}
