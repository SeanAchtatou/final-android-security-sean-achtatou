package com.cyberandsons.tcmaidtrial.lists;

import android.content.Context;
import android.database.Cursor;
import android.widget.AlphabetIndexer;
import android.widget.SectionIndexer;
import android.widget.SimpleCursorAdapter;
import com.cyberandsons.tcmaidtrial.C0000R;

final class r extends SimpleCursorAdapter implements SectionIndexer {

    /* renamed from: a  reason: collision with root package name */
    private AlphabetIndexer f830a;

    public r(Context context, Cursor cursor, String[] strArr, int[] iArr, String str) {
        super(context, C0000R.layout.list_row, cursor, strArr, iArr);
        this.f830a = new AlphabetIndexer(cursor, cursor.getColumnIndexOrThrow(str), "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    }

    public final int getPositionForSection(int i) {
        return this.f830a.getPositionForSection(i);
    }

    public final int getSectionForPosition(int i) {
        return this.f830a.getSectionForPosition(i);
    }

    public final Object[] getSections() {
        return this.f830a.getSections();
    }
}
