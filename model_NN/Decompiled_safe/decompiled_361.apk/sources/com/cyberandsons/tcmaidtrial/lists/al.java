package com.cyberandsons.tcmaidtrial.lists;

import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.misc.ad;

final class al implements SimpleCursorAdapter.ViewBinder {

    /* renamed from: a  reason: collision with root package name */
    private boolean f768a = false;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ bu f769b;

    al(bu buVar) {
        this.f769b = buVar;
    }

    public final boolean setViewValue(View view, Cursor cursor, int i) {
        TextView textView = (TextView) view;
        if (i == 1) {
            textView.setText(ad.a(cursor.getString(1)));
            this.f768a = true;
        }
        return this.f768a;
    }
}
