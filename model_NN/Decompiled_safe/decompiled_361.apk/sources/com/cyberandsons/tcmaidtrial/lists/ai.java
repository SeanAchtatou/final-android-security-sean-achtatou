package com.cyberandsons.tcmaidtrial.lists;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.SimpleCursorAdapter;
import com.cyberandsons.tcmaidtrial.C0000R;

final class ai extends SimpleCursorAdapter {

    /* renamed from: a  reason: collision with root package name */
    SQLiteDatabase f762a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f763b = true;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public int d;

    public ai(Context context, Cursor cursor, String[] strArr, int[] iArr, boolean z, boolean z2, int i, SQLiteDatabase sQLiteDatabase) {
        super(context, C0000R.layout.list_row, cursor, strArr, iArr);
        this.f763b = z;
        this.c = z2;
        this.d = i;
        this.f762a = sQLiteDatabase;
        setViewBinder(new x(this));
    }
}
