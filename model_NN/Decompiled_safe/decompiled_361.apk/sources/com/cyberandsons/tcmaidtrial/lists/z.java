package com.cyberandsons.tcmaidtrial.lists;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.SimpleCursorAdapter;
import com.cyberandsons.tcmaidtrial.C0000R;

final class z extends SimpleCursorAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f840a = false;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f841b = false;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public SQLiteDatabase d;

    public z(Context context, Cursor cursor, String[] strArr, int[] iArr, boolean z, boolean z2, int i, SQLiteDatabase sQLiteDatabase) {
        super(context, C0000R.layout.list_tung_row, cursor, strArr, iArr);
        this.f840a = z;
        this.f841b = z2;
        this.c = i;
        this.d = sQLiteDatabase;
        setViewBinder(new y(this));
    }
}
