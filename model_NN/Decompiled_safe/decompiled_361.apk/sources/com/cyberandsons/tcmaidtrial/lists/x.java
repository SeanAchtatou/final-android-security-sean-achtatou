package com.cyberandsons.tcmaidtrial.lists;

import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.a.s;
import com.cyberandsons.tcmaidtrial.misc.ad;

final class x implements SimpleCursorAdapter.ViewBinder {

    /* renamed from: a  reason: collision with root package name */
    private boolean f836a = false;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ ai f837b;

    x(ai aiVar) {
        this.f837b = aiVar;
    }

    public final boolean setViewValue(View view, Cursor cursor, int i) {
        TextView textView = (TextView) view;
        textView.setTextSize(18.0f);
        if (i == 1) {
            int i2 = cursor.getInt(0);
            String a2 = s.a("ftonemarks", "fid", i2, this.f837b.f762a);
            if (a2 == null || a2.length() == 0 || !this.f837b.f763b) {
                a2 = cursor.getString(1);
            }
            if (this.f837b.c) {
                String a3 = s.a(this.f837b.d == 0 ? "fscc" : "ftcc", "fid", i2, this.f837b.f762a);
                if (a3 != null) {
                    textView.setText(String.format("%s\n%s", ad.a(a2), a3));
                } else {
                    textView.setText(ad.a(a2));
                }
            } else {
                textView.setText(ad.a(a2));
            }
            this.f836a = true;
        }
        return this.f836a;
    }
}
