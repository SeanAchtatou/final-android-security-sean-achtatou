package com.cyberandsons.tcmaidtrial.lists;

import android.content.ContentValues;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.a;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.areas.k;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class bs implements k {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointsList f805a;

    /* synthetic */ bs(PointsList pointsList) {
        this(pointsList, (byte) 0);
    }

    private bs(PointsList pointsList, byte b2) {
        this.f805a = pointsList;
    }

    public final void a(String str, String str2, String str3) {
        boolean z;
        int i;
        if (dh.n) {
            Log.d("PointsList", "Category is " + str);
            Log.d("PointsList", "Description is " + str2);
            Log.d("PointsList", "Points are '" + str3 + "' ");
        }
        ContentValues contentValues = new ContentValues();
        boolean z2 = this.f805a.f740b;
        int a2 = q.a("SELECT _id, name, desc, points  FROM userDB.pointcategory WHERE ", "userDB.pointcategory.", "name", str, this.f805a.u);
        if (a2 == dh.f946a.intValue()) {
            int a3 = q.a("SELECT MAX(userDB.pointcategory._id) FROM userDB.pointcategory ", this.f805a.u) + 1;
            contentValues.put("_id", Integer.toString(a3));
            int i2 = a3;
            z = z2;
            i = i2;
        } else {
            contentValues.put("_id", Integer.toString(a2));
            int i3 = a2;
            z = this.f805a.f739a;
            i = i3;
        }
        contentValues.put("name", str);
        contentValues.put("desc", str2);
        contentValues.put("points", str3.toUpperCase());
        if (!z) {
            try {
                this.f805a.u.insert("userDB.pointcategory", null, contentValues);
            } catch (SQLException e) {
                q.a(e);
            }
        } else {
            this.f805a.u.update("userDB.pointcategory", contentValues, "_id=" + Integer.toString(i), null);
        }
        TcmAid.ao = a.a(str3, this.f805a.v);
        this.f805a.startActivity(this.f805a.getIntent());
        this.f805a.finish();
    }

    public final void a(String str) {
        SQLiteDatabase d = this.f805a.u;
        String str2 = "DELETE FROM userDB.pointcategory ";
        if (str != null && str.length() > 0) {
            str2 = str2 + " WHERE name='" + str + "' ";
        }
        try {
            d.execSQL(str2);
        } catch (SQLException e) {
            q.a(e);
        }
        TcmAid.cE = this.f805a.f739a;
        this.f805a.finish();
    }
}
