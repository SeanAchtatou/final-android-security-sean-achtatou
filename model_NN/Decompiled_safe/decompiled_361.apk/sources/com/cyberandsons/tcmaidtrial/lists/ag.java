package com.cyberandsons.tcmaidtrial.lists;

import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.a.s;
import com.cyberandsons.tcmaidtrial.misc.ad;

final class ag implements SimpleCursorAdapter.ViewBinder {

    /* renamed from: a  reason: collision with root package name */
    private boolean f758a = false;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ ah f759b;

    ag(ah ahVar) {
        this.f759b = ahVar;
    }

    public final boolean setViewValue(View view, Cursor cursor, int i) {
        TextView textView = (TextView) view;
        textView.setTextSize(18.0f);
        if (i == 1 && !this.f759b.f760a) {
            textView.setText(cursor.getString(1));
            this.f758a = true;
        }
        if (i == 2 && !this.f759b.f760a) {
            textView.setText(Integer.toString(cursor.getInt(2)));
            this.f758a = true;
        }
        if (i == 3) {
            if (this.f759b.f761b) {
                textView.setText(ad.a(cursor.getString(4)));
            } else {
                int i2 = cursor.getInt(0);
                String a2 = s.a("ptonemarks", "pid", i2, this.f759b.e);
                if (a2 == null || a2.length() == 0) {
                    a2 = cursor.getString(3);
                }
                if (this.f759b.c) {
                    String a3 = s.a(this.f759b.d == 0 ? "pscc" : "ptcc", "pid", i2, this.f759b.e);
                    if (a3 != null) {
                        textView.setText(String.format("%s - %s", ad.a(a2), a3));
                    } else {
                        textView.setText(ad.a(a2));
                    }
                } else {
                    textView.setText(ad.a(a2));
                }
            }
            this.f758a = true;
        }
        return this.f758a;
    }
}
