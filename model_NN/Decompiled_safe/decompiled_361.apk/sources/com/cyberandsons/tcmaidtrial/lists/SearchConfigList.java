package com.cyberandsons.tcmaidtrial.lists;

import android.app.ListActivity;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.SparseBooleanArray;
import android.widget.ListView;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;

public class SearchConfigList extends ListActivity {

    /* renamed from: a  reason: collision with root package name */
    private ListView f743a;

    /* renamed from: b  reason: collision with root package name */
    private String[] f744b;
    private String c;
    private String d;
    private SQLiteDatabase e;
    private n f;

    /* JADX WARNING: Removed duplicated region for block: B:15:0x015e A[Catch:{ Exception -> 0x01bd }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r8) {
        /*
            r7 = this;
            r6 = 0
            super.onCreate(r8)
            android.database.sqlite.SQLiteDatabase r0 = r7.e
            if (r0 == 0) goto L_0x0010
            android.database.sqlite.SQLiteDatabase r0 = r7.e
            boolean r0 = r0.isOpen()
            if (r0 != 0) goto L_0x00d6
        L_0x0010:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0173 }
            r0.<init>()     // Catch:{ SQLException -> 0x0173 }
            r1 = 2131099683(0x7f060023, float:1.7811726E38)
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLException -> 0x0173 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x0173 }
            r1 = 2131099657(0x7f060009, float:1.7811673E38)
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLException -> 0x0173 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x0173 }
            r1 = 2131099684(0x7f060024, float:1.7811728E38)
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLException -> 0x0173 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x0173 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0173 }
            r1 = 2131099686(0x7f060026, float:1.7811732E38)
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLException -> 0x0173 }
            r2 = 2131099685(0x7f060025, float:1.781173E38)
            java.lang.String r2 = r7.getString(r2)     // Catch:{ SQLException -> 0x0173 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0173 }
            r3.<init>()     // Catch:{ SQLException -> 0x0173 }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ SQLException -> 0x0173 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ SQLException -> 0x0173 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0173 }
            java.lang.String r3 = "SCL:openDataBase()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0173 }
            r4.<init>()     // Catch:{ SQLException -> 0x0173 }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ SQLException -> 0x0173 }
            java.lang.String r5 = " opened."
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x0173 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x0173 }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x0173 }
            r3 = 0
            r4 = 16
            android.database.sqlite.SQLiteDatabase r0 = android.database.sqlite.SQLiteDatabase.openDatabase(r0, r3, r4)     // Catch:{ SQLException -> 0x0173 }
            r7.e = r0     // Catch:{ SQLException -> 0x0173 }
            java.lang.String r0 = "PRAGMA cache_size = 50"
            android.database.sqlite.SQLiteDatabase r3 = r7.e     // Catch:{ SQLException -> 0x0173 }
            r3.execSQL(r0)     // Catch:{ SQLException -> 0x0173 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0173 }
            r0.<init>()     // Catch:{ SQLException -> 0x0173 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x0173 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ SQLException -> 0x0173 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0173 }
            android.database.sqlite.SQLiteDatabase r1 = r7.e     // Catch:{ SQLException -> 0x0173 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0173 }
            r2.<init>()     // Catch:{ SQLException -> 0x0173 }
            java.lang.String r3 = "ATTACH \""
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0173 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ SQLException -> 0x0173 }
            java.lang.String r3 = "\" AS userDB"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0173 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x0173 }
            r1.execSQL(r2)     // Catch:{ SQLException -> 0x0173 }
            java.lang.String r1 = "SCL:openDataBase()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0173 }
            r2.<init>()     // Catch:{ SQLException -> 0x0173 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ SQLException -> 0x0173 }
            java.lang.String r2 = " ATTACHed."
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ SQLException -> 0x0173 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0173 }
            android.util.Log.d(r1, r0)     // Catch:{ SQLException -> 0x0173 }
            com.cyberandsons.tcmaidtrial.a.n r0 = new com.cyberandsons.tcmaidtrial.a.n     // Catch:{ SQLException -> 0x0173 }
            r0.<init>()     // Catch:{ SQLException -> 0x0173 }
            r7.f = r0     // Catch:{ SQLException -> 0x0173 }
            com.cyberandsons.tcmaidtrial.a.n r0 = r7.f     // Catch:{ SQLException -> 0x0173 }
            android.database.sqlite.SQLiteDatabase r1 = r7.e     // Catch:{ SQLException -> 0x0173 }
            r0.a(r1)     // Catch:{ SQLException -> 0x0173 }
        L_0x00d6:
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.d     // Catch:{ Exception -> 0x01bd }
            if (r0 == 0) goto L_0x00de
            r0 = 1
            r7.setRequestedOrientation(r0)     // Catch:{ Exception -> 0x01bd }
        L_0x00de:
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.cO     // Catch:{ Exception -> 0x01bd }
            switch(r0) {
                case 0: goto L_0x0196;
                case 1: goto L_0x01ff;
                case 2: goto L_0x023b;
                case 3: goto L_0x0295;
                case 4: goto L_0x0301;
                case 5: goto L_0x03cd;
                case 6: goto L_0x0409;
                case 7: goto L_0x0445;
                case 8: goto L_0x0355;
                default: goto L_0x00e3;
            }     // Catch:{ Exception -> 0x01bd }
        L_0x00e3:
            r0 = 2130903118(0x7f03004e, float:1.7413045E38)
            r7.setContentView(r0)     // Catch:{ Exception -> 0x01bd }
            r0 = 2131361903(0x7f0a006f, float:1.8343571E38)
            android.view.View r0 = r7.findViewById(r0)     // Catch:{ Exception -> 0x01bd }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x01bd }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cP     // Catch:{ Exception -> 0x01bd }
            r0.setText(r1)     // Catch:{ Exception -> 0x01bd }
            android.graphics.Paint r2 = new android.graphics.Paint     // Catch:{ Exception -> 0x01bd }
            r2.<init>()     // Catch:{ Exception -> 0x01bd }
            float r3 = r0.getTextSize()     // Catch:{ Exception -> 0x01bd }
            r2.setTextSize(r3)     // Catch:{ Exception -> 0x01bd }
            r3 = 1
            r2.setAntiAlias(r3)     // Catch:{ Exception -> 0x01bd }
            float r1 = r2.measureText(r1)     // Catch:{ Exception -> 0x01bd }
            r2 = 1056964608(0x3f000000, float:0.5)
            float r1 = r1 + r2
            int r1 = (int) r1     // Catch:{ Exception -> 0x01bd }
            android.util.DisplayMetrics r2 = new android.util.DisplayMetrics     // Catch:{ Exception -> 0x01bd }
            r2.<init>()     // Catch:{ Exception -> 0x01bd }
            android.view.WindowManager r3 = r7.getWindowManager()     // Catch:{ Exception -> 0x01bd }
            android.view.Display r3 = r3.getDefaultDisplay()     // Catch:{ Exception -> 0x01bd }
            r3.getMetrics(r2)     // Catch:{ Exception -> 0x01bd }
            int r2 = r2.widthPixels     // Catch:{ Exception -> 0x01bd }
            int r2 = r2 / 2
            int r1 = r1 / 2
            int r1 = r2 - r1
            r2 = 10
            r3 = 0
            r4 = 10
            r0.setPadding(r1, r2, r3, r4)     // Catch:{ Exception -> 0x01bd }
            android.widget.ListView r0 = r7.getListView()     // Catch:{ Exception -> 0x01bd }
            r7.f743a = r0     // Catch:{ Exception -> 0x01bd }
            android.widget.ListView r0 = r7.f743a     // Catch:{ Exception -> 0x01bd }
            r1 = 2
            r0.setChoiceMode(r1)     // Catch:{ Exception -> 0x01bd }
            android.widget.ListView r0 = r7.f743a     // Catch:{ Exception -> 0x01bd }
            com.cyberandsons.tcmaidtrial.lists.bw r1 = new com.cyberandsons.tcmaidtrial.lists.bw     // Catch:{ Exception -> 0x01bd }
            r1.<init>(r7)     // Catch:{ Exception -> 0x01bd }
            r0.setOnTouchListener(r1)     // Catch:{ Exception -> 0x01bd }
            android.widget.ArrayAdapter r0 = new android.widget.ArrayAdapter     // Catch:{ Exception -> 0x01bd }
            r1 = 17367056(0x1090010, float:2.516297E-38)
            java.lang.String[] r2 = r7.f744b     // Catch:{ Exception -> 0x01bd }
            r0.<init>(r7, r1, r2)     // Catch:{ Exception -> 0x01bd }
            r7.setListAdapter(r0)     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = r7.c     // Catch:{ Exception -> 0x01bd }
            java.lang.String r1 = ","
            java.lang.String[] r0 = r0.split(r1)     // Catch:{ Exception -> 0x01bd }
            r1 = r6
        L_0x015b:
            int r2 = r0.length     // Catch:{ Exception -> 0x01bd }
            if (r1 >= r2) goto L_0x01fe
            r2 = r0[r1]     // Catch:{ Exception -> 0x01bd }
            int r3 = r2.length()     // Catch:{ Exception -> 0x01bd }
            if (r3 == 0) goto L_0x0170
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Exception -> 0x01bd }
            android.widget.ListView r3 = r7.f743a     // Catch:{ Exception -> 0x01bd }
            r4 = 1
            r3.setItemChecked(r2, r4)     // Catch:{ Exception -> 0x01bd }
        L_0x0170:
            int r1 = r1 + 1
            goto L_0x015b
        L_0x0173:
            r0 = move-exception
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r7)
            android.app.AlertDialog r0 = r0.create()
            java.lang.String r1 = "Attention:"
            r0.setTitle(r1)
            java.lang.String r1 = "Please restart TCM Clinic Aid. The database was not able to reopen."
            r0.setMessage(r1)
            java.lang.String r1 = "OK"
            com.cyberandsons.tcmaidtrial.lists.by r2 = new com.cyberandsons.tcmaidtrial.lists.by
            r2.<init>(r7)
            r0.setButton(r1, r2)
            r0.show()
            goto L_0x00d6
        L_0x0196:
            r0 = 4
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01bd }
            r1 = 0
            java.lang.String r2 = "Name"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 1
            java.lang.String r2 = "Indications"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 2
            java.lang.String r2 = "Point Category"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 3
            java.lang.String r2 = "Notes"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r7.f744b = r0     // Catch:{ Exception -> 0x01bd }
            com.cyberandsons.tcmaidtrial.a.n r0 = r7.f     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = r0.D()     // Catch:{ Exception -> 0x01bd }
            r7.c = r0     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = "auricularSearch"
            r7.d = r0     // Catch:{ Exception -> 0x01bd }
            goto L_0x00e3
        L_0x01bd:
            r0 = move-exception
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()
            java.lang.String r2 = "myVariable"
            java.lang.String r0 = r0.getLocalizedMessage()
            r1.a(r2, r0)
            org.acra.ErrorReporter r0 = org.acra.ErrorReporter.a()
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r2 = "SCL:onCreate()"
            r1.<init>(r2)
            r0.handleSilentException(r1)
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r7)
            android.app.AlertDialog r0 = r0.create()
            java.lang.String r1 = "Attention:"
            r0.setTitle(r1)
            r1 = 2131099670(0x7f060016, float:1.78117E38)
            java.lang.String r1 = r7.getString(r1)
            r0.setMessage(r1)
            java.lang.String r1 = "OK"
            com.cyberandsons.tcmaidtrial.lists.bx r2 = new com.cyberandsons.tcmaidtrial.lists.bx
            r2.<init>(r7)
            r0.setButton(r1, r2)
            r0.show()
        L_0x01fe:
            return
        L_0x01ff:
            r0 = 8
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01bd }
            r1 = 0
            java.lang.String r2 = "Pathology"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 1
            java.lang.String r2 = "Indications"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 2
            java.lang.String r2 = "Tx Principles"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 3
            java.lang.String r2 = "Tongue"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 4
            java.lang.String r2 = "Pulse"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 5
            java.lang.String r2 = "Points"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 6
            java.lang.String r2 = "Formula"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 7
            java.lang.String r2 = "Notes"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r7.f744b = r0     // Catch:{ Exception -> 0x01bd }
            com.cyberandsons.tcmaidtrial.a.n r0 = r7.f     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = r0.E()     // Catch:{ Exception -> 0x01bd }
            r7.c = r0     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = "diagnosisSearch"
            r7.d = r0     // Catch:{ Exception -> 0x01bd }
            goto L_0x00e3
        L_0x023b:
            r0 = 13
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01bd }
            r1 = 0
            java.lang.String r2 = "Pinyin"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 1
            java.lang.String r2 = "English Name"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 2
            java.lang.String r2 = "Diagnosis"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 3
            java.lang.String r2 = "Indications"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 4
            java.lang.String r2 = "Functions"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 5
            java.lang.String r2 = "Tongue"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 6
            java.lang.String r2 = "Pulse"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 7
            java.lang.String r2 = "Ingredients"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 8
            java.lang.String r2 = "Herb Breakdown"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 9
            java.lang.String r2 = "Modification"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 10
            java.lang.String r2 = "Contra-Indications"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 11
            java.lang.String r2 = "Western Usage"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 12
            java.lang.String r2 = "Notes"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r7.f744b = r0     // Catch:{ Exception -> 0x01bd }
            com.cyberandsons.tcmaidtrial.a.n r0 = r7.f     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = r0.F()     // Catch:{ Exception -> 0x01bd }
            r7.c = r0     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = "formulaSearch"
            r7.d = r0     // Catch:{ Exception -> 0x01bd }
            goto L_0x00e3
        L_0x0295:
            r0 = 16
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01bd }
            r1 = 0
            java.lang.String r2 = "Pinyin"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 1
            java.lang.String r2 = "English Name"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 2
            java.lang.String r2 = "Phamaceutical Name"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 3
            java.lang.String r2 = "Meridians"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 4
            java.lang.String r2 = "Taste"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 5
            java.lang.String r2 = "Temperature"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 6
            java.lang.String r2 = "Functions"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 7
            java.lang.String r2 = "Indications"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 8
            java.lang.String r2 = "Contra-Indications"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 9
            java.lang.String r2 = "Dosage"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 10
            java.lang.String r2 = "Cautions"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 11
            java.lang.String r2 = "Combinations"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 12
            java.lang.String r2 = "General Functions"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 13
            java.lang.String r2 = "Toxicology"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 14
            java.lang.String r2 = "Pharmacology"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 15
            java.lang.String r2 = "Notes"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r7.f744b = r0     // Catch:{ Exception -> 0x01bd }
            com.cyberandsons.tcmaidtrial.a.n r0 = r7.f     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = r0.G()     // Catch:{ Exception -> 0x01bd }
            r7.c = r0     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = "herbSearch"
            r7.d = r0     // Catch:{ Exception -> 0x01bd }
            goto L_0x00e3
        L_0x0301:
            r0 = 12
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01bd }
            r1 = 0
            java.lang.String r2 = "Name (i.e., LI4)"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 1
            java.lang.String r2 = "Location"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 2
            java.lang.String r2 = "English"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 3
            java.lang.String r2 = "Indications"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 4
            java.lang.String r2 = "Functions"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 5
            java.lang.String r2 = "Needling Method"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 6
            java.lang.String r2 = "Contra-Indications"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 7
            java.lang.String r2 = "Local Application"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 8
            java.lang.String r2 = "Miscellaneous Use"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 9
            java.lang.String r2 = "Needling Combinations"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 10
            java.lang.String r2 = "Notes"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 11
            java.lang.String r2 = "Pinyin (i.e., He Gu)"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r7.f744b = r0     // Catch:{ Exception -> 0x01bd }
            com.cyberandsons.tcmaidtrial.a.n r0 = r7.f     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = r0.H()     // Catch:{ Exception -> 0x01bd }
            r7.c = r0     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = "pointSearch"
            r7.d = r0     // Catch:{ Exception -> 0x01bd }
            goto L_0x00e3
        L_0x0355:
            r0 = 18
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01bd }
            r1 = 0
            java.lang.String r2 = "Name"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 1
            java.lang.String r2 = "Pinyin"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 2
            java.lang.String r2 = "Category"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 3
            java.lang.String r2 = "Description"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 4
            java.lang.String r2 = "Sensation"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 5
            java.lang.String r2 = "Indications"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 6
            java.lang.String r2 = "Left_Cun"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 7
            java.lang.String r2 = "Left_Guan"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 8
            java.lang.String r2 = "Left_Chi"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 9
            java.lang.String r2 = "Right_Cun"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 10
            java.lang.String r2 = "Right_Guan"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 11
            java.lang.String r2 = "Right_Chi"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 12
            java.lang.String r2 = "Bilateral_Cun"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 13
            java.lang.String r2 = "Bilateral_Guan"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 14
            java.lang.String r2 = "Bilateral_Chi"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 15
            java.lang.String r2 = "Combinations"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 16
            java.lang.String r2 = "Alternate Names"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 17
            java.lang.String r2 = "Notes"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r7.f744b = r0     // Catch:{ Exception -> 0x01bd }
            com.cyberandsons.tcmaidtrial.a.n r0 = r7.f     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = r0.J()     // Catch:{ Exception -> 0x01bd }
            r7.c = r0     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = "pulseDiagSearch"
            r7.d = r0     // Catch:{ Exception -> 0x01bd }
            goto L_0x00e3
        L_0x03cd:
            r0 = 8
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01bd }
            r1 = 0
            java.lang.String r2 = "Stage"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 1
            java.lang.String r2 = "Description"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 2
            java.lang.String r2 = "Indications"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 3
            java.lang.String r2 = "Tongue"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 4
            java.lang.String r2 = "Pulse"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 5
            java.lang.String r2 = "Points"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 6
            java.lang.String r2 = "Formula"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 7
            java.lang.String r2 = "Notes"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r7.f744b = r0     // Catch:{ Exception -> 0x01bd }
            com.cyberandsons.tcmaidtrial.a.n r0 = r7.f     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = r0.K()     // Catch:{ Exception -> 0x01bd }
            r7.c = r0     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = "sixstageSearch"
            r7.d = r0     // Catch:{ Exception -> 0x01bd }
            goto L_0x00e3
        L_0x0409:
            r0 = 8
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01bd }
            r1 = 0
            java.lang.String r2 = "Point Number"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 1
            java.lang.String r2 = "Name"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 2
            java.lang.String r2 = "English Name"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 3
            java.lang.String r2 = "Location"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 4
            java.lang.String r2 = "Indications"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 5
            java.lang.String r2 = "Anatomy"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 6
            java.lang.String r2 = "Preparation"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 7
            java.lang.String r2 = "Notes"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r7.f744b = r0     // Catch:{ Exception -> 0x01bd }
            com.cyberandsons.tcmaidtrial.a.n r0 = r7.f     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = r0.L()     // Catch:{ Exception -> 0x01bd }
            r7.c = r0     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = "tungSearch"
            r7.d = r0     // Catch:{ Exception -> 0x01bd }
            goto L_0x00e3
        L_0x0445:
            r0 = 4
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01bd }
            r1 = 0
            java.lang.String r2 = "Name"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 1
            java.lang.String r2 = "Location"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 2
            java.lang.String r2 = "Indications"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r1 = 3
            java.lang.String r2 = "Notes"
            r0[r1] = r2     // Catch:{ Exception -> 0x01bd }
            r7.f744b = r0     // Catch:{ Exception -> 0x01bd }
            com.cyberandsons.tcmaidtrial.a.n r0 = r7.f     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = r0.M()     // Catch:{ Exception -> 0x01bd }
            r7.c = r0     // Catch:{ Exception -> 0x01bd }
            java.lang.String r0 = "wristankleSearch"
            r7.d = r0     // Catch:{ Exception -> 0x01bd }
            goto L_0x00e3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.SearchConfigList.onCreate(android.os.Bundle):void");
    }

    public void onDestroy() {
        String str = "";
        SparseBooleanArray checkedItemPositions = this.f743a.getCheckedItemPositions();
        boolean z = true;
        for (int i = 0; i < checkedItemPositions.size(); i++) {
            if (checkedItemPositions.valueAt(i)) {
                if (z) {
                    str = Integer.toString(checkedItemPositions.keyAt(i));
                    z = false;
                } else {
                    str = str + ',' + Integer.toString(checkedItemPositions.keyAt(i));
                }
            }
        }
        if (this.d.equalsIgnoreCase("auricularSearch")) {
            this.f.h(str, this.e);
        } else if (this.d.equalsIgnoreCase("diagnosisSearch")) {
            this.f.i(str, this.e);
        } else if (this.d.equalsIgnoreCase("herbSearch")) {
            this.f.k(str, this.e);
        } else if (this.d.equalsIgnoreCase("formulaSearch")) {
            this.f.j(str, this.e);
        } else if (this.d.equalsIgnoreCase("pointSearch")) {
            this.f.l(str, this.e);
        } else if (this.d.equalsIgnoreCase("pulseDiagSearch")) {
            this.f.m(str, this.e);
        } else if (this.d.equalsIgnoreCase("sixstageSearch")) {
            this.f.n(str, this.e);
        } else if (this.d.equalsIgnoreCase("tungSearch")) {
            this.f.o(str, this.e);
        } else if (this.d.equalsIgnoreCase("wristankleSearch")) {
            this.f.p(str, this.e);
        }
        try {
            if (this.e != null && this.e.isOpen()) {
                this.e.close();
                this.e = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }
}
