package com.cyberandsons.tcmaidtrial.lists;

import android.content.DialogInterface;
import android.database.sqlite.SQLiteCursor;
import com.cyberandsons.tcmaidtrial.TcmAid;

final class bf implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointsList f791a;

    bf(PointsList pointsList) {
        this.f791a = pointsList;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        int i2;
        if (i == 0) {
            boolean unused = this.f791a.A = this.f791a.f740b;
            int unused2 = this.f791a.C = 0;
        } else if (i == 1) {
            boolean unused3 = this.f791a.A = this.f791a.f740b;
            int unused4 = this.f791a.C = 1;
        } else if (i == 2) {
            boolean unused5 = this.f791a.A = this.f791a.f739a;
        }
        this.f791a.d = this.f791a.getListView().onSaveInstanceState();
        if (this.f791a.A) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        TcmAid.cW = i2;
        this.f791a.stopManagingCursor(this.f791a.t);
        if (this.f791a.t != null) {
            this.f791a.t.close();
            SQLiteCursor unused6 = this.f791a.t = (SQLiteCursor) null;
        }
        TcmAid.ap = null;
        SQLiteCursor unused7 = this.f791a.t = this.f791a.d();
        this.f791a.setListAdapter(this.f791a.a(this.f791a.c));
        this.f791a.getListView().invalidate();
        this.f791a.getListView().onRestoreInstanceState(this.f791a.d);
    }
}
