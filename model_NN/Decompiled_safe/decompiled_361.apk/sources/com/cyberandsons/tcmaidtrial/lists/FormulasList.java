package com.cyberandsons.tcmaidtrial.lists;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.c;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.detailed.FormulasDetail;
import com.cyberandsons.tcmaidtrial.misc.dh;
import org.acra.ErrorReporter;

public class FormulasList extends ListActivity {
    private static String h;
    private static String i;
    private static boolean m;

    /* renamed from: a  reason: collision with root package name */
    Parcelable f733a;

    /* renamed from: b  reason: collision with root package name */
    private final String f734b = "t_formulas";
    private final String c = "main.formulas.";
    private final String d = "_id";
    private String e = "pinyin";
    private String f;
    private String[] g;
    private String j = "pinyin";
    private String k = "pinyin";
    private String l;
    private boolean n;
    /* access modifiers changed from: private */
    public SQLiteCursor o;
    /* access modifiers changed from: private */
    public SQLiteDatabase p;
    /* access modifiers changed from: private */
    public n q;
    private boolean r = true;
    private boolean s;
    private ImageButton t;
    private ImageButton u;
    private int v;
    /* access modifiers changed from: private */
    public String w;
    private boolean x;
    private int y;
    /* access modifiers changed from: private */
    public int z;

    public FormulasList() {
        boolean z2;
        if (!this.r) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.s = z2;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        e();
        try {
            if (dh.d) {
                setRequestedOrientation(1);
            }
            this.x = this.q.W() == 1 ? this.r : this.s;
            this.v = this.q.N();
            if (TcmAid.cV != -1) {
                this.v = TcmAid.cV;
            }
            this.y = this.q.ac();
            if (this.v == 0) {
                this.v += this.y;
            }
            a(this.v);
            this.l = "SELECT _id, " + this.w + " FROM formulas";
            setContentView((int) C0000R.layout.list_content_with_empty_view_w_language);
            TextView textView = (TextView) findViewById(C0000R.id.tca_label);
            String str = "Individual Formulas";
            this.z = -1;
            if (TcmAid.P) {
                this.z = Integer.parseInt(TcmAid.R[0]);
                str = (String) c.a(TcmAid.R[0], this.p);
            }
            if (TcmAid.cP != null) {
                textView.setText(TcmAid.cP);
            } else {
                textView.setText(str);
            }
            this.t = (ImageButton) findViewById(C0000R.id.add);
            this.t.setOnClickListener(new n(this));
            this.u = (ImageButton) findViewById(C0000R.id.lang);
            this.u.setOnClickListener(new j(this));
            if (this.n) {
                d();
            }
            this.o = b();
            setListAdapter(c());
            TextView textView2 = (TextView) findViewById(C0000R.id.empty);
            textView2.setText("");
            ListView listView = getListView();
            listView.setFastScrollEnabled(true);
            listView.setEmptyView(textView2);
        } catch (Exception e2) {
            ErrorReporter.a().a("myVariable", e2.getLocalizedMessage());
            ErrorReporter.a().handleSilentException(new Exception("FCL:onCreate()"));
            AlertDialog create = new AlertDialog.Builder(this).create();
            create.setTitle("Attention:");
            create.setMessage(getString(C0000R.string.area_list_exception));
            create.setButton("OK", new k(this));
            create.show();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (this.z > 1000) {
            menu.add(0, 2, 0, "Delete").setIcon(17301564);
        }
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 2:
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Warning");
                create.setMessage("Are you sure you want to delete this Formula category and all it's content?");
                create.setButton("OK", new p(this));
                create.setButton2("Cancel", new o(this));
                create.show();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Display Formulas by ...");
        builder.setSingleChoiceItems(new CharSequence[]{"Pinyin w/Simplified Characters", "Pinyin w/Traditional Characters", "English"}, this.v, new l(this));
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        this.v = i2;
        if (i2 == 0) {
            this.w = "pinyin";
            this.e = "pinyin";
            this.j = "pinyin";
            this.k = "pinyin";
            this.x = this.r;
            this.y = 0;
        } else if (i2 == 1) {
            this.w = "pinyin";
            this.e = "pinyin";
            this.j = "pinyin";
            this.k = "pinyin";
            this.x = this.r;
            this.y = 1;
        } else {
            this.w = "english";
            this.e = "english";
            this.j = "english";
            this.k = "english";
            this.x = this.s;
        }
        TcmAid.cV = this.v;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        e();
        if (TcmAid.cB) {
            startActivity(getIntent());
            finish();
            TcmAid.cB = false;
        }
        if (TcmAid.bl) {
            TcmAid.bl = this.s;
            try {
                if (dh.e) {
                    if (TcmAid.W > 0) {
                        TcmAid.W--;
                        TcmAid.S = (String) TcmAid.X.get(TcmAid.W);
                        TcmAid.X.remove(TcmAid.W);
                        if (dh.T) {
                            Log.d("FL:onResume()", "fxCounter-- = " + Integer.toString(TcmAid.W) + "CounterArrary.count = " + Integer.toString(TcmAid.X.size()));
                        }
                    }
                    if (dh.ac) {
                        Log.d("FL:onResume()", TcmAid.S);
                    }
                }
                stopManagingCursor(this.o);
                if (this.o != null) {
                    this.o.close();
                    this.o = null;
                }
                TcmAid.Q = null;
                this.o = b();
                setListAdapter(c());
                getListView().invalidate();
                getListView().onRestoreInstanceState(this.f733a);
            } catch (SQLException e2) {
                q.a(e2);
            } catch (Exception e3) {
                Log.e("FL:onResume()", e3.getLocalizedMessage());
            }
        }
        super.onResume();
    }

    public void onDestroy() {
        try {
            TcmAid.Q = null;
            TcmAid.P = this.s;
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            if (this.p != null && this.p.isOpen()) {
                this.p.close();
                this.p = null;
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        if (i2 >= 0) {
            super.onListItemClick(listView, view, i2, j2);
            if (dh.x) {
                Log.d("FL:onListItemClick()", "position = " + Integer.toString(i2) + ", id = " + Long.toString(j2));
            }
            TcmAid.Q = "main.formulas._id=" + Integer.toString((int) j2) + ')';
            TcmAid.V = (int) j2;
            TcmAid.L = i2;
            TcmAid.P = this.s;
            if (TcmAid.S != null) {
                TcmAid.W++;
                TcmAid.X.add(TcmAid.S);
                if (dh.T) {
                    Log.d("FormulasList:", "fxCounter++ = " + Integer.toString(TcmAid.W) + ", fxCounterArrary.count = " + Integer.toString(TcmAid.X.size()));
                }
            }
            this.f733a = getListView().onSaveInstanceState();
            startActivityForResult(new Intent(this, FormulasDetail.class), 1350);
        }
    }

    /* access modifiers changed from: private */
    public SQLiteCursor b() {
        try {
            if (dh.x) {
                Log.d("FL:createCursor()", "Inserting into t_formulas");
            }
            if (TcmAid.S == null) {
                String str = "";
                if (this.q.ah()) {
                    str = " AND main.formulas.req_state_board=1 ";
                } else if (this.q.af()) {
                    str = " AND main.formulas.nccaom_req=1 ";
                }
                TcmAid.S = c.a(this.w, this.q, str);
            }
            this.p.execSQL(c.a("t_formulas"));
            this.p.execSQL(c.a("t_formulas", TcmAid.S, this.v));
        } catch (SQLException e2) {
            q.a(e2);
        }
        if (TcmAid.Q != null) {
            this.f = TcmAid.Q;
            TcmAid.Q = null;
            this.g = TcmAid.R;
            TcmAid.R = null;
        }
        try {
            this.o = (SQLiteCursor) this.p.query(m, "t_formulas", new String[]{"_id", this.e}, this.f, this.g, h, i, this.j, null);
            startManagingCursor(this.o);
            int count = this.o.getCount();
            if (dh.x) {
                Log.d("FL:createCursor()", "query count = " + Integer.toString(count));
            }
            TcmAid.M.clear();
            if (this.o.moveToFirst()) {
                do {
                    TcmAid.M.add(Integer.valueOf(this.o.getInt(0)));
                } while (this.o.moveToNext());
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        return this.o;
    }

    /* access modifiers changed from: private */
    public ListAdapter c() {
        return new ai(this, this.o, new String[]{"_id", this.e}, new int[]{C0000R.id.text0, C0000R.id.text1}, this.v != 2, this.x, this.y, this.p);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d() {
        /*
            r4 = this;
            r3 = 0
            android.database.sqlite.SQLiteDatabase r0 = r4.p     // Catch:{ SQLException -> 0x0096, all -> 0x00a1 }
            java.lang.String r1 = r4.l     // Catch:{ SQLException -> 0x0096, all -> 0x00a1 }
            r2 = 0
            android.database.Cursor r4 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x0096, all -> 0x00a1 }
            android.database.sqlite.SQLiteCursor r4 = (android.database.sqlite.SQLiteCursor) r4     // Catch:{ SQLException -> 0x0096, all -> 0x00a1 }
            int r0 = r4.getCount()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            boolean r1 = com.cyberandsons.tcmaidtrial.misc.dh.x     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            if (r1 == 0) goto L_0x0056
            java.lang.String r1 = "FL:doRawCheck()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            r2.<init>()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r3 = "query count = "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            android.util.Log.d(r1, r0)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r0 = "FL:doRawCheck()"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            r1.<init>()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r2 = "column count = '"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            int r2 = r4.getColumnCount()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r2 = "' "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            android.util.Log.d(r0, r1)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
        L_0x0056:
            boolean r0 = r4.moveToFirst()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            if (r0 == 0) goto L_0x0090
        L_0x005c:
            r0 = 1
            java.lang.String r0 = r4.getString(r0)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            r1 = 0
            int r1 = r4.getInt(r1)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.x     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            if (r2 == 0) goto L_0x008a
            java.lang.String r2 = "FL:doRawCheck()"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            r3.<init>()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r3 = " - "
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            android.util.Log.d(r2, r0)     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
        L_0x008a:
            boolean r0 = r4.moveToNext()     // Catch:{ SQLException -> 0x00ae, all -> 0x00a9 }
            if (r0 != 0) goto L_0x005c
        L_0x0090:
            if (r4 == 0) goto L_0x0095
            r4.close()
        L_0x0095:
            return
        L_0x0096:
            r0 = move-exception
            r1 = r3
        L_0x0098:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x00ac }
            if (r1 == 0) goto L_0x0095
            r1.close()
            goto L_0x0095
        L_0x00a1:
            r0 = move-exception
            r1 = r3
        L_0x00a3:
            if (r1 == 0) goto L_0x00a8
            r1.close()
        L_0x00a8:
            throw r0
        L_0x00a9:
            r0 = move-exception
            r1 = r4
            goto L_0x00a3
        L_0x00ac:
            r0 = move-exception
            goto L_0x00a3
        L_0x00ae:
            r0 = move-exception
            r1 = r4
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.FormulasList.d():void");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }

    private void e() {
        if (this.p == null || !this.p.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("FL:openDataBase()", str + " opened.");
                this.p = SQLiteDatabase.openDatabase(str, null, 16);
                this.p.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.p.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("FL:openDataBase()", str2 + " ATTACHed.");
                this.q = new n();
                this.q.a(this.p);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new q(this));
                create.show();
            }
        }
    }
}
