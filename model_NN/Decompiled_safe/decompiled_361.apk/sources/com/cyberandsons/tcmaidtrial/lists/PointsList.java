package com.cyberandsons.tcmaidtrial.lists;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.additems.PointsAdd;
import com.cyberandsons.tcmaidtrial.areas.l;
import com.cyberandsons.tcmaidtrial.detailed.PointsDetail;
import com.cyberandsons.tcmaidtrial.misc.dh;

public class PointsList extends ListActivity {
    private static String g = "abbr_name";
    private static String h = "pointnum";
    private static String m;
    private static String n;
    private static String o = (g + ", " + h);
    private static String p = g;
    private static String q = "SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id=main.pointslocation.meridian UNION SELECT userDB.pointslocation._id, main.meridian.abbr_name, userDB.pointslocation.pointnum, userDB.pointslocation.name, userDB.pointslocation.indication FROM userDB.pointslocation LEFT JOIN main.meridian ON main.meridian._id=userDB.pointslocation.meridianORDER BY 2, 3";
    private static boolean r;
    /* access modifiers changed from: private */
    public boolean A;
    private boolean B;
    /* access modifiers changed from: private */
    public int C;

    /* renamed from: a  reason: collision with root package name */
    boolean f739a = true;

    /* renamed from: b  reason: collision with root package name */
    boolean f740b;
    boolean c;
    Parcelable d;
    private final String e;
    private final String f;
    private final String i;
    private final String j;
    private String k;
    private String[] l;
    private boolean s;
    /* access modifiers changed from: private */
    public SQLiteCursor t;
    /* access modifiers changed from: private */
    public SQLiteDatabase u;
    /* access modifiers changed from: private */
    public n v;
    private ImageButton w;
    private ImageButton x;
    private l y;
    private boolean z;

    public PointsList() {
        this.f740b = !this.f739a;
        this.e = "t_points";
        this.f = "_id";
        this.i = "name";
        this.j = "englishname";
        this.c = false;
        this.z = this.f740b;
    }

    /* JADX WARNING: Removed duplicated region for block: B:57:0x01d4 A[SYNTHETIC, Splitter:B:57:0x01d4] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01dd A[Catch:{ Exception -> 0x018b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r7) {
        /*
            r6 = this;
            r3 = 0
            r2 = 1
            super.onCreate(r7)
            r6.e()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.d     // Catch:{ Exception -> 0x018b }
            if (r0 == 0) goto L_0x0010
            r0 = 1
            r6.setRequestedOrientation(r0)     // Catch:{ Exception -> 0x018b }
        L_0x0010:
            com.cyberandsons.tcmaidtrial.a.n r0 = r6.v     // Catch:{ Exception -> 0x018b }
            int r0 = r0.U()     // Catch:{ Exception -> 0x018b }
            if (r0 != r2) goto L_0x0151
            boolean r0 = r6.f739a     // Catch:{ Exception -> 0x018b }
        L_0x001a:
            r6.B = r0     // Catch:{ Exception -> 0x018b }
            com.cyberandsons.tcmaidtrial.a.n r0 = r6.v     // Catch:{ Exception -> 0x018b }
            int r0 = r0.R()     // Catch:{ Exception -> 0x018b }
            if (r0 != r2) goto L_0x0155
            boolean r0 = r6.f739a     // Catch:{ Exception -> 0x018b }
        L_0x0026:
            r6.A = r0     // Catch:{ Exception -> 0x018b }
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.cW     // Catch:{ Exception -> 0x018b }
            r1 = -1
            if (r0 == r1) goto L_0x0035
            int r0 = com.cyberandsons.tcmaidtrial.TcmAid.cW     // Catch:{ Exception -> 0x018b }
            if (r0 != r2) goto L_0x0159
            boolean r0 = r6.f739a     // Catch:{ Exception -> 0x018b }
        L_0x0033:
            r6.A = r0     // Catch:{ Exception -> 0x018b }
        L_0x0035:
            com.cyberandsons.tcmaidtrial.a.n r0 = r6.v     // Catch:{ Exception -> 0x018b }
            boolean r0 = r0.v()     // Catch:{ Exception -> 0x018b }
            r6.c = r0     // Catch:{ Exception -> 0x018b }
            com.cyberandsons.tcmaidtrial.a.n r0 = r6.v     // Catch:{ Exception -> 0x018b }
            int r0 = r0.ac()     // Catch:{ Exception -> 0x018b }
            r6.C = r0     // Catch:{ Exception -> 0x018b }
            r0 = 2130903084(0x7f03002c, float:1.7412976E38)
            r6.setContentView(r0)     // Catch:{ Exception -> 0x018b }
            r0 = 2131362326(0x7f0a0216, float:1.834443E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x018b }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x018b }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cP     // Catch:{ Exception -> 0x018b }
            if (r1 == 0) goto L_0x015d
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cP     // Catch:{ Exception -> 0x018b }
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x005d:
            r1.setText(r0)     // Catch:{ Exception -> 0x018b }
            r0 = 2131362327(0x7f0a0217, float:1.8344431E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x018b }
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0     // Catch:{ Exception -> 0x018b }
            r6.w = r0     // Catch:{ Exception -> 0x018b }
            boolean r0 = com.cyberandsons.tcmaidtrial.TcmAid.cv     // Catch:{ Exception -> 0x018b }
            boolean r1 = r6.f740b     // Catch:{ Exception -> 0x018b }
            if (r0 != r1) goto L_0x016f
            android.widget.ImageButton r0 = r6.w     // Catch:{ Exception -> 0x018b }
            com.cyberandsons.tcmaidtrial.lists.bk r1 = new com.cyberandsons.tcmaidtrial.lists.bk     // Catch:{ Exception -> 0x018b }
            r1.<init>(r6)     // Catch:{ Exception -> 0x018b }
            r0.setOnClickListener(r1)     // Catch:{ Exception -> 0x018b }
            r0 = 2131362328(0x7f0a0218, float:1.8344433E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x018b }
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0     // Catch:{ Exception -> 0x018b }
            r6.x = r0     // Catch:{ Exception -> 0x018b }
            android.widget.ImageButton r0 = r6.x     // Catch:{ Exception -> 0x018b }
            com.cyberandsons.tcmaidtrial.lists.bj r1 = new com.cyberandsons.tcmaidtrial.lists.bj     // Catch:{ Exception -> 0x018b }
            r1.<init>(r6)     // Catch:{ Exception -> 0x018b }
            r0.setOnClickListener(r1)     // Catch:{ Exception -> 0x018b }
        L_0x0090:
            boolean r0 = r6.s     // Catch:{ Exception -> 0x018b }
            if (r0 == 0) goto L_0x0128
            android.database.sqlite.SQLiteDatabase r0 = r6.u     // Catch:{ SQLException -> 0x01cd, all -> 0x01d9 }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.lists.PointsList.q     // Catch:{ SQLException -> 0x01cd, all -> 0x01d9 }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x01cd, all -> 0x01d9 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x01cd, all -> 0x01d9 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.n     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            if (r2 == 0) goto L_0x00e9
            java.lang.String r2 = "PL:doRawCheck()"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            r3.<init>()     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            java.lang.String r4 = "query count = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            android.util.Log.d(r2, r1)     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            java.lang.String r1 = "PL:doRawCheck()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            r2.<init>()     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            java.lang.String r3 = "column count = '"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            int r3 = r0.getColumnCount()     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            java.lang.String r3 = "' "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            android.util.Log.d(r1, r2)     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
        L_0x00e9:
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            if (r1 == 0) goto L_0x0123
        L_0x00ef:
            r1 = 1
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            r2 = 0
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.n     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            if (r3 == 0) goto L_0x011d
            java.lang.String r3 = "PL:doRawCheck()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            r4.<init>()     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            java.lang.String r4 = " - "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            android.util.Log.d(r3, r1)     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
        L_0x011d:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x01e8, all -> 0x01e1 }
            if (r1 != 0) goto L_0x00ef
        L_0x0123:
            if (r0 == 0) goto L_0x0128
            r0.close()     // Catch:{ Exception -> 0x018b }
        L_0x0128:
            android.database.sqlite.SQLiteCursor r0 = r6.d()     // Catch:{ Exception -> 0x018b }
            r6.t = r0     // Catch:{ Exception -> 0x018b }
            boolean r0 = r6.c     // Catch:{ Exception -> 0x018b }
            android.widget.ListAdapter r0 = r6.a(r0)     // Catch:{ Exception -> 0x018b }
            r6.setListAdapter(r0)     // Catch:{ Exception -> 0x018b }
            r0 = 2131361994(0x7f0a00ca, float:1.8343756E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x018b }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x018b }
            java.lang.String r1 = ""
            r0.setText(r1)     // Catch:{ Exception -> 0x018b }
            android.widget.ListView r1 = r6.getListView()     // Catch:{ Exception -> 0x018b }
            r2 = 1
            r1.setFastScrollEnabled(r2)     // Catch:{ Exception -> 0x018b }
            r1.setEmptyView(r0)     // Catch:{ Exception -> 0x018b }
        L_0x0150:
            return
        L_0x0151:
            boolean r0 = r6.f740b     // Catch:{ Exception -> 0x018b }
            goto L_0x001a
        L_0x0155:
            boolean r0 = r6.f740b     // Catch:{ Exception -> 0x018b }
            goto L_0x0026
        L_0x0159:
            boolean r0 = r6.f740b     // Catch:{ Exception -> 0x018b }
            goto L_0x0033
        L_0x015d:
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.an     // Catch:{ Exception -> 0x018b }
            if (r1 != 0) goto L_0x0168
            java.lang.String r1 = "Individual Points"
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x005d
        L_0x0168:
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.an     // Catch:{ Exception -> 0x018b }
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x005d
        L_0x016f:
            android.widget.ImageButton r0 = r6.w     // Catch:{ Exception -> 0x018b }
            android.content.res.Resources r1 = r6.getResources()     // Catch:{ Exception -> 0x018b }
            r2 = 17301566(0x108003e, float:2.497943E-38)
            android.graphics.drawable.Drawable r1 = r1.getDrawable(r2)     // Catch:{ Exception -> 0x018b }
            r0.setImageDrawable(r1)     // Catch:{ Exception -> 0x018b }
            android.widget.ImageButton r0 = r6.w     // Catch:{ Exception -> 0x018b }
            com.cyberandsons.tcmaidtrial.lists.bi r1 = new com.cyberandsons.tcmaidtrial.lists.bi     // Catch:{ Exception -> 0x018b }
            r1.<init>(r6)     // Catch:{ Exception -> 0x018b }
            r0.setOnClickListener(r1)     // Catch:{ Exception -> 0x018b }
            goto L_0x0090
        L_0x018b:
            r0 = move-exception
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()
            java.lang.String r2 = "myVariable"
            java.lang.String r0 = r0.getLocalizedMessage()
            r1.a(r2, r0)
            org.acra.ErrorReporter r0 = org.acra.ErrorReporter.a()
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r2 = "PL:onCreate()"
            r1.<init>(r2)
            r0.handleSilentException(r1)
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r6)
            android.app.AlertDialog r0 = r0.create()
            java.lang.String r1 = "Attention:"
            r0.setTitle(r1)
            r1 = 2131099670(0x7f060016, float:1.78117E38)
            java.lang.String r1 = r6.getString(r1)
            r0.setMessage(r1)
            java.lang.String r1 = "OK"
            com.cyberandsons.tcmaidtrial.lists.bg r2 = new com.cyberandsons.tcmaidtrial.lists.bg
            r2.<init>(r6)
            r0.setButton(r1, r2)
            r0.show()
            goto L_0x0150
        L_0x01cd:
            r0 = move-exception
            r1 = r3
        L_0x01cf:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x01e6 }
            if (r1 == 0) goto L_0x0128
            r1.close()     // Catch:{ Exception -> 0x018b }
            goto L_0x0128
        L_0x01d9:
            r0 = move-exception
            r1 = r3
        L_0x01db:
            if (r1 == 0) goto L_0x01e0
            r1.close()     // Catch:{ Exception -> 0x018b }
        L_0x01e0:
            throw r0     // Catch:{ Exception -> 0x018b }
        L_0x01e1:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x01db
        L_0x01e6:
            r0 = move-exception
            goto L_0x01db
        L_0x01e8:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x01cf
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.PointsList.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public final void a() {
        CharSequence[] charSequenceArr = {"Pinyin w/Simplified Characters", "Pinyin w/Traditional Characters", "English"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Display Formulas by ...");
        builder.setSingleChoiceItems(charSequenceArr, this.A ? 2 : this.C == 0 ? 0 : 1, new bf(this));
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        startActivityForResult(new Intent(this, PointsAdd.class), 1350);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0069, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x006a, code lost:
        r6 = r1;
        r1 = r0;
        r0 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0071, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0072, code lost:
        r2 = null;
        r3 = r0;
        r0 = r1;
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x007d, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x007e, code lost:
        r6 = r3;
        r3 = r0;
        r0 = r6;
        r7 = r1;
        r1 = r2;
        r2 = r7;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0069 A[ExcHandler: all (r1v9 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:3:0x001f] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0085  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c() {
        /*
            r8 = this;
            r4 = 0
            r0 = 2131362326(0x7f0a0216, float:1.834443E38)
            android.view.View r0 = r8.findViewById(r0)     // Catch:{ SQLException -> 0x0053, all -> 0x0061 }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ SQLException -> 0x0053, all -> 0x0061 }
            android.database.sqlite.SQLiteDatabase r1 = r8.u     // Catch:{ SQLException -> 0x0053, all -> 0x0061 }
            java.lang.CharSequence r0 = r0.getText()     // Catch:{ SQLException -> 0x0053, all -> 0x0061 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0053, all -> 0x0061 }
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.a.o.a(r0)     // Catch:{ SQLException -> 0x0053, all -> 0x0061 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x0053, all -> 0x0061 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0053, all -> 0x0061 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0071, all -> 0x0069 }
            if (r1 == 0) goto L_0x008c
            r1 = 0
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0071, all -> 0x0069 }
            r2 = 1
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLException -> 0x0077, all -> 0x0069 }
            r3 = 2
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLException -> 0x007d, all -> 0x0069 }
            r6 = r3
            r3 = r1
            r1 = r6
        L_0x0037:
            if (r0 == 0) goto L_0x0087
            r0.close()
            r4 = r1
            r6 = r2
            r2 = r3
            r3 = r6
        L_0x0040:
            com.cyberandsons.tcmaidtrial.areas.l r0 = new com.cyberandsons.tcmaidtrial.areas.l
            com.cyberandsons.tcmaidtrial.lists.bs r5 = new com.cyberandsons.tcmaidtrial.lists.bs
            r5.<init>(r8)
            r1 = r8
            r0.<init>(r1, r2, r3, r4, r5)
            r8.y = r0
            com.cyberandsons.tcmaidtrial.areas.l r0 = r8.y
            r0.show()
            return
        L_0x0053:
            r0 = move-exception
            r1 = r4
            r2 = r4
            r3 = r4
        L_0x0057:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x006e }
            if (r3 == 0) goto L_0x0085
            r3.close()
            r3 = r1
            goto L_0x0040
        L_0x0061:
            r0 = move-exception
            r1 = r4
        L_0x0063:
            if (r1 == 0) goto L_0x0068
            r1.close()
        L_0x0068:
            throw r0
        L_0x0069:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0063
        L_0x006e:
            r0 = move-exception
            r1 = r3
            goto L_0x0063
        L_0x0071:
            r1 = move-exception
            r2 = r4
            r3 = r0
            r0 = r1
            r1 = r4
            goto L_0x0057
        L_0x0077:
            r2 = move-exception
            r3 = r0
            r0 = r2
            r2 = r1
            r1 = r4
            goto L_0x0057
        L_0x007d:
            r3 = move-exception
            r6 = r3
            r3 = r0
            r0 = r6
            r7 = r1
            r1 = r2
            r2 = r7
            goto L_0x0057
        L_0x0085:
            r3 = r1
            goto L_0x0040
        L_0x0087:
            r4 = r1
            r6 = r2
            r2 = r3
            r3 = r6
            goto L_0x0040
        L_0x008c:
            r1 = r4
            r2 = r4
            r3 = r4
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.PointsList.c():void");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        e();
        if (TcmAid.cF) {
            startActivity(getIntent());
            finish();
            TcmAid.cF = false;
        }
        if (TcmAid.bl) {
            TcmAid.bl = this.f740b;
            try {
                if (dh.e) {
                    if (TcmAid.au > 0) {
                        TcmAid.au--;
                        TcmAid.ar = (String) TcmAid.aw.get(TcmAid.au);
                        TcmAid.aw.remove(TcmAid.au);
                        if (dh.W) {
                            Log.d("PL:onResume()", "ptCounter-- = " + Integer.toString(TcmAid.au) + ", ptCounterArrary.count = " + Integer.toString(TcmAid.aw.size()));
                        }
                    } else if (TcmAid.av > 0) {
                        TcmAid.av--;
                        TcmAid.ao = (String) TcmAid.ax.get(TcmAid.av);
                        TcmAid.ax.remove(TcmAid.av);
                        if (dh.W) {
                            Log.d("PL:onResume()", "ptSelCounter-- = " + Integer.toString(TcmAid.av) + ", ptSelCounterArrary.count = " + Integer.toString(TcmAid.ax.size()));
                        }
                    }
                    if (dh.ae) {
                        Log.d("PL:onResume()", TcmAid.ar);
                    }
                }
                stopManagingCursor(this.t);
                if (this.t != null) {
                    this.t.close();
                    this.t = null;
                }
                TcmAid.ap = null;
                this.t = d();
                setListAdapter(a(this.c));
                getListView().invalidate();
                getListView().onRestoreInstanceState(this.d);
            } catch (SQLException e2) {
                q.a(e2);
            } catch (Exception e3) {
                Log.e("PL:onResume()", e3.getLocalizedMessage());
            }
        }
        super.onResume();
    }

    public void onDestroy() {
        try {
            TcmAid.cv = this.f740b;
            TcmAid.ap = null;
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            if (this.u != null && this.u.isOpen()) {
                this.u.close();
                this.u = null;
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        if (i2 >= 0) {
            super.onListItemClick(listView, view, i2, j2);
            if (dh.n) {
                Log.d("PL:onListItemClick()", "position = " + Integer.toString(i2) + ", id = " + Long.toString(j2));
            }
            TcmAid.ap = "main.pointslocation._id=" + Integer.toString((int) j2);
            TcmAid.at = (int) j2;
            TcmAid.ak = i2;
            if (TcmAid.ar != null) {
                TcmAid.au++;
                TcmAid.aw.add(TcmAid.ar);
                if (dh.W) {
                    Log.d("PL:onListItemClick()", "ptCounter++ = " + Integer.toString(TcmAid.au) + ", ptCounterArrary.count = " + Integer.toString(TcmAid.aw.size()));
                }
            } else if (TcmAid.ao != null) {
                TcmAid.av++;
                TcmAid.ax.add(TcmAid.ao);
                if (dh.W) {
                    Log.d("PL:onListItemClick()", "ptSelCounter++ = " + Integer.toString(TcmAid.av) + ", ptSelCounterArrary.count = " + Integer.toString(TcmAid.ax.size()));
                }
            }
            this.d = getListView().onSaveInstanceState();
            startActivityForResult(new Intent(this, PointsDetail.class), 1350);
        }
    }

    /* access modifiers changed from: private */
    public SQLiteCursor d() {
        String str;
        boolean z2;
        String str2;
        try {
            if (dh.n) {
                Log.d("PL:createCursor()", "Inserting into t_points");
            }
            if (TcmAid.ao != null) {
                this.u.execSQL("DELETE FROM t_points");
                this.u.execSQL("INSERT INTO t_points (_id, abbr_name, pointnum, name, englishname, indication) " + TcmAid.ao);
            } else {
                if (TcmAid.ar == null) {
                    n nVar = this.v;
                    boolean z3 = nVar.z();
                    String r2 = nVar.r();
                    boolean B2 = nVar.B();
                    if (!z3 || r2.length() <= 0) {
                        str = null;
                        z2 = false;
                    } else {
                        str = q.a("pointslocation", "_id", r2);
                        z2 = str.length() > 0;
                    }
                    if (z2) {
                        str2 = "SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian " + " WHERE (main" + str + ") " + " UNION " + "SELECT userDB.pointslocation._id, main.meridian.abbr_name, userDB.pointslocation.pointnum, userDB.pointslocation.name, userDB.pointslocation.englishname, userDB.pointslocation.indication FROM userDB.pointslocation LEFT JOIN main.meridian ON main.meridian._id = userDB.pointslocation.meridian " + " WHERE (userDB" + str + ") " + (B2 ? "" : " ORDER BY 2, 3");
                    } else {
                        str2 = "SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian " + " UNION " + "SELECT userDB.pointslocation._id, main.meridian.abbr_name, userDB.pointslocation.pointnum, userDB.pointslocation.name, userDB.pointslocation.englishname, userDB.pointslocation.indication FROM userDB.pointslocation LEFT JOIN main.meridian ON main.meridian._id = userDB.pointslocation.meridian " + " WHERE userDB.pointslocation._id>1000" + (B2 ? "" : " ORDER BY 2, 3");
                    }
                    TcmAid.ar = str2;
                }
                this.u.execSQL("DELETE FROM t_points");
                this.u.execSQL("INSERT INTO t_points (_id, abbr_name, pointnum, name, englishname, indication) " + TcmAid.ar);
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        if (TcmAid.ap != null) {
            this.k = TcmAid.ap;
            TcmAid.ap = null;
            this.l = TcmAid.aq;
            TcmAid.aq = null;
        }
        try {
            String[] strArr = {"_id", g, h, "name", "englishname"};
            if (this.v.B()) {
                this.z = this.f739a;
            }
            this.t = (SQLiteCursor) this.u.query(r, "t_points", strArr, this.k, this.l, m, n, this.z ? null : o, null);
            startManagingCursor(this.t);
            int count = this.t.getCount();
            if (dh.n) {
                Log.d("PL:createCursor()", "query count = " + Integer.toString(count));
            }
            TcmAid.al.clear();
            if (this.t.moveToFirst()) {
                do {
                    TcmAid.al.add(Integer.valueOf(this.t.getInt(0)));
                } while (this.t.moveToNext());
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        return this.t;
    }

    /* access modifiers changed from: private */
    public ListAdapter a(boolean z2) {
        int i2;
        int[] iArr;
        String[] strArr;
        if (z2) {
            int[] iArr2 = {C0000R.id.text0, C0000R.id.pointName};
            i2 = C0000R.layout.list_point_row_sans_dash;
            iArr = iArr2;
            strArr = new String[]{"_id", "name"};
        } else {
            String[] strArr2 = {"_id", g, h, "name"};
            int[] iArr3 = {C0000R.id.text0, C0000R.id.pointAbbr, C0000R.id.pointNumber, C0000R.id.pointName};
            i2 = C0000R.layout.list_point_row;
            iArr = iArr3;
            strArr = strArr2;
        }
        return new ah(this, i2, this.t, strArr, iArr, z2, this.A, this.B, this.C, this.u);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }

    private void e() {
        if (this.u == null || !this.u.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("PL:openDataBase()", str + " opened.");
                this.u = SQLiteDatabase.openDatabase(str, null, 16);
                this.u.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.u.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("PL:openDataBase()", str2 + " ATTACHed.");
                this.v = new n();
                this.v.a(this.u);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new be(this));
                create.show();
            }
        }
    }
}
