package com.cyberandsons.tcmaidtrial.lists;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.SimpleCursorAdapter;

final class ah extends SimpleCursorAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f760a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f761b = false;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public SQLiteDatabase e;

    public ah(Context context, int i, Cursor cursor, String[] strArr, int[] iArr, boolean z, boolean z2, boolean z3, int i2, SQLiteDatabase sQLiteDatabase) {
        super(context, i, cursor, strArr, iArr);
        this.f760a = z;
        this.f761b = z2;
        this.c = z3;
        this.d = i2;
        this.e = sQLiteDatabase;
        setViewBinder(new ag(this));
    }
}
