package com.cyberandsons.tcmaidtrial.lists;

import android.content.DialogInterface;

final class d implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FormulaCategoryList f815a;

    d(FormulaCategoryList formulaCategoryList) {
        this.f815a = formulaCategoryList;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        FormulaCategoryList.a(this.f815a, this.f815a.d.getText().toString(), "", this.f815a.c.toString());
    }
}
