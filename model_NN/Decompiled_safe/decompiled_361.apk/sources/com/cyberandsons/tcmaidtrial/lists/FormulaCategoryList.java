package com.cyberandsons.tcmaidtrial.lists;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.c;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.misc.dh;
import java.util.ArrayList;
import java.util.Iterator;

public class FormulaCategoryList extends ListActivity {
    private static String j;
    private static String k;
    private static String n = "SELECT main.fcategory._id, main.fcategory.item_name FROM main.fcategory ORDER BY main.fcategory.item_name";
    private static boolean o;

    /* renamed from: a  reason: collision with root package name */
    protected ArrayList f731a;

    /* renamed from: b  reason: collision with root package name */
    CharSequence[] f732b;
    StringBuilder c;
    EditText d;
    private final String e = "tc_formulas";
    private final String f = "_id";
    private final String g = "fcategory";
    private String h;
    private String[] i;
    private final String l = "fcategory";
    private final String m = "fcategory";
    private boolean p;
    private SQLiteCursor q;
    private SQLiteDatabase r;
    private n s;
    private boolean t = true;
    private boolean u;
    private ImageButton v;

    public FormulaCategoryList() {
        this.u = !this.t;
        this.f731a = new ArrayList();
    }

    static /* synthetic */ void a(FormulaCategoryList formulaCategoryList, String str, String str2, String str3) {
        boolean z;
        int i2;
        StringBuilder sb = new StringBuilder();
        String[] split = str3.split(",");
        boolean z2 = formulaCategoryList.t;
        boolean z3 = z2;
        for (String trim : split) {
            int a2 = c.a(trim.trim(), formulaCategoryList.s.N(), formulaCategoryList.r);
            if (z3) {
                sb.append(Integer.toString(a2));
                z3 = formulaCategoryList.u;
            } else {
                sb.append(',' + Integer.toString(a2));
            }
        }
        ContentValues contentValues = new ContentValues();
        boolean z4 = formulaCategoryList.u;
        int a3 = q.a("SELECT _id, name, desc, formulas  FROM userDB.formulascategory WHERE ", "userDB.formulascategory.", "name", str, formulaCategoryList.r);
        if (a3 == dh.f946a.intValue()) {
            int a4 = q.a("SELECT MAX(userDB.formulascategory._id) FROM userDB.formulascategory ", formulaCategoryList.r) + 1;
            if (a4 < 1000) {
                a4 += 1000;
            }
            contentValues.put("_id", Integer.toString(a4));
            int i3 = a4;
            z = z4;
            i2 = i3;
        } else {
            contentValues.put("_id", Integer.toString(a3));
            int i4 = a3;
            z = formulaCategoryList.t;
            i2 = i4;
        }
        contentValues.put("name", str);
        contentValues.put("desc", str2);
        contentValues.put("formulas", sb.toString());
        if (!z) {
            try {
                formulaCategoryList.r.insert("userDB.formulascategory", null, contentValues);
            } catch (SQLException e2) {
                q.a(e2);
            }
        } else {
            formulaCategoryList.r.update("userDB.formulascategory", contentValues, "_id=" + Integer.toString(i2), null);
        }
        if (formulaCategoryList.q != null) {
            formulaCategoryList.q.close();
            formulaCategoryList.q = null;
        }
        formulaCategoryList.q = formulaCategoryList.d();
        formulaCategoryList.setListAdapter(formulaCategoryList.e());
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0101 A[SYNTHETIC, Splitter:B:29:0x0101] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x014b A[SYNTHETIC, Splitter:B:36:0x014b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r7) {
        /*
            r6 = this;
            r3 = 0
            super.onCreate(r7)
            r6.f()
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.d     // Catch:{ Exception -> 0x0105 }
            if (r0 == 0) goto L_0x000f
            r0 = 1
            r6.setRequestedOrientation(r0)     // Catch:{ Exception -> 0x0105 }
        L_0x000f:
            r0 = 2130903082(0x7f03002a, float:1.7412972E38)
            r6.setContentView(r0)     // Catch:{ Exception -> 0x0105 }
            r0 = 2131362326(0x7f0a0216, float:1.834443E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0105 }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x0105 }
            java.lang.String r1 = "Formula Categories"
            r0.setText(r1)     // Catch:{ Exception -> 0x0105 }
            r0 = 2131362327(0x7f0a0217, float:1.8344431E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0105 }
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0     // Catch:{ Exception -> 0x0105 }
            r6.v = r0     // Catch:{ Exception -> 0x0105 }
            android.widget.ImageButton r0 = r6.v     // Catch:{ Exception -> 0x0105 }
            com.cyberandsons.tcmaidtrial.lists.f r1 = new com.cyberandsons.tcmaidtrial.lists.f     // Catch:{ Exception -> 0x0105 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0105 }
            r0.setOnClickListener(r1)     // Catch:{ Exception -> 0x0105 }
            boolean r0 = r6.p     // Catch:{ Exception -> 0x0105 }
            if (r0 == 0) goto L_0x00d0
            android.database.sqlite.SQLiteDatabase r0 = r6.r     // Catch:{ SQLException -> 0x00fa, all -> 0x0147 }
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.lists.FormulaCategoryList.n     // Catch:{ SQLException -> 0x00fa, all -> 0x0147 }
            r2 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r2)     // Catch:{ SQLException -> 0x00fa, all -> 0x0147 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x00fa, all -> 0x0147 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            boolean r2 = com.cyberandsons.tcmaidtrial.misc.dh.w     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            if (r2 == 0) goto L_0x0091
            java.lang.String r2 = "FCL:doRawCheck()"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            r3.<init>()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r4 = "query count = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            android.util.Log.d(r2, r1)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r1 = "FCL:doRawCheck()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            r2.<init>()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r3 = "column count = '"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            int r3 = r0.getColumnCount()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r3 = "' "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            android.util.Log.d(r1, r2)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
        L_0x0091:
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            if (r1 == 0) goto L_0x00cb
        L_0x0097:
            r1 = 1
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            r2 = 0
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            boolean r3 = com.cyberandsons.tcmaidtrial.misc.dh.w     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            if (r3 == 0) goto L_0x00c5
            java.lang.String r3 = "FCL:doRawCheck()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            r4.<init>()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r4 = " - "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            android.util.Log.d(r3, r1)     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
        L_0x00c5:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0156, all -> 0x014f }
            if (r1 != 0) goto L_0x0097
        L_0x00cb:
            if (r0 == 0) goto L_0x00d0
            r0.close()     // Catch:{ Exception -> 0x0105 }
        L_0x00d0:
            android.database.sqlite.SQLiteCursor r0 = r6.d()     // Catch:{ Exception -> 0x0105 }
            r6.q = r0     // Catch:{ Exception -> 0x0105 }
            android.widget.ListAdapter r0 = r6.e()     // Catch:{ Exception -> 0x0105 }
            r6.setListAdapter(r0)     // Catch:{ Exception -> 0x0105 }
            r6.c()     // Catch:{ Exception -> 0x0105 }
            r0 = 2131361994(0x7f0a00ca, float:1.8343756E38)
            android.view.View r0 = r6.findViewById(r0)     // Catch:{ Exception -> 0x0105 }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ Exception -> 0x0105 }
            java.lang.String r1 = ""
            r0.setText(r1)     // Catch:{ Exception -> 0x0105 }
            android.widget.ListView r1 = r6.getListView()     // Catch:{ Exception -> 0x0105 }
            r2 = 1
            r1.setFastScrollEnabled(r2)     // Catch:{ Exception -> 0x0105 }
            r1.setEmptyView(r0)     // Catch:{ Exception -> 0x0105 }
        L_0x00f9:
            return
        L_0x00fa:
            r0 = move-exception
            r1 = r3
        L_0x00fc:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0154 }
            if (r1 == 0) goto L_0x00d0
            r1.close()     // Catch:{ Exception -> 0x0105 }
            goto L_0x00d0
        L_0x0105:
            r0 = move-exception
            org.acra.ErrorReporter r1 = org.acra.ErrorReporter.a()
            java.lang.String r2 = "myVariable"
            java.lang.String r0 = r0.getLocalizedMessage()
            r1.a(r2, r0)
            org.acra.ErrorReporter r0 = org.acra.ErrorReporter.a()
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r2 = "FCL:onCreate()"
            r1.<init>(r2)
            r0.handleSilentException(r1)
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r6)
            android.app.AlertDialog r0 = r0.create()
            java.lang.String r1 = "Attention:"
            r0.setTitle(r1)
            r1 = 2131099670(0x7f060016, float:1.78117E38)
            java.lang.String r1 = r6.getString(r1)
            r0.setMessage(r1)
            java.lang.String r1 = "OK"
            com.cyberandsons.tcmaidtrial.lists.e r2 = new com.cyberandsons.tcmaidtrial.lists.e
            r2.<init>(r6)
            r0.setButton(r1, r2)
            r0.show()
            goto L_0x00f9
        L_0x0147:
            r0 = move-exception
            r1 = r3
        L_0x0149:
            if (r1 == 0) goto L_0x014e
            r1.close()     // Catch:{ Exception -> 0x0105 }
        L_0x014e:
            throw r0     // Catch:{ Exception -> 0x0105 }
        L_0x014f:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0149
        L_0x0154:
            r0 = move-exception
            goto L_0x0149
        L_0x0156:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x00fc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.FormulaCategoryList.onCreate(android.os.Bundle):void");
    }

    public void onDestroy() {
        this.f731a.clear();
        TcmAid.Q = null;
        TcmAid.P = this.u;
        TcmAid.S = null;
        TcmAid.T = null;
        try {
            if (this.r != null && this.r.isOpen()) {
                this.r.close();
                this.r = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        f();
        if (TcmAid.cC) {
            startActivity(getIntent());
            finish();
            TcmAid.cC = false;
        }
        super.onResume();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0058, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0059, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005f, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0064, code lost:
        r2 = "";
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0096, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x009a, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x009b, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0058 A[ExcHandler: SQLException (e android.database.SQLException), Splitter:B:1:0x0006] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x009a A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0006] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:49:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c() {
        /*
            r8 = this;
            r6 = 0
            r5 = 1
            java.lang.String r0 = ""
            java.lang.String r1 = ""
            com.cyberandsons.tcmaidtrial.a.n r2 = r8.s     // Catch:{ SQLException -> 0x0058, NullPointerException -> 0x0063, all -> 0x009a }
            int r2 = r2.N()     // Catch:{ SQLException -> 0x0058, NullPointerException -> 0x0063, all -> 0x009a }
            int r3 = com.cyberandsons.tcmaidtrial.TcmAid.cV     // Catch:{ SQLException -> 0x0058, NullPointerException -> 0x0063, all -> 0x009a }
            r4 = -1
            if (r3 == r4) goto L_0x0013
            int r2 = com.cyberandsons.tcmaidtrial.TcmAid.cV     // Catch:{ SQLException -> 0x0058, NullPointerException -> 0x0063, all -> 0x009a }
        L_0x0013:
            if (r2 == 0) goto L_0x0017
            if (r2 != r5) goto L_0x0055
        L_0x0017:
            java.lang.String r2 = "pinyin"
        L_0x0019:
            java.lang.String r3 = ""
            java.lang.String r2 = com.cyberandsons.tcmaidtrial.a.c.a(r3, r2)     // Catch:{ SQLException -> 0x0058, NullPointerException -> 0x0063, all -> 0x009a }
            java.lang.String r1 = "Pre-rawQuery"
            android.database.sqlite.SQLiteDatabase r0 = r8.r     // Catch:{ SQLException -> 0x0058, NullPointerException -> 0x00a9, all -> 0x009a }
            r3 = 0
            android.database.Cursor r0 = r0.rawQuery(r2, r3)     // Catch:{ SQLException -> 0x0058, NullPointerException -> 0x00a9, all -> 0x009a }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0058, NullPointerException -> 0x00a9, all -> 0x009a }
            int r3 = r0.getCount()     // Catch:{ SQLException -> 0x00ae, NullPointerException -> 0x00ac }
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLException -> 0x00ae, NullPointerException -> 0x00ac }
            r8.f732b = r3     // Catch:{ SQLException -> 0x00ae, NullPointerException -> 0x00ac }
            java.lang.String r1 = "Pre-cursor loop"
            r3 = 0
            boolean r4 = r0.moveToFirst()     // Catch:{ SQLException -> 0x00ae, NullPointerException -> 0x00ac }
            if (r4 == 0) goto L_0x004c
        L_0x003b:
            java.lang.CharSequence[] r4 = r8.f732b     // Catch:{ SQLException -> 0x00ae, NullPointerException -> 0x00ac }
            int r5 = r3 + 1
            r6 = 1
            java.lang.String r6 = r0.getString(r6)     // Catch:{ SQLException -> 0x00ae, NullPointerException -> 0x00ac }
            r4[r3] = r6     // Catch:{ SQLException -> 0x00ae, NullPointerException -> 0x00ac }
            boolean r3 = r0.moveToNext()     // Catch:{ SQLException -> 0x00ae, NullPointerException -> 0x00ac }
            if (r3 != 0) goto L_0x00b3
        L_0x004c:
            r0.close()     // Catch:{ SQLException -> 0x00ae, NullPointerException -> 0x00ac }
            if (r0 == 0) goto L_0x0054
            r0.close()
        L_0x0054:
            return
        L_0x0055:
            java.lang.String r2 = "english"
            goto L_0x0019
        L_0x0058:
            r0 = move-exception
            r1 = r6
        L_0x005a:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x00a7 }
            if (r1 == 0) goto L_0x0054
            r1.close()
            goto L_0x0054
        L_0x0063:
            r2 = move-exception
            r2 = r0
            r0 = r6
        L_0x0066:
            org.acra.ErrorReporter r3 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x00a2 }
            java.lang.String r4 = "myVariable"
            r3.a(r4, r2)     // Catch:{ all -> 0x00a2 }
            org.acra.ErrorReporter r2 = org.acra.ErrorReporter.a()     // Catch:{ all -> 0x00a2 }
            java.lang.NullPointerException r3 = new java.lang.NullPointerException     // Catch:{ all -> 0x00a2 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a2 }
            r4.<init>()     // Catch:{ all -> 0x00a2 }
            java.lang.String r5 = "FCL:loadControlResources( last step completed = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00a2 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x00a2 }
            java.lang.String r4 = " )"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x00a2 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00a2 }
            r3.<init>(r1)     // Catch:{ all -> 0x00a2 }
            r2.handleSilentException(r3)     // Catch:{ all -> 0x00a2 }
            if (r0 == 0) goto L_0x0054
            r0.close()
            goto L_0x0054
        L_0x009a:
            r0 = move-exception
            r1 = r6
        L_0x009c:
            if (r1 == 0) goto L_0x00a1
            r1.close()
        L_0x00a1:
            throw r0
        L_0x00a2:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x009c
        L_0x00a7:
            r0 = move-exception
            goto L_0x009c
        L_0x00a9:
            r0 = move-exception
            r0 = r6
            goto L_0x0066
        L_0x00ac:
            r3 = move-exception
            goto L_0x0066
        L_0x00ae:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x005a
        L_0x00b3:
            r3 = r5
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.lists.FormulaCategoryList.c():void");
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        String str;
        if (i2 >= 0) {
            super.onListItemClick(listView, view, i2, j2);
            if (dh.w) {
                Log.d("FCL:onListItemClick()", "position = " + Integer.toString(i2) + ", id = " + Long.toString(j2));
            }
            TcmAid.R = new String[]{Integer.toString((int) j2)};
            TcmAid.P = this.t;
            TcmAid.cC = true;
            if (j2 > 1000) {
                int N = this.s.N();
                if (TcmAid.cV != -1) {
                    N = TcmAid.cV;
                }
                switch (N) {
                    case 1:
                        str = "" + "english";
                        break;
                    default:
                        str = "" + "pinyin";
                        break;
                }
                TcmAid.S = c.a(str, this.r, (int) j2);
            } else {
                TcmAid.Q = "fcategory=?";
                TcmAid.S = null;
            }
            startActivityForResult(new Intent(this, FormulasList.class), 1350);
        }
    }

    private SQLiteCursor d() {
        try {
            if (dh.w) {
                Log.d("FL:createCursor()", "Inserting into tc_formulas");
            }
            if (TcmAid.T == null) {
                String str = "";
                if (this.s.ah()) {
                    str = " AND main.formulas.req_state_board=1 ";
                } else if (this.s.af()) {
                    str = " AND main.formulas.nccaom_req=1 ";
                }
                TcmAid.T = String.format("SELECT DISTINCT main.fcategory._id, main.fcategory.item_name FROM main.formulas LEFT JOIN main.fcategory ON main.fcategory._id = main.formulas.fcategory WHERE main.formulas.useiPhone=1 AND main.fcategory.item_name NOT LIKE '-- %%' %s UNION SELECT userDB.formulascategory._id, userDB.formulascategory.name FROM userDB.formulascategory ORDER BY 2 ", str);
            }
            this.r.execSQL(c.a("tc_formulas"));
            this.r.execSQL("INSERT INTO " + "tc_formulas" + " (_id, fcategory) " + TcmAid.T);
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            this.q = (SQLiteCursor) this.r.query(o, "tc_formulas", new String[]{"_id", "fcategory"}, this.h, this.i, j, k, "fcategory", null);
            startManagingCursor(this.q);
            int count = this.q.getCount();
            if (dh.w) {
                Log.d("FCL:createCursor()", "query count = " + Integer.toString(count));
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        return this.q;
    }

    private ListAdapter e() {
        return new af(this, this.q, new String[]{"_id", "fcategory"}, new int[]{C0000R.id.text0, C0000R.id.text1}, "fcategory");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }

    private void f() {
        if (this.r == null || !this.r.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("FCL:openDataBase()", str + " opened.");
                this.r = SQLiteDatabase.openDatabase(str, null, 16);
                this.r.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.r.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("FCL:openDataBase()", str2 + " ATTACHed.");
                this.s = new n();
                this.s.a(this.r);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new h(this));
                create.show();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.c = new StringBuilder();
        boolean z = this.t;
        Iterator it = this.f731a.iterator();
        boolean z2 = z;
        while (it.hasNext()) {
            CharSequence charSequence = (CharSequence) it.next();
            if (z2) {
                this.c.append(charSequence);
                z2 = this.u;
            } else {
                this.c.append(',' + charSequence.toString());
            }
        }
    }

    public final void b() {
        this.f731a.clear();
        g gVar = new g(this);
        this.d = new EditText(this);
        this.d.setHint("Enter Formula Category Name");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Formulas");
        builder.setMultiChoiceItems(this.f732b, new boolean[this.f732b.length], gVar);
        builder.setView(this.d);
        AlertDialog create = builder.create();
        create.setOnCancelListener(new d(this));
        create.show();
    }
}
