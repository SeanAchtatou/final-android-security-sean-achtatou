package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.FormulasList;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class bu implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MateriaMedicaArea f382a;

    bu(MateriaMedicaArea materiaMedicaArea) {
        this.f382a = materiaMedicaArea;
    }

    public final void onClick(View view) {
        MateriaMedicaArea materiaMedicaArea = this.f382a;
        TcmAid.Q = null;
        if (TcmAid.S != null) {
            TcmAid.W++;
            TcmAid.X.add(TcmAid.S);
            if (dh.T) {
                Log.d("MMA:MateriaMedicaFormulasList:", "fxCounter++ = " + Integer.toString(TcmAid.W) + ", fxCounterArrary.count = " + Integer.toString(TcmAid.X.size()));
            }
        }
        materiaMedicaArea.startActivityForResult(new Intent(materiaMedicaArea, FormulasList.class), 1350);
    }
}
