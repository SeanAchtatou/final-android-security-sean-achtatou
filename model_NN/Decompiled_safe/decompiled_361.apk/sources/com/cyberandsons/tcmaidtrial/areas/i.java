package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.PointsList;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class i implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SearchArea f401a;

    i(SearchArea searchArea) {
        this.f401a = searchArea;
    }

    public final void onClick(View view) {
        SearchArea searchArea = this.f401a;
        if (TcmAid.ar == null) {
            if (TcmAid.au > 0) {
                TcmAid.au--;
                TcmAid.ar = (String) TcmAid.aw.get(TcmAid.au);
                TcmAid.aw.remove(TcmAid.au);
                if (dh.W) {
                    Log.d("SA:pointsButton_Click()", "ptCounter-- = " + Integer.toString(TcmAid.au) + ", ptCounterArrary.count = " + Integer.toString(TcmAid.aw.size()));
                }
            }
            if (dh.ae) {
                Log.d("SA:pointsButton_Click()", TcmAid.ar);
            }
        }
        TcmAid.ap = null;
        TcmAid.cP = "Results for Points";
        searchArea.startActivityForResult(new Intent(searchArea, PointsList.class), 1350);
    }
}
