package com.cyberandsons.tcmaidtrial.areas;

import android.content.DialogInterface;

final class bz implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MateriaMedicaArea f387a;

    bz(MateriaMedicaArea materiaMedicaArea) {
        this.f387a = materiaMedicaArea;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f387a.setResult(2);
        this.f387a.finish();
    }
}
