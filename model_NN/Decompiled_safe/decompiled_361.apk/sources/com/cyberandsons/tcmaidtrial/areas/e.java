package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.HerbList;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SearchArea f397a;

    e(SearchArea searchArea) {
        this.f397a = searchArea;
    }

    public final void onClick(View view) {
        SearchArea searchArea = this.f397a;
        if (TcmAid.af == null) {
            if (TcmAid.ai > 0) {
                TcmAid.ai--;
                TcmAid.af = (String) TcmAid.aj.get(TcmAid.ai);
                TcmAid.aj.remove(TcmAid.ai);
                if (dh.U) {
                    Log.d("SA:herbsButton_Click()", "hbCounter-- = " + Integer.toString(TcmAid.ai) + ", hbCounterArrary.count = " + Integer.toString(TcmAid.aj.size()));
                }
            }
            if (dh.ad) {
                Log.d("SA:herbsButton_Click()", TcmAid.af);
            }
        }
        TcmAid.ad = null;
        TcmAid.cP = "Results for Herbs";
        searchArea.startActivityForResult(new Intent(searchArea, HerbList.class), 1350);
    }
}
