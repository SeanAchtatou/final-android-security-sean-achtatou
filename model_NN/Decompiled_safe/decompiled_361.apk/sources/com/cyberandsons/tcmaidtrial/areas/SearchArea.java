package com.cyberandsons.tcmaidtrial.areas;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.Button;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.misc.dh;
import org.acra.ErrorReporter;

public class SearchArea extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Button f333a;

    /* renamed from: b  reason: collision with root package name */
    private Button f334b;
    private Button c;
    private Button d;
    private Button e;
    private Button f;
    private Button g;
    private Button h;
    private Button i;
    private Button j;
    private Button k;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            if (dh.d) {
                setRequestedOrientation(1);
            }
            setContentView((int) C0000R.layout.searchresults);
            TextView textView = (TextView) findViewById(C0000R.id.tc_label);
            String str = TcmAid.cP;
            textView.setText(str);
            Paint paint = new Paint();
            paint.setTextSize(textView.getTextSize());
            paint.setAntiAlias(true);
            try {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                textView.setPadding((displayMetrics.widthPixels / 2) - (((int) (paint.measureText(str) + 0.5f)) / 2), 10, 0, 10);
            } catch (Exception e2) {
                ErrorReporter.a().a("myVariable", e2.getLocalizedMessage());
                ErrorReporter.a().handleSilentException(new Exception("SA:onCreate() - No Crash exception on DisplayMetrics"));
            }
            this.f333a = (Button) findViewById(C0000R.id.auricularResults);
            if (TcmAid.i.size() == 0) {
                this.f333a.setVisibility(8);
            } else {
                this.f333a.setText(dh.a("Auricular", TcmAid.i.size(), this.f333a.getTextSize()));
                this.f333a.setOnClickListener(new c(this));
            }
            this.f334b = (Button) findViewById(C0000R.id.zangFuResults);
            if (TcmAid.w.size() == 0) {
                this.f334b.setVisibility(8);
            } else {
                this.f334b.setText(dh.a("Patterns by Internal Organs", TcmAid.w.size(), this.f334b.getTextSize()));
                this.f334b.setOnClickListener(new d(this));
            }
            this.c = (Button) findViewById(C0000R.id.fourLevelsResults);
            if (TcmAid.x.size() == 0) {
                this.c.setVisibility(8);
            } else {
                this.c.setText(dh.a("Four Levels", TcmAid.x.size(), this.c.getTextSize()));
                this.c.setOnClickListener(new a(this));
            }
            this.d = (Button) findViewById(C0000R.id.resPathResults);
            if (TcmAid.y.size() == 0) {
                this.d.setVisibility(8);
            } else {
                this.d.setText(dh.a("Residual Pathogenic", TcmAid.y.size(), this.d.getTextSize()));
                this.d.setOnClickListener(new b(this));
            }
            this.e = (Button) findViewById(C0000R.id.sixStagesResults);
            if (TcmAid.aL.size() == 0) {
                this.e.setVisibility(8);
            } else {
                this.e.setText(dh.a("Six Stages", TcmAid.aL.size(), this.e.getTextSize()));
                this.e.setOnClickListener(new g(this));
            }
            this.f = (Button) findViewById(C0000R.id.pulseDiagResults);
            if (TcmAid.az.size() == 0) {
                this.f.setVisibility(8);
            } else {
                this.f.setText(dh.a("Pulse Diagnosis", TcmAid.az.size(), this.f.getTextSize()));
                this.f.setOnClickListener(new h(this));
            }
            this.g = (Button) findViewById(C0000R.id.herbResults);
            if (TcmAid.Z.size() == 0) {
                this.g.setVisibility(8);
            } else {
                this.g.setText(dh.a("Herbs", TcmAid.Z.size(), this.g.getTextSize()));
                this.g.setOnClickListener(new e(this));
            }
            this.h = (Button) findViewById(C0000R.id.formulasResults);
            if (TcmAid.M.size() == 0) {
                this.h.setVisibility(8);
            } else {
                this.h.setText(dh.a("Formulas", TcmAid.M.size(), this.h.getTextSize()));
                this.h.setOnClickListener(new f(this));
            }
            this.i = (Button) findViewById(C0000R.id.pointsResults);
            if (TcmAid.al.size() == 0) {
                this.i.setVisibility(8);
            } else {
                this.i.setText(dh.a("Points", TcmAid.al.size(), this.i.getTextSize()));
                this.i.setOnClickListener(new i(this));
            }
            this.j = (Button) findViewById(C0000R.id.tungResults);
            if (TcmAid.aU.size() == 0) {
                this.j.setVisibility(8);
            } else {
                this.j.setText(dh.a("Tung", TcmAid.aU.size(), this.j.getTextSize()));
                this.j.setOnClickListener(new n(this));
            }
            this.k = (Button) findViewById(C0000R.id.wristAnkleResults);
            if (TcmAid.be.size() == 0) {
                this.k.setVisibility(8);
            } else {
                this.k.setText(dh.a("WristAnkle", TcmAid.be.size(), this.k.getTextSize()));
                this.k.setOnClickListener(new m(this));
            }
            if (TcmAid.cR == -1) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("There were no search fields defined. Check your search fields in Settings page.");
                create.setButton("OK", new p(this));
                create.show();
            }
        } catch (Exception e3) {
            ErrorReporter.a().a("myVariable", e3.getLocalizedMessage());
            ErrorReporter.a().handleSilentException(new Exception("SA:onCreate()"));
            AlertDialog create2 = new AlertDialog.Builder(this).create();
            create2.setTitle("Attention:");
            create2.setMessage(getString(C0000R.string.area_list_exception));
            create2.setButton("OK", new o(this));
            create2.show();
        }
    }

    public void onDestroy() {
        TcmAid.cR = 0;
        TcmAid.E = null;
        TcmAid.bh = null;
        TcmAid.aY = null;
        TcmAid.aP = null;
        TcmAid.aF = null;
        TcmAid.ar = null;
        TcmAid.S = null;
        TcmAid.af = null;
        TcmAid.o = null;
        TcmAid.cP = null;
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1350 && i3 == 2) {
            setResult(2);
            finish();
        }
    }
}
