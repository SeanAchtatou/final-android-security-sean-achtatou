package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.AuricularList;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class x implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointsArea f417a;

    x(PointsArea pointsArea) {
        this.f417a = pointsArea;
    }

    public final void onClick(View view) {
        PointsArea pointsArea = this.f417a;
        if (TcmAid.o != null) {
            TcmAid.s++;
            TcmAid.t.add(TcmAid.o);
            if (dh.R) {
                Log.d("PA:PointAreaAuricularList:", "auCounter++ = " + Integer.toString(TcmAid.s) + ", auCounterArrary.count = " + Integer.toString(TcmAid.t.size()));
            }
        }
        pointsArea.startActivityForResult(new Intent(pointsArea, AuricularList.class), 1350);
    }
}
