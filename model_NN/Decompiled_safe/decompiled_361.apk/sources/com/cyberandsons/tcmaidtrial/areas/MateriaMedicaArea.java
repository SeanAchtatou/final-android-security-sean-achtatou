package com.cyberandsons.tcmaidtrial.areas;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.misc.dh;
import org.acra.ErrorReporter;

public class MateriaMedicaArea extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Button f327a;

    /* renamed from: b  reason: collision with root package name */
    private Button f328b;
    private Button c;
    private Button d;
    private SQLiteDatabase e;
    private n f;

    public void onCreate(Bundle bundle) {
        String str;
        String str2;
        boolean z;
        String str3;
        String str4;
        String str5;
        boolean z2;
        String str6;
        super.onCreate(bundle);
        a();
        try {
            if (dh.d) {
                setRequestedOrientation(1);
            }
            setContentView((int) C0000R.layout.materiamedica);
            TextView textView = (TextView) findViewById(C0000R.id.tc_label);
            textView.setText("Materia Medica");
            Paint paint = new Paint();
            paint.setTextSize(textView.getTextSize());
            paint.setAntiAlias(true);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            textView.setPadding((displayMetrics.widthPixels / 2) - (((int) (paint.measureText("Materia Medica") + 0.5f)) / 2), 10, 0, 10);
            if (this.f.ag()) {
                str = " WHERE main.materiamedica.req_state_board=1 ";
            } else if (this.f.ae()) {
                str = " WHERE main.materiamedica.nccaom_req=1 ";
            } else {
                str = "";
            }
            this.f327a = (Button) findViewById(C0000R.id.herbCatButton);
            this.f327a.setText(dh.a("Herb Categories", String.format("SELECT COUNT(DISTINCT main.mmcategory.item_name) FROM main.materiamedica LEFT JOIN main.mmcategory ON main.mmcategory._id = main.materiamedica.mmcategory %s", str), "SELECT COUNT(userDB.herbcategory._id) FROM userDB.herbcategory ", this.f327a.getTextSize(), this.e));
            this.f327a.setOnClickListener(new bx(this));
            this.f328b = (Button) findViewById(C0000R.id.herbsButton);
            Button button = this.f328b;
            n nVar = this.f;
            boolean z3 = nVar.z();
            String q = nVar.q();
            if (!z3 || q.length() <= 0) {
                str2 = null;
                z = false;
            } else {
                str2 = q.a("materiamedica", "_id", q);
                z = str2.length() > 0;
            }
            if (z) {
                str3 = "SELECT COUNT(main.materiamedica._id) FROM main.materiamedica" + " WHERE (main" + str2 + ") " + " UNION " + "SELECT COUNT(userDB.materiamedica._id) FROM userDB.materiamedica" + " WHERE (userDB" + str2 + ") " + " AND userDB.materiamedica._id>1000 ";
            } else {
                str3 = "SELECT COUNT(main.materiamedica._id) FROM main.materiamedica" + " " + str + " UNION " + "SELECT COUNT(userDB.materiamedica._id) FROM userDB.materiamedica" + " WHERE userDB.materiamedica._id>1000";
            }
            button.setText(dh.a("Individual Herbs", str3, this.f328b.getTextSize(), this.e));
            this.f328b.setOnClickListener(new bw(this));
            if (this.f.ah()) {
                str4 = " AND main.formulas.req_state_board=1 ";
            } else if (this.f.af()) {
                str4 = " AND main.formulas.nccaom_req=1 ";
            } else {
                str4 = "";
            }
            this.c = (Button) findViewById(C0000R.id.formulaCatButton);
            this.c.setText(dh.a("Formula Categories", "SELECT COUNT(DISTINCT main.fcategory.item_name) FROM main.formulas LEFT JOIN main.fcategory ON main.fcategory._id = main.formulas.fcategory WHERE main.formulas.useiPhone=1 AND main.fcategory.item_name NOT LIKE '-- %' " + str4, "SELECT COUNT(userDB.formulascategory._id) FROM userDB.formulascategory ", this.c.getTextSize(), this.e));
            this.c.setOnClickListener(new bv(this));
            this.d = (Button) findViewById(C0000R.id.formulasButton);
            Button button2 = this.d;
            n nVar2 = this.f;
            boolean z4 = nVar2.z();
            String p = nVar2.p();
            if (!z4 || p.length() <= 0) {
                str5 = null;
                z2 = false;
            } else {
                str5 = q.a("formulas", "_id", p);
                z2 = str5.length() > 0;
            }
            if (z2) {
                str6 = "SELECT COUNT(main.formulas._id) FROM main.formulas " + " WHERE main.formulas.useiPhone=1 AND (main" + str5 + ") " + " UNION " + "SELECT COUNT(userDB.formulas._id) FROM userDB.formulas " + " WHERE (userDB" + str5 + ") " + " AND userDB.formulas._id>1000 ";
            } else {
                str6 = "SELECT COUNT(main.formulas._id) FROM main.formulas " + " WHERE main.formulas.useiPhone=1 " + str4 + " UNION " + "SELECT COUNT(userDB.formulas._id) FROM userDB.formulas " + " WHERE userDB.formulas._id>1000 ";
            }
            button2.setText(dh.a("Individual Formulas", str6, this.d.getTextSize(), this.e));
            this.d.setOnClickListener(new bu(this));
            TcmAid.af = null;
            TcmAid.ag = null;
            TcmAid.S = null;
            TcmAid.T = null;
        } catch (Exception e2) {
            ErrorReporter.a().a("myVariable", e2.getLocalizedMessage());
            ErrorReporter.a().handleSilentException(new Exception("MMA:onCreate()"));
            AlertDialog create = new AlertDialog.Builder(this).create();
            create.setTitle("Attention:");
            create.setMessage(getString(C0000R.string.area_list_exception));
            create.setButton("OK", new bz(this));
            create.show();
        }
    }

    public void onDestroy() {
        try {
            if (this.e != null && this.e.isOpen()) {
                this.e.close();
                this.e = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    public void onResume() {
        a();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1350 && i2 == 2) {
            setResult(2);
            finish();
        }
    }

    private void a() {
        if (this.e == null || !this.e.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("MMA:openDataBase()", str + " opened.");
                this.e = SQLiteDatabase.openDatabase(str, null, 16);
                this.e.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.e.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("MMA:openDataBase()", str2 + " ATTACHed.");
                this.f = new n();
                this.f.a(this.e);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new by(this));
                create.show();
            }
        }
    }
}
