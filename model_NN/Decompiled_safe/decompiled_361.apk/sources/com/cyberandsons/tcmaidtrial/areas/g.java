package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.SixStagesList;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class g implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SearchArea f399a;

    g(SearchArea searchArea) {
        this.f399a = searchArea;
    }

    public final void onClick(View view) {
        SearchArea searchArea = this.f399a;
        if (TcmAid.aP == null) {
            if (TcmAid.aR > 0) {
                TcmAid.aR--;
                TcmAid.aP = (String) TcmAid.aS.get(TcmAid.aR);
                TcmAid.aS.remove(TcmAid.aR);
                if (dh.X) {
                    Log.d("SA:sixStagesButton_Click()", "ssCounter-- = " + Integer.toString(TcmAid.aR) + ", ssCounterArrary.count = " + Integer.toString(TcmAid.aS.size()));
                }
            }
            if (dh.ag) {
                Log.d("SA:sixStagesButton_Click()", TcmAid.aP);
            }
        }
        TcmAid.aN = null;
        TcmAid.cP = "Results for Six Stages";
        searchArea.startActivityForResult(new Intent(searchArea, SixStagesList.class), 1350);
    }
}
