package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.WristAnkleList;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class u implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointsArea f414a;

    u(PointsArea pointsArea) {
        this.f414a = pointsArea;
    }

    public final void onClick(View view) {
        PointsArea pointsArea = this.f414a;
        if (TcmAid.bh != null) {
            TcmAid.bj++;
            TcmAid.bk.add(TcmAid.bh);
            if (dh.Z) {
                Log.d("PA:PointAreaWristAnkleList:", "waCounter++ = " + Integer.toString(TcmAid.bj) + ", waCounterArrary.count = " + Integer.toString(TcmAid.bk.size()));
            }
        }
        pointsArea.startActivityForResult(new Intent(pointsArea, WristAnkleList.class), 1350);
    }
}
