package com.cyberandsons.tcmaidtrial.areas;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.misc.dh;
import org.acra.ErrorReporter;

public class PointsArea extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Button f331a;

    /* renamed from: b  reason: collision with root package name */
    private Button f332b;
    private Button c;
    private Button d;
    private Button e;
    private Button f;
    private SQLiteDatabase g;
    private n h;

    public void onCreate(Bundle bundle) {
        String str;
        boolean z;
        String str2;
        boolean z2;
        String str3;
        super.onCreate(bundle);
        a();
        try {
            if (dh.d) {
                setRequestedOrientation(1);
            }
            setContentView((int) C0000R.layout.points);
            TextView textView = (TextView) findViewById(C0000R.id.tc_label);
            textView.setText("Detailed Points");
            Paint paint = new Paint();
            paint.setTextSize(textView.getTextSize());
            paint.setAntiAlias(true);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            textView.setPadding((displayMetrics.widthPixels / 2) - (((int) (paint.measureText("Detailed Points") + 0.5f)) / 2), 10, 0, 10);
            this.f331a = (Button) findViewById(C0000R.id.auricularCatButton);
            this.f331a.setText(dh.a("Auricular Categories", "SELECT COUNT(DISTINCT main.auricular.pointcat)+5 FROM main.auricular ", this.f331a.getTextSize(), this.g));
            this.f331a.setOnClickListener(new t(this));
            this.f332b = (Button) findViewById(C0000R.id.auricularButton);
            Button button = this.f332b;
            n nVar = this.h;
            boolean z3 = nVar.z();
            String n = nVar.n();
            if (!z3 || n.length() <= 0) {
                str = null;
                z = false;
            } else {
                str = q.a("auricular", "_id", n);
                z = str.length() > 0;
            }
            button.setText(dh.a("Individual Auricular", z ? "SELECT COUNT(main.auricular._id) FROM main.auricular " + " WHERE (main" + str + ") " + " UNION " + "SELECT COUNT(userDB.auricular._id) FROM userDB.auricular " + " WHERE (userDB" + str + ") " + " AND userDB.auricular._id>1000 " : "SELECT COUNT(main.auricular._id) FROM main.auricular " + " UNION " + "SELECT COUNT(userDB.auricular._id) FROM userDB.auricular " + " WHERE userDB.auricular._id>1000", this.f332b.getTextSize(), this.g));
            this.f332b.setOnClickListener(new x(this));
            this.d = (Button) findViewById(C0000R.id.pointsButton);
            Button button2 = this.d;
            n nVar2 = this.h;
            boolean z4 = nVar2.z();
            String r = nVar2.r();
            if (!z4 || r.length() <= 0) {
                str2 = null;
                z2 = false;
            } else {
                str2 = q.a("pointslocation", "_id", r);
                z2 = str2.length() > 0;
            }
            if (z2) {
                str3 = "SELECT COUNT(main.pointslocation._id) FROM main.pointslocation " + " WHERE (main" + str2 + ") " + " UNION " + "SELECT COUNT(userDB.pointslocation._id) FROM userDB.pointslocation " + " WHERE (userDB" + str2 + ") " + " AND userDB.pointslocation._id>1000 ";
            } else {
                str3 = "SELECT COUNT(main.pointslocation._id) FROM main.pointslocation " + " UNION " + "SELECT COUNT(userDB.pointslocation._id) FROM userDB.pointslocation " + " WHERE userDB.pointslocation._id>1000 ";
            }
            button2.setText(dh.a("Individual Points", str3, this.d.getTextSize(), this.g));
            this.d.setOnClickListener(new y(this));
            this.c = (Button) findViewById(C0000R.id.pointCatButton);
            this.c.setText(dh.a("Point Categories", q.a("SELECT COUNT(userDB.pointcategory._id) FROM userDB.pointcategory ", this.g) + 37, this.c.getTextSize()));
            this.c.setOnClickListener(new z(this));
            this.e = (Button) findViewById(C0000R.id.tungButton);
            this.e.setText(dh.a("Master Tung", "SELECT COUNT(main.mastertung._id) FROM main.mastertung" + " UNION " + "SELECT COUNT(userDB.mastertung._id) FROM userDB.mastertung" + " WHERE userDB.mastertung._id>1000", this.e.getTextSize(), this.g));
            this.e.setOnClickListener(new aa(this));
            this.f = (Button) findViewById(C0000R.id.wristAnkleButton);
            this.f.setText(dh.a("Wrist Ankle", "SELECT COUNT(main.wristankle._id) FROM main.wristankle " + " UNION " + "SELECT COUNT(userDB.wristankle._id) FROM userDB.wristankle " + " WHERE userDB.wristankle._id>1000", this.f.getTextSize(), this.g));
            this.f.setOnClickListener(new u(this));
        } catch (Exception e2) {
            ErrorReporter.a().a("myVariable", e2.getLocalizedMessage());
            ErrorReporter.a().handleSilentException(new Exception("PA:onCreate()"));
            AlertDialog create = new AlertDialog.Builder(this).create();
            create.setTitle("Attention:");
            create.setMessage(getString(C0000R.string.area_list_exception));
            create.setButton("OK", new v(this));
            create.show();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        a();
        if (TcmAid.cD) {
            startActivity(getIntent());
            finish();
            TcmAid.cD = false;
        }
        super.onResume();
    }

    public void onDestroy() {
        try {
            if (this.g != null && this.g.isOpen()) {
                this.g.close();
                this.g = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1350 && i2 == 2) {
            setResult(2);
            finish();
        }
    }

    private void a() {
        if (this.g == null || !this.g.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("PA:openDataBase()", str + " opened.");
                this.g = SQLiteDatabase.openDatabase(str, null, 16);
                this.g.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.g.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("PA:openDataBase()", str2 + " ATTACHed.");
                this.h = new n();
                this.h.a(this.g);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new w(this));
                create.show();
            }
        }
    }
}
