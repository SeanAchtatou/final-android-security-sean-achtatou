package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.view.View;
import com.cyberandsons.tcmaidtrial.lists.FormulaCategoryList;

final class bv implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MateriaMedicaArea f383a;

    bv(MateriaMedicaArea materiaMedicaArea) {
        this.f383a = materiaMedicaArea;
    }

    public final void onClick(View view) {
        MateriaMedicaArea materiaMedicaArea = this.f383a;
        materiaMedicaArea.startActivityForResult(new Intent(materiaMedicaArea, FormulaCategoryList.class), 1350);
    }
}
