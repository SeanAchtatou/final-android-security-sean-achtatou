package com.cyberandsons.tcmaidtrial.areas;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.a.t;
import com.cyberandsons.tcmaidtrial.lists.DiagnosisList;
import com.cyberandsons.tcmaidtrial.lists.PulseDiagList;
import com.cyberandsons.tcmaidtrial.lists.SixStagesList;
import com.cyberandsons.tcmaidtrial.misc.dh;
import org.acra.ErrorReporter;

public class DiagnosisCategoryArea extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Button f325a;

    /* renamed from: b  reason: collision with root package name */
    private SQLiteDatabase f326b;
    private n c;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        g();
        try {
            if (dh.d) {
                setRequestedOrientation(1);
            }
            setContentView((int) C0000R.layout.diagnosis_categories);
            TextView textView = (TextView) findViewById(C0000R.id.tc_label);
            textView.setText("Diagnosis");
            Paint paint = new Paint();
            paint.setTextSize(textView.getTextSize());
            paint.setAntiAlias(true);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            textView.setPadding((displayMetrics.widthPixels / 2) - (((int) (paint.measureText("Diagnosis") + 0.5f)) / 2), 10, 0, 10);
            this.f325a = (Button) findViewById(C0000R.id.dc_internalorgans);
            this.f325a.setText(dh.a("Patterns by Internal Organs", "From Macciocia, Diagnosis In Chinese Medicine"));
            this.f325a.setOnClickListener(new ca(this));
            this.f325a = (Button) findViewById(C0000R.id.dc_fourlevels);
            this.f325a.setText(dh.a("Four Levels", "From Macciocia, Diagnosis In Chinese Medicine"));
            this.f325a.setOnClickListener(new cd(this));
            this.f325a = (Button) findViewById(C0000R.id.dc_residualpathogenic);
            this.f325a.setText(dh.a("Residual Pathogenic", "Residual Pathogenic"));
            this.f325a.setOnClickListener(new ce(this));
            this.f325a = (Button) findViewById(C0000R.id.dc_sixstages);
            this.f325a.setText(dh.a("Six Stages", "From Zhang Zhong Jing, Shang Hun Lun"));
            this.f325a.setOnClickListener(new cb(this));
            this.f325a = (Button) findViewById(C0000R.id.dc_pulsediag);
            this.f325a.setText(dh.a("Pulse Diagnosis", "From Li Shi Zhen, Pulse Diagnosis"));
            this.f325a.setOnClickListener(new cc(this));
        } catch (Exception e) {
            ErrorReporter.a().a("myVariable", e.getLocalizedMessage());
            ErrorReporter.a().handleSilentException(new Exception("DCA:onCreate()"));
            AlertDialog create = new AlertDialog.Builder(this).create();
            create.setTitle("Attention:");
            create.setMessage(getString(C0000R.string.area_list_exception));
            create.setButton("OK", new cf(this));
            create.show();
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        a("Patterns by Internal Organs");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        a("Four Levels");
    }

    /* access modifiers changed from: protected */
    public final void c() {
        a("Residual Pathogenic");
    }

    /* access modifiers changed from: protected */
    public final void d() {
        a("Six Stages");
    }

    /* access modifiers changed from: protected */
    public final void e() {
        a("Pulse Diagnosis");
    }

    private void a(String str) {
        if (str.compareTo("Six Stages") == 0) {
            TcmAid.aM = str;
            startActivityForResult(new Intent(this, SixStagesList.class), 1350);
        } else if (str.compareTo("Pulse Diagnosis") == 0) {
            TcmAid.aC = str;
            startActivityForResult(new Intent(this, PulseDiagList.class), 1350);
        } else {
            if (str.compareTo("Patterns by Internal Organs") == 0) {
                TcmAid.C = "pathtype=?";
                TcmAid.D = new String[]{Integer.toString(0)};
                TcmAid.A = 0;
                TcmAid.F = t.a(0, this.c);
            } else if (str.compareTo("Four Levels") == 0) {
                TcmAid.C = "pathtype=?";
                TcmAid.D = new String[]{Integer.toString(1)};
                TcmAid.A = 1;
                TcmAid.G = t.a(1, this.c);
            } else if (str.compareTo("Residual Pathogenic") == 0) {
                TcmAid.C = "pathtype=?";
                TcmAid.D = new String[]{Integer.toString(3)};
                TcmAid.A = 3;
                TcmAid.H = t.a(3, this.c);
            }
            TcmAid.B = str;
            startActivityForResult(new Intent(this, DiagnosisList.class), 1350);
        }
    }

    public void onDestroy() {
        f();
        super.onDestroy();
    }

    public void onResume() {
        g();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1350 && i2 == 2) {
            setResult(2);
            f();
        }
    }

    private void f() {
        try {
            if (this.f326b != null && this.f326b.isOpen()) {
                this.f326b.close();
                this.f326b = null;
            }
        } catch (SQLException e) {
            q.a(e);
        }
    }

    private void g() {
        if (this.f326b == null || !this.f326b.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("DCA:openDataBase()", str + " opened.");
                this.f326b = SQLiteDatabase.openDatabase(str, null, 16);
                this.f326b.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.f326b.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("DCA:openDataBase()", str2 + " ATTACHed.");
                this.c = new n();
                this.c.a(this.f326b);
            } catch (SQLException e) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new cg(this));
                create.show();
            }
        }
    }
}
