package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.DiagnosisList;

final class a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SearchArea f335a;

    a(SearchArea searchArea) {
        this.f335a = searchArea;
    }

    public final void onClick(View view) {
        SearchArea searchArea = this.f335a;
        TcmAid.C = null;
        TcmAid.A = 1;
        TcmAid.cP = "Results for Four Levels";
        searchArea.startActivityForResult(new Intent(searchArea, DiagnosisList.class), 1350);
    }
}
