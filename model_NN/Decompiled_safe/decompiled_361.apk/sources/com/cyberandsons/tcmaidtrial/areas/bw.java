package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.HerbList;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class bw implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MateriaMedicaArea f384a;

    bw(MateriaMedicaArea materiaMedicaArea) {
        this.f384a = materiaMedicaArea;
    }

    public final void onClick(View view) {
        MateriaMedicaArea materiaMedicaArea = this.f384a;
        TcmAid.ad = null;
        if (TcmAid.af != null) {
            TcmAid.ai++;
            TcmAid.aj.add(TcmAid.af);
            if (dh.U) {
                Log.d("MMA:MateriaMedicaHerbList:", "hbCounter++ = " + Integer.toString(TcmAid.ai) + ", hbCounterArrary.count = " + Integer.toString(TcmAid.aj.size()));
            }
        }
        materiaMedicaArea.startActivityForResult(new Intent(materiaMedicaArea, HerbList.class), 1350);
    }
}
