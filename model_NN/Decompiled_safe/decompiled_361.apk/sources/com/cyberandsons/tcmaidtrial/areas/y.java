package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.PointsList;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class y implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointsArea f418a;

    y(PointsArea pointsArea) {
        this.f418a = pointsArea;
    }

    public final void onClick(View view) {
        PointsArea pointsArea = this.f418a;
        if (TcmAid.ar != null) {
            TcmAid.au++;
            TcmAid.aw.add(TcmAid.ar);
            if (dh.W) {
                Log.d("PA:pointsButton_Click()", "ptCounter++ = " + Integer.toString(TcmAid.au) + ", ptCounterArrary.count = " + Integer.toString(TcmAid.aw.size()));
            }
        }
        pointsArea.startActivityForResult(new Intent(pointsArea, PointsList.class), 1350);
    }
}
