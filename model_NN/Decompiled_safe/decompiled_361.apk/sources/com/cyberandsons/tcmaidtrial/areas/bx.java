package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.view.View;
import com.cyberandsons.tcmaidtrial.lists.HerbCategoryList;

final class bx implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MateriaMedicaArea f385a;

    bx(MateriaMedicaArea materiaMedicaArea) {
        this.f385a = materiaMedicaArea;
    }

    public final void onClick(View view) {
        MateriaMedicaArea materiaMedicaArea = this.f385a;
        materiaMedicaArea.startActivityForResult(new Intent(materiaMedicaArea, HerbCategoryList.class), 1350);
    }
}
