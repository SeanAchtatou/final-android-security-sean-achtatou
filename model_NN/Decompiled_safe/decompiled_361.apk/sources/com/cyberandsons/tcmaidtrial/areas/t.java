package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.view.View;
import com.cyberandsons.tcmaidtrial.lists.AuricularCategoryList;

final class t implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointsArea f413a;

    t(PointsArea pointsArea) {
        this.f413a = pointsArea;
    }

    public final void onClick(View view) {
        PointsArea pointsArea = this.f413a;
        pointsArea.startActivityForResult(new Intent(pointsArea, AuricularCategoryList.class), 1350);
    }
}
