package com.cyberandsons.tcmaidtrial.areas;

import android.content.ContentValues;
import android.database.SQLException;
import android.util.Log;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class ab implements k {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointCategoryArea f337a;

    /* synthetic */ ab(PointCategoryArea pointCategoryArea) {
        this(pointCategoryArea, (byte) 0);
    }

    private ab(PointCategoryArea pointCategoryArea, byte b2) {
        this.f337a = pointCategoryArea;
    }

    public final void a(String str, String str2, String str3) {
        boolean z;
        int i;
        if (dh.O) {
            Log.d("PCA:PointCategoryDialog", "Category is " + str);
            Log.d("PCA:PointCategoryDialog", "Description is " + str2);
            Log.d("PCA:PointCategoryDialog", "Points are '" + str3 + "' ");
        }
        ContentValues contentValues = new ContentValues();
        boolean z2 = this.f337a.f330b;
        int a2 = q.a("SELECT _id, name, desc, points  FROM userDB.pointcategory WHERE ", "userDB.pointcategory.", "name", str, this.f337a.e);
        if (a2 == dh.f946a.intValue()) {
            int a3 = q.a("SELECT MAX(userDB.pointcategory._id) FROM userDB.pointcategory ", this.f337a.e) + 1;
            contentValues.put("_id", Integer.toString(a3));
            int i2 = a3;
            z = z2;
            i = i2;
        } else {
            contentValues.put("_id", Integer.toString(a2));
            int i3 = a2;
            z = this.f337a.f329a;
            i = i3;
        }
        contentValues.put("name", str);
        contentValues.put("desc", str2);
        contentValues.put("points", str3.toUpperCase());
        if (!z) {
            try {
                this.f337a.e.insert("userDB.pointcategory", null, contentValues);
            } catch (SQLException e) {
                q.a(e);
            }
        } else {
            this.f337a.e.update("userDB.pointcategory", contentValues, "_id=" + Integer.toString(i), null);
        }
        TcmAid.cE = this.f337a.f329a;
        this.f337a.startActivity(this.f337a.getIntent());
        this.f337a.finish();
    }

    public final void a(String str) {
    }
}
