package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.FormulasList;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SearchArea f398a;

    f(SearchArea searchArea) {
        this.f398a = searchArea;
    }

    public final void onClick(View view) {
        SearchArea searchArea = this.f398a;
        if (TcmAid.S == null) {
            if (TcmAid.W > 0) {
                TcmAid.W--;
                TcmAid.S = (String) TcmAid.X.get(TcmAid.W);
                TcmAid.X.remove(TcmAid.W);
                if (dh.T) {
                    Log.d("SA:formulasButton_Click()", "fxCounter-- = " + Integer.toString(TcmAid.W) + ", fxCounterArrary.count = " + Integer.toString(TcmAid.X.size()));
                }
            }
            if (dh.ac) {
                Log.d("SA:formulasButton_Click()", TcmAid.S);
            }
        }
        TcmAid.Q = null;
        TcmAid.cP = "Results for Formulas";
        searchArea.startActivityForResult(new Intent(searchArea, FormulasList.class), 1350);
    }
}
