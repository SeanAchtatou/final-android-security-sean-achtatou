package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.PulseDiagList;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class h implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SearchArea f400a;

    h(SearchArea searchArea) {
        this.f400a = searchArea;
    }

    public final void onClick(View view) {
        SearchArea searchArea = this.f400a;
        if (TcmAid.aF == null) {
            if (TcmAid.aI > 0) {
                TcmAid.aI--;
                TcmAid.aF = (String) TcmAid.aJ.get(TcmAid.aI);
                TcmAid.aJ.remove(TcmAid.aI);
                if (dh.V) {
                    Log.d("SA:pulseDiagButton_Click()", "pdCounter-- = " + Integer.toString(TcmAid.aI) + ", pdCounterArrary.count = " + Integer.toString(TcmAid.aJ.size()));
                }
            }
            if (dh.af) {
                Log.d("SA:pulseDiagButton_Click()", TcmAid.aF);
            }
        }
        TcmAid.aD = null;
        TcmAid.cP = "Results for Pulse Diagnosis";
        searchArea.startActivityForResult(new Intent(searchArea, PulseDiagList.class), 1350);
    }
}
