package com.cyberandsons.tcmaidtrial.areas;

import android.view.View;
import android.widget.EditText;
import com.cyberandsons.tcmaidtrial.C0000R;

final class s implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ l f412a;

    /* synthetic */ s(l lVar) {
        this(lVar, (byte) 0);
    }

    private s(l lVar, byte b2) {
        this.f412a = lVar;
    }

    public final void onClick(View view) {
        this.f412a.d.a(((EditText) this.f412a.findViewById(C0000R.id.category)).getText().toString(), ((EditText) this.f412a.findViewById(C0000R.id.description)).getText().toString(), ((EditText) this.f412a.findViewById(C0000R.id.points)).getText().toString());
        this.f412a.dismiss();
    }
}
