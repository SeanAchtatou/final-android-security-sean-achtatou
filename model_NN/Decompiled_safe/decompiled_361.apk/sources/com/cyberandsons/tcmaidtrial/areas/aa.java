package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.TungList;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class aa implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointsArea f336a;

    aa(PointsArea pointsArea) {
        this.f336a = pointsArea;
    }

    public final void onClick(View view) {
        PointsArea pointsArea = this.f336a;
        if (TcmAid.aY != null) {
            TcmAid.bb++;
            TcmAid.bc.add(TcmAid.aY);
            if (dh.Y) {
                Log.d("PA:PointAreaTungList:", "tuCounter++ = " + Integer.toString(TcmAid.bb) + ", tuCounterArrary.count = " + Integer.toString(TcmAid.bc.size()));
            }
        }
        pointsArea.startActivityForResult(new Intent(pointsArea, TungList.class), 1350);
    }
}
