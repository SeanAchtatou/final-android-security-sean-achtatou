package com.cyberandsons.tcmaidtrial.areas;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.misc.dh;

public final class r extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    private String f410a;

    /* renamed from: b  reason: collision with root package name */
    private String f411b;
    /* access modifiers changed from: private */
    public bk c;

    public r(Context context, String str, String str2, bk bkVar) {
        super(context);
        this.f410a = str;
        this.f411b = str2;
        this.c = bkVar;
        if (dh.A) {
            Log.d("NAMD:NonAndroidMarketDialog()", "DeviceID is " + this.f410a);
            Log.d("NAMD:NonAndroidMarketDialog()", "RegCode  is " + this.f411b);
        }
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.nonandroidmarket_dialog);
        setTitle("Device Registration");
        EditText editText = (EditText) findViewById(C0000R.id.DeviceID);
        if (this.f410a == null || this.f410a.length() <= 0) {
            editText.setHint("Enter DeviceID...");
        } else {
            editText.setText(this.f410a);
        }
        EditText editText2 = (EditText) findViewById(C0000R.id.regCode);
        if (this.f411b == null || this.f411b.length() <= 0) {
            editText2.setHint("Enter Reg. Code...");
        } else {
            editText2.setText(this.f411b);
        }
        Button button = (Button) findViewById(C0000R.id.buttonOK);
        if (this.f410a != null && this.f410a.length() > 0) {
            button.setText("Register");
        }
        button.setOnClickListener(new j(this));
        Button button2 = (Button) findViewById(C0000R.id.buttonCancel);
        if (this.f410a != null && this.f410a.length() > 0) {
            button2.setText("Cancel");
        }
        button2.setOnClickListener(new q(this));
    }
}
