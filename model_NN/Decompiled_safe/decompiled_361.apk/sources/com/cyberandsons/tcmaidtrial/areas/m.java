package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.WristAnkleList;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class m implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SearchArea f405a;

    m(SearchArea searchArea) {
        this.f405a = searchArea;
    }

    public final void onClick(View view) {
        SearchArea searchArea = this.f405a;
        if (TcmAid.bh == null) {
            if (TcmAid.bj > 0) {
                TcmAid.bj--;
                TcmAid.bh = (String) TcmAid.bk.get(TcmAid.bj);
                TcmAid.bk.remove(TcmAid.bj);
                if (dh.Z) {
                    Log.d("SA:wristAnkleButton_Click()", "waCounter-- = " + Integer.toString(TcmAid.bj) + ", waCounterArrary.count = " + Integer.toString(TcmAid.bk.size()));
                }
            }
            if (dh.ai) {
                Log.d("SA:wristAnkleButton_Click()", TcmAid.bh);
            }
        }
        TcmAid.bf = null;
        TcmAid.cP = "Results for WristAnkle";
        searchArea.startActivityForResult(new Intent(searchArea, WristAnkleList.class), 1350);
    }
}
