package com.cyberandsons.tcmaidtrial.areas;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.a;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.lists.PointsList;
import com.cyberandsons.tcmaidtrial.misc.dh;
import org.acra.ErrorReporter;

public class PointCategoryArea extends Activity {

    /* renamed from: a  reason: collision with root package name */
    boolean f329a = true;

    /* renamed from: b  reason: collision with root package name */
    boolean f330b;
    private Button c;
    private ImageButton d;
    /* access modifiers changed from: private */
    public SQLiteDatabase e;
    private n f;

    public PointCategoryArea() {
        boolean z;
        if (!this.f329a) {
            z = true;
        } else {
            z = false;
        }
        this.f330b = z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x006b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void a(com.cyberandsons.tcmaidtrial.areas.PointCategoryArea r7, android.view.View r8) {
        /*
            r5 = 0
            int r0 = r8.getId()
            android.view.View r0 = r8.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            java.lang.CharSequence r1 = r0.getText()
            java.lang.String r1 = r1.toString()
            r2 = 0
            java.lang.CharSequence r0 = r0.getText()
            java.lang.String r0 = r0.toString()
            java.lang.String r3 = "\n"
            int r0 = r0.indexOf(r3)
            java.lang.String r1 = r1.substring(r2, r0)
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.O
            if (r0 == 0) goto L_0x002f
            java.lang.String r0 = "PCA:udButton_Click category"
            android.util.Log.d(r0, r1)
        L_0x002f:
            java.lang.String r2 = ""
            android.database.sqlite.SQLiteDatabase r0 = r7.e     // Catch:{ SQLException -> 0x005b, all -> 0x0067 }
            java.lang.String r3 = com.cyberandsons.tcmaidtrial.a.o.a(r1)     // Catch:{ SQLException -> 0x005b, all -> 0x0067 }
            r4 = 0
            android.database.Cursor r0 = r0.rawQuery(r3, r4)     // Catch:{ SQLException -> 0x005b, all -> 0x0067 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x005b, all -> 0x0067 }
            boolean r3 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0077, all -> 0x006f }
            if (r3 == 0) goto L_0x0049
            r3 = 2
            java.lang.String r2 = r0.getString(r3)     // Catch:{ SQLException -> 0x0077, all -> 0x006f }
        L_0x0049:
            if (r0 == 0) goto L_0x007c
            r0.close()
            r0 = r2
        L_0x004f:
            com.cyberandsons.tcmaidtrial.a.n r2 = r7.f
            java.lang.String r0 = com.cyberandsons.tcmaidtrial.a.a.a(r0, r2)
            com.cyberandsons.tcmaidtrial.TcmAid.ao = r0
            r7.a(r1)
            return
        L_0x005b:
            r0 = move-exception
            r3 = r5
        L_0x005d:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0074 }
            if (r3 == 0) goto L_0x007c
            r3.close()
            r0 = r2
            goto L_0x004f
        L_0x0067:
            r0 = move-exception
            r1 = r5
        L_0x0069:
            if (r1 == 0) goto L_0x006e
            r1.close()
        L_0x006e:
            throw r0
        L_0x006f:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0069
        L_0x0074:
            r0 = move-exception
            r1 = r3
            goto L_0x0069
        L_0x0077:
            r3 = move-exception
            r6 = r3
            r3 = r0
            r0 = r6
            goto L_0x005d
        L_0x007c:
            r0 = r2
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.areas.PointCategoryArea.a(com.cyberandsons.tcmaidtrial.areas.PointCategoryArea, android.view.View):void");
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        M();
        try {
            if (dh.d) {
                setRequestedOrientation(1);
            }
            setContentView((int) C0000R.layout.point_categories);
            TextView textView = (TextView) findViewById(C0000R.id.tca_label);
            textView.setText("Points Categories");
            Paint paint = new Paint();
            paint.setTextSize(textView.getTextSize());
            paint.setAntiAlias(true);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            textView.setPadding((displayMetrics.widthPixels / 2) - (((int) (paint.measureText("Points Categories") + 0.5f)) / 2), 10, 0, 10);
            this.d = (ImageButton) findViewById(C0000R.id.add);
            this.d.setOnClickListener(new bs(this));
            this.c = (Button) findViewById(C0000R.id.pc_chongmai);
            this.c.setText(dh.a("Chong Mai", "Penetrating Vessel."));
            this.c.setOnClickListener(new bt(this));
            this.c = (Button) findViewById(C0000R.id.pc_daimai);
            this.c.setText(dh.a("Dai Mai", "Belt Channel (Vessel)."));
            this.c.setOnClickListener(new bl(this));
            this.c = (Button) findViewById(C0000R.id.pc_dumai);
            this.c.setText(dh.a("Du Mai", "Governing Vessel."));
            this.c.setOnClickListener(new bm(this));
            this.c = (Button) findViewById(C0000R.id.pc_renmai);
            this.c.setText(dh.a("Ren Mai", "Conception Vessel."));
            this.c.setOnClickListener(new bn(this));
            this.c = (Button) findViewById(C0000R.id.pc_yangqiaomai);
            this.c.setText(dh.a("Yang Qiao Mai", "Yang Heel Vessel."));
            this.c.setOnClickListener(new bo(this));
            this.c = (Button) findViewById(C0000R.id.pc_yangweimai);
            this.c.setText(dh.a("Yang Wei Mai", "Yang Linking Vessel."));
            this.c.setOnClickListener(new bp(this));
            this.c = (Button) findViewById(C0000R.id.pc_yinqiaomai);
            this.c.setText(dh.a("Yin Qiao Mai", "Yin Heel Vessel."));
            this.c.setOnClickListener(new bq(this));
            this.c = (Button) findViewById(C0000R.id.pc_yinweimai);
            this.c.setText(dh.a("Yin Wei Mai", "Yin Linking Vessel."));
            this.c.setOnClickListener(new br(this));
            this.c = (Button) findViewById(C0000R.id.pc_jingwell);
            this.c.setText(dh.a("Jing-Well Points", "Where the Qi of the Channel emerges."));
            this.c.setOnClickListener(new an(this));
            this.c = (Button) findViewById(C0000R.id.pc_yingspring);
            this.c.setText(dh.a("Ying-Spring Points", "Where the Qi of the Channel trickles."));
            this.c.setOnClickListener(new ao(this));
            this.c = (Button) findViewById(C0000R.id.pc_shustream);
            this.c.setText(dh.a("Shu-Stream Points", "Where the Qi of the Channel begins to pour."));
            this.c.setOnClickListener(new ap(this));
            this.c = (Button) findViewById(C0000R.id.pc_jingriver);
            this.c.setText(dh.a("Jing-River Points", "Where the Qi of the Channel begins to flow."));
            this.c.setOnClickListener(new aq(this));
            this.c = (Button) findViewById(C0000R.id.pc_hesea);
            this.c.setText(dh.a("He-Sea Points", "Where the Qi of the Channel runs deeper."));
            this.c.setOnClickListener(new ar(this));
            this.c = (Button) findViewById(C0000R.id.pc_backshu);
            this.c.setText(dh.a("Back-Shu Points", "Qi of each organ moves to and from these points."));
            this.c.setOnClickListener(new as(this));
            this.c = (Button) findViewById(C0000R.id.pc_frontmu);
            this.c.setText(dh.a("Front-Mu Points", "These points have a direct effect on their organs."));
            this.c.setOnClickListener(new au(this));
            this.c = (Button) findViewById(C0000R.id.pc_entry);
            this.c.setText(dh.a("Entry Points", "Used extensively in Five Element Style Acupuncture."));
            this.c.setOnClickListener(new at(this));
            this.c = (Button) findViewById(C0000R.id.pc_exit);
            this.c.setText(dh.a("Exit Points", "Used extensively in Five Element Style Acupuncture."));
            this.c.setOnClickListener(new aw(this));
            this.c = (Button) findViewById(C0000R.id.pc_command);
            this.c.setText(dh.a("Command Points", "Used to treat ay imbalance in their designated areas."));
            this.c.setOnClickListener(new av(this));
            this.c = (Button) findViewById(C0000R.id.pc_confluent);
            this.c.setText(dh.a("Confluent Points", "Where primary channels meet extraordinary channels."));
            this.c.setOnClickListener(new af(this));
            this.c = (Button) findViewById(C0000R.id.pc_ghost);
            this.c.setText(dh.a("Thirteen Ghost Points", "Noted by Sun Si Miao, for mania and epilepsy."));
            this.c.setOnClickListener(new ad(this));
            this.c = (Button) findViewById(C0000R.id.pc_influential);
            this.c.setText(dh.a("Hui-Meeting (Influential) Points", "These points have special effect on their related areas."));
            this.c.setOnClickListener(new ae(this));
            this.c = (Button) findViewById(C0000R.id.pc_luo);
            this.c.setText(dh.a("Luo-Connecting Points", "Connects int.-ext. pairs of Yin and Yang Channels."));
            this.c.setOnClickListener(new ai(this));
            this.c = (Button) findViewById(C0000R.id.pc_lowerhesea);
            this.c.setText(dh.a("Lower He-Sea Points", "Points used to treat their respective Yang organs."));
            this.c.setOnClickListener(new aj(this));
            this.c = (Button) findViewById(C0000R.id.pc_fourseas);
            this.c.setText(dh.a("Four Seas Points", "Points having a strong effect on their related systems."));
            this.c.setOnClickListener(new ag(this));
            this.c = (Button) findViewById(C0000R.id.pc_tendino);
            this.c.setText(dh.a("Tendino Musculo Meridian", "These points affect the muscle aspect of their channels."));
            this.c.setOnClickListener(new ah(this));
            this.c = (Button) findViewById(C0000R.id.pc_sky);
            this.c.setText(dh.a("Window of the Sky Points", "Improve flow of energy between the head and body."));
            this.c.setOnClickListener(new am(this));
            this.c = (Button) findViewById(C0000R.id.pc_xicleft);
            this.c.setText(dh.a("Xi-Cleft Points", "Points where Qi and Blood of the meridian pool."));
            this.c.setOnClickListener(new al(this));
            this.c = (Button) findViewById(C0000R.id.pc_yuansource);
            this.c.setText(dh.a("Yuan-Source Points", "Points where Yuan Qi, primordial Qi, pools."));
            this.c.setOnClickListener(new ak(this));
            this.c = (Button) findViewById(C0000R.id.pc_luo8);
            this.c.setText(dh.a("Luo-Connecting 8 Extra Meridians", "Connects int.-ext. pairs of Yin and Yang channels."));
            this.c.setOnClickListener(new bj(this));
            this.c = (Button) findViewById(C0000R.id.pc_xicleft8);
            this.c.setText(dh.a("Xi-Cleft 8 Extra Meridians", "Points where Qi and Blood of the meridian pool."));
            this.c.setOnClickListener(new bi(this));
            this.c = (Button) findViewById(C0000R.id.pc_mother);
            this.c.setText(dh.a("Mother (Reinforcing) Points", "These points increase the Qi in the channel."));
            this.c.setOnClickListener(new bh(this));
            this.c = (Button) findViewById(C0000R.id.pc_son);
            this.c.setText(dh.a("Son (Reducing) Points", "These points reduce the Qi in the channel."));
            this.c.setOnClickListener(new bg(this));
            this.c = (Button) findViewById(C0000R.id.prec_pregnancy);
            this.c.setText(dh.a("Precautions for Pregnancy", "Forbidden or cautious use during pregnancy."));
            this.c.setOnClickListener(new bf(this));
            this.c = (Button) findViewById(C0000R.id.prec_moxa);
            this.c.setText(dh.a("Precautions for Moxa", "Forbidden or cautious use of moxa."));
            this.c.setOnClickListener(new be(this));
            this.c = (Button) findViewById(C0000R.id.prec_nodeep);
            this.c.setText(dh.a("Precautions for Deep Needling", "Cautions for deep needling."));
            this.c.setOnClickListener(new bd(this));
            this.c = (Button) findViewById(C0000R.id.prec_artery);
            this.c.setText(dh.a("Avoiding Arteries", "Cautions for avoiding arteries."));
            this.c.setOnClickListener(new bc(this));
            this.c = (Button) findViewById(C0000R.id.prec_noneedle);
            this.c.setText(dh.a("Points Not Needled", "Points not needled."));
            this.c.setOnClickListener(new ba(this));
            if (q.a("SELECT COUNT(userDB.pointcategory._id) FROM userDB.pointcategory ", this.e) > 0) {
                L();
            }
        } catch (Exception e2) {
            ErrorReporter.a().a("myVariable", e2.getLocalizedMessage());
            ErrorReporter.a().handleSilentException(new Exception("PCA:onCreate()"));
            AlertDialog create = new AlertDialog.Builder(this).create();
            create.setTitle("Attention:");
            create.setMessage(getString(C0000R.string.area_list_exception));
            create.setButton("OK", new bb(this));
            create.show();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void L() {
        /*
            r7 = this;
            r5 = 0
            r4 = 0
            r0 = 20
            int[] r2 = new int[r0]
            r2 = {2131362073, 2131362074, 2131362075, 2131362076, 2131362077, 2131362078, 2131362079, 2131362080, 2131362081, 2131362082, 2131362083, 2131362084, 2131362085, 2131362086, 2131362087, 2131362088, 2131362089, 2131362090, 2131362091, 2131362092} // fill-array
            r0 = 2131362072(0x7f0a0118, float:1.8343914E38)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r0.setVisibility(r4)
            android.database.sqlite.SQLiteDatabase r0 = r7.e     // Catch:{ SQLException -> 0x005a, all -> 0x0065 }
            java.lang.String r1 = "SELECT _id, name, desc, points  FROM userDB.pointcategory "
            r3 = 0
            android.database.Cursor r0 = r0.rawQuery(r1, r3)     // Catch:{ SQLException -> 0x005a, all -> 0x0065 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x005a, all -> 0x0065 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0074, all -> 0x006d }
            if (r1 == 0) goto L_0x0054
            r3 = r4
        L_0x0027:
            r1 = 1
            java.lang.String r4 = r0.getString(r1)     // Catch:{ SQLException -> 0x0074, all -> 0x006d }
            r1 = 2
            java.lang.String r5 = r0.getString(r1)     // Catch:{ SQLException -> 0x0074, all -> 0x006d }
            r1 = r2[r3]     // Catch:{ SQLException -> 0x0074, all -> 0x006d }
            android.view.View r1 = r7.findViewById(r1)     // Catch:{ SQLException -> 0x0074, all -> 0x006d }
            android.widget.Button r1 = (android.widget.Button) r1     // Catch:{ SQLException -> 0x0074, all -> 0x006d }
            java.lang.CharSequence r4 = com.cyberandsons.tcmaidtrial.misc.dh.a(r4, r5)     // Catch:{ SQLException -> 0x0074, all -> 0x006d }
            r1.setText(r4)     // Catch:{ SQLException -> 0x0074, all -> 0x006d }
            r4 = 0
            r1.setVisibility(r4)     // Catch:{ SQLException -> 0x0074, all -> 0x006d }
            com.cyberandsons.tcmaidtrial.areas.ay r4 = new com.cyberandsons.tcmaidtrial.areas.ay     // Catch:{ SQLException -> 0x0074, all -> 0x006d }
            r4.<init>(r7)     // Catch:{ SQLException -> 0x0074, all -> 0x006d }
            r1.setOnClickListener(r4)     // Catch:{ SQLException -> 0x0074, all -> 0x006d }
            int r1 = r3 + 1
            boolean r3 = r0.moveToNext()     // Catch:{ SQLException -> 0x0074, all -> 0x006d }
            if (r3 != 0) goto L_0x0079
        L_0x0054:
            if (r0 == 0) goto L_0x0059
            r0.close()
        L_0x0059:
            return
        L_0x005a:
            r0 = move-exception
            r1 = r5
        L_0x005c:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0072 }
            if (r1 == 0) goto L_0x0059
            r1.close()
            goto L_0x0059
        L_0x0065:
            r0 = move-exception
            r1 = r5
        L_0x0067:
            if (r1 == 0) goto L_0x006c
            r1.close()
        L_0x006c:
            throw r0
        L_0x006d:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0067
        L_0x0072:
            r0 = move-exception
            goto L_0x0067
        L_0x0074:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x005c
        L_0x0079:
            r3 = r1
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.areas.PointCategoryArea.L():void");
    }

    /* access modifiers changed from: protected */
    public final void a() {
        a("Chong Mai");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        a("Dai Mai");
    }

    /* access modifiers changed from: protected */
    public final void c() {
        a("Du Mai");
    }

    /* access modifiers changed from: protected */
    public final void d() {
        a("Ren Mai");
    }

    /* access modifiers changed from: protected */
    public final void e() {
        a("Yang Qiao Mai");
    }

    /* access modifiers changed from: protected */
    public final void f() {
        a("Yang Wei Mai");
    }

    /* access modifiers changed from: protected */
    public final void g() {
        a("Yin Qiao Mai");
    }

    /* access modifiers changed from: protected */
    public final void h() {
        a("Yin Wei Mai");
    }

    /* access modifiers changed from: protected */
    public final void i() {
        a("Jing-Well Points");
    }

    /* access modifiers changed from: protected */
    public final void j() {
        a("Ying-Spring Points");
    }

    /* access modifiers changed from: protected */
    public final void k() {
        a("Shu-Stream Points");
    }

    /* access modifiers changed from: protected */
    public final void l() {
        a("Jing-River Points");
    }

    /* access modifiers changed from: protected */
    public final void m() {
        a("He-Sea Points");
    }

    /* access modifiers changed from: protected */
    public final void n() {
        a("Back-Shu Points");
    }

    /* access modifiers changed from: protected */
    public final void o() {
        a("Front-Mu Points");
    }

    /* access modifiers changed from: protected */
    public final void p() {
        a("Entry Points");
    }

    /* access modifiers changed from: protected */
    public final void q() {
        a("Exit Points");
    }

    /* access modifiers changed from: protected */
    public final void r() {
        a("Command Points");
    }

    /* access modifiers changed from: protected */
    public final void s() {
        a("Confluent Points");
    }

    /* access modifiers changed from: protected */
    public final void t() {
        a("Thirteen Ghost Points");
    }

    /* access modifiers changed from: protected */
    public final void u() {
        a("Hui-Meeting (Influential) Points");
    }

    /* access modifiers changed from: protected */
    public final void v() {
        a("Luo-Connecting Points");
    }

    /* access modifiers changed from: protected */
    public final void w() {
        a("Lower He-Sea Points");
    }

    /* access modifiers changed from: protected */
    public final void x() {
        a("Four Seas Points");
    }

    /* access modifiers changed from: protected */
    public final void y() {
        a("Tendino Musculo Meridian");
    }

    /* access modifiers changed from: protected */
    public final void z() {
        a("Window of the Sky Points");
    }

    /* access modifiers changed from: protected */
    public final void A() {
        a("Xi-Cleft Points");
    }

    /* access modifiers changed from: protected */
    public final void B() {
        a("Yuan-Source Points");
    }

    /* access modifiers changed from: protected */
    public final void C() {
        a("Luo-Connecting 8 Extra Meridians");
    }

    /* access modifiers changed from: protected */
    public final void D() {
        a("Xi-Cleft 8 Extra Meridians");
    }

    /* access modifiers changed from: protected */
    public final void E() {
        a("Mother (Reinforcing) Points");
    }

    /* access modifiers changed from: protected */
    public final void F() {
        a("Son (Reducing) Points");
    }

    /* access modifiers changed from: protected */
    public final void G() {
        a("Precautions for Pregnancy");
    }

    /* access modifiers changed from: protected */
    public final void H() {
        a("Precautions for Moxa");
    }

    /* access modifiers changed from: protected */
    public final void I() {
        a("Precautions for Deep Needling");
    }

    /* access modifiers changed from: protected */
    public final void J() {
        a("Avoiding Arteries");
    }

    /* access modifiers changed from: protected */
    public final void K() {
        a("Points Not Needled");
    }

    private void a(String str) {
        if (str.compareTo("Back-Shu Points") == 0) {
            TcmAid.ao = a.a("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 35, 47);
        } else if (str.compareTo("Command Points") == 0) {
            TcmAid.ao = "SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian " + " WHERE (main.pointslocation.specificpoint1 >= " + Integer.toString(48) + " AND main.pointslocation.specificpoint1 <= " + Integer.toString(51) + ") " + " OR (main.pointslocation.specificpoint1 >= " + Integer.toString(166) + " AND main.pointslocation.specificpoint1 <= " + Integer.toString(167) + ") " + " OR (main.pointslocation.specificpoint2 >= " + Integer.toString(48) + " AND main.pointslocation.specificpoint2 <= " + Integer.toString(51) + ") " + " OR (main.pointslocation.specificpoint2 >= " + Integer.toString(166) + " AND main.pointslocation.specificpoint2 <= " + Integer.toString(167) + ") " + " OR (main.pointslocation.specificpoint3 >= " + Integer.toString(48) + " AND main.pointslocation.specificpoint3 <= " + Integer.toString(51) + ") " + " OR (main.pointslocation.specificpoint3 >= " + Integer.toString(166) + " AND main.pointslocation.specificpoint3 <= " + Integer.toString(167) + ") " + " OR (main.pointslocation.specificpoint4 >= " + Integer.toString(48) + " AND main.pointslocation.specificpoint4 <= " + Integer.toString(51) + ") " + " OR (main.pointslocation.specificpoint4 >= " + Integer.toString(166) + " AND main.pointslocation.specificpoint4 <= " + Integer.toString(167) + ')';
        } else if (str.compareTo("Confluent Points") == 0) {
            TcmAid.ao = a.a("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 52, 59);
        } else if (str.compareTo("Entry Points") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 102);
        } else if (str.compareTo("Exit Points") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 103);
        } else if (str.compareTo("Front-Mu Points") == 0) {
            TcmAid.ao = a.a("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 60, 72);
        } else if (str.compareTo("Thirteen Ghost Points") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 163);
        } else if (str.compareTo("He-Sea Points") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 6);
        } else if (str.compareTo("Hui-Meeting (Influential) Points") == 0) {
            TcmAid.ao = a.a("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 73, 80);
        } else if (str.compareTo("Jing-River Points") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 5);
        } else if (str.compareTo("Jing-Well Points") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 2);
        } else if (str.compareTo("Lower He-Sea Points") == 0) {
            TcmAid.ao = a.a("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 81, 86);
        } else if (str.compareTo("Luo-Connecting Points") == 0) {
            TcmAid.ao = a.a("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 89, 101);
        } else if (str.compareTo("Four Seas Points") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 162);
        } else if (str.compareTo("Shu-Stream Points") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 4);
        } else if (str.compareTo("Tendino Musculo Meridian") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 165);
        } else if (str.compareTo("Window of the Sky Points") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 164);
        } else if (str.compareTo("Xi-Cleft Points") == 0) {
            TcmAid.ao = a.a("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 19, 30);
        } else if (str.compareTo("Xi-Cleft 8 Extra Meridians") == 0) {
            TcmAid.ao = a.a("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 31, 34);
        } else if (str.compareTo("Luo-Connecting 8 Extra Meridians") == 0) {
            TcmAid.ao = a.a("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 87, 88);
        } else if (str.compareTo("Ying-Spring Points") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 3);
        } else if (str.compareTo("Yuan-Source Points") == 0) {
            TcmAid.ao = a.a("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 7, 18);
        } else if (str.compareTo("Mother (Reinforcing) Points") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 104);
        } else if (str.compareTo("Son (Reducing) Points") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 105);
        } else if (str.compareTo("Chong Mai") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 115);
        } else if (str.compareTo("Dai Mai") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 125);
        } else if (str.compareTo("Du Mai") == 0) {
            TcmAid.ao = a.a("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 13);
        } else if (str.compareTo("Ren Mai") == 0) {
            TcmAid.ao = a.a("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 14);
        } else if (str.compareTo("Yang Qiao Mai") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 124);
        } else if (str.compareTo("Yang Wei Mai") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 123);
        } else if (str.compareTo("Yin Qiao Mai") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 114);
        } else if (str.compareTo("Yin Wei Mai") == 0) {
            TcmAid.ao = a.b("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 113);
        } else if (str.compareTo("Precautions for Pregnancy") == 0) {
            TcmAid.ao = "SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian " + " WHERE main.pointslocation.prec_pregnancy=1";
        } else if (str.compareTo("Precautions for Moxa") == 0) {
            TcmAid.ao = "SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian " + " WHERE main.pointslocation.prec_moxa=1";
        } else if (str.compareTo("Avoiding Arteries") == 0) {
            TcmAid.ao = "SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian " + " WHERE main.pointslocation.prec_artery=1";
        } else if (str.compareTo("Precautions for Deep Needling") == 0) {
            TcmAid.ao = "SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian " + " WHERE main.pointslocation.prec_nodeep=1";
        } else if (str.compareTo("Points Not Needled") == 0) {
            TcmAid.ao = "SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian " + " WHERE main.pointslocation.prec_noneedle=1";
        } else if (str.compareTo("Crossing Points") == 0) {
            TcmAid.ao = a.a("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 106, 125);
        } else if (str.compareTo("Intersection Points") == 0) {
            TcmAid.ao = a.a("SELECT main.pointslocation._id, main.meridian.abbr_name, main.pointslocation.pointnum, main.pointslocation.name, main.pointslocation.englishname, main.pointslocation.indication FROM main.pointslocation LEFT JOIN main.meridian ON main.meridian._id = main.pointslocation.meridian ", 126, 161);
        } else {
            TcmAid.cv = this.f329a;
        }
        TcmAid.an = str;
        startActivityForResult(new Intent(this, PointsList.class), 1350);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        M();
        if (TcmAid.cE) {
            startActivity(getIntent());
            finish();
            TcmAid.cE = false;
        }
        super.onResume();
    }

    public void onDestroy() {
        TcmAid.cD = this.f329a;
        TcmAid.an = null;
        TcmAid.ao = null;
        try {
            if (this.e != null && this.e.isOpen()) {
                this.e.close();
                this.e = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1350 && i2 == 2) {
            setResult(2);
            finish();
        }
    }

    private void M() {
        if (this.e == null || !this.e.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("PCA:openDataBase()", str + " opened.");
                this.e = SQLiteDatabase.openDatabase(str, null, 16);
                this.e.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.e.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("PCA:openDataBase()", str2 + " ATTACHed.");
                this.f = new n();
                this.f.a(this.e);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new az(this));
                create.show();
            }
        }
    }
}
