package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.DiagnosisList;

final class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SearchArea f362a;

    b(SearchArea searchArea) {
        this.f362a = searchArea;
    }

    public final void onClick(View view) {
        SearchArea searchArea = this.f362a;
        TcmAid.C = null;
        TcmAid.A = 3;
        TcmAid.cP = "Results for Residual Pathogenic";
        searchArea.startActivityForResult(new Intent(searchArea, DiagnosisList.class), 1350);
    }
}
