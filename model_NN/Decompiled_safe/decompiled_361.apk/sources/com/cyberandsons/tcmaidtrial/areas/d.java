package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.DiagnosisList;

final class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SearchArea f396a;

    d(SearchArea searchArea) {
        this.f396a = searchArea;
    }

    public final void onClick(View view) {
        SearchArea searchArea = this.f396a;
        TcmAid.C = null;
        TcmAid.A = 0;
        TcmAid.cP = "Results for Patterns by Internal Organs";
        searchArea.startActivityForResult(new Intent(searchArea, DiagnosisList.class), 1350);
    }
}
