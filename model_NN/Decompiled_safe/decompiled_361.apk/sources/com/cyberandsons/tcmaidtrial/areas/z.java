package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.view.View;

final class z implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PointsArea f419a;

    z(PointsArea pointsArea) {
        this.f419a = pointsArea;
    }

    public final void onClick(View view) {
        PointsArea pointsArea = this.f419a;
        pointsArea.startActivityForResult(new Intent(pointsArea, PointCategoryArea.class), 1350);
    }
}
