package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.TungList;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class n implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SearchArea f406a;

    n(SearchArea searchArea) {
        this.f406a = searchArea;
    }

    public final void onClick(View view) {
        SearchArea searchArea = this.f406a;
        if (TcmAid.aY == null) {
            if (TcmAid.bb > 0) {
                TcmAid.bb--;
                TcmAid.aY = (String) TcmAid.bc.get(TcmAid.bb);
                TcmAid.bc.remove(TcmAid.bb);
                if (dh.Y) {
                    Log.d("SA:tungButton_Click()", "tuCounter-- = " + Integer.toString(TcmAid.bb) + ", tuCounterArrary.count = " + Integer.toString(TcmAid.bc.size()));
                }
            }
            if (dh.ah) {
                Log.d("SA:tungButton_Click()", TcmAid.aY);
            }
        }
        TcmAid.aW = null;
        TcmAid.cP = "Results for Master Tung";
        searchArea.startActivityForResult(new Intent(searchArea, TungList.class), 1350);
    }
}
