package com.cyberandsons.tcmaidtrial.areas;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.misc.dh;

public final class l extends Dialog {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f403a;

    /* renamed from: b  reason: collision with root package name */
    private String f404b;
    private String c;
    /* access modifiers changed from: private */
    public k d;

    public l(Context context, String str, String str2, String str3, k kVar) {
        super(context);
        this.f403a = str;
        this.f404b = str2;
        this.c = str3;
        this.d = kVar;
        if (dh.O) {
            Log.d("PCD:PointCategoryDialog", "Category is " + this.f403a);
            Log.d("PCD:PointCategoryDialog", "Description is " + this.f404b);
            Log.d("PCD:PointCategoryDialog", "Points are '" + this.c + "' ");
        }
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.pointcategory_add);
        setTitle("User Defined Point Category");
        EditText editText = (EditText) findViewById(C0000R.id.category);
        if (this.f403a == null || this.f403a.length() <= 0) {
            editText.setHint("Enter Category...");
        } else {
            editText.setText(this.f403a);
        }
        EditText editText2 = (EditText) findViewById(C0000R.id.description);
        if (this.f404b == null || this.f404b.length() <= 0) {
            editText2.setHint("Enter Description...");
        } else {
            editText2.setText(this.f404b);
        }
        EditText editText3 = (EditText) findViewById(C0000R.id.points);
        if (this.c == null || this.c.length() <= 0) {
            editText3.setHint("Enter Points...");
        } else {
            editText3.setText(this.c);
        }
        Button button = (Button) findViewById(C0000R.id.buttonOK);
        if (this.f403a != null && this.f403a.length() > 0) {
            button.setText("Save");
        }
        button.setOnClickListener(new s(this));
        if (this.f403a != null && this.f403a.length() > 0) {
            Button button2 = (Button) findViewById(C0000R.id.buttonDelete);
            button2.setVisibility(0);
            button2.setOnClickListener(new ac(this));
        }
        Button button3 = (Button) findViewById(C0000R.id.buttonCancel);
        if (this.f403a != null && this.f403a.length() > 0) {
            button3.setText("Cancel");
        }
        button3.setOnClickListener(new ax(this));
    }
}
