package com.cyberandsons.tcmaidtrial.areas;

import android.view.View;
import android.widget.EditText;
import com.cyberandsons.tcmaidtrial.C0000R;

final class j implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ r f402a;

    /* synthetic */ j(r rVar) {
        this(rVar, (byte) 0);
    }

    private j(r rVar, byte b2) {
        this.f402a = rVar;
    }

    public final void onClick(View view) {
        this.f402a.c.a(((EditText) this.f402a.findViewById(C0000R.id.DeviceID)).getText().toString(), ((EditText) this.f402a.findViewById(C0000R.id.regCode)).getText().toString());
        this.f402a.dismiss();
    }
}
