package com.cyberandsons.tcmaidtrial.areas;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.AuricularList;
import com.cyberandsons.tcmaidtrial.misc.dh;

final class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SearchArea f388a;

    c(SearchArea searchArea) {
        this.f388a = searchArea;
    }

    public final void onClick(View view) {
        SearchArea searchArea = this.f388a;
        if (TcmAid.o == null) {
            if (TcmAid.s > 0) {
                TcmAid.s--;
                TcmAid.o = (String) TcmAid.t.get(TcmAid.s);
                TcmAid.t.remove(TcmAid.s);
                if (dh.R) {
                    Log.d("SA:auricularButton_Click()", "auCounter-- = " + Integer.toString(TcmAid.s) + ", auCounterArrary.count = " + Integer.toString(TcmAid.t.size()));
                }
            }
            if (dh.aa) {
                Log.d("SA:auricularButton_Click()", TcmAid.o);
            }
        }
        TcmAid.cP = "Results for Auricular";
        searchArea.startActivityForResult(new Intent(searchArea, AuricularList.class), 1350);
    }
}
