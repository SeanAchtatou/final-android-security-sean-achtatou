package com.cyberandsons.tcmaidtrial.iap;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.urbanairship.b.k;
import com.urbanairship.b.l;
import com.urbanairship.b.n;
import com.urbanairship.b.q;
import com.urbanairship.c;

public final class j extends q implements k {

    /* renamed from: a  reason: collision with root package name */
    private Context f719a = c.a().g();

    public final Notification a(l lVar) {
        PendingIntent activity = PendingIntent.getActivity(this.f719a, 0, new Intent(), 0);
        Notification notification = new Notification(C0000R.drawable.icon, lVar.b(), lVar.f());
        notification.flags = lVar.d();
        notification.contentView = new RemoteViews(this.f719a.getPackageName(), (int) C0000R.layout.purchase_notification);
        notification.contentView.setImageViewResource(C0000R.id.status_icon, C0000R.drawable.icon);
        notification.contentIntent = activity;
        notification.contentView.setTextViewText(C0000R.id.status_text, b(lVar));
        notification.contentView.setProgressBar(C0000R.id.status_progress, 100, lVar.e(), false);
        n a2 = lVar.a();
        if (a2 == n.DOWNLOADING) {
            notification.contentView.setViewVisibility(C0000R.id.status_wheel_wrapper, 8);
            notification.contentView.setViewVisibility(C0000R.id.status_progress_wrapper, 0);
            notification.contentView.setProgressBar(C0000R.id.status_progress, 100, lVar.e(), false);
        } else if (a2 == n.VERIFYING_RECEIPT || a2 == n.DECOMPRESSING) {
            notification.contentView.setViewVisibility(C0000R.id.status_progress_wrapper, 8);
            notification.contentView.setViewVisibility(C0000R.id.status_wheel_wrapper, 0);
        } else {
            notification.contentView.setViewVisibility(C0000R.id.status_wheel_wrapper, 8);
            notification.contentView.setViewVisibility(C0000R.id.status_progress_wrapper, 8);
        }
        return notification;
    }
}
