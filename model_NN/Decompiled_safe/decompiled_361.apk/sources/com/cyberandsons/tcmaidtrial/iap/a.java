package com.cyberandsons.tcmaidtrial.iap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.urbanairship.a.d;
import com.urbanairship.b.e;
import com.urbanairship.b.f;
import com.urbanairship.b.h;
import com.urbanairship.b.o;
import java.util.Observable;
import java.util.Observer;

public final class a extends BaseAdapter implements Observer {

    /* renamed from: a  reason: collision with root package name */
    private e f707a = h.a().f();

    /* renamed from: b  reason: collision with root package name */
    private Context f708b;

    public a(Context context) {
        this.f708b = context;
        this.f707a.a(this);
        this.f707a.c();
    }

    public final void update(Observable observable, Object obj) {
        switch (k.f720a[((e) obj).a().ordinal()]) {
            case 1:
                notifyDataSetChanged();
                return;
            default:
                return;
        }
    }

    public final int getCount() {
        return this.f707a.b(o.ALL);
    }

    public final Object getItem(int i) {
        return this.f707a.a(o.ALL).get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        f fVar = (f) getItem(i);
        if (view == null) {
            view2 = ((LayoutInflater) this.f708b.getSystemService("layout_inflater")).inflate((int) C0000R.layout.inventory_list_item, (ViewGroup) null);
        } else {
            view2 = view;
        }
        ((TextView) view2.findViewById(C0000R.id.product_title)).setText(fVar.b());
        ((TextView) view2.findViewById(C0000R.id.product_price)).setText(fVar.h());
        ImageView imageView = (ImageView) view2.findViewById(C0000R.id.product_icon);
        String a2 = fVar.a();
        if (d.a(a2)) {
            imageView.setImageDrawable(d.b(a2));
        } else {
            imageView.setImageDrawable(null);
            new d(fVar.a(), new l(this));
        }
        ((TextView) view2.findViewById(C0000R.id.product_description)).setText(fVar.c());
        return view2;
    }
}
