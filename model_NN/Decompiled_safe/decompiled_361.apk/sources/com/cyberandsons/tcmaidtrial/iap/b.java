package com.cyberandsons.tcmaidtrial.iap;

import android.widget.Button;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.urbanairship.b.f;
import com.urbanairship.b.h;
import com.urbanairship.b.j;
import java.util.Observable;
import java.util.Observer;

final class b implements Observer {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ProductDetailActivity f709a;

    /* synthetic */ b(ProductDetailActivity productDetailActivity) {
        this(productDetailActivity, (byte) 0);
    }

    private b(ProductDetailActivity productDetailActivity, byte b2) {
        this.f709a = productDetailActivity;
    }

    public final void a(j jVar) {
        String str;
        Button button = (Button) this.f709a.findViewById(C0000R.id.detail_buy);
        TextView textView = (TextView) this.f709a.findViewById(C0000R.id.detail_item_product_price);
        switch (f.f715a[jVar.ordinal()]) {
            case 1:
                button.setEnabled(false);
                return;
            case 2:
                button.setEnabled(false);
                str = "Purchased";
                break;
            case 3:
                button.setEnabled(false);
                str = "Loading";
                break;
            case 4:
                button.setEnabled(false);
                str = "Installed";
                break;
            case 5:
                if (h.c()) {
                    button.setEnabled(true);
                } else {
                    button.setEnabled(false);
                }
                str = "Update";
                break;
            case 6:
                if (h.c()) {
                    button.setEnabled(true);
                } else {
                    button.setEnabled(false);
                }
                if (!this.f709a.f705a.g()) {
                    str = this.f709a.f705a.h() + "";
                    break;
                } else {
                    str = "Free";
                    break;
                }
            default:
                return;
        }
        textView.setText(str);
    }

    public final void update(Observable observable, Object obj) {
        a(((f) obj).j());
    }
}
