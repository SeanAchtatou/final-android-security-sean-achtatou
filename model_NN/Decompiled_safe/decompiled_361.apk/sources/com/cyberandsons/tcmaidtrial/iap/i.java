package com.cyberandsons.tcmaidtrial.iap;

import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.urbanairship.b.e;
import java.util.Observable;
import java.util.Observer;

final class i implements Observer {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ InventoryListActivity f718a;

    /* synthetic */ i(InventoryListActivity inventoryListActivity) {
        this(inventoryListActivity, (byte) 0);
    }

    private i(InventoryListActivity inventoryListActivity, byte b2) {
        this.f718a = inventoryListActivity;
    }

    public final void update(Observable observable, Object obj) {
        TextView textView = (TextView) this.f718a.findViewById(C0000R.id.inventory_loading_text);
        switch (g.f716a[((e) obj).a().ordinal()]) {
            case 1:
                textView.setText("No products found.");
                return;
            case 2:
                textView.setText("Inventory failed to load.");
                return;
            case 3:
                textView.setVisibility(8);
                return;
            default:
                return;
        }
    }
}
