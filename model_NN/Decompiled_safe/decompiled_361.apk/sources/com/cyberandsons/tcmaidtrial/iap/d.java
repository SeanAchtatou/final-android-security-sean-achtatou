package com.cyberandsons.tcmaidtrial.iap;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.urbanairship.a.j;

final class d extends j {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ImageView f712a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ ProductDetailActivity f713b;

    d(ProductDetailActivity productDetailActivity, ImageView imageView) {
        this.f713b = productDetailActivity;
        this.f712a = imageView;
    }

    public final void a(Drawable drawable) {
        this.f712a.setImageDrawable(drawable);
    }
}
