package com.cyberandsons.tcmaidtrial.iap;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.urbanairship.a.j;

final class c extends j {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ImageView f710a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ ProductDetailActivity f711b;

    c(ProductDetailActivity productDetailActivity, ImageView imageView) {
        this.f711b = productDetailActivity;
        this.f710a = imageView;
    }

    public final void a(Drawable drawable) {
        this.f710a.setImageDrawable(drawable);
    }
}
