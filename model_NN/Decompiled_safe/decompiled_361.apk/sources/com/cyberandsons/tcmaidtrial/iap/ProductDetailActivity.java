package com.cyberandsons.tcmaidtrial.iap;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.urbanairship.a.d;
import com.urbanairship.b.f;
import com.urbanairship.b.h;

public class ProductDetailActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public f f705a;

    /* renamed from: b  reason: collision with root package name */
    private b f706b;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.product_detail);
        Bundle extras = getIntent().getExtras();
        this.f705a = h.a().f().a(extras != null ? extras.getString("product_id") : "");
        this.f706b = new b(this);
        this.f705a.a(this.f706b);
        Button button = (Button) findViewById(C0000R.id.detail_buy);
        ((TextView) findViewById(C0000R.id.detail_item_product_title)).setText(this.f705a.b());
        ((TextView) findViewById(C0000R.id.detail_item_product_description)).setText(this.f705a.c());
        ((TextView) findViewById(C0000R.id.detail_item_product_revision)).setText("Revision: " + this.f705a.d());
        ((TextView) findViewById(C0000R.id.detail_item_product_filesize)).setText("File Size: " + this.f705a.e());
        this.f706b.a(this.f705a.j());
        new d(this.f705a.a(), new c(this, (ImageView) findViewById(C0000R.id.detail_item_product_icon)));
        if (!(this.f705a.f() == null || this.f705a.f().length() == 0)) {
            new d(this.f705a.f(), new d(this, (ImageView) findViewById(C0000R.id.detail_item_product_preview)));
        }
        button.setOnClickListener(new e(this));
    }

    public void onDestroy() {
        super.onDestroy();
        this.f705a.b(this.f706b);
    }
}
