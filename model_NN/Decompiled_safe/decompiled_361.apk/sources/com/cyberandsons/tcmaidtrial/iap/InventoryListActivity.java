package com.cyberandsons.tcmaidtrial.iap;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.urbanairship.b.f;
import com.urbanairship.b.h;

public class InventoryListActivity extends ListActivity {

    /* renamed from: a  reason: collision with root package name */
    private a f703a;

    /* renamed from: b  reason: collision with root package name */
    private i f704b;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.inventory_list);
        if (!h.c()) {
            showDialog(1);
        }
        this.f703a = new a(this);
        setListAdapter(this.f703a);
        this.f704b = new i(this);
        h.a().f().a(this.f704b);
    }

    public void onDestroy() {
        super.onDestroy();
        h.a().f().b(this.f704b);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Attention");
        builder.setIcon((int) C0000R.drawable.icon);
        builder.setMessage("The version of Android Market installed on this device does not support in-app purchase.");
        builder.setNeutralButton("OK", new h(this));
        return builder.create();
    }

    public void onListItemClick(ListView listView, View view, int i, long j) {
        Intent intent = new Intent(getBaseContext(), ProductDetailActivity.class);
        intent.putExtra("product_id", ((f) this.f703a.getItem(i)).i());
        startActivity(intent);
    }
}
