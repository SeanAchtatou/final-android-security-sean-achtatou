package com.cyberandsons.tcmaidtrial.misc;

import android.content.Intent;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.SearchConfigList;

final class cd implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsData f913a;

    cd(SettingsData settingsData) {
        this.f913a = settingsData;
    }

    public final void onClick(View view) {
        SettingsData settingsData = this.f913a;
        TcmAid.cO = 3;
        TcmAid.cP = "Herb Fields";
        settingsData.startActivityForResult(new Intent(settingsData, SearchConfigList.class), 1350);
    }
}
