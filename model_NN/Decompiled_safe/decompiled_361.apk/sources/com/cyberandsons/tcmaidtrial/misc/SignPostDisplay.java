package com.cyberandsons.tcmaidtrial.misc;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import com.cyberandsons.tcmaidtrial.C0000R;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SignPostDisplay extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private WebView f854a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f855b = true;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (dh.d) {
            setRequestedOrientation(1);
        }
        setContentView((int) C0000R.layout.signpost);
        this.f854a = (WebView) findViewById(C0000R.id.webview);
        this.f854a.setBackgroundColor(0);
        this.f854a.setBackgroundResource(C0000R.drawable.background);
        if (this.f855b) {
            this.f854a.loadUrl("file:///android_asset/signPostContent.html");
            return;
        }
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(getApplicationContext().getAssets().open("signPostContent.html"));
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader, 8192);
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine);
                } else {
                    inputStreamReader.close();
                    String sb2 = sb.toString();
                    bufferedReader.close();
                    this.f854a.loadDataWithBaseURL("file:///sdcard/data/data/com.cyberandsons.tcmaid/", sb2, "text/html", "utf-8", "");
                    return;
                }
            }
        } catch (IOException e) {
            Log.e("SPD:onCreate()", "IOE: " + e.getLocalizedMessage());
        }
    }
}
