package com.cyberandsons.tcmaidtrial.misc;

import android.content.Intent;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.SearchConfigList;

final class cx implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsData f933a;

    cx(SettingsData settingsData) {
        this.f933a = settingsData;
    }

    public final void onClick(View view) {
        SettingsData settingsData = this.f933a;
        TcmAid.cO = 2;
        TcmAid.cP = "Formula Fields";
        settingsData.startActivityForResult(new Intent(settingsData, SearchConfigList.class), 1350);
    }
}
