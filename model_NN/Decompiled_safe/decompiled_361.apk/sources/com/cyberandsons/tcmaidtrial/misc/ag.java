package com.cyberandsons.tcmaidtrial.misc;

import android.view.View;
import com.cyberandsons.tcmaidtrial.draw.colormixer.b;

final class ag implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsData f861a;

    ag(SettingsData settingsData) {
        this.f861a = settingsData;
    }

    public final void onClick(View view) {
        SettingsData settingsData = this.f861a;
        new b(settingsData, settingsData.h, new m(settingsData)).show();
    }
}
