package com.cyberandsons.tcmaidtrial.misc;

import android.view.View;

final class bp implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SendEmail f897a;

    bp(SendEmail sendEmail) {
        this.f897a = sendEmail;
    }

    public final void onClick(View view) {
        SendEmail sendEmail = this.f897a;
        sendEmail.setResult(2);
        sendEmail.finish();
    }
}
