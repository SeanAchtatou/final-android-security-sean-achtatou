package com.cyberandsons.tcmaidtrial.misc;

import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;

final class bs implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FavoriteLists f900a;

    bs(FavoriteLists favoriteLists) {
        this.f900a = favoriteLists;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        TextView textView = (TextView) view.findViewById(C0000R.id.text1);
        int parseInt = Integer.parseInt(((TextView) view.findViewById(C0000R.id.text0)).getText().toString());
        if (this.f900a.h.b(parseInt)) {
            this.f900a.h.a(parseInt, this.f900a.f847b);
            textView.setBackgroundResource(C0000R.color.favlistBGUnCheckedColor);
            return;
        }
        this.f900a.h.a(parseInt, this.f900a.f846a);
        textView.setBackgroundResource(C0000R.color.favlistBGCheckedColor);
    }
}
