package com.cyberandsons.tcmaidtrial.misc;

import android.content.Intent;
import android.view.View;

final class al implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FavoriteIntroduction f866a;

    al(FavoriteIntroduction favoriteIntroduction) {
        this.f866a = favoriteIntroduction;
    }

    public final void onClick(View view) {
        FavoriteIntroduction favoriteIntroduction = this.f866a;
        favoriteIntroduction.startActivityForResult(new Intent(favoriteIntroduction, FavoriteLists.class), 1350);
    }
}
