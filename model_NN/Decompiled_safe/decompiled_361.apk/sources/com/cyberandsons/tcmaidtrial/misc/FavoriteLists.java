package com.cyberandsons.tcmaidtrial.misc;

import android.app.Activity;
import android.app.AlertDialog;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TabHost;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.c;
import com.cyberandsons.tcmaidtrial.a.m;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.p;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.a.t;
import org.acra.ErrorReporter;

public class FavoriteLists extends Activity {
    private static String A;
    private static String B = o;
    private static String C = q;
    private static String F = v;
    private static boolean G;
    /* access modifiers changed from: private */
    public static String o = "point";
    /* access modifiers changed from: private */
    public static String q = "pathology";
    /* access modifiers changed from: private */
    public static String v = "_id";
    private static String z;
    private final String D = this.s;
    private final String E = this.u;
    /* access modifiers changed from: private */
    public TabHost H;
    private SQLiteCursor I;
    private SQLiteCursor J;
    private SQLiteCursor K;
    private SQLiteCursor L;
    private SQLiteCursor M;
    private SQLiteDatabase N;
    private n O;
    /* access modifiers changed from: private */
    public ListView P;
    /* access modifiers changed from: private */
    public ListView Q;
    /* access modifiers changed from: private */
    public ListView R;
    /* access modifiers changed from: private */
    public ListView S;
    /* access modifiers changed from: private */
    public ListView T;
    private int U;
    private int V;

    /* renamed from: a  reason: collision with root package name */
    boolean f846a = true;

    /* renamed from: b  reason: collision with root package name */
    boolean f847b;
    private long[] c = new long[64];
    /* access modifiers changed from: private */
    public c d;
    /* access modifiers changed from: private */
    public c e;
    /* access modifiers changed from: private */
    public c f;
    /* access modifiers changed from: private */
    public c g;
    /* access modifiers changed from: private */
    public c h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private final String n = "_id";
    private final String p = "_id";
    private final String r = "_id";
    /* access modifiers changed from: private */
    public String s = "pinyin";
    private final String t = "_id";
    /* access modifiers changed from: private */
    public String u = "pinyin";
    private final String w = "common_name";
    private String x;
    private String[] y;

    public FavoriteLists() {
        this.f847b = !this.f846a;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.N == null || !this.N.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("FL:openDataBase()", str + " opened.");
                this.N = SQLiteDatabase.openDatabase(str, null, 16);
                this.N.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.N.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("FL:openDataBase()", str2 + " ATTACHed.");
                this.O = new n();
                this.O.a(this.N);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new bj(this));
                create.show();
            }
        }
        try {
            if (dh.d) {
                setRequestedOrientation(1);
            }
            this.U = this.O.N();
            if (this.U == 0 || this.U == 1) {
                this.u = "pinyin";
            } else {
                this.u = "english";
            }
            this.V = this.O.O();
            if (this.V == 0 || this.V == 1) {
                this.s = "pinyin";
            } else if (this.V == 2) {
                this.s = "english";
            } else {
                this.s = "botanical";
            }
            setContentView((int) C0000R.layout.favoritelists);
            this.H = (TabHost) findViewById(C0000R.id.tabhost);
            this.H.setup();
            this.P = new ListView(this.H.getContext());
            this.P.setOnItemClickListener(new cb(this));
            this.Q = new ListView(this.H.getContext());
            this.Q.setOnItemClickListener(new cc(this));
            this.R = new ListView(this.H.getContext());
            this.R.setOnItemClickListener(new bz(this));
            this.S = new ListView(this.H.getContext());
            this.S.setOnItemClickListener(new ca(this));
            this.T = new ListView(this.H.getContext());
            this.T.setOnItemClickListener(new bs(this));
            TabHost.TabSpec newTabSpec = this.H.newTabSpec("auricularTab");
            newTabSpec.setContent(new bq(this));
            newTabSpec.setIndicator("Auricular");
            this.H.addTab(newTabSpec);
            TabHost.TabSpec newTabSpec2 = this.H.newTabSpec("diagnosisTab");
            newTabSpec2.setContent(new bw(this));
            newTabSpec2.setIndicator("Diagnosis");
            this.H.addTab(newTabSpec2);
            TabHost.TabSpec newTabSpec3 = this.H.newTabSpec("herbsTab");
            newTabSpec3.setContent(new bu(this));
            newTabSpec3.setIndicator("Herbs");
            this.H.addTab(newTabSpec3);
            TabHost.TabSpec newTabSpec4 = this.H.newTabSpec("formulasTab");
            newTabSpec4.setContent(new bm(this));
            newTabSpec4.setIndicator("Formulas");
            this.H.addTab(newTabSpec4);
            TabHost.TabSpec newTabSpec5 = this.H.newTabSpec("pointsTab");
            newTabSpec5.setContent(new bl(this));
            newTabSpec5.setIndicator("Points");
            this.H.addTab(newTabSpec5);
            this.d = new c(this.c);
            this.e = new c(this.c);
            this.f = new c(this.c);
            this.g = new c(this.c);
            this.h = new c(this.c);
            i();
        } catch (Exception e3) {
            ErrorReporter.a().a("myVariable", e3.getLocalizedMessage());
            ErrorReporter.a().handleSilentException(new Exception("FavL:onCreate()"));
            AlertDialog create2 = new AlertDialog.Builder(this).create();
            create2.setTitle("Attention:");
            create2.setMessage(getString(C0000R.string.area_list_exception));
            create2.setButton("OK", new bk(this));
            create2.show();
        }
    }

    public void onDestroy() {
        TcmAid.m = null;
        TcmAid.C = null;
        TcmAid.ad = null;
        TcmAid.Q = null;
        TcmAid.ap = null;
        boolean z2 = this.f846a;
        int c2 = this.d.c(0);
        while (c2 >= 0) {
            if (z2) {
                this.i = Integer.toString(c2);
                z2 = this.f847b;
            } else {
                this.i += ',' + Integer.toString(c2);
            }
            c2 = this.d.c(c2 + 1);
        }
        boolean z3 = this.f846a;
        int c3 = this.e.c(0);
        while (c3 >= 0) {
            if (z3) {
                this.j = Integer.toString(c3);
                z3 = this.f847b;
            } else {
                this.j += ',' + Integer.toString(c3);
            }
            c3 = this.e.c(c3 + 1);
        }
        boolean z4 = this.f846a;
        int c4 = this.f.c(0);
        while (c4 >= 0) {
            if (z4) {
                this.k = Integer.toString(c4);
                z4 = this.f847b;
            } else {
                this.k += ',' + Integer.toString(c4);
            }
            c4 = this.f.c(c4 + 1);
        }
        boolean z5 = this.f846a;
        int c5 = this.g.c(0);
        while (c5 >= 0) {
            if (z5) {
                this.l = Integer.toString(c5);
                z5 = this.f847b;
            } else {
                this.l += ',' + Integer.toString(c5);
            }
            c5 = this.g.c(c5 + 1);
        }
        boolean z6 = this.f846a;
        int c6 = this.h.c(0);
        while (c6 >= 0) {
            if (z6) {
                this.m = Integer.toString(c6);
                z6 = this.f847b;
            } else {
                this.m += ',' + Integer.toString(c6);
            }
            c6 = this.h.c(c6 + 1);
        }
        this.O.a(this.i, this.j, this.k, this.l, this.m, this.N);
        try {
            if (this.N != null && this.N.isOpen()) {
                this.N.close();
                this.N = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public SQLiteCursor d() {
        try {
            if (dh.u) {
                Log.d("createCursor()", "Inserting into t_auricular");
            }
            this.N.execSQL(p.a("t_auricular"));
            this.N.execSQL(p.a("t_auricular", "SELECT main.auricular._id, lower(main.auricular.point), main.auricular.pointcat, main.auricular.nada, main.auricular.battlefield, main.auricular.acacd, main.auricular.stopsmoking, main.auricular.weightloss FROM main.auricular " + " UNION " + "SELECT userDB.auricular._id, lower(userDB.auricular.point), userDB.auricular.pointcat, 0, 0, 0, 0, 0 FROM userDB.auricular " + " WHERE userDB.auricular._id>1000 ORDER BY 2"));
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            this.I = (SQLiteCursor) this.N.query(G, "t_auricular", new String[]{"_id", o}, this.x, this.y, z, A, B, null);
            startManagingCursor(this.I);
            int count = this.I.getCount();
            if (dh.u) {
                Log.d("createAuricualrCursor()", "query count = " + Integer.toString(count));
            }
            TcmAid.i.clear();
            if (this.I.moveToFirst()) {
                do {
                    TcmAid.i.add(Integer.valueOf(this.I.getInt(0)));
                } while (this.I.moveToNext());
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        return this.I;
    }

    /* access modifiers changed from: private */
    public SQLiteCursor e() {
        try {
            if (dh.u) {
                Log.d("createDiagnosisCursor()", "Inserting into t_zangfupathology");
            }
            this.N.execSQL("DELETE FROM t_zangfupathology");
            this.N.execSQL(t.a(t.c()));
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            this.J = (SQLiteCursor) this.N.query(G, "t_zangfupathology", new String[]{"_id", q}, this.x, this.y, z, A, C, null);
            startManagingCursor(this.J);
            int count = this.J.getCount();
            if (dh.u) {
                Log.d("createDiagnosisCursor()", "query count = " + Integer.toString(count));
            }
            TcmAid.v.clear();
            if (this.J.moveToFirst()) {
                do {
                    TcmAid.v.add(Integer.valueOf(this.J.getInt(0)));
                } while (this.J.moveToNext());
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        return this.J;
    }

    /* access modifiers changed from: private */
    public SQLiteCursor f() {
        try {
            if (dh.u) {
                Log.d("createHerbCursor()", "Inserting into t_herbs");
            }
            this.N.execSQL(m.a("t_herbs"));
            SQLiteDatabase sQLiteDatabase = this.N;
            String str = this.s;
            sQLiteDatabase.execSQL(m.a("t_herbs", m.b(str) + " UNION " + m.c(str) + " WHERE userDB.materiamedica._id>1000 ORDER BY 2", this.V));
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            this.K = (SQLiteCursor) this.N.query(G, "t_herbs", new String[]{"_id", this.s}, this.x, this.y, z, A, this.D, null);
            startManagingCursor(this.K);
            int count = this.K.getCount();
            if (dh.u) {
                Log.d("createHerbCursor()", "query count = " + Integer.toString(count));
            }
            TcmAid.Z.clear();
            if (this.K.moveToFirst()) {
                do {
                    TcmAid.Z.add(Integer.valueOf(this.K.getInt(0)));
                } while (this.K.moveToNext());
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        return this.K;
    }

    /* access modifiers changed from: private */
    public SQLiteCursor g() {
        try {
            if (dh.u) {
                Log.d("createFormulasCursor()", "Inserting into t_formulas");
            }
            this.N.execSQL(c.a("t_formulas"));
            SQLiteDatabase sQLiteDatabase = this.N;
            String str = this.u;
            sQLiteDatabase.execSQL(c.a("t_formulas", c.b(str) + " WHERE main.formulas.useiPhone=1 " + " UNION " + c.c(str) + " WHERE userDB.formulas._id>1000 ORDER BY 2 ", this.U));
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            this.L = (SQLiteCursor) this.N.query(G, "t_formulas", new String[]{"_id", this.u}, this.x, this.y, z, A, this.E, null);
            startManagingCursor(this.L);
            int count = this.L.getCount();
            if (dh.u) {
                Log.d("createFormulasCursor()", "query count = " + Integer.toString(count));
            }
            TcmAid.M.clear();
            if (this.L.moveToFirst()) {
                do {
                    TcmAid.M.add(Integer.valueOf(this.L.getInt(0)));
                } while (this.L.moveToNext());
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        return this.L;
    }

    /* access modifiers changed from: private */
    public SQLiteCursor h() {
        try {
            if (dh.u) {
                Log.d("createPointCursor()", "Inserting into tf_points");
            }
            this.N.execSQL("DELETE FROM tf_points");
            this.N.execSQL("INSERT INTO tf_points (_id, common_name) " + ("SELECT main.pointslocation._id, main.pointslocation.common_name FROM main.pointslocation " + " UNION " + "SELECT userDB.pointslocation._id, userDB.pointslocation.common_name FROM userDB.pointslocation " + " WHERE userDB.pointslocation._id>1000" + (this.O.B() ? "" : " ORDER BY 2, 3")));
        } catch (SQLException e2) {
            q.a(e2);
        }
        try {
            this.M = (SQLiteCursor) this.N.query(G, "tf_points", new String[]{v, "common_name"}, this.x, this.y, z, A, F, null);
            startManagingCursor(this.M);
            int count = this.M.getCount();
            if (dh.u) {
                Log.d("createPointCursor()", "query count = " + Integer.toString(count));
            }
            TcmAid.al.clear();
            if (this.M.moveToFirst()) {
                do {
                    TcmAid.al.add(Integer.valueOf(this.M.getInt(0)));
                } while (this.M.moveToNext());
            }
        } catch (SQLException e3) {
            q.a(e3);
        }
        return this.M;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x006f A[LOOP:0: B:14:0x006c->B:16:0x006f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a8 A[LOOP:1: B:33:0x00a5->B:35:0x00a8, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00ce A[LOOP:2: B:41:0x00cb->B:43:0x00ce, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00f4 A[LOOP:3: B:49:0x00f1->B:51:0x00f4, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0106  */
    /* JADX WARNING: Removed duplicated region for block: B:70:? A[ADDED_TO_REGION, ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void i() {
        /*
            r6 = this;
            r3 = 0
            r4 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT _id, useAuricularImages, useHerbImages, usePointImages, useTungImages, useWristAnkleImages, searchAuricular, searchDiagnosis, searchFormulas, searchHerbs, searchPoints, searchTung, searchWristAnkle, useEditCapitalize, useEditCorrection, displayAllAdjacent, pointsInColor, showOnlyPointNames, useFavorites,favoriteAuricular, favoriteDiags, favoriteHerbs, favoriteFormulas, favoritePoints, lastRelease,searchSixChannels, syncTimeStamp, email, doTraditional, usePointsLabel, formulaDisplayName, herbDisplayName, pushnotifregistered, auricularSearch, diagnosisSearch, formulaSearch, herbSearch, pointSearch, sixstageSearch, tungSearch, wristankleSearch, showFormulaChineseCharInList, showHerbChineseCharInList, showPointChineseCharInList, showTungChineseCharInList, showEnglishInPointList, showEnglishInTungList, initialTimeStamp, showPointMeridians, showAdjacentPointMeridians, alwaysShowAnswers, usePulseDiagImages, pulseDiagSearch, searchPulseDiag, useNonAndroidMarket, useHomeAsFavorite, useTraditionCharacters, hideLeftRightArrows, showNCCAOMHerbs, showNCCAOMFormulas, showCABHerbs, showCABFormulas, coldherbs, coolherbs, neutralherbs, warmherbs, hotherbs, useColoredHerbNames, allowQuickAdd, showChangeInNetworkStatus, useTanImages, showTanPoints, showTungPoints, searchTan, searchTcmIM, regKey  FROM userDB.settings WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cK
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.database.sqlite.SQLiteDatabase r1 = r6.N     // Catch:{ SQLException -> 0x007d, all -> 0x0088 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x007d, all -> 0x0088 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x007d, all -> 0x0088 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0132, all -> 0x0129 }
            r0.moveToFirst()     // Catch:{ SQLException -> 0x0132, all -> 0x0129 }
            r2 = 1
            if (r1 != r2) goto L_0x0052
            r1 = 19
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0132, all -> 0x0129 }
            r6.i = r1     // Catch:{ SQLException -> 0x0132, all -> 0x0129 }
            r1 = 20
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0132, all -> 0x0129 }
            r6.j = r1     // Catch:{ SQLException -> 0x0132, all -> 0x0129 }
            r1 = 21
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0132, all -> 0x0129 }
            r6.k = r1     // Catch:{ SQLException -> 0x0132, all -> 0x0129 }
            r1 = 22
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0132, all -> 0x0129 }
            r6.l = r1     // Catch:{ SQLException -> 0x0132, all -> 0x0129 }
            r1 = 23
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0132, all -> 0x0129 }
            r6.m = r1     // Catch:{ SQLException -> 0x0132, all -> 0x0129 }
        L_0x0052:
            if (r0 == 0) goto L_0x0057
            r0.close()
        L_0x0057:
            java.lang.String r0 = r6.i
            if (r0 == 0) goto L_0x0090
            java.lang.String r0 = r6.i
            int r0 = r0.length()
            if (r0 <= 0) goto L_0x0090
            java.lang.String r0 = r6.i
            java.lang.String r1 = ","
            java.lang.String[] r0 = r0.split(r1)
            r1 = r4
        L_0x006c:
            int r2 = r0.length
            if (r1 >= r2) goto L_0x0090
            r2 = r0[r1]
            int r2 = java.lang.Integer.parseInt(r2)
            com.cyberandsons.tcmaidtrial.misc.c r3 = r6.d
            r3.a(r2)
            int r1 = r1 + 1
            goto L_0x006c
        L_0x007d:
            r0 = move-exception
            r1 = r3
        L_0x007f:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x012f }
            if (r1 == 0) goto L_0x0057
            r1.close()
            goto L_0x0057
        L_0x0088:
            r0 = move-exception
            r1 = r3
        L_0x008a:
            if (r1 == 0) goto L_0x008f
            r1.close()
        L_0x008f:
            throw r0
        L_0x0090:
            java.lang.String r0 = r6.j
            if (r0 == 0) goto L_0x00b6
            java.lang.String r0 = r6.j
            int r0 = r0.length()
            if (r0 <= 0) goto L_0x00b6
            java.lang.String r0 = r6.j
            java.lang.String r1 = ","
            java.lang.String[] r0 = r0.split(r1)
            r1 = r4
        L_0x00a5:
            int r2 = r0.length
            if (r1 >= r2) goto L_0x00b6
            r2 = r0[r1]
            int r2 = java.lang.Integer.parseInt(r2)
            com.cyberandsons.tcmaidtrial.misc.c r3 = r6.e
            r3.a(r2)
            int r1 = r1 + 1
            goto L_0x00a5
        L_0x00b6:
            java.lang.String r0 = r6.k
            if (r0 == 0) goto L_0x00dc
            java.lang.String r0 = r6.k
            int r0 = r0.length()
            if (r0 <= 0) goto L_0x00dc
            java.lang.String r0 = r6.k
            java.lang.String r1 = ","
            java.lang.String[] r0 = r0.split(r1)
            r1 = r4
        L_0x00cb:
            int r2 = r0.length
            if (r1 >= r2) goto L_0x00dc
            r2 = r0[r1]
            int r2 = java.lang.Integer.parseInt(r2)
            com.cyberandsons.tcmaidtrial.misc.c r3 = r6.f
            r3.a(r2)
            int r1 = r1 + 1
            goto L_0x00cb
        L_0x00dc:
            java.lang.String r0 = r6.l
            if (r0 == 0) goto L_0x0102
            java.lang.String r0 = r6.l
            int r0 = r0.length()
            if (r0 <= 0) goto L_0x0102
            java.lang.String r0 = r6.l
            java.lang.String r1 = ","
            java.lang.String[] r0 = r0.split(r1)
            r1 = r4
        L_0x00f1:
            int r2 = r0.length
            if (r1 >= r2) goto L_0x0102
            r2 = r0[r1]
            int r2 = java.lang.Integer.parseInt(r2)
            com.cyberandsons.tcmaidtrial.misc.c r3 = r6.g
            r3.a(r2)
            int r1 = r1 + 1
            goto L_0x00f1
        L_0x0102:
            java.lang.String r0 = r6.m
            if (r0 == 0) goto L_0x0128
            java.lang.String r0 = r6.m
            int r0 = r0.length()
            if (r0 <= 0) goto L_0x0128
            java.lang.String r0 = r6.m
            java.lang.String r1 = ","
            java.lang.String[] r0 = r0.split(r1)
            r1 = r4
        L_0x0117:
            int r2 = r0.length
            if (r1 >= r2) goto L_0x0128
            r2 = r0[r1]
            int r2 = java.lang.Integer.parseInt(r2)
            com.cyberandsons.tcmaidtrial.misc.c r3 = r6.h
            r3.a(r2)
            int r1 = r1 + 1
            goto L_0x0117
        L_0x0128:
            return
        L_0x0129:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x008a
        L_0x012f:
            r0 = move-exception
            goto L_0x008a
        L_0x0132:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.misc.FavoriteLists.i():void");
    }
}
