package com.cyberandsons.tcmaidtrial.misc;

import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;

final class bh implements SimpleCursorAdapter.ViewBinder {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bq f888a;

    bh(bq bqVar) {
        this.f888a = bqVar;
    }

    public final boolean setViewValue(View view, Cursor cursor, int i) {
        if (i != 1) {
            return false;
        }
        TextView textView = (TextView) view;
        if (this.f888a.f898a.d.b(cursor.getInt(0))) {
            textView.setBackgroundResource(C0000R.color.favlistBGCheckedColor);
        } else {
            textView.setBackgroundResource(C0000R.color.favlistBGUnCheckedColor);
        }
        textView.setText(cursor.getString(1));
        return true;
    }
}
