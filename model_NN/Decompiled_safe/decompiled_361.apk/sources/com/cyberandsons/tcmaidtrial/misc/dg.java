package com.cyberandsons.tcmaidtrial.misc;

import android.view.View;

final class dg implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PreviousSearch f945a;

    dg(PreviousSearch previousSearch) {
        this.f945a = previousSearch;
    }

    public final void onClick(View view) {
        PreviousSearch previousSearch = this.f945a;
        previousSearch.setResult(2);
        previousSearch.finish();
    }
}
