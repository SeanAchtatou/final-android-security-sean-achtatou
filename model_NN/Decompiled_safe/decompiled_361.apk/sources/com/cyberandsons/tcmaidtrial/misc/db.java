package com.cyberandsons.tcmaidtrial.misc;

import android.app.AlertDialog;
import android.view.View;

final class db implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PreviousSearch f940a;

    db(PreviousSearch previousSearch) {
        this.f940a = previousSearch;
    }

    public final void onClick(View view) {
        PreviousSearch previousSearch = this.f940a;
        AlertDialog create = new AlertDialog.Builder(previousSearch).create();
        create.setTitle("Warning");
        create.setMessage("You can delete all previous search phrases or the current selected one! Choose below.");
        create.setButton("Delete All", new dd(previousSearch));
        create.setButton2("Delete Current", new de(previousSearch));
        create.setButton3("Cancel", new cz(previousSearch));
        create.show();
    }
}
