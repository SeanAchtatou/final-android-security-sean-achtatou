package com.cyberandsons.tcmaidtrial.misc;

import android.content.Intent;
import android.view.View;

final class x implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsData f967a;

    x(SettingsData settingsData) {
        this.f967a = settingsData;
    }

    public final void onClick(View view) {
        SettingsData settingsData = this.f967a;
        settingsData.startActivityForResult(new Intent(settingsData, FavoriteIntroduction.class), 1350);
    }
}
