package com.cyberandsons.tcmaidtrial.misc;

import android.view.View;
import com.cyberandsons.tcmaidtrial.draw.colormixer.b;

final class af implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsData f860a;

    af(SettingsData settingsData) {
        this.f860a = settingsData;
    }

    public final void onClick(View view) {
        SettingsData settingsData = this.f860a;
        new b(settingsData, settingsData.i, new n(settingsData)).show();
    }
}
