package com.cyberandsons.tcmaidtrial.misc;

import android.util.Log;

public final class c {
    private static final byte[] c = {-25, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 6, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 7, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 6, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0};

    /* renamed from: a  reason: collision with root package name */
    private long[] f908a;

    /* renamed from: b  reason: collision with root package name */
    private transient int f909b;

    private void a() {
        int i = this.f909b - 1;
        while (i >= 0 && this.f908a[i] == 0) {
            i--;
        }
        this.f909b = i + 1;
    }

    private c() {
        this.f908a = new long[1];
    }

    public c(long[] jArr) {
        if (jArr.length < 0) {
            throw new ArithmeticException("nbits < 0: " + jArr.length);
        }
        this.f908a = new long[jArr.length];
        for (int i = 0; i < jArr.length; i++) {
            this.f908a[i] = jArr[i];
        }
        this.f909b = jArr.length;
        a();
    }

    private void d(int i) {
        if (this.f908a.length < i) {
            long[] jArr = new long[Math.max(this.f908a.length * 2, i)];
            try {
                long[] jArr2 = this.f908a;
                int i2 = this.f909b;
                if (jArr2 == null) {
                    throw new NullPointerException();
                } else if (i2 < 0) {
                    throw new ArrayIndexOutOfBoundsException();
                } else if (jArr2 instanceof long[]) {
                    long[] jArr3 = jArr2;
                    long[] jArr4 = jArr;
                    if (i2 + 0 > jArr3.length || i2 + 0 > jArr4.length) {
                        throw new ArrayIndexOutOfBoundsException();
                    }
                    for (int i3 = 0; i3 < i2; i3++) {
                        jArr4[i3 + 0] = jArr3[i3 + 0];
                    }
                    this.f908a = jArr;
                } else {
                    throw new Exception();
                }
            } catch (Exception e) {
                Log.e("BS:ensureCapacity()", e.getLocalizedMessage());
            }
        }
    }

    public final void a(int i) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("bitIndex < 0: " + i);
        }
        int i2 = i >> 6;
        int i3 = i2 + 1;
        if (this.f909b < i3) {
            d(i3);
            long[] jArr = this.f908a;
            jArr[i2] = jArr[i2] | (1 << (i & 63));
            this.f909b = i3;
            return;
        }
        long[] jArr2 = this.f908a;
        jArr2[i2] = jArr2[i2] | (1 << (i & 63));
    }

    public final void a(int i, boolean z) {
        if (z) {
            a(i);
        } else if (i < 0) {
            throw new IndexOutOfBoundsException("bitIndex < 0: " + i);
        } else {
            int i2 = i >> 6;
            if (i2 < this.f909b) {
                long[] jArr = this.f908a;
                jArr[i2] = jArr[i2] & ((1 << (i & 63)) ^ -1);
                if (this.f908a[this.f909b - 1] == 0) {
                    a();
                }
            }
        }
    }

    public final boolean b(int i) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("bitIndex < 0: " + i);
        }
        int i2 = i >> 6;
        if (i2 >= this.f909b) {
            return false;
        }
        if ((this.f908a[i2] & (1 << (i & 63))) != 0) {
            return true;
        }
        return false;
    }

    /* JADX WARN: Type inference failed for: r0v29, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r0v30, types: [byte] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r0v30, types: [byte] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int c(int r12) {
        /*
            r11 = this;
            r8 = -1
            r6 = 0
            if (r12 >= 0) goto L_0x001e
            java.lang.IndexOutOfBoundsException r0 = new java.lang.IndexOutOfBoundsException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "fromIndex < 0: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x001e:
            int r0 = r12 >> 6
            int r1 = r11.f909b
            if (r0 < r1) goto L_0x0026
            r0 = r8
        L_0x0025:
            return r0
        L_0x0026:
            r1 = r12 & 63
            long[] r2 = r11.f908a
            r2 = r2[r0]
            long r2 = r2 >> r1
            int r4 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r4 != 0) goto L_0x00ca
            r1 = 0
            r9 = r2
            r2 = r0
            r3 = r1
            r0 = r9
        L_0x0036:
            int r4 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r4 != 0) goto L_0x0049
            int r4 = r11.f909b
            r5 = 1
            int r4 = r4 - r5
            if (r2 >= r4) goto L_0x0049
            long[] r0 = r11.f908a
            int r1 = r2 + 1
            r4 = r0[r1]
            r2 = r1
            r0 = r4
            goto L_0x0036
        L_0x0049:
            int r4 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r4 != 0) goto L_0x004f
            r0 = r8
            goto L_0x0025
        L_0x004f:
            int r4 = (int) r0
            r4 = r4 & 255(0xff, float:3.57E-43)
            if (r4 == 0) goto L_0x005d
            byte[] r0 = com.cyberandsons.tcmaidtrial.misc.c.c
            byte r0 = r0[r4]
        L_0x0058:
            int r0 = r0 + r3
            int r1 = r2 * 64
            int r0 = r0 + r1
            goto L_0x0025
        L_0x005d:
            r4 = 8
            long r4 = r0 >>> r4
            int r4 = (int) r4
            r4 = r4 & 255(0xff, float:3.57E-43)
            if (r4 == 0) goto L_0x006d
            byte[] r0 = com.cyberandsons.tcmaidtrial.misc.c.c
            byte r0 = r0[r4]
            int r0 = r0 + 8
            goto L_0x0058
        L_0x006d:
            r4 = 16
            long r4 = r0 >>> r4
            int r4 = (int) r4
            r4 = r4 & 255(0xff, float:3.57E-43)
            if (r4 == 0) goto L_0x007d
            byte[] r0 = com.cyberandsons.tcmaidtrial.misc.c.c
            byte r0 = r0[r4]
            int r0 = r0 + 16
            goto L_0x0058
        L_0x007d:
            r4 = 24
            long r4 = r0 >>> r4
            int r4 = (int) r4
            r4 = r4 & 255(0xff, float:3.57E-43)
            if (r4 == 0) goto L_0x008d
            byte[] r0 = com.cyberandsons.tcmaidtrial.misc.c.c
            byte r0 = r0[r4]
            int r0 = r0 + 24
            goto L_0x0058
        L_0x008d:
            r4 = 32
            long r4 = r0 >>> r4
            int r4 = (int) r4
            r4 = r4 & 255(0xff, float:3.57E-43)
            if (r4 == 0) goto L_0x009d
            byte[] r0 = com.cyberandsons.tcmaidtrial.misc.c.c
            byte r0 = r0[r4]
            int r0 = r0 + 32
            goto L_0x0058
        L_0x009d:
            r4 = 40
            long r4 = r0 >>> r4
            int r4 = (int) r4
            r4 = r4 & 255(0xff, float:3.57E-43)
            if (r4 == 0) goto L_0x00ad
            byte[] r0 = com.cyberandsons.tcmaidtrial.misc.c.c
            byte r0 = r0[r4]
            int r0 = r0 + 40
            goto L_0x0058
        L_0x00ad:
            r4 = 48
            long r4 = r0 >>> r4
            int r4 = (int) r4
            r4 = r4 & 255(0xff, float:3.57E-43)
            if (r4 == 0) goto L_0x00bd
            byte[] r0 = com.cyberandsons.tcmaidtrial.misc.c.c
            byte r0 = r0[r4]
            int r0 = r0 + 48
            goto L_0x0058
        L_0x00bd:
            r4 = 56
            long r0 = r0 >>> r4
            int r0 = (int) r0
            r0 = r0 & 255(0xff, float:3.57E-43)
            byte[] r1 = com.cyberandsons.tcmaidtrial.misc.c.c
            byte r0 = r1[r0]
            int r0 = r0 + 56
            goto L_0x0058
        L_0x00ca:
            r9 = r2
            r2 = r0
            r3 = r1
            r0 = r9
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.misc.c.c(int):int");
    }

    public final int hashCode() {
        long j = 1234;
        int length = this.f908a.length;
        while (true) {
            length--;
            if (length < 0) {
                return (int) ((j >> 32) ^ j);
            }
            j ^= this.f908a[length] * ((long) (length + 1));
        }
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof c)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        c cVar = (c) obj;
        int min = Math.min(this.f909b, cVar.f909b);
        for (int i = 0; i < min; i++) {
            if (this.f908a[i] != cVar.f908a[i]) {
                return false;
            }
        }
        if (this.f909b > min) {
            while (min < this.f909b) {
                if (this.f908a[min] != 0) {
                    return false;
                }
                min++;
            }
        } else {
            while (min < cVar.f909b) {
                if (cVar.f908a[min] != 0) {
                    return false;
                }
                min++;
            }
        }
        return true;
    }

    public final String toString() {
        int i = this.f909b << 6;
        StringBuffer stringBuffer = new StringBuffer((i * 8) + 2);
        stringBuffer.append('{');
        String str = "";
        for (int i2 = 0; i2 < i; i2++) {
            if (b(i2)) {
                stringBuffer.append(str);
                str = ", ";
                stringBuffer.append(i2);
            }
        }
        stringBuffer.append('}');
        return stringBuffer.toString();
    }
}
