package com.cyberandsons.tcmaidtrial.misc;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;

final class cb implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FavoriteLists f911a;

    cb(FavoriteLists favoriteLists) {
        this.f911a = favoriteLists;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        TextView textView = (TextView) view.findViewById(C0000R.id.text1);
        int parseInt = Integer.parseInt(((TextView) view.findViewById(C0000R.id.text0)).getText().toString());
        if (this.f911a.d.b(parseInt)) {
            if (dh.u) {
                Log.d("click", "auricularMemoryBits[" + Integer.toString(parseInt) + "] == NO, Text1=" + ((Object) textView.getText()));
            }
            this.f911a.d.a(parseInt, this.f911a.f847b);
            textView.setBackgroundResource(C0000R.color.favlistBGUnCheckedColor);
            return;
        }
        this.f911a.d.a(parseInt, this.f911a.f846a);
        if (dh.u) {
            Log.d("click", "auricularMemoryBits[" + Integer.toString(parseInt) + "] == YES, Text1=" + ((Object) textView.getText()));
        }
        textView.setBackgroundResource(C0000R.color.favlistBGCheckedColor);
    }
}
