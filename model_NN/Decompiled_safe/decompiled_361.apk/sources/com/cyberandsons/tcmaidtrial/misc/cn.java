package com.cyberandsons.tcmaidtrial.misc;

import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;

final class cn implements SimpleCursorAdapter.ViewBinder {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bw f923a;

    cn(bw bwVar) {
        this.f923a = bwVar;
    }

    public final boolean setViewValue(View view, Cursor cursor, int i) {
        if (i != 1) {
            return false;
        }
        TextView textView = (TextView) view;
        if (this.f923a.f904a.e.b(cursor.getInt(0))) {
            textView.setBackgroundResource(C0000R.color.favlistBGCheckedColor);
        } else {
            textView.setBackgroundResource(C0000R.color.favlistBGUnCheckedColor);
        }
        textView.setText(cursor.getString(1));
        return true;
    }
}
