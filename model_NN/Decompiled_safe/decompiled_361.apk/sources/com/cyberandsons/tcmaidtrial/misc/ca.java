package com.cyberandsons.tcmaidtrial.misc;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;

final class ca implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FavoriteLists f910a;

    ca(FavoriteLists favoriteLists) {
        this.f910a = favoriteLists;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        TextView textView = (TextView) view.findViewById(C0000R.id.text1);
        int parseInt = Integer.parseInt(((TextView) view.findViewById(C0000R.id.text0)).getText().toString());
        if (this.f910a.g.b(parseInt)) {
            if (dh.u) {
                Log.d("click", "formulasMemoryBits[" + Integer.toString(parseInt) + "] == NO, Text1=" + ((Object) textView.getText()));
            }
            this.f910a.g.a(parseInt, this.f910a.f847b);
            textView.setBackgroundResource(C0000R.color.favlistBGUnCheckedColor);
            return;
        }
        if (dh.u) {
            Log.d("click", "formulasMemoryBits[" + Integer.toString(parseInt) + "] == YES, Text1=" + ((Object) textView.getText()));
        }
        this.f910a.g.a(parseInt, this.f910a.f846a);
        textView.setBackgroundResource(C0000R.color.favlistBGCheckedColor);
    }
}
