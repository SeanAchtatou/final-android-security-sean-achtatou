package com.cyberandsons.tcmaidtrial.misc;

import android.view.View;
import com.cyberandsons.tcmaidtrial.draw.colormixer.b;

final class ae implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsData f859a;

    ae(SettingsData settingsData) {
        this.f859a = settingsData;
    }

    public final void onClick(View view) {
        SettingsData settingsData = this.f859a;
        new b(settingsData, settingsData.j, new o(settingsData)).show();
    }
}
