package com.cyberandsons.tcmaidtrial.misc;

import android.app.Activity;
import android.app.AlertDialog;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import com.cyberandsons.tcmaidtrial.network.ae;
import com.cyberandsons.tcmaidtrial.network.ag;
import com.cyberandsons.tcmaidtrial.network.e;
import com.cyberandsons.tcmaidtrial.network.m;
import com.cyberandsons.tcmaidtrial.network.v;
import com.cyberandsons.tcmaidtrial.network.y;
import com.cyberandsons.tcmaidtrial.network.z;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class SettingsData extends Activity {
    private static String at;
    private CheckBox A;
    private CheckBox B;
    private CheckBox C;
    private CheckBox D;
    private CheckBox E;
    private CheckBox F;
    private CheckBox G;
    private CheckBox H;
    private CheckBox I;
    private CheckBox J;
    private CheckBox K;
    private CheckBox L;
    private CheckBox M;
    private CheckBox N;
    private CheckBox O;
    private CheckBox P;
    private CheckBox Q;
    private CheckBox R;
    private CheckBox S;
    private CheckBox T;
    private CheckBox U;
    private CheckBox V;
    private CheckBox W;
    private CheckBox X;
    private CheckBox Y;
    private CheckBox Z;

    /* renamed from: a  reason: collision with root package name */
    Button f852a;
    private CheckBox aa;
    private CheckBox ab;
    private CheckBox ac;
    private Button ad;
    private CheckBox ae;
    private CheckBox af;
    private CheckBox ag;
    private CheckBox ah;
    private CheckBox ai;
    private CheckBox aj;
    private TextView ak;
    private TextView al;
    private TextView am;
    private TextView an;
    private TextView ao;
    /* access modifiers changed from: private */
    public SQLiteDatabase ap;
    /* access modifiers changed from: private */
    public n aq;
    private boolean ar = true;
    private boolean as;

    /* renamed from: b  reason: collision with root package name */
    Button f853b;
    Button c;
    Button d;
    Button e;
    int f;
    int g;
    int h;
    int i;
    int j;
    String k;
    private CheckBox l;
    private CheckBox m;
    private CheckBox n;
    private CheckBox o;
    private CheckBox p;
    private CheckBox q;
    private TextView r;
    private TextView s;
    private TextView t;
    private TextView u;
    private TextView v;
    private TextView w;
    private TextView x;
    private TextView y;
    private TextView z;

    public SettingsData() {
        this.as = !this.ar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:148:0x095f  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x0968  */
    /* JADX WARNING: Removed duplicated region for block: B:161:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r11) {
        /*
            r10 = this;
            r8 = 0
            r7 = 0
            r6 = 1
            super.onCreate(r11)
            android.database.sqlite.SQLiteDatabase r0 = r10.ap
            if (r0 == 0) goto L_0x0012
            android.database.sqlite.SQLiteDatabase r0 = r10.ap
            boolean r0 = r0.isOpen()
            if (r0 != 0) goto L_0x00d8
        L_0x0012:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x08bd }
            r0.<init>()     // Catch:{ SQLException -> 0x08bd }
            r1 = 2131099683(0x7f060023, float:1.7811726E38)
            java.lang.String r1 = r10.getString(r1)     // Catch:{ SQLException -> 0x08bd }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x08bd }
            r1 = 2131099657(0x7f060009, float:1.7811673E38)
            java.lang.String r1 = r10.getString(r1)     // Catch:{ SQLException -> 0x08bd }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x08bd }
            r1 = 2131099684(0x7f060024, float:1.7811728E38)
            java.lang.String r1 = r10.getString(r1)     // Catch:{ SQLException -> 0x08bd }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x08bd }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x08bd }
            r1 = 2131099686(0x7f060026, float:1.7811732E38)
            java.lang.String r1 = r10.getString(r1)     // Catch:{ SQLException -> 0x08bd }
            r2 = 2131099685(0x7f060025, float:1.781173E38)
            java.lang.String r2 = r10.getString(r2)     // Catch:{ SQLException -> 0x08bd }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x08bd }
            r3.<init>()     // Catch:{ SQLException -> 0x08bd }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ SQLException -> 0x08bd }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ SQLException -> 0x08bd }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x08bd }
            java.lang.String r3 = "SD:openDataBase()"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x08bd }
            r4.<init>()     // Catch:{ SQLException -> 0x08bd }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ SQLException -> 0x08bd }
            java.lang.String r5 = " opened."
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLException -> 0x08bd }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLException -> 0x08bd }
            android.util.Log.d(r3, r4)     // Catch:{ SQLException -> 0x08bd }
            r3 = 0
            r4 = 16
            android.database.sqlite.SQLiteDatabase r0 = android.database.sqlite.SQLiteDatabase.openDatabase(r0, r3, r4)     // Catch:{ SQLException -> 0x08bd }
            r10.ap = r0     // Catch:{ SQLException -> 0x08bd }
            java.lang.String r0 = "PRAGMA cache_size = 50"
            android.database.sqlite.SQLiteDatabase r3 = r10.ap     // Catch:{ SQLException -> 0x08bd }
            r3.execSQL(r0)     // Catch:{ SQLException -> 0x08bd }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x08bd }
            r0.<init>()     // Catch:{ SQLException -> 0x08bd }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x08bd }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ SQLException -> 0x08bd }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x08bd }
            android.database.sqlite.SQLiteDatabase r1 = r10.ap     // Catch:{ SQLException -> 0x08bd }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x08bd }
            r2.<init>()     // Catch:{ SQLException -> 0x08bd }
            java.lang.String r3 = "ATTACH \""
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x08bd }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ SQLException -> 0x08bd }
            java.lang.String r3 = "\" AS userDB"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLException -> 0x08bd }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLException -> 0x08bd }
            r1.execSQL(r2)     // Catch:{ SQLException -> 0x08bd }
            java.lang.String r1 = "SD:openDataBase()"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x08bd }
            r2.<init>()     // Catch:{ SQLException -> 0x08bd }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ SQLException -> 0x08bd }
            java.lang.String r2 = " ATTACHed."
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ SQLException -> 0x08bd }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x08bd }
            android.util.Log.d(r1, r0)     // Catch:{ SQLException -> 0x08bd }
            com.cyberandsons.tcmaidtrial.a.n r0 = new com.cyberandsons.tcmaidtrial.a.n     // Catch:{ SQLException -> 0x08bd }
            r0.<init>()     // Catch:{ SQLException -> 0x08bd }
            r10.aq = r0     // Catch:{ SQLException -> 0x08bd }
            com.cyberandsons.tcmaidtrial.a.n r0 = r10.aq     // Catch:{ SQLException -> 0x08bd }
            android.database.sqlite.SQLiteDatabase r1 = r10.ap     // Catch:{ SQLException -> 0x08bd }
            r0.a(r1)     // Catch:{ SQLException -> 0x08bd }
        L_0x00d8:
            boolean r0 = com.cyberandsons.tcmaidtrial.misc.dh.d
            if (r0 == 0) goto L_0x00df
            r10.setRequestedOrientation(r6)
        L_0x00df:
            r0 = 2130903120(0x7f030050, float:1.741305E38)
            r10.setContentView(r0)
            r0 = 2131362240(0x7f0a01c0, float:1.8344255E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.l = r0
            android.widget.CheckBox r0 = r10.l
            com.cyberandsons.tcmaidtrial.misc.cp r1 = new com.cyberandsons.tcmaidtrial.misc.cp
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362241(0x7f0a01c1, float:1.8344257E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.m = r0
            android.widget.CheckBox r0 = r10.m
            com.cyberandsons.tcmaidtrial.misc.cq r1 = new com.cyberandsons.tcmaidtrial.misc.cq
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362243(0x7f0a01c3, float:1.8344261E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.n = r0
            android.widget.CheckBox r0 = r10.n
            com.cyberandsons.tcmaidtrial.misc.cr r1 = new com.cyberandsons.tcmaidtrial.misc.cr
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362244(0x7f0a01c4, float:1.8344263E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.o = r0
            android.widget.CheckBox r0 = r10.o
            com.cyberandsons.tcmaidtrial.misc.cs r1 = new com.cyberandsons.tcmaidtrial.misc.cs
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362242(0x7f0a01c2, float:1.834426E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.p = r0
            android.widget.CheckBox r0 = r10.p
            com.cyberandsons.tcmaidtrial.misc.ct r1 = new com.cyberandsons.tcmaidtrial.misc.ct
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362245(0x7f0a01c5, float:1.8344265E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.q = r0
            android.widget.CheckBox r0 = r10.q
            com.cyberandsons.tcmaidtrial.misc.cu r1 = new com.cyberandsons.tcmaidtrial.misc.cu
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362247(0x7f0a01c7, float:1.834427E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.r = r0
            android.widget.TextView r0 = r10.r
            com.cyberandsons.tcmaidtrial.misc.cv r1 = new com.cyberandsons.tcmaidtrial.misc.cv
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362248(0x7f0a01c8, float:1.8344271E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.s = r0
            android.widget.TextView r0 = r10.s
            com.cyberandsons.tcmaidtrial.misc.cw r1 = new com.cyberandsons.tcmaidtrial.misc.cw
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362249(0x7f0a01c9, float:1.8344273E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.t = r0
            android.widget.TextView r0 = r10.t
            com.cyberandsons.tcmaidtrial.misc.cx r1 = new com.cyberandsons.tcmaidtrial.misc.cx
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362250(0x7f0a01ca, float:1.8344275E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.u = r0
            android.widget.TextView r0 = r10.u
            com.cyberandsons.tcmaidtrial.misc.cd r1 = new com.cyberandsons.tcmaidtrial.misc.cd
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362252(0x7f0a01cc, float:1.834428E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.v = r0
            android.widget.TextView r0 = r10.v
            com.cyberandsons.tcmaidtrial.misc.ce r1 = new com.cyberandsons.tcmaidtrial.misc.ce
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362253(0x7f0a01cd, float:1.8344281E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.w = r0
            android.widget.TextView r0 = r10.w
            com.cyberandsons.tcmaidtrial.misc.cf r1 = new com.cyberandsons.tcmaidtrial.misc.cf
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362254(0x7f0a01ce, float:1.8344283E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.x = r0
            android.widget.TextView r0 = r10.x
            com.cyberandsons.tcmaidtrial.misc.cg r1 = new com.cyberandsons.tcmaidtrial.misc.cg
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362251(0x7f0a01cb, float:1.8344277E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.y = r0
            android.widget.TextView r0 = r10.y
            com.cyberandsons.tcmaidtrial.misc.ci r1 = new com.cyberandsons.tcmaidtrial.misc.ci
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362255(0x7f0a01cf, float:1.8344285E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.z = r0
            android.widget.TextView r0 = r10.z
            com.cyberandsons.tcmaidtrial.misc.ch r1 = new com.cyberandsons.tcmaidtrial.misc.ch
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362257(0x7f0a01d1, float:1.834429E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.A = r0
            android.widget.CheckBox r0 = r10.A
            com.cyberandsons.tcmaidtrial.misc.ck r1 = new com.cyberandsons.tcmaidtrial.misc.ck
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362258(0x7f0a01d2, float:1.8344292E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.B = r0
            android.widget.CheckBox r0 = r10.B
            com.cyberandsons.tcmaidtrial.misc.cj r1 = new com.cyberandsons.tcmaidtrial.misc.cj
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362259(0x7f0a01d3, float:1.8344294E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.C = r0
            android.widget.CheckBox r0 = r10.C
            com.cyberandsons.tcmaidtrial.misc.cm r1 = new com.cyberandsons.tcmaidtrial.misc.cm
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362260(0x7f0a01d4, float:1.8344296E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.D = r0
            android.widget.CheckBox r0 = r10.D
            com.cyberandsons.tcmaidtrial.misc.cl r1 = new com.cyberandsons.tcmaidtrial.misc.cl
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362262(0x7f0a01d6, float:1.83443E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.E = r0
            android.widget.CheckBox r0 = r10.E
            com.cyberandsons.tcmaidtrial.misc.as r1 = new com.cyberandsons.tcmaidtrial.misc.as
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362263(0x7f0a01d7, float:1.8344302E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.F = r0
            android.widget.CheckBox r0 = r10.F
            com.cyberandsons.tcmaidtrial.misc.au r1 = new com.cyberandsons.tcmaidtrial.misc.au
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362261(0x7f0a01d5, float:1.8344298E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.H = r0
            android.widget.CheckBox r0 = r10.H
            com.cyberandsons.tcmaidtrial.misc.at r1 = new com.cyberandsons.tcmaidtrial.misc.at
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362264(0x7f0a01d8, float:1.8344304E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.G = r0
            android.widget.CheckBox r0 = r10.G
            com.cyberandsons.tcmaidtrial.misc.aw r1 = new com.cyberandsons.tcmaidtrial.misc.aw
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362265(0x7f0a01d9, float:1.8344306E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.I = r0
            android.widget.CheckBox r0 = r10.I
            com.cyberandsons.tcmaidtrial.misc.av r1 = new com.cyberandsons.tcmaidtrial.misc.av
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362267(0x7f0a01db, float:1.834431E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.J = r0
            android.widget.CheckBox r0 = r10.J
            com.cyberandsons.tcmaidtrial.misc.an r1 = new com.cyberandsons.tcmaidtrial.misc.an
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362268(0x7f0a01dc, float:1.8344312E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.K = r0
            android.widget.CheckBox r0 = r10.K
            com.cyberandsons.tcmaidtrial.misc.ao r1 = new com.cyberandsons.tcmaidtrial.misc.ao
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362270(0x7f0a01de, float:1.8344316E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.L = r0
            android.widget.CheckBox r0 = r10.L
            com.cyberandsons.tcmaidtrial.misc.ap r1 = new com.cyberandsons.tcmaidtrial.misc.ap
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362269(0x7f0a01dd, float:1.8344314E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.M = r0
            android.widget.CheckBox r0 = r10.M
            com.cyberandsons.tcmaidtrial.misc.aq r1 = new com.cyberandsons.tcmaidtrial.misc.aq
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362271(0x7f0a01df, float:1.8344318E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.N = r0
            android.widget.CheckBox r0 = r10.N
            com.cyberandsons.tcmaidtrial.misc.ar r1 = new com.cyberandsons.tcmaidtrial.misc.ar
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362272(0x7f0a01e0, float:1.834432E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.T = r0
            android.widget.CheckBox r0 = r10.T
            com.cyberandsons.tcmaidtrial.misc.bc r1 = new com.cyberandsons.tcmaidtrial.misc.bc
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362273(0x7f0a01e1, float:1.8344322E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.O = r0
            android.widget.CheckBox r0 = r10.O
            com.cyberandsons.tcmaidtrial.misc.bb r1 = new com.cyberandsons.tcmaidtrial.misc.bb
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362274(0x7f0a01e2, float:1.8344324E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.P = r0
            android.widget.CheckBox r0 = r10.P
            com.cyberandsons.tcmaidtrial.misc.bg r1 = new com.cyberandsons.tcmaidtrial.misc.bg
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362275(0x7f0a01e3, float:1.8344326E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.Q = r0
            android.widget.CheckBox r0 = r10.Q
            com.cyberandsons.tcmaidtrial.misc.bf r1 = new com.cyberandsons.tcmaidtrial.misc.bf
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362276(0x7f0a01e4, float:1.8344328E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.R = r0
            android.widget.CheckBox r0 = r10.R
            com.cyberandsons.tcmaidtrial.misc.be r1 = new com.cyberandsons.tcmaidtrial.misc.be
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362277(0x7f0a01e5, float:1.834433E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.S = r0
            android.widget.CheckBox r0 = r10.S
            com.cyberandsons.tcmaidtrial.misc.bd r1 = new com.cyberandsons.tcmaidtrial.misc.bd
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362279(0x7f0a01e7, float:1.8344334E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.U = r0
            android.widget.CheckBox r0 = r10.U
            com.cyberandsons.tcmaidtrial.misc.az r1 = new com.cyberandsons.tcmaidtrial.misc.az
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362280(0x7f0a01e8, float:1.8344336E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.V = r0
            android.widget.CheckBox r0 = r10.V
            com.cyberandsons.tcmaidtrial.misc.ba r1 = new com.cyberandsons.tcmaidtrial.misc.ba
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362281(0x7f0a01e9, float:1.8344338E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.W = r0
            android.widget.CheckBox r0 = r10.W
            com.cyberandsons.tcmaidtrial.misc.ax r1 = new com.cyberandsons.tcmaidtrial.misc.ax
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362282(0x7f0a01ea, float:1.834434E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.X = r0
            android.widget.CheckBox r0 = r10.X
            com.cyberandsons.tcmaidtrial.misc.ay r1 = new com.cyberandsons.tcmaidtrial.misc.ay
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362283(0x7f0a01eb, float:1.8344342E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.ae = r0
            android.widget.CheckBox r0 = r10.ae
            com.cyberandsons.tcmaidtrial.misc.y r1 = new com.cyberandsons.tcmaidtrial.misc.y
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362284(0x7f0a01ec, float:1.8344344E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.af = r0
            android.widget.CheckBox r0 = r10.af
            com.cyberandsons.tcmaidtrial.misc.aa r1 = new com.cyberandsons.tcmaidtrial.misc.aa
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362278(0x7f0a01e6, float:1.8344332E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.ag = r0
            android.widget.CheckBox r0 = r10.ag
            com.cyberandsons.tcmaidtrial.misc.z r1 = new com.cyberandsons.tcmaidtrial.misc.z
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362301(0x7f0a01fd, float:1.8344379E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.ah = r0
            android.widget.CheckBox r0 = r10.ah
            com.cyberandsons.tcmaidtrial.misc.v r1 = new com.cyberandsons.tcmaidtrial.misc.v
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362303(0x7f0a01ff, float:1.8344383E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.ai = r0
            android.widget.CheckBox r0 = r10.ai
            com.cyberandsons.tcmaidtrial.misc.u r1 = new com.cyberandsons.tcmaidtrial.misc.u
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362300(0x7f0a01fc, float:1.8344377E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.ak = r0
            android.widget.TextView r0 = r10.ak
            com.cyberandsons.tcmaidtrial.misc.x r1 = new com.cyberandsons.tcmaidtrial.misc.x
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362294(0x7f0a01f6, float:1.8344365E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.Y = r0
            android.widget.CheckBox r0 = r10.Y
            r0.setVisibility(r7)
            android.widget.CheckBox r0 = r10.Y
            com.cyberandsons.tcmaidtrial.misc.w r1 = new com.cyberandsons.tcmaidtrial.misc.w
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362297(0x7f0a01f9, float:1.834437E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.Z = r0
            android.widget.CheckBox r0 = r10.Z
            r0.setVisibility(r7)
            android.widget.CheckBox r0 = r10.Z
            com.cyberandsons.tcmaidtrial.misc.r r1 = new com.cyberandsons.tcmaidtrial.misc.r
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362295(0x7f0a01f7, float:1.8344367E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.aa = r0
            android.widget.CheckBox r0 = r10.aa
            r0.setVisibility(r7)
            android.widget.CheckBox r0 = r10.aa
            com.cyberandsons.tcmaidtrial.misc.s r1 = new com.cyberandsons.tcmaidtrial.misc.s
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362298(0x7f0a01fa, float:1.8344373E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.ab = r0
            android.widget.CheckBox r0 = r10.ab
            r0.setVisibility(r7)
            android.widget.CheckBox r0 = r10.ab
            com.cyberandsons.tcmaidtrial.misc.t r1 = new com.cyberandsons.tcmaidtrial.misc.t
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362296(0x7f0a01f8, float:1.8344369E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.al = r0
            android.widget.TextView r0 = r10.al
            com.cyberandsons.tcmaidtrial.misc.am r1 = new com.cyberandsons.tcmaidtrial.misc.am
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362286(0x7f0a01ee, float:1.8344348E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.am = r0
            android.widget.TextView r0 = r10.am
            com.cyberandsons.tcmaidtrial.misc.ak r1 = new com.cyberandsons.tcmaidtrial.misc.ak
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362287(0x7f0a01ef, float:1.834435E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.ac = r0
            android.widget.CheckBox r0 = r10.ac
            com.cyberandsons.tcmaidtrial.misc.aj r1 = new com.cyberandsons.tcmaidtrial.misc.aj
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362288(0x7f0a01f0, float:1.8344352E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r10.f852a = r0
            android.widget.Button r0 = r10.f852a
            com.cyberandsons.tcmaidtrial.misc.ai r1 = new com.cyberandsons.tcmaidtrial.misc.ai
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362289(0x7f0a01f1, float:1.8344354E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r10.f853b = r0
            android.widget.Button r0 = r10.f853b
            com.cyberandsons.tcmaidtrial.misc.ah r1 = new com.cyberandsons.tcmaidtrial.misc.ah
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362290(0x7f0a01f2, float:1.8344356E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r10.c = r0
            android.widget.Button r0 = r10.c
            com.cyberandsons.tcmaidtrial.misc.ag r1 = new com.cyberandsons.tcmaidtrial.misc.ag
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362291(0x7f0a01f3, float:1.8344358E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r10.d = r0
            android.widget.Button r0 = r10.d
            com.cyberandsons.tcmaidtrial.misc.af r1 = new com.cyberandsons.tcmaidtrial.misc.af
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362292(0x7f0a01f4, float:1.834436E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r10.e = r0
            android.widget.Button r0 = r10.e
            com.cyberandsons.tcmaidtrial.misc.ae r1 = new com.cyberandsons.tcmaidtrial.misc.ae
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362293(0x7f0a01f5, float:1.8344362E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r10.ad = r0
            android.widget.Button r0 = r10.ad
            com.cyberandsons.tcmaidtrial.misc.ab r1 = new com.cyberandsons.tcmaidtrial.misc.ab
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362305(0x7f0a0201, float:1.8344387E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.CheckBox r0 = (android.widget.CheckBox) r0
            r10.aj = r0
            android.widget.CheckBox r0 = r10.aj
            com.cyberandsons.tcmaidtrial.misc.ac r1 = new com.cyberandsons.tcmaidtrial.misc.ac
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r0 = 2131362306(0x7f0a0202, float:1.8344389E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.an = r0
            r0 = 2131362307(0x7f0a0203, float:1.834439E38)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r10.ao = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT _id, useAuricularImages, useHerbImages, usePointImages, useTungImages, useWristAnkleImages, searchAuricular, searchDiagnosis, searchFormulas, searchHerbs, searchPoints, searchTung, searchWristAnkle, useEditCapitalize, useEditCorrection, displayAllAdjacent, pointsInColor, showOnlyPointNames, useFavorites,favoriteAuricular, favoriteDiags, favoriteHerbs, favoriteFormulas, favoritePoints, lastRelease,searchSixChannels, syncTimeStamp, email, doTraditional, usePointsLabel, formulaDisplayName, herbDisplayName, pushnotifregistered, auricularSearch, diagnosisSearch, formulaSearch, herbSearch, pointSearch, sixstageSearch, tungSearch, wristankleSearch, showFormulaChineseCharInList, showHerbChineseCharInList, showPointChineseCharInList, showTungChineseCharInList, showEnglishInPointList, showEnglishInTungList, initialTimeStamp, showPointMeridians, showAdjacentPointMeridians, alwaysShowAnswers, usePulseDiagImages, pulseDiagSearch, searchPulseDiag, useNonAndroidMarket, useHomeAsFavorite, useTraditionCharacters, hideLeftRightArrows, showNCCAOMHerbs, showNCCAOMFormulas, showCABHerbs, showCABFormulas, coldherbs, coolherbs, neutralherbs, warmherbs, hotherbs, useColoredHerbNames, allowQuickAdd, showChangeInNetworkStatus, useTanImages, showTanPoints, showTungPoints, searchTan, searchTcmIM, regKey  FROM userDB.settings WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cK
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.database.sqlite.SQLiteDatabase r1 = r10.ap     // Catch:{ SQLException -> 0x0958, all -> 0x0964 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x0958, all -> 0x0964 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0958, all -> 0x0964 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r0.moveToFirst()     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r1 != r6) goto L_0x08b7
            android.widget.CheckBox r1 = r10.l     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 1
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x08e0
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x0618:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.m     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 2
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x08e4
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x0626:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.n     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 3
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x08e8
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x0634:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.p     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 4
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x08ec
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x0642:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.q     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 5
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x08f0
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x0650:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.A     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.B     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.C     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.D     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.E     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.H     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.G     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.I     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.J     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 13
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x08f4
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x0697:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.K     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 14
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x08f8
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x06a6:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.L     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 68
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x08fc
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x06b5:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.M     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 57
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0900
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x06c4:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.N     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 15
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0904
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x06d3:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.O     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 16
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0908
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x06e2:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.P     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 17
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x090c
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x06f1:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.Q     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 28
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0910
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x0700:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.R     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 29
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0914
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x070f:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.S     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 48
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0918
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x071e:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.T     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 49
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x091c
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x072d:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.U     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 41
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0920
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x073c:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.V     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 42
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0924
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x074b:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.W     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 43
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0928
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x075a:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.X     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 44
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x092c
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x0769:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.ae     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 45
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0930
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x0778:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.af     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 46
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0934
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x0787:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.ah     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 55
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0938
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x0796:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.ai     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 69
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x093c
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x07a5:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.ag     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 56
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0940
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x07b4:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.aa     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 60
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0944
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x07c3:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.ab     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 61
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0948
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x07d2:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.Y     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 58
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x094c
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x07e1:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.Z     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 59
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0950
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x07f0:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.ac     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = 67
            int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r2 != r6) goto L_0x0954
            boolean r2 = r10.ar     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x07ff:
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1 = 62
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r10.f = r1     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1 = 63
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r10.g = r1     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1 = 64
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r10.h = r1     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1 = 65
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r10.i = r1     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1 = 66
            int r1 = r0.getInt(r1)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r10.j = r1     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.aj     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setChecked(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.f852a     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            int r2 = r10.f     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setTextColor(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.f853b     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            int r2 = r10.g     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setTextColor(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.c     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            int r2 = r10.h     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setTextColor(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.d     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            int r2 = r10.i     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setTextColor(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.e     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            int r2 = r10.j     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setTextColor(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.ad     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r2 = -256(0xffffffffffffff00, float:NaN)
            r1.setTextColor(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.CheckBox r1 = r10.ac     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r1 = r1.isChecked()     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            if (r1 != 0) goto L_0x08b7
            android.widget.Button r1 = r10.f852a     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setEnabled(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.f853b     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setEnabled(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.c     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setEnabled(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.d     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setEnabled(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.e     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setEnabled(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.ad     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setEnabled(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.f852a     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setPressed(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.f853b     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setPressed(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.c     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setPressed(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.d     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setPressed(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.e     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setPressed(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            android.widget.Button r1 = r10.ad     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            r1.setPressed(r2)     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
        L_0x08b7:
            if (r0 == 0) goto L_0x08bc
            r0.close()
        L_0x08bc:
            return
        L_0x08bd:
            r0 = move-exception
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r10)
            android.app.AlertDialog r0 = r0.create()
            java.lang.String r1 = "Attention:"
            r0.setTitle(r1)
            java.lang.String r1 = "Please restart TCM Clinic Aid. The database was not able to reopen."
            r0.setMessage(r1)
            java.lang.String r1 = "OK"
            com.cyberandsons.tcmaidtrial.misc.q r2 = new com.cyberandsons.tcmaidtrial.misc.q
            r2.<init>(r10)
            r0.setButton(r1, r2)
            r0.show()
            goto L_0x00d8
        L_0x08e0:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x0618
        L_0x08e4:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x0626
        L_0x08e8:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x0634
        L_0x08ec:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x0642
        L_0x08f0:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x0650
        L_0x08f4:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x0697
        L_0x08f8:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x06a6
        L_0x08fc:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x06b5
        L_0x0900:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x06c4
        L_0x0904:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x06d3
        L_0x0908:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x06e2
        L_0x090c:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x06f1
        L_0x0910:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x0700
        L_0x0914:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x070f
        L_0x0918:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x071e
        L_0x091c:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x072d
        L_0x0920:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x073c
        L_0x0924:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x074b
        L_0x0928:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x075a
        L_0x092c:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x0769
        L_0x0930:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x0778
        L_0x0934:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x0787
        L_0x0938:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x0796
        L_0x093c:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x07a5
        L_0x0940:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x07b4
        L_0x0944:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x07c3
        L_0x0948:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x07d2
        L_0x094c:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x07e1
        L_0x0950:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x07f0
        L_0x0954:
            boolean r2 = r10.as     // Catch:{ SQLException -> 0x0973, all -> 0x096c }
            goto L_0x07ff
        L_0x0958:
            r0 = move-exception
            r1 = r8
        L_0x095a:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x0971 }
            if (r1 == 0) goto L_0x08bc
            r1.close()
            goto L_0x08bc
        L_0x0964:
            r0 = move-exception
            r1 = r8
        L_0x0966:
            if (r1 == 0) goto L_0x096b
            r1.close()
        L_0x096b:
            throw r0
        L_0x096c:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0966
        L_0x0971:
            r0 = move-exception
            goto L_0x0966
        L_0x0973:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x095a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.misc.SettingsData.onCreate(android.os.Bundle):void");
    }

    public void onDestroy() {
        try {
            if (this.ap != null && this.ap.isOpen()) {
                this.ap.close();
                this.ap = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Display Herbs by ...");
        builder.setSingleChoiceItems(new CharSequence[]{"Pinyin w/Simplified Characters", "Pinyin w/Traditional Characters", "English", "Phamaceutical"}, this.aq.O(), new h(this));
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Display Formulas by ...");
        builder.setSingleChoiceItems(new CharSequence[]{"Pinyin w/Simplified Characters", "Pinyin w/Traditional Characters", "English"}, this.aq.N(), new i(this));
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public final void c() {
        boolean z2 = !TcmAid.cL;
        TcmAid.cL = z2;
        if (z2) {
            this.an.setVisibility(0);
            this.an.setText("Address");
            this.ao.setVisibility(0);
            try {
                at = "http://" + S() + ':' + Integer.toString(8080);
                this.ao.setText(at);
                File file = new File(getApplicationContext().getString(C0000R.string.httpRoot));
                v vVar = new v(8080);
                TcmAid.cN = vVar;
                e a2 = vVar.a((String) null);
                TcmAid.cM = a2;
                a2.d();
                TcmAid.cM.a("/", new ag(file, "/"));
                TcmAid.cM.a("/files", new j(this));
                TcmAid.cN.a();
            } catch (SocketException e2) {
                Log.e("SD:enableWlanCB_Click()", "SE: " + e2.getLocalizedMessage());
            } catch (IOException e3) {
                Log.e("SD:enableWlanCB_Click()", "IOE: " + e3.getLocalizedMessage());
            }
        } else {
            TcmAid.cN.b();
            this.ao.setVisibility(4);
            this.an.setVisibility(4);
        }
    }

    public final int a(ae aeVar, m mVar) {
        int i2;
        Log.i("Received request: ", aeVar.f());
        String string = getString(C0000R.string.templatePath);
        String string2 = getString(C0000R.string.unlockFileName);
        String string3 = getString(C0000R.string.tcmDatabasePath);
        String string4 = getString(C0000R.string.tcmUserDatabase);
        if (aeVar.f().startsWith("/file") && aeVar.e().e("Content-Type").containsKey("multipart/form-data")) {
            y yVar = new y(aeVar);
            try {
                if (yVar.hasNext()) {
                    z a2 = yVar.next();
                    if (a2.f1024b.equalsIgnoreCase(string2)) {
                        FileOutputStream fileOutputStream = new FileOutputStream(new File(string, string2));
                        int i3 = 0;
                        boolean z2 = true;
                        while (z2) {
                            int read = a2.d.read();
                            if (read == -1) {
                                z2 = false;
                            } else {
                                fileOutputStream.write(read);
                                i3++;
                            }
                        }
                        fileOutputStream.close();
                        Log.i("SD:handle()", String.format("%s - len = %d", string2, Integer.valueOf(i3)));
                        aeVar.h();
                        aeVar.a("/");
                        try {
                            mVar.a(at);
                            return 301;
                        } catch (IOException e2) {
                            e = e2;
                            i2 = 301;
                            Log.i("SD:handler()", e.getLocalizedMessage());
                            return i2;
                        }
                    } else if (a2.f1024b.equalsIgnoreCase(string4)) {
                        FileOutputStream fileOutputStream2 = new FileOutputStream(new File(string3, string4));
                        int i4 = 0;
                        boolean z3 = true;
                        while (z3) {
                            int read2 = a2.d.read();
                            if (read2 == -1) {
                                z3 = false;
                            } else {
                                fileOutputStream2.write(read2);
                                i4++;
                            }
                        }
                        fileOutputStream2.close();
                        Log.i("SD:handle()", String.format("%s - len = %d", string4, Integer.valueOf(i4)));
                        aeVar.h();
                        aeVar.a("/");
                        mVar.a(at);
                        return 301;
                    }
                }
            } catch (IOException e3) {
                e = e3;
                i2 = 0;
                Log.i("SD:handler()", e.getLocalizedMessage());
                return i2;
            }
        }
        return 0;
    }

    private static String S() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress();
                        }
                    }
                }
            }
        } catch (SocketException e2) {
            Log.e("SettingsData:getLocalIpAddress()", e2.toString());
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.aq.t(this.U.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        this.aq.o(this.V.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void f() {
        this.aq.r(this.W.isChecked(), this.ap);
        if (this.aq.U() == 1 && this.aq.R() == 1) {
            this.aq.p(this.as, this.ap);
            this.ae.setChecked(this.as);
        }
    }

    /* access modifiers changed from: protected */
    public final void g() {
        this.aq.s(this.X.isChecked(), this.ap);
        if (this.aq.V() == 1 && this.aq.S() == 1) {
            this.aq.q(this.as, this.ap);
            this.af.setChecked(this.as);
        }
    }

    /* access modifiers changed from: protected */
    public final void h() {
        this.aq.p(this.ae.isChecked(), this.ap);
        if (this.aq.R() == 1 && this.aq.U() == 1) {
            this.aq.r(this.as, this.ap);
            this.W.setChecked(this.as);
        }
    }

    /* access modifiers changed from: protected */
    public final void i() {
        this.aq.q(this.af.isChecked(), this.ap);
        if (this.aq.S() == 1 && this.aq.V() == 1) {
            this.aq.s(this.as, this.ap);
            this.X.setChecked(this.as);
        }
    }

    /* access modifiers changed from: protected */
    public final void j() {
        this.aq.c(this.ag.isChecked() ? 1 : 0, this.ap);
    }

    /* access modifiers changed from: protected */
    public final void k() {
        this.aq.z(this.ah.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void l() {
        this.aq.u(this.ai.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void m() {
        this.aq.m(this.R.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void n() {
        this.aq.v(this.S.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void o() {
        this.aq.w(this.T.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void p() {
        this.aq.l(this.Q.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void q() {
        this.aq.f(this.P.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void r() {
        this.aq.e(this.O.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void s() {
        this.aq.d(this.N.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void t() {
        this.aq.i(this.K.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void u() {
        this.aq.G(this.L.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void v() {
        this.aq.A(this.M.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void w() {
        this.aq.h(this.J.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void x() {
        this.aq.B(this.Y.isChecked(), this.ap);
        if (this.aq.ae() == this.ar && this.aq.ag() == this.ar) {
            this.aq.D(this.as, this.ap);
            this.aa.setChecked(this.as);
        }
    }

    /* access modifiers changed from: protected */
    public final void y() {
        this.aq.C(this.Z.isChecked(), this.ap);
        if (this.aq.af() == this.ar && this.aq.ah() == this.ar) {
            this.aq.E(this.as, this.ap);
            this.ab.setChecked(this.as);
        }
    }

    /* access modifiers changed from: protected */
    public final void z() {
        this.aq.D(this.aa.isChecked(), this.ap);
        if (this.aq.ag() == this.ar && this.aq.ae() == this.ar) {
            this.aq.B(this.as, this.ap);
            this.Y.setChecked(this.as);
        }
    }

    /* access modifiers changed from: protected */
    public final void A() {
        this.aq.E(this.ab.isChecked(), this.ap);
        if (this.aq.ah() == this.ar && this.aq.af() == this.ar) {
            this.aq.C(this.as, this.ap);
            this.Z.setChecked(this.as);
        }
    }

    /* access modifiers changed from: protected */
    public final void B() {
        this.aq.F(this.ac.isChecked(), this.ap);
        if (this.aq.an() == this.ar) {
            this.f852a.setEnabled(this.ar);
            this.f853b.setEnabled(this.ar);
            this.c.setEnabled(this.ar);
            this.d.setEnabled(this.ar);
            this.e.setEnabled(this.ar);
            this.ad.setEnabled(this.ar);
            return;
        }
        this.f852a.setEnabled(this.as);
        this.f853b.setEnabled(this.as);
        this.c.setEnabled(this.as);
        this.d.setEnabled(this.as);
        this.e.setEnabled(this.as);
        this.ad.setEnabled(this.as);
    }

    /* access modifiers changed from: protected */
    public final void C() {
        this.f = -16776961;
        this.aq.d(this.f, this.ap);
        this.f852a.setTextColor(this.f);
        this.g = -13245185;
        this.aq.e(this.g, this.ap);
        this.f853b.setTextColor(this.g);
        this.h = -1;
        this.aq.f(this.h, this.ap);
        this.c.setTextColor(this.h);
        this.i = -2130432;
        this.aq.g(this.i, this.ap);
        this.d.setTextColor(this.i);
        this.j = -65536;
        this.aq.h(this.j, this.ap);
        this.e.setTextColor(this.j);
    }

    /* access modifiers changed from: protected */
    public final void D() {
        a("userDB.wristankle");
        this.I.setChecked(this.as);
    }

    /* access modifiers changed from: protected */
    public final void E() {
        a("userDB.sixchannels");
        this.G.setChecked(this.as);
    }

    /* access modifiers changed from: protected */
    public final void F() {
        a("userDB.mastertung");
        this.H.setChecked(this.as);
    }

    /* access modifiers changed from: protected */
    public final void G() {
        a("userDB.pointslocation");
        this.E.setChecked(this.as);
    }

    /* access modifiers changed from: protected */
    public final void H() {
        a("userDB.pulsediag");
        this.E.setChecked(this.as);
    }

    /* access modifiers changed from: protected */
    public final void I() {
        a("userDB.materiamedica");
        this.D.setChecked(this.as);
    }

    /* access modifiers changed from: protected */
    public final void J() {
        a("userDB.formulas");
        this.C.setChecked(this.as);
    }

    /* access modifiers changed from: protected */
    public final void K() {
        a("userDB.zangfupathology");
        this.B.setChecked(this.as);
    }

    /* access modifiers changed from: protected */
    public final void L() {
        a("userDB.auricular");
        this.A.setChecked(this.as);
    }

    /* access modifiers changed from: protected */
    public final void M() {
        this.aq.k(this.q.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void N() {
        this.aq.c(this.p.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void O() {
        this.aq.b(this.n.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void P() {
        this.aq.n(this.o.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void Q() {
        this.aq.a(this.m.isChecked(), this.ap);
    }

    /* access modifiers changed from: protected */
    public final void R() {
        this.aq.g(this.l.isChecked(), this.ap);
    }

    private void a(String str) {
        this.k = str;
        AlertDialog create = new AlertDialog.Builder(this).create();
        create.setTitle("Warning");
        create.setMessage("Are you sure you want to delete all user entered data for the " + str + " table?");
        create.setButton("OK", new p(this));
        create.setButton2("Cancel", new g(this));
        create.show();
    }
}
