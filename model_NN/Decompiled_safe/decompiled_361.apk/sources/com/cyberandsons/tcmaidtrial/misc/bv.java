package com.cyberandsons.tcmaidtrial.misc;

import android.text.Editable;
import android.text.TextWatcher;

final class bv implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SendEmail f903a;

    bv(SendEmail sendEmail) {
        this.f903a = sendEmail;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (charSequence.length() > dh.al && this.f903a.f850a.getText().toString().length() > dh.aj && this.f903a.f851b.getText().toString().length() > dh.ak) {
            this.f903a.c.setEnabled(this.f903a.d);
        }
    }
}
