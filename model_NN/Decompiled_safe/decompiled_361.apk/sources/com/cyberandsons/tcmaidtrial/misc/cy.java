package com.cyberandsons.tcmaidtrial.misc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.areas.SearchArea;

final class cy extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private ProgressDialog f934a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f935b;
    private /* synthetic */ PreviousSearch c;

    /* synthetic */ cy(PreviousSearch previousSearch) {
        this(previousSearch, (byte) 0);
    }

    private cy(PreviousSearch previousSearch, byte b2) {
        this.c = previousSearch;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        Integer num = (Integer) obj;
        this.f934a.dismiss();
        if (this.f935b) {
            Toast makeText = Toast.makeText(this.c, (int) C0000R.string.search_exception, 1);
            makeText.setGravity(17, makeText.getXOffset() / 2, makeText.getYOffset() / 2);
            makeText.show();
            return;
        }
        TcmAid.cP = num.toString() + " TCM Items";
        this.c.startActivityForResult(new Intent(this.c, SearchArea.class), 1350);
    }

    public final void onPreExecute() {
        this.f934a = ProgressDialog.show(this.c, "", "Searching ...", true, false);
        this.f934a.setProgressStyle(0);
        this.f935b = false;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Integer doInBackground(String... strArr) {
        int i;
        try {
            i = TcmAid.a(strArr[0], this.c.f849b, this.c.j);
        } catch (IllegalStateException e) {
            this.f935b = true;
            i = 0;
        }
        return Integer.valueOf(i);
    }
}
