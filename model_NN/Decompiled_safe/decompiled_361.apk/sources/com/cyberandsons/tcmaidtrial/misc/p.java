package com.cyberandsons.tcmaidtrial.misc;

import android.content.DialogInterface;
import android.database.SQLException;
import com.cyberandsons.tcmaidtrial.a.q;

final class p implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsData f959a;

    p(SettingsData settingsData) {
        this.f959a = settingsData;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        try {
            this.f959a.ap.execSQL("DELETE FROM " + this.f959a.k);
        } catch (SQLException e) {
            q.a(e);
        }
    }
}
