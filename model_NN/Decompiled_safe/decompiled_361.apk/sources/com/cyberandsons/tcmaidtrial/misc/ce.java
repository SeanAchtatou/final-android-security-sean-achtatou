package com.cyberandsons.tcmaidtrial.misc;

import android.content.Intent;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.SearchConfigList;

final class ce implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsData f914a;

    ce(SettingsData settingsData) {
        this.f914a = settingsData;
    }

    public final void onClick(View view) {
        SettingsData settingsData = this.f914a;
        TcmAid.cO = 4;
        TcmAid.cP = "Point Fields";
        settingsData.startActivityForResult(new Intent(settingsData, SearchConfigList.class), 1350);
    }
}
