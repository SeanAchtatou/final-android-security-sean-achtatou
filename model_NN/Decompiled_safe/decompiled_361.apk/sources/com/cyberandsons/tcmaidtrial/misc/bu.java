package com.cyberandsons.tcmaidtrial.misc;

import android.view.View;
import android.widget.ListAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TabHost;
import com.cyberandsons.tcmaidtrial.C0000R;

final class bu implements TabHost.TabContentFactory {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FavoriteLists f902a;

    bu(FavoriteLists favoriteLists) {
        this.f902a = favoriteLists;
    }

    public final View createTabContent(String str) {
        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(this.f902a.H.getContext(), C0000R.layout.list_row_favorites, this.f902a.f(), new String[]{"_id", this.f902a.s}, new int[]{C0000R.id.text0, C0000R.id.text1});
        simpleCursorAdapter.setViewBinder(new e(this));
        this.f902a.R.setAdapter((ListAdapter) simpleCursorAdapter);
        return this.f902a.R;
    }
}
