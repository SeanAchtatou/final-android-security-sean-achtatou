package com.cyberandsons.tcmaidtrial.misc;

import android.view.View;
import android.widget.ListAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TabHost;
import com.cyberandsons.tcmaidtrial.C0000R;

final class bw implements TabHost.TabContentFactory {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FavoriteLists f904a;

    bw(FavoriteLists favoriteLists) {
        this.f904a = favoriteLists;
    }

    public final View createTabContent(String str) {
        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(this.f904a.H.getContext(), C0000R.layout.list_row_favorites, this.f904a.e(), new String[]{"_id", FavoriteLists.q}, new int[]{C0000R.id.text0, C0000R.id.text1});
        simpleCursorAdapter.setViewBinder(new cn(this));
        this.f904a.Q.setAdapter((ListAdapter) simpleCursorAdapter);
        return this.f904a.Q;
    }
}
