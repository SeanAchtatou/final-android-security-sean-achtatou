package com.cyberandsons.tcmaidtrial.misc;

import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;

final class cc implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FavoriteLists f912a;

    cc(FavoriteLists favoriteLists) {
        this.f912a = favoriteLists;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        TextView textView = (TextView) view.findViewById(C0000R.id.text1);
        int parseInt = Integer.parseInt(((TextView) view.findViewById(C0000R.id.text0)).getText().toString());
        if (this.f912a.e.b(parseInt)) {
            this.f912a.e.a(parseInt, this.f912a.f847b);
            textView.setBackgroundResource(C0000R.color.favlistBGUnCheckedColor);
            return;
        }
        this.f912a.e.a(parseInt, this.f912a.f846a);
        textView.setBackgroundResource(C0000R.color.favlistBGCheckedColor);
    }
}
