package com.cyberandsons.tcmaidtrial.misc;

import android.view.View;
import com.cyberandsons.tcmaidtrial.draw.colormixer.b;

final class ah implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsData f862a;

    ah(SettingsData settingsData) {
        this.f862a = settingsData;
    }

    public final void onClick(View view) {
        SettingsData settingsData = this.f862a;
        new b(settingsData, settingsData.g, new l(settingsData)).show();
    }
}
