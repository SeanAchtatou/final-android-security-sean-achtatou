package com.cyberandsons.tcmaidtrial.misc;

import android.view.View;
import android.widget.ListAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TabHost;
import com.cyberandsons.tcmaidtrial.C0000R;

final class bq implements TabHost.TabContentFactory {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FavoriteLists f898a;

    bq(FavoriteLists favoriteLists) {
        this.f898a = favoriteLists;
    }

    public final View createTabContent(String str) {
        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(this.f898a.H.getContext(), C0000R.layout.list_row_favorites, this.f898a.d(), new String[]{"_id", FavoriteLists.o}, new int[]{C0000R.id.text0, C0000R.id.text1});
        simpleCursorAdapter.setViewBinder(new bh(this));
        this.f898a.P.setAdapter((ListAdapter) simpleCursorAdapter);
        return this.f898a.P;
    }
}
