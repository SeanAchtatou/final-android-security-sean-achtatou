package com.cyberandsons.tcmaidtrial.misc;

import android.content.Intent;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.SearchConfigList;

final class cv implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsData f931a;

    cv(SettingsData settingsData) {
        this.f931a = settingsData;
    }

    public final void onClick(View view) {
        SettingsData settingsData = this.f931a;
        TcmAid.cO = 0;
        TcmAid.cP = "Auricular Fields";
        settingsData.startActivityForResult(new Intent(settingsData, SearchConfigList.class), 1350);
    }
}
