package com.cyberandsons.tcmaidtrial.misc;

import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

public final class b {
    public static String a(String str, String str2) {
        byte[] bytes = str.getBytes();
        KeyGenerator instance = KeyGenerator.getInstance("AES");
        SecureRandom instance2 = SecureRandom.getInstance("SHA1PRNG");
        instance2.setSeed(bytes);
        instance.init(128, instance2);
        byte[] encoded = instance.generateKey().getEncoded();
        int length = str2.length() / 2;
        byte[] bArr = new byte[length];
        for (int i = 0; i < length; i++) {
            bArr[i] = Integer.valueOf(str2.substring(i * 2, (i * 2) + 2), 16).byteValue();
        }
        SecretKeySpec secretKeySpec = new SecretKeySpec(encoded, "AES");
        Cipher instance3 = Cipher.getInstance("AES");
        instance3.init(2, secretKeySpec);
        return new String(instance3.doFinal(bArr));
    }
}
