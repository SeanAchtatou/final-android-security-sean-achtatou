package com.cyberandsons.tcmaidtrial.misc;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.network.ac;
import java.util.Locale;
import org.acra.ErrorReporter;

final class d extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private ProgressDialog f937a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ SendEmail f938b;

    /* synthetic */ d(SendEmail sendEmail) {
        this(sendEmail, (byte) 0);
    }

    private d(SendEmail sendEmail, byte b2) {
        this.f938b = sendEmail;
    }

    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return a((String[]) objArr);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        this.f937a.dismiss();
        this.f938b.finish();
    }

    public final void onPreExecute() {
        this.f937a = ProgressDialog.show(this.f938b, "Submitting Email", "Sending....", true, false);
        this.f937a.setProgressStyle(0);
        this.f937a.setIcon((int) C0000R.drawable.email);
    }

    private static Boolean a(String... strArr) {
        boolean z = false;
        try {
            ac acVar = new ac();
            acVar.b(strArr[0]);
            acVar.c(strArr[1]);
            acVar.e(strArr[2]);
            String str = '[' + strArr[3] + "]\n\n" + strArr[4] + "\n\n---------------\n" + "Running\t" + strArr[5] + "\n" + "UUID\t" + TcmAid.f + "\n" + "Name\t" + Build.USER + "\n" + "Locale\t" + Locale.getDefault().toString() + "\n" + "SysVer\t" + Build.VERSION.RELEASE + "\n" + "Brand\t" + Build.BRAND + "\n" + "Product\t" + Build.MANUFACTURER + "\n" + "Mfg\t" + Build.PRODUCT + "\n" + "Model\t" + Build.MODEL + "\n" + "Width\t" + TcmAid.cS + "\n" + "Height\t" + TcmAid.cT + "\n";
            try {
                acVar.f(strArr[6]);
                acVar.a(str);
                acVar.a(new String[]{strArr[7]});
                acVar.d(strArr[3]);
                acVar.b();
                acVar.f("Email Received [re: " + strArr[6] + ']');
                acVar.a(strArr[8]);
                acVar.a(new String[]{strArr[3]});
                acVar.d(strArr[7]);
                acVar.b();
            } catch (Exception e) {
                Log.e("SE:thread()", "E: " + e.getLocalizedMessage(), e);
                ErrorReporter.a().a("myVariable", e.getLocalizedMessage());
                ErrorReporter.a().handleSilentException(new Exception("SE:thread()"));
            }
        } catch (Exception e2) {
            Log.e("SendMail", e2.getMessage());
            ErrorReporter.a().a("myVariable", e2.getLocalizedMessage());
            ErrorReporter.a().handleSilentException(new Exception("SE:thread()"));
        }
        try {
            return true;
        } catch (IllegalStateException e3) {
            ErrorReporter.a().a("myVariable", "Mail failed!");
            ErrorReporter.a().handleSilentException(new Exception("TCM:doInBackground()"));
            return z;
        }
    }
}
