package com.cyberandsons.tcmaidtrial.misc;

import android.view.View;
import android.widget.ListAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TabHost;
import com.cyberandsons.tcmaidtrial.C0000R;

final class bm implements TabHost.TabContentFactory {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FavoriteLists f894a;

    bm(FavoriteLists favoriteLists) {
        this.f894a = favoriteLists;
    }

    public final View createTabContent(String str) {
        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(this.f894a.H.getContext(), C0000R.layout.list_row_favorites, this.f894a.g(), new String[]{"_id", this.f894a.u}, new int[]{C0000R.id.text0, C0000R.id.text1});
        simpleCursorAdapter.setViewBinder(new co(this));
        this.f894a.S.setAdapter((ListAdapter) simpleCursorAdapter);
        return this.f894a.S;
    }
}
