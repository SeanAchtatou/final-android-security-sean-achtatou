package com.cyberandsons.tcmaidtrial.misc;

import android.view.View;
import com.cyberandsons.tcmaidtrial.draw.colormixer.b;

final class ai implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsData f863a;

    ai(SettingsData settingsData) {
        this.f863a = settingsData;
    }

    public final void onClick(View view) {
        SettingsData settingsData = this.f863a;
        new b(settingsData, settingsData.f, new k(settingsData)).show();
    }
}
