package com.cyberandsons.tcmaidtrial.misc;

import android.app.Activity;
import android.app.AlertDialog;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;
import org.acra.ErrorReporter;

public class PreviousSearch extends Activity implements AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    String f848a;

    /* renamed from: b  reason: collision with root package name */
    boolean f849b;
    private Spinner c;
    private TextView d;
    private ImageButton e;
    private ImageButton f;
    private ImageButton g;
    private String[] h;
    private boolean i = true;
    /* access modifiers changed from: private */
    public SQLiteDatabase j;
    private n k;

    public PreviousSearch() {
        boolean z;
        if (!this.i) {
            z = true;
        } else {
            z = false;
        }
        this.f849b = z;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.j == null || !this.j.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("PS:openDataBase()", str + " opened.");
                this.j = SQLiteDatabase.openDatabase(str, null, 16);
                this.j.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.j.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("PS:openDataBase()", str2 + " ATTACHed.");
                this.k = new n();
                this.k.a(this.j);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new da(this));
                create.show();
            }
        }
        try {
            if (dh.d) {
                setRequestedOrientation(1);
            }
            setContentView((int) C0000R.layout.prevsearch);
            this.d = (TextView) findViewById(C0000R.id.tcs_label);
            this.d.setText("Previous Search");
            this.d.setPadding(10, 10, 0, 0);
            this.g = (ImageButton) findViewById(C0000R.id.search);
            this.g.setOnClickListener(new df(this));
            this.e = (ImageButton) findViewById(C0000R.id.homesearches);
            this.e.setOnClickListener(new dg(this));
            this.f = (ImageButton) findViewById(C0000R.id.trashsearches);
            this.f.setOnClickListener(new db(this));
            this.c = (Spinner) findViewById(C0000R.id.previous_search);
            a();
            b();
        } catch (Exception e3) {
            ErrorReporter.a().a("myVariable", e3.getLocalizedMessage());
            ErrorReporter.a().handleSilentException(new Exception("PS:onCreate()"));
            AlertDialog create2 = new AlertDialog.Builder(this).create();
            create2.setTitle("Attention:");
            create2.setMessage(getString(C0000R.string.area_list_exception));
            create2.setButton("OK", new dc(this));
            create2.show();
        }
    }

    public void onItemSelected(AdapterView adapterView, View view, int i2, long j2) {
        this.f848a = this.h[i2];
    }

    public void onNothingSelected(AdapterView adapterView) {
        this.f848a = "";
    }

    public void onDestroy() {
        try {
            if (this.j != null && this.j.isOpen()) {
                this.j.close();
                this.j = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r6 = this;
            r3 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "SELECT _id, searchString  FROM userDB.searches WHERE "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.cyberandsons.tcmaidtrial.TcmAid.cK
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.database.sqlite.SQLiteDatabase r1 = r6.j     // Catch:{ SQLException -> 0x0045, all -> 0x0050 }
            r2 = 0
            android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x0045, all -> 0x0050 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0045, all -> 0x0050 }
            int r1 = r0.getCount()     // Catch:{ SQLException -> 0x005f, all -> 0x0058 }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ SQLException -> 0x005f, all -> 0x0058 }
            r6.h = r1     // Catch:{ SQLException -> 0x005f, all -> 0x0058 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLException -> 0x005f, all -> 0x0058 }
            if (r1 == 0) goto L_0x003f
            r1 = 0
        L_0x002e:
            java.lang.String[] r2 = r6.h     // Catch:{ SQLException -> 0x005f, all -> 0x0058 }
            int r3 = r1 + 1
            r4 = 1
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLException -> 0x005f, all -> 0x0058 }
            r2[r1] = r4     // Catch:{ SQLException -> 0x005f, all -> 0x0058 }
            boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x005f, all -> 0x0058 }
            if (r1 != 0) goto L_0x0064
        L_0x003f:
            if (r0 == 0) goto L_0x0044
            r0.close()
        L_0x0044:
            return
        L_0x0045:
            r0 = move-exception
            r1 = r3
        L_0x0047:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x005d }
            if (r1 == 0) goto L_0x0044
            r1.close()
            goto L_0x0044
        L_0x0050:
            r0 = move-exception
            r1 = r3
        L_0x0052:
            if (r1 == 0) goto L_0x0057
            r1.close()
        L_0x0057:
            throw r0
        L_0x0058:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0052
        L_0x005d:
            r0 = move-exception
            goto L_0x0052
        L_0x005f:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0047
        L_0x0064:
            r1 = r3
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.misc.PreviousSearch.a():void");
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.h.length == 0) {
            this.f.setVisibility(4);
            this.g.setVisibility(4);
        } else {
            this.f.setVisibility(0);
            this.g.setVisibility(0);
        }
        this.c.setOnItemSelectedListener(this);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, this.h);
        arrayAdapter.setDropDownViewResource(17367049);
        this.c.setAdapter((SpinnerAdapter) arrayAdapter);
        this.c.invalidate();
    }
}
