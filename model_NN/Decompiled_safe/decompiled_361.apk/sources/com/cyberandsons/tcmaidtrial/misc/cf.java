package com.cyberandsons.tcmaidtrial.misc;

import android.content.Intent;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.SearchConfigList;

final class cf implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsData f915a;

    cf(SettingsData settingsData) {
        this.f915a = settingsData;
    }

    public final void onClick(View view) {
        SettingsData settingsData = this.f915a;
        TcmAid.cO = 8;
        TcmAid.cP = "Pulse Fields";
        settingsData.startActivityForResult(new Intent(settingsData, SearchConfigList.class), 1350);
    }
}
