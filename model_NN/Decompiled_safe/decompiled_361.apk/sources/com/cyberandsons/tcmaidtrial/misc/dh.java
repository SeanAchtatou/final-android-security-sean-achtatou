package com.cyberandsons.tcmaidtrial.misc;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.text.Editable;
import android.text.Html;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import com.cyberandsons.tcmaidtrial.TcmAid;

public final class dh {
    public static boolean A = true;
    public static boolean B = false;
    public static boolean C = true;
    public static boolean D = true;
    public static boolean E = false;
    public static boolean F = false;
    public static boolean G = false;
    public static boolean H = false;
    public static boolean I = false;
    public static boolean J = false;
    public static boolean K = false;
    public static boolean L = false;
    public static boolean M = false;
    public static boolean N = false;
    public static boolean O = false;
    public static boolean P = false;
    public static boolean Q = false;
    public static boolean R = false;
    public static boolean S = false;
    public static boolean T = false;
    public static boolean U = false;
    public static boolean V = false;
    public static boolean W = false;
    public static boolean X = false;
    public static boolean Y = false;
    public static boolean Z = false;

    /* renamed from: a  reason: collision with root package name */
    public static Integer f946a = -1;
    private static boolean aA = false;
    private static boolean aB = false;
    private static boolean aC = false;
    private static boolean aD = false;
    private static boolean aE = false;
    private static String[] aF = {"Bai Ji Li", "Fang Ji", "Jiang Can"};
    private static String[] aG = {"Ci Ji Li", "Han Fang Ji", "Bai Jiang Can"};
    public static boolean aa = false;
    public static boolean ab = false;
    public static boolean ac = false;
    public static boolean ad = false;
    public static boolean ae = false;
    public static boolean af = false;
    public static boolean ag = false;
    public static boolean ah = false;
    public static boolean ai = false;
    protected static int aj = 8;
    protected static int ak = 3;
    protected static int al = 3;
    public static final String[] am = {"Auricular", "Diagnosis", "Herbs", "Formulas", "Points", "Pulse"};
    public static boolean an = false;
    public static boolean ao = false;
    public static boolean ap = false;
    private static Integer aq = -1;
    private static float ar = 0.55f;
    private static float as = 0.4f;
    private static boolean at = false;
    private static boolean au = false;
    private static boolean av = false;
    private static boolean aw = false;
    private static boolean ax = false;
    private static boolean ay = false;
    private static boolean az = false;

    /* renamed from: b  reason: collision with root package name */
    public static int f947b = 1024;
    public static boolean c = false;
    public static boolean d = false;
    public static boolean e = true;
    public static boolean f = false;
    public static boolean g = false;
    public static boolean h = false;
    public static boolean i = false;
    public static boolean j = false;
    public static boolean k = false;
    public static boolean l = false;
    public static boolean m = false;
    public static boolean n = false;
    public static boolean o = false;
    public static boolean p = false;
    public static boolean q = false;
    public static boolean r = false;
    public static boolean s = false;
    public static boolean t = false;
    public static boolean u = false;
    public static boolean v = false;
    public static boolean w = false;
    public static boolean x = false;
    public static boolean y = false;
    public static boolean z = false;

    public static CharSequence a(String str, int i2, float f2) {
        StringBuffer stringBuffer = new StringBuffer();
        String num = Integer.toString(i2);
        Paint paint = new Paint();
        paint.setTextSize(f2);
        paint.setAntiAlias(true);
        int measureText = (int) (paint.measureText(str + num) + 0.5f);
        int measureText2 = (int) (paint.measureText(" ") + 0.5f);
        int min = (int) (((float) Math.min(TcmAid.cT, TcmAid.cS)) * ar);
        for (int i3 = 0; i3 < min - measureText; i3 += measureText2) {
            stringBuffer.append("&nbsp;");
        }
        return Html.fromHtml(str + stringBuffer.toString() + "<b>" + num + "</b>");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00ed, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00ee, code lost:
        r7 = r1;
        r1 = r0;
        r0 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00fb, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00fc, code lost:
        r7 = r3;
        r3 = r2;
        r2 = r0;
        r0 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0109, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x010a, code lost:
        r7 = r3;
        r3 = r2;
        r2 = r0;
        r0 = r7;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x006a A[LOOP:1: B:14:0x0066->B:16:0x006a, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ed A[ExcHandler: all (r1v8 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:3:0x0010] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:19:0x0073=Splitter:B:19:0x0073, B:25:0x007f=Splitter:B:25:0x007f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.CharSequence a(java.lang.String r8, java.lang.String r9, float r10, android.database.sqlite.SQLiteDatabase r11) {
        /*
            r2 = 0
            r5 = 1056964608(0x3f000000, float:0.5)
            r6 = 0
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            r0 = 0
            android.database.Cursor r0 = r11.rawQuery(r9, r0)     // Catch:{ SQLException -> 0x0071, Exception -> 0x007d, all -> 0x0089 }
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x0071, Exception -> 0x007d, all -> 0x0089 }
            boolean r2 = r0.moveToFirst()     // Catch:{ SQLException -> 0x0102, Exception -> 0x00f5, all -> 0x00ed }
            if (r2 == 0) goto L_0x0116
            r2 = r6
        L_0x0017:
            r3 = 0
            int r3 = r0.getInt(r3)     // Catch:{ SQLException -> 0x0109, Exception -> 0x00fb, all -> 0x00ed }
            int r2 = r2 + r3
            boolean r3 = r0.moveToNext()     // Catch:{ SQLException -> 0x0109, Exception -> 0x00fb, all -> 0x00ed }
            if (r3 != 0) goto L_0x0017
        L_0x0023:
            if (r0 == 0) goto L_0x0113
            r0.close()
            r0 = r2
        L_0x0029:
            java.lang.String r0 = java.lang.Integer.toString(r0)
            android.graphics.Paint r2 = new android.graphics.Paint
            r2.<init>()
            r2.setTextSize(r10)
            r3 = 1
            r2.setAntiAlias(r3)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r3 = r3.append(r8)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            float r3 = r2.measureText(r3)
            float r3 = r3 + r5
            int r3 = (int) r3
            java.lang.String r4 = " "
            float r2 = r2.measureText(r4)
            float r2 = r2 + r5
            int r2 = (int) r2
            int r4 = com.cyberandsons.tcmaidtrial.TcmAid.cT
            int r5 = com.cyberandsons.tcmaidtrial.TcmAid.cS
            int r4 = java.lang.Math.min(r4, r5)
            float r4 = (float) r4
            float r5 = com.cyberandsons.tcmaidtrial.misc.dh.ar
            float r4 = r4 * r5
            int r4 = (int) r4
            r5 = r6
        L_0x0066:
            int r6 = r4 - r3
            if (r5 >= r6) goto L_0x0091
            java.lang.String r6 = "&nbsp;"
            r1.append(r6)
            int r5 = r5 + r2
            goto L_0x0066
        L_0x0071:
            r0 = move-exception
            r3 = r6
        L_0x0073:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x00f2 }
            if (r2 == 0) goto L_0x0110
            r2.close()
            r0 = r3
            goto L_0x0029
        L_0x007d:
            r0 = move-exception
            r3 = r6
        L_0x007f:
            com.cyberandsons.tcmaidtrial.a.q.a(r0)     // Catch:{ all -> 0x00f2 }
            if (r2 == 0) goto L_0x0110
            r2.close()
            r0 = r3
            goto L_0x0029
        L_0x0089:
            r0 = move-exception
            r1 = r2
        L_0x008b:
            if (r1 == 0) goto L_0x0090
            r1.close()
        L_0x0090:
            throw r0
        L_0x0091:
            java.lang.String r2 = "Common:button"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r3 = r3.append(r8)
            java.lang.String r4 = r1.toString()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "<b>"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r4 = "</b>"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.text.Spanned r3 = android.text.Html.fromHtml(r3)
            java.lang.String r3 = r3.toString()
            android.util.Log.d(r2, r3)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = "<b>"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = "</b>"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.text.Spanned r0 = android.text.Html.fromHtml(r0)
            return r0
        L_0x00ed:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x008b
        L_0x00f2:
            r0 = move-exception
            r1 = r2
            goto L_0x008b
        L_0x00f5:
            r2 = move-exception
            r3 = r6
            r7 = r0
            r0 = r2
            r2 = r7
            goto L_0x007f
        L_0x00fb:
            r3 = move-exception
            r7 = r3
            r3 = r2
            r2 = r0
            r0 = r7
            goto L_0x007f
        L_0x0102:
            r2 = move-exception
            r3 = r6
            r7 = r0
            r0 = r2
            r2 = r7
            goto L_0x0073
        L_0x0109:
            r3 = move-exception
            r7 = r3
            r3 = r2
            r2 = r0
            r0 = r7
            goto L_0x0073
        L_0x0110:
            r0 = r3
            goto L_0x0029
        L_0x0113:
            r0 = r2
            goto L_0x0029
        L_0x0116:
            r2 = r6
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.misc.dh.a(java.lang.String, java.lang.String, float, android.database.sqlite.SQLiteDatabase):java.lang.CharSequence");
    }

    /* JADX WARN: Type inference failed for: r2v31, types: [android.database.Cursor] */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0109, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x010a, code lost:
        r8 = r2;
        r2 = r1;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x010e, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x010f, code lost:
        r4 = 0;
        r8 = r1;
        r1 = r2;
        r2 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0114, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0115, code lost:
        r8 = r4;
        r4 = r2;
        r2 = r1;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0120, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0121, code lost:
        r4 = 0;
        r8 = r1;
        r1 = r2;
        r2 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0127, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0128, code lost:
        r8 = r4;
        r4 = r2;
        r2 = r1;
        r1 = r8;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0087 A[LOOP:2: B:24:0x0083->B:26:0x0087, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0109 A[ExcHandler: all (r2v19 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:3:0x0010] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:29:0x0090=Splitter:B:29:0x0090, B:35:0x009c=Splitter:B:35:0x009c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.CharSequence a(java.lang.String r9, java.lang.String r10, java.lang.String r11, float r12, android.database.sqlite.SQLiteDatabase r13) {
        /*
            r6 = 1056964608(0x3f000000, float:0.5)
            r2 = 0
            r7 = 0
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            r1 = 0
            android.database.Cursor r1 = r13.rawQuery(r10, r1)     // Catch:{ SQLException -> 0x008e, Exception -> 0x009a }
            android.database.sqlite.SQLiteCursor r1 = (android.database.sqlite.SQLiteCursor) r1     // Catch:{ SQLException -> 0x008e, Exception -> 0x009a }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLException -> 0x0120, Exception -> 0x010e, all -> 0x0109 }
            if (r2 == 0) goto L_0x013d
            r2 = r7
        L_0x0017:
            r4 = 0
            int r4 = r1.getInt(r4)     // Catch:{ SQLException -> 0x0127, Exception -> 0x0114, all -> 0x0109 }
            int r2 = r2 + r4
            boolean r4 = r1.moveToNext()     // Catch:{ SQLException -> 0x0127, Exception -> 0x0114, all -> 0x0109 }
            if (r4 != 0) goto L_0x0017
            r4 = r2
        L_0x0024:
            r2 = 0
            android.database.Cursor r2 = r13.rawQuery(r11, r2)     // Catch:{ SQLException -> 0x012e, Exception -> 0x011a, all -> 0x0109 }
            r0 = r2
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLException -> 0x012e, Exception -> 0x011a, all -> 0x0109 }
            r1 = r0
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLException -> 0x012e, Exception -> 0x011a, all -> 0x0109 }
            if (r2 == 0) goto L_0x013a
            r2 = r4
        L_0x0034:
            r4 = 0
            int r4 = r1.getInt(r4)     // Catch:{ SQLException -> 0x0127, Exception -> 0x0114, all -> 0x0109 }
            int r2 = r2 + r4
            boolean r4 = r1.moveToNext()     // Catch:{ SQLException -> 0x0127, Exception -> 0x0114, all -> 0x0109 }
            if (r4 != 0) goto L_0x0034
        L_0x0040:
            if (r1 == 0) goto L_0x0137
            r1.close()
            r1 = r2
        L_0x0046:
            java.lang.String r1 = java.lang.Integer.toString(r1)
            android.graphics.Paint r2 = new android.graphics.Paint
            r2.<init>()
            r2.setTextSize(r12)
            r4 = 1
            r2.setAntiAlias(r4)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.StringBuilder r4 = r4.append(r9)
            java.lang.StringBuilder r4 = r4.append(r1)
            java.lang.String r4 = r4.toString()
            float r4 = r2.measureText(r4)
            float r4 = r4 + r6
            int r4 = (int) r4
            java.lang.String r5 = " "
            float r2 = r2.measureText(r5)
            float r2 = r2 + r6
            int r2 = (int) r2
            int r5 = com.cyberandsons.tcmaidtrial.TcmAid.cT
            int r6 = com.cyberandsons.tcmaidtrial.TcmAid.cS
            int r5 = java.lang.Math.min(r5, r6)
            float r5 = (float) r5
            float r6 = com.cyberandsons.tcmaidtrial.misc.dh.ar
            float r5 = r5 * r6
            int r5 = (int) r5
            r6 = r7
        L_0x0083:
            int r7 = r5 - r4
            if (r6 >= r7) goto L_0x00ad
            java.lang.String r7 = "&nbsp;"
            r3.append(r7)
            int r6 = r6 + r2
            goto L_0x0083
        L_0x008e:
            r1 = move-exception
            r4 = r7
        L_0x0090:
            com.cyberandsons.tcmaidtrial.a.q.a(r1)     // Catch:{ all -> 0x00a6 }
            if (r2 == 0) goto L_0x0134
            r2.close()
            r1 = r4
            goto L_0x0046
        L_0x009a:
            r1 = move-exception
            r4 = r7
        L_0x009c:
            com.cyberandsons.tcmaidtrial.a.q.a(r1)     // Catch:{ all -> 0x00a6 }
            if (r2 == 0) goto L_0x0134
            r2.close()
            r1 = r4
            goto L_0x0046
        L_0x00a6:
            r1 = move-exception
        L_0x00a7:
            if (r2 == 0) goto L_0x00ac
            r2.close()
        L_0x00ac:
            throw r1
        L_0x00ad:
            java.lang.String r2 = "Common:button"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.StringBuilder r4 = r4.append(r9)
            java.lang.String r5 = r3.toString()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "<b>"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r1)
            java.lang.String r5 = "</b>"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            android.text.Spanned r4 = android.text.Html.fromHtml(r4)
            java.lang.String r4 = r4.toString()
            android.util.Log.d(r2, r4)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r9)
            java.lang.String r3 = r3.toString()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "<b>"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = "</b>"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.text.Spanned r1 = android.text.Html.fromHtml(r1)
            return r1
        L_0x0109:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x00a7
        L_0x010e:
            r2 = move-exception
            r4 = r7
            r8 = r1
            r1 = r2
            r2 = r8
            goto L_0x009c
        L_0x0114:
            r4 = move-exception
            r8 = r4
            r4 = r2
            r2 = r1
            r1 = r8
            goto L_0x009c
        L_0x011a:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x009c
        L_0x0120:
            r2 = move-exception
            r4 = r7
            r8 = r1
            r1 = r2
            r2 = r8
            goto L_0x0090
        L_0x0127:
            r4 = move-exception
            r8 = r4
            r4 = r2
            r2 = r1
            r1 = r8
            goto L_0x0090
        L_0x012e:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x0090
        L_0x0134:
            r1 = r4
            goto L_0x0046
        L_0x0137:
            r1 = r2
            goto L_0x0046
        L_0x013a:
            r2 = r4
            goto L_0x0040
        L_0x013d:
            r4 = r7
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cyberandsons.tcmaidtrial.misc.dh.a(java.lang.String, java.lang.String, java.lang.String, float, android.database.sqlite.SQLiteDatabase):java.lang.CharSequence");
    }

    public static CharSequence a(String str, String str2, float f2) {
        StringBuffer stringBuffer = new StringBuffer();
        Paint paint = new Paint();
        paint.setTextSize(f2);
        paint.setAntiAlias(true);
        int measureText = (int) (paint.measureText(str) + 0.5f);
        int measureText2 = (int) (paint.measureText(" ") + 0.5f);
        int measureText3 = (int) (paint.measureText(str2) + 0.5f);
        for (int i2 = 0; i2 < measureText - measureText3; i2 += measureText2) {
            stringBuffer.append("&nbsp;");
        }
        return stringBuffer.toString();
    }

    public static CharSequence a(String str, String str2) {
        return Html.fromHtml("<b>" + str + "</b><br><hr><i>" + str2 + "</i>");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static Bitmap b(String str, String str2) {
        Bitmap decodeFile = BitmapFactory.decodeFile("/sdcard/tcmclinicaid/" + str2 + '/' + str.toLowerCase().replace(' ', '_') + ".png");
        if (decodeFile == null) {
            return BitmapFactory.decodeFile("/sdcard/tcmclinicaid/" + str2 + '/' + str.toLowerCase().replace(' ', '_') + ".jpg");
        }
        return decodeFile;
    }

    public static Bitmap a(Context context, String str) {
        return BitmapFactory.decodeStream(context.getAssets().open(str.toLowerCase() + ".png"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static BitmapDrawable a(Bitmap bitmap, int i2, int i3) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) i2) / ((float) width), ((float) i3) / ((float) height));
        Log.d("Common:resizeImage", "deviceWidth = " + Integer.toString(TcmAid.cS) + ", deviceHeight = " + Integer.toString(TcmAid.cT));
        Log.d("Common:resizeImage", "newWidth = " + Integer.toString(i2) + ", newHeight = " + Integer.toString(i3));
        return new BitmapDrawable(Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true));
    }

    public static BitmapDrawable a(Bitmap bitmap, int i2, int i3, Window window) {
        int i4;
        int i5;
        int i6;
        int i7;
        if (i2 <= 0 || i3 <= 0) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            window.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int i8 = displayMetrics.widthPixels;
            i5 = displayMetrics.heightPixels;
            if (i8 < 240) {
                i4 = 240;
            } else if (i8 < 320) {
                i4 = 320;
            } else {
                i4 = 480;
            }
        } else {
            i5 = i3;
            i4 = i2;
        }
        switch (i4) {
            case 240:
                if (i5 != 320) {
                    if (i5 != 400) {
                        if (i5 != 432) {
                            i6 = 160;
                            i7 = 160;
                            break;
                        } else {
                            i6 = 160;
                            i7 = 160;
                            break;
                        }
                    } else {
                        i6 = 240;
                        i7 = 240;
                        break;
                    }
                } else {
                    i6 = 240;
                    i7 = 240;
                    break;
                }
            case 320:
                int i9 = i4;
                i7 = i5;
                i6 = i9;
                break;
            case 480:
                if (i5 != 800) {
                    i6 = 620;
                    i7 = 620;
                    break;
                } else {
                    i6 = 720;
                    i7 = 720;
                    break;
                }
            default:
                int i10 = (int) (((float) i4) * as);
                i7 = (int) (((float) i5) * as);
                i6 = i10;
                break;
        }
        if (av) {
            Log.d("Common:scaleImage()", "deviceWidth = " + Integer.toString(TcmAid.cS) + ", deviceHeight = " + Integer.toString(TcmAid.cT));
            Log.d("Common:scaleImage()", "newWidth = " + Integer.toString(i6) + ", newHeight = " + Integer.toString(i7));
        }
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, i6, i7, true);
        bitmap.recycle();
        return new BitmapDrawable(createScaledBitmap);
    }

    public static String a(String str) {
        for (int i2 = 0; i2 < aG.length; i2++) {
            if (aG[i2].equalsIgnoreCase(str)) {
                return aF[i2];
            }
        }
        return str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static void a(Editable editable) {
        Editable editable2 = null;
        boolean z2 = true;
        for (String str : new bi(TcmAid.cQ.replace('\'', '\"')).a()) {
            boolean z3 = z2;
            Editable editable3 = editable2;
            int indexOf = editable.toString().toLowerCase().indexOf(str.toLowerCase());
            while (indexOf >= 0) {
                if (z3) {
                    z3 = false;
                    editable3 = editable;
                }
                int length = str.length() + indexOf;
                editable3.setSpan(new ForegroundColorSpan(-65536), indexOf, length, 33);
                indexOf = editable.toString().toLowerCase().indexOf(str.toLowerCase(), length);
            }
            editable2 = editable3;
            z2 = z3;
        }
    }
}
