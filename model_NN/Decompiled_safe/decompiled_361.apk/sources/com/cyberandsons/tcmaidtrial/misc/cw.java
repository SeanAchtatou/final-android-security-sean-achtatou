package com.cyberandsons.tcmaidtrial.misc;

import android.content.Intent;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.SearchConfigList;

final class cw implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsData f932a;

    cw(SettingsData settingsData) {
        this.f932a = settingsData;
    }

    public final void onClick(View view) {
        SettingsData settingsData = this.f932a;
        TcmAid.cO = 1;
        TcmAid.cP = "Diagnosis Fields";
        settingsData.startActivityForResult(new Intent(settingsData, SearchConfigList.class), 1350);
    }
}
