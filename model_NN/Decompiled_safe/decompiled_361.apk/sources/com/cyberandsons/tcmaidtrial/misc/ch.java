package com.cyberandsons.tcmaidtrial.misc;

import android.content.Intent;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.SearchConfigList;

final class ch implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsData f917a;

    ch(SettingsData settingsData) {
        this.f917a = settingsData;
    }

    public final void onClick(View view) {
        SettingsData settingsData = this.f917a;
        TcmAid.cO = 7;
        TcmAid.cP = "WristAnkle Fields";
        settingsData.startActivityForResult(new Intent(settingsData, SearchConfigList.class), 1350);
    }
}
