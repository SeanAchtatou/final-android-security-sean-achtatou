package com.cyberandsons.tcmaidtrial.misc;

import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public final class bi {

    /* renamed from: b  reason: collision with root package name */
    private static final Set f889b;

    /* renamed from: a  reason: collision with root package name */
    private String f890a;

    public bi(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Search Text cannot be null.");
        }
        this.f890a = str;
    }

    public final Set a() {
        HashSet hashSet = new HashSet();
        String str = " \t\r\n\"";
        StringTokenizer stringTokenizer = new StringTokenizer(this.f890a, str, true);
        while (stringTokenizer.hasMoreTokens()) {
            String nextToken = stringTokenizer.nextToken(str);
            if (!nextToken.equals("\"")) {
                if (nextToken != null && !nextToken.trim().equals("")) {
                    if (!f889b.contains(nextToken.trim())) {
                        hashSet.add(nextToken.trim());
                    }
                }
            } else {
                str = str.equals(" \t\r\n\"") ? "\"" : " \t\r\n\"";
            }
        }
        return hashSet;
    }

    static {
        HashSet hashSet = new HashSet();
        f889b = hashSet;
        hashSet.add("a");
        f889b.add("and");
        f889b.add("be");
        f889b.add("for");
        f889b.add("from");
        f889b.add("has");
        f889b.add("i");
        f889b.add("in");
        f889b.add("is");
        f889b.add("it");
        f889b.add("of");
        f889b.add("on");
        f889b.add("to");
        f889b.add("the");
    }
}
