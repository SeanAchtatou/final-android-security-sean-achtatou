package com.cyberandsons.tcmaidtrial.misc;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;

public class FavoriteIntroduction extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Button f844a;

    /* renamed from: b  reason: collision with root package name */
    private TextView f845b;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (dh.d) {
            setRequestedOrientation(1);
        }
        setContentView((int) C0000R.layout.favoriteintro);
        this.f845b = (TextView) findViewById(C0000R.id.tc_label);
        this.f845b.setText("Create Favorite List");
        this.f845b.setGravity(1);
        this.f844a = (Button) findViewById(C0000R.id.beg_button);
        this.f844a.setGravity(1);
        this.f844a.setOnClickListener(new al(this));
    }
}
