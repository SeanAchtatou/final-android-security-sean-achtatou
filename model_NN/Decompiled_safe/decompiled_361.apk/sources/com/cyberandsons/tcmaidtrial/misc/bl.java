package com.cyberandsons.tcmaidtrial.misc;

import android.view.View;
import android.widget.ListAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TabHost;
import com.cyberandsons.tcmaidtrial.C0000R;

final class bl implements TabHost.TabContentFactory {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FavoriteLists f893a;

    bl(FavoriteLists favoriteLists) {
        this.f893a = favoriteLists;
    }

    public final View createTabContent(String str) {
        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(this.f893a.H.getContext(), C0000R.layout.list_row_favorites, this.f893a.h(), new String[]{FavoriteLists.v, "common_name"}, new int[]{C0000R.id.text0, C0000R.id.text1});
        simpleCursorAdapter.setViewBinder(new f(this));
        this.f893a.T.setAdapter((ListAdapter) simpleCursorAdapter);
        return this.f893a.T;
    }
}
