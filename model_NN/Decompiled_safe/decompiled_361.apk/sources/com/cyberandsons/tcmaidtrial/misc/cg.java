package com.cyberandsons.tcmaidtrial.misc;

import android.content.Intent;
import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;
import com.cyberandsons.tcmaidtrial.lists.SearchConfigList;

final class cg implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SettingsData f916a;

    cg(SettingsData settingsData) {
        this.f916a = settingsData;
    }

    public final void onClick(View view) {
        SettingsData settingsData = this.f916a;
        TcmAid.cO = 5;
        TcmAid.cP = "Six Stages Fields";
        settingsData.startActivityForResult(new Intent(settingsData, SearchConfigList.class), 1350);
    }
}
