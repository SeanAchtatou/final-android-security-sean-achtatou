package com.cyberandsons.tcmaidtrial.misc;

import android.view.View;
import com.cyberandsons.tcmaidtrial.TcmAid;

final class df implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PreviousSearch f944a;

    df(PreviousSearch previousSearch) {
        this.f944a = previousSearch;
    }

    public final void onClick(View view) {
        PreviousSearch previousSearch = this.f944a;
        TcmAid.cQ = previousSearch.f848a;
        new cy(previousSearch).execute(TcmAid.cQ);
    }
}
