package com.cyberandsons.tcmaidtrial.misc;

import android.app.Activity;
import android.app.AlertDialog;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import com.cyberandsons.tcmaidtrial.C0000R;
import com.cyberandsons.tcmaidtrial.a.n;
import com.cyberandsons.tcmaidtrial.a.q;

public class SendEmail extends Activity {

    /* renamed from: a  reason: collision with root package name */
    EditText f850a;

    /* renamed from: b  reason: collision with root package name */
    EditText f851b;
    Button c;
    boolean d = true;
    private EditText e;
    private TextView f;
    private ImageButton g;
    private ImageButton h;
    private boolean i;
    private SQLiteDatabase j;
    private n k;

    public SendEmail() {
        boolean z;
        if (!this.d) {
            z = true;
        } else {
            z = false;
        }
        this.i = z;
    }

    static /* synthetic */ void a(SendEmail sendEmail) {
        sendEmail.f851b.setText("");
        sendEmail.e.setText("");
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.j == null || !this.j.isOpen()) {
            try {
                String string = getString(C0000R.string.tcmDatabasePath);
                String string2 = getString(C0000R.string.tcmUserDatabase);
                String str = string + (getString(C0000R.string.tcmDatabase) + getString(C0000R.string.database_level) + getString(C0000R.string.database_extension));
                Log.d("SE:openDataBase()", str + " opened.");
                this.j = SQLiteDatabase.openDatabase(str, null, 16);
                this.j.execSQL("PRAGMA cache_size = 50");
                String str2 = string + string2;
                this.j.execSQL("ATTACH \"" + str2 + "\" AS userDB");
                Log.d("SE:openDataBase()", str2 + " ATTACHed.");
                this.k = new n();
                this.k.a(this.j);
            } catch (SQLException e2) {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle("Attention:");
                create.setMessage("Please restart TCM Clinic Aid. The database was not able to reopen.");
                create.setButton("OK", new bn(this));
                create.show();
            }
        }
        if (dh.d) {
            setRequestedOrientation(1);
        }
        setContentView((int) C0000R.layout.email);
        this.f = (TextView) findViewById(C0000R.id.tc_label);
        this.f.setText("Email to Support@CyberAndSons.com");
        this.f.setPadding(10, 10, 0, 0);
        this.f850a = (EditText) findViewById(C0000R.id.userEmail);
        this.f850a.setOnClickListener(new by(this));
        this.f851b = (EditText) findViewById(C0000R.id.subject);
        this.f851b.setOnClickListener(new bx(this));
        this.e = (EditText) findViewById(C0000R.id.body);
        this.e.addTextChangedListener(new bv(this));
        this.e.setOnClickListener(new bt(this));
        this.c = (Button) findViewById(C0000R.id.btnSend);
        this.c.setEnabled(this.i);
        this.c.setOnClickListener(new br(this));
        this.g = (ImageButton) findViewById(C0000R.id.homebutton);
        this.g.setOnClickListener(new bp(this));
        this.h = (ImageButton) findViewById(C0000R.id.clearbutton);
        this.h.setOnClickListener(new bo(this));
        this.f850a.setText(this.k.l());
    }

    public void onDestroy() {
        try {
            if (this.j != null && this.j.isOpen()) {
                this.j.close();
                this.j = null;
            }
        } catch (SQLException e2) {
            q.a(e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.f850a.getWindowToken(), 0);
        this.k.a(this.f850a.getText().toString(), this.j);
        new d(this).execute(getString(C0000R.string.gmailuserid), getString(C0000R.string.gmailpassword), getString(C0000R.string.gmailhost), this.f850a.getText().toString(), this.e.getText().toString(), getString(C0000R.string.versionName), this.f851b.getText().toString(), getString(C0000R.string.emailfrom), getString(C0000R.string.recepientemail));
    }
}
