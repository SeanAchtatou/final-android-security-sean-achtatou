package com.cyberandsons.tcmaidtrial.misc;

import android.widget.EditText;
import android.widget.TableRow;
import com.cyberandsons.tcmaidtrial.C0000R;
import java.util.StringTokenizer;

public final class ad {
    public static String a(String str) {
        StringTokenizer stringTokenizer = new StringTokenizer(str, " (", true);
        StringBuilder sb = new StringBuilder();
        while (stringTokenizer.hasMoreTokens()) {
            String nextToken = stringTokenizer.nextToken();
            sb.append(String.format("%s%s", Character.valueOf(Character.toUpperCase(nextToken.charAt(0))), nextToken.substring(1)));
        }
        return sb.toString();
    }

    public static void a(EditText editText, int i) {
        if (i == 0) {
            editText.setInputType(i);
            editText.setBackgroundResource(C0000R.layout.corners_detailed_add_edit);
            editText.setImeOptions(0);
            editText.setFocusable(false);
            editText.setLayoutParams(new TableRow.LayoutParams(-2, -2));
            return;
        }
        editText.setInputType(i);
        editText.setImeOptions(5);
        editText.setFocusable(true);
        if (editText.getText().toString().length() > 1) {
            editText.setSelection(1);
            editText.extendSelection(0);
            editText.setSelection(0);
        }
    }
}
