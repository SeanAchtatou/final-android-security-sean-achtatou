package com.cyberandsons.tcmaidtrial;

import android.app.Application;
import com.cyberandsons.tcmaidtrial.misc.dh;
import com.urbanairship.b;
import com.urbanairship.c;
import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.a.a;

@a(a = "dDZuM0IwR0VDWmtSbTZMWnRPR3dMb2c6MQ", c = ReportingInteractionMode.TOAST, m = C0000R.string.crash_toast_text)
public class TCACrash extends Application {
    public void onCreate() {
        ACRA.init(this);
        super.onCreate();
        if (dh.an) {
            c.a(this, b.a(this));
        }
    }
}
