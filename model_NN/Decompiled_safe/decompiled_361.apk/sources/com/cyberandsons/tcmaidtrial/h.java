package com.cyberandsons.tcmaidtrial;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

final class h implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ TcmAid f701a;

    h(TcmAid tcmAid) {
        this.f701a = tcmAid;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f701a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://market.android.com/search?q=pname:" + this.f701a.getString(C0000R.string.purchase_package))));
        this.f701a.finish();
    }
}
