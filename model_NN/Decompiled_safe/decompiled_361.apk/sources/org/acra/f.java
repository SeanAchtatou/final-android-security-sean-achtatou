package org.acra;

import android.os.Looper;
import android.widget.Toast;

final class f extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ErrorReporter f1305a;

    f(ErrorReporter errorReporter) {
        this.f1305a = errorReporter;
    }

    public final void run() {
        Looper.prepare();
        Toast.makeText(this.f1305a.g, this.f1305a.j.getInt("RES_TOAST_TEXT"), 1).show();
        Looper.loop();
    }
}
