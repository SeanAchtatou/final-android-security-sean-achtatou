package org.acra;

import android.content.SharedPreferences;
import android.util.Log;

final class a implements SharedPreferences.OnSharedPreferenceChangeListener {
    a() {
    }

    public final void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        Log.d(ACRA.LOG_TAG, "Preferences changed, check if ACRA configuration must change.");
        if (ACRA.PREF_DISABLE_ACRA.equals(str) || ACRA.PREF_ENABLE_ACRA.equals(str)) {
            Boolean bool = false;
            try {
                bool = Boolean.valueOf(sharedPreferences.getBoolean(ACRA.PREF_DISABLE_ACRA, !sharedPreferences.getBoolean(ACRA.PREF_ENABLE_ACRA, true)));
            } catch (Exception e) {
            }
            if (bool.booleanValue()) {
                ErrorReporter.a().d();
                return;
            }
            try {
                ACRA.initAcra();
            } catch (j e2) {
                Log.w(ACRA.LOG_TAG, "Error : ", e2);
            }
        }
    }
}
