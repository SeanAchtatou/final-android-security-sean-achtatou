package org.acra;

import android.content.res.Configuration;
import android.util.Log;
import android.util.SparseArray;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static SparseArray f1300a = new SparseArray();

    /* renamed from: b  reason: collision with root package name */
    private static SparseArray f1301b = new SparseArray();
    private static SparseArray c = new SparseArray();
    private static SparseArray d = new SparseArray();
    private static SparseArray e = new SparseArray();
    private static SparseArray f = new SparseArray();
    private static SparseArray g = new SparseArray();
    private static SparseArray h = new SparseArray();
    private static SparseArray i = new SparseArray();
    private static final HashMap j;

    static {
        HashMap hashMap = new HashMap();
        j = hashMap;
        hashMap.put("HARDKEYBOARDHIDDEN_", f1300a);
        j.put("KEYBOARD_", f1301b);
        j.put("KEYBOARDHIDDEN_", c);
        j.put("NAVIGATION_", d);
        j.put("NAVIGATIONHIDDEN_", e);
        j.put("ORIENTATION_", f);
        j.put("SCREENLAYOUT_", g);
        j.put("TOUCHSCREEN_", h);
        j.put("UI_MODE_", i);
        for (Field field : Configuration.class.getFields()) {
            if (Modifier.isStatic(field.getModifiers()) && Modifier.isFinal(field.getModifiers())) {
                String name = field.getName();
                try {
                    if (name.startsWith("HARDKEYBOARDHIDDEN_")) {
                        f1300a.put(field.getInt(null), name);
                    } else if (name.startsWith("KEYBOARD_")) {
                        f1301b.put(field.getInt(null), name);
                    } else if (name.startsWith("KEYBOARDHIDDEN_")) {
                        c.put(field.getInt(null), name);
                    } else if (name.startsWith("NAVIGATION_")) {
                        d.put(field.getInt(null), name);
                    } else if (name.startsWith("NAVIGATIONHIDDEN_")) {
                        e.put(field.getInt(null), name);
                    } else if (name.startsWith("ORIENTATION_")) {
                        f.put(field.getInt(null), name);
                    } else if (name.startsWith("SCREENLAYOUT_")) {
                        g.put(field.getInt(null), name);
                    } else if (name.startsWith("TOUCHSCREEN_")) {
                        h.put(field.getInt(null), name);
                    } else if (name.startsWith("UI_MODE_")) {
                        i.put(field.getInt(null), name);
                    }
                } catch (IllegalArgumentException e2) {
                    Log.w(ACRA.LOG_TAG, "Error while inspecting device configuration: ", e2);
                } catch (IllegalAccessException e3) {
                    Log.w(ACRA.LOG_TAG, "Error while inspecting device configuration: ", e3);
                }
            }
        }
    }

    public static String a(Configuration configuration) {
        String str;
        StringBuilder sb = new StringBuilder();
        for (Field field : configuration.getClass().getFields()) {
            try {
                if (!Modifier.isStatic(field.getModifiers())) {
                    sb.append(field.getName()).append('=');
                    if (field.getType().equals(Integer.TYPE)) {
                        String name = field.getName();
                        if (name.equals("mcc") || name.equals("mnc")) {
                            str = Integer.toString(field.getInt(configuration));
                        } else if (name.equals("uiMode")) {
                            str = a((SparseArray) j.get("UI_MODE_"), field.getInt(configuration));
                        } else if (name.equals("screenLayout")) {
                            str = a((SparseArray) j.get("SCREENLAYOUT_"), field.getInt(configuration));
                        } else {
                            SparseArray sparseArray = (SparseArray) j.get(String.valueOf(name.toUpperCase()) + '_');
                            if (sparseArray == null) {
                                str = Integer.toString(field.getInt(configuration));
                            } else {
                                str = (String) sparseArray.get(field.getInt(configuration));
                                if (str == null) {
                                    str = Integer.toString(field.getInt(configuration));
                                }
                            }
                        }
                        sb.append(str);
                    } else {
                        sb.append(field.get(configuration).toString());
                    }
                    sb.append(10);
                }
            } catch (IllegalArgumentException e2) {
                Log.e(ACRA.LOG_TAG, "Error while inspecting device configuration: ", e2);
            } catch (IllegalAccessException e3) {
                Log.e(ACRA.LOG_TAG, "Error while inspecting device configuration: ", e3);
            }
        }
        return sb.toString();
    }

    private static String a(SparseArray sparseArray, int i2) {
        int i3;
        StringBuilder sb = new StringBuilder();
        int i4 = 0;
        while (true) {
            int i5 = i4;
            if (i5 >= sparseArray.size()) {
                return sb.toString();
            }
            int keyAt = sparseArray.keyAt(i5);
            if (((String) sparseArray.get(keyAt)).endsWith("_MASK") && (i3 = i2 & keyAt) > 0) {
                if (sb.length() > 0) {
                    sb.append('+');
                }
                sb.append((String) sparseArray.get(i3));
            }
            i4 = i5 + 1;
        }
    }
}
