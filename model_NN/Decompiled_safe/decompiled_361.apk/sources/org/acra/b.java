package org.acra;

import android.util.Log;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;

final class b {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1298a = ACRA.LOG_TAG;

    /* renamed from: b  reason: collision with root package name */
    private static final TrustManager[] f1299b = {new h()};
    private static final AllowAllHostnameVerifier c = new AllowAllHostnameVerifier();

    b() {
    }

    static void a(Map map, URL url) {
        URLConnection openConnection = url.openConnection();
        if (openConnection instanceof HttpsURLConnection) {
            SSLContext instance = SSLContext.getInstance("TLS");
            instance.init(new KeyManager[0], f1299b, new SecureRandom());
            ((HttpsURLConnection) openConnection).setSSLSocketFactory(instance.getSocketFactory());
            ((HttpsURLConnection) openConnection).setHostnameVerifier(c);
        }
        openConnection.setConnectTimeout(3000);
        openConnection.setReadTimeout(3000);
        StringBuilder sb = new StringBuilder();
        for (String str : map.keySet()) {
            if (sb.length() != 0) {
                sb.append('&');
            }
            sb.append(URLEncoder.encode(str, "UTF-8")).append('=').append(URLEncoder.encode((String) map.get(str), "UTF-8"));
        }
        openConnection.setDoOutput(true);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openConnection.getOutputStream());
        Log.d(f1298a, "Posting crash report data");
        outputStreamWriter.write(sb.toString());
        outputStreamWriter.flush();
        outputStreamWriter.close();
        Log.d(f1298a, "Reading response");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(openConnection.getInputStream()));
        int i = 0;
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                bufferedReader.close();
                return;
            }
            i++;
            if (i <= 2) {
                Log.d(f1298a, readLine);
            }
        }
    }
}
