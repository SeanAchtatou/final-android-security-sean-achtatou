package org.acra;

import android.app.Activity;
import android.app.NotificationManager;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class CrashReportDialog extends Activity {

    /* renamed from: a  reason: collision with root package name */
    String f1294a = null;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public EditText f1295b = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f1294a = getIntent().getStringExtra("REPORT_FILE_NAME");
        if (this.f1294a == null) {
            finish();
        }
        requestWindowFeature(3);
        Bundle crashResources = ACRA.getCrashResources();
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(10, 10, 10, 10);
        linearLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        ScrollView scrollView = new ScrollView(this);
        linearLayout.addView(scrollView, new LinearLayout.LayoutParams(-1, -1, 1.0f));
        TextView textView = new TextView(this);
        textView.setText(getText(crashResources.getInt("RES_DIALOG_TEXT")));
        scrollView.addView(textView, -1, -1);
        int i = crashResources.getInt("RES_DIALOG_COMMENT_PROMPT");
        if (i != 0) {
            TextView textView2 = new TextView(this);
            textView2.setText(getText(i));
            textView2.setPadding(textView2.getPaddingLeft(), 10, textView2.getPaddingRight(), textView2.getPaddingBottom());
            linearLayout.addView(textView2, new LinearLayout.LayoutParams(-1, -2));
            this.f1295b = new EditText(this);
            this.f1295b.setLines(2);
            linearLayout.addView(this.f1295b, new LinearLayout.LayoutParams(-1, -2));
        }
        LinearLayout linearLayout2 = new LinearLayout(this);
        linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout2.setPadding(linearLayout2.getPaddingLeft(), 10, linearLayout2.getPaddingRight(), linearLayout2.getPaddingBottom());
        Button button = new Button(this);
        button.setText(17039379);
        button.setOnClickListener(new d(this, crashResources));
        linearLayout2.addView(button, new LinearLayout.LayoutParams(-1, -2, 1.0f));
        Button button2 = new Button(this);
        button2.setText(17039369);
        button2.setOnClickListener(new e(this));
        linearLayout2.addView(button2, new LinearLayout.LayoutParams(-1, -2, 1.0f));
        linearLayout.addView(linearLayout2, new LinearLayout.LayoutParams(-1, -2));
        setContentView(linearLayout);
        int i2 = crashResources.getInt("RES_DIALOG_TITLE");
        if (i2 != 0) {
            setTitle(i2);
        }
        int i3 = crashResources.getInt("RES_DIALOG_ICON");
        if (i3 != 0) {
            getWindow().setFeatureDrawableResource(3, i3);
        } else {
            getWindow().setFeatureDrawableResource(3, 17301543);
        }
        ((NotificationManager) getSystemService("notification")).cancel(666);
    }
}
