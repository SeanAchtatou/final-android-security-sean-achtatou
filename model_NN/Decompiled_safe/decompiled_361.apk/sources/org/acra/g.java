package org.acra;

import java.io.File;
import java.io.FilenameFilter;

final class g implements FilenameFilter {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ErrorReporter f1306a;

    g(ErrorReporter errorReporter) {
        this.f1306a = errorReporter;
    }

    public final boolean accept(File file, String str) {
        return str.endsWith(".stacktrace");
    }
}
