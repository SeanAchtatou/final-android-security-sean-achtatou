package org.acra.a;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.acra.ReportingInteractionMode;

@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface a {
    String a();

    String b() default "";

    ReportingInteractionMode c() default ReportingInteractionMode.SILENT;

    int d() default 0;

    int e() default 17301543;

    int f() default 0;

    int g() default 0;

    int h() default 0;

    int i() default 17301624;

    int j() default 0;

    int k() default 0;

    int l() default 0;

    int m() default 0;

    String n() default "";

    int o() default 0;
}
