package org.acra;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Process;
import android.os.StatFs;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.TreeSet;

public class ErrorReporter implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1296a = ACRA.LOG_TAG;
    private static String d = "";
    private static ErrorReporter f;
    private static Uri k;

    /* renamed from: b  reason: collision with root package name */
    private Properties f1297b = new Properties();
    private Map c = new HashMap();
    private Thread.UncaughtExceptionHandler e;
    /* access modifiers changed from: private */
    public Context g;
    private String h;
    private ReportingInteractionMode i = ReportingInteractionMode.SILENT;
    /* access modifiers changed from: private */
    public Bundle j = new Bundle();

    static void a(Uri uri) {
        k = uri;
    }

    @Deprecated
    public void addCustomData(String str, String str2) {
        this.c.put(str, str2);
    }

    public final String a(String str, String str2) {
        return (String) this.c.put(str, str2);
    }

    private String e() {
        String str = "";
        Iterator it = this.c.keySet().iterator();
        while (true) {
            String str2 = str;
            if (!it.hasNext()) {
                return str2;
            }
            String str3 = (String) it.next();
            str = String.valueOf(str2) + str3 + " = " + ((String) this.c.get(str3)) + "\n";
        }
    }

    public static ErrorReporter a() {
        if (f == null) {
            f = new ErrorReporter();
        }
        return f;
    }

    public final void a(Context context) {
        if (this.e == null) {
            this.e = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(this);
            this.g = context;
            this.h = c.a(this.g.getResources().getConfiguration());
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        i a2 = a(th, this.i);
        if (this.i == ReportingInteractionMode.TOAST) {
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e2) {
                Log.e(f1296a, "Error : ", e2);
            }
        }
        if (a2 != null) {
            while (a2.isAlive()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e3) {
                    Log.e(f1296a, "Error : ", e3);
                }
            }
        }
        if (this.i == ReportingInteractionMode.SILENT) {
            this.e.uncaughtException(thread, th);
            return;
        }
        try {
            Log.e(f1296a, ((Object) this.g.getPackageManager().getApplicationInfo(this.g.getPackageName(), 0).loadLabel(this.g.getPackageManager())) + " fatal error : " + th.getMessage(), th);
        } catch (PackageManager.NameNotFoundException e4) {
            Log.e(f1296a, "Error : ", e4);
        } finally {
            Process.killProcess(Process.myPid());
            System.exit(10);
        }
    }

    private i a(Throwable th, ReportingInteractionMode reportingInteractionMode) {
        boolean z;
        ReportingInteractionMode reportingInteractionMode2;
        Exception exc;
        if (reportingInteractionMode == null) {
            z = false;
            reportingInteractionMode2 = this.i;
        } else if (reportingInteractionMode != ReportingInteractionMode.SILENT || this.i == ReportingInteractionMode.SILENT) {
            z = false;
            reportingInteractionMode2 = reportingInteractionMode;
        } else {
            z = true;
            reportingInteractionMode2 = reportingInteractionMode;
        }
        if (th == null) {
            exc = new Exception("Report requested by developer");
        } else {
            exc = th;
        }
        if (reportingInteractionMode2 == ReportingInteractionMode.TOAST) {
            new f(this).start();
        }
        Context context = this.g;
        try {
            this.f1297b.put("entry.22.single", this.h);
            this.f1297b.put("entry.23.single", c.a(context.getResources().getConfiguration()));
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (packageInfo != null) {
                this.f1297b.put("entry.0.single", packageInfo.versionName != null ? "'" + packageInfo.versionName : "not set");
            } else {
                this.f1297b.put("entry.1.single", "Package info unavailable");
            }
            this.f1297b.put("entry.1.single", context.getPackageName());
            this.f1297b.put("entry.3.single", Build.MODEL);
            this.f1297b.put("entry.4.single", "'" + Build.VERSION.RELEASE);
            this.f1297b.put("entry.5.single", Build.BOARD);
            this.f1297b.put("entry.6.single", Build.BRAND);
            this.f1297b.put("entry.7.single", Build.DEVICE);
            this.f1297b.put("entry.8.single", Build.DISPLAY);
            this.f1297b.put("entry.9.single", Build.FINGERPRINT);
            this.f1297b.put("entry.10.single", Build.HOST);
            this.f1297b.put("entry.11.single", Build.ID);
            this.f1297b.put("entry.12.single", Build.MODEL);
            this.f1297b.put("entry.13.single", Build.PRODUCT);
            this.f1297b.put("entry.14.single", Build.TAGS);
            this.f1297b.put("entry.15.single", new StringBuilder().append(Build.TIME).toString());
            this.f1297b.put("entry.16.single", Build.TYPE);
            this.f1297b.put("entry.17.single", Build.USER);
            Properties properties = this.f1297b;
            StringBuilder sb = new StringBuilder();
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            properties.put("entry.18.single", sb.append(((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize())).toString());
            Properties properties2 = this.f1297b;
            StringBuilder sb2 = new StringBuilder();
            StatFs statFs2 = new StatFs(Environment.getDataDirectory().getPath());
            properties2.put("entry.19.single", sb2.append(((long) statFs2.getAvailableBlocks()) * ((long) statFs2.getBlockSize())).toString());
            this.f1297b.put("entry.2.single", context.getFilesDir().getAbsolutePath());
            Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            Properties properties3 = this.f1297b;
            DisplayMetrics displayMetrics = new DisplayMetrics();
            defaultDisplay.getMetrics(displayMetrics);
            StringBuilder sb3 = new StringBuilder();
            sb3.append("width=").append(defaultDisplay.getWidth()).append(10).append("height=").append(defaultDisplay.getHeight()).append(10).append("pixelFormat=").append(defaultDisplay.getPixelFormat()).append(10).append("refreshRate=").append(defaultDisplay.getRefreshRate()).append("fps").append(10).append("metrics.density=x").append(displayMetrics.density).append(10).append("metrics.scaledDensity=x").append(displayMetrics.scaledDensity).append(10).append("metrics.widthPixels=").append(displayMetrics.widthPixels).append(10).append("metrics.heightPixels=").append(displayMetrics.heightPixels).append(10).append("metrics.xdpi=").append(displayMetrics.xdpi).append(10).append("metrics.ydpi=").append(displayMetrics.ydpi);
            properties3.put("entry.24.single", sb3.toString());
            Time time = new Time();
            time.setToNow();
            this.f1297b.put("entry.26.single", time.format3339(false));
            this.f1297b.put("entry.20.single", e());
        } catch (Exception e2) {
            Log.e(f1296a, "Error while retrieving crash data", e2);
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        exc.printStackTrace(printWriter);
        Log.getStackTraceString(exc);
        for (Throwable cause = exc.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
        }
        this.f1297b.put("entry.21.single", stringWriter.toString());
        printWriter.close();
        String f2 = f();
        if (reportingInteractionMode2 == ReportingInteractionMode.SILENT || reportingInteractionMode2 == ReportingInteractionMode.TOAST) {
            i iVar = new i(this, z);
            iVar.start();
            return iVar;
        }
        if (reportingInteractionMode2 == ReportingInteractionMode.NOTIFICATION) {
            b(f2);
        }
        return null;
    }

    public i handleSilentException(Throwable th) {
        this.f1297b.put("silent", "true");
        return a(th, ReportingInteractionMode.SILENT);
    }

    private void b(String str) {
        NotificationManager notificationManager = (NotificationManager) this.g.getSystemService("notification");
        int i2 = 17301624;
        if (this.j.containsKey("RES_NOTIF_ICON")) {
            i2 = this.j.getInt("RES_NOTIF_ICON");
        }
        Notification notification = new Notification(i2, this.g.getText(this.j.getInt("RES_NOTIF_TICKER_TEXT")), System.currentTimeMillis());
        CharSequence text = this.g.getText(this.j.getInt("RES_NOTIF_TITLE"));
        CharSequence text2 = this.g.getText(this.j.getInt("RES_NOTIF_TEXT"));
        Intent intent = new Intent(this.g, CrashReportDialog.class);
        intent.putExtra("REPORT_FILE_NAME", str);
        notification.setLatestEventInfo(this.g, text, text2, PendingIntent.getActivity(this.g, 0, intent, 0));
        notificationManager.notify(666, notification);
    }

    private String f() {
        try {
            Log.d(f1296a, "Writing crash report file.");
            Time time = new Time();
            time.setToNow();
            String str = time.toMillis(false) + (this.f1297b.getProperty("silent") != null ? "-silent" : "") + ".stacktrace";
            FileOutputStream openFileOutput = this.g.openFileOutput(str, 0);
            if (h()) {
                this.f1297b.storeToXML(openFileOutput, "");
            } else {
                this.f1297b.store(openFileOutput, "");
            }
            openFileOutput.close();
            return str;
        } catch (Exception e2) {
            Log.e(f1296a, "An error occured while writing the report file...", e2);
            return null;
        }
    }

    private String[] g() {
        File filesDir = this.g.getFilesDir();
        if (filesDir != null) {
            Log.d(f1296a, "Looking for error files in " + filesDir.getAbsolutePath());
            return filesDir.list(new g(this));
        }
        Log.w(f1296a, "Application files directory does not exist! The application may not be installed correctly. Please try reinstalling.");
        return new String[0];
    }

    /* access modifiers changed from: package-private */
    public final void a(Context context, String str, boolean z) {
        String str2;
        try {
            String[] g2 = g();
            TreeSet treeSet = new TreeSet();
            treeSet.addAll(Arrays.asList(g2));
            if (g2 != null && g2.length > 0) {
                Properties properties = new Properties();
                int i2 = 0;
                Iterator it = treeSet.iterator();
                while (it.hasNext()) {
                    String str3 = (String) it.next();
                    if (!z || (z && str3.contains("-silent"))) {
                        if (i2 < 5) {
                            FileInputStream openFileInput = context.openFileInput(str3);
                            if (h()) {
                                properties.loadFromXML(openFileInput);
                            } else {
                                properties.load(openFileInput);
                            }
                            openFileInput.close();
                            if (str3.equals(str) || (i2 == treeSet.size() - 1 && !"".equals(d))) {
                                String property = properties.getProperty("entry.20.single");
                                if (property != null) {
                                    String.valueOf(property) + "\n";
                                }
                                properties.put("entry.25.single", d);
                            }
                            properties.put("pageNumber", "0");
                            properties.put("backupCache", "");
                            properties.put("submit", "Envoyer");
                            URL url = new URL(k.toString());
                            Log.d(f1296a, "Connect to " + url.toString());
                            b.a(properties, url);
                            new File(context.getFilesDir(), str3).delete();
                        }
                        i2++;
                    }
                }
            }
            d = "";
        } catch (Exception e2) {
            e2.printStackTrace();
        } finally {
            str2 = "";
            d = str2;
        }
    }

    private static boolean h() {
        return i() < 5;
    }

    /* access modifiers changed from: package-private */
    public final void a(ReportingInteractionMode reportingInteractionMode) {
        this.i = reportingInteractionMode;
    }

    public final void b() {
        boolean z;
        String str;
        String[] g2 = g();
        if (g2 != null && g2.length > 0) {
            int length = g2.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    z = true;
                    break;
                } else if (!g2[i2].contains("-silent")) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (this.i == ReportingInteractionMode.SILENT || this.i == ReportingInteractionMode.TOAST || (this.i == ReportingInteractionMode.NOTIFICATION && z)) {
                if (this.i == ReportingInteractionMode.TOAST && !z) {
                    Toast.makeText(this.g, this.j.getInt("RES_TOAST_TEXT"), 1).show();
                }
                new i(this).start();
            } else if (this.i == ReportingInteractionMode.NOTIFICATION) {
                ErrorReporter a2 = a();
                if (g2 == null || g2.length <= 0) {
                    str = null;
                } else {
                    int length2 = g2.length - 1;
                    while (true) {
                        if (length2 < 0) {
                            str = g2[g2.length - 1];
                            break;
                        } else if (!g2[length2].contains("-silent")) {
                            str = g2[length2];
                            break;
                        } else {
                            length2--;
                        }
                    }
                }
                a2.b(str);
            }
        }
    }

    public final void c() {
        String[] g2 = g();
        if (g2 != null) {
            for (String str : g2) {
                if (str.contains("-silent") || !str.contains("-silent")) {
                    new File(this.g.getFilesDir(), str).delete();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Bundle bundle) {
        this.j = bundle;
    }

    public final void d() {
        if (this.g != null) {
            Log.d(ACRA.LOG_TAG, "ACRA is disabled for " + this.g.getPackageName());
        } else {
            Log.d(ACRA.LOG_TAG, "ACRA is disabled.");
        }
        if (this.e != null) {
            Thread.setDefaultUncaughtExceptionHandler(this.e);
        }
    }

    public static void a(String str) {
        d = str;
    }

    private static int i() {
        try {
            return Build.VERSION.class.getField("SDK_INT").getInt(null);
        } catch (SecurityException e2) {
            return Integer.parseInt(Build.VERSION.SDK);
        } catch (NoSuchFieldException e3) {
            return Integer.parseInt(Build.VERSION.SDK);
        } catch (IllegalArgumentException e4) {
            return Integer.parseInt(Build.VERSION.SDK);
        } catch (IllegalAccessException e5) {
            return Integer.parseInt(Build.VERSION.SDK);
        }
    }
}
