package org.acra;

import android.app.Application;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import org.acra.a.a;

public class ACRA {
    private static /* synthetic */ int[] $SWITCH_TABLE$org$acra$ReportingInteractionMode = null;
    protected static final String LOG_TAG = ACRA.class.getSimpleName();
    static final int NOTIF_CRASH_ID = 666;
    public static final String PREF_DISABLE_ACRA = "acra.disable";
    public static final String PREF_ENABLE_ACRA = "acra.enable";
    static final String RES_DIALOG_COMMENT_PROMPT = "RES_DIALOG_COMMENT_PROMPT";
    static final String RES_DIALOG_ICON = "RES_DIALOG_ICON";
    static final String RES_DIALOG_OK_TOAST = "RES_DIALOG_OK_TOAST";
    static final String RES_DIALOG_TEXT = "RES_DIALOG_TEXT";
    static final String RES_DIALOG_TITLE = "RES_DIALOG_TITLE";
    static final String RES_NOTIF_ICON = "RES_NOTIF_ICON";
    static final String RES_NOTIF_TEXT = "RES_NOTIF_TEXT";
    static final String RES_NOTIF_TICKER_TEXT = "RES_NOTIF_TICKER_TEXT";
    static final String RES_NOTIF_TITLE = "RES_NOTIF_TITLE";
    static final String RES_TOAST_TEXT = "RES_TOAST_TEXT";
    private static Application mApplication;
    private static Bundle mCrashResources;
    private static SharedPreferences.OnSharedPreferenceChangeListener mPrefListener;
    private static a mReportsCrashes;

    static /* synthetic */ int[] $SWITCH_TABLE$org$acra$ReportingInteractionMode() {
        int[] iArr = $SWITCH_TABLE$org$acra$ReportingInteractionMode;
        if (iArr == null) {
            iArr = new int[ReportingInteractionMode.values().length];
            try {
                iArr[ReportingInteractionMode.NOTIFICATION.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ReportingInteractionMode.SILENT.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ReportingInteractionMode.TOAST.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$org$acra$ReportingInteractionMode = iArr;
        }
        return iArr;
    }

    public static void init(Application application) {
        boolean z;
        mApplication = application;
        a aVar = (a) application.getClass().getAnnotation(a.class);
        mReportsCrashes = aVar;
        if (aVar != null) {
            SharedPreferences aCRASharedPreferences = getACRASharedPreferences();
            Log.d(LOG_TAG, "Set OnSharedPreferenceChangeListener.");
            mPrefListener = new a();
            try {
                z = aCRASharedPreferences.getBoolean(PREF_DISABLE_ACRA, !aCRASharedPreferences.getBoolean(PREF_ENABLE_ACRA, true));
            } catch (Exception e) {
                z = false;
            }
            if (z) {
                Log.d(LOG_TAG, "ACRA is disabled for " + mApplication.getPackageName() + ".");
                return;
            }
            try {
                initAcra();
            } catch (j e2) {
                Log.w(LOG_TAG, "Error : ", e2);
            }
            aCRASharedPreferences.registerOnSharedPreferenceChangeListener(mPrefListener);
        }
    }

    /* access modifiers changed from: private */
    public static void initAcra() {
        initCrashResources();
        Log.d(LOG_TAG, "ACRA is enabled for " + mApplication.getPackageName() + ", intializing...");
        ErrorReporter a2 = ErrorReporter.a();
        ErrorReporter.a(getFormUri());
        a2.a(mReportsCrashes.c());
        a2.a(getCrashResources());
        a2.a(mApplication.getApplicationContext());
        a2.b();
    }

    static void initCrashResources() {
        mCrashResources = new Bundle();
        switch ($SWITCH_TABLE$org$acra$ReportingInteractionMode()[mReportsCrashes.c().ordinal()]) {
            case 2:
                if (mReportsCrashes.k() == 0 || mReportsCrashes.l() == 0 || mReportsCrashes.j() == 0 || mReportsCrashes.g() == 0) {
                    throw new j("NOTIFICATION mode: you have to define at least the resNotifTickerText, resNotifTitle, resNotifText, resDialogText parameters in your application @ReportsCrashes() annotation.");
                }
                mCrashResources.putInt(RES_NOTIF_TICKER_TEXT, mReportsCrashes.k());
                mCrashResources.putInt(RES_NOTIF_TITLE, mReportsCrashes.l());
                mCrashResources.putInt(RES_NOTIF_TEXT, mReportsCrashes.j());
                mCrashResources.putInt(RES_DIALOG_TEXT, mReportsCrashes.g());
                mCrashResources.putInt(RES_NOTIF_ICON, mReportsCrashes.i());
                mCrashResources.putInt(RES_DIALOG_ICON, mReportsCrashes.e());
                mCrashResources.putInt(RES_DIALOG_TITLE, mReportsCrashes.h());
                mCrashResources.putInt(RES_DIALOG_COMMENT_PROMPT, mReportsCrashes.d());
                mCrashResources.putInt(RES_DIALOG_OK_TOAST, mReportsCrashes.f());
                return;
            case 3:
                if (mReportsCrashes.m() == 0) {
                    throw new j("TOAST mode: you have to define the resToastText parameter in your application @ReportsCrashes() annotation.");
                }
                mCrashResources.putInt(RES_TOAST_TEXT, mReportsCrashes.m());
                return;
            default:
                return;
        }
    }

    static Bundle getCrashResources() {
        return mCrashResources;
    }

    private static Uri getFormUri() {
        if (mReportsCrashes.b().equals("")) {
            return Uri.parse("https://spreadsheets.google.com/formResponse?formkey=" + mReportsCrashes.a() + "&amp;ifq");
        }
        return Uri.parse(mReportsCrashes.b());
    }

    public static SharedPreferences getACRASharedPreferences() {
        if (!"".equals(mReportsCrashes.n())) {
            Log.d(LOG_TAG, "Retrieve SharedPreferences " + mReportsCrashes.n());
            return mApplication.getSharedPreferences(mReportsCrashes.n(), mReportsCrashes.o());
        }
        Log.d(LOG_TAG, "Retrieve application default SharedPreferences.");
        return PreferenceManager.getDefaultSharedPreferences(mApplication);
    }
}
