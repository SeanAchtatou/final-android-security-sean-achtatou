package org.acra;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

final class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CrashReportDialog f1302a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ Bundle f1303b;

    d(CrashReportDialog crashReportDialog, Bundle bundle) {
        this.f1302a = crashReportDialog;
        this.f1303b = bundle;
    }

    public final void onClick(View view) {
        ErrorReporter a2 = ErrorReporter.a();
        if (this.f1302a.f1295b != null) {
            ErrorReporter.a(this.f1302a.f1295b.getText().toString());
        }
        a2.getClass();
        i iVar = new i(a2);
        iVar.a(this.f1302a.f1294a);
        iVar.start();
        int i = this.f1303b.getInt("RES_DIALOG_OK_TOAST");
        if (i != 0) {
            Toast.makeText(this.f1302a.getApplicationContext(), i, 1).show();
        }
        this.f1302a.finish();
    }
}
