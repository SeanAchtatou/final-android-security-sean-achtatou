package org.a.a.b;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static int f1292a = 0;

    /* renamed from: b  reason: collision with root package name */
    private static int f1293b = 0;

    public static int a() {
        if (f1292a == 0) {
            String substring = System.getProperty("os.name").substring(0, 3);
            if (substring.compareToIgnoreCase("win") == 0) {
                f1292a = 1;
            } else if (substring.compareToIgnoreCase("lin") == 0) {
                f1292a = 2;
            } else {
                f1292a = -1;
            }
        }
        return f1292a;
    }
}
