package org.a.a.a.a;

public final class a extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private final b f1284a;

    public a(b bVar) {
        super("AWT-DataTransferThread");
        setDaemon(true);
        this.f1284a = bVar;
    }

    public final void run() {
        synchronized (this) {
            notifyAll();
        }
    }

    public final void start() {
        synchronized (this) {
            super.start();
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
