package org.a.a.a.b.a;

import java.security.PrivilegedAction;
import java.util.Locale;
import java.util.ResourceBundle;

final class b implements PrivilegedAction {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f1290a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ Locale f1291b;
    private final /* synthetic */ ClassLoader c = null;

    b(String str, Locale locale) {
        this.f1290a = str;
        this.f1291b = locale;
    }

    public final Object run() {
        return ResourceBundle.getBundle(this.f1290a, this.f1291b, this.c != null ? this.c : ClassLoader.getSystemClassLoader());
    }
}
