package org.a.a.a.b.a;

import java.security.AccessController;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static ResourceBundle f1289a;

    static {
        f1289a = null;
        try {
            f1289a = a(Locale.getDefault(), "org.apache.harmony.awt.internal.nls.messages");
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static String a(String str) {
        if (f1289a == null) {
            return str;
        }
        try {
            return f1289a.getString(str);
        } catch (MissingResourceException e) {
            return "Missing message: " + str;
        }
    }

    public static String a(String str, Object obj) {
        return a(str, new Object[]{obj});
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0057  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(java.lang.String r10, java.lang.Object[] r11) {
        /*
            r9 = 123(0x7b, float:1.72E-43)
            r8 = 1
            r5 = 0
            java.util.ResourceBundle r0 = org.a.a.a.b.a.a.f1289a
            if (r0 == 0) goto L_0x0041
            java.util.ResourceBundle r0 = org.a.a.a.b.a.a.f1289a     // Catch:{ MissingResourceException -> 0x0040 }
            java.lang.String r0 = r0.getString(r10)     // Catch:{ MissingResourceException -> 0x0040 }
        L_0x000e:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            int r2 = r0.length()
            int r3 = r11.length
            int r3 = r3 * 20
            int r2 = r2 + r3
            r1.<init>(r2)
            int r2 = r11.length
            java.lang.String[] r2 = new java.lang.String[r2]
            r3 = r5
        L_0x001f:
            int r4 = r11.length
            if (r3 < r4) goto L_0x0043
            int r3 = r0.indexOf(r9, r5)
            r4 = r3
            r3 = r5
        L_0x0028:
            if (r4 >= 0) goto L_0x0057
            int r2 = r0.length()
            if (r3 >= r2) goto L_0x003b
            int r2 = r0.length()
            java.lang.String r0 = r0.substring(r3, r2)
            r1.append(r0)
        L_0x003b:
            java.lang.String r0 = r1.toString()
            return r0
        L_0x0040:
            r0 = move-exception
        L_0x0041:
            r0 = r10
            goto L_0x000e
        L_0x0043:
            r4 = r11[r3]
            if (r4 != 0) goto L_0x004e
            java.lang.String r4 = "<null>"
            r2[r3] = r4
        L_0x004b:
            int r3 = r3 + 1
            goto L_0x001f
        L_0x004e:
            r4 = r11[r3]
            java.lang.String r4 = r4.toString()
            r2[r3] = r4
            goto L_0x004b
        L_0x0057:
            if (r4 == 0) goto L_0x0078
            int r5 = r4 - r8
            char r5 = r0.charAt(r5)
            r6 = 92
            if (r5 != r6) goto L_0x0078
            if (r4 == r8) goto L_0x006e
            int r5 = r4 - r8
            java.lang.String r3 = r0.substring(r3, r5)
            r1.append(r3)
        L_0x006e:
            r1.append(r9)
            int r3 = r4 + 1
        L_0x0073:
            int r4 = r0.indexOf(r9, r3)
            goto L_0x0028
        L_0x0078:
            int r5 = r0.length()
            r6 = 3
            int r5 = r5 - r6
            if (r4 <= r5) goto L_0x0090
            int r4 = r0.length()
            java.lang.String r3 = r0.substring(r3, r4)
            r1.append(r3)
            int r3 = r0.length()
            goto L_0x0073
        L_0x0090:
            int r5 = r4 + 1
            char r5 = r0.charAt(r5)
            r6 = 10
            int r5 = java.lang.Character.digit(r5, r6)
            byte r5 = (byte) r5
            if (r5 < 0) goto L_0x00a9
            int r6 = r4 + 2
            char r6 = r0.charAt(r6)
            r7 = 125(0x7d, float:1.75E-43)
            if (r6 == r7) goto L_0x00b5
        L_0x00a9:
            int r5 = r4 + 1
            java.lang.String r3 = r0.substring(r3, r5)
            r1.append(r3)
            int r3 = r4 + 1
            goto L_0x0073
        L_0x00b5:
            java.lang.String r3 = r0.substring(r3, r4)
            r1.append(r3)
            int r3 = r2.length
            if (r5 < r3) goto L_0x00c7
            java.lang.String r3 = "<missing argument>"
            r1.append(r3)
        L_0x00c4:
            int r3 = r4 + 3
            goto L_0x0073
        L_0x00c7:
            r3 = r2[r5]
            r1.append(r3)
            goto L_0x00c4
        */
        throw new UnsupportedOperationException("Method not decompiled: org.a.a.a.b.a.a.a(java.lang.String, java.lang.Object[]):java.lang.String");
    }

    private static ResourceBundle a(Locale locale, String str) {
        try {
            return (ResourceBundle) AccessController.doPrivileged(new b(str, locale));
        } catch (MissingResourceException e) {
            return null;
        }
    }
}
