package org.a.a.a.a;

import org.a.a.b.a;

public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    private c f1285a = null;

    /* renamed from: b  reason: collision with root package name */
    private c f1286b = null;
    private a c = new a(this);

    protected b() {
        this.c.start();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.a.a.a.a.b a() {
        /*
            java.lang.Object r0 = org.a.a.a.b.b()
            monitor-enter(r0)
            boolean r1 = org.a.a.a.b.c()     // Catch:{ all -> 0x001e }
            if (r1 == 0) goto L_0x000e
            monitor-exit(r0)     // Catch:{ all -> 0x001e }
            r0 = 0
        L_0x000d:
            return r0
        L_0x000e:
            org.a.a.a.a.b r1 = org.a.a.a.b.a()     // Catch:{ all -> 0x001e }
            if (r1 != 0) goto L_0x001b
            org.a.a.a.a.b r1 = b()     // Catch:{ all -> 0x001e }
            org.a.a.a.b.a(r1)     // Catch:{ all -> 0x001e }
        L_0x001b:
            monitor-exit(r0)     // Catch:{ all -> 0x001e }
            r0 = r1
            goto L_0x000d
        L_0x001e:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x001e }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.a.a.a.a.b.a():org.a.a.a.a.b");
    }

    private static b b() {
        String str;
        switch (a.a()) {
            case 1:
                str = "org.apache.harmony.awt.datatransfer.windows.WinDTK";
                break;
            case 2:
                str = "org.apache.harmony.awt.datatransfer.linux.LinuxDTK";
                break;
            default:
                throw new RuntimeException(org.a.a.a.b.a.a.a("awt.4E"));
        }
        try {
            return (b) Class.forName(str).newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
