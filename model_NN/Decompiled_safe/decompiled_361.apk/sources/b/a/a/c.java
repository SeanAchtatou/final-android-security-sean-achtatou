package b.a.a;

import java.io.Serializable;
import java.util.Hashtable;

final class c implements Serializable, Cloneable {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f108a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public String f109b;
    /* access modifiers changed from: private */
    public Hashtable c;
    /* access modifiers changed from: private */
    public Hashtable d;

    c() {
        this.f108a = null;
        this.f109b = null;
        this.c = null;
        this.d = null;
    }

    private c(String str, String str2) {
        this.f108a = str;
        this.f109b = str2;
        this.c = new Hashtable();
        this.d = new Hashtable();
    }

    /* access modifiers changed from: package-private */
    public final boolean a(c cVar) {
        if (cVar == null) {
            return false;
        }
        return c().equals(cVar.c());
    }

    /* access modifiers changed from: package-private */
    public final String a() {
        return this.f108a;
    }

    /* access modifiers changed from: package-private */
    public final String b() {
        return this.f109b;
    }

    /* access modifiers changed from: package-private */
    public final String c() {
        return String.valueOf(this.f108a) + "/" + this.f109b;
    }

    /* access modifiers changed from: package-private */
    public final String a(String str) {
        return (String) this.c.get(str);
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, String str2) {
        String str3;
        if (str2 != null) {
            if (str2.charAt(0) == '\"' && str2.charAt(str2.length() - 1) == '\"') {
                str3 = str2.substring(1, str2.length() - 2);
            } else {
                str3 = str2;
            }
            if (str3.length() != 0) {
                this.c.put(str, str3);
            }
        }
    }

    public final Object clone() {
        c cVar = new c(this.f108a, this.f109b);
        cVar.c = (Hashtable) this.c.clone();
        cVar.d = (Hashtable) this.d.clone();
        return cVar;
    }
}
