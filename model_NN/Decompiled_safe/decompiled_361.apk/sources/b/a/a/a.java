package b.a.a;

import java.util.Hashtable;

final class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f106a;

    private a() {
    }

    static c a(String str) {
        if (f106a == null) {
            f106a = new a();
        }
        c cVar = new c();
        if (str != null) {
            b bVar = new b();
            cVar.f108a = b(str, bVar).toLowerCase();
            bVar.f107a = a(str, bVar.f107a);
            if (bVar.f107a >= str.length() || str.charAt(bVar.f107a) != '/') {
                throw new IllegalArgumentException();
            }
            bVar.f107a++;
            cVar.f109b = b(str, bVar).toLowerCase();
            a(str, cVar, bVar);
        }
        return cVar;
    }

    private static void a(String str, c cVar, b bVar) {
        cVar.c = new Hashtable();
        cVar.d = new Hashtable();
        while (true) {
            bVar.f107a = a(str, bVar.f107a);
            if (bVar.f107a < str.length()) {
                if (str.charAt(bVar.f107a) != ';') {
                    throw new IllegalArgumentException();
                }
                bVar.f107a++;
                String lowerCase = b(str, bVar).toLowerCase();
                bVar.f107a = a(str, bVar.f107a);
                if (bVar.f107a < str.length() && str.charAt(bVar.f107a) == '=') {
                    bVar.f107a++;
                    bVar.f107a = a(str, bVar.f107a);
                    if (bVar.f107a >= str.length()) {
                        throw new IllegalArgumentException();
                    }
                    cVar.c.put(lowerCase, str.charAt(bVar.f107a) == '\"' ? a(str, bVar) : b(str, bVar));
                }
            } else {
                return;
            }
        }
        throw new IllegalArgumentException();
    }

    private static String a(String str, b bVar) {
        StringBuilder sb = new StringBuilder();
        bVar.f107a++;
        boolean z = true;
        do {
            if (str.charAt(bVar.f107a) != '\"' || !z) {
                int i = bVar.f107a;
                bVar.f107a = i + 1;
                char charAt = str.charAt(i);
                if (!z) {
                    z = true;
                } else if (charAt == '\\') {
                    z = false;
                }
                if (z) {
                    sb.append(charAt);
                }
            } else {
                bVar.f107a++;
                return sb.toString();
            }
        } while (bVar.f107a != str.length());
        throw new IllegalArgumentException();
    }

    private static String b(String str, b bVar) {
        StringBuilder sb = new StringBuilder();
        bVar.f107a = a(str, bVar.f107a);
        if (bVar.f107a >= str.length() || a(str.charAt(bVar.f107a))) {
            throw new IllegalArgumentException();
        }
        do {
            int i = bVar.f107a;
            bVar.f107a = i + 1;
            sb.append(str.charAt(i));
            if (bVar.f107a >= str.length() || !b(str.charAt(bVar.f107a))) {
            }
        } while (!a(str.charAt(bVar.f107a)));
        return sb.toString();
    }

    private static int a(String str, int i) {
        int i2 = i;
        while (i2 < str.length() && !b(str.charAt(i2))) {
            i2++;
        }
        return i2;
    }

    private static boolean a(char c) {
        return c == '(' || c == ')' || c == '[' || c == ']' || c == '<' || c == '>' || c == '@' || c == ',' || c == ';' || c == ':' || c == '\\' || c == '\"' || c == '/' || c == '?' || c == '=';
    }

    private static boolean b(char c) {
        return c >= '!' && c <= '~';
    }
}
