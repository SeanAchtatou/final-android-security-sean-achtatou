package b.a.a;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Reader;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.util.Enumeration;
import org.a.a.a.a.b;
import org.a.a.a.b.a.a;

public final class d implements Externalizable, Cloneable {
    @Deprecated

    /* renamed from: a  reason: collision with root package name */
    private static d f110a = new d("text/plain; charset=unicode; class=java.io.InputStream", "Plain Text");

    /* renamed from: b  reason: collision with root package name */
    private static d f111b = new d("application/x-java-serialized-object; class=java.lang.String", "Unicode String");
    private static d c = new d("application/x-java-file-list; class=java.util.List", "application/x-java-file-list");
    private static final String[] d = {"text/sgml", "text/xml", "text/html", "text/rtf", "text/enriched", "text/richtext", "text/uri-list", "text/tab-separated-values", "text/t140", "text/rfc822-headers", "text/parityfec", "text/directory", "text/css", "text/calendar", "application/x-java-serialized-object", "text/plain"};
    private static d e = null;
    private String f;
    private Class g;
    private c h;

    private static boolean a(String str) {
        try {
            return Charset.isSupported(str);
        } catch (IllegalCharsetNameException e2) {
            return false;
        }
    }

    public d() {
        this.h = null;
        this.f = null;
        this.g = null;
    }

    private d(String str, String str2) {
        try {
            this.h = a.a(str);
            if (str2 != null) {
                this.f = str2;
            } else {
                this.f = String.valueOf(this.h.a()) + '/' + this.h.b();
            }
            String a2 = this.h.a("class");
            if (a2 == null) {
                a2 = "java.io.InputStream";
                this.h.a("class", a2);
            }
            this.g = Class.forName(a2);
        } catch (IllegalArgumentException e2) {
            throw new IllegalArgumentException(a.a("awt.16D", str));
        } catch (ClassNotFoundException e3) {
            throw new IllegalArgumentException(a.a("awt.16C", this.h.a("class")), e3);
        }
    }

    private String a() {
        boolean z;
        if (this.h != null) {
            String c2 = this.h.c();
            if (!(c2.equals("text/rtf") || c2.equals("text/tab-separated-values") || c2.equals("text/t140") || c2.equals("text/rfc822-headers") || c2.equals("text/parityfec"))) {
                String a2 = this.h.a("charset");
                String c3 = this.h.c();
                if (c3.equals("text/sgml") || c3.equals("text/xml") || c3.equals("text/html") || c3.equals("text/enriched") || c3.equals("text/richtext") || c3.equals("text/uri-list") || c3.equals("text/directory") || c3.equals("text/css") || c3.equals("text/calendar") || c3.equals("application/x-java-serialized-object") || c3.equals("text/plain")) {
                    z = true;
                } else {
                    z = false;
                }
                if (z && (a2 == null || a2.length() == 0)) {
                    b.a();
                    return "unicode";
                } else if (a2 == null) {
                    return "";
                } else {
                    return a2;
                }
            }
        }
        return "";
    }

    private String b() {
        if (this.h == null) {
            return null;
        }
        c cVar = this.h;
        StringBuilder sb = new StringBuilder();
        sb.append(cVar.c());
        Enumeration keys = cVar.c.keys();
        while (keys.hasMoreElements()) {
            String str = (String) keys.nextElement();
            sb.append("; ");
            sb.append(str);
            sb.append("=\"");
            sb.append((String) cVar.c.get(str));
            sb.append('\"');
        }
        return sb.toString();
    }

    public final synchronized void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.f);
        objectOutput.writeObject(this.h);
    }

    public final synchronized void readExternal(ObjectInput objectInput) {
        this.f = (String) objectInput.readObject();
        this.h = (c) objectInput.readObject();
        this.g = this.h != null ? Class.forName(this.h.a("class")) : null;
    }

    public final Object clone() {
        c cVar;
        d dVar = new d();
        dVar.f = this.f;
        dVar.g = this.g;
        if (this.h != null) {
            cVar = (c) this.h.clone();
        } else {
            cVar = null;
        }
        dVar.h = cVar;
        return dVar;
    }

    public final String toString() {
        return String.valueOf(getClass().getName()) + "[MimeType=(" + b() + ");humanPresentableName=" + this.f + "]";
    }

    public final boolean equals(Object obj) {
        if (obj == null || !(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        if (dVar == this) {
            return true;
        }
        if (dVar == null) {
            return false;
        }
        if (this.h == null) {
            return dVar.h == null;
        }
        if (!this.h.a(dVar.h) || !this.g.equals(dVar.g)) {
            return false;
        }
        if (!this.h.a().equals("text") || c()) {
            return true;
        }
        String a2 = a();
        String a3 = dVar.a();
        return (!a(a2) || !a(a3)) ? a2.equalsIgnoreCase(a3) : Charset.forName(a2).equals(Charset.forName(a3));
    }

    public final int hashCode() {
        String str = String.valueOf(this.h.c()) + ";class=" + this.g.getName();
        if (this.h.a().equals("text") && !c()) {
            str = String.valueOf(str) + ";charset=" + a().toLowerCase();
        }
        return str.hashCode();
    }

    private boolean c() {
        return this.g != null && (this.g.equals(Reader.class) || this.g.equals(String.class) || this.g.equals(CharBuffer.class) || this.g.equals(char[].class));
    }
}
