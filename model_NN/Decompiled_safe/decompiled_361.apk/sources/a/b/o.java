package a.b;

import java.security.PrivilegedExceptionAction;

final class o implements PrivilegedExceptionAction {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Class f98a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f99b;

    o(Class cls, String str) {
        this.f98a = cls;
        this.f99b = str;
    }

    public final Object run() {
        return this.f98a.getResourceAsStream(this.f99b);
    }
}
