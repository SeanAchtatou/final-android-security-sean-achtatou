package a.b;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private Hashtable f87a = new Hashtable();

    public b() {
    }

    public b(String str) {
        a(str);
    }

    private void a(String str) {
        int length;
        int i;
        String substring;
        if (str != null && (length = str.length()) > 0) {
            int a2 = a(str, 0);
            while (a2 < length && str.charAt(a2) == ';') {
                int a3 = a(str, a2 + 1);
                if (a3 < length) {
                    int i2 = a3;
                    while (i2 < length && a(str.charAt(i2))) {
                        i2++;
                    }
                    String lowerCase = str.substring(a3, i2).toLowerCase(Locale.ENGLISH);
                    int a4 = a(str, i2);
                    if (a4 >= length || str.charAt(a4) != '=') {
                        throw new i("Couldn't find the '=' that separates a parameter name from its value.");
                    }
                    int a5 = a(str, a4 + 1);
                    if (a5 >= length) {
                        throw new i("Couldn't find a value for parameter named " + lowerCase);
                    }
                    char charAt = str.charAt(a5);
                    if (charAt == '\"') {
                        int i3 = a5 + 1;
                        if (i3 >= length) {
                            throw new i("Encountered unterminated quoted parameter value.");
                        }
                        int i4 = i3;
                        while (i4 < length) {
                            charAt = str.charAt(i4);
                            if (charAt == '\"') {
                                break;
                            }
                            if (charAt == '\\') {
                                i4++;
                            }
                            i4++;
                        }
                        if (charAt != '\"') {
                            throw new i("Encountered unterminated quoted parameter value.");
                        }
                        substring = c(str.substring(i3, i4));
                        i = i4 + 1;
                    } else if (a(charAt)) {
                        i = a5;
                        while (i < length && a(str.charAt(i))) {
                            i++;
                        }
                        substring = str.substring(a5, i);
                    } else {
                        throw new i("Unexpected character encountered at index " + a5);
                    }
                    this.f87a.put(lowerCase, substring);
                    a2 = a(str, i);
                } else {
                    return;
                }
            }
            if (a2 < length) {
                throw new i("More characters encountered in input than expected.");
            }
        }
    }

    public final String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.ensureCapacity(this.f87a.size() * 16);
        Enumeration keys = this.f87a.keys();
        while (keys.hasMoreElements()) {
            String str = (String) keys.nextElement();
            stringBuffer.append("; ");
            stringBuffer.append(str);
            stringBuffer.append('=');
            stringBuffer.append(b((String) this.f87a.get(str)));
        }
        return stringBuffer.toString();
    }

    private static boolean a(char c) {
        return c > ' ' && c < 127 && "()<>@,;:/[]?=\\\"".indexOf(c) < 0;
    }

    private static int a(String str, int i) {
        int length = str.length();
        int i2 = i;
        while (i2 < length && Character.isWhitespace(str.charAt(i2))) {
            i2++;
        }
        return i2;
    }

    private static String b(String str) {
        int length = str.length();
        boolean z = false;
        for (int i = 0; i < length && !z; i++) {
            z = !a(str.charAt(i));
        }
        if (!z) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.ensureCapacity((int) (((double) length) * 1.5d));
        stringBuffer.append('\"');
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            if (charAt == '\\' || charAt == '\"') {
                stringBuffer.append('\\');
            }
            stringBuffer.append(charAt);
        }
        stringBuffer.append('\"');
        return stringBuffer.toString();
    }

    private static String c(String str) {
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.ensureCapacity(length);
        boolean z = false;
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (!z && charAt != '\\') {
                stringBuffer.append(charAt);
            } else if (z) {
                stringBuffer.append(charAt);
                z = false;
            } else {
                z = true;
            }
        }
        return stringBuffer.toString();
    }
}
