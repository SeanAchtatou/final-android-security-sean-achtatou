package a.b;

import java.io.IOException;
import java.net.URL;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Enumeration;

final class p implements PrivilegedAction {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ClassLoader f100a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f101b;

    p(ClassLoader classLoader, String str) {
        this.f100a = classLoader;
        this.f101b = str;
    }

    public final Object run() {
        try {
            ArrayList arrayList = new ArrayList();
            Enumeration<URL> resources = this.f100a.getResources(this.f101b);
            while (resources != null && resources.hasMoreElements()) {
                URL nextElement = resources.nextElement();
                if (nextElement != null) {
                    arrayList.add(nextElement);
                }
            }
            if (arrayList.size() > 0) {
                return (URL[]) arrayList.toArray(new URL[arrayList.size()]);
            }
            return null;
        } catch (IOException | SecurityException e) {
            return null;
        }
    }
}
