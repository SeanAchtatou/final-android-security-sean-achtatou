package a.b;

import java.net.URL;
import java.security.PrivilegedExceptionAction;

final class l implements PrivilegedExceptionAction {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ URL f96a;

    l(URL url) {
        this.f96a = url;
    }

    public final Object run() {
        return this.f96a.openStream();
    }
}
