package a.b;

import b.a.a.d;
import java.io.OutputStream;

final class c implements j {

    /* renamed from: a  reason: collision with root package name */
    private g f88a = null;

    /* renamed from: b  reason: collision with root package name */
    private d[] f89b = null;
    private j c = null;

    public c(j jVar, g gVar) {
        this.f88a = gVar;
        this.c = jVar;
    }

    public final Object a(g gVar) {
        if (this.c != null) {
            return this.c.a(gVar);
        }
        return gVar.a();
    }

    public final void a(Object obj, String str, OutputStream outputStream) {
        if (this.c != null) {
            this.c.a(obj, str, outputStream);
            return;
        }
        throw new f("no DCH for content type " + this.f88a.b());
    }
}
